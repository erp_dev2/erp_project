﻿#region Update
/*
    28/12/2020 [WED/PHT] new apps
    30/12/2020 [WED/PHT] tambah approval
    30/12/2020 [WED/PHT] tambah kondisi Resign Date
    09/02/2021 [VIN/PHT] New Parprint
    22/03/2021 [TKG/PHT] menghilangkan validasi grade tidak boleh sama
    24/03/2021 [VIN/PHT] Penyesuaian Printout Grade yang dipilih boleh sama
    10/06/2021 [IBL/PHT] Ganti format tahun kolom Work Period I dan Work Period II menjadi tahun dan bulan
    14/06/2021 [DITA/PHT] saat save merit increase, insert juga ke menu ESADA untuk update data salary nya
    24/06/2021 [RDH/PHT] tambah validasi tab allowed apabila di uncheck maka Work Period II tidak dapat diisi dan transaksi tetap bisa di save
    30/06/2021 [BRI/PHT] tambah filter multi level code untuk merit increase
 *  28/06/2021 [ICA/PHT] work period berdasarkan perubahan grade
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpMeritIncrease : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsEmployeeUseGradeSalary = false
            ;
        private string 
            mMonthRangeEmployeeMerit = string.Empty,
            mJobTransferDemotionMerit = string.Empty,
            mLeaveCodeCLTP = string.Empty,
            mInitGrdSalaryCodeOld = string.Empty
            ;
        private bool
            mIsPPSUpdateEmpDivBasedOnDept = false,
            mIsPPSUseSection = false,
            mIsApprovalMeritIncreaseByLevelCode = false
            ;
        internal FrmEmpMeritIncreaseFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmpMeritIncrease(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Salary Merit Increase";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueOption(ref LueSalaryIncreaseCode, "SalaryIncrease");
                if (mIsEmployeeUseGradeSalary)
                {
                    Sl.SetLueGradeSalary(ref LueGrdSalaryCodeOld);
                    Sl.SetLueGradeSalary(ref LueGrdSalaryCodeNew);
                }
                else
                {
                    Sl.SetLueGrdLvlCode(ref LueGrdSalaryCodeOld);
                    Sl.SetLueGrdLvlCode(ref LueGrdSalaryCodeNew);
                }
                SetGrd();
                SetFormControl(mState.View);
                LueGrdSalaryDNo.Visible = false;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "Employee's Code",
                    "",
                    "Employee's Name",
                    "Division",
                    "Department",

                    //6-10
                    "Position",
                    "Level",
                    "CLTP",
                    "Demotion",
                    "Work Period I"+Environment.NewLine+"[year(s)]",

                    //11-15
                    "GradeSalaryDNo",
                    "Work Period II"+Environment.NewLine+"[year(s)]",
                    "Allowed",
                    "PPS#",
                    ""
                },
                new int[] 
                { 
                    0, 
                    120, 20, 200, 180, 200,
                    200, 100, 100, 100, 120,
                    0, 120, 100, 180, 20
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 8, 9, 13 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 11, 14, 15 });
        }

        protected override void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, MeeRemark,
                        DteStartDt, LueSalaryIncreaseCode, LueSiteCode, LueGrdSalaryCodeOld, LueGrdSalaryCodeNew,
                        LueGrdSalaryDNo
                    }, true);
                    //Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 15 });
                    BtnRefreshData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, DteStartDt, LueSiteCode, LueGrdSalaryDNo, LueGrdSalaryCodeOld,
                        LueGrdSalaryCodeNew, LueSalaryIncreaseCode
                    }, false);
                    ChkCancelInd.Checked = false;
                    //Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 12, 13 });
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 15 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnRefreshData.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    //Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 15 });
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mInitGrdSalaryCodeOld = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, MeeRemark,
                DteStartDt, LueSalaryIncreaseCode, LueSiteCode, LueGrdSalaryCodeOld, LueGrdSalaryCodeNew,
                LueGrdSalaryDNo
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            BtnRefreshData.Focus();
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpMeritIncreaseFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteStartDt);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpMeritIncreaseHdr());

            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(CancelPPS(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready()
                ;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblEmpMeritIncreaseHdr " +
                "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand CancelEmpMeritIncreaseHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpMeritIncreaseHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelPPS(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPS Set CancelInd='Y', ProposeCandidateDocNo = null, ProposeCandidateDNo = null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select PPSDocNo From TblEmpMeritIncreaseDtl Where DocNo = @DocNo And DocNo In (Select T.DocNo From TblEmpMeritIncreaseHdr T Where T.DocNo = @DocNo And T.Status = 'A')) And CancelInd='N';");

            SQL.AppendLine("Update TblEmployee A ");
            SQL.AppendLine("Inner Join TblPPS B On A.EmpCode=B.EmpCode And B.DocNo In (Select PPSDocNo From TblEmpMeritIncreaseDtl Where DocNo = @DocNo And DocNo In (Select T.DocNo From TblEmpMeritIncreaseHdr T Where T.DocNo = @DocNo And T.Status = 'A')) ");
            SQL.AppendLine("Set A.PositionStatusCode=B.PositionStatusCodeOld;");

            SQL.AppendLine("Update TblEmployee A ");
            SQL.AppendLine("Inner Join TblEmployeePPS B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And StartDt In ( ");
            SQL.AppendLine("        Select Max(StartDt) StartDt ");
            SQL.AppendLine("        From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=@EmpCode ");
            SQL.AppendLine("        And StartDt<@StartDt ");
            SQL.AppendLine("    ) ");
            if (mIsPPSUpdateEmpDivBasedOnDept)
                SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DeptCode=B.DeptCode, ");
            if (mIsPPSUpdateEmpDivBasedOnDept)
                SQL.AppendLine("    A.DivisionCode=C.DivisionCode, ");
            SQL.AppendLine("    A.PosCode=B.PosCode, ");
            SQL.AppendLine("    A.GrdLvlCode=B.GrdLvlCode, ");
            SQL.AppendLine("    A.EmploymentStatus=B.EmploymentStatus, ");
            SQL.AppendLine("    A.SystemType=B.SystemType, ");
            SQL.AppendLine("    A.PayrunPeriod=B.PayrunPeriod, ");
            SQL.AppendLine("    A.PGCode=B.PGCode, ");
            SQL.AppendLine("    A.SiteCode=B.SiteCode, ");
            if (mIsPPSUseSection)
                SQL.AppendLine("    A.SectionCode = B.SectionCode, ");
            SQL.AppendLine("    A.EntCode=( ");
            SQL.AppendLine("        Select T2.EntCode ");
            SQL.AppendLine("        From TblSite T1 ");
            SQL.AppendLine("        Inner Join TblProfitCenter T2 On T1.ProfitCenterCode=T2.ProfitCenterCode ");
            SQL.AppendLine("        Where T1.SiteCode=B.SiteCode ");
            SQL.AppendLine("    ), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpMeritIncreaseHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Delete From TblEmployeePPS Where StartDt=@StartDt And EmpCode=@EmpCode ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpMeritIncreaseHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblEmployeePPS Set ");
            SQL.AppendLine("    EndDt=Null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And StartDt In ( ");
            SQL.AppendLine("    Select StartDt From (");
            SQL.AppendLine("        Select Max(StartDt) StartDt ");
            SQL.AppendLine("        From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=@EmpCode ");
            SQL.AppendLine("        And StartDt<@StartDt ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpMeritIncreaseHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpMeritIncreaseHdr(DocNo);
                ShowEmpMeritIncreaseDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowEmpMeritIncreaseHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status when 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.StartDt, A.SalaryIncreaseCode, A.SiteCode, A.GrdSalaryCodeOld, ");
            SQL.AppendLine("A.GrdSalaryCodeNew, A.Remark ");
            SQL.AppendLine("From TblEmpMeritIncreaseHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "StartDt", 

                    //6-10
                    "SalaryIncreaseCode", "SiteCode", "GrdSalaryCodeOld", "GrdSalaryCodeNew", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueSalaryIncreaseCode, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueGrdSalaryCodeOld, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueGrdSalaryCodeNew, Sm.DrStr(dr, c[9]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        private void ShowEmpMeritIncreaseDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.EmpCode, A.PPSDocNo, B.EmpName, D.DivisionName, C.DeptName, E.PosName, ");
            SQL.AppendLine("F.LevelName, A.CLTPInd, A.DemotionInd, A.WorkPeriodOld, A.GrdSalaryDNo, Convert(Format(G.WorkPeriod+Cast(G.Month As int)/100, 2) Using utf8) WorkPeriodNew, ");
            SQL.AppendLine("A.AllowedInd ");
            SQL.AppendLine("From TblEmpMeritIncreaseDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblDivision D On B.DivisionCode = D.DivisionCode ");
            SQL.AppendLine("Left Join TblPosition E On B.PosCode = E.PosCode ");
            SQL.AppendLine("Left Join TblLevelHdr F On B.LevelCode = F.LevelCode ");
            SQL.AppendLine("Left Join TblGradeSalaryDtl G On A.GrdSalaryCode = G.GrdSalaryCode And A.GrdSalaryDNo = G.DNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo",
                    "EmpCode", "EmpName", "DivisionName", "DeptName", "PosName",
                    "LevelName", "CLTPInd", "DemotionInd", "WorkPeriodOld", "GrdSalaryDNo",
                    "WorkPeriodNew", "AllowedInd", "PPSDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty, PPSDocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpMeritIncrease", "TblEmpMeritIncreaseHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpMeritIncreaseHdr(DocNo));
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        PPSDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PPS", "TblPPS", (Row+1).ToString());
                        string EmployeeSalaryDNo = Sm.GetValue("Select Count(*) From TblEmployeeSalary Where EmpCode = @Param; ", Sm.GetGrdStr(Grd1, Row, 1));
                        if (EmployeeSalaryDNo.Length == 0) EmployeeSalaryDNo = "0";

                        cml.Add(SaveEmpMeritIncreaseDtl(DocNo, Row, PPSDocNo));
                        cml.Add(SavePPS(DocNo, Row, PPSDocNo));
                        cml.Add(SaveEmployeePPS(DocNo, Row));
                        if (Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8)) <= Decimal.Parse(Sm.GetValue("Select concat(replace(curdate(), '-', ''))")))
                            cml.Add(UpdateEmployee(DocNo, Row));
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                            cml.Add(SaveEmployeeSalary(DocNo, Row, EmployeeSalaryDNo));
                    }
                }

                cml.Add(UpdateEmpMeritIncreaseDtlPPSDocNo(DocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsLueEmpty(LueSalaryIncreaseCode, "Salary Increase") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LueGrdSalaryCodeOld, "Grade Old") ||
                Sm.IsLueEmpty(LueGrdSalaryCodeNew, "Grade New") ||
                IsGradeDuplicate() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGradeDuplicate()
        {
            //if (Sm.CompareStr(Sm.GetLue(LueGrdSalaryCodeNew), Sm.GetLue(LueGrdSalaryCodeOld)))
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Grade could not be the same.");
            //    LueGrdSalaryCodeNew.Focus();
            //    return true;
            //}
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee on the list.");
                BtnRefreshData.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdBool(Grd1, Row, 13))
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 12, false, "Work Period II is empty.")) return true;
                }
            }

            return false;
        }        

        private MySqlCommand SaveEmpMeritIncreaseHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var EmpLevelCode = string.Empty;

            if (mIsApprovalMeritIncreaseByLevelCode)
                EmpLevelCode = GetEmpLevelCodeDocApproval();

            SQL.AppendLine("Insert Into TblEmpMeritIncreaseHdr(DocNo, DocDt, Status, StartDt, ");
            SQL.AppendLine("SalaryIncreaseCode, SiteCode, GrdSalaryCodeOld, GrdSalaryCodeNew, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @StartDt, ");
            SQL.AppendLine("@SalaryIncreaseCode, @SiteCode, @GrdSalaryCodeOld, @GrdSalaryCodeNew, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='EmpMeritIncrease' ");
            if (mIsApprovalMeritIncreaseByLevelCode)
            {
                SQL.AppendLine("And T.LevelCode Is Not Null ");
                SQL.AppendLine("And Find_In_Set(@EmpLevelCode, T.LevelCode)");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblEmpMeritIncreaseHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpMeritIncrease' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpLevelCode", EmpLevelCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@SalaryIncreaseCode", Sm.GetLue(LueSalaryIncreaseCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@GrdSalaryCodeOld", Sm.GetLue(LueGrdSalaryCodeOld));
            Sm.CmParam<String>(ref cm, "@GrdSalaryCodeNew", Sm.GetLue(LueGrdSalaryCodeNew));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpMeritIncreaseDtl(string DocNo, int Row, string PPSDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpMeritIncreaseDtl(DocNo, DNo, EmpCode, PPSDocNo, CLTPInd, DemotionInd, ");
            SQL.AppendLine("WorkPeriodOld, GrdSalaryCode, GrdSalaryDNo, AllowedInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @PPSDocNo, @CLTPInd, @DemotionInd, ");
            SQL.AppendLine("@WorkPeriodOld, @GrdSalaryCode, @GrdSalaryDNo, @AllowedInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PPSDocNo", PPSDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CLTPInd", Sm.GetGrdBool(Grd1, Row, 8) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DemotionInd", Sm.GetGrdBool(Grd1, Row, 9) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@WorkPeriodOld", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@GrdSalaryCode", Sm.GetLue(LueGrdSalaryCodeNew));
            Sm.CmParam<String>(ref cm, "@GrdSalaryDNo", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@AllowedInd", Sm.GetGrdBool(Grd1, Row, 13) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePPS(string DocNo, int Row, string PPSDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPS(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmployeeRequestDocNo, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
            SQL.AppendLine("DeptCodeOld, PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld, ResignDtOld, SectionCodeOld, LevelCodeOld, ");
            SQL.AppendLine("DeptCodeNew, PosCodeNew, PositionStatusCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, ResignDtNew, SectionCodeNew, LevelCodeNew, UpdLeaveStartDtInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PPSDocNo, A.DocDt, 'N', 'O', 'P', A.StartDt, Null, B.EmpCode, Null, Null, ");
            SQL.AppendLine("C.DeptCode, C.PosCode, C.PositionStatusCode, A.GrdSalaryCodeOld, C.EmploymentStatus, C.SystemType, C.PayrunPeriod, C.PGCode, C.SiteCode, C.ResignDt, C.SectionCode, C.LevelCode, ");
            SQL.AppendLine("C.DeptCode, C.PosCode, C.PositionStatusCode, A.GrdSalaryCodeNew, C.EmploymentStatus, C.SystemType, C.PayrunPeriod, C.PGCode, C.SiteCode, C.ResignDt, C.SectionCode, C.LevelCode, 'N', A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblEmpMeritIncreaseHdr A ");
            SQL.AppendLine("Inner Join TblEmpMeritIncreaseDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo And B.DNo = @DNo ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode; ");

            SQL.AppendLine("Update TblPPS Set Status='A' ");
            SQL.AppendLine("Where DocNo=@PPSDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where  ");
            SQL.AppendLine("    DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParam<String>(ref cm, "@PPSDocNo", PPSDocNo);

            return cm;
        }

        private MySqlCommand SaveEmployeePPS(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeePPS Set ");
            SQL.AppendLine("    EndDt=@EndDt ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And StartDt=( ");
            SQL.AppendLine("    Select StartDt From (");
            SQL.AppendLine("        Select Max(StartDt) StartDt ");
            SQL.AppendLine("        From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=@EmpCode ");
            SQL.AppendLine("        And StartDt<@StartDt ");
            SQL.AppendLine("    ) T ) ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("     Select 1 ");
            SQL.AppendLine("     From TblDocApproval ");
            SQL.AppendLine("     Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblEmployeePPS(EmpCode, StartDt, EndDt, ");
            SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, SectionCode, LevelCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.EmpCode, A.StartDt, Null, ");
            SQL.AppendLine("C.DeptCode, C.PosCode, A.GrdSalaryCodeNew, C.EmploymentStatus, C.SystemType, C.PayrunPeriod, C.PGCode, C.SiteCode, C.SectionCode, C.LevelCode, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblEmpMeritIncreaseHdr A ");
            SQL.AppendLine("Inner Join TblEmpMeritIncreaseDtl B On A.DocNo = B.DocNo And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo And B.DNo = @DNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("     Select 1 ");
            SQL.AppendLine("     From TblDocApproval ");
            SQL.AppendLine("     Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            string CurrentDate = Sm.GetDte(DteStartDt);
            DateTime DateMin = Sm.ConvertDate(CurrentDate).AddDays(-1);
            Sm.CmParam<String>(ref cm, "@EndDt", DateMin.ToString("yyyyMMdd"));

            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));

            return cm;
        }

        private MySqlCommand UpdateEmployee(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    GrdLvlCode=@GrdSalaryCodeNew, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And Exists(Select 1 From TblEmpMeritIncreaseHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblEmployeePPS P Set P.ProcessInd = 'Y' ");
            SQL.AppendLine("Where P.EmpCode = @EmpCode And P.ProcessInd = 'N' ");
            SQL.AppendLine("And P.EndDt Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblEmpMeritIncreaseHdr Where DocNo = @DocNo And Status = 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@GrdSalaryCodeNew", Sm.GetLue(LueGrdSalaryCodeNew));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmpMeritIncreaseDtlPPSDocNo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpMeritIncreaseDtl ");
            SQL.AppendLine("Set PPSDocNo = Null ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpMeritIncreaseHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status != 'A' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand SaveEmployeeSalary(string DocNo, int Row, string EmployeeSalaryDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeSalary (EmpCode, DNo, StartDt, Amt, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.EmpCode, @EmployeeSalaryDNo, @StartDt, B.Amt, '0.00', @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblEmpMeritIncreaseDtl A ");
            SQL.AppendLine("Inner Join TblEmpMeritIncreaseHdr A1 On A.DocNo=A1.DocNo ");
            SQL.AppendLine("Inner Join TblGradeSalaryDtl B On A.GrdSalaryCode = B.GrdSalaryCode And A.GrdSalaryDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.DNo = @DNo ");
            SQL.AppendLine("    And A1.Status = 'A'; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParam<String>(ref cm, "@EmployeeSalaryDNo", Sm.Right(string.Concat("000", (Int32.Parse(EmployeeSalaryDNo) + 1).ToString()), 3));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Grid

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (e.ColIndex == 12)
                {
                    LueRequestEdit(Grd1, LueGrdSalaryDNo, ref fCell, ref fAccept, e);
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
            {
                var f = new FrmPPS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 12)
            {
                if (Sm.GetGrdStr(Grd1, 0, 12).Length != 0)
                {
                    string WorkPeriodNew = Sm.GetGrdStr(Grd1, 0, 12);
                    string GrdSalaryDNo = Sm.GetGrdStr(Grd1, 0, 11);

                    for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 11].Value = GrdSalaryDNo;
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 12].Value = WorkPeriodNew;
                    }
                }
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsEmployeeUseGradeSalary = Sm.GetParameterBoo("IsEmployeeUseGradeSalary");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mMonthRangeEmployeeMerit = Sm.GetParameter("MonthRangeEmployeeMerit");
            mJobTransferDemotionMerit = Sm.GetParameter("JobTransferDemotionMerit");
            mLeaveCodeCLTP = Sm.GetParameter("LeaveCodeCLTP");
            mIsPPSUpdateEmpDivBasedOnDept = Sm.GetParameterBoo("IsPPSUpdateEmpDivBasedOnDept");
            mIsPPSUseSection = Sm.GetParameterBoo("IsPPSUseSection");
            mIsApprovalMeritIncreaseByLevelCode = Sm.GetParameterBoo("IsApprovalMeritIncreaseByLevelCode");

            if (mMonthRangeEmployeeMerit.Length == 0) mMonthRangeEmployeeMerit = "3";
            if (mJobTransferDemotionMerit.Length == 0) mJobTransferDemotionMerit = "D";
        }

        private void SetLueGradeSalary(ref DXE.LookUpEdit Lue, string GrdSalaryCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo As Col1, Convert(Format(WorkPeriod+Cast(Month As int)/100, 2) Using utf8) As Col2 ");
            SQL.AppendLine("From TblGradeSalaryDtl ");
            SQL.AppendLine("Where GrdSalaryCode = '" + GrdSalaryCode + "' ");
            SQL.AppendLine("Order By DNo; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ProcessDataEmployee(bool IsRefressAllData)
        {
            var l = new List<Data>();
            var l2 = new List<LeaveRequest>();
            var l3 = new List<PPSDemotion>();
            var l4 = new List<PPS>();

            PrepDataEmployee(ref l, IsRefressAllData);
            if (l.Count > 0)
            {
                string SelectedEmpCode = GetSelectedEmpCode(ref l);
                GetLeaveRequest(ref l2, SelectedEmpCode);
                GetPPSDemotion(ref l3, SelectedEmpCode);
                GetPPSEmployee(ref l4, SelectedEmpCode);
                ProcessData(ref l, ref l2, ref l3, ref l4);
                ShowDataToGrd(ref l, IsRefressAllData);
            }
            else
                Sm.StdMsg(mMsgType.NoData, string.Empty);

            l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
        }

        private void PrepDataEmployee(ref List<Data> l, bool IsRefreshAllData)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string EmpCode = string.Empty;

            if (!IsRefreshAllData && Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        if (EmpCode.Length > 0) EmpCode += ",";
                        EmpCode += Sm.GetGrdStr(Grd1, i, 1);
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.EmpName, C.DivisionName, B.DeptName, ");
            SQL.AppendLine("D.PosName, E.LevelName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            SQL.AppendLine("    And A.GrdLvlCode = @GrdSalaryCode ");
            if (!IsRefreshAllData && Grd1.Rows.Count > 0)
            {
                SQL.AppendLine("    And Not Find_In_set(A.EmpCode, @ExistingEmpCode) ");
            }
            SQL.AppendLine("Left Join TblDivision C On A.DivisionCode = C.DivisionCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblLevelHdr E On A.LevelCode = E.LevelCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > @CurrentDate)) ");
            SQL.AppendLine("Order By A.EmpName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@GrdSalaryCode", Sm.GetLue(LueGrdSalaryCodeOld));
                if (!IsRefreshAllData) Sm.CmParam<String>(ref cm, "@ExistingEmpCode", EmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName",
                    "DivisionName",
                    "DeptName",
                    "PosName",
                    "LevelName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),

                            EmpName = Sm.DrStr(dr, c[1]),
                            DivisionName = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            LevelName = Sm.DrStr(dr, c[5]),
                            CLTPInd = "N",
                            DemotionInd = "N",
                            WorkPeriodOld = 0m,
                            AllowedInd = "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedEmpCode(ref List<Data> l)
        {
            string EmpCode = string.Empty;

            foreach (var x in l)
            {
                if (EmpCode.Length > 0) EmpCode += ",";
                EmpCode += x.EmpCode;
            }

            return EmpCode;
        }

        private void GetLeaveRequest(ref List<LeaveRequest> l, string SelectedEmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string DocDt1 = Sm.GetValue("Select DATE_FORMAT(Date_Add(@Param, INTERVAL -" + mMonthRangeEmployeeMerit + " MONTH), '%Y%m%d') As DocDt1; ", Sm.GetDte(DteStartDt));

            SQL.AppendLine("Select Distinct EmpCode ");
            SQL.AppendLine("From TblEmpLeaveHdr ");
            SQL.AppendLine("Where CancelInd = 'N' And Status In ('O', 'A') ");
            SQL.AppendLine("And Find_In_Set(EmpCode, @SelectedEmpCode) ");
            SQL.AppendLine("And Find_In_Set(LeaveCode, @LeaveCodeCLTP) ");
            SQL.AppendLine("And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                Sm.CmParam<String>(ref cm, "@LeaveCodeCLTP", mLeaveCodeCLTP);
                Sm.CmParamDt(ref cm, "@DocDt1", DocDt1);
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteStartDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveRequest()
                        {
                            EmpCode = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetPPSDemotion(ref List<PPSDemotion> l, string SelectedEmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string DocDt1 = Sm.GetValue("Select DATE_FORMAT(Date_Add(@Param, INTERVAL -" + mMonthRangeEmployeeMerit + " MONTH), '%Y%m%d') As DocDt1; ", Sm.GetDte(DteStartDt));

            SQL.AppendLine("Select Distinct EmpCode ");
            SQL.AppendLine("From TblPPS ");
            SQL.AppendLine("Where CancelInd = 'N' And Status In ('O', 'A') ");
            SQL.AppendLine("And Find_In_Set(EmpCode, @SelectedEmpCode) ");
            SQL.AppendLine("And Find_In_Set(JobTransfer, @JobTransferDemotionMerit) ");
            SQL.AppendLine("And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                Sm.CmParam<String>(ref cm, "@JobTransferDemotionMerit", mJobTransferDemotionMerit);
                Sm.CmParamDt(ref cm, "@DocDt1", DocDt1);
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteStartDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPSDemotion()
                        {
                            EmpCode = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetPPSEmployee(ref List<PPS> l, string SelectedEmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //SQL.AppendLine("Select Distinct T.EmpCode, TIMESTAMPDIFF(YEAR, T.StartDt, @StartDt) As Yr, TIMESTAMPDIFF(MONTH, T.StartDt, @StartDt) As Mth, ");
            //SQL.AppendLine("TIMESTAMPDIFF(YEAR, T.StartDt, @StartDt) + (TIMESTAMPDIFF(MONTh, T.StartDt, @StartDt)%12)/100 As YrMth ");
            SQL.AppendLine("Select T.EmpCode, ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("    When TIMESTAMPDIFF(YEAR, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) < 0 Then 0 ");
            SQL.AppendLine("    Else TIMESTAMPDIFF(YEAR, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) ");
            SQL.AppendLine("End As Yr, ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("    When TIMESTAMPDIFF(MONTH, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) < 0 Then 0 ");
            SQL.AppendLine("    Else TIMESTAMPDIFF(MONTH, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) ");
            SQL.AppendLine("End As Mth, ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("    When TIMESTAMPDIFF(YEAR, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) + (TIMESTAMPDIFF(MONTh, IFNULL(T2.StartDt, T3.JoinDt), @StartDt)%12)/100 < 0 Then 0 ");
            SQL.AppendLine("    Else TIMESTAMPDIFF(YEAR, IFNULL(T2.StartDt, T3.JoinDt), @StartDt) + (TIMESTAMPDIFF(MONTh, IFNULL(T2.StartDt, T3.JoinDt), @StartDt)%12)/100 ");
            SQL.AppendLine("End As YrMth ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeePPS ");
            SQL.AppendLine("    Where EndDt Is Null ");
            SQL.AppendLine("    And Find_IN_Set(EmpCode, @SelectedEmpCode) ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT EmpCode, MAX(StartDt) StartDt ");
            SQL.AppendLine("    FROM TblPPS ");
            SQL.AppendLine("    WHERE CancelInd = 'N' ");
            SQL.AppendLine("    AND GrdLvlCodeOld is NOT Null ");
            SQL.AppendLine("    AND GrdLvlCodeOld <> GrdLvlCodeNew ");
            SQL.AppendLine("    GROUP BY EmpCode ");
            SQL.AppendLine(")T2 ON T.EmpCode = T2.EmpCode ");
            SQL.AppendLine("INNER JOIN TblEmployee T3 ON T.EmpCode = T3.EmpCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Yr", "Mth", "YrMth" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Yr = Sm.DrDec(dr, c[1]),
                            Mth = Sm.DrDec(dr, c[2]),
                            YrMth = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }

            // time stamp diff by year dan by month
            // nanti waktu proses, month nya di mod
        }

        private void ProcessData(ref List<Data> l, ref List<LeaveRequest> l2, ref List<PPSDemotion> l3, ref List<PPS> l4)
        {
            foreach (var x in l)
            {
                foreach (var a in l2.Where(w => w.EmpCode == x.EmpCode)) x.CLTPInd = "Y";

                foreach (var b in l3.Where(w => w.EmpCode == x.EmpCode)) x.DemotionInd = "Y";

                foreach (var c in l4.Where(w => w.EmpCode == x.EmpCode))
                {
                    decimal mVal = 0m;

                    mVal = c.YrMth;
                    //if (c.Mth % 12 > 6) mVal += 1;
                    x.WorkPeriodOld = mVal;
                }

                if (x.CLTPInd == "Y" || x.DemotionInd == "Y") x.AllowedInd = "N";
            }
        }

        private string GetEmpLevelCodeDocApproval()
        {
            var SQL = new StringBuilder();
            var l = new List<EmpLevel>();
            string EmpLevelCodeTmp = string.Empty;

            GetEmpLevelCode(ref l);
            if (l.Count > 0)
            {
                SQL.AppendLine("Select 1 From TblDocApprovalSetting Where DocType = 'EmpMeritIncrease' And Find_In_Set(@Param, LevelCode)");
                foreach (var x in l.OrderBy(o => o.EmpLevelCode))
                {
                    if (Sm.IsDataExist(SQL.ToString(), x.EmpLevelCode))
                    {
                        EmpLevelCodeTmp = x.EmpLevelCode;
                        break;
                    }
                    else
                        continue;
                }
            }

            return EmpLevelCodeTmp;
        }

        private void GetEmpLevelCode(ref List<EmpLevel> level)
        {
            var SQL = new StringBuilder();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (SQL.Length > 0) SQL.AppendLine("Union All ");
                SQL.AppendLine("Select LevelCode From TblEmployee ");
                SQL.AppendLine("Where EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' ");
                SQL.AppendLine("And LevelCode Is Not Null ");
            }

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LevelCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        level.Add(new EmpLevel()
                        {
                            EmpLevelCode = Sm.DrStr(dr, c[0]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDataToGrd(ref List<Data> l, bool IsRefreshAllData)
        {
            int Row = 0;

            if (!IsRefreshAllData) Row = Grd1.Rows.Count;
            else ClearGrd();

            Grd1.BeginUpdate();
            foreach(var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 1].Value = x.EmpCode;
                Grd1.Cells[Row, 3].Value = x.EmpName;
                Grd1.Cells[Row, 4].Value = x.DivisionName;
                Grd1.Cells[Row, 5].Value = x.DeptName;
                Grd1.Cells[Row, 6].Value = x.PosName;
                Grd1.Cells[Row, 7].Value = x.LevelName;
                Grd1.Cells[Row, 8].Value = x.CLTPInd == "Y";
                Grd1.Cells[Row, 9].Value = x.DemotionInd == "Y";
                Grd1.Cells[Row, 10].Value = Sm.FormatNum(x.WorkPeriodOld, 0);
                Grd1.Cells[Row, 13].Value = x.AllowedInd == "Y";

                Row += 1;
            }
            Grd1.EndUpdate();
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("Doctitle");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                var ldtl = new List<DetailMI>();

                string[] TableName = { "DetailMI" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();
                int Nomor = 0;

                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;
                    SQLDtl.AppendLine("Set @No=0;  ");
                    SQLDtl.AppendLine("SELECT  (SELECT  @No:=@No+1) As No_URUT, CONCAT(@No,'/Kpts/Dir/',Left(CurrentDateTime(),4)) Nomor, ");
                    SQLDtl.AppendLine("@CompanyLogo As CompanyLogo,  UPPER(B.EmpName) EmpName, B.EmpName, B.EmpCodeOld, E.PosName, P.GrdSalaryName GradeNew, O.GrdSalaryName GradeOld, ");
                    SQLDtl.AppendLine("A.DocNo, DATE_FORMAT(H.DocDt, '%d %M %Y') DocDt, J.CityName, K.EmpName Approval1, K.PosName ApprovalPosName,  ");
                    SQLDtl.AppendLine("concat(concat(TIMESTAMPDIFF(Year, B.JoinDt, (SELECT concat(Left(CurrentDateTime(),4),'0701'))),' tahun '), ");
                    SQLDtl.AppendLine("concat(TIMESTAMPDIFF(MOnth, B.JoinDt, (SELECT concat(Left(CurrentDateTime(),4),'0701')))%12,' bulan')) DurationPPS, ");
                    SQLDtl.AppendLine("ifnull(Q.Amt, 0) GradeSalaryOld, ifnull(G.Amt, 0.00) GradeSalaryNew, date_format(H.StartDt, '%d %M %Y') StartDt, ");
                    SQLDtl.AppendLine("(SELECT DATE_FORMAT(concat(Left(H.DocDt,4),'0701'), '%d %M %Y')) IncreaseDt, N.Education, ");
                    SQLDtl.AppendLine("(SELECT DATE_FORMAT(concat(Left(H.DocDt,4),'0630'), '%d %M %Y')) IncreaseDt2, ");
                    SQLDtl.AppendLine("CONCAT(ifnull(B.BirthPlace, ''), ', ', DATE_FORMAT(ifnull(B.BirthDt, ''), '%d %M %Y')) TTL, ");
                    SQLDtl.AppendLine("DATE_FORMAT(concat(Left(H.DocDt,4),'0701')+ INTERVAL '6' MONTH, '%d %M %Y') IncreaseDt3, DATE_FORMAT(B.JoinDt, '%d %M %Y') JoinDt "); 

                    SQLDtl.AppendLine("From TblEmpMeritIncreaseDtl A ");
                    SQLDtl.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                    SQLDtl.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode  ");
                    SQLDtl.AppendLine("Left Join TblDivision D On B.DivisionCode = D.DivisionCode ");
                    SQLDtl.AppendLine("Left Join TblPosition E On B.PosCode = E.PosCode  ");
                    SQLDtl.AppendLine("Left Join TblLevelHdr F On B.LevelCode = F.LevelCode  ");
                    SQLDtl.AppendLine("Inner Join TblGradeSalaryDtl G On A.GrdSalaryCode = G.GrdSalaryCode And A.GrdSalaryDNo = G.DNo  ");
                    SQLDtl.AppendLine("Inner Join tblgradesalaryhdr G2 On A.GrdSalaryCode = G2.GrdSalaryCode ");
                    SQLDtl.AppendLine("Inner Join tblempmeritincreasehdr H ON A.DocNo=H.DocNo ");
                    SQLDtl.AppendLine("Inner Join tblsite I ON H.SiteCode= I.SiteCode ");
                    SQLDtl.AppendLine("Inner Join tblcity J ON I.CityCode=J.CityCode ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("     SELECT A.DocNo, C.EmpName, D.PosName, A.CreateBy ");
                    SQLDtl.AppendLine("     FROM tbldocapproval A  ");
                    SQLDtl.AppendLine("     Inner Join tbluser B ON A.LastUpBy=B.UserCode ");
                    SQLDtl.AppendLine("     Inner Join tblemployee C ON B.UserCode=C.UserCode ");
                    SQLDtl.AppendLine("     Inner Join tblposition D ON C.PosCode=D.PosCode ");
                    SQLDtl.AppendLine("     WHERE A.`Status`='A'  And A.DocType ='EmpMeritIncrease'  ");
                    SQLDtl.AppendLine("     And A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("     LIMIT 1 ");
                    SQLDtl.AppendLine(") K ON K.DocNo=A.DocNo And K.DocNo=@DocNo ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    SELECT A.DocNo, A.EmpCode, A.StartDt ");
                    SQLDtl.AppendLine("    FROM tblpps A  ");
                    SQLDtl.AppendLine("    WHERE A.GrdLvlCodeOld!=A.GrdLvlCodeNew ");
                    SQLDtl.AppendLine(") L ON A.PPSDocNo=L.DocNo And B.EmpCode=L.EmpCode ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("     SELECT  A.EmpCode, D.GrdSalaryCode, D.WorkPeriod,  D.Amt ");
                    SQLDtl.AppendLine("     From TblEmpMeritIncreaseDtl A  ");
                    SQLDtl.AppendLine("     Inner Join tblempmeritincreasehdr B ON A.DocNo=B.DocNo  ");
                    SQLDtl.AppendLine("     Inner Join tblgradesalaryhdr C ON B.GrdSalaryCodeOld=C.GrdSalaryCode  ");
                    SQLDtl.AppendLine("     Inner Join tblgradesalarydtl D ON B.GrdSalaryCodeOld = D.GrdSalaryCode And A.WorkPeriodOld = D.WorkPeriod ");
                    SQLDtl.AppendLine("     WHERE A.DocNo=@DocNo  ");
                    SQLDtl.AppendLine("     GROUP BY A.EmpCode ");
                    SQLDtl.AppendLine("     ORDER BY A.DNo  ");
                    SQLDtl.AppendLine(") M ON M.empcode=A.empcode ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("     SELECT A.EmpCode, CONCAT('- ', B.OptDesc, ' ', A.School) Education  ");
                    SQLDtl.AppendLine("     from tblemployeeeducation A ");
                    SQLDtl.AppendLine("     Left Join tbloption B ON A.`Level`=B.OptCode And B.OptCat='EmployeeEducationLevel' ");
                    SQLDtl.AppendLine("     WHERE A.HighestInd='Y' ");
                    SQLDtl.AppendLine(") N ON A.EmpCode=N.EmpCode  ");
                    SQLDtl.AppendLine("Inner Join tblgradesalaryhdr O ON H.GrdSalaryCodeOld=O.GrdSalaryCode ");
                    SQLDtl.AppendLine("Inner Join tblgradesalaryhdr P ON H.GrdSalaryCodeNew=P.GrdSalaryCode ");
                    SQLDtl.AppendLine("LEFT JOIN  ");
                    SQLDtl.AppendLine("( ");
	                SQLDtl.AppendLine("    SELECT A.DocNo, A.Amt, T.empcode ");
	                SQLDtl.AppendLine("    FROM tblempsalarydtl A "); 
	                SQLDtl.AppendLine("    inner JOIN  ");
	                SQLDtl.AppendLine("    ( ");
		            SQLDtl.AppendLine("        SELECT max(A.docno) DocNo, max(A.DocDt), A.EmpCode ");
		            SQLDtl.AppendLine("        FROM tblempsalaryhdr A ");
		            SQLDtl.AppendLine("        INNER JOIN tblempsalarydtl B ON A.DocNo=B.DocNo "); 
		            SQLDtl.AppendLine("        WHERE A.EmpCode  IN  ");
	                SQLDtl.AppendLine("    (SELECT empcode FROM tblempmeritincreasedtl WHERE docno =@DocNo) ");
	                SQLDtl.AppendLine("    GROUP BY empcode  ");
	                SQLDtl.AppendLine("    ) T ON A.DocNo=T.DocNo ");
                    SQLDtl.AppendLine(")Q ON A.EmpCode=Q.EmpCode  ");
                    SQLDtl.AppendLine(" Where A.DocNo = @DocNo  ");
                    SQLDtl.AppendLine("Order By A.DNo;  ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@CompanyLogo", Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    //Sm.CmParam<Decimal>(ref cmDtl, "@No", 0);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                        //0
                        "DocNo" ,

                        //1-5
                        "EmpName" ,
                        "EmpCodeOld",
                        "PosName",
                        "GradeNew",
                        "GradeSalaryOld",

                        //6-10
                        "GradeSalaryNew",
                        "DurationPPS",
                        "DocDt",
                        "CityName",
                        "Approval1",

                        //11-15
                        "ApprovalPosName",
                        "CompanyLogo",
                        "Nomor",
                        "Education",
                        "TTL", 

                        //16-20
                        "GradeOld", 
                        "IncreaseDt",
                        "IncreaseDt2",
                        "IncreaseDt3",
                        "JoinDt", 

                        //21
                        "StartDt",

                    });

                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new DetailMI()
                            {
                                No = Sm.DrStr(drDtl, cDtl[13]),

                                DocNo = Sm.DrStr(drDtl, cDtl[0]),
                                EmpName = Sm.DrStr(drDtl, cDtl[1]),
                                EmpCodeOld = Sm.DrStr(drDtl, cDtl[2]),
                                PosName = Sm.DrStr(drDtl, cDtl[3]),
                                GradeNew = Sm.DrStr(drDtl, cDtl[4]),

                                GradeSalaryOld = Sm.DrDec(drDtl, cDtl[5]),
                                GradeSalaryNew = Sm.DrDec(drDtl, cDtl[6]),
                                DurationDate = Sm.DrStr(drDtl, cDtl[7]),
                                DocDt = Sm.DrStr(drDtl, cDtl[8]),
                                CityName = Sm.DrStr(drDtl, cDtl[9]),

                                Approval1 = Sm.DrStr(drDtl, cDtl[10]),
                                ApprovalPosName = Sm.DrStr(drDtl, cDtl[11]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[6])),
                                CompanyLogo = Sm.DrStr(drDtl, cDtl[12]),
                                Education = Sm.DrStr(drDtl, cDtl[14]),

                                TTL = Sm.DrStr(drDtl, cDtl[15]),
                                GradeOld = Sm.DrStr(drDtl, cDtl[16]),
                                IncreaseDt = Sm.DrStr(drDtl, cDtl[17]),
                                IncreaseDt2 = Sm.DrStr(drDtl, cDtl[18]),
                                IncreaseDt3 = Sm.DrStr(drDtl, cDtl[19]),

                                JoinDt = Sm.DrStr(drDtl, cDtl[20]),
                                StartDt = Sm.DrStr(drDtl, cDtl[21]),
                            });
                        }
                    }

                    drDtl.Close();
                }

                myLists.Add(ldtl);

                #endregion

                if (Sm.GetLue(LueSalaryIncreaseCode) == "2")
                    Sm.PrintReport("EmpMeritIncrease", myLists, TableName, false);
                else
                    Sm.PrintReport("EmpMeritIncrease2", myLists, TableName, false);
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueGrdSalaryDNo_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueGrdSalaryDNo, new Sm.RefreshLue2(SetLueGradeSalary), Sm.GetLue(LueGrdSalaryCodeNew));
            }
        }

        private void LueGrdSalaryDNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd1, ref fAccept, e);
            }
        }

        private void LueGrdSalaryDNo_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueGrdSalaryDNo.Visible && fAccept && fCell.ColIndex == 12)
                {
                    if (Sm.GetLue(LueGrdSalaryDNo).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 11].Value =
                        Grd1.Cells[fCell.RowIndex, 12].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 11].Value = Sm.GetLue(LueGrdSalaryDNo);
                        Grd1.Cells[fCell.RowIndex, 12].Value = LueGrdSalaryDNo.GetColumnValue("Col2");
                    }
                    LueGrdSalaryDNo.Visible = false;
                }
            }
        }

        private void LueSalaryIncreaseCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSalaryIncreaseCode, new Sm.RefreshLue2(Sl.SetLueOption), "SalaryIncrease");
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
                ClearGrd();
        }

        private void LueGrdSalaryCodeOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueGrdSalaryCodeOld, new Sm.RefreshLue1(Sl.SetLueGradeSalary));
                ClearGrd();
            }
        }

        private void LueGrdSalaryCodeNew_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueGrdSalaryCodeNew, new Sm.RefreshLue1(Sl.SetLueGradeSalary));
                if (Sm.GetLue(LueGrdSalaryCodeNew).Length > 0)
                {
                    SetLueGradeSalary(ref LueGrdSalaryDNo, Sm.GetLue(LueGrdSalaryCodeNew));
                }
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Click

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsDteEmpty(DteStartDt, "Start Date") && !Sm.IsLueEmpty(LueSiteCode, "Site") && !Sm.IsLueEmpty(LueGrdSalaryCodeOld, "Grade Old") && !IsGradeDuplicate())
                {
                    bool IsRefreshAllData = true;

                    //if (mInitGrdSalaryCodeOld.Length == 0) mInitGrdSalaryCodeOld = Sm.GetLue(LueGrdSalaryCodeOld);

                    //if (mInitGrdSalaryCodeOld == Sm.GetLue(LueGrdSalaryCodeOld))
                    //{
                    //    if (Sm.StdMsgYN("Question", "Do you want to refresh all data or unlisted employee ?" + Environment.NewLine + "Choose [YES] if you want to refresh all data, otherwise choose [NO].") == DialogResult.No) IsRefreshAllData = false;
                    //}
                    
                    ProcessDataEmployee(IsRefreshAllData);

                    SetLueGradeSalary(ref LueGrdSalaryDNo, Sm.GetLue(LueGrdSalaryCodeNew));

                    //mInitGrdSalaryCodeOld = Sm.GetLue(LueGrdSalaryCodeOld);
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class Data
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DivisionName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string LevelName { get; set; }
            public string CLTPInd { get; set; }
            public string DemotionInd { get; set; }
            public decimal WorkPeriodOld { get; set; }
            public string AllowedInd { get; set; }
        }

        private class LeaveRequest
        {
            public string EmpCode { get; set; }
        }

        private class PPSDemotion
        {
            public string EmpCode { get; set; }
        }

        private class PPS
        {
            public string EmpCode { get; set; }
            public decimal Yr { get; set; }
            public decimal Mth { get; set; }
            public decimal YrMth { get; set; }
        }

        #endregion

        #region Class
        private class DetailMI
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string GradeOld { get; set; }
            public string GradeNew { get; set; }
            public decimal GradeSalaryOld { get; set; }
            public decimal GradeSalaryNew { get; set; }
            public string DurationDate { get; set; }
            public string CityName { get; set; }
            public string PosName { get; set; }
            public string Approval1 { get; set; }
            public string ApprovalPosName { get; set; }
            public string Terbilang { get; set; }
            public string No { get; set; }
            public string Education { get; set; }
            public string TTL { get; set; }
            public string IncreaseDt { get; set; }
            public string IncreaseDt2 { get; set; }
            public string IncreaseDt3 { get; set; }
            public string JoinDt { get; set; }
            public string StartDt { get; set; }
        }

        private class EmpLevel
        {
            public string EmpLevelCode { set; get; }
        }

        #endregion 

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 13 && BtnSave.Enabled)
            {
                for (int Row = 0; Grd1.Rows.Count > Row; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 13))
                    {
                        Grd1.Cells[Row, 12].ReadOnly = iGBool.False;
                        Grd1.Cells[Row, 12].BackColor = Color.White;
                    }
                    else
                    {
                        Grd1.Cells[Row, 12].Value = string.Empty;
                        Grd1.Cells[Row, 12].ReadOnly = iGBool.True;
                        Grd1.Cells[Row, 12].BackColor = Color.FromArgb(224, 224, 224);
                    }
                }

            }
        }
    }
}
