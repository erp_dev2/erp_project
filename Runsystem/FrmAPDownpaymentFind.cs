﻿#region Update
/*
    04/12/2017 [HAR] tambah Local Document filter
    10/04/2018 [ARI] tambah filter dan kolom department
    13/04/2018 [TKG] filter by department
    12/05/2018 [TKG] filter by entity
    05/10/2018 [HAR] tambah payment date dan ftp file
    24/05/2020 [TKG/MMM] Berdasarkan parameter MenuCodeForVendorAPDownpaymentWithoutPO, Vendor's AP Downpayment bisa diinput tanpa PO
    22/07/2020 [TKG] bug saat show data
    17/06/2021 [WED/IOK] saat find data, tidak melihat ada PO atau tidak, berdasarkan parameter VdCtCodeForRawMaterial
    13/10/2021 [VIN/IOK] bug vendor tidak muncul VdCtCodeForRawMaterial
    04/02/2022 [RIS/PRODUCT] Menambah filter VR#
    03/08/2022 [TYO/SIER] filter group vendor berdasarkan parameter IsFilterByVendorCategory
    04/08/2022 [SET/SIER] penyesuaian filter group vendor berdasarkan parameter IsFilterByVendorCategory
    13/09/2022 [SET/SIER] Feedback : penyesuain ShowData() berdasarkan parameter IsFilterByVendorCategory
    03/02/2023 [SET/BBT] menampilkan Site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmAPDownpaymentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAPDownpayment mFrmParent;
        private string mSQL = string.Empty;
        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmAPDownpaymentFind(FrmAPDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode, mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDept?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.* From( ");
            SQL.AppendLine("Select A.DeptCode, A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc,");
            SQL.AppendLine("E.DeptName, G.SiteName, ifnull(C.VdName, C2.VdName) VdName,ifnull(C.VdCode, C2.VdCode) VdCode, A.PODocNo, B.LocalDocNo POLocalDocNo, A.CurCode, A.Amt, A.VoucherRequestDocNo, D.VoucherDocNo, A.PaymentDt, F.EntName, A.Filename,  A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblAPDownpayment A ");
            SQL.AppendLine("Left Join TblPOHdr B On A.PODocNo=B.DocNo ");
            if (mFrmParent.mIsWithoutPO)
                SQL.AppendLine("Left Join TblVendor C On A.VdCode=C.VdCode ");
            else
                SQL.AppendLine("Left Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblVendor C2 On A.VdCode=C2.VdCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblEntity F On A.EntCode=F.EntCode ");
            SQL.AppendLine("Left Join TblSite G On A.SiteCode=G.SiteCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mVdCtCodeForRawMaterial.Length == 0)
            {
                if (mFrmParent.mIsWithoutPO)
                    SQL.AppendLine("And A.PODocNo Is Null ");
                else
                    SQL.AppendLine("And A.PODocNo Is Not Null ");
            }

            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or (A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByEnt)
            {
                SQL.AppendLine("And (A.EntCode Is Null Or (A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=A.EntCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByVendorCategory)
            {
                SQL.AppendLine("And (C.VdCode Is Null Or (C.VdCode Is Not Null ");
                SQL.AppendLine("And EXISTS ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1   ");
                SQL.AppendLine("	FROM TblGroupVendorCategory ");
                SQL.AppendLine("	WHERE VdCtCode = C.VdCtCode  ");
                SQL.AppendLine("	AND GrpCode IN   ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(")A ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Local#",
                        "Date",
                        "Cancel",
                        "Status",

                        //6-10
                        "Department",
                        "Site",
                        "Vendor",
                        "PO",
                        "",
                        
                        //11-15
                        "Currency", 
                        "Amount",
                        "Voucher Request",
                        "Voucher",
                        "Payment Date",
 
                        //16-20
                        "Entity",
                        "",
                        "File",
                        "Remark",
                        "Created By",

                        //21 -25
                        "Created Date",
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        140, 100, 80, 60, 80, 
                        
                        //6-10
                        180, 180, 200, 140, 20, 

                        //11-15
                        60, 150, 130, 130, 100, 

                        //16-20
                        180, 20, 150, 200, 130, 

                        //21-24
                        130, 130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 10 ,17 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 13, 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            if(!mFrmParent.mIsAPDownpaymentUseSite)
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            if (mFrmParent.mIsAPDPAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18 }, true);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] {"A.DocNo", "A.LocalDocNo"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), new string[] { "A.VdCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, new string[] { "A.PODocNo", "A.POLocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVRDocNo.Text, new string[] { "A.VoucherRequestDocno" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "LocalDocNo", "DocDt", "CancelInd", "StatusDesc", "DeptName", 
                            
                            //6-10
                            "SiteName", "VdName", "PODocNo", "CurCode", "Amt",   
                            
                            //11-15
                            "VoucherRequestDocNo", "VoucherDocNo", "PaymentDt", "EntName", "FileName", 
                            
                            //16-19
                             "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 20);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region FTP
        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, TxtFile, mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (TxtFile.Length > 0)
            {
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    //Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                Application.DoEvents();
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;


                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();


                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 18).Length != 0)
            {
                DownloadFileKu(Sm.GetGrdStr(Grd1, e.RowIndex, 18));
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode), mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO number");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtVRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "VR number");
        }

        #endregion

        #endregion

    }
}
