﻿#region Update

#region Old
/*
  07/06/2017 [WED] tambah competence dan print out Employee Profile
14/06/2017 [WED] perbaikan di load print out other competence
15/06/2017 [TKG] tambah graduation training date
01/08/2017 [WED] tambah fitur copy informasi dari tab general saat insert
03/08/2017 [TKG] untuk pilihar resignee dan aktif employee di layar find dan copy diubah prosesnya.
30/08/2017 [TKG] Berdasarkan parameter IsAnnualLeaveUseStartDt, tambah leave start date untuk cuti tahunan dan cuti besar. 
04/09/2017 [TKG] Pada saat insert new employee, insert PPS dan EmployeePPS. 
12/09/2017 [TKG] Pada saat insert PPS dan EmployeePPS, tambah site
19/09/2017 [TKG] Variable IsSiteMandatory digunakan di layar Find
                 Berdasarkan parameter IsEmployeeAutoCreatePPS, pada saat membuat employee baru, sistem secara otomatis akan membuat dokumen PPS atau tidak.
10/10/2017 [TKG] 
    menggunakan parameter IsFilterBySiteHR untuk filter site berdasarkan group
    menggunakan parameter IsFilterByDeptHR untuk filter department berdasarkan group
16/10/2017 [TKG] tambah informasi training
25/01/2018 [HAR] printout pindah posisi gambar employee, urutin work experience sesuai dno 
09/02/2018 [WED] bug di IsGrd1ValueNotValid(), kolom gender diubah dari 3 menjadi 4
07/03/2018 [WED] tab Training tambah combo Training dari master Training
16/05/2018 [TKG] tambah entity (SS)
24/05/2018 [ARI] tambah jaring laba2 potency(printout)
31/05/2018 [WED] printout header, query ke TblPosition diganti jadi Left Join
08/10/2018 [HAR] ada button download picture
20/02/2019 [DITA] penambahan informasi pendidikan: jurusan, program studi dan tahun kelulusan
06/03/2019 [TKG] tambah faculty
10/04/2019 [MEY] tambah informasi training di printout
15/04/2019 [TKG] tambah informasi training evaluation#
02/05/2019 [DITA] Training History dibedakan mana yang standar HIN mana yang di luar standar HIN (Tambah tab control baru khusus HIN)
03/05/2019 [DITA] BUG = Tambah validasi untuk permanent date
17/05/2019 [DITA] BUG = Lue department hanya menampilkan department yg aktif saja
24/05/2019 [TKG] join date tidak bisa diedit.
11/06/2019 [WED] Department tetap harus menampilkan department tidak aktif saat show data
16/08/2019 [TKG] Fitur copy data pada master employee untuk tab family, work experience, education, training, competence, dan social security 
11/09/2019 [DITA] Tambah Contract Date dan Contract Date sebagai generate employee code
12/09/2019 [TKG/IMS] tambah clothes size dan shoe size
17/09/2019 [HAR] tidak boelh edit employeeoldcode di SIER berdasarkan parameter IsEmpOldCodeEditable 
09/10/2019 [HAR/IMS] Format EmployeeCode (2 digit employmentstatus, 2 digit tahun contract, 5 digit urutan)
14/10/2019 [HAR/IMS] Bug subquery return tw rows waktu generate empcode
22/10/2019 [HAR/IMS] tambah kolom education type & tambah attachment File
07/11/2019 [HAR/SIER] attachment File plus modif attachment waktu edit
    17/01/2020 [DIT] Mengubah length sort code
    29/01/2020 [RF] Fix bug ketika menampilkan working group
    18/02/2020 [HAR/IMS] tambah informasi cost group
    09/03/2020 [IBL/SIER] tambah informasi pada tombol browse file dan download file
    10/03/2020 [WED/SIER] generate employee code berdasarkan parameter GenerateEmpCodeFormat
    10/03/2020 [WED/SIER] parameter IsEmployeeEducationInputManualData
    12/03/2020 [IBL/SIER] Pembatasan file type di master employee
    07/04/2020 [HAR/IMS] BUG copy data tidak bisa save  tambah indikator mCopyDataActive
    13/05/2020 [IBL] BUG tidak bisa delete data attachment di grid
    22/05/2020 [TKG/SRN] resign date tidak bisa diedit di master employee
    24/07/2020 [VIN/SIER] tambah check Acting Official Plt
    11/08/2020 [ICA/HIN] menambah parameter IsEmployeeUseCodeOfConduct untuk Reporting Last seen code of conduct di master find employee
    25/08/2020 [HAR/SRN] BUG : PG terbuka waktu edit, payroll group harusnya ke lock, ubahnya lewat PPS
    13/10/2020 [VIN/SRN] penyesuaian Printout Sarinah, menghapus Competance dan Potency
    18/10/2020 [TKG/PHT] Parameter IsAnnualLeaveUseStartDt menggunakan leave start date, bukan join date
    03/11/2020 [VIN/PHT] Generate Employee Code Old
    05/11/2020 [TKG/PHT] old code jadi 20 karakter.
    07/11/2020 [TKG/PHT] family tambah profession, education level
    09/12/2020 [IBL/PHT] Tab competence bisa diedit berdasarkan parameter IsEmployeeAllowedToEditCompetence
    14/12/2020 [TKG/PHT] tambah level
    23/12/2020 [HAR/PHT] BUg upload dowload data
    28/12/2020 [TKG/PHT] Employee Code, Employee Code Old, UserCode, maxlength dirubah jadi 50
    28/12/2020 [WED/PHT] employee menggunakan grade salary berdasarkan parameter IsEmployeeUseGradeSalary
    30/12/2020 [WED/PHT] tambah cek apakah ada data Employee Merit Increase. kalau ada, nanti di find nya menampilkan kolom tanggal Start date terakhir untuk dokumen Employee Merit increase nya
    22/01/2020 [IBL/PHT] Tambah approval saat mengedit data employee general & employee family. Berdasarkan parameter IsEmployeeUpdateNeedApproval
    26/01/2020 [IBL/PHT] Approval berdasarkan group sitenya
    02/06/2021 [RDA/SIER] penyesuaian show data employee berdasarkan parameter IsPltCheckboxNotBasedOnPosition 
    23/08/2021 [TKG/ALL] tambah informasi vaksin 
    25/08/2021 [TRI/PHT] Field specialitation tab training pada tidak mandatory berdasar parameter IsEmpSpecializationNotMandatory
    19/09/2021 [RDA/PHT] Tambah field rhesus untuk menu employee
    27/10/2021 [YOG/ALL] menambah pop up di master employee ketika mengisi address selesai akan muncul pop up akan tercopy otomatis ke domicile ga
    01/11/2021 [YOG/ALL] BUG = BUG Popup untuk mengcopy address ke domicile di Master Employee muncul ketika find dan save
    16/11/2021 [ICA/SIER] menambah panjang karakter display name seperti employee name
    24/11/2021 [VIN/SIER] bug : insert section dan level ke employeepps
    25/11/2021 [VIN/SIER] bug : ambil Acting Official Indicator dr position 
    30/11/2021 [VIN/SIER] balikin lagi 5/11/2021 [VIN/SIER] (bukan bug)
    19/01/2022 [VIN/PHT] Position History berdasarkan pps yg tidak cancel
    21/01/2022 [TKG/PHT] ubah GetParameter()
    09/02/2022 [TKG/PHT] tambah informasi warning letter
 */
#endregion
/*
    10/02/2022 [TKG/PHT] tambah award dan training3 dan position history dan grade history
    11/02/2022 [TKG/PHT] tambah job transfer di position history
    11/02/2022 [SET/PHT] Penyesuain printout
    15/02/2022 [SET/PHT] Penyesuain printout
    17/02/2022 [TKG/PHT] training category diambil dari system option
    17/02/2022 [TKG/PHT] mempercepat proses save
    22/02/2022 [TKG/PHT] bug saat edit seharusnya tidak boleh menghapus tblemployeepositionhistory
    24/02/2022 [SET/PHT] FEEDBACK: Time period di Print out Master Employee belum sesuai dengan akumulasinya
    15/03/2022 [ISD/PHT] Penyesuaian source position history setelah perubahan format import free text
    16/03/2022 [ISD/PHT] Penyesuaian source hasil print out master employee di tab position history/ riwayat jabatan
    22/03/2022 [IBL/PHT] Grade history hasil dari kenaikan salary otomatis (Periodic Salary Advancement) dan kenaikan gaji otomatis (Periodic Grade Advancement) dapat diubah Decision Letter dan Remark nya
    28/03/2022 [ISD/PHT] feedback : PPS yang dicancel masih muncul di jabatan print out Master Employee
    01/04/2022 [VIN/PHT] BUG : beberapa field i printout tidak muncul -> perbaikan query 
    18/04/2022 [ICA/PHT] tambah field First Grade 
    20/04/2022 [ICA/PHT] menambah parameter IsEmployeeUseFirstGrade
    22/04/2022 [BRI/HIN] merubah tampilan reporting berdasarkan param IsPrintoutMasterEmpNotUseDetailInformation
    16/05/2022 [TYO/HIN] menarik data dari personal information saat copy data
    15/06/2022 [ICA/PHT] mengubah modifier variabel mIsEmployeeUseFirstGrade menjadi internal agar bisa di panggil di frm find
    26/07/2022 [VIN/PHT] Bug saat Edit Vaccine
    31/01/2023 [RDA/ALL] bug show data field level (yg txt) dari employee
    15/03/2023 [HAR/PHT] Marital Status pada Master Employee mandatory
                         Menambahkan file category di employee file
                         Menambahkan Upload dan Download File pada tab PERSONAL REF+VAKSIN, WORK EXP, EDUCATION, TRAINING, COMPETENCE, POSITION HISTORY, GRADE HISTORY, WARNING LETTER
                         Menyembunyikan field Biological Mother
    17/03/2023 [HAR/PHT] bug fixing di tab grad history saat view button dowloadnya aktif, harusnya aktif saat posisi edit
    20/03/2023 [RDA/PHT] ubah label di printout employee (Pengalaman Kerja jadi Pengalaman & Penugasan)
    26/03/2023 [SET/PHT] menyesuaikan source data tab warning letter
    05/04/2023 [WED/PHT] tambah parameter baru saat edit Employee
    05/04/2023 [WED/PHT] tambah spouse employee code, berdasarkan parameter IsMaritalStatusMandatory
    18/04/2023 [MYA/PHT] Menambahkan kolom Short Code pada Find Master Employee
    18/04/2023 [MYA/PHT] Menambahkan Validasi pada Filter "Employee" di Master Employee agar bisa search Short Code
    18/04/2023 [MYA/PHT] Menampilkan field Spouse Employee Code ketika find master employee
    18/04/2023 [MYA/PHT] Menambahkan Validasi tidak bisa edit Master Employee Tab Personal Ref+Vaksin (Family)
    25/04/2024 [TKG/GSS] Merubah format setting phone number
*/
#endregion


#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;

using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmEmployee : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal FrmEmployeeFind FrmFind;
        private bool
            mIsEmpCodeAutoGenerate = false,
            mIsInsert = true,
            mIsEmployeeCOAInfoMandatory = false,
            mIsPPSActive = false,
            mIsPayrollActived = false,
            mIsEmpCodeCopyToOldCode = false,
            mIsAnnualLeaveUseStartDt = false,
            mIsEmployeeAutoCreatePPS = false,
            mIsSiteDivisionEnabled = false,
            mIsDivisionDepartmentEnabled = false,
            mIsDepartmentPositionEnabled = false,
            mIsEmployeeAllowToUploadFile = false,
            mIsEmployeeEducationInputManualData = false,
            mIsEmployeePrintoutNotUseCompetenceAndPotency = false,
            mIsOldCodeAutoFillAfterPromotion = false,
            mIsEmployeeAllowedToEditCompetence = false,
            mIsEmployeeUpdateNeedApproval = false,
            mIsEmpSpecializationNotMandatory = false,
            mIsEmployeeTabWarningLetterEnabled = false,
            mIsEmployeeTabAwardEnabled = false,
            mIsEmployeeTabPositionHistoryEnabled = false,
            mIsEmployeeTabTraining3Enabled = false,
            mIsEmployeeTabGradeHistoryEnabled = false,
            mIsPrintoutMasterEmpNotUseDetailInformation = false,
            mIsEmpWLAllowToEditFile = false,
            mIsEmployeeNameNotEditable = false,
            mIsOldCodeNotEditable = false,
            mIsShortCodeNotEditable = false,
            mIsDisplayNameNotEditable = false,
            mIsUserCodeNotEditable = false,
            mIsBirthPlaceNotEditable = false,
            mIsBirthDateNotEditable = false,
            mIsEmployeeTabPersonalRefNotEditable = false
            ;
            
        internal bool
            mIsEmployeeUseCodeOfConduct = false,
            mIsEmployeeUseGradeSalary = false,
            mIsEmpMeritIncreaseExists = false,
            mIsEmployeeUseFirstGrade = false,
            mIsEmployeeUseBiologicalMother = false,
            mIsMaritalStatusMandatory = false,
            mIsEmployeeFindShowFamily = false
            ;

        private string
            mEmployeeAcNo = string.Empty,
            mEmployeeAcDesc = string.Empty,
            mAcNoForAdvancePayment = string.Empty,
            mAcDescForAdvancePayment = string.Empty,
            mProfilePicture = string.Empty,
            mEmployeeTrainingTabCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty,
            mEmpContractDocEmploymentStatus = string.Empty,
            mGenerateEmpCodeFormat = string.Empty,
            mEmploymentStatusTetap = string.Empty,
            mDocTitle = string.Empty,
            mSSRetiredMaxAge2 = string.Empty;

        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        internal bool
            mIsSiteMandatory = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsDeptFilterByDivision = false,
            mIsNewEmployeeInformationAmendmentActive = false,
            mIsEmpContractDtMandatory = false,
            mIsEmpOldCodeEditable = false,
            mCopyDataActive = false,
            mIsEmployeeTabAdditionalEnabled = false,
            mIsPltCheckboxNotBasedOnPosition = false,
            mIsEmployeeLevelEnabled = false,
            mIsEmployeeVaccineEnabled = false,
            mIsUsePeriodicAdvancementMenu = Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmPeriodicSalaryAdvancement' Or Param = 'FrmPeriodicGradeAdvancement';")
            ;
        internal int mUsePersonalInformation = 1;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;
        
        #endregion

        #region Constructor

        public FrmEmployee(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();

                SetFormControl(mState.View);

                tabControl1.SelectTab("TpgAdditional");
                if (mIsEmployeeTabAdditionalEnabled)
                {
                    if (mIsEmployeeLevelEnabled) Sl.SetLueLevelCode(ref LueLevelCode);
                }
                else
                {
                    TpgAdditional.Enabled = false;
                    TpgAdditional.Visible = false;
                }

                tabControl1.SelectTab("TpgGeneral");

                Sl.SetLueEntCode(ref LueEntityCode);
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLuePositionStatusCode(ref LuePositionStatusCode);
                Sl.SetLueGender(ref LueGender);
                Sl.SetLueReligion(ref LueReligion);
                Sl.SetLueCityCode(ref LueCity);
                Sl.SetLueEmployeePayrollType(ref LuePayrollType);
                Sl.SetLuePTKP(ref LuePTKP);
                Sl.SetLueBankCode(ref LueBankCode);
                if (mIsEmployeeUseGradeSalary)
                    Sl.SetLueGradeSalary(ref LueGrdLvlCode);
                else
                    Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLueGrdLvlCode(ref LueFirstGrdLvlCode);
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueOption(ref LueMaritalStatus, "MaritalStatus");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LueBloodType, "BloodType");
                Sl.SetLueOption(ref LueBloodRhesus, "BloodRhesusType");
                Sl.SetLueOption(ref LueClothesSize, "EmployeeClothesSize");
                Sl.SetLueOption(ref LueShoeSize, "EmployeeShoeSize");
                Sl.SetLueOption(ref LueEducationType, "EmployeeEducationType");
                Sl.SetLueOption(ref LueCostGroup, "EmpCostGroup");
                SetLueSection(ref LueSection);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                SetLueWorkGroup(ref LueWorkGroup, Sm.GetLue(LueSection));
                if (!mIsSiteDivisionEnabled) 
                    Sl.SetLueDivisionCode(ref LueDivisionCode);
                //if (!mIsDeptFilterByDivision) Sl.SetLueDeptCode(ref LueDeptCode);

                if (!mIsDeptFilterByDivision)
                    SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");

                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red; 
                if (mIsEmpContractDtMandatory) LblContractDt.ForeColor = Color.Red;
                if (mIsMaritalStatusMandatory) lblMarital.ForeColor = Color.Red;
                LblSpouseEmpCode.Visible = TxtSpouseEmpCode.Visible = BtnSpouseEmpCode.Visible = mIsMaritalStatusMandatory;

                tabControl1.SelectTab("TpgEmployeeFamily");
                Sl.SetLueFamilyStatus(ref LueStatus);
                LueStatus.Visible = false;
                Sl.SetLueGender(ref LueFamGender);
                LueFamGender.Visible = false;
                Sl.SetLueOption(ref LueProfessionCode, "Profession");
                LueProfessionCode.Visible = false;
                Sl.SetLueOption(ref LueEducationLevelCode, "EmployeeEducationLevel");
                LueEducationLevelCode.Visible = false;
                DteFamBirthDt.Visible = false;
                SetGrd();
                SetGrd7();

                tabControl1.SelectTab("TpgEmployeeWorkExp");
                SetGrd2();

                tabControl1.SelectTab("TpgEmployeeEducation");
                Sl.SetLueMajorCode(ref LueMajor);
                Sl.SetLueFacCode(ref LueFacCode, string.Empty);
                Sl.SetLueEmployeeEducationField(ref LueField);
                Sl.SetLueEmployeeEducationLevel(ref LueLevel);
                Sl.SetLueFacCode(ref LueFacCode, string.Empty);
                LueLevel.Visible = false;
                LueField.Visible = false;
                LueMajor.Visible = false;
                LueFacCode.Visible = false;
                SetGrd3();

                tabControl1.SelectTab("TpgEmployeeTraining");
                SetGrd8();
                LueTrainingCode.Visible = false;

                tabControl1.SelectTab("TpgEmployeeTraining2");
                SetGrd9();
                SetGrd10();
                LueTrainingCode2.Visible = false;

                tabControl1.SelectTab("TpgEmployeeCompetence");
                SetGrd4();
                SetLueCompetenceCode(ref LueCompetenceCode);
                LueCompetenceCode.Visible = false;

                tabControl1.SelectTab("TpgEmployeeSS");
                SetGrd5();
                SetGrd6();

                tabControl1.SelectTab("TpgVaccine");
                DteVaccineDt.Visible = false;
                if (mIsEmployeeVaccineEnabled)
                    SetGrd12();
                else
                    tabControl1.TabPages.Remove(TpgVaccine);

                tabControl1.SelectTab("TpgWarningLetter");
                if (mIsEmployeeTabWarningLetterEnabled)
                    SetGrd13();
                else
                    tabControl1.TabPages.Remove(TpgWarningLetter);

                tabControl1.SelectTab("TpgAward");
                if (mIsEmployeeTabAwardEnabled)
                {
                    SetGrd14();
                    Sl.SetLueOption(ref LueAwardCt, "EmployeeAwardCategory");
                    LueAwardCt.Visible = false;
                }
                else
                    tabControl1.TabPages.Remove(TpgAward);

                tabControl1.SelectTab("TpgPositionHistory");
                if (mIsEmployeeTabPositionHistoryEnabled)
                {
                    SetGrd15();
                }
                else
                    tabControl1.TabPages.Remove(TpgPositionHistory);

                tabControl1.SelectTab("TpgGradeHistory");
                if (mIsEmployeeTabGradeHistoryEnabled)
                {
                    SetGrd17();
                }
                else
                    tabControl1.TabPages.Remove(TpgGradeHistory);

                tabControl1.SelectTab("TpgEmployeeTraining3");
                if (mIsEmployeeTabTraining3Enabled)
                {
                    SetGrd16();
                    Sl.SetLueTrainingCode(ref LueTrainingCode3);
                    Sl.SetLueOption(ref LueTrainingType, "EmployeeTrainingType");
                    Sl.SetLueOption(ref LueTrainingSpecialization, "EmployeeTrainingSpecialization");
                    Sl.SetLueOption(ref LueTrainingEducationCenter, "EmployeeTrainingEducationCenter");
                    Sl.SetLueOption(ref LueTrainingCategory, "EmployeeTrainingCategory");
                    LueTrainingCode3.Visible = false;
                    LueTrainingType.Visible = false;
                    LueTrainingSpecialization.Visible = false;
                    LueTrainingEducationCenter.Visible = false;
                    LueTrainingCategory.Visible = false;
                }
                else
                    tabControl1.TabPages.Remove(TpgEmployeeTraining3);

                if (mEmployeeTrainingTabCode == "2")
                {
                    tabControl1.TabPages.Remove(TpgEmployeeTraining);
                    tabControl1.TabPages.Remove(TpgEmployeeTraining3);
                }

                if (mEmployeeTrainingTabCode == "1")
                {
                    tabControl1.TabPages.Remove(TpgEmployeeTraining2);
                    tabControl1.TabPages.Remove(TpgEmployeeTraining3);
                }

                if (mEmployeeTrainingTabCode == "3")
                {
                    tabControl1.TabPages.Remove(TpgEmployeeTraining);
                    tabControl1.TabPages.Remove(TpgEmployeeTraining2);
                }

                tabControl1.SelectTab("TpgPicture");
                SetGrd11();
                Sl.SetLueOption(ref LueUploadFileCategory, "EmployeeFileUploadCategory");
                LueUploadFileCategory.Visible = false;

                tabControl1.SelectTab("TpgGeneral");
                if (mIsPayrollActived)
                {
                    LblDeptCode.ForeColor = Color.Red;   
                    LblPosCode.ForeColor = Color.Red;   
                    LblGrdLvlCode.ForeColor = Color.Red;   
                    LblPGCode.ForeColor = Color.Red;   
                    LblEmploymentStatus.ForeColor = Color.Red;   
                    LblSystemType.ForeColor = Color.Red;
                    LblPayrunPeriod.ForeColor = Color.Red;   
                }
                if (!mIsEmployeeUseBiologicalMother)
                {
                    LblMother.Visible = false;
                    TxtMother.Visible = false;
                }
                
                if(!mIsEmployeeUseFirstGrade)
                {
                    label74.Visible = LueFirstGrdLvlCode.Visible = false;
                }

               

                mCopyDataActive = false;
                //if this application is called from other application
                if (mEmpCode.Length != 0)
                {
                    this.Text = "Master Employee";
                    ShowData(mEmpCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
             if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region SetGrd1

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Family Name",
                        "Status Code",
                        "Status",
                        "Gender Code",
                        "Gender",
                        
                        //6-10
                        "Birth Date",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",
                        "Remark",
                        "Profession Code",

                        //11-13
                        "Profession",
                        "Education Level Code",
                        "Latest Education"
                    },
                     new int[] 
                    {
                        0, 
                        200, 0, 100, 0, 80, 
                        80, 150, 150, 300, 0,
                        200, 0, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] {0, 2, 4, 10, 12}, false);
            Grd1.Cols[11].Move(9);
            Grd1.Cols[13].Move(10);
        }

        #endregion

        #region SetGrd2

        private void SetGrd2()
        {
            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-5
                        "Company",
                        "Position",
                        "Period",
                        "Remark",
                        "File Name",
                        //6-8
                        "U",
                        "D",
                        "File Name2"
                    },
                     new int[] 
                    {
                        0, 
                        150, 150, 150, 250, 200,
                        20, 20, 120

                    }
                );

            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd2, new int[] { 5, 6, 7, 8 }, false);
            else
            {
                Sm.GrdColButton(Grd2, new int[] { 6 }, "1");
                Sm.GrdColButton(Grd2, new int[] { 7 }, "2");
                Sm.GrdColReadOnly(Grd2, new int[] { 5, 8 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 8 }, false);
            }
        }
        #endregion

        #region Setgrd3

        private void SetGrd3()
        {
            Grd3.Cols.Count = 17;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "School",
                        "Level Code",
                        "Level",
                        "Major Code",
                        "Major",

                        //6-10
                        "Field Code",
                        "Field",
                        "Graduation Year",
                        "Highest",
                        "Remark",

                        //11-15
                        "Faculty Code",
                        "Faculty",
                        "File Name",
                        "U",
                        "D",
                        //16
                        "File Name2"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        250, 0, 180, 0, 180, 
                        
                        //6-10
                        0, 180, 100, 80, 300,

                        //11-15
                        0, 200, 200, 20, 20,

                        //16
                        150
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 9 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4, 6, 11 }, false);
            Grd3.Cols[12].Move(8);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd3, new int[] { 13, 14, 15, 16 }, false);
            else
            {
                Sm.GrdColButton(Grd3, new int[] { 14 }, "1");
                Sm.GrdColButton(Grd3, new int[] { 15 }, "2");
                Sm.GrdColReadOnly(Grd3, new int[] { 13, 16 }, true);
                Sm.GrdColInvisible(Grd3, new int[] { 16 }, false);
            }

        }
        #endregion

        #region SetGrd4

        private void SetGrd4()
        {
            Grd4.Cols.Count = 9;
            Grd4.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Code",
                        "Name",
                        "Description",
                        "Score",
                        "File Name",
                        //6-8
                        "U",
                        "D",
                        "File Name",
                    },
                     new int[] 
                    {
                        0, 
                        0, 150, 300, 100, 200,
                        20, 20, 150
                    }
                );

            Sm.GrdColInvisible(Grd4, new int[] { 0, 1 }, false);
            Sm.GrdFormatDec(Grd4, new int[] { 4 }, 0);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd4, new int[] { 5, 6, 7, 8 }, false);
            else
            {
                Sm.GrdColButton(Grd4, new int[] { 6 }, "1");
                Sm.GrdColButton(Grd4, new int[] { 7 }, "2");
                Sm.GrdColReadOnly(Grd4, new int[] { 5, 8 }, true);
                Sm.GrdColInvisible(Grd4, new int[] { 8 }, false);
            }

        }

        #endregion

        #region SetGrd5

        private void SetGrd5()
        {
            Grd5.Cols.Count = 9;
            Grd5.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Social"+Environment.NewLine+"Security Code",
                        "",
                        "Social"+Environment.NewLine+"Security Name",
                        "Card#",
                        "Health Facility",
                        
                        //6-8
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        80, 20, 200, 120, 300, 
                        
                        //6-8
                        80, 80, 300
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd5, new int[] { 6, 7 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1, 2 }, false);
        }
        #endregion

        #region SetGrd6

        private void SetGrd6()
        {
            Grd6.Cols.Count = 14;
            Grd6.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                       //0
                        "DNo",

                        //1-5
                        "Family Name",
                        "Status",
                        "Gender",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",

                        //6-10
                        "Social"+Environment.NewLine+"Security Code",
                        "",
                        "Social"+Environment.NewLine+"Security Name",
                        "Card#",
                        "Health Facility",
                        
                        //11-13
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date", 
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 120, 120, 120, 

                        //6-10
                        100, 20, 200, 120, 300, 

                        //11-13
                        80, 80, 250
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColButton(Grd6, new int[] { 7 });
            Sm.GrdFormatDate(Grd6, new int[] { 11, 12 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 6, 7 }, false);
        }
        #endregion

        #region SetGrd7
    
        private void SetGrd7()
        {
            Grd7.Cols.Count = 5;
            Grd7.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-4
                        "Name",
                        "Phone",
                        "Address",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 
                        150, 150, 250, 250,
                    }
                );

            Sm.GrdColInvisible(Grd7, new int[] { 0 }, false);
        }

        #endregion

        #region SetGrd8

        private void SetGrd8()
        {
            Grd8.Cols.Count = 10;
            Grd8.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd8,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "TrainingCode",
                    "Training",
                    "Specialization",
                    "Place",
                    "Period",

                    //6-8
                    "Month/Year",
                    "Remark",
                    "Training Evaluation#"
                },
                new int[] 
                {
                    0, 
                    100, 180, 200, 200, 150, 
                    120, 300, 130 
                }
            );
            Sm.GrdColInvisible(Grd8, new int[] { 0, 1 }, false); 
            Sm.GrdColReadOnly(true, true, Grd8, new int[] { 8 });
        }

        #endregion

        #region SetGrd9

        private void SetGrd9()
        {
            Grd9.Cols.Count = 9;
            Grd9.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd9,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
		            "Training Evaluation#",
                    "TrainingCode",
                    "Training",
                    "Specialization",
                    "Place",
                    

                    //6-8
		            "Period",
                    "Month/Year",
                    "Remark",
                    
                },
                new int[] 
                {
                    0, 
                    130, 100, 180, 200, 200, 
                    150, 120, 300 
                }
            );
            Sm.GrdColInvisible(Grd9, new int[] { 0, 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd9, new int[] { 1 });
        }

        #endregion

        #region SetGrd10

        private void SetGrd10()
        {
            Grd10.Cols.Count = 9;
            Grd10.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd10,
                new string[] 
                {
                     //0
                    "DNo",
                    
                    //1-5
                    "TrainingCode",
                    "Training",
                    "Specialization",
                    "Place",
                    "Period",

                    //6-8
                    "Month/Year",
                    "Remark",
                    "Training Evaluation#"
                },
                new int[] 
                {
                    0, 
                    100, 180, 200, 200, 150, 
                    120, 300, 130 
                }
            );
            Sm.GrdColInvisible(Grd10, new int[] { 0, 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd10, new int[] { 8 });
        }

        #endregion

        #region SetGrd11

        private void SetGrd11()
        {
            Grd11.Cols.Count = 7;
            Grd11.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd11,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "U",
                        "D",
                        "File Name2",
                        "OptCode",
                        //6
                        "File Category"
                    },
                     new int[] 
                    {
                        0, 
                        200, 20, 20, 200, 20,
                        150
                    }
                );

            Sm.GrdColInvisible(Grd11, new int[] { 0, 4, 5}, false);
            Sm.GrdColButton(Grd11, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd11, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd11, new int[] {0, 1, 4, 5});
        }

        #endregion

        #region SetGrd12

        private void SetGrd12()
        {
            Grd12.Cols.Count = 8;
            Grd12.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd12,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "Date",
                        "Vaccine",
                        "Remark",
                        "File Name",
                        "U",
                        //6
                        "D",
                        "File Name2",
                    },
                     new int[] 
                    {
                        0, 
                        100, 200, 400, 200,20,
                        20, 150
                    }
                );
            Sm.GrdFormatDate(Grd12, new int[] { 1 });
            Sm.GrdColInvisible(Grd12, new int[] { 0 }, false);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd12, new int[] { 4, 5, 6, 7 }, false);
            else
            {
                Sm.GrdColButton(Grd12, new int[] { 5 }, "1");
                Sm.GrdColButton(Grd12, new int[] { 6 }, "2");
                Sm.GrdColReadOnly(Grd12, new int[] { 4, 7 }, true);
                Sm.GrdColInvisible(Grd12, new int[] { 7 }, false);
            }

        }

        #endregion

        #region SetGrd13

        private void SetGrd13()
        {
            Grd13.Cols.Count = 12;
            Grd13.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd13,
                    new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Document",
                        "",
                        "Date",
                        "Reference#",
                        "Year",
                        
                        //6-10
                        "Warning Letter",
                        "Category",
                        "File Name",
                        "U",
                        "D",
                        //11
                        "File Name2",

                    },
                     new int[]
                    {
                        0,
                        180, 20, 100, 150, 80, 
                        200, 150, 200, 20, 20,
                        120
                    }
                );
            Sm.GrdColReadOnly(Grd13, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDate(Grd13, new int[] { 3 });
            Sm.GrdColButton(Grd13, new int[] { 2 });
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd13, new int[] { 8, 9, 10, 11 }, false);
            else
            {
                Sm.GrdColButton(Grd13, new int[] { 9}, "1");
                Sm.GrdColButton(Grd13, new int[] { 10 }, "2");
                Sm.GrdColReadOnly(Grd13, new int[] { 8, 11}, true);
                Sm.GrdColInvisible(Grd13, new int[] { 11 }, false);
            }

        }

        #endregion

        #region SetGrd14

        private void SetGrd14()
        {
            Grd14.Cols.Count = 9;
            Grd14.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd14,
                    new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Category Code",
                        "Category",
                        "Description",
                        "Year",
                        "File Name",
                         //6-8
                        "U",
                        "D",
                        "File Name2",
                    },
                     new int[]
                    {
                        0,
                        0, 200, 400, 80, 200,
                        20, 20, 150
                    }
                );
            Sm.GrdColInvisible(Grd14, new int[] { 0, 1 }, false);
            Sm.GrdFormatDec(Grd14, new int[] { 4 }, 99);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd14, new int[] { 5, 6, 7, 8 }, false);
            else
            {
                Sm.GrdColButton(Grd14, new int[] { 6 }, "1");
                Sm.GrdColButton(Grd14, new int[] { 7 }, "2");
                Sm.GrdColReadOnly(Grd14, new int[] { 5,8 }, true);
                Sm.GrdColInvisible(Grd14, new int[] { 8 }, false);
            }

        }

        #endregion

        #region SetGrd15

        private void SetGrd15()
        {
            Grd15.Cols.Count = 17;
            Grd15.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd15,
                    new string[]
                    {
                        //0
                        "Document#",

                        //1-5
                        "Cancel",
                        "PPS",
                        "Date",
                        "Decision Letter",
                        "Job Transfer",

                        //6-10
                        "Position From",
                        "Position To",
                        "Grade",
                        "Level",
                        "Department",

                        //11-15
                        "Site",
                        "Division",
                         "File Name",
                        "U",
                        "D",
                        //16
                         "File Name2",
                    },
                     new int[]
                    {
                        150,
                        80, 80, 100, 200, 200, 
                        200, 200, 200, 200, 200, 
                        200, 200, 200, 20, 20,
                        150
                    }
                );
            Sm.GrdColInvisible(Grd15, new int[] { 1, 2 }, false);
            Sm.GrdColCheck(Grd15, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd15, new int[] { 3 });
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd15, new int[] { 13, 14, 15, 16 }, false);
            else
            {
                Sm.GrdColButton(Grd15, new int[] { 14 }, "1");
                Sm.GrdColButton(Grd15, new int[] { 15 }, "2");
                Sm.GrdColReadOnly(Grd15, new int[] { 13, 16 }, true);
                Sm.GrdColInvisible(Grd15, new int[] {  16 }, false);
            }

        }

        #endregion

        #region SetGrd16

        private void SetGrd16()
        {
            Grd16.Cols.Count = 22;
            Grd16.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd16,
                    new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Training Code",
                        "Training",
                        "Type Code",
                        "Type",
                        "Specialization Code",

                        //6-10
                        "Specialization",
                        "Category Code",
                        "Place",
                        "Year",
                        "Duration",

                        //11-15
                        "Period",
                        "Value",
                        "Ranking",
                        "Remark",
                        "Education Cente Code",
                        
                        //16-20
                        "Education Center",
                        "Category", 
                        "File Name",
                        "U",
                        "D",
                        //21
                        "File Name2"
                    },
                     new int[]
                    {
                        0,
                        0, 200, 0, 200, 0,
                        200, 0, 200, 100, 100,
                        100, 100, 100, 200, 0,
                        200, 200, 200, 20, 20,
                        150
                    }
                );
            Sm.GrdColInvisible(Grd16, new int[] { 0, 1, 3, 5, 7, 15 }, false);
            Sm.GrdFormatDec(Grd16, new int[] { 9, 11, 13 }, 99);
            Sm.GrdFormatDec(Grd16, new int[] { 12 }, 2);
            Grd16.Cols[16].Move(1);
            Grd16.Cols[17].Move(8);
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd16, new int[] { 18, 19, 20, 21 }, false);
            else
            {
                Sm.GrdColButton(Grd16, new int[] { 19 }, "1");
                Sm.GrdColButton(Grd16, new int[] { 20 }, "2");
                Sm.GrdColReadOnly(Grd16, new int[] { 18, 21 }, true);
                Sm.GrdColInvisible(Grd16, new int[] { 21 }, false);
            }

        }

        #endregion

        #region SetGrd17

        private void SetGrd17()
        {
            Grd17.Cols.Count = 13;
            Grd17.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd17,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Decision Letter",
                        "Duration",
                        "Grade",
                        "Remark",
                        
                        //6-10
                        "Periodic Setting Ind",
                        "Grade Level Code",
                        "Amount",
                        "File Name",
                        "U",
                        //11
                        "D",
                        "File Name2"
                    },
                     new int[]
                    {
                        0,
                        100, 200, 80, 200, 400,
                        0, 0, 0, 200, 20, 
                        20, 150
                    }
                );
            Sm.GrdColCheck(Grd17, new int[] { 6 });
            Sm.GrdColInvisible(Grd17, new int[] { 0, 6, 7, 8 }, false);
            Sm.GrdFormatDec(Grd17, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd17, new int[] { 1 });
            Sm.GrdColReadOnly(true, false, Grd17, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            if (!mIsEmployeeAllowToUploadFile)
                Sm.GrdColInvisible(Grd17, new int[] { 9, 10, 11, 12 }, false);
            else
            {
                Sm.GrdColButton(Grd17, new int[] { 10 }, "1");
                Sm.GrdColButton(Grd17, new int[] { 11 }, "2");
                Sm.GrdColReadOnly(Grd17, new int[] { 9, 12 }, true);
                Sm.GrdColInvisible(Grd17, new int[] {12 }, false);
            }
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode, LuePosCode,
                        DteJoinDt, DteResignDt, DteTGDt, TxtIdNumber,LueGender,LueReligion,
                        TxtBirthPlace,DteBirthDt,MeeAddress,LueCity,TxtPostalCode,LueEntityCode,
                        LueSiteCode, MeeDomicile, LueBloodType, LueSection, LueWorkGroup,
                        TxtPhone,TxtMobile,TxtEmail,LueGrdLvlCode,TxtNPWP,
                        LuePayrollType,LuePTKP,LueBankCode, TxtBankBranch, TxtBankAcName, 
                        TxtBankAcNo, LueStatus,LueFamGender,DteFamBirthDt, TxtEmpCodeOld, 
                        TxtShortCode, TxtBarcodeCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, 
                        TxtVillage, TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt,
                        LueSystemType, LueDivisionCode, LuePGCode, TxtPOH, LueCompetenceCode,
                        DteLeaveStartDt, LueTrainingCode, LuePositionStatusCode, LueTrainingCode2, DteContractDt,
                        LueClothesSize, LueShoeSize, LueEducationType, LueCostGroup, ChkActingOfficial,
                        LueProfessionCode, LueEducationLevelCode, LueLevelCode, DteVaccineDt, LueBloodRhesus, 
                        LueFirstGrdLvlCode, LueUploadFileCategory
                    }, true);
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = Grd9.ReadOnly = Grd10.ReadOnly = 
                        Grd11.ReadOnly = Grd12.ReadOnly = Grd14.ReadOnly = Grd15.ReadOnly = Grd16.ReadOnly = Grd17.ReadOnly = true;
                    BtnPicture.Enabled = true;
                    LblCopyGeneral.Visible = false;
                    BtnCopyGeneral.Visible = false;
                    BtnCopyGeneral.Enabled = false;
                    BtnSpouseEmpCode.Enabled = false;
                    TxtEmpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode, LuePosCode,
                        DteJoinDt,DteResignDt, DteTGDt, TxtIdNumber,LueGender,LueReligion,
                        TxtBirthPlace,DteBirthDt,MeeAddress,LueCity,TxtPostalCode,LueEntityCode,
                        LueSiteCode, MeeDomicile, LueBloodType, LueSection, LueWorkGroup, 
                        TxtPhone,TxtMobile,TxtEmail,LueGrdLvlCode,TxtNPWP,  
                        LuePayrollType,LuePTKP,LueBankCode, TxtBankBranch, TxtBankAcName, 
                        TxtBankAcNo, LueStatus, LueFamGender, DteFamBirthDt, TxtEmpCodeOld, 
                        TxtShortCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, TxtVillage, 
                        TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt, LueSystemType,
                        LueDivisionCode, LuePGCode, TxtPOH, LueCompetenceCode, LueTrainingCode, LueTrainingCode2, 
                        LuePositionStatusCode, DteContractDt, LueClothesSize, LueShoeSize, LueEducationType, 
                        LueCostGroup, LueProfessionCode, LueEducationLevelCode, LueLevelCode, DteVaccineDt, LueBloodRhesus,
                        LueFirstGrdLvlCode, LueUploadFileCategory
                    }, false);
                    if (!mIsEmpCodeAutoGenerate) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtEmpCode}, false );
                    if (mIsSiteDivisionEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDivisionCode }, true);
                    if (mIsDivisionDepartmentEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, true);
                    if (mIsDepartmentPositionEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, true);
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = Grd10.ReadOnly = 
                        Grd11.ReadOnly = Grd12.ReadOnly = Grd14.ReadOnly = Grd15.ReadOnly = Grd16.ReadOnly = false;
                    LblCopyGeneral.Visible = true;
                    BtnCopyGeneral.Visible = true;
                    BtnCopyGeneral.Enabled = true;
                    ChkActingOfficial.Properties.ReadOnly = true;
                    TxtEmpCode.Focus();
                    break;
                case mState.Edit:
                    //GRADE LEVEL TIDAK BOLEH DIEDIT
                    if (mIsNewEmployeeInformationAmendmentActive)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            TxtDisplayName, DteTGDt, MeeAddress, LueCity, TxtSubDistrict, 
                            TxtVillage, TxtRTRW, TxtPostalCode, MeeDomicile, TxtIdNumber, 
                            LueMaritalStatus, DteWeddingDt, LueGender, LueReligion, TxtBirthPlace, 
                            DteBirthDt, LueBloodType, TxtMother, TxtNPWP, LuePTKP, 
                            LueBankCode, TxtBankBranch, TxtBankAcName, TxtBankAcNo, TxtPhone, 
                            TxtMobile, TxtEmail, LueStatus, LueFamGender, DteFamBirthDt, 
                            LueLevel, LueTrainingCode, LueTrainingCode2, LuePositionStatusCode, LueClothesSize, 
                            LueShoeSize, LueEducationType, LueCostGroup, LueProfessionCode, LueEducationLevelCode,
                            DteVaccineDt, LueBloodRhesus, LueUploadFileCategory
                        }, false);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            TxtEmpCode, TxtEmpName, TxtUserCode, TxtShortCode, TxtBarcodeCode, 
                            TxtEmpCodeOld, LueEmploymentStatus, LueEntityCode, LueSiteCode, LueDivisionCode, 
                            LueDeptCode, LuePosCode, LueSection, LueWorkGroup, TxtPOH, 
                            DteJoinDt, LueSystemType, LueGrdLvlCode, LuePGCode, LuePayrollType, 
                            LuePayrunPeriod, DteContractDt, LueCostGroup, LueLevelCode, LueFirstGrdLvlCode
                        }, true);
                        Grd7.ReadOnly = Grd3.ReadOnly = Grd8.ReadOnly = Grd10.ReadOnly = 
                            Grd11.ReadOnly = Grd12.ReadOnly = Grd14.ReadOnly = Grd15.ReadOnly = Grd16.ReadOnly = false;
                        Grd2.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = Grd6.ReadOnly = true;

                        Grd1.ReadOnly = mIsEmployeeTabPersonalRefNotEditable;

                        if (mIsEmployeeAllowedToEditCompetence)
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit>{LueCompetenceCode}, false);
                            Grd4.ReadOnly = false;
                        }

                        BtnPicture.Enabled = false;
                        PicEmployee.Enabled = false;
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueSiteCode, LueDivisionCode, LueDeptCode, LuePosCode, TxtEmpName, 
                            TxtDisplayName, TxtUserCode, DteTGDt, TxtIdNumber, LueGender,
                            LueReligion, TxtBirthPlace, DteBirthDt, MeeAddress, LueCity,
                            TxtPostalCode,TxtPhone, LueEntityCode, MeeDomicile, LueBloodType, 
                            LueSection, LueWorkGroup, TxtMobile, TxtEmail, TxtNPWP, 
                            LuePayrollType, LuePTKP, LueBankCode, TxtBankBranch, TxtBankAcName, 
                            TxtBankAcNo, LueStatus, LueFamGender,DteFamBirthDt, TxtEmpCodeOld, 
                            TxtShortCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, TxtVillage, 
                            TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt, LueSystemType, 
                            LuePGCode, TxtPOH, LueTrainingCode, LueTrainingCode2, LuePositionStatusCode, 
                            DteContractDt, LueClothesSize, LueShoeSize, LueEducationType, LueCostGroup,
                            LueProfessionCode, LueEducationLevelCode, LueLevelCode, DteVaccineDt, LueBloodRhesus,
                            LueUploadFileCategory
                            //, LueCompetenceCode
                        }, false);

                        if(!mIsEmpOldCodeEditable){ Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtEmpCodeOld }, true); }
                        if (mIsPPSActive)
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            { 
                                LueSiteCode, LueDeptCode, LuePosCode, LueGrdLvlCode, LueEmploymentStatus, 
                                LueSystemType, LuePayrunPeriod, LuePositionStatusCode, LuePGCode, LueLevelCode 
                            }, true);
                            if (mIsSiteDivisionEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDivisionCode }, true);
                        }
                        else
                        {
                            if (mIsSiteDivisionEnabled && Sm.GetLue(LueSiteCode).Length==0) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDivisionCode }, true);
                            if (mIsDivisionDepartmentEnabled && Sm.GetLue(LueDivisionCode).Length == 0) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, true);
                            if (mIsDepartmentPositionEnabled && Sm.GetLue(LueDeptCode).Length == 0) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, true);
                        }
                        Grd2.ReadOnly = Grd3.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = Grd10.ReadOnly = 
                            Grd11.ReadOnly = Grd12.ReadOnly = Grd14.ReadOnly = Grd15.ReadOnly = Grd16.ReadOnly = Grd17.ReadOnly = false; //Grd4.ReadOnly = false

                        Grd1.ReadOnly = mIsEmployeeTabPersonalRefNotEditable;

                        if (mIsEmployeeAllowedToEditCompetence)
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCompetenceCode }, false);
                            Grd4.ReadOnly = false;
                        }
                    }
                    if (mIsAnnualLeaveUseStartDt) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteLeaveStartDt }, false);
                    LblCopyGeneral.Visible = false;
                    BtnCopyGeneral.Visible = false;
                    BtnCopyGeneral.Enabled = false;
					if (mIsUsePeriodicAdvancementMenu)
                    {
                        for (int i = 0; i < Grd17.Rows.Count - 1; i++)
                        {
                            if (Sm.GetGrdBool(Grd17, i, 6))
                            {
                                Grd17.Cells[i, 2].ReadOnly = iGBool.False;
                                Grd17.Cells[i, 5].ReadOnly = iGBool.False;
                            }
                        }
                    }

                    if (mIsEmployeeNameNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtEmpName }, true);
                    if (mIsOldCodeNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtEmpCodeOld }, true);
                    if (mIsShortCodeNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtShortCode }, true);
                    if (mIsDisplayNameNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtDisplayName }, true);
                    if (mIsUserCodeNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtUserCode }, true);
                    if (mIsBirthPlaceNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { TxtBirthPlace }, true);
                    if (mIsBirthDateNotEditable) Sm.SetControlReadOnly(new List<BaseEdit> { DteBirthDt }, true);

                    if (mIsMaritalStatusMandatory && Sm.GetLue(LueMaritalStatus) == "5") BtnSpouseEmpCode.Enabled = true;

                    TxtEmpName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode, 
                LuePosCode, DteJoinDt, DteResignDt, DteTGDt, TxtIdNumber, 
                LueGender, LueReligion, TxtBirthPlace, DteBirthDt, MeeAddress, 
                LueCity, TxtPostalCode,LueEntityCode, LueSiteCode, MeeDomicile, 
                LueBloodType, LueSection, LueWorkGroup, TxtPhone, TxtMobile, 
                TxtEmail, LueGrdLvlCode, TxtNPWP, LuePayrollType, LuePTKP, 
                LueBankCode, TxtBankBranch, TxtBankAcName, TxtBankAcNo, LueStatus, 
                LueFamGender, DteFamBirthDt, TxtEmpCodeOld, TxtShortCode, TxtBarcodeCode, 
                LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, TxtVillage, TxtRTRW, 
                TxtMother, LueMaritalStatus, DteWeddingDt, LueSystemType, LueDivisionCode, 
                LuePGCode, TxtPOH, LueCompetenceCode, DteLeaveStartDt, TxtLevelCode, 
                LueTrainingCode, LueTrainingCode2, TxtRegEntCode, LuePositionStatusCode, DteContractDt,
                LueClothesSize, LueShoeSize, LueEducationType, LueCostGroup, LueProfessionCode, 
                LueEducationLevelCode, LueLevel, DteVaccineDt, LueBloodRhesus, LueFirstGrdLvlCode, LueUploadFileCategory,
                TxtSpouseEmpCode
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            ClearGrd3();
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 4 });
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.ClearGrd(Grd9, true);
            Sm.ClearGrd(Grd10, true);
            Sm.ClearGrd(Grd11, true);
            if (mIsEmployeeVaccineEnabled) Sm.ClearGrd(Grd12, true);
            if (mIsEmployeeTabWarningLetterEnabled) Sm.ClearGrd(Grd13, true);
            if (mIsEmployeeTabAwardEnabled) Sm.ClearGrd(Grd14, true);
            if (mIsEmployeeTabPositionHistoryEnabled) Sm.ClearGrd(Grd15, true);
            if (mIsEmployeeTabTraining3Enabled) Sm.ClearGrd(Grd16, true);
            if (mIsEmployeeTabGradeHistoryEnabled) Sm.ClearGrd(Grd17, true);
            ChkActingOfficial.Checked = false;
            PicEmployee.Image = null;
            mProfilePicture = string.Empty;
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd3, 0, new int[] { 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
              if (FrmFind == null) FrmFind = new FrmEmployeeFind(this);
               Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                mIsInsert = true;
                if (!mIsDeptFilterByDivision) 
                    SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR?"Y":"N");
                SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                mCopyDataActive = false;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "", false)) return;
            SetFormControl(mState.Edit);
            mIsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Delete From TblEmployeeFamily Where EmpCode=@EmpCode; ");
                SQL.AppendLine("Delete From TblEmployeeWorkExp Where EmpCode=@EmpCode; ");
                SQL.AppendLine("Delete From TblEmployeeEducation Where EmpCode=@EmpCode; ");
                SQL.AppendLine("Delete From TblEmployeeCompetence Where EmpCode=@EmpCode; ");
                SQL.AppendLine("Delete From TblEmployeePersonalReference Where EmpCode=@EmpCode; ");
                if (mIsEmployeeTabAwardEnabled) SQL.AppendLine("Delete From TblEmployeeAward Where EmpCode=@EmpCode; ");
                if (mIsEmployeeTabPositionHistoryEnabled) SQL.AppendLine("Delete From TblEmployeePositionHistory Where EmpCode=@EmpCode; ");
                if (mIsEmployeeTabTraining3Enabled) SQL.AppendLine("Delete From TblEmployeeTraining2 Where EmpCode=@EmpCode; ");

                var cm = new MySqlCommand();
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                    IsDataNotValid()) return;

                var IsAutoCreatePPS = true;
                if (!mIsEmployeeAutoCreatePPS)
                {
                    if (Sm.StdMsgYN("Question", "Do you want to create PPS document for this new employee ?") == DialogResult.No) 
                        IsAutoCreatePPS = false;                    
                }


                var cml = new List<MySqlCommand>();

                if (mEmploymentStatusTetap.Length != 0 && mEmploymentStatusTetap == Sm.GetLue(LueEmploymentStatus) && mIsOldCodeAutoFillAfterPromotion) GenerateEmpCodeOld();

                if (TxtEmpCode.Text.Length == 0)
                {
                    if (mIsEmpCodeAutoGenerate) SetEmpCode();
                    cml.Add(SaveEmployee(IsAutoCreatePPS));
                }
                else
                    cml.Add(UpdateEmployee());

                cml.Add(SaveFamilyEmployee());
                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveFamilyEmployee(Row));

                cml.Add(SaveEmployeeWorkExp());
                //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) 
                //        cml.Add(SaveEmployeeWorkExp(Row));

                cml.Add(SaveEmployeeEducation());
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                //    {
                //        if (mIsEmployeeEducationInputManualData)
                //        {
                //            Grd3.Cells[Row, 4].Value = Sm.GetGrdStr(Grd3, Row, 5);
                //            Grd3.Cells[Row, 6].Value = Sm.GetGrdStr(Grd3, Row, 7);
                //            Grd3.Cells[Row, 11].Value = Sm.GetGrdStr(Grd3, Row, 12);
                //        }
                //        cml.Add(SaveEmployeeEducation(Row));
                //    }
                //}

                cml.Add(DeleteEmployeeFile());

                cml.Add(SaveEmployeeCompetence());
                //for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) 
                //        cml.Add(SaveEmployeeCompetence(Row));

                cml.Add(SaveEmployeePersonalReference());
                //for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0) 
                //        cml.Add(SaveEmployeePersonalReference(Row));

                cml.Add(SaveEmployeeFile());
                //for (int Row = 0; Row < Grd11.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd11, Row, 1).Length > 0)
                //        cml.Add(SaveEmployeeFile(Row));
                    

                if (mEmployeeTrainingTabCode == "2")
                {
                    cml.Add(SaveEmployeeTraining2());
                    //for (int r = 0; r < Grd10.Rows.Count; r++)
                    //    if (Sm.GetGrdStr(Grd10, r, 1).Length > 0)
                    //        cml.Add(SaveEmployeeTraining2(r));
                }
                else
                {
                    cml.Add(SaveEmployeeTraining());
                    //for (int r = 0; r < Grd8.Rows.Count; r++)
                    //    if (Sm.GetGrdStr(Grd8, r, 1).Length > 0)
                    //        cml.Add(SaveEmployeeTraining(r));
                }

                if (mIsEmployeeVaccineEnabled)
                {
                    cml.Add(SaveEmployeeVaccine());
                    //for (int r = 0; r < Grd12.Rows.Count; r++)
                    //    if (Sm.GetGrdStr(Grd12, r, 1).Length > 0)
                    //        cml.Add(SaveEmployeeVaccine(r));
                }

                if (mIsEmployeeTabAwardEnabled)
                    cml.Add(SaveEmployeeAward());
                
                if (mIsEmployeeTabTraining3Enabled)
                    cml.Add(SaveEmployeeTraining3());
                
				if (mIsEmployeeTabGradeHistoryEnabled && mIsUsePeriodicAdvancementMenu)
                    cml.Add(UpdateEmployeeGradeHistory());
                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd11.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd11, Row, 1).Length > 0)
                    {
                        if (mIsEmployeeAllowToUploadFile && Sm.GetGrdStr(Grd11, Row, 1).Length > 0 && Sm.GetGrdStr(Grd11, Row, 1) != "openFileDialog1")
                        {
                            if (Sm.GetGrdStr(Grd11, Row, 1) != Sm.GetGrdStr(Grd11, Row, 4))
                            {
                                UploadFile(TxtEmpCode.Text, Row, Sm.GetGrdStr(Grd11, Row, 1), "1", "");
                            }
                        }
                    }
                }

                if (mDocTitle == "PHT")
                {
                    UploadFileGrid(Grd2, 5, 8, "workexp");//work exp
                    UploadFileGrid(Grd3, 13, 16, "education");//educ
                    UploadFileGrid(Grd16, 18, 21, "training");//training
                    UploadFileGrid(Grd12, 4, 7, "vaccine");//vaccine
                    UploadFileGrid(Grd14, 5, 8, "award");//award
                    UploadFileGrid(Grd15, 13, 16, "position");//position
                    UploadFileGrid(Grd17, 9, 12, "grade");//grade
                    UploadFileGrid(Grd13, 8, 11, "warning");//warning
                    UploadFileGrid(Grd4, 5, 8, "competence");//competence
                }


                mCopyDataActive = false;
                ShowData(TxtEmpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {            
            ParPrint(mProfilePicture);   
        }

        #endregion

        #region Show Data

        public void ShowData(string EmpCode)
        {
            try
            {
                ClearData();
                ShowEmployee(EmpCode);
                ShowEmployeeFamily(EmpCode);
                ShowEmployeeWorkExp(EmpCode);
                ShowEmployeeEducation(EmpCode);
                ShowEmployeeCompetence(EmpCode);
                ShowEmployeeSS(EmpCode);
                ShowEmployeeFamilySS(EmpCode);
                ShowEmployeePersonalReference(EmpCode);
                if (mIsEmployeeTabAwardEnabled) ShowEmployeeAward(EmpCode);
                if (mIsEmployeeTabTraining3Enabled) ShowEmployeeTraining3(EmpCode);
                ShowEmployeeFile(EmpCode);
                if (mIsEmployeeVaccineEnabled) ShowEmployeeVaccine(EmpCode);
                if (mEmployeeTrainingTabCode == "2")
                {
                    ShowEmployeeTraining2(EmpCode);
                    ShowEmployeeTrainingEvaluation(EmpCode);
                }
                else 
                    ShowEmployeeTraining(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployee(string EmpCode)
        {
            string
                DivisionCode = string.Empty,
                DeptCode = string.Empty,
                PosCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            

            SQL.AppendLine("Select Distinct A.*, C.LevelName, D.EntName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr C On A.LevelCode=C.LevelCode ");
            SQL.AppendLine("Left Join TblEntity D On A.RegEntCode=D.EntCode ");
            //SQL.AppendLine("Inner Join TblPosition E On A.PosCode=E.PosCode ");
            SQL.AppendLine("Where EmpCode=@EmpCode;");
          

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "EmpCode",

                        //1-5
                        "EmpName", "DisplayName", "UserCode", "DeptCode", "PosCode",

                        //6-10
                        "JoinDt","ResignDt","IdNumber","Gender","Religion",

                        //11-15
                        "BirthPlace","BirthDt","Address","CityCode","PostalCode",

                        //16-20
                        "Phone","Mobile","GrdLvlCode","Email","NPWP",

                        //21-25
                        "PayrollType","PTKP","BankCode","BankBranch","BankAcName",

                        //26-30
                        "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode", "PayrunPeriod", 

                        //31-35
                        "EmploymentStatus","Mother", "SubDistrict", "Village", "RTRW", 

                        //36-40
                        "MaritalStatus","WeddingDt", "SystemType", "EntCode", "SiteCode", 
                        
                        //41-45
                         "Domicile", "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", 

                         //46-50
                         "PGCode", "POH", "TGDt", "LeaveStartDt", "LevelName",

                         //51-55
                         "EntName", "PositionStatusCode", "ContractDt", "ClothesSize", "ShoeSize",

                         //56-60
                         "EducationType", "CostGroup", "LevelCode", "ActingOfficialInd", "RhesusType",

                         //61-62
                         "FirstGrdLvlCode", "SpouseEmpCode"
                    },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtEmpCode.EditValue = mCopyDataActive ? string.Empty : Sm.DrStr(dr, c[0]);
                            TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtUserCode.EditValue = Sm.DrStr(dr, c[3]);

                            DeptCode = Sm.DrStr(dr, c[4]);
                            PosCode = Sm.DrStr(dr, c[5]);
                            //Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));

                            Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[6]));
                            Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[7]));
                            TxtIdNumber.EditValue = Sm.DrStr(dr, c[8]);
                            Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                            Sm.SetLue(LueReligion, Sm.DrStr(dr, c[10]));
                            TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                            Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                            MeeAddress.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LueCity, Sm.DrStr(dr, c[14]));
                            TxtPostalCode.EditValue = Sm.DrStr(dr, c[15]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                            Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[18]));
                            TxtEmail.EditValue = Sm.DrStr(dr, c[19]);
                            TxtNPWP.EditValue = Sm.DrStr(dr, c[20]);
                            Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[21]));
                            Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[22]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[23]));
                            TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                            TxtBankAcName.EditValue = Sm.DrStr(dr, c[25]);
                            TxtBankAcNo.EditValue = Sm.DrStr(dr, c[26]);
                            TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[27]);
                            TxtShortCode.EditValue = Sm.DrStr(dr, c[28]);
                            TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[29]);
                            Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[30]));
                            Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[31]));
                            TxtMother.EditValue = Sm.DrStr(dr, c[32]);
                            TxtSubDistrict.EditValue = Sm.DrStr(dr, c[33]);
                            TxtVillage.EditValue = Sm.DrStr(dr, c[34]);
                            TxtRTRW.EditValue = Sm.DrStr(dr, c[35]);
                            Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[36]));
                            Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[37]));
                            Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[38]));
                            Sm.SetLue(LueEntityCode, Sm.DrStr(dr, c[39]));

                            SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[40]), mIsFilterBySiteHR?"Y":"N");
                            
                            MeeDomicile.EditValue = Sm.DrStr(dr, c[41]);
                            Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[42]));
                            Sm.SetLue(LueSection, Sm.DrStr(dr, c[43]));
                            SetLueWorkGroup(ref LueWorkGroup, Sm.DrStr(dr, c[43]));
                            Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[44]));
                            DivisionCode = Sm.DrStr(dr, c[45]);
                            //Sm.SetLue(LueDivisionCode, Sm.DrStr(dr, c[45]));
                            //if(mIsDeptFilterByDivision)
                            //    SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR?"Y":"N", Sm.GetLue(LueDivisionCode));
                            //else
                            //    SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR?"Y":"N");

                            Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[46]));
                            TxtPOH.EditValue = Sm.DrStr(dr, c[47]);
                            Sm.SetDte(DteTGDt, Sm.DrStr(dr, c[48]));
                            Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[49]));
                            TxtLevelCode.EditValue = Sm.DrStr(dr, c[50]);
                            TxtRegEntCode.EditValue = Sm.DrStr(dr, c[51]);
                            Sm.SetLue(LuePositionStatusCode, Sm.DrStr(dr, c[52]));
                            Sm.SetDte(DteContractDt, Sm.DrStr(dr, c[53]));
                            Sm.SetLue(LueClothesSize, Sm.DrStr(dr, c[54]));
                            Sm.SetLue(LueShoeSize, Sm.DrStr(dr, c[55]));
                            if (mIsSiteDivisionEnabled)
                                SetLueDivisionCodeBasedOnSite(ref LueDivisionCode, Sm.GetLue(LueSiteCode), DivisionCode);
                            else
                                Sm.SetLue(LueDivisionCode, DivisionCode);
                            if (mIsDivisionDepartmentEnabled)
                                SetLueDeptCodeBasedOnDivision(ref LueDeptCode, DivisionCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                            else
                            {
                                if (mIsDeptFilterByDivision)
                                    SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N", DivisionCode);
                                else
                                    SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                            }
                            if (mIsDepartmentPositionEnabled)
                                SetLuePosCodeBasedOnDepartment(ref LuePosCode, DeptCode, PosCode);
                            else
                                Sm.SetLue(LuePosCode, PosCode);
                            Sm.SetLue(LueEducationType, Sm.DrStr(dr, c[56]));
                            Sm.SetLue(LueCostGroup, Sm.DrStr(dr, c[57]));
                            if (mIsPltCheckboxNotBasedOnPosition)
                            {
                                ChkActingOfficial.Checked = Sm.CompareStr(Sm.DrStr(dr, c[59]), "Y");
                            }
                            else
                            {
                                ChkActingOfficial.Checked = Sm.CompareStr(Sm.GetValue("Select ActingOfficialInd From TblPosition " +
                                                       "Where PosCode = @Param", Sm.DrStr(dr, c[5])), "Y");
                            }
                            Sm.SetLue(LueLevelCode, Sm.DrStr(dr, c[58]));
                            Sm.SetLue(LueBloodRhesus, Sm.DrStr(dr, c[60]));
                            Sm.SetLue(LueFirstGrdLvlCode, Sm.DrStr(dr, c[61]));
                            if (mIsMaritalStatusMandatory) TxtSpouseEmpCode.EditValue = Sm.DrStr(dr, c[62]);
                        }, true
                    );
            }

        private void ShowEmployee2(string DocNo)
        {
            string
                DivisionCode = string.Empty,
                DeptCode = string.Empty,
                PosCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            

            SQL.AppendLine("Select Distinct A.*, C.LevelName ");
            SQL.AppendLine("From TblEmployeeRecruitment A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr C On B.LevelCode=C.LevelCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");
          
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DocNo",

                        //1-5
                        "EmpName", "DisplayName", "UserCode", "DeptCode", "PosCode",

                        //6-10
                        "JoinDt","ResignDt","IdNumber","Gender","Religion",

                        //11-15
                        "BirthPlace","BirthDt","Address","CityCode","PostalCode",

                        //16-20
                        "Phone","Mobile","GrdLvlCode","Email","NPWP",

                        //21-25
                        "PayrollType","PTKP","BankCode","BankBranch","BankAcName",

                        //26-30
                        "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode", "PayrunPeriod", 

                        //31-35
                        "EmploymentStatus","Mother", "SubDistrict", "Village", "RTRW", 

                        //36-40
                        "MaritalStatus","WeddingDt", "SystemType", "SiteCode", "Domicile",
                        
                        //41-45
                         "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", "PGCode",

                         //46
                          "POH"
                         

                    },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtEmpCode.EditValue = mCopyDataActive ? string.Empty : Sm.DrStr(dr, c[0]);
                            TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtUserCode.EditValue = Sm.DrStr(dr, c[3]);

                            DeptCode = Sm.DrStr(dr, c[4]);
                            PosCode = Sm.DrStr(dr, c[5]);
                            //Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));

                            Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[6]));
                            Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[7]));
                            TxtIdNumber.EditValue = Sm.DrStr(dr, c[8]);
                            Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                            Sm.SetLue(LueReligion, Sm.DrStr(dr, c[10]));
                            TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                            Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                            MeeAddress.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LueCity, Sm.DrStr(dr, c[14]));
                            TxtPostalCode.EditValue = Sm.DrStr(dr, c[15]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                            Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[18]));
                            TxtEmail.EditValue = Sm.DrStr(dr, c[19]);
                            TxtNPWP.EditValue = Sm.DrStr(dr, c[20]);
                            Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[21]));
                            Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[22]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[23]));
                            TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                            TxtBankAcName.EditValue = Sm.DrStr(dr, c[25]);
                            TxtBankAcNo.EditValue = Sm.DrStr(dr, c[26]);
                            TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[27]);
                            TxtShortCode.EditValue = Sm.DrStr(dr, c[28]);
                            TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[29]);
                            Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[30]));
                            Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[31]));
                            TxtMother.EditValue = Sm.DrStr(dr, c[32]);
                            TxtSubDistrict.EditValue = Sm.DrStr(dr, c[33]);
                            TxtVillage.EditValue = Sm.DrStr(dr, c[34]);
                            TxtRTRW.EditValue = Sm.DrStr(dr, c[35]);
                            Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[36]));
                            Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[37]));
                            Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[38]));
                            SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[39]), mIsFilterBySiteHR?"Y":"N");
                            MeeDomicile.EditValue = Sm.DrStr(dr, c[40]);
                            Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[41]));
                            Sm.SetLue(LueSection, Sm.DrStr(dr, c[42]));
                            SetLueWorkGroup(ref LueWorkGroup, Sm.DrStr(dr, c[42]));
                            Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[43]));
                            DivisionCode = Sm.DrStr(dr, c[44]);
                            Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[45]));
                            TxtPOH.EditValue = Sm.DrStr(dr, c[46]);
                        }, true
                    );
            }

        private void ShowEmployeeFamily(string EmpCode)
         {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, ");
            SQL.AppendLine("A.Status, B.OptDesc as StatusDesc, A.Gender, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.BirthDt, A.IDNo, A.NIN, A.Remark, A.ProfessionCode, D.OptDesc As ProfessionDesc, A.EducationLevelCode, E.OptDesc As EducationLevelDesc  ");
            if (mUsePersonalInformation == 1)
                SQL.AppendLine("From TblEmployeeFamily A ");
            else
                SQL.AppendLine("From TblEmployeeFamilyRecruitment A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblOption D On A.ProfessionCode=D.OptCode and D.OptCat='Profession' ");
            SQL.AppendLine("Left Join TblOption E On A.EducationLevelCode=E.OptCode and E.OptCat='EmployeeEducationLevel' ");
            if(mUsePersonalInformation == 1)
                SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            else
                SQL.AppendLine("Where A.DocNo=@EmpCode ");
            SQL.AppendLine("Order By A.FamilyName;");

             Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
             Sm.ShowDataInGrid(
                     ref Grd1, ref cm, SQL.ToString(),
                     new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "Status", "StatusDesc", "Gender", "GenderDesc", 
                        
                        //6-10
                        "BirthDt", "IDNo", "NIN", "Remark", "ProfessionCode", 
                        
                        //11-13
                        "ProfessionDesc", "EducationLevelCode", "EducationLevelDesc"
                    },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                         Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                     }, false, false, true, false
             );
             Sm.FocusGrd(Grd1, 0, 1);
         }

        private void ShowEmployeeAward(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.AwardCt, B.OptDesc As AwardCtDesc, A.Description, A.Yr, A.FileName ");
            SQL.AppendLine("From TblEmployeeAward A ");
            SQL.AppendLine("Left Join TblOption B On A.AwardCt=B.OptCode and B.OptCat='EmployeeAwardCategory' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd14, ref cm, SQL.ToString(),
                    new string[]
                   { 
                        //0
                        "DNo",

                        //1-5
                        "AwardCt", "AwardCtDesc", "Description", "Yr", "FileName"
                   },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd14, 0, 0);
        }

        private void ShowEmployeeTraining3(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.TrainingCode, B.TrainingName, A.TrainingType, C.OptDesc As TrainingTypeDesc, ");
            SQL.AppendLine("A.Specialization, D.OptDesc As SpecializationDesc, ");
            SQL.AppendLine("A.Category, A.Place, A.Yr, A.Duration, A.Period, A.Value, A.Ranking, A.Remark,  ");
            SQL.AppendLine("A.EducationCenter, E.OptDesc As EducationCenterDesc, F.OptDesc As CategoryDesc, A.FileName ");
            SQL.AppendLine("From TblEmployeeTraining2 A ");
            SQL.AppendLine("Left Join TblTraining B On A.TrainingCode=B.TrainingCode ");
            SQL.AppendLine("Left Join TblOption C On A.TrainingType=C.OptCode And C.OptCat='EmployeeTrainingType' ");
            SQL.AppendLine("Left Join TblOption D On A.Specialization=D.OptCode And D.OptCat='EmployeeTrainingSpecialization' ");
            SQL.AppendLine("Left Join TblOption E On A.EducationCenter=E.OptCode And E.OptCat='EmployeeTrainingEducationCenter' ");
            SQL.AppendLine("Left Join TblOption F On A.Category=F.OptCode And F.OptCat='EmployeeTrainingCategory' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd16, ref cm, SQL.ToString(),
                    new string[]
                   { 
                        //0
                        "DNo",

                        //1-5
                        "TrainingCode", "TrainingName", "TrainingType", "TrainingTypeDesc", "Specialization", 

                        //6-10
                        "SpecializationDesc", "Category", "Place", "Yr", "Duration", 

                        //11-15
                        "Period", "Value", "Ranking", "Remark", "EducationCenter", 
                       
                        //16-18
                       "EducationCenterDesc", "CategoryDesc", "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd16, 0, 0);
        }

        private void ShowEmployeeVaccine(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DNo, VaccineDt, Vaccine, Remark, FileName ");
            SQL.AppendLine("From TblEmployeeVaccine ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("Order By VaccineDt;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd12, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-3
                        "VaccineDt", "Vaccine", "Remark", "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd12, 0, 1);
        }

        private void ShowEmployeeWorkExp(string EmpCode)
         {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

             
            SQL.AppendLine("Select DNo, Company,Position,Period,Remark, FileName ");
            if (mUsePersonalInformation == 1)
            {
                SQL.AppendLine("From TblEmployeeWorkExp ");
                SQL.AppendLine("Where EmpCode=@EmpCode Order By DNo ");
            }
            else
            {
                SQL.AppendLine("From TblEmployeeWorkExpRecruitment ");
                SQL.AppendLine("Where DocNo=@EmpCode Order By DNo ");
            }
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                     ref Grd2, ref cm, SQL.ToString(),
                     new string[] 
                    { 
                        "Dno",
                        "Company", "Position", "Period","Remark", "FileName"
                    },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 5);
                     }, false, false, true, false
             );
             Sm.FocusGrd(Grd2, 0, 0);
         }

        private void ShowEmployeeEducation(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.School, A.Level as LevelCode, B.OptDesc As Level, ");
            if (!mIsEmployeeEducationInputManualData)
                SQL.AppendLine("A.Major As MajorCode, D.MajorName As Major, A.Field As FieldCode, C.OptDesc As Field, E.FacCode, E.FacName, ");
            else
                SQL.AppendLine("A.Major As MajorCode, A.Major, A.Field As FieldCode, A.Field, A.FacCode, A.FacCode As FacName, ");
            SQL.AppendLine("A.GraduationYr, A.HighestInd, A.Remark, A.FileName ");
            if (mUsePersonalInformation == 1)
                SQL.AppendLine("From TblEmployeeEducation A ");
            else
                SQL.AppendLine("From TblEmployeeEducationRecruitment A");
            SQL.AppendLine("Left Join TblOption B On A.Level = B.OptCode and B.OptCat = 'EmployeeEducationLevel' ");
            if (!mIsEmployeeEducationInputManualData)
            {
                SQL.AppendLine("Left Join TblOption C On A.Field = C.OptCode and C.OptCat = 'EmployeeEducationField' ");
                SQL.AppendLine("Left Join TblMajor D On A.Major = D.MajorCode ");
                SQL.AppendLine("Left Join TblFaculty E On A.FacCode = E.FacCode ");
            }
            if(mUsePersonalInformation == 1)
                SQL.AppendLine("Where A.EmpCode = @EmpCode ");
            else
                SQL.AppendLine("Where A.DocNo = @EmpCode ");
            SQL.AppendLine("Order By A.HighestInd Desc, A.Level; ");
               
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                 ref Grd3, ref cm, SQL.ToString(),
                 new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "School", "LevelCode", "Level", "MajorCode", "Major",  

                    //6-10
                    "FieldCode", "Field", "GraduationYr" ,"HighestInd", "Remark",
 
                    //11-13
                    "FacCode", "FacName", "FileName"
                },
                 (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                 {
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                     Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 9);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                 }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
         }

        private void ShowEmployeeTraining2(string EmpCode)
        {
            var SQL10 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL10.AppendLine("Select A.DNo, A.TrainingCode, B.TrainingName, A.SpecializationTraining, A.Place, A.Period, A.Yr, A.Remark, ");
            if (mUsePersonalInformation == 1)
            {
                SQL10.AppendLine("A.TrainingEvaluationDocNo ");
                SQL10.AppendLine("From TblEmployeeTraining A ");
            }
            else
            {
                SQL10.AppendLine("Null As TrainingEvaluationDocNo ");
                SQL10.AppendLine("From TblEmployeeTrainingRecruitment A ");
            }
            SQL10.AppendLine("Left Join TblTraining B On A.TrainingCode = B.TrainingCode ");
            if (mUsePersonalInformation == 1)
                SQL10.AppendLine("Where EmpCode=@EmpCode ");
            else
                SQL10.AppendLine("Where DocNo=@EmpCode ");
            SQL10.AppendLine("Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd10, ref cm, SQL10.ToString(),
                    new string[] 
                    { 
                        "DNo",
                        "TrainingCode", "TrainingName", "SpecializationTraining", "Place", "Period", 
                        "Yr", "Remark", "TrainingEvaluationDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);

                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd10, 0, 0);
        }

        private void ShowEmployeeTraining(string EmpCode)
        {
            var SQL8 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL8.AppendLine("Select A.DNo, A.TrainingCode, B.TrainingName, A.SpecializationTraining, A.Place, A.Period, A.Yr, A.Remark,  ");
            if (mUsePersonalInformation == 1)
            {
                SQL8.AppendLine("A.TrainingEvaluationDocNo ");
                SQL8.AppendLine("From TblEmployeeTraining A ");
            }
            else
            {
                SQL8.AppendLine("Null As TrainingEvaluationDocNo ");
                SQL8.AppendLine("From TblEmployeeTrainingRecruitment A ");
            }
            SQL8.AppendLine("Left Join TblTraining B On A.TrainingCode = B.TrainingCode ");
            if (mUsePersonalInformation == 1)
                SQL8.AppendLine("Where EmpCode=@EmpCode ");
            else
                SQL8.AppendLine("Where DocNo=@EmpCode ");
            SQL8.AppendLine("Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd8, ref cm, SQL8.ToString(),
                    new string[] 
                    { 
                        "DNo",
                        "TrainingCode", "TrainingName", "SpecializationTraining", "Place", "Period", 
                        "Yr", "Remark", "TrainingEvaluationDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);

                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        private void ShowEmployeeTrainingEvaluation(string EmpCode)
        {
            var SQL9 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL9.AppendLine("Select A.DocNo, C.DNo, A.TrainingCode, D.TrainingName, DATE_FORMAT(A.DocDt,'%b/%Y')as DocDt, A.Remark  ");
            SQL9.AppendLine("From TblTrainingEvaluationHdr A ");
            SQL9.AppendLine("Inner Join TblTrainingEvaluationDtl B On A.DocNo = B.DocNo And B.Result = '1' ");
            SQL9.AppendLine("Inner Join TblTrainingAssignmentDtl C On B.TrainingSourceDocNo = c.DocNo And B.TrainingSourceDNo = C.DNo ");
            SQL9.AppendLine("Left Join TblTraining d On A.TrainingCode = D.TrainingCode ");
            SQL9.AppendLine("Where C.EmpCode=@EmpCode ");
            SQL9.AppendLine("Order By C.DNo; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd9, ref cm, SQL9.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "DocNo","TrainingCode", "TrainingName", "DocDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);

                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd9, 0, 0);
        }

        private void ShowEmployeeCompetence(string EmpCode)
         {
            var SQLComp = new StringBuilder();
            SQLComp.AppendLine("Select A.DNo, A.CompetenceCode, B.CompetenceName, A.Description, A.Score, A.FileName ");
            if(mUsePersonalInformation == 1)
                SQLComp.AppendLine("From TblEmployeeCompetence A ");
            else
                SQLComp.AppendLine("From TblEmployeeCompetenceRecruitment A ");
            SQLComp.AppendLine("Inner Join TblCompetence B On A.CompetenceCode = B.CompetenceCode ");
            if(mUsePersonalInformation == 1)
                SQLComp.AppendLine("Where A.EmpCode = @EmpCode ");
            else
                SQLComp.AppendLine("Where A.DocNo = @EmpCode ");
            SQLComp.AppendLine("Order By A.DNo;");
            
            var cm = new MySqlCommand();

             Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
             Sm.ShowDataInGrid(
                     ref Grd4, ref cm, SQLComp.ToString(),
                     new string[] 
                    { 
                        "DNo",
                        "CompetenceCode", "CompetenceName", "Description", "Score", "FileName"
                    },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 2);
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                         Sm.SetGrdValue("N", Grd4, dr, c, Row, 4, 4);
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                         Sm.SetGrdValue("S", Grd4, dr, c, Row, 8, 5);
                     }, false, false, true, false
             );
             Sm.FocusGrd(Grd4, 0, 0);
         }

        private void ShowEmployeeSS(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.SSCode, B.SSName, A.CardNo, A.HealthFacility, A.StartDt, A.EndDt, A.Remark ");
            SQL.AppendLine("From TblEmployeeSS A ");
            SQL.AppendLine("Left Join TblSS B On A.SSCode=B.SSCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By B.SSName;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "SSCode", "SSName", "CardNo", "HealthFacility", "StartDt", 
                        
                        //6-7
                        "EndDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowEmployeeFamilySS(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, B.OptDesc as StatusDesc, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.IDNo, A.NIN, A.SSCode, D.SSName, A.CardNo, A.HealthFacility, A.StartDt, A.EndDt, A.Remark ");
            SQL.AppendLine("From TblEmployeeFamilySS A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblSS D On A.SSCode=D.SSCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.FamilyName, D.SSName;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "StatusDesc", "GenderDesc", "IDNo", "NIN", 
                        
                        //6-10
                        "SSCode", "SSName", "CardNo", "HealthFacility", "StartDt", 
                        
                        //11-12
                        "EndDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowEmployeePersonalReference(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            SQL.AppendLine("Select DNo, Name,Phone,Address,Remark ");
            if(mUsePersonalInformation == 1)
            {
                SQL.AppendLine("From  TblEmployeePersonalReference ");
                SQL.AppendLine("Where EmpCode=@EmpCode Order By Dno ");
            }
            else
            {
                SQL.AppendLine("From  TblEmployeePersonalReferenceRecruitment ");
                SQL.AppendLine("Where DocNo=@EmpCode Order By Dno ");
            }
            Sm.ShowDataInGrid(
                    ref Grd7, ref cm,SQL.ToString(),
                    new string[] 
                    { 
                        "Dno",
                        "name", "Phone", "Address","Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
        }

        private void ShowEmployeeFile(string EmpCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd11, ref cm,
                   "select A.DNo, A.FileName, A.EmpFileCt, B.OptDesc, EmpFileCt, OptDesc As EmpFileCtName from  TblEmployeeFile A Left Join tblOption B On A.EmpFileCt = B.OptCode Where A.EmpCode=@EmpCode And B.OptCat = 'EmployeeFileUploadCategory' Order By A.Dno",

                    new string[] 
                    { 
                        "Dno",
                        "FileName",
                        "EmpFileCt",
                        "OptDesc",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd11, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd11, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd11, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd11, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd11, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd11, 0, 0);
        }
        #endregion

        #region Save Data

        private void SetEmpCode()
        {
            //Customized for IOK
            if (TxtEmpCode.Text.Length == 0)
            {
                if (mGenerateEmpCodeFormat == "1")
                {
                    string YYMM;
                    if (mIsEmpContractDtMandatory)
                    {
                        YYMM = Sm.GetDte(DteContractDt).Substring(2, 4);
                    }
                    else YYMM = Sm.GetDte(DteJoinDt).Substring(2, 4);

                    if (!mIsEmpContractDtMandatory)
                    {
                        TxtEmpCode.EditValue = Sm.GetValue(
                             "Select Concat('" + YYMM + "', " +
                             "   IfNull((Select Right(Concat('0000', Convert(EmpCodeTemp+1, Char)), 4) From ( " +
                             "       Select Convert(Right(EmpCode, 5), Decimal) As EmpCodeTemp From TblEmployee Where Left(EmpCode, 4)='" + YYMM + "' Order By Right(EmpCode, 4) Desc Limit 1 " +
                             "       ) As TblEmployeeTemp), '0001')) As ItCode");
                    }
                    else
                    {
                        string EmpStat = Sm.Right(string.Concat("00", Sm.GetLue(LueEmploymentStatus)), 2);
                        string LastEmp = Sm.GetValue("Select ifnull( " +
                                                    "(Select Right(Concat('00000', Convert(EmpCodeTemp+1, Char)), 5) From ( " +
                                                    "    Select Convert(Right(EmpCode, 5), Decimal) As EmpCodeTemp  " +
                                                    "    From TblEmployee  " +
                                                    "    Where Substring(EmpCode, 3, 2) = '" + Sm.Left(YYMM, 2) + "' And EmploymentStatus = '" + Sm.GetLue(LueEmploymentStatus) + "' And Length(EmpCode) = 9  order by Right(EmpCode, 5) Desc Limit 1 " +
                                                    ")As TblEmployeeTemp) " +
                                                    ", '00001') ");
                        TxtEmpCode.EditValue = string.Concat(EmpStat, Sm.Left(YYMM, 2), LastEmp);
                    }
                }
                else
                {
                    if (Sm.IsLueEmpty(LueEmploymentStatus, "Employment Status")) return;

                    string YYMM = string.Empty;
                    string mEmploymentStatusForEmpCode = string.Empty;
                    string sSQL = string.Empty;

                    YYMM = Sm.Left(Sm.GetDte(DteJoinDt), 6);
                    mEmploymentStatusForEmpCode = Sm.GetValue("Select OptDesc From TblOption where OptCat = 'EmploymentStatusForEmpCode' And OptCode = @Param;", Sm.GetLue(LueEmploymentStatus));

                    sSQL += "Select Concat(@Param, ";
                    sSQL += "    IfNull( ";
                    sSQL += "        ( ";
                    sSQL += "        Select Right(Concat('00', Convert(EmpCodeTemp + 1, Char)), 2) ";
                    sSQL += "        From ";
                    sSQL += "        ( ";
                    sSQL += "            Select Convert(Right(EmpCode, 2), Decimal) As EmpCodeTemp ";
                    sSQL += "            From TblEmployee ";
                    sSQL += "            Where Left(EmpCode, 8) = @Param ";
                    sSQL += "            Order By Right(EmpCode, 2) Desc ";
                    sSQL += "            Limit 1 ";
                    sSQL += "        ) T ";
                    sSQL += "        ) ";
                    sSQL += "    , '01')) As EmpCode; ";

                    TxtEmpCode.EditValue = Sm.GetValue(sSQL, string.Concat(YYMM, mEmploymentStatusForEmpCode));
                }
            }
        }

        private void GenerateEmpCodeOld()
        {
                string BirthDt = string.Empty,
                   Date = string.Empty,
                   Gender = string.Empty,
                   Text = string.Empty;

                if (Sm.GetDte(DteBirthDt).Length != 0)
                    BirthDt = Sm.Left(Sm.GetDte(DteBirthDt), 8);
                else
                    BirthDt = "00000000";
                Date = Sm.Left(Sm.GetDte(DteJoinDt), 6);
                Text = "PHT";
                if (Sm.GetLue(LueGender).Length != 0)
                {
                    if (Sm.GetLue(LueGender) == "M")
                        Gender = "1";
                    else
                        Gender = "2";
                }
                else
                    Gender = "0";
                TxtEmpCodeOld.EditValue = Sm.GetValue(
                          "Select Concat('" + Text + "','" + BirthDt + "','" + Date + "','" + Gender + "', " +
                          "   IfNull((Select Right(Concat('0', Convert(EmpCodeTemp+1, CHAR)), 2) From ( " +
                          "   Select Convert(Right(EmpCodeOld, 2), Decimal) As EmpCodeTemp " +
                          "   From TblEmployee Where Left(EmpCodeOld, 18)=CONCAT('" + Text + "','" + BirthDt + "','" + Date + "','" + Gender + "')" +
                          "   Order By Right(EmpCodeOld, 2) Desc Limit 1" +
                          "   ) As TblEmployeeTemp), '01')) As EmpCodeOld");
            }
        
        private bool IsDataNotValid()
        {
            return
                (!mIsEmpCodeAutoGenerate && Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false)) ||
                Sm.IsTxtEmpty(TxtEmpName, "Employee name", false) ||
                Sm.IsDteEmpty(DteJoinDt, "Join date") ||
                (mIsEmpContractDtMandatory && Sm.IsDteEmpty(DteContractDt, "Contract date")) ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (
                    mIsPayrollActived &&
                    (
                        Sm.IsLueEmpty(LueDeptCode, "Department") ||
                        Sm.IsLueEmpty(LuePosCode, "Position") ||
                        Sm.IsLueEmpty(LueGrdLvlCode, "Grade level") ||
                        Sm.IsLueEmpty(LuePGCode, "Payroll group") ||
                        Sm.IsLueEmpty(LueEmploymentStatus, "Employment status") ||
                        Sm.IsLueEmpty(LueSystemType, "System type") ||
                        Sm.IsLueEmpty(LuePayrunPeriod, "Payrun period")
                    )
                ) ||
                IsPermanentDtNotValid() ||
                IsItCodeExisted() ||
                IsDeptCodeNotValid() ||
                IsGrd1ValueNotValid() ||
                IsGrd2ValueNotValid() ||
                IsGrd3ValueNotValid() ||
                IsGrd4ValueNotValid() ||
                IsGrd8ValueNotValid() ||
                IsGrd10ValueNotValid() ||
                IsGrd12ValueNotValid() ||
                IsGrd14ValueNotValid() ||
                IsGrd16ValueNotValid() ||
                IsCompetenceCodeExisted() ||
                (mIsEmployeeUseFirstGrade && Sm.IsLueEmpty(LueFirstGrdLvlCode, "First Grade"))||
                (mIsMaritalStatusMandatory && Sm.IsLueEmpty(LueMaritalStatus, "Marital Status"))||
                IsPhoneNumberNotValid()
                ;
        }

        private bool IsPhoneNumberNotValid()
        {
            const string character = // @"^(\+62|62)8[1-9][0-9]{6,9}$";
                @"^(\+62|62)?[\s-]?0?8[1-9]{1}\d{1}[\s-]?\d{4}[\s-]?\d{2,5}$";
            string phone = TxtPhone.Text;
            if (Regex.IsMatch(phone, character)==false)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Invalid phone number, "+Environment.NewLine+"can only use numbers and phone code country like +62 ");
                return true;
            }
            return false;
        }

         private bool IsDeptCodeNotValid()
        {
            if (!mIsDeptFilterByDivision) return false;
            if (
                Sm.IsLueEmpty(LueDivisionCode, "Division") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") 
                ) return true;

            if (!Sm.IsDataExist(
                "Select 1 From TblDepartment " +
                "Where DeptCode=@Param1 " +
                "And DivisionCode Is Not Null " +
                "And DivisionCode=@Param2; ",
                Sm.GetLue(LueDeptCode), Sm.GetLue(LueDivisionCode), string.Empty
                ))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Division : " + LueDivisionCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "Invalid department.");
                return true;
            }
            return false;

        }

        private bool IsPermanentDtNotValid()
        {
            string mInitLeaveStartDt = Sm.GetValue("Select ifnull(LeaveStartDt,'') from tblemployee where empcode = @Param ", TxtEmpCode.Text);
            string mLeaveStartDt = string.Empty;

            if (DteLeaveStartDt.Text.Length > 0) mLeaveStartDt = Sm.Left(Sm.GetDte(DteLeaveStartDt), 8);

            if (mInitLeaveStartDt != mLeaveStartDt)
            {
                if (Sm.IsDataExist(
                "Select LeaveCode From TblLeaveSummary Where EmpCode=@Param1 " +
                "And LeaveCode In (@Param2, @Param3) limit 1; ",
                TxtEmpCode.Text, mLongServiceLeaveCode, mLongServiceLeaveCode2
                ))
                {
                    Sm.StdMsg(mMsgType.Warning, "You can't edit Permanent Date. ");
                    DteLeaveStartDt.Focus();
                    return true;
                }
            }
            
            return false;

        }

        private bool IsItCodeExisted()
        {
            if (!TxtEmpCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select 1 From TblEmployee Where EmpCode='" + TxtEmpCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Employee code ( " + TxtEmpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Gender is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Family Name is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Company is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd3ValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "School is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Level is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd4ValueNotValid()
        {
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 2, false, "Competence is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 3, false, "Competence Description is empty.")) return true;
                   
                }
            }
            return false;
        }

        private bool IsGrd8ValueNotValid()
        {

            if (mIsEmpSpecializationNotMandatory) return false;

            if (Grd8.Rows.Count > 1)
            {
                for (int r= 0; r < Grd8.Rows.Count-1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd8, r, 3, false, "Specialization training is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd10ValueNotValid()
        {
            if (Grd10.Rows.Count > 1)
            {
                for (int r = 0; r < Grd10.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd10, r, 3, false, "Specialization training is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd12ValueNotValid()
        {
            if (Grd12.Rows.Count > 1)
            {
                for (int r = 0; r < Grd12.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd12, r, 1, false, "Vaccine date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd12, r, 2, false, "Vaccine is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd14ValueNotValid()
        {
            if (Grd14.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd14.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd14, Row, 2, false, "Category is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd14, Row, 3, false, "Description is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd16ValueNotValid()
        {
            if (Grd16.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd16.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd16, Row, 2, false, "Training is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsCompetenceCodeExisted()
        {
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    for (int Row2 = 1; Row2 < Grd4.Rows.Count - 1; Row2++)
                    {
                        if ((Sm.GetGrdStr(Grd4, Row, 1) == Sm.GetGrdStr(Grd4, Row2, 1)) && Row != Row2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "This competence ( " + Sm.GetGrdStr(Grd4, Row, 2) + " ) is already exist.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveEmployee(bool IsAutoCreatePPS)
        {
            var SQL = new StringBuilder();
            string LevelCode2 = Sm.GetValue("Select LevelCode From TblGradeLevelHdr Where GrdLvlCode = @Param", Sm.GetLue(LueGrdLvlCode));

            SQL.AppendLine("Insert Into TblEmployee(EmpCode, EmpName, DisplayName, UserCode, EmpCodeOld, ShortCode, DeptCode, PosCode, PositionStatusCode, ");
            SQL.AppendLine("JoinDt, ResignDt, TGDt, IdNumber, MaritalStatus, WeddingDt, Gender, Religion, Mother, BirthPlace, BirthDt, Address, ");
            SQL.AppendLine("CityCode, SubDistrict, Village, RTRW, PostalCode, EntCode, SiteCode, SectionCode, WorkGroupCode, Domicile, BloodType, RhesusType, Phone, Mobile, Email,GrdLvlCode,NPWP, SystemType, PayrollType, ");
            SQL.AppendLine("PTKP,BankCode,BankAcName,BankAcNo,BankBranch,ContractDt, ");
            SQL.AppendLine("PayrunPeriod, EmploymentStatus, DivisionCode, PGCode, POH, LeaveStartDt, ");
            SQL.AppendLine("ClothesSize, ShoeSize, EducationType, CostGroup, LevelCode,  ");
            if (mIsEmployeeUseFirstGrade)
                SQL.AppendLine("FirstGrdLvlCode, ");

            if (mIsMaritalStatusMandatory) SQL.AppendLine("SpouseEmpCode, ");

            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @EmpName, @DisplayName, @UsCode, @EmpCodeOld, @ShortCode, @DeptCode, @PosCode, @PositionStatusCode, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @TGDt, @IdNumber, @MaritalStatus, @WeddingDt, @Gender, @Religion, @Mother, @BirthPlace, @BirthDt, @Address, ");
            SQL.AppendLine("@CityCode, @SubDistrict, @Village, @RTRW, @PostalCode, @EntCode, @SiteCode, @SectionCode, @WorkGroupCode, @Domicile, @BloodType, @RhesusType, @Phone, @Mobile, @Email, @GrdLvlCode, @NPWP, @SystemType, @PayrollType,@PTKP,@BankCode,@BankAcName,@BankAcNo,@BankBranch,@ContractDt, ");
            SQL.AppendLine("@PayrunPeriod, @EmploymentStatus, @DivisionCode, @PGCode, @POH, ");
            if (mIsAnnualLeaveUseStartDt)
                SQL.AppendLine("@LeaveStartDt, ");
            else
                SQL.AppendLine("Null, ");
            SQL.AppendLine("@ClothesSize, @ShoeSize, @EducationType, @CostGroup, @LevelCode, ");
            if (mIsEmployeeUseFirstGrade)
                SQL.AppendLine("@FirstGrdLvlCode, ");

            if (mIsMaritalStatusMandatory) SQL.AppendLine("@SpouseEmpCode, ");

            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            if (mIsEmployeeCOAInfoMandatory)
            {
                SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                SQL.AppendLine("Select Concat(@EmployeeAcNo, EmpCode) As AcNo, ");
                SQL.AppendLine("Trim(Concat(@EmployeeAcDesc, ' ', EmpCode, ' - ', EmpName)) As AcDesc, ");
                SQL.AppendLine("If(Length(@EmployeeAcNo)<=0, '', Left(@EmployeeAcNo, Length(@EmployeeAcNo)-1)) As Parent, ");
                SQL.AppendLine("If(Length(@EmployeeAcNo)<=0, '', Length(@EmployeeAcNo)-Length(Replace(@EmployeeAcNo, '.', ''))+1) As Level, ");
                SQL.AppendLine("'C', @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblEmployee ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And Concat(@EmployeeAcNo, EmpCode) Not In (Select AcNo from TblCOA); ");
            }

            if (mIsInsert)
            {
                if (mAcNoForAdvancePayment.Length > 0 && mAcDescForAdvancePayment.Length>0)
                {
                    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select Concat(@AcNoForAdvancePayment, @EmpCode) As AcNo, ");
                    SQL.AppendLine("Trim(Concat(@AcDescForAdvancePayment, ' ', @EmpCode)) As AcDesc, ");
                    SQL.AppendLine("If(Length(@AcNoForAdvancePayment)<=0, '', Left(@AcNoForAdvancePayment, Length(@AcNoForAdvancePayment)-1)) As Parent, ");
                    SQL.AppendLine("If(Length(@AcNoForAdvancePayment)<=0, '', Length(@AcNoForAdvancePayment)-Length(Replace(@AcNoForAdvancePayment, '.', ''))+2) As Level, ");
                    SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                }
            }

            if (mIsPPSActive && mIsInsert && IsAutoCreatePPS)
            {
                SQL.AppendLine("Insert Into TblPPS ");
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, Jobtransfer, ");
                SQL.AppendLine("StartDt, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, DeptCodeOld, ");
                SQL.AppendLine("PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, ");
                SQL.AppendLine("PGCodeOld, SiteCodeOld, LevelCodeOld, ResignDtOld, DeptCodeNew, PosCodeNew, PositionStatusCodeNew, ");
                SQL.AppendLine("GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, ");
                SQL.AppendLine("SiteCodeNew, LevelCodeNew, ResignDtNew, UpdLeaveStartDtInd, Remark, CreateBy, ");
                SQL.AppendLine("CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, @DocDt, 'N', 'A', 'N', ");
                SQL.AppendLine("@JoinDt, @EmpCode, Null, Null, Null, ");
                SQL.AppendLine("Null, Null, Null, Null, Null, ");
                SQL.AppendLine("Null, Null, Null, Null, Null, @DeptCode, @PosCode, @PositionStatusCode, ");
                SQL.AppendLine("@GrdLvlCode, @EmploymentStatus, @SystemType, @PayrunPeriod, @PGCode, ");
                SQL.AppendLine("@SiteCode, @LevelCode, @ResignDt, 'N', Null, @UserCode, ");
                SQL.AppendLine("CurrentDateTime()); ");

                SQL.AppendLine("Insert Into TblEmployeePPS ");
                SQL.AppendLine("(EmpCode, StartDt, EndDt, DeptCode, PosCode, ");
                SQL.AppendLine("GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, LevelCode, ");
                SQL.AppendLine("ProcessInd, SectionCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select EmpCode, JoinDt, Null, DeptCode, PosCode, ");
                SQL.AppendLine("GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, @PGCode, @SiteCode, @LevelCode2, ");
                SQL.AppendLine("'Y', @SectionCode, CreateBy, CreateDt ");
                SQL.AppendLine("From TblEmployee Where EmpCode=@EmpCode;");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpName", TxtEmpName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@UsCode", TxtUserCode.Text);
            if (mIsInsert && mIsEmpCodeCopyToOldCode && TxtEmpCodeOld.Text.Length == 0)
                Sm.CmParam<String>(ref cm, "@EmpCodeOld", TxtEmpCode.Text);
            else
                Sm.CmParam<String>(ref cm, "@EmpCodeOld", TxtEmpCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntityCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSection));
            Sm.CmParam<String>(ref cm, "@WorkGroupCode", Sm.GetLue(LueWorkGroup)); 
            Sm.CmParam<String>(ref cm, "@Domicile", MeeDomicile.Text);
            Sm.CmParam<String>(ref cm, "@BloodType", Sm.GetLue(LueBloodType));
            Sm.CmParam<String>(ref cm, "@RhesusType", Sm.GetLue(LueBloodRhesus));
            Sm.CmParamDt(ref cm, "@JoinDt", Sm.GetDte(DteJoinDt));
            Sm.CmParamDt(ref cm, "@ResignDt", Sm.GetDte(DteResignDt));
            Sm.CmParamDt(ref cm, "@ContractDt", Sm.GetDte(DteContractDt));
            Sm.CmParamDt(ref cm, "@TGDt", Sm.GetDte(DteTGDt));
            Sm.CmParam<String>(ref cm, "@IdNumber", TxtIdNumber.Text);
            Sm.CmParam<String>(ref cm, "@MaritalStatus", Sm.GetLue(LueMaritalStatus));
            Sm.CmParamDt(ref cm, "@WeddingDt", Sm.GetDte(DteWeddingDt));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
            Sm.CmParam<String>(ref cm, "@Religion", Sm.GetLue(LueReligion));
            Sm.CmParam<String>(ref cm, "@Mother", TxtMother.Text);
            Sm.CmParam<String>(ref cm, "@BirthPlace", TxtBirthPlace.Text);
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteBirthDt));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@SubDistrict", TxtSubDistrict.Text);
            Sm.CmParam<String>(ref cm, "@Village", TxtVillage.Text);
            Sm.CmParam<String>(ref cm, "@RTRW", TxtRTRW.Text);
            Sm.CmParam<String>(ref cm, "@PostalCode", TxtPostalCode.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@LevelCode2", LevelCode2);
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrollType", Sm.GetLue(LuePayrollType));
            Sm.CmParam<String>(ref cm, "@PTKP", Sm.GetLue(LuePTKP));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@BankAcName", TxtBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@ClothesSize", Sm.GetLue(LueClothesSize));
            Sm.CmParam<String>(ref cm, "@ShoeSize", Sm.GetLue(LueShoeSize));
            Sm.CmParam<String>(ref cm, "@EducationType", Sm.GetLue(LueEducationType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            if (mIsEmployeeCOAInfoMandatory)
            {
                Sm.CmParam<String>(ref cm, "@EmployeeAcNo", mEmployeeAcNo);
                Sm.CmParam<String>(ref cm, "@EmployeeAcDesc", mEmployeeAcDesc);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivisionCode));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@POH", TxtPOH.Text);
            Sm.CmParam<String>(ref cm, "@CostGroup", Sm.GetLue(LueCostGroup));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevelCode));

            if (mIsMaritalStatusMandatory) Sm.CmParam<String>(ref cm, "@SpouseEmpCode", TxtSpouseEmpCode.Text);

            if (mIsEmployeeUseFirstGrade) Sm.CmParam<String>(ref cm, "@FirstGrdLvlCode", Sm.GetLue(LueFirstGrdLvlCode));
            if (mIsAnnualLeaveUseStartDt) Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteLeaveStartDt));
            if (mIsInsert)
            {
                if (mAcNoForAdvancePayment.Length > 0) Sm.CmParam<String>(ref cm, "@AcNoForAdvancePayment", mAcNoForAdvancePayment);
                if (mAcDescForAdvancePayment.Length > 0) Sm.CmParam<String>(ref cm, "@AcDescForAdvancePayment", mAcDescForAdvancePayment);
            }

            if (mIsPPSActive && mIsInsert && IsAutoCreatePPS)
            {
                var DocDt = Sm.Left(Sm.ServerCurrentDateTime(), 8);
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GenerateDocNo(DocDt, "PPS", "TblPPS"));
                Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            }

            return cm;
        }

        private MySqlCommand SaveFamilyEmployee()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var IsApprovalNeed = IsEmployeeUpdateNeedApproval();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Family */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        if (mIsInsert)
                            SQL.AppendLine("Insert Into TblEmployeeFamily(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
                        else
                        {
                            if (IsApprovalNeed)
                                SQL.AppendLine("Insert Into TblEmployeeFamilyUpdate(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
                            else
                                SQL.AppendLine("Insert Into TblEmployeeFamily(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
                        }                        
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@EmpCode, @DNo_" + r.ToString() +
                        ", @FamilyName_" + r.ToString() +
                        ", @Status_" + r.ToString() +
                        ", @Gender_" + r.ToString() +
                        ", @BirthDt_" + r.ToString() +
                        ", @IDNo_" + r.ToString() +
                        ", @NIN_" + r.ToString() +
                        ", @ProfessionCode_" + r.ToString() +
                        ", @EducationLevelCode_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FamilyName_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@Status_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@Gender_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParamDt(ref cm, "@BirthDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@IDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@NIN_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@ProfessionCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<String>(ref cm, "@EducationLevelCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        //private MySqlCommand SaveFamilyEmployee(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    if (mIsInsert)
        //    {
        //        SQL.AppendLine("Insert Into TblEmployeeFamily(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @BirthDt, @IDNo, @NIN, @ProfessionCode, @EducationLevelCode, @Remark, @UserCode, CurrentDateTime()) ");
        //    }
        //    else
        //    {
        //        if (IsEmployeeUpdateNeedApproval())
        //        {
        //            SQL.AppendLine("Insert Into TblEmployeeFamilyUpdate(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @BirthDt, @IDNo, @NIN, @ProfessionCode, @EducationLevelCode, @Remark, @UserCode, CurrentDateTime()) ");
        //        }
        //        else
        //        {
        //            SQL.AppendLine("Insert Into TblEmployeeFamily(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ProfessionCode, EducationLevelCode, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @BirthDt, @IDNo, @NIN, @ProfessionCode, @EducationLevelCode, @Remark, @UserCode, CurrentDateTime()) ");
        //        }
        //    }

        //    var cm = new MySqlCommand()
        //    { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@FamilyName", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Gender", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetGrdDate(Grd1, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@IDNo", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@NIN", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@ProfessionCode", Sm.GetGrdStr(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@EducationLevelCode", Sm.GetGrdStr(Grd1, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeWorkExp()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Work Experience */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeWorkExp ");
                        SQL.AppendLine("(EmpCode, DNo, Company, Position, Period, Remark, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@EmpCode, @DNo_" + r.ToString() +
                        ", @Company_" + r.ToString() +
                        ", @Position_" + r.ToString() +
                        ", @Period_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Company_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    Sm.CmParam<String>(ref cm, "@Position_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@Period_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 4));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    Company = Values(Company), ");
                SQL.AppendLine("    Position = Values(Position), ");
                SQL.AppendLine("    Period = Values(Period), ");
                SQL.AppendLine("    Remark = Values(Remark), ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeWorkExp(int Row)
        //{
        //    var SQL = new StringBuilder();
        //    SQL.AppendLine("Insert Into TblEmployeeWorkExp(EmpCode, DNo, Company, Position, Period, " );
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) " );
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @Company, @Position, @Period, " );
        //    SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) " );
        //    SQL.AppendLine("On Duplicate Key " );
        //    SQL.AppendLine("Update " );
        //    SQL.AppendLine("Company=@Company, Position=@Position, Period=@Period, ");
        //    SQL.AppendLine("Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = SQL.ToString()
        //    };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Company", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd2, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeEducation()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Education */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeEducation(EmpCode, DNo, School, Level, Major, Field, FacCode, GraduationYr, HighestInd, Remark, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@EmpCode, @DNo_" + r.ToString() +
                        ", @School_" + r.ToString() +
                        ", @Level_" + r.ToString() +
                        ", @Major_" + r.ToString() +
                        ", @Field_" + r.ToString() +
                        ", @FacCode_" + r.ToString() +
                        ", @GraduationYr_" + r.ToString() +
                        ", @HighestInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    if (mIsEmployeeEducationInputManualData)
                    {
                        Grd3.Cells[r, 4].Value = Sm.GetGrdStr(Grd3, r, 5);
                        Grd3.Cells[r, 6].Value = Sm.GetGrdStr(Grd3, r, 7);
                        Grd3.Cells[r, 11].Value = Sm.GetGrdStr(Grd3, r, 12);
                    }

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@School_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<String>(ref cm, "@Level_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 2));
                    Sm.CmParam<String>(ref cm, "@Major_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@Field_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 6));
                    Sm.CmParam<String>(ref cm, "@GraduationYr_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 8));
                    Sm.CmParam<String>(ref cm, "@HighestInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 9) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 10));
                    Sm.CmParam<String>(ref cm, "@FacCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 11));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 13));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    School = Values(School), ");
                SQL.AppendLine("    Level = Values(Level), ");
                SQL.AppendLine("    Major = Values(Major), ");
                SQL.AppendLine("    Field = Values(Field), ");
                SQL.AppendLine("    FacCode = Values(FacCode), ");
                SQL.AppendLine("    GraduationYr = Values(GraduationYr), ");
                SQL.AppendLine("    HighestInd = Values(HighestInd), ");
                SQL.AppendLine("    Remark = Values(Remark), ");
                SQL.AppendLine("    FileName = Values(FileName); ");

            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeEducation(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeEducation(EmpCode, DNo, School, Level, Major, Field, FacCode, GraduationYr, HighestInd, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @School, @Level, @Major, @Field, @FacCode, @GraduationYr, @HighestInd, @Remark, @UserCode, CurrentDateTime()) ");
        //    SQL.AppendLine("On Duplicate Key ");
        //    SQL.AppendLine("Update ");
        //    SQL.AppendLine("    School=@School, Level=@Level, Major=@Major, Field=@Field, FacCode=@FacCode, GraduationYr=@GraduationYr, HighestInd=@HighestInd, ");
        //    SQL.AppendLine("    Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@School", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@Level", Sm.GetGrdStr(Grd3, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Major", Sm.GetGrdStr(Grd3, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Field", Sm.GetGrdStr(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@GraduationYr", Sm.GetGrdStr(Grd3, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@HighestInd", Sm.GetGrdBool(Grd3, Row, 9)?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@FacCode", Sm.GetGrdStr(Grd3, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeTraining()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Training */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd8.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd8, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeTraining ");
                        SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, TrainingEvaluationDocNo, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@EmpCode, @DNo_" + r.ToString() +
                        ", @TrainingCode_" + r.ToString() +
                        ", @SpecializationTraining_" + r.ToString() +
                        ", @Place_" + r.ToString() +
                        ", @Period_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @TrainingEvaluationDocNo_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@TrainingCode_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 1));
                    Sm.CmParam<String>(ref cm, "@SpecializationTraining_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 3));
                    Sm.CmParam<String>(ref cm, "@Place_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 4));
                    Sm.CmParam<String>(ref cm, "@Period_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 5));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 6));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 7));
                    Sm.CmParam<String>(ref cm, "@TrainingEvaluationDocNo_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 8));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeTraining(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeTraining ");
        //    SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, TrainingEvaluationDocNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@EmpCode, @DNo, @TrainingCode, @SpecializationTraining, @Place, @Period, @Yr, @TrainingEvaluationDocNo, @Remark, @UserCode, CurrentDateTime());");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd8, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@SpecializationTraining", Sm.GetGrdStr(Grd8, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Place", Sm.GetGrdStr(Grd8, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd8, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd8, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd8, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@TrainingEvaluationDocNo", Sm.GetGrdStr(Grd8, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeTraining2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Training */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd10.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd10, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeTraining ");
                        SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, TrainingEvaluationDocNo, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@EmpCode, @DNo_" + r.ToString() +
                        ", @TrainingCode_" + r.ToString() +
                        ", @SpecializationTraining_" + r.ToString() +
                        ", @Place_" + r.ToString() +
                        ", @Period_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @TrainingEvaluationDocNo_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@TrainingCode_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 1));
                    Sm.CmParam<String>(ref cm, "@SpecializationTraining_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 3));
                    Sm.CmParam<String>(ref cm, "@Place_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 4));
                    Sm.CmParam<String>(ref cm, "@Period_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 5));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 6));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 7));
                    Sm.CmParam<String>(ref cm, "@TrainingEvaluationDocNo_" + r.ToString(), Sm.GetGrdStr(Grd10, r, 8));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeTraining2(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeTraining ");
        //    SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, TrainingEvaluationDocNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@EmpCode, @DNo, @TrainingCode, @SpecializationTraining, @Place, @Period, @Yr, @TrainingEvaluationDocNo, @Remark, @UserCode, CurrentDateTime());");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd10, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@SpecializationTraining", Sm.GetGrdStr(Grd10, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Place", Sm.GetGrdStr(Grd10, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd10, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd10, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd10, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@TrainingEvaluationDocNo", Sm.GetGrdStr(Grd10, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeCompetence()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Competence */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, DNo, CompetenceCode, Description, Score, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@EmpCode, @DNo_" + r.ToString() +
                        ", @CompetenceCode_" + r.ToString() +
                        ", @Description_" + r.ToString() +
                        ", @Score_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");


                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Description_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 3));
                    Sm.CmParam<String>(ref cm, "@CompetenceCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Score_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 4));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 5));

                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    CompetenceCode = Values(CompetenceCode), ");
                SQL.AppendLine("    Description = Values(Description), ");
                SQL.AppendLine("    FileName = Values(FileName), ");
                SQL.AppendLine("    Score = Values(Score); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeCompetence(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @CompetenceCode, @Description, @Score, @UserCode, CurrentDateTime()) ");
        //    SQL.AppendLine("On Duplicate Key ");
        //    SQL.AppendLine("Update ");
        //    SQL.AppendLine("    CompetenceCode = @CompetenceCode, Description=@Description, Score=@Score, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd4, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd4, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd4, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeePersonalReference()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Personal Reference */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd7.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd7, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeePersonalReference ");
                        SQL.AppendLine("(EmpCode, DNo, Name, Phone, Address, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    
                    SQL.AppendLine("(@EmpCode, @DNo_" + r.ToString() +
                        ", @Name_" + r.ToString() +
                        ", @Phone_" + r.ToString() +
                        ", @Address_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Name_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 1));
                    Sm.CmParam<String>(ref cm, "@Phone_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 2));
                    Sm.CmParam<String>(ref cm, "@Address_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 3));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 4));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    Name = Values(Name), ");
                SQL.AppendLine("    Phone = Values(Phone), ");
                SQL.AppendLine("    Address = Values(Address), ");
                SQL.AppendLine("    Remark = Values(Remark); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        //private MySqlCommand SaveEmployeePersonalReference(int Row)
        //{
        //    var SQL = new StringBuilder();
        //    SQL.AppendLine("Insert Into TblEmployeePersonalReference(EmpCode, DNo, Name, Phone, Address, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @Name, @Phone, @Address, ");
        //    SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) ");
        //    SQL.AppendLine("On Duplicate Key ");
        //    SQL.AppendLine("Update ");
        //    SQL.AppendLine("Name=@Name, Phone=@Phone, Address=@Address, ");
        //    SQL.AppendLine("Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = SQL.ToString()
        //    };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd7, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@Phone", Sm.GetGrdStr(Grd7, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Address", Sm.GetGrdStr(Grd7, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd7, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


        //    return cm;
        //}

        private MySqlCommand SaveEmployeeFile()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Personal Reference */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd11.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd11, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeFile(EmpCode, DNo, FileName, EmpFileCt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@EmpCode, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @EmpFileCt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd11, r, 1));
                    Sm.CmParam<String>(ref cm, "@EmpFileCt_" + r.ToString(), Sm.GetGrdStr(Grd11, r, 5));

                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName), ");
                SQL.AppendLine("    EmpFileCt = Values(EmpFileCt) ;");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveEmployeeFile(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeFile(EmpCode, DNo, FileName, ");
        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @FileName, ");
        //    SQL.AppendLine("@UserCode, CurrentDateTime()) ");
        //    SQL.AppendLine("On Duplicate Key ");
        //    SQL.AppendLine("Update ");
        //    SQL.AppendLine("    FileName=@FileName, ");
        //    SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = SQL.ToString()
        //    };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd11, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


        //    return cm;
        //}

        private MySqlCommand UpdateEmployeeFile(string EmpCode, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeeFile Set ");
            SQL.AppendLine("    FileName=@FileName, ");
            SQL.AppendLine("    EmpFileCt=@EmpFileCt ");
            SQL.AppendLine("Where EmpCode=@EmpCode and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName); 
            Sm.CmParam<String>(ref cm, "@EmpFileCt", Sm.GetGrdStr(Grd11, Row, 5));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand UpdateEmployeeFile2(string EmpCode, int Row, string FileName, string nameTab)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update ");
            if (nameTab == "workexp")
                SQL.AppendLine("tblemployeeworkexp ");
            if(nameTab == "education")
                SQL.AppendLine("tblemployeeeducation ");
            if (nameTab == "training")
                SQL.AppendLine("tblemployeetraining2 "); 
            if (nameTab == "competence")
                SQL.AppendLine("tblemployeecompetence ");
            if (nameTab == "award")
                SQL.AppendLine("tblemployeeaward ");
            if (nameTab == "vaccine")
                SQL.AppendLine("tblemployeevaccine ");
            if (nameTab == "position")
                SQL.AppendLine("tblemployeepositionhistory ");
            if (nameTab == "grade")
                SQL.AppendLine("tblemployeegradehistory ");


            SQL.AppendLine("SET FileName=@FileName ");
            if (nameTab == "position")
                SQL.AppendLine("Where EmpCode=@EmpCode And DocNo =@DocNo");
            else
                SQL.AppendLine("Where EmpCode=@EmpCode and right(DNo, 3) = @DNo  ");

            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            if (nameTab == "position")
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd15, Row, 0));

            return cm;
        }



        private MySqlCommand DeleteEmployeeFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblEmployeeFile Where EmpCode=@EmpCode; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        private MySqlCommand UpdateEmployee()
        {
            var SQL = new StringBuilder();

            
            if(IsEmployeeUpdateNeedApproval())
                SQL.AppendLine("Delete From TblEmployeeFamilyUpdate Where EmpCode = @EmpCode;");
            else
                SQL.AppendLine("Delete From TblEmployeeFamily Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeWorkExp Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeEducation Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeTraining Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeCompetence Where EmpCode=@EmpCode; ");
            //SQL.AppendLine("Delete From TblEmployeeSalary Where EmpCode=@EmpCode; ");
            //SQL.AppendLine("Delete From TblEmployeeAllowanceDeduction Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeePersonalReference Where EmpCode=@EmpCode; ");
            //SQL.AppendLine("Delete From TblEmployeeFile Where EmpCode=@EmpCode; ");
            if (mIsEmployeeTabAwardEnabled) SQL.AppendLine("Delete From TblEmployeeAward Where EmpCode=@EmpCode; ");
            //if (mIsEmployeeTabPositionHistoryEnabled) SQL.AppendLine("Delete From TblEmployeePositionHistory Where EmpCode=@EmpCode; ");
            if (mIsEmployeeTabTraining3Enabled) SQL.AppendLine("Delete From TblEmployeeTraining2 Where EmpCode=@EmpCode; ");
            if (mIsEmployeeVaccineEnabled) SQL.AppendLine("Delete From TblEmployeeVaccine Where EmpCode=@EmpCode; ");

            if (IsEmployeeUpdateNeedApproval())
            {
                SQL.AppendLine("Insert Into TblEmployeeUpdate(EmpCode, EmpName, DisplayName, UserCode, EmpCodeOld, ShortCode, DeptCode, PosCode, PositionStatusCode, ");
                SQL.AppendLine("JoinDt, ResignDt, TGDt, IdNumber, MaritalStatus, WeddingDt, Gender, Religion, Mother, BirthPlace, BirthDt, Address, ");
                SQL.AppendLine("CityCode, SubDistrict, Village, RTRW, PostalCode, EntCode, SiteCode, SectionCode, WorkGroupCode, Domicile, BloodType, RhesusType, Phone, Mobile, Email,GrdLvlCode,NPWP, SystemType, PayrollType, ");
                SQL.AppendLine("PTKP,BankCode,BankAcName,BankAcNo,BankBranch,ContractDt, ");
                SQL.AppendLine("PayrunPeriod, EmploymentStatus, DivisionCode, PGCode, POH, LeaveStartDt, ");
                SQL.AppendLine("ClothesSize, ShoeSize, EducationType, CostGroup, LevelCode, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@EmpCode, @EmpName, @DisplayName, @UsCode, @EmpCodeOld, @ShortCode, @DeptCode, @PosCode, @PositionStatusCode, ");
                SQL.AppendLine("@JoinDt, @ResignDt, @TGDt, @IdNumber, @MaritalStatus, @WeddingDt, @Gender, @Religion, @Mother, @BirthPlace, @BirthDt, @Address, ");
                SQL.AppendLine("@CityCode, @SubDistrict, @Village, @RTRW, @PostalCode, @EntCode, @SiteCode, @SectionCode, @WorkGroupCode, @Domicile, @BloodType, @RhesusType, @Phone, @Mobile, @Email, @GrdLvlCode, @NPWP, @SystemType, @PayrollType,@PTKP,@BankCode,@BankAcName,@BankAcNo,@BankBranch,@ContractDt, ");
                SQL.AppendLine("@PayrunPeriod, @EmploymentStatus, @DivisionCode, @PGCode, @POH, ");
                if (mIsAnnualLeaveUseStartDt)
                    SQL.AppendLine("@LeaveStartDt, ");
                else
                    SQL.AppendLine("Null, ");
                SQL.AppendLine("@ClothesSize, @ShoeSize, @EducationType, @CostGroup, @LevelCode, ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update ");
                SQL.AppendLine("    ApproveUserCode=Null, ApproveStatus=Null, ApproveRemark=Null, EmpName=@EmpName, DisplayName=@DisplayName, UserCode=@UsCode, EmpCodeOld=@EmpCodeOld, ShortCode=@ShortCode, DeptCode=@DeptCode, PosCode=@PosCode, PositionStatusCode=@PositionStatusCode, ");
                SQL.AppendLine("    JoinDt=@JoinDt, ResignDt=@ResignDt, TGDt=@TGDt, IdNumber=@IdNumber, MaritalStatus=@MaritalStatus, WeddingDt=@WeddingDt, Gender=@Gender, ");
                SQL.AppendLine("    Religion=@Religion, Mother=@Mother, BirthPlace=@BirthPlace, BirthDt=@BirthDt, Address=@Address, ");
                SQL.AppendLine("    CityCode=@CityCode, SubDistrict=@SubDistrict, Village=@Village, RTRW=@RTRW, PostalCode=@PostalCode, EntCode=@EntCode, SiteCode=@SiteCode, SectionCode=@SectionCode, WorkGroupCode=@WorkGroupCode, Domicile=@Domicile, BloodType=@BloodType, Phone=@Phone, Mobile=@Mobile,Email=@Email, ");
                SQL.AppendLine("    GrdLvlCode=@GrdLvlCode, NPWP=@NPWP, SystemType=@SystemType, PayrollType=@PayrollType, PTKP=@PTKP, BankCode=@BankCode,BankAcName=@BankAcName,BankAcNo=@BankAcNo,BankBranch=@BankBranch,ContractDt=@ContractDt, ");
                SQL.AppendLine("    PayrunPeriod=@PayrunPeriod, EmploymentStatus=@EmploymentStatus, DivisionCode=@DivisionCode, PGCode=@PGCode, POH=@POH, LeaveStartDt=@LeaveStartDt, ");
                if (mIsAnnualLeaveUseStartDt) SQL.AppendLine("LeaveStartDt=@LeaveStartDt, ");
                SQL.AppendLine("    ClothesSize=@ClothesSize, ShoeSize=@ShoeSize, EducationType=@EducationType, CostGroup=@CostGroup, LevelCode=@LevelCode, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            }
            else
            {
                SQL.AppendLine("Update TblEmployee Set ");
                SQL.AppendLine("EmpName=@EmpName, DisplayName=@DisplayName, UserCode=@UsCode, EmpCodeOld=@EmpCodeOld, ShortCode=@ShortCode, DeptCode=@DeptCode, PosCode=@PosCode, PositionStatusCode=@PositionStatusCode, ");
                SQL.AppendLine("JoinDt=@JoinDt, ResignDt=@ResignDt, TGDt=@TGDt, IdNumber=@IdNumber, MaritalStatus=@MaritalStatus, WeddingDt=@WeddingDt, Gender=@Gender, ");
                SQL.AppendLine("Religion=@Religion, Mother=@Mother, BirthPlace=@BirthPlace, BirthDt=@BirthDt, Address=@Address, ");
                SQL.AppendLine("CityCode=@CityCode, SubDistrict=@SubDistrict, Village=@Village, RTRW=@RTRW, PostalCode=@PostalCode, EntCode=@EntCode, SiteCode=@SiteCode, SectionCode=@SectionCode, WorkGroupCode=@WorkGroupCode, Domicile=@Domicile, BloodType=@BloodType, RhesusType=@RhesusType, Phone=@Phone, Mobile=@Mobile,Email=@Email, ");
                SQL.AppendLine("GrdLvlCode=@GrdLvlCode, NPWP=@NPWP, SystemType=@SystemType, PayrollType=@PayrollType, PTKP=@PTKP, BankCode=@BankCode,BankAcName=@BankAcName,BankAcNo=@BankAcNo,BankBranch=@BankBranch,ContractDt=@ContractDt, ");
                SQL.AppendLine("PayrunPeriod=@PayrunPeriod, EmploymentStatus=@EmploymentStatus, DivisionCode=@DivisionCode, PGCode=@PGCode, POH=@POH, LeaveStartDt=@LeaveStartDt, ");
                if (mIsAnnualLeaveUseStartDt) SQL.AppendLine("LeaveStartDt=@LeaveStartDt, ");

                if (mIsMaritalStatusMandatory) SQL.AppendLine("SpouseEmpCode = @SpouseEmpCode, ");

                SQL.AppendLine("ClothesSize=@ClothesSize, ShoeSize=@ShoeSize, EducationType=@EducationType, CostGroup=@CostGroup, LevelCode=@LevelCode, ");
                SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where EmpCode = @EmpCode; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpName", TxtEmpName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@UsCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCodeOld", TxtEmpCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntityCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSection));
            Sm.CmParam<String>(ref cm, "@WorkGroupCode", Sm.GetLue(LueWorkGroup));
            Sm.CmParam<String>(ref cm, "@Domicile", MeeDomicile.Text);
            Sm.CmParam<String>(ref cm, "@BloodType", Sm.GetLue(LueBloodType));
            Sm.CmParam<String>(ref cm, "@RhesusType", Sm.GetLue(LueBloodRhesus));
            Sm.CmParamDt(ref cm, "@JoinDt", Sm.GetDte(DteJoinDt));
            Sm.CmParamDt(ref cm, "@ResignDt", Sm.GetDte(DteResignDt));
            Sm.CmParamDt(ref cm, "@ContractDt", Sm.GetDte(DteContractDt));
            Sm.CmParamDt(ref cm, "@TGDt", Sm.GetDte(DteTGDt));
            Sm.CmParam<String>(ref cm, "@IdNumber", TxtIdNumber.Text);
            Sm.CmParam<String>(ref cm, "@MaritalStatus", Sm.GetLue(LueMaritalStatus));
            Sm.CmParamDt(ref cm, "@WeddingDt", Sm.GetDte(DteWeddingDt));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
            Sm.CmParam<String>(ref cm, "@Religion", Sm.GetLue(LueReligion));
            Sm.CmParam<String>(ref cm, "@Mother", TxtMother.Text);
            Sm.CmParam<String>(ref cm, "@BirthPlace", TxtBirthPlace.Text);
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteBirthDt));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@SubDistrict", TxtSubDistrict.Text);
            Sm.CmParam<String>(ref cm, "@Village", TxtVillage.Text);
            Sm.CmParam<String>(ref cm, "@RTRW", TxtRTRW.Text);
            Sm.CmParam<String>(ref cm, "@PostalCode", TxtPostalCode.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrollType", Sm.GetLue(LuePayrollType));
            Sm.CmParam<String>(ref cm, "@PTKP", Sm.GetLue(LuePTKP));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@BankAcName", TxtBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@ClothesSize", Sm.GetLue(LueClothesSize));
            Sm.CmParam<String>(ref cm, "@ShoeSize", Sm.GetLue(LueShoeSize));
            Sm.CmParam<String>(ref cm, "@EducationType", Sm.GetLue(LueEducationType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivisionCode));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@POH", TxtPOH.Text);
            Sm.CmParam<String>(ref cm, "@CostGroup", Sm.GetLue(LueCostGroup));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevelCode));
            if (mIsAnnualLeaveUseStartDt) Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteLeaveStartDt));
            if (mIsMaritalStatusMandatory) Sm.CmParam<String>(ref cm, "@SpouseEmpCode", TxtSpouseEmpCode.Text);

            return cm;
        }

		private MySqlCommand UpdateEmployeeGradeHistory()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Grade History */ ");

            SQL.AppendLine("Delete From TblEmployeeGradeHistory Where EmpCode = @EmpCode;");

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd17.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd17, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeGradeHistory ");
                        SQL.AppendLine("(EmpCode, DNo, DocDt, DecisionLetter, Duration, PeriodicSettingInd, GrdLvlCode, Amt, Remark, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@EmpCode, @DNo_" + r.ToString() +
                        ", @DocDt_" + r.ToString() +
                        ", @DecisionLetter_" + r.ToString() +
                        ", @Duration_" + r.ToString() +
                        ", @PeriodicSettingInd_" + r.ToString() +
                        ", @GrdLvlCode_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParamDt(ref cm, "@DocDt_" + r.ToString(), Sm.GetGrdDate(Grd17, r, 1));
                    Sm.CmParam<String>(ref cm, "@DecisionLetter_" + r.ToString(), Sm.GetGrdStr(Grd17, r, 2));
                    Sm.CmParam<String>(ref cm, "@Duration_" + r.ToString(), Sm.GetGrdStr(Grd17, r, 3));
                    Sm.CmParam<String>(ref cm, "@PeriodicSettingInd_" + r.ToString(), Sm.GetGrdBool(Grd17, r, 6) ? "Y" : "N" );
                    Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + r.ToString(), Sm.GetGrdStr(Grd17, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd17, r, 8));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd17, r, 5));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd17, r, 9));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        
        private MySqlCommand SaveEmployeeVaccine()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Vaccine */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd12.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd12, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeVaccine ");
                        SQL.AppendLine("(EmpCode, DNo, VaccineDt, Vaccine, Remark, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@EmpCode, @DNo_" + r.ToString() +
                        ", @VaccineDt_" + r.ToString() +
                        ", @Vaccine_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParamDt(ref cm, "@VaccineDt_" + r.ToString(), Sm.GetGrdDate(Grd12, r, 1));
                    Sm.CmParam<String>(ref cm, "@Vaccine_" + r.ToString(), Sm.GetGrdStr(Grd12, r, 2));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd12, r, 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd12, r, 4));

                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        //private MySqlCommand SaveEmployeeVaccine(int r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmployeeVaccine ");
        //    SQL.AppendLine("(EmpCode, DNo, VaccineDt, Vaccine, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@EmpCode, @DNo, @VaccineDt, @Vaccine, @Remark, @UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
        //    Sm.CmParamDt(ref cm, "@VaccineDt", Sm.GetGrdDate(Grd12, r, 1));
        //    Sm.CmParam<String>(ref cm, "@Vaccine", Sm.GetGrdStr(Grd12, r, 2));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd12, r, 3));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveEmployeeAward()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Award */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd14.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd14, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeAward(EmpCode, DNo, AwardCt, Description, Yr, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@EmpCode, @DNo_" + r.ToString() + 
                        ", @AwardCt_" + r.ToString() +
                        ", @Description_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@AwardCt_" + r.ToString(), Sm.GetGrdStr(Grd14, r, 1));
                    Sm.CmParam<String>(ref cm, "@Description_" + r.ToString(), Sm.GetGrdStr(Grd14, r, 3));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd14, r, 4));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd14, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeTraining3()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Employee - Training */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd16.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd16, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeTraining2 ");
                        SQL.AppendLine("(EmpCode, DNo, TrainingCode, TrainingType, Specialization, Category, Place, Yr, Duration, Period, Value, Ranking, Remark, EducationCenter, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@EmpCode, @DNo_" + r.ToString() +
                        ", @TrainingCode_" + r.ToString() +
                        ", @TrainingType_" + r.ToString() +
                        ", @Specialization_" + r.ToString() +
                        ", @Category_" + r.ToString() +
                        ", @Place_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @Duration_" + r.ToString() +
                        ", @Period_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @Ranking_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @EducationCenter_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@TrainingCode_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 1));
                    Sm.CmParam<String>(ref cm, "@TrainingType_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 3));
                    Sm.CmParam<String>(ref cm, "@Specialization_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 5));
                    Sm.CmParam<String>(ref cm, "@Category_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 7));
                    Sm.CmParam<String>(ref cm, "@Place_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 8));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 9));
                    Sm.CmParam<String>(ref cm, "@Duration_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Period_" + r.ToString(), Sm.GetGrdDec(Grd16, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd16, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Ranking_" + r.ToString(), Sm.GetGrdDec(Grd16, r, 13));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 14));
                    Sm.CmParam<String>(ref cm, "@EducationCenter_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 15));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd16, r, 18));

                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsEmployeeNameNotEditable', 'apakah field employee name pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsOldCodeNotEditable', 'apakah field Old Code pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsShortCodeNotEditable', 'apakah field ShortCode pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsDisplayNameNotEditable', 'apakah field Display Name pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsUserCodeNotEditable', 'apakah field UserCode pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsBirthPlaceNotEditable', 'apakah field Birth Place pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt) Values('IsBirthDateNotEditable', 'apakah field Birth Date pada master employee tidak editable, Y: tidak editable, N: editable(default system)', 'N', 'PHT', 'WEDHA', '202304051300'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsMaritalStatusMandatory', 'Indikator untuk status pernikahan wajib di isi', 'N', 'PHT', NULL, 'Y', 'WEDHA', '201704261151', NULL, NULL); ");

            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('EmployeeFileUploadCategory', '01', 'BPJS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sys', '202303130000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('EmployeeFileUploadCategory', '02', 'Kependudukan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sys', '202303130000', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tblemployee` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `SpouseEmpCode` VARCHAR(50) NULL DEFAULT NULL AFTER `EmpCodeOld`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `SpouseEmpCode` (`SpouseEmpCode`); ");

            SQL.AppendLine("ALTER TABLE `tblemployeefile` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `EmpFileCt` VARCHAR(30) NULL DEFAULT NULL AFTER `FileName`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeeworkexp` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeeeducation` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeetraining2` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeecompetence` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Level`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeevaccine` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeeaward` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Yr`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeegradehistory` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeepositionhistory` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `SiteName`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeeworkexprecruitment` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblemployeeeducationrecruitment` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `Remark`; ");

            SQL.AppendLine("ALTER TABLE `tblwarningletter` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `ADCode`; ");

            Sm.ExecQuery(SQL.ToString());
        }

        private bool IsEmployeeUpdateNeedApproval()
        {
            return (mIsEmployeeUpdateNeedApproval &&
                    Sm.IsDataExist("Select 1 " +
                        "From TblDocApprovalSettingEmpUpdate " +
                        "Where SiteCode = @Param", Sm.GetLue(LueSiteCode))
                    );
        }

        private bool IsEmpMeritIncreaseExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblEmpMeritIncreaseHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A') ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString());
        }

        private void SetLueDivisionCodeBasedOnSite(ref LookUpEdit Lue, string SiteCode, string DivisionCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DivisionCode As Col1, DivisionName As Col2 From TblDivision ");
            if (DivisionCode.Length > 0)
            {
                SQL.AppendLine("Where (DivisionCode=@DivisionCode ");
                SQL.AppendLine("Or DivisionCode In (Select DivisionCode From TblSiteDivision Where SiteCode=@SiteCode)) ");
            }
            else
                SQL.AppendLine("Where ActInd='Y' And DivisionCode In (Select DivisionCode From TblSiteDivision Where SiteCode=@SiteCode) ");
            SQL.AppendLine("Order By DivisionName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DivisionCode.Length > 0) Sm.SetLue(Lue, DivisionCode);
        }

        private void SetLuePosCodeBasedOnDepartment(ref LookUpEdit Lue, string DeptCode, string PosCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PosCode As Col1, PosName As Col2 From TblPosition ");
            if (PosCode.Length > 0)
            {
                SQL.AppendLine("Where (PosCode=@PosCode ");
                SQL.AppendLine("Or PosCode In (Select PosCode From TblDepartmentPosition Where DeptCode=@DeptCode)) ");
            }
            else
                SQL.AppendLine("Where PosCode In (Select PosCode From TblDepartmentPosition Where DeptCode=@DeptCode) ");
            SQL.AppendLine("Order By PosName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PosCode.Length > 0) Sm.SetLue(Lue, PosCode);
        }

        private void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.TrainingCode As Col1, T.TrainingName As Col2 ");
            SQL.AppendLine("From TblTraining T ");
            SQL.AppendLine("Where T.ActiveInd = 'Y' ");
            SQL.AppendLine("Order By T.TrainingName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCompetenceCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select CompetenceCode As Col1, CompetenceName As Col2 ");
            //SQL.AppendLine("From TblOrganizationalStructureDtl4 ");
            //SQL.AppendLine("Where PosCode = @PosCode ");
            //SQL.AppendLine("Order By DNo; ");
            SQL.AppendLine("Select T.CompetenceCode As Col1, T.CompetenceName As Col2 ");
            SQL.AppendLine("From TblCompetence T; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            //Sm.CmParam<String>(ref cm, "@PosCode", "1");
            
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsEmpMeritIncreaseExists = IsEmpMeritIncreaseExists();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'LongServiceLeaveCode', 'LongServiceLeaveCode2', 'IsDeptFilterByDivision', 'IsFilterByDeptHR', 'IsFilterBySiteHR', ");
            SQL.AppendLine("'IsEmpCodeAutoGenerate', 'FormatFTPClient', 'EmploymentStatusTetap', 'GenerateEmpCodeFormat', 'FileSizeMaxUploadFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'PasswordForFTPClient', 'UsernameForFTPClient', 'SharedFolderForFTPClient', 'HostAddrForFTPClient', ");
            SQL.AppendLine("'EmployeeTrainingTabCode', 'EmployeeAcNo', 'EmployeeAcDesc', 'AcNoForAdvancePayment', 'AcDescForAdvancePayment', ");
            SQL.AppendLine("'EmpContractDocEmploymentStatus', 'IsPltCheckboxNotBasedOnPosition', 'IsEmployeeVaccineEnabled', 'IsEmpSpecializationNotMandatory', 'IsPPSActive', ");
            SQL.AppendLine("'IsPltCheckboxNotBasedOnPosition', 'IsEmployeeUpdateNeedApproval', 'IsEmployeeUseGradeSalary', 'IsEmployeeLevelEnabled', 'IsEmployeeTabAdditionalEnabled', ");
            SQL.AppendLine("'IsEmployeeAllowedToEditCompetence', 'IsOldCodeAutoFillAfterPromotion', 'IsEmployeePrintoutNotUseCompetenceAndPotency', 'IsEmployeeUseCodeOfConduct', 'IsEmployeeEducationInputManualData', ");
            SQL.AppendLine("'IsEmpOldCodeEditable', 'IsSiteDivisionEnabled', 'IsDivisionDepartmentEnabled', 'IsDepartmentPositionEnabled', 'IsEmployeeAllowToUploadFile', ");
            SQL.AppendLine("'IsEmpCodeCopyToOldCode', 'IsAnnualLeaveUseStartDt', 'IsSiteMandatory', 'IsEmployeeAutoCreatePPS', 'IsEmpContractDtMandatory', ");
            SQL.AppendLine("'IsNewEmployeeInformationAmendmentActive', 'IsEmployeeCOAInfoMandatory', 'IsPayrollActived', 'IsEmployeeTabWarningLetterEnabled', 'IsEmployeeTabAwardEnabled', ");
            SQL.AppendLine("'IsEmployeeTabPositionHistoryEnabled', 'IsEmployeeTabTraining3Enabled', 'IsEmployeeTabGradeHistoryEnabled', 'DocTitle', 'SSRetiredMaxAge2', 'IsEmployeeUseFirstGrade', ");
            SQL.AppendLine("'IsPrintoutMasterEmpNotUseDetailInformation', 'IsEmployeeUseBiologicalMother', 'IsMaritalStatusMandatory', 'IsEmpWLAllowToEditFile', 'IsEmployeeFindShowFamily', ");
            SQL.AppendLine("'IsEmployeeNameNotEditable','IsOldCodeNotEditable','IsShortCodeNotEditable','IsDisplayNameNotEditable','IsUserCodeNotEditable','IsBirthPlaceNotEditable','IsBirthDateNotEditable', ");
            SQL.AppendLine("'IsEmployeeTabPersonalRefNotEditable' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsEmployeeTabGradeHistoryEnabled": mIsEmployeeTabGradeHistoryEnabled = ParValue == "Y"; break;
                            case "IsEmployeeTabAwardEnabled": mIsEmployeeTabAwardEnabled = ParValue == "Y"; break;
                            case "IsEmployeeTabPositionHistoryEnabled": mIsEmployeeTabPositionHistoryEnabled = ParValue == "Y"; break;
                            case "IsEmployeeTabTraining3Enabled": mIsEmployeeTabTraining3Enabled = ParValue == "Y"; break;
                            case "IsNewEmployeeInformationAmendmentActive": mIsNewEmployeeInformationAmendmentActive = ParValue == "Y"; break;
                            case "IsEmployeeCOAInfoMandatory": mIsEmployeeCOAInfoMandatory = ParValue == "Y"; break;
                            case "IsPayrollActived": mIsPayrollActived = ParValue == "Y"; break;
                            case "IsEmpCodeCopyToOldCode": mIsEmpCodeCopyToOldCode = ParValue == "Y"; break;
                            case "IsAnnualLeaveUseStartDt": mIsAnnualLeaveUseStartDt = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsEmployeeAutoCreatePPS": mIsEmployeeAutoCreatePPS = ParValue == "Y"; break;
                            case "IsEmpContractDtMandatory": mIsEmpContractDtMandatory = ParValue == "Y"; break;
                            case "IsEmpOldCodeEditable": mIsEmpOldCodeEditable = ParValue == "Y"; break;
                            case "IsSiteDivisionEnabled": mIsSiteDivisionEnabled = ParValue == "Y"; break;
                            case "IsDivisionDepartmentEnabled": mIsDivisionDepartmentEnabled = ParValue == "Y"; break;
                            case "IsDepartmentPositionEnabled": mIsDepartmentPositionEnabled = ParValue == "Y"; break;
                            case "IsEmployeeAllowToUploadFile": mIsEmployeeAllowToUploadFile = ParValue == "Y"; break;
                            case "IsEmployeeAllowedToEditCompetence": mIsEmployeeAllowedToEditCompetence = ParValue == "Y"; break;
                            case "IsOldCodeAutoFillAfterPromotion": mIsOldCodeAutoFillAfterPromotion = ParValue == "Y"; break;
                            case "IsEmployeePrintoutNotUseCompetenceAndPotency": mIsEmployeePrintoutNotUseCompetenceAndPotency = ParValue == "Y"; break;
                            case "IsEmployeeUseCodeOfConduct": mIsEmployeeUseCodeOfConduct = ParValue == "Y"; break;
                            case "IsEmployeeEducationInputManualData": mIsEmployeeEducationInputManualData = ParValue == "Y"; break;
                            case "IsEmployeeUpdateNeedApproval": mIsEmployeeUpdateNeedApproval = ParValue == "Y"; break;
                            case "IsEmployeeUseGradeSalary": mIsEmployeeUseGradeSalary = ParValue == "Y"; break;
                            case "IsEmployeeLevelEnabled": mIsEmployeeLevelEnabled = ParValue == "Y"; break;
                            case "IsEmployeeTabAdditionalEnabled": mIsEmployeeTabAdditionalEnabled = ParValue == "Y"; break;
                            case "IsPPSActive": mIsPPSActive = ParValue == "Y"; break;
                            case "IsEmpSpecializationNotMandatory": mIsEmpSpecializationNotMandatory = ParValue == "Y"; break;
                            case "IsEmployeeVaccineEnabled": mIsEmployeeVaccineEnabled = ParValue == "Y"; break;
                            case "IsPltCheckboxNotBasedOnPosition": mIsPltCheckboxNotBasedOnPosition = ParValue == "Y"; break;
                            case "IsDeptFilterByDivision": mIsDeptFilterByDivision = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsEmpCodeAutoGenerate": mIsEmpCodeAutoGenerate = ParValue == "Y"; break;
                            case "IsEmployeeTabWarningLetterEnabled": mIsEmployeeTabWarningLetterEnabled = ParValue == "Y"; break;
                            case "IsEmployeeUseFirstGrade": mIsEmployeeUseFirstGrade = ParValue == "Y"; break;
                            case "IsPrintoutMasterEmpNotUseDetailInformation": mIsPrintoutMasterEmpNotUseDetailInformation = ParValue == "Y"; break;
                            case "IsEmployeeUseBiologicalMother": mIsEmployeeUseBiologicalMother = ParValue == "Y"; break;
                            case "IsMaritalStatusMandatory": mIsMaritalStatusMandatory = ParValue == "Y"; break;
                            case "IsEmpWLAllowToEditFile": mIsEmpWLAllowToEditFile = ParValue == "Y"; break;
                            case "IsEmployeeNameNotEditable": mIsEmployeeNameNotEditable = ParValue == "Y"; break;
                            case "IsOldCodeNotEditable": mIsOldCodeNotEditable = ParValue == "Y"; break;
                            case "IsShortCodeNotEditable": mIsShortCodeNotEditable = ParValue == "Y"; break;
                            case "IsDisplayNameNotEditable": mIsDisplayNameNotEditable = ParValue == "Y"; break;
                            case "IsUserCodeNotEditable": mIsUserCodeNotEditable = ParValue == "Y"; break;
                            case "IsBirthPlaceNotEditable": mIsBirthPlaceNotEditable = ParValue == "Y"; break;
                            case "IsBirthDateNotEditable": mIsBirthDateNotEditable = ParValue == "Y"; break;
                            case "IsEmployeeFindShowFamily": mIsEmployeeFindShowFamily = ParValue == "Y"; break;
                            case "IsEmployeeTabPersonalRefNotEditable": mIsEmployeeTabPersonalRefNotEditable = ParValue == "Y"; break;

                            //string
                            case "AcDescForAdvancePayment": mAcDescForAdvancePayment = ParValue; break;
                            case "AcNoForAdvancePayment": mAcNoForAdvancePayment = ParValue; break;
                            case "EmployeeAcDesc": mEmployeeAcDesc = ParValue; break;
                            case "EmployeeAcNo": mEmployeeAcNo = ParValue; break;
                            case "EmployeeTrainingTabCode": mEmployeeTrainingTabCode = ParValue; break;
                            case "EmpContractDocEmploymentStatus": mEmpContractDocEmploymentStatus = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "GenerateEmpCodeFormat": mGenerateEmpCodeFormat = ParValue; break;
                            case "EmploymentStatusTetap": mEmploymentStatusTetap = ParValue; break;
                            case "LongServiceLeaveCode": mLongServiceLeaveCode = ParValue; break;
                            case "LongServiceLeaveCode2": mLongServiceLeaveCode2 = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "SSRetiredMaxAge2": mSSRetiredMaxAge2 = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mGenerateEmpCodeFormat.Length == 0) mGenerateEmpCodeFormat = "1";
            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
        }

        //Untuk input combobox dalam grid
        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private static void LueRequestEdit(
            ref iGrid Grd, ref LookUpEdit Lue,
            ref iGCell fCell, ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e,
            int c)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, c).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, c));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueSection(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SectionCode As Col1, SectionName As Col2 ");
                SQL.AppendLine("From TblSection Where ActInd='Y' Order By SectionName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWorkGroup(ref DXE.LookUpEdit Lue, string SectionCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WorkGroupCode As Col1, WorkGroupName As Col2 ");
                SQL.AppendLine("From TblWorkingGroup Where ActInd='Y' And SectionCode = '" + SectionCode + "' Order By WorkGroupName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T Where 0 = 0 ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("And DeptCode=@Code  ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("Or Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("And ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept, string DivisionCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code ");
                SQL.AppendLine("Or (DivisionCode=IfNull(@DivisionCode, 'XXX') ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where DivisionCode=IfNull(@DivisionCode, 'XXX') ");
                SQL.AppendLine("And T.ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
            {
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            }
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueDeptCodeBasedOnDivision(ref LookUpEdit Lue, string DivisionCode, string DeptCode, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DeptCode As Col1, DeptName As Col2 From TblDepartment T ");
            if (DeptCode.Length > 0)
            {
                SQL.AppendLine("Where (DeptCode=@DeptCode ");
                SQL.AppendLine("Or DeptCode In (Select DeptCode From TblDivisionDepartment Where DivisionCode=@DivisionCode)) ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("Where DeptCode In (Select DeptCode From TblDivisionDepartment Where DivisionCode=@DivisionCode) ");
            }
            SQL.AppendLine("Order By DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DeptCode.Length > 0) Sm.SetLue(Lue, DeptCode);
        }


        private void SetLueSiteCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where SiteCode=@Code ");
                SQL.AppendLine("Or (T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.SiteName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void ParPrint(string ProfilePicture)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "Employee Code", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<EmpHdr>();
            var l1 = new List<EmpGeneral>();
            var l2 = new List<EmpPersonal>();
            var l3 = new List<EmpWorkExperience>();
            var l4 = new List<EmpEducation>();
            var l5 = new List<EmpCompetence>();
            var l6 = new List<EmpSS>();
            var l7 = new List<EmpPersonalRef>();
            var l8 = new List<EmpFamilySS>();
            var l9 = new List<EmpWL>();
            var l10 = new List<EmpPPS>();
            var l11 = new List<EmpCompetenceOther>();
            var l12 = new List<EmpPotency>();
            var l13 = new List<EmpTraining>();
            var l14 = new List<EmpPositionPHT>();
            var l15 = new List<EmpRiwayatPangkatPHT>();
            var l16 = new List<EmpRiwayatJabatanPHT>();
            var l17 = new List<EmpKursusPusdikPHT>();
            var l18 = new List<EmpKursusNonPusdikPHT>();
            var l19 = new List<EmpPenghargaanPHT>();
            var l20 = new List<EmpHukumanPHT>();
            var l21 = new List<EmpPiutangPHT>();

            string[] TableName = { "EmpHdr", "EmpGeneral", "EmpPersonal", "EmpWorkExperience", "EmpEducation", "EmpCompetence", "EmpSS", "EmpPersonalRef", "EmpFamilySS", "EmpWL", "EmpPPS", "EmpCompetenceOther", "EmpPotency", "EmpTraining", "EmpPositionPHT", "EmpRiwayatPangkatPHT", "EmpRiwayatJabatanPHT", "EmpKursusPusdikPHT", "EmpKursusNonPusdik", "EmpPenghargaanPHT", "EmpHukumanPHT", "EmpPiutangPHT" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("A.EmpCode, A.EmpName, B.DeptName, C.PosName, /*@ProfilePicture As ProfilePicture,*/ ");
            SQL.AppendLine("CONCAT(IFNULL(D.ParValue, ''), @EmpCode, '.jpg') AS ProfilePicture, A.EmpCodeOld, A.IDNumber, A.NPWP, A.Phone, A.Mobile, A.Email ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode = C.PosCode ");
            SQL.AppendLine("Left Join TblParameter D ON D.ParCode = 'ImgFileEmployee' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@ProfilePicture", ProfilePicture);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",
                         
                         //6-10
                         "CompanyFax",
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         
                         //11-15
                         "CompLocation2",
                         "EmpCode",
                         "EmpName",
                         "DeptName",
                         "PosName",

                        //16-20
                         "ProfilePicture",
                         "EmpCodeOld",
                         "IDNumber",
                         "NPWP",
                         "Phone",

                         //21-22
                         "Mobile",
                         "Email",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            CompanyFax = Sm.DrStr(dr, c[6]),
                            Shipper1 = Sm.DrStr(dr, c[7]),
                            Shipper2 = Sm.DrStr(dr, c[8]),
                            Shipper3 = Sm.DrStr(dr, c[9]),
                            Shipper4 = Sm.DrStr(dr, c[10]),

                            CompLocation2 = Sm.DrStr(dr, c[11]),
                            EmpCode = Sm.DrStr(dr, c[12]),
                            EmpName = Sm.DrStr(dr, c[13]),
                            DeptName = Sm.DrStr(dr, c[14]),
                            PosName = Sm.DrStr(dr, c[15]),

                            ProfilePicture = Sm.DrStr(dr, c[16]),
                            EmpCodeOld = Sm.DrStr(dr, c[17]),
                            IDNumber = Sm.DrStr(dr, c[18]),
                            NPWP = Sm.DrStr(dr, c[19]),
                            Phone = Sm.DrStr(dr, c[20]),

                            Mobile = Sm.DrStr(dr, c[21]),
                            Email = Sm.DrStr(dr, c[22]),

                            Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                else
                {
                    l.Add(new EmpHdr()
                    {
                        CompanyLogo = "",

                        CompanyName = "",
                        CompanyAddress = "",
                        CompanyAddressCity = "",
                        CompanyPhone = "",
                        CompanyEmail = "",

                        CompanyFax = "",
                        Shipper1 = "",
                        Shipper2 = "",
                        Shipper3 = "",
                        Shipper4 = "",

                        CompLocation2 = "",
                        EmpCode = "",
                        EmpName = "",
                        DeptName = "",
                        PosName = "",

                        ProfilePicture = "",

                        Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                    });
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region General
            var cml1 = new MySqlCommand();

            var SQL1 = new StringBuilder();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cml1.Connection = cn1;

                SQL1.AppendLine("Select If(IfNull(A.BirthPlace, '')='', '', Concat(A.BirthPlace, ',')) As BirthPlace, ");
                SQL1.AppendLine("Concat(Right(A.BirthDt, 2), ' ', ");
                SQL1.AppendLine("  case MONTH(A.BirthDt) ");
                SQL1.AppendLine("      when 1 then 'Januari' ");
                SQL1.AppendLine("      when 2 then 'Februari' ");
                SQL1.AppendLine("      when 3 then 'Maret' ");
                SQL1.AppendLine("      when 4 then 'April' ");
                SQL1.AppendLine("      when 5 then 'Mei' ");
                SQL1.AppendLine("      when 6 then 'Juni' ");
                SQL1.AppendLine("      when 7 then 'Juli' ");
                SQL1.AppendLine("      when 8 then 'Agustus' ");
                SQL1.AppendLine("      when 9 then 'September' ");
                SQL1.AppendLine("      when 10 then 'Oktober' ");
                SQL1.AppendLine("      when 11 then 'November' ");
                SQL1.AppendLine("      when 12 then 'Desember' ");
                SQL1.AppendLine("END, ' ', Left(A.BirthDt, 4))  As BirthDt, ");
                SQL1.AppendLine("A.Address, B.CityName, A.SubDistrict, A.Village, A.RTRW, A.PostalCode, ");
                SQL1.AppendLine("C.OptDesc As Religion, F.OptDesc As Gender, IfNull(DATE_FORMAT(A.JoinDt, '%d %M %Y'), '') As JoinDt, IfNull(DATE_FORMAT(A.ResignDt, '%d %M %Y'), '') As ResignDt, E.OptDesc As EmploymentStatus, ");
                SQL1.AppendLine("D.OptDesc As MaritalStatus, IfNull(DATE_FORMAT(A.WeddingDt, '%d %M %Y'), '') As MaritalDt, Date_Format(A.LeaveStartDt,'%d %M %Y')As LeaveStartDt ");
                SQL1.AppendLine("From TblEmployee A ");
                SQL1.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
                SQL1.AppendLine("Left Join TblOption C On A.Religion = C.OptCode And C.OptCat = 'Religion' ");
                SQL1.AppendLine("Left Join TblOption D On A.MaritalStatus = D.OptCode And D.OptCat = 'MaritalStatus' ");
                SQL1.AppendLine("Left Join TblOption E On A.EmploymentStatus = E.OptCode And E.OptCat = 'EmploymentStatus' ");
                SQL1.AppendLine("Left Join TblOption F On A.Gender = F.OptCode And F.OptCat = 'Gender' ");
                SQL1.AppendLine("Where A.EmpCode = @EmpCode; ");

                cml1.CommandText = SQL1.ToString();

                Sm.CmParam<String>(ref cml1, "@EmpCode", TxtEmpCode.Text);

                var dr1 = cml1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                    {
                     //0
                     "BirthPlace" ,

                     //1-5
                     "BirthDt" ,
                     "Address",
                     "CityName",
                     "SubDistrict",
                     "Village",

                     //6-10
                     "RTRW",
                     "PostalCode",
                     "Religion",
                     "Gender",
                     "JoinDt",

                     //11-15
                     "ResignDt",
                     "EmploymentStatus", 
                     "MaritalStatus", 
                     "MaritalDt",
                     "LeaveStartDt"

                    });
                if (dr1.HasRows)
                {
                    int NomorBaris = 0;
                    while (dr1.Read())
                    {
                        NomorBaris = NomorBaris + 1;
                        l1.Add(new EmpGeneral()
                        {
                            BirthPlace = Sm.DrStr(dr1, c1[0]),
                            BirthDt = Sm.DrStr(dr1, c1[1]),
                            Address = Sm.DrStr(dr1, c1[2]),
                            CityName = Sm.DrStr(dr1, c1[3]),
                            SubDistrict = Sm.DrStr(dr1, c1[4]),
                            Village = Sm.DrStr(dr1, c1[5]),
                            RtRw = Sm.DrStr(dr1, c1[6]),
                            PostalCode = Sm.DrStr(dr1, c1[7]),
                            Religion = Sm.DrStr(dr1, c1[8]),
                            Gender = Sm.DrStr(dr1, c1[9]),
                            JoinDt = Sm.DrStr(dr1, c1[10]),
                            ResignDt = Sm.DrStr(dr1, c1[11]),
                            EmploymentStatus = Sm.DrStr(dr1, c1[12]),
                            MaritalStatus = Sm.DrStr(dr1, c1[13]),
                            MaritalDt = Sm.DrStr(dr1, c1[14]),
                            LeaveStartDt = Sm.DrStr(dr1, c1[15]),
                        });
                    }
                }
                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Personal

            var cml2 = new MySqlCommand();

            var SQL2 = new StringBuilder();
            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cml2.Connection = cn2;

                SQL2.AppendLine("Select A.FamilyName, B.OptDesc As Status, ");
                SQL2.AppendLine("Concat(Right(A.BirthDt, 2), ' ', ");
                SQL2.AppendLine("  case MONTH(A.BirthDt) ");
                SQL2.AppendLine("      when 1 then 'Januari' ");
                SQL2.AppendLine("      when 2 then 'Februari' ");
                SQL2.AppendLine("      when 3 then 'Maret' ");
                SQL2.AppendLine("      when 4 then 'April' ");
                SQL2.AppendLine("      when 5 then 'Mei' ");
                SQL2.AppendLine("      when 6 then 'Juni' ");
                SQL2.AppendLine("      when 7 then 'Juli' ");
                SQL2.AppendLine("      when 8 then 'Agustus' ");
                SQL2.AppendLine("      when 9 then 'September' ");
                SQL2.AppendLine("      when 10 then 'Oktober' ");
                SQL2.AppendLine("      when 11 then 'November' ");
                SQL2.AppendLine("      when 12 then 'Desember' ");
                SQL2.AppendLine("END, ' ', Left(A.BirthDt, 4))  As BirthDt, ");
                SQL2.AppendLine("E.OptDesc Gender, C.OptDesc AS Education, D.OptDesc AS Profession ");
                SQL2.AppendLine("From TblEmployeeFamily A ");
                SQL2.AppendLine("Left Join TblOption B On A.Status = B.OptCode And B.OptCat = 'FamilyStatus' ");
                SQL2.AppendLine("LEFT JOIN tbloption C ON A.EducationLevelCode = C.OptCode AND C.OptCat = 'EmployeeEducationLevel' ");
                SQL2.AppendLine("LEFT JOIN tbloption D ON A.ProfessionCode = D.OptCode AND D.OptCat = 'Profession' ");
                SQL2.AppendLine("LEFT JOIN tbloption E ON A.Gender = E.OptCode AND E.OptCat = 'Gender' ");
                SQL2.AppendLine("Where A.EmpCode = @EmpCode Order By A.DNo; ");

                cml2.CommandText = SQL2.ToString();

                Sm.CmParam<String>(ref cml2, "@EmpCode", TxtEmpCode.Text);

                var dr2 = cml2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0
                     "FamilyName",

                     //1-2
                     "Status", "BirthDt", "Gender", "Education", "Profession"
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new EmpPersonal()
                        {
                            FamilyName = Sm.DrStr(dr2, c2[0]),
                            Status = Sm.DrStr(dr2, c2[1]),
                            BirthDt = Sm.DrStr(dr2, c2[2]),
                            Gender = Sm.DrStr(dr2, c2[3]),
                            Education = Sm.DrStr(dr2, c2[4]),
                            Profession = Sm.DrStr(dr2, c2[5]),

                        });
                    }
                }
                else
                {
                    l2.Add(new EmpPersonal()
                    {
                        FamilyName = "",
                        Status = "",
                        BirthDt = "",
                        Gender = "",
                        Education = "",
                        Profession = "",

                    });
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            #region Work Experience

            var cml3 = new MySqlCommand();

            var SQL3 = new StringBuilder();
            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cml3.Connection = cn3;

                SQL3.AppendLine("Select IfNull(Company, '') As Company, IfNull(Position, '') As Position, IfNull(Period, '') As Period, IfNull(Remark, '') As Remark ");
                SQL3.AppendLine("From TblEmployeeWorkExp ");
                SQL3.AppendLine("Where EmpCode = @EmpCode Order By DNo; ");

                cml3.CommandText = SQL3.ToString();

                Sm.CmParam<String>(ref cml3, "@EmpCode", TxtEmpCode.Text);

                var dr3 = cml3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                    {
                     //0
                     "Company",

                     //1-3
                     "Position", "Period", "Remark"
                    });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new EmpWorkExperience()
                        {
                            Company = Sm.DrStr(dr3, c3[0]),
                            Position = Sm.DrStr(dr3, c3[1]),
                            Period = Sm.DrStr(dr3, c3[2]),
                            Remark = Sm.DrStr(dr3, c3[3])

                        });
                    }
                }
                else
                {
                    l3.Add(new EmpWorkExperience()
                    {
                        Company = "",
                        Position = "",
                        Period = ""

                    });
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            #region Education

            var cml4 = new MySqlCommand();

            var SQL4 = new StringBuilder();
            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cml4.Connection = cn4;

                SQL4.AppendLine("Select IfNull(A.School, '') As School, IfNull(B.OptDesc, A.Major) As Level, IfNull(D.MajorName, A.Field) As Major, IfNull(C.OptDesc, '') As Field, IfNull(A.GraduationYr, '') As GraduationYr ");
                SQL4.AppendLine("From TblEmployeeEducation A ");
                SQL4.AppendLine("Left Join TblOption B On A.Level = B.OptCode And B.OptCat = 'EmployeeEducationLevel' ");
                SQL4.AppendLine("Left Join TblOption C On A.Field = C.OptCode And C.OptCat = 'EmployeeEducationField' ");
                SQL4.AppendLine("Left Join TblMajor D On A.Major = D.MajorCode ");
                SQL4.AppendLine("Where A.EmpCode = @EmpCode Order By GraduationYr DESC; ");

                cml4.CommandText = SQL4.ToString();

                Sm.CmParam<String>(ref cml4, "@EmpCode", TxtEmpCode.Text);

                var dr4 = cml4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                    {
                     //0
                     "School",

                     //1-4
                     "Level", "Major", "Field", "GraduationYr"
                    });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new EmpEducation()
                        {
                            School = Sm.DrStr(dr4, c4[0]),
                            Level = Sm.DrStr(dr4, c4[1]),
                            Major = Sm.DrStr(dr4, c4[2]),
                            Field = Sm.DrStr(dr4, c4[3]),
                            GraduationYr = Sm.DrStr(dr4, c4[4])

                        });
                    }
                }
                else
                {
                    l4.Add(new EmpEducation()
                    {
                        School = "",
                        Level = "",
                        Major = "",
                        Field = "",
                        GraduationYr = "",

                    });
                }
                dr4.Close();
            }
            myLists.Add(l4);

            #endregion

            #region Competence By Employee's Position

            var cml5 = new MySqlCommand();

            var SQL5 = new StringBuilder();
            using (var cn5 = new MySqlConnection(Gv.ConnectionString))
            {
                cn5.Open();
                cml5.Connection = cn5;

                SQL5.AppendLine("Select A.CompetenceCode, C.CompetenceName, E.PosName, A.Description, D.MinScore, A.Score ");
                SQL5.AppendLine("From TblEmployeeCompetence A ");
                SQL5.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL5.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='2' ");
                SQL5.AppendLine("Inner Join TblOrganizationalStructureDtl4 D On A.CompetenceCode = D.CompetenceCode And B.PosCode = D.PosCode ");
                SQL5.AppendLine("Inner Join TblPosition E On D.PosCode = E.PosCode ");
                SQL5.AppendLine("Where A.EmpCode = @EmpCode Order By A.DNo; ");

                cml5.CommandText = SQL5.ToString();

                Sm.CmParam<String>(ref cml5, "@EmpCode", TxtEmpCode.Text);

                var dr5 = cml5.ExecuteReader();
                var c5 = Sm.GetOrdinal(dr5, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-5
                     "CompetenceName", "PosName", "Description", "MinScore", "Score"
                    });
                if (dr5.HasRows)
                {
                    while (dr5.Read())
                    {
                        l5.Add(new EmpCompetence()
                        {
                            CompetenceCode = Sm.DrStr(dr5, c5[0]),
                            CompetenceName = Sm.DrStr(dr5, c5[1]),
                            Position = Sm.DrStr(dr5, c5[2]),
                            Description = Sm.DrStr(dr5, c5[3]),
                            MinScore = Sm.DrDec(dr5, c5[4]),
                            Score = Sm.DrDec(dr5, c5[5])

                        });
                    }
                }
                else
                {
                    l5.Add(new EmpCompetence()
                    {
                        CompetenceCode = "",
                        CompetenceName = "Not Set",
                        Position = "",
                        Description = "",
                        MinScore = 1,
                        Score = 0m

                    });
                }
                dr5.Close();
            }
            myLists.Add(l5);

            #endregion

            #region Social Security

            var cml6 = new MySqlCommand();

            var SQL6 = new StringBuilder();
            using (var cn6 = new MySqlConnection(Gv.ConnectionString))
            {
                cn6.Open();
                cml6.Connection = cn6;

                SQL6.AppendLine("Select B.SSName, A.CardNo, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), If(IfNull(A.EndDt, '')='', '', ' - '), IfNull(DATE_FORMAT(A.EndDt, '%d %M %Y'), '')) As Period ");
                SQL6.AppendLine("From TblEmployeeSS A ");
                SQL6.AppendLine("Left Join TblSS B On A.SSCode=B.SSCode ");
                SQL6.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL6.AppendLine("Order By A.DNo;");

                cml6.CommandText = SQL6.ToString();

                Sm.CmParam<String>(ref cml6, "@EmpCode", TxtEmpCode.Text);

                var dr6 = cml6.ExecuteReader();
                var c6 = Sm.GetOrdinal(dr6, new string[] 
                    {
                     //0
                     "SSName",

                     //1-2
                     "CardNo", "Period"
                    });
                if (dr6.HasRows)
                {
                    while (dr6.Read())
                    {
                        l6.Add(new EmpSS()
                        {
                            SSName = Sm.DrStr(dr6, c6[0]),
                            SSCard = Sm.DrStr(dr6, c6[1]),
                            Period = Sm.DrStr(dr6, c6[2])
                        });
                    }
                }
                else
                {
                    l6.Add(new EmpSS()
                    {
                        SSName = "",
                        SSCard = "",
                        Period = ""
                    });
                }
                dr6.Close();
            }
            myLists.Add(l6);

            #endregion
            
            #region Personal References

            var cml7 = new MySqlCommand();

            var SQL7 = new StringBuilder();
            using (var cn7 = new MySqlConnection(Gv.ConnectionString))
            {
                cn7.Open();
                cml7.Connection = cn7;

                SQL7.AppendLine("Select IfNull(Name, '') As Name, IfNull(Phone, '') As Phone, IfNull(Address, '') As Address ");
                SQL7.AppendLine("From TblEmployeePersonalReference ");
                SQL7.AppendLine("Where EmpCode=@EmpCode ");
                SQL7.AppendLine("Order By DNo;");

                cml7.CommandText = SQL7.ToString();

                Sm.CmParam<String>(ref cml7, "@EmpCode", TxtEmpCode.Text);

                var dr7 = cml7.ExecuteReader();
                var c7 = Sm.GetOrdinal(dr7, new string[] 
                    {
                     //0
                     "Name",

                     //1-2
                     "Address", "Phone"
                    });
                if (dr7.HasRows)
                {
                    while (dr7.Read())
                    {
                        l7.Add(new EmpPersonalRef()
                        {
                            PRName = Sm.DrStr(dr7, c7[0]),
                            PRAddress = Sm.DrStr(dr7, c7[1]),
                            PRPhone = Sm.DrStr(dr7, c7[2])
                        });
                    }
                }
                else
                {
                    l7.Add(new EmpPersonalRef()
                    {
                        PRName = "",
                        PRAddress = "",
                        PRPhone = ""
                    });
                }
                dr7.Close();
            }
            myLists.Add(l7);

            #endregion
            
            #region Family Social Security

            var cml8 = new MySqlCommand();

            var SQL8 = new StringBuilder();
            using (var cn8 = new MySqlConnection(Gv.ConnectionString))
            {
                cn8.Open();
                cml8.Connection = cn8;

                SQL8.AppendLine("Select IfNull(A.FamilyName, '') As FamilyName, IfNull(B.OptDesc, '') as StatusDesc, ");
                SQL8.AppendLine("IfNull(D.SSName, '') As SSName, IfNull(A.CardNo, '') As CardNo, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), If(IfNull(A.EndDt, '')='', '', ' - '), IfNull(DATE_FORMAT(A.EndDt, '%d %M %Y'), '')) As Period ");
                SQL8.AppendLine("From TblEmployeeFamilySS A ");
                SQL8.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
                SQL8.AppendLine("Left Join TblSS D On A.SSCode=D.SSCode ");
                SQL8.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL8.AppendLine("Order By A.DNo;");

                cml8.CommandText = SQL8.ToString();

                Sm.CmParam<String>(ref cml8, "@EmpCode", TxtEmpCode.Text);

                var dr8 = cml8.ExecuteReader();
                var c8 = Sm.GetOrdinal(dr8, new string[] 
                    {
                     //0
                     "FamilyName",

                     //1-4
                     "StatusDesc", "SSName", "CardNo", "Period"
                    });
                if (dr8.HasRows)
                {
                    while (dr8.Read())
                    {
                        l8.Add(new EmpFamilySS()
                        {
                            FSSFamilyName = Sm.DrStr(dr8, c8[0]),
                            FSSStatus = Sm.DrStr(dr8, c8[1]),
                            FSSName = Sm.DrStr(dr8, c8[2]),
                            FSSCard = Sm.DrStr(dr8, c8[3]),
                            FSSPeriod = Sm.DrStr(dr8, c8[4])
                        });
                    }
                }
                else
                {
                    l8.Add(new EmpFamilySS()
                    {
                        FSSFamilyName = "",
                        FSSStatus = "",
                        FSSName = "",
                        FSSCard = "",
                        FSSPeriod = ""
                    });
                }
                dr8.Close();
            }
            myLists.Add(l8);

            #endregion

            #region Warning Letter / Infringement

            var cml9 = new MySqlCommand();

            var SQL9 = new StringBuilder();
            using (var cn9 = new MySqlConnection(Gv.ConnectionString))
            {
                cn9.Open();
                cml9.Connection = cn9;

                SQL9.AppendLine("Select C.WLName As TypeDesc, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), ' - ', DATE_FORMAT(A.EndDt, '%d %M %Y')) As Period ");
                SQL9.AppendLine("From TblEmpWL A ");
                SQL9.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
                SQL9.AppendLine("Inner Join TblWarningLetter C On A.WLCode = C.WLCode ");
                SQL9.AppendLine("Where A.EmpCode=@EmpCode And A.CancelInd = 'N';");

                cml9.CommandText = SQL9.ToString();

                Sm.CmParam<String>(ref cml9, "@EmpCode", TxtEmpCode.Text);

                var dr9 = cml9.ExecuteReader();
                var c9 = Sm.GetOrdinal(dr9, new string[] 
                    {
                     //0
                     "TypeDesc",

                     //1
                     "Period"
                    });
                if (dr9.HasRows)
                {
                    while (dr9.Read())
                    {
                        l9.Add(new EmpWL()
                        {
                            Type = Sm.DrStr(dr9, c9[0]),
                            Period = Sm.DrStr(dr9, c9[1])
                        });
                    }
                }
                else
                {
                    l9.Add(new EmpWL()
                    {
                        Type = "",
                        Period = ""
                    });
                }
                dr9.Close();
            }
            myLists.Add(l9);

            #endregion

            #region Position's History

            var cml10 = new MySqlCommand();

            var SQL10 = new StringBuilder();
            using (var cn10 = new MySqlConnection(Gv.ConnectionString))
            {
                cn10.Open();
                cml10.Connection = cn10;

                SQL10.AppendLine("Select E.OptDesc As Category, ");
                SQL10.AppendLine("Concat(IfNull(C1.DeptName, ''), If(IfNull(A.DeptCodeOld, '')='', '', ' - '), IfNull(D1.PosName, '')) As TransfFrom, ");
                SQL10.AppendLine("Concat(IfNull(C2.DeptName, ''), If(IfNull(A.DeptCodeNew, '')='', '', ' - '), IfNull(D2.POsName, '')) As TransfTo, ");
                SQL10.AppendLine("DATE_FORMAT(IfNull(A.StartDt, ''), '%d %M %Y') As StartDt ");
                SQL10.AppendLine("From TblPPS A ");
                SQL10.AppendLine("Inner Join TblEmployee B ON A.EmpCode = B.EmpCode ");
                SQL10.AppendLine("Left Join TblDepartment C1 On A.DeptCodeOld = C1.DeptCode ");
                SQL10.AppendLine("Left Join TblPosition D1 On A.PosCodeOld = D1.PosCode ");
                SQL10.AppendLine("Left Join TblDepartment C2 On A.DeptCodeNew = C2.DeptCode ");
                SQL10.AppendLine("Left Join TblPosition D2 On A.PosCodeNew = D2.PosCode ");
                SQL10.AppendLine("Inner Join TblOption E On A.JobTransfer = E.OptCode And E.OptCat = 'EmpJobTransfer' ");
                SQL10.AppendLine("Where ((A.DeptCodeOld <> A.DeptCodeNew) Or (A.PosCodeOld <> A.PosCodeNew)) ");
                SQL10.AppendLine("And A.EmpCode = @EmpCode and A.cancelind ='N' Order By A.DocNo; ");

                cml10.CommandText = SQL10.ToString();

                Sm.CmParam<String>(ref cml10, "@EmpCode", TxtEmpCode.Text);

                var dr10 = cml10.ExecuteReader();
                var c10 = Sm.GetOrdinal(dr10, new string[] 
                    {
                     //0
                     "Category",

                     //1-3
                     "TransfFrom", "TransfTo", "StartDt"
                    });
                if (dr10.HasRows)
                {
                    while (dr10.Read())
                    {
                        l10.Add(new EmpPPS()
                        {
                            Category = Sm.DrStr(dr10, c10[0]),
                            From = Sm.DrStr(dr10, c10[1]),
                            To = Sm.DrStr(dr10, c10[2]),
                            StartDt = Sm.DrStr(dr10, c10[3])
                        });
                    }
                }
                else
                {
                    l10.Add(new EmpPPS()
                    {
                        Category = "",
                        From = "",
                        To = "",
                        StartDt = ""
                    });
                }
                dr10.Close();
            }
            myLists.Add(l10);

            #endregion

            #region Competence Besides Employee's Position

            var cml11 = new MySqlCommand();

            var SQL11 = new StringBuilder();
            using (var cn11 = new MySqlConnection(Gv.ConnectionString))
            {
                cn11.Open();
                cml11.Connection = cn11;

                SQL11.AppendLine("Select A.CompetenceCode, C.CompetenceName, A.Description, A.Score ");
                SQL11.AppendLine("From TblEmployeeCompetence A ");
                SQL11.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL11.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='2' ");
                SQL11.AppendLine("Where A.EmpCode = @EmpCode ");
                SQL11.AppendLine("And A.CompetenceCode Not In ");
                SQL11.AppendLine("( ");
                SQL11.AppendLine("  Select T1.CompetenceCode ");
                SQL11.AppendLine("  From TblEmployeeCompetence T1 ");
                SQL11.AppendLine("  Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
                SQL11.AppendLine("  Inner Join TblOrganizationalStructureDtl4 T3 On T1.CompetenceCode = T3.CompetenceCode And T2.PosCode = T3.PosCode ");
                SQL11.AppendLine("  Where T1.EmpCode = @EmpCode ");
                SQL11.AppendLine(") ");
                SQL11.AppendLine("Order By A.DNo; ");

                cml11.CommandText = SQL11.ToString();

                Sm.CmParam<String>(ref cml11, "@EmpCode", TxtEmpCode.Text);

                var dr11 = cml11.ExecuteReader();
                var c11 = Sm.GetOrdinal(dr11, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-3
                     "CompetenceName", "Description", "Score"
                    });
                if (dr11.HasRows)
                {
                    while (dr11.Read())
                    {
                        l11.Add(new EmpCompetenceOther()
                        {
                            OCompetenceCode = Sm.DrStr(dr11, c11[0]),
                            OCompetenceName = Sm.DrStr(dr11, c11[1]),
                            ODescription = Sm.DrStr(dr11, c11[2]),
                            OScore = Sm.DrDec(dr11, c11[3])
                        });
                    }
                }
                else
                {
                    l11.Add(new EmpCompetenceOther()
                    {
                        OCompetenceCode = "",
                        OCompetenceName = "Not Set",
                        ODescription = "",
                        OMinScore = 1,
                        OScore = 0m
                    });
                }
                dr11.Close();
            }
            myLists.Add(l11);

            #endregion

            #region Potency By Employee's Position

            var cml12 = new MySqlCommand();

            var SQL12 = new StringBuilder();
            using (var cn12 = new MySqlConnection(Gv.ConnectionString))
            {
                cn12.Open();
                cml12.Connection = cn12;

                SQL12.AppendLine("Select A.CompetenceCode, C.CompetenceName, A.Description, A.Score ");
                SQL12.AppendLine("From TblEmployeeCompetence A ");
                SQL12.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL12.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='1' ");
                SQL12.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL12.AppendLine("Order By A.DNo; ");

                cml12.CommandText = SQL12.ToString();

                Sm.CmParam<String>(ref cml12, "@EmpCode", TxtEmpCode.Text);

                var dr12 = cml12.ExecuteReader();
                var c12 = Sm.GetOrdinal(dr12, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-3
                     "CompetenceName", "Description", "Score"
                    });
                if (dr12.HasRows)
                {
                    while (dr12.Read())
                    {
                        l12.Add(new EmpPotency()
                        {
                            PotencyCode = Sm.DrStr(dr12, c12[0]),
                            PotencyName = Sm.DrStr(dr12, c12[1]),
                            Description = Sm.DrStr(dr12, c12[2]),
                            Score = Sm.DrDec(dr12, c12[3])
                        });
                    }
                }
                else
                {
                    l12.Add(new EmpPotency()
                    {
                        PotencyCode = "",
                        PotencyName = "Not Set",
                        Description = "",
                        MinScore = 1,
                        Score = 0m
                    });
                }
                dr12.Close();
            }
            myLists.Add(l12);

            #endregion

            #region Training

            var cml13 = new MySqlCommand();

            var SQL13 = new StringBuilder();
            using (var cn13 = new MySqlConnection(Gv.ConnectionString))
            {
                cn13.Open();
                cml13.Connection = cn13;

                SQL13.AppendLine("Select B.TrainingName, A.Place, A.Yr, A.Remark  ");
                SQL13.AppendLine("From TblEmployeeTraining A ");
                SQL13.AppendLine("Left Join TblTraining B On A.TrainingCode = B.TrainingCode ");
                SQL13.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL13.AppendLine("Order By A.DNo;");

                cml13.CommandText = SQL13.ToString();

                Sm.CmParam<String>(ref cml13, "@EmpCode", TxtEmpCode.Text);

                var dr13 = cml13.ExecuteReader();
                var c13 = Sm.GetOrdinal(dr13, new string[] 
                    {
                     //0
                     "TrainingName",

                     //1-3
                     "Place", "Yr", "Remark"
                    });
                if (dr13.HasRows)
                {
                    while (dr13.Read())
                    {
                        l13.Add(new EmpTraining()
                        {
                            TrainingName = Sm.DrStr(dr13, c13[0]),
                            Place = Sm.DrStr(dr13, c13[1]),
                            MonthYear = Sm.DrStr(dr13, c13[2]),
                            Remark = Sm.DrStr(dr13, c13[3]),
                        });
                    }
                }
                else
                {
                    l13.Add(new EmpTraining()
                    {
                        TrainingName = "",
                        Place = "",
                        MonthYear = "",
                        Remark = ""
                    });
                }
                dr13.Close();
            }
            myLists.Add(l13);

            #endregion

            #region PHT

            if (mDocTitle == "PHT")
            {
                #region EmpPositionPHT

                var cml14 = new MySqlCommand();

                var SQL14 = new StringBuilder();
                using (var cn14 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn14.Open();
                    cml14.Connection = cn14;

                    SQL14.AppendLine("Select EmpCode, ");
                    SQL14.AppendLine("Concat(PosName, Case When PosDt Is Not Null Then    ");
                    SQL14.AppendLine("  Concat(' | ', PosDt, '| ', ");
                    SQL14.AppendLine("  TimeStampDiff(Year, PosDt, now()),' tahun, ', ");
                    SQL14.AppendLine("  TimeStampDiff(Month, PosDt, now() ) % 12,' bulan, ', ");
                    SQL14.AppendLine("  round(TimeStampDiff( DAY, PosDt, now() ) % 30.4375 ),' hari') ");
                    SQL14.AppendLine("Else '' End) As Jabatan,");
                    SQL14.AppendLine("Concat(GrdLvlName, ");
                    SQL14.AppendLine("  Case When GrdLvlDt Is Not Null Then ");
                    SQL14.AppendLine("  Concat('| ', GrdLvlDt, ' | ', ");
                    SQL14.AppendLine("  TimeStampDiff(Year, GrdLvlDt, now()),' tahun, ', ");
                    SQL14.AppendLine("  TimeStampDiff(Month, GrdLvlDt, now() ) % 12,' bulan, ', ");
                    SQL14.AppendLine("  round(TimeStampDiff( DAY, GrdLvlDt, now() ) % 30.4375 ),' hari') ");
                    SQL14.AppendLine("Else '' End) As Golongan, ");
                    SQL14.AppendLine("Concat(LevelName, ");
                    SQL14.AppendLine("  Case When LevelDt Is Not Null Then ");
                    SQL14.AppendLine("  Concat('| ', LevelDt, ' | ', ");
                    SQL14.AppendLine("  TimeStampDiff(Year, LevelDt, now()),' tahun, ', ");
                    SQL14.AppendLine("  TimeStampDiff(Month, LevelDt, now() ) % 12,' bulan, ', ");
                    SQL14.AppendLine("  round(TimeStampDiff( DAY, LevelDt, now() ) % 30.4375 ),' hari') ");
                    SQL14.AppendLine("  Else '' End) As Jenjang, ");
                    SQL14.AppendLine("SiteName, DivisionName, DeptName, SectionName, ");
                    SQL14.AppendLine("Concat(Right(JoinDt, 2), ' ', ");
                    SQL14.AppendLine("  case MONTH(JoinDt) ");
                    SQL14.AppendLine("      when 1 then 'Januari' ");
                    SQL14.AppendLine("      when 2 then 'Februari' ");
                    SQL14.AppendLine("      when 3 then 'Maret' ");
                    SQL14.AppendLine("      when 4 then 'April' ");
                    SQL14.AppendLine("      when 5 then 'Mei' ");
                    SQL14.AppendLine("      when 6 then 'Juni' ");
                    SQL14.AppendLine("      when 7 then 'Juli' ");
                    SQL14.AppendLine("      when 8 then 'Agustus' ");
                    SQL14.AppendLine("      when 9 then 'September' ");
                    SQL14.AppendLine("      when 10 then 'Oktober' ");
                    SQL14.AppendLine("      when 11 then 'November' ");
                    SQL14.AppendLine("      when 12 then 'Desember' ");
                    SQL14.AppendLine("END, ' ', Left(JoinDt, 4))  As JoinDt, ");
                    SQL14.AppendLine("Concat( ");
                    SQL14.AppendLine("  TimeStampDiff(Year, JoinDt, now()),' tahun, ', ");
                    SQL14.AppendLine("  TimeStampDiff(Month, JoinDt, now() ) % 12,' bulan, ', ");
                    SQL14.AppendLine("  floor(TimeStampDiff( DAY, JoinDt, now() ) % 30.4375 ),' hari') ");
                    SQL14.AppendLine("As MasaKerja, ");
                    SQL14.AppendLine("Case When PensiunDt Is Null Then Null Else Concat(Right(PensiunDt, 2), ' ', ");
                    SQL14.AppendLine("  case MONTH(PensiunDt) ");
                    SQL14.AppendLine("      when 1 then 'Januari' ");
                    SQL14.AppendLine("      when 2 then 'Februari' ");
                    SQL14.AppendLine("      when 3 then 'Maret' ");
                    SQL14.AppendLine("      when 4 then 'April' ");
                    SQL14.AppendLine("      when 5 then 'Mei' ");
                    SQL14.AppendLine("      when 6 then 'Juni' ");
                    SQL14.AppendLine("      when 7 then 'Juli' ");
                    SQL14.AppendLine("      when 8 then 'Agustus' ");
                    SQL14.AppendLine("      when 9 then 'September' ");
                    SQL14.AppendLine("      when 10 then 'Oktober' ");
                    SQL14.AppendLine("      when 11 then 'November' ");
                    SQL14.AppendLine("      when 12 then 'Desember' ");
                    SQL14.AppendLine("END, ' ', Left(PensiunDt, 4)) End As PensiunDt ");
                    SQL14.AppendLine("From ( ");
                    SQL14.AppendLine("  SELECT A.EmpCode, H.SiteName, A.JoinDt, I.DivisionName, J.DeptName, K.SectionName, ");
                    SQL14.AppendLine("  C.PosName, Concat(Left(B.Dt, 4), '-', Substring(B.Dt, 5, 2), '-', Right(B.Dt, 2)) As PosDt, ");
                    SQL14.AppendLine("  E.GrdLvlName, Concat(Left(D.Dt, 4), '-', Substring(D.Dt, 5, 2), '-', Right(D.Dt, 2)) As GrdLvlDt, ");
                    SQL14.AppendLine("  G.LevelName, Concat(Left(F.Dt, 4), '-', Substring(F.Dt, 5, 2), '-', Right(F.Dt, 2)) As LevelDt, ");
                    //SQL14.AppendLine("  Case When Exists(Select 1 from TblPPS Where EmpCode=A.EmpCode And JobTransfer='S' And CancelInd='N' And Status='A' Limit 1) Then A.ResignDt ");
                    //SQL14.AppendLine("  Else Null End As PensiunDt ");
                    SQL14.AppendLine("  case ");
                    SQL14.AppendLine("      when Right(A.BirthDt, 2)='01' then Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d') ");
                    SQL14.AppendLine("      ELSE concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01') ");
                    SQL14.AppendLine("  END AS PensiunDt ");
                    SQL14.AppendLine("  From TblEmployee A ");
                    SQL14.AppendLine("  Left Join ( ");
                    SQL14.AppendLine("      Select T1.EmpCode, T1.PosCodeTo As PosCode, Min(T1.DocDt) As Dt ");
                    SQL14.AppendLine("      From TblEmployeePositionHistory T1 ");
                    SQL14.AppendLine("      Left Join ( ");
                    SQL14.AppendLine("          Select X2.EmpCode, Max(X2.DocDt) Dt ");
                    SQL14.AppendLine("          From TblEmployee X1 ");
                    SQL14.AppendLine("          Inner Join TblEmployeePositionHistory X2 ");
                    SQL14.AppendLine("              On X1.EmpCode=X2.EmpCode ");
                    SQL14.AppendLine("              And X1.PosCode<>X2.PosCodeTo ");
                    SQL14.AppendLine("          Where X1.EmpCode=@EmpCode ");
                    SQL14.AppendLine("          Group By X2.EmpCode ");
                    SQL14.AppendLine("      ) T2 On T1.EmpCode=T2.EmpCode And T1.DocDt>T2.Dt");
                    SQL14.AppendLine("      Where T1.Empcode=@EmpCode ");
                    SQL14.AppendLine("      Group BY T1.EmpCode, T1.PosCodeTo ");
                    SQL14.AppendLine("  ) B On A.EmpCode=B.EmpCode And A.PosCode=B.PosCode ");
                    SQL14.AppendLine("  Left Join TblPosition C On A.PosCode=C.PosCode ");
                    SQL14.AppendLine("  Left Join ( ");
                    SQL14.AppendLine("      Select T1.EmpCode, T1.GrdLvlCode, Min(T1.DocDt) As Dt ");
                    SQL14.AppendLine("      From TblEmployeePositionHistory T1 ");
                    SQL14.AppendLine("      Left Join ( ");
                    SQL14.AppendLine("          Select X2.EmpCode, Max(X2.DocDt) Dt ");
                    SQL14.AppendLine("          From TblEmployee X1 ");
                    SQL14.AppendLine("          Inner Join TblEmployeePositionHistory X2 ");
                    SQL14.AppendLine("              On X1.EmpCode=X2.EmpCode ");
                    SQL14.AppendLine("              And X1.GrdLvlCode<>X2.GrdLvlCode ");
                    SQL14.AppendLine("          Where X1.EmpCode=@EmpCode ");
                    SQL14.AppendLine("          Group By X2.EmpCode ");
                    SQL14.AppendLine("      ) T2 On T1.EmpCode=T2.EmpCode And T1.DocDt>T2.Dt ");
                    SQL14.AppendLine("      Where T1.Empcode=@EmpCode ");
                    SQL14.AppendLine("      Group BY T1.EmpCode, T1.GrdLvlCode ");
                    SQL14.AppendLine("  ) D On A.EmpCode=D.EmpCode And A.GrdLvlCode=D.GrdLvlCode ");
                    SQL14.AppendLine("  Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");
                    SQL14.AppendLine("  Left Join ( ");
                    SQL14.AppendLine("      Select T1.EmpCode, T1.LevelCode, Min(T1.DocDt) As Dt ");
                    SQL14.AppendLine("      From TblEmployeePositionHistory T1 ");
                    SQL14.AppendLine("      Left Join ( ");
                    SQL14.AppendLine("          Select X2.EmpCode, Max(X2.DocDt) Dt ");
                    SQL14.AppendLine("          From TblEmployee X1 ");
                    SQL14.AppendLine("          Inner Join TblEmployeePositionHistory X2 ");
                    SQL14.AppendLine("              On X1.EmpCode=X2.EmpCode ");
                    SQL14.AppendLine("              And X1.LevelCode<>X2.LevelCode ");
                    SQL14.AppendLine("          Where X1.EmpCode=@EmpCode ");
                    SQL14.AppendLine("          Group By X2.EmpCode ");
                    SQL14.AppendLine("      ) T2 On T1.EmpCode=T2.EmpCode And T1.DocDt>T2.Dt ");
                    SQL14.AppendLine("      Where T1.Empcode=@EmpCode ");
                    SQL14.AppendLine("      Group BY T1.EmpCode, T1.LevelCode ");
                    SQL14.AppendLine("  ) F On A.EmpCode=F.EmpCode And A.LevelCode=F.LevelCode ");
                    SQL14.AppendLine("  Left Join TblLevelHdr G On A.LevelCode=G.LevelCode ");
                    SQL14.AppendLine("  Left Join TblSite H On A.SiteCode=H.SiteCode ");
                    SQL14.AppendLine("  LEFT JOIN tbldivision I ON A.DivisionCode = I.DivisionCode ");
                    SQL14.AppendLine("  LEFT JOIN tbldepartment J ON A.DeptCode = J.DeptCode ");
                    SQL14.AppendLine("  LEFT JOIN tblsection K ON A.SectionCode = K.SectionCode ");
                    SQL14.AppendLine("  Where A.Empcode=@EmpCode ");
                    SQL14.AppendLine(") Tbl; ");

                    cml14.CommandText = SQL14.ToString();

                    Sm.CmParam<String>(ref cml14, "@EmpCode", TxtEmpCode.Text);
                    Sm.CmParam<string>(ref cml14, "@SSRetiredMaxAge", mSSRetiredMaxAge2);

                    var dr14 = cml14.ExecuteReader();
                    var c14 = Sm.GetOrdinal(dr14, new string[]
                        {
                     //0
                     "EmpCode",

                     //1-5
                     "Jabatan", "Golongan", "Jenjang", "SiteName", "JoinDt",

                     //6-10
                     "MasaKerja", "PensiunDt", "DivisionName", "DeptName", "SectionName",
                        });
                    if (dr14.HasRows)
                    {
                        while (dr14.Read())
                        {
                            l14.Add(new EmpPositionPHT()
                            {
                                EmpCode = Sm.DrStr(dr14, c14[0]),

                                Jabatan = Sm.DrStr(dr14, c14[1]),
                                Golongan = Sm.DrStr(dr14, c14[2]),
                                Jenjang = Sm.DrStr(dr14, c14[3]),
                                SiteName = Sm.DrStr(dr14, c14[4]),
                                JoinDt = Sm.DrStr(dr14, c14[5]),

                                MasaKerja = Sm.DrStr(dr14, c14[6]),
                                PensiunDt = Sm.DrStr(dr14, c14[7]),
                                DivisioName = Sm.DrStr(dr14, c14[8]),
                                DeptName = Sm.DrStr(dr14, c14[9]),
                                SectionName = Sm.DrStr(dr14, c14[10]),
                            });
                        }
                    }
                    else
                    {
                        l14.Add(new EmpPositionPHT()
                        {
                            EmpCode = "",
                            Jabatan = "",
                            Golongan = "",
                            Jenjang = "",
                            SiteName = "",
                            JoinDt = "",
                            MasaKerja = "",
                            PensiunDt = "",
                            DivisioName = "",
                            DeptName = "",
                            SectionName = "",
                        });
                    }
                    dr14.Close();
                }
                myLists.Add(l14);

                #endregion

                #region EmpRiwayatPangkatPHT

                var cml15 = new MySqlCommand();

                var SQL15 = new StringBuilder();
                using (var cn15 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn15.Open();
                    cml15.Connection = cn15;

                    SQL15.AppendLine("Select ");
                    SQL15.AppendLine("Concat(Right(A.DocDt, 2), ' ', ");
                    SQL15.AppendLine("  case MONTH(A.DocDt) ");
                    SQL15.AppendLine("      when 1 then 'Januari' ");
                    SQL15.AppendLine("      when 2 then 'Februari' ");
                    SQL15.AppendLine("      when 3 then 'Maret' ");
                    SQL15.AppendLine("      when 4 then 'April' ");
                    SQL15.AppendLine("      when 5 then 'Mei' ");
                    SQL15.AppendLine("      when 6 then 'Juni' ");
                    SQL15.AppendLine("      when 7 then 'Juli' ");
                    SQL15.AppendLine("      when 8 then 'Agustus' ");
                    SQL15.AppendLine("      when 9 then 'September' ");
                    SQL15.AppendLine("      when 10 then 'Oktober' ");
                    SQL15.AppendLine("      when 11 then 'November' ");
                    SQL15.AppendLine("      when 12 then 'Desember' ");
                    SQL15.AppendLine("END, ' ', Left(A.DocDt, 4))  As Dt, ");
                    SQL15.AppendLine("A.DecisionLetter As Nomor, A.Duration As MasaKerja, B.GrdLvlName As Golongan, A.Amt As GajiPokok, ");
                    SQL15.AppendLine("A.Remark As Keterangan ");
                    SQL15.AppendLine("From TblEmployeeGradeHistory A ");
                    SQL15.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
                    SQL15.AppendLine("Where EmpCode=@EmpCode ORDER BY A.DocDt DESC; ");

                    cml15.CommandText = SQL15.ToString();

                    Sm.CmParam<String>(ref cml15, "@EmpCode", TxtEmpCode.Text);

                    var dr15 = cml15.ExecuteReader();
                    var c15 = Sm.GetOrdinal(dr15, new string[]
                        {
                     //0
                     "Dt",

                     //1-5
                     "Nomor", "MasaKerja", "Golongan", "GajiPokok", "Keterangan",
                        });
                    if (dr15.HasRows)
                    {
                        while (dr15.Read())
                        {
                            l15.Add(new EmpRiwayatPangkatPHT()
                            {
                                Dt = Sm.DrStr(dr15, c15[0]),
                                Nomor = Sm.DrStr(dr15, c15[1]),
                                MasaKerja = Sm.DrStr(dr15, c15[2]),
                                Golongan = Sm.DrStr(dr15, c15[3]),
                                GajiPokok = Sm.DrDec(dr15, c15[4]),
                                Keterangan = Sm.DrStr(dr15, c15[5]),

                            });
                        }
                    }
                    else
                    {
                        l15.Add(new EmpRiwayatPangkatPHT()
                        {
                            Dt = "",
                            Nomor = "",
                            MasaKerja = "",
                            Golongan = "",
                            GajiPokok = 0,
                            Keterangan = "",
                        });
                    }
                    dr15.Close();
                }
                myLists.Add(l15);

                #endregion

                #region EmpRiwayatJabatanPHT

                var cml16 = new MySqlCommand();

                var SQL16 = new StringBuilder();
                using (var cn16 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn16.Open();
                    cml16.Connection = cn16;

                    SQL16.AppendLine("Select  ");
                    SQL16.AppendLine("Concat(Right(A.DocDt, 2), ' ', ");
                    SQL16.AppendLine("  case MONTH(A.DocDt) ");
                    SQL16.AppendLine("      when 1 then 'Januari' ");
                    SQL16.AppendLine("      when 2 then 'Februari' ");
                    SQL16.AppendLine("      when 3 then 'Maret' ");
                    SQL16.AppendLine("      when 4 then 'April' ");
                    SQL16.AppendLine("      when 5 then 'Mei' ");
                    SQL16.AppendLine("      when 6 then 'Juni' ");
                    SQL16.AppendLine("      when 7 then 'Juli' ");
                    SQL16.AppendLine("      when 8 then 'Agustus' ");
                    SQL16.AppendLine("      when 9 then 'September' ");
                    SQL16.AppendLine("      when 10 then 'Oktober' ");
                    SQL16.AppendLine("      when 11 then 'November' ");
                    SQL16.AppendLine("      when 12 then 'Desember' ");
                    SQL16.AppendLine("END, ' ', Left(A.DocDt, 4))  As Dt, ");
                    SQL16.AppendLine("A.DecisionLetter As Nomor, B.OptDesc As Kategori, ifnull(C.PosName, A.PosNameFrom) As PosFrom, ifnull(D.PosName, A.PosNameTo) As PosTo, ifnull(E.GrdLvlName, A.GrdLvlName) As Golongan, ifnull(F.LevelName, A.LevelName) As Jenjang, ");
                    SQL16.AppendLine("Concat( ");
                    SQL16.AppendLine("Case When ifnull(I.DivisionName, A.DivisionName) Is Null Then '-' Else ifnull(I.DivisionName, A.DivisionName) End, ' | ', ");
                    SQL16.AppendLine("Case When ifnull(G.SiteName, A.SiteName) Is Null Then '-' Else ifnull(G.SiteName, A.SiteName) End, ' | ', ");
                    SQL16.AppendLine("Case When ifnull(H.DeptName, A.DeptName) Is Null Then '-' Else ifnull(H.DeptName, A.DeptName) End ) As Lokasi ");
                    SQL16.AppendLine("From TblEmployeePositionHistory A ");
                    SQL16.AppendLine("Left Join TblOption B On B.OptCat='EmpJobTransfer' And A.JobTransfer=B.OptCode ");
                    SQL16.AppendLine("Left Join TblPosition C On A.PosCodeFrom=C.PosCode ");
                    SQL16.AppendLine("Left Join TblPosition D On A.PosCodeTo=D.PosCode ");
                    SQL16.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");
                    SQL16.AppendLine("Left Join TblLevelHdr F On A.LevelCode=F.LevelCode ");
                    SQL16.AppendLine("Left Join TblSite G On A.SiteCode=G.SiteCode ");
                    SQL16.AppendLine("Left Join TblDepartment H On A.DeptCode=H.DeptCode ");
                    SQL16.AppendLine("Left Join TblDivision I On H.DivisionCode=I.DivisionCode ");
                    SQL16.AppendLine("Where EmpCode=@EmpCode And A.CancelInd = 'N' Order By A.DocDt DESC;");

                    cml16.CommandText = SQL16.ToString();

                    Sm.CmParam<String>(ref cml16, "@EmpCode", TxtEmpCode.Text);

                    var dr16 = cml16.ExecuteReader();
                    var c16 = Sm.GetOrdinal(dr16, new string[]
                        {
                     //0
                     "Dt",

                     //1-5
                     "Nomor", "Kategori", "PosFrom", "PosTo", "Golongan",

                     //6-7
                     "Jenjang", "Lokasi",
                        });
                    if (dr16.HasRows)
                    {
                        while (dr16.Read())
                        {
                            l16.Add(new EmpRiwayatJabatanPHT()
                            {
                                Dt = Sm.DrStr(dr16, c16[0]),
                                Nomor = Sm.DrStr(dr16, c16[1]),
                                Kategori = Sm.DrStr(dr16, c16[2]),
                                PosFrom = Sm.DrStr(dr16, c16[3]),
                                PosTo = Sm.DrStr(dr16, c16[4]),
                                Golongan = Sm.DrStr(dr16, c16[5]),
                                Jenjang = Sm.DrStr(dr16, c16[6]),
                                Lokasi = Sm.DrStr(dr16, c16[7]),
                            });
                        }
                    }
                    else
                    {
                        l16.Add(new EmpRiwayatJabatanPHT()
                        {
                            Dt = "",
                            Nomor = "",
                            Kategori = "",
                            PosFrom = "",
                            PosTo = "",
                            Golongan = "",
                            Jenjang = "",
                            Lokasi = "",
                        });
                    }
                    dr16.Close();
                }
                myLists.Add(l16);

                #endregion

                #region EmpKursusPusdikPHT

                var cml17 = new MySqlCommand();

                var SQL17 = new StringBuilder();
                using (var cn17 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn17.Open();
                    cml17.Connection = cn17;

                    SQL17.AppendLine("Select  ");
                    SQL17.AppendLine("C.OptDesc As Category, B.TrainingName As KegiatanKursus, A.Yr, ");
                    SQL17.AppendLine("A.Period As Angkatan, A.Value As Nilai, A.Ranking, A.Duration, A.Remark ");
                    SQL17.AppendLine("From TblEmployeeTraining2 A ");
                    SQL17.AppendLine("Left Join TblTraining B On A.TrainingCode=B.TrainingCode ");
                    SQL17.AppendLine("Left Join TblOption C On C.OptCat='EmployeeTrainingCategory' And A.Category=C.OptCode ");
                    SQL17.AppendLine("Where A.EmpCode=@EmpCode ");
                    SQL17.AppendLine("And A.EducationCenter='1'; ");

                    cml17.CommandText = SQL17.ToString();

                    Sm.CmParam<String>(ref cml17, "@EmpCode", TxtEmpCode.Text);

                    var dr17 = cml17.ExecuteReader();
                    var c17 = Sm.GetOrdinal(dr17, new string[]
                        {
                     //0
                     "Category",

                     //1-5
                     "KegiatanKursus", "Yr", "Angkatan", "Nilai", "Ranking",

                     //6-7
                     "Duration", "Remark",
                        });
                    if (dr17.HasRows)
                    {
                        while (dr17.Read())
                        {
                            l17.Add(new EmpKursusPusdikPHT()
                            {
                                Category = Sm.DrStr(dr17, c17[0]),
                                KegiatanKursus = Sm.DrStr(dr17, c17[1]),
                                Yr = Sm.DrStr(dr17, c17[2]),
                                Angkatan = Sm.DrDec(dr17, c17[3]),
                                Nilai = Sm.DrDec(dr17, c17[4]),
                                Ranking = Sm.DrDec(dr17, c17[5]),
                                Duration = Sm.DrStr(dr17, c17[6]),
                                Remark = Sm.DrStr(dr17, c17[7]),
                            });
                        }
                    }
                    else
                    {
                        l17.Add(new EmpKursusPusdikPHT()
                        {
                            Category = "",
                            KegiatanKursus = "",
                            Yr = "",
                            Angkatan = 0,
                            Nilai = 0,
                            Ranking = 0,
                            Duration = "",
                            Remark = "",
                        });
                    }
                    dr17.Close();
                }
                myLists.Add(l17);

                #endregion

                #region EmpKursusNonPusdikPHT

                var cml18 = new MySqlCommand();

                var SQL18 = new StringBuilder();
                using (var cn18 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn18.Open();
                    cml18.Connection = cn18;

                    SQL18.AppendLine("Select D.OptDesc As JenisKursus, C.OptDesc As BidangKursus, B.TrainingName As NamaKursus, A.Duration As LamaKursus, A.Yr, A.Remark ");
                    SQL18.AppendLine("From TblEmployeeTraining2 A ");
                    SQL18.AppendLine("Left Join TblTraining B On A.TrainingCode=B.TrainingCode ");
                    SQL18.AppendLine("Left Join TblOption C On C.OptCat='EmployeeTrainingSpecialization' And A.Specialization=C.OptCode ");
                    SQL18.AppendLine("Left Join TblOption D On D.OptCat='EmployeeTrainingType' And A.TrainingType=D.OptCode ");
                    SQL18.AppendLine("Where A.EmpCode=@EmpCode ");
                    SQL18.AppendLine("And A.EducationCenter='2'; ");

                    cml18.CommandText = SQL18.ToString();

                    Sm.CmParam<String>(ref cml18, "@EmpCode", TxtEmpCode.Text);

                    var dr18 = cml18.ExecuteReader();
                    var c18 = Sm.GetOrdinal(dr18, new string[]
                        {
                     //0
                     "JenisKursus",

                     //1-5
                     "BidangKursus", "NamaKursus", "LamaKursus", "Yr", "Remark",
                        });
                    if (dr18.HasRows)
                    {
                        while (dr18.Read())
                        {
                            l18.Add(new EmpKursusNonPusdikPHT()
                            {
                                JenisKursus = Sm.DrStr(dr18, c18[0]),
                                BidangKursus = Sm.DrStr(dr18, c18[1]),
                                NamaKursus = Sm.DrStr(dr18, c18[2]),
                                LamaKursus = Sm.DrStr(dr18, c18[3]),
                                Yr = Sm.DrStr(dr18, c18[4]),
                                Remark = Sm.DrStr(dr18, c18[5]),
                            });
                        }
                    }
                    else
                    {
                        l18.Add(new EmpKursusNonPusdikPHT()
                        {
                            JenisKursus = "",
                            BidangKursus = "",
                            NamaKursus = "",
                            LamaKursus = "",
                            Yr = "",
                            Remark = "",
                        });
                    }
                    dr18.Close();
                }
                myLists.Add(l18);

                #endregion

                #region EmpPenghargaanPHT

                var cml19 = new MySqlCommand();

                var SQL19 = new StringBuilder();
                using (var cn19 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn19.Open();
                    cml19.Connection = cn19;

                    SQL19.AppendLine("Select B.OptDesc As Kategori, A.Description, A.Yr ");
                    SQL19.AppendLine("From TblEmployeeAward A ");
                    SQL19.AppendLine("Left Join TblOption B On B.OptCat='EmployeeAwardCategory' And A.AwardCt=B.OptCode ");
                    SQL19.AppendLine("Where A.EmpCode=@EmpCode ");

                    cml19.CommandText = SQL19.ToString();

                    Sm.CmParam<String>(ref cml19, "@EmpCode", TxtEmpCode.Text);

                    var dr19 = cml19.ExecuteReader();
                    var c19 = Sm.GetOrdinal(dr19, new string[]
                        {
                     //0
                     "Kategori",

                     //1-5
                     "Description", "Yr",
                        });
                    if (dr19.HasRows)
                    {
                        while (dr19.Read())
                        {
                            l19.Add(new EmpPenghargaanPHT()
                            {
                                Kategori = Sm.DrStr(dr19, c19[0]),
                                Description = Sm.DrStr(dr19, c19[1]),
                                Yr = Sm.DrStr(dr19, c19[2]),
                            });
                        }
                    }
                    else
                    {
                        l19.Add(new EmpPenghargaanPHT()
                        {
                            Kategori = "",
                            Description = "",
                            Yr = "",
                        });
                    }
                    dr19.Close();
                }
                myLists.Add(l19);

                #endregion

                #region EmpHukumanPHT

                var cml20 = new MySqlCommand();

                var SQL20 = new StringBuilder();
                using (var cn20 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn20.Open();
                    cml20.Connection = cn20;

                    SQL20.AppendLine("Select A.ReferenceNo As Nomor, Left(A.DocDt, 4) As Yr, WLName As Name, C.OptDesc As Kategori ");
                    SQL20.AppendLine("From TblEmpWL A ");
                    SQL20.AppendLine("Inner Join TblWarningLetter B On A.WLCode=B.WLCode ");
                    SQL20.AppendLine("Left Join TblOption C On C.OptCat='EmpWLCt' And A.EmpWLCt=C.OptCode ");
                    SQL20.AppendLine("Where A.EmpCode=@EmpCode ");

                    cml20.CommandText = SQL20.ToString();

                    Sm.CmParam<String>(ref cml20, "@EmpCode", TxtEmpCode.Text);

                    var dr20 = cml20.ExecuteReader();
                    var c20 = Sm.GetOrdinal(dr20, new string[]
                        {
                     //0
                     "Nomor",

                     //1-5
                     "Yr", "Name", "Kategori",
                        });
                    if (dr20.HasRows)
                    {
                        while (dr20.Read())
                        {
                            l20.Add(new EmpHukumanPHT()
                            {
                                Nomor = Sm.DrStr(dr20, c20[0]),
                                Yr = Sm.DrStr(dr20, c20[1]),
                                Name = Sm.DrStr(dr20, c20[2]),
                                Kategori = Sm.DrStr(dr20, c20[3]),
                            });
                        }
                    }
                    else
                    {
                        l20.Add(new EmpHukumanPHT()
                        {
                            Nomor = "",
                            Yr = "",
                            Name = "",
                            Kategori = "",
                        });
                    }
                    dr20.Close();
                }
                myLists.Add(l20);

                #endregion

                #region EmpPiutangPHT

                var cml21 = new MySqlCommand();

                var SQL21 = new StringBuilder();
                using (var cn21 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn21.Open();
                    cml21.Connection = cn21;

                    SQL21.AppendLine("Select A.EmpCode, A.Yr, B.CreditName As Kategori, sum(A.Amt) As TotalAmount, ");
                    SQL21.AppendLine("sum(IfNull(C.Amt, 0.00)) As Installment, A.Amt-IfNull(C.Amt, 0.00) As RemainingLoan, ");
                    SQL21.AppendLine("A.Remark As Keterangan, Case When A.Amt-IfNull(C.Amt, 0.00)<=0.00 Then 'Lunas' Else 'Belum Lunas' End As Status ");
                    SQL21.AppendLine("From TblAdvancePaymentHdr A ");
                    SQL21.AppendLine("Left Join TblCredit B On A.CreditCode=B.CreditCode ");
                    SQL21.AppendLine("Left Join ( ");
                    SQL21.AppendLine("  Select DocNo, EmpCode, Sum(Amt) Amt, Mth ");
                    SQL21.AppendLine("  From TblAdvancePaymentProcess ");
                    SQL21.AppendLine("  Where EmpCode=@EmpCode ");
                    SQL21.AppendLine("  Group By DocNo, EmpCode ");
                    SQL21.AppendLine("  ) C On A.DocNo=C.DocNo And A.EmpCode=C.EmpCode ");
                    SQL21.AppendLine("Where A.Status='A' And A.CancelInd='N' And A.EmpCode=@EmpCode ");
                    //SQL21.AppendLine("AND C.Mth <= MONTH(CURDATE()) AND A.Yr <= YEAR(CURDATE()) ");
                    SQL21.AppendLine("AND CONCAT(A.Yr, C.Mth) <= concat(year(CURDATE()), MONTH(CURDATE())) ");
                    SQL21.AppendLine("GROUP BY A.EmpCode;");

                    cml21.CommandText = SQL21.ToString();

                    Sm.CmParam<String>(ref cml21, "@EmpCode", TxtEmpCode.Text);

                    var dr21 = cml21.ExecuteReader();
                    var c21 = Sm.GetOrdinal(dr21, new string[]
                        {
                     //0
                     "EmpCode",

                     //1-5
                     "Yr", "Kategori", "TotalAmount", "Installment", "RemainingLoan",

                     //
                     "Keterangan", "Status",
                        });
                    if (dr21.HasRows)
                    {
                        while (dr21.Read())
                        {
                            l21.Add(new EmpPiutangPHT()
                            {
                                EmpCode = Sm.DrStr(dr21, c21[0]),
                                Yr = Sm.DrStr(dr21, c21[1]),
                                Kategori = Sm.DrStr(dr21, c21[2]),
                                TotalAmt = Sm.DrDec(dr21, c21[3]),
                                Installment = Sm.DrDec(dr21, c21[4]),
                                RemainingLoan = Sm.DrDec(dr21, c21[5]),
                                Keterangan = Sm.DrStr(dr21, c21[6]),
                                Status = Sm.DrStr(dr21, c21[7]),
                            });
                        }
                    }
                    else
                    {
                        l21.Add(new EmpPiutangPHT()
                        {
                            EmpCode = "",
                            Yr = "",
                            Kategori = "",
                            TotalAmt = 0,
                            Installment = 0,
                            RemainingLoan = 0,
                            Keterangan = "",
                            Status = "",
                        });
                    }
                    dr21.Close();
                }
                myLists.Add(l21);

                #endregion
            }

            #endregion

            if (mDocTitle == "PHT")
                Sm.PrintReport("EmployeePHT", myLists, TableName, false);
            else if (mIsPrintoutMasterEmpNotUseDetailInformation)
                Sm.PrintReport("Employee3", myLists, TableName, false);
            else if (mIsEmployeePrintoutNotUseCompetenceAndPotency)
                Sm.PrintReport("Employee2", myLists, TableName, false);
            else
                Sm.PrintReport("Employee", myLists, TableName, false);
        }

        internal void CopyData(string EmpCode)
        {
            if (mUsePersonalInformation == 1)
                CopyData1(EmpCode);
            else
                CopyData2(EmpCode);
            try
            {
                ClearData();

                if (mUsePersonalInformation == 1)
                    ShowEmployee(EmpCode);
                else
                    ShowEmployee2(EmpCode);

                ShowEmployeeFamily(EmpCode);
                ShowEmployeeWorkExp(EmpCode);
                ShowEmployeeEducation(EmpCode);
                ShowEmployeeCompetence(EmpCode);
                if (mUsePersonalInformation == 1)
                {
                    ShowEmployeeSS(EmpCode);
                    ShowEmployeeFamilySS(EmpCode);
                }
                ShowEmployeePersonalReference(EmpCode);
                if (mEmployeeTrainingTabCode == "2")
                {
                    ShowEmployeeTraining2(EmpCode);
                    if(mUsePersonalInformation == 1)
                        ShowEmployeeTrainingEvaluation(EmpCode);
                }
                else
                    ShowEmployeeTraining(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void CopyData1(string EmpCode)
        {
            var DeptCode = string.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInCtrl(
                ref cm,
                "Select * From TblEmployee Where EmpCode=@EmpCode;",
                new string[] 
                {
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "DisplayName", "UserCode", "DeptCode", "PosCode",

                    //6-10
                    "JoinDt","ResignDt","IdNumber","Gender","Religion",

                    //11-15
                    "BirthPlace","BirthDt","Address","CityCode","PostalCode",

                    //16-20
                    "Phone","Mobile","GrdLvlCode","Email","NPWP",

                    //21-25
                    "PayrollType","PTKP","BankCode","BankBranch","BankAcName",

                    //26-30
                    "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode", "PayrunPeriod", 

                    //31-35
                    "EmploymentStatus","Mother", "SubDistrict", "Village", "RTRW", 

                    //36-40
                    "MaritalStatus","WeddingDt", "SystemType", "EntCode", "SiteCode", 
                    
                    //41-45
                    "Domicile", "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", 

                    //46-50
                    "PGCode", "POH", "TGDt", "ContractDt", "ClothesSize",

                    //51-52
                    "ShoeSize", "EducationType"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    DeptCode = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));
                    Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[7]));
                    TxtIdNumber.EditValue = Sm.DrStr(dr, c[8]);
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueReligion, Sm.DrStr(dr, c[10]));
                    TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                    MeeAddress.EditValue = Sm.DrStr(dr, c[13]);
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[14]));
                    TxtPostalCode.EditValue = Sm.DrStr(dr, c[15]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[18]));
                    TxtEmail.EditValue = Sm.DrStr(dr, c[19]);
                    TxtNPWP.EditValue = Sm.DrStr(dr, c[20]);
                    Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[21]));
                    Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[22]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[23]));
                    TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                    TxtBankAcName.EditValue = Sm.DrStr(dr, c[25]);
                    TxtBankAcNo.EditValue = Sm.DrStr(dr, c[26]);
                    TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[27]);
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[28]);
                    TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[29]);
                    Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[30]));
                    Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[31]));
                    TxtMother.EditValue = Sm.DrStr(dr, c[32]);
                    TxtSubDistrict.EditValue = Sm.DrStr(dr, c[33]);
                    TxtVillage.EditValue = Sm.DrStr(dr, c[34]);
                    TxtRTRW.EditValue = Sm.DrStr(dr, c[35]);
                    Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[36]));
                    Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[37]));
                    Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[38]));
                    Sm.SetLue(LueEntityCode, Sm.DrStr(dr, c[39]));
                    SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[40]), mIsFilterBySiteHR?"Y":"N");
                    MeeDomicile.EditValue = Sm.DrStr(dr, c[41]);
                    Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[42]));
                    Sm.SetLue(LueSection, Sm.DrStr(dr, c[43]));
                    Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[44]));
                    Sm.SetLue(LueDivisionCode, Sm.DrStr(dr, c[45]));
                    if (mIsDeptFilterByDivision)
                        SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N", Sm.GetLue(LueDivisionCode));
                    else
                        SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                    Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[46]));
                    TxtPOH.EditValue = Sm.DrStr(dr, c[47]);
                    Sm.SetDte(DteTGDt, Sm.DrStr(dr, c[48]));
                    Sm.SetDte(DteContractDt, Sm.DrStr(dr, c[49]));
                    Sm.SetLue(LueClothesSize, Sm.DrStr(dr, c[50]));
                    Sm.SetLue(LueShoeSize, Sm.DrStr(dr, c[51]));
                    Sm.SetLue(LueEducationType, Sm.DrStr(dr, c[52]));
                }, true
            );
        }

        private void CopyData2(string DocNo)
        {
            var DeptCode = string.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm,
                "Select * From TblEmployeeRecruitment Where DocNo=@DocNo;",
                new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "EmpName", "DisplayName", "UserCode", "DeptCode", "PosCode",

                    //6-10
                    "JoinDt","ResignDt","IdNumber","Gender","Religion",

                    //11-15
                    "BirthPlace","BirthDt","Address","CityCode","PostalCode",

                    //16-20
                    "Phone","Mobile","GrdLvlCode","Email","NPWP",

                    //21-25
                    "PayrollType","PTKP","BankCode","BankBranch","BankAcName",

                    //26-30
                    "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode", "PayrunPeriod", 

                    //31-35
                    "EmploymentStatus","Mother", "SubDistrict", "Village", "RTRW", 

                    //36-40
                    "MaritalStatus","WeddingDt", "SystemType", "SiteCode", "Domicile",
                    
                    //41-45
                     "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", "PGCode",

                    //46
                     "POH"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    DeptCode = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));
                    Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[7]));
                    TxtIdNumber.EditValue = Sm.DrStr(dr, c[8]);
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueReligion, Sm.DrStr(dr, c[10]));
                    TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                    MeeAddress.EditValue = Sm.DrStr(dr, c[13]);
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[14]));
                    TxtPostalCode.EditValue = Sm.DrStr(dr, c[15]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[18]));
                    TxtEmail.EditValue = Sm.DrStr(dr, c[19]);
                    TxtNPWP.EditValue = Sm.DrStr(dr, c[20]);
                    Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[21]));
                    Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[22]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[23]));
                    TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                    TxtBankAcName.EditValue = Sm.DrStr(dr, c[25]);
                    TxtBankAcNo.EditValue = Sm.DrStr(dr, c[26]);
                    TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[27]);
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[28]);
                    TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[29]);
                    Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[30]));
                    Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[31]));
                    TxtMother.EditValue = Sm.DrStr(dr, c[32]);
                    TxtSubDistrict.EditValue = Sm.DrStr(dr, c[33]);
                    TxtVillage.EditValue = Sm.DrStr(dr, c[34]);
                    TxtRTRW.EditValue = Sm.DrStr(dr, c[35]);
                    Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[36]));
                    Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[37]));
                    Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[38]));
                    SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[39]), mIsFilterBySiteHR ? "Y" : "N");
                    MeeDomicile.EditValue = Sm.DrStr(dr, c[40]);
                    Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[41]));
                    Sm.SetLue(LueSection, Sm.DrStr(dr, c[42]));
                    Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[43]));
                    Sm.SetLue(LueDivisionCode, Sm.DrStr(dr, c[44]));
                    if (mIsDeptFilterByDivision)
                        SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N", Sm.GetLue(LueDivisionCode));
                    else
                        SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                    Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[45]));
                }, true
            );
        }

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, string nameTab)
        {
            downloadedData = new byte[0];

            string FileSharedTemp = FileShared;
          
            if (mDocTitle == "PHT")
            {
                if (nameTab == "workexp")
                    FileShared = FileShared + "/work-exp";
                if (nameTab == "education")
                    FileShared = FileShared + "/education";
                if (nameTab == "training")
                    FileShared = FileShared + "/training";
                if (nameTab == "competence")
                    FileShared = FileShared + "/competence";
                if (nameTab == "award")
                    FileShared = FileShared + "/award";
                if (nameTab == "vaccine")
                    FileShared = FileShared + "/vaccine";
                if (nameTab == "position")
                    FileShared = FileShared + "/position";
                if (nameTab == "grade")
                    FileShared = FileShared + "/grade";
            }

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                     request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/"  + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                FileShared = FileSharedTemp;
                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName, string tipe, string nameTab)
        {
            string SharedFolderForFTPClientTemp = mSharedFolderForFTPClient;
            if (tipe == "1")
            {
                if (IsUploadFileNotValid(Row, FileName)) return;
            }


            if(mDocTitle == "PHT")
            {
                if (nameTab == "workexp")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/work-exp";
                if (nameTab == "education")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/education";
                if (nameTab == "training")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/training";
                if (nameTab == "competence")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/competence";
                if (nameTab == "award")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/award";
                if (nameTab == "vaccine")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/vaccine";
                if (nameTab == "position")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/position";
                if (nameTab == "grade")
                    mSharedFolderForFTPClient = mSharedFolderForFTPClient + "/grade";
            }

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                    request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                    request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);

            
            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            if(tipe == "1")
            {
                cml.Add(UpdateEmployeeFile(EmpCode, Row, toUpload.Name));
            }
            else
            {
                cml.Add(UpdateEmployeeFile2(EmpCode, Row, toUpload.Name, nameTab));
            }

            Sm.ExecCommands(cml);
            mSharedFolderForFTPClient = SharedFolderForFTPClientTemp;
        }

        private void UploadFileGrid(iGrid grd, int colFileName, int colFileName2, string tabName)
        {
            
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < grd.Rows.Count - 1; Row++)
            {
                if (IsUploadFileNotValid2(Row, Sm.GetGrdStr(grd, Row, colFileName), grd, colFileName, colFileName2, tabName)) return;

                if (Sm.GetGrdStr(grd, Row, colFileName).Length > 0)
                {
                    if (mIsEmployeeAllowToUploadFile && Sm.GetGrdStr(grd, Row, colFileName).Length > 0 
                        && Sm.GetGrdStr(grd, Row, colFileName) != "openFileDialog1"
                        && (Sm.GetGrdStr(grd, Row, colFileName) != Sm.GetGrdStr(grd, Row, colFileName2)))
                    {
                            UploadFile(TxtEmpCode.Text, Row, Sm.GetGrdStr(grd, Row, colFileName), "2", tabName);
                    }
                }
            }
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }
        private bool IsUploadFileNotValid2(int Row, string FileName, iGrid Grid, int ColFile1, int ColFile2, string tabName)
        {
            return
                IsFileNameAlreadyExisted2(Row, FileName, Grid, ColFile1, ColFile2, tabName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd11, Row, 1) != Sm.GetGrdStr(Grd11, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblEmployeeFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2(int Row, string FileName, iGrid Grd, int ColFile1, int ColFile2, string nameTab)
        {
            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd, Row, ColFile1) != Sm.GetGrdStr(Grd, Row, ColFile2))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                
                SQL.AppendLine("Select FileName From  ");
                if (nameTab == "workexp")
                    SQL.AppendLine("tblemployeeworkexp ");
                if (nameTab == "education")
                    SQL.AppendLine("tblemployeeeducation ");
                if (nameTab == "training")
                    SQL.AppendLine("tblemployeetraining2 ");
                if (nameTab == "competence")
                    SQL.AppendLine("tblemployeecompetence ");
                if (nameTab == "award")
                    SQL.AppendLine("tblemployeeaward ");
                if (nameTab == "vaccine")
                    SQL.AppendLine("tblemployeevaccine ");
                if (nameTab == "position")
                    SQL.AppendLine("tblemployeepositionhistory ");
                if (nameTab == "grade")
                    SQL.AppendLine("tblemployeegradehistory ");
                if (nameTab == "competence")
                    SQL.AppendLine("tblemployeecompetence ");
                
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LblGradeHistoryRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "Employee's code", false)) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int r = 0;

            SQL.AppendLine("Select A.DNo, A.DocDt, A.DecisionLetter, A.Duration, B.GrdLvlName, A.Remark, A.PeriodicSettingInd, ");
            SQL.AppendLine("A.GrdLvlCode, A.Amt, A.FileName ");
            SQL.AppendLine("From TblEmployeeGradeHistory A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode Order By A.DocDt; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.ShowDataInGrid(
                    ref Grd17, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "DNo",
                        "DocDt", "DecisionLetter", "Duration", "GrdLvlName", "Remark",
                        "PeriodicSettingInd", "GrdLvlCode", "Amt", "FileName"                   },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        r++;
                        Grd.Cells[Row, 0].Value = r;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
						Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd17, 0, 0);

            if(BtnSave.Enabled && !mIsInsert && mIsUsePeriodicAdvancementMenu)
            {
                for (int i = 0; i < Grd17.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdBool(Grd17, i, 6))
                    {
                        Grd17.Cells[i, 2].ReadOnly = iGBool.False;
                        Grd17.Cells[i, 5].ReadOnly = iGBool.False;
                    }
                }
            }
        }

        private void LblPositionHistoryRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "Employee's code", false)) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int r = 0;

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.PPSInd, A.DecisionLetter, I.OptDesc As JobTransferDesc, ");
            SQL.AppendLine("ifnull(B.PosName, A.PosNameFrom) As PosNameFrom, Ifnull(C.PosName, A.PosNameTo) As PosNameTo, ifnull(D.GrdLvlName, A.GrdLvlName) As GrdLvlName, ");
            SQL.AppendLine("ifnull(E.LevelName, A.LevelName) As LevelName, ifnull(F.DeptName, A.DeptName) As DeptName, ifnull(G.SiteName, A.SiteName) As SiteName, ifnull(H.DivisionName, A.DivisionName) As DivisionName, A.FileName ");
            SQL.AppendLine("From TblEmployeePositionHistory A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCodeFrom=B.PosCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCodeTo=C.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr E On A.LevelCode=E.LevelCode ");
            SQL.AppendLine("Left Join TblDepartment F On A.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblSite G On A.SiteCode=G.SiteCode ");
            SQL.AppendLine("Left Join TblDivision H On F.DivisionCode=H.DivisionCode ");
            SQL.AppendLine("Left Join TblOption I On A.JobTransfer=I.OptCode And I.OptCat='EmpJobTransfer' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode And A.CancelInd='N' Order By A.DocDt; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.ShowDataInGrid(
                    ref Grd15, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "DocNo",
                        "CancelInd", "PPSInd", "DocDt", "DecisionLetter", "JobTransferDesc", 
                        "PosNameFrom", "PosNameTo", "GrdLvlName", "LevelName", "DeptName", 
                        "SiteName", "DivisionName", "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        r++;
                        Grd.Cells[Row, 0].Value = r;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd15, 0, 0);
        }

        private void LblWarningLetterRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "Employee's code", false)) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int r = 0;
            if (mIsEmpWLAllowToEditFile)
            {
                SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ReferenceNo, LEFT(A.DocDt, 4) AS Yr, C.WlName, D.OptDesc AS EmpWLCtDesc, B.FileName ");
                SQL.AppendLine("FROM tblempwl A ");
                SQL.AppendLine("INNER JOIN tblempwldtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("LEFT JOIN tblwarningletter C ON A.WLCode=C.WlCode ");
                SQL.AppendLine("LEFT JOIN tbloption D ON A.EmpWLCt=D.OptCode AND D.OptCat='EmpWLCt' ");
                SQL.AppendLine("Where A.EmpCode=@EmpCode Order By A.DocDt; ");
            }
            else
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.ReferenceNo, Left(A.DocDt, 4) As Yr, B.WLName, C.OptDesc As EmpWLCtDesc, A.FileName ");
                SQL.AppendLine("From TblEmpWL A ");
                SQL.AppendLine("Inner Join TblWarningLetter B On A.WLCode=B.WLCode ");
                SQL.AppendLine("Left Join TblOption C On A.EmpWLCt=C.OptCode And C.OptCat='EmpWLCt' ");
                SQL.AppendLine("Where A.EmpCode=@EmpCode Order By A.DocDt; ");
            }

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.ShowDataInGrid(
                    ref Grd13, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "DocNo", 
                        "DocDt", "ReferenceNo", "Yr", "WLName", "EmpWLCtDesc",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        r++;
                        Grd.Cells[Row, 0].Value = r;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 6);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd13, 0, 0);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtEmpCode);
        }

        private void TxtEmpName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtEmpName);
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtUserCode);
        }

        private void TxtBirthPlace_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBirthPlace);
        }

        private void TxtPostalCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPostalCode);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPhone);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMobile);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtEmail);
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsEmployeeUseGradeSalary)
                    Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue1(Sl.SetLueGradeSalary));
                else
                    Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
            }
        }
       
        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue1(Sl.SetLueReligion));
        }

        private void LueCity_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCity, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void LuePayrollType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayrollType, new Sm.RefreshLue1(Sl.SetLueEmployeePayrollType));
        }

        private void LueMaritalStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueMaritalStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaritalStatus");
                BtnSpouseEmpCode.Enabled = false;
                TxtSpouseEmpCode.EditValue = null;
                
                if (mIsMaritalStatusMandatory)
                {
                    string SpouseCode = Sm.GetValue("Select OptCode From TblOption Where OptCat = 'MaritalStatus' And OptCode = '5'; ");
                    if (SpouseCode.Length > 0)
                    {
                        if (Sm.GetLue(LueMaritalStatus) == SpouseCode)
                        {
                            BtnSpouseEmpCode.Enabled = true;
                        }
                    }
                }
            }
        }

        private void LuePTKP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePTKP, new Sm.RefreshLue1(Sl.SetLuePTKP));
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(Sl.SetLueFamilyStatus));
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueStatus.Visible && fAccept && fCell.ColIndex == 3)
                {
                    if (Sm.GetLue(LueStatus).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 2].Value =
                        Grd1.Cells[fCell.RowIndex, 3].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueStatus);
                        Grd1.Cells[fCell.RowIndex, 3].Value = LueStatus.GetColumnValue("Col2");
                    }
                    LueStatus.Visible = false;
                }
            }
        }

        private void LueProfessionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProfessionCode, new Sm.RefreshLue2(Sl.SetLueOption), "Profession");
        }

        private void LueProfessionCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueProfessionCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueProfessionCode.Visible && fAccept && fCell.ColIndex == 11)
                {
                    if (Sm.GetLue(LueProfessionCode).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 10].Value =
                        Grd1.Cells[fCell.RowIndex, 11].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 10].Value = Sm.GetLue(LueProfessionCode);
                        Grd1.Cells[fCell.RowIndex, 11].Value = LueProfessionCode.GetColumnValue("Col2");
                    }
                    LueProfessionCode.Visible = false;
                }
            }
        }

        private void LueEducationLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEducationLevelCode, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeEducationLevel");
        }

        private void LueEducationLevelCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEducationLevelCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEducationLevelCode.Visible && fAccept && fCell.ColIndex == 13)
                {
                    if (Sm.GetLue(LueEducationLevelCode).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 12].Value =
                        Grd1.Cells[fCell.RowIndex, 13].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LueEducationLevelCode);
                        Grd1.Cells[fCell.RowIndex, 13].Value = LueEducationLevelCode.GetColumnValue("Col2");
                    }
                    LueEducationLevelCode.Visible = false;
                }
            }
        }

        private void LueFamGender_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFamGender, new Sm.RefreshLue1(Sl.SetLueGender));

        }

        private void LueFamGender_Leave(object sender, EventArgs e)
        {
            if (LueFamGender.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueFamGender).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueFamGender);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueFamGender.GetColumnValue("Col2");
                }
                LueFamGender.Visible = false;
            }
        }

        private void LueFamGender_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void DteFamBirthDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteFamBirthDt_Validated(object sender, EventArgs e)
        {
            DteFamBirthDt.Visible = false;
        }

        private void DteFamBirthDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteFamBirthDt, ref fCell, ref fAccept);
        }       
      
        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationLevel));
        }

        private void LueLevel_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueLevel_Leave(object sender, EventArgs e)
        {
            if (LueLevel.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueLevel.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueLevel).Trim();
                    Grd3.Cells[fCell.RowIndex, 3].Value = LueLevel.GetColumnValue("Col2");
                }
            }    
        }

        private void LueLevel_Validated(object sender, EventArgs e)
        {
            LueLevel.Visible = false;
        }

        private void TxtBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankBranch);
        }

        private void TxtBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankAcNo);
        }

        private void TxtBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankAcName);
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtShortCode);
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void TxtSubDistrict_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSubDistrict);
        }

        private void TxtVillage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVillage);
        }

        private void TxtRTRW_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtRTRW);
        }

        private void TxtMother_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMother);
        }

        private void LueBloodType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBloodType, new Sm.RefreshLue2(Sl.SetLueOption), "BloodType");
        }

        private void FrmEmployee_Activated(object sender, EventArgs e)
        {
            TxtEmpCode.Focus();
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(SetLueSection));
                if (Sm.GetLue(LueSection).Length > 0)
                {
                    SetLueWorkGroup(ref LueWorkGroup, Sm.GetLue(LueSection));
                }
            }
        }

        private void LueWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWorkGroup, new Sm.RefreshLue2(SetLueWorkGroup), Sm.GetLue(LueSection));
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        private void LuePositionStatusCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePositionStatusCode, new Sm.RefreshLue1(Sl.SetLuePositionStatusCode));
        }

        private void LueEntityCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntityCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue1(SetLueTrainingCode));                
            }
        }
        private void LueTrainingCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTrainingCode2, new Sm.RefreshLue1(SetLueTrainingCode));
            }
        }
        private void LueMajor_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueMajor, new Sm.RefreshLue1(Sl.SetLueMajorCode));
        }

        private void LueField_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueField, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationField));
        }

        private void LueMajor_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueField_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueMajor_Leave(object sender, EventArgs e)
        {
            if (LueMajor.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueMajor.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueMajor).Trim();
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueMajor.GetColumnValue("Col2");
                }
            }
        }

        private void LueField_Leave(object sender, EventArgs e)
        {
            if (LueField.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (LueField.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueField).Trim();
                    Grd3.Cells[fCell.RowIndex, 7].Value = LueField.GetColumnValue("Col2");
                }
            }
        }

        private void LueMajor_Validated(object sender, EventArgs e)
        {
            LueMajor.Visible = false;
        }

        private void LueField_Validated(object sender, EventArgs e)
        {
            LueField.Visible = false;
        }

        private void LueFacCode_Validated(object sender, EventArgs e)
        {
            LueFacCode.Visible = false;
        }

        private void LueFacCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueFacCode_Leave(object sender, EventArgs e)
        {
            if (LueFacCode.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueFacCode).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 11].Value =
                    Grd3.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 11].Value = Sm.GetLue(LueFacCode);
                    Grd3.Cells[fCell.RowIndex, 12].Value = LueFacCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueFacCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFacCode, new Sm.RefreshLue2(Sl.SetLueFacCode), string.Empty);
        }

        private void LueLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLevelCode, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void LueAwardCt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAwardCt, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeAwardCategory");
        }

        private void LueAwardCt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueAwardCt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueAwardCt.Visible && fAccept && fCell.ColIndex == 2)
                {
                    if (Sm.GetLue(LueAwardCt).Length == 0)
                        Grd14.Cells[fCell.RowIndex, 1].Value =
                        Grd14.Cells[fCell.RowIndex, 2].Value = null;
                    else
                    {
                        Grd14.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueAwardCt);
                        Grd14.Cells[fCell.RowIndex, 2].Value = LueAwardCt.GetColumnValue("Col2");
                    }
                    LueAwardCt.Visible = false;
                }
            }
        }

        private void LueTrainingType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingType, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeTrainingType");
        }

        private void LueTrainingType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueTrainingType_Leave(object sender, EventArgs e)
        {
            if (LueTrainingType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueTrainingType).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 3].Value =
                    Grd16.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueTrainingType);
                    Grd16.Cells[fCell.RowIndex, 4].Value = LueTrainingType.GetColumnValue("Col2");
                }
                LueTrainingType.Visible = false;
            }
        }

        private void LueTrainingCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void LueTrainingCode_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode).Length == 0)
                    Grd8.Cells[fCell.RowIndex, 1].Value =
                    Grd8.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode);
                    Grd8.Cells[fCell.RowIndex, 2].Value = LueTrainingCode.GetColumnValue("Col2");
                }
                LueTrainingCode.Visible = false;
            }
        }

        private void LueTrainingCode2_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode2.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode2).Length == 0)
                    Grd10.Cells[fCell.RowIndex, 1].Value =
                    Grd10.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode2);
                    Grd10.Cells[fCell.RowIndex, 2].Value = LueTrainingCode2.GetColumnValue("Col2");
                }
                LueTrainingCode2.Visible = false;
            }
        }

        private void LueTrainingCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingCode3, new Sm.RefreshLue1(SetLueTrainingCode));
        }

        private void LueTrainingCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueTrainingCode3_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode3.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode3).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 1].Value =
                    Grd16.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode3);
                    Grd16.Cells[fCell.RowIndex, 2].Value = LueTrainingCode3.GetColumnValue("Col2");
                }
                LueTrainingCode3.Visible = false;
            }
        }

        private void LueTrainingSpecialization_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingSpecialization, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeTrainingSpecialization");
        }

        private void LueTrainingSpecialization_Leave(object sender, EventArgs e)
        {
            if (LueTrainingSpecialization.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueTrainingSpecialization).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 5].Value =
                    Grd16.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueTrainingSpecialization);
                    Grd16.Cells[fCell.RowIndex, 6].Value = LueTrainingSpecialization.GetColumnValue("Col2");
                }
                LueTrainingSpecialization.Visible = false;
            }
        }

        private void LueTrainingSpecialization_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueTrainingEducationCenter_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingEducationCenter, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeTrainingEducationCente");
        }

        private void LueTrainingEducationCenter_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueTrainingEducationCenter_Leave(object sender, EventArgs e)
        {
            if (LueTrainingEducationCenter.Visible && fAccept && fCell.ColIndex == 16)
            {
                if (Sm.GetLue(LueTrainingEducationCenter).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 15].Value =
                    Grd16.Cells[fCell.RowIndex, 16].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 15].Value = Sm.GetLue(LueTrainingEducationCenter);
                    Grd16.Cells[fCell.RowIndex, 16].Value = LueTrainingEducationCenter.GetColumnValue("Col2");
                }
                LueTrainingEducationCenter.Visible = false;
            }
        }

        private void LueUploadFileCategory_EditValueChanged(object sender, EventArgs e)
        {
             if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueUploadFileCategory, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeFileUploadCategory");
        }

        private void LueUploadFileCategory_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void Grd2_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 7)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 14)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 15)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd16_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 20)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 7)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd15_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 14)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 15)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd17_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 10)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 11)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd13_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 10)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd14_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 7)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd12_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 6)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

       

        private void LueUploadFileCategory_Leave(object sender, EventArgs e)
        {
            if (LueUploadFileCategory.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueUploadFileCategory).Length == 0)
                    Grd11.Cells[fCell.RowIndex, 5].Value =
                    Grd11.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUploadFileCategory);
                    Grd11.Cells[fCell.RowIndex, 6].Value = LueUploadFileCategory.GetColumnValue("Col2");
                }
                LueUploadFileCategory.Visible = false;
            }
        }

     

        private void LueTrainingCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingCategory, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeTrainingCategory");
        }

       
        private void LueTrainingCategory_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        
        private void LueTrainingCategory_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCategory.Visible && fAccept && fCell.ColIndex == 17)
            {
                if (Sm.GetLue(LueTrainingCategory).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 7].Value =
                    Grd16.Cells[fCell.RowIndex, 17].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueTrainingCategory);
                    Grd16.Cells[fCell.RowIndex, 17].Value = LueTrainingCategory.GetColumnValue("Col2");
                }
                LueTrainingCategory.Visible = false;
            }
        }

        
        private void LueTrainingCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

       
        private void LueClothesSize_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueClothesSize, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeClothesSize");
        }

      
        private void LueShoeSize_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShoeSize, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeShoeSize");
        }

        
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsSiteDivisionEnabled)
                {
                    var SiteCode = Sm.GetLue(LueSiteCode);
                    LueDivisionCode.EditValue = null;
                    if (SiteCode.Length == 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDivisionCode }, true);
                        SetLueDivisionCodeBasedOnSite(ref LueDivisionCode, "***", string.Empty);
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDivisionCode }, false);
                        SetLueDivisionCodeBasedOnSite(ref LueDivisionCode, SiteCode, string.Empty);
                    }

                    LueDeptCode.EditValue = null;
                    if (mIsDivisionDepartmentEnabled)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, true);
                    }
                    LuePosCode.EditValue = null;
                    if (mIsDepartmentPositionEnabled)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, true);
                    }
                }
            }
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsSiteDivisionEnabled)
                {
                    Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue3(SetLueDivisionCodeBasedOnSite),Sm.GetLue(LueSiteCode), string.Empty);
                }
                else
                {
                    Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
                }
                if (mIsDivisionDepartmentEnabled)
                {
                    var DivisionCode = Sm.GetLue(LueDivisionCode);
                    LueDeptCode.EditValue = null;
                    if (DivisionCode.Length == 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, true);
                        SetLueDeptCodeBasedOnDivision(ref LueDeptCode, "***", string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, false);
                        SetLueDeptCodeBasedOnDivision(ref LueDeptCode, DivisionCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                    }
                    LuePosCode.EditValue = null;
                    if (mIsDepartmentPositionEnabled)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, true);
                    }
                }
                else
                {
                    if (mIsDeptFilterByDivision) SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N", Sm.GetLue(LueDivisionCode));                
                }
            }
        }

        private void LueDeptCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDivisionDepartmentEnabled)
                {
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue4(SetLueDeptCodeBasedOnDivision), Sm.GetLue(LueDivisionCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                }
                else
                {
                    if (!mIsDeptFilterByDivision)
                    {
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                    }
                    else
                    {
                        Sm.RefreshLookUpEdit(LueDeptCode,
                            new Sm.RefreshLue4(SetLueDeptCode),
                                string.Empty,
                                mIsFilterByDeptHR ? "Y" : "N",
                                Sm.GetLue(LueDivisionCode)
                            );
                    }
                }
                if (mIsDepartmentPositionEnabled)
                {
                    var DeptCode = Sm.GetLue(LueDeptCode);
                    LuePosCode.EditValue = null;
                    if (DeptCode.Length == 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, true);
                        SetLuePosCodeBasedOnDepartment(ref LuePosCode, "***", string.Empty);
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePosCode }, false);
                        SetLuePosCodeBasedOnDepartment(ref LuePosCode, DeptCode, string.Empty);
                    }
                }
            }



            //if (BtnSave.Enabled)
            //{
            //    if (!mIsDeptFilterByDivision)
            //    {
            //        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            //    }
            //    else
            //    {
            //        Sm.RefreshLookUpEdit(LueDeptCode,
            //            new Sm.RefreshLue4(SetLueDeptCode),
            //                string.Empty,
            //                mIsFilterByDeptHR ? "Y" : "N",
            //                Sm.GetLue(LueDivisionCode)
            //            );
            //    }
            //}
        }

        private void LuePosCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDepartmentPositionEnabled)
                {
                    Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue3(SetLuePosCodeBasedOnDepartment), Sm.GetLue(LueDeptCode), string.Empty);
                    string ActingOfficialInd = Sm.GetValue("Select ActingOfficialInd From TblPosition Where PosCode = @Param", Sm.GetLue(LuePosCode));
                    ChkActingOfficial.Checked = Sm.CompareStr(ActingOfficialInd, "Y");
                }
                else
                {
                    Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
                    string ActingOfficialInd = Sm.GetValue("Select ActingOfficialInd From TblPosition Where PosCode = @Param", Sm.GetLue(LuePosCode));
                    ChkActingOfficial.Checked = Sm.CompareStr(ActingOfficialInd, "Y");
                }
            }
            //if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueEducationType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEducationType, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeEducationType");
        }

        private void LueCostGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCostGroup, new Sm.RefreshLue2(Sl.SetLueOption), "EmpCostGroup");
        }

        private void DteVaccineDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd12, ref fAccept, e);
        }

        private void DteVaccineDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteVaccineDt, ref fCell, ref fAccept);
        }

        private void DteVaccineDt_Validated(object sender, EventArgs e)
        {
            DteVaccineDt.Visible = false;
        }

        private void LueBloodRhesus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBloodRhesus, new Sm.RefreshLue2(Sl.SetLueOption), "BloodRhesusType");
        }

        private void MeeAddress_EditValueChanged(object sender, EventArgs e)
        {
            if (MeeAddress.IsModified && MeeAddress.Text != string.Empty)
            {
                if (Sm.StdMsgYN("Question",
                       "Do you want to copy address to domicile ?",
                       mMenuCode) == DialogResult.Yes)
                {
                    MeeDomicile.Text = MeeAddress.Text;
                }
                else
                {
                    MeeAddress.Focus();
                    return;
                }
            }
        }

        private void MeeAddress_Validated(object sender, EventArgs e)
        {
           
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 3, 5, 6, 7, 8, 9, 11, 13 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.LueRequestEdit(ref Grd1, ref LueStatus, ref fCell, ref fAccept, e);
                if (e.ColIndex == 5) Sm.LueRequestEdit(ref Grd1, ref LueFamGender, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd1, DteFamBirthDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 11) Sm.LueRequestEdit(ref Grd1, ref LueProfessionCode, ref fCell, ref fAccept, e);
                if (e.ColIndex == 13) Sm.LueRequestEdit(ref Grd1, ref LueEducationLevelCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 7, 8, 9 }, e);
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 5).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 5), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "workexp");
                    SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 5);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 5, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 6)
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd2.Cells[e.RowIndex, 5].Value = OD.FileName;
                }
            }
        }

        #endregion

        #region Grid 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 3, 5, 7, 8, 9, 10, 12 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                //Untuk input combobox dalam grid
                if (e.ColIndex == 3)
                {
                    Sm.LueRequestEdit(ref Grd3, ref LueLevel, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 5)
                {
                    if (!mIsEmployeeEducationInputManualData) Sm.LueRequestEdit(ref Grd3, ref LueMajor, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 7)
                {
                    if (!mIsEmployeeEducationInputManualData) Sm.LueRequestEdit(ref Grd3, ref LueField, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 12)
                {
                    if (!mIsEmployeeEducationInputManualData) Sm.LueRequestEdit(ref Grd3, ref LueFacCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 15)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 13).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 13), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "education");
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 13);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 13, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 14)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 13].Value = OD.FileName;
                }
            }
        }
        #endregion

        #region Grid 4

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
            {
                if (e.ColIndex == 2) LueRequestEdit(Grd4, LueCompetenceCode, ref fCell, ref fAccept, e);
                //if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
                //{
                //    LueRequestEdit(Grd4, LueCompetenceCode, ref fCell, ref fAccept, e);
                //    SetLueCompetenceCode(ref LueCompetenceCode);
                //}
                
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (mIsInsert)
                    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 4 });
                else
                {
                    if ((Sm.GetGrdDec(Grd4, Grd4.Rows.Count - 2, 4) == 0) || (Sm.GetGrdDec(Grd4, Grd4.Rows.Count - 2, 4).ToString().Length == 0))
                        Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 2, new int[] { 4 });

                    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd4, new int[] { 4 }, e);
        }

        private void LueCompetenceCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceCode, new Sm.RefreshLue1(SetLueCompetenceCode));
        }

        private void LueCompetenceCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueCompetenceCode_Leave(object sender, EventArgs e)
        {
            if (LueCompetenceCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueCompetenceCode).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 1].Value =
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueCompetenceCode);
                    Grd4.Cells[fCell.RowIndex, 2].Value = LueCompetenceCode.GetColumnValue("Col2");
                }
                LueCompetenceCode.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd4, (fCell.RowIndex + 1), new int[] { 4 });
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 5).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 5), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "competence");
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 5);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 5, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 6)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 5].Value = OD.FileName;
                }
            }
        }


        #endregion

        #region Grid 7

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            Sm.GrdEnter(Grd7, e);
            Sm.GrdTabInLastCell(Grd7, e, BtnFind, BtnSave);
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
        }

        #endregion

        #region Grid 8

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd8, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd8, LueTrainingCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
                SetLueTrainingCode(ref LueTrainingCode);
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
            Sm.GrdEnter(Grd8, e);
            Sm.GrdTabInLastCell(Grd8, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 10
        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd10, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd10, LueTrainingCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
                SetLueTrainingCode(ref LueTrainingCode2);
            }
        }

        private void Grd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd10, e, BtnSave);
            Sm.GrdEnter(Grd10, e);
            Sm.GrdTabInLastCell(Grd10, e, BtnFind, BtnSave);
        }
        #endregion

        #region Grid 11

        private void Grd11_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd11, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd11, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "");
                    SFD.FileName = Sm.GetGrdStr(Grd11, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd11, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd11, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd11.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) Sm.LueRequestEdit(ref Grd11, ref LueUploadFileCategory, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
                if (Sm.GetGrdStr(Grd11, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd11, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "");
                    SFD.FileName = Sm.GetGrdStr(Grd11, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd11, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd11_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
            Sm.GrdEnter(Grd11, e);
            Sm.GrdTabInLastCell(Grd11, e, BtnFind, BtnSave);
        }
       
        #endregion

        #region Grid 12

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex))
            {
                if (e.ColIndex == 1) Sm.DteRequestEdit(Grd12, DteVaccineDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
            Sm.GrdKeyDown(Grd12, e, BtnFind, BtnSave);
        }

        private void Grd12_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd12, new int[] { 2, 3 }, e);
        }

        private void Grd12_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                if (Sm.GetGrdStr(Grd12, e.RowIndex, 4).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd12, e.RowIndex, 4), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "vaccine");
                    SFD.FileName = Sm.GetGrdStr(Grd12, e.RowIndex, 4);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd12, e.RowIndex, 4, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 5)
                {
                    Sm.GrdRequestEdit(Grd12, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd12.Cells[e.RowIndex, 4].Value = OD.FileName;
                }
            }
        }


        #endregion

        #region Grid 13

        private void Grd13_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd13, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmpWL(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd13, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10)
            {
                if (Sm.GetGrdStr(Grd13, e.RowIndex, 8).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd13, e.RowIndex, 8), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "warning");
                    SFD.FileName = Sm.GetGrdStr(Grd13, e.RowIndex,8);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd13, e.RowIndex, 8, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {
                // upload file WL di menu WL saja
                //if (e.ColIndex == 9)
                //{
                //    Sm.GrdRequestEdit(Grd13, e.RowIndex);
                //    OD.InitialDirectory = "c:";
                //    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                //                "|PDF files (*.pdf)|*.pdf" +
                //                "|Word files (*.doc;*docx)|*.doc;*docx" +
                //                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                //                "|Text files (*.txt)|*.txt";
                //    OD.FilterIndex = 2;
                //    OD.ShowDialog();
                //    Grd13.Cells[e.RowIndex, 8].Value = OD.FileName;
                //}
            }
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd13, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmpWL(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd13, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid 14
        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 3, 4 }, e.ColIndex))
            {
                if (e.ColIndex == 2) Sm.LueRequestEdit(ref Grd14, ref LueAwardCt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
            Sm.GrdKeyDown(Grd14, e, BtnFind, BtnSave);
        }

        private void Grd14_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd14, new int[] { 3, 4 }, e);
        }

        private void Grd14_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7)
            {
                if (Sm.GetGrdStr(Grd14, e.RowIndex, 5).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd14, e.RowIndex, 5), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "award");
                    SFD.FileName = Sm.GetGrdStr(Grd14, e.RowIndex, 5);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd14, e.RowIndex, 5, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 6)
                {
                    Sm.GrdRequestEdit(Grd14, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd14.Cells[e.RowIndex, 5].Value = OD.FileName;
                }
            }
        }



        #endregion

        #region  Grid 15
        private void Grd15_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 15)
            {
                if (Sm.GetGrdStr(Grd15, e.RowIndex, 13).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd15, e.RowIndex, 13), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "position");
                    SFD.FileName = Sm.GetGrdStr(Grd15, e.RowIndex, 13);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd15, e.RowIndex,13, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 14)
                {
                    Sm.GrdRequestEdit(Grd15, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd15.Cells[e.RowIndex, 13].Value = OD.FileName;
                }
            }
        }
        #endregion

        #region Grid 16
        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 4, 6, 8, 9, 10, 11, 12, 13, 14, 16, 17 }, e.ColIndex))
            {
                if (e.ColIndex == 2) Sm.LueRequestEdit(ref Grd16, ref LueTrainingCode3, ref fCell, ref fAccept, e);
                if (e.ColIndex == 4) Sm.LueRequestEdit(ref Grd16, ref LueTrainingType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) Sm.LueRequestEdit(ref Grd16, ref LueTrainingSpecialization, ref fCell, ref fAccept, e);
                if (e.ColIndex == 17) LueRequestEdit(ref Grd16, ref LueTrainingCategory, ref fCell, ref fAccept, e, 7);
                if (e.ColIndex == 16) Sm.LueRequestEdit(ref Grd16, ref LueTrainingEducationCenter, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
            Sm.GrdKeyDown(Grd16, e, BtnFind, BtnSave);
        }

        private void Grd16_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd16, new int[] { 7, 8, 9, 10, 11, 12, 13, 14 }, e);
        }


        private void Grd16_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 20)
            {
                if (Sm.GetGrdStr(Grd16, e.RowIndex, 18).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd16, e.RowIndex, 18), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "training");
                    SFD.FileName = Sm.GetGrdStr(Grd16, e.RowIndex, 18);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd16, e.RowIndex, 18, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 19)
                {
                    Sm.GrdRequestEdit(Grd16, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd16.Cells[e.RowIndex, 18].Value = OD.FileName;
                }
            }
        }

        #endregion

        #region Grid 17
        private void Grd17_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd17, e.RowIndex, 9).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd17, e.RowIndex, 9), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, "grade");
                    SFD.FileName = Sm.GetGrdStr(Grd17, e.RowIndex, 9);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd17, e.RowIndex, 9, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 10)
                {
                    Sm.GrdRequestEdit(Grd17, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd17.Cells[e.RowIndex, 9].Value = OD.FileName;
                }
            }
        }

        #endregion

        #endregion

        #region Button Event

        private void BtnPicture_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false)) return;

            try
            {
                string DBServer = Sm.GetParameter("DBServer");
                string location = Sm.GetParameter("ImgFileEmployee");
                if (DBServer.Length != 0 && location.Length != 0 && Sm.CompareStr(DBServer, Gv.Server))
                {
                    if (Directory.Exists(location))
                    {
                        string ImageFile = String.Concat(location, "\\", TxtEmpCode.Text);
                        foreach (string fe in new string[11] 
                                { ".jpg", ".png", ".bmp", ".jpeg", ".gif", ".dib", 
                                  ".rle", ".jpe", ".jfif", ".tiff", ".tif" })
                        {
                            if (File.Exists(String.Concat(ImageFile, fe)))
                            {
                                PicEmployee.Image = Image.FromFile(String.Concat(ImageFile, fe));
                                mProfilePicture = String.Concat(location, TxtEmpCode.Text, fe);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCopyGeneral_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmployeeDlg2(this));
        }

        private void BtnSpouseEmpCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmEmployeeDlg3(this));
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            ImageFormat format = ImageFormat.Png;
            sfd.FileName = TxtEmpCode.Text;
            if (PicEmployee.Image != null && sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                PicEmployee.Image.Save(sfd.FileName, format);
            }
        }

        

        #endregion

        #endregion

    }

    #region Report Class

    class EmpHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyFax { get; set; }
        public string CompLocation2 { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }
        public string Shipper3 { get; set; }
        public string Shipper4 { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string DeptName { get; set; }
        public string PosName { get; set; }
        public string Printby { get; set; }
        public string ProfilePicture { get; set; }
        public string EmpCodeOld { get; set; }
        public string IDNumber { get; set; }
        public string NPWP { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

    }

    class EmpGeneral
    {
        public string BirthPlace { get; set; }
        public string BirthDt { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string SubDistrict { get; set; }
        public string Village { get; set; }
        public string RtRw { get; set; }
        public string PostalCode { get; set; }
        public string Religion { get; set; }
        public string Gender { get; set; }
        public string JoinDt { get; set; }
        public string ResignDt { get; set; }
        public string EmploymentStatus { get; set; }
        public string MaritalStatus { get; set; }
        public string MaritalDt { get; set; }
        public string LeaveStartDt { get; set; } 
    }

    class EmpPersonal
    {
        public string FamilyName { get; set; }
        public string Status { get; set; }
        public string BirthDt { get; set; }
        public string Gender { get; set; }
        public string Education { get; set; }
        public string Profession { get; set; }
    }

    class EmpWorkExperience
    {
        public string Company { get; set; }
        public string Position { get; set; }
        public string Period { get; set; }
        public string Remark { get; set; }
    }

    class EmpEducation
    {
        public string School { get; set; }
        public string Level { get; set; }
        public string Major { get; set; }
        public string Field { get; set; }
        public string GraduationYr { get; set; }
    }

    class EmpCompetence
    {
        public string CompetenceCode { get; set; }
        public string CompetenceName { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public decimal MinScore { get; set; }
        public decimal Score { get; set; }
    }

    class EmpSS
    {
        public string SSName { get; set; }
        public string SSCard { get; set; }
        public string Period { get; set; }
    }

    class EmpPersonalRef
    {
        public string PRName { get; set; }
        public string PRPhone { get; set; }
        public string PRAddress { get; set; }
    }

    class EmpFamilySS
    {
        public string FSSFamilyName { get; set; }
        public string FSSStatus { get; set; }
        public string FSSName { get; set; }
        public string FSSCard { get; set; }
        public string FSSPeriod { get; set; }
    }

    class EmpTraining
    {
        public string TrainingName { get; set; }
        public string Place { get; set; }
        public string MonthYear { get; set; }
        public string Remark { get; set; }
    }

    class EmpWL
    {
        public string Type { get; set; }
        public string Period { get; set; }
    }

    class EmpPPS
    {
        public string Category { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string StartDt { get; set; }
    }

    class EmpCompetenceOther
    {
        public string OCompetenceCode { get; set; }
        public string OCompetenceName { get; set; }
        public string ODescription { get; set; }
        public decimal OMinScore { get; set; }
        public decimal OScore { get; set; }
    }

    class EmpPotency
    {
        public string PotencyCode { get; set; }
        public string PotencyName { get; set; }
        public string Description { get; set; }
        public decimal MinScore { get; set; }
        public decimal Score { get; set; }
    }

    class EmpPositionPHT
    {
        public string EmpCode { get; set; }
        public string Jabatan { get; set; }
        public string Golongan { get; set; }
        public string Jenjang { get; set; }
        public string SiteName { get; set; }
        public string JoinDt { get; set; }
        public string MasaKerja { get; set; }
        public string PensiunDt { get; set; }
        public string DivisioName { get; set; }
        public string DeptName { get; set; }
        public string SectionName { get; set; }

    }

    class EmpRiwayatPangkatPHT
    {
        public string Dt { get; set; }
        public string Nomor { get; set; }
        public string MasaKerja { get; set; }
        public string Golongan { get; set; }
        public decimal GajiPokok { get; set; }
        public string Keterangan { get; set; }
    }

    class EmpRiwayatJabatanPHT
    {
        public string Dt { get; set; }
        public string Nomor { get; set; }
        public string Kategori { get; set; }
        public string PosFrom { get; set; }
        public string PosTo { get; set; }
        public string Golongan { get; set; }
        public string Jenjang { get; set; }
        public string Lokasi { get; set; }
    }

    class EmpKursusPusdikPHT
    {
        public string Category { get; set; }
        public string KegiatanKursus { get; set; }
        public string Yr { get; set; }
        public decimal Angkatan { get; set; }
        public decimal Nilai { get; set; }
        public decimal Ranking { get; set; }
        public string Duration { get; set; }
        public string Remark { get; set; }
    }

    class EmpKursusNonPusdikPHT
    {
        public string JenisKursus { get; set; }
        public string BidangKursus { get; set; }
        public string NamaKursus { get; set; }
        public string LamaKursus { get; set; }
        public string Yr { get; set; }
        public string Remark { get; set; }
    }

    class EmpPenghargaanPHT
    {
        public string Kategori { get; set; }
        public string Description { get; set; }
        public string Yr { get; set; }
    }

    class EmpHukumanPHT
    {
        public string Nomor { get; set; }
        public string Yr { get; set; }
        public string Name { get; set; }
        public string Kategori { get; set; }
    }

    class EmpPiutangPHT
    {
        public string EmpCode { get; set; }
        public string Yr { get; set; }
        public string Kategori { get; set; }
        public decimal TotalAmt { get; set; }
        public decimal Installment { get; set; }
        public decimal RemainingLoan { get; set; }
        public string Keterangan { get; set; }
        public string Status { get; set; }
    }

    #endregion

}
