﻿#region Update
/*
    19/04/2018 [TKG] bug tampilan showdata ke-double-an karena customer item.
    22/05/2018 [WED] 
            tambah kolom Volume (dari volume item packaging unit * qty packaging SO), 
            packaging qty (dari SO), 
            dan packaging uom (dari SO dan inner join ke item packaging unit)
            berdasarkan parameter IsPPShowPackagingValue
    23/05/2018 [ARI] Print out PP tambah kolom Volume, Packaging Qty, dan Packaging UoM
    10/10/2018 [HAR] bug PP double
    16/10/2018 [HAR] bug PP double di print out
    10/04/2019 [DITA] PP Remark Mandatory, berdasarkan parameter
    06/01/2019 [HAR/MAI] BUG printout jumlah per item group tidak sesuai
    29/01/2019 [TKG/MMM] customized document number
    25/08/2020 [ICA/MGI] Tambah Field Group parameter IsUseProductionWorkGroup
    14/01/2021 [WED/KSM] tambah tarik dari Sales Contract berdasarkan parameter IsSalesContractEnabled
    16/03/2022 [TKG/GSS] merubah GetParameter dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mInformationForItemColour = string.Empty,
            mInformationForItemArtwork = string.Empty,
            mInformationForItemPackaging = string.Empty;
        internal bool 
            mIsPPShowPackagingValue = false,
            mIsPPRemarkMandatory = false,
            mIsUseProductionWorkGroup = false,
            mIsSalesContractEnabled = false
            ;
        private bool mIsPPCustomDocNoEnabled = false;
        internal FrmPPFind FrmFind;

        #endregion

        #region Constructor

        public FrmPP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Planning";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);

                SetLueStatus(ref LueStatus);
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                if (mIsPPRemarkMandatory) LblRemark.ForeColor = Color.Red;
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                if (!mIsUseProductionWorkGroup) panel4.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Document#",
                        "",
                        "Type",
                        "Date",
                        
                        //6-10
                        "Usage Date",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Quantity",

                        //11-15
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Specification",

                        //16-19
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Volume",
                        "Qty",
                        "Packaging UoM"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 150, 20, 0, 80, 
 
                        //6-10
                        80, 80, 20, 250, 100,
 
                        //11-15
                        80, 100, 80, 200, 130,

                        //16-19
                        130, 100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 17, 18 }, 0);
            Grd1.Cols[15].Move(10);
            Grd1.Cols[16].Move(11);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 7, 8, 15, 16 }, false);

            if (!mIsPPShowPackagingValue)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, LueStatus, ChkCancelInd, MeeRemark, LueProductionWorkGroup }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark, LueProductionWorkGroup }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueStatus, ChkCancelInd }, false);
                    LueStatus.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueStatus, MeeRemark, LueProductionWorkGroup
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 12, 17, 18 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, "P");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            
            if (mIsPPCustomDocNoEnabled)
                DocNo = GenerateDocNoMMM();
            else
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PP", "TblPPHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePPHdr(DocNo));
            cml.Add(SavePPDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePPDtl(DocNo, Row));

            cml.Add(SavePPDtl2(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private string GenerateDocNoMMM()
        {
            // 0014/ATT/PPIC/04/20
            string DocDt = Sm.GetDte(DteDocDt);
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var SQL = new StringBuilder();
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='PP';"),
                ItScCode = Sm.GetValue("Select ItScCode From TblItem Where ItScCode Is Not Null And ItCode=@Param;", Sm.GetGrdStr(Grd1, 0, 7)),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            if (ItScCode.Length>0) ItScCode = string.Concat(", '/', '", ItScCode, "'");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") ");
            //SQL.Append("    Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) ");
            SQL.Append("    From ( ");
            SQL.Append("       Select Convert(Left(DocNo, "+DocSeqNo+"), Decimal) As DocNo ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo ");
            SQL.Append("       From TblPPHdr ");
            SQL.Append("       Where Right(DocNo, 2)='" + Yr + "' ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            //SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("    ), '0001') ");
            SQL.Append(ItScCode);
            SQL.Append(", '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo; ");

            return Sm.GetValue(SQL.ToString());
        }


        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                (mIsPPRemarkMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group"))||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsProductionOrderAlreadyCancelled() ||
                IsProductionOrderAlreadyFulfilled() ||
                IsJCAlreadyCancelled() ||
                IsJCAlreadyFulfilled();
        }

        private bool IsProductionOrderAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select DocNo From TblProductionOrderHdr " +
                    "Where Locate(Concat('##', DocNo, '##'), @SelectedDocNo)>0 " +
                    "And CancelInd='Y' Limit 1;"

            };
            Sm.CmParam<String>(ref cm, "@SelectedDocNo", GetSelectedDocNo());
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length!=0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production Order# : " + DocNo + Environment.NewLine + Environment.NewLine + 
                    "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsProductionOrderAlreadyFulfilled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProductionOrderHdr " +
                    "Where Locate(Concat('##', DocNo, '##'), @SelectedDocNo)>0 " +
                    "And ProcessInd='F' Limit 1;"

            };
            Sm.CmParam<String>(ref cm, "@SelectedDocNo", GetSelectedDocNo());
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production Order# : " + DocNo + Environment.NewLine + Environment.NewLine +
                    "This document already processed to production planning.");
                return true;
            }
            return false;
        }

        private bool IsJCAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblJCHdr " +
                    "Where Locate(Concat('##', DocNo, '##'), @SelectedDocNo)>0 " +
                    "And CancelInd='Y' Limit 1;"

            };
            Sm.CmParam<String>(ref cm, "@SelectedDocNo", GetSelectedDocNo());
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Job Costing# : " + DocNo + Environment.NewLine + Environment.NewLine +
                    "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsJCAlreadyFulfilled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblJCHdr " +
                    "Where Locate(Concat('##', DocNo, '##'), @SelectedDocNo)>0 " +
                    "And ProcessInd='F' Limit 1;"

            };
            Sm.CmParam<String>(ref cm, "@SelectedDocNo", GetSelectedDocNo());
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Job Costing# : " + DocNo + Environment.NewLine + Environment.NewLine +
                    "This document already processed to production planning.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 production order.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production order data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Document#.")) return true;
            return false;
        }

        private MySqlCommand SavePPHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "/* Production Planning (Hdr) */ " +
                    "Insert Into TblPPHdr(DocNo, DocDt, Status, CancelInd, ProductionWorkGroup, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Status, 'N', @ProductionWorkGroup, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePPDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Planning (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPPDtl(DocNo, DNo, DocType, ProductionOrderDocNo, JCDocNo, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @ProductionOrderDocNo_" + r.ToString() +
                        ", @JCDocNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    SQL2.AppendLine("Update TblProductionOrderHdr Set ProcessInd='F' ");
                    SQL2.AppendLine("Where DocNo=@ProductionOrderDocNo_" + r.ToString());
                    SQL2.AppendLine(" And CancelInd='N' And ProcessInd<>'F'; ");

                    SQL3.AppendLine("Update TblJCHdr Set ProcessInd='F' ");
                    SQL3.AppendLine("Where DocNo=@JCDocNo_" + r.ToString());
                    SQL3.AppendLine(" And CancelInd='N' And ProcessInd<>'F'; ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    if (Sm.GetGrdStr(Grd1, r, 4) == "1")
                    {
                        Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                        Sm.CmParam<String>(ref cm, "@JCDocNo_" + r.ToString(), string.Empty);
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo_" + r.ToString(), string.Empty);
                        Sm.CmParam<String>(ref cm, "@JCDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString() + SQL3.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePPDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPPDtl(DocNo, DNo, DocType, ProductionOrderDocNo, JCDocNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @ProductionOrderDocNo, @JCDocNo, @CreateBy, CurrentDateTime()); ");

        //    SQL.AppendLine("Update TblProductionOrderHdr Set ProcessInd='F' ");
        //    SQL.AppendLine("Where DocNo=@ProductionOrderDocNo And CancelInd='N' And ProcessInd<>'F'; ");

        //    SQL.AppendLine("Update TblJCHdr Set ProcessInd='F' ");
        //    SQL.AppendLine("Where DocNo=@JCDocNo And CancelInd='N' And ProcessInd<>'F'; ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, Row, 4));
        //    if (Sm.GetGrdStr(Grd1, Row, 4) == "1")
        //    {
        //        Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //        Sm.CmParam<String>(ref cm, "@JCDocNo", "");
        //    }
        //    else
        //    {
        //        Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", "");
        //        Sm.CmParam<String>(ref cm, "@JCDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    }
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePPDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Production Planning (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");
            SQL.AppendLine("Set @DNo:=0; ");

            SQL.AppendLine("Insert Into TblPPDtl2(DocNo, DNo, DocType, WorkCenterDocNo, ItCode, Qty1, Qty2, CreateBy, CreateDt)");
            SQL.AppendLine("Select @DocNo As DocNo, Right(Concat('00', Cast((@DNo:=@DNo+1) As Char(3))), 3) as DNo, ");
            SQL.AppendLine("'1' As DocType, E.WorkCenterDocNo, G.ItCode, Sum(G.Qty) As Qty1, 0.00 As Qty2, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblPPHdr A ");
            SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='1' ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.ProductionOrderDocNo, Max(T5.Sequence) As Sequence ");
            SQL.AppendLine("    From TblPPHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPDtl T2 On T1.DocNo=T2.DocNo And T2.DocType='1' ");
            SQL.AppendLine("    Inner Join TblProductionOrderHdr T3 On T2.ProductionOrderDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionOrderDtl T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionRoutingDtl T5 On T3.ProductionRoutingDocNo=T5.DocNo And T4.ProductionRoutingDNo=T5.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    Group By T1.DocNo, T2.ProductionOrderDocNo ");
            SQL.AppendLine(") F ");
            SQL.AppendLine("    On A.DocNo=F.DocNo ");
            SQL.AppendLine("    And B.ProductionOrderDocNo=F.ProductionOrderDocNo ");
            SQL.AppendLine("    And E.Sequence=F.Sequence ");
            SQL.AppendLine("Inner Join TblBomDtl2 G On D.BomDocNo=G.Docno And G.ItType='1' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Group By E.WorkCenterDocNo, G.ItCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @DocNo As DocNo, Right(Concat('00', Cast((@DNo:=@DNo+1) As Char(3))), 3) as DNo, ");
            SQL.AppendLine("'2' As DocType, E.WorkCenterDocNo, G.ItCode, Sum(G.Qty) As Qty1, 0.00 As Qty2, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblPPHdr A ");
            SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='2' ");
            SQL.AppendLine("Inner Join TblJCHdr C On B.JCDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblJCDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.JCDocNo, Max(T5.Sequence) As Sequence ");
            SQL.AppendLine("    From TblPPHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPDtl T2 On T1.DocNo=T2.DocNo And T2.DocType='2' ");
            SQL.AppendLine("    Inner Join TblJCHdr T3 On T2.JCDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblJCDtl T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionRoutingDtl T5 On T3.ProductionRoutingDocNo=T5.DocNo And T4.ProductionRoutingDNo=T5.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    Group By T1.DocNo, T2.JCDocNo ");
            SQL.AppendLine(") F ");
            SQL.AppendLine("    On A.DocNo=F.DocNo ");
            SQL.AppendLine("    And B.JCDocNo=F.JCDocNo ");
            SQL.AppendLine("    And E.Sequence=F.Sequence ");
            SQL.AppendLine("Inner Join TblBomDtl2 G On D.BomDocNo=G.Docno And G.ItType='1' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Group By E.WorkCenterDocNo, G.ItCode;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPPHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return IsPPAlreadyCancelled();
        }

        private bool IsPPAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPPHdr " +
                    "Where DocNo=@DocNo And CancelInd='Y' Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPHdr Set CancelInd=@CancelInd, Status=@Status, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblProductionOrderHdr Set ProcessInd='O' ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select B.ProductionOrderDocNo ");
            SQL.AppendLine("    From TblPPHdr A, TblPPDtl B ");
            SQL.AppendLine("    Where A.DocNo=B.DocNo And B.DocType='1' And A.CancelInd='Y' And A.DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblJCHdr Set ProcessInd='O' ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select B.JCDocNo ");
            SQL.AppendLine("    From TblPPHdr A, TblPPDtl B ");
            SQL.AppendLine("    Where A.DocNo=B.DocNo And B.DocType='2' And A.CancelInd='Y' And A.DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPPHdr(DocNo);
                ShowPPDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Status, CancelInd, ProductionWorkGroup, Remark From TblPPHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Status", "CancelInd", "ProductionWorkGroup", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[3]) == "Y");
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowPPDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select DISTINCT A.DNo, A.DocType, B.Remark, ");
            //SQL.AppendLine("Case A.DocType When '1' Then A.ProductionOrderDocNo Else A.JCDocNo End As DocNo, ");
            //SQL.AppendLine("Case A.DocType When '1' Then B.DocDt Else C.DocDt End As DocDt, ");
            //SQL.AppendLine("Case A.DocType When '1' Then B.UsageDt Else Null End As UsageDt, ");
            //SQL.AppendLine("Case A.DocType When '1' Then B.ItCode Else C.ItCode End As ItCode, ");
            //SQL.AppendLine("Case A.DocType When '1' Then D.ItName Else E.ItName End As ItName, ");
            //SQL.AppendLine("Case A.DocType When '1' Then B.Qty Else C.Qty End As Qty, ");
            //SQL.AppendLine("Case A.DocType When '1' Then D.PlanningUomCode Else E.PlanningUomCode End As PlanningUomCode, ");
            //SQL.AppendLine("Case A.DocType When '1' Then B.Qty*D.PlanningUomCodeConvert12 Else C.Qty*E.PlanningUomCodeConvert12 End As Qty2, ");
            //SQL.AppendLine("Case A.DocType When '1' Then D.PlanningUomCode2 Else E.PlanningUomCode2 End As PlanningUomCode2, ");
            //SQL.AppendLine("CASE A.DocType WHEN '1' THEN D.Specification ELSE E.Specification END AS Specification, ");
            //SQL.AppendLine("CASE A.DocType WHEN '1' THEN F.CtItCode ELSE F.CtItCode END AS CtItCode ");
            //SQL.AppendLine("From TblPPDtl A ");
            //SQL.AppendLine("Left Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo And A.DocType='1' ");
            //SQL.AppendLine("Left Join TblJCHdr C On A.JCDocNo=C.DocNo And A.DocType='2' ");
            //SQL.AppendLine("Left Join TblItem D On B.ItCode=D.ItCode And A.DocType='1' ");
            //SQL.AppendLine("Left Join TblItem E On C.ItCode=E.ItCode And A.DocType='2' ");
            //SQL.AppendLine("Left Join TblCustomerItem F On B.ItCode=F.ItCode And A.DocType='1' ");
            ////SQL.AppendLine("Left Join TblCustomerItem G On C.ItCode=G.ItCode And A.DocType='2' ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("Select A.DNo, A.DocType, B.Remark, ");
            SQL.AppendLine("A.ProductionOrderDocNo As DocNo, ");
            SQL.AppendLine("B.DocDt, ");
            SQL.AppendLine("B.UsageDt, ");
            SQL.AppendLine("B.ItCode, ");
            SQL.AppendLine("C.ItName, ");
            SQL.AppendLine("B.Qty, ");
            SQL.AppendLine("C.PlanningUomCode, ");
            SQL.AppendLine("B.Qty*C.PlanningUomCodeConvert12 As Qty2, ");
            SQL.AppendLine("C.PlanningUomCode2, ");
            SQL.AppendLine("C.Specification, ");
            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("    Case B.DocType When '1' Then D.CtItCode When '3' Then F.CtItCode Else '' End As CtItCode, ");
                SQL.AppendLine("    Case B.DocType When '1' Then IfNull((E.Volume * E.QtyPackagingUnit), 0) When '3' Then IfNull((F.Volume * F.QtyPackagingUnit), 0) Else 0 End As Volume, ");
                SQL.AppendLine("    Case B.DocType When '1' Then IfNull(E.QtyPackagingUnit, 0) When '3' Then IfNull(F.QtyPackagingUnit, 0) Else 0 End As QtyPackagingUnit, ");
                SQL.AppendLine("    Case B.DocType When '1' Then IfNull(E.PackagingUnitUomCode, '') When '3' Then IfNull(F.PackagingUnitUomCode, '') Else '' End As PackagingUnitUomCode ");
            }
            else
                SQL.AppendLine("D.CtItCode, IfNull((E.Volume * E.QtyPackagingUnit), 0) Volume, IfNull(E.QtyPackagingUnit, 0) QtyPackagingUnit, IfNull(E.PackagingUnitUomCode, '') PackagingUnitUomCode ");
            SQL.AppendLine("From TblPPDtl A ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.ItCode, Group_Concat(Distinct Concat(T2.CtName, ' [', T1.CtItCode, ']') Order By T2.CtName Separator ', ') As CtItCode ");
            SQL.AppendLine("    From TblCustomerItem T1 ");
            SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine("    Inner Join TblProductionOrderHdr T3 On T1.ItCode=T3.ItCode ");
            SQL.AppendLine("    Inner Join TblPPDtl T4 On T3.DocNo=T4.ProductionOrderDocNo And T4.DocNo=@DocNo ");
            SQL.AppendLine("    Group By T1.ItCode ");
            SQL.AppendLine(") D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("     Select T1.DocNo, T2.DNo, T4.ItCode, T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T6.Volume ");
            SQL.AppendLine("     From TblSOHdr T1 ");
            SQL.AppendLine("     Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("     Inner Join TblCtQtDtl T3 On T1.CtQtDocNo = T3.DocNo And T2.CtQtDNo = T3.DNo ");
            SQL.AppendLine("     Inner Join TblItemPriceDtl T4 On T3.ItemPriceDocNo = T4.DocNo And T3.ItemPriceDNo = T4.DNo ");
            SQL.AppendLine("     Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
            SQL.AppendLine("     Inner Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T6.UomCode = T2.PackagingUnitUomCode ");
            SQL.AppendLine(") E On B.SODocNo = E.DocNo And B.SODNo = E.DNo And C.ItCode = E.ItCode ");
            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T1.DocNo, T4.Qty As QtyPackagingUnit, T5.SalesUomCode PackagingUnitUomCode, T6.Volume, T7.CtItCode ");
                SQL.AppendLine("        From TblProductionOrderHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T1.ProcessInd = 'O' ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl T4 On T3.DocNo = T4.DocNo And T1.SMDNo = T4.DNo ");
                SQL.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                SQL.AppendLine("        Left Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T5.SalesUomCode = T6.UomCode ");
                SQL.AppendLine("        Left Join TblCustomerItem T7 On T3.CtCode = T7.CtCode And T5.ItCode = T7.ItCode ");
                SQL.AppendLine("    ) F On A.DocNo = F.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocType='1' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DNo, A.DocType, B.Remark, ");
            SQL.AppendLine("A.JCDocNo As DocNo, ");
            SQL.AppendLine("B.DocDt, ");
            SQL.AppendLine("Null As UsageDt, ");
            SQL.AppendLine("B.ItCode, ");
            SQL.AppendLine("C.ItName, ");
            SQL.AppendLine("B.Qty, ");
            SQL.AppendLine("C.PlanningUomCode, ");
            SQL.AppendLine("B.Qty*C.PlanningUomCodeConvert12 As Qty2, ");
            SQL.AppendLine("C.PlanningUomCode2, ");
            SQL.AppendLine("C.Specification, ");
            SQL.AppendLine("D.CtItCode, 0 As Volume, 0 As QtyPackagingUnit, '' As PackagingUnitUomCode ");
            SQL.AppendLine("From TblPPDtl A ");
            SQL.AppendLine("Inner Join TblJCHdr B On A.JCDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.ItCode, Group_Concat(Distinct Concat(T2.CtName, ' [', T1.CtItCode, ']') Order By T2.CtName Separator ', ') As CtItCode ");
            SQL.AppendLine("    From TblCustomerItem T1 ");
            SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine("    Inner Join TblJCHdr T3 On T1.ItCode=T3.ItCode ");
            SQL.AppendLine("    Inner Join TblPPDtl T4 On T3.DocNo=T4.JCDocNo And T4.DocNo=@DocNo ");
            SQL.AppendLine("    Group By T1.ItCode ");
            SQL.AppendLine(") D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocType='2' ");
            SQL.AppendLine("Order By DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocNo", "DocType", "DocDt", "UsageDt", "ItCode",  
                    
                    //6-10
                    "ItName", "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", 
                    
                    //11-15
                    "Remark", "Specification", "CtItCode", "Volume", "QtyPackagingUnit", 

                    //16
                    "PackagingUnitUomCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 12, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPPDlg(this));
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "1")
                {
                    var f1 = new FrmProductionOrder(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmJC(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmPPDlg(this));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "1")
                {
                    var f1 = new FrmProductionOrder(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmJC(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsPPShowPackagingValue', 'IsPPRemarkMandatory', 'IsPPCustomDocNoEnabled', 'IsUseProductionWorkGroup', 'IsSalesContractEnabled', ");
            SQL.AppendLine("'InformationForItemColour', 'InformationForItemArtwork', 'InformationForItemPackaging' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSalesContractEnabled": mIsSalesContractEnabled = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsPPCustomDocNoEnabled": mIsPPCustomDocNoEnabled = ParValue == "Y"; break;
                            case "IsPPRemarkMandatory": mIsPPRemarkMandatory = ParValue == "Y"; break;
                            case "IsPPShowPackagingValue": mIsPPShowPackagingValue = ParValue == "Y"; break;

                            //string
                            case "InformationForItemPackaging": mInformationForItemPackaging = ParValue; break;
                            case "InformationForItemArtwork": mInformationForItemArtwork = ParValue; break;
                            case "InformationForItemColour": mInformationForItemColour = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }
        
        internal void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'P' As Col1, 'Planned' As Col2 Union All " +
                "Select 'R', 'Released';",
                0, 35, false, true, "Code", "Description", "Col2", "Col1");
        }

        internal string GetSelectedDocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PPHdr>();
            var ldtl = new List<PPDtl>();
            var ldtl2 = new List<PPDtl2>();
            var ldtl3 = new List<PPDtl3>();

            string[] TableName = { "PPHdr", "PPDtl", "PPDtl2", "PPDtl3" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%d %M %Y') AS DocDt, ");
            SQL.AppendLine("A.Remark ");
            //SQL.AppendLine(", IFNULL(d.DocDt, 0) AS FirstSODocDt ");
            SQL.AppendLine("FROM TblPPHdr A ");
            SQL.AppendLine("INNER JOIN TblPPDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblProductionOrderHdr C ON B.ProductionOrderDocNo = C.DocNo ");
            SQL.AppendLine("LEFT JOIN TblSOHdr D ON C.SODocNo = D.DocNo ");
            SQL.AppendLine("WHERE A.DocNo=@DocNo AND A.CancelInd = 'N' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "CompanyFax",
                         "CompLocation2",
                         "DocNo",
                         "DocDt",
                         "Remark",
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            CompanyFax = Sm.DrStr(dr, c[6]),
                            CompLocation2 = Sm.DrStr(dr, c[7]),
                            DocNo = Sm.DrStr(dr, c[8]),
                            DocDt = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT DISTINCT ItName, Specification, ProdOrRemark, Colour, Artwork, Packaging, ItGrpCode, CtItCode, ItCodeInternal, Qty, PlanningUomCode, Volume, QtyPackagingUnit, PackagingUnitUomCode ");
                SQLDtl.AppendLine("FROM ");
                SQLDtl.AppendLine("( ");
                SQLDtl.AppendLine("  SELECT A.DNo, D.ItName, D.Specification, C.Remark AS ProdOrRemark, ");
                SQLDtl.AppendLine("  F.OptDesc AS Colour, G.OptDesc AS Artwork, H.OptDesc AS Packaging, ");
                SQLDtl.AppendLine("  D.ItGrpCode, E.CtItCode, D.ItCodeInternal, C.Qty, D.PlanningUomCode, ");
                if (mIsSalesContractEnabled)
                {
                    SQLDtl.AppendLine("    Case C.DocType When '1' Then IfNull((I.Volume * I.QtyPackagingUnit), 0) When '3' Then IfNull((J.Volume * J.QtyPackagingUnit), 0) Else 0 End As Volume, ");
                    SQLDtl.AppendLine("    Case C.DocType When '1' Then IfNull(I.QtyPackagingUnit, 0) When '3' Then IfNull(J.QtyPackagingUnit, 0) Else 0 End As QtyPackagingUnit, ");
                    SQLDtl.AppendLine("    Case C.DocType When '1' Then IfNull(I.PackagingUnitUomCode, '') When '3' Then IfNull(J.PackagingUnitUomCode, '') Else '' End As PackagingUnitUomCode ");
                }
                else
                    SQLDtl.AppendLine("  IfNull((I.Volume * I.QtyPackagingUnit), 0) Volume, IfNull(I.QtyPackagingUnit, 0) QtyPackagingUnit, IfNull(I.PackagingUnitUomCode, '') PackagingUnitUomCode ");
                SQLDtl.AppendLine("  FROM TblPPDtl A ");
                SQLDtl.AppendLine("  INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo And A.DocNo = @DocNo And A.DocType = '1' ");
                SQLDtl.AppendLine("  INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl.AppendLine("  INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                //SQLDtl.AppendLine("  LEFT JOIN TblCustomerItem E ON C.ItCode = E.ItCode ");
                SQLDtl.AppendLine("Left Join ( ");
                SQLDtl.AppendLine("    Select T1.ItCode, Group_Concat(Distinct Concat(T2.CtName, ' [', T1.CtItCode, ']') Order By T2.CtName Separator ', ') As CtItCode ");
                SQLDtl.AppendLine("    From TblCustomerItem T1 ");
                SQLDtl.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
                SQLDtl.AppendLine("    Inner Join TblProductionOrderHdr T3 On T1.ItCode=T3.ItCode ");
                SQLDtl.AppendLine("    Inner Join TblPPDtl T4 On T3.DocNo=T4.ProductionOrderDocNo And T4.DocNo=@DocNo ");
                SQLDtl.AppendLine("    Group By T1.ItCode ");
                SQLDtl.AppendLine(") E On C.ItCode=E.ItCode ");
                
                if(mInformationForItemColour.Length == 0 || mInformationForItemColour == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption F ON D.Information2 = F.OptCode AND F.OptCat = 'ItemInformation2'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption F ON D."+mInformationForItemColour+" = F.OptCode AND F.OptCat = 'Item"+mInformationForItemColour+"'");

                if (mInformationForItemArtwork.Length == 0 || mInformationForItemArtwork == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D.Information3 = G.OptCode AND G.OptCat = 'ItemInformation3'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D." + mInformationForItemArtwork + " = G.OptCode AND G.OptCat = 'Item" + mInformationForItemArtwork + "'");

                if (mInformationForItemPackaging.Length == 0 || mInformationForItemPackaging == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D.Information4 = H.OptCode AND H.OptCat = 'ItemInformation4'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D." + mInformationForItemPackaging + " = H.OptCode AND H.OptCat = 'Item" + mInformationForItemPackaging + "'");

                SQLDtl.AppendLine("    Left Join ");
                SQLDtl.AppendLine("    ( ");
                SQLDtl.AppendLine("        Select T1.DocNo, T2.DNo, T4.ItCode, T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T6.Volume ");
                SQLDtl.AppendLine("        From TblSOHdr T1 ");
                SQLDtl.AppendLine("        Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo ");
                SQLDtl.AppendLine("        Inner Join TblCtQtDtl T3 On T1.CtQtDocNo = T3.DocNo And T2.CtQtDNo = T3.DNo ");
                SQLDtl.AppendLine("        Inner Join TblItemPriceDtl T4 On T3.ItemPriceDocNo = T4.DocNo And T3.ItemPriceDNo = T4.DNo ");
                SQLDtl.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                SQLDtl.AppendLine("        Inner Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T6.UomCode = T2.PackagingUnitUomCode ");
                SQLDtl.AppendLine("    ) I On C.SODocNo = I.DocNo And C.SODNo = I.DNo And D.ItCode = I.ItCode ");

                if (mIsSalesContractEnabled)
                {
                    SQLDtl.AppendLine("    Left Join ");
                    SQLDtl.AppendLine("    ( ");
                    SQLDtl.AppendLine("        Select T1.DocNo, T4.Qty As QtyPackagingUnit, T5.SalesUomCode PackagingUnitUomCode, T6.Volume, T7.CtItCode ");
                    SQLDtl.AppendLine("        From TblProductionOrderHdr T1 ");
                    SQLDtl.AppendLine("        Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo ");
                    SQLDtl.AppendLine("            And T1.CancelInd = 'N' ");
                    SQLDtl.AppendLine("            And T1.ProcessInd = 'O' ");
                    SQLDtl.AppendLine("        Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                    SQLDtl.AppendLine("        Inner Join TblSalesMemoDtl T4 On T3.DocNo = T4.DocNo And T1.SMDNo = T4.DNo ");
                    SQLDtl.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                    SQLDtl.AppendLine("        Left Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T5.SalesUomCode = T6.UomCode ");
                    SQLDtl.AppendLine("        Left Join TblCustomerItem T7 On T3.CtCode = T7.CtCode And T5.ItCode = T7.ItCode ");
                    SQLDtl.AppendLine("    ) J On C.DocNo = J.DocNo ");
                }
                
                
                SQLDtl.AppendLine("  UNION ALL ");
                
                
                SQLDtl.AppendLine("  SELECT A.DNo, D.ItName, D.Specification, C.Remark AS ProdOrRemark, G.OptDesc AS Colour, ");
                SQLDtl.AppendLine("  H.OptDesc AS Artwork, I.OptDesc AS Packaging, D.ItGrpCode, E.CtItCode, D.ItCodeInternal, ");
                SQLDtl.AppendLine("  F.Qty, D.PlanningUomCode, ");
                SQLDtl.AppendLine("  0 As Volume, 0 As QtyPackagingUnit, '' As PackagingUnitUomCode ");
                SQLDtl.AppendLine("  FROM TblPPDtl A ");
                SQLDtl.AppendLine("  INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo And A.DocNo = @DocNo AND A.DocType != '1' ");
                SQLDtl.AppendLine("  INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl.AppendLine("  LEFT JOIN TblItem D ON C.ItCode = D.ItCode ");
                SQLDtl.AppendLine("  LEFT JOIN TblCustomerItem E ON C.ItCode = E.ItCode ");
                SQLDtl.AppendLine("  LEFT JOIN TblJCHdr F ON A.JCDocNo=F.DocNo ");

                if (mInformationForItemColour.Length == 0 || mInformationForItemColour == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D.Information2 = G.OptCode AND G.OptCat = 'ItemInformation2'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D." + mInformationForItemColour + " = G.OptCode AND G.OptCat = 'Item" + mInformationForItemColour + "'");

                if (mInformationForItemArtwork.Length == 0 || mInformationForItemArtwork == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D.Information3 = H.OptCode AND H.OptCat = 'ItemInformation3'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D." + mInformationForItemArtwork + " = H.OptCode AND H.OptCat = 'Item" + mInformationForItemArtwork + "'");

                if (mInformationForItemPackaging.Length == 0 || mInformationForItemPackaging == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption I ON D.Information4 = I.OptCode AND I.OptCat = 'ItemInformation4'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption I ON D." + mInformationForItemPackaging + " = I.OptCode AND I.OptCat = 'Item" + mInformationForItemPackaging + "'");

                SQLDtl.AppendLine(")T ");
                SQLDtl.AppendLine("ORDER BY DNo; ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItName" ,

                     //1-5
                     "Specification",
                     "ProdOrRemark",
                     "Colour",
                     "Artwork",
                     "Packaging",

                     //6-10
                     "ItGrpCode",
                     "CtItCode",
                     "ItCodeInternal",
                     "Qty",
                     "PlanningUomCode",

                     //11-13
                     "Volume",
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode"
                     
                    });
                if (drDtl.HasRows)
                {
                    int NomorBaris = 0;
                    while (drDtl.Read())
                    {
                        NomorBaris = NomorBaris + 1;
                        ldtl.Add(new PPDtl()
                        {
                            Nomor = NomorBaris,

                            ItName = Sm.DrStr(drDtl, cDtl[0]),
                            Specification = Sm.DrStr(drDtl, cDtl[1]),
                            ProdOrRemark = Sm.DrStr(drDtl, cDtl[2]),
                            Colour = Sm.DrStr(drDtl, cDtl[3]),
                            Artwork = Sm.DrStr(drDtl, cDtl[4]),
                            Packaging = Sm.DrStr(drDtl, cDtl[5]),

                            ItGrpCode = Sm.DrStr(drDtl, cDtl[6]),
                            CtItCode = Sm.DrStr(drDtl, cDtl[7]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[8]),
                            Qty = Sm.DrDec(drDtl, cDtl[9]),
                            PlanningUomCode = Sm.DrStr(drDtl, cDtl[10]),
                            Volume = Sm.DrDec(drDtl, cDtl[11]),
                            QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[12]),
                            PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[13]),
                            IsPPShowPackagingValue = mIsPPShowPackagingValue ? "Y" : "N"
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Detail2 (Tgl MTS / SO)

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("SELECT ");
                if (mIsSalesContractEnabled)
                    SQLDtl2.AppendLine("Date_Format(Case B.DocType When '1' Then D.DocDt When '2' Then C.DocDt When '3' Then E.DocDt End, '%d %M %Y') As MTSODocDt ");
                else
                    SQLDtl2.AppendLine("DATE_FORMAT(CASE WHEN B.SoDocNo IS NULL THEN C.DocDt ELSE D.DocDt END, '%d %M %Y') AS MTSODocDt ");
                SQLDtl2.AppendLine("FROM TblPPDtl A ");
                SQLDtl2.AppendLine("INNER JOIN TblProductionOrderHdr B ON A.ProductionOrderDocNo = B.DocNo ");
                SQLDtl2.AppendLine("LEFT JOIN TblMakeToStockHdr C ON B.MakeToStockDocNo = C.DocNo ");
                SQLDtl2.AppendLine("LEFT JOIN TblSOHdr D ON B.SODocNo = D.DocNo ");
                if (mIsSalesContractEnabled)
                    SQLDtl2.AppendLine("Left Join TblSalesContract E On B.SCDocNo = E.DocNo ");
                SQLDtl2.AppendLine("WHERE A.DocNo = @DocNo ");
                SQLDtl2.AppendLine("ORDER BY MTSODocDt ASC LIMIT 1 ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "MTSODocDt"
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new PPDtl2()
                        {
                            MTSODocDt = Sm.DrStr(drDtl2, cDtl2[0])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion

            #region Detail3 (group name & qty1)

            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("SELECT ItGrpName, SUM(Qty) AS SubQty FROM ");
                SQLDtl3.AppendLine("( ");
                SQLDtl3.AppendLine("   SELECT E.ItGrpName, SUM(C.Qty) As Qty ");
                SQLDtl3.AppendLine("   FROM TblPPDtl A ");
                SQLDtl3.AppendLine("   INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo And A.Dno = B.Dno ");
                SQLDtl3.AppendLine("   INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItem D ON C.ItCode = D.ItCode  ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItemGroup E ON D.ItGrpCode = E.ItGrpCode  ");
                SQLDtl3.AppendLine("   WHERE A.DocNo = @DocNo AND A.DocType = '1' ");
                SQLDtl3.AppendLine("   Group By E.ItGrpName");
                SQLDtl3.AppendLine("   UNION ALL ");
                SQLDtl3.AppendLine("   SELECT E.ItGrpName, SUM(C.Qty) As Qty ");
                SQLDtl3.AppendLine("   FROM TblPPDtl A ");
                SQLDtl3.AppendLine("   INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo And A.Dno = B.Dno ");
                SQLDtl3.AppendLine("   INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItem D ON C.ItCode = D.ItCode ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItemGroup E ON D.ItGrpCode = E.ItGrpCode ");
                SQLDtl3.AppendLine("   WHERE A.DocNo = @DocNo AND A.DocType != '1' ");
                SQLDtl3.AppendLine("   Group By E.ItGrpName");
                SQLDtl3.AppendLine(")T ");
                SQLDtl3.AppendLine("GROUP BY ItGrpName ");
                SQLDtl3.AppendLine("ORDER BY ItGrpName; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);

                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "ItGrpName",

                     //1
                     "SubQty"
                    });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new PPDtl3()
                        {
                            ItGrpName = Sm.DrStr(drDtl3, cDtl3[0]),
                            SubQty = Sm.DrDec(drDtl3, cDtl3[1]),
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            Sm.PrintReport("PP", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
        }

        #endregion
    }

    #region Report Class

    class PPHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyFax { get; set; }
        public string CompLocation2 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string Remark { get; set; }

        public string PrintBy { get; set; }
    }

    class PPDtl
    {
        public int Nomor { get; set; }

        public string ItName { get; set; }
        public string Specification { get; set; }
        public string ProdOrRemark { get; set; }
        public string Colour { get; set; }
        public string Artwork { get; set; }
        public string Packaging { get; set; }
        public string ItGrpCode { get; set; }
        public string CtItCode { get; set; }
        public string ItCodeInternal { get; set; }
        public decimal Qty { get; set; }
        public string PlanningUomCode { get; set; }
        public decimal Volume { get; set; }
        public decimal QtyPackagingUnit { get; set; }
        public string PackagingUnitUomCode { get; set; }
        public string IsPPShowPackagingValue { get; set; }
    }

    class PPDtl2
    {
        public string MTSODocDt { get; set; }
    }

    class PPDtl3
    {
        public string ItGrpName { get; set; }
        public decimal SubQty { get; set; }
    }

    #endregion
}
