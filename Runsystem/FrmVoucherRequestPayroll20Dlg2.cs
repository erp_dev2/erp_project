﻿#region Update
/*
    07/09/2022 [DITA/SKI] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using System.IO;
using System.Net;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPayroll20Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestPayroll20 mFrmParent;
        private string 
            mDocNo = string.Empty, 
            mDocDt = string.Empty, 
            mBankCode = string.Empty, 
            mSQL = string.Empty;

        #region untuk csv

        private string
            mHostAddrForMandiriPayroll = string.Empty,
            mSharedFolderForMandiriPayroll = string.Empty,
            mUserNameForMandiriPayroll = string.Empty,
            mPasswordForMandiriPayroll = string.Empty,
            mPortForMandiriPayroll = string.Empty,
            mHostAddrForBNIPayroll = string.Empty,
            mSharedFolderForBNIPayroll = string.Empty,
            mUserNameForBNIPayroll = string.Empty,
            mPasswordForBNIPayroll = string.Empty,
            mPortForBNIPayroll = string.Empty,
            mProtocolForMandiriPayroll = string.Empty,
            mProtocolForBNIPayroll = string.Empty,
            mCompanyCodeForMandiriPayroll = string.Empty,
            mPathToSaveExportedMandiriPayroll = string.Empty,
            mPathToSaveExportedBNIPayroll = string.Empty;

        private bool 
            mIsCSVUseRealAmt = false,
            mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce = false;

        #endregion
   
        #endregion

        #region Constructor

        public FrmVoucherRequestPayroll20Dlg2(
            FrmVoucherRequestPayroll20 FrmParent, 
            string DocNo, 
            string DocDt, 
            string BankCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mDocDt = DocDt;
            mBankCode = BankCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Bank",
                    "Bank"+Environment.NewLine+"Account#",

                    //6
                    "Transferred"
                  },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 200, 150, 150,   
                    
                    //6
                    130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.EmpCode, D.EmpName, E.BankName, D.BankAcNo, Sum(C.Amt) As Amt ");
            SQL.AppendLine("From TblVoucherRequestPayrollHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPayrollProcess1 C ON B.PayrunCode=C.PayrunCode ");
            SQL.AppendLine("Inner Join TblEmployee D On C.EmpCode=D.EmpCode And IfNull(D.PayrollType, 'T')='T' And D.BankCode = '002' ");
            SQL.AppendLine("Inner join TblBank E On D.BankCode = E.BankCode ");
            SQL.AppendLine("Inner join TblVoucherRequestHdr F On A.VoucherRequestDocNo=F.DocNo And F.CancelInd='N' And F.Status='A' ");
            //And IfNull(F.VoucherDocNo, '')='' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status = 'A' ");
            if (!mFrmParent.mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce)
                SQL.AppendLine("And A.CSVInd = 'N' ");
            SQL.AppendLine("And C.Amt>0 ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "D.EmpCode", "D.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter +
                        "Group By D.EmpCode, D.EmpName, E.BankName, D.BankAcNo " +
                        "Order By D.EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "BankName", "BankAcNo", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return IsGrdEmpty() || IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                    Sm.GetGrdBool(Grd1, r, 1)) return false;
            }
            Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
            return true;
        }

        override protected void ChooseData()
        {
            if (mBankCode == "002")
            {
                if (Sm.StdMsgYN("Question", "Do you want to process this data ?") == DialogResult.No ||
                IsDataNotValid()
                ) return;

                var lBank = new List<Bank>();
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                string mCreditTo = Sm.GetValue(
                    "Select B.BankAcNo " +
                    "From TblVoucherRequestPayrollHdr A " +
                    "Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " +
                    "Where A.DocNo=@Param;", mDocNo);
                string EmpCode = string.Empty, Filter = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                        if (Sm.GetGrdBool(Grd1, r, 1) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(D.EmpCode=@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " And 1=0 ";

                SQL.AppendLine("Select T.EmpCode, T.EmpName, T.BankAcNo, T.BankAcName, T.BankCode, T.BankName, ");
                SQL.AppendLine("T.CurCode, Round(Sum(T.THP), 0) As THP, T.Remark, T.FTS, T.Beneficiary, T.Email, T.DocNo, T.Periode, ");
                SQL.AppendLine("T.VoucherRequestDocNo, T.BankBranch, T.ReferenceNo, T.SwiftBeneCode, T.KliringCode, T.RTGSCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("  Select C.EmpCode, D.EmpName, D.BankAcNo, D.BankAcName, D.BankCode, F.BankName, ");
                SQL.AppendLine("  A.CurCode, C.Amt as THP, A.Remark, 'IBU' as FTS, 'N' As Beneficiary, ");
                SQL.AppendLine("  null As Email, A.DocNo, IfNull(DATE_FORMAT(Left(G.EndDt, 8), '%d/%m/%Y'), '') As Periode, A.VoucherRequestDocNo, ");
                SQL.AppendLine("  D.BankBranch, Concat(Left(B.PayrunCode, 6), C.EmpCode) As ReferenceNo, F.SwiftBeneCode, F.KliringCode, F.RTGSCode ");
                SQL.AppendLine("  From tblvoucherrequestpayrollhdr A ");
                SQL.AppendLine("  inner join tblvoucherrequestpayrolldtl2 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("  inner join tblpayrollprocess1 C ON B.PayrunCode = C.PayrunCode ");
                SQL.AppendLine("  inner join tblemployee D ");
                SQL.AppendLine("    On C.EmpCode = D.EmpCode ");
                SQL.AppendLine("    And IfNull(D.PayrollType, 'T') = 'T' ");
                SQL.AppendLine("    And D.BankCode = '002' "); 
                // employee yang payroll type nya bank transfer (option EmployeePayrollType)
                SQL.AppendLine(Filter);
                SQL.AppendLine("  inner join tblvoucherrequesthdr E On A.VoucherRequestDocNo = E.DocNo ");
                SQL.AppendLine("  left join tblbank F On D.BankCode = F.BankCode ");
                SQL.AppendLine("  inner join tblpayrun G On C.PayrunCode = G.PayrunCode ");
                SQL.AppendLine("  Where A.CancelInd = 'N' ");
                SQL.AppendLine("  And A.Status = 'A' ");
                if (!mFrmParent.mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce)
                    SQL.AppendLine("And A.CSVInd = 'N' ");
                SQL.AppendLine("  And C.Amt > 0 ");
                SQL.AppendLine("  And A.DocNo = @DocNo ");
                //SQL.AppendLine("  And (E.VoucherDocNo Is Null Or Length(Trim(E.VoucherDocNo)) <= 0 ) ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.EmpCode, T.EmpName, T.BankAcNo, T.BankAcName, T.BankCode, T.BankName, T.CurCode, T.Remark, T.FTS, ");
                SQL.AppendLine("T.Beneficiary, T.Email, T.DocNo, T.Periode, T.VoucherRequestDocNo, T.BankBranch, T.ReferenceNo, T.SwiftBeneCode, T.KliringCode, T.RTGSCode; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "EmpCode",

                         //1-5
                         "EmpName",
                         "BankAcNo",
                         "BankAcName",
                         "BankCode",
                         "BankName",

                         //6-10
                         "CurCode",
                         "THP", 
                         "Remark", 
                         "FTS",
                         "Beneficiary",

                         //11-15
                         "Email",
                         "DocNo",
                         "Periode",
                         "VoucherRequestDocNo",
                         "BankBranch",

                         //16-19
                         "ReferenceNo",
                         "SwiftBeneCode",
                         "KliringCode",
                         "RTGSCode"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lBank.Add(new Bank()
                            {
                                EmpCode = Sm.DrStr(dr, c[0]),
                                EmpName = Sm.DrStr(dr, c[1]),
                                EmpAcNo = Sm.DrStr(dr, c[2]),
                                EmpAcName = Sm.DrStr(dr, c[3]),
                                EmpBankCode = Sm.DrStr(dr, c[4]),
                                EmpBankName = Sm.DrStr(dr, c[5]),
                                CurCode = mFrmParent.mIsCSVUseRealAmt ? Sm.DrStr(dr, c[6]) : "IDR",
                                THP = mFrmParent.mIsCSVUseRealAmt ? Sm.DrDec(dr, c[7]) : 1,
                                Remark = Sm.DrStr(dr, c[8]).Replace(Environment.NewLine, " "),
                                FTS = Sm.DrStr(dr, c[9]),
                                BeneficiaryNotFlag = Sm.DrStr(dr, c[10]),
                                Email = Sm.DrStr(dr, c[11]),
                                DocNo = Sm.DrStr(dr, c[12]),
                                VRDocNo = Sm.DrStr(dr, c[14]),
                                BankBranch = Sm.DrStr(dr, c[15]),
                                ReferenceNo = Sm.DrStr(dr, c[16]),
                                SwiftBeneCode = Sm.DrStr(dr, c[17]),
                                KliringCode = Sm.DrStr(dr, c[18]),
                                RTGSCode = Sm.DrStr(dr, c[19]),
                            });
                        }
                    }
                    dr.Close();
                }

                if (lBank.Count > 0)
                {
                    for (int i = 0; i < lBank.Count; i++)
                    {
                        if (lBank[i].EmpAcNo.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Account Number.");
                            return;
                        }

                        if (lBank[i].EmpAcName.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Account Name.");
                            return;
                        }

                        if (lBank[i].EmpBankCode.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Bank Account.");
                            return;
                        }
                    }

                    var lHBRI = new List<HBRI>();
                    var lSBRI = new List<SBRI>();
                    decimal mCount = 0m, mTotal = 0m, mChecksum = 0m;
                    int mNo = 1;
                    string mHashChecksum = string.Empty;

                    mCount = lBank.Count;

                    foreach (var i in lBank)
                    {
                        mTotal += i.THP;
                        mChecksum += Decimal.Parse(Sm.Right(i.EmpAcNo, 4));

                        lHBRI.Add(new HBRI()
                        {
                            No = mNo.ToString(),
                            EmpAcName = i.EmpAcName,
                            EmpAcNo = i.EmpAcNo,
                            THP = i.THP,
                            Email = i.Email
                        });

                        mNo += 1;
                    }
                    mHashChecksum = mFrmParent.GenerateMD5Hash(mChecksum.ToString());

                    lSBRI.Add(new SBRI()
                    {
                        Count = mCount,
                        Total = mTotal,
                        Checksum = mHashChecksum
                    });

                    if (lHBRI.Count > 0)
                    {
                        ExportCSVBRI(ref lHBRI, ref lSBRI);
                    }
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Info, "Bank Name field only support BRI for CSV process.");
                return;
            }
        }

        private void ExportCSVBRI(ref List<HBRI> lH, ref List<SBRI> lS)
        {
            try
            {
                string TimeStamp = Sm.Right(Sm.GetValue("Select Date_Format(Now(), '%Y%m%d%H%i%s') As Tm;"), 14);
                string FileName = string.Concat(mFrmParent.TxtDocNo.Text.Replace("/", "_"), "_", TimeStamp, ".csv");

                if (lS.Count > 0)
                {
                    CreateCSVFileBRI(ref lH, ref lS, FileName);

                    if (Sm.GetParameter("DocTitle") == "GSS")
                    {
                        //if (IsBankBRIDataNotValid() || Sm.StdMsgYN("Question", "Bank BRI" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                        //SendDataBRI(mFrmParent.mPathToSaveExportedBRIVR, FileName);

                        //UpdateCSVInd(TxtDocNo.Text);
                    }
                }
            }
            catch (Exception Ex)
            {
                Sm.ShowErrorMsg(Ex);
            }
        }

        private bool IsBankBRIDataNotValid()
        {
            if (mFrmParent.mProtocolForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Protocol (BRI) is empty.");
                return true;
            }

            if (mFrmParent.mHostAddrForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address (BRI) is empty.");
                return true;
            }

            if (mFrmParent.mSharedFolderForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (BRI) is empty.");
                return true;
            }

            if (mFrmParent.mUserNameForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username (BRI) is empty.");
                return true;
            }

            if (mFrmParent.mPortForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number (BRI) is empty.");
                return true;
            }

            return false;
        }

        private void CreateCSVFileBRI(ref List<HBRI> lH, ref List<SBRI> lS, string FileName)
        {
            StreamWriter sw = new StreamWriter(mFrmParent.mPathToSaveExportedBRIVR + FileName, false, Encoding.GetEncoding(1252));

            sw.WriteLine(
                    "NO,NAMA,ACCOUNT,AMOUNT,Email"
                );

            foreach (var x in lH)
            {
                sw.WriteLine(
                    x.No + "," + x.EmpAcName.Replace(",", "") + "," + x.EmpAcNo + "," + x.THP + "," + x.Email
                );
            }
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mFrmParent.mPathToSaveExportedBRIVR + FileName);
            lS.Clear();
            lH.Clear();
        }

        private void SendDataBRI(string sourceDrive, string FileName)
        {
            if (mFrmParent.mProtocolForBRIVR.ToUpper() == "FTP")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", sourceDrive + FileName));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mFrmParent.mHostAddrForBRIVR, mFrmParent.mPortForBRIVR, mFrmParent.mSharedFolderForBRIVR, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mFrmParent.mUserNameForBRIVR, mFrmParent.mPasswordForBRIVR);
                request.KeepAlive = false;
                request.EnableSsl = true;

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", sourceDrive + FileName));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
            }
            else if (mFrmParent.mProtocolForBRIVR.ToUpper() == "SFTP")
            {

                using (SftpClient client = new SftpClient(mFrmParent.mHostAddrForBRIVR, Int32.Parse(mFrmParent.mPortForBRIVR), mFrmParent.mUserNameForBRIVR, mFrmParent.mPasswordForBRIVR))
                {
                    string destinationPath = string.Format(@"{0}{1}{0}", "/", mFrmParent.mSharedFolderForBRIVR);
                    client.Connect();
                    client.ChangeDirectory(destinationPath);
                    using (FileStream fs = new FileStream(string.Format(@"{0}", sourceDrive + FileName), FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(string.Format(@"{0}", sourceDrive + FileName)), null);
                    }
                    client.Dispose();
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Unknown protocol.");
                return;
            }

            Sm.StdMsg(mMsgType.Info, "File uploaded to BRI Server.");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

        #region Class

        private class Bank
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public string EmpBankCode { get; set; }
            public string EmpBankName { get; set; }
            public decimal THP { get; set; }
            public string Remark { get; set; }
            public string FTS { get; set; }
            public string BeneficiaryNotFlag { get; set; }
            public string Email { get; set; }
            public string DocNo { get; set; }
            public string Periode { get; set; }
            public string VRDocNo { get; set; }
            public string BankBranch { get; set; }
            public string ReferenceNo { get; set; }
            public string SwiftBeneCode { get; set; }
            public string KliringCode { get; set; }
            public string RTGSCode { get; set; }
        }

        private class HBRI
        {
            public string No { get; set; }
            public string EmpAcName { get; set; }
            public string EmpAcNo { get; set; }
            public decimal THP { get; set; }
            public string Email { get; set; }
        }

        private class SBRI
        {
            public decimal Count { get; set; }
            public decimal Total { get; set; }
            public string Checksum { get; set; }
        }

        #endregion
    }
}
