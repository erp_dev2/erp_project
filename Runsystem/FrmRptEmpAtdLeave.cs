﻿#region Update
/*
    15/04/2020 [HAR/MMM] reporting attendancelog validasi berdasarkan system type 
*/
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;



#endregion

namespace RunSystem
{
    public partial class FrmRptEmpAtdLeave : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool
           mIsFilterByDeptHR = false;
        private bool mIsAtdShowDataLogWhenHoliday = false;
        decimal Delay = 0m, RoundSch = 0m;
        private string mLeaveCodeGetMealTransport = string.Empty;
        private string mDocTitle = string.Empty;


        #endregion

        #region Constructor

        public FrmRptEmpAtdLeave(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                
                DteStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteEndDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
               
                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            Delay = Sm.GetParameterDec("DelayTolerance");
            RoundSch = Sm.GetParameterDec("RoundingValueSchedule");
            mLeaveCodeGetMealTransport = Sm.GetParameter("LeaveCodeGetMealTransport");
            mIsAtdShowDataLogWhenHoliday = Sm.GetParameterBoo("IsAtdShowDataLogWhenHoliday");
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd1, 0, 0, "Employee" + Environment.NewLine + "Code", 2, 80);
            SetGrdHdr(ref Grd1, 0, 1, "Employee Old" + Environment.NewLine + "Code", 2, 100);
            SetGrdHdr(ref Grd1, 0, 2, "Employee", 2, 150);
            SetGrdHdr(ref Grd1, 0, 3, "", 2, 20);
            SetGrdHdr(ref Grd1, 0, 4, "Department", 2, 150);
            SetGrdHdr(ref Grd1, 0, 5, "Date", 2, 80);
            SetGrdHdr2(ref Grd1, 1, 6, "Attendance" + Environment.NewLine + "Log", 2);
            SetGrdHdr2(ref Grd1, 1, 8, "Manual" + Environment.NewLine + "Attendance", 2);
            SetGrdHdr2(ref Grd1, 1, 10, "Working" + Environment.NewLine + "Schedule", 2);
            SetGrdHdr(ref Grd1, 0, 12, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + " Manual", 2, 100);
            SetGrdHdr(ref Grd1, 0, 13, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + "Working Schedule", 2, 120);
            SetGrdHdr2(ref Grd1, 1, 14, "Actual (Input)", 2);
            SetGrdHdr3(ref Grd1, 1, 16, "Actual", 4);
            SetGrdHdr(ref Grd1, 0, 20, "Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 21, "Resign Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 22, "No", 2, 30);
            SetGrdHdr(ref Grd1, 0, 23, "Check", 2, 50);
            SetGrdHdr(ref Grd1, 0, 24, "OT Out", 2, 80);
            SetGrdHdr(ref Grd1, 0, 25, "Date Out", 2, 80);
            SetGrdHdr(ref Grd1, 0, 26, "Info", 2, 150);
            SetGrdHdr(ref Grd1, 0, 27, "OT In", 2, 80);
            SetGrdHdr(ref Grd1, 0, 28, "OT Out", 2, 80);
            SetGrdHdr(ref Grd1, 0, 29, "OT Remark", 2, 200);


            Sm.GrdFormatDate(Grd1, new int[] { 5, 16, 18, 20, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 27, 28 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 8, 9, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 23 });
            Grd1.Cols[22].Move(0);
            Grd1.Cols[23].Move(1);
            Grd1.Cols[20].Move(2);
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "OUT", 1, 80);
        }

        private void SetGrdHdr3(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "Date IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "Time IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "Date OUT", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "Time OUT", 1, 80);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueDeptCode, "Department") ||
                        IsGrdMonthNotValid()
                        ) return;

                Sm.ClearGrd(Grd1, false);
                DateTime Dt1 = new DateTime(
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                       0, 0, 0
                       );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                    0, 0, 0
                    );

                var range = (Dt2 - Dt1).Days;

                ListOfEmp();

                if (range >= 0)
                {
                    //ReListEmp(range);
                    ReListEmp();
                }


                ComputeVs(6, 8, 12);
                ComputeVs(6, 10, 13);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Method
        private bool IsGrdMonthNotValid()
        {
            string Msg = "";
            if (Sm.GetDte(DteStartDt).Substring(4, 2) != Sm.GetDte(DteEndDt).Substring(4, 2))
            {
                Sm.StdMsg(mMsgType.Warning,
                    Msg +
                    "Amandment have different month");
                return true;
            }
            return false;
        }

        public void ListOfEmp()
        {
            try
            {
                string days = Sm.GetDte(DteStartDt).Substring(0, 6);

                var l = new List<Employee>();

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                DateTime Dt1 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                    0, 0, 0
                    );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                    0, 0, 0
                    );

                var range = (Dt2 - Dt1).Days;
                DateTime TempDt = Dt1;

                //int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
                string RowCol = Sm.GetDte(DteStartDt).Substring(6, 2);
                string dayPlus;

                var SQL = new StringBuilder();


                SQL.AppendLine("Select A.EmpCode, ifnull(A.EmpCodeOld, '-') As EmpCodeOld, A.EmpName, B.DeptName, ifnull(A.ResignDt, 0) As ResignDt, ");
                SQL.AppendLine("C.Dt As DtList,  E.In1, E.Out1, D.Dt As DtWs, E.Out3 As OT  ");
                SQL.AppendLine("From tblEmployee A  ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Left Join ( ");
                for (int a = 0; a <= range; a++)
                {
                    int RowCols = Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(6, 2)) + a;


                    SQL.AppendLine("    select convert('" + days + RowCols.ToString("00") + "' using latin1) as dt ");
                    if (a != range)
                    {
                        SQL.AppendLine("    union all ");
                    }
                }
                SQL.AppendLine("    ) C on 0=0 ");
                SQL.AppendLine("Left Join TblEmpWorkSchedule D On A.EmpCode = D.EmpCode And C.Dt=D.Dt ");
                SQL.AppendLine("Left Join tblWorkSchedule E On D.WsCode = E.WsCode And E.ActInd = 'Y' And E.HolidayInd = 'N' ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("(   Select Y.EmpCode, Y.AGCode ");
                SQL.AppendLine("    From TblAttendanceGrpHdr X ");
                SQL.AppendLine("    Inner Join TblAttendanceGrpDtl Y On X.AgCode = Y.AGCode ");
                SQL.AppendLine("    Where X.ActInd = 'Y' ");
                SQL.AppendLine(" )F On A.EmpCode = F.EmpCode ");
                SQL.AppendLine("Where (A.ResignDt is null Or (A.ResignDt is not null And (@DocDt2 >A.resignDt>@DocDt1)) Or (A.ResignDt is not null And @DocDt1<A.ResignDt)) ");
                SQL.AppendLine("And A.JoinDt <= @DocDt2 ");
                if (Sm.GetLue(LueDeptCode).Length > 0)
                    SQL.AppendLine("And A.DeptCode=@DeptCode ");
                if (mDocTitle == "IOK")
                    SQL.AppendLine("And A.SystemType = '1' ");
                if (Sm.GetLue(LueAGCode).Length > 0)
                    SQL.AppendLine("And F.AGCode=@AGCode");
                //SQL.AppendLine("And D.Dt Between @DocDt1 And @DocDt2 ");    
                SQL.AppendLine("Order By A.EmpName, C.Dt;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                    Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
                    Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteStartDt).Substring(0, 8));
                    Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteEndDt).Substring(0, 8));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "EmpCode", 
                         "EmpCodeOld", "EmpName", "DeptName", "ResignDt", "DtList",
                         "In1", "Out1", "OT"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Grd1.Rows.Add();
                            int Row = Grd1.Rows.Count - 1;
                            Grd1.Cells[Row, 22].Value = Row + 1;
                            Grd1.Cells[Row, 0].Value = dr.GetString(c[0]);
                            Grd1.Cells[Row, 1].Value = dr.GetString(c[1]);
                            Grd1.Cells[Row, 2].Value = dr.GetString(c[2]);
                            Grd1.Cells[Row, 4].Value = dr.GetString(c[3]);

                            if (dr.GetString(c[4]) == "0")
                                Grd1.Cells[Row, 21].Value = null;
                            else
                                Grd1.Cells[Row, 21].Value = dr.GetString(c[4]);
                            if (dr.GetString(c[5]).Length == 0)
                                Grd1.Cells[Row, 20].Value = null;
                            else
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 20, 5);

                            Grd1.Cells[Row, 21].Value = dr.GetString(c[4]);
                            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 24, 8);

                            if (Sm.GetGrdStr(Grd1, Row, 24).Length > 0 && Sm.GetGrdStr(Grd1, Row, 11).Length > 0 &&
                                Decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 24))) < Decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 11))))
                            {
                                Grd1.Cells[Row, 25].Value = Sm.ConvertDate(dr.GetString(c[5])).AddDays(1);
                            }
                            else
                            {
                                Grd1.Cells[Row, 25].Value = Sm.ConvertDate(dr.GetString(c[5]));
                            }
                            Grd1.Cells[Row, 27].Value = string.Empty;
                            Grd1.Cells[Row, 28].Value = string.Empty;
                            Grd1.Cells[Row, 29].Value = string.Empty; 
                            CekResign(Row);

                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                Grd1.Rows.Add();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void ReListEmp()
        {
            var lWS = new List<WS>();
            var lLog = new List<Log>();
            var lEL = new List<EmpLeave>();
            var lOT = new List<EmpOT>();
            Process1(ref lWS);
            if (lWS.Count > 0)
            {
                Process2(ref lLog);
                Process3(ref lWS, ref lLog);
                Process4(ref lWS);
                Process5(ref lEL);
                Process6(ref lEL);
                Process7(ref lOT);
                Process8(ref lOT);
            }
            lWS.Clear();
            lLog.Clear();
            lEL.Clear();
            lOT.Clear();
            RoundingTime();
        }

        private void Process1(ref List<WS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select A.Dt, A.EmpCode, ");
            SQL.AppendLine("B.In1 As LogIn, B.bIn1 As bLogIn, B.aIn1 As aLogIn, ");
            SQL.AppendLine("Case When B.Out3 Is Null Then B.Out1 Else B.Out3 End As LogOut, ");
            SQL.AppendLine("Case When B.bOut3 Is Null Then B.bOut1 Else B.bOut3 End As bLogOut, ");
            SQL.AppendLine("Case When B.aOut3 Is Null Then B.aOut1 Else B.aOut3 End As aLogOut ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WsCode=B.WsCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",
 
                    //1-5
                    "EmpCode", "LogIn", "bLogIn", "aLogIn", "LogOut", 
                    
                    //6-7
                    "bLogOut", "aLogOut"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WS()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            LogIn = Sm.DrStr(dr, c[2]),
                            bLogIn = Sm.DrStr(dr, c[3]),
                            aLogIn = Sm.DrStr(dr, c[4]),
                            LogOut = Sm.DrStr(dr, c[5]),
                            bLogOut = Sm.DrStr(dr, c[6]),
                            aLogOut = Sm.DrStr(dr, c[7]),
                            IsOneDay = true,
                            IsWarning = false,
                            Dt2 = string.Empty,
                            AGCode = string.Empty,
                            AGName = string.Empty,
                            InDtL = string.Empty,
                            InTmL = string.Empty,
                            OutDtL = string.Empty,
                            OutTmL = string.Empty,
                            OTIn = string.Empty,
                            OTOut = string.Empty,
                            OTRemark = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Log> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteStartDt)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteEndDt)).AddDays(1)));

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt, A.Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Log()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DtTm = Sm.Left(Sm.DrStr(dr, c[1]) + Sm.DrStr(dr, c[2]), 12)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<WS> lWS, ref List<Log> lLog)
        {
            string
                EmpCode = string.Empty,
                Dt = string.Empty,
                Dt0 = string.Empty,
                Dt2 = string.Empty,
                LogIn = string.Empty,
                bLogIn = string.Empty,
                aLogIn = string.Empty,
                LogOut = string.Empty,
                bLogOut = string.Empty,
                aLogOut = string.Empty
                ;

            bool IsOneDay = true;

            string Now = Sm.ServerCurrentDateTime();

            if (Now.Length > 0) Now = Sm.Left(Now, 12);

            for (var i = 0; i < lWS.Count; i++)
            {
                EmpCode = lWS[i].EmpCode;
                Dt = lWS[i].Dt;
                Dt0 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(-1)), 8);
                Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(1)), 8);

                IsOneDay = (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].LogOut) <= 0);

                lWS[i].Dt2 = Dt2;
                lWS[i].IsOneDay = IsOneDay;

                LogIn = Dt + lWS[i].LogIn;
                if (Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn) <= 0)
                    bLogIn = Dt + lWS[i].bLogIn;
                else
                    bLogIn = Dt0 + lWS[i].bLogIn;

                if (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].aLogIn) <= 0)
                    aLogIn = Dt + lWS[i].aLogIn;
                else
                    aLogIn = Dt2 + lWS[i].aLogIn;

                if (IsOneDay)
                    LogOut = Dt + lWS[i].LogOut;
                else
                    LogOut = Dt2 + lWS[i].LogOut;

                if (IsOneDay)
                    bLogOut = Dt + lWS[i].bLogOut;
                else
                {
                    if (Sm.CompareDtTm(lWS[i].bLogOut, lWS[i].LogOut) <= 0)
                        bLogOut = Dt2 + lWS[i].bLogOut;
                    else
                        bLogOut = Dt + lWS[i].bLogOut;
                }

                if (IsOneDay)
                {
                    if (Sm.CompareDtTm(lWS[i].LogOut, lWS[i].aLogOut) <= 0)
                        aLogOut = Dt + lWS[i].aLogOut;
                    else
                        aLogOut = Dt2 + lWS[i].aLogOut;
                }
                else
                    aLogOut = Dt2 + lWS[i].aLogOut;

                if (lLog.Count > 0)
                {
                    if (!mIsAtdShowDataLogWhenHoliday)
                    {
                        //IN
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogIn) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogIn) <= 0
                                )
                            .OrderBy(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].InDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].InTmL = Sm.Right(index.DtTm, 4);
                        }
                        //OUT
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogOut) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogOut) <= 0
                                )
                            .OrderByDescending(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].OutDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].OutTmL = Sm.Right(index.DtTm, 4);
                        }
                    }
                    else
                    {
                        for (int data = 0; data < lLog.Count; data++)
                        {
                            //region IN
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                            {
                                lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogIn.Length < 9)
                                {
                                    lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].InTmL = Sm.GetValue("Select MIN(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                                    {
                                        lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }


                            // Region  OUT
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                            {
                                lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogOut.Length < 9)
                                {
                                    lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].OutTmL = Sm.GetValue("Select MAX(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                                    {
                                        lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }
                            // OUT   
                        }
                    }
                }

                if (LogIn.Length == 12 &&
                    Sm.CompareDtTm(LogIn, Now) < 0 &&
                    (lWS[i].InDtL.Length == 0 || lWS[i].InTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 &&
                    Sm.CompareDtTm(LogOut, Now) < 0 &&
                    (lWS[i].OutDtL.Length == 0 || lWS[i].OutTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogIn.Length == 12 && lWS[i].InDtL.Length > 0 && lWS[i].InTmL.Length > 0 &&
                    Sm.CompareDtTm(LogIn, lWS[i].InDtL + lWS[i].InTmL) < 0)
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 && lWS[i].OutDtL.Length > 0 && lWS[i].OutTmL.Length > 0 &&
                    Sm.CompareDtTm(lWS[i].OutDtL + lWS[i].OutTmL, LogOut) < 0)
                    lWS[i].IsWarning = true;
            }
        }

        private void Process4(ref List<WS> l)
        {
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), l[i].EmpCode) &&
                        Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), l[i].Dt))
                    {
                            SetDt(Row, 5, l[i].Dt);
                            SetTm(Row, 6, l[i].InTmL);
                            SetTm(Row, 14, l[i].InTmL);
                            SetTm(Row, 17, l[i].InTmL);
                            SetTm(Row, 7, l[i].OutTmL);
                            SetTm(Row, 15, l[i].OutTmL);
                            SetTm(Row, 19, l[i].OutTmL);
                            SetDt(Row, 16, l[i].InDtL);
                            SetDt(Row, 18, l[i].OutDtL);

                        if (l[i].IsWarning) Grd1.Rows[Row].BackColor = Color.Red;

                        break;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        //list employee leave
        private void Process5(ref List<EmpLeave> lEL)
        {
            lEL.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.Left(Sm.GetDte(DteStartDt), 8));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteEndDt), 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select A.EmpCode, B.LeaveDt, C.leavename, A.LeaveCode ");
            SQL.AppendLine("From TblEmpLeavehdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join Tblleave C On A.LeaveCode = C.leaveCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.Empcode=D.EmpCode ");
            SQL.AppendLine("Where A.cancelind = 'N' And A.status = 'A' And D.DeptCode=@DeptCode ");
            SQL.AppendLine("And B.LeaveDt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode,  B.LeaveDt, C.leaveName ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "LeaveDt", "LeaveName", "LeaveCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEL.Add(new EmpLeave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            LeaveDt = Sm.DrStr(dr, c[1]),
                            LeaveName = Sm.DrStr(dr, c[2]),
                            LeaveCode = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        //bandingin list employee dgn list employee leave
        private void Process6(ref List<EmpLeave> lEL)
        {
            string[] mLeaves = { };

            if (mLeaveCodeGetMealTransport.Length > 0)
            {
                mLeaves = mLeaveCodeGetMealTransport.Split(',');
            }

            Grd1.BeginUpdate();
            for (var i = 0; i < lEL.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), lEL[i].EmpCode) &&
                        Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), lEL[i].LeaveDt))
                    {
                        Grd1.Rows[Row].BackColor = Color.DodgerBlue;
                        Grd1.Cells[Row, 26].Value = lEL[i].LeaveName;
                        if (mLeaveCodeGetMealTransport.Length > 0)
                        {
                            foreach (string mLeave in mLeaves)
                            {
                                if (lEL[i].LeaveCode == mLeave)
                                {
                                    Grd1.Rows[Row].BackColor = Color.SpringGreen;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        //list employee OT
        private void Process7(ref List<EmpOT> lOT)
        {
            lOT.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.Left(Sm.GetDte(DteStartDt), 8));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteEndDt), 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select B.EmpCode, A.OtDt, B.OtStartTm, B.OTEndTm, A.remark ");
            SQL.AppendLine("From TblOTRequestHdr A ");
            SQL.AppendLine("Inner Join TblOTrequestDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Where A.cancelind = 'N' And A.status = 'A' And A.DeptCode=@DeptCode ");
            SQL.AppendLine("And A.OtDt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By B.EmpCode,  A.OTDt ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "OTDt", "OtStartTm", "OTEndTm", "Remark" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lOT.Add(new EmpOT()
                        {
                            OTEmpCode = Sm.DrStr(dr, c[0]),
                            OTDt = Sm.DrStr(dr, c[1]),
                            OTIn = Sm.DrStr(dr, c[2]),
                            OTOut = Sm.DrStr(dr, c[3]),
                            OTRemark = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        //bandingin list employee dengan list employee  OT
        private void Process8(ref List<EmpOT> lOT)
        {
            Grd1.BeginUpdate();
            for (var i = 0; i < lOT.Count; i++)
            {
                for (int Rowx = 0; Rowx < Grd1.Rows.Count - 1; Rowx++)
                {
                    string dt1 = Sm.GetGrdDate(Grd1, Rowx, 20).Substring(0, 8);
                    string EmpCode = Sm.GetGrdStr(Grd1, Rowx, 0);
                    if (EmpCode == lOT[i].OTEmpCode && dt1 == lOT[i].OTDt)
                    {
                        SetTm(Rowx, 27, lOT[i].OTIn);
                        SetTm(Rowx, 28, lOT[i].OTOut);
                        Grd1.Cells[Rowx, 29].Value =  lOT[i].OTRemark;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void SetDt(int r, int c, string Dt)
        {
            if (Dt.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.ConvertDate(Dt);
        }

        private void SetTm(int r, int c, string Tm)
        {
            if (Tm.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.Left(Tm, 2) + ":" + Tm.Substring(2, 2);
        }

        private void ComputeVs(int Col1, int Col2, int Col3)
        {
            TimeSpan TS = new TimeSpan();
            var Value = string.Empty;
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //CekOT(Row);
                if (Sm.GetGrdStr(Grd1, Row, Col1).Length != 0 && Sm.GetGrdStr(Grd1, Row, Col2).Length != 0
                   )
                {
                    TS = DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col2)).Subtract(DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col1)));
                    Value = TS.ToString();
                    if (Value.Substring(0, 1) == "-")
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 6);
                    }
                    else
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 5);
                    }
                }
            }
            Grd1.EndUpdate();
        }


        #endregion 

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Additional Method

        private void CekResign(int Row)
        {
            decimal DateRow = 0m;
            decimal DateResign = 0m;

            if (Sm.GetGrdDate(Grd1, Row, 20).Length > 1)
            {
                DateRow = Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8));
            }
            if (Sm.GetGrdStr(Grd1, Row, 21).Length > 1)
            {
                DateResign = Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 21));
            }
            Grd1.BeginUpdate();
            if (DateResign > 0 && DateRow >= DateResign)
            {
                Grd1.Rows[Row].ReadOnly = iGBool.True;
                Grd1.Rows[Row].CellStyle.BackColor = Color.Violet;
            }
            Grd1.EndUpdate();
        }

        private void RoundingTime()
        {
            if (RoundSch != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0)
                    {
                        string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 14), 2);
                        if (Convert.ToInt32(MMIn) < 30)
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                            string LogIn = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 14].Value = LogIn.Substring(0, 5);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 14);
                        }
                        else
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(2 * RoundSch), 0);
                            string LogIn = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 14].Value = LogIn.Substring(0, 5);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 14);
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 15).Length > 0)
                    {
                        string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 15), 2);
                        if (Convert.ToInt32(MMIn) < 30)
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 15)).Substring(0, 2)), 0, 0);
                            string LogOut = Convert.ToString(TS);
                            Grd1.Cells[Row, 15].Value = LogOut.Substring(0, 5);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 15);
                        }
                        else
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 15)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                            string LogOut = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 15].Value = LogOut.Substring(0, 5);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 15);
                        }
                    }

                }
            }
            else if (RoundSch == 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    {

                        Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 6);
                        Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 6);
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    {

                        Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 7);
                        Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 7);
                    }
                }
            }
            else
            {

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0 && Sm.GetGrdStr(Grd1, Row, 10).Length > 0)
                    {
                        if (Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 6).Replace(":", "")) < Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 10).Replace(":", "")))
                        {
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 10);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 10);
                        }
                        else
                        {
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 6);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 6);
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0 && Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                    {
                        if (Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 7).Replace(":", "")) > Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 11).Replace(":", "")))
                        {
                            Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 11);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 11);
                        }
                        else
                        {
                            Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 7);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 7);
                        }
                    }
                }
            }
        }


        static string RemoveQuotes(string input)
        {
            int index = 0;
            char[] result = new char[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != ':')
                {
                    result[index++] = input[i];
                }
            }
            return new string(result, 0, index);
        }

        private void SetLueAGCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            }
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #endregion

        #region Event
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), DeptCode);               
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkAGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group Attendance");
        }
        #endregion


        #region Class

        private class Employee
        {
            public string EmpCode { set; get; }
        }

        private class Date
        {
            public string DayName { set; get; }
        }

        private class WS
        {
            public string Dt { get; set; }
            public string Dt2 { get; set; }
            public string EmpCode { get; set; }
            public bool IsOneDay { get; set; }
            public string LogIn { get; set; }
            public string bLogIn { get; set; }
            public string aLogIn { get; set; }
            public string LogOut { get; set; }
            public string bLogOut { get; set; }
            public string aLogOut { get; set; }
            public string AGCode { get; set; }
            public string AGName { get; set; }
            public string InDtL { get; set; }
            public string InTmL { get; set; }
            public string OutDtL { get; set; }
            public string OutTmL { get; set; }
            public string OTIn { get; set; }
            public string OTOut { get; set; }
            public string OTRemark { get; set; }
            public bool IsWarning { get; set; }
        }

        private class Log
        {
            public string EmpCode { get; set; }
            public string DtTm { get; set; }
        }

        private class EmpLeave
        {
            public string EmpCode { get; set; }
            public string LeaveDt { get; set; }
            public string LeaveName { get; set; }
            public string LeaveCode { get; set; }
        }

        private class EmpOT
        {
            public string OTEmpCode { get; set; }
            public string OTDt { get; set; }
            public string OTIn { get; set; }
            public string OTOut { get; set; }
            public string OTRemark { get; set; }
        }

        #endregion

        
        
    }

  
}
