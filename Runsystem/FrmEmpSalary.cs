﻿#region Update
/*
    21/08/2017 [TKG] tambah site di allowance
    22/08/2017 [TKG] 1 Allowance bisa dibagi menjadi lbh dari  1 site utk masing2 employee.
    25/08/2017 [TKG] Utk Allowance ditambah end date.
    28/08/2017 [TKG] Update entity berdasarkan site di master employee.
    26/09/2017 [TKG] tambah employee's salary for social security.
    12/10/2017 [TKG] bug fixing saat simpan employee's allowance deduction.
    16/10/2017 [TKG] filter berdasarkan otorisasi group thd department
    20/10/2017 [TKG] tambah employee's claim.
    25/10/2017 [TKG] tambah performance.
    14/12/2017 [TKG] default untuk allowance/deduction start/end date
    09/03/2018 [TKG] tambah startdate dan end date pada tab deduction
    04/10/2018 [DITA] tambah print out untuk HIN
    02/11/2018 [HAR] tambah validasi hide tab sesuai setting parameter
    17/12/2018 [WED] EmpSalary bisa di panggil setelah PPS, berdasarkan parameter mIsPPSAutoShowEmpSalary
    06/09/2019 [HAR] tambah parameter mIsSalaryAllwDedcFromGradeToo, jika aktif akan cek ke grade level, jik AD sdh di set disana maka tdk bs save
    08/10/2019 [HAR/SIER] tambah parameter EmployeeDeductionHideGroup untuk mendaftar group user yang hanya bisa lihat tab deduction
    09/10/2019 [HAR/SIER] Feedback : tab yang kebuka hanya tab deduction, dan group yg  hanya kebuka deductionnya musti di set di param
    05/02/2020 [WED/SRN] Tab Claim ditambah Start Date dan End Date (mandatory) berdasarkan parameter IsEmpClaimUsePeriod
    21/11/2020 [TKG/PHT] Tambah tombol untuk generate tunjangan pensiun
    30/11/2020 [TKG/PHT] bug generate tunjangan pensiun (tidak memperhitungkan data yg baru diinput yg belum disimpan di db).
    25/01/2022 [TKG/PHT] merubah GetParameter() dan proses save
    02/03/2022 [MYA/PHT] Membuat SMK Statis di master level yang nantinya akan berefek kepada semua karyawan mendapatkan tunjangan SMK statis sesuai levelnya
    24/03/2022 [IBL/PHT] Basic salary ambil dari Grade Salary berdasarkan hasil dari proses kenaikan berkala
    28/03/2022 [MYA/PHT] FEEDBACK : Tunjangan di ESADA kosong setelah mengaktifkan param SMKStatisAmtSource
    29/03/2022 [VIN/PHT] tambah info update periodic query
    04/04/2022 [IBL/PHT] Ubah query pengambilan nilai basic salary ketika IsUsePeriodicAdvancementMenu = true.
    30/06/2022 [TYO/PHT] menampilkan data terbaru berdasarkan parameter IsESADAUseNewestData
    22/07/2022 [ICA/PHT] Show data allowancededuction tidak based on param SMKStatisAmtSource, karena sudah di update di pps dan event (otomatisasi esada)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSalary : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mPPSEmpCode = string.Empty;
        internal FrmEmpSalaryFind FrmFind;
        private string
            mSalaryInd = "1",
            mEmpADStartDtDefault = string.Empty,
            mEmpADEndDtDefault = string.Empty,
            mEmpSalaryGroupAccess = string.Empty,
            mEmployeeDeductionHideGroup = string.Empty,
            mADCodePensionBenefit = string.Empty,
            mADCodePensionBenefitComponent = string.Empty,
            mSMKStatisAmtSource = string.Empty;
        internal bool
            mIsAllowancePerSite = false,
            mIsAllowanceMultiplePeriod = false,
            mIsUseEmpSalarySS = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsUseEmpPerformance = false,
            mIsEmpSalaryHideTab = false,
            mIsPPSAutoShowEmpSalary = false,
            mIsSalaryAllwDedcFromGradeToo = false,
            mIsEmpClaimUsePeriod = false,
            mIsESADAUseNewestData = false;
        private bool
            mIsEmpSalaryPensionBenefitEnabled = false,
            mIsUsePeriodicAdvancementMenu = Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmPeriodicSalaryAdvancement' Or Param = 'FrmPeriodicGradeAdvancement';")
            ;
        
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmpSalary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0)
                this.Text = "Employee Salary, Allowance And Deduction Amendment";
            else
                this.Text = this.Text.Replace("&&", "&");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 7 });
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3 });

                TcEmpSalary.SelectedTabPage = TpAllowance;
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                LueSiteCode.Visible = false;
                DteStartDt2.Visible = false;

                TcEmpSalary.SelectedTabPage = TpClaim;
                Sl.SetLueClaimCode(ref LueClaimCode, string.Empty);
                LueClaimCode.Visible = false; 
                
                if (mIsUseEmpSalarySS)
                {
                    TcEmpSalary.SelectedTabPage = TpSalarySS;
                    Sl.SetLueSSPCode(ref LueSSPCode);
                    LueSSPCode.Visible = false;
                    DteStartDt3.Visible = false;
                    DteEndDt3.Visible = false;
                }
                else
                    TpSalarySS.PageVisible = false;

                if (mIsUseEmpPerformance)
                {
                    TcEmpSalary.SelectedTabPage = TpPerformance;
                    Sl.SetLueOption(ref LuePerformanceStatus, "PerformanceStatus");
                    Sl.SetLueOption(ref LueGradePerformance, "GradePerformance");
                    LueGradePerformance.Visible = false;
                }
                else
                    TpPerformance.PageVisible = false;
                
                TcEmpSalary.SelectedTabPage = TpSalary;
                DteStartDt.Visible = false;

                hideTab();
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mPPSEmpCode.Length > 0 && mIsPPSAutoShowEmpSalary)
                {
                    SetFormControl(mState.Insert);
                    Sm.SetDteCurrentDate(DteDocDt);

                    TxtEmpCode.EditValue = mPPSEmpCode;
                    ShowEmployeeDetail(mPPSEmpCode);
                    ShowEmployeeSalary(mPPSEmpCode);
                    ShowEmployeeAllowanceDeduction(mPPSEmpCode, "A");
                    ShowEmployeeAllowanceDeduction(mPPSEmpCode, "D");
                    ShowEmployeeSalarySS(mPPSEmpCode);
                    ShowEmployeeClaim(mPPSEmpCode);
                    ShowEmployeePerformance(mPPSEmpCode);
                }
                else SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsEmpClaimUsePeriod', 'IsEmpSalaryPensionBenefitEnabled', 'SalaryInd', 'ADCodePensionBenefit', 'ADCodePensionBenefitComponent', ");
            SQL.AppendLine("'IsEmpSalaryHideTab', 'EmpSalaryGroupAccess', 'IsPPSAutoShowEmpSalary', 'IsSalaryAllwDedcFromGradeToo', 'EmployeeDeductionHideGroup', ");
            SQL.AppendLine("'IsUseEmpPerformance', 'EmpADStartDtDefault', 'EmpADEndDtDefault', 'IsAllowanceMultiplePeriod', 'IsAllowancePerSite', ");
            SQL.AppendLine("'IsUseEmpSalarySS', 'IsFilterByDeptHR', 'IsFilterBySiteHR', 'SMKStatisAmtSource', 'IsESADAUseNewestData' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsUseEmpSalarySS": mIsUseEmpSalarySS = ParValue == "Y"; break;
                            case "IsUseEmpPerformance": mIsUseEmpPerformance = ParValue == "Y"; break;
                            case "IsAllowanceMultiplePeriod": mIsAllowanceMultiplePeriod = ParValue == "Y"; break;
                            case "IsAllowancePerSite": mIsAllowancePerSite = ParValue == "Y"; break;
                            case "IsEmpSalaryHideTab": mIsEmpSalaryHideTab = ParValue == "Y"; break;
                            case "IsPPSAutoShowEmpSalary": mIsPPSAutoShowEmpSalary = ParValue == "Y"; break;
                            case "IsSalaryAllwDedcFromGradeToo": mIsSalaryAllwDedcFromGradeToo = ParValue == "Y"; break;
                            case "IsEmpClaimUsePeriod": mIsEmpClaimUsePeriod = ParValue == "Y"; break;
                            case "IsEmpSalaryPensionBenefitEnabled": mIsEmpSalaryPensionBenefitEnabled = ParValue == "Y"; break;
                            case "IsESADAUseNewestData": mIsESADAUseNewestData = ParValue == "Y"; break;

                            //string
                            case "EmpADStartDtDefault": mEmpADStartDtDefault = ParValue; break;
                            case "EmpADEndDtDefault": mEmpADEndDtDefault = ParValue; break;
                            case "ADCodePensionBenefitComponent": mADCodePensionBenefitComponent = ParValue; break;
                            case "ADCodePensionBenefit": mADCodePensionBenefit = ParValue; break;
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "EmployeeDeductionHideGroup": mEmployeeDeductionHideGroup = ParValue; break;
                            case "EmpSalaryGroupAccess": mEmpSalaryGroupAccess = ParValue; break;
                            case "SMKStatisAmtSource" : mSMKStatisAmtSource = ParValue;break;

                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid 1 - Salary

            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ 
                        "DNo", 
                        "Start Date", 
                        "Monthly"+Environment.NewLine+"Basic Salary",
                        "Daily"+Environment.NewLine+"Basic Salary",
                        "GradeSalaryInd"
                    },
                    new int[]{ 0, 100, 200, 200, 0 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4 }, false);

            #endregion

            #region Grid 2 - Allowance

            Grd2.Cols.Count = 10;
            
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    { 
                        "DNo", 
                        "", "Code", "Name", "Amount", "Start Date", 
                        "End Date", "Site Code", "Site", "" 
                    },
                    new int[] 
                    { 
                        0, 
                        20, 100, 200, 150, 100, 
                        100, 0, 180, 20 
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1, 9 });
            Sm.GrdFormatDate(Grd2, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd2, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 7 }, false);
            if (!mIsAllowancePerSite) Sm.GrdColInvisible(Grd2, new int[] { 8 }, false);
            if (!mIsAllowanceMultiplePeriod) Sm.GrdColInvisible(Grd2, new int[] { 6 }, false);
            if (!mIsEmpSalaryPensionBenefitEnabled) Sm.GrdColInvisible(Grd2, new int[] { 9 }, false);

            #endregion

            #region Grid 3 - Deduction

            Grd3.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo", 
                        //1-5
                        "", "Code", "Name", "Amount", "Start Date", 
                        //6
                        "End Date" 
                    },
                    new int[] 
                    {
                        0, 
                        20, 100, 200, 170, 100, 
                        100 
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 4 }, 0);
            Sm.GrdFormatDate(Grd3, new int[] { 5, 6 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2 }, false);

            #endregion

            #region Grid 4 - Approval information

            Grd4.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 200, 80, 80, 300 }
                );
            Sm.GrdFormatDate(Grd4, new int[] { 2 });
            Sm.GrdColInvisible(Grd4, new int[] { 3 }, false);

            #endregion

            #region Grid 5 - Salary SS

            Grd5.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "Code", 
                    "Name", 
                    "Start", 
                    "End", 
                    "Amount"
                },
                new int[] 
                { 
                    //0
                    0, 

                    //1-5
                    0, 200, 80, 80, 130
                }
            );
            Sm.GrdFormatDate(Grd5, new int[] { 3, 4 });
            Sm.GrdFormatDec(Grd5, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1 });

            #endregion

            #region Grid 6 - Claim

            Grd6.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                Grd6,
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "Code", 
                    "Name", 
                    "Year",
                    "Amount",
                    "Start Date",

                    //6
                    "End Date"
                },
                new int[] 
                { 
                    //0
                    0, 

                    //1-5
                    0, 200, 80, 130, 100, 

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd6, new int[] { 4 }, 0);
            Sm.GrdFormatDate(Grd6, new int[] { 5, 6 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1 });
            if (!mIsEmpClaimUsePeriod) Sm.GrdColInvisible(Grd6, new int[] { 5, 6 });

            #endregion

            #region Grid 7 - Performance

            Grd7.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                Grd7,
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-2
                    "Grade", 
                    "Value"
                },
                new int[] 
                { 
                    //0
                    0, 

                    //1-2
                    120, 130
                }
            );
            Sm.GrdFormatDec(Grd7, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd7, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark, LuePerformanceStatus }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 4, 5, 6, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 4, 5, 6 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 1, 2 });
                    BtnEmpCode.Enabled = false;
                    TxtEmpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark, LuePerformanceStatus }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 4, 5, 6, 8, 9 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 4, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1, 2 });
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, TxtStatus, DteDocDt, TxtEmpCode, TxtEmpName, 
                 TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, 
                 MeeRemark, LuePerformanceStatus
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4 });
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4 });
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.ClearGrd(Grd4, false);
            Sm.FocusGrd(Grd4, 0, 0);
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 5 });
            Sm.FocusGrd(Grd5, 0, 1);
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 4 });
            Sm.FocusGrd(Grd6, 0, 1);
            Sm.ClearGrd(Grd7, true);
            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 2 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        internal void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpSalaryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            BtnInsertClick();
        }

        internal void BtnInsertClick()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmEmpSalaryDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData(BtnEmpCode.Enabled?"I":"E", sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0) ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void SaveData(string Process, object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpSalary", "TblEmpSalaryHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpSalaryHdr(DocNo));

            cml.Add(SaveEmpSalaryDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveEmpSalaryDtl(DocNo, Row));

            int DNo = 0;
            cml.Add(SaveEmpSalary2Dtl(DocNo, ref DNo, ref Grd2, "A"));
            cml.Add(SaveEmpSalary2Dtl(DocNo, ref DNo, ref Grd3, "D"));

            //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
            //    {
            //        DNo += 1;
            //        cml.Add(SaveEmpSalary2Dtl(DocNo, DNo, ref Grd2, Row, "A"));
            //    }
            //}

            //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
            //    {
            //        DNo += 1;
            //        cml.Add(SaveEmpSalary2Dtl(DocNo, DNo, ref Grd3, Row, "D"));
            //    }
            //}

            cml.Add(SaveEmpSalary3Dtl(DocNo));
            //for (int r = 0; r < Grd5.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd5, r, 1).Length > 0) 
            //        cml.Add(SaveEmpSalary3Dtl(DocNo, r));

            cml.Add(SaveEmpSalary4Dtl(DocNo));

            //for (int r = 0; r < Grd6.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd6, r, 1).Length > 0) 
            //        cml.Add(SaveEmpSalary4Dtl(DocNo, r));

            if (Sm.GetGrdStr(Grd7, 0, 1).Length > 0) 
                cml.Add(SaveEmpSalary5Dtl(DocNo, 0));

            cml.Add(UpdateEmployeeSalaryActInd());
            cml.Add(SaveEmpSalaryApproval(DocNo));

            if(mIsUsePeriodicAdvancementMenu)
                cml.Add(UpdatePeriodicAdvancment(TxtEmpCode.Text));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee's code", false) ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Salary data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Allowance data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Deduction data entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd5.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Salary for social security data entered (" + (Grd5.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd6.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Claim data entered (" + (Grd6.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd7.Rows.Count > 2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Performance data entered (" + (Grd7.Rows.Count - 1).ToString() + ") exceeds the maximum limit (1).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            string
                StartDt1 = string.Empty,
                EndDt1 = string.Empty,
                StartDt2 = string.Empty,
                EndDt2 = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Started date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Monthly basic salary should not be 0.")) return true;
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                string ADCode = string.Empty, SiteCode = string.Empty,  EmpCode = TxtEmpCode.Text; 
                for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, i, 3, false, "Allowance name is empty.")) return true;
                    if (mIsAllowancePerSite)
                    {
                        ADCode = Sm.GetGrdStr(Grd2, i, 2);
                        SiteCode = Sm.GetGrdStr(Grd2, i, 7);
                        if (ADCode.Length != 0)
                        {
                            for (int j = i; j < Grd2.Rows.Count - 1; j++)
                            {
                                if (i != j)
                                {
                                    if (Sm.CompareStr(ADCode, Sm.GetGrdStr(Grd2, j, 2)) &&
                                        Sm.CompareStr(SiteCode, Sm.GetGrdStr(Grd2, j, 7)))
                                    {
                                        Sm.StdMsg(mMsgType.Warning,
                                            "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                                            "Site : " + Sm.GetGrdStr(Grd2, i, 8) + Environment.NewLine + Environment.NewLine +
                                            "Duplicate site.");
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Sm.IsGrdValueEmpty(Grd2, i, 5, false,
                            "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                            "Started date is empty.")) return true;
                        if (mIsAllowanceMultiplePeriod)
                        {
                            if (Sm.IsGrdValueEmpty(Grd2, i, 6, false,
                            "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                            "End date is empty.")) return true;

                            StartDt1 = Sm.GetGrdDate(Grd2, i, 5).Substring(0, 8);
                            EndDt1 = Sm.GetGrdDate(Grd2, i, 6).Substring(0, 8);

                            if (Sm.CompareDtTm(EndDt1, StartDt1) < 0)
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                                    "End date should not be earlier than start date.");
                                return true;
                            }

                            for (int j = i; j < Grd2.Rows.Count - 1; j++)
                            {
                                if (i != j)
                                {
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd2, i, 3), Sm.GetGrdStr(Grd2, j, 3)))
                                    {
                                        StartDt2 = Sm.GetGrdDate(Grd2, j, 5);
                                        EndDt2 = Sm.GetGrdDate(Grd2, j, 6);

                                        if (StartDt2.Length > 0 && EndDt2.Length > 0)
                                        {
                                            StartDt2 = StartDt2.Substring(0, 8);
                                            EndDt2 = EndDt2.Substring(0, 8);
                                            if (
                                                (Sm.CompareDtTm(StartDt2, StartDt1) <= 0 && Sm.CompareDtTm(StartDt1, EndDt2) <= 0) ||
                                                (Sm.CompareDtTm(StartDt1, StartDt2) <= 0 && Sm.CompareDtTm(StartDt2, EndDt1) <= 0)
                                                )
                                            {
                                                Sm.StdMsg(mMsgType.Warning,
                                                    "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                                                    "Invalid period.");
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (mIsSalaryAllwDedcFromGradeToo)
                    {
                        ADCode = Sm.GetGrdStr(Grd2, i, 2);
                        if (ADCode.Length > 0 && EmpCode.Length > 0)
                        {
                            string dataAllowance = Sm.GetValue("Select * from tblEmployee A " +
                            "Inner Join TblGradeLevelHdr B On A.grdLvlCode = B.GrdLvlCode " +
                            "Inner Join TblGradeLevelDtl C On B.grdLvlCode = C.GrdLvlCode " +
                            "Where A.EmpCode = '" + EmpCode + "' And C.AdCode = '" + ADCode + "' ");

                            if (dataAllowance.Length > 0)
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                                        "Allowance : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine +
                                                        "Already exist in this employee grade.");
                                return true;
                            }
                        }
                    }
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                string ADCode = string.Empty, EmpCode = TxtEmpCode.Text; 
                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, r, 3, false, "Deduction name is empty.")) return true;
                    
                    //if (Sm.IsGrdValueEmpty(Grd3, Row, 4, true, "Deduction amount should not be 0.")) return true;

                    if (Sm.IsGrdValueEmpty(Grd3, r, 5, false,
                            "Deduction : " + Sm.GetGrdStr(Grd3, r, 3) + Environment.NewLine +
                            "Started date is empty.")) return true;

                    if (mIsAllowanceMultiplePeriod)
                    {
                        if (Sm.IsGrdValueEmpty(Grd3, r, 6, false,
                        "Deduction : " + Sm.GetGrdStr(Grd3, r, 3) + Environment.NewLine +
                        "End date is empty.")) return true;

                        StartDt1 = Sm.GetGrdDate(Grd3, r, 5).Substring(0, 8);
                        EndDt1 = Sm.GetGrdDate(Grd3, r, 6).Substring(0, 8);

                        if (Sm.CompareDtTm(EndDt1, StartDt1) < 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Deduction : " + Sm.GetGrdStr(Grd3, r, 3) + Environment.NewLine +
                                "End date should not be earlier than start date.");
                            return true;
                        }

                        for (int j = r; j < Grd3.Rows.Count - 1; j++)
                        {
                            if (r != j)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, r, 3), Sm.GetGrdStr(Grd3, j, 3)))
                                {
                                    StartDt2 = Sm.GetGrdDate(Grd3, j, 5);
                                    EndDt2 = Sm.GetGrdDate(Grd3, j, 6);

                                    if (StartDt2.Length > 0 && EndDt2.Length > 0)
                                    {
                                        StartDt2 = StartDt2.Substring(0, 8);
                                        EndDt2 = EndDt2.Substring(0, 8);
                                        if (
                                            (Sm.CompareDtTm(StartDt2, StartDt1) <= 0 && Sm.CompareDtTm(StartDt1, EndDt2) <= 0) ||
                                            (Sm.CompareDtTm(StartDt1, StartDt2) <= 0 && Sm.CompareDtTm(StartDt2, EndDt1) <= 0)
                                            )
                                        {
                                            Sm.StdMsg(mMsgType.Warning,
                                                "Deduction : " + Sm.GetGrdStr(Grd3, r, 3) + Environment.NewLine +
                                                "Invalid period.");
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (mIsSalaryAllwDedcFromGradeToo)
                    {
                        ADCode = Sm.GetGrdStr(Grd3, r, 2);
                        if (ADCode.Length > 0 && EmpCode.Length > 0)
                        {
                            string dataAllowance = Sm.GetValue("Select * from tblEmployee A " +
                            "Inner Join TblGradeLevelHdr B On A.grdLvlCode = B.GrdLvlCode " +
                            "Inner Join TblGradeLevelDtl C On B.grdLvlCode = C.GrdLvlCode " +
                            "Where A.EmpCode = '" + EmpCode + "' And C.AdCode = '" + ADCode + "' ");

                            if (dataAllowance.Length > 0)
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                                        "Deduction : " + Sm.GetGrdStr(Grd3, r, 3) + Environment.NewLine +
                                                        "Already exist in this employee grade.");
                                return true;
                            }
                        }
                    }
                }
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int r = 0; r < Grd5.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, r, 2, false, "Social security program is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd5, r, 3, false, "Start date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd5, r, 5, true, "Salary for social security should not be 0.")) return true;
                }
            }

            if (Grd6.Rows.Count > 1)
            {
                string ClaimCode = string.Empty, Yr = string.Empty, StartDt = string.Empty, EndDt = string.Empty;
                for (int i = 0; i < Grd6.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd6, i, 1, false, "Claim name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd6, i, 3, false, "Year is empty.")) return true;
                    if (mIsEmpClaimUsePeriod)
                    {
                        if (Sm.IsGrdValueEmpty(Grd6, i, 5, false, "Claim's Start Date is empty.")) return true;
                        if (Sm.IsGrdValueEmpty(Grd6, i, 6, false, "Claim's End Date is empty.")) return true;
                        if (Decimal.Parse(Sm.GetGrdDate(Grd6, i, 5)) > Decimal.Parse(Sm.GetGrdDate(Grd6, i, 6)))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Claim's End date is earlier than claim's start date.");
                            TcEmpSalary.SelectedTabPage = TpClaim;
                            return true;
                        }
                    }
                    ClaimCode = Sm.GetGrdStr(Grd6, i, 1);
                    Yr = Sm.GetGrdStr(Grd6, i, 3);
                    
                    if (mIsEmpClaimUsePeriod)
                    {
                        StartDt = Sm.GetGrdDate(Grd6, i, 5);
                        EndDt = Sm.GetGrdDate(Grd6, i, 6);
                    }

                    if (ClaimCode.Length != 0)
                    {
                        for (int j = i; j < Grd6.Rows.Count - 1; j++)
                        {
                            if (i != j)
                            {
                                if (!mIsEmpClaimUsePeriod)
                                {
                                    if (Sm.CompareStr(ClaimCode, Sm.GetGrdStr(Grd6, j, 1)) &&
                                        Sm.CompareStr(Yr, Sm.GetGrdStr(Grd6, j, 3)))
                                    {
                                        Sm.StdMsg(mMsgType.Warning,
                                            "Claim : " + Sm.GetGrdStr(Grd6, i, 2) + Environment.NewLine +
                                            "Year : " + Sm.GetGrdStr(Grd6, i, 3) + Environment.NewLine + Environment.NewLine +
                                            "Duplicate claim in the same year.");
                                        TcEmpSalary.SelectedTabPage = TpClaim;
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (Sm.CompareStr(ClaimCode, Sm.GetGrdStr(Grd6, j, 1)) &&
                                        Sm.CompareStr(StartDt, Sm.GetGrdDate(Grd6, j, 5)))
                                    {
                                        Sm.StdMsg(mMsgType.Warning,
                                            "Claim : " + Sm.GetGrdStr(Grd6, i, 2) + Environment.NewLine +
                                            "Start Date : " + string.Concat(Sm.GetGrdDate(Grd6, j, 5).Substring(6, 2), "/", Sm.GetGrdDate(Grd6, j, 5).Substring(4, 2), "/", Sm.Left(Sm.GetGrdDate(Grd6, j, 5), 4)) + Environment.NewLine + Environment.NewLine +
                                            "Duplicate claim's Start Date.");
                                        TcEmpSalary.SelectedTabPage = TpClaim;
                                        return true;
                                    }

                                    // Tanggal beririsan untuk Claim yg sama
                                    if(
                                        (Sm.CompareStr(ClaimCode, Sm.GetGrdStr(Grd6, j, 1)) &&
                                        (
                                            Decimal.Parse(Sm.GetGrdDate(Grd6, j, 5)) > Decimal.Parse(StartDt) && 
                                            Decimal.Parse(Sm.GetGrdDate(Grd6, j, 5)) < Decimal.Parse(EndDt)
                                        )
                                        ) ||
                                        (Sm.CompareStr(ClaimCode, Sm.GetGrdStr(Grd6, j, 1)) &&
                                        (
                                            Decimal.Parse(Sm.GetGrdDate(Grd6, j, 6)) > Decimal.Parse(StartDt) &&
                                            Decimal.Parse(Sm.GetGrdDate(Grd6, j, 6)) < Decimal.Parse(EndDt)
                                        )
                                        )
                                       )
                                    {
                                        Sm.StdMsg(mMsgType.Warning,
                                            "Claim : " + Sm.GetGrdStr(Grd6, i, 2) + Environment.NewLine +
                                            "Start Date 1 : " + string.Concat(StartDt.Substring(6, 2), "/", StartDt.Substring(4, 2), "/", Sm.Left(StartDt, 4)) + Environment.NewLine +
                                            "End Date 1   : " + string.Concat(EndDt.Substring(6, 2), "/", EndDt.Substring(4, 2), "/", Sm.Left(EndDt, 4)) + Environment.NewLine +
                                            "Start Date 2 : " + string.Concat(Sm.GetGrdDate(Grd6, j, 5).Substring(6, 2), "/", Sm.GetGrdDate(Grd6, j, 5).Substring(4, 2), "/", Sm.Left(Sm.GetGrdDate(Grd6, j, 5), 4)) + Environment.NewLine +
                                            "End Date 2   : " + string.Concat(Sm.GetGrdDate(Grd6, j, 6).Substring(6, 2), "/", Sm.GetGrdDate(Grd6, j, 6).Substring(4, 2), "/", Sm.Left(Sm.GetGrdDate(Grd6, j, 6), 4)) + Environment.NewLine + Environment.NewLine + 
                                            "Intersection of period is found.");
                                        TcEmpSalary.SelectedTabPage = TpClaim;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Grd7.Rows.Count > 1)
            {
                if (Sm.GetGrdStr(Grd7, 0, 1).Length>0)
                    if (Sm.IsGrdValueEmpty(Grd7, 0, 2, true, "Grade performance's value is not valid.")) return true;
            }

            return false;
        }

        private MySqlCommand SaveEmpSalaryHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpSalaryHdr(DocNo, DocDt, Status, EmpCode, Remark, PerformanceStatus, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'O', @EmpCode, @Remark, @PerformanceStatus, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@PerformanceStatus", Sm.GetLue(LuePerformanceStatus));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveEmpSalaryDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* EmpSalary - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmpSalaryDtl(DocNo, DNo, StartDt, Amt, Amt2, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @DNo_" + r.ToString() +
                        ", @StartDt_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Amt2_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParamDt(ref cm, "@StartDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Amt2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 3));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveEmpSalary2Dtl(string DocNo, ref int DNo, ref iGrid Grd, string ADType)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* EmpSalary - 2Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 2).Length > 0)
                {
                    DNo++;
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmpSalary2Dtl(DocNo, DNo, ADCode, Amt, StartDt, EndDt, SiteCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    
                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + 
                        ", @ADCode_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @StartDt_" + r.ToString() +
                        ", @EndDt_" + r.ToString() +
                        ", @SiteCode_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (DNo).ToString(), 3));
                    if (ADType == "A")
                    {
                        Sm.CmParam<String>(ref cm, "@ADCode_" + r.ToString(), Sm.GetGrdStr(Grd, r, 2));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd, r, 4));
                        if (Sm.GetGrdStr(Grd, r, 5).Length > 0)
                            Sm.CmParamDt(ref cm, "@StartDt_" + r.ToString(), Sm.GetGrdDate(Grd, r, 5));
                        else
                        {
                            if (mEmpADStartDtDefault.Length > 0)
                                Sm.CmParam<String>(ref cm, "@StartDt_" + r.ToString(), mEmpADStartDtDefault);
                            else
                                Sm.CmParam<String>(ref cm, "@StartDt_" + r.ToString(), string.Empty);
                        }
                        if (mIsAllowanceMultiplePeriod)
                            Sm.CmParamDt(ref cm, "@EndDt_" + r.ToString(), Sm.GetGrdDate(Grd, r, 6));
                        else
                        {
                            if (Sm.GetGrdStr(Grd, r, 6).Length > 0)
                                Sm.CmParamDt(ref cm, "@EndDt_" + r.ToString(), Sm.GetGrdDate(Grd, r, 6));
                            else
                            {
                                if (mEmpADEndDtDefault.Length > 0)
                                    Sm.CmParam<String>(ref cm, "@EndDt_" + r.ToString(), mEmpADEndDtDefault);
                                else
                                    Sm.CmParam<String>(ref cm, "@EndDt_" + r.ToString(), string.Empty);
                            }
                        }
                        Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd, r, 7));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@ADCode_" + r.ToString(), Sm.GetGrdStr(Grd, r, 2));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd, r, 4));
                        Sm.CmParamDt(ref cm, "@StartDt_" + r.ToString(), Sm.GetGrdDate(Grd, r, 5));
                        Sm.CmParamDt(ref cm, "@EndDt_" + r.ToString(), Sm.GetGrdDate(Grd, r, 6));
                        Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), string.Empty);
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpSalary3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* EmpSalary - 3Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd5.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmpSalary3Dtl(DocNo, DNo, SSPCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @DNo_" + r.ToString() +
                        ", @SSPCode_" + r.ToString() + 
                        ", @StartDt_" + r.ToString() + 
                        ", @EndDt_" + r.ToString() +
                        ", @Amt_" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@SSPCode_" + r.ToString(), Sm.GetGrdStr(Grd5, r, 1));
                    Sm.CmParamDt(ref cm, "@StartDt_" + r.ToString(), Sm.GetGrdDate(Grd5, r, 3));
                    Sm.CmParamDt(ref cm, "@EndDt_" + r.ToString(), Sm.GetGrdDate(Grd5, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpSalary4Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* EmpSalary - 4Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmpSalary4Dtl(DocNo, DNo, ClaimCode, Yr, ClaimStartDt, ClaimEndDt, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @DNo_" + r.ToString() +
                        ", @ClaimCode_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @ClaimStartDt_" + r.ToString() +
                        ", @ClaimEndDt_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");


                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ClaimCode_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 1));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 4));
                    Sm.CmParamDt(ref cm, "@ClaimStartDt_" + r.ToString(), Sm.GetGrdDate(Grd6, r, 5));
                    Sm.CmParamDt(ref cm, "@ClaimEndDt_" + r.ToString(), Sm.GetGrdDate(Grd6, r, 6));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveEmpSalaryDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* EmpSalary - Dtl */ ");
        //    SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblEmpSalaryDtl(DocNo, DNo, StartDt, Amt, Amt2, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @StartDt, @Amt, @Amt2, @UserCode, @Dt); ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveEmpSalary2Dtl(string DocNo, int DNo, ref iGrid Grd, int Row, string ADType)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmpSalary2Dtl(DocNo, DNo, ADCode, Amt, StartDt, EndDt, SiteCode, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ADCode, @Amt, @StartDt, @EndDt, @SiteCode, @CreateBy, CurrentDateTime(), Null, Null); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (DNo).ToString(), 3));
        //    if (ADType == "A")
        //    {
        //        Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd, Row, 2));
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd, Row, 4));
        //        if (Sm.GetGrdStr(Grd, Row, 5).Length>0)
        //        {
        //            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd, Row, 5));
        //        }
        //        else
        //        {
        //            if (mEmpADStartDtDefault.Length>0)
        //                Sm.CmParam<String>(ref cm, "@StartDt", mEmpADStartDtDefault);    
        //            else
        //                Sm.CmParam<String>(ref cm, "@StartDt", string.Empty);
        //        }

        //        if (mIsAllowanceMultiplePeriod)
        //            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd, Row, 6));
        //        else
        //        {    
        //            if (Sm.GetGrdStr(Grd, Row, 6).Length>0)
        //            {
        //                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd, Row, 6));
        //            }
        //            else
        //            {
        //                if (mEmpADEndDtDefault.Length>0)
        //                    Sm.CmParam<String>(ref cm, "@EndDt", mEmpADEndDtDefault);    
        //                else
        //                    Sm.CmParam<String>(ref cm, "@EndDt", string.Empty);
        //            }
        //        }
        //        Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(Grd, Row, 7));
        //    }
        //    else
        //    {
        //        Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd, Row, 2));
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd, Row, 4));
        //        Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd, Row, 5));
        //        Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd, Row, 6));
        //        Sm.CmParam<String>(ref cm, "@SiteCode", string.Empty);
        //    }
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveEmpSalary3Dtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmpSalary3Dtl(DocNo, DNo, SSPCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @SSPCode, @StartDt, @EndDt, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetGrdStr(Grd5, Row, 1));
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd5, Row, 3));
        //    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd5, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}


        //private MySqlCommand SaveEmpSalary4Dtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmpSalary4Dtl(DocNo, DNo, ClaimCode, Yr, ClaimStartDt, ClaimEndDt, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ClaimCode, @Yr, @ClaimStartDt, @ClaimEndDt, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ClaimCode", Sm.GetGrdStr(Grd6, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd6, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 4));
        //    Sm.CmParamDt(ref cm, "@ClaimStartDt", Sm.GetGrdDate(Grd6, Row, 5));
        //    Sm.CmParamDt(ref cm, "@ClaimEndDt", Sm.GetGrdDate(Grd6, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveEmpSalary5Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* EmpSalary - EmpSalary5Dtl */ ");
            SQL.AppendLine("Insert Into TblEmpSalary5Dtl(DocNo, DNo, GradePerformance, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @GradePerformance,  @Value, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@GradePerformance", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpSalaryApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* EmpSalary - Approval */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmpSalary'; ");

            SQL.AppendLine("Update TblEmpSalaryHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpSalary' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblEmployee Set PerformanceStatus=@PerformanceStatus ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpSalary' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Delete From TblEmployeeSalary ");
            SQL.AppendLine("Where EmpCode In (Select EmpCode From TblEmpSalaryHdr Where Status='A' And DocNo=@DocNo); ");

            SQL.AppendLine("Delete From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("Where EmpCode In (Select EmpCode From TblEmpSalaryHdr Where Status='A' And DocNo=@DocNo); ");

            SQL.AppendLine("Delete From TblEmployeeSalarySS ");
            SQL.AppendLine("Where EmpCode In (Select EmpCode From TblEmpSalaryHdr Where Status='A' And DocNo=@DocNo); ");

            SQL.AppendLine("Delete From TblEmployeeClaim ");
            SQL.AppendLine("Where EmpCode In (Select EmpCode From TblEmpSalaryHdr Where Status='A' And DocNo=@DocNo); ");

            SQL.AppendLine("Delete From TblEmployeePerformance ");
            SQL.AppendLine("Where EmpCode In (Select EmpCode From TblEmpSalaryHdr Where Status='A' And DocNo=@DocNo); ");

            SQL.AppendLine("Insert Into TblEmployeeSalary ");
            SQL.AppendLine("(EmpCode, DNo, StartDt, Amt, Amt2, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select A.EmpCode, B.DNo, B.StartDt, B.Amt, B.Amt2, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmpSalaryDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("(EmpCode, DNo, ADCode, StartDt, EndDt, SiteCode, Amt, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select A.EmpCode, B.DNo, B.ADCode, B.StartDt, B.EndDt, B.SiteCode, B.Amt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmpSalary2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblEmployeeSalarySS ");
            SQL.AppendLine("(EmpCode, DNo, SSPCode, StartDt, EndDt, Amt, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select A.EmpCode, B.DNo, B.SSPCode, B.StartDt, B.EndDt, B.Amt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmpSalary3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblEmployeeClaim ");
            SQL.AppendLine("(EmpCode, DNo, ClaimCode, Yr, ClaimStartDt, ClaimEndDt, Amt, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select A.EmpCode, B.DNo, B.ClaimCode, B.Yr, B.ClaimStartDt, B.ClaimEndDt, B.Amt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmpSalary4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblEmployeePerformance ");
            SQL.AppendLine("(EmpCode, DNo, GradePerformance, Value, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select A.EmpCode, B.DNo, B.GradePerformance, B.Value, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmpSalary5Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@PerformanceStatus", Sm.GetLue(LuePerformanceStatus));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployeeSalaryActInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* EmpSalary - Update  Active Indicator */ ");

            SQL.AppendLine("Update TblEmployeeSalary Set ActInd='N' ");
            SQL.AppendLine("Where EmpCode=@EmpCode And ActInd='Y'; ");

            SQL.AppendLine("Update TblEmployeeSalary Set ActInd='Y' ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And StartDt In ( ");
            SQL.AppendLine("    Select T.StartDt From ( ");
            SQL.AppendLine("    Select Max(T1.StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalary T1 ");
            SQL.AppendLine("    Where T1.EmpCode=@EmpCode ");
            SQL.AppendLine(")T); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            return cm;
        }

        private MySqlCommand UpdatePeriodicAdvancment(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            SQL.AppendLine("/* Update Periodic Advancement*/ ");

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4) == "1")
                {
                    SQL.AppendLine("Update TblPeriodicGradeAdvancement Set EmpSalaryInd = 'Y' Where EmpCode = @EmpCode And DNo = @DNo_" + r.ToString() + ";");
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
                if (Sm.GetGrdStr(Grd1, r, 4) == "2")
                {
                    SQL.AppendLine("Update TblPeriodicSalaryAdvancement Set EmpSalaryInd = 'Y' Where EmpCode = @EmpCode And DNo = @DNo_" + r.ToString() + ";");
                    Sm.CmParam<String>(ref cm, "@DNo_"+r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            cm.CommandText = SQL.ToString();
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowEmpSalaryHdr(DocNo);
                ShowEmpSalaryDtl(DocNo);
                ShowEmpSalary2DtlA(DocNo);
                ShowEmpSalary2DtlD(DocNo);
                ShowEmpSalary3Dtl(DocNo);
                ShowEmpSalary4Dtl(DocNo);
                ShowEmpSalary5Dtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpSalaryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, B.JoinDt, A.Remark, B.PerformanceStatus ");
            SQL.AppendLine("From TblEmpSalaryHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "EmpCode", "EmpName", "EmpCodeOld", 
                        
                        //6-10
                        "PosName", "DeptName", "SiteName", "JoinDt", "Remark", 
                        
                        //11
                        "PerformanceStatus"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[9]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetLue(LuePerformanceStatus, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        private void ShowEmpSalaryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, StartDt, Amt, Amt2 ");
            SQL.AppendLine("From TblEmpSalaryDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By StartDt;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]{ "DNo", "StartDt", "Amt", "Amt2" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowEmpSalary2DtlA(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, A.Amt, ");
            SQL.AppendLine("A.StartDt, A.EndDt, A.SiteCode, C.SiteName ");
            SQL.AppendLine("From TblEmpSalary2Dtl A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.ADName, A.StartDt;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ADCode", "ADName", "Amt", "StartDt", "EndDt", 
                    "SiteCode", "SiteName" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowEmpSalary2DtlD(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, A.Amt, A.StartDt, A.EndDt ");
            SQL.AppendLine("From TblEmpSalary2Dtl A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.ADName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] { "DNo", "ADCode", "ADName", "Amt", "StartDt", "EndDt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowEmpSalary3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.SSPCode, B.SSPName, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmpSalary3Dtl A ");
            SQL.AppendLine("Inner Join TblSSProgram B On A.SSPCode=B.SSPCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "SSPCode", "SSPName", "StartDt", "EndDt", "Amt" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowEmpSalary4Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ClaimCode, B.ClaimName, A.Yr, A.Amt, A.ClaimStartDt, A.ClaimEndDt ");
            SQL.AppendLine("From TblEmpSalary4Dtl A ");
            SQL.AppendLine("Inner Join TblClaim B On A.ClaimCode=B.ClaimCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ClaimCode", "ClaimName", "Yr", "Amt", "ClaimStartDt", 
                    "ClaimEndDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowEmpSalary5Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, GradePerformance, Value ");
            SQL.AppendLine("From TblEmpSalary5Dtl Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DNo", "GradePerformance", "Value" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpSalary' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GeneratePensionBenefit(int r)
        {
            if (Sm.StdMsgYN("Question", "Do you want to generate pension benefit allowance ?") == DialogResult.No) return;

            if (mADCodePensionBenefit.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Info, "You need to set parameter for pension benefit allowance.");
                return;
            }
            if (mADCodePensionBenefitComponent.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Info, "You need to set parameter for pension benefit allowance's components.");
                return;
            }
            if (Sm.GetGrdStr(Grd2, r, 2).Length>0 && !Sm.CompareStr(mADCodePensionBenefit, Sm.GetGrdStr(Grd2, r, 2)))
            {
                Sm.StdMsg(mMsgType.Info, "You can't process in this row.");
                return;
            }

            var l = new List<AD>();
            var StartDt = string.Empty;
            try
            {
                Grd2.Cells[r, 2].Value = mADCodePensionBenefit;
                Grd2.Cells[r, 3].Value = Sm.GetValue("Select ADName From TblAllowanceDeduction Where ADCode=@Param;", mADCodePensionBenefit);
                Grd2.Cells[r, 4].Value = 0.00;
                GeneratePensionBenefit(ref l);

                foreach (var x in l)
                {
                    for (int i = 0; i < Grd2.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd2, i, 2), x.ADCode))
                        {
                            StartDt = Sm.GetGrdDate(Grd2, i, 5);
                            if (StartDt.Length >= 8) StartDt = Sm.Left(StartDt, 8);
                            if (x.StartDt.Length > 0)
                            {
                                if (Sm.CompareDtTm(x.StartDt, StartDt) == -1)
                                {
                                    x.StartDt = StartDt;
                                    x.Amt = Sm.GetGrdDec(Grd2, i, 4);
                                }
                            }
                            else
                            {
                                x.StartDt = StartDt;
                                x.Amt = Sm.GetGrdDec(Grd2, i, 4);
                            }
                        }
                    }
                }
                var Amt = 0m;
                foreach(var x in l.Where(w=>w.Amt>0m))
                    Amt += x.Amt;
                Grd2.Cells[r, 4].Value = Amt;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
            }
        }

        private void GeneratePensionBenefit(ref List<AD> l)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select Distinct ADCode From TblAllowanceDeduction Where Find_In_Set(ADCode, @ADCodePensionBenefitComponent); "
                };
                Sm.CmParam<String>(ref cm, "@ADCodePensionBenefitComponent", mADCodePensionBenefitComponent);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ADCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AD()
                        {
                            ADCode = Sm.DrStr(dr, c[0]),
                            StartDt = string.Empty,
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
                
        }

        private void ShowEmployeeDetail(string EmpCode)
        {
            var SQL = new StringBuilder();
            var l2 = new List<EmpPPS2>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, E.SiteName, A.JoinDt, D.BasicSalary, A.PerformanceStatus ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where A.EmpCode = @EmpCode And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");

            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", 
                    "EmpCodeOld", 
                    "PosName",
                    "DeptName",
                    "SiteName",

                    //6-8
                    "JoinDt",
                    "BasicSalary",
                    "PerformanceStatus"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new EmpPPS2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),

                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            SiteName = Sm.DrStr(dr, c[5]),

                            JoinDt = Sm.DrStr(dr, c[6]),
                            BasicSalary = Sm.DrDec(dr, c[7]),
                            PerformanceStatus = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                TxtEmpName.EditValue = l2[0].EmpName;
                TxtEmpCodeOld.EditValue = l2[0].EmpCodeOld;
                TxtPosCode.EditValue = l2[0].PosName;
                TxtDeptCode.EditValue = l2[0].DeptName;
                TxtSiteCode.EditValue = l2[0].SiteName;
                Sm.SetDte(DteJoinDt, Sm.Left(l2[0].JoinDt, 8));
                Sm.SetLue(LuePerformanceStatus, l2[0].PerformanceStatus);
            }

            l2.Clear();
        }

        private void ParPrint()
        {
            if (Sm.GetParameter("DocTitle") == "HIN")
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

                string[] TableName = { "Emp", "EmpSal1", "EmpSal2", "EmpAD1", "EmpAD2", "EmpPPS" };
                string mEmpCode = string.Empty;
                mEmpCode = Sm.GetValue("Select EmpCode From TblEmpSalaryHdr Where DocNo = @Param; ", TxtDocNo.Text);

                var lE = new List<Emp>();
                var lES1 = new List<EmpSal1>();
                var lES2 = new List<EmpSal2>();
                var lEA1 = new List<EmpAD1>();
                var lEA2 = new List<EmpAD2>();
                var lEP = new List<EmpPPS>();

                List<IList> myLists = new List<IList>();

                #region Emp

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("B.EmpCode, B.EmpName, B.BirthPlace, IfNull(Date_Format(B.BirthDt, '%d %M %Y'), '') BirthDt, B.GrdLvlCode, ");
                SQL.AppendLine("Date_Format(B.JoinDt, '%d %M %Y') JoinDt, C.SiteName, D.DeptName, E.PosName, F.OptDesc As MaritalStatus, ");
                SQL.AppendLine("If(Length(G.OptDesc) > 0, '', 'X') As PKWT, If(Length(G.OptDesc) > 0, 'X', '') As PKWTT, ");
                SQL.AppendLine("Date_Format(A.DocDt, '%d/%m/%Y') DocDt1, Date_Format(A.DocDt, '%d %M %Y') DocDt2, C.Address, ");
                SQL.AppendLine("I.OptDesc As EducationLevel, TimeStampDiff(Year, B.JoinDt, A.DocDt) Yr, Floor(Mod(TimeStampDiff(Month, B.JoinDt, A.DocDt), 12)) Mth, ");
                SQL.AppendLine("B.LeaveStartDt As PermanentDt, A.DocDt ");
                SQL.AppendLine("From TblEmpSalaryHdr A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode And A.DocNo = @DocNo ");
                SQL.AppendLine("Left Join TblSite C On B.SiteCode = C.SiteCode ");
                SQL.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQL.AppendLine("Left Join TblPosition E On B.PosCode = E.PosCode ");
                SQL.AppendLine("Left Join TblOption F On B.MaritalStatus = F.OptCode And F.OptCat = 'MaritalStatus' ");
                SQL.AppendLine("Left Join TblOption G On B.EmploymentStatus = G.OptCode And G.OptCat = 'EmploymentStatus' ");
                SQL.AppendLine("    And G.OptCode In (Select ParValue From TblParameter Where Parcode = 'EmploymentStatusTetap') ");
                SQL.AppendLine("Left Join TblEmployeeEducation H On B.EmpCode = H.EmpCode And H.HighestInd = 'Y' ");
                SQL.AppendLine("Left Join TblOption I On H.Level = I.OptCode And I.OptCat = 'EmployeeEducationLevel'; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "EmpCode",
                    "EmpName",
                    "BirthPlace",
                    "BirthDt",
                    "GrdLvlCode",

                    //6-10
                    "JoinDt", 
                    "SiteName", 
                    "PosName",
                    "MaritalStatus",
                    "PKWT",

                    //11-15
                    "PKWTT",
                    "DocDt1",
                    "DocDt2",
                    "Address",
                    "EducationLevel",

                    //16-20
                    "Yr",
                    "Mth",
                    "PermanentDt",
                    "DocDt",
                    "DeptName"
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lE.Add(new Emp()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                EmpCode = Sm.DrStr(dr, c[1]),
                                EmpName = Sm.DrStr(dr, c[2]),
                                BirthPlace = Sm.DrStr(dr, c[3]),
                                BirthDt = Sm.DrStr(dr, c[4]),
                                GrdLvlCode = Sm.DrStr(dr, c[5]),

                                JoinDt = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                PosName = Sm.DrStr(dr, c[8]),
                                MaritalStatus = Sm.DrStr(dr, c[9]),
                                PKWT = Sm.DrStr(dr, c[10]),

                                PKWTT = Sm.DrStr(dr, c[11]),
                                DocDt1 = Sm.DrStr(dr, c[12]),
                                DocDt2 = Sm.DrStr(dr, c[13]),
                                Address = Sm.DrStr(dr, c[14]),
                                EducationLevel = Sm.DrStr(dr, c[15]),

                                Yr = Sm.DrStr(dr, c[16]),
                                Mth = Sm.DrStr(dr, c[17]),
                                PermanentDt = Sm.DrStr(dr, c[18]),
                                DocDt = Sm.DrStr(dr, c[19]),
                                DeptName = Sm.DrStr(dr, c[20])
                            });
                        }
                    }
                    dr.Close();
                }

                #endregion

                #region EmpSal1

                var SQL2 = new StringBuilder();
                var cm2 = new MySqlCommand();

                SQL2.AppendLine("Select EmpCode, DNo, Amt ");
                SQL2.AppendLine("From TblEmployeeSalary ");
                SQL2.AppendLine("Where EmpCode = @EmpCode ");
                SQL2.AppendLine("And ActInd = 'Y' ");
                SQL2.AppendLine("Order By StartDt Desc Limit 1; ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@EmpCode", mEmpCode);

                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] { "EmpCode", "DNo", "Amt" });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            lES1.Add(new EmpSal1()
                            {
                                EmpCode = Sm.DrStr(dr2, c2[0]),

                                DNo = Sm.DrStr(dr2, c2[1]),
                                Amt = Sm.DrDec(dr2, c2[2])
                            });
                        }
                    }
                    else
                    {
                        lES1.Add(new EmpSal1()
                        {
                            EmpCode = mEmpCode,

                            DNo = "001",
                            Amt = 0m
                        });
                    }
                    dr2.Close();
                }

                #endregion

                #region EmpSal2

                var SQL3 = new StringBuilder();
                var cm3 = new MySqlCommand();

                SQL3.AppendLine("Select EmpCode, DNo, Amt ");
                SQL3.AppendLine("From TblEmployeeSalary ");
                SQL3.AppendLine("Where EmpCode = @EmpCode ");
                SQL3.AppendLine("And Concat(EmpCode, DNo) Not In (@EmpCodeDNo) ");
                SQL3.AppendLine("Order By StartDt Desc Limit 1; ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCode);

                    if (lES1.Count > 0) Sm.CmParam<String>(ref cm3, "@EmpCodeDNo", string.Concat(lES1[0].EmpCode, lES1[0].DNo));
                    else Sm.CmParam<String>(ref cm3, "@EmpCodeDNo", string.Empty);

                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[] { "EmpCode", "DNo", "Amt" });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            lES2.Add(new EmpSal2()
                            {
                                EmpCode = Sm.DrStr(dr3, c3[0]),

                                DNo = Sm.DrStr(dr3, c3[1]),
                                Amt = Sm.DrDec(dr3, c3[2])
                            });
                        }
                    }
                    else
                    {
                        if (lES1.Count > 0)
                        {
                            lES2.Add(new EmpSal2()
                            {
                                EmpCode = lES1[0].EmpCode,
                                DNo = lES1[0].DNo,
                                Amt = lES1[0].Amt
                            });
                        }
                    }
                    dr3.Close();
                }

                #endregion

                #region EmpAD1

                var SQL4 = new StringBuilder();
                var cm4 = new MySqlCommand();

                SQL4.AppendLine("Select EmpCode, DNo, Amt ");
                SQL4.AppendLine("From TblEmployeeAllowanceDeduction ");
                SQL4.AppendLine("Where EmpCode = @EmpCode ");
                SQL4.AppendLine("And ADCode In (Select Parvalue From TblParameter Where Parcode = 'ADCodeFunctional') ");
                SQL4.AppendLine("Order By EndDt Desc Limit 1; ");

                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCode);

                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[] { "EmpCode", "DNo", "Amt" });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            lEA1.Add(new EmpAD1()
                            {
                                EmpCode = Sm.DrStr(dr4, c4[0]),

                                DNo = Sm.DrStr(dr4, c4[1]),
                                Amt = Sm.DrDec(dr4, c4[2])
                            });
                        }
                    }
                    else
                    {
                        lEA1.Add(new EmpAD1()
                        {
                            EmpCode = mEmpCode,

                            DNo = "001",
                            Amt = 0m
                        });
                    }
                    dr4.Close();
                }

                #endregion

                #region EmpAD2

                var SQL5 = new StringBuilder();
                var cm5 = new MySqlCommand();

                SQL5.AppendLine("Select EmpCode, DNo, Amt ");
                SQL5.AppendLine("From TblEmployeeAllowanceDeduction ");
                SQL5.AppendLine("Where EmpCode = @EmpCode ");
                SQL5.AppendLine("And ADCode In (Select Parvalue From TblParameter Where Parcode = 'ADCodeFunctional') ");
                SQL5.AppendLine("And Concat(EmpCode, DNo) Not In (@EmpCodeDNo2) ");
                SQL5.AppendLine("Order By EndDt Desc Limit 1; ");

                using (var cn5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn5.Open();
                    cm5.Connection = cn5;
                    cm5.CommandText = SQL5.ToString();
                    Sm.CmParam<String>(ref cm5, "@EmpCode", mEmpCode);

                    if (lEA1.Count > 0) Sm.CmParam<String>(ref cm5, "@EmpCodeDNo", string.Concat(lEA1[0].EmpCode, lEA1[0].DNo));
                    else Sm.CmParam<String>(ref cm5, "@EmpCodeDNo", string.Empty);

                    var dr5 = cm5.ExecuteReader();
                    var c5 = Sm.GetOrdinal(dr5, new string[] { "EmpCode", "DNo", "Amt" });
                    if (dr5.HasRows)
                    {
                        while (dr5.Read())
                        {
                            lEA2.Add(new EmpAD2()
                            {
                                EmpCode = Sm.DrStr(dr5, c5[0]),

                                DNo = Sm.DrStr(dr5, c5[1]),
                                Amt = Sm.DrDec(dr5, c5[2])
                            });
                        }
                    }
                    else
                    {
                        if (lEA1.Count > 0)
                        {
                            lEA2.Add(new EmpAD2()
                            {
                                EmpCode = lEA1[0].EmpCode,
                                DNo = lEA1[0].DNo,
                                Amt = lEA1[0].Amt
                            });
                        }
                    }
                    dr5.Close();
                }

                #endregion

                #region EmpPPS

                var SQL6 = new StringBuilder();
                var cm6 = new MySqlCommand();

                SQL6.AppendLine("Select A.EmpCode, C.SiteName, D.DeptName, E.PosName, ");
                SQL6.AppendLine("If(Length(F.OptDesc) > 0, '', 'X') As PKWT, If(Length(F.OptDesc) > 0, 'X', '') As PKWTT ");
                SQL6.AppendLine("From TblEmployeePPS A ");
                SQL6.AppendLine("Inner Join TblPPS B On A.EmpCode = B.EmpCode And A.StartDt = B.StartDt ");
                SQL6.AppendLine("    And A.EmpCode = @EmpCode And A.EndDt Is Null ");
                SQL6.AppendLine("Left Join TblSite C On B.SiteCodeOld = C.SiteCode ");
                SQL6.AppendLine("Left Join TblDepartment D On B.DeptCodeOld = D.DeptCode ");
                SQL6.AppendLine("Left Join TblPosition E On B.PosCodeOld = E.PosCode ");
                SQL6.AppendLine("Left Join TblOption F On B.EmploymentStatusOld = F.OptCode And F.OptCat = 'EmploymentStatus' ");
                SQL6.AppendLine("    And F.OptCode In (Select ParValue From TblParameter Where Parcode = 'EmploymentStatusTetap'); ");

                using (var cn6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn6.Open();
                    cm6.Connection = cn6;
                    cm6.CommandText = SQL6.ToString();
                    Sm.CmParam<String>(ref cm6, "@EmpCode", mEmpCode);

                    var dr6 = cm6.ExecuteReader();
                    var c6 = Sm.GetOrdinal(dr6, new string[] 
                { 
                    "EmpCode",
                    
                    "SiteName",
                    "DeptName",
                    "PosName",
                    "PKWT",
                    "PKWTT"
                });
                    if (dr6.HasRows)
                    {
                        while (dr6.Read())
                        {
                            lEP.Add(new EmpPPS()
                            {
                                EmpCode = Sm.DrStr(dr6, c6[0]),

                                SiteName = Sm.DrStr(dr6, c6[1]),
                                DeptName = Sm.DrStr(dr6, c6[2]),
                                PosName = Sm.DrStr(dr6, c6[3]),
                                PKWT = Sm.DrStr(dr6, c6[4]),
                                PKWTT = Sm.DrStr(dr6, c6[5])
                            });
                        }
                    }
                    else
                    {
                        if (lE.Count > 0)
                        {
                            lEP.Add(new EmpPPS()
                            {
                                EmpCode = lE[0].EmpCode,
                                SiteName = lE[0].SiteName,
                                DeptName = lE[0].DeptName,
                                PosName = lE[0].PosName,
                                PKWT = lE[0].PKWT,
                                PKWTT = lE[0].PKWTT
                            });
                        }
                    }
                    dr6.Close();
                }

                #endregion

                if (lE.Count > 0)
                {
                    decimal mPenyesuaianStatus = 0m, mKenaikanJobGrade = 0m,
                        mSal1Amt = 0m, mSal2Amt = 0m, mA1Amt = 0m, mA2Amt = 0m,
                        mTotal7 = 0m, mTotal8a = 0m, mTotal8b = 0m;

                    if (lE[0].PermanentDt == lE[0].DocDt) mPenyesuaianStatus = lES1[0].Amt - lES2[0].Amt;
                    else mKenaikanJobGrade = lES1[0].Amt - lES2[0].Amt;

                    mA1Amt = lEA1[0].Amt;
                    mA2Amt = lEA2[0].Amt;
                    mSal1Amt = lES1[0].Amt;
                    mSal2Amt = lES2[0].Amt;

                    mTotal7 = mPenyesuaianStatus + mKenaikanJobGrade + mA1Amt;
                    mTotal8a = mSal2Amt + mA2Amt;
                    mTotal8b = mSal1Amt + mA1Amt;

                    lE[0].PenyesuaianStatus = mPenyesuaianStatus;
                    lE[0].KenaikanJobGrade = mKenaikanJobGrade;
                    lE[0].Total7 = mTotal7;
                    lE[0].Total8a = mTotal8a;
                    lE[0].Total8b = mTotal8b;
                }

                if (lEP.Count > 0)
                {
                    if (lEP[0].SiteName.Length <= 0) lEP[0].SiteName = lE[0].SiteName;
                    if (lEP[0].DeptName.Length <= 0) lEP[0].DeptName = lE[0].DeptName;
                    if (lEP[0].PosName.Length <= 0) lEP[0].PosName = lE[0].PosName;
                }

                myLists.Add(lE);
                myLists.Add(lES1);
                myLists.Add(lES2);
                myLists.Add(lEA1);
                myLists.Add(lEA2);
                myLists.Add(lEP);

                Sm.PrintReport("EmpSalaryHIN", myLists, TableName, false);
            }
        }

        internal string GetSelectedADCode(iGrid Grd)
        {
            var SQL = string.Empty;
            if (Grd.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowEmployeeSalary(string EmpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select DNo, StartDt, Amt, Amt2, '0' As Ind From TblEmployeeSalary ");
            SQL.AppendLine("    Where EmpCode=@EmpCode ");
            if (mIsUsePeriodicAdvancementMenu)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.DNo, Concat(B.GrdLvlIncreaseYr, B.GrdLvlIncreaseMth,'21') As StartDt, ");
                SQL.AppendLine("    IfNull(C.Amt, 0.00) As Amt, IfNull(C.Amt/Cast(IfNull(D.ParValue, 1) As Unsigned), 0.00) As Amt2, '1' As SourceInd ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Inner Join TblPeriodicGradeAdvancement B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("    	And A.EmpCode = @EmpCode ");
                SQL.AppendLine("    	And A.GrdLvlCode = B.GrdLvlCode ");
                SQL.AppendLine("    	And B.DNo = (Select Max(DNo) From TblPeriodicGradeAdvancement Where EmpCode = A.EmpCode And GrdLvlCode = A.GrdLvlCode) ");
                SQL.AppendLine("    Inner Join TblGradeSalaryDtl C On B.GrdLvlCode = C.GrdSalaryCode ");
                SQL.AppendLine("    	And B.WorkPeriodYr = C.WorkPeriod ");
                SQL.AppendLine("    	And B.WorkPeriodMth = C.Month ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode = 'NoWorkDayBulanan' And D.ParCode Is Not Null ");
                SQL.AppendLine("    And B.EmpSalaryInd = 'N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.DNo, Concat(B.SalaryIncreaseYr, B.SalaryIncreaseMth,'21') As StartDt, ");
                SQL.AppendLine("    IfNull(C.Amt, 0.00) As Amt, IfNull(C.Amt/Cast(IfNull(D.ParValue, 1) As Unsigned), 0.00) As Amt2, '2' As SourceInd ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Inner Join TblPeriodicSalaryAdvancement B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("    	And A.EmpCode = @EmpCode ");
                SQL.AppendLine("    	And A.GrdLvlCode = B.GrdLvlCode ");
                SQL.AppendLine("    	And B.DNo = (Select Max(DNo) From TblPeriodicSalaryAdvancement Where EmpCode = A.EmpCode And GrdLvlCode = A.GrdLvlCode) ");
                SQL.AppendLine("    Inner Join TblGradeSalaryDtl C On B.GrdLvlCode = C.GrdSalaryCode ");
                SQL.AppendLine("    	And B.WorkPeriodYr = C.WorkPeriod ");
                SQL.AppendLine("    	And B.WorkPeriodMth = C.Month ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode = 'NoWorkDayBulanan' And D.ParCode Is Not Null ");
                SQL.AppendLine("    And Not Exists(Select 1 From TblPeriodicGradeAdvancement Where EmpCode = A.EmpCode And GrdLvlCode = A.GrdLvlCode) ");
                SQL.AppendLine("    And B.EmpSalaryInd = 'N' ");
            }
            SQL.AppendLine(")X  Order By X.StartDt;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "DNo", "StartDt", "Amt", "Amt2", "Ind" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowEmployeeAllowanceDeduction(string EmpCode, string ADType)
        {
            bool IsAllowance = ADType == "A";
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@ADType", ADType);

            var SQL = new StringBuilder();

            #region Old
            //if (mSMKStatisAmtSource == "2")
            //{
            //    //SQL.AppendLine(" IFNULL(IFNULL(E.Amt,0.00), IFNULL(A.Amt,0.00)) AS Amt, ");
            //    SQL.AppendLine("SELECT DNo, ADCode, ADName, Amt, StartDt, EndDt, SiteCode, SiteName ");
            //    SQL.AppendLine("FROM ");
            //    SQL.AppendLine("( ");
            //    SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, ");
            //    SQL.AppendLine("IFNULL(A.Amt, 0.00) AS Amt, ");
            //    SQL.AppendLine("A.StartDt, A.EndDt, A.SiteCode, C.SiteName ");
            //    SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //    SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode = B.ADCode And B.ADType = @ADType ");
            //    SQL.AppendLine("Left Join TblSite C On A.SiteCode = C.SiteCode ");
            //    SQL.AppendLine("INNER JOIN tblemployee D ON A.EmpCode = D.EmpCode ");
            //    SQL.AppendLine("Left Join TblLevelDtl3 E ON D.LevelCode = E.LevelCode AND A.ADCode = 'AL025' ");
            //    if (mIsESADAUseNewestData)
            //    {
            //        SQL.AppendLine("Inner JOIN  ");
            //        SQL.AppendLine("( ");
            //        SQL.AppendLine("	SELECT AdCode, MAX(StartDt) StartDt ");
            //        SQL.AppendLine("	FROM TblEmployeeAllowanceDeduction  ");
            //        SQL.AppendLine("	WHERE empcode = @EmpCode ");
            //        SQL.AppendLine("	GROUP BY ADCode ");
            //        SQL.AppendLine(") F ON A.ADCode = F.AdCode AND A.StartDt = F.StartDt ");
            //    }
            //    SQL.AppendLine("Where A.EmpCode = @EmpCode AND A.ADCode != 'AL025' ");

            //    SQL.AppendLine("UNION ALL ");

            //    SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, ");
            //    SQL.AppendLine("IFNULL(IFNULL(E.Amt, 0.00), IFNULL(A.Amt, 0.00)) AS Amt, ");
            //    SQL.AppendLine("A.StartDt, A.EndDt, A.SiteCode, C.SiteName ");
            //    SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //    SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode = B.ADCode And B.ADType = @ADType ");
            //    SQL.AppendLine("Left Join TblSite C On A.SiteCode = C.SiteCode ");
            //    SQL.AppendLine("INNER JOIN tblemployee D ON A.EmpCode = D.EmpCode ");
            //    SQL.AppendLine("Left Join TblLevelDtl3 E ON D.LevelCode = E.LevelCode AND A.ADCode = 'AL025' ");
            //    if (mIsESADAUseNewestData)
            //    {
            //        SQL.AppendLine("Inner JOIN  ");
            //        SQL.AppendLine("( ");
            //        SQL.AppendLine("	SELECT AdCode, MAX(StartDt) StartDt ");
            //        SQL.AppendLine("	FROM TblEmployeeAllowanceDeduction  ");
            //        SQL.AppendLine("	WHERE empcode = @EmpCode ");
            //        SQL.AppendLine("	GROUP BY ADCode ");
            //        SQL.AppendLine(") F ON A.ADCode = F.AdCode AND A.StartDt = F.StartDt ");
            //    }
            //    SQL.AppendLine("Where A.EmpCode = @EmpCode AND A.ADCode = 'AL025' ");
            //    SQL.AppendLine(") T1 ");
            //    SQL.AppendLine("ORDER BY T1.ADName ");

            //}
            //else
            //{
            //    SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, ");
            //    SQL.AppendLine(" IFNULL(A.Amt, 0.00) AS Amt, ");
            //    SQL.AppendLine("A.StartDt, A.EndDt, A.SiteCode, C.SiteName ");
            //    SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //    SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType=@ADType ");
            //    SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            //    SQL.AppendLine("INNER JOIN tblemployee D ON A.EmpCode = D.EmpCode ");
            //    SQL.AppendLine("Left Join TblLevelDtl3 E ON D.LevelCode = E.LevelCode AND A.ADCode = 'AL025' ");
            //    if (mIsESADAUseNewestData)
            //    {
            //        SQL.AppendLine("Inner JOIN  ");
            //        SQL.AppendLine("( ");
            //        SQL.AppendLine("	SELECT AdCode, MAX(StartDt) StartDt ");
            //        SQL.AppendLine("	FROM TblEmployeeAllowanceDeduction  ");
            //        SQL.AppendLine("	WHERE empcode = @EmpCode ");
            //        SQL.AppendLine("	GROUP BY ADCode ");
            //        SQL.AppendLine(") F ON A.ADCode = F.AdCode AND A.StartDt = F.StartDt ");
            //    }
            //    SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            //    SQL.AppendLine("Order By B.ADName;");
            //}
            #endregion

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, ");
            SQL.AppendLine(" IFNULL(A.Amt, 0.00) AS Amt, ");
            SQL.AppendLine("A.StartDt, A.EndDt, A.SiteCode, C.SiteName ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType=@ADType ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("INNER JOIN tblemployee D ON A.EmpCode = D.EmpCode ");
            SQL.AppendLine("Left Join TblLevelDtl3 E ON D.LevelCode = E.LevelCode AND A.ADCode = 'AL025' ");
            if (mIsESADAUseNewestData)
            {
                SQL.AppendLine("Inner JOIN  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	SELECT AdCode, MAX(StartDt) StartDt ");
                SQL.AppendLine("	FROM TblEmployeeAllowanceDeduction  ");
                SQL.AppendLine("	WHERE empcode = @EmpCode ");
                SQL.AppendLine("	GROUP BY ADCode ");
                SQL.AppendLine(") F ON A.ADCode = F.AdCode AND A.StartDt = F.StartDt ");
            }
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By B.ADName;");
            

            if (IsAllowance)
            {
                Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DNo", 
                        "ADCode", "ADName", "Amt", "StartDt", "EndDt", 
                        "SiteCode", "SiteName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4 });
                Sm.FocusGrd(Grd2, 0, 1);
            }
            else
            {
                Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] { "DNo", "ADCode", "ADName", "Amt", "StartDt", "EndDt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4 });
                Sm.FocusGrd(Grd3, 0, 1);
            }
        }

        internal void ShowEmployeeSalarySS(string EmpCode)
        {
            if (!mIsUseEmpSalarySS) return;

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.SSPCode, B.SSPName, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblSSProgram B On A.SSPCode=B.SSPCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.SSPCode, A.StartDt;");

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "SSPCode", "SSPName", "StartDt", "EndDt", "Amt" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd5, 0, 2);
        }

        internal void ShowEmployeeClaim(string EmpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ClaimCode, B.ClaimName, A.Yr, A.Amt, A.ClaimStartDt, A.ClaimEndDt ");
            SQL.AppendLine("From TblEmployeeClaim A ");
            SQL.AppendLine("Inner Join TblClaim B On A.ClaimCode=B.ClaimCode ");
            if (mIsESADAUseNewestData)
            {
                SQL.AppendLine("INNER JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	SELECT ClaimCode, MAX(Yr) Yr ");
                SQL.AppendLine("	FROM tblemployeeClaim ");
                SQL.AppendLine("	WHERE EmpCode = @EmpCode ");
                SQL.AppendLine("	GROUP BY ClaimCode ");
                SQL.AppendLine(") C ON A.ClaimCode = C.ClaimCode AND A.Yr = C.Yr ");
            }
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.ClaimCode, A.Yr;");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ClaimCode", "ClaimName", "Yr", "Amt", "ClaimStartDt", 
                    "ClaimEndDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd6, 0, 2);
        }

        internal void ShowEmployeePerformance(string EmpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, GradePerformance, Value ");
            SQL.AppendLine("From TblEmployeePerformance ");
            SQL.AppendLine("Where EmpCode=@EmpCode;");

            Sm.ShowDataInGrid(
                ref Grd7, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "GradePerformance", "Value" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void hideTab()
        {
            if (mIsEmpSalaryHideTab)
            {
                if (mEmployeeDeductionHideGroup.Length > 0)
                {
                    string grpCode = Sm.GetValue("Select grpCode from Tbluser Where UserCode = '" + Gv.CurrentUserCode + "'");
                    string data = Sm.GetValue("SELECT EXISTS(Select * From Tbluser " +
                                "Where Find_in_set('" + grpCode + "', " +
                                "( " +
                                "    SELECT parvalue From tblparameter " +
                                "    Where ParCode = 'EmployeeDeductionHideGroup' " +
                                "))) ; ");
                    if (data == "1")
                    {
                        TpSalary.PageVisible = TpAllowance.PageVisible = TpSalarySS.PageVisible
                        = TpClaim.PageVisible = TpPerformance.PageVisible = TpApproval.PageVisible = false;
                    }

                }

                try
                {
                    if (Sm.GetParameter("DocTitle") == "TWC")
                    {
                        decimal data = decimal.Parse(Sm.GetValue("Select LOCATE(A.GrpCode, (Select ifnull(parvalue, '0') From tblparameter Where ParCode ='EmpSalaryGroupAccess'))  " +
                        "from TblGroup A " +
                        "inner Join Tbluser B On A.GrpCode = B.GrpCode " +
                        "Where B.UserCode = '" + Gv.CurrentUserCode + "' "), 0);

                        if (data >= 1)
                        {
                            TpApproval.PageVisible = TpClaim.PageVisible = TpPerformance.PageVisible =
                                TpSalary.PageVisible = TpSalarySS.PageVisible = false;
                        }
                    }
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            
        }

        #endregion

        #endregion

        #region Event

        #region Grid Event

        #region Grid1 Event

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 3 }, e);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex))
            {
                if (e.ColIndex == 1) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid2 Event

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmEmpSalaryDlg2(this, "A", Grd2));
                }
                if (mIsEmpSalaryPensionBenefitEnabled && e.ColIndex == 9) GeneratePensionBenefit(e.RowIndex);

                if (Sm.IsGrdColSelected(new int[] { 1, 4, 5, 6, 8, 9 }, e.ColIndex))
                {
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd2, DteStartDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 6) Sm.DteRequestEdit(Grd2, DteEndDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 8) LueRequestEdit(Grd2, LueSiteCode, ref fCell, ref fAccept, e, 7);
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) Sm.FormShowDialog(new FrmEmpSalaryDlg2(this, "A", Grd2));
                if (mIsEmpSalaryPensionBenefitEnabled && e.ColIndex == 9)
                {
                    GeneratePensionBenefit(e.RowIndex);
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 4 }, e);
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 5 || e.ColIndex == 6)
            {
                int Col = e.ColIndex;
                if (Sm.GetGrdStr(Grd2, 0, Col).Length != 0)
                {
                    var Dt = Sm.ConvertDate(Sm.GetGrdDate(Grd2, 0, Col));
                    for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                    {
                        if (Sm.GetGrdStr(Grd2, r, 2).Length != 0)
                            Grd2.Cells[r, Col].Value = Dt;
                    }
                }
            }
        }

        private void LueSiteCode_Leave(object sender, EventArgs e)
        {
            if (LueSiteCode.Visible && fAccept)
            {
                if (Sm.GetLue(LueSiteCode).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LueSiteCode);
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueSiteCode.GetColumnValue("Col2");
                }
                LueSiteCode.Visible = false;
            }
        }

        private void DteStartDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void LueSiteCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void DteStartDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt2, ref fCell, ref fAccept);
        }

        private void DteEndDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt2, ref fCell, ref fAccept);
        }

        private void DteEndDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        #endregion

        #region Grid3 Event

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmpSalaryDlg2(this, "D", Grd3));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 4 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 4 });
                }
                if (e.ColIndex == 5) Sm.DteRequestEdit(Grd3, DteStartDtDed, ref fCell, ref fAccept, e);

                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd3, DteEndDtDed, ref fCell, ref fAccept, e);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) Sm.FormShowDialog(new FrmEmpSalaryDlg2(this, "D", Grd3));
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 4 }, e);
        }

        private void DteStartDtDed_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd3, ref fAccept, e);
        }

        private void DteStartDtDed_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDtDed, ref fCell, ref fAccept);
        }

        private void DteEndDtDed_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd3, ref fAccept, e);
        }

        private void DteEndDtDed_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDtDed, ref fCell, ref fAccept);
        }

      

        #endregion

        #region Grid5 Event

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd5, new int[] { 5 }, e);
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 3, 4, 5 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.DteRequestEdit(Grd5, DteStartDt3, ref fCell, ref fAccept, e);
                if (e.ColIndex == 4) Sm.DteRequestEdit(Grd5, DteEndDt3, ref fCell, ref fAccept, e);
                if (e.ColIndex == 2) LueRequestEdit(Grd5, LueSSPCode, ref fCell, ref fAccept, e, 1);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5 });
            }
        }

        private void LueSSPCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueSSPCode_Leave(object sender, EventArgs e)
        {
            if (LueSSPCode.Visible && fAccept)
            {
                if (Sm.GetLue(LueSSPCode).Length == 0)
                {
                    Grd5.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd5.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd5.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LueSSPCode);
                    Grd5.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueSSPCode.GetColumnValue("Col2");
                }
                LueSSPCode.Visible = false;
            }
        }

        private void DteStartDt3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DteEndDt3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DteStartDt3_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt3, ref fCell, ref fAccept);
        }

        private void DteEndDt3_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt3, ref fCell, ref fAccept);
        }

        #endregion

        #region Grid6 Event

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 3, 4, 5, 6 }, e.ColIndex))
            {
                if (e.ColIndex == 2) LueRequestEdit(Grd6, LueClaimCode, ref fCell, ref fAccept, e, 1);
                if (e.ColIndex == 5) Sm.DteRequestEdit(Grd6, DteClaimStartDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd6, DteClaimEndDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 4 });
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 4 }, e);
        }

        private void Grd6_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                int Col = e.ColIndex;
                if (Sm.GetGrdStr(Grd6, 0, Col).Length != 0)
                {
                    var Yr = Sm.GetGrdStr(Grd6, 0, Col);
                    for (int r = 0; r < Grd6.Rows.Count - 1; r++)
                    {
                        if (Sm.GetGrdStr(Grd6, r, 3).Length != 0)
                            Grd6.Cells[r, Col].Value = Yr;
                    }
                }
            }
        }

        private void LueClaimCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueClaimCode_Leave(object sender, EventArgs e)
        {
            if (LueClaimCode.Visible && fAccept)
            {
                if (Sm.GetLue(LueClaimCode).Length == 0)
                {
                    Grd6.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd6.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd6.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LueClaimCode);
                    Grd6.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueClaimCode.GetColumnValue("Col2");
                }
                LueClaimCode.Visible = false;
            }
        }

        #endregion

        #region Grid7 Event

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd7, LueGradePerformance, ref fCell, ref fAccept, e, 1);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 2 });
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
        }

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd7, new int[] { 2 }, e);
        }

        private void LueGradePerformance_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueGradePerformance_Leave(object sender, EventArgs e)
        {
            if (LueGradePerformance.Visible && fAccept)
            {
                if (Sm.GetLue(LueGradePerformance).Length == 0)
                    Grd7.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd7.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueGradePerformance);
                LueGradePerformance.Visible = false;
            }
        }


        #endregion

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpSalaryDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
                Grd3.Font = TheFont;
                Grd4.Font = TheFont;
                Grd5.Font = TheFont;
                Grd6.Font = TheFont;
                Grd7.Font = TheFont;
            }
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteClaimStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd6, ref fAccept, e);
        }

        private void DteClaimStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteClaimStartDt, ref fCell, ref fAccept);
        }

        private void DteClaimEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd6, ref fAccept, e);
        }

        private void DteClaimEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteClaimEndDt, ref fCell, ref fAccept);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueClaimCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueClaimCode), string.Empty);
        }
       
        private void LueSSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSSPCode, new Sm.RefreshLue1(Sl.SetLueSSPCode));
        }

        private void LuePerformanceStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePerformanceStatus, new Sm.RefreshLue2(Sl.SetLueOption), "PerformanceStatus");
        }

        private void LueGradePerformance_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGradePerformance, new Sm.RefreshLue2(Sl.SetLueOption), "GradePerformance");
        }

        #endregion

        #endregion    

        #region Class

        private class Emp
        {
            public string CompanyLogo { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string BirthPlace { get; set; }
            public string BirthDt { get; set; }
            public string GrdLvlCode { get; set; }
            public string JoinDt { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string MaritalStatus { get; set; }
            public string PKWT { get; set; }
            public string PKWTT { get; set; }
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
            public string Address { get; set; }
            public string EducationLevel { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string PermanentDt { get; set; }
            public string DocDt { get; set; }
            public decimal PenyesuaianStatus { get; set; }
            public decimal KenaikanJobGrade { get; set; }
            public decimal Total7 { get; set; }
            public decimal Total8a { get; set; }
            public decimal Total8b { get; set; }
        }

        private class EmpSal1
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpSal2
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpAD1
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpAD2
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpPPS
        {
            public string EmpCode { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string PKWT { get; set; }
            public string PKWTT { get; set; }
        }

        private class EmpPPS2
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public string JoinDt { get; set; }
            public decimal BasicSalary { get; set; }
            public string PerformanceStatus { get; set; }
        }

        private class AD // untuk menghitung pensiun benefit
        {
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

    }
}
