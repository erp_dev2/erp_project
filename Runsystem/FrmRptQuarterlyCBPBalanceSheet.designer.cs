﻿namespace RunSystem
{
    partial class FrmRptQuarterlyCBPBalanceSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkCCCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueProfitCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkProfitCenter = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkCCCode);
            this.panel2.Controls.Add(this.LueProfitCenter);
            this.panel2.Controls.Add(this.ChkProfitCenter);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.LueCCCode);
            this.panel2.Controls.Add(this.label5);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // ChkCCCode
            // 
            this.ChkCCCode.Location = new System.Drawing.Point(324, 46);
            this.ChkCCCode.Name = "ChkCCCode";
            this.ChkCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCCCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCCCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCCCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCCCode.Properties.Caption = " ";
            this.ChkCCCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCCCode.Size = new System.Drawing.Size(18, 22);
            this.ChkCCCode.TabIndex = 15;
            this.ChkCCCode.ToolTip = "Remove filter";
            this.ChkCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCCCode.ToolTipTitle = "Run System";
            this.ChkCCCode.CheckedChanged += new System.EventHandler(this.ChkCCCode_CheckedChanged);
            // 
            // LueProfitCenter
            // 
            this.LueProfitCenter.EnterMoveNextControl = true;
            this.LueProfitCenter.Location = new System.Drawing.Point(82, 26);
            this.LueProfitCenter.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfitCenter.Name = "LueProfitCenter";
            this.LueProfitCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.Appearance.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfitCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfitCenter.Properties.DropDownRows = 30;
            this.LueProfitCenter.Properties.MaxLength = 16;
            this.LueProfitCenter.Properties.NullText = "[Empty]";
            this.LueProfitCenter.Properties.PopupWidth = 300;
            this.LueProfitCenter.Size = new System.Drawing.Size(234, 20);
            this.LueProfitCenter.TabIndex = 11;
            this.LueProfitCenter.ToolTip = "F4 : Show/hide list";
            this.LueProfitCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfitCenter.EditValueChanged += new System.EventHandler(this.LueProfitCenter_EditValueChanged);
            // 
            // ChkProfitCenter
            // 
            this.ChkProfitCenter.Location = new System.Drawing.Point(324, 26);
            this.ChkProfitCenter.Name = "ChkProfitCenter";
            this.ChkProfitCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenter.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenter.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenter.Properties.Caption = " ";
            this.ChkProfitCenter.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenter.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenter.TabIndex = 12;
            this.ChkProfitCenter.ToolTip = "Remove filter";
            this.ChkProfitCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenter.ToolTipTitle = "Run System";
            this.ChkProfitCenter.CheckedChanged += new System.EventHandler(this.ChkProfitCenter_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(48, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "Year";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Cost Center";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(82, 4);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 9;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(82, 48);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.MaxLength = 16;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 300;
            this.LueCCCode.Size = new System.Drawing.Size(234, 20);
            this.LueCCCode.TabIndex = 14;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 10;
            this.label5.Text = "Profit Center";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRptQuarterlyCBPBalanceSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptQuarterlyCBPBalanceSheet";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkCCCode;
        private DevExpress.XtraEditors.LookUpEdit LueProfitCenter;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label label5;
    }
}