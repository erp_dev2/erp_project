﻿#region Update
/*
    01/03/2023 [WED/MNET] reporting
    06/03/2023 [WED/MNET] bug kolom margin BOQ
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectInitiation : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectInitiation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                ChkDocDt.Checked = true;
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.ProjectName, A.CtCode, B.CtName, C.OptDesc CtSegmentName, D.OptDesc ProjectTypeDesc, ");
            SQL.AppendLine("    E.UserName, Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("    Case A.ProcessInd When 'P' Then 'On Progress' When 'L' Then 'Lose' When 'C' Then 'Cancelled' When 'S' Then 'Lose' Else '' End As ProcessInd, ");
            SQL.AppendLine("    F.DocNo BOQDocNo, Case F.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As BOQStatusDesc, ");
            SQL.AppendLine("    Case F.ProcessInd When 'D' Then 'Draft' When 'F' Then 'Final' Else '' End As BOQProcessInd, IfNull(F.EstRevAmt, 0.00) EstRevAmt, IfNull(F.EstResAmt, 0.00) EstResAmt, ");
            SQL.AppendLine("    IfNull(F.EstRevAmt - F.EstResAmt, 0.00) GrandTotal, IfNull(If(F.EstRevAmt <= 0, 0.00, Round((((F.GrandTotal) / F.EstRevAmt) * 100), 2)), 0.00) MarginPercentage, ");
            SQL.AppendLine("    G.DocNo SOContractDocNo, Case G.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As SOCStatusDesc, ");
            SQL.AppendLine("    IfNull(G.Amt, 0.00) COGSAmt, IfNull(G.TotalResource, 0.00) TotalResource, IfNull(G.ProfitMarginAmt, 0.00) SOCProfitMarginAmt, IfNull(G.ProfitMarginPerc, 0.00) SOCProfitMarginPerc, ");
            SQL.AppendLine("    G.DocDt SOContractDocDt ");
            SQL.AppendLine("    From TblLOPHdr A ");
            SQL.AppendLine("    Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("    Left Join TblOption C On C.OptCat = 'CustomerSegmentation' And B.CtSegmentCode = C.OptCode ");
            SQL.AppendLine("    Left Join TblOption D On D.OptCat = 'ProjectType' And A.ProjectType = D.OptCode ");
            SQL.AppendLine("    Inner Join TblUser E On A.PICCode = E.UserCode ");
            SQL.AppendLine("    Left Join TblBOQHdr F On A.DocNo = F.LOPDocNo ");
            SQL.AppendLine("    Left Join TblSOContractHdr G On F.DocNo = G.BOQDocNo And G.CancelInd = 'N' ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "LOP#",
                    "",
                    "Date",
                    "Project Name",
                    "Customer",
                    
                    //6-10
                    "Customer Segmentation",
                    "Type",
                    "PIC Sales",
                    "Status",
                    "Process",

                    //11-15
                    "Bill of Quantity#",
                    "",
                    "Status",
                    "Process",
                    "Revenue",

                    //16-19
                    "COGS",
                    "Profit Margin",
                    "Margin (%)",
                    "SO Contract#",
                    "",

                    //21-25
                    "Status",
                    "Revenue",
                    "COGS",
                    "Profit Margin",
                    "Margin (%)"
                },
                new int[]
                {
                    //0
                    50, 

                    //1-5
                    180, 20, 80, 200, 200, 
                    
                    //6-10
                    150, 120, 120, 100, 100, 

                    //11-15
                    180, 20, 100, 100, 150,

                    //16-20
                    150, 150, 150, 180, 20,

                    //21-25
                    100, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 18, 22, 23, 24, 25 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 2, 12, 20 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = " Where 0 = 0 ";

                Sm.FilterStr(ref Filter, ref cm, TxtLOPDocNo.Text, new string[] { "T.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtBOQDocNo.Text, new string[] { "T.BOQDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtSOCDocNo.Text, new string[] { "T.SOContractDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "T.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.SOContractDocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocDt, T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-5
                        "DocDt", "ProjectName", "CtName", "CtSegmentName", "ProjectTypeDesc",
                        //6-10
                        "UserName", "StatusDesc", "ProcessInd", "BOQDocNo", "BOQStatusDesc",
                        //11-15
                        "BOQProcessInd", "EstRevAmt", "EstResAmt", "GrandTotal", "MarginPercentage",
                        //16-20
                        "SOContractDocNo", "SOCStatusDesc", "COGSAmt", "TotalResource", "SOCProfitMarginAmt",
                        //21
                        "SOCProfitMarginPerc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length > 0)
            {
                var f = new FrmBOQ3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length > 0)
            {
                var f = new FrmSOContract3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "SO Contract Date");
        }

        private void TxtLOPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLOPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "LOP#");
        }

        private void TxtBOQDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBOQDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "BOQ#");
        }

        private void TxtSOCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SOC#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        #endregion

        #endregion
    }
}
