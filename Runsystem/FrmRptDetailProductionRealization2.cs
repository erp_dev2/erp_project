﻿#region Update
/*
    27/12/2021 [IBL/YK] new apps. based on FrmRptDetailProductionRealization
    12/01/2022 [IBL/YK] BUG: Kolom Sub Bid. ada belum narik sourcenya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDetailProductionRealization2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mSQL = string.Empty, 
            mJointOperationTypeForKSOLead = string.Empty,
            mJointOperationTypeForKSOMember = string.Empty,
            mJointOperationTypeForNonKSO = string.Empty,
            mSiteCode = string.Empty,
            mTypeForDetailRealization = string.Empty,
            mDescForDetailRealization = string.Empty;

        private int[] mColDec = { 14, 15, 16, 20, 26, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55 };

        #endregion

        #region Constructor

        public FrmRptDetailProductionRealization2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 57;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "KSO/Non-KSO",
                    "Carry Over/New",
                    "NAS",
                    "UP",
                    "Project Code",
                    
                    //6-10
                    "Project Name", 
                    "Year",
                    "Resource",
                    //new column
                    "Scope of Work",
                    "Lay.",

                    //11-15
                    "Sub Bid.",
                    "Customer",
                    "Contract Date",
                    //end new column
                    "RKAP Amount",
                    "Total Resource",

                    //16-20
                    "Exclude PPN"+Environment.NewLine+"Percentage",
                    //new column
                    "Position LF",
                    "Position M",
                    "Joint Operation"+Environment.NewLine+"Member",
                    "Percentage (%)",

                    //21-25
                    "Joint Operation"+Environment.NewLine+"NPWP",
                    //end new column
                    "Approve",
                    "Not Approve",
                    "Start Date",
                    "End Date",

                    //26-30
                    "Number of "+Environment.NewLine+"Month",
                    "SY/MY",
                    "Contract Amount",
                    "Outstanding Amount",
                    "Production Plan"+ Environment.NewLine+"Amount",

                    //31-35
                    "Settled"+Environment.NewLine+"Amount",
                    "Realization"+Environment.NewLine+"January",
                    "Realization"+Environment.NewLine+"February",
                    "Realization"+Environment.NewLine+"March",
                    "Total 1st Quarterly",

                    //36-40
                    "Realization"+Environment.NewLine+"April",
                    "Realization"+Environment.NewLine+"May",
                    "Realization"+Environment.NewLine+"June",
                    "Total 2nd Quarterly",
                    "Realization"+Environment.NewLine+"July",

                    //41-45
                    "Realization"+Environment.NewLine+"August",
                    "Realization"+Environment.NewLine+"September",
                    "Total 3rd Quarterly",
                    "Realization"+Environment.NewLine+"October",
                    "Realization"+Environment.NewLine+"November",

                    //46-50
                    "Realization"+Environment.NewLine+"December",
                    "Total 4th Quarterly",
                    "Total Production",
                    "Percentage"+Environment.NewLine+"towards Target",
                    "Total Co +"+Environment.NewLine+"New Production",

                    //51-55
                    "Percentage"+Environment.NewLine+"towards Target",
                    "Outstanding Production",
                    "Percentage of Physical",
                    "Percentage of Time",
                    "Percentage of Cost",

                    //56
                    "Remark"

                    
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 60, 60, 150, 
                    
                    //6-10
                    200, 80, 150, 150, 150,

                    //11-15
                    200, 200, 150, 150, 150, 

                    //16-20
                    150, 80, 80, 200, 100, 

                    //21-25
                    150, 100, 100, 150, 150,
                    
                    //26-30
                    150, 50, 150, 150, 150, 

                    //31-35
                    150, 150, 150, 150, 150, 

                    //36-40
                    150, 150, 150, 150, 150, 

                    //41-45
                    150, 150, 150, 150, 150, 

                    //46-50
                    150, 150, 150, 150, 150, 
                    
                    //51-55
                    150, 150, 150, 150, 150,
                    
                    //56
                    200

                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 13, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProductionRealization>();
                var l2 = new List<AuctionBranchToSite>();
                var l3 = new List<DummyCounter>();
                var l4 = new List<SiteCounter>();

                mSiteCode = Sm.GetLue(LueSiteCode);

                ProcessSiteCounter(ref l3);
                ProcessBranchToSite(ref l2);

                if (l2.Count > 0 && l3.Count > 0)
                {
                    ProcessSiteCounter2(ref l2, ref l3, ref l4);
                }

                Process1(ref l, ref l4);
                if (l.Count > 0)
                {
                   Process2(ref l);
                   Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods


        private void ProcessBranchToSite(ref List<AuctionBranchToSite> l)
        {
            string sSQL = "Select OptCode, OptDesc From TblOption Where OptCat = 'AuctionBranchToSite';";
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = sSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionBranchToSite()
                        {
                            BranchCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                    {
                        if (mSiteCode.Length > 0) mSiteCode += ",";
                        mSiteCode += x.BranchCode;
                    }
                }

                if (mSiteCode.Length == 0) mSiteCode = Sm.GetLue(LueSiteCode);
            }
        }


        private void ProcessSiteCounter(ref List<DummyCounter> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SiteCode, Count(A.DocNo) Counter ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("    And C.Status = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    And Left(C.DocDt, 4) = @Yr ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr D On C.DocNo = D.SOContractDocNo ");
            SQL.AppendLine("Group By A.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "Counter" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DummyCounter()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            No = Sm.DrInt(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSiteCounter2(ref List<AuctionBranchToSite> l, ref List<DummyCounter> l2, ref List<SiteCounter> l3)
        {
            foreach (var x in l2)
            {
                foreach (var y in l)
                {
                    if (x.SiteCode == y.BranchCode)
                    {
                        x.SiteCode = y.SiteCode;
                        break;
                    }
                }
            }

            l3 = l2.GroupBy(x => x.SiteCode)
                .Select(t => new SiteCounter()
                {
                    SiteCode = t.Key,
                    No = 0,
                    No2 = t.Sum(s => s.No)
                }).ToList();

            for (int i = 0; i < l3.Count; ++i)
            {
                if (i != 0)
                {
                    l3[i].No = l3[i - 1].No2 + 1;
                    l3[i].No2 += l3[i - 1].No2;
                }
                else
                {
                    l3[i].No = 1;
                }
            }
        }


        private void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT SiteCode Col1, SiteName Col2 ");
            SQL.AppendLine("FROM tblsite ");
            SQL.AppendLine("WHERE FIND_IN_SET(sitecode, (SELECT parvalue FROM tblparameter WHERE parcode = 'SiteCodeForAuctionInfo')) ");
            SQL.AppendLine("AND sitecode NOT IN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT optcode ");
            SQL.AppendLine("    FROM tbloption ");
            SQL.AppendLine("    WHERE optcat = 'AuctionBranchToSite' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By SiteCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
       


        private void GetParameter()
        {
            mTypeForDetailRealization = Sm.GetParameter("TypeForDetailRealization");
            mDescForDetailRealization = Sm.GetParameter("DescForDetailRealization");
            mJointOperationTypeForKSOLead = Sm.GetParameter("JointOperationTypeForKSOLead");
            mJointOperationTypeForKSOMember = Sm.GetParameter("JointOperationTypeForKSOMember");
            mJointOperationTypeForNonKSO = Sm.GetParameter("JointOperationTypeForNonKSO");

            if (mTypeForDetailRealization.Length == 0) mTypeForDetailRealization = "Carry Over,Proyek Baru";
            if (mDescForDetailRealization.Length == 0) mDescForDetailRealization = "PROYEK-PROYEK NON-KSO,PROYEK-PROYEK KSO,PROYEK-PROYEK KSO (Member) ";
       
        }

        private void Process1(ref List<ProductionRealization> l, ref List<SiteCounter> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int mUP = 1;
            int mNas = 1;
            string[] SiteCode = mSiteCode.Split(',');

            foreach (var x in l2)
            {
                if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                {
                    mNas = x.No;
                    break;
                }
            }

            string[] mProjectDesc = mDescForDetailRealization.Split(',');
            string[] mProjectType = mTypeForDetailRealization.Split(',');

            SQL.AppendLine("SELECT T1.* FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                for (int j = 0; j < mProjectType.Count(); ++j)
                {
                    SQL.AppendLine("SELECT '" + mProjectDesc[i] + "' AS ProjectDesc, '" + mProjectType[j] + "' AS ProjectDesc2,  ");
                    SQL.AppendLine("If(E1.ParValue = '2', A.ProjectCode2, A.ProjectCode) ProjectCode2, C.ProjectName , Left(D.DocDt, 4) SOContractRevisionYr, E.OptDesc AS ProjectResource, IFNULL(F.Amt, 0.00) AS RKAPAmt, ");
                    SQL.AppendLine("(G.RemunerationAmt+G.DirectCostAmt) TotalResource, G.ExclPPNPercentage, Case G.Status When 'A' Then '1' ELSE '0' END AS Approve, ");
                    SQL.AppendLine("Case G.Status When 'O' Then '1' ELSE '0' END AS NotApprove, Date_Format(D.DocDt,'%d %M %Y') DocDt, Date_Format(H.DeliveryDt, '% d %M %Y') DeliveryDt, TIMESTAMPDIFF(MONTH, D.DocDt, H.DeliveryDt) AS NumberOfMonth, ");
                    SQL.AppendLine("Case When TIMESTAMPDIFF(MONTH, D.DocDt, H.DeliveryDt) <=12 Then 'SY'  ");
                    SQL.AppendLine("When TIMESTAMPDIFF(MONTH, D.DocDt, H.DeliveryDt) >12 Then  'MY' END AS Duration, IfNull(H.Amt, 0.00) ProductionTotal,   ");
                    SQL.AppendLine("(IfNull(H.Amt, 0.00)-IfNull(J.SettledAmt, 0.00)) OutstandingAmt , I.TotalPrice ProductionPlanAmt, IfNull(J.SettledAmt, 0.00) SettledAmt, ");
                    SQL.AppendLine("IFNULL(K.Amt1, 0.00) Amt1, IFNULL(K.Amt2, 0.00) Amt2, IFNULL(K.Amt3, 0.00) Amt3, IFNULL(K.Amt4, 0.00) Amt4, IFNULL(K.Amt5, 0.00) Amt5, ");
                    SQL.AppendLine("IFNULL(K.Amt6, 0.00) Amt6, IFNULL(K.Amt7, 0.00) Amt7, IFNULL(K.Amt8, 0.00) Amt8, IFNULL(K.Amt9, 0.00) Amt9, IFNULL(K.Amt10, 0.00) Amt10, ");
                    SQL.AppendLine("IFNULL(K.Amt11, 0.00) Amt11, IFNULL(K.Amt12, 0.00) Amt12, D.Amt SOCRAmt, I.TotalAchievement, R.Remark,");
                    SQL.AppendLine("N.OptDesc As ProjectScope, O.OptDesc As ProjectType, L.ItName, M.CtName, Date_Format(A.DocDt,'%d %M %Y') As ContractDt,");
                    SQL.AppendLine("Case P.OptCode When @JointOperationTypeForKSOLead Then P.OptDesc Else Null End As PositionLF,");
                    SQL.AppendLine("Case P.OptCode When @JointOperationTypeForKSOMember Then P.OptDesc Else Null End As PositionM,");
                    SQL.AppendLine("L.JOName, IfNull(L.PortionPercentage, 0.00) As PortionPercentage,");
                    SQL.AppendLine("L.NPWP , IfNull(((IfNull(Q.Amt, 0.00) / (I.RemunerationAmt + I.DirectCostAmt))*100),0.00) CostPercentage ");
                    SQL.AppendLine("From TblSOContractHdr A ");
                    SQL.AppendLine("INNER JOIN TblBOQHdr B ON A.BOQDocNo = B.DocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND A.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND A.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND A.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("    And LEFT(A.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("    And LEFT(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("INNER JOIN TblLOPHdr C ON B.LOPDocNo =  C.DocNo ");
                    SQL.AppendLine("	AND FIND_IN_SET(C.SiteCode, @SiteCode) ");
                    SQL.AppendLine("INNER JOIN (SELECT MAX(DocNo) DocNo, SOCDocNo FROM TblSOContractRevisionHdr GROUP BY SOCDocNo) C1 ON A.DocNo = C1.SOCDocNo ");
                    SQL.AppendLine("Inner JOin TblSOContractRevisionHdr D ON C1.DocNo = D.DocNo ");
                    SQL.AppendLine("INNER JOIN TblOption E ON C.ProjectResource = E.OptCode AND OptCat = 'ProjectResource' ");
                    SQL.AppendLine("Left Join TblParameter E1 On E1.ParCode = 'ProjectAcNoFormula' ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("(  ");
                    SQL.AppendLine("	SELECT SUM(T2.Amt) Amt ");
                    SQL.AppendLine("	From TblCompanyBudgetPlanHdr T1 ");
                    SQL.AppendLine("	INNER JOIN TblCompanyBudgetPlanDtl T2 ON T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("	    AND T1.Yr = @Yr   ");
                    SQL.AppendLine("	    AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("	    AND T2.Amt != 0.00 ");
                    SQL.AppendLine("	INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
                    SQL.AppendLine("	    AND T3.ActInd = 'Y'");
                    SQL.AppendLine("	INNER JOIN TblParameter T4 ON T4.ParCode = 'COALevelFicoSettingJournalToCBP' ");
                    SQL.AppendLine("	    AND T4.ParValue IS NOT NULL ");
                    SQL.AppendLine("	    AND T3.Level = CONVERT(T4.ParValue, DECIMAL)+1 ");
                    SQL.Append("       AND Find_In_Set(T2.AcNo, '");
                    for (int x = 0; x < SiteCode.Length; x++)
                    {
                        SQL.Append((i == 0 ? "4.00." : "4.10.") + SiteCode[x] + "");
                        if (x != (SiteCode.Length - 1)) SQL.Append(",");
                    }
                    SQL.AppendLine("') ");  
                    SQL.AppendLine(") F On 0 = 0 ");
                    SQL.AppendLine("Inner JOIN TblProjectImplementationHdr I ON D.DocNo = I.SOContractDocNo ");
                    SQL.AppendLine("Inner Join (Select Max(DocNo) DocNo, PRJIDocNo From TblProjectImplementationRevisionHdr Group By PRJIDocNo) G1 ON I.DocNo = G1.PRJIDocNo ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr G ON G1.DocNo = G.DocNo ");
                    SQL.AppendLine("INNER JOIN TblSOContractRevisionDtl H ON D.DocNo = H.DocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("	SELECT DocNo, SUM(SettledAmt) SettledAmt ");
                    SQL.AppendLine("	FROM TblProjectImplementationDtl ");
                    SQL.AppendLine("	WHERE SettledInd = 'Y' ");
                    SQL.AppendLine("	AND SettleDt IS NOT NULL ");
                    SQL.AppendLine("	AND LEFT(SettleDt, 4) = @Yr - 1 ");
                    SQL.AppendLine("	AND SettledAmt != 0 ");
                    SQL.AppendLine("	GROUP BY DocNo ");
                    SQL.AppendLine(") J ON J.DocNo = I.DocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("	SELECT X1.SOCDocNo, SUM(X1.Amt1) Amt1, SUM(X1.Amt2) Amt2, SUM(X1.Amt3) Amt3, SUM(X1.Amt4) Amt4,  ");
                    SQL.AppendLine("	SUM(X1.Amt5) Amt5, SUM(X1.Amt6) Amt6, SUM(X1.Amt7) Amt7, SUM(X1.Amt8) Amt8, SUM(X1.Amt9) Amt9, ");
                    SQL.AppendLine("	SUM(X1.Amt10) Amt10, SUM(X1.Amt11) Amt11, SUM(X1.Amt12) Amt12  ");
                    SQL.AppendLine("	FROM  ");
                    SQL.AppendLine("	(  ");
                    SQL.AppendLine("		SELECT T.SOCDocNo, T.Mth, If(T.Mth = '01', T.SettledAmt, 0.00) Amt1, If(T.Mth = '02', T.SettledAmt, 0.00) Amt2,  ");
                    SQL.AppendLine("		If(T.Mth = '03', T.SettledAmt, 0.00) Amt3, If(T.Mth = '04', T.SettledAmt, 0.00) Amt4, If(T.Mth = '05', T.SettledAmt, 0.00) Amt5,  ");
                    SQL.AppendLine("		If(T.Mth = '06', T.SettledAmt, 0.00) Amt6, If(T.Mth = '07', T.SettledAmt, 0.00) Amt7, If(T.Mth = '08', T.SettledAmt, 0.00) Amt8,  ");
                    SQL.AppendLine("		If(T.Mth = '09', T.SettledAmt, 0.00) Amt9, If(T.Mth = '10', T.SettledAmt, 0.00) Amt10, If(T.Mth = '11', T.SettledAmt, 0.00) Amt11, ");
                    SQL.AppendLine("		If(T.Mth = '12', T.SettledAmt, 0.00) Amt12  ");
                    SQL.AppendLine("		FROM  ");
                    SQL.AppendLine("		(  ");
                    SQL.AppendLine("		  SELECT T3.SOCDocNo, SUBSTR(T2.SettleDt, 5, 2) Mth,");
                    SQL.AppendLine("		  Case when T9.AcType = 'D' Then IfNull((T8.DAmt - T8.CAMt), 0.00)");
                    SQL.AppendLine("		  		 When T9.AcType = 'C' Then IfNull((T8.CAmt - T8.DAMt), 0.00)");
                    SQL.AppendLine("		  End As SettledAmt");
                    SQL.AppendLine("		  FROM (SELECT MAX(DocNo) DocNo, PRJIDocNo FROM TblProjectImplementationRevisionHdr WHERE LEFT(DocDt, 4) = @Yr GROUP BY PRJIDocNo) T1  ");
                    SQL.AppendLine("		  Inner Join TblProjectImplementationHdr T11 On T1.PRJIDocNo = T11.DocNo ");
                    SQL.AppendLine("		  INNER JOIN TblProjectImplementationDtl5 T2 ON T11.DocNo = T2.DocNo  ");
                    SQL.AppendLine("		  		AND T2.SettledInd = 'Y' ");
                    SQL.AppendLine("		  		AND T2.SettleDt IS NOT NULL  ");
                    SQL.AppendLine("		  INNER JOIN TblSOContractRevisionHdr T3 ON T11.SOContractDocNo = T3.DocNo ");
                    SQL.AppendLine("		  INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo  ");
                    SQL.AppendLine("		      AND T4.CancelInd = 'N'  ");
                    SQL.AppendLine("		      AND T4.Status = 'A'  ");
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("    And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("    And LEFT(T4.DocDt, 4) = @Yr ");
                    SQL.AppendLine("		  INNER JOIN TblBOQHdr T5 ON T4.BOQDocNo = T5.DocNo  ");
                    SQL.AppendLine("		  INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo  ");
                    SQL.AppendLine("		      AND FIND_IN_SET(T6.SiteCode, @SiteCode)");
                    SQL.AppendLine("		  Inner Join TblProjectDeliveryHdr T7 On T11.DocNo = T7.PRJIDocNo");
                    SQL.AppendLine("		  		And T7.CancelInd = 'N'");
                    SQL.AppendLine("		  		And T7.Status = 'A'");
                    SQL.AppendLine("		  Inner Join TblJournalDtl T8 On T7.JournalDocNo = T8.DocNo");
                    SQL.AppendLine("		  Inner Join TblCOA T9 On T8.AcNo = T9.AcNo");
                    SQL.AppendLine("		  		And T9.ActInd = 'Y'");
                    SQL.AppendLine("            And ( ");
                    for (int x = 0; x < SiteCode.Length; x++)
                        SQL.AppendLine("            T9.AcNo Like '" + (i == 0 ? "4.00." : "4.10.") + SiteCode[x] + ".%' " + (x != (SiteCode.Length - 1) ? "Or" : ""));
                    SQL.AppendLine(")");
                    SQL.AppendLine("		) T  ");
                    SQL.AppendLine("	) X1  ");
                    SQL.AppendLine("	GROUP BY X1.SOCDocNo ");
                    SQL.AppendLine(") K ON K.SOCDocNo = A.DocNo");
                    SQL.AppendLine("Left Join");
                    SQL.AppendLine("(");
                    SQL.AppendLine("	Select X1.DocNo, Group_Concat(X3.ItName) As ItName,");
                    SQL.AppendLine("	Group_Concat(X5.JOName) As JOName,");
                    SQL.AppendLine("	Group_Concat(IfNull(X4.PortionPercentage, 0.00)) As PortionPercentage,");
                    SQL.AppendLine("	Group_Concat(X4.NPWP) As NPWP");
                    SQL.AppendLine("	From TblSOContractHdr X1");
                    SQL.AppendLine("	Inner Join TblSOContractDtl X2 On X1.DocNo = X2.DocNo");
                    if (i == 0)
                        SQL.AppendLine("    AND X1.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND X1.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND X1.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("    And LEFT(X1.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("    And LEFT(X1.DocDt, 4) = @Yr ");
                    SQL.AppendLine("	Inner Join TblItem X3 On X2.ItCode = X3.ItCode ");
                    SQL.AppendLine("	Left Join TblSOContractDtl3 X4 On X1.DocNo = X4.DocNo");
                    SQL.AppendLine("	Left Join TblJointOperationHdr X5 On X5.JOCode = X4.JOCode");
                    SQL.AppendLine("	Group By X2.DocNo");
                    SQL.AppendLine(")L On A.DocNo = L.DocNo");
                    SQL.AppendLine("Left Join TblCustomer M On A.CtCode = M.CtCode");
                    SQL.AppendLine("Left Join TblOption N On N.OptCat = 'ProjectScope' And C.ProjectScope = N.OptCode");
                    SQL.AppendLine("Inner Join TblOption O On O.OptCat = 'ProjectType' And C.ProjectType = O.OptCode");
                    SQL.AppendLine("Left Join TblOption P On P.OptCat = 'JointOperationType' And P.OptCode = A.JOType");
                    SQL.AppendLine("Left Join");
                    SQL.AppendLine("(");
                    SQL.AppendLine("	Select X1.PRJIDocNo, Sum(X3.Amt) As Amt");
                    SQL.AppendLine("	From TblDroppingRequestHdr X1");
                    SQL.AppendLine("	Left Join TblDroppingPaymentHdr X2 On X1.DocNo = X2.DRQDocNo");
                    SQL.AppendLine("		And X1.CancelInd = 'N' And X1.`Status` = 'A'");
                    SQL.AppendLine("		And X2.CancelInd = 'N' And X2.`Status` = 'A'");
                    SQL.AppendLine("	Left Join TblVoucherHdr X3 On X2.VoucherRequestDocNo = X3.VoucherRequestDocNo");
                    SQL.AppendLine("		And X3.CancelInd = 'N'");
                    SQL.AppendLine("	Group By X1.PRJIDocNo");
                    SQL.AppendLine(")Q On I.DocNo = Q.PRJIDocNo");
                    SQL.AppendLine("Left Join");
                    SQL.AppendLine("(");
                    SQL.AppendLine("	Select PRJIDocNo, Group_Concat(Remark) As Remark");
                    SQL.AppendLine("	From TblProjectDeliveryHdr Where CancelInd = 'N' And Status = 'A'");
                    SQL.AppendLine("	Group By PRJIDocNo");
                    SQL.AppendLine(")R On I.DocNo = R.PRJIDocNo");

                    if (i != (mProjectDesc.Count() - 1) || j != (mProjectType.Count() - 1))
                        SQL.AppendLine("    UNION ALL ");
                }
            }

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Order By T1.ProjectDesc, T1.ProjectDesc2; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOLead", mJointOperationTypeForKSOLead);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOMember", mJointOperationTypeForKSOMember);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForNonKSO", mJointOperationTypeForNonKSO);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-5
                    "ProjectDesc2", "ProjectCode2", "ProjectName", "SOContractRevisionYr", "ProjectResource", 
                    //6-10
                    "RKAPAmt", "TotalResource", "ExclPPNPercentage", "Approve", "NotApprove", 
                    //11-15
                    "DocDt", "DeliveryDt", "NumberOfMonth", "Duration", "ProductionTotal", 
                    //16-20
                    "OutstandingAmt", "ProductionPlanAmt", "SettledAmt", "Amt1", "Amt2", 
                    //21-25
                    "Amt3", "Amt4", "Amt5", "Amt6", "Amt7", 
                    //26-30
                    "Amt8", "Amt9", "Amt10", "Amt11", "Amt12", 
                    //31-35
                    "SOCRAmt", "TotalAchievement", "Remark", "ProjectScope", "ProjectType", 
                    //36-40
                    "ItName", "CtName", "ContractDt", "PositionLF", "PositionM", 
                    //41-44
                    "JOName", "PortionPercentage", "NPWP", "CostPercentage"
            });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionRealization()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            ProjectDesc2 = Sm.DrStr(dr, c[1]),
                            NAS = mNas.ToString(),
                            UP = mUP.ToString(),
                            ProjectCode2 = Sm.DrStr(dr, c[2]),
                            ProjectName = Sm.DrStr(dr, c[3]),
                            Yr = Sm.DrStr(dr, c[4]),
                            ProjectResource = Sm.DrStr(dr, c[5]),
                            RKAPAmt = Sm.DrDec(dr, c[6]),
                            TotalResource = Sm.DrDec(dr, c[7]),
                            ExcludePPNPercentage = Sm.DrDec(dr, c[8]),
                            Approve = Sm.DrStr(dr, c[9]),
                            NotApprove = Sm.DrStr(dr, c[10]),
                            StartDate = Sm.DrStr(dr, c[11]),
                            EndDate = Sm.DrStr(dr, c[12]),
                            NumberofMonth = Sm.DrDec(dr, c[13]),
                            SYMY = Sm.DrStr(dr, c[14]),
                            ContractAmt = Sm.DrDec(dr, c[15]),
                            OutstandingAmt = Sm.DrDec(dr, c[16]),
                            ProductionAmt = Sm.DrDec(dr, c[17]),
                            SettledAmt = Sm.DrDec(dr, c[18]),
                            Amt1 = Sm.DrDec(dr, c[19]),
                            Amt2 = Sm.DrDec(dr, c[20]),
                            Amt3 = Sm.DrDec(dr, c[21]),
                            Total1 = Sm.DrDec(dr, c[19]) + Sm.DrDec(dr, c[20]) + Sm.DrDec(dr, c[21]),
                            Amt4 = Sm.DrDec(dr, c[22]),
                            Amt5 = Sm.DrDec(dr, c[23]),
                            Amt6 = Sm.DrDec(dr, c[24]),
                            Total2 = Sm.DrDec(dr, c[22]) + Sm.DrDec(dr, c[23]) + Sm.DrDec(dr, c[24]),
                            Amt7 = Sm.DrDec(dr, c[25]),
                            Amt8 = Sm.DrDec(dr, c[26]),
                            Amt9 = Sm.DrDec(dr, c[27]),
                            Total3 = Sm.DrDec(dr, c[25]) + Sm.DrDec(dr, c[26]) + Sm.DrDec(dr, c[27]),
                            Amt10 = Sm.DrDec(dr, c[28]),
                            Amt11 = Sm.DrDec(dr, c[29]),
                            Amt12 = Sm.DrDec(dr, c[30]),
                            Total4 = Sm.DrDec(dr, c[28]) + Sm.DrDec(dr, c[29]) + Sm.DrDec(dr, c[30]),
                            TotalProduction = 0m,
                            PercentageTarget1 = 0m,
                            TotalCoNewProduction = Sm.DrDec(dr, c[31]),
                            PercentageTarget2 = 0m,
                            OutstandingProduction = 0m,
                            PercentageofPhysical = Sm.DrDec(dr, c[32]),
                            PercentageofTime = Sm.DrDec(dr, c[32]),
                            Remark = Sm.DrStr(dr, c[33]),
                            ProjectScope = Sm.DrStr(dr, c[34]),
                            Lay = Sm.DrStr(dr, c[35]),
                            ItName = Sm.DrStr(dr, c[36]),
                            CtName = Sm.DrStr(dr, c[37]),
                            ContractDt2 = Sm.DrStr(dr, c[38]),
                            PositionLF = Sm.DrStr(dr, c[39]),
                            PositionM = Sm.DrStr(dr, c[40]),
                            JOName = Sm.DrStr(dr, c[41]),
                            Portion = Sm.DrDec(dr, c[42]),
                            NPWP = Sm.DrStr(dr, c[43]),
                            PercentageofCost = Sm.DrDec(dr, c[44]),
                        });

                        mUP += 1;
                        mNas += 1;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProductionRealization> l)
        {
            foreach (var x in l)
            {
                x.TotalProduction = x.Total1 + x.Total2 + x.Total3 + x.Total4;
                x.PercentageTarget1 = (x.TotalProduction / x.ProductionAmt) * 100;
                x.PercentageTarget2 = (x.TotalCoNewProduction / x.ContractAmt) * 100;
                x.OutstandingProduction = x.ContractAmt - x.TotalCoNewProduction;
            }
        }

        private void Process3(ref List<ProductionRealization> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.ProjectDesc2;
                Grd1.Cells[Row, 3].Value = x.NAS;
                Grd1.Cells[Row, 4].Value = x.UP;
                Grd1.Cells[Row, 5].Value = x.ProjectCode2;
                Grd1.Cells[Row, 6].Value = x.ProjectName;
                Grd1.Cells[Row, 7].Value = x.Yr;
                Grd1.Cells[Row, 8].Value = x.ProjectResource;
                Grd1.Cells[Row, 9].Value = x.ProjectScope;
                Grd1.Cells[Row, 10].Value = x.Lay;
                Grd1.Cells[Row, 11].Value = x.ItName;
                Grd1.Cells[Row, 12].Value = x.CtName;
                Grd1.Cells[Row, 13].Value = x.ContractDt2;
                Grd1.Cells[Row, 14].Value = x.RKAPAmt;
                Grd1.Cells[Row, 15].Value = x.TotalResource;
                Grd1.Cells[Row, 16].Value = x.ExcludePPNPercentage;
                Grd1.Cells[Row, 17].Value = x.PositionLF;
                Grd1.Cells[Row, 18].Value = x.PositionM;
                Grd1.Cells[Row, 19].Value = x.JOName;
                Grd1.Cells[Row, 20].Value = x.Portion;
                Grd1.Cells[Row, 21].Value = x.NPWP;
                Grd1.Cells[Row, 22].Value = x.Approve;
                Grd1.Cells[Row, 23].Value = x.NotApprove;
                Grd1.Cells[Row, 24].Value = x.StartDate;
                Grd1.Cells[Row, 25].Value = x.EndDate;
                Grd1.Cells[Row, 26].Value = x.NumberofMonth;
                Grd1.Cells[Row, 27].Value = x.SYMY;
                Grd1.Cells[Row, 28].Value = x.ContractAmt;
                Grd1.Cells[Row, 29].Value = x.OutstandingAmt;
                Grd1.Cells[Row, 30].Value = x.ProductionAmt;
                Grd1.Cells[Row, 31].Value = x.SettledAmt;
                Grd1.Cells[Row, 32].Value = x.Amt1;
                Grd1.Cells[Row, 33].Value = x.Amt2;
                Grd1.Cells[Row, 34].Value = x.Amt3;
                Grd1.Cells[Row, 35].Value = x.Total1;
                Grd1.Cells[Row, 36].Value = x.Amt4;
                Grd1.Cells[Row, 37].Value = x.Amt5;
                Grd1.Cells[Row, 38].Value = x.Amt6;
                Grd1.Cells[Row, 39].Value = x.Total2;
                Grd1.Cells[Row, 40].Value = x.Amt7;
                Grd1.Cells[Row, 41].Value = x.Amt8;
                Grd1.Cells[Row, 42].Value = x.Amt9;
                Grd1.Cells[Row, 43].Value = x.Total3;
                Grd1.Cells[Row, 44].Value = x.Amt10;
                Grd1.Cells[Row, 45].Value = x.Amt11;
                Grd1.Cells[Row, 46].Value = x.Amt12;
                Grd1.Cells[Row, 47].Value = x.Total4;
                Grd1.Cells[Row, 48].Value = x.TotalProduction;
                Grd1.Cells[Row, 49].Value = x.PercentageTarget1;
                Grd1.Cells[Row, 50].Value = x.TotalCoNewProduction;
                Grd1.Cells[Row, 51].Value = x.PercentageTarget2;
                Grd1.Cells[Row, 52].Value = x.OutstandingProduction;
                Grd1.Cells[Row, 53].Value = x.PercentageofPhysical;
                Grd1.Cells[Row, 54].Value = x.PercentageofTime;
                Grd1.Cells[Row, 55].Value = x.PercentageofCost;
                Grd1.Cells[Row, 56].Value = x.Remark;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.GroupObject.Add(2);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }

  
        #endregion

        #endregion

        #region Class

        private class ProductionRealization
        {
            public string ProjectDesc { get; set; }
            public string ProjectDesc2 { get; set; }
            public string NAS { get; set; }
            public string UP { get; set; }
            public string ProjectCode2 { get; set; }
            public string ProjectName { get; set; }
            public string Yr { get; set; }
            public string ProjectResource { get; set; }
            public decimal RKAPAmt { get; set; }
            public decimal TotalResource { get; set; }
            public decimal ExcludePPNPercentage { get; set; }
            public string Approve { get; set; }
            public string NotApprove { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public decimal NumberofMonth { get; set; }
            public string SYMY { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal OutstandingAmt { get; set; }
            public decimal ProductionAmt { get; set; }
            public decimal SettledAmt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Total1 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Total2 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Total3 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal Total4 { get; set; }
            public decimal TotalProduction { get; set; }
            public decimal PercentageTarget1 { get; set; }
            public decimal TotalCoNewProduction { get; set; }
            public decimal PercentageTarget2 { get; set; }
            public decimal OutstandingProduction { get; set; }
            public decimal PercentageofPhysical { get; set; }
            public decimal PercentageofTime { get; set; }
            public string Remark { get; set; }
            public string ProjectScope { get; set; }
            public string Lay { get; set; }
            public string ItName { get; set; }
            public string CtName { get; set; }
            public string ContractDt2 { get; set; }
            public string PositionLF { get; set; }
            public string PositionM { get; set; }
            public string JOName { get; set; }
            public decimal Portion { get; set; }
            public string NPWP { get; set; }
            public decimal PercentageofCost { get; set; }
        }

        private class AuctionBranchToSite
        {
            public string BranchCode { get; set; }
            public string SiteCode { get; set; }
        }

        private class SiteCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
            public int No2 { get; set; }
        }

        private class DummyCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
        }

        #endregion
    }
}
