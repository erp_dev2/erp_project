﻿#region Update
// 25/07/2018 [HAR] feedback balance
// 25/07/2018 [HAR] feedback nilai opening balance ambil dari closing balance bulan sebelumnya 
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintCashBook : RunSystem.FrmBase11
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mSQL = string.Empty, 
            startYr = string.Empty,
            mPOPrintOutCompanyLogo = "1";
        private bool
           mIsReportingFilterByEntity = false
           ;

        #endregion

        #region Constructor

        public FrmPrintCashBook(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                //Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, 0);
                Sl.SetLueUserCode(ref LueKasir);
                Sl.SetLueUserCode(ref LueMgrKeu);
                Sl.SetLueUserCode(ref LueDirKeu);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date")||
                 Sm.IsDteEmpty(DteDocDt2, "End date")
                ) return;
            try
            {
              if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
              Cursor.Current = Cursors.WaitCursor;
              ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Cursor.Current = Cursors.Default;
            }

        }

        #region additional

        private void ParPrint()
        {
            string BankAcType = string.Empty;
            decimal bals = 0;
            string BankAcCode = string.Empty;
            decimal bals2 = 0;
            decimal No = 0, Nomor = 0;

            var l = new List<CashBook1>();
            var l2 = new List<CashBook2>();

            string[] TableName = { "CashBook1", "CashBook2" };
            List<IList> myLists = new List<IList>();
           
            #region Header CashBook1
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("DATE_FORMAT(@DocDt, '%W, %d %M %Y') As DocDt,");
            SQL.AppendLine("A.BankAcCode, A.BankAcnm, A.bankActp, A.bankAcno, @Year As Year,  @MonthFilter As MonthFilter,  ");
            SQL.AppendLine("@Dt As Dt, ifnull(B.Opening, 0) As opening, ifnull(C.Credit, 0) Credit, ifnull(C.Debet, 0) Debet,  ");
            SQL.AppendLine("(ifnull(B.Opening, 0)+ifnull(C.Credit, 0)-ifnull(C.Debet, 0)) As Balance ");
            SQL.AppendLine("From TblbankAccount A ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select B.BankAcCode, @Year,  @MonthFilter As VcMonth,   ");
	        SQL.AppendLine("    'Opening Balance', ifnull(B.Amt, 0) As Opening ");
	        SQL.AppendLine("    From tblClosingBalanceIncashHdr A   ");
	        SQL.AppendLine("    Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    where A.yr = @year and A.Mth =  Cast(@MonthFilter As Decimal(10,2)) -1  ");
            SQL.AppendLine(")B On A.BankAcCode = B.BankAcCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X.BankAcCode, SUM(X.Credit) Credit, SUM(X.Debet) Debet  ");
	        SQL.AppendLine("    From  ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
		    SQL.AppendLine("        B.Description, B.Amt As Credit, 0 As Debet, 0 As Opening, A.PIC, A.CreateDt ");
		    SQL.AppendLine("        From TblVoucherHdr A ");
		    SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
		    SQL.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N' ");
		    SQL.AppendLine("        And A.BankAcCode Is Not Null ");
		    SQL.AppendLine("        And A.AcType='C'  ");
		    SQL.AppendLine("        Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
		    SQL.AppendLine("        B.Description, 0 As Credit, B.Amt As Debet, 0, A.PIC, A.CreateDt ");
		    SQL.AppendLine("        From TblVoucherHdr A ");
		    SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N' ");
		    SQL.AppendLine("        And A.BankAcCode Is Not Null ");
		    SQL.AppendLine("        And A.AcType='D'  ");
		    SQL.AppendLine("        Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
		    SQL.AppendLine("        B.Description, (B.Amt*A.ExcRate) As Credit, 0 As Debet, 0 As Opening, A.PIC, A.CreateDt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
		    SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N' ");
		    SQL.AppendLine("        And A.Actype2 Is Not Null ");
		    SQL.AppendLine("        And A.Actype2='C'  ");
		    SQL.AppendLine("        Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
		    SQL.AppendLine("        B.Description, 0 As Credit, (B.Amt*A.ExcRate) As Debet, 0, A.PIC, A.CreateDt ");
		    SQL.AppendLine("        From TblVoucherHdr A ");
		    SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 ");
		    SQL.AppendLine("        And A.CancelInd='N' ");
		    SQL.AppendLine("        And A.Actype2 Is Not Null ");
            SQL.AppendLine("        And A.Actype2='D'  ");
		    SQL.AppendLine("        Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth ");
	        SQL.AppendLine("    )X ");
            SQL.AppendLine("    where  X.Vcyear = @year and X.VcMonth = @MonthFilter ");
	        SQL.AppendLine("    group By X.BankAcCode ");
            SQL.AppendLine(")C On A.BankAcCode = C.BankAcCode  ");
            SQL.AppendLine("Order by A.bankActp, A.Sequence ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@Year", Sm.GetDte(DteDocDt1).Substring(0, 4));
                Sm.CmParam<String>(ref cm, "@MonthFilter", Sm.GetDte(DteDocDt1).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Dt", Sm.GetDte(DteDocDt1).Substring(6, 2));
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                //0
                "CompanyLogo",
                //1-5
                "CompanyName",
                "CompanyAddress",
                "CompanyPhone",
                "DocDt",
                "BankAcCode",
                //6-10
                "BankAcNm",
                "bankAcTp",
                "Year",
                "MonthFilter",
                "Dt",
                //11-15
                "Opening",
                "Debet",
                "Credit",
                "Balance",
                "bankAcno"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Nomor = Nomor + 1;
                        if (BankAcType.Length > 0)
                        {
                            bals = BankAcType == Sm.DrStr(dr, c[7]) ? bals : 0;
                        }
                        
                        l.Add(new CashBook1()
                        {
                            No = Nomor,
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            BankAccount = Sm.DrStr(dr, c[5]),
                            BankAcNm = Sm.DrStr(dr, c[6]),
                            bankAcTp = Sm.DrStr(dr, c[7]),
                            Year = Sm.DrStr(dr, c[8]),
                            MonthFilter = Sm.DrStr(dr, c[9]),
                            Dt1 = String.Format("{0:dddd, dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))),
                            Dt2 = String.Format("{0:dddd, dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2))),
                            OpeningBalance = Sm.DrDec(dr, c[11]),
                            Debet = Sm.DrDec(dr, c[12]),
                            Credit = Sm.DrDec(dr, c[13]),
                            Balance = Sm.DrDec(dr, c[14]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            CreateBy = Sm.GetValue("Select username from Tbluser Where userCode = '"+Sm.GetLue(LueKasir)+"'"),
                            MgrBy = Sm.GetValue("Select username from Tbluser Where userCode = '" + Sm.GetLue(LueMgrKeu) + "'"),
                            DirKeuBy = Sm.GetValue("Select username from Tbluser Where userCode = '" + Sm.GetLue(LueDirKeu) + "'"),
                            BankAcNo = Sm.DrStr(dr, c[15]),
                            Bal = bals + Sm.DrDec(dr, c[11]) + Sm.DrDec(dr, c[12]) - Sm.DrDec(dr, c[13]),
                        });
                        bals = bals+Sm.DrDec(dr, c[11]) + Sm.DrDec(dr, c[12]) - Sm.DrDec(dr, c[13]);
                        BankAcType = Sm.DrStr(dr, c[7]);
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Header CashBook2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select * From (  ");
            SQL2.AppendLine("Select T.BankAcCode, T3.bankName, T2.BankAcNm,  T2.BankAcNo, T.DocNo,  ");
            SQL2.AppendLine("T.DNo,  T.Description, T.GiroNo, T.VcYear, T.VcDate, T.VcMonth,  ");
            SQL2.AppendLine("Case T.VCMonth When '01' Then 'January'  ");
            SQL2.AppendLine("        When '02' Then 'February'  ");
            SQL2.AppendLine("        When '03' Then 'March'   ");
            SQL2.AppendLine("        When '04' Then 'April'  ");
            SQL2.AppendLine("        When '05' Then 'May'  ");
            SQL2.AppendLine("        When '06' Then 'June'  ");
            SQL2.AppendLine("        When '07' Then 'July'  ");
            SQL2.AppendLine("        When '08' Then 'August'  ");
            SQL2.AppendLine("        When '09' Then 'September'  ");
            SQL2.AppendLine("        When '10' Then 'October'  ");
            SQL2.AppendLine("        When '11' Then 'November'  ");
            SQL2.AppendLine("        When '12' Then 'December' End As Month,   ");
            SQL2.AppendLine("T.Opening, T.Debet, T.Credit,   ");
            SQL2.AppendLine("if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debet)-T.Credit), @bal:= @bal+(Debet-Credit)) As Balanced,  ");
            SQL2.AppendLine("@prev:=T.BankAcCode, T.Sequence  ");
            SQL2.AppendLine("From(   ");
            SQL2.AppendLine("      Select '2' As Sequence, A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear,  ");
            SQL2.AppendLine("      B.Description, B.Amt As Credit, 0 As Debet, 0 As Opening,  A.GiroNo   ");
            SQL2.AppendLine("      From TblVoucherHdr A   ");
            SQL2.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo  ");
            SQL2.AppendLine("      Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N'  ");
            SQL2.AppendLine("      And A.BankAcCode Is Not Null  ");
            SQL2.AppendLine("      And A.AcType='C'  ");
            SQL2.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth   ");
            SQL2.AppendLine("      Union All  ");
            SQL2.AppendLine("      Select '2', A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, "); 
            SQL2.AppendLine("      B.Description, 0 As Credit, B.Amt As Debet, 0,  A.GiroNo  ");
            SQL2.AppendLine("      From TblVoucherHdr A  ");
            SQL2.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo  ");
            SQL2.AppendLine("      Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N'  ");
            SQL2.AppendLine("      And A.BankAcCode Is Not Null  ");
            SQL2.AppendLine("      And A.AcType='D'  ");
            SQL2.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth   ");
            SQL2.AppendLine("      Union All  ");
            SQL2.AppendLine("      Select '2', A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear,  ");
            SQL2.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Credit, 0 As Debet, 0 As Opening,  A.GiroNo  ");
            SQL2.AppendLine("      From TblVoucherHdr A   ");
            SQL2.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo  ");
            SQL2.AppendLine("      Where A.DocDt between @DocDt1 And @DocDt2 And A.CancelInd='N'  ");
            SQL2.AppendLine("      And A.Actype2 Is Not Null  ");
            SQL2.AppendLine("      And A.Actype2='C'  ");
            SQL2.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth   ");
            SQL2.AppendLine("      Union All  ");
            SQL2.AppendLine("      Select '2', A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear,  ");
            SQL2.AppendLine("      B.Description, 0 As Credit, (B.Amt*A.ExcRate) As Debet, 0,  A.GiroNo  ");
            SQL2.AppendLine("      From TblVoucherHdr A  "); 
            SQL2.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo  ");
            SQL2.AppendLine("      Where A.DocDt between @DocDt1 And @DocDt2 ");
            SQL2.AppendLine("      And A.CancelInd='N'  ");
            SQL2.AppendLine("      And A.Actype2 Is Not Null  ");
            SQL2.AppendLine("      And A.Actype2='D'  ");
            SQL2.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth   ");
            SQL2.AppendLine("      Union All  ");
            SQL2.AppendLine("      Select '1', B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,   ");
            SQL2.AppendLine("      'Opening Balance', 0, 0, ifnull(B.Amt, 0) As Opening, '-'  ");
            SQL2.AppendLine("      From tblClosingBalanceIncashHdr A   ");
            SQL2.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo  ");
            SQL2.AppendLine("      where  ");
            SQL2.AppendLine("      A.yr = @year and A.Mth = Cast(@MonthFilter As Decimal(10,2)) -1  ");
            SQL2.AppendLine("      Order By BankAcCode, VcDate, VcMonth ");
            SQL2.AppendLine(") T  ");
            SQL2.AppendLine("Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode  ");
            SQL2.AppendLine("Inner Join TblBank T3 On T2.bankCode = T3.bankCode ");
            SQL2.AppendLine("Inner Join (  ");
            SQL2.AppendLine("       Select @bal := 0, @prev:=''  ");
            SQL2.AppendLine(") B On 0=0  ");
            SQL2.AppendLine(") Tbl  ");
            SQL2.AppendLine("Order by Tbl.BankAcCode, Tbl.VcMonth, Tbl.VcDate ");

             using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm2, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));
                Sm.CmParam<String>(ref cm2, "@Year", Sm.GetDte(DteDocDt1).Substring(0, 4));
                Sm.CmParam<String>(ref cm2, "@MonthFilter", Sm.GetDte(DteDocDt1).Substring(4, 2));
                Sm.CmParam<String>(ref cm2, "@Dt", Sm.GetDte(DteDocDt1).Substring(6, 2));
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());

                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                {
                //0
                "BankAcCode",
                //1-5
                "bankName",
                "BankAcNm",
                "BankAcNo",
                "DocNo",
                "DNo",
                //6-10
                "Description",
                "GiroNo",
                "VcYear",
                "VcDate",
                "VcMonth",
                //11-15
                "Opening", 
                "Debet", 
                "Credit",
                "balanced",
                "Sequence",
                });

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        No = No + 1;
                        if (BankAcCode.Length > 0)
                        {
                            bals2 = BankAcCode == Sm.DrStr(dr2, c2[0]) ? bals2 : 0;
                        }
                        l2.Add(new CashBook2()
                        {
                            No = No,
                            BankAcCode = Sm.DrStr(dr2, c2[0]),
                            BankName = Sm.DrStr(dr2, c2[1]),
                            BankAcNm = Sm.DrStr(dr2, c2[2]),
                            BankAcNo = Sm.DrStr(dr2, c2[3]),
                            DocNo = Sm.DrStr(dr2, c2[4]),
                            Dno = Sm.DrStr(dr2, c2[5]),
                            Description = Sm.DrStr(dr2, c2[6]),
                            GiroNo = Sm.DrStr(dr2, c2[7]),
                            VcYear = Sm.DrStr(dr2, c2[8]),
                            VcDate = Sm.DrStr(dr2, c2[9]),
                            VcMonth = Sm.DrStr(dr2, c2[10]),
                            Opening = Sm.DrDec(dr2, c2[11]),
                            Debet = Sm.DrDec(dr2, c2[12]),
                            Credit = Sm.DrDec(dr2, c2[13]),
                            Balance = Sm.DrDec(dr2, c2[14]),
                            Dt1 = String.Format("{0:dddd, dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))),
                            Dt2 = String.Format("{0:dddd, dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2))),
                            Sequence = Sm.DrDec(dr2, c2[15]),
                            Bal = bals2 + Sm.DrDec(dr2, c2[11]) + Sm.DrDec(dr2, c2[12]) - Sm.DrDec(dr2, c2[13]),
                        });

                        bals2 = bals2 + Sm.DrDec(dr2, c2[11]) + Sm.DrDec(dr2, c2[12]) - Sm.DrDec(dr2, c2[13]);
                        BankAcCode = Sm.DrStr(dr2, c2[0]);
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            Sm.PrintReport("CashBook", myLists, TableName, false);
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            //if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void LueKasir_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueKasir, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueMgrKeu_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMgrKeu, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueDirKeu_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDirKeu, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }
        #endregion
      
        #endregion

        #region Class

        private class CashBook1
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocDt { get; set; }
            public string BankAcCode { get; set; }
            public string BankAcNm { get; set; }
            public string bankAcTp { get; set; }
            public string Year { get; set; }
            public string MonthFilter { get; set; }
            public string Dt1 { get; set; }
            public string Dt2 { get; set; }
            public string BankAccount { get; set; }
            public decimal OpeningBalance { get; set; }
            public decimal Debet { get; set; }
            public decimal Credit { get; set; }
            public decimal Balance { get; set; }
            public string PrintBy { get; set; }
            public string CreateBy { get; set; }
            public string MgrBy { get; set; }
            public string DirKeuBy { get; set; }
            public string BankAcNo { get; set; }
            public decimal Bal { get; set; }
            public decimal No { get; set; }
        }


        private class CashBook2
        {
            public string BankAcCode { get; set; }
            public string BankName { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string DocNo { get; set; }
            public string Dno { get; set; }
            public string Description { get; set; }
            public string GiroNo { get; set; }
            public string VcYear { get; set; }
            public string VcDate { get; set; }
            public string VcMonth { get; set; }
            public decimal Opening { get; set; }
            public decimal Debet { get; set; }
            public decimal Credit { get; set; }
            public decimal Balance { get; set; }
            public string Dt1 { get; set; }
            public string Dt2 { get; set; }
            public decimal Sequence { get; set; }
            public decimal Bal { get; set; }
            public decimal No { get; set; }
        }

        #endregion

        

    }
}
