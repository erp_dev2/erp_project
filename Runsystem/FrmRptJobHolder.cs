﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptJobHolder : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptJobHolder(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            //Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLuePosCode(ref LuePosition);
            base.FrmLoad(sender, e);
        }


        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.*, (X.Qty1 - X.JobHolderQty) As Outstanding, ifnull(X1.pension, 0) As Pension From ( ");
            SQL.AppendLine("    Select A.DeptCode, C.DeptName, A.Poscode, B.Posname, Ifnull(C.JobHolderQty, 0) As JobHolderQty, Count(*) As Qty1  ");
            SQL.AppendLine("    from Tblemployee A  ");
            SQL.AppendLine("    inner join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("    Inner Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select A.PosCode, A.JobHolderQty ");
	        SQL.AppendLine("        From TblOrganizationalStructure A ");
            SQL.AppendLine("    )C On A.posCode = C.PosCode ");
            SQL.AppendLine("    Where A.PosCode is not null And (A.ResignDt Is Not Null And A.ResignDt>=@DocDt ) Or A.ResignDt Is Null ");
            SQL.AppendLine("    Group by A.DeptCode, C.DeptName, A.Poscode, B.Posname   ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    Select deptCode, PosCode, Count(*) As Pension  ");
	        SQL.AppendLine("    From (  ");
		    SQL.AppendLine("        Select DeptCode, PosCode, EmpCode, BirthDt, concat(replace(curdate(), '-', '')) DtNow,   ");
		    SQL.AppendLine("        Replace(date_add(Birthdt, interval 12 month), '-', '') As PensiunDt,  ");
		    SQL.AppendLine("        Left(concat(replace(curdate(), '-', '')), 4) - Left(Replace(date_add(Birthdt, interval 12 month), '-', ''), 4) As Age  ");
		    SQL.AppendLine("        From TblEmployee  ");
		    SQL.AppendLine("        Where BirthDt is not null  ");
		    SQL.AppendLine("        And ((ResignDt Is Not Null And ResignDt>=@DocDt ) Or ResignDt Is Null)  ");
	        SQL.AppendLine("    )X Where X.Age >= @parcode  ");
            SQL.AppendLine("    Group BY deptCode, PosCode  ");
            SQL.AppendLine(")X1 On X.DeptCode = X1.DeptCode And X.PosCode = X1.PosCode  ");

                        mSQL = SQL.ToString();
                    }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5 
                        "Department Code",
                        "Department",
                        "Position Code",  
                        "Position", 
                        "Employee",
                        //6-8
                        "Job Holder",   
                        "Balanced",
                        "Pension"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 200, 100, 250, 100, 

                        //6-8
                        100, 100,100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                string CurrentDateTime = Sm.ServerCurrentDateTime().Substring(0, 8);


                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosition), "X.PosCode", true);
                Sm.CmParam<String>(ref cm, "@DocDt", CurrentDateTime);
                Sm.CmParam<String>(ref cm, "@ParCode", Sm.GetParameter("SSRetiredMaxAge"));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X.PosName",
                        new string[]
                        {
                            //0
                            "DeptCode",

                            //1-5
                            "DeptName", "PosCode", "PosName", "Qty1", "JobHolderQty", 
                            //6
                            "Outstanding", "Pension"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

      

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkPosition_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }

        private void LuePosition_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

    }
}
