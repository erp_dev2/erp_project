﻿#region Update
/*
    18/07/2019 [WED] master baru keperluan IMM
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMProduct : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmIMMProductFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMProduct(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueIMMProdCtCode(ref LueProdCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCode, MeeProdDesc, TxtColor, LueProdCtCode
                    }, true);
                    TxtProdCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCode, MeeProdDesc, TxtColor, LueProdCtCode
                    }, false);
                    TxtProdCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtProdCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeProdDesc, TxtColor, LueProdCtCode
                    }, false);
                    MeeProdDesc.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProdCode, MeeProdDesc, TxtColor, LueProdCtCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMProductFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProdCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblIMMProduct(ProdCode, ProdDesc, Color, ProdCtCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ProdCode, @ProdDesc, @Color, @ProdCtCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ProdDesc=@ProdDesc, Color = @Color, ProdCtCode=@ProdCtCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ProdCode", TxtProdCode.Text);
                Sm.CmParam<String>(ref cm, "@ProdDesc", MeeProdDesc.Text);
                Sm.CmParam<String>(ref cm, "@Color", TxtColor.Text);
                Sm.CmParam<String>(ref cm, "@ProdCtCode", Sm.GetLue(LueProdCtCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtProdCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProdCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ProdCode", ProdCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select ProdCode, ProdDesc, Color, ProdCtCode From TblIMMProduct Where ProdCode=@ProdCode",
                    new string[] 
                    {
                        "ProdCode", "ProdDesc", "Color", "ProdCtCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtProdCode.EditValue = Sm.DrStr(dr, c[0]);
                        MeeProdDesc.EditValue = Sm.DrStr(dr, c[1]);
                        TxtColor.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueProdCtCode, Sm.DrStr(dr, c[3]));
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProdCode, "Product code", false) ||
                Sm.IsMeeEmpty(MeeProdDesc, "Product description") ||
                IsProdCodeExisted();
        }

        private bool IsProdCodeExisted()
        {
            if (!TxtProdCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select ProdCode From TblIMMProduct Where ProdCode=@Param Limit 1;", TxtProdCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Product code ( " + TxtProdCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProdCode);
        }

        private void TxtColor_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtColor);
        }

        private void LueProdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProdCtCode, new Sm.RefreshLue1(Sl.SetLueIMMProdCtCode));
        }

        #endregion

        #endregion
    }
}
