﻿#region Update
/*
    04/09/2019 [WED] new apps for PEB
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data.OleDb;
using System.Text.RegularExpressions;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt9Dlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt9 mFrmParent;

        #endregion

        #region Constructor

        public FrmDOCt9Dlg6(FrmDOCt9 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -480);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Registration#",
                    "Registration"+Environment.NewLine+"Date",
                    "Customs"+Environment.NewLine+"Doc#",
                    "Submission#",
                    "Packing List#",
                    
                    //6-10
                    "Packing List Date",
                    "Contract#",
                    "Contract Date",
                    "Used",
                    "Used For"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 100, 80, 200, 150, 
                    
                    //6-10
                    120, 150, 120, 80, 200, 
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10 });
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            var l = new List<KB_Data>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<KB_Data> l)
        {
            var SQL = new StringBuilder();
            var Filter = " ";
            string mDocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string mDocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);

            SQL.AppendLine("Select A.NoDaft As KBRegistrationNo, Format(A.TgDaft, 'yyyymmdd') As KBRegistrationDt, ");
            SQL.AppendLine("NULL As KBContractNo, NULL As KBContractDt, ");
            SQL.AppendLine("NULL As KBPLNo, NULL As KBPLDt, ");
            SQL.AppendLine("A.CAR As KBSubmissionNo, @PEBDocType As CustomsDocCode ");
            SQL.AppendLine("From TblPEBHdr A ");
            SQL.AppendLine("Where Len(A.NoDaft) > 0 ");
            SQL.AppendLine("And (Format(A.TgDaft, 'yyyymmdd') Between " + mDocDt1 + " And " + mDocDt2 + ") ");

            using (var cn = new OleDbConnection(ConnectionString))
            {
                var cm = new OleDbCommand();
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                if (TxtDocNo.Text.Length > 0) Filter += " And A.NoDaft Like '%" + TxtDocNo.Text + "%' ";

                CmParam<String>(ref cm, "@PEBDocType", mFrmParent.mPEBDocType);
                cm.CommandText = SQL.ToString() + Filter + "Order By A.TgDaft, A.NoDaft; ";
                var dr = cm.ExecuteReader();
                var c = GetOrdinal(dr, new string[] { 
                    //0
                    "KBRegistrationNo", 
                    
                    //1-5
                    "KBRegistrationDt", 
                    "KBContractNo", 
                    "KBContractDt", 
                    "KBPLNo", 
                    "KBPLDt",

                    //6-7
                    "KBSubmissionNo",
                    "CustomsDocCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_Data()
                        {
                            KBRegistrationNo = DrStr(dr, c[0]),
                            KBRegistrationDt = DrStr(dr, c[1]),
                            KBContractNo = DrStr(dr, c[2]),
                            KBContractDt = DrStr(dr, c[3]),
                            KBPLNo = DrStr(dr, c[4]),
                            KBPLDt = DrStr(dr, c[5]),
                            DocNo = string.Empty,
                            UsedInd = false,
                            KBSubmissionNo = DrStr(dr, c[6]),
                            CustomsDocCode = DrStr(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var l2 = new List<KB_Data2>();

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.KBRegistrationNo=@KBRegistrationNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@KBRegistrationNo0" + i.ToString(), l[i].KBRegistrationNo);
            }

            if (Filter.Length != 0) Filter = " And (" + Filter + ") ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select T.KBRegistrationNo, ");
            SQL.AppendLine("Group_Concat(Distinct T.DocNo Order By T.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.KBRegistrationNo, ");
            SQL.AppendLine("    Group_Concat(Distinct A.DocNo Order By A.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.KBRegistrationDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select A.KBRegistrationNo, ");
            SQL.AppendLine("    Group_Concat(Distinct A.DocNo Order By A.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("    From TblDOCtHdr A ");
            SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.KBRegistrationDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            
            
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.KBRegistrationNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "KBRegistrationNo", "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new KB_Data2()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                foreach (var i in l)
                {
                    foreach (var j in l2.Where(w => Sm.CompareStr(w.KBRegistrationNo, i.KBRegistrationNo)))
                    {
                        if (j.DocNo.Length > 0)
                        {
                            i.DocNo = j.DocNo;
                            i.UsedInd = true;
                        }
                        break;
                    }
                }
                l2.Clear();
            }
        }

        private void Process3(ref List<KB_Data> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].KBRegistrationNo;
                if (l[i].KBRegistrationDt.Length > 0) r.Cells[2].Value = Sm.ConvertDate(l[i].KBRegistrationDt);
                r.Cells[3].Value = l[i].CustomsDocCode;
                r.Cells[4].Value = l[i].KBSubmissionNo;
                r.Cells[5].Value = l[i].KBPLNo;
                if (l[i].KBPLDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(l[i].KBPLDt);
                r.Cells[7].Value = l[i].KBContractNo;
                if (l[i].KBContractDt.Length > 0) r.Cells[8].Value = Sm.ConvertDate(l[i].KBContractDt);
                r.Cells[9].Value = l[i].UsedInd;
                r.Cells[10].Value = l[i].DocNo;
            }
            Grd1.EndUpdate();
        }

        #region OleDB Connection

        private string DrStr(OleDbDataReader dr, int c)
        {
            if (dr[c] == DBNull.Value)
                return string.Empty;
            else
                return dr.GetString(c);
        }

        private void CmParam<T>(ref OleDbCommand cm, string ColumnName, T Value)
        {
            if (string.IsNullOrEmpty(Value.ToString()))
                cm.Parameters.AddWithValue(ColumnName, DBNull.Value);
            else
                cm.Parameters.AddWithValue(ColumnName, Value);
        }

        private void CmParamDt(ref OleDbCommand cm, string Param, string Value)
        {
            if (Value.Length == 0)
                CmParam<String>(ref cm, Param, "");
            else
                CmParam<String>(ref cm, Param, Value.Substring(0, 8));
        }

        private void FilterStr(ref string SQL, ref OleDbCommand cm, string Filter, string Column, bool Fixed)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Index.ToString();
                        CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Index.ToString(),
                            Fixed ? s : "%" + s + "%");
                        //CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
                //SQL = SetFilterString(SQL, Column, Column2);
                //CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2), Fixed ? Filter : "%" + Filter + "%");
            }
        }

        private int[] GetOrdinal(OleDbDataReader dr, string[] s)
        {
            var c = new int[s.GetLength(0)];
            var i = 0;
            foreach (string Index in s)
                c[i++] = dr.GetOrdinal(Index);
            return c;
        }

        private string ConnectionString
        {
            get
            {
                return
                    string.Concat(
                    //@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=", mFrmParent.mPEBFilePath, ";Jet OLEDB:Database Password=", mFrmParent.mPEBPassword, ";"
                    @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=", mFrmParent.mPEBFilePath, ";Jet OLEDB:Database Password=", mFrmParent.mPEBPassword, ";"
                    );
            }
        }

        #endregion

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtKBRegistrationNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                if (Sm.GetGrdDate(Grd1, Row, 2).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBRegistrationDt, Sm.GetGrdDate(Grd1, Row, 2).Substring(0, 8));
                mFrmParent.TxtKBPLNo.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                if (Sm.GetGrdDate(Grd1, Row, 6).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBPLDt, Sm.GetGrdDate(Grd1, Row, 6).Substring(0, 8));
                mFrmParent.TxtKBContractNo.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                if (Sm.GetGrdDate(Grd1, Row, 8).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBContractDt, Sm.GetGrdDate(Grd1, Row, 8).Substring(0, 8));

                mFrmParent.TxtKBSubmissionNo.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.mCustomsDocCode = Sm.GetGrdStr(Grd1, Row, 3);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Registration#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

        #region Class

        private class KB_Data
        {
            public string KBRegistrationNo { get; set; }
            public string KBRegistrationDt { get; set; }
            public string KBContractNo { get; set; }
            public string KBContractDt { get; set; }
            public string KBPLNo { get; set; }
            public string KBPLDt { get; set; }
            public string DocNo { get; set; }
            public bool UsedInd { get; set; }
            public string KBSubmissionNo { get; set; }
            public string CustomsDocCode { get; set; }
        }

        private class KB_Data2
        {
            public string KBRegistrationNo { get; set; }
            public string DocNo { get; set; }
        }

        #endregion

    }
}
