﻿#region Update

/*
   27/12/2021 [YOG/PHT] membuat dialog baru untuk melihat jurnal - jurnal Voucher Payroll yang terbentuk
   20/02/2023 [RDA/MNET] show list of journal untuk voucher yg terbentuk dari vr for external payroll
 */

#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucher mFrmParent;
        private string mSQL = string.Empty, mMInd = "N"; 

        #endregion

        #region Constructor

        public FrmVoucherDlg4(FrmVoucher FrmParent, string MInd) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method 

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = BtnExcel.Visible = BtnPrint.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mFrmParent.mDocType == "57")
            {
                SQL.AppendLine("SELECT * FROM ( ");
                SQL.AppendLine("Select ");
                if (mFrmParent.mJournalType == "1")
                    SQL.AppendLine("A.JournalDocNo As Journal ");
                if (mFrmParent.mJournalType == "2")
                    SQL.AppendLine("A.JournalDocNo2 As Journal ");
                SQL.AppendLine("From tblvoucherrequestexternalpayrolljournal A Where A.DocNo = @DocNo ");
                SQL.AppendLine(") T ");
            }
            else
            {
                SQL.AppendLine("SELECT * FROM ( ");
                SQL.AppendLine("Select ");
                if (mFrmParent.mJournalType == "1")
                    SQL.AppendLine("A.JournalDocNo As Journal ");
                if (mFrmParent.mJournalType == "2")
                    SQL.AppendLine("A.JournalDocNo2 As Journal ");
                SQL.AppendLine("From TblVoucherPayrollJournal A ");
                SQL.AppendLine("INNER JOIN TblVoucherHdr B On B.DocNo = A.VPDocNo AND  A.VPDocNo = @DocNo ");
                SQL.AppendLine(") T ");
            }
            

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-2
                        "Document#",
                        ""
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Filter = "where (0=0) ";
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Journal", false);
                Sm.CmParam<String>(ref cm, "@DocNo", mFrmParent.TxtDocNo.Text);

                Sm.ShowDataInGrid(
                       ref Grd1, ref cm,
                       mSQL + Filter + " Order By Journal Asc;",
                       new string[]
                       { 
                            "Journal"
                       },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        }, true, false, false, true
                    );
                Grd1.Cols.AutoWidth();
                Grd1.Rows.AutoHeight();
            }

            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion

        #region Grid Method
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmJournal(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion
    }
}
