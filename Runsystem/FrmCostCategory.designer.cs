﻿namespace RunSystem
{
    partial class FrmCostCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCostCategory));
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCCtName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtCCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCC = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCCGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueCostOfGroupCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkJoinCostInd = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueCCtProductCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkRevenueInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostOfGroupCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJoinCostInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCtProductCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRevenueInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkRevenueInd);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueCCtProductCode);
            this.panel2.Controls.Add(this.ChkJoinCostInd);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueCostOfGroupCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCCGrpCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueCC);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtCCtName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtCCtCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(168, 94);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 30;
            this.TxtAcNo.Size = new System.Drawing.Size(261, 20);
            this.TxtAcNo.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(65, 99);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "COA\'s Account#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCCtName
            // 
            this.TxtCCtName.EnterMoveNextControl = true;
            this.TxtCCtName.Location = new System.Drawing.Point(168, 50);
            this.TxtCCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCCtName.Name = "TxtCCtName";
            this.TxtCCtName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCCtName.Properties.MaxLength = 1000;
            this.TxtCCtName.Size = new System.Drawing.Size(466, 20);
            this.TxtCCtName.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(44, 53);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Cost Category Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCCtCode
            // 
            this.TxtCCtCode.EnterMoveNextControl = true;
            this.TxtCCtCode.Location = new System.Drawing.Point(168, 28);
            this.TxtCCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCCtCode.Name = "TxtCCtCode";
            this.TxtCCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCtCode.Properties.MaxLength = 30;
            this.TxtCCtCode.Size = new System.Drawing.Size(261, 20);
            this.TxtCCtCode.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(47, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "Cost Category Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(435, 93);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 25;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(168, 115);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Size = new System.Drawing.Size(466, 20);
            this.TxtAcDesc.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(10, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 14);
            this.label2.TabIndex = 26;
            this.label2.Text = "COA\'s Account Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(91, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Cost Center";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCC
            // 
            this.LueCC.EnterMoveNextControl = true;
            this.LueCC.Location = new System.Drawing.Point(168, 72);
            this.LueCC.Margin = new System.Windows.Forms.Padding(5);
            this.LueCC.Name = "LueCC";
            this.LueCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.Appearance.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCC.Properties.DropDownRows = 30;
            this.LueCC.Properties.MaxLength = 40;
            this.LueCC.Properties.NullText = "[Empty]";
            this.LueCC.Properties.PopupWidth = 300;
            this.LueCC.Size = new System.Drawing.Size(261, 20);
            this.LueCC.TabIndex = 22;
            this.LueCC.ToolTip = "F4 : Show/hide list";
            this.LueCC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCC.EditValueChanged += new System.EventHandler(this.LueCC_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(123, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 14);
            this.label4.TabIndex = 28;
            this.label4.Text = "Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCGrpCode
            // 
            this.LueCCGrpCode.EnterMoveNextControl = true;
            this.LueCCGrpCode.Location = new System.Drawing.Point(168, 137);
            this.LueCCGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCGrpCode.Name = "LueCCGrpCode";
            this.LueCCGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCGrpCode.Properties.DropDownRows = 30;
            this.LueCCGrpCode.Properties.MaxLength = 200;
            this.LueCCGrpCode.Properties.NullText = "[Empty]";
            this.LueCCGrpCode.Properties.PopupWidth = 500;
            this.LueCCGrpCode.Size = new System.Drawing.Size(466, 20);
            this.LueCCGrpCode.TabIndex = 29;
            this.LueCCGrpCode.ToolTip = "F4 : Show/hide list (Option : CostCenterGroup)";
            this.LueCCGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCGrpCode.EditValueChanged += new System.EventHandler(this.LueCCGrpCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(80, 162);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Cost of Group";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostOfGroupCode
            // 
            this.LueCostOfGroupCode.EnterMoveNextControl = true;
            this.LueCostOfGroupCode.Location = new System.Drawing.Point(169, 159);
            this.LueCostOfGroupCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostOfGroupCode.Name = "LueCostOfGroupCode";
            this.LueCostOfGroupCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostOfGroupCode.Properties.Appearance.Options.UseFont = true;
            this.LueCostOfGroupCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostOfGroupCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostOfGroupCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostOfGroupCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostOfGroupCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostOfGroupCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostOfGroupCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostOfGroupCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostOfGroupCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostOfGroupCode.Properties.DropDownRows = 30;
            this.LueCostOfGroupCode.Properties.MaxLength = 200;
            this.LueCostOfGroupCode.Properties.NullText = "[Empty]";
            this.LueCostOfGroupCode.Properties.PopupWidth = 500;
            this.LueCostOfGroupCode.Size = new System.Drawing.Size(466, 20);
            this.LueCostOfGroupCode.TabIndex = 31;
            this.LueCostOfGroupCode.ToolTip = "F4 : Show/hide list (Option : CostOfGroup)";
            this.LueCostOfGroupCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostOfGroupCode.EditValueChanged += new System.EventHandler(this.LueCostOfGroupCode_EditValueChanged);
            // 
            // ChkJoinCostInd
            // 
            this.ChkJoinCostInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkJoinCostInd.Location = new System.Drawing.Point(435, 26);
            this.ChkJoinCostInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkJoinCostInd.Name = "ChkJoinCostInd";
            this.ChkJoinCostInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkJoinCostInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkJoinCostInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkJoinCostInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkJoinCostInd.Properties.Appearance.Options.UseFont = true;
            this.ChkJoinCostInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkJoinCostInd.Properties.Caption = "Join Cost";
            this.ChkJoinCostInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkJoinCostInd.Size = new System.Drawing.Size(83, 22);
            this.ChkJoinCostInd.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(113, 184);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 32;
            this.label8.Tag = "";
            this.label8.Text = "Product";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCtProductCode
            // 
            this.LueCCtProductCode.EnterMoveNextControl = true;
            this.LueCCtProductCode.Location = new System.Drawing.Point(169, 181);
            this.LueCCtProductCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCtProductCode.Name = "LueCCtProductCode";
            this.LueCCtProductCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCtProductCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCtProductCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCtProductCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCtProductCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCtProductCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCtProductCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCtProductCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCtProductCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCtProductCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCtProductCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCtProductCode.Properties.DropDownRows = 30;
            this.LueCCtProductCode.Properties.MaxLength = 200;
            this.LueCCtProductCode.Properties.NullText = "[Empty]";
            this.LueCCtProductCode.Properties.PopupWidth = 500;
            this.LueCCtProductCode.Size = new System.Drawing.Size(466, 20);
            this.LueCCtProductCode.TabIndex = 33;
            this.LueCCtProductCode.ToolTip = "F4 : Show/hide list (Option : CCtProduct)";
            this.LueCCtProductCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCtProductCode.EditValueChanged += new System.EventHandler(this.LueCCtProductCode_EditValueChanged);
            // 
            // ChkRevenueInd
            // 
            this.ChkRevenueInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRevenueInd.Location = new System.Drawing.Point(525, 26);
            this.ChkRevenueInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRevenueInd.Name = "ChkRevenueInd";
            this.ChkRevenueInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRevenueInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRevenueInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRevenueInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRevenueInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRevenueInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRevenueInd.Properties.Caption = "Revenue";
            this.ChkRevenueInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRevenueInd.Size = new System.Drawing.Size(83, 22);
            this.ChkRevenueInd.TabIndex = 34;
            // 
            // FrmCostCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmCostCategory";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostOfGroupCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJoinCostInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCtProductCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRevenueInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtCCtName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtCCtCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueCC;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueCCGrpCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.LookUpEdit LueCostOfGroupCode;
        private DevExpress.XtraEditors.CheckEdit ChkJoinCostInd;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueCCtProductCode;
        private DevExpress.XtraEditors.CheckEdit ChkRevenueInd;
    }
}