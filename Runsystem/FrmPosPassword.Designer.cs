﻿namespace RunSystem
{
    partial class FrmPosPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlPwd = new System.Windows.Forms.Panel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOK = new System.Windows.Forms.Button();
            this.TxtPwd = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.PnlPwd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPwd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlPwd
            // 
            this.PnlPwd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPwd.Controls.Add(this.BtnCancel);
            this.PnlPwd.Controls.Add(this.BtnOK);
            this.PnlPwd.Controls.Add(this.TxtPwd);
            this.PnlPwd.Controls.Add(this.label1);
            this.PnlPwd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlPwd.Location = new System.Drawing.Point(0, 0);
            this.PnlPwd.Name = "PnlPwd";
            this.PnlPwd.Size = new System.Drawing.Size(286, 110);
            this.PnlPwd.TabIndex = 0;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(148, 70);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(106, 31);
            this.BtnCancel.TabIndex = 11;
            this.BtnCancel.TabStop = false;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOK.Location = new System.Drawing.Point(30, 70);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(106, 31);
            this.BtnOK.TabIndex = 10;
            this.BtnOK.TabStop = false;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // TxtPwd
            // 
            this.TxtPwd.EditValue = "";
            this.TxtPwd.EnterMoveNextControl = true;
            this.TxtPwd.Location = new System.Drawing.Point(114, 29);
            this.TxtPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.Properties.Appearance.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Properties.Appearance.Options.UseFont = true;
            this.TxtPwd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.TxtPwd.Properties.MaxLength = 20;
            this.TxtPwd.Properties.PasswordChar = '*';
            this.TxtPwd.Size = new System.Drawing.Size(148, 30);
            this.TxtPwd.TabIndex = 9;
            this.TxtPwd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtPwd_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Password";
            // 
            // FrmPosPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 110);
            this.Controls.Add(this.PnlPwd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPosPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPosPassword";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmPosPassword_Load);
            this.PnlPwd.ResumeLayout(false);
            this.PnlPwd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPwd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlPwd;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtPwd;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOK;
    }
}