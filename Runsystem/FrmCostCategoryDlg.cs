﻿#region Update
/*
    12/02/2018 [TKG] account# berdasarkan otorisasi group thd site/entity
    07/06/2018 [HAR] tambah validasi account yng sdh dipake tidak mncul berdasarkan parameter
    19/01/2020 [TKG/IMS] tambah alias nomor rekening coa
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostCategoryDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCostCategory mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCostCategoryDlg(FrmCostCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Account#", 
                        "Description",
                        "Type",
                        "Entity",
                        "Alias"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Grd1.Cols[5].Visible = mFrmParent.mIsCOAUseAlias;
            Grd1.Cols[5].Move(2);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, "); 
            SQL.AppendLine("Case A.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, ");
            SQL.AppendLine("B.EntName, A.Alias "); 
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblEntity B On A.EntCode=B.EntCode ");
            SQL.AppendLine("Where A.AcNo Not In (Select parent From TblCoa Where Parent Is Not Null ) ");
            SQL.AppendLine("And A.Parent Is Not Null ");
            SQL.AppendLine("And A.ActInd='Y' ");
            if (mFrmParent.mIsCostCategoryFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode is Not Null ");
                SQL.AppendLine("And A.EntCode In ( ");
                SQL.AppendLine("    Select T1.EntCode From TblProfitCenter T1, TblSite T2 ");
                SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode ");
                SQL.AppendLine("    And T1.EntCode Is Not Null ");
                SQL.AppendLine("    And T2.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And T2.SiteCode In (");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsOneAccountForOneCostCategory)
            {
                SQL.AppendLine("And A.Acno Not In (Select AcNo From TblCostCategory ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc", "Alias" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo;",
                        new string[]{ "AcNo", "AcDesc", "AcType", "EntName", "Alias" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                if (mFrmParent.mIsCOAUseAlias && Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5).Length > 0)
                    mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2) + " [" + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5) + "]";
                else
                    mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                this.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #region Grid Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
