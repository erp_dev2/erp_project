﻿#region Update
/*
    06/07/2018 [TKG] New Application
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHADocType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmRHADocTypeFind FrmFind;

        #endregion

        #region Constructor

        public FrmRHADocType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocTypeCode, TxtDocTypeName, TxtMIndex }, true);
                    TxtDocTypeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocTypeCode, TxtDocTypeName, TxtMIndex }, false);
                    TxtDocTypeCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocTypeName, TxtMIndex }, false);
                    TxtDocTypeName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtDocTypeCode, TxtDocTypeName });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtMIndex }, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRHADocTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocTypeCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocTypeCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblRHADocType Where DocTypeCode=@DocTypeCode" };
                Sm.CmParam<String>(ref cm, "@DocTypeCode", TxtDocTypeCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblRHADocType(DocTypeCode, DocTypeName, MIndex, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocTypeCode, @DocTypeName, @MIndex, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update DocTypeName=@DocTypeName, MIndex=@MIndex, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocTypeCode", TxtDocTypeCode.Text);
                Sm.CmParam<String>(ref cm, "@DocTypeName", TxtDocTypeName.Text);
                Sm.CmParam<Decimal>(ref cm, "@MIndex", decimal.Parse(TxtMIndex.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtDocTypeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocTypeCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocTypeCode", DocTypeCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DocTypeCode, DocTypeName, MIndex From TblRHADocType Where DocTypeCode=@DocTypeCode",
                        new string[]{ "DocTypeCode", "DocTypeName", "MIndex" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocTypeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtDocTypeName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtMIndex.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 2);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocTypeCode, "Document type code", false) ||
                Sm.IsTxtEmpty(TxtDocTypeName, "Document type name", false) ||
                Sm.IsTxtEmpty(TxtMIndex, "Multiplier index", false) ||
                IsDocTypeCodeExisted();
        }

        private bool IsDocTypeCodeExisted()
        {
            if (!TxtDocTypeCode.Properties.ReadOnly && Sm.IsDataExist("Select DocTypeCode From TblRHADocType Where DocTypeCode='" + TxtDocTypeCode.Text + "' Limit 1;"))
            {
                Sm.StdMsg(mMsgType.Warning, "Document type code ( " + TxtDocTypeCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocTypeCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocTypeCode);
        }

        private void TxtDocTypeName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocTypeName);
        }

        private void TxtMIndex_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMIndex, 2);
        }

        #endregion

        #endregion
    }
}
