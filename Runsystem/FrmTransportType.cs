﻿#region Update
/*
   16/04/2019 [HAR] tanbah informasi tonase
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransportType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmTransportTypeFind FrmFind;

        #endregion

        #region Constructor

        public FrmTransportType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTTCode, TxtTTName, TxtLoadingCost, TxtUnloadingCost, TxtTonase,
                    }, true);
                    TxtTTCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTTCode, TxtTTName, TxtLoadingCost, TxtUnloadingCost, TxtTonase
                    }, false);
                    TxtTTCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTTName, TxtLoadingCost, TxtUnloadingCost }, false);
                    TxtTTName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTTCode, TxtTTName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
              TxtUnloadingCost, TxtLoadingCost, TxtTonase
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTransportTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTTCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTTCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblTransportType Where TTCode=@TTCode" };
                Sm.CmParam<String>(ref cm, "@TTCode", TxtTTCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblTransportType(TTCode, TTName, LoadingCost, UnloadingCost, Tonase, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@TTCode, @TTName, @LoadingCost, @UnloadingCost, @Tonase, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update TTName=@TTName, LoadingCost=@LoadingCost, UnloadingCost = @UnloadingCost, Tonase=@Tonase, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@TTCode", TxtTTCode.Text);
                Sm.CmParam<String>(ref cm, "@TTName", TxtTTName.Text);
                Sm.CmParam<Decimal>(ref cm, "@LoadingCost", Decimal.Parse(TxtLoadingCost.Text));
                Sm.CmParam<Decimal>(ref cm, "@UnloadingCost", Decimal.Parse(TxtUnloadingCost.Text));
                Sm.CmParam<Decimal>(ref cm, "@Tonase", Decimal.Parse(TxtUnloadingCost.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtTTCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TTCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@TTCode", TTCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select TTCode, TTName, LoadingCost, UnloadingCost, Tonase From TblTransportType Where TTCode=@TTCode",
                        new string[] 
                        {
                            //0
                            "TTCode",
                            //1-4
                            "TTName", "LoadingCost", "UnloadingCost", "Tonase"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtTTCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtTTName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtLoadingCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            TxtUnloadingCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                            TxtTonase.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTTCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtTTName, "Name", false) ||
                IsTTCodeExisted();
        }

        private bool IsTTCodeExisted()
        {
            if (!TxtTTCode.Properties.ReadOnly && Sm.IsDataExist("Select TTCode From TblTransportType Where TTCode='" + TxtTTCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Code ( " + TxtTTCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtTTCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTTCode);
        }

        private void TxtTTName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTTName);
        }

        private void TxtLoadingCost_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLoadingCost, 0);          
        }

        private void TxtUnloadingCost_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUnloadingCost, 0);
        }

        private void TxtTonase_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTonase, 0);
        }


        #endregion

        
       

        #endregion

        
    }
}
