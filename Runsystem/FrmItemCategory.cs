﻿#region Update
/*
    04/04/2017 [TKG] tambah indikator wasted.
    14/11/2017 [TKG] tambah nomor rekening coa untuk sales, COGS, Sales Return, Purchase Return
    19/01/2018 [HAR] tambah parameter mandatory coa untuk sales, COGM, Sales Return, Purchase Return
    23/01/2018 [HAR] tambah parameter mandatory coa untuk COGS
    05/02/2018 [TKG] ubah mandatory coa purchase return
    12/06/2018 [TKG] menggunakan parameter untuk menentukan informasi nomor rekening masing-masing coa harus diinput atau tidak.
    27/10/2019 [TKG] tambah indikator moving average 
    19/01/2020 [TKG/IMS] tambah alias nomor rekening coa
    27/01/2021 [ICA/PHT] Tambah nomor coa untuk AP dan AR berdasarakan param IsItemCategoryUseCOAAPAR
    02/02/2021 [WED/PHT] Tambah nomor COA untuk AP invoiced. AP yg sekarang dirubah ke AP Uninvoiced, dan AcNo9 yg sudah ada digeser ke AcNo10
    03/02/2021 [VIN/IMS] Bug --> query kurang koma
    19/08/2021 [RDA/ALL] penambahan field coa ar uninvoice pada menu item category
    08/02/2022 [IBL/PHT] Tambah nomor coa untuk Stock Beginning berdasarakan param IsItemCategoryUseCOAStockBeginning
    10/02/2022 [IBL/PHT] BUG: Muncul warning saat save data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemCategoryFind FrmFind;
        private bool
            mIsItCtAcNoMandatory = false,
            mIsItCtAcNo2Mandatory = false,
            mIsItCtAcNo3Mandatory = false,
            mIsItCtAcNo4Mandatory = false,
            mIsItCtAcNo5Mandatory = false,
            mIsItCtAcNo6Mandatory = false,
            mIsItCtAcNo7Mandatory = false;
        internal string 
            mAcNoForStoreSales = string.Empty,
            mAcNoForSalesReturnInvoice = string.Empty,
            mAcNoForCOGS = string.Empty;
        internal bool 
            mIsMovingAvgEnabled = false,
            mIsCOAUseAlias = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsItemCategoryUseCOAStockBeginning = false;
        #endregion

        #region Constructor

        public FrmItemCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                if (!mIsMovingAvgEnabled) ChkMovingAvgInd.Visible = false;
                if (mIsItCtAcNoMandatory) LblCOAStock.ForeColor = Color.Red;
                if (mIsItCtAcNo2Mandatory) LblCOACost.ForeColor = Color.Red;
                if (mIsItCtAcNo3Mandatory) LblCOACOGM.ForeColor = Color.Red;
                if (mIsItCtAcNo4Mandatory) LblCOASales.ForeColor = Color.Red;
                if (mIsItCtAcNo5Mandatory) LblCOACOGS.ForeColor = Color.Red;
                if (mIsItCtAcNo6Mandatory) LblCOASalesReturn.ForeColor = Color.Red;
                if (mIsItCtAcNo7Mandatory) LblCOAPurchaseReturn.ForeColor = Color.Red;
                LblCOAAPUninvoiced.Visible = TxtAcNo8.Visible = TxtAcDesc8.Visible = BtnAcNo8.Visible = mIsItemCategoryUseCOAAPAR;
                LblCOAAPInvoiced.Visible = TxtAcNo9.Visible = TxtAcDesc9.Visible = BtnAcNo9.Visible = mIsItemCategoryUseCOAAPAR;
                LblCOAAR.Visible = TxtAcNo10.Visible = TxtAcDesc10.Visible = BtnAcNo10.Visible = mIsItemCategoryUseCOAAPAR;
                LblCOAARUninvoiced.Visible = TxtAcNo11.Visible = TxtAcDesc11.Visible = BtnAcNo11.Visible = mIsItemCategoryUseCOAAPAR;
                LblCOAStockBeginning.Visible = TxtAcNo12.Visible = TxtAcDesc12.Visible = BtnAcNo12.Visible = mIsItemCategoryUseCOAStockBeginning;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtItCtCode, TxtItCtName }, true);
                    
                    ChkActiveInd.Properties.ReadOnly = true;
                    ChkWastedInd.Properties.ReadOnly = true;
                    ChkMovingAvgInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    BtnAcNo3.Enabled = false;
                    BtnAcNo4.Enabled = false;
                    BtnAcNo5.Enabled = false;
                    BtnAcNo6.Enabled = false;
                    BtnAcNo7.Enabled = false;
                    BtnAcNo8.Enabled = false;
                    BtnAcNo9.Enabled = false;
                    BtnAcNo10.Enabled = false;
                    BtnAcNo11.Enabled = false;
                    BtnAcNo12.Enabled = false;
                    if (mIsItemCategoryUseCOAAPAR)
                    TxtItCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtItCtCode, TxtItCtName }, false);
                    ChkWastedInd.Properties.ReadOnly = false;
                    if (mIsMovingAvgEnabled) ChkMovingAvgInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;
                    BtnAcNo10.Enabled = true;
                    BtnAcNo11.Enabled = true;
                    BtnAcNo12.Enabled = true;
                    TxtItCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtItCtName }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkWastedInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;
                    BtnAcNo10.Enabled = true;
                    BtnAcNo11.Enabled = true;
                    BtnAcNo12.Enabled = true;
                    TxtItCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCtCode, TxtItCtName, 
                TxtAcNo, TxtAcDesc, 
                TxtAcNo2, TxtAcDesc2, 
                TxtAcNo3, TxtAcDesc3, 
                TxtAcNo4, TxtAcDesc4,
                TxtAcNo5, TxtAcDesc5,
                TxtAcNo6, TxtAcDesc6,
                TxtAcNo7, TxtAcDesc7,
                TxtAcNo8, TxtAcDesc8,
                TxtAcNo9, TxtAcDesc9,
                TxtAcNo10, TxtAcDesc10,
                TxtAcNo11, TxtAcDesc11,
                TxtAcNo12, TxtAcDesc12
            });
            ChkActiveInd.Checked = false;
            ChkWastedInd.Checked = false;
            ChkMovingAvgInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActiveInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCtCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCtCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblItemCategory Where ItCtCode=@ItCtCode" };
                Sm.CmParam<String>(ref cm, "@ItCtCode", TxtItCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            //Moving average indicator jangan bisa diedit, terlalu riskan (TKG)
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemCategory(ItCtCode, ItCtName, ActInd, WastedInd, MovingAvgInd, AcNo, AcNo2, AcNo3, AcNo4, AcNo5, AcNo6, AcNo7, AcNo8, AcNo9, AcNo10, AcNo11, ");
                if (mIsItemCategoryUseCOAStockBeginning) SQL.AppendLine("AcNo12, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItCtCode, @ItCtName, @ActInd, @WastedInd, @MovingAvgInd, @AcNo, @AcNo2, @AcNo3, @AcNo4, @AcNo5, @AcNo6, @AcNo7, @AcNo8, @AcNo9, @AcNo10, @AcNo11, ");
                if(mIsItemCategoryUseCOAStockBeginning) SQL.AppendLine("@AcNo12, ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItCtName=@ItCtName, ActInd=@ActInd, WastedInd=@WastedInd, ");
                SQL.AppendLine("   AcNo=@AcNo, AcNo2=@AcNo2, AcNo3=@AcNo3, AcNo4=@AcNo4, AcNo5=@AcNo5, AcNo6=@AcNo6, AcNo7=@AcNo7, AcNo8=@AcNo8, AcNo9=@AcNo9, AcNo10=@AcNo10, AcNo11=@AcNo11, ");
                if (mIsItemCategoryUseCOAStockBeginning) SQL.AppendLine("AcNo12=@AcNo12, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItCtCode", TxtItCtCode.Text);
                Sm.CmParam<String>(ref cm, "@ItCtName", TxtItCtName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@WastedInd", ChkWastedInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@MovingAvgInd", ChkMovingAvgInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
                Sm.CmParam<String>(ref cm, "@AcNo3", TxtAcNo3.Text);
                Sm.CmParam<String>(ref cm, "@AcNo4", TxtAcNo4.Text);
                Sm.CmParam<String>(ref cm, "@AcNo5", TxtAcNo5.Text);
                Sm.CmParam<String>(ref cm, "@AcNo6", TxtAcNo6.Text);
                Sm.CmParam<String>(ref cm, "@AcNo7", TxtAcNo7.Text);
                Sm.CmParam<String>(ref cm, "@AcNo8", TxtAcNo8.Text);
                Sm.CmParam<String>(ref cm, "@AcNo9", TxtAcNo9.Text);
                Sm.CmParam<String>(ref cm, "@AcNo10", TxtAcNo10.Text);
                Sm.CmParam<String>(ref cm, "@AcNo11", TxtAcNo11.Text);
                Sm.CmParam<String>(ref cm, "@AcNo12", TxtAcNo12.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCtCode, A.ItCtName, A.ActInd, A.WastedInd, A.MovingAvgInd, ");
            SQL.AppendLine("A.AcNo, A.AcNo2, A.AcNo3, A.AcNo4, A.AcNo5, A.AcNo6, A.AcNo7, ");
            if (mIsItemCategoryUseCOAAPAR)
                SQL.AppendLine("A.AcNo8, A.AcNo9, A.AcNo10, A.AcNo11,");
            else
                SQL.AppendLine("null as AcNo8, null As AcNo9, null as AcNo10, null as AcNo11,");
            if(mIsItemCategoryUseCOAStockBeginning)
                SQL.AppendLine("A.AcNo12, ");
            else
                SQL.AppendLine("null as AcNo12,");
            if (mIsCOAUseAlias)
            {
                SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc, ");
                SQL.AppendLine("Concat(C.AcDesc, Case When C.Alias Is Null Then '' Else Concat(' [', C.Alias, ']') End) As AcDesc2, ");
                SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc3, ");
                SQL.AppendLine("Concat(E.AcDesc, Case When E.Alias Is Null Then '' Else Concat(' [', E.Alias, ']') End) As AcDesc4, ");
                SQL.AppendLine("Concat(F.AcDesc, Case When F.Alias Is Null Then '' Else Concat(' [', F.Alias, ']') End) As AcDesc5, ");
                SQL.AppendLine("Concat(G.AcDesc, Case When G.Alias Is Null Then '' Else Concat(' [', G.Alias, ']') End) As AcDesc6, ");
                SQL.AppendLine("Concat(H.AcDesc, Case When H.Alias Is Null Then '' Else Concat(' [', H.Alias, ']') End) As AcDesc7, ");
                if (mIsItemCategoryUseCOAAPAR)
                {
                    SQL.AppendLine("Concat(I.AcDesc, Case When I.Alias Is Null Then '' Else Concat(' [', I.Alias, ']') End) As AcDesc8, ");
                    SQL.AppendLine("Concat(J.AcDesc, Case When J.Alias Is Null Then '' Else Concat(' [', J.Alias, ']') End) As AcDesc9, ");
                    SQL.AppendLine("Concat(K.AcDesc, Case When K.Alias Is Null Then '' Else Concat(' [', K.Alias, ']') End) As AcDesc10, ");
                    SQL.AppendLine("Concat(L.AcDesc, Case When L.Alias Is Null Then '' Else Concat(' [', L.Alias, ']') End) As AcDesc11 ");
                }
                else
                {
                    SQL.AppendLine("null as AcDesc8, null as AcDesc9, null as AcDesc10, null as AcDesc11 ");
                }
                if (mIsItemCategoryUseCOAStockBeginning)
                    SQL.AppendLine(", Concat(M.AcDesc, Case When M.Alias Is Null Then '' Else Concat(' [', M.Alias, ']') End) As AcDesc12 ");
                else
                    SQL.AppendLine(", null as AcDesc12 ");
            }
            else
            {
                SQL.AppendLine("B.AcDesc, C.AcDesc As AcDesc2, D.AcDesc As AcDesc3, ");
                SQL.AppendLine("E.AcDesc As AcDesc4, F.AcDesc As AcDesc5, G.AcDesc As AcDesc6, H.AcDesc As AcDesc7, ");
                if (mIsItemCategoryUseCOAAPAR)
                {
                    SQL.AppendLine("I.AcDesc As AcDesc8, J.AcDesc As AcDesc9, K.AcDesc As AcDesc10, L.AcDesc As AcDesc11 ");
                }
                else
                    SQL.AppendLine("null as AcDesc8, null as AcDesc9, null as AcDesc10, null as AcDesc11 ");
                if (mIsItemCategoryUseCOAStockBeginning)
                    SQL.AppendLine(", M.AcDesc As AcDesc12 ");
                else
                    SQL.AppendLine(", null as AcDesc12 ");
            }
            SQL.AppendLine("From TblItemCategory A ");
            SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo3=D.AcNo ");
            SQL.AppendLine("Left Join TblCoa E On A.AcNo4=E.AcNo ");
            SQL.AppendLine("Left Join TblCoa F On A.AcNo5=F.AcNo ");
            SQL.AppendLine("Left Join TblCoa G On A.AcNo6=G.AcNo ");
            SQL.AppendLine("Left Join TblCoa H On A.AcNo7=H.AcNo ");
            if (mIsItemCategoryUseCOAAPAR)
            {
                SQL.AppendLine("Left Join TblCoa I On A.AcNo8=I.AcNo ");
                SQL.AppendLine("Left Join TblCoa J On A.AcNo9=J.AcNo ");
                SQL.AppendLine("Left Join TblCoa K On A.AcNo10=K.AcNo ");
                SQL.AppendLine("Left Join TblCoa L On A.AcNo11=L.AcNo ");
            }
            if (mIsItemCategoryUseCOAStockBeginning)
                SQL.AppendLine("Left Join TblCoa M On A.AcNo12=M.AcNo ");
            SQL.AppendLine("Where A.ItCtCode=@ItCtCode;");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItCtCode", ItCtCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        "ItCtCode", 
                        "ItCtName", "ActInd", "WastedInd", "MovingAvgInd", "AcNo", 
                        "AcNo2", "AcNo3", "AcNo4", "AcNo5", "AcNo6", 
                        "AcNo7", "AcNo8", "AcNo9", "AcNo10", "AcNo11",
                        "AcNo12", "AcDesc", "AcDesc2", "AcDesc3", "AcDesc4",
                        "AcDesc5", "AcDesc6", "AcDesc7", "AcDesc8", "AcDesc9",
                        "AcDesc10", "AcDesc11", "AcDesc12",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtItCtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtItCtName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        ChkWastedInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        ChkMovingAvgInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAcNo2.EditValue = Sm.DrStr(dr, c[6]);
                        TxtAcNo3.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAcNo4.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAcNo5.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAcNo6.EditValue = Sm.DrStr(dr, c[10]);
                        TxtAcNo7.EditValue = Sm.DrStr(dr, c[11]);
                        TxtAcNo8.EditValue = Sm.DrStr(dr, c[12]);
                        TxtAcNo9.EditValue = Sm.DrStr(dr, c[13]);
                        TxtAcNo10.EditValue = Sm.DrStr(dr, c[14]);
                        TxtAcNo11.EditValue = Sm.DrStr(dr, c[15]);
                        TxtAcNo12.EditValue = Sm.DrStr(dr, c[16]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[17]);
                        TxtAcDesc2.EditValue = Sm.DrStr(dr, c[18]);
                        TxtAcDesc3.EditValue = Sm.DrStr(dr, c[19]);
                        TxtAcDesc4.EditValue = Sm.DrStr(dr, c[20]);
                        TxtAcDesc5.EditValue = Sm.DrStr(dr, c[21]);
                        TxtAcDesc6.EditValue = Sm.DrStr(dr, c[22]);
                        TxtAcDesc7.EditValue = Sm.DrStr(dr, c[23]);
                        TxtAcDesc8.EditValue = Sm.DrStr(dr, c[24]);
                        TxtAcDesc9.EditValue = Sm.DrStr(dr, c[25]);
                        TxtAcDesc10.EditValue = Sm.DrStr(dr, c[26]);
                        TxtAcDesc11.EditValue = Sm.DrStr(dr, c[27]);
                        TxtAcDesc12.EditValue = Sm.DrStr(dr, c[28]);
                    }, true
                );
            }
            catch (Exception Exc)

            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCtCode, "Item's category code", false) ||
                Sm.IsTxtEmpty(TxtItCtName, "Item's category name", false) ||
                IsItCtCodeExisted()||
                IsCOAMandatoryInvalid();
        }

        private bool IsItCtCodeExisted()
        {
            if (!TxtItCtCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select ItCtCode From TblItemCategory Where ItCtCode=@Param;", TxtItCtCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Item's category code ( " + TxtItCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsCOAMandatoryInvalid()
        {
            if (!TxtItCtCode.Properties.ReadOnly) return false;
            if (mIsItCtAcNoMandatory && Sm.IsTxtEmpty(TxtAcNo, "COA account# for stock", false)) return true;
            if (mIsItCtAcNo2Mandatory && Sm.IsTxtEmpty(TxtAcNo2, "COA account# for cost", false)) return true;
            if (mIsItCtAcNo3Mandatory && Sm.IsTxtEmpty(TxtAcNo3, "COA account# for COGM", false)) return true;
            if (mIsItCtAcNo4Mandatory && Sm.IsTxtEmpty(TxtAcNo4, "COA account# for sales", false)) return true;
            if (mIsItCtAcNo5Mandatory && Sm.IsTxtEmpty(TxtAcNo5, "COA account# for COGS", false)) return true;
            if (mIsItCtAcNo6Mandatory && Sm.IsTxtEmpty(TxtAcNo6, "COA account# for sales return", false)) return true;
            if (mIsItCtAcNo7Mandatory && Sm.IsTxtEmpty(TxtAcNo7, "COA account# for purchase return", false)) return true;

            return false;
        }

        #endregion

        #region Addditional Method
        private void GetParameter()
        {
            //mIsCOAStockItemCategoryMandatory = Sm.GetParameterBoo("IsCOAStockItemCategoryMandatory");
            //mIsCOACostItemCategoryMandatory = Sm.GetParameterBoo("IsCOACostItemCategoryMandatory");
            //mIsProductionModulActive = Sm.GetParameterBoo("IsProductionModulActive");
            //mIsAcNoForPurchaseReturnUseItemCategory = Sm.GetParameterBoo("IsAcNoForPurchaseReturnUseItemCategory");

            mAcNoForStoreSales = Sm.GetParameter("AcNoForStoreSales");
            mAcNoForSalesReturnInvoice = Sm.GetParameter("AcNoForSalesReturnInvoice");
            mAcNoForCOGS = Sm.GetParameter("AcNoForCOGS");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsItCtAcNoMandatory = Sm.GetParameterBoo("IsItCtAcNoMandatory");
            mIsItCtAcNo2Mandatory = Sm.GetParameterBoo("IsItCtAcNo2Mandatory");
            mIsItCtAcNo3Mandatory = Sm.GetParameterBoo("IsItCtAcNo3Mandatory");
            mIsItCtAcNo4Mandatory = Sm.GetParameterBoo("IsItCtAcNo4Mandatory");
            mIsItCtAcNo5Mandatory = Sm.GetParameterBoo("IsItCtAcNo5Mandatory");
            mIsItCtAcNo6Mandatory = Sm.GetParameterBoo("IsItCtAcNo6Mandatory");
            mIsItCtAcNo7Mandatory = Sm.GetParameterBoo("IsItCtAcNo7Mandatory");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
            mIsItemCategoryUseCOAAPAR = Sm.GetParameterBoo("IsItemCategoryUseCOAAPAR");
            mIsItemCategoryUseCOAStockBeginning = Sm.GetParameterBoo("IsItemCategoryUseCOAStockBeginning");
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCtCode);
        }

        private void TxtItCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCtName);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 2));
        }

        private void BtnAcNo3_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 3));
        }

        private void BtnAcNo4_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 4));
        }

        private void BtnAcNo5_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 5));
        }

        private void BtnAcNo6_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 6));
        }

        private void BtnAcNo7_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 7));
        }
        
        private void BtnAcNo8_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 8));
        }

        private void BtnAcNo9_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 9));
        }

        private void BtnAcNo10_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 10));
        }

        private void BtnAcNo11_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 11));
        }

        private void BtnAcNo12_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemCategoryDlg(this, 12));
        }

        #endregion

        #endregion

    }
}
