﻿#region Update

#region Old
/*
    15/07/2017 [TKG] GetEntityCostCenter, GetEntityWarehouse
	19/07/2017 [HAR] GetEmpLongServiceLeave1 (tambah parameter)
    21/08/2017 [TKG] function baru untuk save data ditambah parameter menu code. Misal : StdMsgYN("Save", string.Empty, mMenuCode)
    31/08/2017 [TKG] proses cuti tahunan/besar menggunakan employee's leave start date.
    28/09/2017 [WED] tambah case Process data di StdMsgYN
    20/10/2017 [TKG] tambah GetValue dengan 3 parameter.
    27/10/2017 [TKG] pada saat info di grid digroup, dimunculkan informasi jumlah data tiap group. 
    03/11/2017 [TKG] bug fixing saat group di ShowHrdCountAfterContent()
    17/11/2017 [TKG] bug fixing ShowHrdCountAfterContent
    15/12/2017 [TKG] tambah function IsClosingJournalInvalid
    08/01/2018 [WED] bug fix baris terakhir saat export excel format date dan time
    25/01/2018 [TKG] warning per master/transaksi saat save apabila parameter WMenu=Y
    22/05/2018 [TKG] tambah setting access tombol excel di form transaksi dengan grid
    28/07/2018 [TKG] tambah method GetNewJournalDocNo
    07/08/2018 [TKG] tambah method SetLue5
    14/09/2018 [TKG] tambah function IsDataExists
    04/10/2018 [TKG] tambah GetValueDec dengan 4 parameter
    08/10/2018 [TKG] ToRoman
    19/02/2019 [TKG] StdMtd, Voucher, Voucher2, OutgoingPayment
    04/03/2019 [TKG] IsClosingJournalInvalid, IsClosingJournalUseCurrentDt
    12/03/2019 [WED] ubah GetParameterDec
    16/03/2019 [TKG] SetLueSiteCode2
    02/10/2019 [HAR/SRN] Terbilang3
    07/10/2019 [WED/TWC] GenerateDocNoVoucher
    01/02/2020 [TKG/IMS] SetGrdStringValueEmpty
    04/03/2020 [WED/KBN] GenerateDocNo3
    21/04/2020 [WED] ExportToExcel  
    26/05/2020 [HAR/ALL] tambah function InsertButtonUsageHistory
    04/06/2020 [TKG/IMS] tambah FormatNum untuk 3 angka di belakang koma
    22/06/2020 [HAR/KBN] Function PrintReport tambah clear chache
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    21/09/2020 [TKG/HIN] tambah 4 angka di belakang koma utk persentase (Method FormatNum)
    22/09/2020 [TKG/HIN] tambah 4 angka di belakang koma utk persentase (Method GrdFormatDec)
    29/09/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    16/11/2020 [DITA/IMS+ALL] tambah method SetFormForGroupUser untuk memberi validasi form menu berdasarkan group code
    25/11/2020 [WED/IMS] GenerateDocNo reset nomor urut berdasarkan abbreviation nya, berdasarkan parameter IsDocNoResetByDocAbbr
    03/12/2020 [VIN/KSM] Tambah GenerateLocalDocNoSalesMemo
    08/01/2020 [DITA/ALL] tambah method buat gethash sha 256 dan base 64
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
    11/02/2021 [IBL/SIER] tambah function MonthRoman
    02/03/2021 [WED/ALL] tambah Trim() untuk parcode di method GetParameter dkk
    02/03/2021 [TKG/ALL] tambah method ExecCommandSP dan CmParam
    20/04/2021 [TKG/PHT] tambah GetNewVoucherRequestDocNo
    22/07/2021 [WED/PHT] tambah ComputeUsedBudget dan ComputeAvailableBudget
    22/07/2021 [WED/PHT] masih ada bug di ComputeAvailableBudget
    26/07/2021 [WED/PHT] bug saat ComputeAvailableBudget
    26/07/2021 [TKG/PHT] bug saat SelectUsedBudget()
    02/08/2021 [TRI/IMS] bug saat ComputeAvailableBudget
    18/08/2021 [DITA/PHT] Source budget di ComputeAvailableBudget ambil dari RKAP based on param : BudgetSourceFormula
    03/09/2021 [DITA/AMKA] ubah format computeusedbudget based on param : BudgetUsedFormula, kalau 1 = dari transaksi2, klau 2 : dari jurnal berdasarkan coa nya
    08/09/2021 [DITA/SIER] bug saat resfersh budget summary (CAS)
    06/10/2021 [VIN/SIER] bug rumus terbalik  ComputeUsedBudget ((mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)--> ambil dr estimate Price MR)
    22/10/2021 [DITA/ALL] tambah icontype baru di method GrdCollButton
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled utk method GenerateDocNoVoucher
    28/10/2021 [DITA/PHT] ComputeAvailableBudget juga dipengaruhi oleh Budget Transfer
    08/12/2021 [SET/SIER] Merubah "milyar" menjadi "miliar" untuk method Terbilang

*/
#endregion

/*
    31/01/2022 [VIN/SIER] BUG: ComputeUsedBudget -> CAS And A.DocNo != @DocNo dimasukkan ke type 1 
    07/02/2022 [VIN/TWC] BUG: ComputeUsedBudget ->  type 1 samain type 2 tambah :mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice 
    16/02/2022 [DITA/PHT] Filter Single Date Edit
    17/02/2022 [ISD/PHT] tambah IsClosingJournalInvalid dan IsClosingJournalUseCurrentDt dengan multi profit center
    23/02/2022 [WED/ALL] Saat Export Excel, semua kolom yang bukan tanggal dan decimal ditambahkan karakter petik satu di awal, agar jika ada kode angka yang depannya 0 itu ikut ter export
    02/03/2022 [WED/PHT] IsClosingJournalInvalid ditambah query kondisi "ProfitCenterCode is null"
    03/03/2022 [TKG/PHT] ubah IsClosingJournalInvalid versi PHT untuk validasi menggunakan profit center dengan level tertentu
    24/03/2022 [MYA/PHT] Merubah Query ComputeAvailableBudget
    29/03/2022 [ISD/PRODUCT] Penyesuaian penghitungan kolom used pada reporting budget summary di ComputeUsedBudget
    06/04/2022 [DITA/PHT] perubahan validasi IsClosingJournalInvalid dan IsClosingJournalUseCurrentDt saat kondisi cancel transaksi
    11/04/2022 [DITA/PHT] tambah profitcentername saat validasi IsClosingJournalInvalid where cancel true
    18/04/2022 [BRI/PHT] tambah ExportToExcel khusus CBPPL
    18/04/2022 [IBL/PRODUCT] tambah method ComputeStockAccumulaton() utk menghitung akumulasi Qty & AcquisitionCost Investment Stock
    10/05/2022 [WED/ALL] tambah cm.Prepare()
    23/05/2022 [IBL/PRODUC] ubah filter method PrepDataStockAccumulation() dari A.InvestmentCode jadi C.PortofolioId
    24/05/2022 [RIS/PHT] Mengubah generate docno (order by convert)
    06/07/2022 [DITA/PHT] pada method ComputeAvailableBudget include perhitungan dengan transaksi Receiving Item From Vendor Auto DO
    09/06/2022 [RDA/PHT] penambahan method pembulatan RoundDown untuk keperluan pembulatan tax pada Sales Invoice based on DO dan Purchase Invoice - AP client PHT
    21/07/2022 [DITA/SIER] based on param IsBudgetTransactionCanUseOtherDepartmentBudget, untuk pengurang budget di MR + used budget summary MR juga ambil dari department for budget/ DeptCode2
    26/07/2022 [DITA/SIER] perubahaan computeusedbudget utk VR dan MR
    26/07/2022 [SET/SIER] Tambah Method ShowDocApproval() berdasar parameter
    28/07/2022 [SET/SIER] Penyesuaian Method ShowDocApproval() berdasar parameter
    11/08/2022 [SET/SIER] Penyesuaian method ShowDocApproval() @Dt -> CurDate()
    12/08/2022 [DITA/SIER] tambah apram : IsBudgetUseMRProcessInd karna dis eier ingin melihat mr yg sudah final saja
    16/08/2022 [MYA/SIER] FEEDBACK : Memunculkan seluruh approver pada tab approval information (seperti pada monitoring document approval setting) dan menambahkan kolom jabatan
    21/09/2022 [MYA/SIER] FEEDBACK : Jabatan PLT belum ada spasi setelah jabatan
    21/09/2022 [MAU/SIER] FEEDBACK : Jabatan menyesuaikan dengan approver (tidak menampilkan semua)
    11/10/2022 [VIN/SIER] BUG Used budget CAS berdasarkan bc code 
    14/10/2022 [BRI/PHT] tambah generate LocalCode, VC with LocalCode, JN with LocalCode
    22/11/2022 [VIN/SIER] BUG : tambah param IsCASUseBudgetCategory
    05/01/2023 [WED/PHT] tambah GetNewJournalDocNoWithAddCodes
    09/01/2023 [DITA/PHT] tambah param : IsClosingJournalDisableFicoTransactionDocumentEdit saat closing journal supaya dokuemn yg sudah ada closingnya bisa tidak bisa di cancel
    11/01/2023 [WED/PHT] Generate journal docno yg lama ditambahin validasi jumlah "/" nya, biar gak kena ke journal docno yg punya nya PHT baru
    13/01/2023 [WED/PHT] bug fix GenerateJNLocalCode
    08/02/2023 [WED/MNET] tambah ExecQuery
    10/02/2023 [WED/HEX] tambah upload dan download file (termasuk rename file ketika upload)
    15/02/2023 [DITA/SIER] magerin rename upload file menggunakan param : IsUploadFileRenamed
    17/02/2023 [BRI/HEX] bug fix ComputeUsedBudget
    17/02/2023 [DITA/SIER] bug di method UpdateUploadedFile()
    23/02/2023 [BRI/HEX] membuat calculate budget untuk fiscal year yang berbeda berdasarkan param FiscalYearRange
    28/02/2023 [WED/MNET] query untuk Realization Dropping Request
    02/03/2023 [WED/MNET] bug fix query untuk Realization Dropping Request, voucher yg cancel masih muncul + tambah dia RecvVd dan RecvVd2 auto DO atau bukan
    04/03/2023 [WED/HEX] tambah validasi Closing Procurement
    06/03/2023 [WED/HEX] tambah validasi Closing Procurement untuk default version (yg sebelumnya itu khusus untuk MR type 2)
    09/03/2023 [BRI/SIER] Bug fix UpdateUploadedFile()
    14/03/2023 [WED/HEX] bug fix ambil nilai budget awal di method ComputeAvailableBudget2
    23/03/2023 [SET/PHT] Validasi IsTransactionsCantSameDate untuk EmpLeave3 & TravelRequest5
    30/03/2023 [WED/PHT] tambah bank actp di GetCode1ForJournalDocNo
    05/04/2023 [BRI/SIER] Bug fix UpdateUploadedFile()
    10/04/2023 [WED/PRODUCT] bug fix ComputeUsedBudget2()
    12/04/2023 [WED/HEX] bug fix ComputeAvailableBudget2()
    17/04/2023 [WED/ALL] tambah prepared statement di setiap eksekusi query berdasarkan variable IsUsePreparedStatement
 */

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using TenTec.Windows.iGridLib.Printing;
using FastReport;
using System.Drawing.Printing;
using MySql.Data;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Wa = RunSystem.WinAPI;
using System.Security.Cryptography;
using System.Linq;
using System.Net;

namespace RunSystem
{
    #region class/struct/enum/delegate

    public enum mState { View, Insert, Edit };

    public delegate void ShowGrdData(SqlDataReader dr, iGrid Grd, int[] c, int Row);
    public delegate void ShowData(SqlDataReader dr, int[] c);
    public delegate void ShowDataFromFrmFind(string Param);

    public class EmployeeResidualLeave
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string JoinDt { get; set; }
        public string ActiveDt { get; set; }
        public string StartDt { get; set; }
        public string EndDt { get; set; }
        public decimal LeaveDay { get; set; }
        public decimal UsedDay { get; set; }
        public decimal RemainingDay { get; set; }
    }

    public class LeaveSummary
    {
        public string Yr { get; set; }
        public string EmpCode { get; set; }
        public string LeaveCode { get; set; }
        public string InitialDt { get; set; }
        public string StartDt { get; set; }
        public string EndDt { get; set; }
        public decimal NoOfDays1 { get; set; }
        public decimal NoOfDays2 { get; set; }
        public decimal NoOfDays3 { get; set; }
        public decimal NoOfDays4 { get; set; }
        public decimal Balance { get; set; }

        public string EmpName { get; set; }
        public bool IsInvalid { get; set; }
    }

    public class SalesInvoiceSummary
    {
        public string DocNo { get; set; }
        public string DNo { get; set; }
        public string DocType { get; set; }
        public decimal Amt1 { get; set; }
        public decimal Amt2 { get; set; }
        public string Remark { get; set; }
    }

    #endregion

    #region New

    public delegate void RefreshGrdData(MySqlDataReader dr, iGrid Grd, int[] c, int Row);
    public delegate void RefreshData(MySqlDataReader dr, int[] c);
    public enum mMsgType { NoData, Info, Warning };

    public struct ColList1
    {
        public string Code { set; get; }
        public string Name { set; get; }
    }

    public struct Lue1
    {
        public string Col1 { set; get; }
    }

    public struct Lue2
    {
        public string Col1 { set; get; }
        public string Col2 { set; get; }
    }

    public struct Lue3
    {
        public string Col1 { set; get; }
        public string Col2 { set; get; }
        public string Col3 { set; get; }
    }

    public struct Lue4
    {
        public string Col1 { set; get; }
        public string Col2 { set; get; }
        public string Col3 { set; get; }
        public string Col4 { set; get; }
    }

    public struct Lue5
    {
        public string Col1 { set; get; }
        public string Col2 { set; get; }
        public string Col3 { set; get; }
        public string Col4 { set; get; }
        public string Col5 { set; get; }
    }

    #endregion

    public sealed class StdMtd
    {
        #region New

        #region Standard Method

        private static string[] satuanUSD = new string[10] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        private static string[] belasanUSD = new string[10] { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        private static string[] puluhanUSD = new string[10] { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        private static string[] ribuanUSD = new string[5] { "", "Thousand", "Million", "Billion", "Trillion" };


        #region Entry point

        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }

        #endregion

        public static void ShowDataInGridGrdLvl2Grp(
           ref iGrid Grd, ref MySqlCommand cm, string SQL, string[] ColumnTitle,
           RefreshGrdData rgd, bool ShowNoDataInd, bool GroupInd, bool Editable, bool GrdAutoSize
)
        {
            ClearGrd(Grd, Editable);
            if (GroupInd)
            {
                Grd.GroupObject.Clear();
                Grd.Group();
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, ColumnTitle);
                    if (!dr.HasRows)
                    {
                        if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                    }
                    else
                    {
                        int Row = 0;
                        if (!Editable) Grd.Rows.Count = 0;
                        Grd.ProcessTab = true;
                        Grd.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd.Rows.Add();
                            rgd(dr, Grd, c, Row);
                            Row++;
                        }
                        if (GroupInd)
                        {
                            Grd.GroupObject.Add(8);
                            Grd.GroupObject.Add(1);
                            Grd.Group();
                            SetGrdAlwaysShowSubTotal(Grd);
                        }
                    }
                    //Sm.SetGrdAutoSize(Grd);
                    if (GrdAutoSize) Grd.Cols.AutoWidth();
                    Grd.EndUpdate();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
        }

        public static void ButtonReadOnly(
            mState state,
            string AccessInd,
            ref SimpleButton BtnFind,
            ref SimpleButton BtnInsert,
            ref SimpleButton BtnEdit,
            ref SimpleButton BtnDelete,
            ref SimpleButton BtnSave,
            ref SimpleButton BtnCancel,
            ref SimpleButton BtnPrint)
        {
            if (AccessInd.Length == 0)
            {
                BtnFind.Enabled =
                BtnInsert.Enabled =
                BtnEdit.Enabled =
                BtnDelete.Enabled =
                BtnPrint.Enabled =
                BtnSave.Enabled =
                BtnCancel.Enabled = false;
            }
            else
            {
                BtnFind.Enabled = (state == mState.View);
                BtnInsert.Enabled = (state == mState.View) ? CompareStr(AccessInd.Substring(0, 1), "Y") : false;
                BtnEdit.Enabled = (state == mState.View) ? CompareStr(AccessInd.Substring(1, 1), "Y") : false;
                BtnDelete.Enabled = (state == mState.View) ? CompareStr(AccessInd.Substring(2, 1), "Y") : false;
                BtnPrint.Enabled = (state == mState.View) ? CompareStr(AccessInd.Substring(3, 1), "Y") : false;
                BtnSave.Enabled = !BtnFind.Enabled;
                BtnCancel.Enabled = BtnSave.Enabled;
            }
        }

        public static string QueryRealizationDocs()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    -- Procurement ");
            SQL.AppendLine("    SELECT A.DocNo, GROUP_CONCAT(Distinct I.DocNo) AS Realization, A.DocType, Group_Concat(Distinct Date_Format(I.DocDt, '%d/%m/%Y')) RealizationDocDt, Sum(J.UPrice * H.Qty) RealizationAmt, ");
            SQL.AppendLine("    If(K.DocNo Is Null, 'N', 'Y') As AutoDOInd ");
            SQL.AppendLine("    FROM tbldroppingrequesthdr A ");
            SQL.AppendLine("    INNER JOIN tblmaterialrequesthdr B ON A.DocNo=B.DroppingRequestDocNo ");
            SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl C ON B.DocNo=C.DocNo And C.CancelInd = 'N' And C.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN tblporequestdtl D ON D.MaterialRequestDocNo=C.DocNo AND D.MaterialRequestDNo=C.DNo And D.CancelInd = 'N' And D.Status ='A' ");
            SQL.AppendLine("    INNER JOIN tblpodtl F ON D.DocNo=F.PORequestDocNo AND D.DNo=F.PORequestDNo And F.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tblrecvvddtl H ON H.PODocNo=F.DocNo AND H.PODNo=F.DNo And H.CancelInd = 'N' And H.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN tblrecvvdhdr I ON H.DocNo=I.DocNo AND I.POInd='Y' ");
            SQL.AppendLine("    Inner Join TblQtDtl J On D.QtDocNo = J.DocNo And D.QtDNo = J.DNo ");
            SQL.AppendLine("    Left Join TblDODeptHdr K On I.DocNo = K.RecvVdDocNo ");
            SQL.AppendLine("    WHERE A.DocType='1' And Find_In_Set(A.DocNo, @DocNo) And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.DocNo, A.DocType ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    -- Pengadaan Langsung ");
            SQL.AppendLine("    SELECT A.DocNo, GROUP_CONCAT(Distinct I.DocNo) AS Realization, A.DocType, Group_Concat(Distinct Date_Format(I.DocDt, '%d/%m/%Y')) RealizationDocDt, Sum(H.UPrice * H.Qty) RealizationAmt, ");
            SQL.AppendLine("    If(J.DocNo Is Null, 'N', 'Y') As AutoDOInd ");
            SQL.AppendLine("    FROM tbldroppingrequesthdr A ");
            SQL.AppendLine("    INNER JOIN tblmaterialrequesthdr B ON A.DocNo=B.DroppingRequestDocNo ");
            SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl C ON B.DocNo=C.DocNo And C.CancelInd = 'N' And C.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN tblporequestdtl D ON D.MaterialRequestDocNo=C.DocNo AND D.MaterialRequestDNo=C.DNo And D.CancelInd = 'N' And D.Status ='A' ");
            SQL.AppendLine("    INNER JOIN tblpodtl F ON D.DocNo=F.PORequestDocNo AND D.DNo=F.PORequestDNo And F.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tblrecvvddtl H ON H.PODocNo=F.DocNo AND H.PODNo=F.DNo And H.CancelInd = 'N' And H.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN tblrecvvdhdr I ON H.DocNo=I.DocNo AND I.POInd='N' ");
            SQL.AppendLine("    Left Join TblDODeptHdr J On I.DocNo = J.RecvVdDocNo ");
            SQL.AppendLine("    WHERE A.DocType='2' And Find_In_Set(A.DocNo, @DocNo) And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.DocNo, A.DocType ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    -- Cash Advance ");
            SQL.AppendLine("    SELECT A.DocNo, GROUP_CONCAT(Distinct D.DocNo) AS Realization, A.DocType, Group_Concat(Distinct Date_Format(E.DocDt, '%d/%m/%Y')) RealizationDocDt, Sum(D.Amt) RealizationAmt, ");
            SQL.AppendLine("    'N' As AutoDOInd ");
            SQL.AppendLine("    FROM tbldroppingrequesthdr A ");
            SQL.AppendLine("    INNER JOIN tbldroppingpaymenthdr B ON A.DocNo=B.DRQDocNo And B.Status = 'A' And B.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tblvoucherhdr C ON B.VoucherRequestDocNo=C.VoucherRequestDocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tblcashadvancesettlementdtl D ON C.DocNo=D.VoucherDocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr E On D.DocNo = E.DocNo And E.Status = 'A' And E.CancelInd = 'N' ");
            SQL.AppendLine("    WHERE A.DocType='3' And Find_In_Set(A.DocNo, @DocNo) And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.DocNo, A.DocType ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    -- Reimbursement ");
            SQL.AppendLine("    SELECT A.DocNo, GROUP_CONCAT(Distinct B.DocNo) AS Realization, A.DocType, Group_Concat(Distinct Date_Format(B.DocDt, '%d/%m/%Y')) RealizationDocDt, Sum(B.TotalAmt) RealizationAmt, ");
            SQL.AppendLine("    'N' As AutoDOInd ");
            SQL.AppendLine("    FROM tbldroppingrequesthdr A ");
            SQL.AppendLine("    INNER JOIN tblpettycashdisbursementhdr B ON B.DroppingRequestDocNo=A.DocNo And B.CancelInd = 'N' And B.Status = 'A' ");
            SQL.AppendLine("    WHERE A.DocType='4' And Find_In_Set(A.DocNo, @DocNo) And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.DocNo, A.DocType ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    -- Payroll ");
            SQL.AppendLine("    SELECT A.DocNo, GROUP_CONCAT(Distinct E.DocNo) AS Realization, A.DocType, Group_Concat(Distinct Date_Format(E.DocDt, '%d/%m/%Y')) RealizationDocDt, Sum(C.Amt) RealizationAmt, ");
            SQL.AppendLine("    'N' As AutoDOInd ");
            SQL.AppendLine("    FROM tbldroppingrequesthdr A ");
            SQL.AppendLine("    INNER JOIN tbldroppingrequestdtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    INNER JOIN tblvoucherrequestexternalpayrolldtl C ON C.DroppingRequestDocNo=B.DocNo AND C.DroppingRequestDNo=B.DNo ");
            SQL.AppendLine("    INNER JOIN tblvoucherrequestexternalpayrollhdr D ON C.DocNo=D.DocNo And D.CancelInd = 'N' And D.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN tblvoucherhdr E ON D.VoucherRequestDocNo=E.VoucherRequestDocNo And E.CancelInd = 'N' ");
            SQL.AppendLine("    WHERE A.DocType='5' And Find_In_Set(A.DocNo, @DocNo) And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.DocNo, A.DocType ");

            return SQL.ToString();
        }

        public static void ButtonVisible(
            string AccessInd,
            ref SimpleButton BtnFind,
            ref SimpleButton BtnInsert,
            ref SimpleButton BtnEdit,
            ref SimpleButton BtnDelete,
            ref SimpleButton BtnSave,
            ref SimpleButton BtnCancel,
            ref SimpleButton BtnPrint)
        {
            if (AccessInd.Length == 0)
            {
                BtnInsert.Visible =
                BtnEdit.Visible =
                BtnDelete.Visible =
                BtnSave.Visible =
                BtnPrint.Visible = false;
            }
            else
            {
                BtnInsert.Visible = CompareStr(AccessInd.Substring(0, 1), "Y");
                BtnEdit.Visible = CompareStr(AccessInd.Substring(1, 1), "Y");
                BtnDelete.Visible = CompareStr(AccessInd.Substring(2, 1), "Y");
                BtnSave.Visible = BtnCancel.Visible = BtnInsert.Visible || BtnEdit.Visible;
                BtnPrint.Visible = CompareStr(AccessInd.Substring(3, 1), "Y");
            }
        }

        public static void ButtonVisible(
           string AccessInd,
           ref SimpleButton BtnFind,
           ref SimpleButton BtnInsert,
           ref SimpleButton BtnEdit,
           ref SimpleButton BtnDelete,
           ref SimpleButton BtnSave,
           ref SimpleButton BtnCancel,
           ref SimpleButton BtnPrint,
           ref SimpleButton BtnExcel)
        {
            if (AccessInd.Length == 0)
            {
                BtnInsert.Visible =
                BtnEdit.Visible =
                BtnDelete.Visible =
                BtnSave.Visible =
                BtnPrint.Visible =
                BtnExcel.Visible = false;
            }
            else
            {
                BtnInsert.Visible = CompareStr(AccessInd.Substring(0, 1), "Y");
                BtnEdit.Visible = CompareStr(AccessInd.Substring(1, 1), "Y");
                BtnDelete.Visible = CompareStr(AccessInd.Substring(2, 1), "Y");
                BtnSave.Visible = BtnCancel.Visible = BtnInsert.Visible || BtnEdit.Visible;
                BtnPrint.Visible = CompareStr(AccessInd.Substring(3, 1), "Y");
                BtnExcel.Visible = CompareStr(AccessInd.Substring(4, 1), "Y");
            }
        }

        public static void ButtonVisible(
            string AccessInd,
            ref SimpleButton BtnPrint,
            ref SimpleButton BtnExcel)
        {
            BtnPrint.Visible = CompareStr(AccessInd.Substring(3, 1), "Y");
            BtnExcel.Visible = CompareStr(AccessInd.Substring(4, 1), "Y");
        }

        public static void CmParam<T>(ref MySqlCommand cm, string ColumnName, T Value)
        {
            if (string.IsNullOrEmpty(Value.ToString()))
                cm.Parameters.AddWithValue(ColumnName, DBNull.Value);
            else
                cm.Parameters.AddWithValue(ColumnName, Value);
        }

        public static void CmParamDt(ref MySqlCommand cm, string Param, string Value)
        {
            if (Value.Length == 0)
                Sm.CmParam<String>(ref cm, Param, "");
            else
                Sm.CmParam<String>(ref cm, Param, Value.Substring(0, 8));
        }

        public static DateTime ConvertDateTime(string Value)
        {
            DateTime DateValue;
            DateTime.TryParseExact(Value.Substring(0, 12), "yyyyMMddHHmm", null, DateTimeStyles.None, out DateValue);
            return DateValue;
        }

        public static int CompareDtTm(string Value1, string Value2)
        {
            //Value1<Value2 : -1
            //Value1=Value2 : 0
            //Value1>Value2 : 1
            return (Value1.CompareTo(Value2));
        }

        public static bool CompareGrdStr(iGrid Grd1, int Row1, int Col1, iGrid Grd2, int Row2, int Col2)
        {
            return CompareStr(GetGrdStr(Grd1, Row1, Col1), GetGrdStr(Grd2, Row2, Col2));
        }

        public static bool CompareStr(string Par1, string Par2)
        {
            return (string.Compare(Par1, Par2) == 0);
        }

        public static string DrStr(MySqlDataReader dr, int c)
        {
            if (dr[c] == DBNull.Value)
                return string.Empty;
            else
                return dr.GetString(c);
        }

        public static decimal DrDec(MySqlDataReader dr, int c)
        {
            if (dr[c] == DBNull.Value)
                return 0m;
            else
                return dr.GetDecimal(c);
        }

        public static void DteKeyDown(iGrid Grd, ref bool fAccept, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    Grd.Focus();
                    break;
                case Keys.Escape:
                    fAccept = false;
                    Grd.Focus();
                    break;
            }
        }

        public static void DteLeave(DateEdit Dte, ref iGCell fCell, ref bool fAccept)
        {
            if (Dte.Visible)
            {
                // Finish editing.
                Dte.Visible = false;
                if (fAccept) fCell.Value = Sm.SetGrdDate(Sm.GetDte(Dte));
                //iGrid.MouseDownLocked = false;
            }
        }

        public static void DteRequestEdit(iGrid Grd, DateEdit Dte, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            int c = e.ColIndex;
            e.DoDefault = false;

            // Determine the coordinates of the DateTimePicker control.
            fCell = Grd.Cells[r, c];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            // This code is required if you can have cells which
            // is higher or wider than the grid.
            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            // Set up the DateTimePicker control's coordinates.
            Dte.Bounds = myBounds;

            // Put the date value from the current cell into 
            // the DateTimePicker control.

            //MessageBox.Show(fCell.Value.ToString());

            //Dte1.Value = (DateTime)fCell.Value;

            if (Sm.GetGrdStr(Grd, r, c).Length == 0)
                Dte.EditValue = null;
            else
            {
                var Dt = Sm.GetGrdDate(Grd, r, c).Substring(0, 8);
                Sm.SetDte(Dte, Dt);
            }

            // Display our editor and move the focus to it.
            Dte.Visible = true;
            Dte.Focus();

            fAccept = true;

            /*
             * Lock operations with mouse button down to support the editor value validation.
             * If you set the MouseDownLocked property to true iGrid will not perform 
             * any action (column width changing, sorting, cur cell and selection changing) 
             * when user presses a mouse button in the cells area or column header
             * iGrid.MouseDownLocked = true;
            */

        }

        public static void ExecCommand(MySqlCommand cm)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                cm.ExecuteNonQuery();
            }
        }

        public static void ExecCommands(List<MySqlCommand> cml)
        {
            var cn = new MySqlConnection(Gv.ConnectionString);
            MySqlTransaction tr = null;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                cml.ForEach
                (cm =>
                {
                    cm.Connection = cn;
                    cm.CommandTimeout = 1200;
                    cm.Transaction = tr;
                    if (Gv.IsUsePreparedStatement) cm.Prepare();
                    cm.ExecuteNonQuery();
                }
                );
                //tr.Rollback();
                 tr.Commit();
            }
            catch (Exception Exc)
            {
                if (cn.State == ConnectionState.Open) tr.Rollback();
                throw new Exception(Exc.Message);
            }
            finally
            {
                tr = null;
                cn.Close();
            }

            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    var tr = cn.BeginTransaction();
            //    try
            //    {
            //        cml.ForEach
            //        (cm =>
            //        {
            //            cm.Connection = cn;
            //            cm.CommandTimeout = 600;
            //            cm.Transaction = tr;
            //            cm.ExecuteNonQuery();
            //        }
            //        );
            //        //tr.Rollback();
            //        tr.Commit();
            //    }
            //    catch (Exception Exc)
            //    {
            //        tr.Rollback();
            //        throw new Exception(Exc.Message);
            //    }
            //    finally
            //    {
            //        tr = null;
            //    }
            //}
        }

        public static void ExecCommandSP(ref MySqlCommand cm, string ProcedureName)
        {
            cm.CommandText = ProcedureName;
            cm.CommandType = System.Data.CommandType.StoredProcedure;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                cm.ExecuteNonQuery();
            }
        }

        public static void ExportToExcelOld(iGrid Grd)
        {
            try
            {
                #region "Old format or invalid type library" error fix
                // If you use early binding for MS Excel COM Automation as we do, you may get the following error:
                // "Old format or invalid type library. (Exception from HRESULT: 0x80028018 (TYPE_E_INVDATAREAD))"
                // This bug is caused by the use of the Microsoft.Office.Interop.* libraries.
                // It's because the user local settings are different from the office version.
                // For instance, if you use the English version and after setting language to 'English US', and country
                // to 'United States' in the reagional and language settings under control panel it works fine.
                // To overcome the problem in code, you may use the following trick:

                System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                #endregion

                if (!IsExcelExist()) return;

                Cursor.Current = Cursors.WaitCursor;

                // Instantiate Excel and start a new workbook.
                objApp = new Excel.Application();
                objBooks = objApp.Workbooks;
                objBook = objBooks.Add(Missing.Value);
                objSheets = objBook.Worksheets;
                objSheet = (Excel.Worksheet)objSheets.get_Item(1);

                int Col = 0;

                var AdditionalHeaderRowCount = Grd.Header.Rows.Count - 1;
                var HeaderColCount = Grd.Cols.Count;

                object[,] arr = new object[Grd.Rows.Count + 2, Grd.Cols.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];

                object[] arrformat = new object[Grd.Cols.Count + 1];

                foreach (iGColHdr cell in Grd.Header.Cells)
                {
                    if (Grd.Cols[cell.ColIndex].Visible)
                    {
                        arr[0, Col] = cell.Value;
                        Col++;
                    }
                }


                // Export the contents of iGrid
                int Row = 0, RowTemp = 0;
                Col = 0;
                foreach (iGRow row in Grd.Rows)
                {
                    switch (row.Type)
                    {
                        case iGRowType.AutoGroupRow:
                            Row++;
                            arr[Row, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.ManualGroupRow:
                            Row++;
                            arr[Row, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.Normal:
                            string FormatType = string.Empty;

                            foreach (iGCell cell in row.Cells)
                            {
                                if (Grd.Rows[cell.RowIndex].VisibleUnderGrouping && Grd.Cols[cell.ColIndex].Visible)
                                {
                                    switch (Grd.Cols[cell.ColIndex].CellStyle.FormatString)
                                    {
                                        case "{0:dd/MMM/yyyy}":
                                            FormatType = "dd/MMM/yyyy";
                                            break;
                                        case "{0:HH:mm}":
                                            FormatType = "HH:mm";
                                            break;
                                        case "{0:##0}":
                                            FormatType = "##0";
                                            break;
                                        case "{0:#,##0}":
                                            FormatType = "#,##0";
                                            break;
                                        case "{0:#,##0.00##}":
                                            FormatType = "#,##0.00##";
                                            break;
                                        case "{0:#,##0.0000}":
                                            FormatType = "#,##0.0000";
                                            break;
                                        case "{0:#,##0.00######}":
                                            FormatType = "#,##0.00######";
                                            break;
                                        default:
                                            FormatType = string.Empty;
                                            break;
                                    }
                                    if (row.Index != RowTemp)
                                    {
                                        Row++;
                                        Col = 0;
                                    }
                                    else
                                    {
                                        if (Row == 0)
                                        {
                                            Row = 1;
                                            Col = 0;
                                        }
                                        else
                                            Col++;
                                    }
                                    arr[Row, Col] = cell.Value;
                                    arrformat[Col] = FormatType;
                                    RowTemp = row.Index;
                                }
                            }
                            break;
                    }
                }

                Excel.Range c1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)objSheet.Cells[3 + Grd.Rows.Count - 1, Grd.Cols.Count];
                Excel.Range range = objSheet.get_Range(c1, c2);

                range.Value2 = arr;


                Excel.Range cHdr1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range cHdr2 = (Excel.Range)objSheet.Cells[1, Grd.Cols.Count];
                Excel.Range rangeHdr = objSheet.get_Range(cHdr1, cHdr2);

                rangeHdr.Font.Bold = true;

                for (int iCol = 0; iCol < arrformat.GetLength(0); iCol++)
                {
                    if ((arrformat[iCol] != null) && (arrformat[iCol] != string.Empty))
                    {
                        Excel.Range cDet1 = (Excel.Range)objSheet.Cells[2, iCol + 1];
                        Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count - 1, iCol + 1];
                        Excel.Range rangeDet = objSheet.get_Range(cDet1, cDet2);
                        rangeDet.NumberFormat = arrformat[iCol];
                    }
                }

                //Auto Fit
                objSheet.Cells.Select();
                objSheet.Columns.AutoFit();
                objSheet.Rows.AutoFit();

                System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

                //Return control of Excel to the user.
                objApp.Visible = true;
                objApp.UserControl = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;

            }

            #region Old Code

            //#region "Old format or invalid type library" error fix
            //// If you use early binding for MS Excel COM Automation as we do, you may get the following error:
            //// "Old format or invalid type library. (Exception from HRESULT: 0x80028018 (TYPE_E_INVDATAREAD))"
            //// This bug is caused by the use of the Microsoft.Office.Interop.* libraries.
            //// It's because the user local settings are different from the office version.
            //// For instance, if you use the English version and after setting language to 'English US', and country
            //// to 'United States' in the reagional and language settings under control panel it works fine.
            //// To overcome the problem in code, you may use the following trick:
            //System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //#endregion

            //try
            //{
            //    if (!IsExcelExist()) return;

            //    Cursor.Current = Cursors.WaitCursor;

            //    // Instantiate Excel and start a new workbook.
            //    objApp = new Excel.Application();
            //    objBooks = objApp.Workbooks;
            //    objBook = objBooks.Add(Missing.Value);
            //    objSheets = objBook.Worksheets;
            //    objSheet = (Excel.Worksheet)objSheets.get_Item(1);

            //    int Col = 0;

            //    foreach (iGColHdr cell in Grd.Header.Cells)
            //    {
            //        if (Grd.Cols[cell.ColIndex].Visible)
            //        {
            //            //SetExcelCellValue(0, cell.ColIndex, cell.Value, 0);
            //            SetExcelCellValue(0, Col, cell.Value, 0);
            //            Col++;
            //        }
            //    }

            //    // Export the contents of iGrid
            //    int Row = 0, RowTemp = 0;
            //    Col = 0;

            //    foreach (iGRow row in Grd.Rows)
            //    {
            //        switch (row.Type)
            //        {
            //            case iGRowType.AutoGroupRow:
            //                //SetExcelCellValue(row.Index + 1, 0, row.RowTextCell.Value, true, 0);
            //                Row++;
            //                SetExcelCellValue(Row, 0, row.RowTextCell.Value, true, 0);
            //                break;
            //            case iGRowType.ManualGroupRow:
            //                //SetExcelCellValue(row.Index + 1, 0, row.RowTextCell.Value, 0);
            //                Row++;
            //                SetExcelCellValue(Row, 0, row.RowTextCell.Value, 0);
            //                break;
            //            case iGRowType.Normal:
            //                byte FormatType = 0;

            //                foreach (iGCell cell in row.Cells)
            //                {
            //                    if (Grd.Rows[cell.RowIndex].VisibleUnderGrouping && Grd.Cols[cell.ColIndex].Visible)
            //                    {
            //                        switch (Grd.Cols[cell.ColIndex].CellStyle.FormatString)
            //                        {
            //                            case "{0:dd/MMM/yyyy}":
            //                                FormatType = 1;
            //                                break;
            //                            case "{0:HH:mm}":
            //                                FormatType = 2;
            //                                break;
            //                            case "{0:##0}":
            //                                FormatType = 3;
            //                                break;
            //                            case "{0:#,##0}":
            //                                FormatType = 4;
            //                                break;
            //                            case "{0:#,##0.00##}":
            //                                FormatType = 5;
            //                                break;
            //                            case "{0:#,##0.0000}":
            //                                FormatType = 6;
            //                                break;
            //                            default:
            //                                FormatType = 0;
            //                                break;
            //                        }
            //                        //SetExcelCellValue(row.Index + 1, cell.ColIndex, cell.Value, FormatType);
            //                        if (row.Index != RowTemp)
            //                        {
            //                            Row++;
            //                            Col = 0;
            //                        }
            //                        else
            //                        {
            //                            if (Row == 0)
            //                            {
            //                                Row = 1;
            //                                Col = 0;
            //                            }
            //                            else
            //                                Col++;
            //                        }
            //                        SetExcelCellValue(Row, Col, cell.Value, FormatType);
            //                        RowTemp = row.Index;
            //                    }
            //                }
            //                break;
            //        }
            //    }

            //    //Auto Fit
            //    objSheet.Cells.Select();
            //    objSheet.Columns.AutoFit();
            //    objSheet.Rows.AutoFit();

            //    System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

            //    //Return control of Excel to the user.
            //    objApp.Visible = true;
            //    objApp.UserControl = true;
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;

            //}

            #endregion
        }

        public static void ExportToExcel(iGrid Grd)
        {
            try
            {
                #region "Old format or invalid type library" error fix
                // If you use early binding for MS Excel COM Automation as we do, you may get the following error:
                // "Old format or invalid type library. (Exception from HRESULT: 0x80028018 (TYPE_E_INVDATAREAD))"
                // This bug is caused by the use of the Microsoft.Office.Interop.* libraries.
                // It's because the user local settings are different from the office version.
                // For instance, if you use the English version and after setting language to 'English US', and country
                // to 'United States' in the reagional and language settings under control panel it works fine.
                // To overcome the problem in code, you may use the following trick:

                System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                #endregion

                if (!IsExcelExist()) return;

                Cursor.Current = Cursors.WaitCursor;

                // Instantiate Excel and start a new workbook.
                objApp = new Excel.Application();
                objBooks = objApp.Workbooks;
                objBook = objBooks.Add(Missing.Value);
                objSheets = objBook.Worksheets;
                objSheet = (Excel.Worksheet)objSheets.get_Item(1);

                int Col = 0;

                var AdditionalHeaderRowCount = Grd.Header.Rows.Count - 1;
                var HeaderColCount = Grd.Cols.Count;

                object[,] arr = new object[Grd.Rows.Count + AdditionalHeaderRowCount + 2, Grd.Cols.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];

                object[] arrformat = new object[Grd.Cols.Count + 1];

                string ColSpanHeaderTitle = "";
                int ColSpanCount = 0;
                int ColSpanCurrentRow = -1;
                // added by Cuilin. 3 march 2016, to export header row.
                for (int iCol = 0; iCol < Grd.Cols.Count; iCol++)
                {
                    if (Grd.Cols[iCol].Visible)
                    {
                        for (int iRow = (Grd.Header.Rows.Count - 1); iRow >= 0; iRow--)
                        {
                            if (Grd.Header.Cells[iRow, iCol].SpanCols > 1)
                            {
                                ColSpanCurrentRow = iRow;
                                ColSpanCount = Grd.Header.Cells[iRow, iCol].SpanCols;
                                if (Grd.Header.Cells[iRow, iCol].Value != null)
                                    ColSpanHeaderTitle = Grd.Header.Cells[iRow, iCol].Value.ToString();
                            }
                            if ((Grd.Header.Cells[iRow, iCol].Value == null) && (ColSpanCount > 0) && (iRow == ColSpanCurrentRow))
                                arr[Grd.Header.Rows.Count - (iRow + 1), Col] = ColSpanHeaderTitle.Replace(Environment.NewLine, " ");
                            else
                                if (Grd.Header.Cells[iRow, iCol].Value != null)
                                arr[Grd.Header.Rows.Count - (iRow + 1), Col] = Grd.Header.Cells[iRow, iCol].Value.ToString().Replace(Environment.NewLine, " ");
                        }
                        Col++;
                    }
                    ColSpanCount--;
                }

                // remark by Cuilin. 3 march 2016, header only export the first header line.
                //foreach (iGColHdr cell in Grd.Header.Cells)
                //{
                //    if (Grd.Cols[cell.ColIndex].Visible)
                //    {
                //        arr[0, Col] = cell.Value;
                //        Col++;
                //     }
                //}


                int Row = 0, RowTemp = 0;
                Col = 0;
                // Export the contents of iGrid
                foreach (iGRow row in Grd.Rows)
                {
                    switch (row.Type)
                    {
                        case iGRowType.AutoGroupRow:
                            Row++;
                            arr[Row + AdditionalHeaderRowCount, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.ManualGroupRow:
                            Row++;
                            arr[Row + AdditionalHeaderRowCount, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.Normal:
                            string FormatType = string.Empty;

                            foreach (iGCell cell in row.Cells)
                            {
                                if (Grd.Rows[cell.RowIndex].VisibleUnderGrouping && Grd.Cols[cell.ColIndex].Visible)
                                {
                                    switch (Grd.Cols[cell.ColIndex].CellStyle.FormatString)
                                    {
                                        case "{0:dd/MMM/yyyy}":
                                            FormatType = "dd/MMM/yyyy";
                                            break;
                                        case "{0:HH:mm}":
                                            FormatType = "HH:mm";
                                            break;
                                        case "{0:##0}":
                                            FormatType = "##0";
                                            break;
                                        case "{0:#,##0}":
                                            FormatType = "#,##0";
                                            break;
                                        case "{0:#,##0.00##}":
                                            FormatType = "#,##0.00##";
                                            break;
                                        case "{0:#,##0.0000}":
                                            FormatType = "#,##0.0000";
                                            break;
                                        case "{0:#,##0.00######}":
                                            FormatType = "#,##0.00######";
                                            break;
                                        case "{0:#,##0.00}":
                                            FormatType = "#,##0.00";
                                            break;
                                        case "{0:#,##0.00###}":
                                            FormatType = "#,##0.00###";
                                            break;
                                        case "{0:#,##0.00####}":
                                            FormatType = "#,##0.00####";
                                            break;
                                        case "{0:#,##0.00#####}":
                                            FormatType = "#,##0.00#####";
                                            break;
                                        default:
                                            FormatType = string.Empty;
                                            break;
                                    }

                                    if (FormatType == string.Empty)
                                    {
                                        if (cell.Value != null && cell.Value.ToString().Trim().Length > 0)
                                        {
                                            if (cell.Value.ToString().ToLower() != "true" && cell.Value.ToString().ToLower() != "false")
                                            {
                                                if (Left(cell.Value.ToString(), 1) != "'")
                                                {
                                                    cell.Value = string.Concat("'", cell.Value);
                                                }
                                            }
                                        }
                                    }

                                    if (row.Index != RowTemp)
                                    {
                                        Row++;
                                        Col = 0;
                                    }
                                    else
                                    {
                                        if (Row == 0)
                                        {
                                            Row = 1;
                                            Col = 0;
                                        }
                                        else
                                            Col++;
                                    }
                                    arr[Row + AdditionalHeaderRowCount, Col] = cell.Value;
                                    arrformat[Col] = FormatType;
                                    RowTemp = row.Index;

                                    if (FormatType == string.Empty)
                                    {
                                        if (cell.Value != null && cell.Value.ToString().Trim().Length > 0)
                                        {
                                            if (cell.Value.ToString().ToLower() != "true" && cell.Value.ToString().ToLower() != "false")
                                            {
                                                if (Left(cell.Value.ToString(), 1) == "'")
                                                {
                                                    cell.Value = Sm.Right(cell.Value.ToString(), cell.Value.ToString().Length - 1); //string.Concat("'", cell.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }

                Excel.Range c1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)objSheet.Cells[3 + Grd.Rows.Count - 1, Grd.Cols.Count];
                Excel.Range range = objSheet.get_Range(c1, c2);

                range.Value2 = arr;


                Excel.Range cHdr1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range cHdr2 = (Excel.Range)objSheet.Cells[Grd.Header.Rows.Count, Grd.Cols.Count];
                Excel.Range rangeHdr = objSheet.get_Range(cHdr1, cHdr2);

                rangeHdr.Font.Bold = true;

                for (int iCol = 0; iCol < arrformat.GetLength(0); iCol++)
                {
                    if ((arrformat[iCol] != null) && (arrformat[iCol].ToString() != string.Empty))
                    {
                        Excel.Range cDet1 = (Excel.Range)objSheet.Cells[2, iCol + 1];
                        //Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count - 1, iCol + 1];
                        Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count, iCol + 1];
                        Excel.Range rangeDet = objSheet.get_Range(cDet1, cDet2);
                        rangeDet.NumberFormat = arrformat[iCol];
                    }
                }

                //Auto Fit
                objSheet.Cells.Select();
                objSheet.Columns.AutoFit();
                objSheet.Rows.AutoFit();

                System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

                //Return control of Excel to the user.
                objApp.Visible = true;
                objApp.UserControl = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;

            }
        }

        public static void ExportToExcelCBP(iGrid Grd)
        {
            try
            {
                #region "Old format or invalid type library" error fix
                // If you use early binding for MS Excel COM Automation as we do, you may get the following error:
                // "Old format or invalid type library. (Exception from HRESULT: 0x80028018 (TYPE_E_INVDATAREAD))"
                // This bug is caused by the use of the Microsoft.Office.Interop.* libraries.
                // It's because the user local settings are different from the office version.
                // For instance, if you use the English version and after setting language to 'English US', and country
                // to 'United States' in the reagional and language settings under control panel it works fine.
                // To overcome the problem in code, you may use the following trick:

                System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                #endregion

                if (!IsExcelExist()) return;

                Cursor.Current = Cursors.WaitCursor;

                // Instantiate Excel and start a new workbook.
                objApp = new Excel.Application();
                objBooks = objApp.Workbooks;
                objBook = objBooks.Add(Missing.Value);
                objSheets = objBook.Worksheets;
                objSheet = (Excel.Worksheet)objSheets.get_Item(1);

                int Col = 0;

                var AdditionalHeaderRowCount = Grd.Header.Rows.Count - 1;
                var HeaderColCount = Grd.Cols.Count;

                object[,] arr = new object[Grd.Rows.Count + AdditionalHeaderRowCount + 2, Grd.Cols.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];
                object[,] arrtemp = new object[Grd.Rows.Count + AdditionalHeaderRowCount + 2, Grd.Cols.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];

                object[] arrformat = new object[Grd.Cols.Count + 1];

                string ColSpanHeaderTitle = "";
                int ColSpanCount = 0;
                int ColSpanCurrentRow = -1;
                // added by Cuilin. 3 march 2016, to export header row.
                for (int iCol = 0; iCol < Grd.Cols.Count; iCol++)
                {
                    for (int iRow = (Grd.Header.Rows.Count - 1); iRow >= 0; iRow--)
                    {
                        if (Grd.Header.Cells[iRow, iCol].SpanCols > 1)
                        {
                            ColSpanCurrentRow = iRow;
                            ColSpanCount = Grd.Header.Cells[iRow, iCol].SpanCols;
                            if (Grd.Header.Cells[iRow, iCol].Value != null)
                                ColSpanHeaderTitle = Grd.Header.Cells[iRow, iCol].Value.ToString();
                        }
                        if (Grd.Header.Cells[iRow, iCol].Value != null)
                        {
                            if (ColSpanCount > 1 && iRow != 0)
                                for (int i = 0; i < ColSpanCount; i++)
                                    Grd.Header.Cells[iRow, iCol + i].Value = Grd.Header.Cells[iRow, iCol].Value;
                        }
                    }
                    Col++;
                    ColSpanCount--;
                }

                Col = 0;
                ColSpanCount = 0;
                for (int iCol = 0; iCol < Grd.Cols.Count; iCol++)
                {
                    if (Grd.Cols[iCol].Visible)
                    {
                        for (int iRow = (Grd.Header.Rows.Count - 1); iRow >= 0; iRow--)
                        {
                            if (Grd.Header.Cells[iRow, iCol].SpanCols > 1)
                            {
                                ColSpanCurrentRow = iRow;
                                ColSpanCount = Grd.Header.Cells[iRow, iCol].SpanCols;
                                if (Grd.Header.Cells[iRow, iCol].Value != null)
                                    ColSpanHeaderTitle = Grd.Header.Cells[iRow, iCol].Value.ToString();
                            }
                            if ((Grd.Header.Cells[iRow, iCol].Value == null) && (ColSpanCount > 0) && (iRow == ColSpanCurrentRow))
                                arr[Grd.Header.Rows.Count - (iRow + 1), Col] = ColSpanHeaderTitle.Replace(Environment.NewLine, " ");
                            else
                                if (Grd.Header.Cells[iRow, iCol].Value != null)
                                arr[Grd.Header.Rows.Count - (iRow + 1), Col] = Grd.Header.Cells[iRow, iCol].Value.ToString().Replace(Environment.NewLine, " ");
                        }
                        Col++;
                    }
                    ColSpanCount--;
                }

                int Row = 0, RowTemp = 0;
                Col = 0;
                // Export the contents of iGrid
                foreach (iGRow row in Grd.Rows)
                {
                    switch (row.Type)
                    {
                        case iGRowType.AutoGroupRow:
                            Row++;
                            arr[Row + AdditionalHeaderRowCount, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.ManualGroupRow:
                            Row++;
                            arr[Row + AdditionalHeaderRowCount, 0] = row.RowTextCell.Value;
                            arrformat[0] = string.Empty;
                            break;
                        case iGRowType.Normal:
                            string FormatType = string.Empty;

                            foreach (iGCell cell in row.Cells)
                            {
                                if (Grd.Rows[cell.RowIndex].VisibleUnderGrouping && Grd.Cols[cell.ColIndex].Visible)
                                {
                                    switch (Grd.Cols[cell.ColIndex].CellStyle.FormatString)
                                    {
                                        case "{0:dd/MMM/yyyy}":
                                            FormatType = "dd/MMM/yyyy";
                                            break;
                                        case "{0:HH:mm}":
                                            FormatType = "HH:mm";
                                            break;
                                        case "{0:##0}":
                                            FormatType = "##0";
                                            break;
                                        case "{0:#,##0}":
                                            FormatType = "#,##0";
                                            break;
                                        case "{0:#,##0.00##}":
                                            FormatType = "#,##0.00##";
                                            break;
                                        case "{0:#,##0.0000}":
                                            FormatType = "#,##0.0000";
                                            break;
                                        case "{0:#,##0.00######}":
                                            FormatType = "#,##0.00######";
                                            break;
                                        case "{0:#,##0.00}":
                                            FormatType = "#,##0.00";
                                            break;
                                        case "{0:#,##0.00###}":
                                            FormatType = "#,##0.00###";
                                            break;
                                        case "{0:#,##0.00####}":
                                            FormatType = "#,##0.00####";
                                            break;
                                        case "{0:#,##0.00#####}":
                                            FormatType = "#,##0.00#####";
                                            break;
                                        default:
                                            FormatType = string.Empty;
                                            break;
                                    }

                                    if (FormatType == string.Empty)
                                    {
                                        if (cell.Value != null && cell.Value.ToString().Trim().Length > 0)
                                        {
                                            if (cell.Value.ToString().ToLower() != "true" && cell.Value.ToString().ToLower() != "false")
                                            {
                                                if (Left(cell.Value.ToString(), 1) != "'")
                                                {
                                                    cell.Value = string.Concat("'", cell.Value);
                                                }
                                            }
                                        }
                                    }

                                    if (row.Index != RowTemp)
                                    {
                                        Row++;
                                        Col = 0;
                                    }
                                    else
                                    {
                                        if (Row == 0)
                                        {
                                            Row = 1;
                                            Col = 0;
                                        }
                                        else
                                            Col++;
                                    }
                                    arr[Row + AdditionalHeaderRowCount, Col] = cell.Value;
                                    arrformat[Col] = FormatType;
                                    RowTemp = row.Index;

                                    if (FormatType == string.Empty)
                                    {
                                        if (cell.Value != null && cell.Value.ToString().Trim().Length > 0)
                                        {
                                            if (cell.Value.ToString().ToLower() != "true" && cell.Value.ToString().ToLower() != "false")
                                            {
                                                if (Left(cell.Value.ToString(), 1) == "'")
                                                {
                                                    cell.Value = Sm.Right(cell.Value.ToString(), cell.Value.ToString().Length - 1); //string.Concat("'", cell.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }

                Excel.Range c1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)objSheet.Cells[3 + Grd.Rows.Count - 1, Grd.Cols.Count];
                Excel.Range range = objSheet.get_Range(c1, c2);

                range.Value2 = arr;


                Excel.Range cHdr1 = (Excel.Range)objSheet.Cells[1, 1];
                Excel.Range cHdr2 = (Excel.Range)objSheet.Cells[Grd.Header.Rows.Count, Grd.Cols.Count];
                Excel.Range rangeHdr = objSheet.get_Range(cHdr1, cHdr2);

                rangeHdr.Font.Bold = true;

                for (int iCol = 0; iCol < arrformat.GetLength(0); iCol++)
                {
                    if ((arrformat[iCol] != null) && (arrformat[iCol].ToString() != string.Empty))
                    {
                        Excel.Range cDet1 = (Excel.Range)objSheet.Cells[2, iCol + 1];
                        //Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count - 1, iCol + 1];
                        Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count, iCol + 1];
                        Excel.Range rangeDet = objSheet.get_Range(cDet1, cDet2);
                        rangeDet.NumberFormat = arrformat[iCol];
                    }
                }

                //Auto Fit
                objSheet.Cells.Select();
                objSheet.Columns.AutoFit();
                objSheet.Rows.AutoFit();

                System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

                //Return control of Excel to the user.
                objApp.Visible = true;
                objApp.UserControl = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;

            }
        }

        public static void FilterDt(ref string SQL, ref MySqlCommand cm, string Filter1, string Filter2, string Column)
        {
            if (!string.IsNullOrEmpty(Filter1) && !string.IsNullOrEmpty(Filter2))
            {
                string Column2 = (Column.IndexOf(".") == -1 ? Column : Sm.Right(Column, Column.Length - 1 - Column.IndexOf(".")));
                SQL = SetFilterDate(SQL, Column, Column2);
                CmParam<String>(ref cm, "@" + Column2 + "1", Filter1);
                CmParam<String>(ref cm, "@" + Column2 + "2", Filter2.Substring(0, 8) + "999999");
            }
        }

        public static void FilterDt(ref string SQL, string Filter1, string Filter2, string Column)
        {
            if (!string.IsNullOrEmpty(Filter1) && !string.IsNullOrEmpty(Filter2))
            {
                string Column2 = (Column.IndexOf(".") == -1 ? Column : Sm.Right(Column, Column.Length - 1 - Column.IndexOf(".")));
                SQL = SetFilterDate(SQL, Column, Column2);
                SQL = SQL.Replace("@" + Column + "1", "'" + Filter1 + "'");
                SQL = SQL.Replace("@" + Column + "2", "'" + Filter2 + "'");

            }
        }

        public static void FilterSetTextEdit(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (FindControl(Frm, "Txt" + ControlName).Text == string.Empty)
                {
                    MessageBox.Show(Warning + " is empty.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((TextEdit)FindControl(Frm, "Txt" + ControlName)).Focus();
                }
            }
            else
            {
                FindControl(Frm, "Txt" + ControlName).Text = string.Empty;
                ((TextEdit)FindControl(Frm, "Txt" + ControlName)).Focus();
            }
        }

        public static void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, bool Fixed)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Index.ToString();
                        CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Index.ToString(),
                            Fixed ? s : "%" + s + "%");
                        //CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
                //SQL = SetFilterString(SQL, Column, Column2);
                //CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2), Fixed ? Filter : "%" + Filter + "%");
            }
        }

        public static void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string ParamName, bool Fixed)
        {
            if (ParamName.Length == 0) ParamName = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + ParamName + Index.ToString();
                        CmParam<String>(ref cm, "@" + ParamName + Index.ToString(), Fixed ? s : "%" + s + "%");
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        public static void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string[] Columns)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string
                    SQL2 = string.Empty, c2 = string.Empty,
                    pattern = @"(""[^""]+""|\w+)\s*";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;
                int NumberOfCharacter = 0;

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                foreach (string c1 in Columns)
                {
                    NumberOfCharacter = Filter.Length - Filter.Replace(@"""", "").Length;

                    c2 = Sm.Right(c1, c1.Length - 1 - c1.IndexOf("."));

                    if (NumberOfCharacter == 1)
                    {
                        group.Clear();
                        group.Add(Filter.Trim());

                        Index = 0;
                        foreach (string s in group)
                        {
                            if (s.Length != 0)
                            {
                                Index += 1;
                                SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + c1 + ") Like @" + @c2 + Index.ToString();
                                CmParam<String>(ref cm, "@" + (c1.IndexOf(".") == -1 ? c1 : c2) + Index.ToString(), "%" + s + "%");
                            }
                        }
                    }
                    else
                    {
                        mc = Regex.Matches(Filter, pattern);

                        group.Clear();

                        foreach (Match m in mc)
                            group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                        Index = 0;
                        foreach (string s in group)
                        {
                            if (s.Length != 0)
                            {
                                Index += 1;
                                SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + c1 + ") Like @" + @c2 + Index.ToString();
                                CmParam<String>(ref cm, "@" + (c1.IndexOf(".") == -1 ? c1 : c2) + Index.ToString(), "%" + s + "%");
                            }
                        }
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        public static void FilterStr(ref string SQL, string Filter, string Column, bool Fixed)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                SQL = SQL + ((SQL.Length == 0) ? " Where " : " And ") + "Upper(" + Column + ") Like '%" + Filter + "%' ";
            }
        }

        public static void FilterStr(ref string SQL, string Filter, string[] Columns)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string SQL2 = string.Empty, c2 = string.Empty;
                foreach (string c1 in Columns)
                {
                    c2 = Sm.Right(c1, c1.Length - 1 - c1.IndexOf("."));
                    SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + c1 + ") Like '%" + Filter + "%' ";
                }
                SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        public static void FilterTxtSetCheckEdit(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Txt = "Txt" + ControlName;
            FindControl(Frm, Txt).Text = FindControl(Frm, Txt).Text.Trim();
            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = !(FindControl(Frm, Txt).Text == string.Empty);
        }

        public static string FormatNum(string Value, byte FormatType)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                if (NumValue < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    NumValue = 0m;
                }
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    //return String.Format("{0:#,##0.00##}", NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 4: return String.Format("{0:#,##0.00##}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        public static string FormatNum(Decimal NumValue, byte FormatType)
        {
            try
            {
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 4: return String.Format("{0:#,##0.00##}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        public static void FormatNumTxt(TextEdit Txt, Byte FormatType)
        {
            Txt.EditValue = Sm.FormatNum(Txt.Text, FormatType);
        }

        public static void FormLoad(Form Frm)
        {
        }

        public static void FormShowDialog(Form Frm)
        {
            Frm.TopLevel = true;
            Frm.ShowDialog();
        }

        public static string GenerateLocalCode(string DocDt, string DocType)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2);

            var SQL = new StringBuilder();

            SQL.Append("Select Concat('" + Yr + Mth + "', ");
            SQL.Append("IfNull(( ");
            SQL.Append("    Select Right(Concat('000', Convert(LocalCode+1, Char)), 3) From ( ");
            SQL.Append("        Select Convert(Substring(LocalCode, 5, 3), Decimal) As LocalCode From TblVoucherRequestPayrollHdr ");
            SQL.Append("        Where Substring(DocDt, 3, 4)='" + Yr + Mth + "' ");
            SQL.Append("        Order By Substring(LocalCode, 5, 3) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001') ");
            SQL.Append(", Floor(Rand()*(100-10)+10), '/', '" + DocType + "' ");
            SQL.Append(") As LocalCode ");

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateJNLocalCode(string DocDt, int Value, string LocalCode)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo");
            var SQL = new StringBuilder();

            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + JournalDocSeqNo + "), Convert(DocNo+" + Value + ", Char)), " + JournalDocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Substring(DocNo, 17, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       And Right(DocNo, 12)= '" + LocalCode + "' ");
            SQL.Append("       And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 6 ");
            SQL.Append("       Order By DocNo Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + JournalDocSeqNo + "), '" + Value + "'), " + JournalDocSeqNo + ")) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "', '/', '" + LocalCode + "' ");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateLocalDocNoSalesMemo(string SalesMemoDocNo, string DocType)
        {
            bool mIsSalesMemoGenerateLocalDocNo;
            mIsSalesMemoGenerateLocalDocNo = Sm.GetParameterBoo("IsSalesMemoGenerateLocalDocNo");

            if (!mIsSalesMemoGenerateLocalDocNo) return string.Empty;
            else
            {
                string
                    DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

                var SQL = new StringBuilder();

                SQL.Append("SELECT REPLACE(LocalDocNo, 'MPJ','" + DocAbbr + "') SalesMemoLocalDocNO ");
                SQL.Append("FROM tblsalesmemohdr  ");
                SQL.Append("WHERE DocNo ='" + SalesMemoDocNo + "' ");

                return Sm.GetValue(SQL.ToString());
            }
        }

        public static string GenerateDocNo(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool
                IsDocNoFormatUseFullYear = Sm.GetParameterBoo("IsDocNoFormatUseFullYear"),
                IsDocNoResetByDocAbbr = Sm.GetParameterBoo("IsDocNoResetByDocAbbr"),
                IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "/%' ");
                //SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5," + DocSeqNo + "), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "/%' ");
                //SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5," + DocSeqNo + "), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateDocNo(string DocDt, string DocType, string Tbl, string Val)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + Val + ", Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+" + Val + ", Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Val + "'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '000" + Val + "') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('00000000', Convert(DocNo+1, Char)), 8) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 8), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 8) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '00000001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateDocNoJournal(string DocDt, int Value)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo");
            var SQL = new StringBuilder();

            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + JournalDocSeqNo + "), Convert(DocNo+" + Value + ", Char)), " + JournalDocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 4 ");
            SQL.Append("       Order By DocNo Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + JournalDocSeqNo + "), '" + Value + "'), " + JournalDocSeqNo + ")) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateDocNoVoucher(string DocDt, string DocType, string Tbl, string AcType, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                type = string.Empty,
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (AcType == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");

                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                //SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                //SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }

            return Sm.GetValue(SQL.ToString());
        }

        public static string GetNewVoucherDocNo(string DocDt, string DocType, string Val)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                YrMth = string.Concat(DocDt.Substring(2, 2), "/", DocDt.Substring(4, 2)),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(A.YrMth, '/', ");
            SQL.AppendLine("    Right(Concat(Repeat('0', " + DocSeqNo + "), IfNull(C.v, D.v)), " + DocSeqNo + "), ");
            SQL.AppendLine("    '/', B.DocAbbr) As DocNo ");
            SQL.AppendLine("    From (Select '" + YrMth + "' As YrMth) A ");
            SQL.AppendLine("    Left Join (Select DocAbbr From TblDocAbbreviation Where DocAbbr Is Not Null And DocType='" + DocType + "') B On 1=1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(No+" + Val + ", Char)), " + DocSeqNo + ") As v ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Convert(Substring(DocNo, 7, " + DocSeqNo + "), Decimal) As No ");
            SQL.AppendLine("            From TblVoucherHdr ");
            SQL.AppendLine("            Where Left(DocNo, 5)='" + YrMth + "' ");
            SQL.AppendLine("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.AppendLine("        ) Temp ");
            SQL.AppendLine("    ) C On 1=1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Val + "'), " + DocSeqNo + ") As v ");
            SQL.AppendLine("    ) D On 1=1 ");
            SQL.AppendLine(") ");

            return SQL.ToString();
        }

        public static string GetNewVoucherRequestDocNo(string DocDt, string DocType, int Val)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                YrMth = string.Concat(DocDt.Substring(2, 2), "/", DocDt.Substring(4, 2)),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(A.YrMth, '/', ");
            SQL.AppendLine("    Right(Concat(Repeat('0', " + DocSeqNo + "), IfNull(C.v, D.v)), " + DocSeqNo + "), ");
            SQL.AppendLine("    '/', B.DocAbbr) As DocNo ");
            SQL.AppendLine("    From (Select '" + YrMth + "' As YrMth) A ");
            SQL.AppendLine("    Left Join (Select DocAbbr From TblDocAbbreviation Where DocAbbr Is Not Null And DocType='" + DocType + "') B On 1=1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(No+" + Val.ToString() + ", Char)), " + DocSeqNo + ") As v ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Convert(Substring(DocNo, 7, " + DocSeqNo + "), Decimal) As No ");
            SQL.AppendLine("            From TblVoucherRequestHdr ");
            SQL.AppendLine("            Where Left(DocNo, 5)='" + YrMth + "' ");
            SQL.AppendLine("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.AppendLine("        ) Temp ");
            SQL.AppendLine("    ) C On 1=1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Val.ToString() + "'), " + DocSeqNo + ") As v ");
            SQL.AppendLine("    ) D On 1=1 ");
            SQL.AppendLine(") ");

            return SQL.ToString();
        }

        public static string GenerateDocNo3(string DocDt, string DocType, string Tbl, string Additional, string DocSeqNo2)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = Sm.GetParameter("DocSeqNo"),
                VoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType"),
                RightLength = (Additional.Length + 6).ToString();

            var SQL = new StringBuilder();

            if (DocType == "Voucher" || DocType == "VoucherRequest")
            {
                if (VoucherCodeFormatType == "2")
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + DocSeqNo2 + ", Char)), " + DocSeqNo + ") From ( ");
                    SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                    SQL.Append("       From " + Tbl + " ");
                    SQL.Append("       Where Right(DocNo, " + RightLength + ")=Concat('" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By DocNo Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', '" + Additional + "', '/', ");
                    SQL.Append("(   Select IfNull( ");
                    SQL.Append("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + DocSeqNo2 + ", Char)), " + DocSeqNo + ") As Numb ");
                    SQL.Append("        From ( ");
                    SQL.Append("            Select Convert(SUBSTRING(DocNo," + (Int32.Parse(RightLength) + 2).ToString() + "," + DocSeqNo + "), Decimal) As DocNo From " + Tbl + " ");
                    SQL.Append("            Where Left(DocNo, " + RightLength + ")= Concat('" + Yr + "','/', '" + Mth + "', '/', '" + Additional + "') ");
                    SQL.Append("            Order By SUBSTRING(DocNo," + (Int32.Parse(RightLength) + 2).ToString() + "," + DocSeqNo + " ) Desc Limit 1 ");
                    SQL.Append("            ) As temp ");
                    SQL.Append("        ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) As Number), '/', '" + DocAbbr + "'");
                    SQL.Append(") As DocNo ");
                }
            }
            else
            {
                if (DocType == "Journal")
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + DocSeqNo2 + ", Char)), " + DocSeqNo + ") From ( ");
                    SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                    SQL.Append("       From " + Tbl + " ");
                    SQL.Append("       Where Right(DocNo, " + RightLength + ")=Concat('" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By DocNo Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + DocSeqNo2 + ", Char)), " + DocSeqNo + ") From ( ");
                    SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                    SQL.Append("       From " + Tbl + " ");
                    SQL.Append("       Where Right(DocNo, " + RightLength + ")=Concat('" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By DocNo Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));


                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Additional + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        public static string GetNewDocNo(string DocDt, string DocType, string Tbl, string Val)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            SQL.Append(" (Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + Val + ", Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+" + Val + ", Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            //SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Val + "'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '000" + Val + "') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static string GetNewVoucherRequestDocNo(string DocDt, string DocType, string Tbl)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                YrMth = string.Concat(DocDt.Substring(2, 2), "/", DocDt.Substring(4, 2)),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(A.YrMth, '/', Right(Concat(Repeat('0', " + DocSeqNo + "), IfNull(C.v, '1')), " + DocSeqNo + "), '/', B.DocAbbr) As DocNo ");
            SQL.AppendLine("    From (Select '" + YrMth + "' As YrMth) A ");
            SQL.AppendLine("    Left Join (Select DocAbbr From TblDocAbbreviation Where DocAbbr Is Not Null And DocType='" + DocType + "') B On 1=1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(Val+1, Char)), " + DocSeqNo + ") As v From (  ");
            SQL.AppendLine("            Select docno, Convert(Substring(DocNo, 7, " + DocSeqNo + "), Decimal) As val ");
            SQL.AppendLine("            From " + Tbl);
            SQL.AppendLine("            Where Left(DocNo, 5)='" + YrMth + "'  ");
            SQL.AppendLine("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("    ) C On 1=1) ");

            return SQL.ToString();
        }


        public static string GetNewJournalDocNo(string DocDt, int Value)
        {
            string
                MthYr = string.Concat(DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo")
                ;
            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat( ");

            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat( ");
            SQL.Append("   Repeat('0', " + JournalDocSeqNo + "), ");
            SQL.Append("   Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("   ), " + JournalDocSeqNo + ") ");
            SQL.Append("   From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNoTemp ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Right(DocNo, 5)='" + MthYr + "'");
            SQL.Append("       And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 4 ");
            SQL.Append("       Order By DocNo Desc ");
            SQL.Append("       Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("   ), ");
            SQL.Append("   Right(Concat( ");
            SQL.Append("        Repeat('0', " + JournalDocSeqNo + "), '" + Value + "'), " + JournalDocSeqNo + ")) ");

            SQL.Append(", '/', '");
            SQL.Append(DocTitle);
            SQL.Append("', '/', '");
            SQL.Append(DocAbbr);
            SQL.Append("', '/', '" + MthYr + "'");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static string GetNewJournalDocNoWithAddCodes(string DocDt, int Value, string Code1, string Code2, string Code3, string Code4, string Code5)
        {
            /*
                default journal docno format for this method : code2/code1/doc_title/doc_abbreviation/year/month/auto_increment
                for now on, code3 til code5 is unused, but prepared first for future usage (05/01/2023 - wed)
             */
            string
                MthYr = string.Concat(DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                YrMth = string.Concat(DocDt.Substring(2, 2), "/", DocDt.Substring(4, 2)),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo")
                ;
            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat('" + Code2 + "', '/', '" + Code1 + "', '/" + DocTitle + "/', '" + DocAbbr + "', '/" + YrMth + "/', ");

            SQL.Append("IfNull(( ");

            SQL.Append("    Select Right(Concat( ");
            SQL.Append("    Repeat('0', " + JournalDocSeqNo + "), ");
            SQL.Append("    Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("    ), " + JournalDocSeqNo + ") ");
            SQL.Append("    From ( ");
            SQL.Append("        SELECT CONVERT( ");
            SQL.Append("            RIGHT(DocNo, " + JournalDocSeqNo + ") ");
            SQL.Append("        , DECIMAL) As DocNoTemp ");
            SQL.Append("        From TblJournalHdr ");
            SQL.Append("        Where RIGHT( ");
            SQL.Append("            SUBSTRING_INDEX(DocNo, '/', 6), ");
            SQL.Append("            LENGTH(SUBSTRING_INDEX(DocNo, '/', 6)) - IFNULL(LENGTH(SUBSTRING_INDEX(DocNo, '/', 4)) + 1, 0) ");
            SQL.Append("        ) = '" + YrMth + "' ");
            SQL.Append("        And SUBSTRING_INDEX(DocNo, '/', 1) = '" + Code2 + "' ");
            SQL.Append("        And RIGHT( ");
            SQL.Append("            SUBSTRING_INDEX(DocNo, '/', 2), ");
            SQL.Append("            LENGTH(SUBSTRING_INDEX(DocNo, '/', 2)) - IFNULL(LENGTH(SUBSTRING_INDEX(DocNo, '/', 1)) + 1, 0) ");
            SQL.Append("        ) = '" + Code1 + "' ");
            SQL.Append("        And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 6 "); // jumlah slash nya ada 6 kalau gak pakai random number
            //SQL.Append("        And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 7 "); // jumlah slash nya ada 7 kalau pakai random number
            SQL.Append("        And Right(DocNo, " + JournalDocSeqNo + ") Not Like '%/%' "); // digit terakhir tidak ada slash nya
            SQL.Append("        Order By DocNo Desc ");
            SQL.Append("        Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("    ), ");

            SQL.Append("    Right(Concat( ");
            SQL.Append("        Repeat('0', " + JournalDocSeqNo + "), " + Value + "), " + JournalDocSeqNo + ")) ");

            //SQL.Append(", '/', '");
            //SQL.Append(DocTitle);
            //SQL.Append("', '/', '");
            //SQL.Append(Code2);
            //SQL.Append("', '/', '");
            //SQL.Append(DocAbbr);
            //SQL.Append("', '/', '");
            //SQL.Append(MthYr);
            //SQL.Append("' ");

            //SQL.Append(", '/', Right(Concat('00', FLOOR(0 + RAND() * (99))), 2) ");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static string GetNewVoucherDocNoWithAddCodes(string DocDt, int Value, string Code1, string Code2, string Code3, string Code4, string Code5)
        {
            /*
                default journal docno format for this method : code2/code1/doc_abbreviation/year/month/auto_increment
                for now on, code3 til code5 is unused, but prepared first for future usage (05/01/2023 - wed)
             */
            string
                MthYr = string.Concat(DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                YrMth = string.Concat(DocDt.Substring(2, 2), "/", DocDt.Substring(4, 2)),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                DocSeqNo = "4"
                ;

            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (DocSeqNo.Length == 0) DocSeqNo = "4";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat('" + Code2 + "', '/', '" + Code1 + "', '/" + DocAbbr + "/', '" + YrMth + "/', ");

            SQL.Append("IfNull(( ");

            SQL.Append("    Select Right(Concat( ");
            SQL.Append("    Repeat('0', " + DocSeqNo + "), ");
            SQL.Append("    Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("    ), " + DocSeqNo + ") ");
            SQL.Append("    From ( ");
            SQL.Append("        SELECT CONVERT( ");
            SQL.Append("            RIGHT(DocNo, " + DocSeqNo + ") ");
            SQL.Append("        , DECIMAL) As DocNoTemp ");
            SQL.Append("        From TblVoucherHdr ");
            SQL.Append("        Where RIGHT( ");
            SQL.Append("            SUBSTRING_INDEX(DocNo, '/', 5), ");
            SQL.Append("            LENGTH(SUBSTRING_INDEX(DocNo, '/', 5)) - IFNULL(LENGTH(SUBSTRING_INDEX(DocNo, '/', 3)) + 1, 0) ");
            SQL.Append("        ) = '" + YrMth + "' ");
            SQL.Append("        And SUBSTRING_INDEX(DocNo, '/', 1) = '" + Code2 + "' ");
            SQL.Append("        And RIGHT( ");
            SQL.Append("            SUBSTRING_INDEX(DocNo, '/', 2), ");
            SQL.Append("            LENGTH(SUBSTRING_INDEX(DocNo, '/', 2)) - IFNULL(LENGTH(SUBSTRING_INDEX(DocNo, '/', 1)) + 1, 0) ");
            SQL.Append("        ) = '" + Code1 + "' ");
            SQL.Append("        And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 5 "); // jumlah slash nya ada 5 kalau gak pakai random number
            //SQL.Append("        And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 6 "); // jumlah slash nya ada 6 kalau pakai random number
            SQL.Append("        And Right(DocNo, " + DocSeqNo + ") Not Like '%/%' "); // digit terakhir tidak ada slash nya
            SQL.Append("        Order By DocNo Desc ");
            SQL.Append("        Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("    ), ");

            SQL.Append("    Right(Concat( ");
            SQL.Append("        Repeat('0', " + DocSeqNo + "), " + Value + "), " + DocSeqNo + ")) ");

            //SQL.Append(", '/', '");
            //SQL.Append(DocTitle);
            //SQL.Append("', '/', '");
            //SQL.Append(Code2);
            //SQL.Append("', '/', '");
            //SQL.Append(DocAbbr);
            //SQL.Append("', '/', '");
            //SQL.Append(MthYr);
            //SQL.Append("' ");

            //SQL.Append(", '/', Right(Concat('00', FLOOR(0 + RAND() * (99))), 2) ");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static string GetCode1ForJournalDocNo(string Param, string BankAccountType, string AcType, string JournalDocNoFormat)
        {
            if (JournalDocNoFormat == "1") return string.Empty;

            if (Param.ToLower() != "frmvoucher") return "M";

            string a = "B";
            string[] bankAcTpList = { "bank", "umk", "cash card", "kredit investasi bank" };
            bool flag = true;
            foreach (var x in bankAcTpList)
            {
                if (BankAccountType.ToLower() == x)
                {
                    flag = false;
                    break;
                }
            }
            if (flag) a = "K";
            //if (BankAccountType.ToLower() != "bank") a = "K";

            string b = "M";
            if (AcType != "D") b = "K";

            return a + b;
        }

        public static string GetNewJournalDocNo3(string DocDt, string Additional, string Value)
        {
            string
                DocTitle = Sm.GetParameter("DocTitle"),
                RightLength = (Additional.Length + 6).ToString(),
                MthYr = string.Concat(Additional, "/", DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                DocSeqNo = Sm.GetParameter("DocSeqNo");
            if (DocSeqNo.Length == 0) DocSeqNo = "6";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat( ");

            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat( ");
            SQL.Append("   Repeat('0', " + DocSeqNo + "), ");
            SQL.Append("   Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("   ), " + DocSeqNo + ") ");
            SQL.Append("   From ( ");
            SQL.Append("       Select Convert(Left(DocNo, " + DocSeqNo + "), Decimal) As DocNoTemp ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Right(DocNo, " + RightLength + ")='" + MthYr + "'");
            SQL.Append("       And Substr(DocNo, " + (Int32.Parse(DocSeqNo) + 1).ToString() + ", 1 ) = '/' ");
            SQL.Append("       And Length(DocNo) - Length(Replace(DocNo, '/', '')) = 5 ");
            SQL.Append("       Order By DocNo Desc ");
            SQL.Append("       Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("   ), ");
            SQL.Append("   Right(Concat( ");
            SQL.Append("        Repeat('0', " + DocSeqNo + "), '" + Value + "'), " + DocSeqNo + ")) ");

            SQL.Append(", '/', '");
            SQL.Append(DocTitle);
            SQL.Append("', '/', '");
            SQL.Append(DocAbbr);
            SQL.Append("', '/', '" + MthYr + "'");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static int[] GetOrdinal(MySqlDataReader dr, string[] s)
        {
            var c = new int[s.GetLength(0)];
            var i = 0;
            foreach (string Index in s)
                c[i++] = dr.GetOrdinal(Index);
            return c;
        }

        public static string GetParameter(string ParCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select ParValue From TblParameter Where ParCode=@ParCode;"
            };
            Sm.CmParam<String>(ref cm, "@ParCode", ParCode.Trim());
            return Sm.GetValue(cm);
        }

        public static bool GetParameterBoo(string ParCode)
        {
            return GetValue(
                "Select ParValue From TblParameter Where ParCode=@Param;",
                ParCode.Trim()) == "Y";
        }

        public static decimal GetParameterDec(string ParCode)
        {
            object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.CommandText = "Select Cast(IfNull(ParValue, '0') As Decimal(12, 4)) From TblParameter Where ParCode=@ParCode;";
                Sm.CmParam<string>(ref cm, "@ParCode", ParCode.Trim());
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? 0m : Convert.ToDecimal(Value));
        }

        public static string GetTme(TimeEdit Tme)
        {
            return (Tme.EditValue == null || Tme.EditValue.ToString() == string.Empty) ? "" : Tme.Text.Replace(":", string.Empty); //Pm.GenerateTimeFormat(Tme.Time);
        }

        public static void SetTme(TimeEdit Tme, string Value)
        {
            if (Value.Length == 0)
                Tme.EditValue = null;
            else
                Tme.EditValue = FormatTime(Value);
        }

        public static string GetValue(string SQL)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? "" : Value.ToString());
        }

        public static string GetValue(MySqlCommand cm)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? string.Empty : Value.ToString());
        }

        public static string GetValue(string SQL, string Param)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param", Param);
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? string.Empty : Value.ToString());
        }

        public static string GetValue(string SQL, string Param1, string Param2, string Param3)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param1", Param1);
                Sm.CmParam<String>(ref cm, "@Param2", Param2);
                Sm.CmParam<String>(ref cm, "@Param3", Param3);
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? string.Empty : Value.ToString());
        }

        public static decimal GetValueDec(MySqlCommand cm)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            if (Value == null)
                return 0m;
            else
                return (decimal)Value;
        }

        public static decimal GetValueDec(string SQL, string Param)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param", Param);
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            if (Value == null)
                return 0m;
            else
                return (decimal)Value;
        }

        public static decimal GetValueDec(string SQL, string Param1, string Param2)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param1", Param1);
                Sm.CmParam<String>(ref cm, "@Param2", Param2);
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            if (Value == null)
                return 0m;
            else
                return (decimal)Value;
        }

        public static decimal GetValueDec(string SQL, string Param1, string Param2, string Param3, string Param4)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param1", Param1);
                Sm.CmParam<String>(ref cm, "@Param2", Param2);
                Sm.CmParam<String>(ref cm, "@Param3", Param3);
                Sm.CmParam<String>(ref cm, "@Param4", Param4);
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            if (Value == null)
                return 0m;
            else
                return (decimal)Value;
        }

        public static string GetHashSha256(string text)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(text));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public static string GetHashBase64(string text)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(text);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        public static void GrdEnter(iGrid Grd, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && Grd.CurRow.Index != Grd.Rows.Count - 1)
                Sm.FocusGrd(Grd, Grd.CurRow.Index + 1, Grd.CurCell.Col.Index);
        }

        public static void GrdExpand(iGrid Grd)
        {
            Grd.Rows.ExpandAll();
            if (Grd.GroupObject.Count > 0)
            {
                for (int row = 0; row <= Grd.Rows.Count - 1; row++)
                    if (Grd.Rows[row].Tag as string != "Subtotal" &&
                        Grd.Rows[row].Level >= Grd.GroupObject.Count - 1)
                        Grd.Rows[row].Expanded = false;
            }
        }

        public static void GrdKeyDown(iGrid Grd, KeyEventArgs e, SimpleButton Btn1, SimpleButton Btn2)
        {
            if (e.KeyCode == Keys.Enter && Grd.CurRow.Index != Grd.Rows.Count - 1)
                Sm.FocusGrd(Grd, Grd.CurRow.Index + 1, Grd.CurCell.Col.Index);

            if (e.KeyCode == Keys.Tab && Grd.CurCell != null && Grd.CurCell.RowIndex == Grd.Rows.Count - 1)
            {
                int LastVisibleCol = Grd.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd.Cols.Count - 1; Col++)
                    if (Grd.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd.CurCell.Col.Order == LastVisibleCol)
                {
                    if (Btn1.Enabled)
                        Btn1.Focus();
                    else
                        Btn2.Focus();
                }
            }
        }

        public static void GrdTabInLastCell(iGrid Grd, KeyEventArgs e, SimpleButton Btn)
        {
            if (e.KeyCode == Keys.Tab && Grd.CurCell != null && Grd.CurCell.RowIndex == Grd.Rows.Count - 1)
            {
                int LastVisibleCol = Grd.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd.Cols.Count - 1; Col++)
                {
                    if (Grd.Cols[Col].Visible) LastVisibleCol = Col;
                }
                if (Grd.CurCell.Col.Order == LastVisibleCol) Btn.Focus();
            }
        }

        public static void GrdTabInLastCell(iGrid Grd, KeyEventArgs e, SimpleButton Btn1, SimpleButton Btn2)
        {
            if (e.KeyCode == Keys.Tab && Grd.CurCell != null && Grd.CurCell.RowIndex == Grd.Rows.Count - 1)
            {
                int LastVisibleCol = Grd.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd.Cols.Count - 1; Col++)
                {
                    if (Grd.Cols[Col].Visible) LastVisibleCol = Col;
                }
                if (Grd.CurCell.Col.Order == LastVisibleCol)
                {
                    if (Btn1.Enabled)
                        Btn1.Focus();
                    else
                        Btn2.Focus();
                }
            }
        }

        public static void GrdRemoveRow(iGrid Grd, KeyEventArgs e, SimpleButton Btn)
        {
            if (Btn.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd.SelectedRows.Count > 0)
                {
                    if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
                            if (Grd.Rows.Count <= 0) Grd.Rows.Add();
                        }
                    }
                }
            }
        }

        public static void GrdFormatDec(iGrid Grd, int[] ColIndex, byte FormatType)
        {
            string FormatDec = string.Empty;
            switch (FormatType)
            {
                case 0:
                    FormatDec =
                        (Gv.FormatNum0.Length != 0) ?
                        Gv.FormatNum0 : "{0:#,##0.00##}";
                    //FormatDec = "{0:#,##0.00##}";
                    break;
                case 1:
                    FormatDec = "{0:#,##0}";
                    break;
                case 2:
                    FormatDec = "{0:#,##0.00}";
                    break;
                case 3:
                    FormatDec = "{0:#,##0.00#}";
                    break;
                case 4:
                    FormatDec = "{0:#,##0.00##}";
                    break;
                case 6:
                    FormatDec = "{0:#,##0.00####}";
                    break;
                case 8:
                    FormatDec = "{0:#,##0.00######}";
                    break;
                default:
                    FormatDec = "{0:###0}";
                    break;
            }

            for (int Col = 0; Col < ColIndex.Length; Col++)
            {
                Grd.Cols[ColIndex[Col]].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd.Cols[ColIndex[Col]].CellStyle.FormatString = FormatDec;
                Grd.Cols[ColIndex[Col]].CellStyle.ValueType = typeof(decimal);
            }
        }

        public static void GrdRequestEdit(iGrid Grd, int RowIndex)
        {
            if ((Grd.Rows.Count == 1) || (Grd.Rows.Count != 1 && RowIndex == Grd.Rows.Count - 1)) Grd.Rows.Add();
        }

        public static void GrdAfterCommitEditSetZeroIfEmpty(iGrid Grd, int[] ColIndex, iGAfterCommitEditEventArgs e)
        {
            if (IsGrdColSelected(ColIndex, e.ColIndex))
            {
                if (GetGrdStr(Grd, e.RowIndex, e.ColIndex).Length == 0)
                    Grd.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                else
                {
                    if (Sm.GetGrdDec(Grd, e.RowIndex, e.ColIndex) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Value should not less than 0.");
                        Grd.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                    }
                }
            }
        }

        public static void GrdAfterCommitEditTrimString(iGrid Grd, int[] ColIndex, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(ColIndex, e.ColIndex) && Sm.GetGrdStr(Grd, e.RowIndex, e.ColIndex).Length != 0)
                Grd.Cells[e.RowIndex, e.ColIndex].Value = Sm.GetGrdStr(Grd, e.RowIndex, e.ColIndex).Trim();
        }

        public static int GrdColMove(int CurCol, int SrcOrder, int DstOrder)
        {

            if (CurCol >= 0)
            {
                if ((((CurCol + 1) >= SrcOrder) && ((CurCol + 1) <= DstOrder)) ||
                (((CurCol + 1) >= DstOrder) && ((CurCol + 1) <= SrcOrder)))
                {
                    if (SrcOrder == (CurCol + 1))
                        CurCol = DstOrder - 1;
                    else
                        if (SrcOrder <= (CurCol + 1)) CurCol--;
                    else
                            if (DstOrder <= (CurCol + 1)) CurCol++;
                }
            }
            return CurCol;
        }

        public static bool IsClosingProcurementInvalidDefault(string DocDt)
        {
            string FiscalYrRange = GetParameter("FiscalYearRange");
            if (FiscalYrRange.Length > 0)
            {
                if (DocDt.Length > 8) DocDt = Sm.Left(DocDt, 8);
                string[] explodes = FiscalYrRange.Split(',');
                string selectedPeriod = string.Empty;

                if (explodes.Count() > 0)
                {
                    string testPeriod = string.Concat(Sm.Left(DocDt, 4), explodes[0]);

                    if (Int32.Parse(testPeriod) > Int32.Parse(Sm.Left(DocDt, 6))) selectedPeriod = (Int32.Parse(Sm.Left(DocDt, 4)) - 1).ToString();

                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblClosingProcurement ");
                    SQL.AppendLine("Where CancelInd = 'N' And `Year` = @Param ");
                    SQL.AppendLine("Limit 1; ");

                    if (IsDataExist(SQL.ToString(), selectedPeriod))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid period.\nYou need to input this documet after the period closing procurement.");

                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsClosingProcurementInvalid(bool IsClosingProcurementUseMRType, string MRType, string DocDt)
        {
            string FiscalYrRange = GetParameter("FiscalYearRange");
            if (IsClosingProcurementUseMRType && MRType == "2" && DocDt.Length > 0 && FiscalYrRange.Length > 0)
            {
                if (DocDt.Length > 8) DocDt = Sm.Left(DocDt, 8);
                string[] explodes = FiscalYrRange.Split(',');
                string selectedPeriod = string.Empty;

                if (explodes.Count() > 0)
                {
                    //FiscalYrRange1 = 
                    string testPeriod = string.Concat(Sm.Left(DocDt, 4), explodes[0]);

                    if (Int32.Parse(testPeriod) > Int32.Parse(Sm.Left(DocDt, 6))) selectedPeriod = (Int32.Parse(Sm.Left(DocDt, 4)) - 1).ToString();

                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblClosingProcurement ");
                    SQL.AppendLine("Where CancelInd = 'N' And MRType = '2' And `Year` = @Param ");
                    SQL.AppendLine("Limit 1; ");

                    if (IsDataExist(SQL.ToString(), selectedPeriod))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid period.\nYou need to input this documet after the period closing procurement.");

                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsClosingJournalInvalid(string Dt)
        {
            var SQL = new StringBuilder();
            var msg = new StringBuilder();

            SQL.AppendLine("/*StdMtd - IsClosingJournalInvalid*/ ");
            SQL.AppendLine("Select 1 From TblClosingJournal ");
            SQL.AppendLine("Where CancelInd='N' And ClosingDt>=@Param ");
            SQL.AppendLine("And ProfitCenterCode Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblParameter Where ParCode='IsClosingJournalActived' And ParValue='Y') ");
            SQL.AppendLine("Limit 1; ");

            msg.AppendLine("Invalid document date. ");
            msg.AppendLine("You need to input this document after the monthly accounting journal entries's Closing date. ");

            return IsDataExist(SQL.ToString(), Left(Dt, 8), msg.ToString());
        }

        public static bool IsClosingJournalInvalid(bool IsAutoJournalActived, bool IsCancel, string Dt, string ProfitCenterCode)
        {
            if (!IsAutoJournalActived) return false;
            var ClosingJournalProfitCenterLevelToBeValidated = GetParameter("ClosingJournalProfitCenterLevelToBeValidated");
            bool mIsClosingJournalDisableFicoTransactionDocumentEdit = GetParameterBoo("IsClosingJournalDisableFicoTransactionDocumentEdit");


            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Val :=(Select Length(ProfitCenterCode) From TblProfitCenter Where Level=@Param3 Limit 1); ");

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblClosingJournal A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And @Param1<=A.ClosingDt ");
            SQL.AppendLine("And @Param2 In (Select ProfitCenterCode From TblProfitCenter Where Level>=@Param3) ");
            SQL.AppendLine("And A.ProfitCenterCode=Left(Case When Length(@Param2)>=@Val Then @Param2 Else Left(Concat(@Param2, '**********'), @Val) End, @Val) ");
            SQL.AppendLine("And Exists(Select 1 From TblParameter Where ParCode = 'IsClosingJournalActived' And ParValue='Y') ");
            SQL.AppendLine("Limit 1;");

            string ProfitCenterName = Sm.GetValue("Select ProfitCenterName From TblProfitCenter Where ProfitCenterCode = @Param; ", ProfitCenterCode);
            if (IsDataExist(SQL.ToString(), Left(Dt, 8), ProfitCenterCode, ClosingJournalProfitCenterLevelToBeValidated))
            {
                if (!IsCancel)
                {
                    Sm.StdMsg(mMsgType.Info,
                        "Monthly closing (" + ProfitCenterName + ") has been processed already."
                        );
                    return true;
                }
                else
                {
                    if (!mIsClosingJournalDisableFicoTransactionDocumentEdit)
                    {
                        Sm.StdMsg(mMsgType.Info,
                            "Monthly closing (" + ProfitCenterName + ") has been processed already."
                                + Environment.NewLine +
                                ("Your cancelled transaction will be processed in today's journal.")
                            );
                        return false;
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Info,
                       "Monthly closing (" + ProfitCenterName + ") has been processed already."
                       );
                        return true;
                    }

                }
            }
            else
                return false;
        }


        public static bool IsClosingJournalInvalid(bool IsAutoJournalActived, bool IsCancel, string Dt)
        {
            if (!IsAutoJournalActived) return false;
            if (IsClosingJournalUseCurrentDt(Dt))
                Sm.StdMsg(mMsgType.Info,
                    "Monthly closing already been done." + Environment.NewLine +
                    (IsCancel ? "Your cancel transaction will be processed in today's journal." : "Your transaction will be processed in today's journal.")
                    );
            return false;
        }

        public static bool IsClosingJournalUseCurrentDt(string Dt)
        {
            return IsDataExist(
                "Select 1 From TblClosingJournal " +
                "Where CancelInd='N' And ClosingDt>=@Param " +
                "And Exists(Select 1 From TblParameter Where ParCode='IsClosingJournalActived' And ParValue='Y') " +
                "Limit 1;",
                Left(Dt, 8));
        }

        public static bool IsClosingJournalUseCurrentDt(string Dt, string ProfitCenterCode)
        {
            var ClosingJournalProfitCenterLevelToBeValidated = GetParameter("ClosingJournalProfitCenterLevelToBeValidated");

            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Val :=(Select Length(ProfitCenterCode) From TblProfitCenter Where Level=@Param3 Limit 1); ");

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblClosingJournal A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And @Param1<=A.ClosingDt ");
            SQL.AppendLine("And @Param2 In (Select ProfitCenterCode From TblProfitCenter Where Level>=@Param3) ");
            SQL.AppendLine("And A.ProfitCenterCode=Left(Case When Length(@Param2)>=@Val Then @Param2 Else Left(Concat(@Param2, '**********'), @Val) End, @Val) ");
            SQL.AppendLine("And Exists(Select 1 From TblParameter Where ParCode = 'IsClosingJournalActived' And ParValue='Y') ");
            SQL.AppendLine("Limit 1;");

            return (IsDataExist(SQL.ToString(), Left(Dt, 8), ProfitCenterCode, ClosingJournalProfitCenterLevelToBeValidated));
        }


        public static bool IsDataExist(MySqlCommand cm)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                return !(cm.ExecuteScalar() == null);
            }
        }

        public static bool IsDataExist(MySqlCommand cm, string Warning)
        {
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        public static bool IsDataExist(string SQL)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var result = cm.ExecuteScalar();
                cn.Close();
                return !(result == null);
            }
        }

        public static bool IsDataExist(string SQL, string Param)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            return Sm.IsDataExist(cm);
        }

        public static bool IsDataExist(string SQL, string Param1, string Param2, string Param3)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param1", Param1);
            Sm.CmParam<String>(ref cm, "@Param2", Param2);
            Sm.CmParam<String>(ref cm, "@Param3", Param3);
            return Sm.IsDataExist(cm);
        }

        public static bool IsDataExist(string SQL, string Param1, string Param2, string Param3, string Param4)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param1", Param1);
            Sm.CmParam<String>(ref cm, "@Param2", Param2);
            Sm.CmParam<String>(ref cm, "@Param3", Param3);
            Sm.CmParam<String>(ref cm, "@Param4", Param4);
            return Sm.IsDataExist(cm);
        }

        public static bool IsDataExist(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        public static bool IsDataNotValid(string SQL, string Param1, ref string Param2, string Msg)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param1);
            Param2 = Sm.GetValue(cm);
            if (Param2.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        public static bool IsDocApprovalByDept(string DocType)
        {
            return IsDataExist("Select 1 from TblDocApprovalSetting Where DocType=@Param And DeptCode Is Not Null Limit 1;", DocType);
        }

        public static bool IsDocApprovalBySite(string DocType)
        {
            return IsDataExist("Select 1 from TblDocApprovalSetting Where DocType=@Param And SiteCode Is Not Null Limit 1;", DocType);
        }


        public static bool IsDocDtNotValid(bool mIsShowDocDtWarning, string DocDt)
        {
            //customized for ASA
            if (mIsShowDocDtWarning)
            {
                DocDt = DocDt.Substring(6, 2) + "-" + Sm.MonthName(DocDt.Substring(4, 2)) + "-" + Sm.Left(DocDt, 4);
                if (Sm.StdMsgYN("Question",
                    "Document Date : " + DocDt + Environment.NewLine +
                    "Is document date valid ?") == DialogResult.No)
                    return true;
            }
            return false;
        }

        public static bool IsFilterByDtNotValid(bool IsUseFilterByDate, string Dt1, string Dt2, string Warning)
        {
            var DocDt1 = new DateTime(int.Parse(Sm.Left(Dt1, 4)), int.Parse(Dt1.Substring(4, 2)), int.Parse(Dt1.Substring(6, 2)));
            var DocDt2 = new DateTime(int.Parse(Sm.Left(Dt2, 4)), int.Parse(Dt2.Substring(4, 2)), int.Parse(Dt2.Substring(6, 2)));

            if (IsUseFilterByDate && (DocDt2.Subtract(DocDt1)).Days > 31)
            {
                Sm.StdMsg(mMsgType.Warning, (Warning.Length > 0) ? Warning : "You can't filter by date more than 1 month.");
                return true;
            }
            return false;
        }

        public static bool IsFilterByDateInvalid(ref DateEdit Dte1, ref DateEdit Dte2)
        {
            if (Sm.CompareDtTm(Sm.GetDte(Dte1), Sm.GetDte(Dte2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        public static bool IsTxtEmpty(TextEdit Txt, string Message, bool IsNumeric)
        {
            bool Result = true;
            if (Txt.EditValue != null && Txt.Text.Length != 0)
            {
                if (IsNumeric)
                {
                    if (decimal.Parse(Txt.Text) != 0)
                        Result = false;
                    else
                    {
                        StdMsg(mMsgType.Warning, Message + " is zero.");
                        Txt.Focus();
                    }
                }
                else
                    Result = false;
            }
            else
            {
                if (Message.Length == 0)
                    StdMsg(mMsgType.NoData, "");
                else
                    StdMsg(mMsgType.Warning, Message + " is empty.");
                Txt.Focus();
            }
            return Result;
        }

        public static bool IsGrdValueEmpty(iGrid Grd, int Row, int Col, bool IsNumeric, string Message)
        {
            bool Result = false;
            if (IsNumeric)
            {
                if ((Sm.GetGrdStr(Grd, Row, Col).Length == 0) ||
                        (Sm.GetGrdStr(Grd, Row, Col).Length != 0 &&
                            decimal.Parse(Sm.GetGrdStr(Grd, Row, Col)) == 0m))
                {
                    Sm.StdMsg(mMsgType.Warning, Message);
                    Sm.FocusGrd(Grd, Row, Col);
                    Result = true;
                }
            }
            else
            {
                if (Sm.GetGrdStr(Grd, Row, Col).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Message);
                    Sm.FocusGrd(Grd, Row, Col);
                    Result = true;
                }
            }
            return Result;
        }

        public static void MeeCancelReasonValidated(MemoExEdit Mee, CheckEdit Chk)
        {
            Sm.MeeTrim(Mee);
            Chk.Checked = (Mee.Text.Length > 0);
        }

        public static void MeeTrim(MemoExEdit Mee)
        {
            Mee.Text = Mee.Text.Trim();
        }

        public static string MonthName(string Mth)
        {
            switch (Mth)
            {
                case "01":
                    return "January";
                case "02":
                    return "February";
                case "03":
                    return "March";
                case "04":
                    return "April";
                case "05":
                    return "May";
                case "06":
                    return "June";
                case "07":
                    return "July";
                case "08":
                    return "August";
                case "09":
                    return "September";
                case "10":
                    return "October";
                case "11":
                    return "November";
                case "12":
                    return "December";
            }
            return "";
        }

        public static string MonthRoman(string Mth)
        {
            switch (Mth)
            {
                case "01":
                    return "I";
                case "02":
                    return "II";
                case "03":
                    return "III";
                case "04":
                    return "IV";
                case "05":
                    return "V";
                case "06":
                    return "VI";
                case "07":
                    return "VII";
                case "08":
                    return "VIII";
                case "09":
                    return "IX";
                case "10":
                    return "X";
                case "11":
                    return "XI";
                case "12":
                    return "XII";
            }
            return "";
        }

        public static void PrintReport(string ReportName, List<IList> TableList, string[] TableName, bool DesignInd)
        {
            try
            {
                var report = new Report();
                report.Load(Application.StartupPath + @"\\Report\\rep" + ReportName + ".frx");

                for (int intX = 0; intX < TableList.Count; intX++)
                    report.RegisterData(TableList[intX], TableName[intX]);

                report.UseFileCache = true;
                if (DesignInd)
                    report.Design();
                else
                {
                    report.Show();
                    report.Dispose();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static string ReadSetting(string key)
        {
            //read setting from app.config
            return ConfigurationManager.AppSettings[key];
        }

        public static string RepeatString(string value, int count)
        {
            return new StringBuilder(value.Length * count).Insert(0, value, count).ToString();
        }

        public static void SetCcb(ref CheckedComboBoxEdit Ccb, MySqlCommand cm)
        {
            Ccb.DataBindings.Clear();
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (Gv.IsUsePreparedStatement) cm.Prepare();
                    var dr = cm.ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col") };
                    while (dr.Read())
                        Ccb.Properties.Items.Add(Sm.DrStr(dr, c[0]), CheckState.Unchecked, true);
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Ccb.Properties.DisplayMember = "Col";
                Ccb.Properties.ValueMember = "Col";
            }
        }

        public static string SetFormForGroupUser(string MenuCode)
        {
            string GrpCode = string.Empty;
            try
            {
                GrpCode = Sm.GetValue(
                        "Select A.GrpCode From TblGroupMenu A " +
                        "Inner Join TblGroup B On A.GrpCode=B.GrpCode " +
                        "Inner Join TblUser C On B.GrpCode=C.GrpCode " +
                        "Where C.UserCode='" + Gv.CurrentUserCode + "' And A.MenuCode='" + MenuCode + "' Limit 1"
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return GrpCode;
        }

        public static string SetFormAccessInd(string MenuCode)
        {
            string AccessInd = string.Empty;
            try
            {
                if (MenuCode.Length >= 2 && Sm.CompareStr(Sm.Left(MenuCode, 2), "RS"))
                    AccessInd = Sm.Right(MenuCode, MenuCode.Length - 2);
                else
                    AccessInd = Sm.GetValue(
                            "Select IfNull(AccessInd, '') As AccessInd From (" +
                            "   Select A.UserCode, C.AccessInd, D.MenuCode " +
                            "   From TblUser A " +
                            "   Inner Join TblGroup B On A.GrpCode=B.GrpCode " +
                            "   Inner Join TblGroupMenu C On B.GrpCode=C.GrpCode " +
                            "   Inner Join TblMenu D On C.MenuCode=D.MenuCode" +
                            ") T Where UserCode='" + Gv.CurrentUserCode + "' And MenuCode='" + MenuCode + "' Limit 1"
                        );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return AccessInd;
        }

        public static void SetGrdAutoSize(iGrid Grd)
        {
            Grd.Cols.AutoWidth();
            Grd.Rows.AutoHeight();
        }

        public static void SetGrdBoolValueFalse(ref iGrid Grd, int Row, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cells[Row, ColIndex[Col]].Value = false;
        }

        public static void SetGrdBoolValueFalse(iGrid Grd, int Row, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cells[Row, ColIndex[Col]].Value = false;
        }

        public static void SetGrdNumValueZero(ref iGrid Grd, int Row, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cells[Row, ColIndex[Col]].Value = 0m;
        }

        public static void SetGrdNumValueZero(iGrid Grd, int Row, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cells[Row, ColIndex[Col]].Value = 0m;
        }

        public static void SetGrdStringValueEmpty(iGrid Grd, int r, int[] ColIndex)
        {
            for (int c = 0; c < ColIndex.Length; c++)
                Grd.Cells[r, ColIndex[c]].Value = null;
        }

        public static void SetGrdValue(string Type, iGrid Grd, MySqlDataReader dr, int[] c, int Row, int Col, int i)
        {
            switch (Type)
            {
                case "S":
                    Grd.Cells[Row, Col].Value = (dr[c[i]] == DBNull.Value) ? "" : dr.GetString(c[i]);
                    break;
                case "N":
                    Grd.Cells[Row, Col].Value = (dr[c[i]] == DBNull.Value) ? 0m : dr.GetDecimal(c[i]);
                    break;
                case "D":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.ConvertDate(dr.GetString(c[i]));
                    break;
                case "T":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.ConvertDateTime(dr.GetString(c[i]));
                    break;
                case "T2":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.Left(dr.GetString(c[i]), 2) + ":" + dr.GetString(c[i]).Substring(2, 2); // Sm.Right(dr.GetString(c[i]), 2);
                    break;
                case "T3":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.Left(dr.GetString(c[i]), 2) + ":" + dr.GetString(c[i]).Substring(2, 2) + ":" + Sm.Right(dr.GetString(c[i]), 2);
                    break;
                case "B":
                    Grd.Cells[Row, Col].Value =
                        (dr[c[i]] == DBNull.Value) ?
                            false :
                            (string.Compare(dr.GetString(c[i]), "Y") == 0);
                    break;
                case "I":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = 0;
                    else
                    {
                        try { Grd.Cells[Row, Col].Value = dr.GetInt16(c[i]); }
                        catch { Grd.Cells[Row, Col].Value = dr.GetInt32(c[i]); }
                    }
                    break;
            }
        }

        public static void SetGrdVal(string Type, iGrid Grd, MySqlDataReader dr, int[] c, int Row, int Col, int i)
        {
            switch (Type)
            {
                case "S":
                    Grd.Cells[Row, Col].Value = dr.GetString(c[i]);
                    break;
                case "N":
                    Grd.Cells[Row, Col].Value = dr.GetDecimal(c[i]);
                    break;
                case "D":
                    Grd.Cells[Row, Col].Value = Sm.ConvertDate(dr.GetString(c[i]));
                    break;
                case "T":
                    Grd.Cells[Row, Col].Value = Sm.ConvertDateTime(dr.GetString(c[i]));
                    break;
                case "T2":
                    Grd.Cells[Row, Col].Value = Sm.Left(dr.GetString(c[i]), 2) + ":" + Sm.Right(dr.GetString(c[i]), 2);
                    break;
                case "B":
                    Grd.Cells[Row, Col].Value = (string.Compare(dr.GetString(c[i]), "Y") == 0);
                    break;
                case "I":
                    try { Grd.Cells[Row, Col].Value = dr.GetInt16(c[i]); }
                    catch { Grd.Cells[Row, Col].Value = dr.GetInt32(c[i]); }
                    break;
            }
        }

        public static void SetGrdValueNull(ref iGrid Grd, int Row, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cells[Row, ColIndex[Col]].Value = null;
        }

        public static void SetGridNo(iGrid Grd, int Rows, int Cols, bool FocusInd)
        {
            int No = 1;
            Grd.BeginUpdate();
            try
            {
                for (int Row = 1; Row <= Grd.Rows.Count; Row++)
                {
                    if (Grd.Rows[Row - 1].Type == iGRowType.Normal &&
                        !iGSubtotalManager.IsSubtotal(Grd.Rows[Row - 1]))
                        Grd.Cells[Row - 1, 0].Value = No++;
                    else
                        No = 1;
                }
                ShowHrdCountAfterContent(ref Grd);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd.EndUpdate();
                if (FocusInd) Sm.FocusGrd(Grd, Rows, Cols);
            }
        }

        public static void SetImg(ref ImageEdit Img, string FileName, string FileLocation)
        {
            try
            {
                Img.Image = (FileName.Length == 0) ? null : Image.FromFile(@FileLocation + FileName + ".png");
            }
            catch (System.IO.FileNotFoundException)
            {
                Img.Image = null;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        public static void SetPeriod(ref DateEdit Dte1, ref DateEdit Dte2, ref CheckEdit Chk, int Days)
        {
            string CurrentDate = Sm.ServerCurrentDateTime();
            if (CurrentDate.Length == 0)
            {
                Dte1.EditValue = null;
                Dte2.EditValue = null;
                Chk.Checked = false;
            }
            else
            {
                if (Days == 0) Days = -7;
                Dte1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(Days);
                Dte2.DateTime = Sm.ConvertDate(CurrentDate);
                Chk.Checked = true;
            }
        }

        public static void SetPrintManagerProperty(iGPrintManager PM, iGrid Grd, bool AutoWidth)
        {
            try
            {
                PM.Document.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(78, 39, 125, 78);
                PM.PageFooter.LeftSection.Text = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));
                PM.PageFooter.RightSection.Text = "Page: %[PageNumber] of %[PageCount]";
                PM.PrintingStyle = iGPrintingStyle.PlainView;
                PM.FitColWidths = AutoWidth;
                PM.PrintPreview(Grd);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void SetLue1(ref LookUpEdit Lue, string SQL, string ColTitle)
        {
            var ListOfObject = new List<Lue1>()
                {
                    new Lue1{Col1 = null},
                    new Lue1{Col1 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue1 { Col1 = Sm.DrStr(dr, c[0]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle;
                Lue.Properties.DisplayMember = "Col1";
                Lue.Properties.ValueMember = "Col1";
            }
        }

        public static void SetLue1(ref LookUpEdit Lue, ref MySqlCommand cm, string ColTitle)
        {
            var ListOfObject = new List<Lue1>()
                {
                    new Lue1{Col1 = null},
                    new Lue1{Col1 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (Gv.IsUsePreparedStatement) cm.Prepare();
                    var dr = cm.ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue1 { Col1 = Sm.DrStr(dr, c[0]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle;
                Lue.Properties.DisplayMember = "Col1";
                Lue.Properties.ValueMember = "Col1";
            }
        }

        public static void SetLue2(
                ref LookUpEdit Lue, string SQL,
                int ColWidth1, int ColWidth2,
                bool ColVisible1, bool ColVisible2,
                string ColTitle1, string ColTitle2,
                string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue2>()
                {
                    new Lue2{Col1 = null, Col2 = null},
                    new Lue2{Col1 = "<Refresh>", Col2 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue2 { Col1 = Sm.DrStr(dr, c[0]), Col2 = Sm.DrStr(dr, c[1]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }

        public static void SetLue2(
                ref LookUpEdit Lue, ref MySqlCommand cm,
                int ColWidth1, int ColWidth2,
                bool ColVisible1, bool ColVisible2,
                string ColTitle1, string ColTitle2,
                string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue2>()
                {
                    new Lue2{Col1 = null, Col2 = null},
                    new Lue2{Col1 = "<Refresh>", Col2 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (Gv.IsUsePreparedStatement) cm.Prepare();
                    var dr = cm.ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue2 { Col1 = Sm.DrStr(dr, c[0]), Col2 = Sm.DrStr(dr, c[1]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }

        public static void SetLue3(
        ref LookUpEdit Lue, ref MySqlCommand cm,
        int ColWidth1, int ColWidth2, int ColWidth3,
        bool ColVisible1, bool ColVisible2, bool ColVisible3,
        string ColTitle1, string ColTitle2, string ColTitle3,
        string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue3>()
                {
                    new Lue3{Col1 = null, Col2 = null, Col3 = null},
                    new Lue3{Col1 = "<Refresh>", Col2 = "<Refresh>", Col3 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (Gv.IsUsePreparedStatement) cm.Prepare();
                    var dr = cm.ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2"), dr.GetOrdinal("Col3") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue3
                        {
                            Col1 = Sm.DrStr(dr, c[0]),
                            Col2 = Sm.DrStr(dr, c[1]),
                            Col3 = Sm.DrStr(dr, c[2])
                        });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col3"].Caption = ColTitle3;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col3"].Width = ColWidth3;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.Columns["Col3"].Visible = ColVisible3;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }


        public static void SetLue2(
                ref List<DevExpress.XtraEditors.LookUpEdit> ListOfLue, string SQL,
                int ColWidth1, int ColWidth2,
                bool ColVisible1, bool ColVisible2,
                string ColTitle1, string ColTitle2,
                string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue2>()
                {
                    new Lue2{Col1 = null, Col2 = null},
                    new Lue2{Col1 = "<Refresh>", Col2 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue2 { Col1 = Sm.DrStr(dr, c[0]), Col2 = Sm.DrStr(dr, c[1]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                ListOfLue.ForEach(Lue =>
                {
                    SetLookUpEdit(Lue, ListOfObject);
                    Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                    Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                    Lue.Properties.Columns["Col1"].Width = ColWidth1;
                    Lue.Properties.Columns["Col2"].Width = ColWidth2;
                    Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                    Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                    Lue.Properties.DisplayMember = DisplayMember;
                    Lue.Properties.ValueMember = ValueMember;
                });
            }
        }

        public static string ToRoman(decimal n)
        {
            string mRoman = string.Empty;

            if (n < 0 || n > 3999) mRoman = "Value must be between 1 and 3999";
            if (n < 1) mRoman = string.Empty;
            if (n >= 1000) mRoman = "M" + ToRoman(n - 1000);
            if (n >= 900) mRoman = "CM" + ToRoman(n - 900);
            if (n >= 500) mRoman = "D" + ToRoman(n - 500);
            if (n >= 400) mRoman = "CD" + ToRoman(n - 400);
            if (n >= 100) mRoman = "C" + ToRoman(n - 100);
            if (n >= 90) mRoman = "XC" + ToRoman(n - 90);
            if (n >= 50) mRoman = "L" + ToRoman(n - 50);
            if (n >= 40) mRoman = "XL" + ToRoman(n - 40);
            if (n >= 10) mRoman = "X" + ToRoman(n - 10);
            if (n >= 9) mRoman = "IX" + ToRoman(n - 9);
            if (n >= 5) mRoman = "V" + ToRoman(n - 5);
            if (n >= 4) mRoman = "IV" + ToRoman(n - 4);
            if (n >= 1) mRoman = "I" + ToRoman(n - 1);

            return mRoman;
        }

        public static string ServerCurrentDate()
        {
            return GetValue("Select Replace(CurDate(), '-', '');");
        }

        public static string ServerCurrentDateTime()
        {
            return GetValue("Select CurrentDateTime()");
        }

        public static void ShowDataInCtrl(ref MySqlCommand cm, string SQL, string[] ColumnTitle, RefreshData rd, bool ShowNoDataInd)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                }
                else
                {
                    while (dr.Read()) rd(dr, c);
                }
                dr.Close();
                dr.Dispose();
                cm.Dispose();
            }
        }

        public static void ShowDataInGrid(
            ref iGrid Grd, ref MySqlCommand cm, string SQL, string[] ColumnTitle,
            RefreshGrdData rgd, bool ShowNoDataInd, bool GroupInd, bool Editable, bool GrdAutoSize
)
        {
            ClearGrd(Grd, Editable);
            if (GroupInd)
            {
                Grd.GroupObject.Clear();
                Grd.Group();
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, ColumnTitle);
                    if (!dr.HasRows)
                    {
                        if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, string.Empty);
                    }
                    else
                    {
                        int Row = 0;
                        if (!Editable) Grd.Rows.Count = 0;
                        //Grd.ProcessTab = true;
                        Grd.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd.Rows.Add();
                            rgd(dr, Grd, c, Row);
                            Row++;
                        }
                        if (GroupInd)
                        {
                            Grd.GroupObject.Add(1);
                            Grd.Group();
                            SetGrdAlwaysShowSubTotal(Grd);
                        }
                    }
                    //Sm.SetGrdAutoSize(Grd);
                    if (GrdAutoSize)
                        Grd.Cols.AutoWidth();
                    Grd.EndUpdate();
                    dr.Close();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
        }

        public static void ShowDocApproval(string DocNo, string DocType, ref iGrid GrdTemp)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType=@DocType ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 0);
        }

        public static void ShowDocApproval(string DocNo, string DocType, ref iGrid GrdTemp, bool Param)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (!Param)
            {
                SQL.AppendLine("Select A.ApprovalDNo, B.UserName, NULL AS Jabatan, A.LastUpDt, A.Remark, ");
                SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            }
            else
            {
                SQL.AppendLine("Select A.ApprovalDNo, IFNULL(D.UserName, GROUP_CONCAT(C.UserName SEPARATOR ' / ')) AS Username, ");
                SQL.AppendLine("IFNULL(CONCAT(H.PosName, case when F.ActingOfficialInd = 'Y' then ' (PLT)' ELSE '' end), GROUP_CONCAT(G.PosName, ");
                SQL.AppendLine("case when E.ActingOfficialInd = 'Y' then ' (PLT)' ");
                SQL.AppendLine("when F.ActingOfficialInd = 'Y' then ' (PLT)' else '' end SEPARATOR ' / ')) AS Jabatan, ");
                SQL.AppendLine("A.LastUpDt, A.Remark, ");
                SQL.AppendLine("Case when A.Status IS NULL then 'Outstanding' When A.Status = 'A' Then 'Approved' When A.Status = 'C' Then 'Cancelled' When A.Status = 'O' Then 'Outstanding' End As StatusDesc, A.Status  ");
            }
            SQL.AppendLine("From TblDocApproval A ");
            if (!Param)
            {
                SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
                SQL.AppendLine("Where A.DocType=@DocType ");
                SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
                SQL.AppendLine("And A.DocNo=@DocNo ");
                SQL.AppendLine("Order By A.ApprovalDNo;");
            }
            else
            {
                SQL.AppendLine("LEFT JOIN tbldocapprovalsetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo ");
                SQL.AppendLine("LEFT JOIN tbluser C ON B.UserCode LIKE CONCAT('%',C.UserCode,'%') AND (C.ExpDt is null OR DATE(C.ExpDt) >= DATE(CURDATE())) ");
                SQL.AppendLine("LEFT JOIN tbluser D ON A.UserCode = D.UserCode AND (D.ExpDt is null OR DATE(D.ExpDt) >= DATE(CURDATE())) ");
                SQL.AppendLine("LEFT JOIN tblemployee E ON C.UserCode = E.UserCode ");
                SQL.AppendLine("LEFT JOIN tblemployee F ON D.UserCode = F.UserCode ");
                SQL.AppendLine("LEFT JOIN tblposition G ON E.PosCode = G.PosCode AND (E.ResignDt IS NULL OR DATE(E.ResignDt) >= DATE(CURDATE())) ");
                SQL.AppendLine("LEFT JOIN tblposition H ON F.PosCode = H.PosCode AND (F.ResignDt IS NULL OR DATE(F.ResignDt) >= DATE(CURDATE()))");
                SQL.AppendLine("WHERE A.DocType=@DocType ");
                SQL.AppendLine("And A.DocNo=@DocNo ");
                SQL.AppendLine("GROUP BY A.ApprovalDNo ");
                SQL.AppendLine("Order By A.ApprovalDNo;");
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "ApprovalDNo",
                        "UserName","Jabatan","StatusDesc","LastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 0);
        }

        public static void ShowErrorMsg(Exception Exc)
        {
            switch (Exc.Message)
            {
                case "Fatal error encountered during command execution.":
                    Sm.StdMsg(mMsgType.Warning, "Unstable connection.");
                    break;
                case "Unable to connect to any of the specified MySQL hosts.":
                    Sm.StdMsg(mMsgType.Warning, "No connection to database server.");
                    break;
                default:
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                    break;
            }
        }

        public static void ShowHrdCountAfterContent(ref iGrid Grd)
        {
            if (Grd.Rows.Count == 0) return;

            // The array to store totals by levels;
            // the last row is always a row with normal cells with max available level:
            int maxGroupLevel = Grd.Rows[Grd.Rows.Count - 1].Level - 1;
            int[] LevelSum = new int[maxGroupLevel + 1];
            int count = 0;

            for (int iRow = Grd.Rows.Count - 1; iRow >= 0; iRow--)
            {
                iGRow curRow = Grd.Rows[iRow];
                switch (curRow.Type)
                {
                    case iGRowType.Normal:
                        if (curRow.Visible)
                            count++;
                        break;
                    case iGRowType.AutoGroupRow:
                        if (maxGroupLevel < 0) return;
                        if (curRow.Level == maxGroupLevel)
                        {
                            for (int i = 0; i <= maxGroupLevel; i++)
                                LevelSum[i] += count;
                        }
                        var Value = curRow.RowTextCell.Value.ToString();
                        if (Value.Length > 0 && Value.IndexOf(" (Count=") != -1)
                            curRow.RowTextCell.Value = Sm.Left(Value, Value.IndexOf(" (Count="));
                        curRow.RowTextCell.Value += " (Count=" + LevelSum[curRow.Level].ToString() + ")";
                        LevelSum[curRow.Level] = 0;
                        count = 0;
                        break;
                }
            }
        }

        public static void ShowItemInfo(string MenuCode, string ItCode)
        {
            var f = new FrmItem(MenuCode);
            f.Tag = MenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mItCode = ItCode;
            f.ShowDialog();
        }

        public static void StdMsg(mMsgType MsgType, string Message)
        {
            switch (MsgType)
            {
                case mMsgType.NoData:
                    MessageBox.Show("No data.", Gv.App, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case mMsgType.Info:
                    MessageBox.Show(Message, Gv.App, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case mMsgType.Warning:
                    MessageBox.Show(Message, Gv.App, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }
        }

        public static string Terbilang2(Decimal d)
        {
            #region sik
            //d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = "Dollar And " + Terbilang2(Decimal.Round(frac * 100)) + " Cents";
            else
                strHasil = " ";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuanUSD[nDigit] + "  ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasanUSD[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuanUSD[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = ribuanUSD[(int)Decimal.Round(nPosisi / 3m)] + "  ";
                            else
                                tmpBuff = tmpBuff + ribuanUSD[(int)Decimal.Round(nPosisi / 3)] + "  ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhanUSD[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = satuanUSD[nDigit] + " Hundred " + strHasil;
                            else
                                strHasil = satuanUSD[nDigit] + " Hundred " + strHasil;
                        }
                        break;
                }
            }



            strHasil = strHasil.Trim();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;

            #endregion


        }

        public static string Terbilang3(Decimal d)
        {
            //d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = " koma " + Terbilang(Decimal.Round(frac * 100));
            else
                strHasil = "rupiah";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " ratus " + strHasil;
                        }
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }

        public static void TmeKeyDown(iGrid Grd, ref bool fAccept, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fAccept = false;
                    Grd.Focus();
                    break;
            }
        }

        public static void TmeLeave(TimeEdit Tme, ref iGCell fCell, ref bool fAccept)
        {
            if (Tme.Visible)
            {
                Tme.Visible = false;
                if (fAccept)
                {
                    var value = Sm.GetTme(Tme);
                    if (value.Length == 0)
                        fCell.Value = null;
                    else
                        fCell.Value = string.Concat(Sm.Left(value, 2), ":", Sm.Right(value, 2));
                }


            }
        }

        public static void TmeRequestEdit(iGrid Grd, TimeEdit Tme, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;
            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;
            myBounds.Offset(Grd.Location);
            Tme.Bounds = myBounds;
            var value = Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex);
            if (value.Length == 0)
                Tme.EditValue = null;
            else
            {
                Sm.SetTme(Tme, value);
            }
            Tme.Visible = true;
            Tme.Focus();
            fAccept = true;
        }


        public static void TxtTrim(TextEdit Txt)
        {
            Txt.Text = Txt.Text.Trim();
        }

        public static void WriteSetting(string key, string value)
        {
            //write setting from app.config

            // load config document for current assembly
            XmlDocument doc = loadConfigDocument();

            // retrieve appSettings node
            XmlNode node = doc.SelectSingleNode("//appSettings");

            if (node == null)
                throw new InvalidOperationException("appSettings section not found in config file.");

            try
            {
                // select the 'add' element that contains the key
                XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                if (elem != null)
                {
                    // add value for key
                    elem.SetAttribute("value", value);
                }
                else
                {
                    // key was not found so create the 'add' element 
                    // and set it's key/value attributes 
                    elem = doc.CreateElement("add");
                    elem.SetAttribute("key", key);
                    elem.SetAttribute("value", value);
                    node.AppendChild(elem);
                }
                doc.Save(getConfigFilePath());
            }
            catch
            {
                throw;
            }
        }

        public static bool Find_In_Set(string Value, string Values)
        {
            foreach (var x in Values.Split(','))
            {
                if (Sm.CompareStr(x.Trim(), Value)) return true;
            }
            return false;
        }

        public static decimal RoundDown(decimal value, int precision)
        {
            bool negativeInd = value < 0 ? true : false;
            value = (value < 0 ? value * -1 : value);
            decimal factor = (decimal)Math.Pow(10, precision);
            decimal unround = decimal.Floor(value * factor);
            decimal fraction = value * factor - unround;
            if (fraction <= 1m)
                return unround / factor * (negativeInd == true ? -1 : 1);
            else
                return (unround + 1m) / factor * (negativeInd == true ? -1 : 1);
        }

        public static bool IsTransactionsCantSameDate(string Tbl, string EmpCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Msg = string.Empty;

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From Tbl" + Tbl + " A ");
            if (Tbl == "TravelRequestHdr")
                SQL.AppendLine("Inner Join TblTravelRequestDtl7 B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.`Status` In ('O', 'A') ");
            if (Tbl == "EmpLeaveHdr")
                SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And @Dt1 Between A.StartDt And A.EndDt ");
            SQL.AppendLine("And @Dt2 Between A.StartDt And A.EndDt ");
            SQL.AppendLine("Limit 1;");
            cm.CommandText = SQL.ToString();
            //Sm.CmParam<String>(ref cm, "@Tbl", Tbl);
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);
            if (Sm.GetParameter("DocTitle") != "PHT")
            {
                if (Tbl == "TravelRequestHdr")
                    Msg = "The start date and end date are already used in the Travel Request transaction";
                else if (Tbl == "EmpLeaveHdr")
                    Msg = "The start date and end date are already used in the Employee Leave Request transaction";
            }
            else
            {
                if (Tbl == "TravelRequestHdr")
                    Msg = "Pada tanggal pengajuan sudah digunakan untuk pengajuan di transaksi Permintaan Perjalanan Dinas";
                else if (Tbl == "EmpLeaveHdr")
                    Msg = "Pada tanggal pengajuan sudah digunakan untuk pengajuan di transaksi Permintaan Cuti Karyawan";
            }
            return (IsDataExist(cm, Msg));
            //return (IsDataExist(SQL.ToString(), Left(Dt, 8), ProfitCenterCode, ClosingJournalProfitCenterLevelToBeValidated));
        }

        #endregion

        #region Leave Method

        #region Annual Leave

        public static void GetEmpAnnualLeave(ref List<EmployeeResidualLeave> l, string Yr)
        {
            if (l.Count == 0 || Sm.GetParameter("AnnualLeaveCode").Length == 0) return;
            GetEmpAnnualLeave1(ref l, Yr);
            GetEmpAnnualLeave2(ref l, Yr);
        }

        private static void GetEmpAnnualLeave1(ref List<EmployeeResidualLeave> l, string Yr)
        {
            var RLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN
            var IsLongServiceLeaveExisted = Sm.GetParameter("LongServiceLeaveCode").Length > 0;

            var AnnualLeaveNoOfYrToActive = 100m;
            var Value = Sm.GetParameter("AnnualLeaveNoOfYrToActive");
            if (Value.Length > 0) AnnualLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYrToActive = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive");
            if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYr = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
            if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);

            var LeaveDay = 0m;
            Value = Sm.GetValue(
                 "Select A.NoOfDay From TblLeave A, TblParameter B " +
                 "Where A.LeaveCode=B.ParValue And B.ParCode='AnnualLeaveCode';"
                 );
            if (Value.Length > 0) LeaveDay = decimal.Parse(Value);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, YrTemp = string.Empty, MthTemp = string.Empty;
            decimal y1 = decimal.Parse(Yr), y2 = 0;
            bool IsLongServiceYr = false;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            SQL.AppendLine("Select A.EmpCode, ");
            SQL.AppendLine("Case When IfNull(B.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As JoinDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where (" + Filter + ");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].JoinDt = Sm.DrStr(dr, c[1]);
                                l[i].LeaveDay = LeaveDay;
                                if (RLPType == "1" || RLPType == "3")
                                {
                                    YrTemp = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + AnnualLeaveNoOfYrToActive).ToString();
                                    MthTemp = l[i].JoinDt.Substring(4, 2);
                                    if (MthTemp == "12")
                                    {
                                        YrTemp = (int.Parse(YrTemp) + 1).ToString();
                                        MthTemp = "01";
                                    }
                                    else
                                    {
                                        MthTemp = Sm.Right("0" + (int.Parse(MthTemp) + 1).ToString(), 2);
                                    }
                                    l[i].ActiveDt = string.Concat(YrTemp, MthTemp, "01");
                                    l[i].StartDt = string.Concat(Yr, "0101");
                                    l[i].EndDt = string.Concat(Yr, "1231");
                                }
                                else
                                {
                                    if (RLPType == "2")
                                    {
                                        l[i].ActiveDt = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + AnnualLeaveNoOfYrToActive).ToString() + Sm.Right(l[i].JoinDt, 4);
                                        l[i].StartDt = string.Concat(Yr, Sm.Right(l[i].JoinDt, 4));
                                        l[i].EndDt = Sm.Left(Sm.FormatDate(Sm.ConvertDate(l[i].StartDt).AddYears(1).AddDays(-1)), 8);
                                    }
                                    else
                                    {
                                        l[i].ActiveDt = string.Empty;
                                        l[i].StartDt = string.Empty;
                                        l[i].EndDt = string.Empty;
                                        l[i].LeaveDay = 0;
                                    }
                                }
                                if (IsLongServiceLeaveExisted)
                                {
                                    IsLongServiceYr = false;
                                    y2 = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive);
                                    while (y2 <= y1)
                                    {
                                        if (y2 == y1)
                                        {
                                            IsLongServiceYr = true;
                                            break;
                                        }
                                        y2 += LongServiceLeaveNoOfYr;
                                    }
                                    if (IsLongServiceYr) l[i].LeaveDay = 0;
                                }

                                //Value1<Value2 : -1
                                //Value1=Value2 : 0
                                //Value1>Value2 : 1

                                if (Sm.CompareDtTm(l[i].StartDt, l[i].ActiveDt) == -1)
                                    l[i].StartDt = l[i].ActiveDt;

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == -1)
                                {
                                    l[i].StartDt = string.Empty;
                                    l[i].EndDt = string.Empty;
                                }

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == 0)
                                {
                                    l[i].StartDt = l[i].ActiveDt;
                                    l[i].EndDt = l[i].ActiveDt;
                                }

                                if (l[i].ActiveDt.Length == 0 || l[i].StartDt.Length == 0 || l[i].EndDt.Length == 0)
                                    l[i].LeaveDay = 0;
                                l[i].RemainingDay = l[i].LeaveDay;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private static void GetEmpAnnualLeave2(ref List<EmployeeResidualLeave> l, string Yr)
        {
            var LeaveCode = Sm.GetParameter("AnnualLeaveCode");
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].StartDt.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(X1.EmpCode=@EmpCode" + i.ToString() + " And X2.LeaveDt Between @StartDt" + i.ToString() + " And @EndDt" + i.ToString() + " ) ";
                    if (Filter2.Length > 0) Filter2 += " Or ";
                    Filter2 += "(B.EmpCode=@EmpCode" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
                    Sm.CmParamDt(ref cm, "@StartDt" + i.ToString(), l[i].StartDt);
                    Sm.CmParamDt(ref cm, "@EndDt" + i.ToString(), l[i].EndDt);
                }
            }

            if (Filter.Length == 0 && Filter2.Length == 0) return;

            SQL.AppendLine("Select T.EmpCode, Sum(T.UsedDay) As UsedDay From (");

            if (Filter.Length > 0)
            {
                SQL.AppendLine("    Select A.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeaveHdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "A.").Replace("X2.", "B.") + ") ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeave2Hdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "B.").Replace("X2.", "C.") + ") ");
            }
            if (Filter.Length > 0 && Filter2.Length > 0)
                SQL.AppendLine("    Union All ");

            if (Filter2.Length > 0)
            {
                SQL.AppendLine("    Select B.EmpCode, B.NoOfDay As UsedDay ");
                SQL.AppendLine("    From TblRLPHdr A ");
                SQL.AppendLine("    Inner Join TblRLPDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (" + Filter2 + ") ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.RLPCode='01' ");
                SQL.AppendLine("    And A.Yr=@Yr ");
            }

            SQL.AppendLine(") T Group By T.EmpCode Order By T.EmpCode;");

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "UsedDay" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].UsedDay = Sm.DrDec(dr, c[1]);
                                l[i].RemainingDay = l[i].LeaveDay - l[i].UsedDay;
                                if (l[i].RemainingDay < 0) l[i].RemainingDay = 0;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        public static void SetLeaveSummaryForAnnualLeave(ref List<LeaveSummary> l, string Yr)
        {
            var RLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN
            var IsLongServiceLeaveExisted = Sm.GetParameter("LongServiceLeaveCode").Length > 0;

            var AnnualLeaveNoOfYrToActive = 100m;
            var Value = Sm.GetParameter("AnnualLeaveNoOfYrToActive");
            if (Value.Length > 0) AnnualLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYrToActive = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive");
            if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYr = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
            if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);

            var LeaveDay = 0m;
            Value = Sm.GetValue(
                 "Select A.NoOfDay From TblLeave A, TblParameter B " +
                 "Where A.LeaveCode=B.ParValue " +
                 "And B.ParCode='AnnualLeaveCode' " +
                 "And B.ParValue Is Not Null;"
                 );
            if (Value.Length > 0) LeaveDay = decimal.Parse(Value);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, YrTemp = string.Empty, MthTemp = string.Empty;
            decimal y1 = decimal.Parse(Yr), y2 = 0;
            bool IsLongServiceYr = false;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            SQL.AppendLine("Select A.EmpCode, ");
            SQL.AppendLine("Case When IfNull(B.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As InitialDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where (" + Filter + ");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "InitialDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].InitialDt = Sm.DrStr(dr, c[1]);
                                l[i].NoOfDays1 = LeaveDay;
                                if (RLPType == "1" || RLPType == "3")
                                {
                                    YrTemp = (decimal.Parse(Sm.Left(l[i].InitialDt, 4)) + AnnualLeaveNoOfYrToActive).ToString();
                                    MthTemp = l[i].InitialDt.Substring(4, 2);
                                    if (MthTemp == "12")
                                    {
                                        YrTemp = (int.Parse(YrTemp) + 1).ToString();
                                        MthTemp = "01";
                                    }
                                    else
                                    {
                                        MthTemp = Sm.Right("0" + (int.Parse(MthTemp) + 1).ToString(), 2);
                                    }
                                    l[i].InitialDt = string.Concat(YrTemp, MthTemp, "01");
                                    l[i].StartDt = string.Concat(Yr, "0101");
                                    l[i].EndDt = string.Concat(Yr, "1231");
                                }
                                else
                                {
                                    if (RLPType == "2")
                                    {
                                        l[i].InitialDt = (decimal.Parse(Sm.Left(l[i].InitialDt, 4)) + AnnualLeaveNoOfYrToActive).ToString() + Sm.Right(l[i].InitialDt, 4);
                                        l[i].StartDt = string.Concat(Yr, Sm.Right(l[i].InitialDt, 4));
                                        l[i].EndDt = Sm.Left(Sm.FormatDate(Sm.ConvertDate(l[i].StartDt).AddYears(1).AddDays(-1)), 8);
                                    }
                                    else
                                    {
                                        l[i].InitialDt = string.Empty;
                                        l[i].StartDt = string.Empty;
                                        l[i].EndDt = string.Empty;
                                        l[i].NoOfDays1 = 0;
                                    }
                                }
                                if (IsLongServiceLeaveExisted)
                                {
                                    IsLongServiceYr = false;
                                    y2 = (decimal.Parse(Sm.Left(l[i].InitialDt, 4)) + LongServiceLeaveNoOfYrToActive);
                                    while (y2 <= y1)
                                    {
                                        if (y2 == y1)
                                        {
                                            IsLongServiceYr = true;
                                            break;
                                        }
                                        y2 += LongServiceLeaveNoOfYr;
                                    }
                                    if (IsLongServiceYr) l[i].NoOfDays1 = 0;
                                }

                                //Value1<Value2 : -1
                                //Value1=Value2 : 0
                                //Value1>Value2 : 1

                                if (Sm.CompareDtTm(l[i].StartDt, l[i].InitialDt) == -1)
                                    l[i].StartDt = l[i].InitialDt;

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].InitialDt) == -1)
                                {
                                    l[i].StartDt = string.Empty;
                                    l[i].EndDt = string.Empty;
                                }

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].InitialDt) == 0)
                                {
                                    l[i].StartDt = l[i].InitialDt;
                                    l[i].EndDt = l[i].InitialDt;
                                }

                                if (l[i].InitialDt.Length == 0 || l[i].StartDt.Length == 0 || l[i].EndDt.Length == 0)
                                    l[i].NoOfDays1 = 0;
                                l[i].Balance = l[i].NoOfDays1;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Long Service Leave

        public static void GetEmpLongServiceLeave(ref List<EmployeeResidualLeave> l, string Yr)
        {
            var RLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN

            if (l.Count == 0 || RLPType.Length == 0) return;

            if (RLPType == "2")
            {
                if (Sm.GetParameter("LongServiceLeaveCode").Length == 0) return;
                GetEmpLongServiceLeave2a(ref l, Yr);
                GetEmpLongServiceLeave2b(ref l, Yr);
            }

            if (RLPType == "3")
            {
                if (Sm.GetParameter("LongServiceLeaveCode").Length == 0) return;

                GetEmpLongServiceLeave3a(ref l, Yr, string.Empty);
                GetEmpLongServiceLeave3b(ref l, Yr, string.Empty);
            }
        }

        public static void GetEmpLongServiceLeave2(ref List<EmployeeResidualLeave> l, string Yr)
        {
            var RLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN

            if (l.Count == 0 || RLPType.Length == 0) return;

            if (RLPType == "3")
            {
                if (Sm.GetParameter("LongServiceLeaveCode2").Length == 0) return;

                GetEmpLongServiceLeave3a(ref l, Yr, "2");
                GetEmpLongServiceLeave3b(ref l, Yr, "2");
            }
        }

        private static void GetEmpLongServiceLeave2a(ref List<EmployeeResidualLeave> l, string Yr)
        {
            int DurationOfLongServiceLeavePayment = 0;

            var RLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN

            var DurationOfLongServiceLeavePaymentString = Sm.GetParameter("DurationOfLongServiceLeavePayment");//parameter nentuin waktu expired cuti besar dalm bulan
            if (DurationOfLongServiceLeavePaymentString.Length > 0)
                DurationOfLongServiceLeavePayment = Convert.ToInt32(DurationOfLongServiceLeavePaymentString);

            var LongServiceLeaveNoOfYrToActive = 100m;
            var Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive");
            if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYr = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
            if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);

            var LeaveDay = 0m;
            Value = Sm.GetValue(
                 "Select A.NoOfDay From TblLeave A, TblParameter B " +
                 "Where A.LeaveCode=B.ParValue And B.ParCode='LongServiceLeaveCode';"
                 );
            if (Value.Length > 0) LeaveDay = decimal.Parse(Value);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, YrTemp = string.Empty;
            decimal y1 = decimal.Parse(Yr), y2 = 0;
            bool IsLongServiceYr = false;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            SQL.AppendLine("Select A.EmpCode, ");
            SQL.AppendLine("Case When IfNull(B.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As JoinDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where (" + Filter + ");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].JoinDt = Sm.DrStr(dr, c[1]);
                                l[i].LeaveDay = LeaveDay;
                                if (RLPType == "1" || RLPType == "3")
                                {
                                    YrTemp = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive).ToString();
                                    //MthTemp = l[i].JoinDt.Substring(4, 2);

                                    //if (MthTemp == "12")
                                    //{
                                    //    YrTemp = (int.Parse(YrTemp) + 1).ToString();
                                    //    MthTemp = "01";
                                    //}
                                    //else
                                    //{
                                    //    MthTemp = Sm.Right("0" + (int.Parse(MthTemp) + 1).ToString(), 2);
                                    //}

                                    l[i].ActiveDt = string.Concat(YrTemp, "0101");
                                    l[i].StartDt = string.Concat(Yr, "0101");
                                    l[i].EndDt = string.Concat(Yr, "1231");
                                }
                                else
                                {
                                    if (RLPType == "2")
                                    {
                                        l[i].ActiveDt = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive).ToString() + Sm.Right(l[i].JoinDt, 4);
                                        l[i].StartDt = string.Concat(Yr, Sm.Right(l[i].JoinDt, 4));
                                        //OLd
                                        //l[i].EndDt = Sm.Left(Sm.FormatDate(Sm.ConvertDate(l[i].StartDt).AddYears(1).AddDays(-1)), 8);
                                        l[i].EndDt = Sm.Left(Sm.FormatDate(Sm.ConvertDate(l[i].StartDt).AddMonths(DurationOfLongServiceLeavePayment)), 8);
                                    }
                                    else
                                    {
                                        l[i].ActiveDt = string.Empty;
                                        l[i].StartDt = string.Empty;
                                        l[i].EndDt = string.Empty;
                                        l[i].LeaveDay = 0;
                                    }
                                }

                                IsLongServiceYr = false;
                                y2 = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive);
                                while (y2 <= y1)
                                {
                                    if (y2 == y1)
                                    {
                                        IsLongServiceYr = true;
                                        break;
                                    }
                                    y2 += LongServiceLeaveNoOfYr;
                                }
                                if (!IsLongServiceYr) l[i].LeaveDay = 0;

                                //Value1<Value2 : -1
                                //Value1=Value2 : 0
                                //Value1>Value2 : 1

                                if (Sm.CompareDtTm(l[i].StartDt, l[i].ActiveDt) == -1)
                                    l[i].StartDt = l[i].ActiveDt;

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == -1)
                                {
                                    l[i].StartDt = string.Empty;
                                    l[i].EndDt = string.Empty;
                                }

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == 0)
                                {
                                    l[i].StartDt = l[i].ActiveDt;
                                    l[i].EndDt = l[i].ActiveDt;
                                }
                                if (l[i].ActiveDt.Length == 0 || l[i].StartDt.Length == 0 || l[i].EndDt.Length == 0)
                                    l[i].LeaveDay = 0;
                                l[i].RemainingDay = l[i].LeaveDay;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private static void GetEmpLongServiceLeave2b(ref List<EmployeeResidualLeave> l, string Yr)
        {
            var LeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].StartDt.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(X1.EmpCode=@EmpCode" + i.ToString() + " And X2.LeaveDt Between @StartDt" + i.ToString() + " And @EndDt" + i.ToString() + " ) ";
                    if (Filter2.Length > 0) Filter2 += " Or ";
                    Filter2 += "(B.EmpCode=@EmpCode" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
                    Sm.CmParamDt(ref cm, "@StartDt" + i.ToString(), l[i].StartDt);
                    Sm.CmParamDt(ref cm, "@EndDt" + i.ToString(), l[i].EndDt);
                }
            }

            if (Filter.Length == 0 && Filter2.Length == 0) return;

            SQL.AppendLine("Select T.EmpCode, Sum(T.UsedDay) As UsedDay From (");

            if (Filter.Length > 0)
            {
                SQL.AppendLine("    Select A.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeaveHdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "A.").Replace("X2.", "B.") + ") ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeave2Hdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "B.").Replace("X2.", "C.") + ") ");
            }

            if (Filter.Length > 0 && Filter2.Length > 0)
            {
                SQL.AppendLine("    Union All ");
            }

            if (Filter2.Length > 0)
            {
                SQL.AppendLine("    Select B.EmpCode, B.NoOfDay As UsedDay ");
                SQL.AppendLine("    From TblRLPHdr A ");
                SQL.AppendLine("    Inner Join TblRLPDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (" + Filter2 + ") ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.RLPCode='02' ");
                SQL.AppendLine("    And A.Yr=@Yr ");
            }

            SQL.AppendLine(") T Group By T.EmpCode Order By T.EmpCode;");

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "UsedDay" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].UsedDay = Sm.DrDec(dr, c[1]);
                                l[i].RemainingDay = l[i].LeaveDay - l[i].UsedDay;
                                if (l[i].RemainingDay < 0) l[i].RemainingDay = 0;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private static void GetEmpLongServiceLeave3a(ref List<EmployeeResidualLeave> l, string Yr, string No)
        {
            var LongServiceLeaveNoOfYrToActive = 100m;
            var Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive" + No);
            if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYr = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
            if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);

            var LeaveDay = 0m;
            Value = Sm.GetValue(
                 "Select A.NoOfDay From TblLeave A, TblParameter B " +
                 "Where A.LeaveCode=B.ParValue " +
                 "And B.ParCode='LongServiceLeaveCode" + No + "';"
                 );
            if (Value.Length > 0) LeaveDay = decimal.Parse(Value);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, YrTemp = string.Empty;
            decimal y1 = decimal.Parse(Yr), y2 = 0;
            bool IsLongServiceYr = false;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            SQL.AppendLine("Select A.EmpCode, ");
            SQL.AppendLine("Case When IfNull(B.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As JoinDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where (" + Filter + ");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].JoinDt = Sm.DrStr(dr, c[1]);
                                l[i].LeaveDay = LeaveDay;

                                YrTemp = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive).ToString();

                                l[i].ActiveDt = string.Concat(YrTemp, "0101");
                                l[i].StartDt = string.Concat(Yr, "0101");
                                l[i].EndDt = string.Concat(Yr, "1231");

                                IsLongServiceYr = false;
                                y2 = (decimal.Parse(Sm.Left(l[i].JoinDt, 4)) + LongServiceLeaveNoOfYrToActive);
                                while (y2 <= y1)
                                {
                                    if (y2 == y1)
                                    {
                                        IsLongServiceYr = true;
                                        break;
                                    }
                                    y2 += LongServiceLeaveNoOfYr;
                                }
                                if (!IsLongServiceYr) l[i].LeaveDay = 0;

                                //Value1<Value2 : -1
                                //Value1=Value2 : 0
                                //Value1>Value2 : 1

                                if (Sm.CompareDtTm(l[i].StartDt, l[i].ActiveDt) == -1)
                                    l[i].StartDt = l[i].ActiveDt;

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == -1)
                                {
                                    l[i].StartDt = string.Empty;
                                    l[i].EndDt = string.Empty;
                                }

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].ActiveDt) == 0)
                                {
                                    l[i].StartDt = l[i].ActiveDt;
                                    l[i].EndDt = l[i].ActiveDt;
                                }
                                if (l[i].ActiveDt.Length == 0 || l[i].StartDt.Length == 0 || l[i].EndDt.Length == 0)
                                    l[i].LeaveDay = 0;
                                l[i].RemainingDay = l[i].LeaveDay;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private static void GetEmpLongServiceLeave3b(ref List<EmployeeResidualLeave> l, string Yr, string No)
        {
            var LeaveCode = Sm.GetParameter("LongServiceLeaveCode" + No);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].StartDt.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(X1.EmpCode=@EmpCode" + i.ToString() + " And X2.LeaveDt Between @StartDt" + i.ToString() + " And @EndDt" + i.ToString() + " ) ";
                    Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
                    Sm.CmParamDt(ref cm, "@StartDt" + i.ToString(), l[i].StartDt);
                    Sm.CmParamDt(ref cm, "@EndDt" + i.ToString(), l[i].EndDt);
                }
            }

            if (Filter.Length == 0) return;

            SQL.AppendLine("Select T.EmpCode, Sum(T.UsedDay) As UsedDay From (");

            if (Filter.Length > 0)
            {
                SQL.AppendLine("    Select A.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeaveHdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "A.").Replace("X2.", "B.") + ") ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.EmpCode, ");
                SQL.AppendLine("    Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ");
                SQL.AppendLine("    From TblEmpLeave2Hdr A ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.LeaveCode=@LeaveCode ");
                SQL.AppendLine("    And (" + Filter.Replace("X1.", "B.").Replace("X2.", "C.") + ") ");
            }

            SQL.AppendLine(") T Group By T.EmpCode Order By T.EmpCode;");

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "UsedDay" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].UsedDay = Sm.DrDec(dr, c[1]);
                                l[i].RemainingDay = l[i].LeaveDay - l[i].UsedDay;
                                if (l[i].RemainingDay < 0) l[i].RemainingDay = 0;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        public static void SetLeaveSummaryForLongServiceLeave(ref List<LeaveSummary> l, string Yr, string No)
        {
            var LongServiceLeaveNoOfYrToActive = 100m;
            var Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive" + No);
            if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);

            var LongServiceLeaveNoOfYr = 100m;
            Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
            if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);

            var LeaveDay = 0m;
            Value = Sm.GetValue(
                 "Select A.NoOfDay From TblLeave A, TblParameter B " +
                 "Where A.LeaveCode=B.ParValue " +
                 "And B.ParCode='LongServiceLeaveCode" + No + "';"
                 );
            if (Value.Length > 0) LeaveDay = decimal.Parse(Value);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, YrTemp = string.Empty;
            decimal y1 = decimal.Parse(Yr), y2 = 0;
            bool IsLongServiceYr = false;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            SQL.AppendLine("Select A.EmpCode, ");
            SQL.AppendLine("Case When IfNull(B.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As JoinDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where (" + Filter + ");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].InitialDt = Sm.DrStr(dr, c[1]);
                                l[i].NoOfDays1 = LeaveDay;

                                YrTemp = (decimal.Parse(Sm.Left(l[i].InitialDt, 4)) + LongServiceLeaveNoOfYrToActive).ToString();

                                l[i].InitialDt = string.Concat(YrTemp, "0101");
                                l[i].StartDt = string.Concat(Yr, "0101");
                                l[i].EndDt = string.Concat(Yr, "1231");

                                IsLongServiceYr = false;
                                y2 = (decimal.Parse(Sm.Left(l[i].InitialDt, 4)) + LongServiceLeaveNoOfYrToActive);
                                while (y2 <= y1)
                                {
                                    if (y2 == y1)
                                    {
                                        IsLongServiceYr = true;
                                        break;
                                    }
                                    y2 += LongServiceLeaveNoOfYr;
                                }
                                if (!IsLongServiceYr) l[i].NoOfDays1 = 0;

                                //Value1<Value2 : -1
                                //Value1=Value2 : 0
                                //Value1>Value2 : 1

                                if (Sm.CompareDtTm(l[i].StartDt, l[i].InitialDt) == -1)
                                    l[i].StartDt = l[i].InitialDt;

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].InitialDt) == -1)
                                {
                                    l[i].StartDt = string.Empty;
                                    l[i].EndDt = string.Empty;
                                }

                                if (Sm.CompareDtTm(l[i].EndDt, l[i].InitialDt) == 0)
                                {
                                    l[i].StartDt = l[i].InitialDt;
                                    l[i].EndDt = l[i].InitialDt;
                                }
                                if (l[i].InitialDt.Length == 0 || l[i].StartDt.Length == 0 || l[i].EndDt.Length == 0)
                                    l[i].NoOfDays1 = 0;
                                l[i].Balance = l[i].NoOfDays1;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region POS Method

        public static string GetPosSetting(string Code)
        {
            return Sm.GetValue(
                "SELECT SetValue FROM TblPosSetting " +
                "WHERE PosNo='" + Gv.PosNo + "' " +
                "AND SetCode='" + Code + "'"
                );
        }

        public static void SetPosSetting(string Code, string Value)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblPosSetting  " +
                    "Set SetValue='" + Value + "' " +
                    "Where SetCode='" + Code + "' " +
                    "And PosNo='" + Gv.PosNo + "'; "
            };

            Sm.ExecCommand(cm);
        }

        public static Decimal GetDecValue(string strValue)
        {
            Decimal MyDecValue = 0;
            try
            {
                MyDecValue = Decimal.Parse(strValue);
            }
            catch
            {
            }
            return MyDecValue;
        }

        public static string PadLeft(string strToPad, int MaxLength)
        {
            if (strToPad.Length < MaxLength)
                return strToPad + CopyStr(" ", MaxLength - strToPad.Length);
            else
                return strToPad.Substring(0, MaxLength);
        }

        public static string PadRight(string strToPad, int MaxLength)
        {
            if (strToPad.Length < MaxLength)
                return CopyStr(" ", MaxLength - strToPad.Length) + strToPad;
            else
                return strToPad.Substring(0, MaxLength);
        }

        public static string PadCenter(string strToPad, int MaxLength)
        {
            if (strToPad.Length < MaxLength)
                return CopyStr(" ", ((MaxLength - strToPad.Length) / 2)) + strToPad;
            else
                return strToPad.Substring(0, MaxLength);
        }

        public static string GetAuditRollFileName(string BusinessDate)
        {
            if (BusinessDate == "") BusinessDate = Sm.GetPosSetting("CurrentBnsDate");
            if (Gv.PosNo == "") Gv.PosNo = "1";// GetComputerName();

            string filename = Gv.ApplicationPath + "\\" + BusinessDate + Gv.PosNo;

            return filename + ".aro";
            //int FileSeq = 0;
            //while (File.Exists(filename + ("00" + FileSeq.ToString()).Substring(("00" + FileSeq.ToString()).Length - 3, 3) + ".aro"))
            //    FileSeq++;

            //return filename + ("00" + FileSeq.ToString()).Substring(("00" + FileSeq.ToString()).Length - 3, 3) + ".aro";
        }

        public static void AddToAuditRollFile(string BusinessDate, string Message)
        {
            if (Gv.AuditRollFileName == "") Gv.AuditRollFileName = GetAuditRollFileName(BusinessDate);
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            File.AppendAllText(Gv.AuditRollFileName,
                Environment.NewLine + CurrentDateTime.Substring(0, 4) + "-" +
                CurrentDateTime.Substring(4, 2) + "-" +
                CurrentDateTime.Substring(6, 2) + "  " +
                CurrentDateTime.Substring(8, 2) + ":" +
                CurrentDateTime.Substring(10, 2) + " " +
                Message);

        }

        public static List<string> WordWrap(string text, int maxLineLength)
        {
            var list = new List<string>();

            int currentIndex;
            var lastWrap = 0;
            var whitespace = new[] { ' ', '\r', '\n', '\t' };
            do
            {
                currentIndex = lastWrap + maxLineLength > text.Length ? text.Length : (text.LastIndexOfAny(new[] { ' ', ',', '.', '?', '!', ':', ';', '-', '\n', '\r', '\t' }, Math.Min(text.Length - 1, lastWrap + maxLineLength)) + 1);
                if (currentIndex <= lastWrap)
                    currentIndex = Math.Min(lastWrap + maxLineLength, text.Length);
                list.Add(text.Substring(lastWrap, currentIndex - lastWrap).Trim(whitespace));
                lastWrap = currentIndex;
            } while (currentIndex < text.Length);

            return list;
        }

        #endregion

        #region Sales Invoice Summary

        public static void ProcessSalesInvoiceSummary(
            ref List<SalesInvoiceSummary> l,
            string DocType,
            string SalesInvoiceDocNo,
            bool SalesInvoiceCancelInd,
            string ARSDocNo,
            bool ARSCancelInd,
            decimal ARSAmt,
            string VoucherDocNo,
            bool VoucherCancelInd,
            decimal VoucherAmt)
        {
            //DocType=1 : Sales Invoice
            //DocType=2 : AR Settlement
            //DocType=3 : Voucher

            // Proses Sales Invoice
            if (!(DocType == "1" && !SalesInvoiceCancelInd))
                ProcessSalesInvoiceSummary1(ref l, SalesInvoiceDocNo);
            else
            {
                if (DocType == "1" && SalesInvoiceCancelInd)
                    l.Clear();
            }

            if (l.Count > 0)
            {
                // Proses AR Settlement
                if (DocType == "2" && !ARSCancelInd)
                    ProcessSalesInvoiceSummary2a(ref l, SalesInvoiceDocNo, ARSAmt);
                else
                {
                    if (DocType == "2" && ARSCancelInd)
                        ProcessSalesInvoiceSummary2b(ref l, SalesInvoiceDocNo, ARSDocNo, ARSAmt);
                    else
                        ProcessSalesInvoiceSummary2c(ref l, SalesInvoiceDocNo);
                }

                // Proses Voucher
                if (DocType == "3" && !VoucherCancelInd)
                    ProcessSalesInvoiceSummary3a(ref l, SalesInvoiceDocNo, VoucherAmt);
                else
                {
                    if (DocType == "3" && VoucherCancelInd)
                        ProcessSalesInvoiceSummary3b(ref l, SalesInvoiceDocNo, VoucherDocNo, VoucherAmt);
                    else
                        ProcessSalesInvoiceSummary3c(ref l, SalesInvoiceDocNo);
                }
            }
        }

        private static void ProcessSalesInvoiceSummary1(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, '1' As DocType, ");
            SQL.AppendLine("B.Qty*B.UPriceAfterTax As Amt1 ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join tblSalesInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, '000' As DNo, '2' As DocType, ");
            SQL.AppendLine("A.Amt+A.Downpayment-IfNull(B.Amt, 0.00) As Amt1 ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(T2.Qty*T2.UPriceAfterTax) As Amt ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join tblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");
        }

        private static void ProcessSalesInvoiceSummary2a(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo, decimal ARSAmt)
        {

        }

        private static void ProcessSalesInvoiceSummary2b(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo, string ARSDocNo, decimal ARSAmt)
        {

        }

        private static void ProcessSalesInvoiceSummary2c(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo)
        {

        }

        private static void ProcessSalesInvoiceSummary3a(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo, decimal VoucherAmt)
        {

        }

        private static void ProcessSalesInvoiceSummary3b(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo, string VoucherDocNo, decimal VoucherAmt)
        {

        }

        private static void ProcessSalesInvoiceSummary3c(ref List<SalesInvoiceSummary> l, string SalesInvoiceDocNo)
        {

        }

        #endregion

        #endregion

        #region Cuilin

        public static void ViewChart(string ReportName, List<IList> TableList, string[] TableName, bool DesignInd)
        {
            try
            {
                var report = new Report();
                report.Load(Application.StartupPath + @"\\Report\\ch" + ReportName + ".frx");

                for (int intX = 0; intX < TableList.Count; intX++)
                    report.RegisterData(TableList[intX], TableName[intX]);

                if (DesignInd)
                    report.Design();
                else
                {
                    report.Show();
                    report.Dispose();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static int DrInt(MySqlDataReader dr, int c)
        {
            // Get datareader for integer
            if (dr[c] == DBNull.Value)
                return 0;
            else
                try
                {
                    return dr.GetInt32(c);
                }
                catch
                {
                    return dr.GetInt16(c);
                }
        }

        public static void SetLue3(
                ref LookUpEdit Lue, string SQL,
                int ColWidth1, int ColWidth2, int ColWidth3,
                bool ColVisible1, bool ColVisible2, bool ColVisible3,
                string ColTitle1, string ColTitle2, string ColTitle3,
                string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue3>()
                {
                    new Lue3{Col1 = null, Col2 = null, Col3 = null},
                    new Lue3{Col1 = "<Refresh>", Col2 = "<Refresh>", Col3 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2"), dr.GetOrdinal("Col3") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue3
                        {
                            Col1 = Sm.DrStr(dr, c[0]),
                            Col2 = Sm.DrStr(dr, c[1]),
                            Col3 = Sm.DrStr(dr, c[2])
                        });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col3"].Caption = ColTitle3;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col3"].Width = ColWidth3;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.Columns["Col3"].Visible = ColVisible3;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }

        public static void SetLue4(
               ref LookUpEdit Lue, string SQL,
               int ColWidth1, int ColWidth2, int ColWidth3, int ColWidth4,
               bool ColVisible1, bool ColVisible2, bool ColVisible3, bool ColVisible4,
               string ColTitle1, string ColTitle2, string ColTitle3, string ColTitle4,
               string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue4>()
                {
                    new Lue4{Col1 = null, Col2 = null, Col3 = null, Col4 = null},
                    new Lue4{Col1 = "<Refresh>", Col2 = "<Refresh>", Col3 = "<Refresh>", Col4 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2"), dr.GetOrdinal("Col3"), dr.GetOrdinal("Col4") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue4
                        {
                            Col1 = Sm.DrStr(dr, c[0]),
                            Col2 = Sm.DrStr(dr, c[1]),
                            Col3 = Sm.DrStr(dr, c[2]),
                            Col4 = Sm.DrStr(dr, c[3]),
                        });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col3"].Caption = ColTitle3;
                Lue.Properties.Columns["Col4"].Caption = ColTitle4;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col3"].Width = ColWidth3;
                Lue.Properties.Columns["Col4"].Width = ColWidth4;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.Columns["Col3"].Visible = ColVisible3;
                Lue.Properties.Columns["Col4"].Visible = ColVisible4;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }

        public static void SetLue5(
               ref LookUpEdit Lue, string SQL,
               int ColWidth1, int ColWidth2, int ColWidth3, int ColWidth4, int ColWidth5,
               bool ColVisible1, bool ColVisible2, bool ColVisible3, bool ColVisible4, bool ColVisible5,
               string ColTitle1, string ColTitle2, string ColTitle3, string ColTitle4, string ColTitle5,
               string DisplayMember, string ValueMember)
        {
            var ListOfObject = new List<Lue5>()
                {
                    new Lue5{Col1 = null, Col2 = null, Col3 = null, Col4 = null, Col5 = null},
                    new Lue5{Col1 = "<Refresh>", Col2 = "<Refresh>", Col3 = "<Refresh>", Col4 = "<Refresh>", Col5 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2"), dr.GetOrdinal("Col3"), dr.GetOrdinal("Col4"), dr.GetOrdinal("Col5") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue5
                        {
                            Col1 = Sm.DrStr(dr, c[0]),
                            Col2 = Sm.DrStr(dr, c[1]),
                            Col3 = Sm.DrStr(dr, c[2]),
                            Col4 = Sm.DrStr(dr, c[3]),
                            Col5 = Sm.DrStr(dr, c[4])
                        });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle1;
                Lue.Properties.Columns["Col2"].Caption = ColTitle2;
                Lue.Properties.Columns["Col3"].Caption = ColTitle3;
                Lue.Properties.Columns["Col4"].Caption = ColTitle4;
                Lue.Properties.Columns["Col5"].Caption = ColTitle5;
                Lue.Properties.Columns["Col1"].Width = ColWidth1;
                Lue.Properties.Columns["Col2"].Width = ColWidth2;
                Lue.Properties.Columns["Col3"].Width = ColWidth3;
                Lue.Properties.Columns["Col4"].Width = ColWidth4;
                Lue.Properties.Columns["Col5"].Width = ColWidth5;
                Lue.Properties.Columns["Col1"].Visible = ColVisible1;
                Lue.Properties.Columns["Col2"].Visible = ColVisible2;
                Lue.Properties.Columns["Col3"].Visible = ColVisible3;
                Lue.Properties.Columns["Col4"].Visible = ColVisible4;
                Lue.Properties.Columns["Col5"].Visible = ColVisible5;
                Lue.Properties.DisplayMember = DisplayMember;
                Lue.Properties.ValueMember = ValueMember;
            }
        }

        public static void SetGridComboList(ref iGrid Grd, int GrdRow, int GrdCol, string SQL,
            string Col1Title, bool IsCol1Value, bool IsCol1Visible,
            string Col2Title, bool IsCol2Value, bool IsCol2Visible)
        {
            var ddl = new iGMultiColDropDownList();
            ddl.AddCol(Col1Title, IsCol1Value, IsCol1Visible);
            ddl.AddCol(Col2Title, IsCol2Value, IsCol2Visible);
            //ddl.MaxWidth = Grd1.Cols[0].MaxWidth;

            try
            {
                var ListOfObject = new List<ColList1>()
                {
                    new ColList1{Code = null, Name = null},
                    new ColList1{Code = "<Refresh>", Name = "<Refresh>"}
                };
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();

                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1"), dr.GetOrdinal("Col2") };
                    while (dr.Read())
                    {
                        ListOfObject.Add(
                            new ColList1
                            {
                                Code = Sm.DrStr(dr, c[0]),
                                Name = Sm.DrStr(dr, c[1])
                            });
                    }
                    dr.Close();
                }
                ListOfObject.ForEach(Index => { ddl.AddRow(new object[] { Index.Code, Index.Name }); });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            Grd.Cols[GrdCol].CellStyle.DropDownControl = ddl;
            Grd.Cells[GrdRow, GrdCol].Value = null;
        }

        private static void GetCurrencyRoundingInfo(string CurCode, ref decimal RndUp, ref int DecPnt)
        {
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var SQL =
                        "Select RndUp, DecPnt From tblcurrency Where CurCode='" + CurCode + "' Limit 1 ";
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("RndUp"), dr.GetOrdinal("DecPnt") };
                    while (dr.Read())
                    {
                        RndUp = Sm.DrDec(dr, c[0]);
                        DecPnt = Sm.DrInt(dr, c[1]);
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static decimal ComputeSalesPriceNettWithoutTax(decimal UnitPriceBeforeTax,
            decimal TaxRt1, decimal TaxRt2, decimal TaxRt3,
            bool PriceIncludeTax1, bool PriceIncludeTax2, bool PriceIncludeTax3,
            ref decimal TaxAmt1, ref decimal TaxAmt2, ref decimal TaxAmt3)
        {
            decimal UnitPriceAfterTax = UnitPriceBeforeTax;
            if (PriceIncludeTax1)
            {
                TaxAmt1 = (UnitPriceAfterTax / ((100 + TaxRt1) / 100)) * (TaxRt1 / 100);
                UnitPriceAfterTax = UnitPriceAfterTax - TaxAmt1;
            }
            else
                TaxAmt1 = UnitPriceAfterTax * (TaxRt1 / 100);

            if (PriceIncludeTax2)
            {
                TaxAmt2 = (UnitPriceAfterTax / ((100 + TaxRt2) / 100)) * (TaxRt2 / 100);
                UnitPriceAfterTax = UnitPriceAfterTax - TaxAmt2;
            }
            else
                TaxAmt2 = UnitPriceAfterTax * (TaxRt2 / 100);

            if (PriceIncludeTax3)
            {
                TaxAmt3 = (UnitPriceAfterTax / ((100 + TaxRt3) / 100)) * (TaxRt3 / 100);
                UnitPriceAfterTax = UnitPriceAfterTax - TaxAmt3;
            }
            else
                TaxAmt3 = UnitPriceAfterTax * (TaxRt3 / 100);


            return UnitPriceAfterTax;
        }

        public static decimal ComputeSalesPriceAfterDisc(string CurCode, decimal UnitPrice,
            decimal DiscRt1, decimal DiscRt2, decimal DiscRt3, decimal DiscRt4, decimal DiscRt5,
            ref decimal DiscAmt1, ref decimal DiscAmt2, ref decimal DiscAmt3, ref decimal DiscAmt4, ref decimal DiscAmt5)
        {
            decimal UnitPriceAfterDiscount = 0m;
            decimal RndUp = 0m;
            int DecPnt = 0;

            try
            {
                bool IsDisc2DiscOnDisc = (Sm.GetValue("Select ParValue From TblParameter where ParCode='SlsDisc2OnDisc'") == "Y") ? true : false;
                bool IsDisc3DiscOnDisc = (Sm.GetValue("Select ParValue From TblParameter where ParCode='SlsDisc3OnDisc'") == "Y") ? true : false;
                bool IsDisc4DiscOnDisc = (Sm.GetValue("Select ParValue From TblParameter where ParCode='SlsDisc4OnDisc'") == "Y") ? true : false;
                bool IsDisc5DiscOnDisc = (Sm.GetValue("Select ParValue From TblParameter where ParCode='SlsDisc5OnDisc'") == "Y") ? true : false;

                GetCurrencyRoundingInfo(CurCode, ref RndUp, ref DecPnt);

                DiscAmt1 = (UnitPrice * (DiscRt1 / 100));

                UnitPriceAfterDiscount = UnitPrice - DiscAmt1;

                if (IsDisc2DiscOnDisc)
                    DiscAmt2 = (UnitPriceAfterDiscount * (DiscRt2 / 100));
                else
                    DiscAmt2 = (UnitPrice * (DiscRt2 / 100));

                UnitPriceAfterDiscount = UnitPriceAfterDiscount - DiscAmt2;

                if (IsDisc3DiscOnDisc)
                    DiscAmt3 = (UnitPriceAfterDiscount * (DiscRt3 / 100));
                else
                    DiscAmt3 = (UnitPrice * (DiscRt3 / 100));

                UnitPriceAfterDiscount = UnitPriceAfterDiscount - DiscAmt3;

                if (IsDisc4DiscOnDisc)
                    DiscAmt4 = (UnitPriceAfterDiscount * (DiscRt4 / 100));
                else
                    DiscAmt4 = (UnitPrice * (DiscRt4 / 100));

                UnitPriceAfterDiscount = UnitPriceAfterDiscount - DiscAmt4;

                if (IsDisc5DiscOnDisc)
                    DiscAmt5 = (UnitPriceAfterDiscount * (DiscRt5 / 100));
                else
                    DiscAmt5 = (UnitPrice * (DiscRt5 / 100));

                UnitPriceAfterDiscount = UnitPriceAfterDiscount - DiscAmt5;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Error.StdMtd.ComputeSalesPriceAfterDisc:" + Exc.Message);
            }

            return Sm.Round(UnitPriceAfterDiscount, 2);
        }

        public static void SaveLogAction(string CtCode, string DbName, string DbHost, string MenuCode, string MenuDesc, string Action)
        {
            /*
                Action Glossary :
                    - I : Insert
                    - E : Edit
                    - F : Find
                    - R : Refresh
                    - S : Save
                    - C : Cancel
                    - N : Print
                    - O : Process
                    - D : Delete
                    - X : Excel
                    - V : CSV
             */

            if (Gv.IsActivityLogStored == false) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into `runsystemlog`.`tbllogactivity`(CtCode, DbName, DbHost, UserCode, MenuCode, MenuDesc, Action) ");
            SQL.AppendLine("Values(@CtCode, @DbName, @DbHost, @UserCode, @MenuCode, @MenuDesc, @Action); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DbName", DbName);
            Sm.CmParam<String>(ref cm, "@DbHost", DbHost);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MenuCode", MenuCode);
            Sm.CmParam<String>(ref cm, "@MenuDesc", MenuDesc);
            Sm.CmParam<String>(ref cm, "@Action", Action);

            ExecCommand(cm);

        }

        public static string[] UsedBudgetTable = { "Tb1", "Tb2", "Tb3", "Tb5", "Tb6", "Tb7", "Tb8", "Tb9", "Tb10", "Tb11", "Tb12" };

        public static string SelectUsedBudget()
        {
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");
            string mBudgetUsedFormula = GetParameter("BudgetUsedFormula");
            bool mIsBudgetTransactionCanUseOtherDepartmentBudget = GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");
            if (mBudgetUsedFormula.Length == 0) mBudgetUsedFormula = "1";

            var SQL = new StringBuilder();
            int index = 0;

            SQL.AppendLine("(0.00 ");
            if (mBudgetUsedFormula == "1")
            {
                for (int i = 0; i < UsedBudgetTable.Length; ++i)
                {
                    index = Int32.Parse(UsedBudgetTable[i].Replace("Tb", string.Empty));
                    if (index < 7 ||
                        (index == 7 && mIsCASUsedForBudget) ||
                        (index == 8 && mIsOutgoingPaymentUseBudget) ||
                        (index == 9 && mIsEmpClaimRequestUseBudget) ||
                        (index == 10 && mIsVoucherRequestSSUseBudget) ||
                        (index == 11 && mIsVoucherRequestPayrollUseBudget) ||
                        (index == 12 && mIsTravelRequestUseBudget)
                        )

                        SQL.AppendLine("+IfNull(" + UsedBudgetTable[i] + ".Amt, 0.00) ");
                    //if (index >= 7)
                    //{
                    //    if (
                    //        (index == 7 && mIsCASUsedForBudget) ||
                    //        (index == 8 && mIsOutgoingPaymentUseBudget) ||
                    //        (index == 9 && mIsEmpClaimRequestUseBudget) ||
                    //        (index == 10 && mIsVoucherRequestSSUseBudget) ||
                    //        (index == 11 && mIsVoucherRequestPayrollUseBudget) ||
                    //        (index == 12 && mIsTravelRequestUseBudget)
                    //        )
                    //    {
                    //        SQL.AppendLine("IfNull(" + UsedBudgetTable[i] + ".Amt, 0.00) ");
                    //        if (i != UsedBudgetTable.Length - 1)
                    //            SQL.AppendLine("+ ");
                    //    }
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("IfNull(" + UsedBudgetTable[i] + ".Amt, 0.00) ");
                    //    if (i != UsedBudgetTable.Length - 1)
                    //        if ((index == 6 &&
                    //            (
                    //                (index == 7 && mIsCASUsedForBudget) ||
                    //                (index == 8 && mIsOutgoingPaymentUseBudget) ||
                    //                (index == 9 && mIsEmpClaimRequestUseBudget) ||
                    //                (index == 10 && mIsVoucherRequestSSUseBudget) ||
                    //                (index == 11 && mIsVoucherRequestPayrollUseBudget) ||
                    //                (index == 12 && mIsTravelRequestUseBudget)
                    //            )
                    //            ) || index != 6)
                    //        SQL.AppendLine("+ ");
                    //}
                }
            }

            else
            {
                SQL.AppendLine("+IfNull(TbJN.Amt, 0.00) ");
            }
            SQL.AppendLine(") ");

            return SQL.ToString();
        }

        public static string ComputeUsedBudget(byte type, string DeptCode, string Yr, string Mth, string BCCode)
        {
            /*
                type : 1 = default, 2 = from reporting budget summary
                jangan lupa tambahin ke variable UsedBudgetTable
             */
            bool mIsBudget2YearlyFormat = GetParameterBoo("IsBudget2YearlyFormat");
            bool mIsMRBudgetBasedOnBudgetCategory = GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");
            bool mIsMRShowEstimatedPrice = GetParameterBoo("IsMRShowEstimatedPrice");
            bool mIsBudgetCalculateFromEstimatedPrice = GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            bool mIsBudgetTransactionCanUseOtherDepartmentBudget = GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");

            string mMRAvailableBudgetSubtraction = GetParameter("MRAvailableBudgetSubtraction");
            string mBudgetBasedOn = GetParameter("BudgetBasedOn");
            string mReqTypeForNonBudget = GetParameter("ReqTypeForNonBudget");
            string mMainCurCode = GetParameter("MainCurCode");
            string mBudgetUsedFormula = GetParameter("BudgetUsedFormula");
            bool mIsBudgetUseMRProcessInd = GetParameterBoo("IsBudgetUseMRProcessInd");
            bool mIsCASUseBudgetCategory = GetParameterBoo("IsCASUseBudgetCategory");
            if (mBudgetUsedFormula.Length == 0) mBudgetUsedFormula = "1";

            var Selection = new StringBuilder();
            var Closure = new StringBuilder();
            var Table = new StringBuilder();
            var SQL = new StringBuilder();

            if (mBudgetUsedFormula == "1")
            {
                #region Default Transaction

                Selection.AppendLine("Left Join ( ");
                Selection.AppendLine("    Select X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) Yr, ");
                if (!mIsBudget2YearlyFormat)
                    Selection.AppendLine("    Substring(X3.DocDt, 5, 2) Mth, ");
                else
                    Selection.AppendLine("    '00' As Mth, ");
                Selection.AppendLine("    Sum(X4.Amt) Amt ");

                Closure.AppendLine("    Group By X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) ");
                if (!mIsBudget2YearlyFormat)
                    Closure.AppendLine("    , Substring(X3.DocDt, 5, 2) ");

                Table.AppendLine(") TbXXX On A.BCCode = TbXXX.BCCode And A.DeptCode = TbXXX.DeptCode And A.Yr = TbXXX.Yr ");
                if (!mIsBudget2YearlyFormat)
                    Table.AppendLine("And A.Mth = TbXXX.Mth ");


                #region VR

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(Amt) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                }
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                else
                    SQL.AppendLine("    '00' As Mth, ");
                SQL.AppendLine("Amt, DocNo, DocDt ");
                SQL.AppendLine("From TblVoucherRequestHdr ");
                SQL.AppendLine("Where 0 = 0 ");
                SQL.AppendLine("And Find_In_Set(DocType, ");
                SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), ''))  ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And Status In('O', 'A') ");
                SQL.AppendLine("And ReqType Is Not Null ");
                SQL.AppendLine("And DeptCode2 Is null ");
                if (type == 1)
                {
                    SQL.AppendLine("        And DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                }
                if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                {
                    SQL.AppendLine("Union all ");
                    SQL.AppendLine("Select DeptCode2 as DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("Amt, DocNo, DocDt ");
                    SQL.AppendLine("From TblVoucherRequestHdr ");
                    SQL.AppendLine("Where 0 = 0 ");
                    SQL.AppendLine("And Find_In_Set(DocType, ");
                    SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), ''))  ");
                    SQL.AppendLine("And CancelInd = 'N' ");
                    SQL.AppendLine("And Status In('O', 'A') ");
                    SQL.AppendLine("And ReqType Is Not Null ");
                    SQL.AppendLine("And DeptCode2 Is Not null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("        And DeptCode2 = @DeptCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                    }
                }
                SQL.AppendLine(") T ");

                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1) SQL.AppendLine("        And T.DocNo <> @DocNo ");
                //SQL.AppendLine("        And Find_In_Set(DocType, ");
                //SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
                //SQL.AppendLine("        And CancelInd = 'N' ");
                //SQL.AppendLine("        And Status In ('O', 'A') ");
                //SQL.AppendLine("        And ReqType Is Not Null ");
                //SQL.AppendLine("        And ReqType <> "+type+" ");
                //if (type == 1)
                //{
                //    if (mIsBudgetTransactionCanUseOtherDepartmentBudget) SQL.AppendLine("And (DeptCode=@DeptCode Or DeptCode2=@DeptCode) ");
                //    else SQL.AppendLine("        And DeptCode = @DeptCode ");
                //    if (!mIsBudget2YearlyFormat)
                //        SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                //    else
                //        SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                //    SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                //}
                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) * -1 ");
                    SQL.AppendLine("- ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        );
                    if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        SQL.AppendLine(") Tb1 On A.BCCode = Tb1.BCCode And A.DeptCode = Tb1.DeptCode And A.Yr = Tb1.Yr ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("And A.Mth = Tb1.Mth ");
                    }
                    else
                    {
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb1"));
                    }
                }

                #endregion

                #region Travel Request All (Selain PHT)

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "C.")
                        .Replace("X2.", "B.")
                        .Replace("X3.", "A.")
                        .Replace("X4.Amt", "B.Amt1 + B.Amt2 + B.Amt3 + B.Amt4 + B.Amt5 + B.Amt6 + B.Detasering")
                        );
                }
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1) SQL.AppendLine("        And A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                if (type == 1)
                {
                    SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                }
                SQL.AppendLine("        And Not Exists (Select 1 From TblMenu Where Param = 'FrmTravelRequest5') ");
                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) ");
                    SQL.AppendLine("- ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                        .Replace("X1.", "C.")
                        .Replace("X2.", "B.")
                        .Replace("X3.", "A.")
                        );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb2"));
                }

                #endregion

                #region MR

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");
                        if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                            SQL.AppendLine("        Select Sum(T.Qty*Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End) ");
                        else
                            SQL.AppendLine("        Select Sum(T.Qty*T.UPrice) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.Amt", (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice) ? "T.Qty * Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End" : "T.Qty * T.UPrice")
                        );
                    }

                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("Select DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, DocDt ");
                    SQL.AppendLine("From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("Where A.DocNo=B.DocNo ");
                    SQL.AppendLine("And A.ReqType='1' ");
                    SQL.AppendLine("And B.CancelInd='N' ");
                    SQL.AppendLine("And B.Status<>'C' ");
                    if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                    SQL.AppendLine("And DeptCode2 Is null ");
                    if (type == 1)
                    {
                        if (mBudgetBasedOn == "1")
                        {
                            SQL.AppendLine(" And A.DeptCode=@DeptCode  ");
                        }
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                        {
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                        }
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                        SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    }
                    if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        SQL.AppendLine("Union all ");
                        SQL.AppendLine("Select DeptCode2 as DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                        else
                            SQL.AppendLine("    '00' As Mth, ");
                        SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, DocDt ");
                        SQL.AppendLine("From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                        SQL.AppendLine("Where A.DocNo=B.DocNo ");
                        SQL.AppendLine("And A.ReqType='1' ");
                        SQL.AppendLine("And B.CancelInd='N' ");
                        SQL.AppendLine("And B.Status<>'C' ");
                        if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                        SQL.AppendLine("And DeptCode2 Is Not null ");
                        if (type == 1)
                        {
                            if (mBudgetBasedOn == "1")
                            {
                                SQL.AppendLine(" And A.DeptCode2=@DeptCode  ");
                            }
                            if (mBudgetBasedOn == "2")
                                SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                            if (mIsMRBudgetBasedOnBudgetCategory)
                            {
                                SQL.AppendLine("        And A.BCCode=@BCCode ");
                            }
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                            else
                                SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                            SQL.AppendLine("       And A.DocNo<>@DocNo ");
                        }
                    }
                    SQL.AppendLine(") T ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    ), 0.00) ");
                        SQL.AppendLine("+ ");
                    }
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                       .Replace("X1.", "T.")
                       .Replace("X2.", "T.")
                       .Replace("X3.", "T.")
                       );

                        if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                        {
                            SQL.AppendLine(") Tb3 On A.BCCode = Tb3.BCCode And A.DeptCode = Tb3.DeptCode And A.Yr = Tb3.Yr ");
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("And A.Mth = Tb3.Mth ");
                        }
                        else
                        {
                            SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                        }
                    }
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    var AmtTxt = new StringBuilder();

                    AmtTxt.AppendLine("    X2.Amt -  ");
                    AmtTxt.AppendLine("    ( ");
                    AmtTxt.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    AmtTxt.AppendLine("          Case When X1.CurCode = '" + mMainCurCode + "' Then 1.00 Else ");
                    AmtTxt.AppendLine("          IfNull(( ");
                    AmtTxt.AppendLine("              Select Amt From TblCurrencyRate  ");
                    AmtTxt.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2='" + mMainCurCode + "'  ");
                    AmtTxt.AppendLine("              Order By RateDt Desc Limit 1  ");
                    AmtTxt.AppendLine("        ), 0.00) End ");
                    AmtTxt.AppendLine("      , 0.00) ");
                    AmtTxt.AppendLine("    ) ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");

                        #region Old Code

                        //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                        //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                        //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                        //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                        //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                        //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                        //if (mBudgetBasedOn == "1")
                        //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                        //if (mBudgetBasedOn == "2")
                        //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        //SQL.AppendLine("        And A.ReqType='1' ");
                        //SQL.AppendLine("        And B.CancelInd='N' ");
                        //SQL.AppendLine("        And B.Status<>'C' ");
                        //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                        #endregion

                        SQL.AppendLine("    Select Sum( ");
                        SQL.AppendLine(AmtTxt.ToString());
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "X2.")
                        .Replace("X2.", "X2.")
                        .Replace("X3.", "X2.")
                        .Replace("X4.Amt", AmtTxt.ToString())
                        );
                    }
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, ");
                    if (type == 2)
                        SQL.AppendLine("        Left(A.DocDt, 6) DocDt, A.BCCode, ");
                    SQL.AppendLine("        Sum( ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode='" + mMainCurCode + "' Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2='" + mMainCurCode + "'  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                        SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    }
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (type == 1)
                    {
                        if (mBudgetBasedOn == "1")
                            SQL.AppendLine("        And A.DeptCode=@DeptCode  ");
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    if (type == 2)
                        SQL.AppendLine("        , Left(A.DocDt, 6), A.BCCode ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");
                    if (type == 2)
                        SQL.AppendLine("    , X2.BCCode, X2.DocDt ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    ),0.00) ");
                        SQL.AppendLine("+ ");
                    }
                    else
                    {
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                    }
                }

                #endregion

                #region PO Qty Cancel

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "E.")
                        .Replace("X2.", "E.")
                        .Replace("X3.", "A.")
                        .Replace("X4.Amt", "A.Qty * D.UPrice")
                        );
                }

                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (type == 1)
                {
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                        SQL.AppendLine("        And E.BCCode=@BCCode ");
                }

                SQL.AppendLine("            And E.ReqType='1' ");
                if (type == 1)
                {
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And E.DocNo<>@DocNo ");
                }
                SQL.AppendLine("        Where A.CancelInd='N' ");
                if (type == 1)
                {
                    SQL.AppendLine("    ),0.00) ");
                    SQL.AppendLine("+ ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "E.")
                            .Replace("X2.", "E.")
                            .Replace("X3.", "A.")
                            );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb5"));
                }

                #endregion

                #region VC

                if (type == 1)
                {
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                }
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Case when A.AcType = 'D' Then IfNull(A.Amt, 0.00) Else IfNull(A.Amt*-1, 0.00) END As Amt ");
                if (type == 2)
                    SQL.AppendLine("        , D.DeptCode, D.BCCode, Left(A.DocDt, 6) DocDt ");
                SQL.AppendLine("        From TblVoucherHdr A  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblVoucherRequestHdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
                SQL.AppendLine("                And X1.DocType = '58' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("                AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("            INNER JOIN TblVoucherRequestHdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("                AND X1.DocType = '56' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N' ");
                SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
                SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1)
                {
                    SQL.AppendLine("        And A.DocNo <> @DocNo ");
                    SQL.AppendLine("        And D.DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                }
                SQL.AppendLine("    ) T ");

                if (type == 1)
                    SQL.AppendLine("), 0.00 ) ");
                else
                {
                    SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                            );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb6"));
                }

                #endregion

                #region CAS

                if (mIsCASUsedForBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(T.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                    }
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt, A.DocDt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    if (mIsCASUseBudgetCategory) SQL.AppendLine(" And B.BCCode=C.BCCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("            And C.BCCode = @BCCode ");
                        SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                        SQL.AppendLine("        Where 0 = 0 ");
                        SQL.AppendLine("        And A.DocNo != @DocNo ");
                    }
                    SQL.AppendLine("    ) T ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb7"));
                    }
                }

                #endregion

                #region OP

                if (mIsOutgoingPaymentUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblOutgoingPaymentHdr A ");
                    SQL.AppendLine("    Inner Join ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select T1.DocNo, IfNull(T3.DeptCode, IfNull(T4.DeptCode, '')) DeptCode ");
                    SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
                    SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        Left Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo = T3.DocNo ");
                    SQL.AppendLine("        Left Join TblPurchaseReturnInvoiceHdr T4 On T2.InvoiceDocNo = T4.DocNo ");
                    SQL.AppendLine("        Where T2.DNo = '001' ");
                    SQL.AppendLine("    ) B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("    And B.DeptCode = @DeptCode ");
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb8"));
                    }
                }

                #endregion

                #region EmpClaim

                if (mIsEmpClaimRequestUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }

                    SQL.AppendLine("    From TblEmpClaimHdr A ");
                    SQL.AppendLine("    Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        SQL.AppendLine("    And B.DeptCode = @DeptCode ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb9"));
                    }
                }

                #endregion

                #region VRSS

                if (mIsVoucherRequestSSUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblVoucherRequestSSHdr A ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1) SQL.AppendLine("    And A.BCCode = @BCCode ");
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                    }
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb10"));
                    }
                }

                #endregion

                #region VRPayroll

                if (mIsVoucherRequestPayrollUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblVoucherRequestPayrollHdr A ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb11"));
                    }
                }

                #endregion

                #region Travel Request PHT

                if (mIsTravelRequestUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("    - IfNull(( ");
                        SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                            .Replace("X1.", "C.")
                            .Replace("X2.", "B.")
                            .Replace("X3.", "A.")
                            .Replace("X4.Amt", "B.Amt1 + B.Amt2 + B.Amt3 + B.Amt4 + B.Amt5 + B.Amt6 + B.Detasering")
                            );
                    }
                    SQL.AppendLine("        From TblTravelRequestHdr A ");
                    SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("        Inner Join TblEmployee C On A.PICCode = C.EmpCode ");
                    SQL.AppendLine("        Where 0 = 0 ");
                    if (type == 1) SQL.AppendLine("        And A.DocNo <> @DocNo ");
                    SQL.AppendLine("        And A.CancelInd = 'N' ");
                    SQL.AppendLine("        And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("        And IfNull(A.BCCode, '') = IfNull(@BCCode, '') ");
                    }
                    SQL.AppendLine("        And Exists (Select 1 From TblMenu Where Param = 'FrmTravelRequest5') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    ), 0.00) ");
                    }
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "C.")
                            .Replace("X2.", "B.")
                            .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb12"));
                    }
                }

                #endregion

                #endregion
            }
            else
            {
                #region based on Journal

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt)) Amt ");
                }
                else
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) Yr, ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(T1.DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("    Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt) End) Amt ");
                }

                SQL.AppendLine("From TblJournalHdr T1 ");
                SQL.AppendLine("Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    And T1.CancelInd = 'N' ");

                if (Yr.Length > 0 && Mth.Length > 0)
                {
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(T1.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(T1.DocDt, 4) = Left(@DocDt, 4) ");
                }

                SQL.AppendLine("Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                if (DeptCode.Length > 0) SQL.AppendLine("    And T3.DeptCode = @DeptCode ");
                SQL.AppendLine("Inner Join TblBudgetCategory T4 On ");
                SQL.AppendLine("    T2.AcNo = T4.AcNo  ");
                if (BCCode.Length > 0) SQL.AppendLine("And Find_In_Set(T4.BCCode, @BCCode) ");
                SQL.AppendLine("Inner Join TblCOA T5 On T2.AcNo = T5.AcNo And T5.ActInd = 'Y' ");

                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) ");
                }
                else
                {
                    SQL.AppendLine("    Group By T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        Closure.AppendLine("    , Substring(T1.DocDt, 5, 2) ");
                    SQL.AppendLine(") TbJN On A.BCCode = TbJN.BCCode And A.DeptCode = TbJN.DeptCode And A.Yr = TbJN.Yr ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("And A.Mth = TbJN.Mth ");
                }

                #endregion
            }

            return SQL.ToString();
        }

        public static decimal ComputeAvailableBudget(
            string DocNo, string DocDt, string SiteCode, string DeptCode, string BCCode
            )
        {
            bool mIsBudget2YearlyFormat = GetParameterBoo("IsBudget2YearlyFormat");
            bool mIsMRBudgetBasedOnBudgetCategory = GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");

            string mMRAvailableBudgetSubtraction = GetParameter("MRAvailableBudgetSubtraction");
            string mBudgetBasedOn = GetParameter("BudgetBasedOn");
            string mReqTypeForNonBudget = GetParameter("ReqTypeForNonBudget");
            string mMainCurCode = GetParameter("MainCurCode");
            string mBudgetSourceFormula = Sm.GetParameter("BudgetSourceFormula");

            var SQL = new StringBuilder();
            decimal AvailableBudgetAmt = 0m;
            string data = string.Empty;

            if (mIsMRBudgetBasedOnBudgetCategory)
            {
                if (mBudgetSourceFormula == "1")
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        Amt2 ");
                    else
                        SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00) ");
                }
                else
                {
                    if (BCCode.Length > 0 && DocDt.Length > 0)
                    {
                        SQL.AppendLine("Select ");
                        SQL.AppendLine("    IfNull(( ");
                        SQL.AppendLine("        Select T.Amt From ( ");
                        SQL.AppendLine("    	Select Sum(IfNull(T1.Amt, 0.00) - (IfNull(T2.CASAmt, 0.00) + IfNull(T3.JournalAmt, 0.00)) - (IfNull(T4.TransferredAmt, 0.00)) - (IfNull(T6.RecvVdAutoDOAmt, 0.00)) + (IfNull(T5.ReceivedAmt, 0.00))) As Amt ");
                        SQL.AppendLine("    	From ( ");
                        SQL.AppendLine("    		Select SUm(B.Amt" + DocDt.Substring(4, 2) + ") Amt ");
                        SQL.AppendLine("    		From TblCompanyBudgetPlanHdr A ");
                        SQL.AppendLine("    		Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And A.CancelInd = 'N'  ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null  ");
                        SQL.AppendLine("    			And A.CompletedInd = 'Y'  ");
                        SQL.AppendLine("    			And A.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("            Inner Join TblCostCenter C On A.CCCode = C.CCCode And C.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T1 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        SQL.AppendLine("    		Select Sum(B.Amt) CASAmt ");
                        SQL.AppendLine("    		From TblCashAdvanceSettlementHdr A ");
                        SQL.AppendLine("    		Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null  ");
                        SQL.AppendLine("    			And A.Status = 'A'  ");
                        SQL.AppendLine("    			And A.CancelInd = 'N'  ");
                        SQL.AppendLine("    			And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    			And B.ItCode Is Not Null ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.CCtCode = @CCtCode ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X3.ItCode ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo ");
                        SQL.AppendLine("    		) C On A.CCCode = C.CCCode And B.ItCode = C.ItCode ");
                        SQL.AppendLine("    		Inner Join TblCostCenter D On A.CCCode = D.CCCode And D.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T2 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        //SQL.AppendLine("    		Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
                        //SQL.AppendLine("    		Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End As Amt, E.DeptCode, A.CCCode ");
                        SQL.AppendLine("    		Select Case D.AcType When 'D' Then SUM(B.DAmt - B.CAmt) Else SUM(B.CAmt - B.DAmt) End As JournalAmt");
                        SQL.AppendLine("    		From TblJournalHdr A ");
                        SQL.AppendLine("    		Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    			And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
                        SQL.AppendLine("    			And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
                        SQL.AppendLine("    			And A.DocNo Not In ");
                        SQL.AppendLine("    			    ( ");
                        SQL.AppendLine("    			    Select A.JournalDocNo ");
                        SQL.AppendLine("    			    From TblRecvVdHdr A ");
                        SQL.AppendLine("    			    Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    			    Union All ");
                        SQL.AppendLine("    			    Select A2.JournalDocNo ");
                        SQL.AppendLine("    			    From TblRecvVdHdr A ");
                        SQL.AppendLine("    			    Inner Join TblRecvVdDtl A2 On A.DocNo = A2.DocNo ");
                        SQL.AppendLine("    			    Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    			   ) ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X2.AcNo ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		) C On A.CCCode = C.CCCode And B.AcNo = C.AcNo ");
                        SQL.AppendLine("    		Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                        SQL.AppendLine("    		Inner Join TblCostCenter E On A.CCCode = E.CCCode And E.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T3 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Sum(X2.TransferredAmt) TransferredAmt ");
                        SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
                        SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
                        SQL.AppendLine("            And X1.Status In ('A') ");
                        SQL.AppendLine("            And X1.CancelInd = 'N' ");
                        SQL.AppendLine("            And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("            AND X1.CCCode = @CCCode");
                        SQL.AppendLine("            AND (X1.Mth = Substring(@DocDt, 5, 2) Or X1.Mth2 = Substring(@DocDt, 5, 2)) ");
                        SQL.AppendLine("    ) T4 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Sum(X2.TransferredAmt) ReceivedAmt ");
                        SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
                        SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
                        SQL.AppendLine("            And X1.Status In ('A') ");
                        SQL.AppendLine("            And X1.CancelInd = 'N' ");
                        SQL.AppendLine("            And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("             AND X1.CCCode2 = @CCCode");
                        SQL.AppendLine("            AND (X1.Mth = Substring(@DocDt, 5, 2) Or X1.Mth2 = Substring(@DocDt, 5, 2)) ");
                        SQL.AppendLine("    ) T5 On 0=0 ");

                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        SQL.AppendLine("    	    Select Sum(If(A.POInd = 'Y', ifnull(F.UPrice* B.Qty, 0.00), ifnull(B.UPrice * B.Qty, 0.00))) RecvVdAutoDOAmt ");
                        SQL.AppendLine("    	    From TblRecvVdHdr A ");
                        SQL.AppendLine("    	    Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    	       And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    	       And B.Status = 'A' ");
                        SQL.AppendLine("    	       And B.CancelInd = 'N' ");
                        SQL.AppendLine("    	    Inner Join TblDODeptHdr C On A.DocNo = C.RecvVdDocNo And C.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    	       And C.CCCode Is Not Null ");
                        SQL.AppendLine("    	       And Find_In_Set(C.CCCode, @CCCode) ");
                        SQL.AppendLine("    	    Inner Join TblItemCostCategory D On B.ItCode = D.ItCode And C.CCCode = D.CCCode ");
                        SQL.AppendLine("    	    Inner Join TblCostCategory E On D.CCtCode = E.CCtCode And E.AcNo Is Not Null ");
                        SQL.AppendLine("    	    Inner Join ");
                        SQL.AppendLine("    	    ( ");
                        SQL.AppendLine("    	        Select T1.DocNo, T1.DNo, T3.UPrice ");
                        SQL.AppendLine("    	        From TblPODtl T1 ");
                        SQL.AppendLine("    	        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo= T2.DocNo  ");
                        SQL.AppendLine("    	            And T1.PORequestDNo= T2.DNo And T2.Status = 'A' And T2.CancelInd = 'N' ");
                        SQL.AppendLine("    	            And Concat(T1.DocNo, T1.DNo) In ( ");
                        SQL.AppendLine("    	                Select Distinct Concat(X.PODocNo, X.PODNo) ");
                        SQL.AppendLine("    	                From TblRecvVdDtl X ");
                        SQL.AppendLine("    	                Inner Join TblRecvVdHdr X1 On X.DocNo = X1.DocNo ");
                        SQL.AppendLine("    	                    And Left(X1.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    	                    And X.Status = 'A' ");
                        SQL.AppendLine("    	                    And X.CancelInd = 'N' ");
                        SQL.AppendLine("    	               Inner Join TblDODeptHdr X2 On X1.DocNo = X2.RecvVdDocNo And X2.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    	                   And X2.CCCode Is Not Null ");
                        SQL.AppendLine("    	                   And Find_In_Set(X2.CCCode, @CCCode) ");
                        SQL.AppendLine("    	            )  ");
                        SQL.AppendLine("    	       Inner Join TblQtDtl T3 On T2.QtDocNo = T3.DocNo And T2.QtDNo = T3.DNo ");
                        SQL.AppendLine("    	       Where T1.CancelInd = 'N' ");
                        SQL.AppendLine("    	    ) F On B.PODocNo = F.DocNo And B.PODNo = F.DNo ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X2.AcNo ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		) G On C.CCCode = G.CCCode And E.AcNo = G.AcNo ");
                        SQL.AppendLine("    		Inner Join TblCostCenter H On C.CCCode = H.CCCode And H.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T6 On 0 = 0 ");

                        SQL.AppendLine("    ) T ");
                        SQL.AppendLine("     ), 0.00) ");
                    }
                    else
                    {
                        SQL.AppendLine("Select 0.00 ");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Select ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblBudget ");
                SQL.AppendLine("        Where DeptCode=@DeptCode ");
                SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                SQL.AppendLine("    ), 0.00) ");
            }

            SQL.AppendLine("+ ");
            SQL.AppendLine("IfNull( ");
            string Yr = string.Empty;
            string Mth = string.Empty;

            if (DocDt.Length >= 6)
            {
                Yr = Sm.Left(DocDt, 4);
                Mth = DocDt.Substring(4, 2);
            }
            SQL.AppendLine(ComputeUsedBudget(1, DeptCode, Yr, Mth, BCCode));
            SQL.AppendLine(", 0.00) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mBudgetBasedOn == "1")
                Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            if (mBudgetBasedOn == "2")
                Sm.CmParam<String>(ref cm, "@DeptCode", SiteCode);

            string CCtCode = Sm.GetValue("Select CCtCode From TblBudgetCategory Where BCCode = @Param; ", BCCode);
            string CCCode = Sm.GetValue("Select Group_Concat(Distinct A.CCCode) From TblCostCategory A Inner Join TblCostCenter B On A.CCCode = B.CCCode Where CCtCode = @Param1 And B.ActInd='Y' And ProfitCenterCode Is Not Null And ProfitCenterCode In (Select Distinct ProfitCenterCode From TblGroupProfitCenter Where GrpCode In (Select GrpCode From TblUser Where UserCode=@Param2 ))", CCtCode, Gv.CurrentUserCode, "");

            Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", CCtCode);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);

            data = Sm.GetValue(cm);
            if (data.Length > 0) AvailableBudgetAmt = Decimal.Parse(data);

            return AvailableBudgetAmt;
        }

        public static string[] UsedBudgetTable2 = { "Tb3" };

        public static string SelectUsedBudget2()
        {
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");
            string mBudgetUsedFormula = GetParameter("BudgetUsedFormula");
            bool mIsBudgetTransactionCanUseOtherDepartmentBudget = GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");
            if (mBudgetUsedFormula.Length == 0) mBudgetUsedFormula = "1";

            var SQL = new StringBuilder();
            int index = 0;

            SQL.AppendLine("(0.00 ");
            if (mBudgetUsedFormula == "1")
            {
                for (int i = 0; i < UsedBudgetTable2.Length; ++i)
                {
                    index = Int32.Parse(UsedBudgetTable2[i].Replace("Tb", string.Empty));
                    if (index < 7 ||
                        (index == 7 && mIsCASUsedForBudget) ||
                        (index == 8 && mIsOutgoingPaymentUseBudget) ||
                        (index == 9 && mIsEmpClaimRequestUseBudget) ||
                        (index == 10 && mIsVoucherRequestSSUseBudget) ||
                        (index == 11 && mIsVoucherRequestPayrollUseBudget) ||
                        (index == 12 && mIsTravelRequestUseBudget)
                        )

                        SQL.AppendLine("+IfNull(" + UsedBudgetTable2[i] + ".Amt, 0.00) ");
                }
            }

            else
            {
                SQL.AppendLine("+IfNull(TbJN.Amt, 0.00) ");
            }
            SQL.AppendLine(") ");

            return SQL.ToString();
        }

        public static string ComputeUsedBudget2(byte type, string DeptCode, string Yr, string Mth, string BCCode)
        {
            /*
                type : 1 = default, 2 = from reporting budget summary
                jangan lupa tambahin ke variable UsedBudgetTable
             */
            bool mIsBudget2YearlyFormat = GetParameterBoo("IsBudget2YearlyFormat");
            bool mIsMRBudgetBasedOnBudgetCategory = GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");
            bool mIsMRShowEstimatedPrice = GetParameterBoo("IsMRShowEstimatedPrice");
            bool mIsBudgetCalculateFromEstimatedPrice = GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            bool mIsBudgetTransactionCanUseOtherDepartmentBudget = GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");

            string mMRAvailableBudgetSubtraction = GetParameter("MRAvailableBudgetSubtraction");
            string mBudgetBasedOn = GetParameter("BudgetBasedOn");
            string mReqTypeForNonBudget = GetParameter("ReqTypeForNonBudget");
            string mMainCurCode = GetParameter("MainCurCode");
            string mBudgetUsedFormula = GetParameter("BudgetUsedFormula");
            bool mIsBudgetUseMRProcessInd = GetParameterBoo("IsBudgetUseMRProcessInd");
            bool mIsCASUseBudgetCategory = GetParameterBoo("IsCASUseBudgetCategory");
            if (mBudgetUsedFormula.Length == 0) mBudgetUsedFormula = "1";
            string mFiscalYearRange = Sm.GetParameter("FiscalYearRange");
            string[] mFiscalYear = mFiscalYearRange.Split(',');

            var Selection = new StringBuilder();
            var Closure = new StringBuilder();
            var Table = new StringBuilder();
            var SQL = new StringBuilder();

            if (mBudgetUsedFormula == "1")
            {
                #region Default Transaction

                Selection.AppendLine("Left Join ( ");
                Selection.AppendLine("    Select X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) Yr, ");
                if (!mIsBudget2YearlyFormat)
                    Selection.AppendLine("    Substring(X3.DocDt, 5, 2) Mth, ");
                else
                    Selection.AppendLine("    '00' As Mth, ");
                Selection.AppendLine("    Sum(X4.Amt) Amt ");

                Closure.AppendLine("    Group By X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) ");
                if (!mIsBudget2YearlyFormat)
                    Closure.AppendLine("    , Substring(X3.DocDt, 5, 2) ");

                Table.AppendLine(") TbXXX On A.BCCode = TbXXX.BCCode And A.DeptCode = TbXXX.DeptCode And A.Yr = TbXXX.Yr ");
                if (!mIsBudget2YearlyFormat)
                    Table.AppendLine("And A.Mth = TbXXX.Mth ");

                #region MR

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");
                        if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                            SQL.AppendLine("        Select Sum(T.Qty*Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End) ");
                        else
                            SQL.AppendLine("        Select Sum(T.Qty*T.UPrice) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.Amt", (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice) ? "T.Qty * Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End" : "T.Qty * T.UPrice")
                        );
                    }

                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("Select A.DeptCode, A.BCCode, @Yr As Yr, ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(A.DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, A.DocDt ");
                    SQL.AppendLine("From TblMaterialRequestHdr A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("Where A.ReqType='1' ");
                    SQL.AppendLine("And B.CancelInd='N' ");
                    SQL.AppendLine("And B.Status<>'C' ");
                    if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                    SQL.AppendLine("And A.DeptCode2 Is null ");
                    if (type == 1)
                    {
                        if (mBudgetBasedOn == "1")
                        {
                            SQL.AppendLine(" And A.DeptCode=@DeptCode  ");
                        }
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                        {
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                        }
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                        {
                            if (mFiscalYearRange.Length == 0)
                                SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                            else
                                SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
                        }
                        SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    }
                    if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        SQL.AppendLine("Union all ");
                        SQL.AppendLine("Select DeptCode2 as DeptCode, BCCode, @Yr As Yr,  ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                        else
                            SQL.AppendLine("    '00' As Mth, ");
                        SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, DocDt ");
                        SQL.AppendLine("From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                        SQL.AppendLine("Where A.DocNo=B.DocNo ");
                        SQL.AppendLine("And A.ReqType='1' ");
                        SQL.AppendLine("And B.CancelInd='N' ");
                        SQL.AppendLine("And B.Status<>'C' ");
                        if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                        SQL.AppendLine("And DeptCode2 Is Not null ");
                        if (type == 1)
                        {
                            if (mBudgetBasedOn == "1")
                            {
                                SQL.AppendLine(" And A.DeptCode2=@DeptCode  ");
                            }
                            if (mBudgetBasedOn == "2")
                                SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                            if (mIsMRBudgetBasedOnBudgetCategory)
                            {
                                SQL.AppendLine("        And A.BCCode=@BCCode ");
                            }
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                            else
                            {
                                if (mFiscalYearRange.Length == 0)
                                    SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                                else
                                    SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
                            }
                            SQL.AppendLine("       And A.DocNo<>@DocNo ");
                        }
                    }
                    SQL.AppendLine(") T ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    ), 0.00) ");
                        //SQL.AppendLine("+ ");
                    }
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                       .Replace("X1.", "T.")
                       .Replace("X2.", "T.")
                       .Replace("X3.", "T.")
                       );

                        if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                        {
                            SQL.AppendLine(") Tb3 On A.BCCode = Tb3.BCCode And A.DeptCode = Tb3.DeptCode And A.Yr = Tb3.Yr ");
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("And A.Mth = Tb3.Mth ");
                        }
                        else
                        {
                            SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                        }
                    }
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    var AmtTxt = new StringBuilder();

                    AmtTxt.AppendLine("    X2.Amt -  ");
                    AmtTxt.AppendLine("    ( ");
                    AmtTxt.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    AmtTxt.AppendLine("          Case When X1.CurCode = '" + mMainCurCode + "' Then 1.00 Else ");
                    AmtTxt.AppendLine("          IfNull(( ");
                    AmtTxt.AppendLine("              Select Amt From TblCurrencyRate  ");
                    AmtTxt.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2='" + mMainCurCode + "'  ");
                    AmtTxt.AppendLine("              Order By RateDt Desc Limit 1  ");
                    AmtTxt.AppendLine("        ), 0.00) End ");
                    AmtTxt.AppendLine("      , 0.00) ");
                    AmtTxt.AppendLine("    ) ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");

                        SQL.AppendLine("    Select Sum( ");
                        SQL.AppendLine(AmtTxt.ToString());
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "X2.")
                        .Replace("X2.", "X2.")
                        .Replace("X3.", "X2.")
                        .Replace("X4.Amt", AmtTxt.ToString())
                        );
                    }
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, ");
                    if (type == 2)
                        SQL.AppendLine("        Left(E.DocDt, 6) DocDt, A.BCCode, ");
                    SQL.AppendLine("        Sum( ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode='" + mMainCurCode + "' Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2='" + mMainCurCode + "'  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N' ");
                    SQL.AppendLine("            And B.Status In ('A', 'O') ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1' ");

                    if (type == 1)
                    {
                        SQL.AppendLine("            And A.DocNo <> @DocNo ");

                        if (mBudgetBasedOn == "1")
                            SQL.AppendLine("        And A.DeptCode=@DeptCode  ");
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");

                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                        {
                            if (mFiscalYearRange.Length == 0)
                                SQL.AppendLine("        And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                            else
                                SQL.AppendLine("        And Left(E.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
                        }
                    }

                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    if (type == 2)
                        SQL.AppendLine("        , Left(E.DocDt, 6), A.BCCode ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");
                    if (type == 2)
                        SQL.AppendLine("    , X2.BCCode, X2.DocDt ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    ),0.00) ");
                    }
                    else
                    {
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                    }
                }

                #endregion

                #endregion
            }
            else
            {
                #region based on Journal

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt)) Amt ");
                }
                else
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) Yr, ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(T1.DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("    Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt) End) Amt ");
                }

                SQL.AppendLine("From TblJournalHdr T1 ");
                SQL.AppendLine("Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    And T1.CancelInd = 'N' ");

                if (Yr.Length > 0 && Mth.Length > 0)
                {
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(T1.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(T1.DocDt, 4) = Left(@DocDt, 4) ");
                }

                SQL.AppendLine("Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                if (DeptCode.Length > 0) SQL.AppendLine("    And T3.DeptCode = @DeptCode ");
                SQL.AppendLine("Inner Join TblBudgetCategory T4 On ");
                SQL.AppendLine("    T2.AcNo = T4.AcNo  ");
                if (BCCode.Length > 0) SQL.AppendLine("And Find_In_Set(T4.BCCode, @BCCode) ");
                SQL.AppendLine("Inner Join TblCOA T5 On T2.AcNo = T5.AcNo And T5.ActInd = 'Y' ");

                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) ");
                }
                else
                {
                    SQL.AppendLine("    Group By T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        Closure.AppendLine("    , Substring(T1.DocDt, 5, 2) ");
                    SQL.AppendLine(") TbJN On A.BCCode = TbJN.BCCode And A.DeptCode = TbJN.DeptCode And A.Yr = TbJN.Yr ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("And A.Mth = TbJN.Mth ");
                }

                #endregion
            }

            return SQL.ToString();
        }

        public static string QueryBudgetTransfer()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select X.Yr, X.Mth, X.DeptCode, X.BCCode, Sum(X.Amt) Amt ");
            SQL.AppendLine("    From( ");
            SQL.AppendLine("        Select If(T3.ParValue Is Null, Left(T1.DocDt, 4), ");
            SQL.AppendLine("        If(Left(T1.DocDt, 6) < Concat(Left(T1.DocDt, 4), Left(T3.ParValue, 2)), (Left(T1.DocDt, 4) - 1), Left(T1.DocDt, 4))) Yr, ");
            SQL.AppendLine("        T1.DeptCode, T2.BCCode, T2.Amt * -1 As Amt, ");
            SQL.AppendLine("        '00' As Mth ");
            SQL.AppendLine("        From TblBudgetTransferHdr T1 ");
            SQL.AppendLine("        Inner Join TblBudgetTransferDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.Status = 'A' And T1.CancelInd = 'N' ");
            SQL.AppendLine("        Left Join TblParameter T3 On T3.ParCode = 'FiscalYearRange' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select If(T3.ParValue Is Null, Left(T1.DocDt, 4), ");
            SQL.AppendLine("        If(Left(T1.DocDt, 6) < Concat(Left(T1.DocDt, 4), Left(T3.ParValue, 2)), (Left(T1.DocDt, 4) - 1), Left(T1.DocDt, 4))) Yr, ");
            SQL.AppendLine("        T1.DeptCode2 As DeptCode, T2.BCCode2 As BCCode, T2.Amt, ");
            SQL.AppendLine("        '00' As Mth ");
            SQL.AppendLine("        From TblBudgetTransferHdr T1 ");
            SQL.AppendLine("        Inner Join TblBudgetTransferDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.Status = 'A' And T1.CancelInd = 'N' ");
            SQL.AppendLine("        Left Join TblParameter T3 On T3.ParCode = 'FiscalYearRange' ");
            SQL.AppendLine("    ) X ");
            SQL.AppendLine("    Group By X.Yr, X.Mth, X.DeptCode, X.BCCode ");

            return SQL.ToString();
        }

        public static decimal ComputeAvailableBudget2(
            string DocNo, string DocDt, string SiteCode, string DeptCode, string BCCode
            )
        {
            bool mIsBudget2YearlyFormat = GetParameterBoo("IsBudget2YearlyFormat");
            bool mIsMRBudgetBasedOnBudgetCategory = GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            bool mIsCASUsedForBudget = GetParameterBoo("IsCASUsedForBudget");
            bool mIsOutgoingPaymentUseBudget = GetParameterBoo("IsOutgoingPaymentUseBudget");
            bool mIsVoucherRequestSSUseBudget = GetParameterBoo("IsVoucherRequestSSUseBudget");
            bool mIsVoucherRequestPayrollUseBudget = GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            bool mIsTravelRequestUseBudget = GetParameterBoo("IsTravelRequestUseBudget");
            bool mIsEmpClaimRequestUseBudget = GetParameterBoo("IsEmpClaimRequestUseBudget");

            string mMRAvailableBudgetSubtraction = GetParameter("MRAvailableBudgetSubtraction");
            string mBudgetBasedOn = GetParameter("BudgetBasedOn");
            string mReqTypeForNonBudget = GetParameter("ReqTypeForNonBudget");
            string mMainCurCode = GetParameter("MainCurCode");
            string mBudgetSourceFormula = Sm.GetParameter("BudgetSourceFormula");
            string mFiscalYearRange = Sm.GetParameter("FiscalYearRange");
            string[] mFiscalYear = mFiscalYearRange.Split(',');

            var SQL = new StringBuilder();
            decimal AvailableBudgetAmt = 0m;
            string data = string.Empty;

            if (mIsMRBudgetBasedOnBudgetCategory)
            {
                if (mBudgetSourceFormula == "1")
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");

                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        A.Amt2 ");
                    else
                        SQL.AppendLine("        SUM(IfNull(E.Amt, A.Amt2 - IfNull(F.Amt, 0.00))) "); // ini ambil dari TblBudgetDtl (bukan langsung ambil dari Budget Summary) karena di BudgetTransfer itu, aku langsung update nilai Amt2 nya (gak on the fly). tapi di buat ifnull, karena bisa jadi ada budget yg di generate nya itu dari budget transfer, bukan dari budget request -> wed

                    SQL.AppendLine("        From TblBudgetSummary A ");

                    if (mIsBudget2YearlyFormat)
                    {
                        SQL.AppendLine("        Left Join TblBudgetRequestHdr B On A.Yr = B.Yr And A.DeptCode = B.DeptCode And B.CancelInd = 'N' And B.Status = 'A' ");
                        SQL.AppendLine("        Left Join TblBudgetRequestDtl C On B.DocNo = C.DocNo And A.BCCode = C.BCCode ");
                        SQL.AppendLine("        Left Join TblBudgetHdr D On B.DocNo = D.BudgetRequestDocNo And D.CancelInd = 'N' And D.Status != 'C' ");
                        SQL.AppendLine("        Left Join TblBudgetDtl E On D.DocNo = E.DocNo And C.DNo = E.BudgetRequestDNo ");
                        SQL.AppendLine("        Left Join ( ");
                        SQL.AppendLine(QueryBudgetTransfer());
                        SQL.AppendLine("        ) F On A.Yr = F.Yr And A.DeptCode = F.DeptCode And A.BCCode = F.BCCode ");
                    }

                    SQL.AppendLine("        Where A.DeptCode=@DeptCode AND A.BCCode = @BCCode ");

                    if (mFiscalYearRange.Length == 0)
                        SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    else
                        SQL.AppendLine("        And A.Yr=@Yr ");

                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");

                    SQL.AppendLine("    ), 0.00) ");
                }
                else
                {
                    if (BCCode.Length > 0 && DocDt.Length > 0)
                    {
                        SQL.AppendLine("Select ");
                        SQL.AppendLine("    IfNull(( ");
                        SQL.AppendLine("        Select T.Amt From ( ");
                        SQL.AppendLine("    	Select Sum(IfNull(T1.Amt, 0.00) - (IfNull(T2.CASAmt, 0.00) + IfNull(T3.JournalAmt, 0.00)) - (IfNull(T4.TransferredAmt, 0.00)) - (IfNull(T6.RecvVdAutoDOAmt, 0.00)) + (IfNull(T5.ReceivedAmt, 0.00))) As Amt ");
                        SQL.AppendLine("    	From ( ");
                        SQL.AppendLine("    		Select SUm(B.Amt" + DocDt.Substring(4, 2) + ") Amt ");
                        SQL.AppendLine("    		From TblCompanyBudgetPlanHdr A ");
                        SQL.AppendLine("    		Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And A.CancelInd = 'N'  ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null  ");
                        SQL.AppendLine("    			And A.CompletedInd = 'Y'  ");
                        if (mFiscalYearRange.Length == 0)
                            SQL.AppendLine("    			And A.Yr = Left(@DocDt, 4) ");
                        else
                            SQL.AppendLine("                And A.Yr = @Yr ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("            Inner Join TblCostCenter C On A.CCCode = C.CCCode And C.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T1 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        SQL.AppendLine("    		Select Sum(B.Amt) CASAmt ");
                        SQL.AppendLine("    		From TblCashAdvanceSettlementHdr A ");
                        SQL.AppendLine("    		Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null  ");
                        SQL.AppendLine("    			And A.Status = 'A'  ");
                        SQL.AppendLine("    			And A.CancelInd = 'N'  ");
                        SQL.AppendLine("    			And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    			And B.ItCode Is Not Null ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.CCtCode = @CCtCode ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X3.ItCode ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo ");
                        SQL.AppendLine("    		) C On A.CCCode = C.CCCode And B.ItCode = C.ItCode ");
                        SQL.AppendLine("    		Inner Join TblCostCenter D On A.CCCode = D.CCCode And D.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T2 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        //SQL.AppendLine("    		Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
                        //SQL.AppendLine("    		Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End As Amt, E.DeptCode, A.CCCode ");
                        SQL.AppendLine("    		Select Case D.AcType When 'D' Then SUM(B.DAmt - B.CAmt) Else SUM(B.CAmt - B.DAmt) End As JournalAmt");
                        SQL.AppendLine("    		From TblJournalHdr A ");
                        SQL.AppendLine("    		Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    			And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    			And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
                        SQL.AppendLine("    			And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
                        SQL.AppendLine("    			And A.DocNo Not In ");
                        SQL.AppendLine("    			    ( ");
                        SQL.AppendLine("    			    Select A.JournalDocNo ");
                        SQL.AppendLine("    			    From TblRecvVdHdr A ");
                        SQL.AppendLine("    			    Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    			    Union All ");
                        SQL.AppendLine("    			    Select A2.JournalDocNo ");
                        SQL.AppendLine("    			    From TblRecvVdHdr A ");
                        SQL.AppendLine("    			    Inner Join TblRecvVdDtl A2 On A.DocNo = A2.DocNo ");
                        SQL.AppendLine("    			    Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    			   ) ");
                        SQL.AppendLine("    			And A.CCCode Is Not Null ");
                        SQL.AppendLine("                And Find_In_Set(A.CCCode, @CCCode) ");
                        SQL.AppendLine("                And B.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X2.AcNo ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		) C On A.CCCode = C.CCCode And B.AcNo = C.AcNo ");
                        SQL.AppendLine("    		Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                        SQL.AppendLine("    		Inner Join TblCostCenter E On A.CCCode = E.CCCode And E.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T3 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Sum(X2.TransferredAmt) TransferredAmt ");
                        SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
                        SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
                        SQL.AppendLine("            And X1.Status In ('A') ");
                        SQL.AppendLine("            And X1.CancelInd = 'N' ");
                        SQL.AppendLine("            And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("            AND X1.CCCode = @CCCode");
                        SQL.AppendLine("            AND (X1.Mth = Substring(@DocDt, 5, 2) Or X1.Mth2 = Substring(@DocDt, 5, 2)) ");
                        SQL.AppendLine("    ) T4 On 0 = 0 ");
                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Sum(X2.TransferredAmt) ReceivedAmt ");
                        SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
                        SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
                        SQL.AppendLine("            And X1.Status In ('A') ");
                        SQL.AppendLine("            And X1.CancelInd = 'N' ");
                        SQL.AppendLine("            And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("             AND X1.CCCode2 = @CCCode");
                        SQL.AppendLine("            AND (X1.Mth = Substring(@DocDt, 5, 2) Or X1.Mth2 = Substring(@DocDt, 5, 2)) ");
                        SQL.AppendLine("    ) T5 On 0=0 ");

                        SQL.AppendLine("Left Join ");
                        SQL.AppendLine("    	( ");
                        SQL.AppendLine("    	    Select Sum(If(A.POInd = 'Y', ifnull(F.UPrice* B.Qty, 0.00), ifnull(B.UPrice * B.Qty, 0.00))) RecvVdAutoDOAmt ");
                        SQL.AppendLine("    	    From TblRecvVdHdr A ");
                        SQL.AppendLine("    	    Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    	       And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    	       And B.Status = 'A' ");
                        SQL.AppendLine("    	       And B.CancelInd = 'N' ");
                        SQL.AppendLine("    	    Inner Join TblDODeptHdr C On A.DocNo = C.RecvVdDocNo And C.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    	       And C.CCCode Is Not Null ");
                        SQL.AppendLine("    	       And Find_In_Set(C.CCCode, @CCCode) ");
                        SQL.AppendLine("    	    Inner Join TblItemCostCategory D On B.ItCode = D.ItCode And C.CCCode = D.CCCode ");
                        SQL.AppendLine("    	    Inner Join TblCostCategory E On D.CCtCode = E.CCtCode And E.AcNo Is Not Null ");
                        SQL.AppendLine("    	    Inner Join ");
                        SQL.AppendLine("    	    ( ");
                        SQL.AppendLine("    	        Select T1.DocNo, T1.DNo, T3.UPrice ");
                        SQL.AppendLine("    	        From TblPODtl T1 ");
                        SQL.AppendLine("    	        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo= T2.DocNo  ");
                        SQL.AppendLine("    	            And T1.PORequestDNo= T2.DNo And T2.Status = 'A' And T2.CancelInd = 'N' ");
                        SQL.AppendLine("    	            And Concat(T1.DocNo, T1.DNo) In ( ");
                        SQL.AppendLine("    	                Select Distinct Concat(X.PODocNo, X.PODNo) ");
                        SQL.AppendLine("    	                From TblRecvVdDtl X ");
                        SQL.AppendLine("    	                Inner Join TblRecvVdHdr X1 On X.DocNo = X1.DocNo ");
                        SQL.AppendLine("    	                    And Left(X1.DocDt, 6) = Left(@DocDt, 6) ");
                        SQL.AppendLine("    	                    And X.Status = 'A' ");
                        SQL.AppendLine("    	                    And X.CancelInd = 'N' ");
                        SQL.AppendLine("    	               Inner Join TblDODeptHdr X2 On X1.DocNo = X2.RecvVdDocNo And X2.RecvVdDocNo Is Not Null ");
                        SQL.AppendLine("    	                   And X2.CCCode Is Not Null ");
                        SQL.AppendLine("    	                   And Find_In_Set(X2.CCCode, @CCCode) ");
                        SQL.AppendLine("    	            )  ");
                        SQL.AppendLine("    	       Inner Join TblQtDtl T3 On T2.QtDocNo = T3.DocNo And T2.QtDNo = T3.DNo ");
                        SQL.AppendLine("    	       Where T1.CancelInd = 'N' ");
                        SQL.AppendLine("    	    ) F On B.PODocNo = F.DocNo And B.PODNo = F.DNo ");
                        SQL.AppendLine("    		Inner Join ");
                        SQL.AppendLine("    		( ");
                        SQL.AppendLine("    			Select Distinct X1.CCCode, X2.AcNo ");
                        SQL.AppendLine("    			From TblCompanyBudgetPlanHdr X1 ");
                        SQL.AppendLine("    			Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                        SQL.AppendLine("    				And X1.CancelInd = 'N' ");
                        SQL.AppendLine("    				And X1.CompletedInd = 'Y' ");
                        SQL.AppendLine("    				And X1.CCCode Is Not Null ");
                        SQL.AppendLine("    				And X1.Yr = Left(@DocDt, 4) ");
                        SQL.AppendLine("                    And Find_In_Set(X1.CCCode, @CCCode) ");
                        SQL.AppendLine("                    And X2.AcNo In (Select AcNo From TblCostCategory Where CCtCode = @CCtCode) ");
                        SQL.AppendLine("    		) G On C.CCCode = G.CCCode And E.AcNo = G.AcNo ");
                        SQL.AppendLine("    		Inner Join TblCostCenter H On C.CCCode = H.CCCode And H.DeptCode = @DeptCode ");
                        SQL.AppendLine("    	) T6 On 0 = 0 ");

                        SQL.AppendLine("    ) T ");
                        SQL.AppendLine("     ), 0.00) ");
                    }
                    else
                    {
                        SQL.AppendLine("Select 0.00 ");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Select ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblBudget ");
                SQL.AppendLine("        Where DeptCode=@DeptCode ");
                SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                SQL.AppendLine("    ), 0.00) ");
            }

            SQL.AppendLine("+ 0.00 - ");
            SQL.AppendLine("IfNull( ");
            string Yr = string.Empty;
            string Mth = string.Empty;

            if (DocDt.Length >= 6)
            {
                Yr = Sm.Left(DocDt, 4);
                Mth = DocDt.Substring(4, 2);
                if (mFiscalYearRange.Length > 0)
                {
                    string DocDt1 = Sm.Left(DocDt, 6);
                    string DocDt2 = Yr + mFiscalYear[0];
                    if (decimal.Parse(DocDt1) < decimal.Parse(DocDt2))
                        Yr = (decimal.Parse(Yr) - 1).ToString();
                }
            }
            SQL.AppendLine(ComputeUsedBudget2(1, DeptCode, Yr, Mth, BCCode));
            SQL.AppendLine(", 0.00) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mBudgetBasedOn == "1")
                Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            if (mBudgetBasedOn == "2")
                Sm.CmParam<String>(ref cm, "@DeptCode", SiteCode);

            string CCtCode = Sm.GetValue("Select CCtCode From TblBudgetCategory Where BCCode = @Param; ", BCCode);
            string CCCode = Sm.GetValue("Select Group_Concat(Distinct A.CCCode) From TblCostCategory A Inner Join TblCostCenter B On A.CCCode = B.CCCode Where CCtCode = @Param1 And B.ActInd='Y' And ProfitCenterCode Is Not Null And ProfitCenterCode In (Select Distinct ProfitCenterCode From TblGroupProfitCenter Where GrpCode In (Select GrpCode From TblUser Where UserCode=@Param2 ))", CCtCode, Gv.CurrentUserCode, "");

            Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", CCtCode);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            data = Sm.GetValue(cm);
            if (data.Length > 0) AvailableBudgetAmt = Decimal.Parse(data);

            return AvailableBudgetAmt;
        }

        #region tambah method by IBL

        //DocDt = End Date Filter, InvestmentCode = Filter Investment (boleh string.empty)
        public static void ComputeStockAccumulaton(
            string DocDt, string InvestmentCtCode, string InvestmentCode,
            ref iGrid Grd, int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost
            )
        {
            var lacc = PrepDataStockAccumulation(DocDt, InvestmentCtCode, InvestmentCode);
            ProcessAccumulationStock(ref Grd, ref lacc, ColCreateDt, ColInvCode, ColInvCtCode, ColInvType, ColAccQty, ColAccAcqCost, ColMovingAvgPrice, ColMovingAvgCost);
        }

        public static List<StockAccumulation> PrepDataStockAccumulation(string DocDt, string InvestmentCtCode, string InvestmentCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockAccumulation>();
            var l2 = new List<StockAccumulation>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, InvestmentCode, new string[] { "C.PortofolioId", "C.PortofolioName" });

            SQL.AppendLine("Select A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType, E.OptDesc,  ");
            SQL.AppendLine("IfNull(A.Qty, 0.00) As Qty, IfNull(A.Qty, 0.00) * IfNull(D.UPrice, 0.00) AcquisitionCost  ");
            SQL.AppendLine("From TblInvestmentStockMovement A  ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode  ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source = D.Source ");
            SQL.AppendLine("	And A.InvestmentCode = D.InvestmentCode ");
            SQL.AppendLine("Inner Join TblOption E On A.InvestmentType = E.OptCode And E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            if (DocDt.Length > 0) SQL.AppendLine("And A.DocDt <= @DocDt  ");
            SQL.AppendLine("And C.InvestmentCtCode = @InvestmentCtCode ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("	Select Distinct(X1.DocNo) ");
            SQL.AppendLine("	From TblInvestmentStockMovement X1 ");
            SQL.AppendLine("	Where 1=1 ");
            if (DocDt.Length > 0) SQL.AppendLine("	And X1.DocDt <= @DocDt ");
            SQL.AppendLine("	Group By X1.DocType, X1.InvestmentType, X1.DocNo, X1.DNo, X1.DocDt, X1.BankAcCode, X1.Lot, X1.BatchNo, X1.Source   ");
            SQL.AppendLine("	Having Sum(X1.Qty)=0.00 ");
            SQL.AppendLine(") ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter + "Order By A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType;",
                new string[]
                {
                    "CreateDt",
                    "InvestmentCode", "InvestmentCtCode", "InvestmentType", "OptDesc", "Qty",
                    "AcquisitionCost",
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockAccumulation()
                        {
                            CreateDt = Convert.ToDecimal(Sm.DrStr(dr, 0)),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentCtCode = Sm.DrStr(dr, 2),
                            InvestmentType = Sm.DrStr(dr, 3),
                            InvestmentTypeName = Sm.DrStr(dr, 4),
                            Qty = Sm.DrDec(dr, 5),
                            AcquisitionCost = Sm.DrDec(dr, 6),
                        }
                    );
               }, false
            );

            var lg = l.GroupBy(g => new { CreateDt = g.CreateDt, InvCode = g.InvestmentCode, InvCtCode = g.InvestmentCtCode, InvType = g.InvestmentType, InvTypeNm = g.InvestmentTypeName })
                .Select(group => new
                {
                    CreateDt = group.Key.CreateDt,
                    InvestmentCode = group.Key.InvCode,
                    InvestmentCtCode = group.Key.InvCtCode,
                    InvestmentType = group.Key.InvType,
                    InvestmentTypeName = group.Key.InvTypeNm,
                });

            foreach (var x in lg)
            {
                decimal mQty = 0m, mAcquisitionPrice = 0m;
                foreach (var y in l)
                {
                    if (y.CreateDt <= x.CreateDt &&
                        y.InvestmentCode == x.InvestmentCode &&
                        y.InvestmentCtCode == x.InvestmentCtCode &&
                        y.InvestmentType == x.InvestmentType)
                    {
                        mQty += y.Qty;
                        mAcquisitionPrice += y.AcquisitionCost;
                    }
                }
                l2.Add(
                    new StockAccumulation()
                    {
                        CreateDt2 = x.CreateDt.ToString(),
                        InvestmentCode = x.InvestmentCode,
                        InvestmentCtCode = x.InvestmentCtCode,
                        InvestmentType = x.InvestmentType,
                        InvestmentTypeName = x.InvestmentTypeName,
                        Qty = mQty,
                        AcquisitionCost = mAcquisitionPrice,
                    }
                );
            }

            return l2;
        }

        private static void ProcessAccumulationStock(
            ref iGrid Grd, ref List<StockAccumulation> lacc,
            int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost
            )
        {
            foreach (var x in lacc)
            {
                for (int row = 0; row < Grd.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(Grd, row, 0).Length > 0 &&
                        Sm.GetGrdStr(Grd, row, ColCreateDt) == x.CreateDt2 &&
                        Sm.GetGrdStr(Grd, row, ColInvCode) == x.InvestmentCode &&
                        Sm.GetGrdStr(Grd, row, ColInvCtCode) == x.InvestmentCtCode &&
                        Sm.GetGrdStr(Grd, row, ColInvType) == x.InvestmentTypeName)
                    {
                        Grd.Cells[row, ColAccQty].Value = x.Qty;
                        Grd.Cells[row, ColAccAcqCost].Value = x.AcquisitionCost;
                        Grd.Cells[row, ColMovingAvgPrice].Value = x.AcquisitionCost / x.Qty;
                        Grd.Cells[row, ColMovingAvgCost].Value = x.Qty * (x.AcquisitionCost / x.Qty);
                    }
                }
            }
        }

        #endregion

        public static string GetFormatNumber(string ParCode, string ParCodeReset, string DocDt,
            ref int RunningNumberStartPos, ref int RunningNumberEndPos,
            ref int ResetStartPos, ref int ResetLength, ref string ResetValue)
        {
            string MonthRoman = string.Empty;
            switch (DocDt.Substring(4, 2))
            {
                case "01": MonthRoman = "   I"; break;
                case "02": MonthRoman = "  II"; break;
                case "03": MonthRoman = " III"; break;
                case "04": MonthRoman = "  IV"; break;
                case "05": MonthRoman = "   V"; break;
                case "06": MonthRoman = "  VI"; break;
                case "07": MonthRoman = " VII"; break;
                case "08": MonthRoman = "VIII"; break;
                case "09": MonthRoman = "  IX"; break;
                case "10": MonthRoman = "   X"; break;
                case "11": MonthRoman = "  XI"; break;
                case "12": MonthRoman = " XII"; break;
            }

            string MyFormat = Sm.GetValue("Select ParValue from tblparameter Where ParCode='" + ParCode + "'");
            string MyResetFormat = Sm.GetValue("Select ParValue from tblparameter Where ParCode='" + ParCodeReset + "'");
            for (int Index = 0; Index < MyFormat.Length; Index++)
            {
                if (MyFormat.Length >= (Index + MyResetFormat.Length))
                    if (MyFormat.Substring(Index, MyResetFormat.Length) == MyResetFormat)
                        ResetStartPos = Index;
            }

            MyFormat = MyFormat.Replace("%YYY", DocDt.Substring(0, 4));
            MyFormat = MyFormat.Replace("%Y", DocDt.Substring(2, 2));
            MyFormat = MyFormat.Replace("%RMM", MonthRoman);
            MyFormat = MyFormat.Replace("%M", DocDt.Substring(4, 2));
            MyFormat = MyFormat.Replace("%D", DocDt.Substring(6, 2));

            MyResetFormat = MyResetFormat.Replace("%YYY", DocDt.Substring(0, 4));
            MyResetFormat = MyResetFormat.Replace("%Y", DocDt.Substring(2, 2));
            MyResetFormat = MyResetFormat.Replace("%RMM", MonthRoman);
            MyResetFormat = MyResetFormat.Replace("%M", DocDt.Substring(4, 2));
            MyResetFormat = MyResetFormat.Replace("%D", DocDt.Substring(6, 2));

            ResetValue = MyResetFormat;
            ResetLength = MyResetFormat.Length;

            RunningNumberStartPos = -1;
            RunningNumberEndPos = -1;
            for (int Index = 0; Index < MyFormat.Length; Index++)
            {
                if (MyFormat.Substring(Index, 1) == "#")
                {
                    if (RunningNumberStartPos == -1)
                    {
                        RunningNumberStartPos = Index;
                        RunningNumberEndPos = Index;
                    }
                    else
                        RunningNumberEndPos = Index;
                }
            }
            return MyFormat;
        }

        public static void ComputeQtyBasedOnConvertionFormula(
            string ConvertType, iGrid Grd, int Row, int ColItCode,
            int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        {
            try
            {
                if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
                {
                    decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
                    if (Convert != 0)
                        Grd.Cells[Row, ColQty2].Value = Sm.FormatNum(Convert * Sm.GetGrdDec(Grd, Row, ColQty1), 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void ComputeQtyBasedOnConvertionFormula(
           string ConvertType, iGrid Grd, int Row, int ColItCode,
           int ColQty1, int ColQty2, int ColQty3, int ColUom1, int ColUom2, int ColUom3)
        {
            if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
            {
                decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
                if (Convert != 0)
                {
                    Grd.Cells[Row, ColQty2].Value = Sm.FormatNum(Convert * Sm.GetGrdDec(Grd, Row, ColQty1), 0);
                    if (Sm.CompareGrdStr(Grd, Row, ColUom2, Grd, Row, ColUom3))
                        Sm.CopyGrdValue(Grd, Row, ColQty3, Grd, Row, ColQty2);
                }
            }
        }

        public static string CopyStr(string Value, int Number)
        {
            string result = string.Empty;
            for (int Index = 0; Index < Number; Index++)
                result = result + Value;
            return result;
        }

        public static void SetFormAdditionalSetting(Form FormAdditional)
        {
            FormAdditional.TopLevel = true;
            FormAdditional.ShowDialog();
        }

        public static void ShowDataInGridNoAutoSize(
            ref iGrid Grd, ref MySqlCommand cm, string SQL, string[] ColumnTitle,
            RefreshGrdData rgd, bool ShowNoDataInd, bool GroupInd, bool Editable
)
        {
            ClearGrd(Grd, Editable);
            if (GroupInd)
            {
                Grd.GroupObject.Clear();
                Grd.Group();
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                }
                else
                {
                    int Row = 0;
                    if (!Editable) Grd.Rows.Count = 0;
                    Grd.ProcessTab = true;
                    Grd.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd.Rows.Add();
                        rgd(dr, Grd, c, Row);
                        Row++;
                    }
                    if (GroupInd)
                    {
                        Grd.GroupObject.Add(1);
                        Grd.Group();
                        SetGrdAlwaysShowSubTotal(Grd);
                    }
                }
                Grd.EndUpdate();
                dr.Close();
            }
        }

        public static void SetGrdHdrSpanCol(iGrid Grd, int HeaderRow, int HeaderCol, int SpanCol, string TopTitle, string[] BottomTitle)
        {
            if (SpanCol > 0) Grd.Header.Cells[HeaderRow + 1, HeaderCol].SpanCols = SpanCol;

            Grd.Header.Cells[HeaderRow + 1, HeaderCol].Value = TopTitle;
            for (int Index = HeaderCol; Index < (HeaderCol + SpanCol); Index++)
                Grd.Header.Cells[HeaderRow, Index].Value = BottomTitle[Index - HeaderCol];

        }

        public static void SetGrdHdrSpanRow(iGrid Grd, int HeaderRow, int HeaderCol, int SpanRow, string Title)
        {
            if (SpanRow > 0) Grd.Header.Cells[HeaderRow, HeaderCol].SpanRows = SpanRow;

            Grd.Header.Cells[HeaderRow, HeaderCol].Value = Title;

        }

        #endregion

        #region Upload / Download File

        public static void ChooseUploadedFile(ref CheckEdit ChkFile, ref OpenFileDialog OD, ref TextEdit TxtFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static bool DownloadFileWithProgressBar(
            ref byte[] downloadedData, ref System.Windows.Forms.ProgressBar PbUpload,
            string FormatFTPClient, string FTPAddress, string port, string filename, string username,
            string password, string FileShared
            )
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = "Connecting...";
                Application.DoEvents();
                string requestUriString = "ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename;
                if (FormatFTPClient == "1") requestUriString = "ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename;

                FtpWebRequest request = FtpWebRequest.Create(requestUriString) as FtpWebRequest;

                //Now get the actual data
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Get the file size first (for progress bar)
                int dataLength = (int)request.GetResponse().ContentLength;
                if (dataLength < 0) dataLength = 100;

                //this.Text = "Downloading File...";
                Application.DoEvents();

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static void DownloadFile(ref byte[] downloadedData,
            string FormatFTPClient, string FTPAddress, string port, string filename, string username,
            string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                string requestUriString = "ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename;
                if (FormatFTPClient == "1") requestUriString = "ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename;

                FtpWebRequest request = FtpWebRequest.Create(requestUriString) as FtpWebRequest;

                //Get the file size first (for progress bar)
                int dataLength = (int)request.GetResponse().ContentLength;

                //this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        public static void DownloadAction(ref SaveFileDialog SFD, ref byte[] downloadedData, string FileName)
        {
            SFD.FileName = FileName;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (FileName.Length > 0 && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        public static bool UploadFile(
            bool paramToAllowUploadFile, string FileName, string FormatFTPClient, string RenameTo,
            string HostAddrForFTPClient, string PortForFTPClient,
            string SharedFolderForFTPClient, string UsernameForFTPClient,
            string PasswordForFTPClient, bool IsUploadFileRenamed
            )
        {
            try
            {
                if (paramToAllowUploadFile)
                {
                    if (FileName != "openFileDialog1" && FileName.Length > 0)
                    {
                        FtpWebRequest request;

                        FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
                        long mFileSize = toUpload.Length;
                        string requestUriString = string.Format(@"ftp://{0}:{1}/{2}", HostAddrForFTPClient, PortForFTPClient, toUpload.Name);
                        if (FormatFTPClient == "1") requestUriString = string.Format(@"ftp://{0}:{1}/{2}/{3}", HostAddrForFTPClient, PortForFTPClient, SharedFolderForFTPClient, IsUploadFileRenamed ? RenameTo : toUpload.Name);

                        request = (FtpWebRequest)WebRequest.Create(requestUriString);

                        request.Method = WebRequestMethods.Ftp.UploadFile;
                        request.Credentials = new NetworkCredential(UsernameForFTPClient, PasswordForFTPClient);
                        request.KeepAlive = false;
                        request.UseBinary = true;
                        if (IsUploadFileRenamed) request.RenameTo = RenameTo;

                        Stream ftpStream = request.GetRequestStream();

                        FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

                        int length = 1024;
                        byte[] buffer = new byte[length];
                        int bytesRead = 0;

                        do
                        {
                            bytesRead = file.Read(buffer, 0, length);
                            ftpStream.Write(buffer, 0, bytesRead);

                            byte[] buffers = new byte[10240];
                            int read;
                            while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                            {
                                ftpStream.Write(buffers, 0, read);

                            }
                        }
                        while (bytesRead != 0);

                        file.Close();
                        ftpStream.Close();

                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Sm.StdMsg(mMsgType.Warning, e.Message);
                return false;
            }
        }

        public static void InsertUploadedFile(
            bool paramToAllowUploadFile, ref List<UploadFileClass> l,
            string TblName
            )
        {
            //Tested in BudgetTransfer
            if (!paramToAllowUploadFile) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            int i = 0;

            SQL.AppendLine("Insert Ignore Into {0}(DocNo, DNo, FileName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            foreach (var x in l)
            {
                if (IsFirst) IsFirst = false;
                else SQL.AppendLine(", ");

                SQL.AppendLine("(@DocNo__" + i.ToString() + ", ");
                SQL.AppendLine("@DNo__" + i.ToString() + ", ");
                SQL.AppendLine("@FileName__" + i.ToString() + ", ");
                SQL.AppendLine("@CreateBy, CurrentDateTime() ");
                SQL.AppendLine(") ");

                CmParam<String>(ref cm, "@DocNo__" + i.ToString(), x.DocNo);
                CmParam<String>(ref cm, "@DNo__" + i.ToString(), x.DNo);
                CmParam<String>(ref cm, "@FileName__" + i.ToString(), x.FileName);

                i++;
            }

            SQL.AppendLine("; ");

            cm.CommandText = string.Format(SQL.ToString(), TblName);
            CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            ExecCommands(cml);
            cml.Clear();
        }

        public static void UpdateUploadedFile(
           bool paramToAllowUploadFile, ref List<UploadFileClass> l,
           string TblName
           )
        {
            //Tested in MaterialRequest5, PORequest
            if (!paramToAllowUploadFile) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            int i = 0;

            foreach (var y in l)
            {
                SQL.AppendLine("Select DocNo From {0} Where DocNo = @DocNo; ");
                if (y.FileName != null && y.FileName.Length > 0) SQL.AppendLine("Update {0} Set FileName = @FileName Where DocNo = @DocNo; ");
                if (y.FileName2 != null && y.FileName2.Length > 0) SQL.AppendLine("Update {0} Set FileName2 = @FileName2 Where DocNo = @DocNo; ");
                if (y.FileName3 != null && y.FileName3.Length > 0) SQL.AppendLine("Update {0} Set FileName3 = @FileName3 Where DocNo = @DocNo; ");

                CmParam<String>(ref cm, "@DocNo", y.DocNo);
                if (y.FileName != null && y.FileName.Length > 0) CmParam<String>(ref cm, "@FileName", y.FileName);
                if (y.FileName2 != null && y.FileName2.Length > 0) CmParam<String>(ref cm, "@FileName2", y.FileName2);
                if (y.FileName3 != null && y.FileName3.Length > 0) CmParam<String>(ref cm, "@FileName3", y.FileName3);
            }


            cm.CommandText = string.Format(SQL.ToString(), TblName);
            CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            ExecCommands(cml);
            cml.Clear();
        }


        #region validation

        public static bool IsFTPClientDataNotValid(
            bool paramToAllowUploadFile, string FileName,
            string HostAddrForFTPClient, string SharedFolderForFTPClient,
            string UsernameForFTPClient, string PortForFTPClient)
        {
            if (paramToAllowUploadFile)
            {
                if (FileName.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "There is no file to upload.");
                    return true;
                }

                string msg = string.Empty;
                bool flagged = false;

                if (!flagged && HostAddrForFTPClient.Length == 0)
                {
                    msg = "host address";
                    flagged = true;
                }

                if (!flagged && SharedFolderForFTPClient.Length == 0)
                {
                    msg = "shared folder";
                    flagged = true;
                }

                if (!flagged && UsernameForFTPClient.Length == 0)
                {
                    msg = "shared folder's username";
                    flagged = true;
                }

                if (!flagged && PortForFTPClient.Length == 0)
                {
                    msg = "port number";
                    flagged = true;
                }

                if (flagged)
                {
                    Sm.StdMsg(mMsgType.Warning, "Parameter for " + msg + " is empty.");
                    return true;
                }
            }

            return false;
        }

        public static bool IsFileSizeNotValid(
            bool paramToAllowUploadFile, string FileName,
            bool isUploadOnDetail, int Row, ref FileInfo f,
            string FileSizeMaxUploadFTPClient
            )
        {
            if (!paramToAllowUploadFile) return false;

            if (FileName.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no file to upload.");
            }

            long bytes = 0;
            double kilobytes = 0;
            double megabytes = 0;
            double gibabytes = 0;

            if (f.Exists)
            {
                bytes = f.Length;
                kilobytes = (double)bytes / 1024;
                megabytes = kilobytes / 1024;
                gibabytes = megabytes / 1024;
            }

            if (megabytes > Double.Parse(FileSizeMaxUploadFTPClient))
            {
                string msg = "File's too large to upload";
                if (isUploadOnDetail)
                {
                    msg += " in row " + (Row + 1).ToString();
                }
                Sm.StdMsg(mMsgType.Warning, msg);
                return true;
            }

            return false;
        }

        public static bool IsFileNameAlreadyExisted(
            bool paramToAllowUploadFile, string FileName,
            string TblName, string ColumnName
            )
        {
            if (!paramToAllowUploadFile) return false;

            if (FileName.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no file to upload.");
            }

            var toUpload = new FileInfo(string.Format(@"{0}", FileName));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select {0} From {1} ");
            SQL.AppendLine("Where {0} = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (IsDataExist(string.Format(SQL.ToString(), ColumnName, TblName), toUpload.Name))
            {
                Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") has been used by other document.");
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Old

        #region Field
        private const string _userPwd = "AndersEmmanuelTan141721";

        private static Excel.Application objApp;
        private static Excel.Workbooks objBooks;
        private static Excel.Workbook objBook;
        private static Excel.Sheets objSheets;
        private static Excel.Worksheet objSheet;

        private static string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
        private static string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
        private static string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
        private static string[] ribuan = new string[5] { "", "ribu", "juta", "miliar", "triliyun" };

        #endregion

        #region Delegate
        public delegate void RefreshLue();
        public delegate void BtnClick(object sender, KeyEventArgs e);
        public delegate void RefreshLue1(ref LookUpEdit Lue);
        public delegate void RefreshLue2(ref LookUpEdit Lue, string Param);
        public delegate void RefreshLue3(ref LookUpEdit Lue, string Param1, string Param2);
        public delegate void RefreshLue4(ref LookUpEdit Lue, string Param1, string Param2, string Param3);
        public delegate void ShowData<T>(iGrid Grd, int Row, List<T> l);
        #endregion

        public static void BtnFindToolTip(ref Button BtnRefresh, ref Button BtnChoose, ref Button BtnExit)
        {
            BtnToolTip(ref BtnRefresh, "Shortcut key:<F5>");
            BtnToolTip(ref BtnChoose, "Shortcut key:<F6>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnInputToolTip(ref Button BtnSave, ref Button BtnExit)
        {
            BtnToolTip(ref BtnSave, "Shortcut key:<F6>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnInputToolTip(ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel, ref Button BtnPrint, ref Button BtnExit)
        {
            BtnToolTip(ref BtnFind, "Shortcut key:<F7>");
            BtnToolTip(ref BtnInsert, "Shortcut key:<F2>");
            BtnToolTip(ref BtnEdit, "Shortcut key:<F3>");
            BtnToolTip(ref BtnSave, "Shortcut key:<F6>");
            BtnToolTip(ref BtnPrint, "Shortcut key:<F8>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnInputToolTip(ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel, ref Button BtnExit)
        {
            BtnToolTip(ref BtnFind, "Shortcut key:<F7>");
            BtnToolTip(ref BtnInsert, "Shortcut key:<F2>");
            BtnToolTip(ref BtnEdit, "Shortcut key:<F3>");
            BtnToolTip(ref BtnSave, "Shortcut key:<F6>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnReportToolTip(ref Button BtnRefresh, ref Button BtnPrint, ref Button BtnExcel, ref Button BtnExit)
        {
            BtnToolTip(ref BtnRefresh, "Shortcut key:<F5>");
            BtnToolTip(ref BtnPrint, "Shortcut key:<F8>");
            BtnToolTip(ref BtnExcel, "Shortcut key:<F9>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnChartToolTip(ref Button BtnRefresh, ref Button BtnExit)
        {
            BtnToolTip(ref BtnRefresh, "Shortcut key:<F5>");
            BtnToolTip(ref BtnExit, "Shortcut key:<F12>");
        }

        public static void BtnToolTip(ref Button Btn, string ToolTips)
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip1 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 500;
            toolTip1.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip1.SetToolTip(Btn, ToolTips);
        }

        public static void ChkCancelIndCheckedChanged(SimpleButton Btn, MemoExEdit Mee, CheckEdit Chk)
        {
            if (Btn.Enabled)
            {
                if (Chk.Checked)
                {
                    if (Mee.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        Chk.Checked = false;
                    }
                }
                else
                    Mee.EditValue = null;
            }
        }

        public static void ClearGrd(iGrid Grd, bool Editable)
        {
            Grd.Rows.Count = 0;
            if (Editable) Grd.Rows.Count = 1;
        }

        public static void CmParam<T>(ref SqlCommand cm, string ColumnName, T Value)
        {
            if (string.IsNullOrEmpty(Value.ToString()))
                cm.Parameters.AddWithValue(ColumnName, DBNull.Value);
            else
                cm.Parameters.AddWithValue(ColumnName, Value);
        }

        public static void CmParam<T>(ref MySqlCommand cm, string ColumnName, bool IsDate, Byte Direction, T Value)
        {
            if (string.IsNullOrEmpty(Value.ToString()))
                cm.Parameters.AddWithValue(ColumnName, DBNull.Value);
            else
            {
                if (IsDate)
                    cm.Parameters.AddWithValue(ColumnName, (Value.ToString()).Substring(0, 8));
                else
                    cm.Parameters.AddWithValue(ColumnName, Value);
            }
            switch (Direction)
            {
                case 1: cm.Parameters[ColumnName].Direction = ParameterDirection.Input; break;
                case 2: cm.Parameters[ColumnName].Direction = ParameterDirection.Output; break;
                case 3: cm.Parameters[ColumnName].Direction = ParameterDirection.InputOutput; break;
                    //case 4 : cm.Parameters[ColumnName].Direction = ParameterDirection.ReturnValue; break;
            }
        }

        public static void CmParamStr(ref SqlCommand cm, string ColumnName, string Value)
        {
            if (string.IsNullOrEmpty(Value))
                cm.Parameters.AddWithValue(ColumnName, "");
            else
                cm.Parameters.AddWithValue(ColumnName, Value);
        }

        public static string CompanyLogo()
        {
            //Load Company Logo
            string FileName = string.Empty;
            try
            {
                FileName =
                    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                    @"\" + Gv.CompanyLogoFile;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return FileName;
        }

        public static string CompanyLogo(string Value)
        {
            //Load Company Logo
            string FileName = string.Empty;
            try
            {
                switch (Value)
                {
                    case "2":
                        FileName = Gv.CompanyLogoFile2;
                        break;
                    default:
                        FileName = Gv.CompanyLogoFile;
                        break;
                }
                FileName = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\" + FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return FileName;
        }

        public static string CompanyLogo2(string LogoName)
        {
            //Load Company Logo
            string FileName = string.Empty;
            try
            {
                FileName =
                    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                    @"\" + LogoName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return FileName;
        }

        public static DateTime ConvertDate(string Value)
        {
            DateTime DateValue;
            DateTime.TryParseExact(Value.Substring(0, 8), "yyyyMMdd", null, DateTimeStyles.None, out DateValue);
            return DateValue;
            //return new DateTime(
            //            Int32.Parse(Value.Substring(0, 4)),
            //            Int32.Parse(Value.Substring(4, 2)),
            //            Int32.Parse(Value.Substring(6, 2)),
            //            0, 0, 0
            //            );
        }

        private static string Decrypt(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the decoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[]
            byte[] DataToDecrypt = Convert.FromBase64String(Message);

            // Step 5. Attempt to decrypt the string
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the decrypted string in UTF8 format
            return UTF8.GetString(Results);
        }

        public static string GeneratePwd(string UserCode, string UserName)
        {
            return DateTime.Now.Millisecond.ToString() + UserCode.Substring(1, 1) + DateTime.Now.Second.ToString() + UserName.Substring(UserName.Length - 1, 1) + DateTime.Now.Minute.ToString();
        }

        public static string DecryptUserPwd(string Passphrase)
        {
            return Decrypt(_userPwd, Passphrase);
        }

        public static decimal GetPriceAfterDiscount(decimal UnitPrice, decimal Disc1, decimal Disc2, decimal Disc3, decimal DiscPromo)
        {
            decimal UnitPriceResult = UnitPrice - (UnitPrice * (Disc1 / 100));
            UnitPriceResult = UnitPriceResult - (UnitPriceResult * (Disc2 / 100));
            UnitPriceResult = UnitPriceResult - (UnitPriceResult * (Disc3 / 100));
            UnitPriceResult = UnitPriceResult - (UnitPriceResult * (DiscPromo / 100));
            return UnitPriceResult;
        }

        public static string DrStr(SqlDataReader dr, int c)
        {
            // Get datareader for string
            return (dr[c] == DBNull.Value) ? "" : dr.GetString(c);
        }

        public static decimal DrDec(SqlDataReader dr, int c)
        {
            // Get datareader for decimal
            if (dr[c] == DBNull.Value)
                return 0m;
            else
                return dr.GetDecimal(c);
        }

        public static double DrDbl(SqlDataReader dr, int c)
        {
            // Get datareader for decimal
            if (dr[c] == DBNull.Value)
                return 0;
            else
                return dr.GetDouble(c);
        }

        public static int DrInt(SqlDataReader dr, int c)
        {
            // Get datareader for integer
            if (dr[c] == DBNull.Value)
                return 0;
            else
                try
                {
                    return dr.GetInt32(c);
                }
                catch
                {
                    return dr.GetInt16(c);
                }
        }

        public static void ExecQuery(string query)
        {
            var cm = new MySqlCommand() { CommandText = query };
            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
            cml.Clear();
        }

        public static bool IsUseMenu(string Param)
        {
            return IsDataExist("Select 1 From TblMenu Where Param = @Param Limit 1; ", Param);
        }

        private static string Encrypt(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the encoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[]
            byte[] DataToEncrypt = UTF8.GetBytes(Message);

            // Step 5. Attempt to encrypt the string
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the encrypted string as a base64 encoded string
            return Convert.ToBase64String(Results);
        }

        public static string EncryptUserPwd(string Passphrase)
        {
            return Encrypt(_userPwd, Passphrase);
        }

        public static void ExecCommand(SqlCommand cm)
        {
            //Execute SQL command (Example: Insert, Update, Delete)
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                cm.ExecuteNonQuery();
            }
        }

        public static string ExecCommandWithReturnValue(SqlCommand cm)
        {
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                return cm.ExecuteScalar().ToString();
            }
        }

        public static void ExecCommands(List<SqlCommand> cml)
        {
            //Execute SQL (Example: Insert, Update, Delete) for more than 1 table
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var tr = cn.BeginTransaction();
                try
                {
                    cml.ForEach
                    (cm =>
                    {
                        cm.Connection = cn;
                        cm.Transaction = tr;
                        if (Gv.IsUsePreparedStatement) cm.Prepare();
                        cm.ExecuteNonQuery();
                    }
                    );
                    //tr.Rollback();
                    tr.Commit();
                }
                catch (Exception Exc)
                {
                    tr.Rollback();
                    throw new Exception(Exc.Message);
                }
                finally
                {
                    tr = null;
                }
            }
        }

        private static void InsertIntoUpdaterError(string SQLCommand, string ErrMessage)
        {
            string LastID = Sm.GetValue("SELECT Top 1 ID FROM TblUpdaterError Order by ID Desc");
            if (LastID == string.Empty) LastID = "0";
            LastID = (decimal.Parse(LastID) + 1).ToString();
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new SqlCommand()
                {
                    CommandText =
                        "Insert Into TblUpdaterError" +
                        "(ID,SQLStm,Result, SQLDate) " +
                        "Values (@ID,@SQLStm,@Result, " + Gv.CurrentDateTime + ") "
                };
                Sm.CmParam<String>(ref cm, "@ID", LastID);
                Sm.CmParam<String>(ref cm, "@SQLStm", SQLCommand);
                Sm.CmParam<String>(ref cm, "@Result", ErrMessage);

                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                cm.ExecuteNonQuery();
            }
        }

        public static void InsertButtonUsageHistory(string versi, string MenuCode, string MenuDesc, string button)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into tblbuttonusagehistory ");
            SQL.AppendLine("(Versi, menuCode, menudesc, button, createby, createDt) ");
            SQL.AppendLine("Values(@Versi, @menuCode, @menudesc, @button, @createby, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Versi", versi);
            Sm.CmParam<String>(ref cm, "@menuCode", MenuCode);
            Sm.CmParam<String>(ref cm, "@menudesc", MenuDesc);
            Sm.CmParam<String>(ref cm, "@button", button);
            Sm.CmParam<String>(ref cm, "@createby", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }

        public static bool ExecCommandsDownloader(List<SqlCommand> cml)
        {
            bool result = true;
            //Execute SQL (Example: Insert, Update, Delete) for more than 1 table
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                string SQLCommand = string.Empty;
                cn.Open();
                var tr = cn.BeginTransaction();
                try
                {
                    cml.ForEach
                    (cm =>
                    {
                        SQLCommand = cm.CommandText;
                        cm.Connection = cn;
                        cm.Transaction = tr;
                        if (Gv.IsUsePreparedStatement) cm.Prepare();
                        cm.ExecuteNonQuery();
                        result = true;
                    }
                    );
                    tr.Commit();
                }
                catch (Exception Exc)
                {
                    result = false;
                    tr.Rollback();
                    InsertIntoUpdaterError(SQLCommand, Exc.Message);
                    throw new Exception(Exc.Message);
                }
                finally
                {
                    tr = null;
                }
            }
            return result;
        }

        public static void ExportToExcel2(iGrid Grd, string Title)
        {
            #region "Old format or invalid type library" error fix
            // If you use early binding for MS Excel COM Automation as we do, you may get the following error:
            // "Old format or invalid type library. (Exception from HRESULT: 0x80028018 (TYPE_E_INVDATAREAD))"
            // This bug is caused by the use of the Microsoft.Office.Interop.* libraries.
            // It's because the user local settings are different from the office version.
            // For instance, if you use the English version and after setting language to 'English US', and country
            // to 'United States' in the reagional and language settings under control panel it works fine.
            // To overcome the problem in code, you may use the following trick:
            System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            #endregion

            try
            {
                if (!IsExcelExist()) return;

                // Instantiate Excel and start a new workbook.
                objApp = new Excel.Application();
                objBooks = objApp.Workbooks;
                objBook = objBooks.Add(Missing.Value);
                objSheets = objBook.Worksheets;
                objSheet = (Excel.Worksheet)objSheets.get_Item(1);

                foreach (iGColHdr cell in Grd.Header.Cells)
                {
                    if (Grd.Cols[cell.ColIndex].Visible) SetExcelCellValue(0, cell.ColIndex, cell.Value, 0);
                }

                // Export the contents of iGrid
                foreach (iGRow row in Grd.Rows)
                {
                    switch (row.Type)
                    {
                        case iGRowType.AutoGroupRow:
                            SetExcelCellValue(row.Index + 1, 0, row.RowTextCell.Value, true, 0);
                            break;
                        case iGRowType.ManualGroupRow:
                            SetExcelCellValue(row.Index + 1, 0, row.RowTextCell.Value, 0);
                            break;
                        case iGRowType.Normal:
                            byte FormatType = 0;
                            foreach (iGCell cell in row.Cells)
                            {
                                if (Grd.Rows[cell.RowIndex].Visible)
                                {
                                    if (Grd.Cols[cell.ColIndex].Visible)
                                    {
                                        switch (Grd.Cols[cell.ColIndex].CellStyle.FormatString)
                                        {
                                            case "{0:dd/MMM/yyyy}":
                                                FormatType = 1;
                                                break;
                                            case "{0:HH:mm}":
                                                FormatType = 2;
                                                break;
                                            case "{0:##0}":
                                                FormatType = 3;
                                                break;
                                            case "{0:#,##0}":
                                                FormatType = 4;
                                                break;
                                            case "{0:#,##0.00##}":
                                                FormatType = 5;
                                                break;
                                            case "{0:#,##0.0000}":
                                                FormatType = 6;
                                                break;
                                            case "{0:#,##0.00######}":
                                                FormatType = 7;
                                                break;
                                            default:
                                                FormatType = 0;
                                                break;
                                        }
                                        SetExcelCellValue(row.Index + 1, cell.ColIndex, cell.Value, FormatType);
                                    }
                                }
                            }
                            break;
                    }
                }

                //Auto Fit
                objSheet.Cells.Select();
                objSheet.Columns.AutoFit();
                objSheet.Rows.AutoFit();

                System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

                //Return control of Excel to the user.
                objApp.Visible = true;
                objApp.UserControl = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void FilterDate2(ref string SQL, string Filter1, string Filter2, string Column)
        {
            if (!string.IsNullOrEmpty(Filter1) && !string.IsNullOrEmpty(Filter2))
            {
                SQL = SetFilterDate2(SQL, Filter1, Filter2, Column);
            }
        }

        public static void FilterCcbSetCheckEdit(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Lue = "Ccb" + ControlName;
            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = ((CheckedComboBoxEdit)(FindControl(Frm, Lue))).EditValue != null && ((CheckedComboBoxEdit)(FindControl(Frm, Lue))).EditValue.ToString() != string.Empty;
        }

        public static void FilterSingleDteSetCheckEdit(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);

            string Dte = "Dte" + ControlName;

            string Tag = ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag == null ? "" : ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag.ToString();

            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked =
                ((DateEdit)(FindControl(Frm, Dte))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte))).EditValue.ToString() != string.Empty;
        }

        public static void FilterDteSetCheckEdit(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            ControlName = ControlName.Substring(0, ControlName.Length - 1);
            string Dte1 = "Dte" + ControlName + "1";
            string Dte2 = "Dte" + ControlName + "2";

            if (((DateEdit)(FindControl(Frm, Dte1))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte1))).EditValue.ToString() != string.Empty &&
                (((DateEdit)(FindControl(Frm, Dte2))).EditValue == null || ((DateEdit)(FindControl(Frm, Dte2))).EditValue.ToString() == string.Empty))

                ((DateEdit)(FindControl(Frm, Dte2))).DateTime = ((DateEdit)(FindControl(Frm, Dte1))).DateTime;


            string Tag = ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag == null ? "" : ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag.ToString();
            if (String.Compare(Tag, "Filter", false) != 0)
            {
                if (((DateEdit)(FindControl(Frm, Dte2))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte2))).EditValue.ToString() != string.Empty &&
                    (((DateEdit)(FindControl(Frm, Dte1))).EditValue == null || ((DateEdit)(FindControl(Frm, Dte1))).EditValue.ToString() == string.Empty))

                    ((DateEdit)(FindControl(Frm, Dte1))).DateTime = ((DateEdit)(FindControl(Frm, Dte2))).DateTime;
            }

            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked =
                ((DateEdit)(FindControl(Frm, Dte1))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte1))).EditValue.ToString() != string.Empty &&
                ((DateEdit)(FindControl(Frm, Dte2))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte2))).EditValue.ToString() != string.Empty;
        }

        public static void FilterLueSetCheckEdit(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Lue = "Lue" + ControlName;
            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = ((LookUpEdit)(FindControl(Frm, Lue))).EditValue != null && ((LookUpEdit)(FindControl(Frm, Lue))).EditValue.ToString() != string.Empty;
        }

        public static void FilterDteSetCheckEdit2(Form Frm, object sender)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Dte = "Dte" + ControlName;
            ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = ((DateEdit)(FindControl(Frm, Dte))).EditValue != null && ((DateEdit)(FindControl(Frm, Dte))).EditValue.ToString() != string.Empty;
        }

        public static void FilterSingleSetDateEdit(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Dte = "Dte" + ControlName;

            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (((DateEdit)(FindControl(Frm, Dte))).EditValue == null || ((DateEdit)(FindControl(Frm, Dte))).EditValue.ToString() == string.Empty)
                {
                    Sm.StdMsg(mMsgType.Warning, Warning + " is empty.");
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((DateEdit)FindControl(Frm, Dte)).Focus();

                }
            }
            else
            {
                string OriginalTag = ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag == null ? "" : ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag.ToString();
                ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag = "Filter";
                ((DateEdit)(FindControl(Frm, Dte))).EditValue = null;
                ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag = (OriginalTag.Length == 0) ? null : OriginalTag;
                ((DateEdit)FindControl(Frm, Dte)).Focus();
            }
        }

        public static void FilterSetDateEdit(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            string Dte1 = "Dte" + ControlName + "1";
            string Dte2 = "Dte" + ControlName + "2";

            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (((DateEdit)(FindControl(Frm, Dte1))).EditValue == null || ((DateEdit)(FindControl(Frm, Dte1))).EditValue.ToString() == string.Empty)
                {
                    Sm.StdMsg(mMsgType.Warning, Warning + " is empty.");
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((DateEdit)FindControl(Frm, Dte1)).Focus();

                }
                else
                {
                    if (((DateEdit)(FindControl(Frm, Dte2))).EditValue == null || ((DateEdit)(FindControl(Frm, Dte2))).EditValue.ToString() == string.Empty)
                    {
                        Sm.StdMsg(mMsgType.Warning, Warning + " is empty.");
                        ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                        ((DateEdit)FindControl(Frm, Dte2)).Focus();
                    }
                }
            }
            else
            {
                string OriginalTag = ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag == null ? "" : ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag.ToString();
                ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag = "Filter";
                ((DateEdit)(FindControl(Frm, Dte2))).EditValue = null;
                ((DateEdit)(FindControl(Frm, Dte1))).EditValue = null;
                ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Tag = (OriginalTag.Length == 0) ? null : OriginalTag;
                ((DateEdit)FindControl(Frm, Dte1)).Focus();
            }
        }

        public static void FilterSetLookUpEdit(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (((LookUpEdit)(FindControl(Frm, "Lue" + ControlName))).EditValue == null || ((LookUpEdit)(FindControl(Frm, "Lue" + ControlName))).EditValue.ToString() == string.Empty)
                {
                    MessageBox.Show(Warning + " is empty.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((LookUpEdit)FindControl(Frm, "Lue" + ControlName)).Focus();
                }
            }
            else
            {
                ((LookUpEdit)(FindControl(Frm, "Lue" + ControlName))).EditValue = null;
                ((LookUpEdit)FindControl(Frm, "Lue" + ControlName)).Focus();
            }
        }

        public static void FilterSetCheckedComboBoxEdit(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (((CheckedComboBoxEdit)(FindControl(Frm, "Ccb" + ControlName))).EditValue == null ||
                    ((CheckedComboBoxEdit)(FindControl(Frm, "Ccb" + ControlName))).EditValue.ToString() == string.Empty)
                {
                    MessageBox.Show(Warning + " is empty.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((CheckedComboBoxEdit)FindControl(Frm, "Ccb" + ControlName)).Focus();
                }
            }
            else
            {
                ((CheckedComboBoxEdit)(FindControl(Frm, "Ccb" + ControlName))).EditValue = null;
                ((CheckedComboBoxEdit)FindControl(Frm, "Ccb" + ControlName)).Focus();
            }
        }

        public static void FilterSetDateEdit2(Form Frm, object sender, string Warning)
        {
            string ControlName = (((Control)sender).Name).ToString().Substring((((Control)sender).Name).Length - (((Control)sender).Name.Length - 3), ((Control)sender).Name.Length - 3);
            if (((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked)
            {
                if (((DateEdit)(FindControl(Frm, "Dte" + ControlName))).EditValue == null || ((DateEdit)(FindControl(Frm, "Dte" + ControlName))).EditValue.ToString() == string.Empty)
                {
                    MessageBox.Show(Warning + " is empty.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ((CheckEdit)FindControl(Frm, "Chk" + ControlName)).Checked = false;
                    ((DateEdit)FindControl(Frm, "Dte" + ControlName)).Focus();
                }
            }
            else
            {
                ((DateEdit)(FindControl(Frm, "Dte" + ControlName))).EditValue = null;
                ((DateEdit)FindControl(Frm, "Dte" + ControlName)).Focus();
            }
        }

        public static void FilterStr(ref string SQL, ref SqlCommand cm, string Filter, string Column, bool Fixed)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));
                SQL = SetFilterString(SQL, Column, Column2);
                CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2), Fixed ? Filter : "%" + Filter + "%");
            }
        }

        public static void FilterStr(ref string SQL, ref SqlCommand cm, string Filter, string[] Columns)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string SQL2 = string.Empty, c2 = string.Empty;
                foreach (string c1 in Columns)
                {
                    c2 = Sm.Right(c1, c1.Length - 1 - c1.IndexOf("."));
                    SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + c1 + ") Like @" + c2;
                    CmParam<String>(ref cm, "@" + (c1.IndexOf(".") == -1 ? c1 : c2), "%" + Filter + "%");
                }
                SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        private static Control FindControl(Form frm, string ControlName)
        {
            if (ControlName.Length == 0 || frm.Controls.Find(ControlName, true).Length == 0)
                return null;
            else
                return frm.Controls.Find(ControlName, true)[0];
        }

        public static void FocusGrd(iGrid Grd, int Row, int Col)
        {
            if (Grd.Rows.Count != 0)
            {
                Grd.CurCell = Grd.Cells[Row, Col];
                Grd.Focus();
            }
        }

        public static string GetMenuDesc(string Param)
        {
            return Sm.GetValue("Select IfNull(MenuDesc, '') MenuDesc From TblMenu Where Param=@Param Limit 1;", Param);
        }

        public static string GetMonthName(int MonthIndex)
        {
            string MonthName = string.Empty;
            switch (MonthIndex)
            {
                case 1:
                    MonthName = "Jan";
                    break;
                case 2:
                    MonthName = "Feb";
                    break;
                case 3:
                    MonthName = "Mar";
                    break;
                case 4:
                    MonthName = "Apr";
                    break;
                case 5:
                    MonthName = "May";
                    break;
                case 6:
                    MonthName = "Jun";
                    break;
                case 7:
                    MonthName = "Jul";
                    break;
                case 8:
                    MonthName = "Aug";
                    break;
                case 9:
                    MonthName = "Sep";
                    break;
                case 10:
                    MonthName = "Oct";
                    break;
                case 11:
                    MonthName = "Nov";
                    break;
                case 12:
                    MonthName = "Dec";
                    break;
            }
            return MonthName;
        }

        public static string FormatDate(DateTime Value)
        {
            return
                Value.Year.ToString() +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                "000000";
        }

        public static string FormatDateTime(DateTime Value)
        {
            return
                Value.Year.ToString() +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                ("00" + Value.Hour.ToString()).Substring(("00" + Value.Hour.ToString()).Length - 2, 2) +
                ("00" + Value.Minute.ToString()).Substring(("00" + Value.Minute.ToString()).Length - 2, 2) +
                ("00" + Value.Millisecond.ToString()).Substring(("00" + Value.Millisecond.ToString()).Length - 2, 2);
        }

        public static void FrmFindKeydown(object sender, KeyEventArgs e,
            ref Button BtnRefresh, ref Button BtnChoose, ref Button BtnExit,
            BtnClick BtnRefreshClick, BtnClick BtnChooseClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F5":
                    if (BtnRefresh.Enabled) BtnRefreshClick(sender, e);
                    break;
                case "F6":
                    if (BtnChoose.Enabled) BtnChooseClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void FrmInputKeydown(object sender, KeyEventArgs e,
            ref Button BtnSave, ref Button BtnExit,
            BtnClick BtnSaveClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F6":
                    if (BtnSave.Enabled) BtnSaveClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void FrmInputKeydown(object sender, KeyEventArgs e,
            ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel, ref Button BtnPrint, ref Button BtnExit,
            BtnClick BtnFindClick, BtnClick BtnInsertClick, BtnClick BtnEditClick, BtnClick BtnDeleteClick, BtnClick BtnSaveClick, BtnClick BtnCancelClick, BtnClick BtnPrintClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F7":
                    if (BtnFind.Enabled) BtnFindClick(sender, e);
                    break;
                case "F2":
                    if (BtnInsert.Enabled) BtnInsertClick(sender, e);
                    break;
                case "F3":
                    if (BtnEdit.Enabled) BtnEditClick(sender, e);
                    break;
                case "F6":
                    if (BtnSave.Enabled) BtnSaveClick(sender, e);
                    break;
                case "F8":
                    if (BtnPrint.Enabled) BtnPrintClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void FrmInputKeydown(object sender, KeyEventArgs e,
            ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel, ref Button BtnExit,
            BtnClick BtnFindClick, BtnClick BtnInsertClick, BtnClick BtnEditClick, BtnClick BtnDeleteClick, BtnClick BtnSaveClick, BtnClick BtnCancelClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F7":
                    if (BtnFind.Enabled) BtnFindClick(sender, e);
                    break;
                case "F2":
                    if (BtnInsert.Enabled) BtnInsertClick(sender, e);
                    break;
                case "F3":
                    if (BtnEdit.Enabled) BtnEditClick(sender, e);
                    break;
                case "F6":
                    if (BtnSave.Enabled) BtnSaveClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void FrmReportKeydown(object sender, KeyEventArgs e,
            ref Button BtnRefresh, ref Button BtnPrint, ref Button BtnExcel, ref Button BtnExit,
            BtnClick BtnRefreshClick, BtnClick BtnPrintClick, BtnClick BtnExcelClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F5":
                    if (BtnRefresh.Enabled) BtnRefreshClick(sender, e);
                    break;
                case "F8":
                    if (BtnPrint.Enabled) BtnPrintClick(sender, e);
                    break;
                case "F9":
                    if (BtnExcel.Enabled) BtnExcelClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void FrmChartKeydown(
            object sender, KeyEventArgs e,
            ref Button BtnRefresh, ref Button BtnExit, BtnClick BtnRefreshClick, BtnClick BtnExitClick)
        {
            switch (e.KeyCode.ToString())
            {
                case "F5":
                    if (BtnRefresh.Enabled) BtnRefreshClick(sender, e);
                    break;
                case "F12":
                    if (BtnExit.Enabled) BtnExitClick(sender, e);
                    break;
            }
        }

        public static void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int ColSource, int ColLot, int ColBin)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(Lot=@Lot" + No + " And Bin=@Bin" + No + " And Source=@Source" + No + ") ";
            Sm.CmParam<String>(ref cm, "@Source" + No, Sm.GetGrdStr(Grd, Row, ColSource));
            Sm.CmParam<String>(ref cm, "@Lot" + No, Sm.GetGrdStr(Grd, Row, ColLot));
            Sm.CmParam<String>(ref cm, "@Bin" + No, Sm.GetGrdStr(Grd, Row, ColBin));
        }

        public static void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int ColSource)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(Source=@Source" + No + ") ";
            Sm.CmParam<String>(ref cm, "@Source" + No, Sm.GetGrdStr(Grd, Row, ColSource));
        }

        public static void GenerateSQLFilterForInventory(
            ref MySqlCommand cm, ref string Filter, string Table,
            ref iGrid Grd, int ColSource, int ColLot, int ColBin)
        {
            if (Grd.Rows.Count != 1)
            {
                for (int R = 0; R < Grd.Rows.Count - 1; R++)
                    if (Sm.GetGrdStr(Grd, R, ColSource).Length != 0)
                        GenerateSQLFilterForInventory(ref cm, ref Filter, Table, R + 1, ref Grd, R, ColSource, ColLot, ColBin);
            }
        }

        public static void GenerateSQLFilterForInventory(
            ref MySqlCommand cm, ref string Filter, string Table, int No,
            ref iGrid Grd, int Row, int ColSource, int ColLot, int ColBin)
        {
            if (Filter.Length > 0) Filter += " And ";
            Filter += " Not(" + Table + ".Source=@Source" + Sm.Right("00" + No, 3) + " And " + Table + ".Lot=@Lot" + Sm.Right("00" + No, 3) + " And " + Table + ".Bin=@Bin" + Sm.Right("00" + No, 3) + ") ";
            Sm.CmParam<String>(ref cm, "@Source" + Sm.Right("00" + No, 3), Sm.GetGrdStr(Grd, Row, ColSource));
            Sm.CmParam<String>(ref cm, "@Lot" + Sm.Right("00" + No, 3), Sm.GetGrdStr(Grd, Row, ColLot));
            Sm.CmParam<String>(ref cm, "@Bin" + Sm.Right("00" + No, 3), Sm.GetGrdStr(Grd, Row, ColBin));
        }

        public static string GetCcb(CheckedComboBoxEdit Ccb)
        {
            return (Ccb.EditValue == null) ? "" : Ccb.EditValue.ToString();
        }

        private static string getConfigFilePath()
        {
            return Assembly.GetExecutingAssembly().Location + ".config";
        }

        public static string GetDte(DateEdit Dte)
        {
            return (Dte.EditValue == null || Dte.EditValue.ToString().Length == 0) ? "" : Sm.FormatDate(Dte.DateTime);
        }

        private static string GetExcelAddr(int rowNo, int colNo)
        {
            string a = string.Empty;
            if (colNo < 26)
            {
                a = (char)(65 + colNo) + (rowNo + 1).ToString();
            }
            else
            {
                if (colNo >= 26 && colNo < 51)
                {
                    a = "A" + ((char)(65 + colNo - 26) + (rowNo + 1).ToString());
                }
                else
                {
                    if (colNo >= 51 && colNo < 76)
                    {
                        a = "B" + ((char)(65 + colNo - 51) + (rowNo + 1).ToString());
                    }
                }
            }
            return a;
        }

        public static decimal GetGrdDec(iGrid Grd, int RowIndex, int ColIndex)
        {
            var Value = 0m;
            if (GetGrdStr(Grd, RowIndex, ColIndex).Length > 0)
                Value = Decimal.Parse(Grd.Cells[RowIndex, ColIndex].Value.ToString());
            return Value;
        }

        public static string GetGrdDec(iGrid Grd, int RowIndex, int ColIndex, byte FormatType)
        {
            decimal Value = 0m;
            if (GetGrdStr(Grd, RowIndex, ColIndex).Length > 0)
                Value = Decimal.Parse(Grd.Cells[RowIndex, ColIndex].Value.ToString());
            return Sm.FormatNum(Value, FormatType);
        }

        public static Int32 GetGrdInt(iGrid Grd, int RowIndex, int ColIndex)
        {
            try
            {
                return
                    (Grd.Cells[RowIndex, ColIndex].Value == null || Grd.Cells[RowIndex, ColIndex].Value.ToString().Length == 0)
                    ? 0 : Int32.Parse(Grd.Cells[RowIndex, ColIndex].Value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        public static string GetGrdText(iGrid Grd, int RowIndex, int ColIndex)
        {
            return
                (Grd.Cells[RowIndex, ColIndex].Text.Length == 0)
                ? "" : Grd.Cells[RowIndex, ColIndex].Text;
        }

        public static string GetGrdStr(iGrid Grd, int RowIndex, int ColIndex)
        {
            return (Grd.Cells[RowIndex, ColIndex].Value == null)
                ? "" : Grd.Cells[RowIndex, ColIndex].Value.ToString().Trim();
        }

        public static decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            return Sm.GetValueDec(cm);
        }

        public static string GetLue(LookUpEdit Lue)
        {
            return (Lue.EditValue == null) ? "" : Lue.EditValue.ToString();
        }

        public static string GetValue(SqlCommand cm)
        {
            Object Value;
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? "" : Value.ToString());
        }

        public static void GrdColInvisible(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].Visible = false;
        }

        public static void GrdColPercentBar(iGrid Grd, int[] ColIndex)
        {
            iGCellStyle myPercentBarStyle = new iGCellStyle();
            myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
            myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));

            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].CellStyle = myPercentBarStyle;
        }

        public static void GrdColInvisible(iGrid Grd, int[] ColIndex, bool Visible)
        {
            Grd.BeginUpdate();
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].Visible = Visible;
            Grd.EndUpdate();
        }

        public static void GrdColReadOnly(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
            {
                Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
                Grd.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.True;
            }
        }

        public static void GrdColReadOnly(bool ReadOnly, bool IsChangeBackColor, iGrid Grd, int[] ColIndex)
        {
            Grd.BeginUpdate();
            if (ReadOnly)
            {
                for (int Col = 0; Col < ColIndex.Length; Col++)
                {
                    if (IsChangeBackColor) Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
                    Grd.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.True;
                }
            }
            else
            {
                for (int Col = 0; Col < ColIndex.Length; Col++)
                {
                    if (IsChangeBackColor) Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.White;
                    Grd.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.False;
                }
            }
            Grd.EndUpdate();
        }

        public static void GrdColReadOnly(iGrid Grd, int[] ColIndex, bool IsBackColorChanged)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
            {
                if (IsBackColorChanged) Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
                Grd.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.True;
            }
        }

        public static void GrdColEditable(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
            {
                Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.White;
                Grd.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.False;
            }
        }

        public static void GrdCustomDrawCellForeground(iGrid Grd, int[] ColIndex, object sender, iGCustomDrawCellEventArgs e)
        {
            try
            {
                for (int Col = 0; Col < ColIndex.Length; Col++)
                {
                    if (GetGrdStr(Grd, e.RowIndex, ColIndex[Col]).Length == 0) return;

                    object myObjValue = Grd.Cells[e.RowIndex, ColIndex[Col]].Value;
                    if (myObjValue == null)
                        return;

                    Rectangle myBounds = e.Bounds;
                    myBounds.Inflate(-2, -2);
                    myBounds.Width = myBounds.Width - 1;
                    myBounds.Height = myBounds.Height - 1;
                    if (myBounds.Width > 0)
                    {
                        e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                        double myValue = (double)myObjValue;
                        int myWidth = (int)(myBounds.Width * myValue);
                        e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                        e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                        StringFormat myStringFormat = new StringFormat();
                        myStringFormat.Alignment = StringAlignment.Center;
                        myStringFormat.LineAlignment = StringAlignment.Center;
                        e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Grd.Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                    }
                }
            }
            catch (Exception) { }
        }

        public static void GrdFormatDate(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].CellStyle.FormatString = "{0:dd/MMM/yyyy}";
        }

        public static string FormatDate2(String Format, String Value)
        {
            //Format date in string
            return
                (Value.Length == 0) ? "" :
                    String.Format("{0:" + Format + "}",
                        new DateTime(
                        Int32.Parse(Value.Substring(0, 4)),
                        Int32.Parse(Value.Substring(4, 2)),
                        Int32.Parse(Value.Substring(6, 2))
                        ));
        }

        public static void GrdFormatTime(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].CellStyle.FormatString = "{0:HH:mm}";
        }

        public static void GrdRefresh<T>(iGrid Grd, List<T> l, ShowData<T> d, int Col)
        {
            Cursor.Current = Cursors.WaitCursor;

            Sm.ClearGrd(Grd, false);
            Grd.ProcessTab = true;
            Grd.BeginUpdate();
            try
            {
                if (l.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    int Row = 0;
                    Grd.Rows.Count = l.Count;
                    d(Grd, Row, l);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.SetGrdAutoSize(Grd);
                Grd.EndUpdate();
                Sm.FocusGrd(Grd, 0, Col);

                Cursor.Current = Cursors.Default;
            }
        }

        public static void GrdColCheck(iGrid Grd, int[] ColIndex)
        {
            foreach (int Col in ColIndex)
            {
                Grd.Cols[Col].CellStyle.Type = iGCellType.Check;
                Grd.Cols[Col].CellStyle.ImageAlign = iGContentAlignment.MiddleCenter;
            }
        }

        public static void GrdHdr(iGrid Grd, string[] Title)
        {
            for (int Col = 0; Col <= Title.Length - 1; Col++)
            {
                Grd.Cols[Col].Text = Title[Col];
                Grd.Cols[Col].CellStyle.ValueType = typeof(string);
            }
        }

        public static void GrdHdrWithColWidth(iGrid Grd, string[] Title, int[] ColWidth)
        {
            for (int Col = 0; Col <= Title.Length - 1; Col++)
            {
                Grd.Cols[Col].Text = Title[Col];
                Grd.Cols[Col].Width = ColWidth[Col];
                Grd.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;
                Grd.Cols[Col].CellStyle.ValueType = typeof(string);
            }
        }

        public static void GrdKeyDown(iGrid Grd, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Grd.SelectedRows.Count > 0)
                {
                    if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't delete last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
                            }
                            if (Grd.Rows.Count <= 0) Grd.Rows.Add();
                        }
                    }
                }
            }
        }

        public static bool IsApprovalNeedXCode(string DocType, string XCode)
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where " + XCode + "Code Is Not Null And DocType=@Param Limit 1;",
                DocType);
        }

        public static bool IsDataExist(SqlCommand cm) //Cheking wether data exist
        {
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var result = cm.ExecuteScalar();
                cn.Close();
                return !(result == null);
            }

        }

        public static bool IsDteEmpty(DateEdit Dte, string Message)
        {
            bool Result = true;
            if (Dte.EditValue != null)
                Result = false;
            else
            {
                StdMsg(mMsgType.Warning, Message + " is empty.");
                Dte.Focus();
            }
            return Result;
        }

        private static bool IsExcelExist()
        {
            bool f = true;
            if (Type.GetTypeFromProgID("Excel.Application") == null)
            {
                Sm.StdMsg(mMsgType.Warning, "No excel application in your PC");
                f = false;
            }
            return f;
        }

        public static bool IsFindGridValid(iGrid Grd, int Col)
        {
            bool IsNotValid = false;

            //if (
            //    Grd.CurRow == null ||
            //    (Grd.Rows[Grd.CurRow.Index].Type == iGRowType.Normal && Grd.Cells[Grd.CurRow.Index, Col].Value == null) ||
            //    (Grd.Rows[Grd.CurRow.Index].Type != iGRowType.Normal && Grd.Cells[Grd.CurRow.Index + 1, Col].Value == null)
            //    )

            //if ((Grd.CurRow == null || Grd.Cells[Grd.CurRow.Index, Col].Value == null) &&  
            //    Grd.Rows[Grd.CurRow.Index].BackColor != Color.LightSalmon)

            if (Grd.CurRow == null) IsNotValid = true;

            if (!IsNotValid && Grd.Cells[Grd.CurRow.Index, Col].Value == null) IsNotValid = true;

            if (!IsNotValid && Grd.Rows[Grd.CurRow.Index].BackColor == Color.LightSalmon) IsNotValid = true;

            if (IsNotValid)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                return false;
            }
            return true;
        }

        public static void ChooseDataInFrmFind(Form FrmFind, iGrid Grd, int Col, ShowDataFromFrmFind ShowData)
        {
            if (Grd.CurRow != null)
            {
                int CurRow = Grd.CurRow.Index;

                if (Grd.Rows[CurRow].Type != iGRowType.Normal)
                {
                    for (int Row = CurRow + 1; Row < Grd.Rows.Count; Row++)
                    {
                        if (Grd.Rows[Row].Type == iGRowType.Normal)
                        {
                            CurRow = Row;
                            break;
                        }
                    }
                }

                if (Grd.Cells[CurRow, Col].Value != null)
                {
                    ShowData(Sm.GetGrdStr(Grd, CurRow, Col));
                    FrmFind.Hide();
                    return;
                }
            }
            Sm.StdMsg(mMsgType.Warning, "You need to choose 1 data.");
        }

        public static bool IsLueEmpty(LookUpEdit Lue, string Message)
        {
            bool Result = true;
            if (Lue.EditValue == null ||
                Lue.Text.Trim().Length == 0 ||
                Lue.EditValue.ToString().Trim().Length == 0)
            {
                if (Message.Length == 0)
                    StdMsg(mMsgType.NoData, "");
                else
                    StdMsg(mMsgType.Warning, Message + " still empty.");
                Lue.Focus();
            }
            else
                Result = false;
            return Result;
        }

        public static bool IsMeeEmpty(MemoExEdit Mee, string Message)
        {
            bool Result = true;
            if (Mee.EditValue != null && Mee.Text.Trim().Length != 0)
            {
                Result = false;
            }
            else
            {
                StdMsg(mMsgType.Warning, Message + " is empty.");
                Mee.Focus();
            }
            return Result;
        }

        private static bool IsNumeric(string Value)
        {
            try
            {
                if (Value.Length == 0)
                    return false;
                else
                {
                    decimal NumValue = decimal.Parse(Value);
                    return true;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
                return false;
            }
        }

        public static bool IsPressTabInGrdLastCell(iGrid Grd, KeyEventArgs e, int LastVisibleCol)
        {
            //Check wether user press tab button in the last cell inside the grid
            bool Result = false;
            if (Grd.CurCell != null)
            {
                LastVisibleCol = LastVisibleCol == 0 ? Grd.Cols.Count - 1 : LastVisibleCol;
                if (Grd.CurCell.RowIndex == Grd.Rows.Count - 1 &&
                    Grd.CurCell.Col.Order == LastVisibleCol &&
                    e.KeyCode == Keys.Tab)

                    Result = true;
            }
            return Result;
        }

        public static bool IsPwdValid(string UserCode, string Pwd)
        {
            bool Valid = false;

            if (Pwd.Length == 0)
            {
                StdMsg(mMsgType.Warning, "Password is empty.");
                return false;
            }

            if (Pwd.Length < 6)
            {
                StdMsg(mMsgType.Warning, "Password is less than 6 characters.");
                return false;
            }

            if (Pwd.Length > 20)
            {
                StdMsg(mMsgType.Warning, "Password is more than 20 characters.");
                return false;
            }

            if ((Pwd.IndexOf(" ") != -1))
            {
                StdMsg(mMsgType.Warning, "Password contain empty character(s).");
                return false;
            }

            try
            {
                var cm = new SqlCommand()
                {
                    CommandText = "Select Top 1 UserCode From TblUser Where UserCode=@UserCode And Pwd=@Pwd"
                };
                Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
                Sm.CmParam<String>(ref cm, "@Pwd", Sm.EncryptUserPwd(Pwd));
                Valid = Sm.IsDataExist(cm);
                if (!Valid) Sm.StdMsg(mMsgType.Warning, "Password is wrong.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return Valid;
        }

        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(getConfigFilePath());
                return doc;
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new Exception("No configuration file found.", e);
            }
        }

        public static string DateValue(string Value)
        {
            return String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDate(Value.Substring(0, 8)));
        }

        public static string NumValue(string Value, byte FormatType)
        {
            string NumValue = "0";
            Value = Value.Trim();
            if (Value.Length == 0) Value = "0";
            if (!IsNumeric(Value)) Value = "0";
            switch (FormatType)
            {
                case 0:
                    NumValue = String.Format(
                                (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}", decimal.Parse(Value));
                    //NumValue = String.Format("{0:#,##0.00##}", decimal.Parse(Value));
                    break;
                case 1:
                    NumValue = String.Format("{0:#,##0.0000}", decimal.Parse(Value));
                    break;
                case 2:
                    NumValue = String.Format("{0:#,##0}", decimal.Parse(Value));
                    break;
                case 3:
                    NumValue = String.Format("{0:###0}", decimal.Parse(Value));
                    break;
                case 4:
                    NumValue = String.Format("{0:#,##0.000000}", decimal.Parse(Value));
                    break;
            }
            return NumValue;
        }

        public static string NumValue2(decimal Value, byte FormatType)
        {
            string NumValue = "0";
            switch (FormatType)
            {
                case 0:
                    NumValue = String.Format(
                                (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}", Value);
                    //NumValue = String.Format("{0:#,##0.00##}", Value);
                    break;
                case 1:
                    NumValue = String.Format("{0:#,##0.0000}", Value);
                    break;
                case 2:
                    NumValue = String.Format("{0:#,##0}", Value);
                    break;
                case 3:
                    NumValue = String.Format("{0:###0}", Value);
                    break;
                case 4:
                    NumValue = String.Format("{0:#,##0.000000}", Value);
                    break;
            }
            return NumValue;
        }

        public static string NumValue3(string Value, string FormatStyle)
        {
            if ((FormatStyle == null ? "" : FormatStyle).Length == 0)
            {
                FormatStyle = (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}";
                //FormatStyle = "{0:#,##0.00##}";
            }
            string NumValue = "0";
            Value = Value.Trim();
            if (Value.Length == 0) Value = "0";
            if (IsNumeric(Value))
            {
                NumValue = String.Format(FormatStyle, decimal.Parse(Value));
            }
            return NumValue;
        }

        public static string NzStr(Object Value)
        {
            return (Value == null ? "" : Value.ToString());
        }

        public static decimal NzDec(Object Value)
        {
            return (Value == null ? 0m : Decimal.Parse(Value.ToString()));
        }

        public static void RefreshLookUpEdit(LookUpEdit lue, RefreshLue1 d)
        {
            try
            {
                if (String.Compare((lue.EditValue != null) ? lue.EditValue.ToString() : "", "<Refresh>", true) == 0)
                    d(ref lue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void RefreshLookUpEdit(LookUpEdit lue, RefreshLue2 d, string Param)
        {
            try
            {
                if (String.Compare((lue.EditValue != null) ? lue.EditValue.ToString() : "", "<Refresh>", true) == 0)
                    d(ref lue, Param);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void RefreshLookUpEdit(LookUpEdit lue, RefreshLue3 d, string Param1, string Param2)
        {
            try
            {
                if (Sm.CompareStr(((lue.EditValue != null) ? lue.EditValue.ToString() : ""), "<Refresh>"))
                    d(ref lue, Param1, Param2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void RefreshLookUpEditUnCheck(LookUpEdit lue, RefreshLue3 d, string Param1, string Param2)
        {
            d(ref lue, Param1, Param2);
        }

        public static void RefreshLookUpEdit(LookUpEdit lue, RefreshLue4 d, string Param1, string Param2, string Param3)
        {
            if (String.Compare((lue.EditValue != null) ? lue.EditValue.ToString() : "", "<Refresh>", true) == 0)
                d(ref lue, Param1, Param2, Param3);
        }

        public static void RefreshLookUpEditUnCheck(LookUpEdit lue, RefreshLue4 d, string Param1, string Param2, string Param3)
        {
            d(ref lue, Param1, Param2, Param3);
        }

        public static decimal Round(decimal x, int y)
        {
            return (decimal)Math.Round(x, y, MidpointRounding.AwayFromZero);
        }

        public static void SetControlReadOnly(Control Ctrl, bool ReadOnly)
        {
            switch (Ctrl.GetType().ToString())
            {
                case "DevExpress.XtraEditors.TextEdit":
                    ((TextEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((TextEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.LookUpEdit":
                    ((LookUpEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((LookUpEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.DateEdit":
                    ((DateEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((DateEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.MemoExEdit":
                    ((MemoExEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((MemoExEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.MemoEdit":
                    ((MemoEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((MemoEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.TimeEdit":
                    ((TimeEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    ((TimeEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                    break;
                case "DevExpress.XtraEditors.CheckEdit":
                    ((DevExpress.XtraEditors.CheckEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                    break;
                case "System.Windows.Forms.CheckBox":
                    ((CheckBox)Ctrl).Enabled = !ReadOnly;
                    break;
            }
        }

        public static void SetControlReadOnly(List<Control> Ctrls, bool ReadOnly)
        {
            foreach (Control Ctrl in Ctrls)
            {
                switch (Ctrl.GetType().ToString())
                {
                    case "DevExpress.XtraEditors.TextEdit":
                        ((TextEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((TextEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.LookUpEdit":
                        ((LookUpEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((LookUpEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.DateEdit":
                        ((DateEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((DateEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.MemoExEdit":
                        ((MemoExEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((MemoExEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.MemoEdit":
                        ((MemoEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((MemoEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.TimeEdit":
                        ((TimeEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        ((TimeEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                        break;
                    case "DevExpress.XtraEditors.CheckEdit":
                        ((DevExpress.XtraEditors.CheckEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                        break;
                    case "System.Windows.Forms.CheckBox":
                        ((CheckBox)Ctrl).Enabled = !ReadOnly;
                        break;
                }
            }
        }

        public static void SetControlReadOnly(List<DevExpress.XtraEditors.BaseEdit> Ctrls, bool ReadOnly)
        {
            foreach (DevExpress.XtraEditors.BaseEdit Ctrl in Ctrls)
            {
                Ctrl.Properties.ReadOnly = ReadOnly;

                if (Ctrl.GetType().ToString() != "DevExpress.XtraEditors.CheckEdit")
                    Ctrl.BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);


                //case "DevExpress.XtraEditors.TextEdit":
                //    ((TextEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((TextEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.LookUpEdit":
                //    ((LookUpEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((LookUpEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.DateEdit":
                //    ((DateEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((DateEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.MemoExEdit":
                //    ((MemoExEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((MemoExEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.MemoEdit":
                //    ((MemoEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((MemoEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.TimeEdit":
                //    ((TimeEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    ((TimeEdit)Ctrl).BackColor = ReadOnly ? Color.FromArgb(195, 195, 195) : Color.FromArgb(255, 255, 255);
                //    break;
                //case "DevExpress.XtraEditors.CheckEdit":
                //    ((DevExpress.XtraEditors.CheckEdit)Ctrl).Properties.ReadOnly = ReadOnly;
                //    break;
                //case "System.Windows.Forms.CheckBox":
                //    ((CheckBox)Ctrl).Enabled = !ReadOnly;
                //    break;
                //}
            }
        }

        public static void SetControlEditValueNull(List<DevExpress.XtraEditors.BaseEdit> Ctrls)
        {
            foreach (DevExpress.XtraEditors.BaseEdit Ctrl in Ctrls)
                Ctrl.EditValue = null;
        }

        public static void SetControlNumValueZero(List<DevExpress.XtraEditors.TextEdit> Txts, byte FormatType)
        {
            foreach (DevExpress.XtraEditors.TextEdit Txt in Txts)
            {
                Txt.EditValue = 0;
                Sm.FormatNumTxt(Txt, FormatType);
            }
        }

        public static void SetDefaultPeriod(ref DateEdit Dte1, ref DateEdit Dte2, double NumberOfDays)
        {
            var CurrentDate = Sm.ServerCurrentDateTime();
            Dte1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(NumberOfDays);
            Dte2.DateTime = Sm.ConvertDate(CurrentDate);
        }

        public static void SetDte(DateEdit Dte, string Value)
        {
            if (Value.Length == 0)
                Dte.EditValue = null;
            else
                Dte.DateTime = Sm.ConvertDate(Value);
        }

        public static void SetDteCurrentDate(DateEdit Dte)
        {
            Dte.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        public static void SetDteTime(DateEdit Dte, string Value)
        {
            if (Value.Length == 0)
                Dte.EditValue = null;
            else
                Dte.DateTime = Sm.ConvertDateTime(Value);
        }

        private static void SetExcelCellValue(int rowNo, int colNo, object value, byte FormatType)
        {
            SetExcelCellValue(rowNo, colNo, value, false, FormatType);
        }

        private static void SetExcelCellValue(int rowNo, int colNo, object value, bool bold, byte FormatType)
        {
            Excel.Range range = objSheet.get_Range(GetExcelAddr(rowNo, colNo), Missing.Value);
            range.Value2 = value;
            switch (FormatType)
            {
                case 0:
                    range.NumberFormat = "@";
                    range.Value2 = value;
                    break;
                case 1:
                    range.NumberFormat = "dd/MMM/yyyy";
                    break;
                case 2:
                    range.NumberFormat = "HH:mm";
                    break;
                case 3:
                    range.NumberFormat = "##0";
                    break;
                case 4:
                    range.NumberFormat = "#,##0";
                    break;
                case 5:
                    range.NumberFormat = "#,##0.00##";
                    break;
                case 6:
                    range.NumberFormat = "#,##0.0000";
                    break;
                case 7:
                    range.NumberFormat = "#,##0.00######";
                    break;
            }

            if (bold) range.Font.Bold = true;
        }

        public static string SetFilterDate(string SQL, string Column1, string Column2)
        {
            // Filter condition based on date period
            return
                SQL + ((SQL.Length == 0) ? " Where " : " And ") +
                "(Left(Concat(" + Column1 + ", '000000'), 14) >=  @" + Column2 + "1 And Left(Concat(" + Column1 + ", '000000'), 14) <=  @" + Column2 + "2)";
        }

        public static string SetFilterDate2(string SQL, string Filter1, string Filter2, string Column)
        {
            // Filter condition based on date period
            return
                SQL + ((SQL.Length == 0) ? " Where " : " And ") +
                "(Left(Concat(" + Column + ", '000000'), 14) >=  " + Filter1 + " And Left(Concat(" + Column + ", '000000'), 14) <=  " + Filter2 + ")";
        }

        public static string SetFilterNull(string SQL, string Column, string IsNull)
        {
            //Filter condition based on Null value
            return
                (IsNull.Length == 0) ? SQL :
                    SQL + ((SQL.Length == 0) ? " Where " : " And ") +
                    Column + " Is " + ((IsNull == "Y") ? "" : "Not") + " Null ";
        }

        public static string SetFilterString(string SQL, string Column1, string Column2)
        {
            //Filter condition based on 1 string value

            return SQL + ((SQL.Length == 0) ? " Where " : " And ") + "Upper(" + Column1 + ") Like @" + Column2;
        }

        public static void SetFrmButtonEnabled(
            string Type,
            ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel,
            bool InsertRights, bool EditRights, bool DeleteRights)
        {
            if (String.Compare(Type, "View", true) == 0)
            {
                BtnFind.Enabled = true;
                BtnInsert.Enabled = (InsertRights) ? true : false;
                BtnEdit.Enabled = (EditRights) ? true : false;
                BtnDelete.Enabled = (DeleteRights) ? true : false;
                BtnSave.Enabled = false;
                BtnCancel.Enabled = false;
            }

            if (String.Compare(Type, "Insert", true) == 0 || String.Compare(Type, "Edit", true) == 0)
            {
                BtnFind.Enabled = false;
                BtnInsert.Enabled = false;
                BtnEdit.Enabled = false;
                BtnDelete.Enabled = false;
                BtnSave.Enabled = true;
                BtnCancel.Enabled = true;
            }
        }

        public static void SetFormButtonEnabled(
            mState state,
            ref Button BtnFind, ref Button BtnInsert, ref Button BtnEdit, ref Button BtnDelete, ref Button BtnSave, ref Button BtnCancel,
            bool InsertRights, bool EditRights, bool DeleteRights)
        {
            BtnFind.Enabled = (state == mState.View);
            BtnInsert.Enabled = (state == mState.View) ? InsertRights : false;
            BtnEdit.Enabled = (state == mState.View) ? EditRights : false;
            BtnDelete.Enabled = (state == mState.View) ? DeleteRights : false;
            BtnSave.Enabled = !BtnFind.Enabled;
            BtnCancel.Enabled = BtnSave.Enabled;
        }

        public static void SetFormFindSetting(Form FormMain, Form FormFind)
        {
            FormFind.MdiParent = FormMain.MdiParent;
            //FormFind.WindowState = FormWindowState.Maximized;
            //Sm.SetFormSize(FormFind, ((FrmMain)FormMain.MdiParent).panel1.Visible, ((FrmMain)FormMain.MdiParent).panel1.Height);
            FormFind.Show();
        }

        public static void SetFormUserRights(
            string CurrentUserCode, string MenuCode,
            ref bool InsertRights, ref bool EditRights, ref bool DeleteRights)
        {
            string r = string.Empty;
            InsertRights = false;
            EditRights = false;
            DeleteRights = false;
            try
            {
                r = Sm.GetValue(new SqlCommand
                {
                    CommandText =
                            "Select Top 1 IsNull(AcRights, '') AcRights From (" +
                            "   Select A.UserCode, C.AcRights, D.MenuCode " +
                            "   From TblUser A " +
                            "   Inner Join TblGroup B On A.GrpCode=B.GrpCode " +
                            "   Inner Join TblAcRights C On B.GrpCode=C.GrpCode " +
                            "   Inner Join TblMenu D On C.MenuCode=D.MenuCode" +
                            ") T Where UserCode='" + CurrentUserCode + "' And MenuCode='" + MenuCode + "'"
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if ((r.Length != 0) && (r.Length == 3))
                {
                    InsertRights = r.Substring(0, 1) == "Y" ? true : false;
                    EditRights = r.Substring(1, 1) == "Y" ? true : false;
                    DeleteRights = r.Substring(2, 1) == "Y" ? true : false;
                }
            }
        }

        public static object SetGrdDate(string Value)
        {
            if (Value.Length == 0)
                return "";
            else
                return Sm.ConvertDate(Value);
        }

        public static object SetGrdDate2(string Value)
        {
            if (Value.Length == 0)
                return "";
            else
                return Value.Substring(6, 2) + "/" + Value.Substring(4, 2) + "/" + Value.Substring(0, 4);
        }

        public static object SetGrdDateYYYYMMDD(string Value)
        {
            if (Value.Length == 0)
                return "";
            else
                return Value.Substring(0, 4) + "/" + Value.Substring(4, 2) + "/" + Value.Substring(6, 2);
        }

        public static void SetGrdProperty(iGrid Grd, bool GrdAutoSize)
        {
            Grd.Cols[0].CellStyle.BackColor = Grd.Header.BackColor;
            Grd.GroupRowLevelStyles = new iGCellStyle[]{
                new iGCellStyle{
                    BackColor = Color.FromArgb(224, 224, 224),
                    ForeColor = Color.FromKnownColor(KnownColor.Black)
                }};
            for (int Index = 0; Index < Grd.Cols.Count; Index++)
                Grd.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            if (GrdAutoSize) Grd.Cols.AutoWidth();
            Grd.Rows.AutoHeight();
        }

        public static void SetGrdColHdrAutoAlign(iGrid Grd)
        {
            for (int Index = 0; Index < Grd.Cols.Count; Index++)
                Grd.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        public static object SetGrdTime(string Value)
        {
            if (Value.Length == 0)
                return "";
            else
                return Sm.ConvertDateTime(Value);
        }

        public static void SetLue(LookUpEdit Lue, string Value)
        {
            Lue.EditValue = (Value.Length == 0 ? null : Value);
        }

        public static void SetPeriodByFirstDayOfTheMonth(ref DateEdit Dte1, ref DateEdit Dte2, int Days)
        {
            string FirstDate = Sm.ServerCurrentDateTime().Substring(0, 6) + "01";
            if (FirstDate.Length == 0)
                Dte1.EditValue = null;
            else
            {
                if (Days == 0) Days = -7;
                Dte1.DateTime = Sm.ConvertDate(FirstDate);
                Dte2.DateTime = Sm.ConvertDate(FirstDate).AddMonths(1).AddDays(-1);
            }
        }

        public static void SetPeriodByDays(ref DateEdit Dte1, ref DateEdit Dte2, int StartDay, int EndDay)
        {
            string CurrentDate = Sm.ServerCurrentDateTime();
            if (CurrentDate.Length == 0)
                Dte1.EditValue = null;
            else
            {
                //if (EndDay == 0) EndDay = 7;
                Dte1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(StartDay);
                Dte2.DateTime = Sm.ConvertDate(CurrentDate).AddDays(EndDay);
            }
        }

        //public static void SetPrintManagerPropertyScreen(iGPrintManager PM, iGrid Grd, GrdPrintSetting PrintSetting)
        //{
        //    try
        //    {
        //        Font GrdFont = Grd.Font;
        //        //Grd.Font = new Font("Tahoma", (PrintSetting.FontSize == 0) ? 8 : PrintSetting.FontSize, FontStyle.Regular);
        //        SetGrdAutoSize(Grd);
        //        PM.PageHeader.LeftSection.Font = new Font("Tahoma", 12, FontStyle.Bold);
        //        PM.PageHeader.LeftSection.Text = PrintSetting.ReportTitle;
        //        PM.PageFooter.LeftSection.Text = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));
        //        PM.PageFooter.RightSection.Text = "Page: %[PageNumber] of %[PageCount]";
        //        PM.FitColWidths = PrintSetting.AutoWidth;
        //        PM.PrintingStyle = iGPrintingStyle.ScreenView;
        //        PM.Document.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(
        //            (PrintSetting.LeftMargin == 0) ? 50 : PrintSetting.LeftMargin,
        //            (PrintSetting.RightMargin == 0) ? 50 : PrintSetting.RightMargin,
        //            (PrintSetting.TopMargin == 0) ? 50 : PrintSetting.TopMargin,
        //            (PrintSetting.BottomMargin == 0) ? 50 : PrintSetting.BottomMargin);
        //        PM.Document.DefaultPageSettings.Landscape = PrintSetting.Landscape;
        //        PM.Document.DocumentName = PrintSetting.DocumentName;
        //        PM.PrintPreview(Grd);
        //        Grd.Font = GrdFont;
        //        SetGrdAutoSize(Grd);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        public static DialogResult StdMsgYN(string Type, string Message)
        {
            DialogResult Result = DialogResult.No;
            switch (Type)
            {
                case ("Approve"):
                    Result = MessageBox.Show("Do you want to approve this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Save"):
                    Result = MessageBox.Show("Do you want to save this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Delete"):
                    Result = MessageBox.Show("Do you want to delete this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Cancel"):
                    Result = MessageBox.Show("Do you want to cancel this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Print"):
                    Result = MessageBox.Show("Do you want to print this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("CancelProcess"):
                    Result = MessageBox.Show("Do you want to cancel this process ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Process"):
                    Result = MessageBox.Show("Do you want to process this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case "Question":
                    Result = MessageBox.Show(Message, Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
            }
            return Result;
        }

        public static DialogResult StdMsgYN(string Type, string Message, string MenuCode)
        {
            DialogResult Result = DialogResult.No;
            switch (Type)
            {
                case ("Approve"):
                    Result = MessageBox.Show("Do you want to approve this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Save"):
                    if (IsSavingProcessInvalid(MenuCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "You can't save this data." + Environment.NewLine +
                            "Pls contact finance department.");
                        Result = DialogResult.No;
                    }
                    else
                        Result = MessageBox.Show("Do you want to save this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Delete"):
                    Result = MessageBox.Show("Do you want to delete this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Cancel"):
                    Result = MessageBox.Show("Do you want to cancel this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Print"):
                    Result = MessageBox.Show("Do you want to print this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("CancelProcess"):
                    Result = MessageBox.Show("Do you want to cancel this process ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case ("Process"):
                    Result = MessageBox.Show("Do you want to process this data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
                case "Question":
                    Result = MessageBox.Show(Message, Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    break;
            }
            return Result;
        }

        public static bool IsSavingProcessInvalid(string MenuCode)
        {
            var WMenu = Sm.GetParameter("WMenu");
            if (WMenu.Length > 0)
            {
                if (WMenu == "Y")
                {
                    // untuk masing2 menu master/transaksi (Voucher, Receiving Vendor, SFC, dll)
                    return Sm.IsDataExist(
                        "Select 1 from TblWMenu Where MenuCode=@Param;",
                        MenuCode);
                }
                else
                {
                    // untuk menu modul (sales, hr, produksi, purchase, dll).   
                    return WMenu.Contains(Left(MenuCode, 4));
                }
            }
            return false;
        }

        public static bool IsAbleToConnectToDBServer()
        {
            bool Connect = false;
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                Connect = true;
            }
            return Connect;
        }

        public static bool IsTmeEmpty(TimeEdit Tme, string Message)
        {
            bool Result = true;
            if (Tme.EditValue != null)
                Result = false;
            else
            {
                StdMsg(mMsgType.Warning, Message + " is empty.");
                Tme.Focus();
            }
            return Result;
        }

        public static string FormatTime(string Value)
        {
            return Value.Substring(0, 2) + ":" + Value.Substring(Value.Length - 2, 2);
        }

        public static void GrdColButton(iGrid Grd, int[] ColIndex)
        {
            for (int Col = 0; Col < ColIndex.Length; Col++)
                Grd.Cols[ColIndex[Col]].CellStyle.TypeFlags = iGCellTypeFlags.HasEllipsisButton;
            Grd.EllipsisButtonGlyph = Properties.Resources.SearchGlyph;
        }

        public static void GrdColButton(iGrid Grd, int[] ColIndex, string iconType)
        {

            for (int Col = 0; Col < ColIndex.Length; Col++)
            {

                if (iconType == "1") // upload
                {
                    Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.Yellow;
                    Grd.Cols[ColIndex[Col]].CellStyle.TypeFlags = iGCellTypeFlags.HasEllipsisButton;
                }
                else if (iconType == "2") // download
                {
                    Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.Red;
                    Grd.Cols[ColIndex[Col]].CellStyle.TypeFlags = iGCellTypeFlags.HasEllipsisButton;
                }
                else if (iconType == "3") // view
                {
                    Grd.Cols[ColIndex[Col]].CellStyle.BackColor = Color.Green;
                    Grd.Cols[ColIndex[Col]].CellStyle.TypeFlags = iGCellTypeFlags.HasEllipsisButton;
                }
            }

            Grd.EllipsisButtonGlyph = Properties.Resources.SearchGlyph;


        }

        public static void SetDateFromGrdToDte(DateEdit Dte, iGrid Grd, int RowIndex, int ColIndex)
        {
            if (Grd.Cells[RowIndex, ColIndex].Value.ToString().Length == 0)
                Dte.EditValue = null;
            else
                Dte.EditValue = Grd.Cells[RowIndex, ColIndex].Value;
        }

        public static void SetDateFromDteToGrd(ref DateEdit Dte, iGrid Grd, int RowIndex, int ColIndex)
        {
            if (Dte.EditValue == null || Dte.EditValue.ToString().Length == 0)
                Grd.Cells[RowIndex, ColIndex].Value = string.Empty;
            else
                Grd.Cells[RowIndex, ColIndex].Value = Dte.DateTime;

            //string Value = Pm.FormatDate(Pm.GetDte(Dte));
            //if (Value.Length == 0)
            //    Grd.Cells[RowIndex, ColIndex].Value = string.Empty;
            //else
            //    Grd.Cells[RowIndex, ColIndex].Value = Convert.ToDateTime(Convert.ToDateTime(Value).ToShortDateString());
        }

        public static string GetEntityCostCenter(string CCCode)
        {
            return Sm.GetValue(
                "Select B.EntCode " +
                "From TblCostCenter A, TblProfitCenter B " +
                "Where A.ProfitCenterCode=B.ProfitCenterCode And A.CCCode=@Param;",
                CCCode);
        }

        public static string GetEntityWarehouse(string WhsCode)
        {
            return Sm.GetValue(
                "Select C.EntCode " +
                "From TblWarehouse A, TblCostCenter B, TblProfitCenter C " +
                "Where A.CCCode=B.CCCode And B.ProfitCenterCode=C.ProfitCenterCode And A.WhsCode=@Param;",
                WhsCode);
        }

        public static bool GetGrdBool(iGrid Grd, int RowIndex, int ColIndex)
        {
            return
                (Grd.Cells[RowIndex, ColIndex].Value == null)
                ? false : ((bool)Grd.Cells[RowIndex, ColIndex].Value);
        }

        public static string GetGrdDate(iGrid Grd, int RowIndex, int ColIndex)
        {
            return
                (Grd.Cells[RowIndex, ColIndex].Value == null || Grd.Cells[RowIndex, ColIndex].Value.ToString().Length == 0)
                ? "" : Sm.GenerateDateFormat(Convert.ToDateTime(Convert.ToDateTime(Grd.Cells[RowIndex, ColIndex].Value.ToString()).ToShortDateString()));
        }

        public static string GetCompanyAutoNumberTag()
        {

            string CompanyName = Sm.GetParamValue("CompSystemAutoNo");
            if (CompanyName.Length == 0) { CompanyName = "KNE"; }
            return CompanyName;
        }

        public static string GenerateDateFormat(DateTime Value)
        {
            return
                Value.Year.ToString() +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                "000000";
        }

        public static string GetEmailNo(string WHSCode)
        {
            return Sm.GetValue("SELECT Top 1 EmlAddNo FROM TblWarehouse WHERE WHSCode='" + WHSCode + "'");
        }

        public static string GetEmailAddress(string EmlAddNo)
        {
            return Sm.GetValue("SELECT Top 1 EmlAddrs FROM TblEmailAddressBook WHERE EmlAddNo='" + EmlAddNo + "'");
        }

        public static decimal GetLastEmailSenderNo()
        {
            string LastNumber = Sm.GetValue("SELECT TOP 1 EmlNo FROM TblEmailSender ORDER BY EmlNo Desc");
            if (LastNumber.Length == 0) { LastNumber = "0"; };
            return decimal.Parse(LastNumber);
        }

        public static decimal GetLastEmailDownloaderNo()
        {
            string LastNumber = Sm.GetValue("SELECT TOP 1 EmlNo FROM TblEmailDownloader ORDER BY EmlNo Desc");
            if (LastNumber.Length == 0) { LastNumber = "0"; };
            return decimal.Parse(LastNumber);
        }

        public static decimal GetLastStockMovementNo()
        {
            string LastNumber = Sm.GetValue("SELECT TOP 1 STMNo FROM TblStockMovement ORDER BY STMNo Desc");
            if (LastNumber.Length == 0) { LastNumber = "0"; };
            return decimal.Parse(LastNumber);
        }

        public static string GetListSeparator()
        {
            string Value = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (Value.Length == 0) Value = ",";
            return Value;
        }



        public static string Terbilang(Decimal d)
        {
            d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = Terbilang(Decimal.Round(frac * 100)) + " sen";
            else
                strHasil = "rupiah";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " ratus " + strHasil;
                        }
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }

        public static bool IsFileExist(string Location, string FileName, string FileExtention)
        {
            bool Result = false;
            try
            {
                Result = File.Exists(@Location + FileName + "." + FileExtention);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return Result;
        }

        public static void SetDteInGrd(iGrid Grd, DateEdit Dte)
        {
            // Set up the DateTimePicker control that we will use to edit cells.
            Grd.DefaultRow.Height = Dte.Height + Grd.GridLines.Horizontal.Width;
        }

        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static void SetGrdValue(string Type, iGrid Grd, SqlDataReader dr, int[] c, int Row, int Col, int i)
        {
            switch (Type)
            {
                case "S":
                    Grd.Cells[Row, Col].Value = (dr[c[i]] == DBNull.Value) ? "" : dr.GetString(c[i]);
                    break;
                case "N":
                    Grd.Cells[Row, Col].Value = (dr[c[i]] == DBNull.Value) ? 0m : dr.GetDecimal(c[i]);
                    break;
                case "D":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.ConvertDate(dr.GetString(c[i]));
                    break;
                case "T":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.ConvertDateTime(dr.GetString(c[i]));
                    break;
                case "T2":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.Left(dr.GetString(c[i]), 2) + ":" + Sm.Right(dr.GetString(c[i]), 2);
                    break;
                case "T3":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = string.Empty;
                    else
                        Grd.Cells[Row, Col].Value = Sm.Left(dr.GetString(c[i]), 2) + ":" + dr.GetString(c[i]).Substring(2, 2) + ":" + Sm.Right(dr.GetString(c[i]), 2);
                    break;
                case "B":
                    Grd.Cells[Row, Col].Value =
                        (dr[c[i]] == DBNull.Value) ?
                            false :
                            (string.Compare(dr.GetString(c[i]), "Y") == 0);
                    break;
                case "I":
                    if (dr[c[i]] == DBNull.Value)
                        Grd.Cells[Row, Col].Value = 0;
                    else
                    {
                        try { Grd.Cells[Row, Col].Value = dr.GetInt16(c[i]); }
                        catch { Grd.Cells[Row, Col].Value = dr.GetInt32(c[i]); }
                    }
                    break;
            }
        }

        public static void CopyGrdValue(iGrid Grd1, int Row1, int Col1, iGrid Grd2, int Row2, int Col2)
        {
            Grd1.Cells[Row1, Col1].Value = Grd2.Cells[Row2, Col2].Value;
        }

        public static int[] GetOrdinal(SqlDataReader dr, string[] s)
        {
            var c = new int[s.GetLength(0)];
            var i = 0;
            foreach (string Index in s)
            {
                c[i++] = dr.GetOrdinal(Index);
            }
            return c;
        }

        public static void SetFormSize(Form Frm, bool PanelVisible, int PanelHeight)
        {
            if (Frm.WindowState == FormWindowState.Maximized &&
                Frm.MdiParent != null)
            {
                Frm.WindowState = FormWindowState.Normal;
                var x = Frm.MdiParent.ClientRectangle;

                //x.Height -= (PanelVisible ? 86 : 29);
                x.Height -= (PanelVisible ? (PanelHeight + 30) : 28);
                x.Width -= 5;
                Frm.Size = x.Size;
                Frm.Location = new Point(0, 0);
            }
        }

        public static string Right(string Par, int length)
        {
            return Par.Substring(Par.Length - length, length);
        }

        public static string Left(string Par, int length)
        {
            return Par.Substring(0, length);
        }

        public static void LueKeyDown(iGrid Grd, ref bool fAccept, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //case Keys.Enter:
                //    Grd.Focus();
                //    break;
                case Keys.Escape:
                    fAccept = false;
                    Grd.Focus();
                    break;
            }
        }

        public static void LueLeave(
            ref LookUpEdit Lue, ref iGrid Grd,
            ref bool fAccept, ref iGCell fCell,
            EventArgs e, int Col1, int Col2)
        {
            if (Lue.Visible)
            {
                Lue.Visible = false;
                if (fAccept && fCell.ColIndex == Col2)
                {
                    Grd.Cells[fCell.RowIndex, Col1].Value = (Sm.GetLue(Lue).Length == 0) ? null : Sm.GetLue(Lue);
                    Grd.Cells[fCell.RowIndex, Col2].Value = (Sm.GetLue(Lue).Length == 0) ? null : Lue.GetColumnValue("Col2");
                }
            }
        }

        public static void LueRequestEdit(
            ref iGrid Grd, ref LookUpEdit Lue,
            ref iGCell fCell, ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex - 1).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        public static string GenerateDocumentNumber(string DocumentType, string Yr, string SQL)
        {
            //Example : 00021/XX/13
            string No = Sm.GetValue(SQL);
            return
                ((No.Length == 0) ? "00001" : Sm.Right("00000" + No, 5)) +
                "/" + DocumentType + "/" + Yr;
        }

        public static string GenerateJnNo(string JnDate)
        {
            //Normal : 201305290021
            JnDate = JnDate.Substring(0, 8);
            string No =
                Sm.GetValue(
                    "Select Top 1 NoNew=Cast(Cast(IsNull(Right(JnNo, 4), '0000') As BigInt) + 1 AS NVarChar) " +
                    "From TblJournalHdr " +
                    "Where JnDate='" + JnDate + "' " +
                    "Order By JnNo Desc"
                  );

            return JnDate + ((No.Length == 0) ? "0001" : Sm.Right("0000" + No, 4));
        }

        public static string GenerateJnNoSQL(string JnDate)
        {
            return
                "(  Select Substring(" + JnDate + ", 1, 8)+ " +
                "   Right('0000'+IsNull(( " +
                "       Select Top 1 Cast(Cast(IsNull(Right(JnNo, 4), '0000') As BigInt) + 1 AS NVarChar) " +
                "       From TblJournalHdr Where Left(JnNo, 8)=Substring(" + JnDate + ", 1, 8) " +
                "       Order By JnNo Desc " +
                "   ), '1'), 4)) ";
        }

        public static bool IsGrdColSelected(int[] Data, int Value)
        {
            return Array.IndexOf(Data, Value) > -1 ? true : false;
        }

        public static void ShowDataInGrd(
            ref iGrid Grd,
            ref SqlCommand cm,
            string[] ColumnTitle,
            Action<SqlDataReader, iGrid, int[], int> ShowData,
            bool ShowNoDataInd,
            bool GroupInd,
            bool Editable
        )
        {
            ClearGrd(Grd, Editable);
            if (GroupInd)
            {
                Grd.GroupObject.Clear();
                Grd.Group();
            }
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                }
                else
                {
                    int Row = 0;
                    if (!Editable) Grd.Rows.Count = 0;
                    Grd.ProcessTab = true;
                    Grd.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd.Rows.Add();
                        ShowData(dr, Grd, c, Row);
                        Row++;
                    }
                    if (GroupInd)
                    {
                        Grd.GroupObject.Add(1);
                        Grd.Group();
                    }
                    Grd.EndUpdate();
                }
                Sm.SetGrdAutoSize(Grd);
                dr.Close();
            }
        }

        public static void ShowDataInGrid(
            ref iGrid Grd, ref SqlCommand cm, string SQL, string[] ColumnTitle,
            ShowGrdData sgd, bool ShowNoDataInd, bool GroupInd, bool Editable
        )
        {
            ClearGrd(Grd, Editable);
            if (GroupInd)
            {
                Grd.GroupObject.Clear();
                Grd.Group();
            }
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                }
                else
                {
                    int Row = 0;
                    if (!Editable) Grd.Rows.Count = 0;
                    Grd.ProcessTab = true;
                    Grd.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd.Rows.Add();
                        sgd(dr, Grd, c, Row);
                        Row++;
                    }
                    if (GroupInd)
                    {
                        Grd.GroupObject.Add(1);
                        Grd.Group();
                        SetGrdAlwaysShowSubTotal(Grd);
                    }
                    Grd.EndUpdate();
                }
                Sm.SetGrdAutoSize(Grd);
                dr.Close();
            }
        }

        public static string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }

        public static void ShowDataInCtrl(ref SqlCommand cm, string SQL, string[] ColumnTitle, ShowData sd, bool ShowNoDataInd)
        {
            using (var cn = new SqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                if (Gv.IsUsePreparedStatement) cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd) Sm.StdMsg(mMsgType.NoData, "");
                }
                else
                {
                    while (dr.Read()) sd(dr, c);
                }
                dr.Close();
            }
        }

        public static void SetGrdAlwaysShowSubTotal(iGrid Grd)
        {
            for (int Row = 0; Row < Grd.Rows.Count; Row++)
                if (iGSubtotalManager.IsSubtotal(Grd.Rows[Row]) && Grd.Rows[Row].Level > 0)
                    Grd.Rows[Row].Level--;
        }

        public static bool IsTimeValid(string time)
        {
            DateTime dt;

            if (!DateTime.TryParseExact(time, "HHmm",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out dt))
            {
                Sm.StdMsg(mMsgType.Warning, "Time format is not valid.");
                return false;
            }
            return true;
        }

        public static string GetParamValue(string ParCode)
        {
            var cm = new SqlCommand() { CommandText = "Select Top 1 ParValue From TblParameter Where ParCode=@ParCode" };
            Sm.CmParam<String>(ref cm, "@ParCode", ParCode.Trim());
            return GetValue(cm);
        }

        public static void HideInfoInGrd(iGrid Grd, int[] Cols, bool Hide)
        {
            Sm.GrdColInvisible(Grd, Cols, Hide);
            Sm.SetGrdAutoSize(Grd);
        }

        //public static void SetFormChartSetting(Form FormMain, FrmBaseChart FormChart)
        //{

        //    FormChart.MdiParent = FormMain.MdiParent;
        //    FormChart.Show();
        //    FormChart.ReloadData();

        //}
        #endregion

        #region Class

        public class UploadFileClass
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string FileName { get; set; }
            public string FileName2 { get; set; }
            public string FileName3 { get; set; }
        }

        public class StockAccumulation
        {
            public decimal CreateDt { get; set; }
            public string CreateDt2 { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentType { get; set; }
            public string InvestmentTypeName { get; set; }
            public decimal Qty { get; set; }
            public decimal AcquisitionCost { get; set; }
        }

        #endregion
    }
}