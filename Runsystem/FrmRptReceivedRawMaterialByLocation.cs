﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptReceivedRawMaterialByLocation : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool IsReCompute = false;

        #endregion

        #region Constructor

        public FrmRptReceivedRawMaterialByLocation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

            SetLueOptMonitoring(LueOptMonitoring);
            Sm.SetLue(LueOptMonitoring, "Kota");
            SetGrd();
            SetSQL();

            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            base.FrmLoad(sender, e);
            BtnChart.Visible = (Sm.GetValue("Select IfNull(ParValue, '') from tblparameter where ParCode='ChartAvailable'") == "Yes");

        }

        override protected void SetSQL()
        {
            string ItCtRMPActual = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual'");
            string ItCtRMPActual2 = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual2'");
            string lokasi = string.Empty;
            if (Sm.GetLue(LueOptMonitoring) == "Kota")
            {
                lokasi = "1";
            }
            else if (Sm.GetLue(LueOptMonitoring) == "Kecamatan")
            {
                lokasi = "2";
            }
            else if (Sm.GetLue(LueOptMonitoring) == "Desa")
            {
                lokasi = "3";
            }

            var SQL = new StringBuilder();
           
            SQL.AppendLine("Select ");
            SQL.AppendLine("Case '" + lokasi + "' When '1' Then  J.CityName ");
            SQL.AppendLine("When '2' Then K.SDName ");
            SQL.AppendLine("When '3' Then L.VilName ");
			SQL.AppendLine("Else Null End As Location, ");
            SQL.AppendLine("D.QueueNo, M.Netto, H.DocNo, H.DocDt, G.ItName,  ");
            SQL.AppendLine("            Concat(G.ItName, ' ( ', ");
            SQL.AppendLine("                Case G.ItCtCode ");
            SQL.AppendLine("                    When '"+ItCtRMPActual+"' Then ");
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                        'L: ', Convert(Format(G.Length, 2) Using utf8), ");
            SQL.AppendLine("                        'D: ', Convert(Format(G.Diameter, 2) Using utf8) ");
            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                    When '"+ItCtRMPActual2+"' Then ");
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                       'L: ', Convert(Format(G.Length, 2) Using utf8), ");
            SQL.AppendLine("                        'W: ', Convert(Format(G.Width, 2) Using utf8), ");
            SQL.AppendLine("                        ', H: ', Convert(Format(G.Height, 2) Using utf8) ");
            SQL.AppendLine("                       ) ");
            SQL.AppendLine("                End, ");
            SQL.AppendLine("            ' )') As Item, H.Qty1,  ");
            
            SQL.AppendLine("			  Case G.ItCtCode "); 
            SQL.AppendLine("When '" + ItCtRMPActual + "' Then Truncate(((0.25*3.1415926*G.Diameter*G.Diameter*G.Length)/1000000), 4) *H.Qty1 ");
            SQL.AppendLine("When '" + ItCtRMPActual2 + "' Then Truncate(((G.Length*G.Width*G.Height)/1000000), 4) * H.Qty1 ");
            SQL.AppendLine("Else 0 End  As Qty2, I.VdName As Vendor ");

            SQL.AppendLine("From TblRawMaterialVerify C ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo = D.DocNo And D.CancelInd = 'N' And D.ProcessInd1 = 'F' And D.ProcessInd2 = 'F' ");
            SQL.AppendLine("Inner Join (Select DocNo, DocDateAf, WeightBf, WeightAf, Netto From TblLoadingQueue  ");
            SQL.AppendLine("Where Left(DocNo, 1) In ('A', 'B', 'C') ");
            SQL.AppendLine(") M On M.DocNo = D.QueueNo "); 
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("      Select T2.DocNo, T2.DocDt, T2.LegalDocVerifyDocNo, T3.ItCode, Sum(T3.Qty) As Qty1 ");
            SQL.AppendLine("      From TblRecvRawMaterialHdr T2 ");
            SQL.AppendLine("      Inner Join TblRecvRawMaterialDtl2 T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("	   Where T2.cancelInd = 'N' And T2.ProcessInd = 'F' And T2.DocDt Between @DocDt1 And @DocDt2");
            SQL.AppendLine("			Group By T2.DocNo, T2.LegalDocVerifyDocNo, T3.ItCode");
            SQL.AppendLine("     ) H On H.LegalDocverifyDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblItem G On H.ItCode=G.ItCode ");
            SQL.AppendLine("Inner Join TblVendor I On D.VdCode = I.VdCOde");
            SQL.AppendLine("Left Join TblCity J On I.CityCode = J.CityCode");
            SQL.AppendLine("Left Join TblSubDistrict K On I.SdCode = K.SdCode");
            SQL.AppendLine("Left Join TblVillage L On I.VilCode = L.VilCode");
            SQL.AppendLine("Where C.CancelInd = 'N'   ");         

            
            mSQL = SQL.ToString();

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Location",
                        "No Antrian",
                        "Berat Bersih",
                        "Kategori",
                        "Dokumen Penerimaan",
                        

                        //6-10
                        "Tanggal",
                        "Item",
                        "Quantity"+Environment.NewLine+"(batang)",
                        "Volume"+Environment.NewLine+"(kubik)",
                        "Vendor",
                    },
                     new int[] 
                    {
                        50,
                        150, 100, 80, 150, 150, 
                        100, 320, 100, 100, 150                             
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Document Date ") || Sm.IsDteEmpty(DteDocDt2, "Document Date")) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0";
               
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                IsReCompute = false;
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Location",
                        new string[]
                        {
                            //0
                            "Location", 

                            //1-5
                            "QueueNo", "Netto", "ItName", "DocNo", "DocDt", 
                            
                            //6-9
                            "Item", "Qty1",  "Qty2", "Vendor" 
                                                        
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                        }, true, true, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9 });
                Grd1.Rows.CollapseAll();               
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueOptMonitoring(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { "Kota", "Kecamatan", "Desa" });
        }

        internal static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }
        #endregion

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (IsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void LueOptMonitoring_EditValueChanged(object sender, EventArgs e)
        {
            SetSQL();
        }

        #endregion
    }
}
