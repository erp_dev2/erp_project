﻿#region update

/*
 * 02/11/2021 [SET/PHT] Membuat menu transaksi baru untuk RUP (Rencana Umum Pengadaan)
 * 24/11/2021 [SET/PHT] BUG : Pada menu CPP, ketika insert dan loop milih item, UoM nya tidak sesuai dengan master item
 * 05/01/2022 [RIS/PHT] Menambah filter isfilterbysite
   08/03/2022 [TRI/PHT] bug saat showData, item di detail tidak tampil ketika UOM null
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyProcurementPlan : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool mIsFilterBySite = false;
        internal FrmCompanyProcurementPlanFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmCompanyProcurementPlan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            Sl.SetLueYr(LueYr, "");
            Sl.SetLueSiteCode(ref LueSiteCode);
            Sl.SetLueOption(ref LueProcurementType, "ProcurementType");
            LueProcurementType.Visible = false;
            LueUoMCode.Visible = false;
            SetGrd();
            SetFormControl(mState.View);
            GetParameter();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueSiteCode, LueYr, MeeCancelReason, ChkCancelInd, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    LueUoMCode.Visible = false;
                    LueProcurementType.Visible = false;
                    BtnImport.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueSiteCode, LueYr, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnImport.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason, ChkCancelInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { });
                    MeeCancelReason.Focus();
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Items Code", "Items Name", "ProcurementType", "Metode", "Quantity", 

                        //6-10
                        "UoMCode", "UoM", "Price", "Total", "1", 

                        //11-15
                        "2", "3", "4", "5", "6", 

                        //16-20
                        "7", "8", "9", "10", "11", 

                        //21
                        "12", "Remark"
                    },
                    new int[] 
                    {
                       //0
                       20, 

                       //1-5
                       100, 150, 150, 170, 100, 

                       //6-10
                       50, 170, 100, 100, 100, 

                       //11-15
                       100, 100, 100, 100, 100, 

                       //16-20
                       100, 100, 100, 100, 100, 

                       //21-22
                       100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21}, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 6, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6 });
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueSiteCode, LueYr, MeeCancelReason, MeeRemark, LueProcurementType, LueUoMCode
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCompanyProcurementPlanFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                   InsertData();
                else
                    EditData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CompanyProcurementPlan", "TblCompanyProcurementPlanHdr");

            cml.Add(SaveCompanyProcurementPlanHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveCompanyProcurementPlanDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCompanyProcurementPlanInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2) == Sm.GetGrdStr(Grd1, Row + 1, 2) && Sm.GetGrdStr(Grd1, Row, 4) == Sm.GetGrdStr(Grd1, Row + 1, 4))
                    {
                    Sm.StdMsg(mMsgType.Warning, "Item & Method is not the same.");
                    return true;
                    }
                }

                decimal jan = 0m, feb = 0m, mar = 0m, apr = 0m, mei = 0m, jun = 0m, jul = 0m, agt = 0m, sep = 0m, okt = 0m, nov = 0m, des = 0m;
                jan = Sm.GetGrdDec(Grd1, Row, 10);
                feb = Sm.GetGrdDec(Grd1, Row, 11);
                mar = Sm.GetGrdDec(Grd1, Row, 12);
                apr = Sm.GetGrdDec(Grd1, Row, 13);
                mei = Sm.GetGrdDec(Grd1, Row, 14);
                jun = Sm.GetGrdDec(Grd1, Row, 15);
                jul = Sm.GetGrdDec(Grd1, Row, 16);
                agt = Sm.GetGrdDec(Grd1, Row, 17);
                sep = Sm.GetGrdDec(Grd1, Row, 18);
                okt = Sm.GetGrdDec(Grd1, Row, 19);
                nov = Sm.GetGrdDec(Grd1, Row, 20);
                des = Sm.GetGrdDec(Grd1, Row, 21);
                decimal summonth = jan + feb + mar + apr + mei + jun + jul + agt + sep + okt + nov + des;
                if (summonth != Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning, "Total price is not the same as total price of 12 month.");
                    return true;
                }
            }
            return false;
        }

        private bool IsCompanyProcurementPlanInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblCompanyProcurementPlanHdr Where CancelInd = 'N' ");
            SQL.AppendLine("And Yr = '" + Sm.GetLue(LueYr) + "' ");
            SQL.AppendLine("And SiteCode = '" + Sm.GetLue(LueSiteCode) + "' Limit 1 ");

            var Yr = Sm.GetLue(LueYr);
            var Site = Sm.GetLue(LueSiteCode);
            string SiteName = Sm.GetValue("Select SiteName From tblsite Where SiteCode = '" + Site + "'; ");

            if (Sm.IsDataExist(SQL.ToString()))
            {
                Sm.StdMsg(mMsgType.Warning, "Document in " + Yr + " and Site " + SiteName + " Already Exists.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveCompanyProcurementPlanHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCompanyProcurementPlanHdr ");
            SQL.AppendLine("(DocNo, DocDt, Yr, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, @SiteCode, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCompanyProcurementPlanDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCompanyProcurementPlanDtl ");
            SQL.AppendLine("(DocNo, DNo, ItCode, ProcurementType, Qty, UoMCode, UPrice, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, ");
            SQL.AppendLine("`9`, `10`, `11`, `12`, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @ItCode, @ProcurementType, @Qty, @UoMCode, @UPrice, @1, @2, @3, @4, @5, @6, @7, @8, @9, ");
            SQL.AppendLine("@10, @11, @12, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UoMCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@1", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@2", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@3", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@4", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@5", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@6", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@7", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@8", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@9", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@10", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@11", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@12", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData(string DocNo)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditCompanyProcurementPlanHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblCompanyProcurementPlanHdr Where CancelInd = 'Y' And DocNo = @DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCompanyProcurementPlanHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCompanyProcurementPlanHdr ");
            SQL.AppendLine("Set CancelInd=@CancelInd, CancelReason=@CancelReason, ");
            SQL.AppendLine("LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                //IsInsert = false;
                ClearData();
                ShowCompanyProcurementPlanHdr(DocNo);
                ShowCompanyProcurementPlanDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCompanyProcurementPlanHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select DocNo, CancelInd, CancelReason, DocDt, SiteCode, Yr, Remark " +
                  "From TblCompanyProcurementPlanHdr Where DocNo=@DocNo",
                 new string[] 
                   {
                      //0
                      "DocNo",

                      //1-5
                      "CancelReason", "CancelInd", "DocDt", "SiteCode", "Yr",

                      //6
                      "Remark",

                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[1]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                     Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[4]));
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                 }, true
             );
        }

        private void ShowCompanyProcurementPlanDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.ItCode, B.ItName, A.ProcurementType, C.OptDesc Metode, A.Qty, A.UoMCode, D.UoMName, A.UPrice, A.`1`, A.`2`, A.`3`, ");
            SQL.AppendLine("A.`4`, A.`5`, A.`6`, A.`7`, A.`8`, A.`9`, A.`10`, A.`11`, A.`12`, A.Remark ");
            SQL.AppendLine("FROM tblcompanyprocurementplandtl A ");
            SQL.AppendLine("INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine("INNER JOIN tbloption C ON A.ProcurementType = C.OptCode AND C.OptCat = 'ProcurementType' ");
            SQL.AppendLine("LEFT JOIN tbluom D ON A.UoMCode = D.UoMCode ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                       { 
                           //0
                           "ItCode", 

                           //1-5
                           "ItName",
                           "ProcurementType",
                           "Metode",
                           "Qty",
                           "UoMCode",

                           //6-10
                           "UoMName",
                           "UPrice",
                           "1",
                           "2",
                           "3",

                           //11-15
                           "4",
                           "5",
                           "6",
                           "7",
                           "8",

                           //16-20
                           "9",
                           "10",
                           "11",
                           "12",
                           "Remark",
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Grd.Cells[Row, 9].Value = dr.GetDecimal(c[4]) * dr.GetDecimal(c[7]);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (e.ColIndex == 4) Sm.LueRequestEdit(ref Grd1, ref LueProcurementType, ref fCell, ref fAccept, e);
            }
            if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
            {
                if (e.ColIndex == 7)
                {
                    string ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    SetLueUoMCode(ref LueUoMCode, ItCode);
                    Sm.LueRequestEdit(ref Grd1, ref LueUoMCode, ref fCell, ref fAccept, e);
                }
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmCompanyProcurementPlanDlg(this));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 21 }, e);
            if (e.ColIndex == 5 || e.ColIndex == 8)
            {
                ComputeTotal(e.RowIndex);
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 5);
            if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 8);

            Grd1.Cells[Row, 9].Value = Qty * UPrice;
        }

        internal void SetLueUoMCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            try
            {
                //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                //ItCode = Sm.GetGrdStr(Grd1, Row, 1);

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.PurchaseUOMCode Col1, B.UomName Col2 ");
                SQL.AppendLine("FROM tblitem A ");
                SQL.AppendLine("INNER JOIN tbluom B ON A.PurchaseUOMCode = B.UomCode ");
                SQL.AppendLine("WHERE A.ItCode = '" + ItCode + "' ");
                SQL.AppendLine("UNION ");
                SQL.AppendLine("SELECT A.InventoryUOMCode Col1, B.UomName Col2 ");
                SQL.AppendLine("FROM tblitem A ");
                SQL.AppendLine("INNER JOIN tbluom B ON A.InventoryUOMCode = B.UomCode ");
                SQL.AppendLine("WHERE A.ItCode = '" + ItCode + "' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void Import1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string ItCodeTemp = string.Empty, ProcurementTypeTemp = string.Empty, RemarkTemp = string.Empty;
            decimal QtyTemp = 0m, PriceTemp = 0m, Mth01Temp = 0m, Mth02Temp = 0m, Mth03Temp = 0m, Mth04Temp = 0m, Mth05Temp = 0m,
                    Mth06Temp = 0m, Mth07Temp = 0m, Mth08Temp = 0m, Mth09Temp = 0m, Mth10Temp = 0m, Mth11Temp = 0m, Mth12Temp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            ItCodeTemp = arr[0].Trim();
                            ProcurementTypeTemp = arr[1].Trim();
                            QtyTemp = (arr[2].Trim().Length > 0 ? decimal.Parse(arr[2].Trim()) : 0m);
                            PriceTemp = (arr[3].Trim().Length > 0 ? decimal.Parse(arr[3].Trim()) : 0m);
                            Mth01Temp = (arr[4].Trim().Length > 0 ? decimal.Parse(arr[4].Trim()) : 0m);
                            Mth02Temp = (arr[5].Trim().Length > 0 ? decimal.Parse(arr[5].Trim()) : 0m);
                            Mth03Temp = (arr[6].Trim().Length > 0 ? decimal.Parse(arr[6].Trim()) : 0m);
                            Mth04Temp = (arr[7].Trim().Length > 0 ? decimal.Parse(arr[7].Trim()) : 0m);
                            Mth05Temp = (arr[8].Trim().Length > 0 ? decimal.Parse(arr[8].Trim()) : 0m);
                            Mth06Temp = (arr[9].Trim().Length > 0 ? decimal.Parse(arr[9].Trim()) : 0m);
                            Mth07Temp = (arr[10].Trim().Length > 0 ? decimal.Parse(arr[10].Trim()) : 0m);
                            Mth08Temp = (arr[11].Trim().Length > 0 ? decimal.Parse(arr[11].Trim()) : 0m);
                            Mth09Temp = (arr[12].Trim().Length > 0 ? decimal.Parse(arr[12].Trim()) : 0m);
                            Mth10Temp = (arr[13].Trim().Length > 0 ? decimal.Parse(arr[13].Trim()) : 0m);
                            Mth11Temp = (arr[14].Trim().Length > 0 ? decimal.Parse(arr[14].Trim()) : 0m);
                            Mth12Temp = (arr[15].Trim().Length > 0 ? decimal.Parse(arr[15].Trim()) : 0m);
                            RemarkTemp = arr[16].Trim();
                            l.Add(new Result()
                            {
                                ItCode = ItCodeTemp,
                                ItName = string.Empty,
                                ProcurementCode = ProcurementTypeTemp,
                                ProcurementName = string.Empty,
                                Qty = QtyTemp,
                                UoM = string.Empty,
                                Price = PriceTemp,
                                Mth01 = Mth01Temp,
                                Mth02 = Mth02Temp,
                                Mth03 = Mth03Temp,
                                Mth04 = Mth04Temp,
                                Mth05 = Mth05Temp,
                                Mth06 = Mth06Temp,
                                Mth07 = Mth07Temp,
                                Mth08 = Mth08Temp,
                                Mth09 = Mth09Temp,
                                Mth10 = Mth10Temp,
                                Mth11 = Mth11Temp,
                                Mth12 = Mth12Temp,
                                Remark = RemarkTemp,
                            });
                        }
                    }
                }
            }
        }

        private void Import2(ref List<Item> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, ItName, PurchaseUOMCode UOM From TblItem ");
            SQL.AppendLine("Union ");
            SQL.AppendLine("Select ItCode, ItName, InventoryUOMCode UOM From TblItem; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Itname", "UOM" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Item()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItName = Sm.DrStr(dr, c[1]),
                            UoM = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import3(ref List<ProcurementType> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As ProcurementType, OptDesc As ProcurementName ");
            SQL.AppendLine("From TblOption Where OptCat = 'ProcurementType'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProcurementType", "ProcurementName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProcurementType()
                        {
                            ProcurementCode = Sm.DrStr(dr, c[0]),
                            ProcurementName = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import4(ref List<Result> l, ref List<Item> l2, ref List<ProcurementType> l3)
        {
            byte v = 0, z = 0;
            foreach (var i in l)
            {
                v = 0;
                foreach (var j in l2)
                {
                    if (Sm.CompareStr(i.ItCode, j.ItCode))
                    {
                        i.ItName = j.ItName;
                        v++;
                    }

                    if (Sm.CompareStr(i.ItCode, j.ItCode))
                    {
                        i.UoM = j.UoM;
                        v++;
                    }
                    if (v == 2) break;
                }

                z = 0;
                foreach (var k in l3)
                {
                    if(Sm.CompareStr(i.ProcurementCode, k.ProcurementCode))
                    {
                        i.ProcurementName = k.ProcurementName;
                        z++;
                    }
                    if (z == 1) break;
                }
            }
        }

        private void Import5(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l[i].ItCode;
                r.Cells[2].Value = l[i].ItName;
                r.Cells[3].Value = l[i].ProcurementCode;
                r.Cells[4].Value = l[i].ProcurementName;
                r.Cells[5].Value = l[i].Qty;
                r.Cells[6].Value = l[i].UoM;
                r.Cells[7].Value = l[i].Price;
                r.Cells[8].Value = l[i].Qty * l[i].Price;
                r.Cells[9].Value = l[i].Mth01;
                r.Cells[10].Value = l[i].Mth02;
                r.Cells[11].Value = l[i].Mth03;
                r.Cells[12].Value = l[i].Mth04;
                r.Cells[13].Value = l[i].Mth05;
                r.Cells[14].Value = l[i].Mth06;
                r.Cells[15].Value = l[i].Mth07;
                r.Cells[16].Value = l[i].Mth08;
                r.Cells[17].Value = l[i].Mth09;
                r.Cells[18].Value = l[i].Mth10;
                r.Cells[19].Value = l[i].Mth11;
                r.Cells[20].Value = l[i].Mth12;
                r.Cells[21].Value = l[i].Remark;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueYr, new Sm.RefreshLue1(Sl.SetLueYr));
        }

        private void LueProcurementType_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueProcurementType, new Sm.RefreshLue2(Sl.SetLueOption));
        }

        private void LueUoMCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueUoMCode, new Sm.RefreshLue1(SetLueUoMCode));
        }

        private void LueProcurementType_Validated(object sender, EventArgs e)
        {
            LueProcurementType.Visible = false;
        }

        private void LueProcurementType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueProcurementType_Leave(object sender, EventArgs e)
        {
            if (LueProcurementType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (LueProcurementType.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueProcurementType).Trim();
                    Grd1.Cells[fCell.RowIndex, 4].Value = LueProcurementType.GetColumnValue("Col2");
                }
            }
        }

        private void LueUoMCode_Validated(object sender, EventArgs e)
        {
            LueUoMCode.Visible = false;
        }

        private void LueUoMCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueUoMCode_Leave(object sender, EventArgs e)
        {
            if (LueUoMCode.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (LueUoMCode.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueUoMCode).Trim();
                    Grd1.Cells[fCell.RowIndex, 7].Value = LueUoMCode.GetColumnValue("Col2");
                }
            }
        }

        private void BtnImport_click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();
            var l2 = new List<Item>();
            var l3 = new List<ProcurementType>();

            Sm.ClearGrd(Grd1, true);
            try
            {
                Import1(ref l);
                if (l.Count > 0)
                {
                    Import2(ref l2);
                    Import3(ref l3);
                    Import4(ref l, ref l2, ref l3);
                    Import5(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                l2.Clear();
                l3.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Class

        private class Result
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ProcurementCode { get; set; }
            public string ProcurementName { get; set; }
            public decimal Qty { get; set; }
            public string UoM { get; set; }
            public decimal Price { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth02 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth04 { get; set; }
            public decimal Mth05 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth07 { get; set; }
            public decimal Mth08 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth10 { get; set; }
            public decimal Mth11 { get; set; }
            public decimal Mth12 { get; set; }
            public string Remark { get; set; }
        }

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string UoM { get; set; }
        }

        private class ProcurementType
        {
            public string ProcurementCode { get; set; }
            public string ProcurementName { get; set; }
        }

        #endregion
    }
}
