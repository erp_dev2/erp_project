﻿#region Update
/*
    01/08/2017 [Ari] tambah kolom Location
    31/01/2018 [WED] tambah copy Display Name ke Main Form WOR
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWORDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmWOR mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmWORDlg(FrmWOR FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset Code",
                        "Asset Name",
                        "Display Name",
                        "Location",
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 150, 300, 150, 150 
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, B.AssetName, B.DisplayName, E.LocName, D.SiteName ");
            SQL.AppendLine("From TblToHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Left Join TblLocation C On A.LocCode = C.LocCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblSite D ");
                SQL.AppendLine("    On C.SiteCode=D.SiteCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=D.SiteCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            }
            SQL.AppendLine("Left Join TblLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Where B.ActiveInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "B.AssetName", "B.DisplayName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AssetName Desc;",
                        new string[]{ "AssetCode", "AssetName", "DisplayName", "LocName", "SiteName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtToCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtAssetName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtDisplayName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtSite.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }
       
        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

     
        #endregion

    }
}
