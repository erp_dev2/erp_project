﻿#region Update
/*
    15/05/2018 [TKG] tambah informasi (category, short name, dll) 
    26/05/2018 [HAR] tambah informasi NIB
    08/06/2018 [TKG] tambah customer group
    27/05/2021 [MYA/SIER] Kolom category pada menu Customer Data terfilter berdasarkan grup log in
    24/01/2023 [IBL/MNET] Tambah kolom customer segmentation berdasarkan param IsCustomerDataUseCustomerSegmentation
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCustomer mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsCustomerCategoryFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmCustomerFind(FrmCustomer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, A.CtName, A.ActInd, A.AgentInd, A.CtShortCode, D.CtCtName, F.CtGrpName, ");
            SQL.AppendLine("A.Address, A.PostalCd, B.CityName, C.CntName, ");
            SQL.AppendLine("A.Phone, A.Fax, A.Mobile, A.Email, A.NPWP, E.ContactPerson, A.NIB, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            if (mFrmParent.mIsCustomerDataUseCustomerSegmentation)
                SQL.AppendLine(", G.OptDesc As CtSegmentName ");
            else
                SQL.AppendLine(", Null As CtSegmentName ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.CntCode=C.CntCode ");
            SQL.AppendLine("Left Join TblCustomerCategory D On A.CtCtCode = D.CtCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat(ContactPersonName, Case When Position Is Null Then '' Else Concat(' (', Position, ')') End) Order By ContactPersonName Separator ', ') As ContactPerson ");
            SQL.AppendLine("    From TblCustomerContactPerson ");
            SQL.AppendLine("    Group By CtCode ");
            SQL.AppendLine(") E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Left Join TblCustomerGroup F On A.CtGrpCode=F.CtGrpCode ");
            if (mFrmParent.mIsCustomerDataUseCustomerSegmentation)
                SQL.AppendLine("Left Join TblOption G On A.CtSegmentCode = G.OptCode And G.OptCat = 'CustomerSegmentation' ");

            if (mIsCustomerCategoryFilteredByGroup)
            {
                SQL.AppendLine("Where A.CtCtCode Is Null Or (A.CtCtCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=A.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode))) ");
            }
            else SQL.AppendLine("Where 0=0 ");
            //SQL.AppendLine("Order By D.CtCtName; ");


            mSQL = SQL.ToString();

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Agent",
                        "Short Code",

                        //6-10
                        "Category",
                        "Group",
                        "Address",
                        "Postal Code",
                        "City",
                        
                        //11-15
                        "Country",
                        "Phone",
                        "Fax",
                        "Mobile",
                        "Email",
                        
                        //16-20
                        "NPWP",
                        "Contact Person",
                        "NIB",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        
                        //21-25
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //26
                        "Customer Segmentation"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 300, 60, 60, 100, 
                        
                        //6-10
                        200, 200, 300, 90, 200, 
                        
                        //11-15
                        200, 120, 120, 120, 150, 

                        //16-20
                        150, 300, 130, 300, 100, 

                        //21-25
                        100, 100, 100, 100, 100,

                        //26
                        150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24, 25 }, false);
            if(!mFrmParent.mIsCustomerDataUseCustomerSegmentation)
                Sm.GrdColInvisible(Grd1, new int[] { 26 }, false);
            Grd1.Cols[26].Move(3);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCtName.Text, new string[] { "A.CtCode", "A.CtName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CtName;",
                        new string[]
                        {
                            //0
                            "CtCode", 
                                
                            //1-5
                            "CtName", "ActInd", "AgentInd", "CtShortCode", "CtCtName", 
                            
                            //6-10
                            "CtGrpName", "Address", "PostalCd", "CityName", "CntName", 
                            
                            //11-15
                            "Phone", "Fax", "Mobile", "Email", "NPWP", 
                            
                            //16-20
                            "ContactPerson", "NIB", "Remark", "CreateBy", "CreateDt", 
                            
                            //21-23
                            "LastUpBy", "LastUpdt", "CtSegmentName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsCustomerCategoryFilteredByGroup = Sm.GetParameterBoo("IsCustomerCategoryFilteredByGroup");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        #endregion

        #endregion
    }
}
