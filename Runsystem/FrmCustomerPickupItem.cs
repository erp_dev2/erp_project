﻿#region Update
/*
    18/08/2020 [WED/MGI] new apps
    24/08/2020 [WED/MGI] DO nya per detail. di header pilih customer aja
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerPickupItem : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty
            ;
        internal FrmCustomerPickupItemFind FrmFind;

        #endregion

        #region Constructor

        public FrmCustomerPickupItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer Pickup Item";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Cancel Reason",
                    "",
                    "Status",
                    
                    //6-10
                    "Item's Code",
                    "",
                    "Local Code",                    
                    "Item's Name",
                    "Batch#",
                    
                    //11-15
                    "DODNo",
                    "DO Quantity",
                    "Uom",
                    "Quantity",
                    "Balance",

                    //16-18
                    "Remark",
                    "Outstanding Pickup",
                    "DO#"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    50, 50, 200, 20, 80, 
                    
                    //6-10
                    100, 20, 100, 250, 120, 
                    
                    //11-15
                    0, 150, 100, 150, 150, 

                    //16-18
                    200, 0, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 15, 17 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 7 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 11, 17 }, false);            
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 5, 6, 8, 9, 10, 11, 12, 13, 15, 17, 18 });
            Grd1.Cols[18].Move(6);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, TxtLocalDocNo, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, MeeRemark, LueCtCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 7, 14, 16 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 4);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 14, 15, 17 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCustomerPickupItemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 4 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCustomerPickupItemDlg(this, Sm.GetLue(LueCtCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 4, 13, 15 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 14, 15, 17 });
                    }
                }
                else
                {
                    //if (!(
                    //    e.ColIndex == 1 &&
                    //    !Sm.GetGrdBool(Grd1, e.RowIndex, 2) &&
                    //    Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0
                    //    ))
                    //    e.DoDefault = false;
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && 
                !Sm.IsLueEmpty(LueCtCode, "Customer") && 
                TxtDocNo.Text.Length == 0 && 
                BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmCustomerPickupItemDlg(this, Sm.GetLue(LueCtCode)));
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 12, 14, 15, 17 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 16 }, e);
                if (Sm.IsGrdColSelected(new int[] { 14, 16 }, e.ColIndex)) ComputeBalance();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CustomerPickupItem", "TblCustomerPickupItemHdr");
            var cml = new List<MySqlCommand>();

            string DODNo = string.Empty;

            cml.Add(SaveCustomerPickupItemHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                {
                    if (DODNo.Length > 0) DODNo += ",";
                    DODNo += string.Concat(Sm.GetGrdStr(Grd1, Row, 18), Sm.GetGrdStr(Grd1, Row, 11));

                    cml.Add(SaveCustomerPickupItemDtl(DocNo, Row));
                }
            }

            cml.Add(UpdateCPIStatus(DODNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            RecomputeOutstanding();
            string DODNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DODNo.Length > 0) DODNo += ",";
                DODNo += string.Concat(Sm.GetGrdStr(Grd1, i, 18), Sm.GetGrdStr(Grd1, i, 11));
            }
            
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsDOItemCancelled(DODNo) ||
                IsItemFulfilled(DODNo)
                ;
        }

        private bool IsDOItemCancelled(string DODNo)
        {
            var SQL = new StringBuilder();
            string ItName = string.Empty;

            if (DODNo.Length > 0)
            {
                SQL.AppendLine("Select Group_Concat(Distinct T.ItName Separator '\r\n') ItName From ( ");

                SQL.AppendLine("Select B.ItName ");
                SQL.AppendLine("From TblDOCtDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("And Find_In_set(Concat(A.DocNo, A.DNo), @Param) ");
                SQL.AppendLine("And A.CancelInd = 'Y' ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select B.ItName ");
                SQL.AppendLine("From TblDOCt2Dtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("And Find_In_set(Concat(A.DocNo, A.DNo), @Param) ");
                SQL.AppendLine("And A.CancelInd = 'Y' ");

                SQL.AppendLine(") T; ");

                ItName = Sm.GetValue(SQL.ToString(), DODNo);

                if (ItName.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This/these item(s) is/are already cancelled." + Environment.NewLine + ItName);
                    return true;
                }
            }

            return false;
        }

        private bool IsItemFulfilled(string DODNo)
        {
            var SQL = new StringBuilder();
            string ItName = string.Empty;

            if (DODNo.Length > 0)
            {
                SQL.AppendLine("Select Group_Concat(Distinct T.ItName Separator '\r\n') ItName From ( ");

                SQL.AppendLine("Select B.ItName ");
                SQL.AppendLine("From TblDOCtDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("And Find_In_set(Concat(A.DocNo, A.DNo), @Param) ");
                SQL.AppendLine("And A.CPIStatus = 'F' ");
                
                SQL.AppendLine("union All ");
                
                SQL.AppendLine("Select B.ItName ");
                SQL.AppendLine("From TblDOCt2Dtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("And Find_In_set(Concat(A.DocNo, A.DNo), @Param) ");
                SQL.AppendLine("And A.CPIStatus = 'F' ");

                SQL.AppendLine(") T; ");

                ItName = Sm.GetValue(SQL.ToString(), DODNo);

                if (ItName.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This/these item(s) is/are already fulfilled." + Environment.NewLine + ItName);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Local Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine
                   ;

                if (Sm.GetGrdDec(Grd1, Row, 14) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 17))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be greater than outstanding quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 17), 0) + ").");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveCustomerPickupItemHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerPickupItemHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, CtCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @CtCode, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCustomerPickupItemDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerPickupItemDtl(DocNo, DNo, DOCtDocNo, DOCtDNo, DRInd, ");
            SQL.AppendLine("CancelInd, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DOCtDocNo, @DOCtDNo, @DRInd, ");
            SQL.AppendLine("'N', @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@DRInd", Sm.GetGrdStr(Grd1, Row, 18).Contains("/DOCDR/") ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";
            string DODNo = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 6).Length > 0 && !Sm.GetGrdBool(Grd1, i, 2))
                {
                    if (Sm.GetGrdBool(Grd1, i, 1))
                    {
                        if (DODNo.Length > 0) DODNo += ",";
                        DODNo += string.Concat(Sm.GetGrdStr(Grd1, i, 18), Sm.GetGrdStr(Grd1, i, 11));

                        cml.Add(EditCustomerPickupItem(i));
                    }
                }
            }

            cml.Add(UpdateCPIStatus(DODNo));
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblCustomerPickupItemDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {            
            return
                IsCancelledItemNotExisted(DNo) ||
                IsItemAlreadyCancelled() ||
                IsCancelReasonEmpty()
                ;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private bool IsItemAlreadyCancelled()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd1, i, 2) == true &&
                    Sm.GetGrdBool(Grd1, i, 1) != Sm.GetGrdBool(Grd1, i, 2)
                    )
                {
                    Sm.StdMsg(mMsgType.Warning, "This item has been cancelled. You can't reactivate it.");
                    Sm.FocusGrd(Grd1, i, 9);
                    return true;
                }
            }

            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCustomerPickupItem(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCustomerPickupItemDtl ");
            SQL.AppendLine("Set CancelInd = 'Y', CancelReason = @CancelReason, ");
            SQL.AppendLine("LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo ");
            SQL.AppendLine("And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowCustomerPickupItemHdr(DocNo);
                ShowCustomerPickupItemDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowDOCt(string DODNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowDOCtDtl2(DODNo);
                ComputeBalance();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCustomerPickupItemHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.CtCode, A.Remark ");
            SQL.AppendLine("From TblCustomerPickupItemHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-4
                        "DocDt", "LocalDocNo", "CtCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));                        
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowCustomerPickupItemDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            string CreateDt = Sm.GetValue("Select CreateDt From TblCustomerPickupItemHdr Where DocNo = @Param;", DocNo);

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason,  ");
            SQL.AppendLine("Case B.CPIStatus When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fulfilled' Else '' End As CPIStatus, ");
            SQL.AppendLine("B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, A.DOCtDNo, (B.Qty - IfNull(D.Qty, 0.00)) DOQty, C.InventoryUomCode, A.Qty, ");
            SQL.AppendLine("(B.Qty - A.Qty - IfNull(D.Qty, 0.00)) Balance, A.Remark, A.DOCtDocNo ");
            SQL.AppendLine("from TblCustomerPickupItemDtl A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DOCtDocNo, T1.DOCtDNo, Sum(T1.Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl T1 ");
            SQL.AppendLine("    Where T1.DocNo != @DocNo ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.CreateDt <= @CreateDt ");
            SQL.AppendLine("    Group By T1.DOCtDocNo, T1.DOCtDNo ");
            SQL.AppendLine(") D On A.DOCtDocNo = D.DOCtDocNo And A.DOCtDNo = D.DOCtDNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason,  ");
            SQL.AppendLine("Case B.CPIStatus When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fulfilled' Else '' End As CPIStatus, ");
            SQL.AppendLine("B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, A.DOCtDNo, (B.Qty - IfNull(D.Qty, 0.00)) DOQty, C.InventoryUomCode, A.Qty, ");
            SQL.AppendLine("(B.Qty - A.Qty - IfNull(D.Qty, 0.00)) Balance, A.Remark, A.DOCtDocNo ");
            SQL.AppendLine("from TblCustomerPickupItemDtl A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DOCtDocNo, T1.DOCtDNo, Sum(T1.Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl T1 ");
            SQL.AppendLine("    Where T1.DocNo != @DocNo ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.CreateDt <= @CreateDt ");
            SQL.AppendLine("    Group By T1.DOCtDocNo, T1.DOCtDNo ");
            SQL.AppendLine(") D On A.DOCtDocNo = D.DOCtDocNo And A.DOCtDNo = D.DOCtDNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateDt", CreateDt);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "CancelReason", "CPIStatus", "ItCode", "ItCodeInternal", 
                    
                    //6-10
                    "ItName", "BatchNo", "DOCtDNo", "DOQty", "InventoryUomCode", 
                    
                    //11-14
                    "Qty", "Balance", "Remark", "DOCtDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 14, 15, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDOCtDtl2(string DODNo)
        {
            var SQL = new StringBuilder();
            var ld = new List<DOCtDtl>();

            SQL.AppendLine("Select A.DNo DOCtDNo, A.ItCode, B.ItCodeInternal, B.ItName, B.InventoryUomCode, A.BatchNo, A.Qty DOQty, ");
            SQL.AppendLine("(A.Qty - IfNull(C.Qty, 0.00)) PickupQty, ");
            SQL.AppendLine("Case A.CPIStatus When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fulfilled' Else '' End As CPIStatus, A.DocNo DOCtDocNo ");
            SQL.AppendLine("From TblDOCtDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(Concat(A.DocNo, A.DNo), @DODNo) ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DOCtDocNo, DOCtDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl ");
            SQL.AppendLine("    Where Find_In_Set(Concat(DOCtDocNo, DOCtDNo), @DODNo) ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    Group By DOCtDocNo, DOCtDNo ");
            SQL.AppendLine(") C On A.DocNo = C.DOCtDocNo And A.DNo = C.DOCtDNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo DOCtDNo, A.ItCode, B.ItCodeInternal, B.ItName, B.InventoryUomCode, A.BatchNo, A.Qty DOQty, ");
            SQL.AppendLine("(A.Qty - IfNull(C.Qty, 0.00)) PickupQty, ");
            SQL.AppendLine("Case A.CPIStatus When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fulfilled' Else '' End As CPIStatus, A.DocNo DOCtDocNo ");
            SQL.AppendLine("From TblDOCt2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(Concat(A.DocNo, A.DNo), @DODNo) ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DOCtDocNo, DOCtDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl ");
            SQL.AppendLine("    Where Find_In_Set(Concat(DOCtDocNo, DOCtDNo), @DODNo) ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    Group By DOCtDocNo, DOCtDNo ");
            SQL.AppendLine(") C On A.DocNo = C.DOCtDocNo And A.DNo = C.DOCtDNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DODNo", DODNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DOCtDNo", 

                    //1-5
                    "ItCode", "ItCodeInternal", "ItName", "InventoryUomCode", "BatchNo", 
                    
                    //6-9
                    "DOQty", "PickupQty", "CPIStatus", "DOCtDocNo"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new DOCtDtl()
                        {
                            DOCtDNo = Sm.DrStr(dr, 0),
                            ItCode = Sm.DrStr(dr, 1),
                            ItCodeInternal = Sm.DrStr(dr, 2),
                            ItName = Sm.DrStr(dr, 3),
                            InventoryUomCode = Sm.DrStr(dr, 4),
                            BatchNo = Sm.DrStr(dr, 5),
                            DOQty = Sm.DrDec(dr, 6),
                            PickupQty = Sm.DrDec(dr, 7),
                            CPIStatus = Sm.DrStr(dr, 8),
                            DOCtDocNo = Sm.DrStr(dr, 9),
                        });
                    }
                }
                dr.Close();
            }

            if (ld.Count > 0)
            {
                foreach (var x in ld)
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[Grd1.Rows.Count - 2, 1].Value = false;
                    Grd1.Cells[Grd1.Rows.Count - 2, 2].Value = false;
                    Grd1.Cells[Grd1.Rows.Count - 2, 3].Value = null;
                    Grd1.Cells[Grd1.Rows.Count - 2, 5].Value = x.CPIStatus;
                    Grd1.Cells[Grd1.Rows.Count - 2, 6].Value = x.ItCode;
                    Grd1.Cells[Grd1.Rows.Count - 2, 8].Value = x.ItCodeInternal;
                    Grd1.Cells[Grd1.Rows.Count - 2, 9].Value = x.ItName;
                    Grd1.Cells[Grd1.Rows.Count - 2, 10].Value = x.BatchNo;
                    Grd1.Cells[Grd1.Rows.Count - 2, 11].Value = x.DOCtDNo;
                    Grd1.Cells[Grd1.Rows.Count - 2, 12].Value = Sm.FormatNum(x.PickupQty, 0);
                    Grd1.Cells[Grd1.Rows.Count - 2, 13].Value = x.InventoryUomCode;
                    Grd1.Cells[Grd1.Rows.Count - 2, 14].Value = Sm.FormatNum(x.PickupQty, 0);
                    Grd1.Cells[Grd1.Rows.Count - 2, 15].Value = Sm.FormatNum(0m, 0);
                    Grd1.Cells[Grd1.Rows.Count - 2, 16].Value = null;
                    Grd1.Cells[Grd1.Rows.Count - 2, 17].Value = Sm.FormatNum(x.PickupQty, 0);
                    Grd1.Cells[Grd1.Rows.Count - 2, 18].Value = x.DOCtDocNo;
                }
            }

            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 14, 15, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private MySqlCommand UpdateCPIStatus(string DODNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update ");
            SQL.AppendLine("TblDOCt2Dtl A ");
            SQL.AppendLine("Left join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DOCtDocNo, DOCtDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And Find_In_Set(Concat(DOCtDocNo, DOCtDNo), @DODNo) ");
            SQL.AppendLine("    Group By DOCtDocNo, DOCtDNo ");
            SQL.AppendLine(") B On A.DocNo = B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo = B.DOCtDNo ");
            SQL.AppendLine("Set A.CPIStatus =  ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When B.Qty Is Null Then 'O' ");
            SQL.AppendLine("        When A.Qty = B.Qty Then 'F' ");
            SQL.AppendLine("        When (A.Qty > B.Qty And B.Qty != 0.00) Then 'P' ");
            SQL.AppendLine("        Else 'O' ");
            SQL.AppendLine("    End ");
            SQL.AppendLine(", A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @DODNo) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update ");
            SQL.AppendLine("TblDOCtDtl A ");
            SQL.AppendLine("Left join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DOCtDocNo, DOCtDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblCustomerPickupItemDtl ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And Find_In_Set(Concat(DOCtDocNo, DOCtDNo), @DODNo) ");
            SQL.AppendLine("    Group By DOCtDocNo, DOCtDNo ");
            SQL.AppendLine(") B On A.DocNo = B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo = B.DOCtDNo ");
            SQL.AppendLine("Set A.CPIStatus =  ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When B.Qty Is Null Then 'O' ");
            SQL.AppendLine("        When A.Qty = B.Qty Then 'F' ");
            SQL.AppendLine("        When (A.Qty > B.Qty And B.Qty != 0.00) Then 'P' ");
            SQL.AppendLine("        Else 'O' ");
            SQL.AppendLine("    End ");
            SQL.AppendLine(", A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @DODNo) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DODNo", DODNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void ComputeBalance()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                decimal Balance = 0m, DOQty = 0m, PickupQty = 0m;

                DOQty = Sm.GetGrdDec(Grd1, i, 12);
                PickupQty = Sm.GetGrdDec(Grd1, i, 14);
                Balance = DOQty - PickupQty;

                Grd1.Cells[i, 15].Value = Sm.FormatNum(Balance, 0);
            }
        }

        private void RecomputeOutstanding()
        {
            var lO = new List<Outstanding>();
            ComputeOutstanding(ref lO);

            if (lO.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    foreach(var x in lO.Where(w => w.DOCtDNo == Sm.GetGrdStr(Grd1, i, 11)))
                    {
                        Grd1.Cells[i, 17].Value = Sm.FormatNum(x.OutstandingQty, 0);
                        break;
                    }
                }
            }
        }

        private void ComputeOutstanding(ref List<Outstanding> lO)
        {
            var SQL = new StringBuilder();
            string DOCtDNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DOCtDNo.Length > 0) DOCtDNo += ",";
                DOCtDNo += string.Concat(Sm.GetGrdStr(Grd1, i, 18), Sm.GetGrdStr(Grd1, i, 11));
            }

            SQL.AppendLine("Select A.DocNo, A.DNo, A.ItCode, (A.Qty - Sum(IfNull(B.Qty, 0.00))) OutstandingQty ");            
            SQL.AppendLine("From TblDOCtDtl A ");
            SQL.AppendLine("Left Join TblCustomerPickUpItemDtl B On A.DocNo = B.DOCtDocNo And A.DNo = B.DOCtDNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @DOCtDNo) ");
            SQL.AppendLine("Group By A.DocNo, A.DNo, A.ItCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DocNo, A.DNo, A.ItCode, (A.Qty - Sum(IfNull(B.Qty, 0.00))) OutstandingQty ");
            SQL.AppendLine("From TblDOCt2Dtl A ");
            SQL.AppendLine("Left Join TblCustomerPickUpItemDtl B On A.DocNo = B.DOCtDocNo And A.DNo = B.DOCtDNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @DOCtDNo) ");
            SQL.AppendLine("Group By A.DocNo, A.DNo, A.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DOCtDNo", DOCtDNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "ItCode", "OutstandingQty" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lO.Add(new Outstanding() 
                        {
                            DOCtDocNo = Sm.DrStr(dr, 0),
                            DOCtDNo = Sm.DrStr(dr, 1),
                            ItCode = Sm.DrStr(dr, 2),
                            OutstandingQty = Sm.DrDec(dr, 3)
                        });
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedDO()
        {
            string data = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    string DODNo = Sm.GetGrdStr(Grd1, i, 11);
                    string DODocNo = Sm.GetGrdStr(Grd1, i, 18);

                    if (data.Length > 0) data += ",";
                    data += string.Concat(DODocNo, DODNo);
                }
            }

            return data;
        }

        #endregion

        #endregion

        #region Class

        private class Outstanding
        {
            public string DOCtDocNo { get; set; }
            public string DOCtDNo { get; set; }
            public string ItCode { get; set; }
            public decimal OutstandingQty { get; set; }
        }

        private class DOCtDtl
        {
            public string DOCtDocNo { get; set; }
            public string DOCtDNo { get; set; }
            public string CPIStatus { get; set; }
            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string InventoryUomCode { get; set; }
            public string BatchNo { get; set; }
            public decimal DOQty { get; set; }
            public decimal PickupQty { get; set; }
        }

        #endregion
    }
}
