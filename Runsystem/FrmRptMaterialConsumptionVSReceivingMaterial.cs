﻿#region Update 
 /*
  13/02/2019 [MEY] Menambahkan kolom Machine disamping Result (SFC)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialConsumptionVSReceivingMaterial : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMaterialConsumptionVSReceivingMaterial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.DocNo, X.DocDt, X.WorkcenterDocNo, X.DocName, X.ItCode, X.Itname, ");
            SQL.AppendLine("SUM(X.Qtybom) As QtyBom, X.UomBom, ");
            SQL.AppendLine("SUM(X.Qtybom2) As QtyBom2, X.UomBom2, ");
            SQL.AppendLine("SUM(X.QtySFC) As QtySFC, X.UomSFc, ");
            SQL.AppendLine("SUM(X.QtySFC2) As QtySFC2, X.UomSFc2, ");
            SQL.AppendLine("X.QtyRecv, X.UomRecv, ");
            SQL.AppendLine("X.QtyRecv2, X.UomRecv2, X.AssetName  ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select T.DocNo, T.DocDt, T.WorkcenterDocNo, T4.DocName, T.ItCode, T3.Itname, ");
            SQL.AppendLine("if(T.Code='1', T.Qty, 0) As Qtybom, ");
            SQL.AppendLine("T.PlanningUomCode As Uombom, ");
            SQL.AppendLine("if(T.Code='1', T.Qty2, 0) As Qtybom2, ");
            SQL.AppendLine("T.PlanningUomCode2 As Uombom2, ");
            SQL.AppendLine("if(T.Code='2', T.Qty, 0) As QtySfc, ");
            SQL.AppendLine("T.PlanningUomCode As UomSFc, ");
            SQL.AppendLine("if(T.Code='2', T.Qty2, 0) As QtySfc2, ");
            SQL.AppendLine("T.PlanningUomCode2 As UomSFc2, ");
            SQL.AppendLine("ifnull(T2.Qty, 0) As QtyRecv, T2.PlanningUomCode As UomRecv, ");
            SQL.AppendLine("ifnull(T2.Qty2, 0) As QtyRecv2, T2.PlanningUomCode2 As UomRecv2, T.AssetName ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WorkcenterDocNo, B.ItCode, '2' As Code,  SUM(B.Qty) As Qty, C.PlanningUomCode,  ");
            SQL.AppendLine("    SUM(B.Qty2) As Qty2, C.PlanningUomCode2, D.AssetName ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.docno = B.DocNo And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join TblAsset D On A.MachineCode = D.AssetCode ");
            SQL.AppendLine("    Where A.cancelInd = 'N'  ");
            SQL.AppendLine("    Group By A.DocNo, A.DocDt, A.WorkcenterDocNo, B.ItCode, C.PlanningUomCode, C.PlanningUomCode2, D.AssetName ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WorkcenterDocNo, B.ItCode, '1' As Code, SUM(B.Qty) As Qty, C.PlanningUomCode, ");
            SQL.AppendLine("    SUM(B.Qty2) As Qty2, C.PlanningUomCode2, D.AssetName ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl B On A.docno = B.DocNo And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join TblAsset D On A.MachineCode = D.AssetCode ");
            SQL.AppendLine("    Where A.cancelInd = 'N'  ");
            SQL.AppendLine("    Group By A.DocNo, A.DocDt, A.WorkcenterDocNo, B.ItCode, C.PlanningUomCode, C.PlanningUomCode2, D.AssetName ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select C.ShopFloorControlDocNo As SfcDocNo, A.DocDt, A.WorkCenterDocNo, C.ItCode, '3' As Code, SUM(B.Qty) As Qty, ");
            SQL.AppendLine("    SUM(B.Qty2) As Qty2, D.PlanningUomCode, D.PlanningUomCode2 ");     
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.Docno = B.Docno ");
            SQL.AppendLine("    Inner Join TblRecvproductionDtl C On A.DocNo =C.ShopFloorControlDocno ");
            SQL.AppendLine("    And B.Dno = C.ShopFloorControlDno And C.cancelInd ='N' ");
            SQL.AppendLine("    Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("    Group by C.ShopFloorControlDocNo, A.WorkCenterDocNo, C.ItCode, D.PlanningUomCode, D.PlanningUomCode2 ");
            SQL.AppendLine(")T2 On T.DocNo = T2.SfcDocNo And T.WorkCenterDocNo = T2.WorkCenterDocNo And T.ItCode = T2.ItCode ");
            SQL.AppendLine("Inner Join TblItem T3 On T.ItCode = T3.ItCode ");
            SQL.AppendLine("Inner Join TblWorkcenterHdr T4 on T.WorkcenterDocNo = T4.DocNo ");
            SQL.AppendLine(" )X");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Workcenter Number", 
                        "Workcenter",
                        "",
                        "Shop Floor Control",
                        "",

                        //6-10
                        "Date",
                        "ItCode",
                        "",
                        "Item Name",
                        "Material"+Environment.NewLine+"Consumption"+Environment.NewLine+"(BOM)",

                        //11-15
                        "UoM",
                        "Material"+Environment.NewLine+"Consumption 2"+Environment.NewLine+"(BOM)",
                        "Uom 2",
                        "Result"+Environment.NewLine+"(SFC)",
                        "Machine",
                   

                        //16-20
                        "UoM",
                        "Result 2"+Environment.NewLine+"(SFC)",
                        "Uom 2",
                        "Received",
                        "UoM",


                        //21
                       "Received 2",
                        "Uom 2",
                    },
                    new int[]
                    {
                        40, 
                        130, 180, 20, 130, 20,
                        100, 80, 20, 230, 120, 
                        80, 120, 80, 100, 200,
                        80, 100, 80, 100, 80, 
                        100, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 14, 17, 19, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 6, 7, 9, 10, 11, 15, 14, 16, 17, 18, 19, 20, 21, 22 }, false);
            Sm.GrdColInvisible(Grd1, new int[] {1, 3, 5, 7, 8, 12, 13, 17,18,21,22}, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 7, 8, 12, 13, 17, 18, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterName.Text, new string[] { "X.WorkCenterDocNo", "X.DocName" });
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By X.DocNo, X.DocDt, X.WorkcenterDocNo, X.DocName, X.ItCode, X.Itname, X.AssetName ",
                        new string[]
                        {
                            //0
                            "WorkCenterDocNo", 

                            //1-5
                            "DocName", "DocNo", "DocDt", "ItCode", "ItName",   
                            
                            //6-10
                            "QtyBom", "UomBom", "QtyBom2", "UomBom2", "QtySfc",   
                            
                            //11-15 
                            "AssetName", "UomSfc", "QtySfc2", "UomSfc2", "QtyRecv",
                            
                            //16-18
                            "UomRecv",  "QtyRecv2", "UomRecv2",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 18);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(2);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 14, 17, 19, 21 });
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo= Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #region Event

        private void ChkWorkCenterName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Workcenter");
        }

        private void TxtWorkCenterName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion
    }
}
