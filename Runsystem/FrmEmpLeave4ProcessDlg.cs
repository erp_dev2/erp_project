﻿#region Update
/* 
  17/02/2020 [HAR/IMS] : leave hour ambil dari leeave yg type nya cuti ijin pribadi, tambah info break 
  19/02/2020 [HAR/IMS] : tambah informasi break 2 
  21/02/2020 [HAR/IMS] : annual leave berdasrakan start date end date leave summary
  03/06/2021 [TRI/IMS] : Fixing Dialog detail emp leave belum tampil (FrmEmpLeave4 diganti FrmEmpLeave)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave4ProcessDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpLeave4Process mFrmParent;
        private string mADType = string.Empty;
        private string mStartDt = string.Empty;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpLeave4ProcessDlg(FrmEmpLeave4Process FrmParent, string StarDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mStartDt = StarDt;
        }

        #endregion



        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetSQL();
                this.Text = "List Document Leave";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] { //0S
                    "No.",
                    //1-5
                    "",
                    "Document",  
                    "",
                    "Document"+Environment.NewLine+"Date", 
                    "Employee"+Environment.NewLine+"Code", 
                    //6-10
                    "Employee"+Environment.NewLine+"Old Code",
                    "Employee"+Environment.NewLine+"Name", 
                    "Department",
                    "Position",
                    "Site",
                    //11 - 15
                    "Start Date",
                    "Start Time",
                    "End Time",
                    "Break",
                    "Break Time Out",
                    //16-19
                    "Break Time In",
                    "Break Time 2 Out",
                    "Break Time 2 In",
                    "Annual"
                },
                new int[] { //0
                    50,
                    //1-5
                    20, 120, 20, 120, 120,
                    //6-10
                    120, 200, 150, 150, 150,
                    //11-15
                    120, 100, 100, 80, 80, 
                    //16-19
                    80, 80, 80, 100
                }
            );
            
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 13, 15, 16, 17, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 19 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNO, A.DocDt, A.EmpCode, B.EmpCodeOld, B.Empname, C.DeptName, D.PosName, E.Sitename, ");
            SQL.AppendLine("A.StartDt, A.StartTm StartTm, A.Endtm Endtm, A.BreakInd, ");
            SQL.AppendLine("if(A.BreakInd = 'Y', G.In2, null) In2, if(A.BreakInd = 'Y', G.Out2, null) Out2,  ");
            SQL.AppendLine("if(A.BreakInd = 'Y', G.In4, null) In4, if(A.BreakInd = 'Y', G.Out4, null) Out4, ");
            SQL.AppendLine("if(A.StartDt<F.StartDt, 0,  ");
            SQL.AppendLine("If(A.StartDt>=F.StartDt,   ");
            SQL.AppendLine("if(A.StartDt<=F.EndDt, F.Balance,  ");
            SQL.AppendLine("0), 0)) As BalAnnualAmt    ");
            SQL.AppendLine("From TblEmpLeaveHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join tbldepartment C On b.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D on B.PosCode = D.posCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode = E.SiteCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, StartDt, EndDt, Balance From TblleaveSummary ");
            SQL.AppendLine("    Where Yr = Left(@StartDt, 4) ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    select A.Dt, A.EmpCode, B.In2, B.Out2, B.In4, B.Out4 ");
	        SQL.AppendLine("    from TblEmpWorkSchedule A ");
	        SQL.AppendLine("    Inner Join TblWorkSchedule B On A.WSCode = b.WsCode ");
            SQL.AppendLine(")G On A.EmpCode = G.EmpCode And A.StartDt = G.Dt  ");
            SQL.AppendLine("Where A.cancelind = 'N' And A.Status = 'A' And left(A.StartDt, 6)  = @StartDt And A.LeaveCode = @LeaveCode ");
            SQL.AppendLine("And A.DocNo not in ( ");
            SQL.AppendLine("   Select B.LeaveHourDocNo From  tblempleavehourprocesshdr A ");
            SQL.AppendLine("   Inner Join tblempleavehourprocessDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("   Where A.CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL =  SQL.ToString();
        }

        override protected void ShowData()
        {
             if (
                 mStartDt.Length == 0
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

              
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@StartDt", mStartDt);
                Sm.CmParam<String>(ref cm, "@LeaveCode", mFrmParent.mLeaveCodePermisionPersonal);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "Order By A.CreateDt ",
                        new string[] { 
                        
                            //0
                            "DocNo",  

                            //1-5
                            "DocDt", "EmpCode", "EmpCodeOld", "Empname", "DeptName", 
                            
                            //6-10
                            "PosName", "SiteName", "StartDt", "StartTm", "Endtm",
                            //11-15
                            "BreakInd", "In2", "Out2", "In4", "Out4", 
                            //16
                            "BalAnnualAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 19);
                        mFrmParent.Grd1.Cells[Row1, 13].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 14].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 15].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 16].Value = null;
                        mFrmParent.Grd1.Rows.Add();

                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document");
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    key, Sm.GetGrdStr(mFrmParent.Grd1, Index, 2)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmpLeave(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmEmpLeave(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        

        #endregion

        

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        #endregion
    }
}
