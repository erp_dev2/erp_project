﻿#region Update
// 2017/09/28 [HAR] retur POS nilainya minus
// 2018/03/22 [HAR] bug saat manggil form transaksi sales TOP 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesPersonPerformanceDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptSalesPersonPerformance mFrmParent;
        private string mSQL = string.Empty, mSPCode = string.Empty, mSPName = string.Empty, mYr = string.Empty, mMth = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesPersonPerformanceDlg(FrmRptSalesPersonPerformance FrmParent, String SPCode, String SPName, String Yr, String Mth)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSPCode = SPCode;
            mSPName = SPName;
            mYr = Yr;
            mMth = Mth;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                ShowData();
                TxtMth.Properties.ReadOnly = TxtSalesPerson.Properties.ReadOnly = TxtYear.Properties.ReadOnly = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select X.DocNo, X.Dt, X.DocType, X.CtCode, X.DocDt,  X3.Ctname, X.SpCode, X.Salesname, X.Yr, X.Mth,  SUM(X.Amtinvoice) AmtInv, SUM(AmtAdd) AmtAdd, SUM(AmtNet) AmtNet, ifnull(X2.Amt, 0) As AmtVc, SUM(Amtmin) AmtMin  ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.CtCode,  if(A.SODocNo is null, 'TOP','Non TOP') As DocType, A.DocDt, C.SpCode, A.DocNo, A.Salesname, Left(A.DocDt, 4) As Yr,  ");
            SQL.AppendLine("        SubString(A.DOcDt, 5, 2) As mth, Right(A.DocDt, 2) As Dt, A.Amt As AmtInvoice, ifnull(B.Freight, 0) As AmtAdd, ifnull(D.AddDisc, 0) As AmtMin,  ");
            SQL.AppendLine("        (A.Amt-ifnull(B.Freight, 0)) As AmtNet ");
            SQL.AppendLine("        From TblsalesInvoicehdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, SUM(CAmt) As Freight  ");
            SQL.AppendLine("            from TblSalesInvoiceDtl2 ");
            SQL.AppendLine("            Where AcInd = 'N' And OptAcDesc = '1'   ");
            SQL.AppendLine("            Group By DocNo ");
            SQL.AppendLine("        )B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        left Join Tblsalesperson C On A.SalesName = C.SPname ");
            SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select DocNo, (DAmt+Camt) As AddDisc   ");
            SQL.AppendLine("            from TblSalesInvoiceDtl2   ");
            SQL.AppendLine("            Where OptAcDesc = '2'     ");
            SQL.AppendLine("        )D On A.DocNo = D.DocNo  ");
            SQL.AppendLine("        Where A.cancelInd = 'N'  ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X.InvoiceDocNo, SUM(X.Amt) Amt  From  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select B.InvoiceDocNo, B.Amt ");
            SQL.AppendLine("            From TblIncomingPaymenthdr A ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl B On A.DocNo = B.Docno ");
            SQL.AppendLine("            Inner Join TblIncomingpaymentHdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("            Inner Join tblVoucherRequesthdr D On C.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblVoucherhdr E On D.VoucherDocno = E.DocNo ");
            SQL.AppendLine("            Where A.cancelInd = 'N' ");
            SQL.AppendLine("        )X ");
            SQL.AppendLine("        Group By X.InvoiceDocNo ");
            SQL.AppendLine("    )X2 On X.DocNo = X2.InvoiceDocNo ");
            SQL.AppendLine("    Inner Join tblCustomer X3 On X.CtCode = X3.CtCode");
            SQL.AppendLine("    Where X.SPCode=@SpCode And X.Mth=@Mth And X.Yr=@Yr ");
            SQL.AppendLine("    Group BY X.DocNo, X.Dt, X.DocType, X.CtCode, X.DocDt,  X3.Ctname, X.SpCode, X.Salesname, X.Yr, X.Mth ");
            
            //ini untuk nyambungin ke transkasi POS
            SQL.AppendLine("    UNION ALL");
            SQL.AppendLine("    Select Z1.TrnNo, Z1.Dt, if(Z1.SlsType='S', 'POS', 'POS (Retur)') As DocType,  ");
            SQL.AppendLine("    (Select Parvalue From TblParameter A Where parCode = 'StoreCtCode') As CtCode,  ");
            SQL.AppendLine("    Z1.BSDate,  ");
            SQL.AppendLine("    (Select CtName From TblCustomer Where CtCode = (Select Parvalue From TblParameter A Where parCode = 'StoreCtCode') ) As CtName, ");
            SQL.AppendLine("    Z1.SpCode, Z2.Spname,  Z1.Yr, Z1.mth, Z1.PayAmtNett, 0, If(length(Z1.VoucherDocNo) = 0, 0, Z1.PayAmtNett), If(length(Z1.VoucherDocNo) = 0, 0, Z1.PayAmtNett) As VcAmt, 0 ");
            SQL.AppendLine("    From (    ");
            SQL.AppendLine("          Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth,  ");
            SQL.AppendLine("          Right(A.BSDate, 2) As Dt, if(A.SlsType='S', B.PayAmtNett, (-1*B.PayAmtNett)) As PayAmtNett, D.VoucherDocNo, A.SlsType ");
            SQL.AppendLine("          From TblPostrnhdr A    ");
            SQL.AppendLine("          Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo   "); 
            SQL.AppendLine("          Inner Join Tblsalesperson C On A.UserCode = C.UserCode  ");
		    SQL.AppendLine("            Left Join  ");
		    SQL.AppendLine("            ( ");
			SQL.AppendLine("                select DocDt, Group_concat(VoucherDocNo) VoucherDocNo   ");
			SQL.AppendLine("                From TblVoucherRequestHdr D  ");
			SQL.AppendLine("                Where  DocType = '10' And D.CancelInd='N' ");
			SQL.AppendLine("                Group By DocDt ");
		    SQL.AppendLine("            )D On A.BsDate = D.DocDt  ");
            SQL.AppendLine("    )Z1    ");
            SQL.AppendLine("    Inner Join Tblsalesperson Z2 On Z1.SPCode = Z2.SpCode ");
            SQL.AppendLine("    Where Left(Z1.BSDate, 4) = @Yr  ");
            SQL.AppendLine("    And Z1.SpCode = @SpCode And Z1.Mth=@Mth");
            SQL.AppendLine(")Z ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document Number", 
                        "Document Date",
                        "",
                        "Type",
                        "Customer",

                        //6-10
                        "SO Amount",
                        "Additional Discount",
                        "Additional Freight",
                        "Invoice Net", 
                        "Invoice",
                        //11
                        "Voucher",
                    },
                    new int[] 
                    {
                        //0
                        30,
                        //1-5
                        150, 120, 20, 80, 200,    
                        //6-10
                        150, 150, 150, 150, 150,
                        //11
                        150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10,11 }, false);
            Sm.GrdColInvisible(Grd1, new int[] {  },false);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@SpCode", mSPCode);
                Sm.CmParam<String>(ref cm, "@Mth", mMth);

                Sm.FilterStr(ref Filter, ref cm, mSPCode, "Z.SPCode", true);
                Sm.FilterStr(ref Filter, ref cm, mYr, "Z.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, mMth, "Z.Mth", true);
           
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order by Z.DocDt ",
                        new string[]
                        {
                            //0
                            "DocNo",  
                            //1-5
                            "DocDt", "DocType", "CtName", "AmtMin", "AmtAdd", 
                            //6
                            "AmtNet", "AmtInv", "AmtVc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                            Grd1.Cells[Row, 6].Value = Sm.GetGrdDec(Grd1, Row, 9) + Sm.GetGrdDec(Grd1, Row, 7);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8, 9, 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "TOP")
                {
                    var f1 = new FrmSalesInvoice("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "Non TOP")
                {
                    var f1 = new FrmSalesInvoice2("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
            }

           
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex,1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "TOP")
                {
                    var f1 = new FrmSalesInvoice("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "Non TOP")
                {
                    var f1 = new FrmSalesInvoice2("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
            }
        }
        private void BtnExcell_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion       

        #endregion

      

    }
}
