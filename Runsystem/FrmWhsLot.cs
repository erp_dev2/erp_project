﻿#region Update
/*
    08/04/2020 [DITA/SIER] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWhsLot : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        iGCell fCell;
        bool fAccept;
        internal FrmWhsLotFind FrmFind;

        #endregion

        #region Constructor

        public FrmWhsLot(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Warehouse-Lot";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetLueLot(ref LueLot);
                LueLot.Visible = false;
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Warehouse Code",
                        
                        //1-2
                        "Lot",
                        "Remark",

                       

                    },
                     new int[] 
                    {
                        //0
                        0,
                        
                        //1-2
                        100, 200
                    }
                );
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueWhsCode, MeeRemark, LueLot
                    }, true);

                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2});
                    ChkActInd.Enabled = false;
                    LueWhsCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueWhsCode, MeeRemark, LueLot
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 });
                    LueWhsCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                      MeeRemark, LueLot
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 });
                    ChkActInd.Enabled = true;
                    MeeRemark.Focus();
                    break;
            }
        }

        private void ClearData()
        {

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueWhsCode, MeeRemark, LueLot
            });
            ChkActInd.Checked = false;
            ClearGrd();

        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWhsLotFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                return;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWhsCode, "")) return;
            else if (ChkActInd.Checked == false) Sm.StdMsg(mMsgType.Warning, "Document is inactive.");
            else SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
               InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                    if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                    {
                        if (e.ColIndex == 1) LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }

             }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {

            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
           
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWhsLotHdr());
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveWhsLotDtl(Sm.GetLue(LueWhsCode), Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(Sm.GetLue(LueWhsCode));

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Lot is empty.")
                    ) return true;
            }

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                {
                    if ((Sm.GetGrdStr(Grd1, r1, 1) == Sm.GetGrdStr(Grd1, r2, 1)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate Lot: " + Sm.GetGrdStr(Grd1, r2, 1) + " found");
                        Sm.FocusGrd(Grd1, r2, 1);
                        return true;
                    }
                }
            }

            return false;
        }


        private MySqlCommand SaveWhsLotHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWhsLotHdr(WhsCode, ActInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WhsCode, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update ActInd=@ActInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblWhsLotDtl Where WhsCode =@WhsCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWhsLotDtl(string WhsCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWhsLotDtl(WhsCode, Lot, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@WhsCode, @Lot, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string WhsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowWhsLotHdr(WhsCode);
                ShowWhsLotDtl(WhsCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWhsLotHdr(string WhsCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select WhsCode, ActInd, Remark");
            SQL.AppendLine("From TblWhsLotHdr ");
            SQL.AppendLine("Where WhsCode=@WhsCode; ");

            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "WhsCode", 

                        //1-2
                        "ActInd", "Remark"
                        
 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[0]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[2]);

                    }, true
                );
        }


        private void ShowWhsLotDtl(string WhsCode)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT WhsCode, Lot, Remark ");
            SQL.AppendLine("FROM TblWhsLotDtl  ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("Order By Lot");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "WhsCode",
 
                    //1-2
                    "Lot", "Remark"
 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot", "Lot");
        }


        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }
        #endregion
    }
}
