﻿#region Update
/*
    20/11/2020 [WED/IMS] new apps
    14/12/2020 [WED/IMS] menampilkan SO Contract yg masih ada item belom ter DO
    01/02/2021 [WED/IMS] bug query
    17/06/2021 [VIN/IMS] SO COntract Cancel tidak dapat ditarik di DOCt
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt8Dlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt8 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOCt8Dlg6(FrmDOCt8 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
        }

        #endregion

        #region Standard Methods

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T4.ProjectCode, IfNull(T4.ProjectName, T3.ProjectName) ProjectName, T1.PONo ");
            SQL.AppendLine("From TblSOContractHdr T1 ");
            SQL.AppendLine("Inner Join TblBOQHdr T2 On T1.BOQDocNO = T2.DocNo ");
            SQL.AppendLine("    And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("INner Join TblLOPHdr T3 On T2.LOPDocNo = T3.DocNo And T3.CtCode = @CtCode ");
            SQL.AppendLine("Left Join TblProjectGroup T4 On T3.PGCode = T4.PGCode ");
            SQL.AppendLine("Where T1.DocNo In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct A.DocNo ");
            SQL.AppendLine("    From TblSOContractDtl A  ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.SOContractDocNo, X2.SOContractDNo, Sum(X2.Qty) DOQty ");
            SQL.AppendLine("        From TblDOCtHdr X1 ");
            SQL.AppendLine("        Inner Join TblDOCtDtl X2 On X1.DocNo = X2.DocNo And X2.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.SOContractDocNo Is Not Null ");
            SQL.AppendLine("            And X2.SOContractDNo Is Not Null ");
            SQL.AppendLine("        Group By X1.SOContractDocNo, X2.SOContractDNo ");
            SQL.AppendLine("    ) B On A.DocNo = B.SOContractDocNo And A.DNo = B.SOContractDNo ");
            SQL.AppendLine("    Where A.Qty - IfNull(B.DOQty, 0.00) > 0 ");
            SQL.AppendLine(")AND T1.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Project Code",
                    "Project Name",

                    //6
                    "Customer PO#"

                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    140, 20, 80, 120, 200, 

                    //6
                    140
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mFrmParent.mIsAbleToAccessSOContractData)
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(mFrmParent.LueCtCode));
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, "T4.ProjectCode", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, "T1.PONo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T1.CreateDt Desc; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-4
                        "DocDt", "ProjectCode", "ProjectName", "PONo"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtSOContractDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);

                if (mFrmParent.TxtSOContractDocNo.Text.Length > 0) mFrmParent.ClearGrd();
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Code");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        #endregion

        #endregion

    }
}
