﻿#region Update 
/*
    19/01/2021 [VIN/IMS] Tambah kolom ItCodeInternal, Spec berdasarkan parameter IsBOMShowSpecifications
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

#region Note Programmer
  // (HAR) 04/18/2017 perubahan di query left join ke subcategory untuk tipe runsystem, clear grid saat refresh button  
#endregion

namespace RunSystem
{
    public partial class FrmReorderPoint : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmReorderPointFind FrmFind;
        internal string
           mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int ColInd = 0;
        internal bool mIsItemCategoryMandatory = false,
            mIsBOMShowSpecifications = false;
        string paramFormulaX = string.Empty;

        #endregion

        #region Constructor

        public FrmReorderPoint(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Inventory Level Setup";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                BtnPrint.Visible = false;
                paramFormulaX = Sm.GetParameter("ReorderPointFormulaCodeRunsystem");
                base.FrmLoad(sender, e);
                SetGrd();
                SetLueOptCode(ref LueOptCode);
                Sl.SetLueItCtCode(ref LueItCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetLueFormulaCode(ref LueFormulaCode);
                SetLueItGrpCode(ref LueItGrpCode);

               
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method
        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Item Code",
                        "Item's Local Code",
                        "Item Name",
                        "Item Category",
                        "Sub Category",

                        //6-10
                        "UsageDt vs recvDt",
                        "Avg Usage Qty",
                        "MRDt Vs recvDt",
                        "Price",
                        "Minimum Stock"+Environment.NewLine+"Recomended",

                        //11-15
                        "Minimum Stock",
                        "Reorder Point",
                        "Minimum Stock"+Environment.NewLine+"value",
                        "Reorder Point"+Environment.NewLine+"Value",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        40,
                        //1-5
                        100, 130, 250, 150, 150,  
                        //6-10
                        100, 100, 100, 100, 100,  
                        //11-15
                        100, 100, 150, 150, 150
                    }

                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd1, new int[] {  6, 7, 8, 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 8, 9 }, false);
            if(!mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            Grd1.Cols[15].Move(4);

        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueOptCode, TxtKoefisien, TxtItCode, LueItCtCode, LueItGrpCode, LueWhsCode, LueFormulaCode, LueItScCode, MeeRemark,
                        ChkItCode, ChkItCtCode, ChkItScCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    TxtROPValue.Enabled = false;
                    TxtMinStockValue.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueOptCode, TxtKoefisien, TxtItCode, LueItCtCode, LueItGrpCode, LueWhsCode, LueFormulaCode, MeeRemark,
                        ChkItCode, ChkItCtCode, ChkItScCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 11 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueOptCode, TxtKoefisien, TxtItCode, LueItCtCode, LueWhsCode, LueItCtCode, LueFormulaCode, LueItScCode 
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtMinStockValue, TxtROPValue
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtMinStockValue, TxtROPValue
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmReorderPointFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                TxtKoefisien.EditValue = Sm.FormatNum(1, 0);
                TxtROPValue.EditValue = Sm.FormatNum(0, 0);
                TxtMinStockValue.EditValue = Sm.FormatNum(0, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           
        }


        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFormulaCode) != paramFormulaX)
            {
                if (!Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    SetSQL();
                    ShowDataItemReorderPoint();
                }
            }
            else
            {
                SetSQL();
                ShowDataItemReorderPoint();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
           
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
           
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 11)
            {
                decimal AvgQty = 0m, UPrice = 0m, UsageDtRecv = 0m,
                  MrDtRecv = 0m, koefisien = 0m, ROP = 0m, MinStock = 0m;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0) UsageDtRecv = Sm.GetGrdDec(Grd1, e.RowIndex, 6);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0) AvgQty = Sm.GetGrdDec(Grd1, e.RowIndex, 7);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0) MrDtRecv = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0) UPrice = Sm.GetGrdDec(Grd1, e.RowIndex, 9);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0) MinStock = Sm.GetGrdDec(Grd1, e.RowIndex, 11);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0) ROP = Sm.GetGrdDec(Grd1, e.RowIndex, 12);

                koefisien = Decimal.Parse(TxtKoefisien.Text);

                Grd1.Cells[e.RowIndex, 12].Value = (AvgQty * MrDtRecv) + MinStock;
                Grd1.Cells[e.RowIndex, 13].Value = MinStock * UPrice;
                Grd1.Cells[e.RowIndex, 14].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 12) * UPrice;

                ComputeShowTotal();
            }
        }

        #endregion

        #region Save Data

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ReOrderPoint", "TblReorderPointHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveROPHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveROPDtl(DocNo, Row));
                    cml.Add(UpdateROPItem(Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
            FocusGrid(ColInd);
        }

        private void FocusGrid(int Col)
        {
            Sm.FocusGrd(Grd1, Col, 0);
            Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueOptCode, "Option Month") ||
                Sm.IsTxtEmpty(TxtKoefisien, "Coefficient", true) ||
                Sm.IsLueEmpty(LueFormulaCode, "Formula") ||
                IsGrdEmpty() ||
                IsItemCategoryNull()
                ;
        }

        private MySqlCommand SaveROPHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblReOrderPointHdr(DocNo, DocDt, Monthly, Coefficient, WhsCode, FormulaCode, ItCtCode, ItScCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Monthly, @Coefficient, @WhsCode, @FormulaCode, @ItCtCode, @ItScCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Monthly", Sm.GetLue(LueOptCode));
            Sm.CmParam<Decimal>(ref cm, "@Coefficient", Decimal.Parse(TxtKoefisien.Text));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@FormulaCode", Sm.GetLue(LueFormulaCode));
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueItScCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveROPDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblReOrderPointDtl(DocNo, DNo, ItCode, MinStock, ReOrderPoint, UPrice, MinStockValue, ReorderPointValue,  CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ItCode, @MinStock, @ReOrderPoint, @UPrice, @MinStockValue, @ReorderPointValue, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@MinStock", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@ReOrderPoint", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@MinStockValue", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@ReorderPointValue", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateROPItem(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "   Update TblItem SET MinStock=@MinStock, ReOrderStock=@ReOrderPoint, " +
                "   LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where ItCode=@ItCode "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItName", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@MinStock", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@ReOrderPoint", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to select at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsItemCategoryNull()
        {
            if (mIsItemCategoryMandatory)
            {
                if (!Sm.IsLueEmpty(LueItCtCode, "Item Category"))
                return true;
            }
            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowROHdr(DocNo);
                ShowRODtl(DocNo);
                ComputeShowTotal();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowROHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Monthly, Coefficient, WhsCode, FormulaCode, ItCtCode, ItScCode, Remark From TblReorderPointhdr Where DocNo=@DocNo",
                    new string[] { 
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",  "Monthly", "Coefficient", "WhsCode", "FormulaCode", 
                        //6
                        "ItCtCode", "ItScCode", "Remark"
                         },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueOptCode, Sm.DrStr(dr, c[2]));
                        TxtKoefisien.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueFormulaCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueItCtCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueItScCode, Sm.DrStr(dr, c[7]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowRODtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.ItCode, B.ItCodeInternal, B.ItName, C.ItCtName, D.ItScName, A.UPrice, "); 
            SQL.AppendLine("A.MinStock, A.ReorderPoint, A.MinStockValue, A.ReorderPointValue, B.Specification  ");
            SQL.AppendLine("From TblReorderPointDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory C On B.ItCtCode = C.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubcategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItCodeInternal", "ItName", "ItCtName",  "ItScName",
                        
                    //6-10
                    "UPrice",  "MinStock", "ReorderPoint", "MinStockValue","ReorderPointValue", 

                    //11
                    "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);

                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetSQL()
        {
            StringBuilder SQL = new StringBuilder();
            string formula = Sm.GetLue(LueFormulaCode);
            string paramFormula = Sm.GetParameter("ReorderPointFormulaCodeRunsystem");

            #region rumus runsystem
            if (formula==paramFormula)
            {
                SQL.AppendLine("Select X.MonthFilter, X.ItCode, X.ItCodeInternal, X.ItName, X.ItCtCode, X.ItCtName, X.ItScName, X.AvgUsageDtVsRecvDt, X.Qty, X.AvgMRDtVsRecvDt, X.MinStock, X.MinStock2, X.Reorder, X.Uprice, X.MinStockValue, X.ReorderValue, X.ReorderValue ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Z1.MonthFilter, S1.ItCode, S1.ItCodeInternal, S1.ItName, S1.ItCtCode, Y1.ItCtName, Y2.ItScName, Round(Z1.AvgUsageDtVsRecvDt, 0) As AvgUsageDtVsRecvDt, Round(Z1.Qty, 0) As Qty, Round(Z1.AvgMRDtVsRecvDt, 0) As AvgMRDtVsRecvDt, Round(Z1.MinStock, 0) MinStock, round(if(Z1.MinStock<1, 0, Z1.MinStock),0) As MinStock2, Round(Z1.Reorder,0) As Reorder, Z1.Uprice, (ifnull(Round(Z1.Minstock, 0), 0)*ifnull(Z1.Uprice, 0)) As MinStockValue, ");
                SQL.AppendLine("    (ifnull(round(Z1.Reorder, 0), 0)*ifnull(Z1.Uprice, 0)) As ReorderValue");
                SQL.AppendLine("    From TblItem S1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select Z1.MonthFilter, Z1.ItCtCode, Z1.ItCode, Z1.ItName, Z1.AvgUsageDtVsRecvDt, Z1.Qty, Z1.AvgMRDtVsRecvDt, ");
                SQL.AppendLine("        (Z1.AvgUsageDtVsRecvDt * Z1.Qty * @Tho) As MinStock, ");
                SQL.AppendLine("        (Z1.Qty * (Z1.AvgMRDtVsRecvDt + (Z1.AvgUsageDtVsRecvDt* @Tho))) As Reorder,  Z1.QtDocNo, Z1.UPrice ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select TT.MonthFilter, TT.ItCtCode, TT.ItCode, TT.ItName,  ");
                SQL.AppendLine("            Case when MRDtVsRecvDt <> 0 then (MRDtVsRecvDt/MRDtVsRecvDtCount) else 0 end As AvgMRDtVsRecvDt, ");
                SQL.AppendLine("	        Case when UsageDtVsRecvDt <> 0 then (UsageDtVsRecvDt/UsageDtVsRecvDtCount) else 0 end As AvgUsageDtVsRecvDt, IfNull(Y1.Qty, 0) As Qty, TT.QtDocNo, TT.UPrice ");
                SQL.AppendLine("            From ( ");
                SQL.AppendLine("                Select  T.MonthFilter, T.ItCtCode, T.ItCode, T.ItName, ");
                SQL.AppendLine("                Sum(IfNull(DateDiff(RecvDt, UsageDt), 0)) As UsageDtVsRecvDt, ");
                SQL.AppendLine("                Sum(Case when DateDiff(RecvDt, UsageDt) is null then 0 else 1 end) As UsageDtVsRecvDtCount, ");
                SQL.AppendLine("                Sum(IfNull(DateDiff(RecvDt, MRDt), 0)) As MRDtVsRecvDt, ");
                SQL.AppendLine("                Sum(Case when DateDiff(RecvDt, MRDt) is null then 0 else 1 end) As MRDtVsRecvDtCount, T.QtDocNo, T.UPrice ");
                SQL.AppendLine("                From ( ");
                SQL.AppendLine("                Select T4.ItCtCode, T4.ItCode, T4.ItName, T2.DocNo, Left(T2.DocDt, 6) As MonthFilter, ");
                SQL.AppendLine("                T2.DocDt As MRDt, T3.UsageDt, T6.DocDt As POReqDt, ");
                SQL.AppendLine("                    T8.DocDt As PODt, T7.EstRecvDt As ETADt, T10.DocDt As RecvDt, IfNull(T10.POInd, 'Y') As NotAutoPO, T5.QtDocNo, T12.UPrice  ");
                SQL.AppendLine("                    from tblmaterialrequesthdr T2 ");
                SQL.AppendLine("                    inner join tblmaterialrequestdtl T3 on T3.DocNo=T2.DocNo And T3.CancelInd='N' And T3.Status<>'C' ");
                SQL.AppendLine("                    Left join tblitem T4 on T4.ItCode=T3.ItCode ");
                SQL.AppendLine("                    left join tblporequestdtl T5 on T5.MaterialRequestDocNo=T3.DocNo AND T5.MaterialRequestDNo=T3.DNo  And T5.CancelInd='N' And T5.Status<>'C' ");
                SQL.AppendLine("                    left join tblporequesthdr T6 on T6.DocNo=T5.DocNo ");
                SQL.AppendLine("                    left join tblpodtl T7 on T7.PORequestDocNo=T5.DocNo and T7.PORequestDNo=T5.DNo And T7.CancelInd='N' ");
                SQL.AppendLine("                    left join tblpohdr T8 on T8.DocNo=T7.DocNo ");
                SQL.AppendLine("                    left join tblrecvvddtl T9 on T9.PODocNo=T7.DocNo AND T9.PODNo=T7.DNo And T9.CancelInd='N' ");
                SQL.AppendLine("                    left join tblrecvvdhdr T10 on T10.DocNo=T9.DocNo");
                SQL.AppendLine("                    Left Join TblQtHdr T11 On T5.QtDocNo=T11.DocNo ");
                SQL.AppendLine("                    Left Join TblQtDtl T12 On T5.QtDocNo=T12.DocNo And T5.QtDNo=T12.DNo ");
                SQL.AppendLine("	                Where T2.DocDt Between @DocDt1 And @DocDt2  ");
                SQL.AppendLine("                ) T ");
                SQL.AppendLine("                where T.NotAutoPO = 'Y' And T.MonthFilter Between @MonthFilter1 And @MonthFilter2 ");
                SQL.AppendLine("                Group By  T.ItCtCode, T.ItCode, T.ItName ");
                SQL.AppendLine("            ) TT ");
                SQL.AppendLine("            Left join ( ");
                SQL.AppendLine("	            Select B.ItCode,  D.ItName, B.Lot, B.Bin, ");
                SQL.AppendLine("		        SUM(B.Qty) / (dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d'))) As Qty, ");
                SQL.AppendLine("		        D.InventoryUomCode ");
                SQL.AppendLine("                From TblDODeptHdr A ");
                SQL.AppendLine("                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("                Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
                SQL.AppendLine("                Left Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt1 And @DocDt2  ");
                SQL.AppendLine("                Group By B.ItCode, D.InventoryUomCode, B.Lot, B.Bin");
                SQL.AppendLine("	        )Y1 On Y1.itCode = TT.ItCode");
                SQL.AppendLine("            Where TT.MonthFilter Between @MonthFilter1 And @MonthFilter2 ");
                SQL.AppendLine("        )Z1 Group By Z1.ItCode ");
                SQL.AppendLine("    )Z1 On Z1.ItCode = S1.ItCode  ");
                SQL.AppendLine("    Inner Join TblItemCategory Y1 On Y1.ItCtCode = S1.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemSubCategory Y2 On S1.ItScCode = Y2.ItScCode ");
                SQL.AppendLine("    Where S1.InventoryItemInd = 'Y' And S1.ActInd = 'Y' ");
                if(Sm.GetLue(LueItScCode).Length >0)
                {
                    SQL.AppendLine("And S1.ItScCode=@ItScCode ");
                }
                if(Sm.GetLue(LueItGrpCode).Length>0)
                {
                    SQL.AppendLine("And S1.ItGrpCode=@ItGrpCode ");
                }
                SQL.AppendLine(")X ");
            }
            #endregion

            #region rumus internal 1 (monthly usage + stock minimal - stok akhir) 
            else if (Sm.GetLue(LueFormulaCode) == Sm.GetParameter("ReorderFormulaCodeInternal"))
            {
                SQL.AppendLine("Select X.ItCode, X.ItCodeInternal, X.ItName, X.ItCtName, X.ItScname, X.AvgUsageDtVsRecvDt, (X.Qty*-1) As Qty, X.AvgMRDtVsRecvDt,  X.Uprice,  Ceil(X.MinStock) As MinStock, Ceil(X.MinStock) As MinStock2, ");
                SQL.AppendLine("Ceil(X.Reorder) As Reorder, (Ceil(X.MinStock) * X.UPrice) As MinStockValue, (Ceil(X.Reorder) * X.Uprice) ReorderValue,  X.ItCtCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.ItCode, D.ItCodeInternal, D.ItName, D.ItCtCode, E.ItCtName, F.ItScname, 0 As AvgUsageDtVsRecvDt, A.Qty, ");
                SQL.AppendLine("    0 As AvgMRDtVsRecvDt, 0 Uprice,   ");
                SQL.AppendLine("    ((A.Qty / @Mth) * -1) monthly,  ");
                SQL.AppendLine("    (((A.Qty / @Mth)/30 * -1)* ifnull(LeadtimeValue, 0) * @Tho) As MinStock, ");
                SQL.AppendLine("    ((A.Qty / @Mth) * -1) +  (((A.Qty / @mth)/30 * -1)* ifnull(LeadtimeValue, 0) * @Tho) - ifnull(C.Qty, 0) as Reorder ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ItCode, SUM(Qty) As Qty ");
                SQL.AppendLine("        From  TblStockMovement "); 
                SQL.AppendLine("        Where Qty<0 And CancelInd = 'N' And DocDt Between @DocDt1 And @DocDt2 And WhsCode = @WhsCode  ");
                SQL.AppendLine("    Group By ItCode ");
                SQL.AppendLine("    )A ");
                SQL.AppendLine("    Left Join  ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select B.ItCode, ifnull(B.LeadTimevalue, 0) As LeadTimeValue ");
                SQL.AppendLine("        from TblitemleadtimeHdr A ");
                SQL.AppendLine("        Inner Join tblitemleadTimeDtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("        Where A.ActInd = 'Y' ");
                SQL.AppendLine("    )B On A.itCode = B.ItCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select WhsCode,  ItCode, Sum(Qty) As Qty    ");
                SQL.AppendLine("        From TblStockSummary ");
                SQL.AppendLine("        Where (Qty<>0) And WhsCode = @WhsCode ");
                SQL.AppendLine("        Group By WhsCode, ItCode  ");
                SQL.AppendLine("    )C On A.ItCode = C.itCode ");
                SQL.AppendLine("    Inner Join TblItem D On A.ItCode = D.itCode  ");
                SQL.AppendLine("    Inner Join TblItemCategory E On D.ItCtCode  = E.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemSubcategory F On D.ItScCode = F.ItScCode ");
                SQL.AppendLine("    Where 0=0 ");
                if (Sm.GetLue(LueItScCode).Length > 0)
                {
                    SQL.AppendLine("And D.ItScCode=@ItScCode ");
                }
                if (Sm.GetLue(LueItGrpCode).Length > 0)
                {
                    SQL.AppendLine("And D.ItGrpCode=@ItGrpCode ");
                }
                SQL.AppendLine("    Order by A.ItCode  ");
                SQL.AppendLine(")X  ");
            }
            #endregion

            #region rumus internal 2 (internal 1 - outstanding PO)
            else if (Sm.GetLue(LueFormulaCode) == Sm.GetParameter("ReorderFormulaCodeInternal2"))
            {
                SQL.AppendLine("Select X.ItCode, X.ItCodeInternal, X.ItName, X.ItCtName, X.ItScname, X.AvgUsageDtVsRecvDt, (X.Qty*-1) As Qty, X.AvgMRDtVsRecvDt,  X.Uprice,  Ceil(X.MinStock) As MinStock, Ceil(X.MinStock) As MinStock2, ");
                SQL.AppendLine("Ceil(X.Reorder) As Reorder, (Ceil(X.MinStock) * X.UPrice) As MinStockValue, (Ceil(X.Reorder) * X.Uprice) ReorderValue,  X.ItCtCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.ItCode, D.ItCodeInternal, D.ItName, D.ItCtCode, E.ItCtName, F.ItScname, 0 As AvgUsageDtVsRecvDt, A.Qty, ");
                SQL.AppendLine("    0 As AvgMRDtVsRecvDt, 0 Uprice,   ");
                SQL.AppendLine("    ((A.Qty / @Mth) * -1) monthly,  ");
                SQL.AppendLine("    (((A.Qty / @Mth)/30 * -1)* ifnull(LeadtimeValue, 0) * @Tho) As MinStock, ifnull(H.OutstandingPO, 0) As PO, ");
                SQL.AppendLine("    ((A.Qty / @Mth) * -1) +  (((A.Qty / @mth)/30 * -1)* ifnull(LeadtimeValue, 0) * @Tho) - ifnull(C.Qty, 0) - ifnull(H.OutstandingPO, 0) as Reorder ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ItCode, SUM(Qty) As Qty ");
                SQL.AppendLine("        From  TblStockMovement ");
                SQL.AppendLine("        Where Qty<0 And CancelInd = 'N' And DocDt Between @DocDt1 And @DocDt2 And WhsCode = @WhsCode  ");
                SQL.AppendLine("        Group By ItCode ");
                SQL.AppendLine("    )A ");
                SQL.AppendLine("    Left Join  ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select B.ItCode, ifnull(B.LeadTimevalue, 0) As LeadTimeValue ");
                SQL.AppendLine("        from TblitemleadtimeHdr A ");
                SQL.AppendLine("        Inner Join tblitemleadTimeDtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("        Where A.ActInd = 'Y' ");
                SQL.AppendLine("    )B On A.itCode = B.ItCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select WhsCode,  ItCode, Sum(Qty) As Qty    ");
                SQL.AppendLine("        From TblStockSummary ");
                SQL.AppendLine("        Where (Qty<>0) And WhsCode = @WhsCode ");
                SQL.AppendLine("        Group By WhsCode, ItCode  ");
                SQL.AppendLine("    )C On A.ItCode = C.itCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Z.ItCode, SUM(Z.balance) As OutstandingPO ");
                SQL.AppendLine("        from ( ");
                SQL.AppendLine("            Select E.ItCode, E.ItName, "); 
                SQL.AppendLine("            B.Qty-IfNull(G.RecvVdQty, 0)-IfNull(H.POQtyCancelQty, 0) As Balance ");
                SQL.AppendLine("            From TblPOHdr A ");
                SQL.AppendLine("            Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("            Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Left Join (  ");
                SQL.AppendLine("                Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As RecvVdQty  ");
                SQL.AppendLine("                From TblRecvVdDtl T1  ");
                SQL.AppendLine("                Inner Join TblPOHdr T2 On T1.PODocNo=T2.DocNo  ");
                SQL.AppendLine("                Inner Join TblPODtl T3 On T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo  ");
                SQL.AppendLine("                Inner Join TblItem T4 On T1.ItCode=T4.ItCode  ");
                SQL.AppendLine("                Where T1.CancelInd='N'  ");
                SQL.AppendLine("                Group By T1.PODocNo, T1.PODNo  ");
                SQL.AppendLine("            ) G On A.DocNo=G.DocNo And B.DNo=G.DNo  ");
                SQL.AppendLine("            Left Join (  ");
                SQL.AppendLine("                Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As POQtyCancelQty  ");
                SQL.AppendLine("                From TblPOQtyCancel T1  ");
                SQL.AppendLine("                Inner Join TblPOHdr T2 On T1.PODocNo=T2.DocNo  ");
                SQL.AppendLine("                Inner Join TblPODtl T3 On T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo  ");
                SQL.AppendLine("                Inner Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo  ");
                SQL.AppendLine("                Inner Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo=T5.DocNo And T4.MaterialRequestDNo=T5.DNo  ");
                SQL.AppendLine("                Inner Join TblItem T6 On T5.ItCode=T6.ItCode  ");
                SQL.AppendLine("                Where T1.CancelInd='N'  ");
                SQL.AppendLine("                Group By T1.PODocNo, T1.PODNo  ");
                SQL.AppendLine("            ) H On A.DocNo=H.DocNo And B.DNo=H.DNo  ");
                SQL.AppendLine("            Inner Join TblMaterialRequestHdr I On C.MaterialRequestDocNo=I.DocNo  ");
                SQL.AppendLine("            Where A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Order by E.ItCode ");
                SQL.AppendLine("        )Z  ");
                SQL.AppendLine("        Where Z.balance <>0 ");
                SQL.AppendLine("        group by Z.itCode ");
                SQL.AppendLine("    )H On A.ItCode = H.ItCode ");    
                SQL.AppendLine("    Inner Join TblItem D On A.ItCode = D.itCode  ");
                SQL.AppendLine("    Inner Join TblItemCategory E On D.ItCtCode  = E.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemSubcategory F On D.ItScCode = F.ItScCode ");
                SQL.AppendLine("    Where 0=0 ");
                if (Sm.GetLue(LueItScCode).Length > 0)
                {
                    SQL.AppendLine("And D.ItScCode=@ItScCode ");
                }
                if (Sm.GetLue(LueItGrpCode).Length > 0)
                {
                    SQL.AppendLine("And D.ItGrpCode=@ItGrpCode ");
                }
                SQL.AppendLine("    Order by A.ItCode  ");
                SQL.AppendLine(")X  ");
            }
            #endregion

            mSQL = SQL.ToString();
        }


        private void ShowDataItemReorderPoint()
        {
            try
            {
                if (Sm.IsLueEmpty(LueOptCode, "Month Option") || Sm.IsLueEmpty(LueFormulaCode, "Formula") || Sm.IsTxtEmpty(TxtKoefisien, "Coefficient", true)) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0=0";

                string month = Sm.GetLue(LueOptCode);
                decimal MthDiv = 0;

                string tglsekarang = Sm.GetDte(DteDocDt);
                DateTime DtDocDt2 = Sm.ConvertDate(tglsekarang).AddMonths(-1);

                int totaldayofmonth = DateTime.DaysInMonth(Convert.ToInt32(Sm.FormatDate(DtDocDt2).Substring(0, 4)), Convert.ToInt32(Sm.FormatDate(DtDocDt2).Substring(4, 2)));

                DateTime DtDocDt1 = DtDocDt2;

                if (month == "1")
                {
                    DtDocDt1 = DtDocDt2.AddMonths(-2);
                    MthDiv = 3;
                }
                else if (month == "2")
                {
                    DtDocDt1 = DtDocDt2.AddMonths(-5);
                    MthDiv = 6;
                }
                else if (month == "3")
                {
                    DtDocDt1 = DtDocDt2.AddMonths(-8);
                    MthDiv = 9;
                }
                else
                {
                    DtDocDt1 = DtDocDt2.AddMonths(-11);
                    MthDiv = 12;
                }

                string monthly1 = Sm.FormatDate(DtDocDt1);
                string monthly2 = Sm.FormatDate(DtDocDt2);

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", string.Concat(Sm.FormatDate(DtDocDt1).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DtDocDt2).Substring(0, 6), totaldayofmonth));
                Sm.CmParam<String>(ref cm, "@MonthFilter1", monthly1.Substring(0, 6));
                Sm.CmParam<String>(ref cm, "@MonthFilter2", monthly2.Substring(0, 6));
                Sm.CmParam<Decimal>(ref cm, "@Tho", Decimal.Parse(TxtKoefisien.Text));
                Sm.CmParam<Decimal>(ref cm, "@Mth", MthDiv);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueItScCode));
                Sm.CmParam<String>(ref cm, "@ItGrpCode", Sm.GetLue(LueItGrpCode));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X.ItCode ",
                        new string[] 
                        { 
                            "ItCode", 
                            
                            "ItCodeInternal", "ItName", "ItCtName", "ItScName", "AvgUsageDtVsRecvDt", 
                            
                            "Qty", "AvgMRDtVsRecvDt", "Uprice", "MinStock", "MinStock2",   
                            
                            "Reorder", "MinStockValue", "ReorderValue" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);

                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);

                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                        }, true, false, true, false
                    );
                ListDataPrice();
                ComputeShowTotal();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                //Sm.FocusGrd(Grd1, Col, 0);
                //Cursor.Current = Cursors.WaitCursor;
            }
        }


        private void ListDataPrice()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, B.UPrice ");
	        SQL.AppendLine("From TblQtHdr A ");
	        SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("Inner Join ( ");
		    SQL.AppendLine("    Select T2.ItCode, ");
		    SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo, T2.DNo)) As Key1 ");
		    SQL.AppendLine("    From TblQtHdr T1, TblQtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.CancelInd = 'N' And T1.Status = 'A'  And T2.ActInd = 'N' ");
		    SQL.AppendLine("    Group By T2.ItCode ");
	        SQL.AppendLine(")C On Concat(A.DocDt, B.DocNo, B.DNo)=C.Key1 And B.ItCode=C.ItCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                       "ItCode",
                        //1
                       "UPrice"
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 1) == Sm.DrStr(dr, 0))
                            {
                                Grd1.Cells[Row, 9].Value = Sm.DrDec(dr, 1);
                                Grd1.Cells[Row, 13].Value = Sm.DrDec(dr, 1) *  Sm.GetGrdDec(Grd1, Row, 11);
                                Grd1.Cells[Row, 14].Value = Sm.DrDec(dr, 1) * Sm.GetGrdDec(Grd1, Row, 12);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

     

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
        }
        private decimal ComputeShowTotal()
        {
            decimal TotalMinStockValue = 0m; decimal TotalROPValue = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0 && Sm.GetGrdStr(Grd1, Row, 14).Length != 0)
                {
                    TotalMinStockValue += Sm.GetGrdDec(Grd1, Row, 13);
                    TotalROPValue += Sm.GetGrdDec(Grd1, Row, 14);
                }
            TxtMinStockValue.EditValue = Sm.FormatNum(Math.Round(TotalMinStockValue, 0), 0);
            TxtROPValue.EditValue = Sm.FormatNum(Math.Round(TotalROPValue, 0), 0);

            return TotalMinStockValue; return TotalROPValue;
        }

        #region setlue

        private void SetLueOptCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("    Select '1' As Col1, '3' As Col2 Union All");
                SQL.AppendLine("    Select '2' As Col1, '6' As Col2 Union All");
                SQL.AppendLine("    Select '3' As Col1, '9' As Col2 Union All");
                SQL.AppendLine("    Select '4' As Col1, '12' As Col2 ");
                SQL.AppendLine(") T Order By Col1");

                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueItGrpCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("    Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ActInd = 'Y'  ");
            SQL.AppendLine(") Tbl Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        private void SetLueFormulaCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From tblOption Where OptCat = 'ReorderPointFormula' ");
               
                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Formula", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueItScCode(ref LookUpEdit Lue, string ItCtCode)
        {
            var SQL = new StringBuilder();

            if (TxtItCode.Text.Length != 0)
            {
                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "'  ");
                SQL.AppendLine("Union All");
                SQL.AppendLine("Select B.ItScCode As Col1, A.ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItScCode = B.ItScCode ");
                SQL.AppendLine("Where B.ItCode='" + TxtItCode.Text + "'  ");
                SQL.AppendLine(")Tbl Order By Col2 ");
            }
            else
            {
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "' Order By ItScName ");
            }

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
      
        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
            var ItCtCode = Sm.GetLue(LueItCtCode);
            LueItScCode.EditValue = null;
            if (ItCtCode.Length != 0) SetLueItScCode(ref LueItScCode, ItCtCode);
            Sm.SetControlReadOnly(LueItScCode, ItCtCode.Length == 0);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

         private void LueOptCode_EditValueChanged(object sender, EventArgs e)
        {
             Sm.RefreshLookUpEdit(LueOptCode, new Sm.RefreshLue1(SetLueOptCode));
        }
        
        private void TxtKoefisien_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtKoefisien, 0);
        }
        private void LueFormulaCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData2();
            Sm.RefreshLookUpEdit(LueFormulaCode, new Sm.RefreshLue1(SetLueFormulaCode));
            if (Sm.GetLue(LueFormulaCode) == paramFormulaX)
            {
                label9.ForeColor = Color.Black;
            }
            else
            {
                label9.ForeColor = Color.Red;
            }
        }

        private void ChkItScCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's subcategory");
        }

        private void LueItScCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItScCode, new Sm.RefreshLue2(SetLueItScCode), Sm.GetLue(LueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue1(SetLueItGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's group");
        }

        #endregion
 
    }
}
