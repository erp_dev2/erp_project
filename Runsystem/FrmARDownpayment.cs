﻿#region Update
/*
    18/05/2017 [TKG] tambah info downpayment dari dokumen ap downpayment yg lain
    12/07/2017 [TKG] bug fixing apabila sales order memiliki dokumen uang muka lbh dari 1 ketika data ditampilkan akan keluar warning.
    13/04/2018 [TKG] filter by site
    07/08/2018 [HAR] Bisa ambil data dari SO contract untuk VIR
    14/08/2018 [WED] BUG typo nama parameter GenerateCustomerCOAFormat
    27/08/2018 [TKG] tambah Customer's PO#
    19/11/2019 [WED/IMS] amount SO Contract inventory belum ketarik
    06/01/2019 [VIN/SIER] Bug Combobox customer saat parameter IsARDownpaymentSONotMandatory bernilai N masih readonly
    06/01/2019 [HAR/KBN] AR downpayment saat save error description
    18/03/2020 [TKG/KBN] tambah entity code (belum ditambah coding untuk save/show, kbn belum bisa diakses.
    19/03/2020 [VIN/KBN] tambah entcode dan docnoformat
    22/09/2020 [WED/YK] parameter IsARDownpaymentSONotMandatory kebalik fungsi nya di SetLueCtCode dan BtnSODocNo nya
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    17/03/2021 [RDH/KSM] tambah Customer's Category saat memilih Customer (Customer's Category tertulis di samping nama Master Customer)
    21/04/2021 [IBL/KSM] tambah param IsSalesContractEnabled untuk menarik SC ketika milih SO
    18/05/2021 [TRI/KSM] tambah parameter (mIsARDownpaymentUseDepartment) 
    19/07/2021 [DITA/PHT] tambah fieldtype untuk AcNo based on param : IsItemCategoryUseCOAAPAR
    24/08/2021 [BRI/AMKA] tambah validasi debit & credit COA balance param IsCheckJournalNotBalance && membuat 1 journal param IsARDPProcessTo1Journal
    03/09/2021 [RDA/KSM] tambah tab tax pada menu AR DP
    28/10/2021 [ARI/AMKA] tambah filter field Department dan Customer berdasarkan group
    25/11/2021 [RIS/AMKA] Merubah param IsFilterBydept menjadi : internal bool
    15/12/2021 [IBL/AMKA] Tambah CheckBox AddCost dan akan berpengaruh ketika checkbox tercentang
                          VR ARDP dan VR ARDP Additional Amt jadi satu berdasarkan parameter IsARDPProcessTo1Journal
    20/12/2021 [IBL/AMKA] BUG: warning ketika COAAmt kosong
    23/12/2021 [IBL/AMKA] Feedback: Amount AddCost tidak melihat natura COA. Jika dicentang di debit maka menjadi pengurang, jika dicentang di credit maka menjadi penambah.
    26/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses save
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    16/08/2022 [MAU/PRODUCT] Membuat validasi COA berdasarkan apa yang telah terpasang di Group (system tools).
    18/10/2022 [TYO/SKI] menambah printout untuk SKI
    19/10/2022 [TYO/SKI] menambah generate ReceiptNo ketika save
    19/12/2022 [RDA/MNET] tambah tab upload file berdasarkan param IsARDPAllowToUploadFile
    22/12/2022 [RDA/MNET] benerin validasi pas cancel data
    02/02/2023 [TYO/MNET] set upload file mandatory berdasarkan parameter IsARDPUploadFileMandatory
    10/02/2023 [IBL/MNET] VR tidak auto approve sebelum dokumen customer AR DP di approve
    13/02/2023 [IBL/MNET] VR tidak membentuk doc approval, status akan auto ngkut statusnya ARDP.
    07/03/2023 [SET/MNET] Penyesuaian printout
    08/03/2023 [BRI/KBN] bug save data
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpayment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mDocTitle = string.Empty;
        internal FrmARDownpaymentFind FrmFind;
        private bool mIsARDownpaymentSONotMandatory = false;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsARDPUseCOA = false,
            mIsFilterByCtCt = false,
            mIsARDPAllowToUploadFile = false,
            mIsARDPUploadFileMandatory = false;

        private string mVoucherCodeFormatType = "1";
        internal string mGenerateCustomerCOAFormat = string.Empty;
        internal bool mIsSalesContractEnabled = false,
        mIsARDownpaymentUseDepartment = false,
        mIsItemCategoryUseCOAAPAR = false,
        mIsCheckJournalNotBalance = false,
        mIsARDPProcessTo1Journal = false,
        mIsARDPTaxEnabled = false,
        mIsFilterByDept = false,
        mIsCOAFilteredByGroup = false,
        mIsReceiptARDPGenerateDocNo = false;

        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        private string mStateIndicator = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmARDownpayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer's AR Downpayment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                TcARDownpayment.SelectedTabPage = TpTax;
                Sl.SetLueTaxCode(ref LueTaxCode);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);

                TcARDownpayment.SelectedTabPage = TpAdditional;
                Sl.SetLueOption(ref LueAcNoType, "AcNoTypeForARDP");
                TpAdditional.PageVisible = mIsItemCategoryUseCOAAPAR;

                TcARDownpayment.SelectedTabPage = TpCOA;
                TpCOA.PageVisible = mIsARDPUseCOA;

                TcARDownpayment.SelectedTabPage = TpGeneral;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);

                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Sl.SetLueCtCode(ref LueCtCode, Gv.CurrentUserCode, mIsFilterByCtCt ? "Y" : "N");

                if (!mIsARDPTaxEnabled)
                {
                    TcARDownpayment.TabPages.Remove(TpTax);
                }

                if (!mIsARDownpaymentUseDepartment)
                {
                    lblDepartment.Visible = LueDeptCode.Visible = false;
                }

                if (!mIsARDPAllowToUploadFile)
                {
                    TcARDownpayment.TabPages.Remove(TpUploadFile);
                }

                SetFormControl(mState.View);
                
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid Approval Info
            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]{ "Checked By", "Status", "Date", "Remark" },
                    new int[]{ 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
            #endregion

            #region Grid coa

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Account#",
                    "Description",
                    "Debit",
                    "Credit",
                    "Remark",

                    //6-8
                    "", //Checkbox debit ind
                    "", //Checkbox credit ind
                    "" //CheckBox Db Cr Indicator
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    150, 300, 100, 100, 400,

                    //6-8
                    20, 20, 0
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 6, 7 , 8 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2 });
            if(!mIsARDPProcessTo1Journal)
                Sm.GrdColInvisible(Grd3, new int[] { 6, 7, 8 }, false);

            Grd3.Cols[6].Move(3);
            Grd3.Cols[7].Move(5);

            #endregion

            #region Grid Upload File

            if (mIsARDPAllowToUploadFile)
            {
                Grd4.Cols.Count = 7;
                Grd4.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd4,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100, 
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd4, new int[] { 5 });
                Sm.GrdFormatTime(Grd4, new int[] { 6 });
                Sm.GrdColButton(Grd4, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd4.Cols[2].Move(1);
            }

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueCurCode, TxtAmt, LuePIC, 
                        MeeRemark, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                        LueEntCode, MeeCancelReason, LueDeptCode,LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, 
                        LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, 
                        DteTaxInvoiceDt3, TxtAmtBefTax

                    }, true);
                    BtnSODocNo.Enabled = false;
                    Sm.SetControlReadOnly(ChkCancelInd, true);
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    if (mIsARDPAllowToUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode,  LuePIC, LuePaymentType, TxtAmt,
                        LueEntCode, MeeRemark, LueDeptCode, LueAcNoType, LueDeptCode,
                        LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, TxtTaxInvoiceNo2, 
                        DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtAmtBefTax

                    }, false);
                    if (mIsARDownpaymentSONotMandatory)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueCtCode }, false);
                    if (mIsARDPTaxEnabled)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAmt }, true);
                    BtnSODocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7 });
                    if (mIsARDPAllowToUploadFile) Sm.GrdColReadOnly(false, true, Grd4, new int[] { 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    Grd3.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateIndicator = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtSODocNo, TxtNoOfDownpayment, 
                LueCtCode, TxtCtPONo, TxtSOCurCode, LueCurCode, LuePIC,  
                LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, MeeCancelReason,
                TxtVoucherRequestDocNo, TxtVoucherDocNo, LueEntCode, MeeRemark, LueDeptCode, LueAcNoType, LueDeptCode,
                LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, TxtTaxInvoiceNo2, 
                DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { 
                TxtSOAmt, TxtOtherAmt, TxtAmt, TxtAmtBefTax, TxtTaxAmt, 
                TxtTaxAmt2, TxtTaxAmt3, TxtAmtAftTax, TxtCOAAmt
            }, 0);
            ChkCancelInd.Checked = false;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            if(mIsARDPAllowToUploadFile) Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmARDownpaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateIndicator = "I";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtCOAAmt }, 0);

                if (mIsARDownpaymentSONotMandatory)
                    Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                else
                    BtnSODocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = "E";

            if (TxtStatus.Text != "Approved")
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                    }
                    else
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                   Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                }
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mEntCode = Sm.GetLue(LueEntCode);
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SODocNo, ");
                SQL.AppendLine("A.CurCode, A.Amt, A.PIC, A.VoucherRequestDocNo, A.CancelReason, A.EntCode, A.Remark, A.DeptCode, ");
                SQL.AppendLine("B.CurCode As SOCurCode, A.AcNoType, ");
                if (mGenerateCustomerCOAFormat == "1")
                    SQL.AppendLine("B.Amt As SOAmt, ");
                else
                    SQL.AppendLine("(B.Amt+B.AmtBOM) As SOAmt, ");
                SQL.AppendLine("IfNull(B.CtCode, A.CtCode) As CtCode, ");
                SQL.AppendLine("C.VoucherDocNo,  ");
                SQL.AppendLine("Case IfNull(A.Status, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'A' Then 'Approved' ");
                SQL.AppendLine("    When 'C' Then 'Cancel' End ");
                SQL.AppendLine("As StatusDesc,  A.paymentType,A.bankCode, A.GiroNo, A.DueDt,  ");
                SQL.AppendLine("IfNull((Select Count(DocNo) From TblARDownpayment Where SODocNo=A.SODocNo And CancelInd='N'), 0) As NoOfDownpayment, A.VoucherRequestDocNo2, D.VoucherDocNo As VoucherDocNo2, ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(Amt) ");
                SQL.AppendLine("    From TblARDownpayment ");
                SQL.AppendLine("    Where SODocNo=A.SODocNo ");
                SQL.AppendLine("    And DocNo<>@DocNo ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("), 0) As OtherAmt, ");
                if (mGenerateCustomerCOAFormat == "1")
                    SQL.AppendLine("B.CtPONo, ");
                else
                    SQL.AppendLine("Null As CtPONo, ");
                SQL.AppendLine("A.AmtBefTax, ");
                SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, ");
                SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxCode2, A.TaxAmt2, ");
                SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCode3, A.TaxAmt3 ");
                SQL.AppendLine("From TblARDownpayment A ");
                if (mGenerateCustomerCOAFormat == "1")
                    SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
                else 
                    SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo2=D.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "SODocNo", "NoOfDownpayment",
                            
                            //6-10
                            "CtCode", "SOCurcode", "SOAmt", "CurCode", "Amt",  
                            
                            //11-15
                            "PIC", "VoucherRequestDocNo", "VoucherDocNo",  "paymentType", "bankCode", 
                            
                            //16-20
                            "GiroNo", "DueDt", "CancelReason", "VoucherRequestDocNo2", "VoucherDocNo2", 
                            
                            //21-25
                            "Remark", "OtherAmt", "CtPONo", "EntCode", "DeptCode",

                            //26-30
                            "AcNoType", "AmtBefTax", "TaxInvoiceNo", "TaxInvoiceDt", "TaxCode", 
                            
                            //31-35
                            "TaxAmt", "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxCode2", "TaxAmt2", 
                            
                            //36-39
                            "TaxInvoiceNo3", "TaxInvoiceDt3", "TaxCode3", "TaxAmt3",

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[18]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                            TxtSODocNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 2);
                            Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[6]));
                            TxtSOCurCode.EditValue = Sm.DrStr(dr, c[7]);
                            TxtSOAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                            TxtAmtAftTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                            Sm.SetLue(LuePIC, Sm.DrStr(dr, c[11]));
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[15]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[16]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[17]));
                            TxtVoucherRequestDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[12]) : Sm.DrStr(dr, c[19]));
                            TxtVoucherDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[13]) : Sm.DrStr(dr, c[20]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[21]);
                            TxtOtherAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[22]), 0);
                            TxtCtPONo.EditValue = Sm.DrStr(dr, c[23]);
                            Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[24]));
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[25]));
                            Sm.SetLue(LueAcNoType, Sm.DrStr(dr, c[26]));
                            TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[27]), 0);
                            TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[28]);
                            Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[29]));
                            Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[30]));
                            TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                            TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[32]);
                            Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[33]));
                            Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[34]));
                            TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                            TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[36]);
                            Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[37]));
                            Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[38]));
                            TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);

                        }, true
                    );
                    ShowDocApproval(DocNo);
                    ShowARDownPaymentDtl(DocNo);
                    if(mIsARDPAllowToUploadFile) ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowARDownPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.ActInd ");
            SQL.AppendLine("From TblARDownPaymentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    if (Sm.DrStr(dr, c[5]) == "Y")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 3) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 5);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 1);
            ComputeCOAAmt();
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ARDownpayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  TblARDownpaymentfile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd4, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd4, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            if (mDocNoFormat == "1")
                 DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ARDownpayment", "TblARDownpayment");
            else
                 DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "ARDownpayment", "TblARDownpayment", mEntCode, "1");

            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                if (mDocNoFormat =="1")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
            else
                if (mDocNoFormat == "1")
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo();
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");

            string ReceiptNo = GenerateReceipt(Sm.GetDte(DteDocDt));
            var cml = new List<MySqlCommand>();

            cml.Add(SaveARDownPayment(DocNo, VoucherRequestDocNo, ReceiptNo));

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveARDownPaymentDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveARDownPaymentDtl2(DocNo, Row));
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsARDPAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            if (!mIsARDPProcessTo1Journal) InsertDataVR(DocNo);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsARDPAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        //if (Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
                        //{
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        //}
                    }
                }
            }

            ShowData(DocNo);
        }

        private void InsertDataVR(string ARDocNo)
        {
            var cml = new List<MySqlCommand>();
            if (mIsARDPUseCOA && Grd3.Rows.Count>1)
            {
                var VoucherRequestDocNo2 = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    if(mDocNoFormat == "1")
                        VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                    else
                        VoucherRequestDocNo2 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
                else
                    VoucherRequestDocNo2 = GenerateVoucherRequestDocNo();

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo2, ARDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo2));
            }
            Sm.ExecCommands(cml);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "CurCode") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdValueNotValid() ||
                IsAmtNotValid() ||
                (mIsARDPAllowToUploadFile && IsUploadFileEmpty()) ||
                (mIsCheckJournalNotBalance && IsCOAAmtNotValid()) ||
                (mIsARDownpaymentUseDepartment && Sm.IsLueEmpty(LueDeptCode, "Department"))||
                (mIsItemCategoryUseCOAAPAR && Sm.IsLueEmpty(LueAcNoType, "Account Number Type")) ||
                (mIsClosingJournalBasedOnMultiProfitCenter && Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()))
                ;
        }

        private bool IsUploadFileEmpty()
        {
            if (mIsARDPUploadFileMandatory && Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                return true;
            }
            return false;
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueDeptCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter " +
                    "Where ProfitCenterCode Is Not Null And ActInd='Y' And DeptCode=@Param Limit 1;",
                    Value);
        }

        private bool IsAmtNotValid()
        {
            decimal SOAmt = decimal.Parse(TxtSOAmt.Text);
            decimal OtherAmt = decimal.Parse(TxtOtherAmt.Text);
            decimal Amt = decimal.Parse(TxtAmt.Text);

            if ((OtherAmt + Amt) > SOAmt)
            {
                if (Sm.StdMsgYN("Question",
                    "SO : " + Sm.FormatNum(SOAmt, 0) + Environment.NewLine +
                    "Other Downpayment : " + Sm.FormatNum(OtherAmt, 0) + Environment.NewLine +
                    "Downpayment : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                    "AR downpayment amount is bigger than SO's amount." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsCOAAmtNotValid()
        {
            decimal Debit = 0m;
            decimal Credit = 0m;
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    Debit += Sm.GetGrdDec(Grd3, Row, 3);
                    Credit += Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Debit != Credit)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                        "Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine +
                        "Debit and credit is not balance.");
                        return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Grd3.Rows.Count > 1)
                {
                    for (int RowX = 0; RowX < Grd3.Rows.Count - 1; RowX++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd3, RowX, 1, false, "COA's account is empty.")) return true;
                        if (Sm.GetGrdDec(Grd3, RowX, 3) == 0m && Sm.GetGrdDec(Grd3, RowX, 4) == 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Account# : " + Sm.GetGrdStr(Grd3, RowX, 1) + Environment.NewLine +
                                "Description : " + Sm.GetGrdStr(Grd3, RowX, 2) + Environment.NewLine + Environment.NewLine +
                                "Both debit and credit amount can't be 0.");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveARDownPayment(string DocNo, string VoucherRequestDocNo, string ReceiptNo)
        {
            var SQL = new StringBuilder();

            #region save AR

            SQL.AppendLine("Insert Into TblARDownpayment ");
            SQL.AppendLine("(DocNo, ReceiptNo, DocDt, CancelInd, Status, SODocNo, CtCode, PaymentType, BankCode, GiroNo, DueDt, VoucherRequestDocNo, CurCode, Amt, PIC, EntCode, Remark, DeptCode, AcNoType, CreateBy, CreateDt, ");
            SQL.AppendLine("AmtBefTax, TaxInvoiceNo, TaxInvoiceDt, TaxCode, TaxAmt, ");
            SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxAmt2, ");
            SQL.AppendLine("TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, TaxAmt3 ) ");
            SQL.AppendLine("Values(@DocNo,  ");
            if (mIsReceiptARDPGenerateDocNo)
                SQL.AppendLine("@ReceiptNo, ");
            else
                SQL.AppendLine("Null, ");
            SQL.AppendLine("@DocDt, 'N', 'O', @SODocNo, @CtCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @VoucherRequestDocNo, @CurCode, @Amt, @PIC, @EntCode, @Remark, @DeptCode, @AcNoType, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime(), ");
            SQL.AppendLine("@AmtBefTax, @TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @TaxAmt, ");
            SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxAmt2, ");
            SQL.AppendLine("@TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, @TaxAmt3 );");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ARDownpayment' ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblARDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblARDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ARDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");
            
            #endregion

            #region save VR
            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            if (mIsARDownpaymentUseDepartment)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), ");
            SQL.AppendLine("'05', Null, 'D', Null, @PaymentType, @GiroNo, @BankCode, @DueDt, @PIC, 0, @CurCode, "+(mIsARDPProcessTo1Journal ? "@Amt2" : "@Amt") +", Null, ");
            if (mIsARDownpaymentSONotMandatory)
                SQL.AppendLine("Concat(@CtName, '. ', @Remark), ");
            else
                SQL.AppendLine("Concat(@SODocNo, ' (', @CtName, '). ', @Remark), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', ");
            if (mIsARDownpaymentSONotMandatory)
            {
                SQL.AppendLine("Concat(ifnull(@CtName, '-'), '. ', ifnull(@Remark, '-')), ");
            }
            else
            {
                SQL.AppendLine("Concat(Case When @SODocNo Is Null Then '-' Else @SODocNo End, ");
                SQL.AppendLine("Case When @CtName Is Null Then '' Else Concat(' (', @CtName, '). ') End, ");
                SQL.AppendLine("Case When @Remark is Null Then '' Else Concat(' ', @Remark) End ");
                SQL.AppendLine("), ");
            }
            SQL.AppendLine("@Amt, Null, @CreateBy, CurrentDateTime()); ");

            if (mIsARDPProcessTo1Journal && Decimal.Parse(TxtCOAAmt.Text) != 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@VoucherRequestDocNo, '002', 'Customer Account Receivable Downpayment', @Amt3, Null, @CreateBy, CurrentDateTime()); ");
            }

            #region Old By IBL 13/02/2023
            //SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            //SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType='VoucherRequest' ");
            //SQL.AppendLine("    And DocNo=@VoucherRequestDocNo ");
            //SQL.AppendLine("    ); ");
            #endregion

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @VoucherRequestDocNo And Exists( ");
            SQL.AppendLine("    Select 1 From TblARDownpayment ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtSODocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtName", Sm.GetLue(LueCtCode).Length > 0 ? LueCtCode.GetColumnValue("Col2").ToString() : string.Empty);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text) + Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AcNoType", Sm.GetLue(LueAcNoType));
            Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            


            return cm;
        }

        private MySqlCommand SaveARDownPaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AR Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @ActInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@ActInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveARDownPaymentDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @ActInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd3, Row, 8) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //save vr additional COA
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string ARDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', ");
            if (mIsARDownpaymentUseDepartment)
            {
                SQL.AppendLine("@DeptCode, ");
            }
            else
            {
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), ");
            }
            SQL.AppendLine("'20', Null, ");
            SQL.AppendLine("'D', @PaymentType, null, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='IncomingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblARDownPayment Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@ARDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@ARDocNo", ARDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("IncomingPaymentDeptCode"));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (ChkCancelInd.Checked == true)
            {
                cml.Add(EditARDownpayment());
                Sm.ExecCommands(cml);
            }

            //EDIT UPLOAD

            if (mIsARDPAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (mIsARDPAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(TxtDocNo.Text));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (mIsARDPAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        }
                    }
                }
            }
            //EDIT UPLOAD

            ShowData(TxtDocNo.Text);
            if (mIsARDPAllowToUploadFile) ShowUploadFile(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||

                ((mIsARDPAllowToUploadFile && TxtStatus.Text == "Approved") && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") || !mIsARDPAllowToUploadFile && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation")) ||
                ((mIsARDPAllowToUploadFile && TxtStatus.Text == "Approved") && IsCancelIndNotTrue() || !mIsARDPAllowToUploadFile && IsCancelIndNotTrue()) ||

                (mIsClosingJournalBasedOnMultiProfitCenter && Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()))
                ;
        }



        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblARDownpayment " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditARDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARDownpayment Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where (DocNo=@VoucherRequestDocNo Or DocNo=@VoucherRequestDocNo2)  And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo2", TxtVoucherRequestDocNo2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        //UPLOAD FILE - START
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Customer's AR Downpayment - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblARDownpaymentFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsARDPAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsARDPAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsARDPAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsARDPAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsARDPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsARDPAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblARDownpaymentFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARDownpaymentFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteEmployeeFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblARDownpaymentFile Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtDocNo.Text);

            return cm;
        }


        //UPLOAD FILE - END


        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'VoucherCodeFormatType', 'GenerateCustomerCOAFormat', 'DocNoFormat', 'IsARDownpaymentSONotMandatory', 'IsARDownpaymentUseDepartment', ");
            SQL.AppendLine("'IsARDPUseCOA', 'IsSalesContractEnabled', 'IsARDPTaxEnabled', 'IsFilterByDept', 'IsFilterByCtCt', ");
            SQL.AppendLine("'IsItemCategoryUseCOAAPAR', 'IsCheckJournalNotBalance', 'IsARDPProcessTo1Journal', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsCOAFilteredByGroup', 'IsReceiptARDPGenerateDocNo', ");
            SQL.AppendLine("'IsARDPAllowToUploadFile', 'FileSizeMaxUploadFTPClient','PortForFTPClient','PasswordForFTPClient','UsernameForFTPClient','SharedFolderForFTPClient','HostAddrForFTPClient', 'IsARDPUploadFileMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsARDPUseCOA": mIsARDPUseCOA = ParValue == "Y"; break;
                            case "IsSalesContractEnabled": mIsSalesContractEnabled = ParValue == "Y"; break;
                            case "IsARDPTaxEnabled": mIsARDPTaxEnabled = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsARDPProcessTo1Journal": mIsARDPProcessTo1Journal = ParValue == "Y"; break;
                            case "IsCheckJournalNotBalance": mIsCheckJournalNotBalance = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsARDownpaymentUseDepartment": mIsARDownpaymentUseDepartment = ParValue == "Y"; break;
                            case "IsARDownpaymentSONotMandatory": mIsARDownpaymentSONotMandatory = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            case "IsReceiptARDPGenerateDocNo ": mIsReceiptARDPGenerateDocNo = ParValue == "Y"; break;
                            case "IsARDPAllowToUploadFile": mIsARDPAllowToUploadFile = ParValue == "Y"; break;
                            case "IsARDPUploadFileMandatory": mIsARDPUploadFileMandatory = ParValue == "Y"; break;

                            //string
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "GenerateCustomerCOAFormat": mGenerateCustomerCOAFormat = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;

                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            if (Sm.GetParameter("DocTitle") == "SKI")
            {
                var l = new List<ARDPSKI>();

                string[] TableName = { "ARDPHdr" };
                List<IList> myLists = new List<IList>();

                #region Header
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL.AppendLine("A.DocNo, Date_format(A.DocDt, '%d %M %Y') ARDocDt, B.DocNo SODocNo, Date_format(B.DocDt, '%d %M %Y') SODocDt, B.Remark SORemark,  ");
                SQL.AppendLine("C.CtName CustomerName, C.Address CustomerAddress, C.Phone CustomerPhone, C.Fax CustomerFax,  ");
                SQL.AppendLine("A.AmtBefTax DownpaymentAmount, A.Remark ARDPRemark, D.BankAcNo, D.BranchAcNm, E.BankName, ");
                SQL.AppendLine("IFNULL(F1.TaxName, '') TaxName1, IFNULL(F2.TaxName, '') TaxName2, IFNULL(F3.TaxName, '') TaxName3, ");
                SQL.AppendLine("IFNULL(A.TaxAmt, '') TaxAmt1, IFNULL(A.TaxAmt2, '') TaxAmt2, IFNULL(A.TaxAmt3, '') TaxAmt3,  ");
                SQL.AppendLine("A.AmtBefTax+(IFNULL(A.TaxAmt, 0)+IFNULL(A.TaxAmt2, 0)+IFNULL(A.TaxAmt3, 0)) TerbilangInvoice, A.ReceiptNo, A.Amt, Date_format(CURDATE(), '%d %M %Y') Today ");
                SQL.AppendLine("FROM tblardownpayment A ");
                SQL.AppendLine("LEFT JOIN tblsohdr B ON A.SODocNo = B.DocNo ");
                SQL.AppendLine("LEFT JOIN tblcustomer C ON A.CtCode = C.CtCode ");
                SQL.AppendLine("LEFT JOIN tblbankaccount D ON A.BankAcCode = D.BankAcCode");
                SQL.AppendLine("LEFT JOIN tblbank E ON D.BankCode = E.BankCode");
                SQL.AppendLine("LEFT JOIN tbltax F1 ON A.TaxCode = F1.TaxCode ");
                SQL.AppendLine("LEFT JOIN tbltax F2 ON A.TaxCode2 = F2.TaxCode ");
                SQL.AppendLine("LEFT JOIN tbltax F3 ON A.TaxCode3 = F3.TaxCode ");

                SQL.AppendLine("Where A.DocNo = @DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompLocation2",
                         "DocNo",
                         "ARDocDt",

                         //6-10
                         "SODocNo",
                         "SODocDt",
                         "SORemark",
                         "CustomerName",
                         "CustomerAddress",

                         //11-15
                         "CustomerPhone",
                         "CustomerFax",
                         "DownpaymentAmount",
                         "ARDPRemark",
                         "BankAcNo",

                         //16-20
                         "BranchAcNm",
                         "BankName",
                         "TaxName1",
                         "TaxName2",
                         "TaxName3",

                         //21-25
                         "TaxAmt1",
                         "TaxAmt2",
                         "TaxAmt3",
                         "TerbilangInvoice",
                         "ReceiptNo",

                         //26-27
                         "Amt",
                         "Today",

                        });
                    if (dr.HasRows)
                    {
                        int nomor = 0;
                        while (dr.Read())
                        {
                            nomor = nomor + 1;
                            l.Add(new ARDPSKI()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddress2 = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                ARDocDt = Sm.DrStr(dr, c[5]),

                                SODocNo = Sm.DrStr(dr, c[6]),
                                SODocDt = Sm.DrStr(dr, c[7]),
                                SORemark = Sm.DrStr(dr, c[8]),
                                CustomerName = Sm.DrStr(dr, c[9]),
                                CustomerAddress = Sm.DrStr(dr, c[10]),

                                CustomerPhone = Sm.DrStr(dr, c[11]),
                                CustomerFax = Sm.DrStr(dr, c[12]),
                                DownpaymentAmount = Sm.DrDec(dr, c[13]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[13])),
                                ARDPRemark = Sm.DrStr(dr, c[14]),
                                BankAcNo = Sm.DrStr(dr, c[15]),

                                BranchAcNm = Sm.DrStr(dr, c[16]),
                                BankName = Sm.DrStr(dr, c[17]),
                                TaxName1 = Sm.DrStr(dr,c[18]),
                                TaxName2 = Sm.DrStr(dr, c[19]),
                                TaxName3 = Sm.DrStr(dr, c[20]),

                                TaxAmt1 = Sm.DrDec(dr, c[21]),
                                TaxAmt2 = Sm.DrDec(dr, c[22]),
                                TaxAmt3 = Sm.DrDec(dr, c[23]),
                                TerbilangInvoice = Sm.Terbilang(Sm.DrDec(dr, c[24])),
                                ReceiptNo = Sm.DrStr(dr, c[25]),
                                Nomor = nomor,

                                Amt = Sm.DrDec(dr, c[26]),
                                TodayDate = Sm.DrStr(dr, c[27]),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                Sm.PrintReport("ARDownpaymentSKI", myLists, TableName, false);
                Sm.PrintReport("ARDownpaymentReceiptSKI", myLists, TableName, false);
            }
            else
            {
                var ld = new List<ARDep>();

                string[] TableName = { "ARDep" };
                List<IList> myLists = new List<IList>();

                var cm = new MySqlCommand();

                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleARDP1') As 'SignUser', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleARDP2') As 'SignUserPos', ");
                SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt,  B.CurCode, A.Amt, Ifnull(A.TaxAmt, 0.00)+Ifnull(A.TaxAmt2, 0.00)+Ifnull(A.TaxAmt3, 0.00) TaxAmt, ");
                if (mGenerateCustomerCOAFormat == "1")
                    SQL.AppendLine("Ifnull(B.Amt, 0.00) As SoAmt, ");
                else
                    SQL.AppendLine("Ifnull((B.Amt + B.AmtBOM), 0.00) As SoAmt, ");
                SQL.AppendLine("C.CtName, A.SODocNo, E.DocNo As DocNoVC, DATE_FORMAT(E.DocDt,'%M %d, %Y') As DocDtVC, C.Address, A.Remark ");
                SQL.AppendLine("From TblARDownpayment A   ");
                if (mGenerateCustomerCOAFormat == "1")
                {
                    if(mDocTitle == "MNET")
                        SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
                    else
                        SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
                }
                else
                {
                    if(mDocTitle == "MNET")
                        SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                    else
                        SQL.AppendLine("Inner Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                }
                if(mDocTitle == "MNET")
                    SQL.AppendLine("Left Join TblCustomer C On B.CtCode=C.CtCode ");
                else
                    SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo=D.DocNo And D.CancelInd= 'N'");
                SQL.AppendLine("Left Join tblvoucherhdr E On D.DocNo=E.VoucherRequestDocNo And E.CancelInd= 'N'  ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");


                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                     //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "CurCode",
                         "Amt",
                         "SoAmt",
                         //11-15
                         "CtName",
                         "SODocNo",
                         "DocNoVC",
                         "DocDtVC",
                         "Address",
                         //16-19
                         "Remark",
                         "TaxAmt",
                         "SignUser",
                         "SignUserPos"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ld.Add(new ARDep()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                DocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CurCode = Sm.DrStr(dr, c[8]),
                                Amt = Sm.DrDec(dr, c[9]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[10])),
                                Terbilang2 = Convert(Sm.DrDec(dr, c[10])),
                                Terbilang3 = Sm.Terbilang(Sm.DrDec(dr, c[9])+Sm.DrDec(dr, c[17])),
                                Terbilang4 = Convert(Sm.DrDec(dr, c[9])+Sm.DrDec(dr, c[17])),
                                SoAmt = Sm.DrDec(dr, c[10]),

                                CtName = Sm.DrStr(dr, c[11]),
                                SODocNo = Sm.DrStr(dr, c[12]),
                                DocNoVC = Sm.DrStr(dr, c[13]),
                                DocDtVC = Sm.DrStr(dr, c[14]),
                                Address = Sm.DrStr(dr, c[15]),

                                Remark = Sm.DrStr(dr, c[16]),
                                TaxAmt = Sm.DrDec(dr, c[17]),
                                SignUser = Sm.DrStr(dr, c[18]),
                                SignUserPos = Sm.DrStr(dr, c[19]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(ld);

                if(Sm.GetParameter("DocTitle") == "MNET")
                    Sm.PrintReport("ARDownpaymentMNET", myLists, TableName, false);
                else
                    Sm.PrintReport("ARDownpayment", myLists, TableName, false);
            }
        }

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblARDownpayment ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        // comment by RDH karena di ubah pakau common method setLue.cs untuk kebutuhan parameter isCustomerComboShow

        //internal void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        //{
        //    try
        //    {
        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
        //        SQL.AppendLine("From TblCustomer ");
        //        if (CtCode.Length == 0)
        //            SQL.AppendLine("Where ActInd='Y' ");
        //        else
        //            SQL.AppendLine("Where CtCode=@CtCode ");
        //        SQL.AppendLine("Order By CtName; ");

        //        var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //        if (CtCode.Length != 0)
        //            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

        //        Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //        if (CtCode.Length != 0) Sm.SetLue(LueCtCode, CtCode);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m;
            try
            {
                var SQL = new StringBuilder();

                if (mIsARDPProcessTo1Journal)
                {
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdBool(Grd3, Row, 8))
                        {
                            
                            if (Sm.GetGrdBool(Grd3, Row, 6) && Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                //kalau dicentang di debit, (-)
                                COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdBool(Grd3, Row, 7) && Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                //kalau dicentang di credit, (+)
                                COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                else
                {
                    SQL.AppendLine("Select Concat(C.ParValue, A.CtCode) As AcNo ");
                    SQL.AppendLine("From TblCustomer A ");
                    SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode ");
                    SQL.AppendLine("Left Join TblParameter C On C.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("Where A.CtCtCode is Not Null ");
                    SQL.AppendLine("And A.CtCode=@CtCode Limit 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    var AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    string AcType = Sm.GetValue(cm);

                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd3, Row, 1)))
                        {
                            if (Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeOtherAmt()
        {
            var OtherAmt = Sm.GetValue(
                "Select Amt From TblARDownpayment " +
                "Where SODocNo=@Param And CancelInd='N' And Status In ('O', 'A');",
                TxtSODocNo.Text);
            if (OtherAmt.Length == 0) OtherAmt = "0";
            TxtOtherAmt.EditValue = Sm.FormatNum(decimal.Parse(OtherAmt), 0);
        }

        
        internal void ComputeAmtAftTax()
        {
            decimal
                AmtBefTax = 0m,
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m
                ;
            string
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3)
                ;
            
            if (TxtAmtBefTax.Text.Length > 0) AmtBefTax = decimal.Parse(TxtAmtBefTax.Text);
            
            TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);

            if (TaxCode.Length != 0)
            {
                var TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
                if (TaxRate != 0) TaxAmt = TaxRate * 0.01m * AmtBefTax;
            }
            if (TaxCode2.Length != 0)
            {
                var TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
                if (TaxRate2 != 0) TaxAmt2 = TaxRate2 * 0.01m * AmtBefTax;
            }
            if (TaxCode3.Length != 0)
            {
                var TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
                if (TaxRate3 != 0) TaxAmt3 = TaxRate3 * 0.01m * AmtBefTax;
            }

            TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
            TxtAmtAftTax.EditValue = Sm.FormatNum(AmtBefTax + TaxAmt + TaxAmt2 + TaxAmt3, 0);
            //TxtAmt.EditValue = TxtAmtAftTax.EditValue;
        }

        private decimal GetAmtBefTax()
        {
            var SQL = new StringBuilder();
            var Amt = 0m;

            SQL.AppendLine("Select (A.Amt+A.AmtBOM)-IfNull(B.OtherAmt, 0.00) As Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Sum(T2.Amt) As OtherAmt ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblARDownpayment T2 On T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            SQL.AppendLine("        And T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("    Where T1.Amt+T1.AmtBOM>0.00 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocNo=@Param ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Amt+A.AmtBOM>0.00 ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@Param;");

            Amt = Sm.GetValueDec(SQL.ToString(), TxtSODocNo.Text);
            return Amt;
        }

        #endregion

        #endregion

        #region grid Event

        //UPLOAD FILE - START

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd4.SelectedRows.Count > 0)
            {
                for (int Index = Grd4.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd4.SelectedRows[Index].Index;
            }

            if (mStateIndicator == "E" && TxtStatus.Text != "Approved")
            {
                if (Sm.GetGrdStr(Grd4, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd4, e, BtnSave);
                    Sm.GrdEnter(Grd4, e);
                    Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
                }
            }
            else if (mStateIndicator == "I")
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.GrdEnter(Grd4, e);
                Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
            }
        }


        //UPLOAD FILE - END

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmARDownPaymentDlg2(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmARDownPaymentDlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 4].Value = 0;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 3].Value = 0;

            if (e.ColIndex == 6 && Sm.GetGrdBool(Grd3, e.RowIndex, 6))
                Grd3.Cells[e.RowIndex, 7].Value = false;

            if (e.ColIndex == 7 && Sm.GetGrdBool(Grd3, e.RowIndex, 7))
                Grd3.Cells[e.RowIndex, 6].Value = false;

            if (e.ColIndex == 6 || e.ColIndex == 7)
                Grd3.Cells[e.RowIndex, 8].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 7 }, e.ColIndex))
                ComputeCOAAmt();
        }

        #region Event

        #region Button Event

        private void BtnSODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmARDownpaymentDlg(this));
        }

        private void BtnSODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSODocNo, "SO#", false))
            {
                try
                {
                    if (TxtSODocNo.Text.Contains("SC"))
                    {
                        var f = new FrmSalesContract(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = TxtSODocNo.Text;
                        f.ShowDialog();
                    }
                    else
                    {
                        if (mGenerateCustomerCOAFormat == "1")
                        {
                            var f = new FrmSO2(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtSODocNo.Text;
                            f.ShowDialog();

                        }
                        else  
                        {
                            var f = new FrmSOContract(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtSODocNo.Text;
                            f.ShowDialog();
                        }
                    }
                    
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #endregion

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt?"Y":"N");
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept? "Y" : "N");
        }
        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
        }

        private void LueAcNoType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcNoType, new Sm.RefreshLue2(Sl.SetLueOption), "AcNoTypeForARDP");
        }


        // === TAX
        private void TxtAmtBefTax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmtBefTax, 2);
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void TxtAmtAftTax_EditValueChanged(object sender, EventArgs e)
        {
            TxtAmt.Text = TxtAmtAftTax.Text;
        }
        // === TAX


        #endregion

        #endregion

        #region Class

        private class ARDep
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }

            public string CompanyFax { get; set; }
            public string SignUser { get; set; }
            public string SignUserPos { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public decimal TaxAmt { get; set; }

            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string Terbilang3 { get; set; }
            public string Terbilang4 { get; set; }
            public decimal SoAmt { get; set; }
            public string CtName { get; set; }
            public string SODocNo { get; set; }

            public string DocNoVC { get; set; }
            public string DocDtVC { get; set; }
            public string Address { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
        }

        private class ARDPSKI
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddress2 { get; set; }
            public string DocNo { get; set; }
            public string ARDocDt { get; set; }
            public string SODocNo { get; set; }
            public string SODocDt { get; set; }
            public string SORemark { get; set; }
            public string CustomerName { get; set; }
            public string CustomerAddress { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerFax { get; set; }
            public decimal DownpaymentAmount { get; set; }
            public string ARDPRemark { get; set; }
            public string Terbilang { get; set; }
            public string BankAcNo { get; set; }
            public string BranchAcNm { get; set; }
            public string BankName { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public string TerbilangInvoice { get; set; }
            public int Nomor { get; set; }
            public string ReceiptNo { get; set; }
            public decimal Amt { get; set; }
            public string TodayDate { get; set; }
        }

        #endregion      
    }
}
