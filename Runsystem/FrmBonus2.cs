﻿#region Update
/*
    12/12/2018 [TKG] Bonus untuk HIN
    26/12/2018 [TKG] bug saat save 
    02/05/2019 [TKG] tambah validasi dan perubahan perhitungan amount untuk bonus tanpa tax allowance.
    13/03/2020 [WED/HIN] decimal point menjadi 2 untuk CSV nya, berdasarkan parameter DecimalPointForMandiriPayroll
    09/11/2020 [WED/PHT] tambah rumus baru untuk gross dan gross up berdasarkan parameter IsBonus2UseTaxCalcFormula
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    08/06/2022 [HAR/GSS] bug jurnal tidak terbentuk karena salaah saat save deptcode
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBonus2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mMainCurCode = string.Empty,
            mDecimalPointForMandiriPayroll = string.Empty;
        internal bool 
            mIsBonusWithoutTaxAllowance = false,
            mIsBonusWithoutTaxAllowanceInclResignee = false;
        private string  mBankCode = string.Empty, mBonusDeptCode = string.Empty;
        private decimal mFunctionalExpenses = 0m, mFunctionalExpensesMaxAmt = 0m;
        private List<TI> mlTI = null;
        private List<NTI> mlNTI = null;
        private bool mIsBonus2UseTaxCalcFormula = false;
        private string mGrossCodeForBonus2 = string.Empty;
        private string mGrossUpCodeForBonus2 = string.Empty;

        internal FrmBonus2Find FrmFind;
        
        #endregion

        #region Constructor

        public FrmBonus2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmBonus");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                GetData();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                if (mIsBonus2UseTaxCalcFormula) Sl.SetLueOption(ref LueTaxCalc, "Bonus2TaxCalc");
                else
                {
                    LblTaxCalc.Visible = LueTaxCalc.Visible = false;
                }
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueCurCode(ref LueCurCode);
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Join",

                        //6-10
                        "Resign",
                        "Bank",
                        "Bank"+Environment.NewLine+"Account#",
                        "Value",
                        "Tax",
                        
                        //11-14
                        "Amount",
                        "Initial Amount",
                        "Initial Tax",
                        "DeptCode"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        80, 200, 150, 150, 100, 
                        
                        //6-10
                        100, 150, 150, 120, 120, 

                        //11-14
                        130, 0, 0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 14 });
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueSiteCode, TxtCoefficient,
                        LuePaymentType, LueBankAcCode, MeeRemark, LueTaxCalc
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    BtnProcess.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueSiteCode, TxtCoefficient, LuePaymentType, LueBankAcCode, 
                        MeeRemark, LueTaxCalc
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mBankCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, LueSiteCode, 
                LuePaymentType, LueBankAcCode, TxtBankCode, LueCurCode, TxtVoucherRequestDocNo, 
                TxtVoucherDocNo, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2, LueTaxCalc
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtCoefficient }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 11, 12, 13 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        private void ClearGrd1()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            TxtCoefficient.EditValue = Sm.FormatNum(1m, 0);
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 11, 12, 13 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBonus2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                TxtCoefficient.EditValue = Sm.FormatNum(1m, 0);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Bonus", "TblBonusHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBonusHdr(DocNo, VoucherRequestDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveBonusDtl(DocNo, Row));
            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtCoefficient, "Coefficient", true) ||
                IsTaxCalcEmpty() ||
                Sm.IsTxtEmpty(TxtAmt, "Total amount", true) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsEmployeeResigneeExisted() ||
                IsEmployeeWithTheSameBonusYrExisted();
        }

        private bool IsTaxCalcEmpty()
        {
            if (!mIsBonus2UseTaxCalcFormula) return false;

            return Sm.IsLueEmpty(LueTaxCalc, "Tax Calculation");
        }

        private bool IsEmployeeWithTheSameBonusYrExisted()
        {
            if (mIsBonusWithoutTaxAllowance) return false;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')')  From TblEmployee ");
            SQL.AppendLine("Where EmpCode In (");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblBonusHdr T1 ");
            SQL.AppendLine("    Inner Join TblBonusDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.TaxAllowanceInd='Y' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And Left(DocDt, 4)=@DocYr ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocYr", Sm.Left(Sm.GetDte(DteDocDt), 4));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already processed in another document."
                    );
                return true;
            }
            return false;
        }

        private bool IsEmployeeResigneeExisted()
        {
            if (mIsBonusWithoutTaxAllowanceInclResignee) return false;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')')  From TblEmployee ");
            SQL.AppendLine("Where ResignDt Is Not Null ");
            SQL.AppendLine("And ResignDt<=@DocDt ");
            SQL.AppendLine("And EmpCode Not In (");
            SQL.AppendLine("    Select EmpCode From TblEmployee ");
            SQL.AppendLine("    Where Left(Resigndt, 4)=@DocYr ");
            SQL.AppendLine("    And ");
            SQL.AppendLine("    EmpCode In ( ");
            SQL.AppendLine("        Select EmpCode From TblPPS ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            SQL.AppendLine("        And JobTransfer='T' ");
            SQL.AppendLine("        And ResignDtNew Is Not Null ");
            SQL.AppendLine("        And Left(ResignDtNew, 4)=@DocYr ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            var DocDt = Sm.GetDte(DteDocDt);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@DocYr", Sm.Left(DocDt, 4));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already resigned."
                    );
                return true;
            }
            return false;
        }

        private bool IsEmployeeNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsEmployeeNotValid(r)) return true;
                }
            }
            return false;
        }

        private bool IsEmployeeNotValid(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblBonusHdr A ");
            SQL.AppendLine("Inner Join TblBonusDtl B On A.DocNo=B.DocNo And B.EmpCode=@Param1 ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@Param2 ");
            SQL.AppendLine("And A.Status In ('O', 'A') Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Param2", Sm.Left(Sm.GetDte(DteDocDt), 6));
            Sm.CmParam<String>(ref cm, "@Param1", Sm.GetGrdStr(Grd1, r, 1));

            return Sm.IsDataExist(cm,
                "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                "This employee already processed in another bonus document (" + Sm.GetValue(SQL.ToString(), Sm.GetGrdStr(Grd1, r, 1), Sm.Left(Sm.GetDte(DteDocDt), 6), string.Empty) + ")"
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Employee's code is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, 
                    "Employee's code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Employee's name is empty.")) return true;
            }
            return false;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.AppendLine("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.AppendLine("Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNoTemp ");
                SQL.AppendLine("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.AppendLine("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        type = string.Empty;

        //    type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

        //    var SQL = new StringBuilder();

        //    if (type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}

        private MySqlCommand SaveBonusHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBonusHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, Coefficient, SiteCode, ");
            SQL.AppendLine("PaymentType, BankAcCode, BankCode, CurCode, Amt, VoucherRequestDocNo, TaxAllowanceInd, Remark, ");
            if (mIsBonus2UseTaxCalcFormula)
                SQL.AppendLine("TaxCalc, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', @Coefficient, @SiteCode, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @BankCode, @CurCode, @Amt, @VoucherRequestDocNo, @TaxAllowanceInd, @Remark, ");
            if (mIsBonus2UseTaxCalcFormula)
                SQL.AppendLine("@TaxCalc, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Coefficient", Decimal.Parse(TxtCoefficient.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@TaxAllowanceInd", mIsBonusWithoutTaxAllowance?"N":"Y");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsBonus2UseTaxCalcFormula)
                Sm.CmParam<String>(ref cm, "@TaxCalc", Sm.GetLue(LueTaxCalc));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBonusDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBonusDtl(DocNo, EmpCode, DeptCode, Value, Tax, Amt, PayrunCode, CreateBy, CreateDt) " +
                    "Values (@DocNo, @EmpCode, @DeptCode, @Value, @Tax, @Amt, Null, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @DeptCode, ");
            SQL.AppendLine("'54', 'C', @PaymentType, @BankAcCode, ");
            SQL.AppendLine("@CreateBy, 0, @CurCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl ");
            SQL.AppendLine("(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Remark, @Amt, @CreateBy, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @BonusDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='Bonus' ");
            if (Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where DocType='Bonus' And SiteCode Is Not Null " +
                "Limit 1;"))
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And T.SiteCode = @SiteCode ");
            }
            SQL.AppendLine("; ");
            
            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='Bonus' ");
            SQL.AppendLine("    And DocNo=@BonusDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblBonusHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@BonusDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='Bonus' ");
            SQL.AppendLine("    And DocNo=@BonusDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@BonusDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", mBonusDeptCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelBonusHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessed();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBonusHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private MySqlCommand CancelBonusHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBonusHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowBonusHdr(DocNo);
                ShowBonusDtl(DocNo);
                Sm.ShowDocApproval(DocNo, "Bonus", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBonusHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.SiteCode, A.PaymentType, A.BankAcCode, A.BankCode, B.BankName, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.VoucherRequestDocNo, C.VoucherDocNo, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2, A.Coefficient ");
            if (mIsBonus2UseTaxCalcFormula)
                SQL.AppendLine(", A.TaxCalc ");
            else
                SQL.AppendLine(", null As TaxCalc ");
            SQL.AppendLine("From TblBonusHdr A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "SiteCode", 
                    
                    //6-10
                    "PaymentType", "BankAcCode", "BankCode", "BankName", "CurCode", 
                    
                    //11-15
                    "Amt", "VoucherRequestDocNo", "VoucherDocNo", "Remark", "JournalDocNo", 
                    
                    //16-18
                    "JournalDocNo2", "Coefficient", "TaxCalc"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[7]));
                     mBankCode = Sm.DrStr(dr, c[8]);
                     TxtBankCode.EditValue = Sm.DrStr(dr, c[9]);
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[10]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[13]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                     TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[15]);
                     TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[16]);
                     TxtCoefficient.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                     if (mIsBonus2UseTaxCalcFormula)
                         Sm.SetLue(LueTaxCalc, Sm.DrStr(dr, c[18]));
                 }, true
             );
        }

        private void ShowBonusDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("B.LeaveStartDt, B.ResignDt, F.BankName, B.BankAcNo, ");
            SQL.AppendLine("A.Value, A.Tax, A.Amt ");
            SQL.AppendLine("From TblBonusDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblBank F On B.BankCode=F.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "PosName", "DeptName", "LeaveStartDt", "ResignDt", 

                        //6-10
                        "BankName", "BankAcNo", "Value", "Tax", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ProcessTax(ref List<Result> l, int i)
        {
            string NTI = l[i].PTKP;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                //GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                //PPH21Setahun = 0m,
                PPH21Sebulan = 0m,

                TotalImbalan2 = 0m,
                TotalPengurangan2 = 0m,
                GajiBersihSebulan2 = 0m,
                //GajiBersihDisetahunkan = 0m,
                NTIAmt2 = 0m,
                PKP2 = 0m,
                TaxTemp2 = PKP,
                Amt2Temp2 = 0m,
                //PPH21Setahun = 0m,
                PPH21Sebulan2 = 0m;

            TotalImbalan = (l[i].Value) * 12m;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt * 12m)
                ? mFunctionalExpensesMaxAmt * 12m
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;

            foreach (var x in mlNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihSebulan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihSebulan - NTIAmt;

            PPH21Sebulan = decimal.Truncate(PKP);
            PPH21Sebulan = PPH21Sebulan - (PPH21Sebulan % 1000);


            if (PPH21Sebulan > 0m && mlTI.Count > 0)
            {
                TaxTemp = PPH21Sebulan;
                Amt2Temp = 0m;
                PPH21Sebulan = 0m;

                foreach (TI x in mlTI.OrderBy(o => o.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (x.Amt2 - Amt2Temp))
                        {
                            PPH21Sebulan += (TaxTemp * x.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Sebulan += ((x.Amt2 - Amt2Temp) * x.TaxRate * 0.01m);
                            TaxTemp -= (x.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = x.Amt2;
                }
            }

            TotalImbalan2 = TotalImbalan + l[i].Amt;

            TotalPengurangan2 =
                ((mFunctionalExpenses * 0.01m * TotalImbalan2 > mFunctionalExpensesMaxAmt * 12m)
                ? mFunctionalExpensesMaxAmt * 12m
                : mFunctionalExpenses * 0.01m * TotalImbalan2);

            GajiBersihSebulan2 = TotalImbalan2 - TotalPengurangan2;

            foreach (var x in mlNTI.Where(x => x.Status == NTI))
                NTIAmt2 = x.Amt;

            if (GajiBersihSebulan2 - NTIAmt2 < 0m)
                PKP2 = 0m;
            else
                PKP2 = GajiBersihSebulan2 - NTIAmt2;

            PPH21Sebulan2 = decimal.Truncate(PKP2);
            PPH21Sebulan2 = PPH21Sebulan2 - (PPH21Sebulan2 % 1000);


            if (PPH21Sebulan2 > 0m && mlTI.Count > 0)
            {
                TaxTemp2 = PPH21Sebulan2;
                Amt2Temp2 = 0m;
                PPH21Sebulan2 = 0m;

                foreach (TI x in mlTI.OrderBy(o => o.SeqNo))
                {
                    if (TaxTemp2 > 0m)
                    {
                        if (TaxTemp2 <= (x.Amt2 - Amt2Temp2))
                        {
                            PPH21Sebulan2 += (TaxTemp2 * x.TaxRate * 0.01m);
                            TaxTemp2 = 0m;
                        }
                        else
                        {
                            PPH21Sebulan2 += ((x.Amt2 - Amt2Temp2) * x.TaxRate * 0.01m);
                            TaxTemp2 -= (x.Amt2 - Amt2Temp2);
                        }
                    }
                    Amt2Temp2 = x.Amt2;
                }
            }
            l[i].Tax = PPH21Sebulan2 - PPH21Sebulan;
            if (l[i].Tax <= 0) l[i].Tax = 0m;
            l[i].InitialTax = l[i].Tax;
        }

        private void CalculateValueByCoefficient()
        {
            decimal Coefficient = 0m;
            if (TxtCoefficient.Text.Length != 0) Coefficient = Decimal.Parse(TxtCoefficient.Text);

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if(Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 12) * Coefficient;
                        Grd1.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 13) * Coefficient;
                        if (!mIsBonus2UseTaxCalcFormula) // non-PHT
                        {
                            if (mIsBonusWithoutTaxAllowance)
                                Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            else
                                Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9);
                        }
                        else // PHT
                        {
                            if (Sm.GetLue(LueTaxCalc) == mGrossCodeForBonus2)
                                Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            else if (Sm.GetLue(LueTaxCalc) == mGrossUpCodeForBonus2)
                                Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9);
                            else
                            {
                                if (mIsBonusWithoutTaxAllowance)
                                    Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                                else
                                    Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9);
                            }
                        }
                    }
                }
                ComputeAmt();
            }
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;

            if(Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        if (SQL.Length > 0) SQL += ",";
                        SQL += Sm.GetGrdStr(Grd1, i, 1);
                    }
                }
            }

            return (SQL.Length <= 0) ? "'XXX'" : SQL;
        }

        private void GetParameter()
        {
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mBonusDeptCode = Sm.GetParameter("BonusDeptCode");
            //mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            var MenuCodeForBonusWithoutTaxAllowance = Sm.GetParameter("MenuCodeForBonusWithoutTaxAllowance");
            if (MenuCodeForBonusWithoutTaxAllowance.Length > 0)
            {
                mIsBonusWithoutTaxAllowance = Sm.CompareStr(
                    MenuCodeForBonusWithoutTaxAllowance,
                    mMenuCode);
                if (mIsBonusWithoutTaxAllowance)
                    mIsBonusWithoutTaxAllowanceInclResignee = 
                        Sm.GetParameterBoo("IsBonusWithoutTaxAllowanceInclResignee");
            }
            mDecimalPointForMandiriPayroll = Sm.GetParameter("DecimalPointForMandiriPayroll");
            mIsBonus2UseTaxCalcFormula = Sm.GetParameterBoo("IsBonus2UseTaxCalcFormula");
            mGrossCodeForBonus2 = Sm.GetParameter("GrossCodeForBonus2");
            mGrossUpCodeForBonus2 = Sm.GetParameter("GrossUpCodeForBonus2");
        }

        internal void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Amt += Sm.GetGrdDec(Grd1, r, 11);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ProcessCSVa(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            var AmtTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            EmpCodeTemp = arr[0].Trim();
                            if (EmpCodeTemp.Length < 8) EmpCodeTemp = Sm.Right(string.Concat("00000000", EmpCodeTemp), 8);
                            if (arr[1].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[1].Trim());
                            else
                                AmtTemp = 0m;
                            l.Add(new Result()
                            {
                                EmpCode = EmpCodeTemp,
                                EmpName = string.Empty,
                                PosName = string.Empty,
                                DeptName = string.Empty,
                                JoinDt = string.Empty,
                                ResignDt = string.Empty,
                                BankName = string.Empty,
                                BankAcNo = string.Empty,
                                NPWP = string.Empty,
                                PTKP = string.Empty,
                                Value = AmtTemp,
                                Tax = 0m,
                                Amt = AmtTemp,
                                InitialAmt = AmtTemp,
                                InitialTax = 0m
                            });
                        }
                    }
                }
            }
        }

        private void ProcessCSVb(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), l[i].EmpCode);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName, A.JoinDt, A.ResignDt, D.BankName, A.BankAcNo, A.PTKP, A.NPWP ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblBank D On A.BankCode=D.BankCode ");
            SQL.AppendLine("Where A.SiteCode=@SiteCode ");
            //SQL.AppendLine("And A.EmploymentStatus = '2' ");
            if (!mIsBonusWithoutTaxAllowance)
            {
                SQL.AppendLine("And A.EmpCode Not In (");
                SQL.AppendLine("    Select T2.EmpCode ");
                SQL.AppendLine("    From TblBonusHdr T1 ");
                SQL.AppendLine("    Inner Join TblBonusDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.TaxAllowanceInd='Y' ");
                SQL.AppendLine("    And T1.Status In ('O', 'A') ");
                SQL.AppendLine("    And Left(DocDt, 4)=@DocYr ");
                SQL.AppendLine(") ");
            }
            if (!mIsBonusWithoutTaxAllowanceInclResignee)
            {
                SQL.AppendLine("And (A.ResignDt Is Null Or ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        A.ResignDt Is Not Null And ( ");
                SQL.AppendLine("            A.ResignDt>@CurrentDate ");
                SQL.AppendLine("            Or ");
                SQL.AppendLine("            A.EmpCode In (");
                SQL.AppendLine("                Select EmpCode From TblEmployee ");
                SQL.AppendLine("                Where Left(Resigndt, 4)=@DocYr ");
                SQL.AppendLine("                And ");
                SQL.AppendLine("                EmpCode In ( ");
                SQL.AppendLine("                    Select EmpCode From TblPPS ");
                SQL.AppendLine("                    Where CancelInd='N' ");
                SQL.AppendLine("                    And Status In ('O', 'A') ");
                SQL.AppendLine("                    And JobTransfer='T' ");
                SQL.AppendLine("                    And ResignDtNew Is Not Null ");
                SQL.AppendLine("                    And Left(ResignDtNew, 4)=@DocYr ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            var DocDt = Sm.GetDte(DteDocDt);

            Sm.CmParamDt(ref cm, "@CurrentDate", DocDt);
            Sm.CmParam<String>(ref cm, "@DocYr", Sm.Left(DocDt, 4));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpName", 
                    "DeptName", 
                    "PosName", 
                    "JoinDt", 
                    "ResignDt",

                    //6-9
                    "BankName",
                    "BankAcNo",
                    "NPWP",
                    "PTKP"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].EmpName = Sm.DrStr(dr, c[1]);
                                l[i].DeptName = Sm.DrStr(dr, c[2]);
                                l[i].PosName = Sm.DrStr(dr, c[3]);
                                l[i].JoinDt = Sm.DrStr(dr, c[4]);
                                l[i].ResignDt = Sm.DrStr(dr, c[5]);
                                l[i].BankName = Sm.DrStr(dr, c[6]);
                                l[i].BankAcNo = Sm.DrStr(dr, c[7]);
                                l[i].NPWP = Sm.DrStr(dr, c[8]);
                                l[i].PTKP = Sm.DrStr(dr, c[9]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCSVc(ref List<Result> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].NPWP.Length != 0 && l[i].PTKP.Length != 0)
                    ProcessTax(ref l, i);
            }
        }

        private void ProcessCSVd(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].PosName;
                r.Cells[4].Value = l[i].DeptName;
                if (l[i].JoinDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].JoinDt);
                if (l[i].ResignDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(l[i].ResignDt);
                r.Cells[7].Value = l[i].BankName;
                r.Cells[8].Value = l[i].BankAcNo;
                r.Cells[9].Value = l[i].Value;
                r.Cells[10].Value = l[i].Tax;
                if (mIsBonusWithoutTaxAllowance)
                    r.Cells[11].Value = l[i].Value - l[i].Tax;
                else
                    r.Cells[11].Value = l[i].Amt;
                r.Cells[12].Value = l[i].InitialAmt;
                r.Cells[13].Value = l[i].InitialTax;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13 });
            Grd1.EndUpdate();
            CalculateValueByCoefficient();
        }

        private void GetData()
        {
            mlTI = new List<TI>();
            mlNTI = new List<NTI>();

            ProcessTI(ref mlTI);
            ProcessNTI(ref mlNTI);
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y' And A.UsedFor = '01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd1();
        }
        
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
                ClearGrd1();
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
                mBankCode=string.Empty;
                TxtBankCode.EditValue = null;
                var BankAcCode = Sm.GetLue(LueBankAcCode);
                if (BankAcCode.Length>0)
                {
                    mBankCode = Sm.GetValue("Select BankCode From TblBankAccount Where BankCode Is Not Null And BankAcCode=@Param;", BankAcCode);
                    if (mBankCode.Length>0) TxtBankCode.EditValue = Sm.GetValue("Select BankName From TblBank Where BankCode=@Param;", mBankCode);
                }
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtCoefficient_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCoefficient, 0);
                if (Decimal.Parse(TxtCoefficient.Text) == 0m) TxtCoefficient.Text = Sm.FormatNum(1m, 0);
                if (Grd1.Rows.Count > 1) CalculateValueByCoefficient();
            }
        }

        private void LueTaxCalc_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsBonus2UseTaxCalcFormula)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueOption), "Bonus2TaxCalc");
                CalculateValueByCoefficient();
            }
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                        if (e.ColIndex == 0 &&
                            !Sm.IsDteEmpty(DteDocDt, "Date") &&
                            !Sm.IsLueEmpty(LueSiteCode, "Site"))
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmBonus2Dlg(
                                    this, 
                                    Sm.GetDte(DteDocDt), 
                                    Sm.GetLue(LueSiteCode),
                                    decimal.Parse(TxtCoefficient.Text)
                                    ));
                        }
                    
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (
                    e.ColIndex == 0 &&
                    !Sm.IsDteEmpty(DteDocDt, "Date") &&
                    !Sm.IsLueEmpty(LueSiteCode, "Site")
                    )
                    Sm.FormShowDialog(new FrmBonus2Dlg(
                        this,
                        Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode),
                        decimal.Parse(TxtCoefficient.Text)
                        ));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Button Click Event

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 8].Value = "'" + Sm.GetGrdStr(Grd1, Row, 8);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 8].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 8), Sm.GetGrdStr(Grd1, Row, 8).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsTxtEmpty(TxtCoefficient, "Coefficient", true) ||
                IsTaxCalcEmpty()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                ProcessCSVa(ref lResult);
                if (lResult.Count > 0)
                {
                    ProcessCSVb(ref lResult);
                    ProcessCSVc(ref lResult);
                    ProcessCSVd(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnCSV_Click(object sender, EventArgs e)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblBonusHdr T ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And BankCode Is Not Null ");
            SQL.AppendLine("And Status='A' ");
            SQL.AppendLine("And DocNo=@Param ");
            SQL.AppendLine("And Not Exists(Select 1 from TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=T.VoucherRequestDocNo) ");
            SQL.AppendLine("And Exists(Select 1 From TblParameter Where ParCode='DocTitle' And ParValue='HIN') ");

            if (
                !Sm.IsTxtEmpty(TxtDocNo, "Document#", false) &&
                mBankCode.Length > 0 &&
                Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text)
                )
            {
                Sm.FormShowDialog(new FrmBonus2Dlg2(
                                    this,
                                    TxtDocNo.Text,
                                    Sm.GetDte(DteDocDt).Substring(0, 8),
                                    mBankCode
                                    ));
            }
            else
                Sm.StdMsg(mMsgType.Warning, "You can't process this document.");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string BankName { get; set; }
            public string BankAcNo { get; set; }
            public string PTKP { get; set; }
            public string NPWP { get; set; }
            public decimal Value { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public decimal InitialAmt { get; set; }
            public decimal InitialTax { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion
    }
}
