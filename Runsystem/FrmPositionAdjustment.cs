﻿#region Update
/*
    21/02/2022 [SET/PHT] New Apps
    13/04/2022 [SET/PHT] Bug Event LueLeave karena value 1, harusnya 0
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPositionAdjustment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPositionAdjustmentFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPositionAdjustment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Position Adjustment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueLevelCode(ref LueLevel);
                Sl.SetLueOption(ref LueYoS, "PositionAdjustmentServiceYrRange");
                LueLevel.Visible = false;
                LueYoS.Visible = false;
                LueGrade.Visible = false;
                LueLevel3.Visible = false;
                LueYoS3.Visible = false;
                TcPositionAdjustment.SelectedTabPage = TpLevelAllowance;

                SetFormControl(mState.View);
                
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Tab1

            Grd2.Cols.Count = 6;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "LevelCode",

                    //1-5
                    "Level",
                    "YoSCode",
                    "Years of Service",
                    "Allowance I",
                    "Allowance II",
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    100, 0, 100, 100, 100
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2 });

            #endregion

            #region Tab2

            Grd3.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        "Grade Code",

                        "Grade", 
                        "Level Code", 
                        "Level", 
                        "YoSCode", 
                        "Years of Service",

                        "Allowance"
                    },
                    new int[]
                    {
                        100,

                        100, 100, 100, 100, 100,

                        100
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 4 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4 });

            #endregion

            #region Tab3

            Grd4.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] { "Years of Service", "Allowance" },
                    new int[] { 100, 100 }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 0, 1 }, 0);

            #endregion

        }

        internal void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt,  MeeRemark, LueLevel, LueYoS, LueGrade, LueLevel3, LueYoS3

                    }, true);
                    BtnCopyData.Enabled = false;
                    Sm.SetControlReadOnly(ChkCancelInd, true);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueLevel, LueYoS, LueGrade, LueLevel3, LueYoS3

                    }, false);
                    BtnCopyData.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 3, 4, 5 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 3, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    Grd2.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark, LueLevel, LueYoS, LueGrade, LueLevel3, LueYoS3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { 
                
            }, 0);
            ChkCancelInd.Checked = false;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 0, 1 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPositionAdjustmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo, DocDt, CancelInd, Remark ");
                SQL.AppendLine("FROM tblpositionadjustmenthdr ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-3
                            "DocDt", "CancelInd", "Remark",

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
                ShowPositionAdjustmentDtl(DocNo);
                ShowPositionAdjustmentDtl2(DocNo);
                ShowPositionAdjustmentDtl3(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPositionAdjustmentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.LevelCode, B.LevelName, A.ServiceYrRange, C.OptDesc, A.AllowanceAmt, A.AllowanceAmt2 ");
            SQL.AppendLine("From tblpositionadjustmentdtl A ");
            SQL.AppendLine("INNER JOIN tbllevelhdr B ON A.LevelCode = B.LevelCode ");
            SQL.AppendLine("INNER JOIN tbloption C ON A.ServiceYrRange = C.OptCode AND C.OptCat = 'PositionAdjustmentServiceYrRange' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "LevelCode", "LevelName", "ServiceYrRange", "OptDesc", "AllowanceAmt", "AllowanceAmt2" },
                (MySqlDataReader dr, iGrid Grd2, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPositionAdjustmentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.GrdLvlCode, B.GrdLvlName, A.LevelCode, C.LevelName, A.ServiceYrRange, D.OptDesc, A.AllowanceAmt ");
            SQL.AppendLine("From tblpositionadjustmentdtl2 A ");
            SQL.AppendLine("INNER JOIN tblgradelevelhdr B ON A.GrdLvlCode = B.GrdLvlCode ");
            SQL.AppendLine("INNER JOIN tbllevelhdr C ON A.LevelCode = C.LevelCode ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.ServiceYrRange = D.OptCode AND D.OptCat = 'PositionAdjustmentServiceYrRange' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "GrdLvlCode", "GrdLvlName", "LevelCode", "LevelName", "ServiceYrRange", "OptDesc", "AllowanceAmt" },
                (MySqlDataReader dr, iGrid Grd3, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPositionAdjustmentDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select ServiceYr, AllowanceAmt ");
            SQL.AppendLine("From tblpositionadjustmentdtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { "ServiceYr", "AllowanceAmt" },
                (MySqlDataReader dr, iGrid Grd4, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PositionAdjustment", "TblPositionAdjustmentHdr");

            cml.Add(SavePositionAdjustmentHdr(DocNo));

            cml.Add(SavePositionAdjustmentDtl(DocNo));
            cml.Add(SavePositionAdjustmentDtl2(DocNo));
            cml.Add(SavePositionAdjustmentDtl3(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for(int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 0, false, "Level is null")) return true;
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Year of Service is null")) return true;
            }
            
            for(int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 0, false, "Grade is null")) return true;
                if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Level is null")) return true;
                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Year of Service is null")) return true;
            }
            
            //for(int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            //{
            //    if (Sm.IsGrdValueEmpty(Grd4, Row, 0, false, "Year of Service is null")) return true;
            //}

            return false;
        }


        private MySqlCommand SavePositionAdjustmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            //Cancel Document
            SQL.AppendLine("UPDATE tblpositionadjustmenthdr ");
            SQL.AppendLine("SET CancelInd = 'Y', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("WHERE CancelInd = 'N'; ");

            SQL.AppendLine("Insert Into TblPositionAdjustmentHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
            
            #endregion


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePositionAdjustmentDtl(string DocNo)
        {
            bool IsFirst = true;
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            SQLDtl.AppendLine("Insert Into TblPositionAdjustmentDtl(DocNo, DNo, LevelCode, ServiceYrRange, AllowanceAmt, AllowanceAmt2, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl.AppendLine(", ");
                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @LevelCode_" + r.ToString() + ", @ServiceYrRange_" + r.ToString() + ", @AllowanceAmt_" + r.ToString() + ", @AllowanceAmt2_" + r.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@LevelCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 0));
                Sm.CmParam<String>(ref cm, "@ServiceYrRange_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                Sm.CmParam<Decimal>(ref cm, "@AllowanceAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 4));
                Sm.CmParam<Decimal>(ref cm, "@AllowanceAmt2_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 5));
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cm.CommandText = SQLDtl.ToString();

            return cm;
        }
        
        private MySqlCommand SavePositionAdjustmentDtl2(string DocNo)
        {
            bool IsFirst = true;
            var SQLDtl2 = new StringBuilder();
            var cm = new MySqlCommand();

            SQLDtl2.AppendLine("Insert Into TblPositionAdjustmentDtl2(DocNo, DNo, GrdLvlCode, LevelCode, ServiceYrRange, AllowanceAmt, CreateBy, CreateDt) ");
            SQLDtl2.AppendLine("Values ");

            for (int r = 0; r < Grd3.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl2.AppendLine(", ");
                SQLDtl2.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @GrdLvlCode_" + r.ToString() + ", @LevelCode_" + r.ToString() + ", @ServiceYrRange_" + r.ToString() + ", @AllowanceAmt_" + r.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 0));
                Sm.CmParam<String>(ref cm, "@LevelCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 2));
                Sm.CmParam<String>(ref cm, "@ServiceYrRange_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 4));
                Sm.CmParam<Decimal>(ref cm, "@AllowanceAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 6));
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cm.CommandText = SQLDtl2.ToString();
            return cm;
        }

        private MySqlCommand SavePositionAdjustmentDtl3(string DocNo)
        {
            bool IsFirst = true;
            var SQLDtl3 = new StringBuilder();
            var cm = new MySqlCommand();

            SQLDtl3.AppendLine("Insert Into TblPositionAdjustmentDtl3(DocNo, DNo, ServiceYr, AllowanceAmt, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values ");

            for (int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl3.AppendLine(", ");
                SQLDtl3.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ServiceYr_" + r.ToString() + ", @AllowanceAmt_" + r.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ServiceYr_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 0));
                Sm.CmParam<Decimal>(ref cm, "@AllowanceAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 1));
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cm.CommandText = SQLDtl3.ToString();
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPositionAdjustment());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditPositionAdjustment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update Tblpositionadjustmenthdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            
        }


        #endregion    

        #region grid Event
        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                if (e.ColIndex == 1) Sm.LueRequestEdit(ref Grd2, ref LueLevel, ref fCell, ref fAccept, e);
            }
            if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.LueRequestEdit(ref Grd2, ref LueYoS, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd2, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5 });
        }
        
        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                if (e.ColIndex == 1) Sm.LueRequestEdit(ref Grd3, ref LueGrade, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueGrdLvlCode(ref LueGrade);
            }
            if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.LueRequestEdit(ref Grd3, ref LueLevel3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueLevelCode(ref LueLevel3);
            }
            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) Sm.LueRequestEdit(ref Grd3, ref LueYoS3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueOption(ref LueYoS3, "PositionAdjustmentServiceYrRange");
            }
            Sm.GrdRequestEdit(Grd3, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 6 });
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.GrdRequestEdit(Grd4, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 0, 1 });
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        #region Event

        #region Button Event


        #endregion

        #endregion

        #region Misc Control Event

        private void LueLevel_Validated(object sender, EventArgs e)
        {
            LueLevel.Visible = false;
        }

        private void LueLevel_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueLevel_Leave(object sender, EventArgs e)
        {
            if (LueLevel.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (LueLevel.Text.Trim().Length == 0)
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueLevel).Trim();
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueLevel.GetColumnValue("Col2");
                }
            }
        }

        private void LueYoS_Leave(object sender, EventArgs e)
        {
            if (LueYoS.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueYoS.Text.Trim().Length == 0)
                    Grd2.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueYoS).Trim();
                    Grd2.Cells[fCell.RowIndex, 3].Value = LueYoS.GetColumnValue("Col2");
                }
            }
        }

        private void LueYoS_Validated(object sender, EventArgs e)
        {
            LueYoS.Visible = false;
        }

        private void LueYoS_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void LueYoS_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueYoS, new Sm.RefreshLue2(Sl.SetLueOption), "PositionAdjustmentServiceYrRange");
        }

        private void LueGrade_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrade, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void LueGrade_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueGrade_Validated(object sender, EventArgs e)
        {
            LueGrade.Visible = false;
        }

        private void LueGrade_Leave(object sender, EventArgs e)
        {
            if (LueGrade.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (LueGrade.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueGrade).Trim();
                    Grd3.Cells[fCell.RowIndex, 1].Value = LueGrade.GetColumnValue("Col2");
                }
            }
        }

        private void LueLevel3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel3, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void LueLevel3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueLevel3_Leave(object sender, EventArgs e)
        {
            if (LueLevel3.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueLevel3.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueLevel3).Trim();
                    Grd3.Cells[fCell.RowIndex, 3].Value = LueLevel3.GetColumnValue("Col2");
                }
            }
        }

        private void LueLevel3_Validated(object sender, EventArgs e)
        {
            LueLevel3.Visible = false;
        }
        
        private void LueYoS3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueYoS3, new Sm.RefreshLue2(Sl.SetLueOption), "PositionAdjustmentServiceYrRange");
        }

        private void LueYoS3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueYoS3_Leave(object sender, EventArgs e)
        {
            if (LueYoS3.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueYoS.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueYoS3).Trim();
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueYoS3.GetColumnValue("Col2");
                }
            }
        }

        private void LueYoS3_Validated(object sender, EventArgs e)
        {
            LueYoS3.Visible = false;
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPositionAdjustmentDlg(this));
        }

        #endregion

        #endregion

        #region Class

        private class ARDep
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }

            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }

            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public decimal SoAmt { get; set; }
            public string CtName { get; set; }
            public string SODocNo { get; set; }

            public string DocNoVC { get; set; }
            public string DocDtVC { get; set; }
            public string Address { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
        }

        #endregion      
    }
}
