﻿#region Update
/*
    06/07/2022 [BRI/PRODUCT] new apps base on COA
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCOA2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCOA2 mFrmParent; private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCOA2Dlg(FrmCOA2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of Local Account";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                mFrmParent.SetLueEntCode(ref LueEntCode);
                ChkEntCode.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Local Account#",
                        "Local Account Description",
                        "Entity Code",
                        "Entity Name",

                        //6
                        "Closing Date"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, B.AcDesc, A.EntCode, C.EntName, A.ClosingDt From TblFinancialStatementHdr A ");
            SQL.AppendLine("Inner Join TblFinancialStatementDtl B On A.DocNo=B.DocNo And CancelInd!='Y' ");
            SQL.AppendLine("Inner Join TblEntity C On A.EntCode=C.EntCode And ActInd='Y' ");
            SQL.AppendLine("Where A.DocNo = (Select Max(DocNo) From TblFinancialStatementHdr Where DocNo=A.DocNo And CancelInd!='Y') ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueEntCode, "Entity")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "A.EntCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo2.Text, new string[] { "B.AcNo", "B.AcDesc" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.AcNo",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "EntCode", "EntName", "ClosingDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);

                        mFrmParent.Grd1.Rows.Add();

                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 entity.");
        }

        private bool IsEntCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(mFrmParent.SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void TxtAcNo2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Account");
        }

        #endregion

        #endregion

    }
}
