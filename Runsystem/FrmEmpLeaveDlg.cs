﻿#region Update
/*
    23/08/2017 [HAR] bug fixing jumlah cuti besar tidak reload
    31/08/2017 [TKG] menggunakan validasi Employee's Leave Start Date untuk cuti tahunan/besar.
    28/10/2017 [TKG] perhitungan cuti besar utk HIN
    15/11/2017 [TKG] bug fixing perhitungan cuti tahunan
    11/07/2018 [HAR] filter employee old code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeaveDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpLeave mFrmParent;
        private string mSQL = string.Empty;
        private string mLeaveCode = string.Empty;
        private string mRLPCode = string.Empty;
        private string mYr = string.Empty;
        private string
           mAnnualLeaveCode = string.Empty,
           mTrainingPosCode = string.Empty,
           mLongServiceLeaveCode = string.Empty,
           mLongServiceLeaveCode2 = string.Empty,
           mNeglectLeaveCode = string.Empty;
        internal bool mIsSiteMandatory = false, mIsApprovalBySiteMandatory = false;

        #endregion

        #region Constructor

         public FrmEmpLeaveDlg(FrmEmpLeave FrmParent, string LeaveCode, string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mLeaveCode = LeaveCode;
            mYr = Yr;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        
                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position", 
                        "Department",

                        //6-10
                        "Site",
                        "Resign Date",
                        "Permanent"+Environment.NewLine+"Date", 
                        "Remaining"+Environment.NewLine+"leave (day)",
                        "Duration" + Environment.NewLine + "(Hour)",
                        
                        //11-12
                        "Include"+Environment.NewLine+"Break Time",
                        "Break Schedule"
                    },
                     new int[] 
                    {
                        //0
                        40, 

                        //1-5
                        80, 200, 100, 180, 180,  
                        
                        //6-10
                        180, 100, 100, 100, 100, 

                        //11-12
                        150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 11, 12 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowEmployee()
        {
            Sm.ClearGrd(Grd1, false);

            if (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            var SQL = new StringBuilder();

            bool
                IsFilterBySiteCode = Sm.GetLue(LueSiteCode).Length > 0;

            if (Sm.CompareStr(mLeaveCode, mFrmParent.mAnnualLeaveCode) ||
                Sm.CompareStr(mLeaveCode, mFrmParent.mLongServiceLeaveCode))
            {
                SQL.AppendLine("Select T1.EmpCode, T1.EmpCodeOld, T1.EmpName, T1.PosName, ");
                SQL.AppendLine("T1.JoinDt, T1.ResignDt, 0.00 As NoOfDay, T1.DeptCode, T1.DeptName ");
                SQL.AppendLine(" , T1.SiteCode, T1.SiteName  ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.DeptCode, B.PosName, ");
                SQL.AppendLine("    Case When IfNull(E.ParValue, '')='Y' Then ");
                SQL.AppendLine("        IfNull(A.LeaveStartDt, A.JoinDt) ");
                SQL.AppendLine("    Else A.JoinDt End As JoinDt, ");
                SQL.AppendLine("    A.ResignDt, C.DeptName, A.SiteCode, D.SiteName  ");
                SQL.AppendLine("    From TblEmployee A  ");
                SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                SQL.AppendLine("    Left Join TblSite D On A.SiteCode=D.SiteCode ");
                SQL.AppendLine("    Left Join TblParameter E On E.ParCode='IsAnnualLeaveUseStartDt' ");
                SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                SQL.AppendLine("    And A.EmpCode Not In ( ");
                SQL.AppendLine("        Select EmpCode From TblEmpPd ");
                SQL.AppendLine("        Where JobTransfer = 'P' And PosCodeOld=@TrainingPosCode  ");
                SQL.AppendLine("        )  ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                if (mFrmParent.mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(") T1  ");
            }
            else
            {
                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, A.JoinDt, A.ResignDt, ");
                SQL.AppendLine("    If(@leaveCode = (Select ParValue From Tblparameter Where ParCode = 'LongServiceLeaveCode'), ");
                SQL.AppendLine("    0.00, G.NoOfDay) As NoOfDay, A.DeptCode, C.DeptName, ");
                SQL.AppendLine("    A.SiteCode, D.SiteName  ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                SQL.AppendLine("    Left Join TblSite D On A.SiteCode=D.SiteCode "); 
                SQL.AppendLine("    Left Join TblLeave G On LeaveCode=@LeaveCode ");
                SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(") T1 ");
            }
            var cm = new MySqlCommand();

            string Filter = "";
          
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T1.EmpName", "T1.EmpCodeOld" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T1.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T1.SiteCode", true);
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetParameter("TrainingPosCode"));
            Sm.CmParam<String>(ref cm, "@LeaveCode", mLeaveCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString()+ Filter + " Order By T1.EmpName;",
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName", 
                    
                    //6-8
                    "ResignDt", "JoinDt", "NoOfDay"
        
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = false;
                    Grd.Cells[Row, 12].Value = string.Empty;
                }, true, false, false, false
            );
            ComputeDurationHour();
            ComputeLeaveRemainingDay();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        override protected void ShowData()
        {
            try
            {
                ShowEmployee();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtEmpCodeOld.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtPosCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                Sm.SetDte(mFrmParent.DteJoinDt, Sm.GetGrdDate(Grd1, Row, 8).Substring(0, 8));
                if (Sm.GetGrdDate(Grd1, Row, 7).Length>=8)
                    Sm.SetDte(mFrmParent.DteResignDt, Sm.GetGrdDate(Grd1, Row, 7).Substring(0, 8));
                mFrmParent.TxtLeaved.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.ComputeDurationDay();
                mFrmParent.ComputeDurationHour();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void ComputeDurationHour()
        {
            var DurationHour = 0m;

            if (Sm.GetDte(mFrmParent.DteStartDt).ToString().Length > 0 &&
                Sm.GetTme(mFrmParent.TmeStartTm).ToString().Length > 0 &&
                Sm.GetTme(mFrmParent.TmeEndTm).ToString().Length > 0)
            {
                string
                StartDt = Sm.GetDte(mFrmParent.DteStartDt).Substring(0, 8),
                StartTm = Sm.GetTme(mFrmParent.TmeStartTm),
                EndTm = Sm.GetTme(mFrmParent.TmeEndTm);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(StartTm.Substring(0, 2)),
                    Int32.Parse(StartTm.Substring(2, 2)),
                    0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(EndTm.Substring(0, 2)),
                    Int32.Parse(EndTm.Substring(2, 2)),
                    0
                    );

                if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                double TotalHours = (Dt2 - Dt1).TotalHours;

                if (TotalHours - (int)(TotalHours) > 0.5)
                    DurationHour = (int)(TotalHours) + 1;
                else
                {
                    if (TotalHours - (int)(TotalHours) == 0)
                        DurationHour = (decimal)TotalHours;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) > 0 &&
                            TotalHours - (int)(TotalHours) <= 0.5)
                            DurationHour = (decimal)((int)(TotalHours) + 0.5);
                    }
                }
            }
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Grd1.Cells[Row, 10].Value = DurationHour - (Sm.GetGrdBool(Grd1, Row, 11) ? 1 : 0);
            }
        }

        private void ComputeLeaveRemainingDay()
        {
            var LeaveCode = mLeaveCode;

            if (LeaveCode.Length == 0 ||
                !(
                Sm.CompareStr(mAnnualLeaveCode, LeaveCode) || 
                Sm.CompareStr(mLongServiceLeaveCode, LeaveCode) ||
                Sm.CompareStr(mLongServiceLeaveCode2, LeaveCode)
                ))
                return;

            var l = new List<EmployeeLeave>();
            string
                Dt1 = Sm.GetDte(mFrmParent.DteStartDt),
                Dt2 = Sm.GetDte(mFrmParent.DteEndDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    l.Add(new EmployeeLeave
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 1),
                        EmpName = Sm.GetGrdStr(Grd1, r, 2),
                        LeaveStartDt = Sm.Left(Dt1, 8),
                        LeaveEndDt = Sm.Left(Dt2, 8),
                        JoinDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 8), 8),
                        Period = string.Empty,
                        IsValid = false
                    });
                }
            }

            if (l.Count <= 0) return;

            var RLPType = Sm.GetParameter("RLPType");

            if (RLPType == "1" || RLPType == "3")
            {
                if (Dt2.Length > 0 && !Sm.CompareStr(Sm.Left(Dt1, 4), Sm.Left(Dt2, 4)))
                {
                    return;
                }
                for (int i = 0; i < l.Count; i++)
                    l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
            }

            if (RLPType == "2")
            {
                if (Dt2.Length > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (Sm.CompareDtTm(Sm.Right(l[i].LeaveStartDt, 4), Sm.Right(l[i].JoinDt, 4)) < 0)
                            l[i].Period = (int.Parse(Sm.Left(l[i].LeaveStartDt, 4)) - 1).ToString();
                        else
                            l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
                        var v = (int.Parse(l[i].Period) + 1).ToString() + Sm.Right(l[i].JoinDt, 4);
                        if (Sm.CompareDtTm(l[i].LeaveEndDt, v) < 0)
                            l[i].IsValid = true;
                        else
                            return;
                    }
                }
            }

            string Period2 = Sm.Left(Dt1, 4);
            string Period1 = (int.Parse(Period2) - 1).ToString();

            var l2 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period2)
                {
                    l2.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }

            if (LeaveCode == mAnnualLeaveCode)
                Sm.GetEmpAnnualLeave(ref l2, Period2);
            
            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l2, Period2);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l2, Period2);
            
            var l3 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period1)
                {
                    l3.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }
         
            if (LeaveCode == mAnnualLeaveCode)
                 Sm.GetEmpAnnualLeave(ref l3, Period1);
            
            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l3, Period1);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l3, Period1);
            

            var LeaveDuration = decimal.Parse(mFrmParent.TxtDurationDay.Text);

            if (l2.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.CompareStr(l2[i].EmpCode, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            string EndDate = "0";
                            if (l2[i].EndDt.Length > 0)
                                EndDate = l2[i].EndDt;
                            if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                                Grd1.Cells[r, 9].Value = l2[i].RemainingDay;
                            else
                                Grd1.Cells[r, 9].Value = 0m;
                            break;
                        }
                    }
                }
            }

            if (l3.Count > 0)
            {
                for (int i = 0; i < l3.Count; i++)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.CompareStr(l3[i].EmpCode, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            string EndDate = "0";
                            if (l3[i].EndDt.Length > 0)
                                EndDate = l3[i].EndDt;
                            if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                                Grd1.Cells[r, 9].Value = l3[i].RemainingDay;
                            else
                                Grd1.Cells[r, 9].Value = 0m;
                            break;
                        }
                    }
                }
            }
        }

        private void GetParameter()
        {
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mTrainingPosCode = Sm.GetParameter("TrainingPosCode");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion
       
        #endregion

        #region Class

        private class EmpLeaveDtl
        {
            public string DNo { get; set; }
            public string LeaveDt { get; set; }
        }

        private class Holiday
        {
            public string HolidayDt { get; set; }
        }

        private class EmployeeLeave
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string LeaveStartDt { get; set; }
            public string LeaveEndDt { get; set; }
            public string JoinDt { get; set; }
            public string Period { get; set; }
            public bool IsValid { get; set; }
        }

        #endregion
    }
}
