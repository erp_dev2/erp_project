﻿namespace RunSystem
{
    partial class FrmPropertyInventoryFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkPropertyCategory = new DevExpress.XtraEditors.CheckEdit();
            this.LblPropertyCategory = new System.Windows.Forms.Label();
            this.LuePropertyCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPropertyInventoryCode = new System.Windows.Forms.Label();
            this.TxtPropertyInventoryCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkPropertyInventoryCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyInventoryCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LblPropertyInventoryCode);
            this.panel2.Controls.Add(this.TxtPropertyInventoryCode);
            this.panel2.Controls.Add(this.ChkPropertyInventoryCode);
            this.panel2.Controls.Add(this.ChkPropertyCategory);
            this.panel2.Controls.Add(this.LblPropertyCategory);
            this.panel2.Controls.Add(this.LuePropertyCategory);
            this.panel2.Size = new System.Drawing.Size(672, 70);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 403);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkPropertyCategory
            // 
            this.ChkPropertyCategory.Location = new System.Drawing.Point(350, 36);
            this.ChkPropertyCategory.Name = "ChkPropertyCategory";
            this.ChkPropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.ChkPropertyCategory.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPropertyCategory.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPropertyCategory.Properties.Caption = " ";
            this.ChkPropertyCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPropertyCategory.Size = new System.Drawing.Size(20, 22);
            this.ChkPropertyCategory.TabIndex = 20;
            this.ChkPropertyCategory.ToolTip = "Remove filter";
            this.ChkPropertyCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPropertyCategory.ToolTipTitle = "Run System";
            this.ChkPropertyCategory.CheckedChanged += new System.EventHandler(this.ChkPropertyCategoryCode_CheckedChanged);
            this.ChkPropertyCategory.Validated += new System.EventHandler(this.ChkPropertyCategoryCode_CheckedChanged);
            // 
            // LblPropertyCategory
            // 
            this.LblPropertyCategory.AutoSize = true;
            this.LblPropertyCategory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyCategory.ForeColor = System.Drawing.Color.Black;
            this.LblPropertyCategory.Location = new System.Drawing.Point(6, 39);
            this.LblPropertyCategory.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyCategory.Name = "LblPropertyCategory";
            this.LblPropertyCategory.Size = new System.Drawing.Size(107, 14);
            this.LblPropertyCategory.TabIndex = 18;
            this.LblPropertyCategory.Text = "Property Category";
            this.LblPropertyCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePropertyCategory
            // 
            this.LuePropertyCategory.EnterMoveNextControl = true;
            this.LuePropertyCategory.Location = new System.Drawing.Point(117, 37);
            this.LuePropertyCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LuePropertyCategory.Name = "LuePropertyCategory";
            this.LuePropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropertyCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropertyCategory.Properties.DropDownRows = 30;
            this.LuePropertyCategory.Properties.NullText = "[Empty]";
            this.LuePropertyCategory.Properties.PopupWidth = 250;
            this.LuePropertyCategory.Size = new System.Drawing.Size(229, 20);
            this.LuePropertyCategory.TabIndex = 19;
            this.LuePropertyCategory.ToolTip = "F4 : Show/hide list";
            this.LuePropertyCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropertyCategory.EditValueChanged += new System.EventHandler(this.LuePropertyCategory_EditValueChanged);
            // 
            // LblPropertyInventoryCode
            // 
            this.LblPropertyInventoryCode.AutoSize = true;
            this.LblPropertyInventoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyInventoryCode.Location = new System.Drawing.Point(4, 18);
            this.LblPropertyInventoryCode.Name = "LblPropertyInventoryCode";
            this.LblPropertyInventoryCode.Size = new System.Drawing.Size(111, 14);
            this.LblPropertyInventoryCode.TabIndex = 21;
            this.LblPropertyInventoryCode.Text = "Property Inventory";
            // 
            // TxtPropertyInventoryCode
            // 
            this.TxtPropertyInventoryCode.EnterMoveNextControl = true;
            this.TxtPropertyInventoryCode.Location = new System.Drawing.Point(117, 16);
            this.TxtPropertyInventoryCode.Name = "TxtPropertyInventoryCode";
            this.TxtPropertyInventoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyInventoryCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyInventoryCode.Properties.MaxLength = 250;
            this.TxtPropertyInventoryCode.Size = new System.Drawing.Size(229, 20);
            this.TxtPropertyInventoryCode.TabIndex = 22;
            this.TxtPropertyInventoryCode.Validated += new System.EventHandler(this.TxtPropertyInventoryCode_Validated);
            // 
            // ChkPropertyInventoryCode
            // 
            this.ChkPropertyInventoryCode.Location = new System.Drawing.Point(350, 16);
            this.ChkPropertyInventoryCode.Name = "ChkPropertyInventoryCode";
            this.ChkPropertyInventoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPropertyInventoryCode.Properties.Appearance.Options.UseFont = true;
            this.ChkPropertyInventoryCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPropertyInventoryCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPropertyInventoryCode.Properties.Caption = " ";
            this.ChkPropertyInventoryCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPropertyInventoryCode.Size = new System.Drawing.Size(20, 22);
            this.ChkPropertyInventoryCode.TabIndex = 23;
            this.ChkPropertyInventoryCode.ToolTip = "Remove filter";
            this.ChkPropertyInventoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPropertyInventoryCode.ToolTipTitle = "Run System";
            this.ChkPropertyInventoryCode.CheckedChanged += new System.EventHandler(this.ChkPropertyInventoryCode_CheckedChanged);
            // 
            // FrmPropertyInventoryFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmPropertyInventoryFind";
            this.Text = "List of Property Inventory";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyInventoryCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkPropertyCategory;
        private System.Windows.Forms.Label LblPropertyCategory;
        private DevExpress.XtraEditors.LookUpEdit LuePropertyCategory;
        private System.Windows.Forms.Label LblPropertyInventoryCode;
        private DevExpress.XtraEditors.TextEdit TxtPropertyInventoryCode;
        private DevExpress.XtraEditors.CheckEdit ChkPropertyInventoryCode;
    }
}