﻿#region update
/*
    08/04/2020 [WED/SRN] new apps, Qt pakai discount rate 5 biji
*/

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmQt3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mVdCode = string.Empty, mItCode = string.Empty, mPtCode = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mItCtExim = string.Empty;
        private string 
            mMainCurCode = string.Empty,
            mPtCodeForQt = string.Empty;
        private bool mIsNeedApproval = false;
        internal bool 
            mIsShowForeignName = false,
            mIsGroupPaymentTermActived = false,
            mIsQTValidateByItemCategory = false,
            mIsQTAllowToUploadFile = false,
            mIsBOMShowSpecifications = false;
        internal FrmQt3Find FrmFind;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmQt3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Vendor's Quotation with Discount Rate Level";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueDTCode(ref LueDTCode);
                Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                base.FrmLoad(sender, e);

                if (mItCode.Length != 0) InsertData();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Item's Code",
                    "",
                    "Local Code", 
                    "Item's Name",
                    "Foreign Name",

                    //6-10
                    "Active",
                    "UoM",
                    "Latest Price"+Environment.NewLine+"(Based On Vendor"+Environment.NewLine+"And Term of Payment)",
                    "Latest"+Environment.NewLine+"Quotation Date",
                    "Unit Price"+Environment.NewLine+"After Discount",

                    //11-15
                    "Remark", 
                    "Specification",
                    "Unit Price"+Environment.NewLine+"Before Discount",
                    "Discount (%)"+Environment.NewLine+"(1)",
                    "Discount (%)"+Environment.NewLine+"(2)",

                    //16-18
                    "Discount (%)"+Environment.NewLine+"(3)",
                    "Discount (%)"+Environment.NewLine+"(4)",
                    "Discount (%)"+Environment.NewLine+"(5)"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    80, 20, 200, 130, 230,

                    //6-10
                    50, 80, 150, 100, 100,
                    
                    //11-15
                    300, 300, 120, 100, 100, 

                    //16-18
                    100, 100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 13, 14, 15, 16, 17, 18 }, 0);
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 9 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 9 }, false);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            Grd1.Cols[12].Move(8);
            Grd1.Cols[18].Move(10);
            Grd1.Cols[17].Move(10);
            Grd1.Cols[16].Move(10);
            Grd1.Cols[15].Move(10);
            Grd1.Cols[14].Move(10);
            Grd1.Cols[13].Move(10);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueVdCode, TxtStatus, LuePtCode, LueCurCode, LueDTCode, DteExpDt, MeeRemark,
                        TxtFile1, TxtFile2, TxtFile3, TxtFile4, TxtFile5
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
                    BtnUpload1.Enabled = BtnUpload2.Enabled = BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled = false;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled = true;
                    if (mIsQTAllowToUploadFile)
                    {
                        PnlUpload.Visible = true;
                    }
                    else
                    {
                        PnlUpload.Visible = false;
                    }
                    TxtDocNo.Focus();
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled =  false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueVdCode, LuePtCode, LueCurCode, LueDTCode, DteExpDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 11, 13, 14, 15, 16, 17, 18 });
                    BtnUpload1.Enabled = BtnUpload2.Enabled =  BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled = true;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled = false;
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled =  true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    BtnUpload1.Enabled = BtnUpload2.Enabled = BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled = true;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled = false;
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled = true;
                    BtnUpload1.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueVdCode, LuePtCode, LueCurCode, TxtStatus,
                LueDTCode, DteExpDt, MeeRemark, TxtFile1, TxtFile2, TxtFile3, TxtFile4, TxtFile5
            });
            ClearGrd();
            ChkFile1.Checked = ChkFile2.Checked = ChkFile3.Checked = ChkFile4.Checked = ChkFile5.Checked = false;
            PbUpload1.Value = PbUpload2.Value = PbUpload3.Value = PbUpload4.Value = PbUpload5.Value = 0;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10, 13, 14, 15, 16, 17, 18 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmQt3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                string DocNo = string.Empty;
                if (TxtDocNo.Text.Length > 0)
                {
                    DocNo = TxtDocNo.Text;
                    if (mIsQTAllowToUploadFile)
                    {
                        if (TxtFile1.Text.Length > 1 && ChkFile1.Checked == true)
                            UploadFile(DocNo, TxtFile1, PbUpload1, "1");
                        if (TxtFile2.Text.Length > 1 && ChkFile2.Checked == true)
                            UploadFile(DocNo, TxtFile2, PbUpload2, "2");
                        if (TxtFile3.Text.Length > 1 && ChkFile3.Checked == true)
                            UploadFile(DocNo, TxtFile3, PbUpload3, "3");
                        if (TxtFile4.Text.Length > 1 && ChkFile4.Checked == true)
                            UploadFile(DocNo, TxtFile4, PbUpload4, "4");
                        if (TxtFile5.Text.Length > 1 && ChkFile5.Checked == true)
                            UploadFile(DocNo, TxtFile5, PbUpload5, "5");
                    }
                    ShowData(DocNo);
                }

                else
                {
                    if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                        IsInsertedDataNotValid())
                        return;

                    Cursor.Current = Cursors.WaitCursor;

                    DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Qt3", "TblQtHdr");

                    var cml = new List<MySqlCommand>();

                    cml.Add(SaveQtHdr(DocNo));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveQtDtl(DocNo, Row));

                    Sm.ExecCommands(cml);

                    if (mIsQTAllowToUploadFile)
                    {
                        if (TxtFile1.Text.Length > 1)
                            UploadFile(DocNo, TxtFile1, PbUpload1, "1");
                        if (TxtFile2.Text.Length > 1)
                            UploadFile(DocNo, TxtFile2, PbUpload2, "2");
                        if (TxtFile3.Text.Length > 1)
                            UploadFile(DocNo, TxtFile3, PbUpload3, "3");
                        if (TxtFile4.Text.Length > 1)
                            UploadFile(DocNo, TxtFile4, PbUpload4, "4");
                        if (TxtFile5.Text.Length > 1)
                            UploadFile(DocNo, TxtFile5, PbUpload5, "5");
                    }
                    ShowData(DocNo);
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (mIsQTAllowToUploadFile)
            {
                SetFormControl(mState.Edit);
            }
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        #endregion

        #region Save Data

        #region Insert Data

        public void InsertData()
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            if (mMainCurCode.Length > 0)
            Sm.SetLue(LueCurCode, mMainCurCode);
            Sl.SetLuePtCode(ref LuePtCode, string.Empty);
            if (!mIsGroupPaymentTermActived && mPtCodeForQt.Length>0)
                Sm.SetLue(LuePtCode, mPtCodeForQt);

            TxtStatus.EditValue = "Outstanding";

            if (mItCode.Length != 0)
            {
                if (mVdCode.Length != 0)
                {
                    Sm.SetLue(LueVdCode, mVdCode);
                    ShowItemInfo();
                }
                else
                    ShowItemInfo2();
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDocDtNotValid() ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsLueEmpty(LuePtCode, "Term of Payment") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords()||
                IsCategoryDifferent() ||
                IsQtNotValid();
        }

        private bool IsQtNotValid()
        {
            if (!mIsNeedApproval) return false;
            
            var VdCode = Sm.GetLue(LueVdCode);
            var PtCode = Sm.GetLue(LuePtCode);
            var ItCode = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                ItCode = Sm.GetGrdStr(Grd1, Row, 1);
                if (ItCode.Length > 0 && IsQtNotValid(VdCode, PtCode, ItCode))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Item Code : " + ItCode + Environment.NewLine +
                        "Local Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Foreign Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "This item has outstanding approval.");
                    return true;
                }
            }
            return false;
        }

        private bool IsQtNotValid(string VdCode, string PtCode, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From TblQtHdr A, TblQtDtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.VdCode=@VdCode ");
            SQL.AppendLine("And A.PtCode=@PtCode ");
            SQL.AppendLine("And B.ItCode=@ItCode ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And B.ActInd='Y' Limit 1;");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@PtCode", PtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            return Sm.IsDataExist(cm);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {

                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 13, true, "Unit price before discount is empty.")) return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsDocDtNotValid()
        {
            var InValid = true;
            var QtDtPlus1Ind = Sm.GetParameter("QtDtPlus1Ind");
            var CurrentDt = Sm.Left(Sm.ServerCurrentDateTime(), 8);
            var DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (QtDtPlus1Ind == "Y" &&
                IsCheckingDocDtMandatory() &&
                decimal.Parse(DocDt) <= decimal.Parse(CurrentDt))
            {
                Sm.StdMsg(mMsgType.Warning, "Document date is not valid.");
                DteDocDt.Focus();
                return InValid;
            }
            return !InValid;
        }

        private bool IsCheckingDocDtMandatory()
        {
            var ItCode = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        ItCode += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            ItCode = (ItCode.Length == 0 ? "##XXX##" : ItCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode From TblItem ");
            SQL.AppendLine("Where Position(Concat('##', ItCode, '##') In @SelectedItem)>0 ");
            SQL.AppendLine("And Position(Concat('##', ItCtCode, '##') In @SelectedItCtCode)>0 ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SelectedItem", ItCode);
            Sm.CmParam<String>(ref cm, "@SelectedItCtCode", Sm.GetParameter("QtDtPlus1ItCtCode"));

            if (Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private bool IsCategoryDifferent()
        {
            var ItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode=@Param;", Sm.GetGrdStr(Grd1, 0, 1));
            var ItCode = string.Empty;
            if (mIsQTValidateByItemCategory)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (ItCode.Length > 0 &&
                        !Sm.CompareStr(ItCtCode, Sm.GetValue("Select ItCtCode From TblItem Where ItCode=@Param;", ItCode))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "All item should have the same category.");
                        return true;
                    }
                }
            }
          
            return false;
        }

        private bool IsQtForExim()
        {
            return mItCtExim.Length > 0 && 
            Sm.IsDataExist(
                "Select 1 From TblItem " +
                "Where ItCode=@Param " +
                "And Find_In_Set( " +
                "IfNull(ItCtCode, ''), " +
                "IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') " +
                ") ", Sm.GetGrdStr(Grd1, 0, 1));
        }

        private MySqlCommand SaveQtHdr(string DocNo)
        {
            bool IsExim = IsQtForExim();
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblQtHdr(DocNo, DocDt, DocType, SystemNo, VdCode, Status, PtCode, CurCode, DTCode, ExpiredDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 2, Concat(Left(@DocDt, 6), Left(@DocNo, 4)), @VdCode, @Status, @PtCode, @CurCode, @DTCode, @ExpiredDt, @Remark, @UserCode, CurrentDateTime()); ");

            if (mIsNeedApproval && IsExim)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='QT'; ");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", mIsNeedApproval && IsExim ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@DTCode", Sm.GetLue(LueDTCode));
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetDte(DteExpDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveQtDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Update TblQtDtl As T1  ");
            //SQL.AppendLine("Inner Join TblQtHdr T2 On T1.DocNo=T2.DocNo And T2.VdCode=@VdCode And T2.PtCode=@PtCode ");
            //SQL.AppendLine("Set T1.ActInd='N', T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where T1.ItCode=@ItCode And T1.ActInd='Y'; ");

            SQL.AppendLine("Update TblQtDtl T1  ");
            SQL.AppendLine("Inner Join TblQtHdr T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Status='A' ");
            SQL.AppendLine("    And T2.VdCode=@VdCode ");
            SQL.AppendLine("    And T2.PtCode=@PtCode ");
            SQL.AppendLine("Set T1.ActInd='N', T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where T1.ItCode=@ItCode ");
            SQL.AppendLine("And T1.ActInd='Y' ");
            SQL.AppendLine("And Exists(");
            SQL.AppendLine("    Select DocNo From TblQtHdr Where DocNo=@DocNo And Status='A' ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblQtDtl(DocNo, DNo, ItCode, ActInd, UPriceBefDisc, UPrice, DiscRt1, DiscRt2, DiscRt3, DiscRt4, DiscRt5, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, @DNo, @ItCode, 'Y', @UPriceBefDisc, @UPrice, @DiscRt1, @DiscRt2, @DiscRt3, @DiscRt4, @DiscRt5, @Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblQtHdr Where DocNo=@DocNo;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefDisc", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt1", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt2", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt3", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt4", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt5", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowQtHdr(DocNo);
                ShowQtDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowQtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, if(Status='A', 'Approved', 'Outstanding') As Status, "+
                    "VdCode, PtCode, ExpiredDt, CurCode, DTCode, FileName1, FileName2, FileName3, "+
                    "FileName4,  FileName5, Remark " +
                    "From TblQtHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Status", "VdCode", "PtCode", "CurCode",  
                        "DTCode", "ExpiredDt", "FileName1", "FileName2", "FileName3", 
                        "FileName4", "FileName5", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueVdCode2(ref LueVdCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[3]));
                        Sl.SetLuePtCode(ref LuePtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueDTCode, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteExpDt, Sm.DrStr(dr, c[7])); 
                        TxtFile1.EditValue = Sm.DrStr(dr, c[8]);
                        if (TxtFile1.Text.Length>0)
                            ChkFile1.Checked = false;
                        TxtFile2.EditValue = Sm.DrStr(dr, c[9]);
                        if (TxtFile2.Text.Length > 0)
                            ChkFile2.Checked = false;
                        TxtFile3.EditValue = Sm.DrStr(dr, c[10]);
                        if (TxtFile3.Text.Length > 0)
                            ChkFile3.Checked = false;
                        TxtFile4.EditValue = Sm.DrStr(dr, c[11]);
                        if (TxtFile4.Text.Length > 0)
                            ChkFile4.Checked = false;
                        TxtFile5.EditValue = Sm.DrStr(dr, c[12]);
                        if (TxtFile5.Text.Length > 0)
                            ChkFile5.Checked = false;
                        MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowQtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, A.ActInd, B.PurchaseUomCode, A.UPrice, A.Remark, B.Specification, ");
            SQL.AppendLine("A.UPriceBefDisc, A.DiscRt1, A.DiscRt2, A.DiscRt3, A.DiscRt4, A.DiscRt5 ");
            SQL.AppendLine("From TblQtDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItCodeInternal", "ItName", "ForeignName", "ActInd", "PurchaseUomCode",
                    "UPrice", "Remark", "Specification", "UPriceBefDisc", "DiscRt1", 
                    "DiscRt2", "DiscRt3", "DiscRt4", "DiscRt5"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                    Grd1.Cells[Row, 8].Value = 0m;
                    Grd1.Cells[Row, 9].Value = null;
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 14);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 13, 14, 15, 16, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 13, 14, 15, 16, 17, 18 }, e.ColIndex))
            {
                ComputeDiscountRateLevel(e.RowIndex);
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && 
                    !Sm.IsLueEmpty(LueVdCode, "Vendor") && 
                    !Sm.IsLueEmpty(LuePtCode, "Term of payment"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmQt3Dlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LuePtCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 11, 13, 14, 15, 16, 17, 18 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 13, 14, 15, 16, 17, 18 });
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 &&
                !Sm.IsLueEmpty(LueVdCode, "Vendor") &&
                !Sm.IsLueEmpty(LuePtCode, "Term of payment"))
                Sm.FormShowDialog(new FrmQt3Dlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LuePtCode)));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Additional Method

        private void ComputeDiscountRateLevel(int Row)
        {
            decimal UPrice = Sm.GetGrdDec(Grd1, Row, 13),
                DiscRt1 = Sm.GetGrdDec(Grd1, Row, 14),
                DiscRt2 = Sm.GetGrdDec(Grd1, Row, 15),
                DiscRt3 = Sm.GetGrdDec(Grd1, Row, 16),
                DiscRt4 = Sm.GetGrdDec(Grd1, Row, 17),
                DiscRt5 = Sm.GetGrdDec(Grd1, Row, 18);

            decimal UPriceAfterDiscount = UPrice;

            decimal[] Discs = { DiscRt1, DiscRt2, DiscRt3, DiscRt4, DiscRt5 };

            foreach (var x in Discs.Where(y => y != 0m))
            {
                UPriceAfterDiscount -= (x / 100m) * UPriceAfterDiscount;
            }

            Grd1.Cells[Row, 10].Value = UPriceAfterDiscount;
        }

        private void GetParameter()
        {
            mItCtExim = Sm.GetParameter("ItCtExim");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsNeedApproval = Sm.IsDataExist(new MySqlCommand()
            { CommandText = "Select DocType From TblDocApprovalSetting Where DocType='QT' Limit 1;" });
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mPtCodeForQt = Sm.GetParameter("PtCodeForQt");
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mIsQTValidateByItemCategory = Sm.GetParameterBoo("IsQTValidateByItemCategory");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsQTAllowToUploadFile = Sm.GetParameterBoo("IsQTAllowToUploadFile");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ShowItemInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.ItCode, T1.ItCodeInternal, T1.ItName, T1.ForeignName, T1.PurchaseUomCode, ");
            SQL.AppendLine("IfNull(T2.UPrice, 0) As UPrice, T2.DocDt ");
            SQL.AppendLine("From TblItem T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2b.ItCode, T2a.DocDt, T2b.UPrice  ");
            SQL.AppendLine("    From TblQtHdr T2a ");
            SQL.AppendLine("    Inner Join TblQtDtl T2b On T2a.DocNo=T2b.DocNo And T2b.ActInd='Y' ");
            SQL.AppendLine("    Where T2a.VdCode=@VdCode And T2a.PtCode=@PtCode And T2a.Status='A' ");
            SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode  ");
            SQL.AppendLine("Where T1.ItCode=@ItCode");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", mItCode);
            Sm.CmParam<String>(ref cm, "@VdCode", (mVdCode.Length!=0?mVdCode:Sm.GetLue(LueVdCode)));
            Sm.CmParam<String>(ref cm, "@PtCode", (mPtCode.Length != 0 ? mPtCode : Sm.GetLue(LuePtCode)));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "ForeignName", "PurchaseUomCode", "UPrice",
                    
                    //6
                    "DocDt" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Grd1.Cells[Row, 6].Value = false; 
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 6);
                    Grd1.Cells[Row, 10].Value = 0m;
                    Grd1.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowItemInfo2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", mItCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select ItCode, ItCodeInternal, ItName, ForeignName, PurchaseUomCode " +
                "From TblItem Where ItCode=@ItCode",
                new string[] 
                { 
                    "ItCode", 
                    "ItCodeInternal", "ItName", "ForeignName", "PurchaseUomCode" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Grd1.Cells[Row, 6].Value = false; 
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 3);
                    Grd1.Cells[Row, 8].Value = Grd1.Cells[Row, 10].Value = 0m;
                    Grd1.Cells[Row, 9].Value = Grd1.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region ftp


        private bool IsUploadFileNotValid2()
        {
            return
                IsFileSizeNotvalid(TxtFile1) ||
                IsFileSizeNotvalid(TxtFile2) 
             ;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsUploadFileNotValid2()
                ;
            ;
        }


        private bool IsFTPClientDataNotValid()
        {

            if (mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(DevExpress.XtraEditors.TextEdit TxtFile)
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(DevExpress.XtraEditors.TextEdit TxtFile)
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblQtHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }


        private void AddFile(DevExpress.XtraEditors.TextEdit TxtFile, DevExpress.XtraEditors.CheckEdit ChkFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD1.InitialDirectory = "c:";
                OD1.Filter = "PDF files (*.pdf)|*.pdf";
                OD1.FilterIndex = 2;
                OD1.ShowDialog();

                TxtFile.Text = OD1.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void UploadFile(string DocNo, DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload, string Code)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

            if (mFormatFTPClient == "1")
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);

                    PbUpload.Invoke(
                        (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                    byte[] buffers = new byte[10240];
                    int read;
                    while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                    {
                        ftpStream.Write(buffers, 0, read);
                        PbUpload.Invoke(
                            (MethodInvoker)delegate
                            {
                                PbUpload.Value = (int)file.Position;
                            });
                    }
                }
                while (bytesRead != 0);

                //Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

                file.Close();
                ftpStream.Close();
            }
            else
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);

                    PbUpload.Invoke(
                        (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                    byte[] buffers = new byte[10240];
                    int read;
                    while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                    {
                        ftpStream.Write(buffers, 0, read);
                        PbUpload.Invoke(
                            (MethodInvoker)delegate
                            {
                                PbUpload.Value = (int)file.Position;
                            });
                    }
                }
                while (bytesRead != 0);

                //Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

                file.Close();
                ftpStream.Close();
            }

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAttachmentFile(DocNo, toUpload.Name, Code));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateAttachmentFile(string DocNo, string FileName, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblQtHdr Set ");
            SQL.AppendLine("    FileName" + Code + "=@FileName, LastUpBy=@UserCode, lastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }


        private void DownloadFileKu(DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, PbUpload);
            SFD1.FileName = TxtFile.Text;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, System.Windows.Forms.ProgressBar PbUpload)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = "Connecting...";
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion 

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<VdQt>();
            var l1 = new List<VdQt1>();
            var ldtl = new List<VdQtDtl>();

            string[] TableName = { "VdQt", "VdQtDtl", "VdQt1" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, Date_Format(A.ExpiredDt,'%d %M %Y')As ExpiredDt, B.VdName, B.Address, C.ContactPersonName, ");
            SQL.AppendLine("Round(D.PtDay,0)As PtDay, E.DtName, A.Remark, A.Curcode ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join tblvendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblVendorContactPerson C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblPaymentterm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join TblDeliveryType E On A.DtCode=E.DtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "ExpiredDt",
                         "VdName",
                         "Address",
                         //11-15
                         "ContactPersonName",
                         "PtDay",
                         "DtName",
                         "Remark",
                         "Curcode"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VdQt()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            ExpiredDt = Sm.DrStr(dr, c[8]),
                            VdName = Sm.DrStr(dr, c[9]),
                            Address = Sm.DrStr(dr, c[10]),
                            ContactPersonName = Sm.DrStr(dr, c[11]),
                            PtDay = Sm.DrStr(dr, c[12]),
                            DtName = Sm.DrStr(dr, c[13]),
                            HRemark = Sm.DrStr(dr, c[14]),
                            Curcode = Sm.DrStr(dr, c[15]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, B.PurchaseUomCode, A.UPrice, A.Remark  ");
                SQLDtl.AppendLine("From TblQtDtl A  ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("Order By A.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-4
                         "ItName" ,
                         "PurchaseUomCode" ,
                         "UPrice" ,
                         "Remark",
                        
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new VdQtDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),

                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[2]),
                            UPrice = Sm.DrDec(drDtl, cDtl[3]),
                            DRemark = Sm.DrStr(drDtl, cDtl[4]),

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Approve1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  C.UserName, ");
            SQL1.AppendLine("Concat(IfNull(D.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, F.PosName, "); 
            SQL1.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner join TblDocApprovalsetting B On A.DocType=B.DocType ");
            SQL1.AppendLine("Inner Join TblUser C On A.UserCode = C.UserCode ");
            SQL1.AppendLine("Left Join TblParameter D On D.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Left Join tblemployee E On A.UserCode=E.UserCode ");
            SQL1.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
            SQL1.AppendLine("Where A.DocType = 'QT' and B.level='1' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1 ");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",
                         //6
                         "LastUpDt"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new VdQt1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                            PosName = Sm.DrStr(dr1, c1[5]),
                            LastUpDt = Sm.DrStr(dr1, c1[6]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

           Sm.PrintReport("VdQt", myLists, TableName, false);
  
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue2(Sl.SetLuePtCode), string.Empty);
                ClearGrd();
                if (mItCode.Length != 0)
                {
                    if (Sm.GetLue(LueVdCode).Length != 0 && Sm.GetLue(LuePtCode).Length != 0)
                        ShowItemInfo();
                    else
                        ShowItemInfo2();
                }
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode2), string.Empty);
                if (mItCode.Length == 0) ClearGrd();
            }
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mItCode.Length != 0)
                {
                    ClearGrd();
                    if (Sm.GetLue(LueVdCode).Length != 0 && Sm.GetLue(LuePtCode).Length != 0)
                        ShowItemInfo();
                    else
                        ShowItemInfo2();
                }
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueDTCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDTCode, new Sm.RefreshLue1(Sl.SetLueDTCode));
        }

        private void BtnUpload1_Click(object sender, EventArgs e)
        {
            //if (TxtFile1.Text.Length < 0)
            AddFile(TxtFile1, ChkFile1);
        }

        private void BtnUpload2_Click(object sender, EventArgs e)
        {
            //if (TxtFile2.Text.Length > 0)
            AddFile(TxtFile2, ChkFile2);
        }

        private void BtnUpload3_Click(object sender, EventArgs e)
        {
            //if (TxtFile3.Text.Length > 0)
            AddFile(TxtFile3, ChkFile3);
        }

        private void BtnUpload4_Click(object sender, EventArgs e)
        {
           // if (TxtFile4.Text.Length > 0)
            AddFile(TxtFile4, ChkFile4);
        }

        private void BtnUpload5_Click(object sender, EventArgs e)
        {
           // if (TxtFile5.Text.Length > 0)
            AddFile(TxtFile5, ChkFile5);
        }
        private void BtnDownload1_Click(object sender, EventArgs e)
        {
            if (TxtFile1.Text.Length > 0)
                DownloadFileKu(TxtFile1, PbUpload1);
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            if (TxtFile2.Text.Length > 0)
                DownloadFileKu(TxtFile2, PbUpload2);
        }



        private void ChkFile1_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile1.Checked == false)
            {
                TxtFile1.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            if (TxtFile3.Text.Length > 0)
                DownloadFileKu(TxtFile3, PbUpload3);
        }

        private void BtnDownload4_Click(object sender, EventArgs e)
        {
            if (TxtFile4.Text.Length > 0)
                DownloadFileKu(TxtFile4, PbUpload4);
        }

        private void BtnDownload5_Click(object sender, EventArgs e)
        {
            if (TxtFile5.Text.Length > 0)
                DownloadFileKu(TxtFile5, PbUpload5);
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void ChkFile4_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile4.Checked == false)
            {
                TxtFile4.EditValue = string.Empty;
            }
        }

        private void ChkFile5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile5.Checked == false)
            {
                TxtFile5.EditValue = string.Empty;
            }
        }

        #endregion

        #endregion

        #region Report Class

        private class VdQt
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ExpiredDt { get; set; }
            public string VdName { get; set; }
            public string Address { get; set; }
            public string ContactPersonName { get; set; }
            public string PtDay { get; set; }
            public string DtName { get; set; }
            public string HRemark { get; set; }
            public string Curcode { get; set; }
            public string PrintBy { get; set; }
        }
      
        private class VdQtDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public string PurchaseUomCode { get; set; }
            public string DRemark { get; set; }
        }

        class VdQt1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        #endregion     
       
    }
}
