﻿#region Update
/*
    21/09/2021 [DITA/AMKA] New Apps
    27/09/2021 [DITA/AMKA] Printout baru
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting4 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private List<COA> lCOA = null;


        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mPrintRptFicoSettingParenthesesFormat = string.Empty,
            mPrintRptFicoSettingCOAParentheses = string.Empty,
            mInitYr = string.Empty,
            mInitMth = string.Empty,
            mInitStartFrom = string.Empty,
            mInitProfitCenter = string.Empty;

        private bool
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsFilterByEnt = false,
            mIsFilterBySite = false,
            mIsPrintRptFicoSettingShowPercentage = false,
            mIsPrintRptFicoSettingYrAscOrder = false,
            mIsPrintRptFicoSettingYr2DisplayMth = false,
            mPrintFicoSettingLastYearBasedOnFilterMonth = false,
            mIsPrintRptFicoSettingUseParentheses = false,
            mIsPrintRptFicoSettingDisplayDate = false,
            //mIsPrintRptFicoSettingUseMultiProfitCenter = false,
            mIsPrintRptFicoSettingUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsPrintRptFicoSettingUseEntity = false,
            mIsPrintRptFicoSettingDisplayPrintDate = false;

        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueFicoFormula(ref LueOptionCode);
                TxtStartYr.EditValue = Sm.GetLue(LueYr);
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                lCOA = new List<COA>();
                SetGrd();
                TxtStartYr.Enabled = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }



        private void GetParameter()
        {

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'AcNoForCurrentEarning', 'AcNoForIncome', 'AcNoForCost', 'COAAssetStartYr', 'COAAssetAcNo', ");
            SQL.AppendLine("'AccountingRptStartFrom', 'PrintRptFicoSettingCOAParentheses', 'PrintRptFicoSettingParenthesesFormat', 'IsEntityMandatory', 'IsCOAAssetUseStartYr', ");
            SQL.AppendLine("'IsReportingFilterByEntity', 'IsFilterByEnt', 'IsFilterBySite', 'IsPrintRptFicoSettingShowPercentage', 'IsPrintRptFicoSettingYr2DisplayMth', ");
            SQL.AppendLine("'IsPrintRptFicoSettingYrAscOrder', 'PrintFicoSettingLastYearBasedOnFilterMonth', 'IsPrintRptFicoSettingUseParentheses', 'IsPrintRptFicoSettingDisplayDate', 'IsPrintRptFicoSettingUseProfitCenter', ");
            SQL.AppendLine("'IsPrintRptFicoSettingUseEntity', 'IsPrintRptFicoSettingDisplayPrintDate' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsFilterByEnt": mIsFilterByEnt = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingShowPercentage": mIsPrintRptFicoSettingShowPercentage = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingYr2DisplayMth": mIsPrintRptFicoSettingYr2DisplayMth = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingYrAscOrder": mIsPrintRptFicoSettingYrAscOrder = ParValue == "Y"; break;
                            case "PrintFicoSettingLastYearBasedOnFilterMonth": mPrintFicoSettingLastYearBasedOnFilterMonth = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseParentheses": mIsPrintRptFicoSettingUseParentheses = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingDisplayDate": mIsPrintRptFicoSettingDisplayDate = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseProfitCenter": mIsPrintRptFicoSettingUseProfitCenter = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseEntity": mIsPrintRptFicoSettingUseEntity = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingDisplayPrintDate": mIsPrintRptFicoSettingDisplayPrintDate = ParValue == "Y"; break;

                            //string
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "PrintRptFicoSettingCOAParentheses": mPrintRptFicoSettingCOAParentheses = ParValue; break;
                            case "PrintRptFicoSettingParenthesesFormat": mPrintRptFicoSettingParenthesesFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Account#",

                        //1-5,
                        "Description", 
                        "Budget Plan", 
                        "Year To Date"+ Environment.NewLine + "This Year",
                        "Year To Date" + Environment.NewLine + "Past Year",
                        "Current Month"+ Environment.NewLine + "This Year", 

                        //6
                        "Current Month"+ Environment.NewLine + "Past Year", 
                        
                    },
                    new int[] 
                    {
                        //0
                        200,

                        //1-5
                        200, 150, 150, 150,150 ,
                        
                        //6
                       150, 
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsPrintRptFicoSettingUseProfitCenter) return;
            //if (!ChkProfitCenterCode.Checked)
            //    mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                //if (ChkProfitCenterCode.Checked)
                //{
                SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                IsCompleted = false;
                //}
                //else
                //    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueOptionCode, "Type") ||
                IsProfitCenterInvalid() ||
                IsFilterDifferent() ||
                IsCOAEmpty()) return;

            Cursor.Current = Cursors.WaitCursor;

            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();
            string mListAcNo = string.Empty, mUsedAcNo = string.Empty;


            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueYr);
            var Yr2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var StartFrom2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var Mth2 = mPrintFicoSettingLastYearBasedOnFilterMonth ? Sm.GetLue(LueMth) : Convert.ToString(12);

            try
            {
                Sm.ClearGrd(Grd1, true);
                //var lCOA = new List<COA>();

                var lEntityCOA = new List<EntityCOA>();
                var lRptFicoSetting = new List<RptFicoSetting>();
                var lRptFicoSetting2 = new List<RptFicoSetting>();
                var lRptFicoSetting3 = new List<COAFicoSetting>();
                var lRptFicoSetting4 = new List<RptFicoSetting2>();


                if (lCOA.Count > 0)
                {
                    ProcessFicoCOA(ref lRptFicoSetting);
                    if (lRptFicoSetting.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSetting, ref lRptFicoSetting2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3);

                    if (lCOA.Count > 0)
                    {
                        Grd1.BeginUpdate();

                        foreach (var x in lRptFicoSetting2.OrderBy(o => o.AcNo))
                        {
                            foreach (var y in lCOA.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                            {
                                lRptFicoSetting4.Add(new RptFicoSetting2()
                                {
                                    FicoCode = x.FicoCode,
                                    SettingCode = x.SettingCode,
                                    SettingDesc = x.SettingDesc,
                                    Sequence = x.Sequence,
                                    Formula = x.Formula,
                                    AcNo = x.AcNo,
                                    //ThisYearToDateBalance = (y.Year == "New") ? y.YearToDateBalance : 0m,
                                    ThisYearToDateBalance = y.YearToDateBalance,
                                    PastYearToDateBalance = y.PastYearToDateBalance,
                                    CurrentMonthThisYearBalance = y.CurrentMonthBalance,
                                    CurrentMonthPastYearBalance = y.PastCurrentMonthBalance,
                                    BudgetBalance = y.BudgetBalance
                                });
                                break;
                            }
                        }

                        ProcessFicoCOA4(ref lRptFicoSetting3, ref lRptFicoSetting4, Grd1);
                        Grd1.EndUpdate();
                    }

                    lEntityCOA.Clear();
                }
                lCOATemp.Clear();
                lJournalCurrentMonthTemp.Clear();
                lJournalTemp.Clear();
                lCOAOpeningBalanceTemp.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsFilterDifferent()
        {
            if (mInitYr != Sm.GetLue(LueYr) || mInitMth != Sm.GetLue(LueMth) || mInitStartFrom != Sm.GetLue(LueYr) || mInitProfitCenter != CcbProfitCenterCode.Text)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process it first.");
                return true;
            }

            return false;
        }

        private bool IsCOAEmpty()
        {
            if (lCOA.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process it first.");
                BtnProcess.Focus();
                return true;
            }

            return false;
        }

        private void SetLueFicoFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RptFicoCode As Col1, T.RptFicoName As Col2 ");
            SQL.AppendLine("From TblRptFicoSettingHdr T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (T.SiteCode Is Null Or ");
                SQL.AppendLine("(T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetAllCOA(ref List<COA2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("Order By A.AcNo; ");

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            PastOpeningBalanceDAmt = 0m,
                            PastOpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            PastMonthToDateDAmt = 0m,
                            PastMonthToDateCAmt = 0m,
                            PastMonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            PastCurrentMonthDAmt = 0m,
                            PastCurrentMonthCAmt = 0m,
                            PastCurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m,
                            PastYearToDateDAmt = 0m,
                            PastYearToDateCAmt = 0m,
                            PastYearToDateBalance = 0m,
                            BudgetDAmt = 0m,
                            BudgetCAmt = 0m,
                            BudgetBalance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournal(ref List<Journal2> l,
            string Yr, string Mth, string StartFrom,
            string Yr2, string Mth2, string StartFrom2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    SQL.AppendLine("    ) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode ");
                    //    SQL.AppendLine("        From TblCostCenter ");
                    //    SQL.AppendLine("        Where ActInd='Y' ");
                    //    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    //    SQL.AppendLine("        And ProfitCenterCode In (");
                    //    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("        )))) ");
                    //}
                }
            }
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            // SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom2.Length == 0)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr2 ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth2 ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom2 ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth2 ");
            }
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    SQL.AppendLine("    ) ");
                    //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode ");
                    //    SQL.AppendLine("        From TblCostCenter ");
                    //    SQL.AppendLine("        Where ActInd='Y' ");
                    //    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    //    SQL.AppendLine("        And ProfitCenterCode In (");
                    //    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("        )))) ");
                    //}
                }
            }

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth2));
            if (StartFrom2.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom2", string.Concat(StartFrom2, "01"));

            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string Yr, string Mth, string Yr2, string Mth2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth ");
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    SQL.AppendLine("    ) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode ");
                    //    SQL.AppendLine("        From TblCostCenter ");
                    //    SQL.AppendLine("        Where ActInd='Y' ");
                    //    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    //    SQL.AppendLine("        And ProfitCenterCode In (");
                    //    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("        )))) ");
                    //}
                }
            }
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth2 ");
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    SQL.AppendLine("    ) ");
                    //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode ");
                    //    SQL.AppendLine("        From TblCostCenter ");
                    //    SQL.AppendLine("        Where ActInd='Y' ");
                    //    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    //    SQL.AppendLine("        And ProfitCenterCode In (");
                    //    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("        )))) ");
                    //}
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth2));
            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            //SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                    //    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                    //    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("    ) ");
                    //}
                }
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Old' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            //SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.Yr = @Yr2 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");

            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                    //else
                    //{
                    //    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                    //    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                    //    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("    ) ");
                    //}
                }
            }

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllBudget(ref List<Budget2> l, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, Sum(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, D.AcNo, ");
            SQL.AppendLine("Case E.AcType When 'D' Then C.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case E.AcType When 'C' Then C.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblBudgetRequestHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And A.CancelReason is null ");
            SQL.AppendLine("Inner Join TblBudgetDtl C On B.DocNo = C.BudgetRequestDocNo And B.DNo=C.BudgetRequestDNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory D On B.BCCode=D.BCCode  ");
            SQL.AppendLine("    And D.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA E On D.AcNo = E.AcNo And E.ActInd = 'Y'  ");
            SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode = F.CCCode And F.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");

            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                SQL.AppendLine("    And F.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (F.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    SQL.AppendLine("    And Find_In_Set(F.ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                    //    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                    //    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("    ) ");
                    //}
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Budget2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COAOpeningBalance2> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Year = x.Year
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            if (DocType == "New")
                            {
                                lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                                lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                                lCOA[j].CurrYr = true;
                            }
                            else
                            {
                                lCOA[j].PastOpeningBalanceDAmt += lJournal[i].DAmt;
                                lCOA[j].PastOpeningBalanceCAmt += lJournal[i].CAmt;
                                lCOA[j].PastYr = true;
                            }
                            lCOA[j].Year = lJournal[i].Year;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<Journal2> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Year = x.Year
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }

                            if (DocType == "New")
                            {
                                lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                                lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                                lCOA[j].CurrYr = true;
                            }
                            else
                            {
                                lCOA[j].PastMonthToDateDAmt += lJournal[i].DAmt;
                                lCOA[j].PastMonthToDateCAmt += lJournal[i].CAmt;
                                lCOA[j].PastYr = true;
                            }
                            lCOA[j].Year = lJournal[i].Year;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                       
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<Journal3> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(p => p.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Year = x.Year
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }

                            if (DocType == "New")
                            {
                                lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                                lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                                lCOA[j].CurrYr = true;
                            }
                            else
                            {
                                lCOA[j].PastCurrentMonthDAmt += lJournal[i].DAmt;
                                lCOA[j].PastCurrentMonthCAmt += lJournal[i].CAmt;
                                lCOA[j].PastYr = true;
                            }

                            lCOA[j].Year = lJournal[i].Year;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<Budget2> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            //foreach (var x in l)
            //{
            //    foreach (var y in lCOA.Where(w => w.AcNo == x.AcNo))
            //    {
            //        y.BudgetDAmt += x.DAmt;
            //        y.BudgetCAmt += x.CAmt;
            //    }
            //}

            foreach (var x in l.OrderBy(o => o.AcNo).Where(p => p.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Year = x.Year
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].BudgetDAmt += lJournal[i].DAmt;
                            lCOA[j].BudgetCAmt += lJournal[i].CAmt;

                            lCOA[j].Year = lJournal[i].Year;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process6(ref List<COA> lCOA)
        {
            return;
            if (
                mAcNoForCurrentEarning.Length == 0 ||
                mAcNoForIncome.Length == 0 ||
                mAcNoForCost.Length == 0
                ) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            //var CurrentProfiLossParentIndex = -1;
            //var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //    {
            //        CurrentProfiLossParentIndex = i;
            //        break;
            //    }
            //}

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;
            //    (lCOA[IncomeIndex].OpeningBalanceCAmt - lCOA[IncomeIndex].OpeningBalanceDAmt)-
            //    (lCOA[CostIndex].OpeningBalanceDAmt - lCOA[CostIndex].OpeningBalanceCAmt);

            //lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceCAmt += Amt;

            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Year To Date
            //Amt =
            //    (lCOA[IncomeIndex].YearToDateCAmt - lCOA[IncomeIndex].YearToDateDAmt) -
            //    (lCOA[CostIndex].YearToDateDAmt - lCOA[CostIndex].YearToDateCAmt);

            //lCOA[CurrentProfiLossIndex].YearToDateDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateCAmt += Amt;

            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}
        }

        private void Process7(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                lCOA[i].PastYearToDateDAmt =
                    lCOA[i].PastOpeningBalanceDAmt +
                    lCOA[i].PastMonthToDateDAmt +
                    lCOA[i].PastCurrentMonthDAmt;

                lCOA[i].PastYearToDateCAmt =
                    lCOA[i].PastOpeningBalanceCAmt +
                    lCOA[i].PastMonthToDateCAmt +
                    lCOA[i].PastCurrentMonthCAmt;
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].PastMonthToDateBalance =
                        lCOA[i].PastOpeningBalanceDAmt +
                        lCOA[i].PastMonthToDateDAmt -
                        lCOA[i].PastMonthToDateCAmt;

                    lCOA[i].PastCurrentMonthBalance =
                        lCOA[i].PastCurrentMonthDAmt -
                        lCOA[i].PastCurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    lCOA[i].BudgetBalance = lCOA[i].BudgetDAmt - lCOA[i].BudgetCAmt;

                    lCOA[i].PastYearToDateBalance = lCOA[i].PastYearToDateDAmt - lCOA[i].PastYearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt -
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].PastMonthToDateBalance =
                        lCOA[i].PastOpeningBalanceCAmt +
                        lCOA[i].PastMonthToDateCAmt -
                        lCOA[i].PastMonthToDateDAmt;

                    lCOA[i].PastCurrentMonthBalance =
                        lCOA[i].PastCurrentMonthCAmt -
                        lCOA[i].PastCurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                    lCOA[i].BudgetBalance = lCOA[i].BudgetCAmt - lCOA[i].BudgetDAmt;

                    lCOA[i].PastYearToDateBalance = lCOA[i].PastYearToDateCAmt - lCOA[i].PastYearToDateDAmt;
                }
            }
        }

        private void ProcessFicoCOA(ref List<RptFicoSetting> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by Cast(sequence As UNSIGNED); ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new RptFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            ThisYearToDateBalance = 0,
                            PastYearToDateBalance = 0,
                            CurrentMonthThisYearBalance = 0,
                            CurrentMonthPastYearBalance = 0,
                            BudgetBalance = 0,
                            AcNo = string.Empty,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA2(ref List<RptFicoSetting> l, ref List<RptFicoSetting> lRptFicoSetting)
        {
            lRptFicoSetting.Clear();

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SettingDesc.Length > 0)
                {
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();

                    SQL.AppendLine("SET SESSION group_concat_max_len = 1000000; ");
                    SQL.AppendLine("Select AcNo From TblCOA ");
                    SQL.AppendLine("Where Find_In_Set(AcNo,( ");
                    SQL.AppendLine("    Select Group_Concat(Acno) As AcNo ");
                    SQL.AppendLine("    From TblRptFicoSettingDtl ");
                    SQL.AppendLine("    Where SettingCode=@SettingCode ");
                    SQL.AppendLine("    And RptFicoCode=@FicoCode ");
                    SQL.AppendLine("    Order by Cast(sequence As UNSIGNED) ");
                    SQL.AppendLine(")) Order By AcNo;");
                    Sm.CmParam<String>(ref cm, "@SettingCode", l[i].SettingCode);
                    Sm.CmParam<String>(ref cm, "@FicoCode", l[i].FicoCode);

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lRptFicoSetting.Add(new RptFicoSetting()
                                {
                                    FicoCode = l[i].FicoCode,
                                    SettingCode = l[i].SettingCode,
                                    SettingDesc = l[i].SettingDesc,
                                    Sequence = l[i].Sequence,
                                    Formula = l[i].Formula,
                                    AcNo = Sm.DrStr(dr, c[0]),
                                    ThisYearToDateBalance = 0,
                                    PastYearToDateBalance = 0,
                                    CurrentMonthThisYearBalance = 0,
                                    CurrentMonthPastYearBalance = 0,
                                    BudgetBalance = 0,
                                });
                            }
                        }
                        dr.Close();
                    }
                }
            }
        }

        private void ProcessFicoCOA3(ref List<COAFicoSetting> lCOASet)
        {
            lCOASet.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettinghdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By Cast(B.sequence As UNSIGNED);");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        lCOASet.Add(new COAFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            ThisYearToDateBalance = 0,
                            PastYearToDateBalance = 0,
                            CurrentMonthThisYearBalance = 0,
                            CurrentMonthPastYearBalance = 0,
                            BudgetBalance = 0,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA4(ref List<COAFicoSetting> lCOASet, ref List<RptFicoSetting2> l, iGrid Grdxx)
        {
            Sm.ClearGrd(Grdxx, true);
            iGRow r;


            for (var i = 0; i < lCOASet.Count; i++)
            {
                decimal ytdbal = 0, pastytdbal = 0, currentmonthbal = 0, pastcurrentmonthbal = 0, budgetbal = 0;
                r = Grdxx.Rows.Add();
                r.Cells[0].Value = lCOASet[i].SettingCode;
                r.Cells[1].Value = lCOASet[i].SettingDesc;


                if (lCOASet[i].Formula.Length == 0)
                {
                    l.OrderBy(o => o.Sequence).ToList();
                    for (var j = 0; j < l.Count; j++)
                    {
                        if (lCOASet[i].SettingCode == l[j].SettingCode)
                        {
                            r.Cells[2].Value = budgetbal + l[j].BudgetBalance;
                            r.Cells[3].Value = ytdbal + l[j].ThisYearToDateBalance;
                            r.Cells[4].Value = pastytdbal + l[j].PastYearToDateBalance;
                            r.Cells[5].Value = currentmonthbal + l[j].CurrentMonthThisYearBalance;
                            r.Cells[6].Value = pastcurrentmonthbal + l[j].CurrentMonthPastYearBalance;


                            budgetbal = budgetbal + l[j].BudgetBalance;
                            ytdbal = ytdbal + l[j].ThisYearToDateBalance;
                            pastytdbal = pastytdbal + l[j].PastYearToDateBalance;
                            currentmonthbal = currentmonthbal + l[j].CurrentMonthThisYearBalance;
                            pastcurrentmonthbal = pastcurrentmonthbal + l[j].CurrentMonthPastYearBalance;
                        }
                    }
                }
                else
                {
                    char[] delimiters = { '+', '-' };
                    string SQLFormula = lCOASet[i].Formula,
                        SQLFormula2 = lCOASet[i].Formula,
                        SQLFormula3 = lCOASet[i].Formula,
                        SQLFormula4 = lCOASet[i].Formula,
                        SQLFormula5 = lCOASet[i].Formula;
                    string[] ArraySQLFormula = lCOASet[i].Formula.Split(delimiters);
                    for (int ind = 0; ind < ArraySQLFormula.Count(); ind++)
                    {
                        for (var h = 0; h < Grdxx.Rows.Count; h++)
                        {
                            if (ArraySQLFormula[ind].ToString() == Sm.GetGrdStr(Grdxx, h, 0))
                            {
                                string oldS = ArraySQLFormula[ind].ToString();
                                string newS = Sm.GetGrdDec(Grdxx, h, 3).ToString();
                                string newS2 = Sm.GetGrdDec(Grdxx, h, 4).ToString();
                                string newS3 = Sm.GetGrdDec(Grdxx, h, 5).ToString();
                                string newS4 = Sm.GetGrdDec(Grdxx, h, 6).ToString();
                                string newS5 = Sm.GetGrdDec(Grdxx, h, 2).ToString();

                                SQLFormula = SQLFormula.Replace(oldS, newS);
                                SQLFormula2 = SQLFormula2.Replace(oldS, newS2);
                                SQLFormula3 = SQLFormula3.Replace(oldS, newS3);
                                SQLFormula4 = SQLFormula4.Replace(oldS, newS4);
                                SQLFormula5 = SQLFormula5.Replace(oldS, newS5);
                            }
                        }
                    }
                    decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
                    decimal baltemp2 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula2 + " "));
                    decimal baltemp3 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula3 + " "));
                    decimal baltemp4 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula4 + " "));
                    decimal baltemp5 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula5 + " "));

                    r.Cells[3].Value = baltemp;
                    r.Cells[4].Value = baltemp2;
                    r.Cells[5].Value = baltemp3;
                    r.Cells[6].Value = baltemp4;
                    r.Cells[2].Value = baltemp5;

                }
            }
        }

        private void ParPrint()
        {
            int nomor = 0;
            var l = new List<TB>();
            var l2 = new List<TBDtl>();

            string[] TableName = { "TB", "TBDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("A.RptFicoCode, A.Remark, A.Createby ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Where A.RptFIcoCode=@RptFicoCode  ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@RptFicoCode", Sm.GetLue(LueOptionCode));
                Sm.CmParam<String>(ref cm, "@CompanyLogo", Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-6
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "Remark",
                         "CreateBy",
                        });
                if (dr.HasRows)
                {
                    int month = Convert.ToInt32(Sm.GetLue(LueMth));
                    string mth = string.Empty;
                    switch (month)
                    {
                        case 1:
                            mth = "January";
                            break;
                        case 2:
                            mth = "February";
                            break;
                        case 3:
                            mth = "March";
                            break;
                        case 4:
                            mth = "April";
                            break;
                        case 5:
                            mth = "May";
                            break;
                        case 6:
                            mth = "June";
                            break;
                        case 7:
                            mth = "July";
                            break;
                        case 8:
                            mth = "August";
                            break;
                        case 9:
                            mth = "September";
                            break;
                        case 10:
                            mth = "October";
                            break;
                        case 11:
                            mth = "November";
                            break;
                        case 12:
                            mth = "December";
                            break;
                    }
                    while (dr.Read())
                    {
                        l.Add(new TB()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            Yr = Sm.GetLue(LueYr),
                            Mth = mth,
                            //Entity = Sm.GetValue("select EntName From tblentity Where EntCode = '" + Sm.GetLue(LueEntCode) + "' "),
                            HRemark = Sm.DrStr(dr, c[5]),
                            Type = LueOptionCode.Text,
                            Yr2 = (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            IsPrintRptFicoSettingShowPercentage = mIsPrintRptFicoSettingShowPercentage,
                            IsPrintRptFicoSettingDisplayDate = mIsPrintRptFicoSettingDisplayDate,
                            IsPrintRptFicoSettingDisplayPrintDate = mIsPrintRptFicoSettingDisplayPrintDate,
                            Date = System.DateTime.DaysInMonth(Convert.ToInt32(Sm.GetLue(LueYr)), month),
                            ProfitCenter = CcbProfitCenterCode.Text,
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select B.AcNo AcNo2, A.SettingCode, A.SettingDesc, A.Sequence, A.AcNo, A.Formula, A.Font,  A.Line, A.Align, A.IndentInd  ");
                SQLDtl.AppendLine("From TblRptFicoSettingDtl A   ");
                SQLDtl.AppendLine("LEFT JOIN  ");
                SQLDtl.AppendLine("(SELECT AcNo FROM tblcoa WHERE FIND_IN_SET (Acno, '" + mPrintRptFicoSettingCOAParentheses + "')) B ON A.AcNo=B.AcNo ");
                SQLDtl.AppendLine("Where A.RptFIcoCode=@RptFicoCode And PrintInd = 'Y'  order by Cast(sequence As UNSIGNED) ");

                cmDtl.CommandText = SQLDtl.ToString();
                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@RptFicoCode", Sm.GetLue(LueOptionCode));


                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "SettingCode" ,

                         //1-5
                         "SettingDesc",
                         "Sequence",
                         "Formula",
                         "Font",
                         "Line",

                         //6-9
                         "AcNo",
                         "AcNo2",
                         "Align",
                         "IndentInd"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        decimal budgetbal = 0;
                        decimal ytdbal = 0;
                        decimal pastytdbal = 0;
                        decimal currentmonthbal = 0;
                        decimal pastcurrentmonthbal = 0;
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && Sm.GetGrdStr(Grd1, Row, 0) == Sm.DrStr(drDtl, cDtl[0]))
                            {
                                budgetbal = Sm.GetGrdDec(Grd1, Row, 2);
                                ytdbal = Sm.GetGrdDec(Grd1, Row, 3);
                                pastytdbal = Sm.GetGrdDec(Grd1, Row, 4);
                                currentmonthbal = Sm.GetGrdDec(Grd1, Row, 5);
                                pastcurrentmonthbal = Sm.GetGrdDec(Grd1, Row, 6);
                            }
                        } 
                        nomor = nomor + 1;
                        l2.Add(new TBDtl()
                        {
                            nomor = nomor,
                            SettingCode = Sm.DrStr(drDtl, cDtl[0]),
                            SettingDesc = Sm.DrStr(drDtl, cDtl[1]),
                            Sequence = Sm.DrStr(drDtl, cDtl[2]),
                            Formula = Sm.DrStr(drDtl, cDtl[3]),
                            Font = Sm.DrStr(drDtl, cDtl[4]),
                            Line = Sm.DrStr(drDtl, cDtl[5]) == "P" ? "" + Environment.NewLine + "" + Environment.NewLine + "" : Sm.DrStr(drDtl, cDtl[5]),
                            BudgetBalance = budgetbal,
                            YTDBalance = ytdbal,
                            PastYTDBalance = pastytdbal,
                            CurrentMonthBalance = currentmonthbal,
                            PastCurrentMonthBalance = pastcurrentmonthbal,
                            mIsPrintRptFicoSettingUseParentheses = mIsPrintRptFicoSettingUseParentheses,
                            mPrintRptFicoSettingParenthesesFormat = mPrintRptFicoSettingParenthesesFormat,
                            AcNo = Sm.DrStr(drDtl, cDtl[6]),
                            AcNo2 = Sm.DrStr(drDtl, cDtl[7]),
                            Align = Sm.DrStr(drDtl, cDtl[8]),
                            IndentInd = Sm.DrStr(drDtl, cDtl[9]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(l2);
            #endregion

            Sm.PrintReport("RptFicoSetting2", myLists, TableName, false);
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
            //SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));

            //var IsIncludeConsolidate = Value.Contains("Consolidate");
            //return string.Concat(
            //    (IsIncludeConsolidate?"Consolidate, ":string.Empty), 
            //    Sm.GetValue(
            //        "Select Group_Concat(T.Code Separator ', ') As Code " +
            //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ", 
            //        Value.Replace(", ", ",")));
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }
            ParPrint();
        }


        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsProfitCenterInvalid()) return;

            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();
            var lBudgetTemp = new List<Budget2>();
            mInitYr = Sm.GetLue(LueYr);
            mInitMth = Sm.GetLue(LueMth);
            mInitStartFrom = Sm.GetLue(LueYr);
            mInitProfitCenter = CcbProfitCenterCode.Text;
            string mListAcNo = string.Empty, mUsedAcNo = string.Empty;

            SetProfitCenter();
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueYr);
            var Yr2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var StartFrom2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var Mth2 = mPrintFicoSettingLastYearBasedOnFilterMonth ? Sm.GetLue(LueMth) : Convert.ToString(12);

            GetAllCOA(ref lCOATemp);
            //if (lCOATemp.Count > 0)
            //{
            //    foreach (var i in lCOATemp)
            //    {
            //        if (mUsedAcNo.Length > 0) mUsedAcNo += ",";
            //        mUsedAcNo += i.AcNo;
            //    }
            //}

            if (StartFrom.Length > 0)
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, StartFrom, StartFrom2);
                GetAllJournal(ref lJournalTemp, Yr, Mth, StartFrom, Yr2, Mth2.ToString(), StartFrom2);
                GetAllBudget(ref lBudgetTemp, StartFrom, StartFrom2);
            }
            else
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, Yr, Yr2);
                GetAllJournal(ref lJournalTemp, Yr, Mth, string.Empty, Yr2, Mth2.ToString(), string.Empty);
                GetAllBudget(ref lBudgetTemp, Yr, Yr2);
            }

            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, Yr, Mth, Yr2, Mth2.ToString());


            try
            {
                Sm.ClearGrd(Grd1, true);
                lCOA.Clear();
                foreach (var x in lCOATemp)
                {
                    lCOA.Add(new COA()
                    {
                        AcNo = x.AcNo,
                        AcDesc = x.AcDesc,
                        AcType = x.AcType,
                        HasChild = x.HasChild,
                        Level = x.Level,
                        Parent = x.Parent,
                        CurrentMonthBalance = x.CurrentMonthBalance,
                        CurrentMonthCAmt = x.CurrentMonthCAmt,
                        CurrentMonthDAmt = x.CurrentMonthDAmt,
                        MonthToDateBalance = x.MonthToDateBalance,
                        MonthToDateCAmt = x.MonthToDateCAmt,
                        MonthToDateDAmt = x.MonthToDateDAmt,
                        OpeningBalanceCAmt = x.OpeningBalanceCAmt,
                        OpeningBalanceDAmt = x.OpeningBalanceDAmt,
                        YearToDateBalance = x.YearToDateBalance,
                        YearToDateCAmt = x.YearToDateCAmt,
                        YearToDateDAmt = x.YearToDateDAmt,
                        BudgetCAmt = x.BudgetCAmt,
                        BudgetDAmt = x.BudgetDAmt,
                        BudgetBalance = x.BudgetBalance,
                        Year = x.Year,
                        CurrYr = false,
                        PastYr = false
                    });
                }

                if (lCOA.Count > 0)
                {
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOA, "New");
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOA, "Old");
                    Process3(ref lJournalTemp, ref lCOA, "New");
                    Process3(ref lJournalTemp, ref lCOA, "Old");
                    Process4(ref lJournalCurrentMonthTemp, ref lCOA, "New");
                    Process4(ref lJournalCurrentMonthTemp, ref lCOA, "Old");
                    Process5(ref lBudgetTemp, ref lCOA, "New");
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);

                }
                Sm.StdMsg(mMsgType.Info, "Done Processing");

                lCOATemp.Clear();
                lJournalCurrentMonthTemp.Clear();
                lJournalTemp.Clear();
                lCOAOpeningBalanceTemp.Clear();
                lBudgetTemp.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void LueOptionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptionCode, new Sm.RefreshLue1(SetLueFicoFormula));
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                TxtStartYr.EditValue = Sm.GetLue(LueYr);
            }
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal PastOpeningBalanceDAmt { get; set; }
            public decimal PastOpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal PastMonthToDateDAmt { get; set; }
            public decimal PastMonthToDateCAmt { get; set; }
            public decimal PastMonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal PastCurrentMonthDAmt { get; set; }
            public decimal PastCurrentMonthCAmt { get; set; }
            public decimal PastCurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
            public decimal PastYearToDateDAmt { get; set; }
            public decimal PastYearToDateCAmt { get; set; }
            public decimal PastYearToDateBalance { get; set; }
            public decimal BudgetDAmt { get; set; }
            public decimal BudgetCAmt { get; set; }
            public decimal BudgetBalance { get; set; }
            public string Year { get; set; }
            public bool CurrYr { get; set; }
            public bool PastYr { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal PastOpeningBalanceDAmt { get; set; }
            public decimal PastOpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal PastMonthToDateDAmt { get; set; }
            public decimal PastMonthToDateCAmt { get; set; }
            public decimal PastMonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal PastCurrentMonthDAmt { get; set; }
            public decimal PastCurrentMonthCAmt { get; set; }
            public decimal PastCurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
            public decimal PastYearToDateDAmt { get; set; }
            public decimal PastYearToDateCAmt { get; set; }
            public decimal PastYearToDateBalance { get; set; }
            public decimal BudgetDAmt { get; set; }
            public decimal BudgetCAmt { get; set; }
            public decimal BudgetBalance { get; set; }
            public string Year { get; set; }
            public bool CurrYr { get; set; }
            public bool PastYr { get; set; }
        }

        private class Journal
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Budget2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }


        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COAFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal ThisYearToDateBalance { get; set; }
            public decimal PastYearToDateBalance { get; set; }
            public decimal CurrentMonthThisYearBalance { get; set; }
            public decimal CurrentMonthPastYearBalance { get; set; }
            public decimal BudgetBalance { get; set; }
        }

        private class RptFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal ThisYearToDateBalance { get; set; }
            public decimal PastYearToDateBalance { get; set; }
            public decimal CurrentMonthThisYearBalance { get; set; }
            public decimal CurrentMonthPastYearBalance { get; set; }
            public decimal BudgetBalance { get; set; }
        }

        private class RptFicoSetting2
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal ThisYearToDateBalance { get; set; }
            public decimal PastYearToDateBalance { get; set; }
            public decimal CurrentMonthThisYearBalance { get; set; }
            public decimal CurrentMonthPastYearBalance { get; set; }
            public decimal BudgetBalance { get; set; }
        }

        private class TB
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Yr { get; set; }
            public string Yr2 { get; set; }
            public string Mth { get; set; }
            public string Entity { get; set; }
            public string HRemark { get; set; }
            public string Type { get; set; }
            public string PrintBy { get; set; }
            public bool IsPrintRptFicoSettingShowPercentage { get; set; }
            public bool IsPrintRptFicoSettingDisplayDate { get; set; }
            public bool IsPrintRptFicoSettingDisplayPrintDate { get; set; }
            public int Date { get; set; }
            public string ProfitCenter { get; set; }
        }

        private class TBDtl
        {
            public int nomor { set; get; }
            public string SettingCode { set; get; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public decimal Balance2 { get; set; }
            public string Font { get; set; }
            public string Line { get; set; }
            public string Align { get; set; }
            public string IndentInd { get; set; }
            public decimal Percentage { get; set; }
            public bool mIsPrintRptFicoSettingUseParentheses { get; set; }
            public string mPrintRptFicoSettingParenthesesFormat { get; set; }
            public string AcNo { get; set; }
            public string AcNo2 { get; set; }
            public decimal BudgetBalance  { get; set; }
            public decimal YTDBalance  { get; set; }
            public decimal PastYTDBalance  { get; set; }
            public decimal CurrentMonthBalance  { get; set; }
            public decimal PastCurrentMonthBalance { get; set; }
        }
        #endregion

     

        
    }
}
