﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpAtdPoint : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmEmpAtdPointFind FrmFind;
        internal bool mIsFilterBySiteHR = false;
        internal string mDocNo = string.Empty;


        #endregion

        #region Constructor

        public FrmEmpAtdPoint(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "PO Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueYr(LueYr, "");
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application

             
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Old Code",
                        "Employee",
                        "Department",
                        //6-7
                        "Site",
                        "Position"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        20, 100, 100, 200, 180,
                        
                        //6-10
                        180, 180
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
         
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueYr, MeeCancelReason, ChkCancelInd, LueSiteCode, TxtPoint, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, LueSiteCode, TxtPoint, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtDocNo, MeeCancelReason, ChkCancelInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, MeeCancelReason, ChkCancelInd, LueSiteCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtPoint
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                if (!mIsFilterBySiteHR)
                {
                    if (!Sm.IsLueEmpty(LueSiteCode, "Site"))
                        Sm.FormShowDialog(new FrmEmpAtdPointDlg(this, Sm.GetLue(LueSiteCode), Sm.GetLue(LueYr)));
                }
                else
                {
                    Sm.FormShowDialog(new FrmEmpAtdPointDlg(this, Sm.GetLue(LueSiteCode), Sm.GetLue(LueYr)));
                }
            }         
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                if (mIsFilterBySiteHR)
                {
                    if (!Sm.IsLueEmpty(LueSiteCode, "Site"))
                        Sm.FormShowDialog(new FrmEmpAtdPointDlg(this, Sm.GetLue(LueSiteCode), Sm.GetLue(LueYr)));
                }
                else
                {
                    Sm.FormShowDialog(new FrmEmpAtdPointDlg(this, Sm.GetLue(LueSiteCode), Sm.GetLue(LueYr)));
                }
            }

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpAtdPointFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;


            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpAtdPoint", "TblEmpAtdPointHdr");
            
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpAtdPointHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveEmpAtdPointDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Period") ||
                Sm.IsTxtEmpty(TxtPoint, "Point", true)||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsPeriodeEmployeeExist()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsPeriodeEmployeeExist()
        {
            string yr = Sm.GetLue(LueYr);

            for (int XRow = 0; XRow < Grd1.Rows.Count-1; XRow++)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select A.DocNo From TblEmpAtdPointHdr A " +
                        "Inner Join TblEmpAtdPointDtl B On A.DocNo=B.DocNo "+
                        "Where A.CancelInd='Y' And A.Yr='"+yr+"' And A.DocNo=@DocNo "+
                        "And B.EmpCode = '"+Sm.GetGrdStr(Grd1, XRow, 2)+"' "
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "employee '" + Sm.GetGrdStr(Grd1, XRow, 4) + "' for this period has been created.");
                    Sm.FocusGrd(Grd1, XRow, 2);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveEmpAtdPointHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpAtdPointHdr " +
                    "(DocNo, DocDt, Yr, SiteCode, Point, CancelInd, CancelReason, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Yr, @SiteCode, @Point, 'N', @CancelReason, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<Decimal>(ref cm, "@Point", Decimal.Parse(TxtPoint.Text));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveEmpAtdPointDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpAtdPointDtl ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpAtdPoint());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpAtdPointHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditEmpAtdPoint()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpAtdPointHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
             Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion
        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpAtdPointHdr(DocNo);
                ShowEmpAtdPointDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpAtdPointHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.SiteCode, A.CancelReason, A.CancelInd, A.Point, A.Remark " +
                    "From TblEmpAtdPointHdr A " +
                    "Where A.DocNo=@DocNo;",
                    new string[]
                    { 
                        "DocNo", 
                        "DocDt", "Yr", "SiteCode", "CancelReason", "CancelInd",
                        "Point", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                        TxtPoint.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowEmpAtdPointDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DNo, B.EmpCode, C.EmpCodeOld, C.EmpName, D.Deptname, E.posname, F.Sitename ");
                SQL.AppendLine("From TblEmpAtdPointHdr A ");
                SQL.AppendLine("Inner Join TblEmpAtdPointDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
                SQL.AppendLine("Inner Join TblPosition E On C.PosCode = E.PosCode");
                SQL.AppendLine("Left join tblSite F On C.SiteCode = F.SiteCode");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "EmpCode", "EmpCodeOld", "EmpName", "Deptname", "Sitename", 
                        
                        //6-10
                        "posname", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        internal string GetSelectedEmployeeCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 )
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        
        #endregion

        #region event
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");

        }
        private void TxtPoint_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPoint, 0);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
        }
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        #endregion
        
    }
}
