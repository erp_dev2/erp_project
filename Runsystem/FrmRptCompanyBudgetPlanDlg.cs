﻿#region Update
/*
    15/11/2021 [TKG/PHT] new dialog 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion


namespace RunSystem
{
    public partial class FrmRptCompanyBudgetPlanDlg : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private FrmRptCompanyBudgetPlan mFrmParent;
        
        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlanDlg(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Field

        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlanDlg(FrmRptCompanyBudgetPlan FrmParent, string AcNo, string AcDesc)
        {
            InitializeComponent();

            mFrmParent = FrmParent;
            TxtAcNo.EditValue = AcNo;
            TxtAcDesc.EditValue = AcDesc;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetGrd();
            ShowData();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 4;
            Grd1.Cols.Count = mFrmParent.mDecimalCols[mFrmParent.mDecimalCols.Count() - 1] + 1;
            Grd1.FrozenArea.ColCount = 2;

            #region general

            Grd1.Cols[0].Width = 0;
            Grd1.Header.Cells[0, 0].Value = "Account Name";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 4;

            Grd1.Cols[1].Width = 0;
            Grd1.Header.Cells[0, 1].Value = "Account#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 4;

            Grd1.Cols[2].Width = 180;
            Grd1.Header.Cells[0, 2].Value = "Directorate" + Environment.NewLine + "(Department's Group)";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 4;

            Grd1.Cols[3].Width = 150;
            Grd1.Header.Cells[0, 3].Value = "Item's Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 4;

            Grd1.Cols[4].Width = 150;
            Grd1.Header.Cells[0, 4].Value = "Item's Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 4;

            Grd1.Cols[5].Width = 100;
            Grd1.Header.Cells[0, 5].Value = "UoM";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 4;

            Grd1.Cols[6].Width = 100;
            Grd1.Header.Cells[0, 6].Value = "Budget Type";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 4;
            Grd1.Cols[6].Move(2);

            #endregion

            #region year

            Grd1.Header.Cells[0, 4 + 3].Value = "Average" + Environment.NewLine + "Rate";
            Grd1.Header.Cells[0, 4 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 5 + 3].Value = "1 Year";
            Grd1.Header.Cells[3, 5 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 5 + 3].SpanCols = 6;
            Grd1.Header.Cells[1, 5 + 3].Value = "RKAP";
            Grd1.Header.Cells[1, 5 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 5 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 5 + 3].SpanRows = 2;
            Grd1.Header.Cells[1, 7 + 3].Value = "Realization";
            Grd1.Header.Cells[1, 7 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 7 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 7 + 3].SpanRows = 2;
            Grd1.Header.Cells[1, 9 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[1, 9 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 9 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 9 + 3].SpanRows = 2;
            Grd1.Header.Cells[0, 5 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 6 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 7 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 8 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 9 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 10 + 3].Value = "Amount";

            #endregion

            #region januari

            Grd1.Header.Cells[0, 11 + 3].Value = "Rate" + Environment.NewLine + "Jan";
            Grd1.Header.Cells[0, 11 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 12 + 3].Value = "Jan";
            Grd1.Header.Cells[3, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 12 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 12 + 3].Value = "RO";
            Grd1.Header.Cells[2, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 12 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 16 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 16 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 16 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 20 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 20 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 20 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 12 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 12 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 14 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 14 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 14 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 16 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 16 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 16 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 18 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 18 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 18 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 20 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 20 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 20 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 22 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 22 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 22 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 12 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 13 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 14 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 15 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 16 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 17 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 18 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 19 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 20 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 21 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 22 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 23 + 3].Value = "Amount";

            #endregion

            #region februari

            Grd1.Header.Cells[0, 24 + 3].Value = "Rate" + Environment.NewLine + "Feb";
            Grd1.Header.Cells[0, 24 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 24 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 25 + 3].Value = "Feb";
            Grd1.Header.Cells[3, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 25 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 25 + 3].Value = "RO";
            Grd1.Header.Cells[2, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 25 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 29 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 29 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 29 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 33 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 33 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 33 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 25 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 25 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 27 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 27 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 27 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 29 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 29 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 29 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 31 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 31 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 31 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 33 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 33 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 33 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 35 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 35 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 35 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 25 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 26 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 27 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 28 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 29 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 30 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 31 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 32 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 33 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 34 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 35 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 36 + 3].Value = "Amount";

            #endregion

            #region maret

            Grd1.Header.Cells[0, 37 + 3].Value = "Rate" + Environment.NewLine + "Mar";
            Grd1.Header.Cells[0, 37 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 37 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 38 + 3].Value = "Mar";
            Grd1.Header.Cells[3, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 38 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 38 + 3].Value = "RO";
            Grd1.Header.Cells[2, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 38 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 42 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 42 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 42 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 46 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 46 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 46 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 38 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 38 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 40 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 40 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 40 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 42 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 42 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 42 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 44 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 44 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 44 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 46 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 46 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 46 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 48 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 48 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 48 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 38 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 39 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 40 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 41 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 42 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 43 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 44 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 45 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 46 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 47 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 48 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 49 + 3].Value = "Amount";

            #endregion

            #region q1

            Grd1.Header.Cells[0, 50 + 3].Value = "Rate" + Environment.NewLine + "Q1";
            Grd1.Header.Cells[0, 50 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 50 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 51 + 3].Value = "Q1";
            Grd1.Header.Cells[3, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 51 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 51 + 3].Value = "RO";
            Grd1.Header.Cells[2, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 51 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 55 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 55 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 55 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 59 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 59 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 59 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 51 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 51 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 53 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 53 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 53 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 55 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 55 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 55 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 57 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 57 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 57 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 59 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 59 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 59 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 61 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 61 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 61 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 51 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 52 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 53 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 54 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 55 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 56 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 57 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 58 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 59 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 60 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 61 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 62 + 3].Value = "Amount";

            #endregion

            #region april

            Grd1.Header.Cells[0, 63 + 3].Value = "Rate" + Environment.NewLine + "Apr";
            Grd1.Header.Cells[0, 63 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 63 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 64 + 3].Value = "Apr";
            Grd1.Header.Cells[3, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 64 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 64 + 3].Value = "RO";
            Grd1.Header.Cells[2, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 64 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 68 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 68 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 68 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 72 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 72 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 72 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 64 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 64 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 66 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 66 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 66 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 68 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 68 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 68 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 70 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 70 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 70 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 72 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 72 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 72 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 74 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 74 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 74 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 64 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 65 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 66 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 67 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 68 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 69 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 70 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 71 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 72 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 73 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 74 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 75 + 3].Value = "Amount";

            #endregion

            #region mei

            Grd1.Header.Cells[0, 76 + 3].Value = "Rate" + Environment.NewLine + "May";
            Grd1.Header.Cells[0, 76 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 76 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 77 + 3].Value = "May";
            Grd1.Header.Cells[3, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 77 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 77 + 3].Value = "RO";
            Grd1.Header.Cells[2, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 77 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 81 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 81 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 81 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 85 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 85 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 85 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 77 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 77 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 79 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 79 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 79 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 81 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 81 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 81 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 83 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 83 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 83 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 85 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 85 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 85 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 87 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 87 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 87 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 77 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 78 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 79 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 80 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 81 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 82 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 83 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 84 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 85 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 86 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 87 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 88 + 3].Value = "Amount";

            #endregion

            #region juni

            Grd1.Header.Cells[0, 89 + 3].Value = "Rate" + Environment.NewLine + "Jun";
            Grd1.Header.Cells[0, 89 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 89 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 90 + 3].Value = "Jun";
            Grd1.Header.Cells[3, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 90 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 90 + 3].Value = "RO";
            Grd1.Header.Cells[2, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 90 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 94 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 94 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 94 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 98 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 98 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 98 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 90 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 90 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 92 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 92 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 92 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 94 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 94 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 94 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 96 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 96 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 96 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 98 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 98 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 98 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 100 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 100 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 100 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 90 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 91 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 92 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 93 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 94 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 95 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 96 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 97 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 98 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 99 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 100 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 101 + 3].Value = "Amount";

            #endregion

            #region q2

            Grd1.Header.Cells[0, 102 + 3].Value = "Rate" + Environment.NewLine + "Q2";
            Grd1.Header.Cells[0, 102 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 102 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 103 + 3].Value = "Q2";
            Grd1.Header.Cells[3, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 103 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 103 + 3].Value = "RO";
            Grd1.Header.Cells[2, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 103 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 107 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 107 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 107 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 111 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 111 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 111 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 103 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 103 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 105 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 105 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 105 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 107 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 107 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 107 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 109 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 109 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 109 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 111 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 111 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 111 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 113 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 113 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 113 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 103 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 104 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 105 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 106 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 107 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 108 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 109 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 110 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 111 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 112 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 113 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 114 + 3].Value = "Amount";

            #endregion

            #region juli

            Grd1.Header.Cells[0, 115 + 3].Value = "Rate" + Environment.NewLine + "Jul";
            Grd1.Header.Cells[0, 115 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 115 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 116 + 3].Value = "Jul";
            Grd1.Header.Cells[3, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 116 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 116 + 3].Value = "RO";
            Grd1.Header.Cells[2, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 116 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 120 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 120 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 120 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 124 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 124 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 124 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 116 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 116 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 118 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 118 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 118 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 120 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 120 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 120 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 122 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 122 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 122 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 124 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 124 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 124 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 126 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 126 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 126 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 116 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 117 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 118 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 119 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 120 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 121 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 122 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 123 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 124 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 125 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 126 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 127 + 3].Value = "Amount";

            #endregion

            #region agustus

            Grd1.Header.Cells[0, 128 + 3].Value = "Rate" + Environment.NewLine + "Ags";
            Grd1.Header.Cells[0, 128 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 128 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 129 + 3].Value = "Ags";
            Grd1.Header.Cells[3, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 129 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 129 + 3].Value = "RO";
            Grd1.Header.Cells[2, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 129 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 133 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 133 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 133 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 137 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 137 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 137 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 129 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 129 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 131 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 131 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 131 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 133 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 133 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 133 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 135 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 135 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 135 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 137 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 137 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 137 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 139 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 139 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 139 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 129 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 130 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 131 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 132 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 133 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 134 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 135 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 136 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 137 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 138 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 139 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 140 + 3].Value = "Amount";

            #endregion

            #region september

            Grd1.Header.Cells[0, 141 + 3].Value = "Rate" + Environment.NewLine + "Sep";
            Grd1.Header.Cells[0, 141 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 141 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 142 + 3].Value = "Sep";
            Grd1.Header.Cells[3, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 142 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 142 + 3].Value = "RO";
            Grd1.Header.Cells[2, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 142 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 146 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 146 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 146 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 150 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 150 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 150 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 142 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 142 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 144 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 144 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 144 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 146 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 146 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 146 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 148 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 148 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 148 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 150 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 150 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 150 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 152 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 152 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 152 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 142 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 143 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 144 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 145 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 146 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 147 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 148 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 149 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 150 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 151 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 152 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 153 + 3].Value = "Amount";

            #endregion

            #region q3

            Grd1.Header.Cells[0, 154 + 3].Value = "Rate" + Environment.NewLine + "Q3";
            Grd1.Header.Cells[0, 154 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 154 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 155 + 3].Value = "Q3";
            Grd1.Header.Cells[3, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 155 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 155 + 3].Value = "RO";
            Grd1.Header.Cells[2, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 155 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 159 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 159 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 159 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 163 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 163 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 163 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 155 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 155 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 157 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 157 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 157 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 159 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 159 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 159 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 161 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 161 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 161 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 163 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 163 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 163 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 165 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 165 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 165 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 155 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 156 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 157 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 158 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 159 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 160 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 161 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 162 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 163 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 164 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 165 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 166 + 3].Value = "Amount";

            #endregion

            #region oktober

            Grd1.Header.Cells[0, 167 + 3].Value = "Rate" + Environment.NewLine + "Oct";
            Grd1.Header.Cells[0, 167 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 167 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 168 + 3].Value = "Oct";
            Grd1.Header.Cells[3, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 168 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 168 + 3].Value = "RO";
            Grd1.Header.Cells[2, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 168 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 172 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 172 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 172 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 176 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 176 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 176 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 168 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 168 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 170 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 170 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 170 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 172 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 172 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 172 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 174 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 174 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 174 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 176 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 176 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 176 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 178 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 178 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 178 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 168 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 169 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 170 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 171 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 172 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 173 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 174 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 175 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 176 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 177 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 178 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 179 + 3].Value = "Amount";

            #endregion

            #region november

            Grd1.Header.Cells[0, 180 + 3].Value = "Rate" + Environment.NewLine + "Nov";
            Grd1.Header.Cells[0, 180 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 180 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 181 + 3].Value = "Nov";
            Grd1.Header.Cells[3, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 181 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 181 + 3].Value = "RO";
            Grd1.Header.Cells[2, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 181 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 185 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 185 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 185 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 189 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 189 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 189 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 181 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 181 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 183 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 183 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 183 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 185 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 185 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 185 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 187 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 187 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 187 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 189 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 189 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 189 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 191 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 191 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 191 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 181 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 182 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 183 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 184 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 185 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 186 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 187 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 188 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 189 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 190 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 191 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 192 + 3].Value = "Amount";

            #endregion

            #region desember

            Grd1.Header.Cells[0, 193 + 3].Value = "Rate" + Environment.NewLine + "Dec";
            Grd1.Header.Cells[0, 193 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 193 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 194 + 3].Value = "Dec";
            Grd1.Header.Cells[3, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 194 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 194 + 3].Value = "RO";
            Grd1.Header.Cells[2, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 194 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 198 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 198 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 198 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 202 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 202 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 202 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 194 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 194 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 196 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 196 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 196 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 198 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 198 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 198 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 200 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 200 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 200 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 202 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 202 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 202 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 204 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 204 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 204 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 194 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 195 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 196 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 197 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 198 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 199 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 200 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 201 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 202 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 203 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 204 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 205 + 3].Value = "Amount";

            #endregion

            #region q4

            Grd1.Header.Cells[0, 206 + 3].Value = "Rate" + Environment.NewLine + "Q4";
            Grd1.Header.Cells[0, 206 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 206 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 207 + 3].Value = "Q4";
            Grd1.Header.Cells[3, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 207 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 207 + 3].Value = "RO";
            Grd1.Header.Cells[2, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 207 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 211 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 211 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 211 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 215 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 215 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 215 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 207 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 207 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 209 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 209 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 209 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 211 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 211 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 211 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 213 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 213 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 213 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 215 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 215 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 215 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 217 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 217 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 217 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 207 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 208 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 209 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 210 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 211 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 212 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 213 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 214 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 215 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 216 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 217 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 218 + 3].Value = "Amount";

            #endregion


            Sm.GrdFormatDec(Grd1, mFrmParent.mDecimalCols, 0);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            foreach (var x in mFrmParent.mDecimalCols)
                Grd1.Cols[x].Width = 130;

            Sm.GrdColInvisible(Grd1, new int[] { 3 });

            if (mFrmParent.mQuarterMenu)
            {
                Sm.GrdColInvisible(Grd1, mFrmParent.mPartOfQ1Cols);
                Sm.GrdColInvisible(Grd1, mFrmParent.mPartOfQ2Cols);
                Sm.GrdColInvisible(Grd1, mFrmParent.mPartOfQ3Cols);
                Sm.GrdColInvisible(Grd1, mFrmParent.mPartOfQ4Cols);
            }
        }

        override protected void ShowData()
        {
            try
            {
                Grd1.BeginUpdate();
                foreach (var x in mFrmParent.ml.Where(w=>Sm.CompareStr(w.AcNo, TxtAcNo.Text)).OrderBy(o => o.AcNo))
                {
                    Grd1.Rows.Add();

                    #region general
                    //Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = x.AcDesc;
                    //Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                    Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.DeptGrpName;
                    Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.ItCode;
                    Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = x.ItName;
                    Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = x.UoM;
                    Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = x.BudgetType;
                    #endregion

                    #region year
                    Grd1.Cells[Grd1.Rows.Count - 1, 4 + 3].Value = x.Rate;
                    Grd1.Cells[Grd1.Rows.Count - 1, 5 + 3].Value = x.QtyCBP;
                    Grd1.Cells[Grd1.Rows.Count - 1, 6 + 3].Value = x.AmtCBP;
                    Grd1.Cells[Grd1.Rows.Count - 1, 7 + 3].Value = x.QtyCAS;
                    Grd1.Cells[Grd1.Rows.Count - 1, 8 + 3].Value = x.AmtCAS;
                    Grd1.Cells[Grd1.Rows.Count - 1, 9 + 3].Value = x.QtyRemain;
                    Grd1.Cells[Grd1.Rows.Count - 1, 10 + 3].Value = x.AmtRemain;
                    #endregion

                    #region januari
                    Grd1.Cells[Grd1.Rows.Count - 1, 11 + 3].Value = x.Rate01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 12 + 3].Value = x.QtyCBP01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 13 + 3].Value = x.AmtCBP01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 14 + 3].Value = x.QtyCBPTo01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 15 + 3].Value = x.AmtCBPTo01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 16 + 3].Value = x.QtyCAS01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 17 + 3].Value = x.AmtCAS01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 18 + 3].Value = x.QtyCASTo01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 19 + 3].Value = x.AmtCASTo01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 20 + 3].Value = x.QtyRemain01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 21 + 3].Value = x.AmtRemain01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 22 + 3].Value = x.QtyRemainTo01;
                    Grd1.Cells[Grd1.Rows.Count - 1, 23 + 3].Value = x.AmtRemainTo01;
                    #endregion

                    #region februari
                    Grd1.Cells[Grd1.Rows.Count - 1, 24 + 3].Value = x.Rate02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 25 + 3].Value = x.QtyCBP02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 26 + 3].Value = x.AmtCBP02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 27 + 3].Value = x.QtyCBPTo02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 28 + 3].Value = x.AmtCBPTo02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 29 + 3].Value = x.QtyCAS02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 30 + 3].Value = x.AmtCAS02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 31 + 3].Value = x.QtyCASTo02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 32 + 3].Value = x.AmtCASTo02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 33 + 3].Value = x.QtyRemain02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 34 + 3].Value = x.AmtRemain02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 35 + 3].Value = x.QtyRemainTo02;
                    Grd1.Cells[Grd1.Rows.Count - 1, 36 + 3].Value = x.AmtRemainTo02;
                    #endregion

                    #region maret
                    Grd1.Cells[Grd1.Rows.Count - 1, 37 + 3].Value = x.Rate03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 38 + 3].Value = x.QtyCBP03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 39 + 3].Value = x.AmtCBP03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 40 + 3].Value = x.QtyCBPTo03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 41 + 3].Value = x.AmtCBPTo03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 42 + 3].Value = x.QtyCAS03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 43 + 3].Value = x.AmtCAS03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 44 + 3].Value = x.QtyCASTo03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 45 + 3].Value = x.AmtCASTo03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 46 + 3].Value = x.QtyRemain03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 47 + 3].Value = x.AmtRemain03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 48 + 3].Value = x.QtyRemainTo03;
                    Grd1.Cells[Grd1.Rows.Count - 1, 49 + 3].Value = x.AmtRemainTo03;
                    #endregion

                    #region q1
                    Grd1.Cells[Grd1.Rows.Count - 1, 50 + 3].Value = x.RateQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 51 + 3].Value = x.QtyCBPQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 52 + 3].Value = x.AmtCBPQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 53 + 3].Value = x.QtyCBPToQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 54 + 3].Value = x.AmtCBPToQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 55 + 3].Value = x.QtyCASQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 56 + 3].Value = x.AmtCASQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 57 + 3].Value = x.QtyCASToQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 58 + 3].Value = x.AmtCASToQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 59 + 3].Value = x.QtyRemainQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 60 + 3].Value = x.AmtRemainQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 61 + 3].Value = x.QtyRemainToQ1;
                    Grd1.Cells[Grd1.Rows.Count - 1, 62 + 3].Value = x.AmtRemainToQ1;
                    #endregion

                    #region april
                    Grd1.Cells[Grd1.Rows.Count - 1, 63 + 3].Value = x.Rate04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 64 + 3].Value = x.QtyCBP04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 65 + 3].Value = x.AmtCBP04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 66 + 3].Value = x.QtyCBPTo04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 67 + 3].Value = x.AmtCBPTo04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 68 + 3].Value = x.QtyCAS04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 69 + 3].Value = x.AmtCAS04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 70 + 3].Value = x.QtyCASTo04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 71 + 3].Value = x.AmtCASTo04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 72 + 3].Value = x.QtyRemain04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 73 + 3].Value = x.AmtRemain04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 74 + 3].Value = x.QtyRemainTo04;
                    Grd1.Cells[Grd1.Rows.Count - 1, 75 + 3].Value = x.AmtRemainTo04;
                    #endregion

                    #region mei
                    Grd1.Cells[Grd1.Rows.Count - 1, 76 + 3].Value = x.Rate05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 77 + 3].Value = x.QtyCBP05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 78 + 3].Value = x.AmtCBP05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 79 + 3].Value = x.QtyCBPTo05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 80 + 3].Value = x.AmtCBPTo05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 81 + 3].Value = x.QtyCAS05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 82 + 3].Value = x.AmtCAS05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 83 + 3].Value = x.QtyCASTo05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 84 + 3].Value = x.AmtCASTo05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 85 + 3].Value = x.QtyRemain05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 86 + 3].Value = x.AmtRemain05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 87 + 3].Value = x.QtyRemainTo05;
                    Grd1.Cells[Grd1.Rows.Count - 1, 88 + 3].Value = x.AmtRemainTo05;
                    #endregion

                    #region juni
                    Grd1.Cells[Grd1.Rows.Count - 1, 89 + 3].Value = x.Rate06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 90 + 3].Value = x.QtyCBP06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 91 + 3].Value = x.AmtCBP06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 92 + 3].Value = x.QtyCBPTo06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 93 + 3].Value = x.AmtCBPTo06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 94 + 3].Value = x.QtyCAS06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 95 + 3].Value = x.AmtCAS06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 96 + 3].Value = x.QtyCASTo06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 97 + 3].Value = x.AmtCASTo06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 98 + 3].Value = x.QtyRemain06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 99 + 3].Value = x.AmtRemain06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 100 + 3].Value = x.QtyRemainTo06;
                    Grd1.Cells[Grd1.Rows.Count - 1, 101 + 3].Value = x.AmtRemainTo06;
                    #endregion

                    #region q2
                    Grd1.Cells[Grd1.Rows.Count - 1, 102 + 3].Value = x.RateQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 103 + 3].Value = x.QtyCBPQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 104 + 3].Value = x.AmtCBPQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 105 + 3].Value = x.QtyCBPToQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 106 + 3].Value = x.AmtCBPToQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 107 + 3].Value = x.QtyCASQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 108 + 3].Value = x.AmtCASQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 109 + 3].Value = x.QtyCASToQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 110 + 3].Value = x.AmtCASToQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 111 + 3].Value = x.QtyRemainQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 112 + 3].Value = x.AmtRemainQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 113 + 3].Value = x.QtyRemainToQ2;
                    Grd1.Cells[Grd1.Rows.Count - 1, 114 + 3].Value = x.AmtRemainToQ2;
                    #endregion

                    #region juli
                    Grd1.Cells[Grd1.Rows.Count - 1, 115 + 3].Value = x.Rate07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 116 + 3].Value = x.QtyCBP07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 117 + 3].Value = x.AmtCBP07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 118 + 3].Value = x.QtyCBPTo07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 119 + 3].Value = x.AmtCBPTo07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 120 + 3].Value = x.QtyCAS07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 121 + 3].Value = x.AmtCAS07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 122 + 3].Value = x.QtyCASTo07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 123 + 3].Value = x.AmtCASTo07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 124 + 3].Value = x.QtyRemain07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 125 + 3].Value = x.AmtRemain07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 126 + 3].Value = x.QtyRemainTo07;
                    Grd1.Cells[Grd1.Rows.Count - 1, 127 + 3].Value = x.AmtRemainTo07;
                    #endregion

                    #region agustus
                    Grd1.Cells[Grd1.Rows.Count - 1, 128 + 3].Value = x.Rate08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 129 + 3].Value = x.QtyCBP08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 130 + 3].Value = x.AmtCBP08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 131 + 3].Value = x.QtyCBPTo08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 132 + 3].Value = x.AmtCBPTo08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 133 + 3].Value = x.QtyCAS08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 134 + 3].Value = x.AmtCAS08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 135 + 3].Value = x.QtyCASTo08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 136 + 3].Value = x.AmtCASTo08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 137 + 3].Value = x.QtyRemain08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 138 + 3].Value = x.AmtRemain08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 139 + 3].Value = x.QtyRemainTo08;
                    Grd1.Cells[Grd1.Rows.Count - 1, 140 + 3].Value = x.AmtRemainTo08;
                    #endregion

                    #region september
                    Grd1.Cells[Grd1.Rows.Count - 1, 141 + 3].Value = x.Rate09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 142 + 3].Value = x.QtyCBP09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 143 + 3].Value = x.AmtCBP09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 144 + 3].Value = x.QtyCBPTo09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 145 + 3].Value = x.AmtCBPTo09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 146 + 3].Value = x.QtyCAS09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 147 + 3].Value = x.AmtCAS09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 148 + 3].Value = x.QtyCASTo09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 149 + 3].Value = x.AmtCASTo09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 150 + 3].Value = x.QtyRemain09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 151 + 3].Value = x.AmtRemain09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 152 + 3].Value = x.QtyRemainTo09;
                    Grd1.Cells[Grd1.Rows.Count - 1, 153 + 3].Value = x.AmtRemainTo09;
                    #endregion

                    #region q3
                    Grd1.Cells[Grd1.Rows.Count - 1, 154 + 3].Value = x.RateQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 155 + 3].Value = x.QtyCBPQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 156 + 3].Value = x.AmtCBPQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 157 + 3].Value = x.QtyCBPToQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 158 + 3].Value = x.AmtCBPToQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 159 + 3].Value = x.QtyCASQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 160 + 3].Value = x.AmtCASQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 161 + 3].Value = x.QtyCASToQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 162 + 3].Value = x.AmtCASToQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 163 + 3].Value = x.QtyRemainQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 164 + 3].Value = x.AmtRemainQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 165 + 3].Value = x.QtyRemainToQ3;
                    Grd1.Cells[Grd1.Rows.Count - 1, 166 + 3].Value = x.AmtRemainToQ3;
                    #endregion

                    #region oktober
                    Grd1.Cells[Grd1.Rows.Count - 1, 167 + 3].Value = x.Rate10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 168 + 3].Value = x.QtyCBP10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 169 + 3].Value = x.AmtCBP10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 170 + 3].Value = x.QtyCBPTo10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 171 + 3].Value = x.AmtCBPTo10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 172 + 3].Value = x.QtyCAS10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 173 + 3].Value = x.AmtCAS10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 174 + 3].Value = x.QtyCASTo10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 175 + 3].Value = x.AmtCASTo10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 176 + 3].Value = x.QtyRemain10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 177 + 3].Value = x.AmtRemain10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 178 + 3].Value = x.QtyRemainTo10;
                    Grd1.Cells[Grd1.Rows.Count - 1, 179 + 3].Value = x.AmtRemainTo10;
                    #endregion

                    #region november
                    Grd1.Cells[Grd1.Rows.Count - 1, 180 + 3].Value = x.Rate11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 181 + 3].Value = x.QtyCBP11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 182 + 3].Value = x.AmtCBP11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 183 + 3].Value = x.QtyCBPTo11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 184 + 3].Value = x.AmtCBPTo11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 185 + 3].Value = x.QtyCAS11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 186 + 3].Value = x.AmtCAS11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 187 + 3].Value = x.QtyCASTo11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 188 + 3].Value = x.AmtCASTo11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 189 + 3].Value = x.QtyRemain11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 190 + 3].Value = x.AmtRemain11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 191 + 3].Value = x.QtyRemainTo11;
                    Grd1.Cells[Grd1.Rows.Count - 1, 192 + 3].Value = x.AmtRemainTo11;
                    #endregion

                    #region desember
                    Grd1.Cells[Grd1.Rows.Count - 1, 193 + 3].Value = x.Rate12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 194 + 3].Value = x.QtyCBP12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 195 + 3].Value = x.AmtCBP12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 196 + 3].Value = x.QtyCBPTo12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 197 + 3].Value = x.AmtCBPTo12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 198 + 3].Value = x.QtyCAS12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 199 + 3].Value = x.AmtCAS12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 200 + 3].Value = x.QtyCASTo12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 201 + 3].Value = x.AmtCASTo12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 202 + 3].Value = x.QtyRemain12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 203 + 3].Value = x.AmtRemain12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 204 + 3].Value = x.QtyRemainTo12;
                    Grd1.Cells[Grd1.Rows.Count - 1, 205 + 3].Value = x.AmtRemainTo12;
                    #endregion

                    #region q4
                    Grd1.Cells[Grd1.Rows.Count - 1, 206 + 3].Value = x.RateQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 207 + 3].Value = x.QtyCBPQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 208 + 3].Value = x.AmtCBPQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 209 + 3].Value = x.QtyCBPToQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 210 + 3].Value = x.AmtCBPToQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 211 + 3].Value = x.QtyCASQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 212 + 3].Value = x.AmtCASQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 213 + 3].Value = x.QtyCASToQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 214 + 3].Value = x.AmtCASToQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 215 + 3].Value = x.QtyRemainQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 216 + 3].Value = x.AmtRemainQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 217 + 3].Value = x.QtyRemainToQ4;
                    Grd1.Cells[Grd1.Rows.Count - 1, 218 + 3].Value = x.AmtRemainToQ4;
                    #endregion

                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion
    }
}
