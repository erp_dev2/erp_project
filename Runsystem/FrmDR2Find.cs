﻿#region Update
/*
    13/11/2019 [HAR/IMS] bug saat join ke tabel customer
    27/11/2019 [WED/IMS] tambah kolom project code, project name, customer po#
    30/01/2020 [TKG/IMS] tambah project code, project name, po# (tidak perlu dari NTP)
 *  10/06/2021 [SET/IMS] Menambah ceklist exclude cancel ketika find DR for project
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDR2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmDR2 mFrmParent;
        string mSQL = string.Empty;
        bool mCBDInd = false;
       
        #endregion

        #region Constructor

        public FrmDR2Find(FrmDR2 FrmParent, bool CBDInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCBDInd = CBDInd;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.DocNo, A.DocDt, A.CancelInd, J.ItName AS LogisticItName, A.LocalDocNo, ");
            SQL.AppendLine("Case A.ProcessInd ");
            SQL.AppendLine("    When 'O' Then 'Outstanding'  ");
            SQL.AppendLine("    When 'F' Then 'FulFilled'  ");
            SQL.AppendLine("    When 'M' Then 'Manual FulFilled'  ");
            SQL.AppendLine("    When 'P' Then 'Partial FulFilled'  ");
            SQL.AppendLine("    When 'C' Then 'Cancelled'  ");
            SQL.AppendLine("END AS StatusDesc, C.WhsName, H.CtName, E.ItCode, I.ItCodeInternal, I.ItName, ");
            SQL.AppendLine("I.Specification, B.SODocNo, B.QtyPackagingUnit, B.Qty, B.QtyInventory, ");
            SQL.AppendLine("E.PackagingUnitUomCode, E.PackagingUnitUomCode PriceUomCode, I.InventoryUomCode, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("M.ProjectName, M.ProjectCode, D.PONo ");
            SQL.AppendLine("FROM TblDRHdr A ");
            SQL.AppendLine("INNER JOIN TblDRDtl B ON A.DocNo = B.DocNo AND A.DocType = '2' ");
            SQL.AppendLine("LEFT JOIN TblWarehouse C ON A.WhsCode = C.WhsCode ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr D ON B.SODocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractDtl E ON B.SODocNo = E.DocNo AND B.SODNo = E.DNo ");
            SQL.AppendLine("INNER JOIN TblBOQHdr F ON D.BOQDocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr G ON F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblCustomer H ON G.CtCode = H.CtCode ");
            SQL.AppendLine("INNER JOIN TblItem I ON E.ItCode = I.ItCode ");
            SQL.AppendLine("LEFT JOIN TblItem J ON A.ItCode = J.ItCode ");
            SQL.AppendLine("left JOIN tblsocontractrevisionhdr K ON A.DocNo=K.SOCDocNo");
            SQL.AppendLine("left JOIN tblnoticetoproceed L ON G.DocNo=L.LOPDocNo");
            SQL.AppendLine("left JOIN  tblprojectgroup M ON G.PGCode=M.PGCode");
            if (mCBDInd == true)
                SQL.AppendLine("INNER JOIN TblSalesInvoiceHdr K On D.Docno = K.SODocNo ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Customer",

                    //6-10
                    "Item's Code",
                    "",
                    "Item's"+Environment.NewLine+"Local Code",
                    "Item's Name",
                    "SO#",

                    //11-15
                    "",
                    "Quantity"+Environment.NewLine+"(Packaging Unit)",
                    "UoM"+Environment.NewLine+"(Packaging Unit)",
                    "Quantity"+Environment.NewLine+"(Sales)",
                    "UoM"+Environment.NewLine+"(Sales)",
                    
                    //16-20
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time",
                    
                    //21-25
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",
                    "Local"+Environment.NewLine+"Document",
                    "Logistic", 

                    //26-28
                    "Project Name",
                    "Project Code",
                    "Customer PO"
                }, 
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 50, 80, 200, 
                    
                    //6-10
                    80, 20, 80, 200, 130,
                    
                    //11-15
                    20, 100, 100, 100, 100, 

                    //16-20
                    100, 100, 100, 100, 100,

                    //21-25
                    100, 100, 100, 130, 250,

                    //26-28
                    200, 150, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 7, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Grd1.Cols[24].Move(2);
            Grd1.Cols[25].Move(7);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 18, 19, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                string Filter = string.Empty;
                Cursor.Current = Cursors.WaitCursor;
                if (mCBDInd == false)
                {
                    Filter = "Where D.DocNo Not In (Select SODocNo From TblSalesInvoiceHdr Where CancelInd = 'N' and SODocno Is not null ) ";
                }

                if (ChkExcCancelDoc.Checked)
                {
                    var mSQL2 = new StringBuilder();

                    mSQL2.AppendLine("And A.CancelInd = 'N' ");

                    Filter += mSQL2.ToString();
                }

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "G.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "B.SODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "I.ItCodeInternal", "I.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc",  "CtName", "ItCode",      
                            
                            //6-10
                            "ItCodeInternal", "ItName", "SODocNo", "QtyPackagingUnit", "PackagingUnitUomCode",  
                            
                            //11-15
                            "Qty", "PriceUomCode", "QtyInventory", "InventoryUomCode", "CreateBy",   
                
                            //16-20
                            "CreateDt", "LastUpBy", "LastUpDt", "LocalDocNo", "LogisticItName",

                            //21-23
                            "ProjectName", "ProjectCode", "PONo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion
    }
}
