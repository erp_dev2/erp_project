﻿#region Update
/*
  14/12/2020 [DITA/IMS] New Apps
  23/06/2021 [IQA/IMS] Menambah Kolom Local Document 
  23/07/2021 [BRI/IMS] Penyesuaian reporting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptARProjectMonitoring : RunSystem.FrmBase6
    {
        #region Field

        internal string
           mAccessInd = string.Empty,
           mMenuCode = string.Empty;
        private string
           mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptARProjectMonitoring(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, C.CtName, B.PONo, H.ProjectCode, H.ProjectName, A.SOContractDocNo, B.Amt+B.AmtBOM SOContractAmt, ");
            SQL.AppendLine("D.PtName, I.CurName, A.Amt TotalAmtDP, If(J.DocNo Is Null, 0, E.Amt) BeforeTax, E.TaxAmt, E.TaxAmt2, E.AfterTax, J.DocNo SLIForProjectDocNo, (ifnull(A.Amt, 0)- ifnull(E.AfterTax, 0)) Balance, A.LocalDocNo, ");
            SQL.AppendLine("E.ItCodeInternal, E.ItName, (A.Amt-(A.TaxAmt+A.TaxAmt2+A.TaxAmt3+A.TaxAmt4)) TotalBeforeTax, E.Amt AmtBeforeTax, (ifnull((A.Amt-A.TaxAmt), 0)- ifnull(E.Amt, 0)) Balance2 ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo And A.CancelInd='N'   ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode    ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode   ");
            SQL.AppendLine("LEFT JOIN    ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("   SELECT A.DocNo, B.DNo, B.Amt, B.TaxAmt, B.TaxAmt2, B.AfterTax, C.ItCodeInternal, C.ItName ");
            SQL.AppendLine("   FROM TblSOContractDownpaymentHdr A   ");
            SQL.AppendLine("   INNER JOIN TblSOContractDownpaymentDtl B ON A.DocNo = B.DocNo  And A.CancelInd='N'  ");
            SQL.AppendLine("   INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("   GROUP BY A.DocNo, B.DNo "); 

            SQL.AppendLine("   UNION ALL   ");

            SQL.AppendLine("   SELECT A.DocNo, B.DNo, B.Amt, B.TaxAmt, B.TaxAmt2, B.AfterTax, C.ItCodeInternal, C.ItName ");
            SQL.AppendLine("   FROM TblSOContractDownpaymentHdr A   ");
            SQL.AppendLine("   INNER JOIN TblSOContractDownpaymentDtl2 B ON A.DocNo = B.DocNo  And A.CancelInd='N'  ");
            SQL.AppendLine("   INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("   GROUP BY A.DocNo, B.DNo "); 

            SQL.AppendLine(") E ON A.DocNo =E.DocNo   ");
            SQL.AppendLine("Inner Join TblBOQHdr F On B.BOQDocNo=F.DocNo   ");
            SQL.AppendLine("Inner Join TblLOphdr G On F.LOPDocNo=G.DocNO   ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup H ON G.PGCode = H.PGCode   ");
            SQL.AppendLine("LEFT JOIN TblCurrency I ON A.CurCode = I.CurCode    "); 
            SQL.AppendLine("Left JOIN TblSOContractDownpayment2Hdr J On A.DocNo = J.SOContractDownpaymentDocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2   ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Document#",
                        "Customer",
                        "Customer PO#",
                        "Project Code",

                        //6-10
                        "Project Name",
                        "SO Contract#",
                        "SO Contract"+Environment.NewLine+"Amount",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",

                        //11-15
                        "Sales Invoice" +Environment.NewLine+"For Project",
                        "Amount",
                        "Tax 1",
                        "Tax 2",
                        "Total Amount DP",

                        //16-20
                        "Amount DP" +Environment.NewLine+"Movement",
                        "Balance DP" +Environment.NewLine+"Amount",
                        "Local Document#",
                        "Local Code",
                        "Item's Name",

                        //21-23
                        "Total Amount DP"+Environment.NewLine+"Exclude Tax",
                        "Amount DP Movement"+Environment.NewLine+"Exclude Tax",
                        "Balance DP Amount"+Environment.NewLine+"Exclude Tax",
                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        120, 180, 200, 100, 100,

                        //6-10
                        150, 200, 150, 200, 110, 

                        //11-15
                        150, 120, 120, 120, 150,

                        //16-20
                        120, 120, 120, 150, 150,

                        //21-23
                        150, 150, 150,
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 12, 13, 14, 15, 16, 17, 21, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, false);
            Grd1.Cols[18].Move(3);
            Grd1.Cols[19].Move(13);
            Grd1.Cols[20].Move(14);
            Grd1.Cols[21].Move(18);
            Grd1.Cols[22].Move(20);
            Grd1.Cols[23].Move(22);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, new string[] { "A.SOContractDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "H.ProjectName", "H.ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, new string[] { "B.PONo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Group By A.DocNo, E.DNo Order By A.DocNo, A.DocDt;",
                        new string[]
                        { 
                            //0
                            "DocDt", 

                            //1-5
                            "DocNo", "CtName", "PONo", "ProjectCode", "ProjectName",
                            
                            //6-10
                            "SOContractDocNo", "SOContractAmt", "PtName", "CurName", "SLIForProjectDocNo", 

                            //11-15
                            "BeforeTax", "TaxAmt", "TaxAmt2", "TotalAmtDP","AfterTax",

                            //16-20
                            "Balance", "LocalDocNo", "ItCodeInternal", "ItName", "TotalBeforeTax",

                            //21-22
                            "AmtBeforeTax", "Balance2",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                          
                        }, true, false, false, false
                    );
                    Process1();
                 }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void Process1()
        {
            if(Grd1.Rows.Count > 1)
            {
                string ARDPDocNo = string.Empty;
                for (int i = 1; i < Grd1.Rows.Count; i++)
                {
                    if (ARDPDocNo.Length == 0) ARDPDocNo = Sm.GetGrdStr(Grd1, i, 2);
                    if (ARDPDocNo == Sm.GetGrdStr(Grd1, i, 2))
                    {
                        Grd1.Cells[i, 23].Value = Sm.GetGrdDec(Grd1, i - 1, 23) - Sm.GetGrdDec(Grd1, i, 22);
                    }
                    else
                    {
                        ARDPDocNo = Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }
        }

        #endregion
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SLI For Project#");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
