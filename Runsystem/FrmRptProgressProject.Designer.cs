﻿namespace RunSystem
{
    partial class FrmRptProgressProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkAchievementDt = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueProjectCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteAchievementDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkProjectCode = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPDDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPDDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDeviation = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDeviation = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDeviation2 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAchievementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAchievementDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAchievementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPDDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPDDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeviation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeviation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeviation2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtDeviation2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.ChkDeviation);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDeviation);
            this.panel2.Controls.Add(this.ChkPDDocNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPDDocNo);
            this.panel2.Controls.Add(this.ChkDocNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(772, 93);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 380);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 93);
            this.panel3.Size = new System.Drawing.Size(772, 380);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkAchievementDt);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.LueProjectCode);
            this.panel4.Controls.Add(this.DteAchievementDt);
            this.panel4.Controls.Add(this.ChkProjectCode);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(472, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(296, 89);
            this.panel4.TabIndex = 0;
            // 
            // ChkAchievementDt
            // 
            this.ChkAchievementDt.Location = new System.Drawing.Point(222, 23);
            this.ChkAchievementDt.Name = "ChkAchievementDt";
            this.ChkAchievementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAchievementDt.Properties.Appearance.Options.UseFont = true;
            this.ChkAchievementDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAchievementDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAchievementDt.Properties.Caption = " ";
            this.ChkAchievementDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAchievementDt.Size = new System.Drawing.Size(19, 22);
            this.ChkAchievementDt.TabIndex = 36;
            this.ChkAchievementDt.ToolTip = "Remove filter";
            this.ChkAchievementDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAchievementDt.ToolTipTitle = "Run System";
            this.ChkAchievementDt.CheckedChanged += new System.EventHandler(this.ChkAchievementDt_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(6, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 14);
            this.label8.TabIndex = 34;
            this.label8.Text = "Achievement Date";
            // 
            // LueProjectCode
            // 
            this.LueProjectCode.EnterMoveNextControl = true;
            this.LueProjectCode.Location = new System.Drawing.Point(119, 4);
            this.LueProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectCode.Name = "LueProjectCode";
            this.LueProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.Appearance.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectCode.Properties.DropDownRows = 30;
            this.LueProjectCode.Properties.NullText = "[Empty]";
            this.LueProjectCode.Properties.PopupWidth = 300;
            this.LueProjectCode.Size = new System.Drawing.Size(152, 20);
            this.LueProjectCode.TabIndex = 27;
            this.LueProjectCode.ToolTip = "F4 : Show/hide list";
            this.LueProjectCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectCode.EditValueChanged += new System.EventHandler(this.LueProjectCode_EditValueChanged);
            // 
            // DteAchievementDt
            // 
            this.DteAchievementDt.EditValue = null;
            this.DteAchievementDt.EnterMoveNextControl = true;
            this.DteAchievementDt.Location = new System.Drawing.Point(119, 25);
            this.DteAchievementDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteAchievementDt.Name = "DteAchievementDt";
            this.DteAchievementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAchievementDt.Properties.Appearance.Options.UseFont = true;
            this.DteAchievementDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAchievementDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteAchievementDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteAchievementDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteAchievementDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAchievementDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteAchievementDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAchievementDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteAchievementDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteAchievementDt.Size = new System.Drawing.Size(101, 20);
            this.DteAchievementDt.TabIndex = 35;
            this.DteAchievementDt.EditValueChanged += new System.EventHandler(this.DteAchievementDt_EditValueChanged);
            // 
            // ChkProjectCode
            // 
            this.ChkProjectCode.Location = new System.Drawing.Point(273, 3);
            this.ChkProjectCode.Name = "ChkProjectCode";
            this.ChkProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProjectCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProjectCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProjectCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProjectCode.Properties.Caption = " ";
            this.ChkProjectCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProjectCode.Size = new System.Drawing.Size(19, 22);
            this.ChkProjectCode.TabIndex = 28;
            this.ChkProjectCode.ToolTip = "Remove filter";
            this.ChkProjectCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProjectCode.ToolTipTitle = "Run System";
            this.ChkProjectCode.CheckedChanged += new System.EventHandler(this.ChkProjectCode_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(30, 7);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 14);
            this.label7.TabIndex = 26;
            this.label7.Text = "Project Status";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(215, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(74, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 20;
            this.label1.Text = "Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(227, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 23;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(110, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 21;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(25, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 14);
            this.label6.TabIndex = 24;
            this.label6.Text = "Project Impl#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(110, 25);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDocNo.TabIndex = 25;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // ChkDocNo
            // 
            this.ChkDocNo.Location = new System.Drawing.Point(353, 24);
            this.ChkDocNo.Name = "ChkDocNo";
            this.ChkDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNo.Properties.Caption = " ";
            this.ChkDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNo.TabIndex = 26;
            this.ChkDocNo.ToolTip = "Remove filter";
            this.ChkDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNo.ToolTipTitle = "Run System";
            this.ChkDocNo.CheckedChanged += new System.EventHandler(this.ChkDocNo_CheckedChanged);
            // 
            // ChkPDDocNo
            // 
            this.ChkPDDocNo.Location = new System.Drawing.Point(353, 45);
            this.ChkPDDocNo.Name = "ChkPDDocNo";
            this.ChkPDDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPDDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkPDDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPDDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPDDocNo.Properties.Caption = " ";
            this.ChkPDDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPDDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkPDDocNo.TabIndex = 29;
            this.ChkPDDocNo.ToolTip = "Remove filter";
            this.ChkPDDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPDDocNo.ToolTipTitle = "Run System";
            this.ChkPDDocNo.CheckedChanged += new System.EventHandler(this.ChkPDDocNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 14);
            this.label2.TabIndex = 27;
            this.label2.Text = "Project Delivery#";
            // 
            // TxtPDDocNo
            // 
            this.TxtPDDocNo.EnterMoveNextControl = true;
            this.TxtPDDocNo.Location = new System.Drawing.Point(110, 46);
            this.TxtPDDocNo.Name = "TxtPDDocNo";
            this.TxtPDDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPDDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPDDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPDDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPDDocNo.Properties.MaxLength = 30;
            this.TxtPDDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtPDDocNo.TabIndex = 28;
            this.TxtPDDocNo.Validated += new System.EventHandler(this.TxtPDDocNo_Validated);
            // 
            // ChkDeviation
            // 
            this.ChkDeviation.Location = new System.Drawing.Point(249, 67);
            this.ChkDeviation.Name = "ChkDeviation";
            this.ChkDeviation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeviation.Properties.Appearance.Options.UseFont = true;
            this.ChkDeviation.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeviation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeviation.Properties.Caption = " ";
            this.ChkDeviation.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeviation.Size = new System.Drawing.Size(19, 22);
            this.ChkDeviation.TabIndex = 32;
            this.ChkDeviation.ToolTip = "Remove filter";
            this.ChkDeviation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeviation.ToolTipTitle = "Run System";
            this.ChkDeviation.CheckedChanged += new System.EventHandler(this.ChkDeviation_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(21, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 14);
            this.label4.TabIndex = 30;
            this.label4.Text = "Task Deviation";
            // 
            // TxtDeviation
            // 
            this.TxtDeviation.EnterMoveNextControl = true;
            this.TxtDeviation.Location = new System.Drawing.Point(110, 67);
            this.TxtDeviation.Name = "TxtDeviation";
            this.TxtDeviation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeviation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeviation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeviation.Properties.Appearance.Options.UseFont = true;
            this.TxtDeviation.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDeviation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDeviation.Properties.MaxLength = 30;
            this.TxtDeviation.Size = new System.Drawing.Size(41, 20);
            this.TxtDeviation.TabIndex = 31;
            this.TxtDeviation.Validated += new System.EventHandler(this.TxtDeviation_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(212, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 14);
            this.label5.TabIndex = 33;
            this.label5.Text = "days";
            // 
            // TxtDeviation2
            // 
            this.TxtDeviation2.EnterMoveNextControl = true;
            this.TxtDeviation2.Location = new System.Drawing.Point(170, 67);
            this.TxtDeviation2.Name = "TxtDeviation2";
            this.TxtDeviation2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeviation2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeviation2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeviation2.Properties.Appearance.Options.UseFont = true;
            this.TxtDeviation2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDeviation2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDeviation2.Properties.MaxLength = 30;
            this.TxtDeviation2.Size = new System.Drawing.Size(41, 20);
            this.TxtDeviation2.TabIndex = 34;
            this.TxtDeviation2.Validated += new System.EventHandler(this.TxtDeviation2_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(155, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 14);
            this.label9.TabIndex = 35;
            this.label9.Text = "-";
            // 
            // FrmRptProgressProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptProgressProject";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAchievementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAchievementDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAchievementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPDDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPDDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeviation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeviation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeviation2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label label6;
        protected internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDocNo;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkDeviation;
        private System.Windows.Forms.Label label4;
        protected internal DevExpress.XtraEditors.TextEdit TxtDeviation;
        private DevExpress.XtraEditors.CheckEdit ChkPDDocNo;
        private System.Windows.Forms.Label label2;
        protected internal DevExpress.XtraEditors.TextEdit TxtPDDocNo;
        internal DevExpress.XtraEditors.LookUpEdit LueProjectCode;
        private DevExpress.XtraEditors.CheckEdit ChkProjectCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkAchievementDt;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteAchievementDt;
        private System.Windows.Forms.Label label9;
        protected internal DevExpress.XtraEditors.TextEdit TxtDeviation2;
    }
}