﻿#region Update
/*
    20/09/17 [ARI] tambah kolom Remark
    27/04/2020 [VIN/MMM] tambah kolom mtsdocno & quntity
    13/10/2020 [IBL/MGI] membuat highlight untuk row yang usage datenya = H-1
 *  17/03/2021 [ICA/KSM] bisa menarik data/dokumen Sales Contract 
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionCalendar : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSalesContractEnabled = false;

        #endregion

        #region Constructor

        public FrmRptProductionCalendar(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSalesContractEnabled = Sm.GetParameterBoo("IsSalesContractEnabled");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

           
            SQL.AppendLine("SELECT IFNULL(E.MakeToStockDocNo, F.SODocNo) MakeToStockDocNo, IFNULL(E.Qty, IfNull(F.Qty, 0.00)) MakeToStockQty, A.Docno, A.DocDt, ");
            SQL.AppendLine("Case A.Status when 'P' Then 'Planning' ");
            SQL.AppendLine("when 'R' Then 'Released'end as Status, ");
            SQL.AppendLine("B.ProductionOrderDocNo, C.ItCode, D.Itname, ");
            SQL.AppendLine("C.Qty, D.PlanningUomCode, (C.Qty*D.PlanningUomCodeConvert12) As Qty2, D.PlanningUomCode2, A.Remark, E.UsageDt ");
            SQL.AppendLine("From TblPPhdr A ");
            SQL.AppendLine("Inner Join TblPpDtl B On A.DocNo = B.docNo ");
            SQL.AppendLine("Inner Join TblProductionorderHdr C On B.ProductionOrderDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT T1.DocNo, T1.MakeToStockDocNo, T3.Qty, T3.UsageDt ");
            SQL.AppendLine("FROM TblProductionOrderHdr T1 ");
            SQL.AppendLine("INNER JOIN TblMakeToStockHdr T2 ON T1.MakeToStockDocNo = T2.DocNo ");
            SQL.AppendLine("AND T1.MakeToStockDocNo IS NOT NULL ");
            SQL.AppendLine("INNER JOIN TblMakeToStockDtl T3 ON T1.MakeToStockDocNo = T3.DocNo ");
            SQL.AppendLine("AND T1.MakeToStockDNo = T3.DNo ");
            SQL.AppendLine(") E ON C.DocNo = E.DocNo ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.DocNo, T1.SODocNo as SODocNo, T3.Qty ");
            SQL.AppendLine("    FROM TblProductionOrderHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblSOHdr T2 ON T1.SODocNo = T2.DocNo ");
            SQL.AppendLine("        AND T1.SODocNo IS NOT NULL             ");
            SQL.AppendLine("    INNER JOIN TblSODtl T3 ON T1.SODocNo = T3.DocNo ");
            SQL.AppendLine("        AND T1.SODNo = T3.DNo ");
            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("    SELECT T1.DocNo, T1.SCDocNo as SODocNo, T3.Qty ");
                SQL.AppendLine("    FROM TblProductionOrderHdr T1 ");
                SQL.AppendLine("    INNER JOIN TblSalesContract T2 ON T1.SCDocNo = T2.DocNo ");
                SQL.AppendLine("        AND T1.SCDocNo IS NOT NULL             ");
                SQL.AppendLine("    INNER JOIN TblSalesMemoDtl T3 ON T2.SalesMemoDocNo = T3.DocNo ");
                SQL.AppendLine("        AND T1.SMDNo = T3.DNo ");
            }
            SQL.AppendLine(") F ON C.DocNo = F.DocNo ");

            SQL.AppendLine("Where A.cancelInd = 'N' ");



            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        (mIsSalesContractEnabled ? "SC/SO/MTS" : "SO/MTS"),
                        "Quantity",
                        "Production"+Environment.NewLine+"Order",
                        "Production"+Environment.NewLine+"Planning", 
                        "",
                        
                        
                        //6-10
                        "Date",
                        "Status",
                        "",
                        "Item's Code",
                        "",
                        

                        //11-15
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        

                        //16-17
                        "Remark",
                        "Usage Date"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 200, 150, 20,   
                        
                        //6-10
                        100, 80, 20, 100, 20, 

                        //11-15
                        250, 80, 80, 80, 80, 

                        //16-17
                        150, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 8, 10 });
            Sm.GrdFormatDec(Grd1, new int[] {2, 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 9, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 17 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "MakeToStockDocNo",

                            //1-5
                            "MakeToStockQty", "DocNo", "DocDt", "status", "ProductionOrderDocno", 

                            //6-10
                            "ItCode", "ItName", "Qty", "PlanningUomCode", "Qty2", 
                            //11-13
                            "PlanningUomCode2", "Remark", "UsageDt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 13);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(6);
                Grd1.Group();
                AdjustSubtotals();
                CompareUsageDate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 12, 14 });
        }

        private void CompareUsageDate()
        {
            decimal mCurrentDt = decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8));
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0 && Sm.GetGrdDate(Grd1, Row, 17).Length !=0)
                    {
                        if (decimal.Parse(Sm.Left(Sm.GetGrdDate(Grd1, Row, 17), 8)) - mCurrentDt == 1)
                        {
                            Grd1.Rows[Row].BackColor = Color.Red;
                        }
                    }
                }
            }
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion
    }
}
