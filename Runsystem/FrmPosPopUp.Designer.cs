﻿namespace RunSystem
{
    partial class FrmPosPopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlMain = new System.Windows.Forms.Panel();
            this.PnlAmount = new System.Windows.Forms.Panel();
            this.TxtAmount = new DevExpress.XtraEditors.TextEdit();
            this.LblAmount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOK = new System.Windows.Forms.Button();
            this.PnlRemark = new System.Windows.Forms.Panel();
            this.rTxtRemark = new System.Windows.Forms.RichTextBox();
            this.LblRemark = new System.Windows.Forms.Label();
            this.PnlMain.SuspendLayout();
            this.PnlAmount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmount.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.PnlRemark.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.PnlRemark);
            this.PnlMain.Controls.Add(this.PnlAmount);
            this.PnlMain.Controls.Add(this.panel1);
            this.PnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlMain.Location = new System.Drawing.Point(0, 0);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(388, 191);
            this.PnlMain.TabIndex = 1;
            // 
            // PnlAmount
            // 
            this.PnlAmount.Controls.Add(this.TxtAmount);
            this.PnlAmount.Controls.Add(this.LblAmount);
            this.PnlAmount.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlAmount.Location = new System.Drawing.Point(0, 0);
            this.PnlAmount.Name = "PnlAmount";
            this.PnlAmount.Size = new System.Drawing.Size(386, 51);
            this.PnlAmount.TabIndex = 15;
            // 
            // TxtAmount
            // 
            this.TxtAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TxtAmount.EnterMoveNextControl = true;
            this.TxtAmount.Location = new System.Drawing.Point(167, 12);
            this.TxtAmount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmount.Name = "TxtAmount";
            this.TxtAmount.Properties.Appearance.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmount.Properties.Appearance.Options.UseFont = true;
            this.TxtAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TxtAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TxtAmount.Properties.Mask.EditMask = "##,##0.00";
            this.TxtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtAmount.Properties.MaxLength = 40;
            this.TxtAmount.Size = new System.Drawing.Size(206, 30);
            this.TxtAmount.TabIndex = 45;
            this.TxtAmount.EditValueChanged += new System.EventHandler(this.TxtAmount_EditValueChanged);
            this.TxtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtAmount_KeyPress);
            // 
            // LblAmount
            // 
            this.LblAmount.AutoSize = true;
            this.LblAmount.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAmount.Location = new System.Drawing.Point(30, 15);
            this.LblAmount.Name = "LblAmount";
            this.LblAmount.Size = new System.Drawing.Size(65, 21);
            this.LblAmount.TabIndex = 13;
            this.LblAmount.Text = "Amount";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnCancel);
            this.panel1.Controls.Add(this.BtnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 138);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 51);
            this.panel1.TabIndex = 13;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(167, 9);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(137, 31);
            this.BtnCancel.TabIndex = 13;
            this.BtnCancel.TabStop = false;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOK.Location = new System.Drawing.Point(26, 9);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(135, 31);
            this.BtnOK.TabIndex = 12;
            this.BtnOK.TabStop = false;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // PnlRemark
            // 
            this.PnlRemark.Controls.Add(this.rTxtRemark);
            this.PnlRemark.Controls.Add(this.LblRemark);
            this.PnlRemark.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlRemark.Location = new System.Drawing.Point(0, 51);
            this.PnlRemark.Name = "PnlRemark";
            this.PnlRemark.Size = new System.Drawing.Size(386, 87);
            this.PnlRemark.TabIndex = 18;
            // 
            // rTxtRemark
            // 
            this.rTxtRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rTxtRemark.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTxtRemark.Location = new System.Drawing.Point(167, 9);
            this.rTxtRemark.Name = "rTxtRemark";
            this.rTxtRemark.Size = new System.Drawing.Size(206, 70);
            this.rTxtRemark.TabIndex = 38;
            this.rTxtRemark.Text = "";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(30, 9);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(65, 21);
            this.LblRemark.TabIndex = 13;
            this.LblRemark.Text = "Remark";
            // 
            // FrmPosPopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 191);
            this.Controls.Add(this.PnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPosPopUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmPosGetAmount_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlAmount.ResumeLayout(false);
            this.PnlAmount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmount.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.PnlRemark.ResumeLayout(false);
            this.PnlRemark.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Panel PnlAmount;
        private System.Windows.Forms.Label LblAmount;
        public DevExpress.XtraEditors.TextEdit TxtAmount;
        private System.Windows.Forms.Panel PnlRemark;
        public System.Windows.Forms.RichTextBox rTxtRemark;
        private System.Windows.Forms.Label LblRemark;
    }
}