﻿namespace RunSystem
{
    partial class FrmProjectImplementation3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjectImplementation3));
            this.panel3 = new System.Windows.Forms.Panel();
            this.MeeApprovalRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnBOQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtAchievement = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtPICSales = new DevExpress.XtraEditors.TextEdit();
            this.TxtEstimatedCost = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtTotalRev = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtType = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TpgDocument = new System.Windows.Forms.TabPage();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgIssue = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgResource = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtDirectPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtIndirectPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalResourcePerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtRemunerationPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtIndirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtCostPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalResource = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtDirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtRemunerationCost = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.BtnImportResource = new DevExpress.XtraEditors.SimpleButton();
            this.TpgWBS2 = new System.Windows.Forms.TabPage();
            this.DtePlanEndDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DtePlanStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueTaskCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStageCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtContractAmtBefTax2 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtTotalWeight2 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtTotalPrice2 = new DevExpress.XtraEditors.TextEdit();
            this.TpgWBS = new System.Windows.Forms.TabPage();
            this.DtePlanEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DtePlanStartDt = new DevExpress.XtraEditors.DateEdit();
            this.LueStageCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtContractAmtBefTax = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtTotalWeight = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTotalPrice = new DevExpress.XtraEditors.TextEdit();
            this.TcProjectImplementation = new System.Windows.Forms.TabControl();
            this.TpgApproval = new System.Windows.Forms.TabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgUploadFile = new System.Windows.Forms.TabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAchievement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICSales.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimatedCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalRev.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            this.TpgDocument.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgResource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResourcePerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).BeginInit();
            this.TpgWBS2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeight2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice2.Properties)).BeginInit();
            this.TpgWBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).BeginInit();
            this.TcProjectImplementation.SuspendLayout();
            this.TpgApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgUploadFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(917, 0);
            this.panel1.Size = new System.Drawing.Size(70, 525);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcProjectImplementation);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(917, 525);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.MeeApprovalRemark);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.LueProcessInd);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.TxtBOQDocNo);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.BtnLOPDocNo);
            this.panel3.Controls.Add(this.BtnBOQDocNo);
            this.panel3.Controls.Add(this.TxtSOContractDocNo);
            this.panel3.Controls.Add(this.TxtAchievement);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TxtLOPDocNo);
            this.panel3.Controls.Add(this.BtnSOContractDocNo);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.BtnSOContractDocNo2);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(917, 219);
            this.panel3.TabIndex = 10;
            // 
            // MeeApprovalRemark
            // 
            this.MeeApprovalRemark.EnterMoveNextControl = true;
            this.MeeApprovalRemark.Location = new System.Drawing.Point(160, 68);
            this.MeeApprovalRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeApprovalRemark.Name = "MeeApprovalRemark";
            this.MeeApprovalRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeApprovalRemark.Properties.MaxLength = 700;
            this.MeeApprovalRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeApprovalRemark.Properties.ShowIcon = false;
            this.MeeApprovalRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeApprovalRemark.TabIndex = 18;
            this.MeeApprovalRemark.ToolTip = "F4 : Show/hide text";
            this.MeeApprovalRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeApprovalRemark.ToolTipTitle = "Run System";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(49, 70);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(106, 14);
            this.label37.TabIndex = 17;
            this.label37.Text = "Approval\'s Remark";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(160, 89);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 30;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 300;
            this.LueProcessInd.Size = new System.Drawing.Size(230, 20);
            this.LueProcessInd.TabIndex = 20;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(107, 90);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 14);
            this.label11.TabIndex = 19;
            this.label11.Text = "Process";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(160, 47);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(230, 20);
            this.TxtStatus.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(113, 49);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "Status";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(160, 152);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 30;
            this.TxtBOQDocNo.Properties.ReadOnly = true;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtBOQDocNo.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(392, 196);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 14);
            this.label9.TabIndex = 36;
            this.label9.Text = "%";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(391, 173);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnLOPDocNo.TabIndex = 33;
            this.BtnLOPDocNo.ToolTip = "Show LOP";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // BtnBOQDocNo
            // 
            this.BtnBOQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBOQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBOQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBOQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseFont = true;
            this.BtnBOQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBOQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBOQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBOQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOQDocNo.Image")));
            this.BtnBOQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBOQDocNo.Location = new System.Drawing.Point(391, 152);
            this.BtnBOQDocNo.Name = "BtnBOQDocNo";
            this.BtnBOQDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnBOQDocNo.TabIndex = 30;
            this.BtnBOQDocNo.ToolTip = "Show BOQ";
            this.BtnBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBOQDocNo.ToolTipTitle = "Run System";
            this.BtnBOQDocNo.Click += new System.EventHandler(this.BtnBOQDocNo_Click);
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(160, 131);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtSOContractDocNo.TabIndex = 25;
            // 
            // TxtAchievement
            // 
            this.TxtAchievement.EnterMoveNextControl = true;
            this.TxtAchievement.Location = new System.Drawing.Point(160, 194);
            this.TxtAchievement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAchievement.Name = "TxtAchievement";
            this.TxtAchievement.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAchievement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAchievement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAchievement.Properties.Appearance.Options.UseFont = true;
            this.TxtAchievement.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAchievement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAchievement.Properties.MaxLength = 30;
            this.TxtAchievement.Properties.ReadOnly = true;
            this.TxtAchievement.Size = new System.Drawing.Size(230, 20);
            this.TxtAchievement.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(76, 195);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 14);
            this.label8.TabIndex = 34;
            this.label8.Text = "Achievement";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(72, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "SO Contract#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(160, 173);
            this.TxtLOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 30;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtLOPDocNo.TabIndex = 32;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(391, 133);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnSOContractDocNo.TabIndex = 26;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 112);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 21;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(117, 174);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 31;
            this.label7.Text = "LOP#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(414, 134);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(24, 18);
            this.BtnSOContractDocNo2.TabIndex = 27;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(160, 110);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(230, 20);
            this.MeeCancelReason.TabIndex = 22;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(392, 110);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 23;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(160, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(82, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(114, 153);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "BOQ#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(160, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(122, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtPICSales);
            this.panel5.Controls.Add(this.TxtEstimatedCost);
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.TxtTotalRev);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.TxtType);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.TxtProjectCode);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.TxtProjectName);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueCtCode);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.LueCtContactPersonName);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(471, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(442, 215);
            this.panel5.TabIndex = 37;
            // 
            // TxtPICSales
            // 
            this.TxtPICSales.EnterMoveNextControl = true;
            this.TxtPICSales.Location = new System.Drawing.Point(207, 92);
            this.TxtPICSales.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICSales.Name = "TxtPICSales";
            this.TxtPICSales.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPICSales.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICSales.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICSales.Properties.Appearance.Options.UseFont = true;
            this.TxtPICSales.Properties.MaxLength = 40;
            this.TxtPICSales.Properties.ReadOnly = true;
            this.TxtPICSales.Size = new System.Drawing.Size(229, 20);
            this.TxtPICSales.TabIndex = 47;
            // 
            // TxtEstimatedCost
            // 
            this.TxtEstimatedCost.EnterMoveNextControl = true;
            this.TxtEstimatedCost.Location = new System.Drawing.Point(207, 180);
            this.TxtEstimatedCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstimatedCost.Name = "TxtEstimatedCost";
            this.TxtEstimatedCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEstimatedCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstimatedCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstimatedCost.Properties.Appearance.Options.UseFont = true;
            this.TxtEstimatedCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstimatedCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstimatedCost.Properties.ReadOnly = true;
            this.TxtEstimatedCost.Size = new System.Drawing.Size(230, 20);
            this.TxtEstimatedCost.TabIndex = 54;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(26, 182);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(177, 14);
            this.label39.TabIndex = 53;
            this.label39.Text = "Total Estimated Cost Recorded";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalRev
            // 
            this.TxtTotalRev.EnterMoveNextControl = true;
            this.TxtTotalRev.Location = new System.Drawing.Point(207, 158);
            this.TxtTotalRev.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalRev.Name = "TxtTotalRev";
            this.TxtTotalRev.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalRev.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalRev.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalRev.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalRev.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalRev.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalRev.Properties.ReadOnly = true;
            this.TxtTotalRev.Size = new System.Drawing.Size(230, 20);
            this.TxtTotalRev.TabIndex = 52;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(72, 160);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(131, 14);
            this.label38.TabIndex = 51;
            this.label38.Text = "Total Settled Revenue";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtType
            // 
            this.TxtType.EnterMoveNextControl = true;
            this.TxtType.Location = new System.Drawing.Point(207, 114);
            this.TxtType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtType.Name = "TxtType";
            this.TxtType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtType.Properties.Appearance.Options.UseFont = true;
            this.TxtType.Properties.MaxLength = 30;
            this.TxtType.Properties.ReadOnly = true;
            this.TxtType.Size = new System.Drawing.Size(230, 20);
            this.TxtType.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(125, 116);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 14);
            this.label17.TabIndex = 48;
            this.label17.Text = "Project Type";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(207, 7);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 30;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(230, 20);
            this.TxtProjectCode.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(125, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 14);
            this.label15.TabIndex = 38;
            this.label15.Text = "Project Code";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(207, 28);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(230, 20);
            this.TxtProjectName.TabIndex = 41;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(122, 32);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 14);
            this.label12.TabIndex = 40;
            this.label12.Text = "Project Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(207, 49);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 43;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(144, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 42;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(56, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 44;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(156, 139);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 50;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(147, 94);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 14);
            this.label16.TabIndex = 46;
            this.label16.Text = "PIC Sales";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(207, 70);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 30;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 300;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(230, 20);
            this.LueCtContactPersonName.TabIndex = 45;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(207, 136);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeRemark.TabIndex = 32;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TpgDocument
            // 
            this.TpgDocument.Controls.Add(this.DteDocDt2);
            this.TpgDocument.Controls.Add(this.Grd4);
            this.TpgDocument.Location = new System.Drawing.Point(4, 26);
            this.TpgDocument.Name = "TpgDocument";
            this.TpgDocument.Padding = new System.Windows.Forms.Padding(3);
            this.TpgDocument.Size = new System.Drawing.Size(764, 0);
            this.TpgDocument.TabIndex = 2;
            this.TpgDocument.Text = "Document";
            this.TpgDocument.UseVisualStyleBackColor = true;
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(71, 30);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(138, 20);
            this.DteDocDt2.TabIndex = 42;
            this.DteDocDt2.Visible = false;
            this.DteDocDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDocDt2_KeyDown);
            this.DteDocDt2.Leave += new System.EventHandler(this.DteDocDt2_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 3);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(758, 0);
            this.Grd4.TabIndex = 45;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgIssue
            // 
            this.TpgIssue.Controls.Add(this.Grd3);
            this.TpgIssue.Location = new System.Drawing.Point(4, 26);
            this.TpgIssue.Name = "TpgIssue";
            this.TpgIssue.Padding = new System.Windows.Forms.Padding(3);
            this.TpgIssue.Size = new System.Drawing.Size(764, 0);
            this.TpgIssue.TabIndex = 3;
            this.TpgIssue.Text = "Issue";
            this.TpgIssue.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 3);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 0);
            this.Grd3.TabIndex = 45;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgResource
            // 
            this.TpgResource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgResource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgResource.Controls.Add(this.Grd2);
            this.TpgResource.Controls.Add(this.panel6);
            this.TpgResource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgResource.Location = new System.Drawing.Point(4, 26);
            this.TpgResource.Name = "TpgResource";
            this.TpgResource.Size = new System.Drawing.Size(764, 0);
            this.TpgResource.TabIndex = 0;
            this.TpgResource.Text = "Resource";
            this.TpgResource.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 120);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 0);
            this.Grd2.TabIndex = 77;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label47);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.label36);
            this.panel6.Controls.Add(this.TxtDirectPerc);
            this.panel6.Controls.Add(this.TxtIndirectPerc);
            this.panel6.Controls.Add(this.TxtTotalResourcePerc);
            this.panel6.Controls.Add(this.TxtRemunerationPerc);
            this.panel6.Controls.Add(this.TxtIndirectCost);
            this.panel6.Controls.Add(this.label35);
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Controls.Add(this.TxtTotalResource);
            this.panel6.Controls.Add(this.label44);
            this.panel6.Controls.Add(this.TxtDirectCost);
            this.panel6.Controls.Add(this.label45);
            this.panel6.Controls.Add(this.TxtRemunerationCost);
            this.panel6.Controls.Add(this.label46);
            this.panel6.Controls.Add(this.BtnImportResource);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(764, 120);
            this.panel6.TabIndex = 55;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(313, 94);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(19, 14);
            this.label47.TabIndex = 72;
            this.label47.Text = "%";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(313, 71);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 14);
            this.label14.TabIndex = 68;
            this.label14.Text = "%";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(313, 52);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 14);
            this.label18.TabIndex = 64;
            this.label18.Text = "%";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(313, 31);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 14);
            this.label36.TabIndex = 60;
            this.label36.Text = "%";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectPerc
            // 
            this.TxtDirectPerc.EnterMoveNextControl = true;
            this.TxtDirectPerc.Location = new System.Drawing.Point(258, 50);
            this.TxtDirectPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectPerc.Name = "TxtDirectPerc";
            this.TxtDirectPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectPerc.Properties.MaxLength = 30;
            this.TxtDirectPerc.Properties.ReadOnly = true;
            this.TxtDirectPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtDirectPerc.TabIndex = 63;
            // 
            // TxtIndirectPerc
            // 
            this.TxtIndirectPerc.EnterMoveNextControl = true;
            this.TxtIndirectPerc.Location = new System.Drawing.Point(258, 71);
            this.TxtIndirectPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndirectPerc.Name = "TxtIndirectPerc";
            this.TxtIndirectPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIndirectPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndirectPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndirectPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtIndirectPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndirectPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndirectPerc.Properties.MaxLength = 30;
            this.TxtIndirectPerc.Properties.ReadOnly = true;
            this.TxtIndirectPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtIndirectPerc.TabIndex = 67;
            // 
            // TxtTotalResourcePerc
            // 
            this.TxtTotalResourcePerc.EnterMoveNextControl = true;
            this.TxtTotalResourcePerc.Location = new System.Drawing.Point(258, 92);
            this.TxtTotalResourcePerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalResourcePerc.Name = "TxtTotalResourcePerc";
            this.TxtTotalResourcePerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalResourcePerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalResourcePerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalResourcePerc.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalResourcePerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalResourcePerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalResourcePerc.Properties.MaxLength = 30;
            this.TxtTotalResourcePerc.Properties.ReadOnly = true;
            this.TxtTotalResourcePerc.Size = new System.Drawing.Size(52, 20);
            this.TxtTotalResourcePerc.TabIndex = 71;
            // 
            // TxtRemunerationPerc
            // 
            this.TxtRemunerationPerc.EnterMoveNextControl = true;
            this.TxtRemunerationPerc.Location = new System.Drawing.Point(258, 29);
            this.TxtRemunerationPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationPerc.Name = "TxtRemunerationPerc";
            this.TxtRemunerationPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationPerc.Properties.MaxLength = 30;
            this.TxtRemunerationPerc.Properties.ReadOnly = true;
            this.TxtRemunerationPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtRemunerationPerc.TabIndex = 59;
            // 
            // TxtIndirectCost
            // 
            this.TxtIndirectCost.EnterMoveNextControl = true;
            this.TxtIndirectCost.Location = new System.Drawing.Point(100, 71);
            this.TxtIndirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndirectCost.Name = "TxtIndirectCost";
            this.TxtIndirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIndirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndirectCost.Properties.MaxLength = 30;
            this.TxtIndirectCost.Properties.ReadOnly = true;
            this.TxtIndirectCost.Size = new System.Drawing.Size(156, 20);
            this.TxtIndirectCost.TabIndex = 66;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(22, 74);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 14);
            this.label35.TabIndex = 65;
            this.label35.Text = "Indirect Cost";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.label19);
            this.panel11.Controls.Add(this.label20);
            this.panel11.Controls.Add(this.TxtCostPerc);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(335, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(429, 120);
            this.panel11.TabIndex = 73;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(403, 27);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 14);
            this.label19.TabIndex = 77;
            this.label19.Text = "%";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(146, 26);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 14);
            this.label20.TabIndex = 74;
            this.label20.Text = "Cost Percentage ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtCostPerc
            // 
            this.TxtCostPerc.EnterMoveNextControl = true;
            this.TxtCostPerc.Location = new System.Drawing.Point(251, 24);
            this.TxtCostPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostPerc.Name = "TxtCostPerc";
            this.TxtCostPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtCostPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCostPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCostPerc.Properties.MaxLength = 30;
            this.TxtCostPerc.Properties.ReadOnly = true;
            this.TxtCostPerc.Size = new System.Drawing.Size(149, 20);
            this.TxtCostPerc.TabIndex = 75;
            // 
            // TxtTotalResource
            // 
            this.TxtTotalResource.EnterMoveNextControl = true;
            this.TxtTotalResource.Location = new System.Drawing.Point(100, 92);
            this.TxtTotalResource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalResource.Name = "TxtTotalResource";
            this.TxtTotalResource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalResource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalResource.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalResource.Properties.MaxLength = 30;
            this.TxtTotalResource.Properties.ReadOnly = true;
            this.TxtTotalResource.Size = new System.Drawing.Size(156, 20);
            this.TxtTotalResource.TabIndex = 70;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(64, 96);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 69;
            this.label44.Text = "Total";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectCost
            // 
            this.TxtDirectCost.EnterMoveNextControl = true;
            this.TxtDirectCost.Location = new System.Drawing.Point(100, 50);
            this.TxtDirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectCost.Name = "TxtDirectCost";
            this.TxtDirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectCost.Properties.MaxLength = 30;
            this.TxtDirectCost.Properties.ReadOnly = true;
            this.TxtDirectCost.Size = new System.Drawing.Size(156, 20);
            this.TxtDirectCost.TabIndex = 62;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(32, 53);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 14);
            this.label45.TabIndex = 61;
            this.label45.Text = "Direct Cost";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemunerationCost
            // 
            this.TxtRemunerationCost.EnterMoveNextControl = true;
            this.TxtRemunerationCost.Location = new System.Drawing.Point(100, 29);
            this.TxtRemunerationCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationCost.Name = "TxtRemunerationCost";
            this.TxtRemunerationCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationCost.Properties.MaxLength = 30;
            this.TxtRemunerationCost.Properties.ReadOnly = true;
            this.TxtRemunerationCost.Size = new System.Drawing.Size(156, 20);
            this.TxtRemunerationCost.TabIndex = 58;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(16, 32);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(83, 14);
            this.label46.TabIndex = 57;
            this.label46.Text = "Remuneration";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnImportResource
            // 
            this.BtnImportResource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportResource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportResource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportResource.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportResource.Appearance.Options.UseBackColor = true;
            this.BtnImportResource.Appearance.Options.UseFont = true;
            this.BtnImportResource.Appearance.Options.UseForeColor = true;
            this.BtnImportResource.Appearance.Options.UseTextOptions = true;
            this.BtnImportResource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportResource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportResource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportResource.Location = new System.Drawing.Point(3, 5);
            this.BtnImportResource.Name = "BtnImportResource";
            this.BtnImportResource.Size = new System.Drawing.Size(86, 21);
            this.BtnImportResource.TabIndex = 56;
            this.BtnImportResource.Text = "Import Data";
            this.BtnImportResource.ToolTip = "Import Data";
            this.BtnImportResource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportResource.ToolTipTitle = "Run System";
            this.BtnImportResource.Click += new System.EventHandler(this.BtnImportResource_Click);
            // 
            // TpgWBS2
            // 
            this.TpgWBS2.Controls.Add(this.DtePlanEndDt2);
            this.TpgWBS2.Controls.Add(this.DtePlanStartDt2);
            this.TpgWBS2.Controls.Add(this.LueTaskCode);
            this.TpgWBS2.Controls.Add(this.LueStageCode2);
            this.TpgWBS2.Controls.Add(this.Grd5);
            this.TpgWBS2.Controls.Add(this.panel10);
            this.TpgWBS2.Location = new System.Drawing.Point(4, 26);
            this.TpgWBS2.Name = "TpgWBS2";
            this.TpgWBS2.Size = new System.Drawing.Size(909, 276);
            this.TpgWBS2.TabIndex = 4;
            this.TpgWBS2.Text = "WBS 2";
            this.TpgWBS2.UseVisualStyleBackColor = true;
            // 
            // DtePlanEndDt2
            // 
            this.DtePlanEndDt2.EditValue = null;
            this.DtePlanEndDt2.EnterMoveNextControl = true;
            this.DtePlanEndDt2.Location = new System.Drawing.Point(505, 97);
            this.DtePlanEndDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanEndDt2.Name = "DtePlanEndDt2";
            this.DtePlanEndDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt2.Properties.Appearance.Options.UseFont = true;
            this.DtePlanEndDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanEndDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanEndDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanEndDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanEndDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanEndDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanEndDt2.Size = new System.Drawing.Size(138, 20);
            this.DtePlanEndDt2.TabIndex = 65;
            this.DtePlanEndDt2.Visible = false;
            this.DtePlanEndDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanEndDt2_KeyDown);
            this.DtePlanEndDt2.Leave += new System.EventHandler(this.DtePlanEndDt2_Leave);
            this.DtePlanEndDt2.Validated += new System.EventHandler(this.DtePlanEndDt2_Validated);
            // 
            // DtePlanStartDt2
            // 
            this.DtePlanStartDt2.EditValue = null;
            this.DtePlanStartDt2.EnterMoveNextControl = true;
            this.DtePlanStartDt2.Location = new System.Drawing.Point(663, 96);
            this.DtePlanStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanStartDt2.Name = "DtePlanStartDt2";
            this.DtePlanStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DtePlanStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanStartDt2.Size = new System.Drawing.Size(138, 20);
            this.DtePlanStartDt2.TabIndex = 66;
            this.DtePlanStartDt2.Visible = false;
            this.DtePlanStartDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanStartDt2_KeyDown);
            this.DtePlanStartDt2.Leave += new System.EventHandler(this.DtePlanStartDt2_Leave);
            this.DtePlanStartDt2.Validated += new System.EventHandler(this.DtePlanStartDt2_Validated);
            // 
            // LueTaskCode
            // 
            this.LueTaskCode.EnterMoveNextControl = true;
            this.LueTaskCode.Location = new System.Drawing.Point(332, 97);
            this.LueTaskCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaskCode.Name = "LueTaskCode";
            this.LueTaskCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaskCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaskCode.Properties.DropDownRows = 30;
            this.LueTaskCode.Properties.NullText = "[Empty]";
            this.LueTaskCode.Properties.PopupWidth = 150;
            this.LueTaskCode.Size = new System.Drawing.Size(152, 20);
            this.LueTaskCode.TabIndex = 64;
            this.LueTaskCode.ToolTip = "F4 : Show/hide list";
            this.LueTaskCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaskCode.EditValueChanged += new System.EventHandler(this.LueTaskCode2_EditValueChanged);
            this.LueTaskCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaskCode2_KeyDown);
            this.LueTaskCode.Leave += new System.EventHandler(this.LueTaskCode2_Leave);
            this.LueTaskCode.Validated += new System.EventHandler(this.LueTaskCode_Validated);
            // 
            // LueStageCode2
            // 
            this.LueStageCode2.EnterMoveNextControl = true;
            this.LueStageCode2.Location = new System.Drawing.Point(84, 97);
            this.LueStageCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode2.Name = "LueStageCode2";
            this.LueStageCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode2.Properties.DropDownRows = 30;
            this.LueStageCode2.Properties.NullText = "[Empty]";
            this.LueStageCode2.Properties.PopupWidth = 150;
            this.LueStageCode2.Size = new System.Drawing.Size(224, 20);
            this.LueStageCode2.TabIndex = 63;
            this.LueStageCode2.ToolTip = "F4 : Show/hide list";
            this.LueStageCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode2.EditValueChanged += new System.EventHandler(this.LueStageCode2_EditValueChanged);
            this.LueStageCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStageCode2_KeyDown);
            this.LueStageCode2.Leave += new System.EventHandler(this.LueStageCode2_Leave);
            this.LueStageCode2.Validated += new System.EventHandler(this.LueStageCode2_Validated);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 74);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(909, 202);
            this.Grd5.TabIndex = 62;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.TxtContractAmtBefTax2);
            this.panel10.Controls.Add(this.label40);
            this.panel10.Controls.Add(this.TxtTotalWeight2);
            this.panel10.Controls.Add(this.label41);
            this.panel10.Controls.Add(this.label42);
            this.panel10.Controls.Add(this.TxtTotalPrice2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(909, 74);
            this.panel10.TabIndex = 55;
            // 
            // TxtContractAmtBefTax2
            // 
            this.TxtContractAmtBefTax2.EnterMoveNextControl = true;
            this.TxtContractAmtBefTax2.Location = new System.Drawing.Point(183, 4);
            this.TxtContractAmtBefTax2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmtBefTax2.Name = "TxtContractAmtBefTax2";
            this.TxtContractAmtBefTax2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmtBefTax2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmtBefTax2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmtBefTax2.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmtBefTax2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmtBefTax2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmtBefTax2.Properties.ReadOnly = true;
            this.TxtContractAmtBefTax2.Size = new System.Drawing.Size(192, 20);
            this.TxtContractAmtBefTax2.TabIndex = 57;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(7, 7);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(172, 14);
            this.label40.TabIndex = 56;
            this.label40.Text = "Contract Amount(Before Tax)";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalWeight2
            // 
            this.TxtTotalWeight2.EnterMoveNextControl = true;
            this.TxtTotalWeight2.Location = new System.Drawing.Point(183, 46);
            this.TxtTotalWeight2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeight2.Name = "TxtTotalWeight2";
            this.TxtTotalWeight2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalWeight2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeight2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeight2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeight2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeight2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeight2.Properties.ReadOnly = true;
            this.TxtTotalWeight2.Size = new System.Drawing.Size(192, 20);
            this.TxtTotalWeight2.TabIndex = 61;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(114, 28);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 14);
            this.label41.TabIndex = 58;
            this.label41.Text = "Total Price";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(84, 49);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(95, 14);
            this.label42.TabIndex = 60;
            this.label42.Text = "Total Weight %";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPrice2
            // 
            this.TxtTotalPrice2.EnterMoveNextControl = true;
            this.TxtTotalPrice2.Location = new System.Drawing.Point(183, 25);
            this.TxtTotalPrice2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPrice2.Name = "TxtTotalPrice2";
            this.TxtTotalPrice2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPrice2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPrice2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPrice2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPrice2.Properties.ReadOnly = true;
            this.TxtTotalPrice2.Size = new System.Drawing.Size(192, 20);
            this.TxtTotalPrice2.TabIndex = 59;
            // 
            // TpgWBS
            // 
            this.TpgWBS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgWBS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgWBS.Controls.Add(this.DtePlanEndDt);
            this.TpgWBS.Controls.Add(this.DtePlanStartDt);
            this.TpgWBS.Controls.Add(this.LueStageCode);
            this.TpgWBS.Controls.Add(this.Grd1);
            this.TpgWBS.Controls.Add(this.panel4);
            this.TpgWBS.Location = new System.Drawing.Point(4, 26);
            this.TpgWBS.Name = "TpgWBS";
            this.TpgWBS.Size = new System.Drawing.Size(909, 276);
            this.TpgWBS.TabIndex = 1;
            this.TpgWBS.Text = "WBS";
            this.TpgWBS.UseVisualStyleBackColor = true;
            // 
            // DtePlanEndDt
            // 
            this.DtePlanEndDt.EditValue = null;
            this.DtePlanEndDt.EnterMoveNextControl = true;
            this.DtePlanEndDt.Location = new System.Drawing.Point(451, 116);
            this.DtePlanEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanEndDt.Name = "DtePlanEndDt";
            this.DtePlanEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanEndDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanEndDt.TabIndex = 64;
            this.DtePlanEndDt.Visible = false;
            this.DtePlanEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanEndDt_KeyDown);
            this.DtePlanEndDt.Leave += new System.EventHandler(this.DtePlanEndDt_Leave);
            // 
            // DtePlanStartDt
            // 
            this.DtePlanStartDt.EditValue = null;
            this.DtePlanStartDt.EnterMoveNextControl = true;
            this.DtePlanStartDt.Location = new System.Drawing.Point(614, 116);
            this.DtePlanStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanStartDt.Name = "DtePlanStartDt";
            this.DtePlanStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanStartDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanStartDt.TabIndex = 65;
            this.DtePlanStartDt.Visible = false;
            this.DtePlanStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanStartDt_KeyDown);
            this.DtePlanStartDt.Leave += new System.EventHandler(this.DtePlanStartDt_Leave);
            // 
            // LueStageCode
            // 
            this.LueStageCode.EnterMoveNextControl = true;
            this.LueStageCode.Location = new System.Drawing.Point(28, 116);
            this.LueStageCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode.Name = "LueStageCode";
            this.LueStageCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode.Properties.DropDownRows = 30;
            this.LueStageCode.Properties.NullText = "[Empty]";
            this.LueStageCode.Properties.PopupWidth = 150;
            this.LueStageCode.Size = new System.Drawing.Size(224, 20);
            this.LueStageCode.TabIndex = 63;
            this.LueStageCode.ToolTip = "F4 : Show/hide list";
            this.LueStageCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode.EditValueChanged += new System.EventHandler(this.LueStageCode_EditValueChanged);
            this.LueStageCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStageCode_KeyDown);
            this.LueStageCode.Leave += new System.EventHandler(this.LueStageCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 74);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(905, 198);
            this.Grd1.TabIndex = 62;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtContractAmtBefTax);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.TxtTotalWeight);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.TxtTotalPrice);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 74);
            this.panel4.TabIndex = 55;
            // 
            // TxtContractAmtBefTax
            // 
            this.TxtContractAmtBefTax.EnterMoveNextControl = true;
            this.TxtContractAmtBefTax.Location = new System.Drawing.Point(183, 5);
            this.TxtContractAmtBefTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmtBefTax.Name = "TxtContractAmtBefTax";
            this.TxtContractAmtBefTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmtBefTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmtBefTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmtBefTax.Properties.ReadOnly = true;
            this.TxtContractAmtBefTax.Size = new System.Drawing.Size(192, 20);
            this.TxtContractAmtBefTax.TabIndex = 57;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(7, 8);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(172, 14);
            this.label34.TabIndex = 56;
            this.label34.Text = "Contract Amount(Before Tax)";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalWeight
            // 
            this.TxtTotalWeight.EnterMoveNextControl = true;
            this.TxtTotalWeight.Location = new System.Drawing.Point(183, 47);
            this.TxtTotalWeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeight.Name = "TxtTotalWeight";
            this.TxtTotalWeight.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalWeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeight.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeight.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeight.Properties.ReadOnly = true;
            this.TxtTotalWeight.Size = new System.Drawing.Size(192, 20);
            this.TxtTotalWeight.TabIndex = 61;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(114, 29);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 14);
            this.label32.TabIndex = 58;
            this.label32.Text = "Total Price";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(84, 50);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(95, 14);
            this.label33.TabIndex = 60;
            this.label33.Text = "Total Weight %";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPrice
            // 
            this.TxtTotalPrice.EnterMoveNextControl = true;
            this.TxtTotalPrice.Location = new System.Drawing.Point(183, 26);
            this.TxtTotalPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPrice.Name = "TxtTotalPrice";
            this.TxtTotalPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPrice.Properties.ReadOnly = true;
            this.TxtTotalPrice.Size = new System.Drawing.Size(192, 20);
            this.TxtTotalPrice.TabIndex = 59;
            // 
            // TcProjectImplementation
            // 
            this.TcProjectImplementation.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TcProjectImplementation.Controls.Add(this.TpgWBS);
            this.TcProjectImplementation.Controls.Add(this.TpgWBS2);
            this.TcProjectImplementation.Controls.Add(this.TpgResource);
            this.TcProjectImplementation.Controls.Add(this.TpgIssue);
            this.TcProjectImplementation.Controls.Add(this.TpgDocument);
            this.TcProjectImplementation.Controls.Add(this.TpgApproval);
            this.TcProjectImplementation.Controls.Add(this.TpgUploadFile);
            this.TcProjectImplementation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcProjectImplementation.Location = new System.Drawing.Point(0, 219);
            this.TcProjectImplementation.Multiline = true;
            this.TcProjectImplementation.Name = "TcProjectImplementation";
            this.TcProjectImplementation.SelectedIndex = 0;
            this.TcProjectImplementation.ShowToolTips = true;
            this.TcProjectImplementation.Size = new System.Drawing.Size(917, 306);
            this.TcProjectImplementation.TabIndex = 52;
            // 
            // TpgApproval
            // 
            this.TpgApproval.Controls.Add(this.Grd6);
            this.TpgApproval.Location = new System.Drawing.Point(4, 26);
            this.TpgApproval.Name = "TpgApproval";
            this.TpgApproval.Size = new System.Drawing.Size(764, 0);
            this.TpgApproval.TabIndex = 5;
            this.TpgApproval.Text = "Approval Information";
            this.TpgApproval.UseVisualStyleBackColor = true;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(764, 0);
            this.Grd6.TabIndex = 46;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgUploadFile
            // 
            this.TpgUploadFile.Controls.Add(this.Grd7);
            this.TpgUploadFile.Location = new System.Drawing.Point(4, 26);
            this.TpgUploadFile.Name = "TpgUploadFile";
            this.TpgUploadFile.Size = new System.Drawing.Size(764, 0);
            this.TpgUploadFile.TabIndex = 6;
            this.TpgUploadFile.Text = "Upload File";
            this.TpgUploadFile.UseVisualStyleBackColor = true;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(764, 0);
            this.Grd7.TabIndex = 47;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            this.Grd7.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd7_RequestCellToolTipText);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmProjectImplementation3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 525);
            this.Name = "FrmProjectImplementation3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAchievement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICSales.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimatedCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalRev.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            this.TpgDocument.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgIssue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgResource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResourcePerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).EndInit();
            this.TpgWBS2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeight2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice2.Properties)).EndInit();
            this.TpgWBS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).EndInit();
            this.TcProjectImplementation.ResumeLayout(false);
            this.TpgApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgUploadFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnBOQDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtAchievement;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtType;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabControl TcProjectImplementation;
        private System.Windows.Forms.TabPage TpgWBS;
        internal DevExpress.XtraEditors.DateEdit DtePlanEndDt;
        internal DevExpress.XtraEditors.DateEdit DtePlanStartDt;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeight;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPrice;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage TpgWBS2;
        private System.Windows.Forms.TabPage TpgResource;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnImportResource;
        private System.Windows.Forms.TabPage TpgIssue;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgDocument;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.DateEdit DtePlanEndDt2;
        internal DevExpress.XtraEditors.DateEdit DtePlanStartDt2;
        private DevExpress.XtraEditors.LookUpEdit LueTaskCode;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode2;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.MemoExEdit MeeApprovalRemark;
        private System.Windows.Forms.TabPage TpgApproval;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.TabPage TpgUploadFile;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.TextEdit TxtEstimatedCost;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtTotalRev;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmtBefTax;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmtBefTax2;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeight2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPrice2;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtDirectPerc;
        internal DevExpress.XtraEditors.TextEdit TxtIndirectPerc;
        internal DevExpress.XtraEditors.TextEdit TxtTotalResourcePerc;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationPerc;
        internal DevExpress.XtraEditors.TextEdit TxtIndirectCost;
        private System.Windows.Forms.Label label35;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtCostPerc;
        internal DevExpress.XtraEditors.TextEdit TxtTotalResource;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtDirectCost;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationCost;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtPICSales;
    }
}