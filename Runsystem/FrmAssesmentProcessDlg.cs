﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssesmentProcessDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAssesmentProcess mFrmParent;
        private string mSQL = string.Empty;
        private string mDocDt;
        private int mType;

        #endregion

        #region Constructor

        public FrmAssesmentProcessDlg(FrmAssesmentProcess FrmParent, string DocDt, int Type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
            mType = Type;
        }
        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Employee";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode);
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.DeptCode, C.DeptName, A.PosCode, D.PosName, A.JoinDt ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    left Join tbldepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join tblposition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("    Left Join tbloption E On A.Gender=E.OptCode and E.OptCat='Gender' ");
            SQL.AppendLine("    Where (A.ResignDt Is Not Null And A.ResignDt>=@DocDt ) Or A.ResignDt Is Null ");
            SQL.AppendLine(")X ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-5
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name",
                        "Department Code",
                        "Department",
                        "Position Code",
 
                        //6
                        "Position",
                        "Join Date",                       
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                String Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "X.EmpCode", "X.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By X.EmpName ",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName", "DeptCode", "DeptName", "PosCode", "PosName",
                            //6
                            "JoinDt"
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                if (mType == 1)
                {
                    mFrmParent.TxtEmpCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtEmpName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    mFrmParent.TxtDeptName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtPosition.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                    Sm.SetDte(mFrmParent.DteJoinDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 7));
                }
                else
                {
                    mFrmParent.TxtEmpCode2.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtEmpName2.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    mFrmParent.TxtDeptName2.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtPosition2.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                }
                this.Close();
            }

        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }


        #endregion

        #region Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       
    }
}
