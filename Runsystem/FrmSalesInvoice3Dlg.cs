﻿#region Update
/*
    03/04/2018 [HAR] Rounding DO amount ke atas berdasarkan parameter
    21/05/2018 [HAR] dialog tambah info shipping address dan remark header
    09/08/2018 [TKG] tambah status do
    30/09/2018 [TKG] menghilangkan angka di belakang koma
    21/08/2019 [DITA] ambil data berdasarkan parameter mSalesBasedDOQtyIndex perihal beda harga di KSM
    21/08/2019 [TKG] tambah DO local document
    22/08/2019 [DITA] menampilkan UOM berdasarkan parameter quantity nya
    25/02/2020 [WED/SIER] ambil dari DO yg processind = 'F'
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    15/04/2021 [RDH/SIER] Feedback nambah kolom remark based on detail DO to Customer
    22/04/2021 [WED/SRN] ketika parameter SalesInvoiceTaxCalculationFormula nilainya 2, maka amount before tax akan terisi dari kalkulasi uprice bef tax * qty
    14/06/2021 [RDH/SIER] menambah filter yang warehouse berdasarkan group  login
    17/06/2021 [IQA/SIER] menambahkan parameter filter mIsItCtFilteredByGroup filter berdasarkan grup login
    21/06/2021 [TKG/SIER] bug setting UserCode
    25/01/2022 [MYA/PHT] Penyesuaian jurnal SLI based on DO NON IDR dan penyesuaian kolom detail DO  di SLI
    18/03/2022 [VIN/SIER] DO yang sudah dipilih tidak muncul kembali
	20/08/2022 [MAU/SIER] menampilkan Foreign Name ketika choose data
    20/02/2023 [WED/MNET] ambil data dari Service Delivery juga berdasarkan IsUseMenu ServiceDelivery
    10/03/2023 [VIN/KBN] tambah where 1=1
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoice3 mFrmParent;
        string mSQL = string.Empty, mCtCode = string.Empty, mMenuCode="XXX";
        internal int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmSalesInvoice3Dlg(FrmSalesInvoice3 FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                mNumberOfInventoryUomCode = Convert.ToInt32(Sm.GetParameter("NumberOfInventoryUomCode"));
                SetGrd();
                SetSQL();
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method
        
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.ItCode, C.ItCodeInternal, C.Specification, C.ItName, C.ForeignName,  ");
            SQL.AppendLine("D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode3, A.CurCode, B.UPrice, ");
            if (mFrmParent.mIsDOCtShowPackagingUnit)
                SQL.AppendLine("B.QtyPackagingUnit as Qty2, B.PackagingUnitUomCode as InventoryUomCode2,  ");
            else
                SQL.AppendLine("B.Qty2, C.InventoryUomCode2, ");
            if (mFrmParent.mSalesBasedDOQtyIndex == "1") 
                SQL.AppendLine("(B.Qty*B.UPrice) TotalPrice, ");
            else 
                SQL.AppendLine("(B.Qty2*B.UPrice) TotalPrice, ");
            SQL.AppendLine("B.Remark, '3' As DocType, ");
            SQL.AppendLine("I.EntCode, I.EntName,  Concat(A.SAName, ', ', A.SAAddress) As SA, A.Remark As RemarkHdr, A.DocNoInternal, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.ProcessInd = 'F' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode  ");
            SQL.AppendLine("Left Join TblStockSummary E  ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode  ");
            SQL.AppendLine("    And B.Lot=E.Lot  ");
            SQL.AppendLine("    And B.Bin=E.Bin  ");
            SQL.AppendLine("    And B.Source=E.Source  ");
            SQL.AppendLine("Left Join TblWarehouse F On A.WhsCode=F.WhsCode  ");
            SQL.AppendLine("Left Join TblCostCenter G On F.CCCode=G.CCCode  ");
            SQL.AppendLine("Left Join TblProfitCenter H On G.ProfitCenterCode=H.ProfitCenterCode  ");
            SQL.AppendLine("Left Join TblEntity I On H.EntCode=I.EntCode  ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And Not Exists(  ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1  ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd = 'N'  ");
            SQL.AppendLine("    And T2.DOCtDocNo=B.DocNo ");
            SQL.AppendLine("    And T2.DOCtDNo=B.DNo ");
            SQL.AppendLine("    And T1.CtCode=@CtCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("and Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedDO)<1 ");
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And A.DocNo Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo=T2.DocNo ");
     		    SQL.AppendLine("        AND T1.CtCode = @CtCode ");
	  		    SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("        And T1.ProcessInd = 'F' ");
     		    SQL.AppendLine("    Inner Join TblItem T3 On T2.ItCode=T3.ItCode "); 
                SQL.AppendLine("        And T3.ItCtCode Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select ItCtCode ");
                SQL.AppendLine("            From TblGroupItemCategory ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(") ");
            }

            if (mFrmParent.mIsUseServiceDelivery)
            {
                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.ItCode, C.ItCodeInternal, C.Specification, C.ItName, C.ForeignName,  ");
                SQL.AppendLine("Null As PropName, '-' As BatchNo, '-' As Source, '-' As Lot, '-' As Bin, B.Qty, B.Qty3,  ");
                SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode3, A.CurCode, B.UPrice, ");
                SQL.AppendLine("B.Qty2, C.InventoryUomCode2, ");
                if (mFrmParent.mSalesBasedDOQtyIndex == "1")
                    SQL.AppendLine("(B.Qty*B.UPrice) TotalPrice, ");
                else
                    SQL.AppendLine("(B.Qty2*B.UPrice) TotalPrice, ");
                SQL.AppendLine("B.Remark, '3' As DocType, ");
                SQL.AppendLine("I.EntCode, I.EntName,  Concat(A.SAName, ', ', A.SAAddress) As SA, A.Remark As RemarkHdr, A.DocNoInternal, A.CreateDt ");
                SQL.AppendLine("From TblServiceDeliveryHdr A  ");
                SQL.AppendLine("Inner Join TblServiceDeliveryDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("    And A.ProcessInd = 'F' ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
                SQL.AppendLine("Inner Join TblCostCenter G On A.CCCode=G.CCCode  ");
                SQL.AppendLine("Left Join TblProfitCenter H On G.ProfitCenterCode=H.ProfitCenterCode  ");
                SQL.AppendLine("Left Join TblEntity I On H.EntCode=I.EntCode  ");
                SQL.AppendLine("Where A.CtCode=@CtCode ");
                SQL.AppendLine("And A.Status='A' ");
                SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And Not Exists(  ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblSalesInvoiceHdr T1  ");
                SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.CancelInd = 'N'  ");
                SQL.AppendLine("    And T2.DOCtDocNo=B.DocNo ");
                SQL.AppendLine("    And T2.DOCtDNo=B.DNo ");
                SQL.AppendLine("    And T1.CtCode=@CtCode ");
                SQL.AppendLine(") ");
                SQL.AppendLine("and Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedDO)<1 ");
                if (mFrmParent.mIsItCtFilteredByGroup)
                {
                    SQL.AppendLine("And A.DocNo Not In ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Distinct T1.DocNo ");
                    SQL.AppendLine("    From TblServiceDeliveryHdr T1 ");
                    SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        AND T1.CtCode = @CtCode ");
                    SQL.AppendLine("        And T2.CancelInd='N' ");
                    SQL.AppendLine("        And T1.ProcessInd = 'F' ");
                    SQL.AppendLine("    Inner Join TblItem T3 On T2.ItCode=T3.ItCode ");
                    SQL.AppendLine("        And T3.ItCtCode Not In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select ItCtCode ");
                    SQL.AppendLine("            From TblGroupItemCategory ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0                        
                        "No",

                        //1-5
                        "",
                        "Document#",
                        "DNo",
                        "Date",
                        "Item's Code",

                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Foreign Name",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Currency",

                        //21-25
                        "Price",
                        "Amount",
                        "Remark",
                        "Doctype",
                        "Entity Code",

                        //26-30
                        "Entity",
                        "Shipping Address",
                        "Document's Remark",
                        "DO's Local#",
                        "Specification",
                        
                    },
                     new int[] 
                    {
                        //0
                        40, 
                        
                        //1-5
                        20, 150, 0, 120, 100,  
                        
                        //6-10
                        100, 200, 200, 150, 100,  
                        
                        //11-15
                        200, 100, 100, 100, 80,  
                        
                        //16-20
                        100, 80, 100, 80, 180,   
                        
                        //21-25
                        80, 200, 200, 80, 0,

                        //26-30
                        150, 400, 400, 130, 200,

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] {4});
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 21, 22 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 9, 12, 13, 16, 17, 18, 19, 24, 25, 26, 27, 28, 29, 30 });
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 6, 30 }, true);
                Grd1.Cols[30].Move(8);
            }
            if (mFrmParent.mIsEntityMandatory)
                Grd1.Cols[26].Move(4);
            else
                Grd1.Cols[26].Visible = false;
            Grd1.Cols[29].Move(5);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            if (!mFrmParent.mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 9, 12, 13, 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 5, 9, 12, 13, 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);

        }
       
        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
            }
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 1=1 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SelectedDO", mFrmParent.GetSelectedDO());

                if (mFrmParent.mIsItCtFilteredByGroup || mFrmParent.mIsWarehouseFilteredByGroup) 
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNoInternal.Text, "T.DocNoInternal", false);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By T.CreateDt Desc;",
                        new string[] 
                        { 
                           //0
                            "DocNo", 
                            
                            //1-5
                            "DNo", "DocDt", "ItCode", "ItCodeInternal", "ItName", 
                            
                            //6-10
                            "ForeignName", "PropName", "BatchNo", "Source", "Lot",  

                            //11-15
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",

                            //16-20
                            "Qty3", "InventoryUomCode3", "CurCode", "UPrice", "TotalPrice", 
                            
                            //21-25
                            "Remark", "DocType", "EntCode", "EntName", "SA",

                            //26-28
                            "RemarkHdr", "DocNoInternal", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 22].Value = decimal.Truncate(dr.GetDecimal(c[20]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                        }, true, false, false, false
                    ); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);//DO
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);//Dno
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);//Date
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 6);//item local code 
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 5);//itItCode 
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 7);//It Name
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 16);//qty2
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 17);//uom2

                        if (mFrmParent.mSalesBasedDOQtyIndex == "1")
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 14);//qty
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 15);//uom
                        }
                        else
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 16);//qty2
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 17);//uom2
                        }
                        
                        
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 20);//cur
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 21);//up
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 21);//up
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 22);//amt
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 30);//spec item
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 23); // remark

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 50, Grd1, Row2, 8); // ForeignName


                        if (mFrmParent.mSalesInvoiceTaxCalculationFormula != "1")
                            mFrmParent.Grd1.Cells[Row1, 42].Value = Sm.GetGrdDec(Grd1, Row2, 22);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45 });
                    }
                }
            }
            mFrmParent.ComputeAmt();
            mFrmParent.ComputeAmtFromTax();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = String.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 5));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(key, String.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(mFrmParent.Grd1, Index, 3), Sm.GetGrdStr(mFrmParent.Grd1, Index, 15))))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }
       
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNoInternal_TextChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNoInternal_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO's local#");
        }


        #endregion

        #endregion
    }
}
