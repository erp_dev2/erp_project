﻿#region Update

// 02/11/2017 [ari] ubah ukuran font printout(size)
// 31/03/2020 [IBL/SRN] tambah kolom Batch No
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemBarcode : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmItemBarcodeFind FrmFind;
        private bool mIsItemBarcode = false;

        #endregion

        #region Constructor

        public FrmItemBarcode(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Item's Barcode";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter(); 
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsItemBarcode = Sm.GetParameter("IsItemBarcodeUseDefault") == "Y";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "DNo",
                        "Item Code", 
                        "Item Name",
                        "Foreign Name",
                        "Category",
                       

                        //6-9
                        "Group",
                        "Size",
                        "Color",
                        "Batch#"
                    },
                     new int[] 
                    {
                        20,
                        20, 80, 300, 300, 150,
                        150, 100, 100, 100,
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, false);

        }

        private void ChkHideInfoInGrd_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 9 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {

                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark
            });
            Sm.ClearGrd(Grd1, true);
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemBarcodeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            string CurrentDateTime = Sm.ServerCurrentDateTime();

            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }


        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                MessageBox.Show("");
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmItemBarcodeDlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmItemBarcodeDlg(this));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
           
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemBarcode", "TblItemBarcodeHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveItemBarcodeHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveItemBarcodeDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItemBarcodeHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemBarcodeHdr(DocNo, DocDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Remark, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveItemBarcodeDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemBarcodeDtl(DocNo, DNo, ItCode, BatchNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @ItCode, @BatchNo, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemBarcode", "TbLItemBarcodeHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(CancelItemBarcode());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDocumentNotCancelled();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblItemBarcodeHdr " +
                    "Where CancelInd='Y'  And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelItemBarcode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemBarcodeHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItemBarcodeHdr(DocNo);
                ShowItemBarcodeDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItemBarcodeHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, A.remark From TblItemBarcodeHdr A Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        public void ShowItemBarcodeDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BatchNo, B.ItCode, B.ItName, B.ForeignName, C.ItCtName, D.ItGrpName, E.OptDesc As Size, F.OptDesc As Color ");
            SQL.AppendLine("From tblitembarcodedtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Inner Join TblItemGroup D On B.ItGrpCode=D.ItGrpCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCat='ItemInformation1' And B.Information1=E.OptCode ");
            SQL.AppendLine("Left Join TblOption F On F.OptCat='ItemInformation2' And B.Information2=F.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.ItName ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
           
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "ItCode", "ItName", "ForeignName", "ItCtName", "ItGrpName",

                    //6-8
                    "Size", "Color", "BatchNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additinal Method

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<ItBarcode>();

            string[] TableName = { "ItBarcode" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Dno, ");
            if (mIsItemBarcode)
            {
                SQL.AppendLine(" B.ItCode, ");
            }
            else
            {
                SQL.AppendLine(" Replace(B.ItCode,'-','')As ItCode, ");
            }
            SQL.AppendLine("substring_index(C.itname, '-',1)As ItName, D.OptDesc As Size, E.OptDesc As Color, C.ForeignName ");
            SQL.AppendLine("From tblItemBarcodeHdr A ");
            SQL.AppendLine("Inner Join TblItemBarcodeDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On D.OptCat='ItemInformation1' And C.Information1=D.OptCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCat='ItemInformation2' And C.Information2=E.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "DocNo",

                         //1-5
                         "ItCode",
                         "ItName",
                         "Size",
                         "Color",
                         "ForeignName",
                         "Dno",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ItBarcode()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            ItCode = Sm.DrStr(dr, c[1]),
                            ItName = Sm.DrStr(dr, c[2]),
                            Size = Sm.DrStr(dr, c[3]),
                            Color = Sm.DrStr(dr, c[4]),
                            ForeignName = Sm.DrStr(dr, c[5]),
                            Dno = Sm.DrStr(dr, c[6]),
                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            Sm.PrintReport("ItemBarcode", myLists, TableName, false);
        }

        #endregion

        #endregion
    }
       
    #region Report Class

    class ItBarcode
    {
        public string DocNo { set; get; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string ForeignName { get; set; }
        public string Dno { get; set; }
    }

  

    #endregion
}
