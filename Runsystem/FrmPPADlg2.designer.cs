﻿namespace RunSystem
{
    partial class FrmPPADlg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtGrdLevel = new DevExpress.XtraEditors.TextEdit();
            this.ChkGrdLevel = new DevExpress.XtraEditors.CheckEdit();
            this.ChkGrpLevelCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueGrpLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGrdLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGrpLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrpLevelCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkGrpLevelCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueGrpLevelCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtGrdLevel);
            this.panel2.Controls.Add(this.ChkGrdLevel);
            this.panel2.Size = new System.Drawing.Size(672, 61);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 412);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "Grade Level";
            // 
            // TxtGrdLevel
            // 
            this.TxtGrdLevel.EnterMoveNextControl = true;
            this.TxtGrdLevel.Location = new System.Drawing.Point(93, 6);
            this.TxtGrdLevel.Name = "TxtGrdLevel";
            this.TxtGrdLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrdLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtGrdLevel.Properties.MaxLength = 30;
            this.TxtGrdLevel.Size = new System.Drawing.Size(242, 20);
            this.TxtGrdLevel.TabIndex = 11;
            this.TxtGrdLevel.Validated += new System.EventHandler(this.TxtGrdLevel_Validated);
            // 
            // ChkGrdLevel
            // 
            this.ChkGrdLevel.Location = new System.Drawing.Point(337, 4);
            this.ChkGrdLevel.Name = "ChkGrdLevel";
            this.ChkGrdLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGrdLevel.Properties.Appearance.Options.UseFont = true;
            this.ChkGrdLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkGrdLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkGrdLevel.Properties.Caption = " ";
            this.ChkGrdLevel.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkGrdLevel.Size = new System.Drawing.Size(31, 22);
            this.ChkGrdLevel.TabIndex = 12;
            this.ChkGrdLevel.ToolTip = "Remove filter";
            this.ChkGrdLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkGrdLevel.ToolTipTitle = "Run System";
            this.ChkGrdLevel.CheckedChanged += new System.EventHandler(this.ChkGrdLevel_CheckedChanged);
            // 
            // ChkGrpLevelCode
            // 
            this.ChkGrpLevelCode.Location = new System.Drawing.Point(337, 27);
            this.ChkGrpLevelCode.Name = "ChkGrpLevelCode";
            this.ChkGrpLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGrpLevelCode.Properties.Appearance.Options.UseFont = true;
            this.ChkGrpLevelCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkGrpLevelCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkGrpLevelCode.Properties.Caption = " ";
            this.ChkGrpLevelCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkGrpLevelCode.Size = new System.Drawing.Size(19, 22);
            this.ChkGrpLevelCode.TabIndex = 15;
            this.ChkGrpLevelCode.ToolTip = "Remove filter";
            this.ChkGrpLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkGrpLevelCode.ToolTipTitle = "Run System";
            this.ChkGrpLevelCode.CheckedChanged += new System.EventHandler(this.ChkGrpLevelCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(16, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Group Level";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGrpLevelCode
            // 
            this.LueGrpLevelCode.EnterMoveNextControl = true;
            this.LueGrpLevelCode.Location = new System.Drawing.Point(93, 28);
            this.LueGrpLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrpLevelCode.Name = "LueGrpLevelCode";
            this.LueGrpLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrpLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrpLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrpLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrpLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrpLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrpLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrpLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrpLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrpLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrpLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrpLevelCode.Properties.DropDownRows = 20;
            this.LueGrpLevelCode.Properties.NullText = "[Empty]";
            this.LueGrpLevelCode.Properties.PopupWidth = 500;
            this.LueGrpLevelCode.Size = new System.Drawing.Size(242, 20);
            this.LueGrpLevelCode.TabIndex = 14;
            this.LueGrpLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueGrpLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrpLevelCode.EditValueChanged += new System.EventHandler(this.LueGrpLevelCode_EditValueChanged);
            // 
            // FrmPPADlg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmPPADlg2";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGrdLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGrpLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrpLevelCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtGrdLevel;
        private DevExpress.XtraEditors.CheckEdit ChkGrdLevel;
        private DevExpress.XtraEditors.CheckEdit ChkGrpLevelCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueGrpLevelCode;
    }
}