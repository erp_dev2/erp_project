﻿#region Update
/*
    30/08/2019 [TKG][KMI] tambah payroll tax formula
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollGrp : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal bool mIsPayrollGrpPayrollTaxFormulaEnabled = false;
        internal FrmPayrollGrpFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPayrollGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Payroll Group";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueDurationInd, "PayrunPeriod");
                Sl.SetLueOption(ref LuePayrollTaxFormula, "PayrollTaxFormula");
                DteStartDt.Visible = false;
                if (mIsPayrollGrpPayrollTaxFormulaEnabled) LblPayrollTaxFormula.ForeColor = Color.Red;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsPayrollGrpPayrollTaxFormulaEnabled = Sm.GetParameterBoo("IsPayrollGrpPayrollTaxFormulaEnabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Start Date" },
                    new int[] { 150 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 0 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPGCode, TxtPGName, LueDurationInd, TxtNoofDays, LuePayrollTaxFormula, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtPGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPGCode, TxtPGName, LueDurationInd, TxtNoofDays, LuePayrollTaxFormula, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtPGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPGName, LuePayrollTaxFormula, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtPGName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPGCode, TxtPGName, LueDurationInd, LuePayrollTaxFormula, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtNoofDays }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPayrollGrpFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPGCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {    
                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                {
                    Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        private void SaveData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SavePayrollGrpHdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SavePayrollGrpDtl(Row));

            Sm.ExecCommands(cml);

            if (TxtPGCode.Properties.ReadOnly)
                ShowData(TxtPGCode.Text);
            else
                BtnInsertClick(sender, e);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPGCode, "Payroll group code", false) ||
                Sm.IsTxtEmpty(TxtPGName, "Payroll group name", false) ||
                Sm.IsLueEmpty(LueDurationInd, "Type") ||
                (mIsPayrollGrpPayrollTaxFormulaEnabled && Sm.IsLueEmpty(LuePayrollTaxFormula, "Tax formula")) ||
                IsPGCodeExisted() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsPGCodeExisted()
        {
            if (TxtPGCode.Properties.ReadOnly) return false;

            var cm = new MySqlCommand()
            {
                CommandText = "Select PGCode From TblPayrollGrpHdr Where PGCode=@PGCode Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Payroll group code ( " + TxtPGCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Start date is empty.")) return true;
                for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if (Row != Row2 && Sm.GetGrdDate(Grd1, Row, 0) == Sm.GetGrdDate(Grd1, Row2, 0))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Duplicate start date.");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SavePayrollGrpHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPayrollGrpHdr(PGCode, PGName, DurationInd, NoofDays, PayrollTaxFormula, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PGCode, @PGName, @DurationInd, @NoofDays, @PayrollTaxFormula, @Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("  update PGName=@PGName, PayrollTaxFormula=@PayrollTaxFormula, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtPGCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblPayrollGrpDtl Where PGCode=@PGCode; ");
            
            var cm = new MySqlCommand()
            { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
            Sm.CmParam<String>(ref cm, "@PGName", TxtPGName.Text);
            Sm.CmParam<String>(ref cm, "@DurationInd", Sm.GetLue(LueDurationInd));
            Sm.CmParam<Decimal>(ref cm, "@NoofDays", decimal.Parse(TxtNoofDays.Text));
            Sm.CmParam<String>(ref cm, "@PayrollTaxFormula", Sm.GetLue(LuePayrollTaxFormula));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePayrollGrpDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPayrollGrpDtl(PGCode, StartDt, CreateBy, CreateDt) " +
                    "Values(@PGCode, @StartDt, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string PGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPayrollGrpHdr(PGCode);
                ShowPayrollGrpDtl(PGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPayrollGrpHdr(string PGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PGCode", PGCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select PGCode, PGName, DurationInd, NoofDays, PayrollTaxFormula, Remark From TblPayrollGrpHdr Where PGCode=@PGCode;",
                    new string[] 
                    { 
                        "PGCode", 
                        "PGName", "DurationInd", "NoofDays", "PayrollTaxFormula", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPGName.EditValue = Sm.DrStr(dr, c[1]); 
                        Sm.SetLue(LueDurationInd, Sm.DrStr(dr, c[2]));
                        TxtNoofDays.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        Sm.SetLue(LuePayrollTaxFormula, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowPayrollGrpDtl(string PGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PGCode", PGCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, "Select StartDt From TblPayrollGrpDtl Where PGCode=@PGCode;",
                new string[]{ "StartDt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 0, 0);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPGCode);
        }

        private void TxtPGName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPGName);
        }

        private void TxtNoofDays_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtNoofDays, 0);
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDurationInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDurationInd, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        #endregion

        private void LuePayrollTaxFormula_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayrollTaxFormula, new Sm.RefreshLue2(Sl.SetLueOption), "PayrollTaxFormula");
        }

        #endregion
    }
}
