﻿#region Update
/*
    26/10/2020 [WED/PHT] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRegionalMinimumWages : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmRegionalMinimumWagesFind FrmFind;

        #endregion

        #region Constructor

        public FrmRegionalMinimumWages(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Regional Minimum Wages";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                SetGrd();
                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                { 
                    //0
                    "Site Code", 
                    
                    //1-5
                    "Site", 
                    "City Code", 
                    "City",
                    "Amount", 
                    "Remark"
                },
                new int[] 
                { 
                    0, 
                    200, 0, 400, 120, 200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, MeeCancelReason, TxtDocName, DteStartDt, 
                        DteEndDt, MeeRemark
                    }, true);
                    BtnProcessData.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtDocName, DteStartDt, 
                        DteEndDt, MeeRemark
                    }, false);
                    BtnProcessData.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    BtnProcessData.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocName, DteStartDt, 
                        DteEndDt, MeeRemark, ChkCancelInd, MeeCancelReason
                    }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, ChkCancelInd, MeeCancelReason, TxtDocName, DteStartDt, 
                DteEndDt, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRegionalMinimumWagesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 5 }, e);
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RegionalMinimumWages", "TblRegionalMinimumWagesHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRegionalMinimumWagesHdr(DocNo));

            for (int i = 0; i < Grd1.Rows.Count; ++i)
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                    cml.Add(SaveRegionalMinimumWagesDtl(DocNo, i));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtDocName, "Name", false) ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords()
                ;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (i != Grd1.Rows.Count - 1)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, i, 1, false, "Site is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, i, 3, false, "City is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, i, 4, true, "Amount is empty/zero.")) return true;

                    for (int j = (i + 1); j < Grd1.Rows.Count; ++j)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 0) == Sm.GetGrdStr(Grd1, j, 0))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate site found.");
                            Sm.FocusGrd(Grd1, j, 1);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least one site.");
                BtnProcessData.Focus();
                return true;
            }

            return false;
        }

        private MySqlCommand SaveRegionalMinimumWagesHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRegionalMinimumWagesHdr(DocNo, DocDt, CancelInd, DocName, StartDt, EndDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @DocName, @StartDt, @EndDt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRegionalMinimumWagesDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRegionalMinimumWagesDtl(DocNo, DNo, SiteCode, CityCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SiteCode, @CityCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRegionalMinimumWagesHdr());

            for (int i = 0; i < Grd1.Rows.Count; ++i)
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                    cml.Add(SaveRegionalMinimumWagesDtl(TxtDocNo.Text, i));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtDocName, "Name", false) ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblRegionalMinimumWagesHdr " +
                    "Where (CancelInd='Y') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditRegionalMinimumWagesHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRegionalMinimumWagesHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, ");
            SQL.AppendLine("    DocName = @DocName, StartDt = @StartDt, EndDt = @EndDt, Remark = @Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblRegionalMinimumWagesDtl Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRegionalMinimumWagesHdr(DocNo);
                ShowRegionalMinimumWagesDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRegionalMinimumWagesHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblRegionalMinimumWagesHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "DocName", "StartDt",  
                    
                    //6-7
                    "EndDt", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtDocName.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                    Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[6]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        private void ShowRegionalMinimumWagesDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SiteCode, B.SiteName, A.CityCode, C.CityName, A.Amt, A.Remark ");
            SQL.AppendLine("From TblRegionalMinimumWagesDtl A ");
            SQL.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Inner Join TblCity C On A.CityCode = C.CityCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SiteCode",
                    
                    //1-5
                    "SiteName", "CityCode", "CityName", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }


        #endregion

        #region Additional Method

        private void ProcessData()
        {
            string SiteCode = string.Empty;
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (SiteCode.Length > 0) SiteCode += ",";
                    SiteCode += Sm.GetGrdStr(Grd1, i, 0);
                }
            }

            SQL.AppendLine("Select A.SiteCode, A.SiteName, A.CityCode, B.CityName ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Inner Join TblCity B On A.CityCode = B.CityCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.CityCode Is Not Null ");
            if (SiteCode.Length > 0)
                SQL.AppendLine("And Not Find_In_set(SiteCode, @SiteCode) ");
            SQL.AppendLine("Order By A.SiteName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString(),
                    CommandTimeout = 600
                };
                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName", "CityCode", "CityName" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Sm.DrStr(dr, c[0]);
                        Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = Sm.DrStr(dr, c[1]);
                        Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = Sm.DrStr(dr, c[2]);
                        Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.DrStr(dr, c[3]);
                        Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = 0m;
                        Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = string.Empty;
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Click

        private void BtnProcessData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ProcessData();
            }
        }

        #endregion

        #endregion
    }
}
