﻿#region Update
/*
    14/04/2022 [RDA/PRODUCT] new menu
    27/05/2022 [RDA/PRODUCT] perubahan isi dan tampilan menu sesuai requirement terbaru (ganti base frm)
    31/05/2022 [IBL/PRODUCT] Bug: LueTransactionCode blm refresh data saat memilih <Refresh>
    24/01/2023 [IBL/BBT] Penyesuaian menu berdasarkan param IsTypeofExpenseForFICOActive:
                         - Hide kolom Tax, Rate, dan Formula
                         - Validasi 1 transaksi hanya boleh 1 setting aktif.
                         - Saat edit, bisa edit DocType, COA, Amount, dan Remark pada detail
    30/03/2023 [MYA/BBT] Penambahan Cash Type pada menu Type of Expenses and Other Setting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmExpensesType : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal bool mIsTypeofExpenseForFICOActive = false;
        internal FrmExpensesTypeFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmExpensesType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                BtnDelete.Visible = false;
                BtnPrint.Visible = false;
                LueFormula.Visible = false;
                LueCashType.Visible = false;
                LueCashTypeGroup.Visible = false;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueTransactionCode, "InvestmentTransaction");
                Sl.SetLueOption(ref LueFormula, "InvestmentDebtMultiplierField");
                SetLueCashTypeGroup(ref LueCashTypeGroup);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "",
 
                        //1-5
                        "Expenses Code", 
                        "Document Type", 
                        "",
                        "Account#", 
                        "Account Description", 
                        
                        //6-10
                        "Rate (%)",
                        "Amount", 
                        "Tax", 
                        "Formula Code",
                        "Formula", 
                        

                        //11-15
                        "Remark",
                        "Create By",
                        "Create Date",
                        "Last Up By",
                        "Last Up Date",

                        //16-19
                        "Cash Type Group Code",
                        "Cash Type Group",
                        "Cash Type Code",
                        "Cash Type"
                    },
                    new int[]
                    {
                        //0
                        20, 
                        
                        //1-5
                        200, 200, 20, 200, 180,
                        
                        //6-10
                        120, 120, 50, 100, 150,
                        
                        //11-15
                        200, 200, 200, 200, 200,

                        //16-19
                        0, 150, 0, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColCheck(Grd1, new int[] { 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5, 9, 12, 13, 14, 15, 16, 18  }, false);
            //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 4, 5, 9, 12, 13, 14, 15 });
            if (mIsTypeofExpenseForFICOActive)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 10 }, false);
                Grd1.Cols[17].Move(7);
                Grd1.Cols[19].Move(8);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 17, 19 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtExpensesCode, DteDocDt, LueTransactionCode, MeeRemark, ChkActInd }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 3, 6, 7, 8, 10, 11, 16, 17, 18, 19 });
                    TxtExpensesCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueTransactionCode, MeeRemark }, false);
                    ChkActInd.Checked = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 6, 7, 8, 10, 11, 17, 19 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd }, false);
                    if(mIsTypeofExpenseForFICOActive)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 7, 11, 17, 19 });
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtExpensesCode, DteDocDt, LueTransactionCode, MeeRemark });
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmExpensesTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtExpensesCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtExpensesCode.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 6, 7, 10, 17, 19 }, e.ColIndex))
            {
                if (e.ColIndex == 6)
                {
                    Grd1.Cells[e.RowIndex, 7].ReadOnly = iGBool.True;
                    Grd1.Cells[e.RowIndex, 7].BackColor = Color.FromArgb(224, 224, 224);
                }

                if (e.ColIndex == 7)
                {
                    Grd1.Cells[e.RowIndex, 6].ReadOnly = iGBool.True;
                    Grd1.Cells[e.RowIndex, 6].BackColor = Color.FromArgb(224, 224, 224);
                }

                if (e.ColIndex == 10) Sm.LueRequestEdit(ref Grd1, ref LueFormula, ref fCell, ref fAccept, e);
                if (e.ColIndex == 17) Sm.LueRequestEdit(ref Grd1, ref LueCashTypeGroup, ref fCell, ref fAccept, e);
                if (e.ColIndex == 19)
                {
                    Sm.LueRequestEdit(ref Grd1, ref LueCashType, ref fCell, ref fAccept, e);
                    SetLueCashType(ref LueCashType, Sm.GetGrdStr(Grd1, e.RowIndex, 16));
                }
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled) Sm.FormShowDialog(new FrmExpensesTypeDlg(this));
        }

        private void Grd_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 11, 16, 17, 18, 19 }, e.ColIndex))
                {
                    SetLueCashType(ref LueCashType, Sm.GetGrdStr(Grd1, e.RowIndex, 16));
                    //ComputeDirectCost(); ComputeRemunerationCost();
                }
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string ExpensesCode)
        {
            try
            {
                ShowDataHdr(ExpensesCode);
                ShowDataDtl(ExpensesCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataHdr(string ExpensesCode)
        {
            Cursor.Current = Cursors.WaitCursor;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ExpensesCode, A.DocDt, A.TransactionCode, A.ActInd, A.Remark ");
            SQL.AppendLine("From TblExpensesTypeHdr A  ");
            SQL.AppendLine("Where A.ExpensesCode=@ExpensesCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ExpensesCode", ExpensesCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[]
                    {
                            //0
                            "ExpensesCode",

                            //1-4
                            "DocDt", "TransactionCode", "ActInd",  "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtExpensesCode.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueTransactionCode, Sm.DrStr(dr, c[2]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowDataDtl(string ExpensesCode)
        {
            Cursor.Current = Cursors.WaitCursor;
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.ExpensesCode, B.DocType, B.AcNo, C.AcDesc, B.Rate,  ");
            SQL.AppendLine("B.Amt, B.TaxInd, B.Formula, D.OptDesc as FormulaName, B.CashTypeGrpCode, E.CashTypeGrpName, B.CashTypeCode, F.CashTypeName, B.Remark,  ");
            SQL.AppendLine("B.CreateBy, B.CreateDt, B.LastUpBy, B.LastUpDt ");
            SQL.AppendLine("FROM tblexpensestypehdr A ");
            SQL.AppendLine("INNER JOIN tblexpensestypedtl B ON A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("INNER JOIN tblcoa C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("LEFT JOIN tbloption D ON B.Formula = D.OptCode AND D.OptCat = 'InvestmentDebtMultiplierField' ");
            SQL.AppendLine("LEFT JOIN TblCashTypeGroup E ON B.CashTypeGrpCode = E.CashTypeGrpCode ");
            SQL.AppendLine("LEFT JOIN TblCashType F ON B.CashTypeCode = F.CashTypeCode ");
            SQL.AppendLine("WHERE A.ExpensesCode = @ExpensesCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ExpensesCode", ExpensesCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                            //0
                            "ExpensesCode",

                            //1-5
                            "DocType", "AcNo", "AcDesc", "Rate",  "Amt", 

                            //6-10
                            "TaxInd", "Formula", "FormulaName", "Remark", "CreateBy",

                            //11-15
                            "CreateDt", "LastUpBy", "LastUpDt", "CashTypeGrpCode", "CashTypeGrpName",

                            //16-17
                            "CashTypeCode", "CashTypeName"
                    },
                    (MySqlDataReader dr, iGrid Grid1, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);  //ExpensesCode
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);  //DocumentType
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);  //AcNo
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);  //AcDesc
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4); //Rate
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5); //Amt
                        Sm.SetGrdValue("B", Grd1, dr, c, Row, 8, 6); //Tax
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7); //FormulaCode
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8); //Formula
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9); //Remark
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10); //CreateBy
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 11); //CreateDt
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12); //LastUpBy
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13); //LastUpDt
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 14); 
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15); 
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 16); 
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 17); 
                    }, false, false, true, false
                );
                //Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 17 });
                //Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            var ExpensesCode = GenerateExpensesCode();
            var cml = new List<MySqlCommand>();

            cml.Add(SaveExpensesTypeHdr(ExpensesCode));
            cml.Add(SaveExpensesTypeDtl(ExpensesCode));

            Sm.ExecCommands(cml);

            ShowData(ExpensesCode);
        }

        private MySqlCommand SaveExpensesTypeHdr(string ExpensesCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* ExpensesType - Hdr */ ");
            SQL.AppendLine("Insert Into TblExpensesTypeHdr(ExpensesCode, DocDt, TransactionCode, ActInd, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@ExpensesCode, @DocDt, @TransactionCode, @ActInd, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ExpensesCode", ExpensesCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TransactionCode", Sm.GetLue(LueTransactionCode));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveExpensesTypeDtl(string ExpensesCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* ExpensesType - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblExpensesTypeDtl(ExpensesCode, DNo, DocType, AcNo, Rate, Amt, TaxInd, Formula, CashTypeGrpCode, CashTypeCode, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@ExpensesCode " +
                        ", @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @Rate_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @TaxInd_" + r.ToString() +
                        ", @Formula_" + r.ToString() +
                        ", @CashTypeGrpCode_" + r.ToString() +
                        ", @CashTypeCode_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@Rate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@TaxInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Formula_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@CashTypeGrpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 16));
                    Sm.CmParam<String>(ref cm, "@CashTypeCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 18));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@ExpensesCode", ExpensesCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsNonActiveDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Update TblExpensesTypeHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where ExpensesCode=@ExpensesCode; ");

            if (mIsTypeofExpenseForFICOActive)
                SQL.AppendLine("Delete From TblExpensesTypeDtl Where ExpensesCode = @ExpensesCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ExpensesCode", TxtExpensesCode.Text);

            cml.Add(cm);
            if(mIsTypeofExpenseForFICOActive)
                cml.Add(SaveExpensesTypeDtl(TxtExpensesCode.Text));
            Sm.ExecCommands(cml);

            ShowData(TxtExpensesCode.Text);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date ") ||
                Sm.IsLueEmpty(LueTransactionCode, "Transaction") ||
                (mIsTypeofExpenseForFICOActive && IsTransactionSettingAlreadyExists()) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsTransactionSettingAlreadyExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblExpensesTypeHdr ");
            SQL.AppendLine("Where TransactionCode = @Param ");
            SQL.AppendLine("And ActInd = 'Y' Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueTransactionCode), "This transaction ("+LueTransactionCode.Text+") already has active settings.");
        }

        private bool IsNonActiveDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtExpensesCode, "Setting Code", false) ||
                IsDataInactiveAlready();
        }

        private bool IsDataInactiveAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblExpensesTypeHdr Where ActInd='N' And ExpensesCode=@Param;",
                TxtExpensesCode.Text,
                "This data already inactive.");
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Document type is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 4, false, "Account# is empty.")) return true;
                if (mIsTypeofExpenseForFICOActive)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 16, false, "Cash Type Group is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 18, false, "Cash Type is empty.")) return true;
                }
                if (Sm.GetGrdDec(Grd1, r, 6) == 0m && Sm.GetGrdDec(Grd1, r, 7) == 0m)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length != 0 && Sm.GetGrdStr(Grd1, r, 4).Length != 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Document Type List" + Environment.NewLine +
                            "Document Type : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine + Environment.NewLine +
                            "Rate and amount value should not be 0.");
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Rate and amount value should not be 0.");
                    }

                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 record.");
                return true;
            }

            return false;
        }

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsTypeofExpenseForFICOActive' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsTypeofExpenseForFICOActive": mIsTypeofExpenseForFICOActive = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }

        }

        private string GenerateExpensesCode()
        {
            var ExpensesCode = string.Empty;

            ExpensesCode = Sm.GetValue("Select IfNull(Right(Concat('00000', ExpensesCodeMax) , 5), '00001') As ExpensesCodeTemp " +
                "From( " +
                "   Select Max(Cast(Trim(ExpensesCode) As Decimal)) + 1 ExpensesCodeMax " +
                "   From TblExpensesTypeHdr " +
                "   ) T; ");

            return ExpensesCode;
        }

        //private void FormatNumTxt(TextEdit Txt, Byte FormatType)
        //{
        //    Txt.EditValue = FormatNum(Txt.Text, FormatType);
        //}

        private string FormatNum(string Value, byte FormatType)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();

                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }

                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    //return String.Format("{0:#,##0.00##}", NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 4: return String.Format("{0:#,##0.00##}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private void SetLueCashTypeGroup(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CashTypeGrpCode As Col1, CashTypeGrpName As Col2 ");
            SQL.AppendLine("From TblCashTypeGroup");
            SQL.AppendLine("Order By CashTypeGrpCode ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCashType(ref DXE.LookUpEdit Lue, string CashTypeGrpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CashTypeCode As Col1, A.CashTypeName As Col2 ");
            SQL.AppendLine("From TblCashType A");
            SQL.AppendLine("Where CashTypeGrpCode = '" + CashTypeGrpCode + "' And A.Level = (SELECT MAX(`LEVEL`) FROM tblcashtype LIMIT 1) ");
            SQL.AppendLine("Order By CashTypeCode ;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueTransactionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTransactionCode, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentTransaction");
        }

        private void LueFormula_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFormula, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentDebtMultiplierField");
        }

        //private void LueFormula_KeyDown(object sender, KeyEventArgs e)
        //{
        //    Sm.LueKeyDown(Grd1, ref fAccept, e);
        //}

        private void LueFormula_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueFormula.Visible && fAccept && fCell.ColIndex == 10)
                {
                    if (Sm.GetLue(LueFormula).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 9].Value =
                        Grd1.Cells[fCell.RowIndex, 10].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueFormula);
                        Grd1.Cells[fCell.RowIndex, 10].Value = LueFormula.GetColumnValue("Col2");
                    }
                    LueFormula.Visible = false;
                }
            }
        }

        private void LueCashTypeGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCashTypeGroup, new Sm.RefreshLue1(SetLueCashTypeGroup));
            }
        }

        private void LueCashTypeGroup_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCashTypeGroup_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueCashTypeGroup.Visible && fAccept && fCell.ColIndex == 17)
                {
                    if (Sm.GetLue(LueCashTypeGroup).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 16].Value =
                        Grd1.Cells[fCell.RowIndex, 17].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 16].Value = Sm.GetLue(LueCashTypeGroup);
                        Grd1.Cells[fCell.RowIndex, 17].Value = LueCashTypeGroup.GetColumnValue("Col2");
                    }
                    LueCashTypeGroup.Visible = false;
                }
            }
        }

        private void LueCashType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCashType, new Sm.RefreshLue2(SetLueCashType), Sm.GetLue(LueCashTypeGroup));
            }
        }

        private void LueCashType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCashType_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueCashType.Visible && fAccept && fCell.ColIndex == 19)
                {
                    if (Sm.GetLue(LueCashType).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 18].Value =
                        Grd1.Cells[fCell.RowIndex, 19].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 18].Value = Sm.GetLue(LueCashType);
                        Grd1.Cells[fCell.RowIndex, 19].Value = LueCashType.GetColumnValue("Col2");
                    }
                    LueCashType.Visible = false;
                }
            }
        }


        #endregion

        #endregion
    }
}
