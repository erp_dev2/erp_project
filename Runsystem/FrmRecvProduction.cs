﻿#region Update
/* 
    20/06/2017 [TKG] Update remark di journal
    23/07/2018 [TKG] mempercepat proses cancel
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/07/2019 [TKG] stock price menggunakan price dari sfc
    13/08/2021 [TKG/ALL] tambah validasi setting journal, ubah GetParameter()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvProduction : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvProductionFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal int mNumberOfPlanningUomCode = 1;
        private string 
            mDocType = "23",
            mDocNoSeqNumberType = "1";
        private bool 
            mIsAutoJournalActived = false,
            mIsCancelledRecvProductionHasDOWhsValidation = false,
            mIsJournalValidationRecvProductionEnabled = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
        
        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Production";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetNumberOfInventoryUomCode();
                SetNumberOfPlanningUomCode();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                LueProperty.Visible = false;
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Sequence#",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "SFC#",
                        "",

                        //6-10
                        "Sequence#",
                        "Work Center",
                        "Item's Code",
                        "",
                        "Item's Name",

                        //11-15
                        "PropCode",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",

                        //16-20
                        "Bin",
                        "Planning"+Environment.NewLine+"Quantity",
                        "Planning"+Environment.NewLine+"UoM",
                        "Planning"+Environment.NewLine+"Quantity",
                        "Planning"+Environment.NewLine+"UoM",
                        
                        //21-25
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        
                        //26-30
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        //1-5
                        50, 0, 20, 130, 20, 
                        //6-10
                        0, 200, 100, 20, 250, 
                        //11-15
                        80, 0, 200, 170, 60, 
                        //16-20
                         80, 80, 80, 80, 80, 
                        //21-25
                        80, 80, 80, 80, 80,  
                        //26-30
                        80, 80, 80, 80, 400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 19, 21, 22, 24, 25, 27, 28 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 8, 9, 11, 12, 14, 19, 20, 24, 25, 26, 27, 28, 29 }, false);
            ShowInventoryUomCode();

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 16;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "SFC#",

                        //1-5
                        "",
                        "Sequence#",
                        "Work Center",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's Name",
                        "Batch#",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Balance",

                        //11-15
                        "Planning"+Environment.NewLine+"UoM",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Balance",
                        "Planning"+Environment.NewLine+"UoM"
                    },
                     new int[] 
                    {
                        //0
                        130,
 
                        //1-5
                        20, 0, 200, 80, 20,
 
                        //6-10
                        250, 200, 80, 80, 80,
 
                        //11-15
                        80, 80, 80, 80, 80
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9, 10, 12, 13, 14 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 1, 5 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 4, 5, 12, 13, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

            #endregion

            ShowPlanningUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 12, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 12, 13, 14, 15 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 12, 15, 16, 17, 19, 22, 25, 28, 30 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 17, 19, 21, 22, 24, 25, 27, 28 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 12, 13, 14 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvProductionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }


        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint((int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (mDocNoSeqNumberType == "2")
                DocNo = Sm.GenerateDocNo2(Sm.GetDte(DteDocDt), "RecvProduction", "TblRecvProductionHdr");
            else
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvProduction", "TblRecvProductionHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvProductionHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvProductionDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));
            cml.Add(SaveShopFloorControlDtl(DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Production's warehouse") ||
                Sm.IsLueEmpty(LueWhsCode, "Transfer To") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsSFCBalanceNotValid() ||
                IsJournalSettingInvalid();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationRecvProductionEnabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And (B.AcNo Is Null Or B.AcNo3 Is Null) ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "SFC# is empty.")) return true;
                Msg =
                    "SFC# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Work Center : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row,10) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 16) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 17) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Planning) should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Planning 2) should be greater than 0.");
                        return true;
                    }
                }

                if (Sm.GetGrdDec(Grd1, Row, 22) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[25].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 25) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }

                if (Grd1.Cols[28].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 28) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsSFCBalanceNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                {
                    Msg =
                        "SFC# : " + Sm.GetGrdStr(Grd2, Row, 0) + Environment.NewLine +
                        "Work Center : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 6) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine + Environment.NewLine;

                    if (Sm.GetGrdDec(Grd2, Row, 10) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                        return true;
                    }
                    if (Grd1.Cols[14].Visible && Sm.GetGrdDec(Grd2, Row, 14) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg +"Balance (2) should not be less than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveRecvProductionHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "/* Recv From Production */ " +
                    "Insert Into TblRecvProductionHdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvProductionDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "/* Recv From Production */ " +
                    "Insert Into TblRecvProductionDtl(DocNo, DNo, CancelInd, ShopFloorControlDocNo, ShopFloorControlDNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, QtyPlanning, QtyPlanning2, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ShopFloorControlDocNo, @ShopFloorControlDNo, @ItCode, IfNull(@PropCode, '-'), IfNull(@BatchNo, '-'), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), " +
                    "@QtyPlanning, @QtyPlanning2, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@QtyPlanning", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@QtyPlanning2", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControlDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Recv From Production */ ");
            SQL.AppendLine("Update TblShopFloorControlDtl T1 ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select T.* From (");
            SQL.AppendLine("        Select A.DocNo, A.DNo, ");
            
            if (mNumberOfPlanningUomCode==2)
                SQL.AppendLine("        Case When (IfNull(C.Qty, 0)=0 And IfNull(C.Qty2, 0)=0) Then 'O' ");
            else
                SQL.AppendLine("        Case When IfNull(C.Qty, 0)=0 Then 'O' ");
            
            SQL.AppendLine("        Else ");

            if (mNumberOfPlanningUomCode == 2)
                SQL.AppendLine("            Case When (A.Qty=IfNull(C.Qty, 0) And A.Qty2=IfNull(C.Qty2, 0)) Then 'F' ");
            else
                SQL.AppendLine("            Case When A.Qty=IfNull(C.Qty, 0) Then 'F' ");

            SQL.AppendLine("            Else 'P' End ");
            SQL.AppendLine("        End As ProcessInd ");
            SQL.AppendLine("        From TblShopFloorControlDtl A ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Distinct ShopFloorControlDocNo, ShopFloorControlDNo ");
            SQL.AppendLine("            From TblRecvProductionDtl ");
            SQL.AppendLine("            Where DocNo=@DocNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.ShopFloorControlDocNo And A.DNo=B.ShopFloorControlDNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select C1.ShopFloorControlDocNo, C1.ShopFloorControlDNo, ");
            SQL.AppendLine("            Sum(C1.QtyPlanning) As Qty ");

            if (mNumberOfPlanningUomCode == 2)
                SQL.AppendLine("            , Sum(C1.QtyPlanning2) As Qty2  ");
            SQL.AppendLine("            From TblRecvProductionDtl C1 ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct ShopFloorControlDocNo, ShopFloorControlDNo ");
            SQL.AppendLine("                From TblRecvProductionDtl ");
            SQL.AppendLine("                Where DocNo=@DocNo ");
            SQL.AppendLine("            ) C2 ");
            SQL.AppendLine("            On C1.ShopFloorControlDocNo=C2.ShopFloorControlDocNo ");
            SQL.AppendLine("            And C1.ShopFloorControlDNo=C2.ShopFloorControlDNo ");
            SQL.AppendLine("            Where C1.CancelInd='N' ");
            SQL.AppendLine("            Group By C1.ShopFloorControlDocNo, C1.ShopFloorControlDNo ");
            SQL.AppendLine("        ) C On A.DocNo=C.ShopFloorControlDocNo And A.DNo=C.ShopFloorControlDNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd=T2.ProcessInd, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where T1.ProcessInd<>T2.ProcessInd; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Recv From Production */ ");
            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* Recv From Production */ ");
            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo,  B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* Recv From Production */ ");
            SQL.AppendLine("Insert Into TblStockPrice(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("(Select ParValue from TblParameter Where ParCode='MainCurCode'), C.UPrice, 1.00, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblRecvProductionHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Production : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvProductionHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, B.UPrice*A.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo3 As AcNo, 0 As DAmt, B.UPrice*A.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo3 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void CancelData()
        {
            UpdateCancelledItem();
            ReComputeStock();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            //cml.Add(CancelDOProductionDtl(DNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(EditRecvProductionDtl(r));
            }

            cml.Add(SaveShopFloorControlDtl(TxtDocNo.Text));

            if (mIsAutoJournalActived && IsJournalDataExisted()) 
                cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOProductionDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledQtyBiggerThanStock() ||
                IsDOAlreadyExisted();
        }

        private bool IsDOAlreadyExisted()
        {
            if (!mIsCancelledRecvProductionHasDOWhsValidation) return false;

            if (Grd1.Rows.Count >= 1)
            {
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (
                        Sm.GetGrdBool(Grd1, r, 1) && 
                        !Sm.GetGrdBool(Grd1, r, 2) && 
                        Sm.GetGrdStr(Grd1, r, 14).Length > 0 &&
                        IsDOAlreadyExisted(r)
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Item Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine + 
                            "Item Name : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + 
                            "Batch# : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine + 
                            "Source : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine +
                            "You can't cancel this item." + Environment.NewLine + 
                            "This item already processed to outstanding DO to other warehouse."
                            );
                        return true;        
                    }
                }
            }
            return false;
        }

        private bool IsDOAlreadyExisted(int r)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.ProcessInd='O' ");
            SQL.AppendLine("    And B.Source=@Source ");
            SQL.AppendLine("Where A.Status='A' And A.CancelInd='N' ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 14));

            if (Sm.IsDataExist(cm)) return true;
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledQtyBiggerThanStock()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length>0 &&
                    Sm.GetGrdBool(Grd1, Row, 1) &&
                    !Sm.GetGrdBool(Grd1, Row, 2)
                    )
                {
                    Msg =
                        "SFC# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Work Center : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Local Code : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Property : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 16) + Environment.NewLine + Environment.NewLine;

                    if (Sm.GetGrdDec(Grd1, Row, 22) > Sm.GetGrdDec(Grd1, Row, 21))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should be greater than cancelled quantity.");
                        return true;
                    }

                    if (Grd1.Cols[25].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 25) > Sm.GetGrdDec(Grd1, Row, 24))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should be greater than cancelled quantity (2).");
                            return true;
                        }
                    }

                    if (Grd1.Cols[26].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 28) > Sm.GetGrdDec(Grd1, Row, 27))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should be greater than cancelled quantity (3).");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblRecvProductionHdr Where JournalDocNo Is Not Null And DocNo=@Param Limit 1;",
                TxtDocNo.Text);
        }

        private MySqlCommand EditRecvProductionDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvProductionDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1.00*B.Qty, -1.00*B.Qty2, -1.00*B.Qty3, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var cm = new MySqlCommand();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2))
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DNo=@DNo" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length>0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1<>0 ";

            SQL.AppendLine("Update TblRecvProductionDtl A Set ");
            SQL.AppendLine("    A.JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");
            
            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Receiving Item From Production : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblRecvProductionHdr Where DocNo=@DocNo); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo3 As AcNo, B.UPrice*A.Qty As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo3 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, B.UPrice*A.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        internal void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRecvProductionHdr(DocNo);
                InsertSummary(DocNo);
                ShowRecvProductionDtl(DocNo);
                ReComputeSummary();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvProductionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, WhsCode2, WhsCode, Remark ");
            SQL.AppendLine("From TblRecvProductionHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-4
                        "DocDt", "WhsCode2", "WhsCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[2]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowRecvProductionDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ShopFloorControlDocNo, B.ShopFloorControlDNo, G.DocName, ");
            SQL.AppendLine("B.ItCode, C.ItName, B.PropCode, E.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPlanning, B.QtyPlanning2, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.PlanningUomCode, C.PlanningUomCode2, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.Remark, ");
            SQL.AppendLine("IfNull(D.Qty, 0) As Stock, IfNull(D.Qty2, 0) As Stock2, IfNull(D.Qty3, 0) As Stock3 ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblStockSummary D ");
            SQL.AppendLine("    On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("    And B.ItCode=D.ItCode ");
            SQL.AppendLine("    And B.PropCode=D.PropCode");
            SQL.AppendLine("    And B.BatchNo=D.BatchNo ");
            SQL.AppendLine("    And B.Source=D.Source ");
            SQL.AppendLine("    And B.Lot=D.Lot ");
            SQL.AppendLine("    And B.Bin=D.Bin ");
            SQL.AppendLine("Left Join TblProperty E On B.PropCode = E.PropCode");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr F On B.ShopFloorControlDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr G On F.WorkCenterDocNo=G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ShopFloorControlDocNo", "ShopFloorControlDNo", "DocName", "ItCode",    
                    
                    //6-10
                    "ItName", "PropCode", "PropName", "BatchNo", "Source",    
                    
                    //11-15
                    "Lot", "Bin", "QtyPlanning", "PlanningUomCode", "QtyPlanning2",   
                    
                    //16-20
                    "PlanningUomCode2", "Stock", "Qty", "InventoryUomCode", "Stock2", 
                    
                    //21-25
                    "Qty2", "InventoryUomCode2", "Stock3", "Qty3",  "InventoryUomCode3", 
                    
                    //26
                    "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 26);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 19, 21, 22, 24, 25, 27, 28 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode2, "Production's warehouse"))
                    { 
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvProductionDlg(this, Sm.GetLue(LueWhsCode2)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 12, 15, 16, 17, 19, 22, 25, 28, 30 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 19, 21, 22, 24, 25, 27, 28 });
                    }
                }
                else
                {
                    if (
                        e.ColIndex == 1 && 
                        (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueProperty, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLuePropertyCode(ref LueProperty, Sm.GetGrdStr(Grd1, e.RowIndex, 7)); 
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                            RemoveSummary();
                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (
                e.ColIndex == 3 && 
                BtnSave.Enabled && 
                !Sm.IsLueEmpty(LueWhsCode2, "Production's warehouse") 
                && TxtDocNo.Text.Length == 0
                )
                Sm.FormShowDialog(new FrmRecvProductionDlg(this, Sm.GetLue(LueWhsCode2)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17, 19, 22, 25, 28 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 15, 16, 30 }, e);

            if (e.ColIndex == 17)
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 17, 19, 18, 20);

            if (e.ColIndex == 19)
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 19, 17, 20, 18);

            if (e.ColIndex == 22)
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 22, 25, 28, 23, 26, 29);

            if (e.ColIndex == 25)
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 25, 22, 28, 26, 23, 29);


            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 22, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 26)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 25, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 29)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 19 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 20), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 22, Grd1, e.RowIndex, 19);

            if (e.ColIndex == 19 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 20), Sm.GetGrdStr(Grd1, e.RowIndex, 26)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 25, Grd1, e.RowIndex, 19);

            if (e.ColIndex == 19 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 20), Sm.GetGrdStr(Grd1, e.RowIndex, 29)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 19);

            if (e.ColIndex == 22 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 23), Sm.GetGrdStr(Grd1, e.RowIndex, 26)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 25, Grd1, e.RowIndex, 22);

            if (e.ColIndex == 22 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 23), Sm.GetGrdStr(Grd1, e.RowIndex, 29)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 22);

            if (e.ColIndex == 25 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 26), Sm.GetGrdStr(Grd1, e.RowIndex, 29)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 25);

            if (Sm.IsGrdColSelected(new int[] { 1, 17, 19 }, e.ColIndex)) ReComputeSummary();

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 17, 19, 21, 22, 24, 25, 27, 28 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsJournalValidationRecvProductionEnabled', 'IsAutoJournalActived', 'IsCancelledRecvProductionHasDOWhsValidation', 'DocNoSeqNumberType' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsJournalValidationRecvProductionEnabled": mIsJournalValidationRecvProductionEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCancelledRecvProductionHasDOWhsValidation": mIsCancelledRecvProductionHasDOWhsValidation = ParValue == "Y"; break;
                                                
                            //string
                            case "DocNoSeqNumberType": mDocNoSeqNumberType = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

        }

        private void SetLuePropertyCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropCode As Col1, B.PropName As Col2 ");
            SQL.AppendLine("From TblItemProperty A ");
            SQL.AppendLine("Inner Join TblProperty B On A.PropCode = B.PropCode ");
            SQL.AppendLine("Where ItCode = '"+ItCode+"';");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void SetNumberOfPlanningUomCode()
        {
            string NumberOfPlanningUomCode = Sm.GetParameter("NumberOfPlanningUomCode");
            if (NumberOfPlanningUomCode.Length == 0)
                mNumberOfPlanningUomCode = 1;
            else
                mNumberOfPlanningUomCode = int.Parse(NumberOfPlanningUomCode);
        }

        private string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            Sm.GetGrdStr(Grd1, Row, 14) +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            Sm.GetGrdStr(Grd1, Row, 16) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 14).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 14) +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            Sm.GetGrdStr(Grd1, Row, 16) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedSFC()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row, 0) +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 14).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 14);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 14), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 16), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 27, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void ReComputeSummary()
        {
            decimal Qty = 0m, Qty2 = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 0).Length != 0)
                {
                    Qty = 0m;
                    Qty2 = 0m;
                    for (int row2 = 0; row2 < Grd1.Rows.Count; row2++)
                    {
                        if (
                           !Sm.GetGrdBool(Grd1, row2, 1) &&
                           Sm.CompareGrdStr(Grd2, row, 0, Grd1, row2, 4) &&
                           Sm.CompareGrdStr(Grd2, row, 2, Grd1, row2, 6)
                           )
                        {
                            Qty += Sm.GetGrdDec(Grd1, row2, 17);
                            Qty2 += Sm.GetGrdDec(Grd1, row2, 19);
                        }
                    }
                    Grd2.Cells[row, 9].Value = Qty;
                    Grd2.Cells[row, 10].Value = Sm.GetGrdDec(Grd2, row, 8) - Qty;
                    Grd2.Cells[row, 13].Value = Qty2;
                    Grd2.Cells[row, 14].Value = Sm.GetGrdDec(Grd2, row, 12) - Qty2;
                }
            }
        }

        private void RemoveSummary()
        {
            bool NotFound = true;
            for (int row = Grd2.Rows.Count - 1; row >= 0; row--)
            {
                if (Sm.GetGrdStr(Grd2, row, 0).Length != 0)
                {
                    NotFound = true;
                    for (int row2 = 0; row2 < Grd1.Rows.Count; row2++)
                    {
                        if (
                            Sm.CompareGrdStr(Grd2, row, 0, Grd1, row2, 4) &&
                            Sm.CompareGrdStr(Grd2, row, 2, Grd1, row2, 6) 
                            )
                        {
                            NotFound = false;
                            break;
                        }
                    }
                    if (NotFound)
                    {
                        Grd2.Rows.RemoveAt(row);
                        if (Grd2.Rows.Count <= 0)
                        {
                            Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 12, 13, 14 });
                        }
                    }
                    ReComputeSummary();
                }
            }
        }

        private void InsertSummary(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, ");
            SQL.AppendLine("F.DocName, A.ItCode, C.ItName, A.BatchNo, ");
            SQL.AppendLine("(A.Qty-IfNull(B.RecvProductionQty, 0)) As Qty, C.PlanningUomCode, ");
            SQL.AppendLine("(A.Qty2-IfNull(B.RecvProductionQty2, 0)) As Qty2, C.PlanningUomCode2 ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.ShopFloorControlDocNo, T1.ShopFloorControlDNo, ");
            SQL.AppendLine("    Sum(T1.QtyPlanning) As RecvProductionQty, Sum(T1.QtyPlanning2) As RecvProductionQty2  ");
            SQL.AppendLine("    From TblRecvProductionDtl T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct ShopFloorControlDocNo, ShopFloorControlDNo ");
            SQL.AppendLine("        From TblRecvProductionDtl ");
            SQL.AppendLine("        Where DocNo=@DocNo ");
            SQL.AppendLine("    ) T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T1.DNo=T2.ShopFloorControlDNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.ShopFloorControlDocNo, T1.ShopFloorControlDNo ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.DocNo=B.ShopFloorControlDocNo ");
            SQL.AppendLine("    And A.DNo=B.ShopFloorControlDNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct ShopFloorControlDocNo, ShopFloorControlDNo ");
            SQL.AppendLine("    From TblRecvProductionDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.DocNo=D.ShopFloorControlDocNo ");
            SQL.AppendLine("    And A.DNo=D.ShopFloorControlDNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            SQL.AppendLine("Order By A.DocNo, A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DNo", "DocName", "ItCode", "ItName", "BatchNo", 
                    
                    //6-9
                    "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 12, 13, 14 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ReComputeOutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("(A.Qty-IfNull(B.RecvQty, 0)) As Qty, (A.Qty2-IfNull(B.RecvQty2, 0)) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ShopFloorControlDocNo As DocNo, ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(QtyPlanning) As RecvQty, Sum(QtyPlanning2) As RecvQty2 ");
            SQL.AppendLine("    From TblRecvProductionDtl ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Locate(Concat('##', ShopFloorControlDocNo, ShopFloorControlDNo, '##'), @SelectedSFC)>0 ");
            SQL.AppendLine("    Group By ShopFloorControlDocNo, ShopFloorControlDNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @SelectedSFC)>0 ");
            SQL.AppendLine("Order By DocNo, DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedSFC", GetSelectedSFC());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "DocNo", "DNo", "Qty", "Qty2" });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 12, 3);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
            ReComputeSummary();
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvProdHdr>();
            var ldtl = new List<RecvProdDtl>();

            string[] TableName = { "RecvProdHdr", "RecvProdDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName As WhsFrom, C.WhsName As WhsTo, A.Remark From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2 = C.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         //6-10
                         "WhsFrom",
                         "WhsTo",
                         "Remark",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvProdHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsFrom = Sm.DrStr(dr, c[6]),
                            WhsTo = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select B.ShopFloorControlDocNo, B.ShopFloorControlDNo, ");
                SQLDtl.AppendLine("B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQLDtl.AppendLine("B.QtyPlanning, B.Qty, B.Qty2, B.Qty3, ");
                SQLDtl.AppendLine("C.PlanningUomCode, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
                SQLDtl.AppendLine("B.Remark, ");
                SQLDtl.AppendLine("IfNull(D.Qty, 0) As Stock, IfNull(D.Qty2, 0) As Stock2, IfNull(D.Qty3, 0) As Stock3 ");
                SQLDtl.AppendLine("From TblRecvProductionHdr A ");
                SQLDtl.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Inner Join TblStockSummary D ");
                SQLDtl.AppendLine("    On A.WhsCode=D.WhsCode ");
                SQLDtl.AppendLine("    And B.ItCode=D.ItCode ");
                SQLDtl.AppendLine("    And B.BatchNo=D.BatchNo ");
                SQLDtl.AppendLine("    And B.Source=D.Source ");
                SQLDtl.AppendLine("    And B.Lot=D.Lot ");
                SQLDtl.AppendLine("    And B.Bin=D.Bin ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.CancelInd = 'N' ");
                SQLDtl.AppendLine("Order By B.DNo;");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName",
                         "BatchNo",
                         "Source",
                         "Lot",
                         "Bin",
                        

                         //6-10
                         "Qty" ,
                         "Qty2",
                         "Qty3",
                         "InventoryUomCode" ,                        
                         "InventoryUomCode2" ,
                         
                         //11
                         "InventoryUomCode3" ,
                         "Remark"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RecvProdDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),

                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            Remark = Sm.DrStr(drDtl, cDtl[12])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion


            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("RecvProd1", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("RecvProd2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("RecvProd3", myLists, TableName, false);
                    break;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            ClearGrd();
        }

        private void LueProperty_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProperty, new Sm.RefreshLue2(SetLuePropertyCode), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7));
        }

        private void LueProperty_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueProperty_Leave(object sender, EventArgs e)
        {
            if (LueProperty.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueProperty).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 11].Value =
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 11].Value = Sm.GetLue(LueProperty);
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueProperty.GetColumnValue("Col2");
                }
                LueProperty.Visible = false;
            }
        }


        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }

    #region Report Class

    class RecvProdHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string WhsFrom { get; set; }
        public string WhsTo { get; set; }
        public string Remark { get; set; }
        public string PrintBy { get; set; }
    }

    class RecvProdDtl
    {
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string BatchNo { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }

        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }

        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }

        public string Remark { get; set; }
    }

    #endregion
}
