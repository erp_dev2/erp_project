﻿namespace RunSystem
{
    partial class FrmEmpLeave4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpLeave4));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TcEmpLeave3 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.DteLeaveStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.DteResignDt = new DevExpress.XtraEditors.DateEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnDurationHrDefault = new DevExpress.XtraEditors.SimpleButton();
            this.ChkBreakInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtDurationDay = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDurationHour = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueLeaveCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TmeStartTm = new DevExpress.XtraEditors.TimeEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TmeEndTm = new DevExpress.XtraEditors.TimeEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcEmpLeave3)).BeginInit();
            this.TcEmpLeave3.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBreakInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDurationDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDurationHour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLeaveCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeStartTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEndTm.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcEmpLeave3);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 74);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(391, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(381, 74);
            this.panel4.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(7, 8);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 8;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(7, 27);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(368, 20);
            this.MeeCancelReason.TabIndex = 9;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(7, 48);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 10;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(80, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(104, 20);
            this.DteDocDt.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(44, 30);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 3;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(80, 49);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(104, 20);
            this.TxtStatus.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(35, 52);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "Status";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(80, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(251, 20);
            this.TxtDocNo.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(4, 9);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 1;
            this.label18.Text = "Document#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcEmpLeave3
            // 
            this.TcEmpLeave3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcEmpLeave3.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcEmpLeave3.Location = new System.Drawing.Point(0, 74);
            this.TcEmpLeave3.Name = "TcEmpLeave3";
            this.TcEmpLeave3.SelectedTabPage = this.Tp1;
            this.TcEmpLeave3.Size = new System.Drawing.Size(772, 399);
            this.TcEmpLeave3.TabIndex = 11;
            this.TcEmpLeave3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp3,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.DteLeaveStartDt);
            this.Tp1.Controls.Add(this.label24);
            this.Tp1.Controls.Add(this.DteResignDt);
            this.Tp1.Controls.Add(this.label22);
            this.Tp1.Controls.Add(this.BtnDurationHrDefault);
            this.Tp1.Controls.Add(this.ChkBreakInd);
            this.Tp1.Controls.Add(this.TxtSiteCode);
            this.Tp1.Controls.Add(this.label21);
            this.Tp1.Controls.Add(this.TxtDurationDay);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.MeeRemark);
            this.Tp1.Controls.Add(this.label12);
            this.Tp1.Controls.Add(this.TxtDurationHour);
            this.Tp1.Controls.Add(this.label15);
            this.Tp1.Controls.Add(this.BtnEmpCode);
            this.Tp1.Controls.Add(this.DteJoinDt);
            this.Tp1.Controls.Add(this.label10);
            this.Tp1.Controls.Add(this.TxtDeptCode);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.TxtPosCode);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.TxtEmpCodeOld);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.TxtEmpName);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.TxtEmpCode);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.LueLeaveCode);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.DteStartDt);
            this.Tp1.Controls.Add(this.label2);
            this.Tp1.Controls.Add(this.TmeStartTm);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.TmeEndTm);
            this.Tp1.Controls.Add(this.label23);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 371);
            this.Tp1.Text = "General";
            // 
            // DteLeaveStartDt
            // 
            this.DteLeaveStartDt.EditValue = null;
            this.DteLeaveStartDt.EnterMoveNextControl = true;
            this.DteLeaveStartDt.Location = new System.Drawing.Point(130, 222);
            this.DteLeaveStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt.Name = "DteLeaveStartDt";
            this.DteLeaveStartDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteLeaveStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLeaveStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.ReadOnly = true;
            this.DteLeaveStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt.Size = new System.Drawing.Size(112, 20);
            this.DteLeaveStartDt.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(28, 225);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(97, 14);
            this.label24.TabIndex = 33;
            this.label24.Text = "Permanent Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteResignDt
            // 
            this.DteResignDt.EditValue = null;
            this.DteResignDt.EnterMoveNextControl = true;
            this.DteResignDt.Location = new System.Drawing.Point(130, 201);
            this.DteResignDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDt.Name = "DteResignDt";
            this.DteResignDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteResignDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteResignDt.Properties.Appearance.Options.UseFont = true;
            this.DteResignDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDt.Properties.ReadOnly = true;
            this.DteResignDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDt.Size = new System.Drawing.Size(112, 20);
            this.DteResignDt.TabIndex = 32;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(54, 204);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 14);
            this.label22.TabIndex = 31;
            this.label22.Text = "Resign Date";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDurationHrDefault
            // 
            this.BtnDurationHrDefault.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDurationHrDefault.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDurationHrDefault.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDurationHrDefault.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnDurationHrDefault.Appearance.Options.UseBackColor = true;
            this.BtnDurationHrDefault.Appearance.Options.UseFont = true;
            this.BtnDurationHrDefault.Appearance.Options.UseForeColor = true;
            this.BtnDurationHrDefault.Appearance.Options.UseTextOptions = true;
            this.BtnDurationHrDefault.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDurationHrDefault.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDurationHrDefault.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDurationHrDefault.Location = new System.Drawing.Point(244, 284);
            this.BtnDurationHrDefault.Name = "BtnDurationHrDefault";
            this.BtnDurationHrDefault.Size = new System.Drawing.Size(50, 21);
            this.BtnDurationHrDefault.TabIndex = 43;
            this.BtnDurationHrDefault.Text = "Default";
            this.BtnDurationHrDefault.ToolTip = "Set Default";
            this.BtnDurationHrDefault.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDurationHrDefault.ToolTipTitle = "Run System";
            this.BtnDurationHrDefault.Click += new System.EventHandler(this.BtnDurationHrDefault_Click);
            // 
            // ChkBreakInd
            // 
            this.ChkBreakInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkBreakInd.Location = new System.Drawing.Point(297, 284);
            this.ChkBreakInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkBreakInd.Name = "ChkBreakInd";
            this.ChkBreakInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkBreakInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBreakInd.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ChkBreakInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkBreakInd.Properties.Appearance.Options.UseFont = true;
            this.ChkBreakInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkBreakInd.Properties.Caption = "Include Break Time";
            this.ChkBreakInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBreakInd.Size = new System.Drawing.Size(140, 22);
            this.ChkBreakInd.TabIndex = 44;
            this.ChkBreakInd.CheckedChanged += new System.EventHandler(this.ChkBreakInd_CheckedChanged);
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(130, 159);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 80;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(369, 20);
            this.TxtSiteCode.TabIndex = 28;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(99, 162);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 14);
            this.label21.TabIndex = 27;
            this.label21.Text = "Site";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDurationDay
            // 
            this.TxtDurationDay.EnterMoveNextControl = true;
            this.TxtDurationDay.Location = new System.Drawing.Point(130, 243);
            this.TxtDurationDay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDurationDay.Name = "TxtDurationDay";
            this.TxtDurationDay.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDurationDay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDurationDay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDurationDay.Properties.Appearance.Options.UseFont = true;
            this.TxtDurationDay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDurationDay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDurationDay.Properties.ReadOnly = true;
            this.TxtDurationDay.Size = new System.Drawing.Size(112, 20);
            this.TxtDurationDay.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(40, 246);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 35;
            this.label16.Text = "Duration (Day)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(130, 306);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(650, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(315, 20);
            this.MeeRemark.TabIndex = 46;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(78, 309);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 45;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDurationHour
            // 
            this.TxtDurationHour.EnterMoveNextControl = true;
            this.TxtDurationHour.Location = new System.Drawing.Point(130, 285);
            this.TxtDurationHour.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDurationHour.Name = "TxtDurationHour";
            this.TxtDurationHour.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDurationHour.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDurationHour.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDurationHour.Properties.Appearance.Options.UseFont = true;
            this.TxtDurationHour.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDurationHour.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDurationHour.Size = new System.Drawing.Size(112, 20);
            this.TxtDurationHour.TabIndex = 42;
            this.TxtDurationHour.Validated += new System.EventHandler(this.TxtDurationHour_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(33, 288);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 14);
            this.label15.TabIndex = 41;
            this.label15.Text = "Duration (Hour)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(271, 54);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 18;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(130, 180);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.ReadOnly = true;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(112, 20);
            this.DteJoinDt.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(69, 183);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Join Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(130, 138);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 80;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(369, 20);
            this.TxtDeptCode.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(54, 141);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Department";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(130, 117);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 80;
            this.TxtPosCode.Properties.ReadOnly = true;
            this.TxtPosCode.Size = new System.Drawing.Size(369, 20);
            this.TxtPosCode.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(78, 120);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 23;
            this.label8.Text = "Position";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(130, 96);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 30;
            this.TxtEmpCodeOld.Properties.ReadOnly = true;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(140, 20);
            this.TxtEmpCodeOld.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(70, 98);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Old Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(130, 75);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 80;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(369, 20);
            this.TxtEmpName.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(24, 78);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Employee\'s Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(130, 54);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 30;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(140, 20);
            this.TxtEmpCode.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(27, 57);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Employee\'s Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLeaveCode
            // 
            this.LueLeaveCode.EnterMoveNextControl = true;
            this.LueLeaveCode.Location = new System.Drawing.Point(130, 12);
            this.LueLeaveCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLeaveCode.Name = "LueLeaveCode";
            this.LueLeaveCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveCode.Properties.Appearance.Options.UseFont = true;
            this.LueLeaveCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLeaveCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLeaveCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLeaveCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLeaveCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLeaveCode.Properties.DropDownRows = 30;
            this.LueLeaveCode.Properties.NullText = "[Empty]";
            this.LueLeaveCode.Properties.PopupWidth = 380;
            this.LueLeaveCode.Size = new System.Drawing.Size(369, 20);
            this.LueLeaveCode.TabIndex = 13;
            this.LueLeaveCode.ToolTip = "F4 : Show/hide list";
            this.LueLeaveCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLeaveCode.EditValueChanged += new System.EventHandler(this.LueLeaveCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(53, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Leave Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(130, 33);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(139, 20);
            this.DteStartDt.TabIndex = 15;
            this.DteStartDt.EditValueChanged += new System.EventHandler(this.DteStartDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(58, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Leave Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeStartTm
            // 
            this.TmeStartTm.EditValue = null;
            this.TmeStartTm.EnterMoveNextControl = true;
            this.TmeStartTm.Location = new System.Drawing.Point(130, 264);
            this.TmeStartTm.Name = "TmeStartTm";
            this.TmeStartTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeStartTm.Properties.Appearance.Options.UseFont = true;
            this.TmeStartTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeStartTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeStartTm.Size = new System.Drawing.Size(112, 20);
            this.TmeStartTm.TabIndex = 38;
            this.TmeStartTm.EditValueChanged += new System.EventHandler(this.TmeStartTm_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(56, 267);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 37;
            this.label7.Text = "Leave Time";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeEndTm
            // 
            this.TmeEndTm.EditValue = null;
            this.TmeEndTm.EnterMoveNextControl = true;
            this.TmeEndTm.Location = new System.Drawing.Point(261, 264);
            this.TmeEndTm.Name = "TmeEndTm";
            this.TmeEndTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeEndTm.Properties.Appearance.Options.UseFont = true;
            this.TmeEndTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeEndTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeEndTm.Size = new System.Drawing.Size(112, 20);
            this.TmeEndTm.TabIndex = 40;
            this.TmeEndTm.EditValueChanged += new System.EventHandler(this.TmeEndTm_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(245, 267);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 14);
            this.label23.TabIndex = 39;
            this.label23.Text = "-";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd1);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 131);
            this.Tp3.Text = "Leave Date List";
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 131);
            this.Grd1.TabIndex = 12;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 131);
            this.Tp2.Text = "Approval Information";
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 131);
            this.Grd2.TabIndex = 12;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // FrmEmpLeave3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmEmpLeave3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcEmpLeave3)).EndInit();
            this.TcEmpLeave3.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBreakInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDurationDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDurationHour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLeaveCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeStartTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEndTm.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraTab.XtraTabControl TcEmpLeave3;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.DateEdit DteResignDt;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.SimpleButton BtnDurationHrDefault;
        private DevExpress.XtraEditors.CheckEdit ChkBreakInd;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label21;
        public DevExpress.XtraEditors.TextEdit TxtDurationDay;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDurationHour;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPosCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueLeaveCode;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TimeEdit TmeStartTm;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TimeEdit TmeEndTm;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        protected TenTec.Windows.iGridLib.iGrid Grd1;
    }
}