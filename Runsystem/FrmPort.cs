﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPort : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mPortCode = string.Empty;
        internal FrmPortFind FrmFind;

        #endregion

        #region Constructor

        public FrmPort(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
           
            //if this application is called from other application
            if (mPortCode.Length != 0)
            {
                ShowData(mPortCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPortCode, TxtPortName
                    }, true);
                    ChkLoadingInd.Properties.ReadOnly = true;
                    ChkDischargeInd.Properties.ReadOnly = true;
                    TxtPortCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPortCode, TxtPortName
                    }, false);
                    ChkLoadingInd.Properties.ReadOnly = false;
                    ChkDischargeInd.Properties.ReadOnly = false;
                    TxtPortCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPortName
                    }, false);
                    ChkLoadingInd.Properties.ReadOnly = false;
                    ChkDischargeInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtPortName, TxtPortCode
            });
            ChkLoadingInd.Checked = false;
            ChkDischargeInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPortFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPortCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPort(PortCode, PortName, LoadingInd, DischargeInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PortCode, @PortName, @LoadingInd, @DischargeInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PortName=@PortName, LoadingInd=@LoadingInd, DischargeInd=@DischargeInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PortCode", TxtPortCode.Text);
                Sm.CmParam<String>(ref cm, "@PortName", TxtPortName.Text);
                Sm.CmParam<String>(ref cm, "@LoadingInd", ChkLoadingInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@DischargeInd", ChkDischargeInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPortCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PortCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PortCode", PortCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblPort Where PortCode = @PortCode",
                        new string[] 
                        {
                            "PortCode", "PortName","LoadingInd", "DischargeInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPortCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPortName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkLoadingInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            ChkDischargeInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPortCode, "Port code", false) ||
                Sm.IsTxtEmpty(TxtPortName, "Port Name", false) ||
                IsPortCodeExisted();
        }

        private bool IsPortCodeExisted()
        {
            if (!TxtPortCode.Properties.ReadOnly && Sm.IsDataExist("Select PortCode From TblPort Where PortCode ='" + TxtPortCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Port code ( " + TxtPortCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion

        #region Event
        private void TxtPortCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPortCode);
        }

        private void TxtPortName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPortName);
        }
        #endregion
    }
}
