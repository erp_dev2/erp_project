﻿namespace RunSystem
{
    partial class FrmImportData2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportData2));
            this.LblImportCode = new System.Windows.Forms.Label();
            this.LueImportCode = new DevExpress.XtraEditors.LookUpEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.BtnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFileName = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkDelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtImportCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueImportCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtImportCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panel1.Size = new System.Drawing.Size(105, 150);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtImportCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkDelInd);
            this.panel2.Controls.Add(this.BtnBrowse);
            this.panel2.Controls.Add(this.TxtFileName);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LblImportCode);
            this.panel2.Controls.Add(this.LueImportCode);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panel2.Size = new System.Drawing.Size(723, 150);
            // 
            // LblImportCode
            // 
            this.LblImportCode.AutoSize = true;
            this.LblImportCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblImportCode.ForeColor = System.Drawing.Color.Red;
            this.LblImportCode.Location = new System.Drawing.Point(64, 16);
            this.LblImportCode.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.LblImportCode.Name = "LblImportCode";
            this.LblImportCode.Size = new System.Drawing.Size(64, 22);
            this.LblImportCode.TabIndex = 3;
            this.LblImportCode.Text = "Import";
            this.LblImportCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueImportCode
            // 
            this.LueImportCode.EnterMoveNextControl = true;
            this.LueImportCode.Location = new System.Drawing.Point(137, 12);
            this.LueImportCode.Margin = new System.Windows.Forms.Padding(8);
            this.LueImportCode.Name = "LueImportCode";
            this.LueImportCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.Appearance.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueImportCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueImportCode.Properties.DropDownRows = 30;
            this.LueImportCode.Properties.MaxLength = 1;
            this.LueImportCode.Properties.NullText = "[Empty]";
            this.LueImportCode.Properties.PopupWidth = 350;
            this.LueImportCode.Size = new System.Drawing.Size(460, 28);
            this.LueImportCode.TabIndex = 4;
            this.LueImportCode.ToolTip = "F4 : Show/hide list";
            this.LueImportCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueImportCode.EditValueChanged += new System.EventHandler(this.LueImportCode_EditValueChanged);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // BtnBrowse
            // 
            this.BtnBrowse.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBrowse.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBrowse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBrowse.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBrowse.Appearance.Options.UseBackColor = true;
            this.BtnBrowse.Appearance.Options.UseFont = true;
            this.BtnBrowse.Appearance.Options.UseForeColor = true;
            this.BtnBrowse.Appearance.Options.UseTextOptions = true;
            this.BtnBrowse.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBrowse.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("BtnBrowse.Image")));
            this.BtnBrowse.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBrowse.Location = new System.Drawing.Point(599, 43);
            this.BtnBrowse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnBrowse.Name = "BtnBrowse";
            this.BtnBrowse.Size = new System.Drawing.Size(36, 32);
            this.BtnBrowse.TabIndex = 7;
            this.BtnBrowse.ToolTip = "Browse File";
            this.BtnBrowse.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBrowse.ToolTipTitle = "Run System";
            this.BtnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
            // 
            // TxtFileName
            // 
            this.TxtFileName.EnterMoveNextControl = true;
            this.TxtFileName.Location = new System.Drawing.Point(137, 44);
            this.TxtFileName.Margin = new System.Windows.Forms.Padding(8);
            this.TxtFileName.Name = "TxtFileName";
            this.TxtFileName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFileName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFileName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFileName.Properties.Appearance.Options.UseFont = true;
            this.TxtFileName.Properties.MaxLength = 25;
            this.TxtFileName.Size = new System.Drawing.Size(460, 28);
            this.TxtFileName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(40, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 22);
            this.label4.TabIndex = 5;
            this.label4.Text = "File (CSV)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkDelInd
            // 
            this.ChkDelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkDelInd.Location = new System.Drawing.Point(137, 108);
            this.ChkDelInd.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ChkDelInd.Name = "ChkDelInd";
            this.ChkDelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkDelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkDelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkDelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkDelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkDelInd.Properties.Caption = "Delete all existing data";
            this.ChkDelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDelInd.ShowToolTips = false;
            this.ChkDelInd.Size = new System.Drawing.Size(236, 27);
            this.ChkDelInd.TabIndex = 10;
            // 
            // TxtImportCode
            // 
            this.TxtImportCode.EnterMoveNextControl = true;
            this.TxtImportCode.Location = new System.Drawing.Point(138, 76);
            this.TxtImportCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtImportCode.Name = "TxtImportCode";
            this.TxtImportCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtImportCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtImportCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtImportCode.Properties.Appearance.Options.UseFont = true;
            this.TxtImportCode.Properties.MaxLength = 100;
            this.TxtImportCode.Size = new System.Drawing.Size(460, 28);
            this.TxtImportCode.TabIndex = 9;
            this.TxtImportCode.Validated += new System.EventHandler(this.TxtImportCode_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(18, 81);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 22);
            this.label8.TabIndex = 8;
            this.label8.Text = "Import Code";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmImportData2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 150);
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "FrmImportData2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueImportCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtImportCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblImportCode;
        private DevExpress.XtraEditors.LookUpEdit LueImportCode;
        private System.Windows.Forms.OpenFileDialog OD;
        public DevExpress.XtraEditors.SimpleButton BtnBrowse;
        internal DevExpress.XtraEditors.TextEdit TxtFileName;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkDelInd;
        internal DevExpress.XtraEditors.TextEdit TxtImportCode;
        private System.Windows.Forms.Label label8;
    }
}