﻿#region Update

/*
 * 11/05/2022 [SET/PRODUCT] 
 */ 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEquitySecurities : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEquitySecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DocNo, C.DocDt, D.OptDesc MenuDesc, C.InvestmentName, C.InvestmentCode, C.BankAcCode, C.BankAcNm, C.Issuer, C.InvestmentCtCode, ");
            SQL.AppendLine("C.InvestmentCtName, E.OptDesc InvestmentType, IFNULL(A.Qty, 0.00) Qty, C.UomCode, F.BasePrice, IFNULL(A.Qty, 0.00)*F.BasePrice AS InvestmentCost, C.AcquisitionPrice, ");
            SQL.AppendLine("IFNULL(A.Qty, 0.00)*C.AcquisitionPrice AS AcquisitionCost, C.SellPrice, C.SalesAmt, if(C.SellPrice=0.00, 0.00, C.SellPrice-F.BasePrice) AS PriceMargin ");
            SQL.AppendLine("FROM tblinvestmentstockmovement A ");
            SQL.AppendLine("INNER JOIN tblinvestmentstocksummary B ON A.BankAcCode = B.BankAcCode AND A.Source = B.Source ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("	SELECT A.DocNo, A.DocDt, D.PortofolioName InvestmentName, C.PortofolioId InvestmentCode, A.BankAcCode, E.BankAcNm, D.Issuer, D.InvestmentCtCode, ");
            SQL.AppendLine("	F.InvestmentCtName, B.InvestmentType, C.UomCode, B.AcquisitionPrice, B.Qty, 0.00 AS SellPrice, 0.00 AS SalesAmt, B.Source ");
            SQL.AppendLine("	FROM tblacquisitionequitysecuritieshdr A ");
            SQL.AppendLine("	INNER JOIN tblacquisitionequitysecuritiesdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	INNER JOIN tblinvestmentitemequity C ON B.InvestmentEquityCode = C.InvestmentEquityCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("	INNER JOIN tblbankaccount E ON A.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
            SQL.AppendLine("	UNION ALL ");
            SQL.AppendLine("	SELECT A.DocNo, A.DocDt, D.PortofolioName InvestmentName, C.PortofolioId InvestmentCode, B.BankAcCode, E.BankAcNm, D.Issuer, D.InvestmentCtCode, ");
            SQL.AppendLine("	F.InvestmentCtName, B.InvestmentType, C.UomCode, 0.00 AS AcquisitionPrice, B.Qty, B.SellPrice, B.SalesAmt, B.Source ");
            SQL.AppendLine("	FROM tblsalesequitysecuritieshdr A ");
            SQL.AppendLine("	INNER JOIN tblsalesequitysecuritiesdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	INNER JOIN tblinvestmentitemequity C ON B.InvestmentEquityCode = C.InvestmentEquityCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("	INNER JOIN tblbankaccount E ON B.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
            SQL.AppendLine("	UNION ALL ");
            SQL.AppendLine("	SELECT A.DocNo, A.DocDt, D.PortofolioName InvestmentName, C.PortofolioId InvestmentCode, B.BankAcCode, E.BankAcNm, D.Issuer, D.InvestmentCtCode, ");
            SQL.AppendLine("	F.InvestmentCtName, B.InvestmentType, C.UomCode, B.UPrice AcquisitionPrice, B.Qty, 0.00 AS SellPrice, 0.00 AS SalesAmt, B.Source ");
            SQL.AppendLine("	FROM tblinvestmentstockinitialhdr A ");
            SQL.AppendLine("	INNER JOIN tblinvestmentstockinitialdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	INNER JOIN tblinvestmentitemequity C ON B.InvestmentCode = C.InvestmentEquityCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("	INNER JOIN tblbankaccount E ON B.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
            SQL.AppendLine("	) C ON A.DocNo = C.DocNo AND A.Source = C.Source ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.DocType = D.OptCode AND D.OptCat = 'InvestmentTransType' ");
            SQL.AppendLine("INNER JOIN tbloption E ON C.InvestmentType = E.OptCode AND E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblinvestmentstockprice F ON A.Source = F.Source ");
            SQL.AppendLine("WHERE C.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine(Filter);

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5  
                        "Document#",
                        "Document Date",
                        "Menu Desc",
                        "Investment's" + Environment.NewLine + "Name",
                        "Investment's Code",

                        //6-10
                        "Investment's" + Environment.NewLine + "Bank Account (RDN)#",
                        "Investment's" + Environment.NewLine + "Bank Account Name",
                        "Issuer",
                        "Category Code",
                        "Category",

                        //11-15
                        "Type",
                        "Quantity",
                        "UoM",
                        "Base Price",
                        "Investment Cost",

                        //16-20
                        "Acquisition Price",
                        "Acquisition Cost",
                        "Selling Price",
                        "Sales Amount",
                        "Price Margin",

                        //21-22
                        "Realized (Gain)/Loss",
                        "Create Date",

                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 180, 150, 250, 
                        
                        //6-10
                        180, 200, 250, 0, 130,
                        
                        //11-15
                        150, 100, 130, 130, 130,
  
                        //16-20
                        150, 150, 150, 150, 150,

                        //21-22
                        150, 100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 9, 22 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20, 21 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                //Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "C.InvestmentCode", "C.InvestmentName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter) + " Order By A.InvestmentCode, E.OptDesc; ",
                new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "MenuDesc", "InvestmentName", "InvestmentCode", "BankAcCode",
                        //6-10
                        "BankAcNm", "Issuer", "InvestmentCtCode", "InvestmentCtName", "InvestmentType",
                        //11-15
                        "Qty", "UomCode", "BasePrice", "InvestmentCost", "AcquisitionPrice",
                        //16-19
                        "AcquisitionCost", "SellPrice", "SalesAmt", "PriceMargin"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                        Grd.Cells[Row, 21].Value = Sm.GetGrdDec(Grd1, Row, 12)* Sm.GetGrdDec(Grd1, Row, 20);
                        //Grd.Cells[Row, 19].Value = 0m;
                        //Grd.Cells[Row, 20].Value = 0m;
                        //Grd.Cells[Row, 21].Value = 0m;
                    }, true, false, false, false
                );

                //Sm.ComputeStockAccumulaton(Sm.GetDte(DteDocDt2), mEquityInvestmentCtCode, TxtItInvestmentCode.Text, ref Grd1, 22, 4, 9, 11, 18, 19, 20, 21);
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20, 21 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Event

        private void TxtItInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        #endregion
    }
}
