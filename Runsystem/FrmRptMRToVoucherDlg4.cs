﻿#region Update
// 13/des/2017 [HAR] Bug Fixing outgoing approvrnya gak muncul
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMRToVoucherDlg4 : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptMRToVoucher mFrmParent;
        private string mSQL = string.Empty, mDNo = string.Empty, mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMRToVoucherDlg4(FrmRptMRToVoucher FrmParent, string DNo, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDNo = DNo;
            mDocNo = DocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDept, TxtItCode, TxtItName, TxtQty
                    }, true);
                    TxtDept.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDept, TxtItCode, TxtItName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
               TxtQty
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Outgoing Payment",

                        //1-3
                        "", 
                        "Checked By",
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0,  2 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select E.OPDocNo, E.UserOP  ");
                SQL.AppendLine("From TblMaterialrequesthdr A  ");
                SQL.AppendLine("Inner Join TblmaterialRequestDtl B On A.DocNo = b.DocNo   ");
                SQL.AppendLine("Inner Join TblPorequestDtl C On C.materialRequestDocNo = B.DocNo And C.MaterialrequestDno = B.Dno And C.cancelInd = 'N'  ");
                SQL.AppendLine("Inner Join TblPODtl D On D.PORequestDocNo = C.DocNo And D.PORequestDno = C.Dno And D.cancelInd = 'N'  ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select E.PoDocNO, E.PODno, SUM(E.QTy) Qty, group_concat(distinct F.DocNo) As PIDocNo, G.OpDocNo,  ");
                SQL.AppendLine("    G.VCRDocNo, G.VCDocno, G.UserOp   ");
                SQL.AppendLine("    From  TblrecvVdHdr D  ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl E On D.DocNo = E.DocNo    ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl F On E.DocNo = F.RecvVdDocNo And E.DNo = F.RecvVdDNo  ");
                SQL.AppendLine("    left Join (  ");
		        SQL.AppendLine("        Select G.InvoiceDocNo As PIDocNo, H.DocNo AS OPDocNo,  ");
		        SQL.AppendLine("        I.DocNo AS VCRDocNo,J.DocNo AS VCDocNo, K.UserOP  ");
		        SQL.AppendLine("        From TblOutgoingpaymentDtl G "); 
		        SQL.AppendLine("        Left Join TblOutgoingpaymentHdr H On G.DocNo = H.DocNo And H.cancelInd = 'N' And H.Status = 'A'  ");
		        SQL.AppendLine("        Left Join TblVoucherRequestHdr I On H.VoucherRequestDocNo = I.DocNo  ");
		        SQL.AppendLine("        Left Join TblVoucherHdr  J On I.VoucherDocNo = J.DocNo And J.CancelInd = 'N'  ");
		        SQL.AppendLine("        Left Join (  ");
			    SQL.AppendLine("            Select A.DocNo, Case When A.LastUpDt Is Not Null Then  ");
                SQL.AppendLine("            Group_Concat(B.UserName, ' (', Concat(Substring(A.LastUpDt, 7, 2), '/', Substring(A.LastUpDt, 5, 2), '/', Left(A.LastUpDt, 4)) , ')')  ");
			    SQL.AppendLine("            else  null end As UserOP  ");
			    SQL.AppendLine("            From TblDocApproval A  ");
			    SQL.AppendLine("            Inner Join TblUser B On A.UserCode = B.UserCode  ");
			    SQL.AppendLine("            Where DocType = 'OutgoingPayment'  ");
			    SQL.AppendLine("            Group By DocNo  ");
    	        SQL.AppendLine("        ) K On H.DocNo=K.DocNo ");
                SQL.AppendLine("    )G On F.DocNo = G.PIDocNo  ");
                SQL.AppendLine("    Where E.CancelInd = 'N' And D.POInd = 'Y'  "); 
                SQL.AppendLine("    Group BY E.PoDocNO, E.PODno  ");
                SQL.AppendLine(")E On D.DocNo = E.PODocNo And D.Dno = E.PODno  ");
                SQL.AppendLine("Where 0=0 ");
                
                   
                mSQL = SQL.ToString();

                string Filter = "And B.DocNo = '" + mDocNo + "' And B.DNo = '" + mDNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By E.OPDocNo",
                        new string[]
                        {
                            //0
                            "OPDocNo",
                            //1-5
                            "UserOP"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        #endregion

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmOutgoingPayment(Sm.GetGrdStr(Grd1, e.RowIndex, 0));
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmOutgoingPayment(Sm.GetGrdStr(Grd1, e.RowIndex, 0));
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #endregion

     
    }
}
