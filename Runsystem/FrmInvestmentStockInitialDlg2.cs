﻿#region Update
/*
    12/04/2022 [RDA/PRODUCT] new dialog
    27/04/2022 [RDA/PRODUCT] tambah param BankAccountTypeForInvestment
    25/05/2022 [IBL/PRODUCT] tambah type untuk bisa membedakan copy data ke tab Equity dan tab Debt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentStockInitialDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentStockInitial mFrmParent;
        private string mSQL = string.Empty;
        private int mType = 0;

        #endregion

        #region Constructor

        public FrmInvestmentStockInitialDlg2(FrmInvestmentStockInitial FrmParent, int Type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mType = Type;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            //Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Bank Account Code",
                        "Investment Bank"+Environment.NewLine+"Account Name",
                        "Investment Bank"+Environment.NewLine+"Account (RDN)",
                        "Account Description",
                        "Currency"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 150, 150, 150, 100
                    }
                );
            //Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.BankAcCode, B.BankName, A.BankAcNo, A.ActInd, A.BankAcTp, A.BankAcNm, ");
                SQL.AppendLine("C.AcNo, C.AcDesc, A.CurCode, A.BranchAcNm, A.BranchAcSwiftCode, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode = B.BankCode ");
                SQL.AppendLine("Left Join TblCOA C On A.COAAcNo = C.AcNo ");
                SQL.AppendLine("Where A.ActInd = 'Y' and FIND_IN_SET (A.BankAcTp, @BankAcTp) ");

                mSQL = SQL.ToString();

                var cm = new MySqlCommand();
                string Filter = "And 1=1 ";

                Sm.CmParam<String>(ref cm, "@BankAcTp", mFrmParent.mBankAccountTypeForInvestment);
                Sm.FilterStr(ref Filter, ref cm, TxtBankCode.Text, new string[] { "A.BankAcCode", "A.BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.BankAcCode;",

                        new string[]
                        { 
                            //0
                            "BankAcCode", 

                            //1-5
                            "BankAcNm", "BankAcNo", "AcDesc", "CurCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                IsChoose = true;
                
                if (mType == 1)
                {
                    Sm.CopyGrdValue(mFrmParent.Grd3, mFrmParent.Grd3.CurRow.Index, 23, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd3, mFrmParent.Grd3.CurRow.Index, 24, Grd1, Grd1.CurRow.Index, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd3, mFrmParent.Grd3.CurRow.Index, 33, Grd1, Grd1.CurRow.Index, 3);
                }
                else
                {
                    Sm.CopyGrdValue(mFrmParent.Grd4, mFrmParent.Grd4.CurRow.Index, 7, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd4, mFrmParent.Grd4.CurRow.Index, 8, Grd1, Grd1.CurRow.Index, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd4, mFrmParent.Grd4.CurRow.Index, 9, Grd1, Grd1.CurRow.Index, 2);
                }

                FrmInvestmentStockInitialDlg2.ActiveForm.Close();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Bank Account Name.");
            
        }

        #endregion


        #region Grid Method

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtBankCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBankCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion


    }
}
