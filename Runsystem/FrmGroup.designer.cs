﻿namespace RunSystem
{
    partial class FrmGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGroup));
            this.TxtGrpName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtGrpCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnSourceGroup = new DevExpress.XtraEditors.SimpleButton();
            this.TcGroup = new DevExpress.XtraTab.XtraTabControl();
            this.TpDept = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TpWhs = new DevExpress.XtraTab.XtraTabPage();
            this.TpCC = new DevExpress.XtraTab.XtraTabPage();
            this.TpSite = new DevExpress.XtraTab.XtraTabPage();
            this.TpBankAc = new DevExpress.XtraTab.XtraTabPage();
            this.TpItCt = new DevExpress.XtraTab.XtraTabPage();
            this.TpJobTransfer = new DevExpress.XtraTab.XtraTabPage();
            this.TpPaymentTerm = new DevExpress.XtraTab.XtraTabPage();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.TpLevel = new DevExpress.XtraTab.XtraTabPage();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.TpEntity = new DevExpress.XtraTab.XtraTabPage();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.TpCtCt = new DevExpress.XtraTab.XtraTabPage();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.TpProfitCenter = new DevExpress.XtraTab.XtraTabPage();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.TpType = new DevExpress.XtraTab.XtraTabPage();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.TpVendor = new DevExpress.XtraTab.XtraTabPage();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcGroup)).BeginInit();
            this.TcGroup.SuspendLayout();
            this.TpDept.SuspendLayout();
            this.panel4.SuspendLayout();
            this.TpWhs.SuspendLayout();
            this.TpCC.SuspendLayout();
            this.TpSite.SuspendLayout();
            this.TpBankAc.SuspendLayout();
            this.TpItCt.SuspendLayout();
            this.TpJobTransfer.SuspendLayout();
            this.TpPaymentTerm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.TpLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.TpEntity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.TpCOA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.TpCtCt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.TpProfitCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.TpType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.TpVendor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 490);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnSourceGroup);
            this.panel2.Controls.Add(this.TxtGrpName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtGrpCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 33);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TcGroup);
            this.panel3.Location = new System.Drawing.Point(0, 33);
            this.panel3.Size = new System.Drawing.Size(772, 457);
            this.panel3.Controls.SetChildIndex(this.TcGroup, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 468);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 218);
            this.Grd1.TabIndex = 15;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtGrpName
            // 
            this.TxtGrpName.EnterMoveNextControl = true;
            this.TxtGrpName.Location = new System.Drawing.Point(250, 6);
            this.TxtGrpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName.Name = "TxtGrpName";
            this.TxtGrpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGrpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName.Properties.MaxLength = 80;
            this.TxtGrpName.Size = new System.Drawing.Size(481, 20);
            this.TxtGrpName.TabIndex = 13;
            this.TxtGrpName.Validated += new System.EventHandler(this.TxtGrpName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(207, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpCode
            // 
            this.TxtGrpCode.EnterMoveNextControl = true;
            this.TxtGrpCode.Location = new System.Drawing.Point(46, 6);
            this.TxtGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpCode.Name = "TxtGrpCode";
            this.TxtGrpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpCode.Properties.MaxLength = 16;
            this.TxtGrpCode.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpCode.TabIndex = 11;
            this.TxtGrpCode.Validated += new System.EventHandler(this.TxtGrpCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 189);
            this.Grd2.TabIndex = 17;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 189);
            this.Grd3.TabIndex = 17;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 189);
            this.Grd4.TabIndex = 17;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 189);
            this.Grd5.TabIndex = 17;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 189);
            this.Grd6.TabIndex = 17;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 189);
            this.Grd7.TabIndex = 17;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(766, 189);
            this.Grd8.TabIndex = 17;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // BtnSourceGroup
            // 
            this.BtnSourceGroup.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSourceGroup.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSourceGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSourceGroup.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSourceGroup.Appearance.Options.UseBackColor = true;
            this.BtnSourceGroup.Appearance.Options.UseFont = true;
            this.BtnSourceGroup.Appearance.Options.UseForeColor = true;
            this.BtnSourceGroup.Appearance.Options.UseTextOptions = true;
            this.BtnSourceGroup.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSourceGroup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSourceGroup.Image = ((System.Drawing.Image)(resources.GetObject("BtnSourceGroup.Image")));
            this.BtnSourceGroup.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSourceGroup.Location = new System.Drawing.Point(735, 5);
            this.BtnSourceGroup.Name = "BtnSourceGroup";
            this.BtnSourceGroup.Size = new System.Drawing.Size(24, 21);
            this.BtnSourceGroup.TabIndex = 14;
            this.BtnSourceGroup.ToolTip = "Find Group\'s Source";
            this.BtnSourceGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSourceGroup.ToolTipTitle = "Run System";
            this.BtnSourceGroup.Click += new System.EventHandler(this.BtnSourceGroup_Click);
            // 
            // TcGroup
            // 
            this.TcGroup.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TcGroup.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcGroup.Location = new System.Drawing.Point(0, 218);
            this.TcGroup.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.TcGroup.Name = "TcGroup";
            this.TcGroup.SelectedTabPage = this.TpDept;
            this.TcGroup.Size = new System.Drawing.Size(772, 239);
            this.TcGroup.TabIndex = 16;
            this.TcGroup.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpDept,
            this.TpWhs,
            this.TpCC,
            this.TpSite,
            this.TpBankAc,
            this.TpItCt,
            this.TpJobTransfer,
            this.TpPaymentTerm,
            this.TpLevel,
            this.TpEntity,
            this.TpCOA,
            this.TpCtCt,
            this.TpProfitCenter,
            this.TpType,
            this.TpVendor});
            // 
            // TpDept
            // 
            this.TpDept.Appearance.Header.Options.UseTextOptions = true;
            this.TpDept.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpDept.Controls.Add(this.panel4);
            this.TpDept.Name = "TpDept";
            this.TpDept.Size = new System.Drawing.Size(766, 189);
            this.TpDept.Text = "Department";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Grd2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 189);
            this.panel4.TabIndex = 22;
            // 
            // TpWhs
            // 
            this.TpWhs.Appearance.Header.Options.UseTextOptions = true;
            this.TpWhs.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpWhs.Controls.Add(this.Grd3);
            this.TpWhs.Name = "TpWhs";
            this.TpWhs.Size = new System.Drawing.Size(766, 189);
            this.TpWhs.Text = "Warehouse";
            // 
            // TpCC
            // 
            this.TpCC.Appearance.Header.Options.UseTextOptions = true;
            this.TpCC.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCC.Controls.Add(this.Grd4);
            this.TpCC.Name = "TpCC";
            this.TpCC.Size = new System.Drawing.Size(766, 189);
            this.TpCC.Text = "Cost Center";
            // 
            // TpSite
            // 
            this.TpSite.Appearance.Header.Options.UseTextOptions = true;
            this.TpSite.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpSite.Controls.Add(this.Grd5);
            this.TpSite.Name = "TpSite";
            this.TpSite.Size = new System.Drawing.Size(766, 189);
            this.TpSite.Text = "Site";
            // 
            // TpBankAc
            // 
            this.TpBankAc.Appearance.Header.Options.UseTextOptions = true;
            this.TpBankAc.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBankAc.Controls.Add(this.Grd6);
            this.TpBankAc.Name = "TpBankAc";
            this.TpBankAc.Size = new System.Drawing.Size(766, 189);
            this.TpBankAc.Text = "Bank Account";
            // 
            // TpItCt
            // 
            this.TpItCt.Appearance.Header.Options.UseTextOptions = true;
            this.TpItCt.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItCt.Controls.Add(this.Grd7);
            this.TpItCt.Name = "TpItCt";
            this.TpItCt.Size = new System.Drawing.Size(766, 189);
            this.TpItCt.Text = "Item\'s Category";
            // 
            // TpJobTransfer
            // 
            this.TpJobTransfer.Appearance.Header.Options.UseTextOptions = true;
            this.TpJobTransfer.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpJobTransfer.Controls.Add(this.Grd8);
            this.TpJobTransfer.Name = "TpJobTransfer";
            this.TpJobTransfer.Size = new System.Drawing.Size(766, 189);
            this.TpJobTransfer.Text = "Job Transfer";
            // 
            // TpPaymentTerm
            // 
            this.TpPaymentTerm.Appearance.Header.Options.UseTextOptions = true;
            this.TpPaymentTerm.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpPaymentTerm.Controls.Add(this.Grd9);
            this.TpPaymentTerm.Name = "TpPaymentTerm";
            this.TpPaymentTerm.Size = new System.Drawing.Size(766, 189);
            this.TpPaymentTerm.Text = "Term of Payment";
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 0);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(766, 189);
            this.Grd9.TabIndex = 18;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd9_EllipsisButtonClick);
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9_KeyDown);
            // 
            // TpLevel
            // 
            this.TpLevel.Appearance.Header.Options.UseTextOptions = true;
            this.TpLevel.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpLevel.Controls.Add(this.Grd10);
            this.TpLevel.Name = "TpLevel";
            this.TpLevel.Size = new System.Drawing.Size(766, 189);
            this.TpLevel.Text = "Level";
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 0);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(766, 189);
            this.Grd10.TabIndex = 19;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd10_EllipsisButtonClick);
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10_KeyDown);
            // 
            // TpEntity
            // 
            this.TpEntity.Appearance.Header.Options.UseTextOptions = true;
            this.TpEntity.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpEntity.Controls.Add(this.Grd11);
            this.TpEntity.Name = "TpEntity";
            this.TpEntity.Size = new System.Drawing.Size(766, 189);
            this.TpEntity.Text = "Entity";
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(0, 0);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(766, 189);
            this.Grd11.TabIndex = 20;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd11_EllipsisButtonClick);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Controls.Add(this.Grd12);
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 189);
            this.TpCOA.Text = "COA";
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(0, 0);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(766, 189);
            this.Grd12.TabIndex = 18;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd12_EllipsisButtonClick);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // TpCtCt
            // 
            this.TpCtCt.Appearance.Header.Options.UseTextOptions = true;
            this.TpCtCt.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCtCt.Controls.Add(this.Grd13);
            this.TpCtCt.Name = "TpCtCt";
            this.TpCtCt.Size = new System.Drawing.Size(766, 189);
            this.TpCtCt.Text = "Customer\'s Category";
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 0);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(766, 189);
            this.Grd13.TabIndex = 19;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd13_EllipsisButtonClick);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            // 
            // TpProfitCenter
            // 
            this.TpProfitCenter.Appearance.Header.Options.UseTextOptions = true;
            this.TpProfitCenter.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpProfitCenter.Controls.Add(this.Grd14);
            this.TpProfitCenter.Name = "TpProfitCenter";
            this.TpProfitCenter.Size = new System.Drawing.Size(766, 189);
            this.TpProfitCenter.Text = "Profit Center";
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 0);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(766, 189);
            this.Grd14.TabIndex = 20;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd14_EllipsisButtonClick);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // TpType
            // 
            this.TpType.Controls.Add(this.Grd15);
            this.TpType.Name = "TpType";
            this.TpType.Size = new System.Drawing.Size(766, 189);
            this.TpType.Text = "Type";
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 0);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(766, 189);
            this.Grd15.TabIndex = 18;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd15_EllipsisButtonClick);
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            // 
            // TpVendor
            // 
            this.TpVendor.Controls.Add(this.Grd16);
            this.TpVendor.Name = "TpVendor";
            this.TpVendor.Size = new System.Drawing.Size(766, 189);
            this.TpVendor.Text = "Vendor\'s Category";
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 0);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(766, 189);
            this.Grd16.TabIndex = 19;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd16_EllipsisButtonClick);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // FrmGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 490);
            this.Name = "FrmGroup";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcGroup)).EndInit();
            this.TcGroup.ResumeLayout(false);
            this.TpDept.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.TpWhs.ResumeLayout(false);
            this.TpCC.ResumeLayout(false);
            this.TpSite.ResumeLayout(false);
            this.TpBankAc.ResumeLayout(false);
            this.TpItCt.ResumeLayout(false);
            this.TpJobTransfer.ResumeLayout(false);
            this.TpPaymentTerm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.TpLevel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.TpEntity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.TpCOA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.TpCtCt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.TpProfitCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.TpType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.TpVendor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtGrpName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtGrpCode;
        private System.Windows.Forms.Label label1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        public DevExpress.XtraEditors.SimpleButton BtnSourceGroup;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private DevExpress.XtraTab.XtraTabControl TcGroup;
        private DevExpress.XtraTab.XtraTabPage TpDept;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraTab.XtraTabPage TpWhs;
        private DevExpress.XtraTab.XtraTabPage TpCC;
        private DevExpress.XtraTab.XtraTabPage TpSite;
        private DevExpress.XtraTab.XtraTabPage TpBankAc;
        private DevExpress.XtraTab.XtraTabPage TpItCt;
        private DevExpress.XtraTab.XtraTabPage TpJobTransfer;
        private DevExpress.XtraTab.XtraTabPage TpPaymentTerm;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        private DevExpress.XtraTab.XtraTabPage TpLevel;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        private DevExpress.XtraTab.XtraTabPage TpEntity;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        private DevExpress.XtraTab.XtraTabPage TpCtCt;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        private DevExpress.XtraTab.XtraTabPage TpProfitCenter;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        private DevExpress.XtraTab.XtraTabPage TpType;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        private DevExpress.XtraTab.XtraTabPage TpVendor;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
    }
}