﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDinasKehutanan : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDinasKehutanan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Y1.DlCode, Y1.Noseri, Y1.DocDt As DateVcr, Y1.jenis, Y1.Qty As Jumlah,  Y1.kubik As Volume, ");
            SQL.AppendLine("Concat(ifnull(Concat(Y1.vendor, ', '), ''), ifnull(concat(Y1.City, ', '), ''), ifnull(Concat(Y1.SubDistrict, ', '),''),  ifnull(Y1.Village, '')) As Vendor, Y1.transport As Angkutan ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select L.DlCode, L.Noseri, t1.DocDt,  ");
            SQL.AppendLine("Case T1.DocType when '1' then E.CityName when '2' then I.CityName else null End As City, ");
            SQL.AppendLine("Case T1.DocType when '1' then F.SDName when '2' then J.SDName else null End As SubDistrict, ");
            SQL.AppendLine("Case T1.DocType when '1' then G.VilName when '2' then K.VilName else null End As Village, ");
            SQL.AppendLine("Case T1.DocType when '1' then D.VdName when '2' then H.VdName else null End As Vendor, ");
            SQL.AppendLine("case t1.Doctype when '1' then 'Sengon' when '2' then 'Kayu gergajian' end As jenis, t1.qty1 as qty, t1.Qty2 As kubik, ");
            SQL.AppendLine("L.Transport ");
            SQL.AppendLine("From ");
            SQL.AppendLine("(");
            SQL.AppendLine("select D.QueueNo, A.DocType, G.Qty1, Sum(B.Qty) As Qty2, F.DocDt");
            SQL.AppendLine("From tblPurchaseInvoiceRawMaterialHdr A");
            SQL.AppendLine("Inner Join tblPurchaseInvoiceRawMaterialDtl B On A.DocNo = B.DocNo");
            SQL.AppendLine("Inner Join tblRawMaterialVerify C On A.RawMaterialVerifyDocNo= C.DocNo And C.CancelInd = 'N'");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegaldocVerifyDocNo = D.DocNo And D.CancelInd = 'N'");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr E On A.VoucherRequestDocNo = E.DocNo And E.CancelInd = 'N'");
            SQL.AppendLine("Inner Join TblVoucherhdr F On E.VoucherDocNo = F.DocNo And F.CancelInd = 'N' And F.DocDt Between @DocDt1 And @DocDt2");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("   Select G1.LegaldocVerifyDocNo, G1.DocType, Sum(G2.Qty) Qty1");
            SQL.AppendLine("   From TblRecvRawMaterialHdr G1");
            SQL.AppendLine("    Inner Join TblRecvrawMaterialDtl2 G2 On G1.DocNo = G2.DocNo");
            SQL.AppendLine("    Where G1.ProcessInd='F'");
            SQL.AppendLine("     Group By G1.LegaldocVerifyDocNo, G1.DocType");
            SQL.AppendLine("   ) G On D.DocNo=G.LegaldocVerifyDocNo And A.DocType=G.DocType");
            SQL.AppendLine("   Group By A.DocNo, A.DocType, g.Qty1");
            SQL.AppendLine(") t1");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr C On t1.QueueNo = C.QueueNo And C.cancelInd = 'N'");
            SQL.AppendLine("Left Join TblVendor D On C.VdCode1 = D.VdCode And t1.doctype='1'");
            SQL.AppendLine("Left Join TblCity E On D.CityCode = E.CityCode");
            SQL.AppendLine("Left Join TblSubDistrict F On D.SdCode = F.SdCOde");
            SQL.AppendLine("Left Join TblVillage G On D.VilCode = G.VilCOde");

            SQL.AppendLine("Left Join TblVendor H On C.VdCode2 = H.VdCode And t1.doctype='2'");
            SQL.AppendLine("Left Join TblCity I On H.CityCode = I.CityCode");
            SQL.AppendLine("Left Join TblSubDistrict J On H.SdCode = J.SdCOde");
            SQL.AppendLine("Left Join TblVillage K On H.VilCode = K.VilCOde");

            SQL.AppendLine("Join ");
            SQL.AppendLine("  (");
            SQL.AppendLine("      Select Distinct A.DocNo, A.DlType, group_concat(case A.DlCode when 'NA' then 'Nota Angkutan' ");
            SQL.AppendLine("      When 'FAKO' then 'FAKO' When 'SKAU' then 'SKAU' Else null End Separator '/ ') As DlCode,");
            SQL.AppendLine("      group_concat(case A.DlCode when 'NA' then B.QueueNo ");
            SQL.AppendLine("      When 'FAKO' then A.DlDocNo When 'SKAU' then A.DlDOcNo Else null End Separator '/ ') As NoSeri,");
            SQL.AppendLine("     Concat(D.TTName,' ', B.VehicleRegNo) As Transport");
            SQL.AppendLine("       From TblLegalDocVerifyDtl A");
            SQL.AppendLine("       Inner Join TblLegalDocverifyHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N'");
            SQL.AppendLine("       Inner Join TblLoadingQueue C On B.QueueNo = C.DOcNo");
            SQL.AppendLine("       Inner Join TblTransporttype D On C.TtCode = D.TtCOde");
            SQL.AppendLine("       Where A.DLCode = 'NA' Or A.DlCode = 'FAKO' Or A.DlCode = 'SKAU'");
            SQL.AppendLine("      group by A.DocNo, A.DlType");
            SQL.AppendLine("      )L On C.DOcNo = L.DocNo And T1.DocType = L.DlType	 ");
            SQL.AppendLine(")Y1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Jenis"+Environment.NewLine+"Document",
                        "Nomor"+Environment.NewLine+"Seri",
                        "Tanggal",
                        "Jenis"+Environment.NewLine+"Kayu",
                        "Jumlah",
                        
                        //6-8
                        "Volume",
                        "Pengirim",
                        "Jenis Alat"+Environment.NewLine+"Angkut",
                       
                    },
                    new int[] 
                    {
                        50,
                        150, 200, 100, 100, 150,
                        150, 300, 100,    
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                SetSQL();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        { 
                            //0
                            "DlCode", 

                            //1-5
                            "NoSeri", "DateVcr",  "Jenis", "Jumlah", "Volume", 
                            
                            //6-10
                            "Vendor", "Angkutan", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
