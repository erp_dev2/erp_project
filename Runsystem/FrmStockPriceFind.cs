﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmStockPriceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmStockPrice mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmStockPriceFind(FrmStockPrice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode,B.ItName,A.BatchNo,A.Source,C.CurName,A.UPrice, ");
            SQL.AppendLine("A.ExcRate,A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblStockPrice A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblCurrency C On A.CurCode=C.CurCode ");


            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item"+Environment.NewLine+"Code", 
                        "Item"+Environment.NewLine+"Name",
                        "Batch"+Environment.NewLine+"Number",
                        "Source",
                        "Currency",
                        //6-10
                        "Unit"+Environment.NewLine+"Price", 
                        "Exchage"+Environment.NewLine+"Rate",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date",
                        //11-14
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,

                        mSQL + Filter + " Order By B.ItName ",
                        new string[]
                        {   
                            //0
                            "ItCode",
                            //1-5
                            "ItName","BatchNo","Source","CurName","UPrice",
                            //6-10
                            "ExcRate","Remark","CreateBy","CreateDt","LastUpBy",
                            //11
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 11);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }
        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch Number");
        }

        #endregion

        #endregion
        
    }
}
