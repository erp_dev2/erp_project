﻿#region Update
    // 22/07/2017 [TKG] ditambah validasi untuk working schedule tanpa istirahat
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIncentiveDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmIncentive mFrmParent;
        private string 
            mDocDt = string.Empty, mDeptCode = string.Empty, mAGCode = string.Empty;

        private string
          mSystemType = string.Empty,
          mPayrunPeriod = string.Empty,
          mPGCode = string.Empty,
          mSiteCode = string.Empty,

          mCoordinatorLeaveCode = "007",
          mDisasterLeaveCode = "014",
          mGoHomeEarlyLeaveCode = "017",
          mMaternityLeaveCode = "015",
          mMiscarriageLeaveCode = "016",
          mOperatorLeaveCode = "001",
          mEmpSystemTypeHarian = "1",
          mEmpSystemTypeBorongan = "2",
          mEmploymentStatusTetap = "1",
          mEmploymentStatusLepas = "2",
          mEmploymentStatusSpesial = "3",
          mPayrunPeriodBulanan = "1",
          mPayrunPeriod2Mingguan = "2",
          mADCodeIncPerformance = "03",
          mADCodeIncPerformance2 = "19",
          mADCodeTransport = "03",
          mADCodeMeal = "05",
          mSSPCodeForHealth = "BPJS-KES",
          mSSPCodeForEmployment = "BPJS-TNK",
          mSalaryInd = "1"
          ;

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mPresenceReward2Mingguan = 50000m,
            mPresenceRewardBulanan = 300000m,
            mGoHomeEarlyLeaveHr1 = 4.5m,
            mGoHomeEarlyLeaveHr2 = 5m,
            mGoHomeEarlyLeavePerc1 = 50m,
            mGoHomeEarlyLeavePerc2 = 75m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m
            ;

        private bool
            mIsUseLateValidation = false,
            mIsOTRoutineRounding = false,
            mIsWorkingHrBasedOnSchedule = false
            ;

        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;

        #endregion

        #region Constructor

        public FrmIncentiveDlg(FrmIncentive FrmParent, string DocDt, string DeptCode, string AGCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt.Substring(0, 8);
            mDeptCode = DeptCode;
            mAGCode = AGCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            mlAttendanceLog = new List<AttendanceLog>();
            mlOT = new List<OT>();
            mlOTAdjustment = new List<OTAdjustment>();
            ChkOnlyDailyEmployee.Checked = true;
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Employee's"+Environment.NewLine+"Code",
                        "Old Code",
                        "Employee's Name", 
                        "Position",

                        //6-7
                        "Daily Salary",
                        "System Type"
                    },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 100, 250, 150, 
                    
                    //6-7
                    120, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 7 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, B.PosName, A.SystemType ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.AGCode, T2.EmpCode ");
            SQL.AppendLine("    From TblAttendanceGrpHdr T1 ");
            SQL.AppendLine("    Inner Join TblAttendanceGrpDtl T2 On T1.AGCode=T2.AGCode ");
            SQL.AppendLine("    Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And T3.DeptCode=@DeptCode ");
            SQL.AppendLine("    Where T1.ActInd='Y' ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Where A.ResignDt Is Null ");
            SQL.AppendLine("And IfNull(D.AGCode, '')=@AGCode ");
            SQL.AppendLine("And A.SystemType<>'3' ");
            if (ChkOnlyDailyEmployee.Checked)
                SQL.AppendLine("And A.SystemType='1' ");
            SQL.AppendLine("And IfNull(A.DeptCode, '')=@DeptCode ");
            SQL.AppendLine("And (A.SystemType<>'2' ");
            SQL.AppendLine("Or (A.SystemType='2' ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select T1.DocNo ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.DocDt=@DocDt And T2.EmpCode=A.EmpCode ");
            SQL.AppendLine("))) ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Position(Concat('##', A.EmpCode, '##') In @SelectedEmployee)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedEmployee", mFrmParent.GetSelectedEmployee());
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@AGCode", mAGCode);
                Sm.CmParamDt(ref cm, "@DocDt", mDocDt);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By A.EmpName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode",
 
                            //1-4
                            "EmpCodeOld", "EmpName", "PosName", "SystemType"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Grd.Cells[Row, 6].Value = 0m;
                        }, true, false, false, false
                    );
                if (Grd1.Rows.Count > 0) ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsEmpCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        mFrmParent.Grd1.Cells[Row1, 7].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 8].Value = string.Empty;
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(mFrmParent.Grd1, Index, 2))) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Compute Daily Salary

        private void GetParameter()
        {
            mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            mWorkingHr = Sm.GetParameterDec("WorkingHr");
            mNoWorkDay2Mingguan = Sm.GetParameterDec("NoWorkDay2Mingguan");
            mNoWorkDayBulanan = Sm.GetParameterDec("NoWorkDayBulanan");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mPresenceReward2Mingguan = Sm.GetParameterDec("PresenceReward2Mingguan");
            mPresenceRewardBulanan = Sm.GetParameterDec("PresenceRewardBulanan");
            mHOWorkingDayPerMth = Sm.GetParameterDec("HOWorkingDayPerMth");
            mSiteWorkingDayPerMth = Sm.GetParameterDec("SiteWorkingDayPerMth");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
            mDisasterLeaveCode = Sm.GetParameter("DisasterLeaveCode");
            mGoHomeEarlyLeaveCode = Sm.GetParameter("GoHomeEarlyLeaveCode");
            mMaternityLeaveCode = Sm.GetParameter("MaternityLeaveCode");
            mMiscarriageLeaveCode = Sm.GetParameter("MiscarriageLeaveCode");
            mOperatorLeaveCode = Sm.GetParameter("OperatorLeaveCode");
            mEmpSystemTypeHarian = Sm.GetParameter("EmpSystemTypeHarian");
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            mPayrunPeriod2Mingguan = Sm.GetParameter("PayrunPeriod2Mingguan");
            mADCodeIncPerformance = Sm.GetParameter("ADCodeIncPerformance");
            mADCodeIncPerformance2 = Sm.GetParameter("ADCodeIncPerformance2");
            mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            mIsWorkingHrBasedOnSchedule = Sm.GetParameter("IsWorkingHrBasedOnSchedule") == "Y";
            mIsUseLateValidation = Sm.GetParameter("IsUseLateValidation") == "Y";
            mIsOTRoutineRounding = Sm.GetParameter("IsOTRoutineRounding") == "Y";
        }

        private void ProcessData()
        {
            var lEmployeePPS = new List<EmployeePPS>();
            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r =>{ 
                    Process4(ref r); 
                });
                lResult1.ForEach(r =>
                {
                    Process5(r.EmpCode, r.Salary);
                });
            }
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (Grd1.Rows.Count >= 1 && lEmployeePPS.Count > 0)
            {
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCodeTemp.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        DeptCodeTemp = string.Empty;
                        GrdLvlCodeTemp = string.Empty;
                        EmploymentStatusTemp = string.Empty;
                        SystemTypeTemp = string.Empty;
                        PayrunPeriodTemp = string.Empty;
                        PGCodeTemp = string.Empty;
                        SiteCodeTemp = string.Empty;
                        PosCodeTemp = string.Empty;

                        foreach (var i in
                            lEmployeePPS
                                .Where(Index => Index.EmpCode == EmpCodeTemp)
                                .OrderByDescending(x => x.StartDt))
                        {
                            if (Sm.CompareDtTm(mDocDt, i.StartDt) >= 0)
                            {
                                DeptCodeTemp = i.DeptCode;
                                GrdLvlCodeTemp = i.GrdLvlCode;
                                EmploymentStatusTemp = i.EmploymentStatus;
                                SystemTypeTemp = i.SystemType;
                                PayrunPeriodTemp = i.PayrunPeriod;
                                PGCodeTemp = i.PGCode;
                                SiteCodeTemp = i.SiteCode;
                                PosCodeTemp = i.PosCode;
                                break;
                            };
                        }

                        lResult1.Add(new Result1()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = mDocDt,
                            DeptCode = DeptCodeTemp,
                            GrdLvlCode = GrdLvlCodeTemp,
                            EmploymentStatus = EmploymentStatusTemp,
                            SystemType = SystemTypeTemp,
                            PayrunPeriod = PayrunPeriodTemp,
                            PGCode = PGCodeTemp,
                            SiteCode = SiteCodeTemp,
                            PosCode = PosCodeTemp,
                            ProcessInd = true,
                            LatestPaidDt = string.Empty,
                            JoinDt = string.Empty,
                            ResignDt = string.Empty,
                            WorkingDay = 0m,
                            WSCode = string.Empty,
                            HolInd = false,
                            HolidayIndex = mStdHolidayIndex,
                            WSHolidayInd = false,
                            WSIn1 = string.Empty,
                            WSOut1 = string.Empty,
                            WSIn2 = string.Empty,
                            WSOut2 = string.Empty,
                            WSIn3 = string.Empty,
                            WSOut3 = string.Empty,
                            OneDayInd = true,
                            LateInd = false,
                            ActualIn = string.Empty,
                            ActualOut = string.Empty,
                            WorkingIn = string.Empty,
                            WorkingOut = string.Empty,
                            WorkingDuration = 0m,
                            BasicSalary = 0m,
                            BasicSalary2 = 0m,
                            ProductionWages = 0m,
                            Salary = 0m,
                            LeaveCode = string.Empty,
                            LeaveType = string.Empty,
                            PaidLeaveInd = false,
                            CompulsoryLeaveInd = false,
                            _DeductTHPInd = false,
                            LeaveStartTm = string.Empty,
                            LeaveEndTm = string.Empty,
                            LeaveDuration = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            ProcessPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            OTToLeaveInd = false,
                            IncMinWages = 0m,
                            IncProduction = 0m,
                            IncPerformance = 0m,
                            PresenceRewardInd = false,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            DedProduction = 0m,
                            DedProdLeave = 0m,
                            EmpSalary = 0m,
                            EmpSalary2 = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            ProcessUPLAmt = 0m,
                            Transport = 0m,
                            Meal = 0m,
                            _ExtraFooding = 0m,
                            _IncPerformance = 0m,
                            _IsFullDayInd = false
                        });
                    }
                }
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                foreach (var ews in lEmpWorkSchedule)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd == "Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                        break;
                    }
                }
                lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd == "Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        break;
                    }
                }
                lLeave.Clear();
            }

            #endregion

            #region Holiday

            var lHoliday = new List<Holiday>();
            ProcessHoliday(ref lHoliday);
            if (lHoliday.Count > 0)
            {
                foreach (var h in lHoliday)
                {
                    foreach (var r in lResult1.Where(Index => Index.Dt == h.HolDt))
                        r.HolInd = true;
                }
                lHoliday.Clear();
            }

            #endregion

            #region Grade Level

            var lGradeLevel = new List<GradeLevel>();
            ProcessGradeLevel(ref lGradeLevel);
            if (lGradeLevel.Count > 0)
            {
                foreach (var gl in lGradeLevel)
                {
                    foreach (var r in lResult1.Where(Index => Index.GrdLvlCode == gl.GrdLvlCode))
                    {
                        r.BasicSalary = gl.BasicSalary;
                        r.BasicSalary2 = gl.BasicSalary2;
                    }
                }
                lGradeLevel.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0)
                            r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0)
                                r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.LatestPaidDt) <= 0)
                                r.ProcessInd = false;
                        }
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            //var PayrunPeriodTemp = string.Empty;
            //var WorkingDayTemp = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                //string SystemTypeTemp = string.Empty, GrdLvlCodeTemp = string.Empty;
                //decimal SalaryTemp = 0m, BasicSalaryTemp = 0m, BasicSalary2Temp = 0m;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCodeTemp.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        //SystemTypeTemp = string.Empty;
                        //PayrunPeriodTemp = string.Empty;
                        //SalaryTemp = 0m;
                        //GrdLvlCodeTemp = string.Empty;
                        //BasicSalaryTemp = 0m;
                        //BasicSalary2Temp = 0m;

                        foreach (var x in lResult1.Where(x => x.EmpCode == EmpCodeTemp).OrderBy(x => x.Dt))
                        {
                            x.Salary = x.BasicSalary2;
                            if (x.ProcessInd)
                            {
                                if (!(
                                    Sm.CompareStr(x.DeptCode, mDeptCode) //&&
                                    //Sm.CompareStr(x.SystemType, mSystemType) &&
                                    //Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                    //Sm.CompareStr(x.PGCode, mPGCode) &&
                                    //Sm.CompareStr(x.SiteCode, mSiteCode)
                                    ))
                                {
                                    x.ProcessInd = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Process4(ref Result1 r)
        {
            string
                ActualDtIn = string.Empty,
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty,
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode,
                Dt = r.Dt;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 &&
                r.WSOut1.Length > 0 &&
                Sm.CompareDtTm(r.WSIn1, r.WSOut1) > 0)
                //int.Parse(r.WSIn1) > int.Parse(r.WSOut1))
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                //if (decimal.Parse(r.Dt + r.WSIn1) < decimal.Parse(ActualDtIn + ActualTmIn))
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                {
                    r.LateInd = true;
                    r.PresenceRewardInd = false;
                }
            }

            if (!mIsUseLateValidation && r.LateInd) r.LateInd = false;

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            //if (decimal.Parse(r.Dt + r.WSOut1) > decimal.Parse(ActualDtOut + ActualTmOut))
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            //if (decimal.Parse(Dt2 + r.WSOut1) > decimal.Parse(ActualDtOut + ActualTmOut))
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            string ROT1 = string.Empty, ROT2 = string.Empty;

            #region Set OT Rutin (In)

            if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        ROT1 = r.Dt + r.WSIn3;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                        {
                            //1500 < 1600
                            ROT1 = ActualDtIn + ActualTmIn;
                        }
                        else
                        {
                            //1500 > 1400
                            ROT1 = r.Dt + r.WSIn3;
                        }
                    }
                }
                else
                {
                    //Shift 3
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        ROT1 = Dt2 + r.WSIn3;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                        //if (decimal.Parse(Dt2 + r.WSIn3) < decimal.Parse(ActualDtIn + ActualTmIn))
                        {
                            //0400<0500
                            ROT1 = ActualDtIn + ActualTmIn;
                        }
                        else
                        {
                            //0400>0300
                            ROT1 = Dt2 + r.WSIn3;
                        }
                    }
                }
            }

            #endregion

            #region Set OT Rutin (Out)

            if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        ROT2 = r.Dt + r.WSOut3;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                        {
                            //1900<2000
                            ROT2 = r.Dt + r.WSOut3;
                        }
                        else
                        {
                            //1900>1800
                            ROT2 = ActualDtOut + ActualTmOut;
                        }
                    }
                }
                else
                {
                    //Shift 3
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        ROT2 = Dt2 + r.WSOut3;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                        {
                            //0300<0400
                            ROT2 = Dt2 + r.WSOut3;
                        }
                        else
                        {
                            //0300>0200
                            ROT2 = ActualDtOut + ActualTmOut;
                        }
                    }
                }
            }

            #endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            if (ROT1.Length > 0 && ROT2.Length > 0)
            {
                ROTDuration = ComputeDuration(ROT2, ROT1);
                if (ROTDuration < 0) ROTDuration = 0m;
                if (mIsOTRoutineRounding && ROTDuration > 0)
                {
                    ROTDuration = RoundTo1Hour((double)ROTDuration);
                }
            }

            #endregion

            #region set working duration

            decimal
                WorkingDuration = 0m,
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    //Full Day Leave
                    WorkingDuration = 0m;
                    ROTDuration = 0m;
                }
                else
                {
                    if (r.LeaveType.Length == 0)
                    {
                        //No Leave
                        WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                        {
                            // Menghitung lama waktu istirahat
                            if (r.OneDayInd)
                            {
                                // Shift 1, 2
                                if (Sm.CompareDtTm(r.WorkingOut, r.Dt + r.WSIn2) > 0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            }
                            else
                            {
                                //Shift 3
                                if (Sm.CompareDtTm(r.WSIn2, r.WSOut2) <= 0)
                                    //if (decimal.Parse(r.WSIn2) <= decimal.Parse(r.WSOut2))
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                                else
                                    BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                            }
                        }
                        WorkingDuration -= BreakDuration;
                    }
                    else
                    {
                        //Hourly Leave
                        string
                            LeaveStartDtTm = string.Empty,
                            LeaveEndDtTm = string.Empty,
                            BreakStartDtTm = string.Empty,
                            BreakEndDtTm = string.Empty;

                        if (r.OneDayInd)
                        {
                            //shift 1, 2
                            LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                            LeaveEndDtTm = r.Dt + r.LeaveEndTm;
                            BreakStartDtTm = r.Dt + r.WSIn2;
                            BreakEndDtTm = r.Dt + r.WSOut2;
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.LeaveStartTm, r.WSIn1) < 0)
                            {
                                LeaveStartDtTm = Dt2 + r.LeaveStartTm;
                                LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                            }
                            else
                            {
                                LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                                if (Sm.CompareDtTm(r.LeaveEndTm, r.WSOut1) <= 0)
                                    LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                                else
                                    LeaveEndDtTm = Dt + r.LeaveEndTm;
                            }

                            if (Sm.CompareDtTm(r.WSIn2, r.WSIn1) < 0)
                                BreakStartDtTm = Dt2 + r.WSIn2;
                            else
                                BreakStartDtTm = Dt + r.WSIn2;

                            if (Sm.CompareDtTm(r.WSOut2, r.WSOut1) < 0)
                                BreakEndDtTm = Dt2 + r.WSOut2;
                            else
                                BreakEndDtTm = Dt + r.WSOut2;
                        }

                        if (r.OneDayInd)
                        {
                            if (r.WSOut3.Length > 0)
                                WorkingDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn1);
                            else
                                WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                        }
                        else
                        {
                            if (r.WSOut3.Length > 0)
                                WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1);
                            else
                                WorkingDuration = ComputeDuration(Dt2 + r.WSOut1, Dt + r.WSIn1);
                        }
                        //WorkingDuration += ROTDuration;
                        WorkingDuration -= r.LeaveDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                            WorkingDuration -= ComputeDuration(BreakEndDtTm, BreakStartDtTm);
                    }
                }
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }

            #endregion

            #region Menghitung OT

            decimal TotalOTHr = 0m;

            if (r.SystemType == mEmpSystemTypeHarian && !r.LateInd)
            {
                decimal OTHr = 0m;
                string
                    StartTm = string.Empty,
                    EndTm = string.Empty;


                    bool
                        IsOTAdjusted = false,
                        IsOTSpecial = false; // beda tanggal

                    string
                        StartTmTemp = string.Empty,
                        EndTmTemp = string.Empty,
                        OTDtTemp = r.OneDayInd ? Dt : Dt2,
                        SpecialStartDt = string.Empty,
                        SpecialStartTm = string.Empty,
                        SpecialEndDt = string.Empty,
                        SpecialEndTm = string.Empty
                        ;

                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        IsOTAdjusted = false;
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        #region Use OT Adjustment

                        foreach (var ota in mlOTAdjustment.Where(
                            ota =>
                                ota.OTDocNo == x.OTDocNo &&
                                ota.EmpCode == EmpCode &&
                                ota.Dt == Dt
                                ))
                        {
                            IsOTAdjusted = true;
                            StartTm = ota.Tm1;
                            EndTm = ota.Tm2;
                        }

                        #endregion

                        #region Use Attendance Barcode

                        if (!IsOTAdjusted)
                        {
                            StartTmTemp = string.Empty;
                            EndTmTemp = string.Empty;

                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            var IsOTOneDayInd = true;
                            var Tm1Temp = string.Empty;

                            if (r.OneDayInd)
                            {
                                #region Shift 1 & 2

                                if (Sm.CompareDtTm(StartTm, EndTm) > 0)
                                {
                                    #region Case 1

                                    IsOTSpecial = true;

                                    foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(Dt + "0000", (i.Dt + i.Tm)) <= 0
                                                        )
                                                    .OrderBy(o1 => o1.Dt)
                                                    .ThenBy(o2 => o2.Tm)
                                                    .Take(1)
                                                    )
                                    {
                                        SpecialStartDt = index.Dt;
                                        SpecialStartTm = index.Tm;
                                    }

                                    if (
                                        SpecialStartDt.Length > 0 &&
                                        SpecialStartTm.Length > 0 &&
                                        Sm.CompareDtTm(SpecialStartDt + SpecialStartTm, Dt + StartTm) <= 0
                                        )
                                    {
                                        SpecialStartDt = Dt;
                                        SpecialStartTm = StartTm;
                                    }

                                    if (SpecialStartDt.Length > 0 && SpecialStartTm.Length > 0)
                                    {
                                        foreach (var index in mlAttendanceLog
                                               .Where(
                                                   i =>
                                                       i.EmpCode == EmpCode &&
                                                       (i.Dt == Dt || i.Dt == Dt2) &&
                                                       Sm.CompareDtTm(Dt + StartTm, (i.Dt + i.Tm)) < 0 &&
                                                       Sm.CompareDtTm((i.Dt + i.Tm), Dt2 + EndTm) <= 0
                                                   )
                                               .OrderByDescending(o1 => o1.Dt)
                                               .ThenByDescending(o2 => o2.Tm)
                                               .Take(1)
                                               )
                                        {
                                            SpecialEndDt = index.Dt;
                                            SpecialEndTm = index.Tm;
                                        }


                                        if (SpecialEndDt.Length <= 0 || SpecialEndTm.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                   .Where(
                                                       i =>
                                                           i.EmpCode == EmpCode &&
                                                           i.Dt == Dt2 &&
                                                           Sm.CompareDtTm(Dt2 + EndTm, i.Dt + i.Tm) <= 0
                                                       )
                                                   .OrderBy(o1 => o1.Dt)
                                                   .ThenBy(o2 => o2.Tm)
                                                   .Take(1)
                                                   )
                                            {
                                                SpecialEndDt = index.Dt;
                                                SpecialEndTm = index.Tm;
                                            }

                                            if (
                                                SpecialEndDt.Length > 0 &&
                                                SpecialEndTm.Length > 0 &&
                                                Sm.CompareDtTm(Dt2 + EndTm, SpecialEndDt + SpecialEndTm) <= 0
                                            )
                                            {
                                                SpecialEndDt = Dt2;
                                                SpecialEndTm = EndTm;
                                            }
                                        }
                                    }
                                    StartTmTemp = SpecialStartTm;
                                    EndTmTemp = SpecialEndTm;

                                    #endregion
                                }

                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                {
                                    #region case 2
                                    // Standard

                                    foreach (var index in mlAttendanceLog
                                    .Where(
                                        i =>
                                            i.EmpCode == EmpCode &&
                                            i.Dt == Dt &&
                                            Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                        )
                                    .OrderByDescending(o => o.Tm)
                                    .Take(1)
                                    )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }

                                    if (StartTmTemp.Length > 0)
                                    {
                                        if (x.Tm2 == "0000")
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, "2359") <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            if (EndTmTemp.Length <= 0)
                                            {
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt2 &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm2) <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            if (EndTmTemp.Length <= 0)
                                            {
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = Sm.Left(index.Tm, 4);
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Shift 3
                                if (Sm.CompareDtTm(r.WSOut1, x.Tm1) <= 0)
                                {
                                    if (Sm.CompareDtTm(r.WSOut1, ActualTmOut) <= 0)
                                        StartTmTemp = r.WSOut1;
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            StartTmTemp = Sm.Left(index.Tm, 4);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }
                                }

                                if (StartTmTemp.Length > 0)
                                {
                                    if (x.Tm2 == "0000")
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + "2359") <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            var OTDtTemp2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp2 &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm2) <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (StartTmTemp.Length <= 0 || EndTmTemp.Length <= 0)
                            {
                                StartTm = string.Empty;
                                EndTm = string.Empty;
                            }
                            else
                            {
                                if (!IsOTSpecial)
                                {
                                    StartTm = x.Tm1;
                                    if (x.Tm2 == "0000")
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) != 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) < 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                }
                            }
                        }

                        #endregion

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            if (IsOTSpecial)
                            {
                                OTHr = ComputeDuration(SpecialEndDt + SpecialEndTm, SpecialStartDt + SpecialStartTm);
                            }
                            else
                            {
                                if (EndTm == "0000")
                                {
                                    if (r.OneDayInd)
                                        OTHr = ComputeDuration(Dt2 + EndTm, OTDtTemp + StartTm);
                                    else
                                    {
                                        var Dt3 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                        OTHr = ComputeDuration(Dt3 + EndTm, OTDtTemp + StartTm);
                                    }
                                }
                                else
                                    OTHr = ComputeDuration(OTDtTemp + EndTm, OTDtTemp + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }
            }

            #endregion

            #region OT menggantikan leave

            var NonROTDuration = RoundTo30Minutes((double)(TotalOTHr));
            TotalOTHr = RoundTo30Minutes((double)(TotalOTHr + ROTDuration));
            
            if (TotalOTHr > 0m && r.LeaveDuration > 0m)
            {
                r.OTToLeaveInd = true;
                if (r.LeaveType != "F")
                {
                    r.WorkingDuration = r.WorkingDuration + r.LeaveDuration;
                    if (r.LeaveDuration >= NonROTDuration)
                    {
                        r.LeaveDuration -= NonROTDuration;
                        NonROTDuration = 0m;
                    }
                    else
                    {
                        NonROTDuration -= r.LeaveDuration;
                        r.LeaveDuration = 0;
                    }
                    r.WorkingDuration = r.WorkingDuration - r.LeaveDuration;

                    if (r.WSOut3.Length > 0)
                    {
                        if (Sm.CompareDtTm(r.WSIn3, r.WSOut3) <= 0)
                            ROTDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn3);
                        else
                            ROTDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn3);
                    }
                    else
                        ROTDuration = 0m;

                    if (r.LeaveDuration >= ROTDuration)
                    {
                        r.LeaveDuration -= ROTDuration;
                        ROTDuration = 0m;
                    }
                    else
                    {
                        ROTDuration -= r.LeaveDuration;
                        r.LeaveDuration = 0;
                    }
                    TotalOTHr = RoundTo30Minutes((double)(NonROTDuration + ROTDuration));
                }
            }


            #endregion

            #region Update Salary based on GoHomeEarlyLeave

            if (r.SystemType == mEmpSystemTypeHarian && r.LeaveCode.Length > 0 && r.LeaveCode == mGoHomeEarlyLeaveCode)
            {
                if (r.WorkingDuration <= mGoHomeEarlyLeaveHr1)
                {
                    r.Salary = (mGoHomeEarlyLeavePerc1 / 100) * r.Salary;
                }
                else
                {
                    if (r.WorkingDuration <= mGoHomeEarlyLeaveHr2)
                    {
                        r.Salary = (mGoHomeEarlyLeavePerc2 / 100) * r.Salary;
                    }
                }
            }

            #endregion

            #region Recompute Salary

            if (r.SystemType != mEmpSystemTypeBorongan)
            {
                if (r.HolInd || r.WSHolidayInd)
                    r.Salary = 0m;
            }

            if (!((r.HolInd || r.WSHolidayInd) && r.SystemType == mEmpSystemTypeBorongan))
            {
                if (r.WSHolidayInd && r.WorkingDuration <= 0)
                    r.Salary = 0m;
            }

            if (!(r.HolInd || r.WSHolidayInd) && r.SystemType != mEmpSystemTypeBorongan)
            {
                if (r.LeaveCode != mGoHomeEarlyLeaveCode)
                {
                    var Duration = r.WorkingDuration;
                    if (Duration > mWorkingHr) Duration = mWorkingHr;
                    r.Salary = (Duration / mWorkingHr) * r.Salary;
                }
            }

            #endregion

            #region Unprocessed data

            if (mIsUseLateValidation && r.LateInd) r.ProcessInd = false;

            if (!r.ProcessInd)
            {
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                r.EmpSalary = 0m;
                r.EmpSalary2 = 0m;
                r.BasicSalary = 0m;
                r.ProductionWages = 0m;
                r.Salary = 0m;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr = 0m;
                r.OT2Hr = 0m;
                r.OTHolidayHr = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt = 0m;
                r.OTHolidayAmt = 0m;
                r.OTToLeaveInd = false;
                r.IncMinWages = 0m;
                r.IncProduction = 0m;
                r.IncPerformance = 0m;
                r.HolidayEarning = 0m;
                r.ExtraFooding = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                r.DedProduction = 0m;
                r.DedProdLeave = 0m;
            }

            #endregion
        }

        private void Process5(string EmpCode, decimal Salary)
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, Row, 2)) && Sm.GetGrdStr(Grd1, Row, 7)=="1")
                {
                    Grd1.Cells[Row, 6].Value = Salary;
                    break;
                }
            }
        }

        #region Get Tables Data

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7)=="1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, StartDt, EndDt, ");
            SQL.AppendLine("PosCode, DeptCode, GrdLvlCode, EmploymentStatus, ");
            SQL.AppendLine("SystemType, PayrunPeriod, PGCode, SiteCode ");
            SQL.AppendLine("From TblEmployeePPS Where ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter + ";");
            else
                SQL.AppendLine(" 0=1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", "SiteCode"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            EmploymentStatus = Sm.DrStr(dr, c[6]),
                            SystemType = Sm.DrStr(dr, c[7]),
                            PayrunPeriod = Sm.DrStr(dr, c[8]),
                            PGCode = Sm.DrStr(dr, c[9]),
                            SiteCode = Sm.DrStr(dr, c[10])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt=@Dt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "In1", "Out1", 
                    
                    //6-9
                    "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            In1 = Sm.DrStr(dr, c[4]),
                            Out1 = Sm.DrStr(dr, c[5]),
                            In2 = Sm.DrStr(dr, c[6]),
                            Out2 = Sm.DrStr(dr, c[7]),
                            In3 = Sm.DrStr(dr, c[8]),
                            Out3 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt=@Dt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("Where A.CancelInd='N'; ");

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt=@Dt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T1.") + ") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt=@Dt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessHoliday(ref List<Holiday> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@Dt", mDocDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select HolDt From TblHoliday Where HolDt=@Dt;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new Holiday() { HolDt = Sm.DrStr(dr, c[0]) });
                }
                dr.Close();
            }
        }

        private void ProcessGradeLevel(ref List<GradeLevel> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select GrdLvlCode, BasicSalary, BasicSalary2 From TblGradeLevelHdr;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "BasicSalary", "BasicSalary2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new GradeLevel()
                        {
                            GrdLvlCode = Sm.DrStr(dr, c[0]),
                            BasicSalary = Sm.DrDec(dr, c[1]),
                            BasicSalary2 = Sm.DrDec(dr, c[2])
                        });
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter.Replace("Tbl.", "A.") + ") ;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", "JoinDt", "ResignDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, StartDt, Amt, Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary ");
            SQL.AppendLine("Where StartDt<=@Dt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ;");

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "Amt", "Amt2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Case When IfNull(Tm, '')='' Then '' Else Left(Tm, 4) End Tm ");
            SQL.AppendLine("From TblAttendanceLog Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", mDocDt);
            var EndDtTemp = mDocDt;

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Tm = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            SQL.AppendLine("And T1.OTDt=@Dt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    if (EmpCode.Length != 0 && Sm.GetGrdStr(Grd1, Row, 7) == "1")
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@Dt", mDocDt);

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt=@Dt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

        #region Class

        private class Result1
        {
            public bool _IsFullDayInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
        }

        #region Additional Class

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
        }

        private class EmpWorkSchedule
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class Holiday
        {
            public string HolDt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
        }

        private class GradeLevel
        {
            public string GrdLvlCode { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        #endregion

        #endregion
    }
}
