﻿namespace RunSystem
{
    partial class FrmBehaviourInd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBehaviourInd));
            this.panel6 = new System.Windows.Forms.Panel();
            this.MeeInd = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtCompetenceName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnCompetenceCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCompetenceCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCompetenceCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.TcBehavior = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRemarkGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeBhvGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRemarkGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeBhvGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRemarkGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeBhvGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRemarkGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeBhvGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRemarkGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeBhvGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcBehavior)).BeginInit();
            this.TcBehavior.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.Tp5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 372);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcBehavior);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Size = new System.Drawing.Size(772, 372);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.MeeInd);
            this.panel6.Controls.Add(this.ChkActInd);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.LueLevel);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.TxtCompetenceName);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.BtnCompetenceCode2);
            this.panel6.Controls.Add(this.BtnCompetenceCode);
            this.panel6.Controls.Add(this.TxtCompetenceCode);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.TxtDocNo);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.DteDocDt);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(772, 159);
            this.panel6.TabIndex = 9;
            // 
            // MeeInd
            // 
            this.MeeInd.EnterMoveNextControl = true;
            this.MeeInd.Location = new System.Drawing.Point(127, 111);
            this.MeeInd.Margin = new System.Windows.Forms.Padding(5);
            this.MeeInd.Name = "MeeInd";
            this.MeeInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeInd.Properties.Appearance.Options.UseFont = true;
            this.MeeInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeInd.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeInd.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeInd.Properties.MaxLength = 400;
            this.MeeInd.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeInd.Properties.ShowIcon = false;
            this.MeeInd.Size = new System.Drawing.Size(579, 20);
            this.MeeInd.TabIndex = 24;
            this.MeeInd.ToolTip = "F4 : Show/hide text";
            this.MeeInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeInd.ToolTipTitle = "Run System";
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(386, 4);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(127, 133);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(579, 20);
            this.MeeRemark.TabIndex = 26;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 135);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Remark";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(68, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Indicator";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(127, 90);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 25;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(253, 20);
            this.LueLevel.TabIndex = 22;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueLevel_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(88, 93);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Level";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCompetenceName
            // 
            this.TxtCompetenceName.EnterMoveNextControl = true;
            this.TxtCompetenceName.Location = new System.Drawing.Point(127, 69);
            this.TxtCompetenceName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompetenceName.Name = "TxtCompetenceName";
            this.TxtCompetenceName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCompetenceName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompetenceName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompetenceName.Properties.Appearance.Options.UseFont = true;
            this.TxtCompetenceName.Properties.MaxLength = 30;
            this.TxtCompetenceName.Properties.ReadOnly = true;
            this.TxtCompetenceName.Size = new System.Drawing.Size(253, 20);
            this.TxtCompetenceName.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(11, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Competence Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCompetenceCode2
            // 
            this.BtnCompetenceCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCompetenceCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCompetenceCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCompetenceCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCompetenceCode2.Appearance.Options.UseBackColor = true;
            this.BtnCompetenceCode2.Appearance.Options.UseFont = true;
            this.BtnCompetenceCode2.Appearance.Options.UseForeColor = true;
            this.BtnCompetenceCode2.Appearance.Options.UseTextOptions = true;
            this.BtnCompetenceCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCompetenceCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCompetenceCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnCompetenceCode2.Image")));
            this.BtnCompetenceCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCompetenceCode2.Location = new System.Drawing.Point(413, 47);
            this.BtnCompetenceCode2.Name = "BtnCompetenceCode2";
            this.BtnCompetenceCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnCompetenceCode2.TabIndex = 18;
            this.BtnCompetenceCode2.ToolTip = "Show Competence";
            this.BtnCompetenceCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCompetenceCode2.ToolTipTitle = "Run System";
            this.BtnCompetenceCode2.Click += new System.EventHandler(this.BtnCompetenceCode2_Click);
            // 
            // BtnCompetenceCode
            // 
            this.BtnCompetenceCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCompetenceCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCompetenceCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCompetenceCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCompetenceCode.Appearance.Options.UseBackColor = true;
            this.BtnCompetenceCode.Appearance.Options.UseFont = true;
            this.BtnCompetenceCode.Appearance.Options.UseForeColor = true;
            this.BtnCompetenceCode.Appearance.Options.UseTextOptions = true;
            this.BtnCompetenceCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCompetenceCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCompetenceCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCompetenceCode.Image")));
            this.BtnCompetenceCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCompetenceCode.Location = new System.Drawing.Point(389, 47);
            this.BtnCompetenceCode.Name = "BtnCompetenceCode";
            this.BtnCompetenceCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCompetenceCode.TabIndex = 17;
            this.BtnCompetenceCode.ToolTip = "Find Competence";
            this.BtnCompetenceCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCompetenceCode.ToolTipTitle = "Run System";
            this.BtnCompetenceCode.Click += new System.EventHandler(this.BtnCompetenceCode_Click);
            // 
            // TxtCompetenceCode
            // 
            this.TxtCompetenceCode.EnterMoveNextControl = true;
            this.TxtCompetenceCode.Location = new System.Drawing.Point(127, 48);
            this.TxtCompetenceCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompetenceCode.Name = "TxtCompetenceCode";
            this.TxtCompetenceCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCompetenceCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompetenceCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompetenceCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCompetenceCode.Properties.MaxLength = 30;
            this.TxtCompetenceCode.Properties.ReadOnly = true;
            this.TxtCompetenceCode.Size = new System.Drawing.Size(253, 20);
            this.TxtCompetenceCode.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(14, 51);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Competence Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(127, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(253, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(50, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(90, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(127, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // TcBehavior
            // 
            this.TcBehavior.Dock = System.Windows.Forms.DockStyle.Top;
            this.TcBehavior.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcBehavior.Location = new System.Drawing.Point(0, 159);
            this.TcBehavior.Name = "TcBehavior";
            this.TcBehavior.SelectedTabPage = this.Tp1;
            this.TcBehavior.Size = new System.Drawing.Size(772, 334);
            this.TcBehavior.TabIndex = 22;
            this.TcBehavior.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp4,
            this.Tp5});
            // 
            // Tp1
            // 
            this.Tp1.Controls.Add(this.MeeRemarkGrd1);
            this.Tp1.Controls.Add(this.MeeBhvGrd1);
            this.Tp1.Controls.Add(this.Grd1);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 306);
            this.Tp1.Text = "Behavior 1";
            // 
            // MeeRemarkGrd1
            // 
            this.MeeRemarkGrd1.EnterMoveNextControl = true;
            this.MeeRemarkGrd1.Location = new System.Drawing.Point(272, 54);
            this.MeeRemarkGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd1.Name = "MeeRemarkGrd1";
            this.MeeRemarkGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd1.Properties.MaxLength = 400;
            this.MeeRemarkGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd1.Properties.ShowIcon = false;
            this.MeeRemarkGrd1.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd1.TabIndex = 36;
            this.MeeRemarkGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd1.ToolTipTitle = "Run System";
            this.MeeRemarkGrd1.Leave += new System.EventHandler(this.MeeRemarkGrd1_Leave);
            this.MeeRemarkGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd1_KeyDown);
            // 
            // MeeBhvGrd1
            // 
            this.MeeBhvGrd1.EnterMoveNextControl = true;
            this.MeeBhvGrd1.Location = new System.Drawing.Point(71, 52);
            this.MeeBhvGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBhvGrd1.Name = "MeeBhvGrd1";
            this.MeeBhvGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeBhvGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBhvGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBhvGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBhvGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBhvGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBhvGrd1.Properties.MaxLength = 400;
            this.MeeBhvGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBhvGrd1.Properties.ShowIcon = false;
            this.MeeBhvGrd1.Size = new System.Drawing.Size(180, 20);
            this.MeeBhvGrd1.TabIndex = 35;
            this.MeeBhvGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeBhvGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBhvGrd1.ToolTipTitle = "Run System";
            this.MeeBhvGrd1.Leave += new System.EventHandler(this.MeeBhvGrd1_Leave);
            this.MeeBhvGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeBhvGrd1_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 306);
            this.Grd1.TabIndex = 45;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp2
            // 
            this.Tp2.Controls.Add(this.MeeRemarkGrd2);
            this.Tp2.Controls.Add(this.MeeBhvGrd2);
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 306);
            this.Tp2.Text = "Behaviour 2";
            // 
            // MeeRemarkGrd2
            // 
            this.MeeRemarkGrd2.EnterMoveNextControl = true;
            this.MeeRemarkGrd2.Location = new System.Drawing.Point(388, 97);
            this.MeeRemarkGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd2.Name = "MeeRemarkGrd2";
            this.MeeRemarkGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd2.Properties.MaxLength = 400;
            this.MeeRemarkGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd2.Properties.ShowIcon = false;
            this.MeeRemarkGrd2.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd2.TabIndex = 38;
            this.MeeRemarkGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd2.ToolTipTitle = "Run System";
            this.MeeRemarkGrd2.Leave += new System.EventHandler(this.MeeRemarkGrd2_Leave);
            this.MeeRemarkGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd2_KeyDown);
            // 
            // MeeBhvGrd2
            // 
            this.MeeBhvGrd2.EnterMoveNextControl = true;
            this.MeeBhvGrd2.Location = new System.Drawing.Point(187, 95);
            this.MeeBhvGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBhvGrd2.Name = "MeeBhvGrd2";
            this.MeeBhvGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeBhvGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBhvGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBhvGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBhvGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBhvGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBhvGrd2.Properties.MaxLength = 400;
            this.MeeBhvGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBhvGrd2.Properties.ShowIcon = false;
            this.MeeBhvGrd2.Size = new System.Drawing.Size(180, 20);
            this.MeeBhvGrd2.TabIndex = 37;
            this.MeeBhvGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeBhvGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBhvGrd2.ToolTipTitle = "Run System";
            this.MeeBhvGrd2.Leave += new System.EventHandler(this.MeeBhvGrd2_Leave);
            this.MeeBhvGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeBhvGrd2_KeyDown);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 306);
            this.Grd2.TabIndex = 45;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // Tp3
            // 
            this.Tp3.Controls.Add(this.MeeRemarkGrd3);
            this.Tp3.Controls.Add(this.MeeBhvGrd3);
            this.Tp3.Controls.Add(this.Grd3);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 306);
            this.Tp3.Text = "Behaviour 3";
            // 
            // MeeRemarkGrd3
            // 
            this.MeeRemarkGrd3.EnterMoveNextControl = true;
            this.MeeRemarkGrd3.Location = new System.Drawing.Point(457, 113);
            this.MeeRemarkGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd3.Name = "MeeRemarkGrd3";
            this.MeeRemarkGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd3.Properties.MaxLength = 400;
            this.MeeRemarkGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd3.Properties.ShowIcon = false;
            this.MeeRemarkGrd3.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd3.TabIndex = 38;
            this.MeeRemarkGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd3.ToolTipTitle = "Run System";
            this.MeeRemarkGrd3.Leave += new System.EventHandler(this.MeeRemarkGrd3_Leave);
            this.MeeRemarkGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd3_KeyDown);
            // 
            // MeeBhvGrd3
            // 
            this.MeeBhvGrd3.EnterMoveNextControl = true;
            this.MeeBhvGrd3.Location = new System.Drawing.Point(256, 111);
            this.MeeBhvGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBhvGrd3.Name = "MeeBhvGrd3";
            this.MeeBhvGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeBhvGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBhvGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBhvGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBhvGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBhvGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBhvGrd3.Properties.MaxLength = 400;
            this.MeeBhvGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBhvGrd3.Properties.ShowIcon = false;
            this.MeeBhvGrd3.Size = new System.Drawing.Size(180, 20);
            this.MeeBhvGrd3.TabIndex = 37;
            this.MeeBhvGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeBhvGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBhvGrd3.ToolTipTitle = "Run System";
            this.MeeBhvGrd3.Leave += new System.EventHandler(this.MeeBhvGrd3_Leave);
            this.MeeBhvGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeBhvGrd3_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 306);
            this.Grd3.TabIndex = 45;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // Tp4
            // 
            this.Tp4.Controls.Add(this.MeeRemarkGrd4);
            this.Tp4.Controls.Add(this.MeeBhvGrd4);
            this.Tp4.Controls.Add(this.Grd4);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(766, 306);
            this.Tp4.Text = "Behaviour 4";
            // 
            // MeeRemarkGrd4
            // 
            this.MeeRemarkGrd4.EnterMoveNextControl = true;
            this.MeeRemarkGrd4.Location = new System.Drawing.Point(336, 121);
            this.MeeRemarkGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd4.Name = "MeeRemarkGrd4";
            this.MeeRemarkGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd4.Properties.MaxLength = 400;
            this.MeeRemarkGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd4.Properties.ShowIcon = false;
            this.MeeRemarkGrd4.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd4.TabIndex = 38;
            this.MeeRemarkGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd4.ToolTipTitle = "Run System";
            this.MeeRemarkGrd4.Leave += new System.EventHandler(this.MeeRemarkGrd4_Leave);
            this.MeeRemarkGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd4_KeyDown);
            // 
            // MeeBhvGrd4
            // 
            this.MeeBhvGrd4.EnterMoveNextControl = true;
            this.MeeBhvGrd4.Location = new System.Drawing.Point(135, 119);
            this.MeeBhvGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBhvGrd4.Name = "MeeBhvGrd4";
            this.MeeBhvGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeBhvGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBhvGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBhvGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBhvGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBhvGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBhvGrd4.Properties.MaxLength = 400;
            this.MeeBhvGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBhvGrd4.Properties.ShowIcon = false;
            this.MeeBhvGrd4.Size = new System.Drawing.Size(180, 20);
            this.MeeBhvGrd4.TabIndex = 37;
            this.MeeBhvGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeBhvGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBhvGrd4.ToolTipTitle = "Run System";
            this.MeeBhvGrd4.Leave += new System.EventHandler(this.MeeBhvGrd4_Leave);
            this.MeeBhvGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeBhvGrd4_KeyDown);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 306);
            this.Grd4.TabIndex = 45;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // Tp5
            // 
            this.Tp5.Controls.Add(this.MeeRemarkGrd5);
            this.Tp5.Controls.Add(this.MeeBhvGrd5);
            this.Tp5.Controls.Add(this.Grd5);
            this.Tp5.Name = "Tp5";
            this.Tp5.Size = new System.Drawing.Size(766, 306);
            this.Tp5.Text = "Behaviour 5";
            // 
            // MeeRemarkGrd5
            // 
            this.MeeRemarkGrd5.EnterMoveNextControl = true;
            this.MeeRemarkGrd5.Location = new System.Drawing.Point(397, 101);
            this.MeeRemarkGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd5.Name = "MeeRemarkGrd5";
            this.MeeRemarkGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd5.Properties.MaxLength = 400;
            this.MeeRemarkGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd5.Properties.ShowIcon = false;
            this.MeeRemarkGrd5.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd5.TabIndex = 38;
            this.MeeRemarkGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd5.ToolTipTitle = "Run System";
            this.MeeRemarkGrd5.Leave += new System.EventHandler(this.MeeRemarkGrd5_Leave);
            this.MeeRemarkGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd5_KeyDown);
            // 
            // MeeBhvGrd5
            // 
            this.MeeBhvGrd5.EnterMoveNextControl = true;
            this.MeeBhvGrd5.Location = new System.Drawing.Point(196, 99);
            this.MeeBhvGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBhvGrd5.Name = "MeeBhvGrd5";
            this.MeeBhvGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeBhvGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBhvGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBhvGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBhvGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBhvGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBhvGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBhvGrd5.Properties.MaxLength = 400;
            this.MeeBhvGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBhvGrd5.Properties.ShowIcon = false;
            this.MeeBhvGrd5.Size = new System.Drawing.Size(180, 20);
            this.MeeBhvGrd5.TabIndex = 37;
            this.MeeBhvGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeBhvGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBhvGrd5.ToolTipTitle = "Run System";
            this.MeeBhvGrd5.Leave += new System.EventHandler(this.MeeBhvGrd5_Leave);
            this.MeeBhvGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeBhvGrd5_KeyDown);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 306);
            this.Grd5.TabIndex = 45;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // FrmBehaviourInd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 372);
            this.Name = "FrmBehaviourInd";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcBehavior)).EndInit();
            this.TcBehavior.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.Tp4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.Tp5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBhvGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraTab.XtraTabControl TcBehavior;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        public DevExpress.XtraEditors.SimpleButton BtnCompetenceCode2;
        public DevExpress.XtraEditors.SimpleButton BtnCompetenceCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtCompetenceCode;
        public DevExpress.XtraEditors.TextEdit TxtCompetenceName;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        public DevExpress.XtraEditors.LookUpEdit LueLevel;
        private DevExpress.XtraEditors.MemoExEdit MeeInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeBhvGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeBhvGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeBhvGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeBhvGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd5;
        private DevExpress.XtraEditors.MemoExEdit MeeBhvGrd5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
    }
}