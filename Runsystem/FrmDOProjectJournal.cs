﻿#region Update
/*
    09/02/2021 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOProjectJournal : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDOProjectJournalFind FrmFind;

        #endregion

        #region Constructor

        public FrmDOProjectJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    BtnDODocNo.Enabled = false;
                    BtnJournalDocNo.Enabled = false;
                    break;
                case mState.Insert:
                    BtnDODocNo.Enabled = true;
                    BtnJournalDocNo.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDODocNo, TxtJournalDocNo });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOProjectJournalFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblDOProjectJournal(DODocNo, JournalDocNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DODocNo, @JournalDocNo, @UserCode, CurrentDateTime()); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DODocNo", TxtDODocNo.Text);
                Sm.CmParam<String>(ref cm, "@JournalDocNo", TxtJournalDocNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtDODocNo.Text, TxtJournalDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DODocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DODocNo, JournalDocNo From TblDOProjectJournal Where DODocNo = @DODocNo And JournalDocNo = @JournalDocNo; ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DODocNo", DODocNo);
                Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] { "DODocNo", "JournalDocNo" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDODocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDODocNo, "DO#", false) ||
                Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false) ||
                IsJournalExisted()
                ;
        }

        private bool IsJournalExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOProjectJournal ");
            SQL.AppendLine("Where JournalDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtJournalDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Journal existed.");
                TxtJournalDocNo.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void BtnDODocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDOProjectJournalDlg(this, 1));
        }

        private void BtnDODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDODocNo, "DO#", false))
            {
                var f = new FrmDOProject(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmDOProject");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDODocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsTxtEmpty(TxtDODocNo, "DO#", false)) Sm.FormShowDialog(new FrmDOProjectJournalDlg(this, 2));
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmJournal");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
