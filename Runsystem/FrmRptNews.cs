#region Update
/*
    05/11/2021 [IBL/PRODUCT] Menu baru Reporting News
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Drawing.Printing;
using System.Net;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Wa = RunSystem.WinAPI;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptNews : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string
            mNoOfActiveNews = string.Empty,
            mNoOfNewsPriority = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty;


        private byte[] downloadedData;
        private byte[] image = null;
        #endregion

        #region Constructor

        public FrmRptNews(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                ShowData();
                Grd1.GroupBox.Visible = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mNoOfActiveNews = Sm.GetParameter("NoOfActiveNews");
            mNoOfNewsPriority = Sm.GetParameter("NoOfNewsPriority");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");

            if (mNoOfActiveNews.Length == 0) mNoOfActiveNews = "3";
            if (mNoOfNewsPriority.Length == 0) mNoOfNewsPriority = "20";
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select* From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X.* From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select '1' As DocType, PostingDt, Concat(NewsTitle, '\n\n', NewsLead, '\n\n', Date_Format(PostingDt, '%d/%m/%Y')) As TitleLead, ");
            SQL.AppendLine("        NewsLink, Photo ");
            SQL.AppendLine("        From TblNews ");
            SQL.AppendLine("        Where PriorityInd = 'Y' ");
            SQL.AppendLine("        And PostingDt <= Left(CurrentDateTime(), 8) ");
            SQL.AppendLine("        And ExpiredDt >= Left(CurrentDateTime(), 8) ");
            SQL.AppendLine("        Limit " + mNoOfActiveNews);
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, PostingDt, Concat(NewsTitle, '\n\n', NewsLead, '\n\n', Date_Format(PostingDt, '%d/%m/%Y')) As TitleLead, ");
            SQL.AppendLine("    NewsLink, Photo ");
            SQL.AppendLine("    From TblNews ");
            SQL.AppendLine("    Where PriorityInd = 'N' ");
            SQL.AppendLine("    And PostingDt <= Left(CurrentDateTime(), 8) ");
            SQL.AppendLine("    And ExpiredDt >= Left(CurrentDateTime(), 8) ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Order By T.DocType Asc, T.PostingDt Desc ");
            SQL.AppendLine("Limit " + mNoOfNewsPriority);

            return SQL.ToString();
        }

        private void SetGrd()
        {   
            Grd1.Cols.Count = 5;
            //Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "Title & Lead",

                    //1-4
                    "Link",
                    "",
                    "Photo",
                    "Photo"
                },
                new int[]
                {
                    //0
                    250,

                    //1-4
                    200, 20, 0, 65
                }
            );

            Sm.GrdColButton(Grd1, new int[] { 2, 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 });
        }

        override protected void HideInfoInGrd()
        {
            
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string HRemark = string.Empty, DRemark = string.Empty;
                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(),
                        new string[]
                        { 
                            //0
                            "TitleLead", 

                            //1-5
                            "NewsLink", "Photo",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        }, true, false, false, true
                    );

                Sm.SetGrdColHdrAutoAlign(Grd1);
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Additional Method

        private void OpenURL(string Link)
        {

            string path = Link;
            string iexplore = @"C:\Program Files\Internet Explorer\iexplore.exe";
            try
            {
                System.Diagnostics.Process.Start(iexplore, path);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }

        }

        private byte[] DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }

            return downloadedData;
        }

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string mURL = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                if (MessageBox.Show("Open this link ("+mURL+")?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    OpenURL(mURL);
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                string mFileName = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                if (image != null) image = null;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
                    image = DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, mFileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);

                Sm.FormShowDialog(new FrmRptNewsDlg(this, mFileName, image));
            }
        }

        protected override void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string mURL = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                if (MessageBox.Show("This will open the " + mURL + " link, do you want to continue?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    OpenURL(mURL);
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                string mFileName = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                if (image != null) image = null;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
                    image = DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, mFileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);

                Sm.FormShowDialog(new FrmRptNewsDlg(this, mFileName, image));
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Grid Event

        private void Grd1_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Open Link";
            }
            else if (e.ColIndex == 4)
            {
                e.Text = "View Photo";
                e.GetType();
            }
        }

        #endregion

        #endregion
    }
}
