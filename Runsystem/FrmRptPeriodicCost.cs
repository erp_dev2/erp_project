﻿#region Update
/*
    10/01/2019 [DITA] Dikarenakan adanya menu reporting pelengkap Monthly Cost (With DO Information) (020202), maka reporting Cost By Period 0202990102 ingin diadakan juga yang versi with DO Information.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptPeriodicCost : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPeriodicCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueEntCode(ref LueEntCode);
                SetLueCCCode(ref LueCCCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName, ");
            SQL.AppendLine("(Sum(Mth01)+Sum(Mth02)+Sum(Mth03)+Sum(Mth04)+Sum(Mth05)+Sum(Mth06)+ ");
            SQL.AppendLine("Sum(Mth07)+Sum(Mth08)+Sum(Mth09)+Sum(Mth10)+Sum(Mth11)+Sum(Mth12)) As Total");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName, ");
            SQL.AppendLine("        Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            SQL.AppendLine("        Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            SQL.AppendLine("        Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            SQL.AppendLine("        Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            SQL.AppendLine("        Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
            SQL.AppendLine("        Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            SQL.AppendLine("        Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
            SQL.AppendLine("        Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
            SQL.AppendLine("        Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            SQL.AppendLine("        Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            SQL.AppendLine("        Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            SQL.AppendLine("        Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.CCCode As CodeParent, IfNull(C.CCName, '') As Parent, B.CCCode As CodeChild, B.CCName As Child, ");
            SQL.AppendLine("        A.CCtCode As CCtCode, E.AcNo, E.AcDesc, A.CCtName, F.OptDesc As CCGrpName, D.Mth, H.EntCode, H.EntName, Sum(IfNull(E.Amt, 0)) As Amt ");
            SQL.AppendLine("        From TblCostCategory A  ");
            SQL.AppendLine("        Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("        Left Join TblCostCenter C On B.Parent = C.CCCode  ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("            Select convert('02' using latin1) Union All ");
            SQL.AppendLine("            Select convert('03' using latin1) Union All ");
            SQL.AppendLine("            Select convert('04' using latin1) Union All ");
            SQL.AppendLine("            Select convert('05' using latin1) Union All ");
            SQL.AppendLine("            Select convert('06' using latin1) Union All ");
            SQL.AppendLine("            Select convert('07' using latin1) Union All ");
            SQL.AppendLine("            Select convert('08' using latin1) Union All ");
            SQL.AppendLine("            Select convert('09' using latin1) Union All ");
            SQL.AppendLine("            Select convert('10' using latin1) Union All ");
            SQL.AppendLine("            Select convert('11' using latin1) Union All ");
            SQL.AppendLine("            Select convert('12' using latin1)  ");
            SQL.AppendLine("        ) D On 0=0  ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select Mth, AcNo, Sum(Amt) Amt, AcDesc ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select Substring(T1.DocDt, 5, 2) As Mth, T2.AcNo, T3.AcDesc, ");
            SQL.AppendLine("                Case T3.AcType  ");
            SQL.AppendLine("                    When 'D' Then IfNull(T2.DAmt, 0)- IfNull(T2.CAmt,0) ");
            SQL.AppendLine("                    When 'C' Then IfNull(T2.CAmt, 0)- IfNull(T2.DAmt,0) ");
            SQL.AppendLine("                End As Amt ");
            SQL.AppendLine("                From TblJournalHdr T1 ");
            SQL.AppendLine("                Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("                Inner Join TblCoa T3 On T2.AcNo = T3.AcNo ");
            SQL.AppendLine("            ) T Group By Mth, AcNo, AcDesc ");
            SQL.AppendLine("            Having Amt<>0 ");
            SQL.AppendLine("        ) E On A.AcNo=E.AcNo And D.Mth=convert(E.Mth using latin1) ");
            SQL.AppendLine("        Inner Join TblOption F On A.CCGrpCode = F.OptCode And F.OptCat = 'CostCenterGroup' ");
            SQL.AppendLine("        Left Join TblProfitCenter G On B.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("        Left Join TblEntity H On G.EntCode=H.EntCode ");
            SQL.AppendLine("        Group By C.CCCode, C.CCName, B.CCCode, B.CCName, ");
            SQL.AppendLine("        A.CCtCode, E.AcNo, E.AcDesc, A.CCtName, F.OptDesc , D.Mth, H.EntCode, H.EntName ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") X ");

            SQL.AppendLine("Group By CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName ");
            SQL.AppendLine("Having  ");
            SQL.AppendLine("    Sum(Mth01)<>0 Or Sum(Mth02)<>0 Or Sum(Mth03)<>0 Or Sum(Mth04)<>0 Or Sum(Mth05)<>0 Or Sum(Mth06)<>0 Or ");
            SQL.AppendLine("    Sum(Mth07)<>0 Or Sum(Mth08)<>0 Or Sum(Mth09)<>0 Or Sum(Mth10)<>0 Or Sum(Mth11)<>0 Or Sum(Mth12)<>0 ");
            SQL.AppendLine("Order By Parent, Child, CCtName ");
            SQL.AppendLine(") Z1 ");
            SQL.AppendLine("Where Z1.CodeChild In (select Distinct A.CCCode From ");
            SQL.AppendLine("TblGroupCostCenter A ");
            SQL.AppendLine("Inner Join TblUser B On A.GrpCode = B.GrpCode ");
            SQL.AppendLine("Where Upper(B.UserCode)= Upper('" + Gv.CurrentUserCode + "'))");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Cost Center"+Environment.NewLine+"Code Parent",

                        //1-5
                        "Cost Center ",
                        "Cost Center"+Environment.NewLine+"Code Child",
                        "Cost Center ", 
                        "Cost Category Code",
                        "Acount",

                        //6-10
                        "Description",
                        "Cost Category",
                        "Cost Category's"+Environment.NewLine+"Group",
                        "Entity",
                        "Total",

                        //11
                        "",
                    },
                    new int[] 
                    {
                        //0
                        80, 

                        //1-5
                        100, 80, 100, 80, 150, 
                        
                        //6-10
                        200, 100, 180, 180, 180,

                        //11
                        20
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4 });
            Sm.GrdColButton(Grd1, new int[] { 11 });
        }


        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "Z1.CodeChild", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "Z1.EntCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        { 
                            //0
                            "CodeParent",  
                            //1-5
                            "Parent", "CodeChild", "Child", "CCtCode", "AcNo",   
                            //6-10
                            "AcDesc", "CCtName", "CCGrpName", "EntName", "Total"                            
                        },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 7 : 3);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }

        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10 });
        }

        private static void SetLueCCCode(ref LookUpEdit Lue, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select Distinct A.CCCode As Col1, C.CCName As Col2 From ");
            SQL.AppendLine("TblGroupCostCenter A ");
            SQL.AppendLine("Inner Join TblUser B On A.GrpCode = B.GrpCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCOde ");
            if (EntCode.Length > 0)
            {
                SQL.AppendLine("And C.CCCode In (");
                SQL.AppendLine("    Select CCCOde From TblCostCenter T1, TblProfitCenter T2 ");
                SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode  ");
                SQL.AppendLine("    And T2.EntCode Is Not Null And T2.EntCode='" + EntCode + "' ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Where B.UserCode = '" + Gv.CurrentUserCode + "' ");
            SQL.AppendLine("Order By C.CCName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptPeriodicCostDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 4), Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtCCCodeParent.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCCodeChild.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtCCtCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRptPeriodicCostDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 4), Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtCCCodeParent.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCCodeChild.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtCCtCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
            var EntCode = Sm.GetLue(LueEntCode);
            LueCCCode.EditValue = null;
            ChkCCCode.Checked = false;
            if (EntCode.Length == 0)
                SetLueCCCode(ref LueCCCode, string.Empty);
            else
                SetLueCCCode(ref LueCCCode, EntCode);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            var EntCode = Sm.GetLue(LueEntCode);
            if (EntCode.Length == 0)
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), EntCode);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion

    }
}
