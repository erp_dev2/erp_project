﻿#region Update
/*
    18/10/2019 [TKG/SIER] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJobCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmJobCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmJobCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtJobCtCode, TxtJobCtName }, true);
                    TxtJobCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtJobCtCode, TxtJobCtName }, false);
                    TxtJobCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtJobCtName }, false);
                    TxtJobCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtJobCtCode, TxtJobCtName });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJobCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobCtCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobCtCode, string.Empty, false) || Sm.StdMsgYN("Delete", string.Empty) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblJobCategory Where JobCtCode=@JobCtCode;" };
                Sm.CmParam<String>(ref cm, "@JobCtCode", TxtJobCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblJobCategory(JobCtCode, JobCtName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@JobCtCode, @JobCtName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update JobCtName=@JobCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@JobCtCode", TxtJobCtCode.Text);
                Sm.CmParam<String>(ref cm, "@JobCtName", TxtJobCtName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtJobCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string JobCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@JobCtCode", JobCtCode);
                Sm.ShowDataInCtrl(
                        ref cm, "Select JobCtCode, JobCtName From TblJobCategory Where JobCtCode=@JobCtCode;",
                        new string[]{ "JobCtCode", "JobCtName" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtJobCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtJobCtName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtJobCtCode, "Job's category code", false) ||
                Sm.IsTxtEmpty(TxtJobCtName, "Job's category name", false) ||
                IsCodeExisted();
        }

        private bool IsCodeExisted()
        {
            if (TxtJobCtCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                    "Select 1 From TblJobCategory Where JobCtCode=@Param Limit 1;",
                    TxtJobCtCode.Text,
                    "Job's category code ( " + TxtJobCtCode.Text + " ) already existed.");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtJobCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJobCtCode);
        }

        private void TxtJobCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJobCtName);
        }

        #endregion

        #endregion
    }
}
