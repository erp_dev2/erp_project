﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptLogisticPerformance : RunSystem.FrmBase6
    {
        //update by ari [19/05/17] tambah type transaksi Rcv item from other WHS, Rcv item from other WHS (Without DO) 

        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string DocDt = string.Empty;
        private bool mIsFilterByItCt = false; 
        
        #endregion

        #region Constructor

        public FrmRptLogisticPerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();   
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetLueTransaksi(ref LueTrxCode);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private void SetSQL(string TypeTrx)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from ( ");
            // MR
            if (TypeTrx == "1")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter,A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,  ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '1' as DocType   ");
                SQL.AppendLine("From tblmaterialrequesthdr A   ");
                SQL.AppendLine("inner join tblmaterialrequestdtl B on B.DocNo=A.DocNo And B.CancelInd='N' And B.Status<>'C'   ");
                SQL.AppendLine("Inner JOin TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // Recv VD
            if (TypeTrx == "2")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,    ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '2' as DocType   ");
                SQL.AppendLine("From TblRecvVdHdr A  ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By  B.ItCode  ");
            }
            //DO To Other Whs
            if (TypeTrx == "3")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc, B.ItCode, C.Itname, C.ForeignName,   ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday,  '3' as DocType  ");
                SQL.AppendLine("From TblDOWhsHdr A  ");
                SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // DO To Department with DO Request
            if (TypeTrx == "4")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,    ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday,  '4' as DocType   ");
                SQL.AppendLine("From TblDODeptHdr A  ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And A.DORequestDeptDocNo is not null  And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // DO To Customer  
            if (TypeTrx == "5")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,    ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday,  '5' as DocType  ");
                SQL.AppendLine("From TblDOCtHdr A  ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // DO To Dept Without Do Request
            if (TypeTrx == "6")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,    ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday,  '6' as DocType   ");
                SQL.AppendLine("From TblDODeptHdr A  ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And A.DORequestDeptDocNo is null And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // DO Request By Department
            if (TypeTrx == "7")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc,  B.ItCode, C.Itname, C.ForeignName,    ");
                SQL.AppendLine("Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode),  ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '7' as DocType    ");
                SQL.AppendLine("From TblDORequestDeptHdr A  ");
                SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNO  ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode  ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }

            //Recv other WHS
            if (TypeTrx == "8")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, E.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc, ");
                SQL.AppendLine("C.ItCode, D.Itname, D.ForeignName, Right(Last_day(A.DocDt), 2) AS Totalday, Count(C.ItCode), ");
                SQL.AppendLine("Round(Count(C.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '8' as DocType ");
                SQL.AppendLine("From TblRecvWhsHdr A ");
                SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group by C.ItCode ");
            }

            //Recv other WHS (Without DO)
            if (TypeTrx == "9")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, E.Username, group_Concat(Distinct A.DocNo) As DocNo, count(A.DocNo)As TotDoc, ");
                SQL.AppendLine("B.ItCode, C.Itname, C.ForeignName, Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode), ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '9' as DocType ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
                SQL.AppendLine("Where B.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group by B.ItCode ");
            }
           

            SQL.AppendLine(")X ");
            SQL.AppendLine("Where X.MonthFilter=@MonthFilter ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "",
                        "User", 
                        "Document",
                        "Total"+Environment.NewLine+"Document",
                        
 
                        //6-8
                        "Item Name",
                        "Foreign Name",
                        "Average Usage Day"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        120, 20, 150, 200, 80,
                        //6-8
                        180, 200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColButton(Grd1, new int[] {2}); 
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
                return;
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }

            if (Sm.IsLueEmpty(LueTrxCode, "Transaction type")) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MonthFilter", DocDt);
                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "X.UserName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.UserName, X.ItName;",
                    new string[] { 
                        "ItCode", 
                        "UserName", "DocNo", "TotDoc", "ItName", "ForeignName", 
                        "AvgDay" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueTransaksi(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Material Request' As Col2 " +
                "Union All " +
                "Select '2' As Col1, 'Receiving Item From Vendor' As Col2 " +
                "Union All " +
                "Select '3' As Col1, 'DO to Other Warehouse' As Col2 " +
                "Union All " +
                "Select '4' As Col1, 'DO To Department with DO Request' As Col2 " +
                "Union All " +
                "Select '5' As Col1, 'DO to Customer' As Col2 " +
                "Union All " +
                "Select '6' As Col1, 'DO To Dept Without Do Request' As Col2 " +
                "Union All " +
                "Select '7' As Col1, 'DO Request By Department' As Col2 " +
                "Union All " +
                "Select '8' As Col1, 'Receiving Item From Other WHS' As Col2 " +
                "Union All " +
                "Select '9' As Col1, 'Receiving Item From Other WHS (Without DO)' As Col2 ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Username");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueTrxCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrxCode, new Sm.RefreshLue1(SetLueTransaksi));
            if (Sm.GetLue(LueTrxCode).Length > 0)
                SetSQL(Sm.GetLue(LueTrxCode));
        }

       

        #endregion    
    }
}
