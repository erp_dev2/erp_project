﻿#region Update
/*
    12/10/2017 [HAR] tambah informasi settle by
    18/10/2017 [HAR] WOR yang blm kebikin WO nya pingin dimunculin
    24/10/2017 [HAR] tambah filter location
    05/05/2018 [ARI] tambah filter TO dan Kolom Description
    07/06/2018 [TKG] Pada saat digroup, keluar keterangan jumlah record per group.
    27/07/2018 [HAR] tambah informasi duration date duration time
    30/07/2018 [HAR] bug label duration hours 
    02/12/2021 [RDA/IOK] tambah kolom DOD# dan informasi item berdasarkan param ShowWOInfoFormat
    02/12/2021 [MYA/IOK] Pada menu WOR - WOR Settlement (020813), menambah sebelah Document Date WO muncul durasi WO (hours minute), Mechanic, Activity dan Total di bagian detail baris paling bawah
    26/01/2022 [MYA/IOK] Support : Pada menu WOR - WOR Settlement (020813), menambah sebelah Document Date WO muncul durasi WO (hours minute), Mechanic, Activity dan Total di bagian detail baris paling bawah
    13/14/2023 [VIN/IOK] kolom to name terbuka
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWORSettlement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool mIsFilterBySite = false;

        private string mShowWOInfoFormat = string.Empty;

        #endregion

        #region Constructor

        public FrmRptWORSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
                SetGrd();
                SetSQL();
                SetLueLocCode(ref LueLocCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DocNoWOR, A.TOCode, A.DocDt, A.DownDt, Concat(Left(A.DownTm,2),':', Right(A.DownTm,2))As DownTm, ");
            SQL.AppendLine("Case A.WOStatus When 'O' Then 'Open' When 'C' Then 'Close' End WOStatus, A.CreateBy As CreateByWOR, D.AssetName As TOName, ");
            SQL.AppendLine("E.OptDesc As Type, F.OptDesc As Status, G.OptDesc As Problem, H.LocName, I.DocNo As DocNoWO, I.DocDt As DocDtWO, ");
            SQL.AppendLine("I.CreateBy As CreateByWO, I.SettleInd, J.UserName As SettleBy, I.SettleDt, A.Description, ");
            SQL.AppendLine("Concat(date_Format(A.DownDt, '%Y-%m-%d'), ' ', Concat(Left(A.DownTm, 2),':',Right(A.DownTM, 2))) As timedown, ");
            SQL.AppendLine("Concat(date_format(A.CloseDt, '%Y-%m-%d'), ' ', ");
            SQL.AppendLine("Concat(left(right(if(A.CloseDt is null, null, A.lastUpdt), 4), 2), ':', ");
            SQL.AppendLine("Right(right(if(A.CloseDt is null,null, A.lastUpdt), 4), 2))) As timeSettle, ");
            SQL.AppendLine("DateDiff(ifnull(CloseDt, DownDt), DownDt) As DurationDt, ");
            SQL.AppendLine("TIME_FORMAT(TimeDiff( ");
            SQL.AppendLine("Concat(date_format(ifnull(A.CloseDt, null), '%Y-%m-%d'), ' ', ");
            SQL.AppendLine("Concat(left(right(if(A.CloseDt is null, null, A.lastUpdt), 4), 2), ':', ");
            SQL.AppendLine("Right(right(if(A.CloseDt is null, null, A.lastUpdt), 4), 2))), ");
            SQL.AppendLine("Concat(date_Format(A.DownDt, '%Y-%m-%d'), ' ', Concat(Left(A.DownTm, 2),':',Right(A.DownTM, 2))) ");
            SQL.AppendLine("), '%H:%i') As DurationTm, ");
            SQL.AppendLine("K.DocNo as DocNoDOD, K.ItName, K.Qty3, K.Price, K.TotalPrice, ");
            SQL.AppendLine("L.EquipmentName, L.Tm3, L.MCode, L.MName, L.Activity ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Inner Join TblTOHdr B On A.TOCode=B.AssetCode ");
            SQL.AppendLine("Inner Join TblAsset D On B.AssetCode=D.AssetCode ");
            SQL.AppendLine("Left Join TblOption E On A.MtcType=E.OptCode And E.OptCat ='MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption F On A.MtcStatus=F.OptCode And F.OptCat ='MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption G On A.SymProblem=G.OptCode And G.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Left Join TblLocation H On B.LocCode=H.LocCode ");
            SQL.AppendLine("Left Join TblWOHdr I On A.DocNo=I.WORDocNo And I.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblUser J On I.SettleBy = J.UserCode ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("    SELECT A.WORDocNo, A.DocNo, C.ItName, SUM(B.Qty3) Qty3, IFNULL(D.UPrice, 0) AS Price, (SUM(B.Qty3) * IFNULL(D.UPrice, 0)) AS TotalPrice ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    INNER JOIN tblstockprice D ON B.ItCode = D.ItCode ");
            SQL.AppendLine("        AND B.BatchNo = D.BatchNo ");
            SQL.AppendLine("        AND B.Source = D.Source ");
            SQL.AppendLine("    GROUP BY A.WORDocNo, A.DocNo, C.ItName ");
            SQL.AppendLine(")K ON I.WORDocNo = K.WORDocNo ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("	SELECT A.DocNo, F.EquipmentName, B.Tm3, concat(Group_Concat(D.MechanicCode separator '#'), '#') Mcode, group_Concat(E.MechanicName)MName, B.Remark AS Activity ");
            SQL.AppendLine("	FROM TblWOHdr A ");
            SQL.AppendLine("	Inner Join TblWoDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	Inner Join TblOption C On C.OptCat='WOActivity' And B.WOActivity=C.OptCode ");
            SQL.AppendLine("	Left Join TblWODtl2 D ON B.DocNo = D.DocNo AND B.DNo = D.DNo ");
            SQL.AppendLine("	Left Join TblMechanic E ON D.MechanicCode = E.MechanicCode ");
            SQL.AppendLine("	LEFT JOIN tblequipment F ON A.EquipmentCode = F.EquipmentCode ");
            SQL.AppendLine("	GROUP BY A.DocNo, F.EquipmentName, B.Tm3, C.OptDesc ");
            SQL.AppendLine(")L ON I.DocNo = L.DocNo ");
            SQL.AppendLine("Where A.cancelInd = 'N'  ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document WO Request", 
                        "",
                        "Document Date"+Environment.NewLine+"WO Request", 
                        "Status",
                        "Down Date",
                        
                        //6-10
                        "Down Time",
                        "Type",
                        "Symptom Problem",
                        "TO Code",
                        "TO Name",

                        //11-15
                        "Location",
                        "WO Status",
                        "Create By"+Environment.NewLine+"WO Request",
                        "DOD#",
                        "Item Name",

                        //16-20
                        "Quantity",
                        "Price",
                        "Total Price / Value",
                        "Document WO",
                        "",

                        //21-25
                        "Document Date WO",
                        "Equipment",
                        "Hours Minute",
                        "Mechanic",
                        "Activity",

                        //26-30
                        "Create By"+Environment.NewLine+"Work Order",
                        "Settlement",
                        "Settlement By",
                        "Settlement Date",
                        "Description",

                        //31-35
                        "Date And Time WOR",
                        "Date And Time WOR Settle",
                        "Duration"+Environment.NewLine+"Days",
                        "Duration"+Environment.NewLine+"Hours",
                        "Hours Minute"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 20, 120, 250, 100,
                        
                        //6-10
                        90, 150, 180, 120, 180, 
                       
                        //11-15
                        150, 100, 120, 150, 150,

                        //16-20
                        100, 150, 150, 150, 20,

                        //21-25
                        130, 150, 150, 150, 150,
                        
                        //26-30
                        140, 90, 200, 120, 200,

                        //31-35
                        150, 150, 150, 150, 0

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 27 });
            Sm.GrdColButton(Grd1, new int[] { 2, 20 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5, 21, 29 });
            Sm.GrdFormatDec(Grd1, new int[] {16, 17, 18, 33}, 0);
            //Sm.GrdFormatTime(Grd1, new int[] { 23 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9, 12, 27, 31, 32 }, false);
            if ((mShowWOInfoFormat.Trim()) == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 22, 23, 24, 25 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9, 12, 27, 31, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtWORDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWODocNo.Text, "I.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "B.LocCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtTOCode.Text, new string[] { "A.TOCode", "D.AssetName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNoWOR",  

                            //1-5
                            "DocDt", "Status", "DownDt", "DownTm", "Type",

                            //6-10
                            "Problem", "TOCode", "TOName", "LocName", "WOStatus",

                            //11-15
                            "CreateByWOR", "DocNoDOD", "ItName", "Qty3", "Price",

                            //16-20
                            "TotalPrice", "DocNoWO", "DocDtWO", "EquipmentName", "Tm3",   

                            //21-25
                            "MName", "Activity", "CreateByWO", "SettleInd", "SettleBy",  

                            //26-30
                            "SettleDt", "Description", "timedown", "timesettle","DurationDt",

                            //31
                            "Durationtm"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 20);
                        }, true, false, false, false
                    );
                ComputeEstPrice();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mShowWOInfoFormat = Sm.GetParameter("ShowWOInfoFormat");

            if (mShowWOInfoFormat.Length == 0) mShowWOInfoFormat = "1";
        }

        internal void ComputeEstPrice()
        {
            decimal TotalHours = 0m;
            
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                TotalHours += Sm.GetGrdDec(Grd1, r, 35);
            }
            string mTotalHours = Convert.ToString(TotalHours);
            if (mTotalHours.Length > 1)
            {
                TxtTotalHoursMinute.EditValue = Sm.Left(mTotalHours, mTotalHours.Length - 2) + ":" + Sm.Right(Convert.ToString(TotalHours), 2);
            }
            else
            {
                TxtTotalHoursMinute.EditValue = "00:00";

            }

        }

        #endregion

        #endregion

        #region Event

        private void ChkWORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document WO Request");
        }

        private void ChkWODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Work Order");
        }

        private void TxtWORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtWODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtTOCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTOCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Technical Object");
        }
      
        #endregion

    }
}
