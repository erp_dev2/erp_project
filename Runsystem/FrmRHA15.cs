﻿#region Update
/* 
    11/10/2019 [TKG] MMM
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

// export excel

using System.IO;
//using System.Net;
//using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmRHA15 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mMainCurCode = string.Empty,
            mRHAProratedSite = string.Empty;

        private string mBankCode = string.Empty;
        internal FrmRHA15Find FrmFind;
        
        #endregion

        #region Constructor

        public FrmRHA15(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Religius Holiday Allowance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnExcel.Visible = false;
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueCurCode(ref LueCurCode);
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Religion",

                        //6-10
                        "Join",
                        "Resign",
                        "Bank",
                        "Bank"+Environment.NewLine+"Account#",
                        "Value",
                        
                        //11-12
                        "Tax",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        80, 200, 150, 150, 150,  
                        
                        //6-10
                        100, 100, 150, 150, 120, 

                        //11-12
                        120, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteHolidayDt, LueSiteCode, 
                        LuePaymentType, LueBankAcCode, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    if(TxtDocNo.Text.Length > 0)
                    {
                        var SQL = new StringBuilder();
                        SQL.AppendLine("Select DocNo ");
                        SQL.AppendLine("From TblRHAHdr ");
                        SQL.AppendLine("Where DocNo = @Param ");
                        SQL.AppendLine("And Status <> 'C' ");
                        SQL.AppendLine("And CancelInd = 'N' Limit 1; ");

                        if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
                            BtnExcel.Visible = true;
                    }
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteHolidayDt, LuePaymentType, LueBankAcCode, LueSiteCode, 
                        MeeRemark 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    BtnExcel.Visible = false;
                    break;
            }
        }

        private void ClearData()
        {
            mBankCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, DteHolidayDt, 
                LueSiteCode, LuePaymentType, LueBankAcCode, TxtBankCode, LueCurCode, 
                TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 12 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        private void ClearGrd1()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRHA15Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RHA", "TblRHAHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRHAHdr(DocNo, VoucherRequestDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveRHADtl(DocNo, Row));
            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteHolidayDt, "Holiday date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtAmt, "Total amount", true) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsEmployeeNotValid();
        }

        private bool IsEmployeeNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsEmployeeNotValid(r)) return true;
                }
            }
            return false;
        }

        private bool IsEmployeeNotValid(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Holidaydt=@HolidayDt ");
            SQL.AppendLine("And A.Status In ('O', 'A') Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 1));

            return Sm.IsDataExist(cm,
                "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                "This employee already processed in another document."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Employee is empty.")) return true;
            }
            return false;
        }

        private string GenerateVoucherRequestDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty;

            type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveRHAHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRHAHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, HolidayDt, DeptCode, SiteCode, ");
            SQL.AppendLine("PaymentType, BankAcCode, BankCode, CSVInd, PayrunCode, CurCode, Amt, VoucherRequestDocNo, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', @HolidayDt, Null, @SiteCode, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @BankCode, 'N', Null, @CurCode, @Amt, @VoucherRequestDocNo, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRHADtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRHADtl(DocNo, EmpCode, Value, Value2, Value3, Value4, Tax, Amt, PayrunCode, CreateBy, CreateDt) " +
                    "Values (@DocNo, @EmpCode, @Value, 0.00, 0.00, 0.00, @Tax, @Amt, Null, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestPayrollDeptCode'), ");
            SQL.AppendLine("'13', 'C', @PaymentType, @BankAcCode, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy), ");
            SQL.AppendLine("0, @CurCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl ");
            SQL.AppendLine("(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Remark, @Amt, @CreateBy, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @RHADocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='RHA'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@RHADocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblRHAHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@RHADocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@RHADocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@RHADocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRLPHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessed();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblRHAHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private MySqlCommand CancelRLPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRHAHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRHAHdr(DocNo);
                ShowRHADtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRHAHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.HolidayDt, A.SiteCode, A.PaymentType, A.BankAcCode, A.BankCode, B.BankName, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.VoucherRequestDocNo, C.VoucherDocNo, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2  ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "HolidayDt", 
                    
                    //6-10
                    "SiteCode", "PaymentType", "BankAcCode", "BankCode", "BankName",
                    
                    //11-15
                    "CurCode", "Amt", "VoucherRequestDocNo", "VoucherDocNo", "Remark", 
                    
                    //16-17
                    "JournalDocNo", "JournalDocNo2"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     Sm.SetDte(DteHolidayDt, Sm.DrStr(dr, c[5]));
                     Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[8]));
                     mBankCode = Sm.DrStr(dr, c[9]);
                     TxtBankCode.EditValue = Sm.DrStr(dr, c[10]);
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[11]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[13]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[14]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                     TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[16]);
                     TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[17]);
                 }, true
             );
        }

        private void ShowRHADtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.OptDesc As Religious, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, F.BankName, B.BankAcNo, ");
            SQL.AppendLine("A.Value, A.Tax, A.Amt ");
            SQL.AppendLine("From TblRHADtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.Religion=E.OptCode And E.OptCat='Religion' ");
            SQL.AppendLine("Left Join TblBank F On B.BankCode=F.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "PosName", "DeptName", "Religious", "JoinDt",

                        //6-10
                        "ResignDt", "BankName", "BankAcNo", "Value", "Tax", 
                        
                        //11
                        "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='RHA' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status In ('A', 'C') ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            if (Grd2.Rows.Count > 0)
                Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mRHAProratedSite = Sm.GetParameter("RHAProratedSite");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            //mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
        }

        internal void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Amt += Sm.GetGrdDec(Grd1, r, 12);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "RHA" };

            var l = new List<RHA>();

            List<IList> myLists = new List<IList>();

            #region RHA
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, Date_Format(A.HolidayDt,'%d %M %Y')As HolidayDt, B.SiteName, ");
            SQL.AppendLine("C.OptDesc As PaymentType, A.CurCode, A.Amt, A.Remark ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                         "DocDt",
                         //6-10
                         "HolidayDt",
                         "SiteName",
                         "PaymentType",
                         "CurCode",
                         "Amt",
                         //11
                         "Remark",
                         

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RHA()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            HolidayDt = Sm.DrStr(dr, c[8]),
                            SiteName = Sm.DrStr(dr, c[9]),
                            PaymentType = Sm.DrStr(dr, c[10]),

                            CurCode = Sm.DrStr(dr, c[11]),
                            Amt = Sm.DrDec(dr, c[12]),
                            Remark = Sm.DrStr(dr, c[13]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[12])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[12])),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            Sm.PrintReport("RHA", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteHolidayDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd1();
        }

        
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
                ClearGrd1();
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
                mBankCode=string.Empty;
                TxtBankCode.EditValue = null;
                var BankAcCode = Sm.GetLue(LueBankAcCode);
                if (BankAcCode.Length>0)
                {
                    mBankCode = Sm.GetValue("Select BankCode From TblBankAccount Where BankCode Is Not Null And BankAcCode=@Param;", BankAcCode);
                    if (mBankCode.Length>0) TxtBankCode.EditValue = Sm.GetValue("Select BankName From TblBank Where BankCode=@Param;", mBankCode);
                }
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                        if (e.ColIndex == 0 &&
                            !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                            !Sm.IsLueEmpty(LueSiteCode, "Site"))
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmRHA15Dlg(
                                    this, 
                                    Sm.GetDte(DteHolidayDt), 
                                    Sm.GetLue(LueSiteCode)
                                    ));
                        }
                    
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (
                    e.ColIndex == 0 &&
                    !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                    !Sm.IsLueEmpty(LueSiteCode, "Site")
                    )
                    Sm.FormShowDialog(new FrmRHA15Dlg(
                        this,
                        Sm.GetDte(DteHolidayDt),
                        Sm.GetLue(LueSiteCode)
                        ));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Button Click Events

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            //string DocTitle = Sm.GetParameter("DocTitle");
            //string FileName = DocTitle + "_" + TxtDocNo.Text.Replace("/", "-") + ".csv";
            //var sf = new SaveFileDialog();
            //sf.Filter = "CSV files (*.csv)|*.csv";
            //sf.FileName = FileName;

            //if (sf.ShowDialog() == DialogResult.OK)
            //{
            //    StreamWriter sw = new StreamWriter(sf.FileName, false, Encoding.GetEncoding(1252));

            //    sw.WriteLine(TxtVoucherRequestDocNo.Text);

            //    if (Grd1.Rows.Count > 0)
            //    {
            //        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //        {
            //            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            //            { // 1 2 4 8 9 10 11 12
            //                sw.WriteLine
            //                (
            //                    string.Concat("'", Sm.GetGrdStr(Grd1, Row, 1)) + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 2).Replace(",", "") + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 4).Replace(",", "") + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 8).Replace(",", "") + "," +
            //                    string.Concat("'", Sm.GetGrdStr(Grd1, Row, 9)) + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 10) + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 11) + "," +
            //                    Sm.GetGrdStr(Grd1, Row, 12)
            //                );
            //            }
            //        }
            //    }
            //}

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 9].Value = "'" + Sm.GetGrdStr(Grd1, Row, 9);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 9].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 9), Sm.GetGrdStr(Grd1, Row, 9).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Class

        private class RHA
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }

            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string HolidayDt { get; set; }
            public string SiteName { get; set; }
            public string PaymentType { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
        }

        #endregion         
    }
}
