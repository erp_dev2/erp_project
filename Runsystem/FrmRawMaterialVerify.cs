﻿#region Update
/*
    10/07/2017 [TKG] saat menyimpan data ke stock, menggunakan lot terakhir yg ada di master bin.
    13/07/2017 [TKG] mempercepat proses penyimpanan terutama untuk journal.
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    13/06/2019 [TKG] mempercepat proses penyimpanan.
    07/06/2020 [TKG/IOK] mengubah proses save table2 terkait 
    13/05/2022 [DITA/IOK] tambah RAK 16-25 untuk verifikasi
    18/05/2022 [DITA/IOK] bug saat savestock untuk yg rak nya 16-25 tidak masuk stock
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRawMaterialVerify : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRawMaterialVerifyFind FrmFind;
        private string 
            mDocType = "51",
            mGlobalVendorRMP = string.Empty;
        private bool mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmRawMaterialVerify(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Verifikasi Penerimaan Dan Opname Bahan Baku";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter(); 
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "Dokumen"+Environment.NewLine+"Penerimaan",

                        //1-3
                        "",
                        "Jumlah",
                        "Print"
                    },
                     new int[] 
                    {
                        //0
                        150, 

                        //1-3
                        20, 100, 50
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 1);
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                         //0
                        "Dokumen"+Environment.NewLine+"Opname",

                        //1-3
                        "",
                        "Jumlah"
                    },
                     new int[] 
                    {
                        //0
                        150, 

                        //1-2
                        20, 100
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 1);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, TxtLegalDocVerifyDocNo, TxtQueueNo, 
                        TxtVdCode, MeeRemark
                    }, true);
                    BtnLegalDocVerify.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    BtnLegalDocVerify.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLegalDocVerifyDocNo, TxtQueueNo, TxtVdCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRawMaterialVerifyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            BtnLegalDocVerify_Click(sender, e);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        private void Process()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Docno, DocDt from tblrawmaterialverify where docdt='20150630' and docno not in (select docno from tblrawmaterialverifydtl) order by docdt, docno;");

            var cm = new MySqlCommand();

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-2
                        "DocDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Process2(Sm.DrStr(dr, c[0]), Sm.DrStr(dr, c[1]));
                    }, true
                );

            Cursor.Current = Cursors.Arrow;

            Sm.StdMsg(mMsgType.Info, "OK");
        }

        private void Process2(string DocNo, string DocDt)
        {
            //if (Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            Sm.SetDte(DteDocDt, DocDt);
            var RecvVdRawMaterialDocNo = GenerateRecvVdRawMaterialDocNo(DocNo);

            var ItCtRMPActual = Sm.GetParameter("ItCtRMPActual");
            var ItCtRMPActual2 = Sm.GetParameter("ItCtRMPActual2");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterialVerifyDtl(DocNo, RecvVdRawMaterialDocNo));

            cml.Add(SaveStock(DocNo, ItCtRMPActual, ItCtRMPActual2));

            Sm.ExecCommands(cml);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RawMaterialVerify", "TblRawMaterialVerify");
            var RecvVdRawMaterialDocNo = GenerateRecvVdRawMaterialDocNo(DocNo);

            var ItCtRMPActual = Sm.GetParameter("ItCtRMPActual");
            var ItCtRMPActual2 = Sm.GetParameter("ItCtRMPActual2");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterialVerify(DocNo));

            cml.Add(SaveRawMaterialVerifyDtl(DocNo, RecvVdRawMaterialDocNo));

            cml.Add(SaveStock(DocNo, ItCtRMPActual, ItCtRMPActual2));

            if (mIsAutoJournalActived) 
                cml.Add(SaveJournal(DocNo));

            cml.Add(DeleteStockMovementRawMaterialVerify(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ShowRecvRawMaterial();
            ShowRawMaterialOpname();

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtLegalDocVerifyDocNo, "Dokumen verifikasi", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty1() ||
                IsGrdEmpty2() ||
                IsLegalDocVerifyAlreadyProcessed() ||
                IsLegalDocVerifyDocNoNotValid() ||
                IsVendorInvalid();
        }

        private bool IsGrdEmpty1()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Tidak ada data penerimaan bahan baku.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Tidak ada data opname bahan baku.");
                return true;
            }
            return false;
        }

        private bool IsLegalDocVerifyDocNoNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLegalDocVerifyHdr ");
            SQL.AppendLine("Where CancelInd='N' And ProcessInd1='P' And ProcessInd2='P' ");
            SQL.AppendLine("And DocNo=@LegalDocVerifyDocNo ");
            SQL.AppendLine("And DocNo Not In ( ");

            #region Query 1

            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, ");
            SQL.AppendLine("        Case T3.SectionNo ");
            SQL.AppendLine("            When '1' Then T2.Shelf1 ");
            SQL.AppendLine("            When '2' Then T2.Shelf2 ");
            SQL.AppendLine("            When '3' Then T2.Shelf3 ");
            SQL.AppendLine("            When '4' Then T2.Shelf4 ");
            SQL.AppendLine("            When '5' Then T2.Shelf5 ");
            SQL.AppendLine("            When '6' Then T2.Shelf6 ");
            SQL.AppendLine("            When '7' Then T2.Shelf7 ");
            SQL.AppendLine("            When '8' Then T2.Shelf8 ");
            SQL.AppendLine("            When '9' Then T2.Shelf9 ");
            SQL.AppendLine("            When '10' Then T2.Shelf10 ");
            SQL.AppendLine("            When '11' Then T2.Shelf11 ");
            SQL.AppendLine("            When '12' Then T2.Shelf12 ");
            SQL.AppendLine("            When '13' Then T2.Shelf13 ");
            SQL.AppendLine("            When '14' Then T2.Shelf14 ");
            SQL.AppendLine("            When '15' Then T2.Shelf15 ");
            SQL.AppendLine("            When '16' Then T2.Shelf16 ");
            SQL.AppendLine("            When '17' Then T2.Shelf17 ");
            SQL.AppendLine("            When '18' Then T2.Shelf18 ");
            SQL.AppendLine("            When '19' Then T2.Shelf19 ");
            SQL.AppendLine("            When '20' Then T2.Shelf20 ");
            SQL.AppendLine("            When '21' Then T2.Shelf21 ");
            SQL.AppendLine("            When '22' Then T2.Shelf22 ");
            SQL.AppendLine("            When '23' Then T2.Shelf23 ");
            SQL.AppendLine("            When '24' Then T2.Shelf24 ");
            SQL.AppendLine("            When '25' Then T2.Shelf25 ");
            SQL.AppendLine("        End As Bin, ");
            SQL.AppendLine("        Sum(T3.Qty) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(Z3.Qty) Qty2 ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr Z1 ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr Z2 On Z1.DocNo=Z2.LegalDocVerifyDocNo And Z2.CancelInd='N' And Z2.DocType='1' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl Z3 On Z2.DocNo=Z3.DocNo ");
            SQL.AppendLine("            Where Z1.CancelInd='N' And Z1.ProcessInd1='P' And Z1.ProcessInd2='P' ");
            SQL.AppendLine("            And Z1.DocNo=T1.DocNo ");
            SQL.AppendLine("            And Bin=Z3.Shelf ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("        From TblLegalDocVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialHdr T2 On T1.DocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' And T2.DocType='1' ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialDtl2 T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.ProcessInd1='P' And T1.ProcessInd2='P' ");
            SQL.AppendLine("        Group By T1.DocNo, Bin ");
            SQL.AppendLine("        Having IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo From ( ");
            SQL.AppendLine("        Select T.DocNo, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("    From TblLegalDocVerifyHdr T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.ProcessInd1='P' And T.ProcessInd2='P' ");
            SQL.AppendLine("    ) X Where IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");

            #endregion

            SQL.AppendLine("    Union All ");

            #region Query 2

            SQL.AppendLine("        Select Distinct DocNo From ( ");
            SQL.AppendLine("            Select T1.DocNo ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select A.DocNo, ");
            SQL.AppendLine("                        Case C.SectionNo ");
            SQL.AppendLine("                            When '1' Then B.Shelf1 ");
            SQL.AppendLine("                            When '2' Then B.Shelf2 ");
            SQL.AppendLine("                            When '3' Then B.Shelf3 ");
            SQL.AppendLine("                            When '4' Then B.Shelf4 ");
            SQL.AppendLine("                            When '5' Then B.Shelf5 ");
            SQL.AppendLine("                            When '6' Then B.Shelf6 ");
            SQL.AppendLine("                            When '7' Then B.Shelf7 ");
            SQL.AppendLine("                            When '8' Then B.Shelf8 ");
            SQL.AppendLine("                            When '9' Then B.Shelf9 ");
            SQL.AppendLine("                            When '10' Then B.Shelf10 ");
            SQL.AppendLine("                            When '11' Then B.Shelf11 ");
            SQL.AppendLine("                            When '12' Then B.Shelf12 ");
            SQL.AppendLine("                            When '13' Then B.Shelf13 ");
            SQL.AppendLine("                            When '14' Then B.Shelf14 ");
            SQL.AppendLine("                            When '15' Then B.Shelf15 ");
            SQL.AppendLine("                            When '16' Then B.Shelf16 ");
            SQL.AppendLine("                            When '17' Then B.Shelf17 ");
            SQL.AppendLine("                            When '18' Then B.Shelf18 ");
            SQL.AppendLine("                            When '19' Then B.Shelf19 ");
            SQL.AppendLine("                            When '20' Then B.Shelf20 ");
            SQL.AppendLine("                            When '21' Then B.Shelf21 ");
            SQL.AppendLine("                            When '22' Then B.Shelf22 ");
            SQL.AppendLine("                            When '23' Then B.Shelf23 ");
            SQL.AppendLine("                            When '24' Then B.Shelf24 ");
            SQL.AppendLine("                            When '25' Then B.Shelf25 ");
            SQL.AppendLine("                    End As Bin, C.Qty ");
            SQL.AppendLine("                    From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("                            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                            And B.CancelInd='N' ");
            SQL.AppendLine("                            And B.DocType='2' ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                    Where A.CancelInd='N' ");
            SQL.AppendLine("                    And A.ProcessInd1='P' ");
            SQL.AppendLine("                    And A.ProcessInd2='P' ");
            SQL.AppendLine("                ) T Group By DocNo, Bin ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("                From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                    On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                    And B.CancelInd='N' ");
            SQL.AppendLine("                    And B.DocType='2' ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                Where A.CancelInd='N' ");
            SQL.AppendLine("                And A.ProcessInd1='P' ");
            SQL.AppendLine("                And A.ProcessInd2='P' ");
            SQL.AppendLine("            ) T Group By DocNo, Bin ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("            From TblParameter Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("        ) T3 On 0=0 ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("            IfNull(T2.Qty, 0)<(IfNull(T1.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("            Or IfNull(T2.Qty, 0)>(IfNull(T1.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo From ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                And B.CancelInd='N' ");
            SQL.AppendLine("                And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, ");
            SQL.AppendLine("                Case C.SectionNo ");
            SQL.AppendLine("                    When '1' Then B.Shelf1 ");
            SQL.AppendLine("                    When '2' Then B.Shelf2 ");
            SQL.AppendLine("                    When '3' Then B.Shelf3 ");
            SQL.AppendLine("                    When '4' Then B.Shelf4 ");
            SQL.AppendLine("                    When '5' Then B.Shelf5 ");
            SQL.AppendLine("                    When '6' Then B.Shelf6 ");
            SQL.AppendLine("                    When '7' Then B.Shelf7 ");
            SQL.AppendLine("                    When '8' Then B.Shelf8 ");
            SQL.AppendLine("                    When '9' Then B.Shelf9 ");
            SQL.AppendLine("                    When '10' Then B.Shelf10 ");
            SQL.AppendLine("                    When '11' Then B.Shelf11 ");
            SQL.AppendLine("                    When '12' Then B.Shelf12 ");
            SQL.AppendLine("                    When '13' Then B.Shelf13 ");
            SQL.AppendLine("                    When '14' Then B.Shelf14 ");
            SQL.AppendLine("                    When '15' Then B.Shelf15 ");
            SQL.AppendLine("                    When '16' Then B.Shelf16 ");
            SQL.AppendLine("                    When '17' Then B.Shelf17 ");
            SQL.AppendLine("                    When '18' Then B.Shelf18 ");
            SQL.AppendLine("                    When '19' Then B.Shelf19 ");
            SQL.AppendLine("                    When '20' Then B.Shelf20 ");
            SQL.AppendLine("                    When '21' Then B.Shelf21 ");
            SQL.AppendLine("                    When '22' Then B.Shelf22 ");
            SQL.AppendLine("                    When '23' Then B.Shelf23 ");
            SQL.AppendLine("                    When '24' Then B.Shelf24 ");
            SQL.AppendLine("                    When '25' Then B.Shelf25 ");
            SQL.AppendLine("                End As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("        From TblParameter ");
            SQL.AppendLine("        Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("    ) T3 On 0=0 ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("        IfNull(T1.Qty, 0)<(IfNull(T2.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        Or IfNull(T1.Qty, 0)>(IfNull(T2.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl ");

            #endregion

            SQL.AppendLine(") ");

            var cm = new MySqlCommand(){ CommandText=SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data ini tidak benar (Jumlah penerimaan dan opname tidak benar).");
                return true;
            }

            return false;
        }

        private bool IsLegalDocVerifyAlreadyProcessed()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblRawMaterialVerify " +
                    "Where CancelInd='N' And LegalDocVerifyDocNo=@LegalDocVerifyDocNo; "
            };
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data ini telah di-verifikasi.");
                return true;
            }

            return false;
        }

        private bool IsVendorInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where DocNo=@DocNo And VdCode=@VdCode;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", mGlobalVendorRMP);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid vendor.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRawMaterialVerify(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRawMaterialVerify(DocNo, DocDt, CancelInd, ProcessInd, LegalDocVerifyDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @LegalDocVerifyDocNo, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    ProcessInd1='F', ProcessInd2='F' ");
            SQL.AppendLine("Where CancelInd='N' And DocNo=@LegalDocVerifyDocNo; ");

            SQL.AppendLine("Update TblRecvRawMaterialHdr Set ");
            SQL.AppendLine("    ProcessInd='F' ");
            SQL.AppendLine("Where CancelInd='N' And LegalDocVerifyDocNo=@LegalDocVerifyDocNo; ");

            SQL.AppendLine("Update TblRawMaterialOpnameHdr Set ");
            SQL.AppendLine("    ProcessInd='F' ");
            SQL.AppendLine("Where CancelInd='N' And LegalDocVerifyDocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateRecvVdRawMaterialDocNo(string DocNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='RecvVdRawMaterial'");

            var SQL = new StringBuilder();

            //SQL.Append("Select Concat( ");
            //SQL.Append("IfNull(( ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblStockMovement ");
            //SQL.Append("       Where DocType='" + mDocType + "' And Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            //SQL.Append("       Order By Left(DocNo, 5) Desc Limit 1 ");
            //SQL.Append("       ) As Temp ");
            //SQL.Append("   ), '0000') ");
            //SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            //SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            //SQL.Append(") As DocNo;");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo, Char)), 4) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblRecvVdRawMaterial ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 5) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '0000') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo;");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveRawMaterialVerifyDtl(string DocNo, string RecvVdRawMaterialDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @RecvVdRawMaterialDocNo:=@RecvVdRawMaterialDocNoTemp; ");
            SQL.AppendLine("Set @DocNumber:=Left(@RecvVdRawMaterialDocNo, 4); ");
            SQL.AppendLine("Set @DNo:=0; ");

            SQL.AppendLine("Insert Into TblRawMaterialVerifyDtl ");
            SQL.AppendLine("(DocNo, DNo, RecvVdRawMaterialDocNo, WhsCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct @DocNo, Right(Concat('00', Cast((@DNo:=@DNo+1) As Char(3))), 3) as DNo, ");
            SQL.AppendLine("Concat( ");
            SQL.AppendLine("    Right(Concat('0000', Cast((@DocNumber:=@DocNumber+1) As Char(4))), 4), ");
            SQL.AppendLine("    Right(@RecvVdRawMaterialDocNo, Length(@RecvVdRawMaterialDocNo)-4) ");
            SQL.AppendLine(") As RecvVdRawMaterialDocNo, ");
            SQL.AppendLine("T.WhsCode, @CreateBy, CurrentDateTime() From ( ");
            SQL.AppendLine("    Select Distinct WhsCode From ( ");
            for (int Index = 1; Index <= 25; Index++)
            {
                if (Index > 1) SQL.AppendLine(" Union All ");
                SQL.AppendLine("Select Distinct B.WhsCode" + Index.ToString() + " As WhsCode ");
                SQL.AppendLine("From TblRawMaterialVerify A ");
                SQL.AppendLine("Inner Join TblRecvRawMaterialHdr B  ");
                SQL.AppendLine("    On A.LegalDocVerifyDocNo=B.LegalDocVerifyDocNo  ");
                SQL.AppendLine("    And B.CancelInd='N'  ");
                SQL.AppendLine("    And B.WhsCode" + Index.ToString() + " Is Not null ");
                SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            }
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine(") T; ");

            SQL.AppendLine("Insert Into TblRecvVdRawMaterial(DocNo) ");
            SQL.AppendLine("Select Distinct RecvVdRawMaterialDocNo From TblRawMaterialVerifyDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RecvVdRawMaterialDocNoTemp", RecvVdRawMaterialDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveStock(string DocNo, string ItCtRMPActual, string ItCtRMPActual2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @DNo:=0; ");

            SQL.AppendLine("Delete From TblStockMovementRawMaterialVerify Where RawMaterialVerifyDocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblStockMovementRawMaterialVerify ");
            SQL.AppendLine("(RawMaterialVerifyDocNo, DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @DocNo, @DocType, DocNo, DNo, DocDt, Concat(@DocType, '*', DocNo, '*', DNo) As Source, CancelInd, '', ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Sum(Qty), Sum(Qty2), Sum(Qty3), ");
            SQL.AppendLine("@DocNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select X.RecvVdRawMaterialDocNo As DocNo, ");
            SQL.AppendLine("    Right(Concat('00', Cast((@DNo:=@DNo+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("    X.DocDt, 'N' As CancelInd, '' As Source2, ");
            SQL.AppendLine("    X.WhsCode, IfNull(X2.Lot, '-') As Lot, X.Bin As Bin, ");
            SQL.AppendLine("    X.ItCode, Concat(X.QueueNo, '-', X.Bin) As BatchNo, ");
            SQL.AppendLine("    X.Qty, X.Qty2 As Qty2, X.Qty2 As Qty3 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select RecvVdRawMaterialDocNo, WhsCode, ItCode, QueueNo, DocDt, Bin, Sum(Qty) Qty, Sum(Qty2) Qty2 ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select B.RecvVdRawMaterialDocNo, B.WhsCode, D.ItCode, F.QueueNo, A.DocDt, ");
            SQL.AppendLine("            Case D.SectionNo ");
            SQL.AppendLine("                When '1' Then C.Shelf1 ");
            SQL.AppendLine("                When '2' Then C.Shelf2 ");
            SQL.AppendLine("                When '3' Then C.Shelf3 ");
            SQL.AppendLine("                When '4' Then C.Shelf4 ");
            SQL.AppendLine("                When '5' Then C.Shelf5 ");
            SQL.AppendLine("                When '6' Then C.Shelf6 ");
            SQL.AppendLine("                When '7' Then C.Shelf7 ");
            SQL.AppendLine("                When '8' Then C.Shelf8 ");
            SQL.AppendLine("                When '9' Then C.Shelf9 ");
            SQL.AppendLine("                When '10' Then C.Shelf10 ");
            SQL.AppendLine("                When '11' Then C.Shelf11 ");
            SQL.AppendLine("                When '12' Then C.Shelf12 ");
            SQL.AppendLine("                When '13' Then C.Shelf13 ");
            SQL.AppendLine("                When '14' Then C.Shelf14 ");
            SQL.AppendLine("                When '15' Then C.Shelf15 ");
            SQL.AppendLine("                When '16' Then C.Shelf16 ");
            SQL.AppendLine("                When '17' Then C.Shelf17 ");
            SQL.AppendLine("                When '18' Then C.Shelf18 ");
            SQL.AppendLine("                When '19' Then C.Shelf19 ");
            SQL.AppendLine("                When '20' Then C.Shelf20 ");
            SQL.AppendLine("                When '21' Then C.Shelf21 ");
            SQL.AppendLine("                When '22' Then C.Shelf22 ");
            SQL.AppendLine("                When '23' Then C.Shelf23 ");
            SQL.AppendLine("                When '24' Then C.Shelf24 ");
            SQL.AppendLine("                When '25' Then C.Shelf25 ");
            SQL.AppendLine("            End As Bin, ");
            SQL.AppendLine("            (D.Qty * ( ");
            SQL.AppendLine("                Case E.ItCtCode ");
            SQL.AppendLine("                    When @ItCtRMPActual Then Truncate(((0.25*3.1415926*E.Diameter*E.Diameter*E.Length)/1000000), 4) ");
            SQL.AppendLine("                    When @ItCtRMPActual2 Then Truncate(((E.Length*E.Width*E.Height)/1000000), 4) ");
            SQL.AppendLine("                Else 0 End ");
            SQL.AppendLine("                )) As Qty, D.Qty As Qty2 ");
            SQL.AppendLine("            From TblRawMaterialVerify A ");
            SQL.AppendLine("            Inner Join TblRawMaterialVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialHdr C ");
            SQL.AppendLine("                On A.LegalDocVerifyDocNo=C.LegalDocVerifyDocNo ");
            SQL.AppendLine("                And C.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 D ");
            SQL.AppendLine("                On C.DocNo=D.DocNo ");
            SQL.AppendLine("                And Case D.SectionNo ");
            SQL.AppendLine("                    When '1' Then C.WhsCode1 ");
            SQL.AppendLine("                    When '2' Then C.WhsCode2 ");
            SQL.AppendLine("                    When '3' Then C.WhsCode3 ");
            SQL.AppendLine("                    When '4' Then C.WhsCode4 ");
            SQL.AppendLine("                    When '5' Then C.WhsCode5 ");
            SQL.AppendLine("                    When '6' Then C.WhsCode6 ");
            SQL.AppendLine("                    When '7' Then C.WhsCode7 ");
            SQL.AppendLine("                    When '8' Then C.WhsCode8 ");
            SQL.AppendLine("                    When '9' Then C.WhsCode9 ");
            SQL.AppendLine("                    When '10' Then C.WhsCode10 ");
            SQL.AppendLine("                    When '11' Then C.WhsCode11 ");
            SQL.AppendLine("                    When '12' Then C.WhsCode12 ");
            SQL.AppendLine("                    When '13' Then C.WhsCode13 ");
            SQL.AppendLine("                    When '14' Then C.WhsCode14 ");
            SQL.AppendLine("                    When '15' Then C.WhsCode15 ");
            SQL.AppendLine("                    When '16' Then C.WhsCode16 ");
            SQL.AppendLine("                    When '17' Then C.WhsCode17 ");
            SQL.AppendLine("                    When '18' Then C.WhsCode18 ");
            SQL.AppendLine("                    When '19' Then C.WhsCode19 ");
            SQL.AppendLine("                    When '20' Then C.WhsCode20 ");
            SQL.AppendLine("                    When '21' Then C.WhsCode21 ");
            SQL.AppendLine("                    When '22' Then C.WhsCode22 ");
            SQL.AppendLine("                    When '23' Then C.WhsCode23 ");
            SQL.AppendLine("                    When '24' Then C.WhsCode24 ");
            SQL.AppendLine("                    When '25' Then C.WhsCode25 ");
            SQL.AppendLine("                    Else 'XXX' ");
            SQL.AppendLine("                    End=B.WhsCode ");
            SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("            Inner Join TblLegalDocVerifyHdr F On A.LegalDocVerifyDocNo=F.DocNo And F.CancelInd='N' ");
            SQL.AppendLine("            Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T Group By RecvVdRawMaterialDocNo, WhsCode, ItCode, QueueNo, DocDt, Bin ");
            SQL.AppendLine("    ) X ");
            SQL.AppendLine("    Left Join TblBin X2 On X.Bin=X2.Bin ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("Group By DocNo, DNo, DocDt, CancelInd, WhsCode, Lot, Bin, ItCode, BatchNo; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblStockMovementRawMaterialVerify Where RawMaterialVerifyDocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, ItCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select WhsCode, Lot, Bin, ItCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Null, CreateBy, CreateDt ");
            SQL.AppendLine("From TblStockMovementRawMaterialVerify T ");
            SQL.AppendLine("Where RawMaterialVerifyDocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice ");
            SQL.AppendLine("(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select Distinct Tbl1.ItCode, Tbl1.BatchNo, Tbl1.Source, Tbl2.CurCode, Tbl3.UPrice, ");
            SQL.AppendLine("Case When Tbl2.CurCode=Tbl4.ParValue Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=Tbl2.DocDt And CurCode1=Tbl2.CurCode And CurCode2=Tbl4.ParValue ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As ExcRate, ");
            SQL.AppendLine("Null As Remark, @UserCode As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.ItCode, A.BatchNo, A.Source, A.CreateBy, A.CreateDt, ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(T1.DocNo, T2.DNo) As UPriceKey ");
            SQL.AppendLine("        From TblQt2Hdr T1, TblQt2Dtl T2 ");
            SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.DocDt<=Left(E.CreateDt, 8) ");
            SQL.AppendLine("        And T2.PGCode=F.PGCode ");
            SQL.AppendLine("        Order By T1.DocDt Desc, T1.DocNo Desc ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) As UPriceKey ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select DocNo, ItCode, BatchNo, Source, CreateBy, CreateDt ");
            SQL.AppendLine("        From TblStockMovementRawMaterialVerify ");
            SQL.AppendLine("        Where RawMaterialVerifyDocNo=@DocNo ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    Inner Join TblRawMaterialVerifyDtl B On A.DocNo=B.RecvVdRawMaterialDocNo And B.DocNo=@DocNo ");
            SQL.AppendLine("    Inner Join TblRawMaterialVerify C On B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblLoadingQueue E On D.QueueNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblItem F On A.ItCode=F.ItCode ");
            SQL.AppendLine("    ");
            SQL.AppendLine("    ) Tbl1 ");
            SQL.AppendLine("    Inner Join TblQt2Hdr Tbl2 On Left(Tbl1.UPriceKey, Length(Tbl1.UPriceKey)-3)=Tbl2.DocNo ");
            SQL.AppendLine("    Inner Join TblQt2Dtl Tbl3 On Tbl2.DocNo=Tbl3.DocNo And Right(Tbl1.UPriceKey, 3)=Tbl3.DNo  ");
            SQL.AppendLine("    Left Join TblParameter Tbl4 On Tbl4.ParCode='MainCurCode'; ");

            //SQL.AppendLine("Delete From TblStockMovementRawMaterialVerify Where RawMaterialVerifyDocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual", ItCtRMPActual);
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", ItCtRMPActual2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblRawMaterialVerify Set ");
            SQL.AppendLine("    JournalDocNo= ");
            //@JournalDocNo ");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Raw Material Verification : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblRawMaterialVerify Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblStockMovementRawMaterialVerify A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocType=@DocType ");
            SQL.AppendLine("        And A.DocNo In ( ");
            SQL.AppendLine("            Select RecvVdRawMaterialDocNo ");
            SQL.AppendLine("            From TblRawMaterialVerifyDtl ");
            SQL.AppendLine("            Where DocNo=@DocNo ");
            SQL.AppendLine("        )");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(C.ParValue, D.VdCode) As AcNo, ");
            SQL.AppendLine("        0 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblStockMovementRawMaterialVerify A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='VendorAcNoUnInvoiceAP' And IfNull(C.ParValue, '')<>'' ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr D On D.DocNo In ( ");
            SQL.AppendLine("            Select LegalDocVerifyDocNo ");
            SQL.AppendLine("            From TblRawMaterialVerify ");
            SQL.AppendLine("            Where DocNo=@DocNo ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        Where A.DocType=@DocType ");
            SQL.AppendLine("        And A.DocNo In ( ");
            SQL.AppendLine("            Select RecvVdRawMaterialDocNo ");
            SQL.AppendLine("            From TblRawMaterialVerifyDtl ");
            SQL.AppendLine("            Where DocNo=@DocNo ");
            SQL.AppendLine("        )");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblRawMaterialVerify ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        private MySqlCommand DeleteStockMovementRawMaterialVerify(string DocNo)
        {
            var cm = new MySqlCommand() 
            {
                CommandText = "Delete From TblStockMovementRawMaterialVerify Where RawMaterialVerifyDocNo=@DocNo;" 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            var Current = Sm.ServerCurrentDateTime();
            var l = new List<string>();

            l = GetSource();
            cml.Add(EditRawMaterialVerify(Current, ref l));

            l.Clear();

            if (mIsAutoJournalActived) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsRawMaterialVerifyAlreadyProcessed()
                ;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblRawMaterialVerify " +
                    "Where CancelInd='Y' And DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data ini sudah dibatalkan.");
                return true;
            }

            return false;
        }

        private bool IsRawMaterialVerifyAlreadyProcessed()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPurchaseInvoiceRawMaterialHdr " +
                    "Where CancelInd='N' And RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo; "
            };
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data ini sudah diproses menjadi Purchase Invoice.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditRawMaterialVerify(string CurrentDateTime, ref List<string> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRawMaterialVerify Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
                SQL.AppendLine("    ProcessInd1= ");
                SQL.AppendLine("        Case When Exists( ");
                SQL.AppendLine("            Select 1 From TblRecvRawMaterialHdr ");
                SQL.AppendLine("            Where CancelInd='N' ");
                SQL.AppendLine("            And LegalDocVerifyDocNo=@LegalDocVerifyDocNo Limit 1 ");
                SQL.AppendLine("        ) Then 'P' Else 'O' End, ");
                SQL.AppendLine("    ProcessInd2= ");
                SQL.AppendLine("        Case When Exists( ");
                SQL.AppendLine("            Select 1 From TblRawMaterialOpnameHdr ");
                SQL.AppendLine("            Where CancelInd='N' ");
                SQL.AppendLine("            And LegalDocVerifyDocNo=@LegalDocVerifyDocNo Limit 1 ");
                SQL.AppendLine("        ) Then 'P' Else 'O' End, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo And CancelInd='N'; ");

                SQL.AppendLine("Update TblRecvRawMaterialHdr Set ");
                SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where LegalDocVerifyDocNo=@LegalDocVerifyDocNo And CancelInd='N'; ");

                SQL.AppendLine("Update TblRawMaterialOpnameHdr Set ");
                SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where LegalDocVerifyDocNo=@LegalDocVerifyDocNo And CancelInd='N'; ");

                SQL.AppendLine("Insert Into TblStockMovement ");
                SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
                SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, Qty, Qty2, Qty3, ");
                SQL.AppendLine("Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
                SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, -1*Qty, -1*Qty2, -1*Qty3, ");
                SQL.AppendLine("Remark, CreateBy, CreateDt ");
                SQL.AppendLine("From TblStockMovement T ");
                SQL.AppendLine("Where DocType=@DocType ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblRawMaterialVerifyDtl ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And RecvVdRawMaterialDocNo=T.DocNo ");
                SQL.AppendLine("); ");

                string Filter = string.Empty;
                for (int i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter+= "( Source=@Source"+i.ToString()+" )";

                    Sm.CmParam<String>(ref cm, "@Source"+i.ToString(), l[i]);
                }
                if (Filter.Length>0)
                {
                    SQL.AppendLine("Update TblStockSummary Set ");
                    SQL.AppendLine("    Qty=0, Qty2=0, Qty3=0, LastUpBy=@UserCode, LastUpDt=@CurrentDateTime ");
                    SQL.AppendLine("Where ("+ Filter + ");");
                }
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurrentDateTime", CurrentDateTime);
            

            return cm;
        }

        private List<string> GetSource()
        {
            var l = new List<string>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Source From TblStockMovement ");
            SQL.AppendLine("Where DocType=@DocType ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select RecvVdRawMaterialDocNo ");
            SQL.AppendLine("    From TblRawMaterialVerifyDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("); ");

            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "Source" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }

            return l.Distinct().ToList();
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine(Sm.GetNewJournalDocNo(CurrentDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo(DocDt, 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblRawMaterialVerify Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Verification Raw Material : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblRawMaterialVerify Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblRawMaterialVerify Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //if (IsClosingJournalUseCurrentDt)
            //    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            //else
            //    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRawMaterialVerify(DocNo);
                ShowRecvRawMaterial();
                ShowRawMaterialOpname();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRawMaterialVerify(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.LegalDocVerifyDocNo, C.VdName, B.QueueNo, A.Remark ");
            SQL.AppendLine("From TblRawMaterialVerify A ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "LegalDocVerifyDocNo", "VdName", "QueueNo",  

                        //6
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmRecvRawMaterial(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && !ChkCancelInd.Checked && 
                !BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
                Printdata(e.RowIndex);
                
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmRecvRawMaterial(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && !ChkCancelInd.Checked && 
                !BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
                Printdata(e.RowIndex);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mGlobalVendorRMP = Sm.GetParameter("GlobalVendorRMP");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
        }

        private void Printdata(int Row)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvRawMat>();
            var ldtl = new List<RecvRawMatDtl>();
            var ldtl2 = new List<RecvRawMatDtlEmployee>();

            string[] TableName = { "RecvRawMat", "RecvRawMatDtl", "RecvRawMatDtlEmployee" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.QueueNo, B.VehicleRegNo, A.LegalDocVerifyDocNo, C.VdName, ");
            SQL.AppendLine("E.TTName, A.UnloadStartDt, A.UnloadStartTm, A.UnloadEndDt, A.UnloadEndTm, A.Remark, ");
            SQL.AppendLine("F.UserName As CreateBy ");
            SQL.AppendLine("From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue D On B.QueueNo=D.DocNo ");
            SQL.AppendLine("Left Join TblTransportType E On D.TTCode=E.TTCode ");
            SQL.AppendLine("Left Join TblUser F On A.CreateBy = F.UserCode ");
            SQL.AppendLine("Where A.ProcessInd='F' And A.CancelInd='N' And A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 0));
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         
                         //6-10
                         "QueueNo", 
                         "VehicleRegNo", 
                         "LegalDocVerifyDocNo", 
                         "VdName", 
                         "TTName", 
                        
                         //11-15
                         "UnloadStartDt", 
                         "UnloadStartTm", 
                         "UnloadEndDt", 
                         "UnloadEndTm",                        
                         "Remark", 
                        
                         "CreateBy"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvRawMat()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            QueueNo = Sm.DrStr(dr, c[6]),
                            VehicleReqNo = Sm.DrStr(dr, c[7]),
                            LegalDocVerifyDocNo = Sm.DrStr(dr, c[8]),
                            VdName = Sm.DrStr(dr, c[9]),
                            TTName = Sm.DrStr(dr, c[10]),
                            UnloadStartDtTm = Sm.DrStr(dr, c[11]).Substring(6, 2) + "/" + Sm.DrStr(dr, c[11]).Substring(4, 2) + "/" + Sm.DrStr(dr, c[11]).Substring(0, 4) + " " + Sm.DrStr(dr, c[12]).Substring(0, 2) + ":" + Sm.DrStr(dr, c[12]).Substring(2, 2),
                            UnloadEndDtTm = Sm.DrStr(dr, c[13]).Substring(6, 2) + "/" + Sm.DrStr(dr, c[13]).Substring(4, 2) + "/" + Sm.DrStr(dr, c[13]).Substring(0, 4) + " " + Sm.DrStr(dr, c[14]).Substring(0, 2) + ":" + Sm.DrStr(dr, c[14]).Substring(2, 2),
                            Remark = Sm.DrStr(dr, c[15]),
                            CreateBy = Sm.DrStr(dr, c[16]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine(" Select A.SectionNo As ShelfNo, RTRIM(LTRIM(B.ItName)) As ItName, Concat(Round(B.Length, 0), ' ', B.LengthUOMCode) As Length, A.Diameter, A.Qty, B.PurchaseUomCode, ");
                SQLDtl.AppendLine(" Case  ");
                SQLDtl.AppendLine(" When A.SectionNo = 1 then RTRIM(LTRIM(C.Shelf1)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 2 then RTRIM(LTRIM(C.Shelf2)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 3 then RTRIM(LTRIM(C.Shelf3)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 4 then RTRIM(LTRIM(C.Shelf4)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 5 then RTRIM(LTRIM(C.Shelf5)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 6 then RTRIM(LTRIM(C.Shelf6)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 7 then RTRIM(LTRIM(C.Shelf7)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 8 then RTRIM(LTRIM(C.Shelf8)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 9 then RTRIM(LTRIM(C.Shelf9)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 10 then RTRIM(LTRIM(C.Shelf10)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 11 then RTRIM(LTRIM(C.Shelf11)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 12 then RTRIM(LTRIM(C.Shelf12)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 13 then RTRIM(LTRIM(C.Shelf13)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 14 then RTRIM(LTRIM(C.Shelf14)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 15 then RTRIM(LTRIM(C.Shelf15)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 16 then RTRIM(LTRIM(C.Shelf16)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 17 then RTRIM(LTRIM(C.Shelf17)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 18 then RTRIM(LTRIM(C.Shelf18)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 19 then RTRIM(LTRIM(C.Shelf19)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 20 then RTRIM(LTRIM(C.Shelf20)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 21 then RTRIM(LTRIM(C.Shelf21)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 22 then RTRIM(LTRIM(C.Shelf22)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 23 then RTRIM(LTRIM(C.Shelf23)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 24 then RTRIM(LTRIM(C.Shelf24)) ");
                SQLDtl.AppendLine(" When A.SectionNo = 25 then RTRIM(LTRIM(C.Shelf25)) ");
                SQLDtl.AppendLine(" end As ShelfName, ");
                SQLDtl.AppendLine(" (Select Count(ShelfNo) As Ttl from TblRecvRawMaterialDtl2 X Inner Join TblItem Y On X.ItCode=Y.ItCode Where X.DocNo=A.DocNo And Y.ItName=B.ItName) As TotalLine ");
                SQLDtl.AppendLine(" From TblRecvRawMaterialDtl2 A ");
                SQLDtl.AppendLine(" Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine(" Inner Join TblRecvRawMaterialHdr C On A.DocNo=C.DocNo And C.ProcessInd='F' And C.CancelInd='N' ");
                SQLDtl.AppendLine(" Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine(" Order By B.ItName, A.SectionNo, B.Length, A.Diameter ");
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", Sm.GetGrdStr(Grd1, Row, 0));
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ShelfNo" ,

                         //1-5
                         "ItName" ,
                         "Diameter",
                         "Qty",
                         "PurchaseUomCode",
                         "ShelfName",

                         //6
                         "TotalLine",
                         "Length"
                        });
                if (drDtl.HasRows)
                {
                    string strItemName = string.Empty;
                    string strShelfName = string.Empty;
                    int LineNo = 0;
                    int MyCol = 0;
                    int PrevCol = 0;
                    int CurrentRow = 0;
                    int FirstRow = 0;
                    while (drDtl.Read())
                    {
                        if (strItemName != Sm.DrStr(drDtl, cDtl[1]))
                        {
                            strItemName = Sm.DrStr(drDtl, cDtl[1]);

                            LineNo = 0;
                            PrevCol = 0;

                            strShelfName = Sm.DrStr(drDtl, cDtl[5]);

                            ldtl.Add(new RecvRawMatDtl()
                            {
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                OrderNumber = ldtl.Count,
                                Section1 = Sm.DrStr(drDtl, cDtl[5]),
                                Panjang1 = string.Empty,
                                Diameter1 = 0,
                                Qty1 = 0,
                                PurchaseUomCode1 = string.Empty,
                                Section2 = string.Empty,
                                Panjang2 = string.Empty,
                                Diameter2 = 0,
                                Qty2 = 0,
                                PurchaseUomCode2 = string.Empty,
                                Section3 = string.Empty,
                                Panjang3 = string.Empty,
                                Diameter3 = 0,
                                Qty3 = 0,
                                PurchaseUomCode3 = string.Empty
                            });
                            CurrentRow = ldtl.Count;
                            FirstRow = ldtl.Count;
                        }

                        MyCol = (Int32)(LineNo * 3 / Sm.DrDec(drDtl, cDtl[6]));
                        if (PrevCol != MyCol)
                        {
                            PrevCol = MyCol;
                            CurrentRow = FirstRow;

                            strShelfName = Sm.DrStr(drDtl, cDtl[5]);


                            if (MyCol == 1) ldtl[CurrentRow - 1].Section2 = Sm.DrStr(drDtl, cDtl[5]);
                            if (MyCol == 2) ldtl[CurrentRow - 1].Section3 = Sm.DrStr(drDtl, cDtl[5]);
                        }
                        else
                            if ((strItemName == Sm.DrStr(drDtl, cDtl[1])) && (strShelfName != Sm.DrStr(drDtl, cDtl[5])))
                            {
                                strShelfName = Sm.DrStr(drDtl, cDtl[5]);

                                ldtl.Add(new RecvRawMatDtl() { OrderNumber = ldtl.Count, ItName = Sm.DrStr(drDtl, cDtl[1]) });

                                if (MyCol == 0) ldtl[CurrentRow].Section1 = Sm.DrStr(drDtl, cDtl[5]);
                                if (MyCol == 1) ldtl[CurrentRow].Section2 = Sm.DrStr(drDtl, cDtl[5]);
                                if (MyCol == 2) ldtl[CurrentRow].Section3 = Sm.DrStr(drDtl, cDtl[5]);

                                CurrentRow++;
                            }

                        if (MyCol == 0)
                        {
                            ldtl.Add(new RecvRawMatDtl()
                            {
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                OrderNumber = ldtl.Count,
                                Section1 = string.Empty,
                                Panjang1 = Sm.DrStr(drDtl, cDtl[7]),
                                Diameter1 = Sm.DrDec(drDtl, cDtl[2]),
                                Qty1 = Sm.DrDec(drDtl, cDtl[3]),
                                PurchaseUomCode1 = Sm.DrStr(drDtl, cDtl[4]),
                                Section2 = string.Empty,
                                Panjang2 = string.Empty,
                                Diameter2 = 0,
                                Qty2 = 0,
                                PurchaseUomCode2 = string.Empty,
                                Section3 = string.Empty,
                                Panjang3 = string.Empty,
                                Diameter3 = 0,
                                Qty3 = 0,
                                PurchaseUomCode3 = string.Empty
                            });
                        };
                        if (MyCol == 1)
                        {
                            ldtl[CurrentRow].Section2 = string.Empty;
                            ldtl[CurrentRow].Panjang2 = Sm.DrStr(drDtl, cDtl[7]);
                            ldtl[CurrentRow].Diameter2 = Sm.DrDec(drDtl, cDtl[2]);
                            ldtl[CurrentRow].Qty2 = Sm.DrDec(drDtl, cDtl[3]);
                            ldtl[CurrentRow].PurchaseUomCode2 = Sm.DrStr(drDtl, cDtl[4]);

                        }

                        if (MyCol == 2)
                        {
                            ldtl[CurrentRow].Section3 = string.Empty;
                            ldtl[CurrentRow].Panjang3 = Sm.DrStr(drDtl, cDtl[7]);
                            ldtl[CurrentRow].Diameter3 = Sm.DrDec(drDtl, cDtl[2]);
                            ldtl[CurrentRow].Qty3 = Sm.DrDec(drDtl, cDtl[3]);
                            ldtl[CurrentRow].PurchaseUomCode3 = Sm.DrStr(drDtl, cDtl[4]);

                        }

                        for (int intX = 0; intX < ldtl.Count; intX++)
                        {
                            if (ldtl[intX].ItName == Sm.DrStr(drDtl, cDtl[1]))
                            {
                                if (ldtl[intX].Section1 == Sm.DrStr(drDtl, cDtl[5]))
                                    ldtl[intX].Total1 = ldtl[intX].Total1 + Sm.DrDec(drDtl, cDtl[3]);

                            }
                        }

                        for (int intX = 0; intX < ldtl.Count; intX++)
                        {
                            if (ldtl[intX].ItName == Sm.DrStr(drDtl, cDtl[1]))
                            {
                                if (ldtl[intX].Section2 == Sm.DrStr(drDtl, cDtl[5]))
                                {
                                    if (ldtl[intX].Total2 == 0)
                                    {
                                        for (int intY = 0; intY < ldtl.Count; intY++)
                                            if ((ldtl[intY].ItName == ldtl[intX].ItName) && (ldtl[intY].Section1 == ldtl[intX].Section2))
                                                ldtl[intX].Total2 = ldtl[intY].Total1;
                                        if (ldtl[intX].Total2 == 0)
                                            ldtl[intX].Total2 = ldtl[intX].Total2 + Sm.DrDec(drDtl, cDtl[3]);
                                    }
                                    else
                                        ldtl[intX].Total2 = ldtl[intX].Total2 + Sm.DrDec(drDtl, cDtl[3]);
                                }
                            }

                        }

                        for (int intX = 0; intX < ldtl.Count; intX++)
                        {
                            if (ldtl[intX].ItName == Sm.DrStr(drDtl, cDtl[1]))
                            {
                                if (ldtl[intX].Section3 == Sm.DrStr(drDtl, cDtl[5]))
                                {
                                    if (ldtl[intX].Total3 == 0)
                                    {
                                        for (int intY = 0; intY < ldtl.Count; intY++)
                                            if ((ldtl[intY].ItName == ldtl[intX].ItName) && (ldtl[intY].Section2 == ldtl[intX].Section3))
                                                ldtl[intX].Total3 = ldtl[intY].Total2;
                                        if (ldtl[intX].Total3 == 0)
                                            ldtl[intX].Total3 = ldtl[intX].Total3 + Sm.DrDec(drDtl, cDtl[3]);
                                    }
                                    else
                                        ldtl[intX].Total3 = ldtl[intX].Total3 + Sm.DrDec(drDtl, cDtl[3]);
                                }
                            }

                        }

                        CurrentRow++;
                        LineNo++;

                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Employee
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.EmpType, A.EmpCode, Concat(B.EmpName, '  ') As EmpName, ");
                SQLDtl2.AppendLine("Case When A.EmpType='1' then 'Grader' When A.EmpType='2' then 'Asisten Grader' ");
                SQLDtl2.AppendLine("When A.EmpType='3' then 'Bongkar Dalam' else 'Bongkar Luar' end As EmpTypeName ");
                SQLDtl2.AppendLine("From TblRecvRawMaterialDtl A ");
                SQLDtl2.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
                SQLDtl.AppendLine("Inner Join TblRecvRawMaterialHdr C On A.DocNo=C.DocNo And C.ProcessInd='F' And C.CancelInd='N' ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine("Order By B.EmpName");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", Sm.GetGrdStr(Grd1, Row, 0));
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "EmpType" ,

                         //1-5
                         "EmpCode" ,
                         "EmpName" ,
                         "EmpTypeName" ,
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new RecvRawMatDtlEmployee()
                        {
                            EmpType = Sm.DrStr(drDtl2, cDtl2[0]),
                            EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpName = Sm.DrStr(drDtl2, cDtl2[2]),
                            EmpTypeName = Sm.DrStr(drDtl2, cDtl2[3]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            Sm.PrintReport("RecvRawMaterial", myLists, TableName, false);
        }

        internal void ShowRecvRawMaterial()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select A.DocNo, Sum(B.Qty) Qty From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B " +
                "Where A.DocNo=B.DocNo And A.CancelInd='N' And A.LegalDocVerifyDocNo=@LegalDocVerifyDocNo Group By A.DocNo " +
                "Order By A.DocDt, A.DocNo;",
                new string[] { "DocNo", "Qty" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ShowRawMaterialOpname() 
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                "Select A.DocNo, Sum(B.Qty) Qty From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B " +
                "Where A.DocNo=B.DocNo And A.CancelInd='N' And A.LegalDocVerifyDocNo=@LegalDocVerifyDocNo Group By A.DocNo " +
                "Order By A.DocDt, A.DocNo;",
                new string[] { "DocNo", "Qty" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private string GetShelfName(
            string ShelfNo, string Shelf1, string Shelf2, string Shelf3, string Shelf4,
            string Shelf5, string Shelf6, string Shelf7, string Shelf8, string Shelf9, string Shelf10)
        {
            return
                (ShelfNo == "1" ? Shelf1 : (ShelfNo == "2" ? Shelf2 : (ShelfNo == "3" ? Shelf3 :
                (ShelfNo == "4" ? Shelf4 : (ShelfNo == "5" ? Shelf5 : (ShelfNo == "6" ? Shelf6 :
                (ShelfNo == "7" ? Shelf7 : (ShelfNo == "8" ? Shelf8 : (ShelfNo == "9" ? Shelf9 : Shelf10)))))))));
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnLegalDocVerify_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRawMaterialVerifyDlg(this));
        }

        private void BtnLegalDocVerify2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLegalDocVerifyDocNo, "Dokumen Legalitas", false))
            {
                try
                {
                    var f = new FrmLegalDocVerify(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtLegalDocVerifyDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRawMaterialOpname(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmRawMaterialOpname(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Report Class

        private class RecvRawMat
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string QueueNo { get; set; }
            public string VehicleReqNo { get; set; }
            public string LegalDocVerifyDocNo { get; set; }
            public string VdName { get; set; }
            public string TTName { get; set; }
            public string UnloadStartDtTm { get; set; }
            public string UnloadEndDtTm { get; set; }
            public string Remark { get; set; }
            public string Shelf1 { get; set; }
            public string Shelf2 { get; set; }
            public string Shelf3 { get; set; }
            public string Shelf4 { get; set; }
            public string Shelf5 { get; set; }
            public string Shelf6 { get; set; }
            public string Shelf7 { get; set; }
            public string Shelf8 { get; set; }
            public string Shelf9 { get; set; }
            public string Shelf10 { get; set; }
            public string Shelf11 { get; set; }
            public string Shelf12 { get; set; }
            public string Shelf13 { get; set; }
            public string Shelf14 { get; set; }
            public string Shelf15 { get; set; }
            public string CreateBy { get; set; }
            public string PrintBy { get; set; }
        }

        private class RecvRawMatDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal OrderNumber { get; set; }
            public string SectionTitle { get; set; }
            public string Section1 { get; set; }
            public decimal Diameter1 { get; set; }
            public string Panjang1 { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Total1 { get; set; }
            public string PurchaseUomCode1 { get; set; }
            public string Section2 { get; set; }
            public decimal Diameter2 { get; set; }
            public string Panjang2 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Total2 { get; set; }
            public string PurchaseUomCode2 { get; set; }
            public string Section3 { get; set; }
            public decimal Diameter3 { get; set; }
            public string Panjang3 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal Total3 { get; set; }
            public string PurchaseUomCode3 { get; set; }
        }

        private class RecvRawMatDtlEmployee
        {
            public string EmpType { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpTypeName { get; set; }
        }

        #endregion
    }
}
