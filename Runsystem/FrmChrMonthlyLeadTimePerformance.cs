﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmChrMonthlyLeadTimePerformance : RunSystem.FrmBase10
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            exportFileName = string.Empty,
            file = string.Empty;

        private decimal reduction = 0m;

        private bool
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = false;

        #endregion

        #region Constructor

        public FrmChrMonthlyLeadTimePerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnExcel.Visible = BtnPDF.Visible = BtnWord.Visible = false;
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = Sm.GetParameter("IsNoOfDaysPurchasedItemComparisonReportShowDoc") == "Y";
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            DateTime dt = Sm.ConvertDate(CurrentDateTime).AddDays(-365);

            string yearAgo = dt.Year.ToString() + ("00" + dt.Month.ToString()).Substring(("00" + dt.Month.ToString()).Length - 2, 2);
            
            Sl.SetLueMth(LueMth2);
            Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
            Sl.SetLueYr(LueYr2, "");
            Sm.SetLue(LueYr2, CurrentDateTime.Substring(0, 4));

            Sl.SetLueMth(LueMth1);
            Sm.SetLue(LueMth1, yearAgo.Substring(4, 2));
            Sl.SetLueYr(LueYr1, "");
            Sm.SetLue(LueYr1, yearAgo.Substring(0, 4));
            
            Sl.SetLueItCtCode(ref LueItCtCode);
            SetLueProcess(ref LueProcess);

            LueMth1.Properties.ReadOnly = LueYr1.Properties.ReadOnly = 
                TxtItCode.Properties.ReadOnly = TxtItName.Properties.ReadOnly =  true;

            reduction = Decimal.Parse(Sm.GetLue(LueYr2)) - Decimal.Parse(Sm.GetLue(LueYr1));

            base.FrmLoad(sender, e);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueMth1, "Start Month") ||
                Sm.IsLueEmpty(LueYr1, "Start Year") ||
                Sm.IsLueEmpty(LueMth2, "End Month") ||
                Sm.IsLueEmpty(LueYr2, "End Year") ||
                IsFilterByDateInvalid() ||
                IsFilterEmpty() ||
                IsBothFilterFilled() ||
                Sm.IsLueEmpty(LueProcess, "Process")
            ) return;
            
            try
            {
                ChartAppearance.ApplyChartStyles(this.Chart);
                LoadData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Info, Exc.Message);
            }
        }

        private void ProcessData(ref List<PerItem> l, string YrMth)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter1 = "  ";

            SQL.AppendLine("Select X.ItCode, X.ProcNo, X.Performance, Round(Sum(X.NoD)/Count(X.ItCode), 2) As NoD, DATE_FORMAT(X.TrxDt, '%M %Y') As TrxDt, X.ItName, X.ForeignName, X.ItCtCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select X1.ItCode, X1.ProcNo, X1.Performance, Sum(X1.NoD)/Count(X1.ItCode) As NoD, X1.TrxDt, Y.ItName, Y.ForeignName, Y.ItCtCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                SQL.AppendLine("Select T1.ItCode, '1' As ProcNo, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '2' As ProcNo, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '3' As ProcNo, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '4' As ProcNo, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

            }
            else
            {
                SQL.AppendLine("Select T1.ItCode, '1' As ProcNo, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '2' As ProcNo, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '3' As ProcNo, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, '4' As ProcNo, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD, T1.DocDt As TrxDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode, A.DocDt ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where(Left(A.DocDt, 6)  = @YrMth) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");
            }

            SQL.AppendLine(")X1  ");
            SQL.AppendLine("Inner Join TblItem Y On X1.ItCode = Y.ItCode ");

            Sm.CmParam<String>(ref cm, "@YrMth", YrMth);
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueItCtCode), "Y.ItCtCode", true);
            Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, "X1.ItCode", true);
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueProcess), "X1.ProcNo", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + Filter1 + " Group By X1.ItCode ) X Group By Left(X.TrxDt, 6); ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "TrxDt",

                    //1
                    "NoD"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PerItem()
                        {
                            TransactionDt = Sm.DrStr(dr, c[0]),
                            NoD = Sm.DrDec(dr, c[1])
                        });
                    }
                }

                dr.Close();
            }
        }

        private void LoadData()
        {
            var l = new List<PerItem>();
            l.Clear();

            var startMonth = Decimal.Parse(string.Concat(Sm.GetLue(LueYr1), Sm.GetLue(LueMth1)));
            string YrMth = string.Empty;

            for (int i = 0; i < 13; i++)
            {
                if (startMonth == Decimal.Parse(string.Concat(Sm.GetLue(LueYr1), "13")))
                {
                    startMonth = Decimal.Parse(string.Concat(Sm.GetLue(LueYr2), "01"));
                }
                YrMth = startMonth.ToString();

                ProcessData(ref l, YrMth);

                startMonth++;
            }

            BindChart(l);
        }

        private void BindChart(object dataChart)
        {
            this.Chart.Series.Clear();
            ChartSeries series = new ChartSeries("Per Item", ChartSeriesType.Line);
            //ChartSeries seriesAvg = new ChartSeries("All Item", ChartSeriesType.Line);
            ChartDataBindModel dataSeriesModel = new ChartDataBindModel(dataChart);
            //ChartDataBindModel dataSeriesModelAvg = new ChartDataBindModel(dataChartAvg);
            //ChartLegend legends = new ChartLegend(Chart);

            // If ChartDataBindModel.XName is empty or null, X value is index of point.
            dataSeriesModel.XName = "TransactionDt";
            dataSeriesModel.YNames = new string[] { "NoD" };
            series.Text = series.Name;
            series.Style.Border.Width = 3;

            //dataSeriesModelAvg.XName = "vs";
            //dataSeriesModelAvg.YNames = new string[] { "AvgPoint" };
            //seriesAvg.Text = seriesAvg.Name;

            //display point on top of series
            series.Style.DisplayText = true;
            series.Style.TextOrientation = ChartTextOrientation.UpRight;

            //seriesAvg.Style.DisplayText = true;
            //seriesAvg.Style.TextOrientation = ChartTextOrientation.Right;

            //series.SeriesModel = dataSeriesModel;
            series.SeriesIndexedModelImpl = dataSeriesModel;
            //seriesAvg.SeriesIndexedModelImpl = dataSeriesModelAvg;

            ChartDataBindAxisLabelModel dataLabelsModel = new ChartDataBindAxisLabelModel(dataChart);
            //ChartDataBindAxisLabelModel dataLabelsModelAvg = new ChartDataBindAxisLabelModel(dataChartAvg);
            dataLabelsModel.LabelName = "TransactionDt";
            //dataLabelsModelAvg.LabelName = "vs";
            Chart.Series.Add(series);
            //Chart.Series.Add(seriesAvg);
            //Chart.Legends.Add(legends);

            //Chart.Axes.Add(seriesAvg);

            Chart.PrimaryXAxis.LabelsImpl = dataLabelsModel;
            Chart.PrimaryXAxis.ValueType = ChartValueType.Custom;
            Chart.PrimaryXAxis.Title = "Month";
            Chart.PrimaryYAxis.Title = "Average Lead Time (Days)";
        }

        #endregion

        #region Additional Method

        private void SetLueProcess(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select '1' As Col1, 'POR v Recv' Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Recv v ETA' Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As Col1, 'MR v Recv' Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '4' As Col1, 'Usage v Recv' Col2 ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsFilterByDateInvalid()
        {
            var Yr1 = LueYr1.Text;
            var Yr2 = LueYr2.Text;

            if (Decimal.Parse(Yr1) > Decimal.Parse(Yr2))
            {
                Sm.StdMsg(mMsgType.Warning, "End year is earlier than start year.");
                return true;
            }
            return false;
        }

        private bool IsFilterNotValid()
        {
            return                
                    IsFilterEmpty() ||
                    IsBothFilterFilled()
                ;
        }

        private bool IsFilterEmpty()
        {
            if (TxtItCode.Text.Length == 0 && LueItCtCode.EditValue == null)
            {
                Sm.StdMsg(mMsgType.Info, "Data should be filtered either by Item or Category.");
                BtnItCode.Focus();
                return true;
            }
            
            return false;
        }

        private bool IsBothFilterFilled()
        {
            if (TxtItCode.Text.Length > 0 && !(LueItCtCode.EditValue == null ||
                LueItCtCode.Text.Trim().Length == 0 ||
                LueItCtCode.EditValue.ToString().Trim().Length == 0))
            {
                Sm.StdMsg(mMsgType.Info, "Only one filter allowed.");
                return true;
            }

            return false;
        }

        protected void OpenFile(string filetype, string exportFileName)
        {
            try
            {
                //if (filetype == "Grid")
                //    gridForm.ShowDialog();
                //else
                System.Diagnostics.Process.Start(exportFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #region Button Click

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pr = new PrintDialog();
            PrintPreviewDialog ppd = new PrintPreviewDialog();

            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.Info, "No Data");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty");
                else
                {
                    pr.AllowSomePages = true;
                    pr.AllowSelection = true;
                    pr.PrinterSettings.Clone();
                    pr.Document = Chart.PrintDocument;
                    if (pr.ShowDialog() == DialogResult.OK)
                        pr.Document.Print();
                }
            }

            //if (pr.ShowDialog() == DialogResult.OK)
            //{
            //    pr.Document = Chart.PrintDocument;
            //    ppd.Document = Chart.PrintDocument;
            //    if (ppd.ShowDialog() == DialogResult.OK)
            //        //pr.Document.Print();
            //        ppd.Document.Print();
            //}
        }

        private void BtnWord_Click(object sender, EventArgs e)
        {
            try
            {
                if(Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".doc";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + "_" + Sm.ConvertDate(Sm.GetDte(DteDocDt1)) + "_TO_" + Sm.ConvertDate(Sm.GetDte(DteDocDt2)) + ".gif";
                        //if (!System.IO.File.Exists(file))
                        Chart.SaveImage(file);

                        //Create a new document
                        WordDocument document = new WordDocument();
                        
                        //Adding a new section to the document.
                        IWSection section = document.AddSection();
                        //Adding a paragraph to the section
                        IWParagraph paragraph = section.AddParagraph();
                        //Writing text.
                        paragraph.AppendText("Top Destination Diagram");
                        //Adding a new paragraph		
                        paragraph = section.AddParagraph();
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        //Inserting chart.
                        paragraph.AppendPicture(Image.FromFile(file));
                        //Save the Document to disk.
                        document.Save(exportFileName, Syncfusion.DocIO.FormatType.Doc);
                        System.Diagnostics.Process.Start(exportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.NoData, "");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty.");
                else
                {
                    exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".xls";

                    // A new workbook with a worksheet should be created. 
                    SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
                    SyXL.IWorksheet sheet = chartBook.Worksheets[0];

                    //if chart is not empty
                    // Fill the worksheet with chart data. 
                    for (int i = 1; i <= Chart.Series[0].Points.Count; i++)
                    {
                        sheet.Range[i, 1].Number = Chart.Series[0].Points[i - 1].X;
                        sheet.Range[i, 2].Number = Chart.Series[0].Points[i - 1].YValues[0];
                    }

                    // Create a chart worksheet. 
                    SyXL.IChart chart = chartBook.Charts.Add(Chart.Title.Text);

                    // Specify the title of the Chart.
                    chart.ChartTitle = Chart.Title.Text;

                    // Initialize a new series instance and add it to the series collection of the chart. 
                    SyXL.IChartSerie series = chart.Series.Add("Top Destination");

                    // Specify the chart type of the series. 
                    series.SerieType = SyXL.ExcelChartType.Column_Clustered;

                    // Specify the name of the series. This will be displayed as the text of the legend. 
                    //series.Name = Chart.Name;

                    // Specify the value ranges for the series.
                    series.Values = sheet.Range["B1:B10"];

                    // Specify the Category labels for the series. 
                    series.CategoryLabels = sheet.Range["A1:A10"];

                    // Make the chart as active sheet. 
                    chart.Activate();

                    // Save the Chart book. 
                    chartBook.SaveAs(exportFileName); chartBook.Close();
                    SyXL.ExcelUtils.Close();

                    // Launches the file. 
                    System.Diagnostics.Process.Start(exportFileName);
                }
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".pdf";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //if (!System.IO.File.Exists(file))
                        this.Chart.SaveImage(file);

                        //Create a new PDF Document. The pdfDoc object represents the PDF document.
                        //This document has one page by default and additional pages have to be added.
                        PdfDocument pdfDoc = new PdfDocument();

                        pdfDoc.Pages.Add();

                        pdfDoc.Pages[0].Graphics.DrawImage(PdfImage.FromFile(file), new PointF(10, 30));

                        //Save the PDF Document to disk.
                        pdfDoc.Save(exportFileName);
                        OpenFile("Pdf", exportFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmChrMonthlyLeadTimePerformanceDlg(this));
        }

        private void BtnItCodeDelete_Click(object sender, EventArgs e)
        {
            if (TxtItCode.Text.Length > 0)
            {
                TxtItCode.Text = string.Empty;
                TxtItName.Text = string.Empty;
            }
            else
            {
                Sm.StdMsg(mMsgType.Info, "There is no item filtered.");
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
        }

        private void LueProcess_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcess, new Sm.RefreshLue1(SetLueProcess));
        }

        private void LueYr2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr2).Length > 0)
            {
                Sl.SetLueYr(LueYr1, (Decimal.Parse(Sm.GetLue(LueYr2)) - reduction).ToString());
                Sm.SetLue(LueYr1, (Decimal.Parse(Sm.GetLue(LueYr2)) - reduction).ToString());
            }
            else
            {
                Sl.SetLueYr(LueYr1, "");
                Sm.SetLue(LueYr1, string.Empty);
            }
        }

        private void LueMth2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueMth2).Length > 0)
            {
                Sl.SetLueMth(LueMth1);
                Sm.SetLue(LueMth1, Sm.GetLue(LueMth2));
            }
            else
            {
                Sl.SetLueMth(LueMth1);
                Sm.SetLue(LueMth1, string.Empty);
            }
        }

        #endregion

        #endregion

        #region Class

        private class PerItem
        {
            public string TransactionDt { get; set; }
            public decimal NoD { get; set; }
        }

        //private class AverageAllItem
        //{
        //    public string vs { get; set; }
        //    public decimal AvgPoint { get; set; }
        //}

        #endregion

    }
}
