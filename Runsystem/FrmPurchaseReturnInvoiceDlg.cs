﻿#region Update
/*
    01/08/2018 [WED] tambah Service Code untuk form utama nya
    12/10/2022 [RDA/PRODUCT] Purchase Return Invoice dapat menarik docno DO to Vendor dengan replace item ind aktif berdasarkan param DOtoVendorReplacedItem 
    25/10/2022 [RDA/PRODUCT] bug source receiving, PI, dan voucher tetap muncul ketika grid parent terisi (berdasar param PurchaseReturnInvoiceShowSource)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseReturnInvoiceDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseReturnInvoice mFrmParent;
        private string mVdCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseReturnInvoiceDlg(FrmPurchaseReturnInvoice FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 38;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo", 
                        "Date",
                        "Received#",

                        //6-10
                        "Received DNo",
                        "PO#",
                        "PO DNo",
                        "Item's Code",
                        "", 

                        //11-15
                        "Item's Name", 
                        "Quantity",
                        "UoM",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",

                        //16-20
                        "Unit"+Environment.NewLine+"Price",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Total",

                        //21-25
                        "Tax#"+Environment.NewLine+"(1)",
                        "Tax"+Environment.NewLine+"Date (1)",
                        "Tax Code"+Environment.NewLine+"(1)",
                        "Tax"+Environment.NewLine+"(1)",
                        "Tax Code"+Environment.NewLine+"2",

                        //26-30
                        "Tax"+Environment.NewLine+"(2)",
                        "Tax"+Environment.NewLine+"Code 3",
                        "Tax"+Environment.NewLine+"(3)",
                        "Site Code",
                        "Site Name",

                        //31-35
                        "Tax#"+Environment.NewLine+"(2)",
                        "Tax"+Environment.NewLine+"Date (2)",
                        "Tax#"+Environment.NewLine+"(3)",
                        "Tax"+Environment.NewLine+"Date (3)",
                        "Service1",

                        //36-37
                        "Service2",
                        "Service3"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 0, 80, 150, 
                        
                        //6-10
                        0, 150, 0, 80, 20,  
                        
                        //11-15
                        200, 100, 60, 180, 60, 

                        //16-20
                        120, 100, 130, 120, 130, 

                        //21-25
                        130, 100, 0, 150, 0,
 
                        //26-30
                        150, 0, 150, 0, 180,

                        //31-35
                        100, 80, 100, 80, 0, 

                        //36-37
                        0, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 10 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 16, 17, 18, 19, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 22, 32, 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7, 8, 9, 10, 14, 23, 25, 27, 29, 30, 35, 36, 37 }, false);
            if (mFrmParent.mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 30 }, true);
                Grd1.Cols[30].Move(9);
            }
            Grd1.Cols[31].Move(25);
            Grd1.Cols[32].Move(26);
            Grd1.Cols[33].Move(29);
            Grd1.Cols[34].Move(30);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 10, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.RecvVdDocNo, B.RecvVdDNo, D.PODocNo, D.PODNo, ");
            SQL.AppendLine("B.ItCode, K.ItName, B.Qty, K.InventoryUomCode, ");
            SQL.AppendLine("L.PtName, I.CurCode, ");
            SQL.AppendLine("((D.QtyPurchase/D.Qty)*J.UPrice) As UPrice, ");
            SQL.AppendLine("F.Discount As Discount, ");
            SQL.AppendLine("((1/D.Qty)*(D.QtyPurchase/F.Qty)*F.DiscountAmt) As DiscountAmt, ");
            SQL.AppendLine("((1/D.Qty)*(D.QtyPurchase/F.Qty)*F.RoundingValue) As RoundingValue, ");
            SQL.AppendLine("M.SiteCode, M.SiteName, ");
            SQL.AppendLine("O.TaxInvoiceNo, O.TaxInvoiceDt, ");
            SQL.AppendLine("O.TaxInvoiceNo2, O.TaxInvoiceDt2, ");
            SQL.AppendLine("O.TaxInvoiceNo3, O.TaxInvoiceDt3, ");
            SQL.AppendLine("O.TaxCode1, O.TaxCode2, O.TaxCode3, ");
            SQL.AppendLine("O.ServiceCode1, O.ServiceCode2, O.ServiceCode3, ");
            SQL.AppendLine("P.TaxName As TaxName1, Q.TaxName As TaxName2, R.TaxName As TaxName3 ");
            SQL.AppendLine("From TblDOVdHdr A ");
            SQL.AppendLine("Inner Join TblDOVdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And Not Exists(");
            SQL.AppendLine("        Select T1.DocNo ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T2.DOVdDocNo=B.DocNo ");
            SQL.AppendLine("        And T2.DOVdDNo=B.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblRecvVdHdr C On B.RecvVdDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblRecvVdDtl D On B.RecvVdDocNo=D.DocNo And B.RecvVdDNo=D.DNo ");
            SQL.AppendLine("Left Join TblPOHdr E On D.PODocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblPODtl F On D.PODocNo=F.DocNo And D.PODNo=F.DNo ");
            SQL.AppendLine("Left Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl H On G.MaterialRequestDocNo=H.DocNo And G.MaterialRequestDNo=H.DNo ");
            SQL.AppendLine("Left Join TblQtHdr I On G.QtDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblQtDtl J On G.QtDocNo=J.DocNo And G.QtDNo=J.DNo ");
            SQL.AppendLine("Left Join TblItem K On B.ItCode=K.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm L On I.PtCode=L.PtCode ");
            SQL.AppendLine("Left Join TblSite M On E.SiteCode=M.SiteCode ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceDtl N ");
            SQL.AppendLine("    On B.RecvVdDocNo=N.RecvVdDocNo ");
            SQL.AppendLine("    And B.RecvVdDNo=N.RecvVdDNo ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select DocNo ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And DocNo=N.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr O On N.DocNo=O.DocNo And O.CancelInd='N' ");
            SQL.AppendLine("Left Join TblTax P On O.TaxCode1=P.TaxCode ");
            SQL.AppendLine("Left Join TblTax Q On O.TaxCode2=Q.TaxCode ");
            SQL.AppendLine("Left Join TblTax R On O.TaxCode3=R.TaxCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode ");
            if(mFrmParent.mDOtoVendorReplacedItem == "1") SQL.AppendLine("And A.ReplacedItemInd='N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedDOVd() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "K.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DNo", "DocDt", "RecvVdDocNo", "RecvVdDNo", "PODocNo", 
                            
                            //6-10
                            "PODNo", "ItCode", "ItName", "Qty", "InventoryUomCode", 
                            
                            //11-15
                            "PtName", "CurCode", "UPrice", "Discount", "DiscountAmt", 
                            
                            //16-20
                            "RoundingValue", "TaxInvoiceNo", "TaxInvoiceDt", "TaxCode1", "TaxName1",
 
                            //21-25
                            "TaxCode2", "TaxName2", "TaxCode3", "TaxName3", "SiteCode", 
                            
                            //26-30
                            "SiteName", "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxInvoiceNo3", "TaxInvoiceDt3",

                            //31-33
                            "ServiceCode1", "ServiceCode2", "ServiceCode3"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Grd.Cells[Row, 20].Value =
                                Sm.GetGrdDec(Grd, Row, 12) *
                                (
                                    Sm.GetGrdDec(Grd, Row, 16) -
                                    (Sm.GetGrdDec(Grd, Row, 16)*Sm.GetGrdDec(Grd, Row, 17)*0.01m) -
                                    Sm.GetGrdDec(Grd, Row, 18) +
                                    Sm.GetGrdDec(Grd, Row, 19)
                                );
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 33);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 32);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 33);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 34);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 39, Grd1, Row2, 35);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 40, Grd1, Row2, 36);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 41, Grd1, Row2, 37);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 15, 19, 20, 21, 22, 23 });
                    }
                }
            }

            if (mFrmParent.mPurchaseReturnInvoiceShowSource && mFrmParent.Grd1.Rows.Count != 0)
            {
                // untuk param ini -> hanya 1 docno DOV yg dipergunakan untuk tracing source nya
                string DOVdDocNo = Sm.GetGrdStr(mFrmParent.Grd1, 0, 2);

                var cmi = new MySqlCommand();
                Sm.CmParam<String>(ref cmi, "@DOVdDocNo", DOVdDocNo);

                var SQLi = new StringBuilder();

                SQLi.AppendLine("SELECT DOVdDocNo, RecvVdDocNo, PIDocNo, GROUP_CONCAT(VoucherDocNo) VoucherDocNo  ");
                SQLi.AppendLine("FROM( 	 ");
                SQLi.AppendLine("	SELECT A.DocNo DOVdDocNo, C.DocNo RecvVdDocNo, D.DocNo PIDocNo, F.DocNo OPDocNo, G.DocNo VoucherDocNo  ");
                SQLi.AppendLine("	FROM tbldovdhdr A ");
                SQLi.AppendLine("	INNER JOIN TblDOVdDtl B ON A.DocNo=B.DocNo  ");
                SQLi.AppendLine("	LEFT JOIN tblrecvvddtl C ON B.RecvVdDocNo=C.DocNo AND B.RecvVdDNo=C.DNo  ");
                SQLi.AppendLine("	LEFT JOIN tblpurchaseinvoicedtl D ON C.DocNo=D.RecvVdDocNo AND C.DNo=D.RecvVdDNo  ");
                SQLi.AppendLine("	LEFT JOIN tbloutgoingpaymentdtl E ON D.DocNo=E.InvoiceDocNo  ");
                SQLi.AppendLine("	LEFT JOIN tbloutgoingpaymenthdr F ON E.DocNo=F.DocNo  ");
                SQLi.AppendLine("	LEFT JOIN tblvoucherhdr G ON F.VoucherRequestDocNo=G.VoucherRequestDocNo  ");
                SQLi.AppendLine("	WHERE A.DocNo=@DOVdDocNo ");
                SQLi.AppendLine("	GROUP BY C.DocNo, D.DocNo, E.DocNo, F.DocNo  ");
                SQLi.AppendLine(")X  ");
                SQLi.AppendLine("GROUP BY DOVdDocNo, RecvVdDocNo, PIDocNo  ");

                Sm.ShowDataInCtrl(
                        ref cmi, SQLi.ToString(),
                        new string[]
                        { 
                        //0
                        "DOVdDocNo",

                        //1-5
                        "RecvVdDocNo",
                        "PIDocNo",
                        "VoucherDocNo"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            mFrmParent.TxtRecvVd.EditValue = Sm.DrStr(dr, c[1]);
                            mFrmParent.TxtPurchaseInvoice.EditValue = Sm.DrStr(dr, c[2]);
                            mFrmParent.TxtVoucher.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );

            }

            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 DO to vendor document.");
            else
                mFrmParent.ComputeAmt();
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3);

            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 3),
                    key)) return true;
            return false;
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 9));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 9));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
