﻿#region Update
/*
    01/08/2018 [TKG] tambah filter entity berdasarkan group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmReturnAPDownpaymentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmReturnAPDownpayment mFrmParent;

        #endregion

        #region Constructor

        public FrmReturnAPDownpaymentDlg(FrmReturnAPDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor",
                        "Vendor",
                        "Entity",
                        "Entity",
                        "Currency",

                        //6
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        0, 200, 0, 200, 80, 
                        
                        //6
                        150
                    }
                );
            
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VdCode, B.VdName, A.EntCode, C.EntName, A.CurCode, ");
            SQL.AppendLine("A.Amt-IfNull(D.Amt, 0) As Amt ");
            SQL.AppendLine("From TblVendorDepositSummary A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode " + Filter);
            SQL.AppendLine("Left Join TblEntity C On A.EntCode=C.EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.VdCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.VdCode, IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.VdCode=D.VdCode ");
            SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(D.EntCode, '') ");
            SQL.AppendLine("    And A.CurCode=D.CurCode ");
            SQL.AppendLine("Where A.Amt-IfNull(D.Amt, 0)>0 ");
            if (mFrmParent.mIsFilterByEnt)
            {
                SQL.AppendLine("And (A.EntCode Is Null Or (A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=A.EntCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By B.VdName;");
            
            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtVdCode.Text, "B.VdName", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[] 
                        { 
                             //0
                             "VdCode", 
                             
                             //1-5
                             "VdName", 
                             "EntCode", 
                             "EntName", 
                             "CurCode", 
                             "Amt"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.mVdCode = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtVdCode.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.mEntCode = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtEntCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtSummary.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 6), 0);
                mFrmParent.TxtReturnAmt.EditValue = Sm.FormatNum(0m, 0);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
