﻿#region Update
/*
    26/08/2017 [TKG] SO yg tdicancel tidak muncul.
    13/04/2018 [TKG] filter by site
    07/08/2018 [HAR] Bisa ambil data dari SO contract untuk VIR
    27/08/2018 [TKG] tambah filter dan info customer's PO
    19/11/2019 [WED/IMS] amount inventory SO Contract belum terhitung
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpayment2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmARDownpayment2 mFrmParent;

        #endregion

        #region Constructor

        public FrmARDownpayment2Dlg(FrmARDownpayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CtCode, B.CtName, A.CurCode, ");
            SQL.AppendLine("    (A.Amt+A.AmtBOM) Amt, ");
            SQL.AppendLine("    D.PtName, G.ProjectCode, G.ProjectName, A.PONo, C.NoOfDownpayment ");
            SQL.AppendLine("    From TblSOContractHdr A ");
            SQL.AppendLine("    Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.DocNo, ");
            //SQL.AppendLine("        Sum(T2.Amt) As OtherAmt, ");
            SQL.AppendLine("        Count(1) As NoOfDownpayment ");
            SQL.AppendLine("        From TblSOContractHdr T1 ");
            SQL.AppendLine("        Inner Join TblARDownpayment T2 On T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("            And T2.CancelInd='N' ");
            SQL.AppendLine("            And T2.Status In ('O', 'A') ");
            SQL.AppendLine("            And T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("        Where T1.Amt+T1.AmtBOM>0.00 ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Group By T1.DocNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo ");
            SQL.AppendLine("    Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("    Inner Join TblBOQHdr E On A.BOQDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNO ");
            SQL.AppendLine("    Left Join TblProjectGroup G On F.PGCode=G.PGCode ");
            SQL.AppendLine("    Where A.Amt+A.AmtBOM>0.00 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "SO Contract#", 
                        "",
                        "Date",
                        "Customer's Code",
                        "Customer",
                        
                        //6-10
                        "Currency",
                        "Amount",
                        "Term of Payment",
                        "Project's Code",
                        "Project's Name",

                        //11-12
                        "Customer's PO#",
                        "Number of"+Environment.NewLine+"Downpayment"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtCtPONo.Text, "CtPONo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL() + Filter + " Order By DocDt, DocNo;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CtCode", "CtName", "CurCode", "Amt", 
                            
                            //6-10
                            "PtName", "ProjectCode", "ProjectName", "PONo", "NoOfDownpayment"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int r = Grd1.CurRow.Index;
                mFrmParent.TxtSODocNo.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.SetLueCtCode(ref mFrmParent.LueCtCode, Sm.GetGrdStr(Grd1, r, 4));
                mFrmParent.TxtSOCurCode.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                Sm.SetLue(mFrmParent.LueCurCode, Sm.GetGrdStr(Grd1, r, 6));
                mFrmParent.TxtSOAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 7), 0);
                mFrmParent.TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 12), 2);
                mFrmParent.TxtPtName.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                mFrmParent.ComputeOtherAmt();
                mFrmParent.ComputeAmtAftTax();
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtCtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer's PO#");
        }

        #endregion

        #region Grid Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion
    }
}
