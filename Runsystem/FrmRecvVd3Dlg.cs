﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvVd3 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvVd3Dlg(FrmRecvVd3 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -360);
                mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo", 
                        "Date",
                        "Item's"+Environment.NewLine+"Code", 

                        //6-10
                        "", 
                        "Item's Name", 
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "UoM"+Environment.NewLine+"(Purchase)",
                        "UoM"+Environment.NewLine+"(Inventory 1)",

                        //11-15
                        "UoM"+Environment.NewLine+"(Inventory 2)",
                        "UoM"+Environment.NewLine+"(Inventory 3)",
                        "PO's Remark",
                        "Department",
                        "ItScCode",

                        //16-20
                        "Sub-Category",
                        "Source",
                        "Local#",
                        "Foreign Name",
                        "Import"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 20 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            if (mFrmParent.mIsShowForeignName)
            {
                Grd1.Cols[19].Move(8);
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 16, 17 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 17 }, false);
                    Grd1.Cols[16].Move(9);
                }
            }
            else
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 16, 17, 19 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 17, 19 }, false);
                    Grd1.Cols[16].Move(9);
                }
            }
            Grd1.Cols[18].Move(3);
            Grd1.Cols[20].Move(6);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, false);

            Sm.SetGrdProperty(Grd1, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.ImportInd, A.DocNoSource, A.DocDt, G.DeptName, D.ItCode, E.ItName, E.ForeignName, ");
            SQL.AppendLine("    E.PurchaseUomCode, E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("    Trim(Concat(IfNull(A.Remark, ''), ' ', IfNull(B.Remark, ''))) As PORemark, ");
            SQL.AppendLine("    (B.Qty-IfNull(H.Qty2, 0)-IfNull(I.Qty3, 0)+IfNull(J.Qty4, 0)) As OutstandingQty, E.ItScCode, J.ItScName, A.LocalDocNo ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd<>'F' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr F On D.DocNo=F.DocNo And F.EximInd = 'Y' ");
            SQL.AppendLine("    Inner Join TblDepartment G On F.DeptCode=G.DeptCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) H On A.DocNo=H.DocNo And B.DNo=H.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) I On A.DocNo=I.DocNo And B.DNo=I.DNo  ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' And T4.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("        Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine("    ) J On A.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("    left Join TblItemSubcategory J On E.ItScCode = J.ItScCode");
            SQL.AppendLine("    Where A.VdCode=@VdCode ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine(")X Where X.OutstandingQty>0 ");

            mSQL = SQL.ToString();
        }


        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X.DocDt, X.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "ItCode", "ItName", "OutstandingQty", 
                            
                            //6-10
                            "PurchaseUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", "PORemark",
                            
                            //11-15
                            "DeptName", "ItScCode", "ItScName", "DocNoSource", "LocalDocNo",

                            //16-17
                            "ForeignName", "ImportInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 20);

                        mFrmParent.Grd1.Cells[Row1, 0].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 1].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 2].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 10].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 11].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 12].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 13].Value = null;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 10)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 8);
                        else
                            mFrmParent.Grd1.Cells[Row1, 16].Value = 0;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 11)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 8);
                        else
                            mFrmParent.Grd1.Cells[Row1, 18].Value = 0;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 12)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 8);
                        else
                            mFrmParent.Grd1.Cells[Row1, 20].Value = 0;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 10)))
                            Sm.ComputeQtyBasedOnConvertionFormula("12", mFrmParent.Grd1, Row1, 8, 16, 18, 20, 17, 19, 21);

                        mFrmParent.Grd1.Cells[Row1, 22].Value = null;
                        mFrmParent.ShowPOInfo(Row1);
                        mFrmParent.Grd1.Rows.Add();
                        int r = mFrmParent.Grd1.Rows.Count - 1;
                        mFrmParent.Grd1.Cells[r, 1].Value = false;
                        mFrmParent.Grd1.Cells[r, 2].Value = false;
                        mFrmParent.Grd1.Cells[r, 14].Value = 0;
                        mFrmParent.Grd1.Cells[r, 16].Value = 0;
                        mFrmParent.Grd1.Cells[r, 18].Value = 0;
                        mFrmParent.Grd1.Cells[r, 20].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
