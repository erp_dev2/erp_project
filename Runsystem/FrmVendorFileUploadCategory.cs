﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorFileUploadCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mStateInd = 0;
        internal bool mIsVendorFileUploadCtUseMandatoryIndicator = false;
        internal FrmVendorFileUploadCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmVendorFileUploadCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmVendorUploadFileCategory");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetFormControl(mState.View);
            if (!mIsVendorFileUploadCtUseMandatoryIndicator)
                ChkMandatoryInd.Visible = false;
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCategoryCode, TxtCategoryName, ChkActInd, ChkMandatoryInd
                    }, true);
                    TxtCategoryCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCategoryCode, TxtCategoryName, ChkMandatoryInd
                    }, false);
                    TxtCategoryCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtCategoryCode, true);
                    if (Sm.IsDataExist("Select CategoryCode From TblVendorFileUploadCategory Where CategoryCode = @Param And ActInd = 'Y'; ", TxtCategoryCode.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCategoryName, ChkActInd, ChkMandatoryInd }, false);
                    TxtCategoryName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateInd = 0;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCategoryCode, TxtCategoryName
            });
            ChkActInd.Checked = false;
            ChkMandatoryInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorFileUploadCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateInd = 1;
            ChkActInd.Checked = true;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCategoryCode, "", false)) return;

            if (Sm.IsDataExist("Select CategoryCode From TblVendorFileUploadCategory Where CategoryCode = @Param And ActInd = 'Y'; ", TxtCategoryCode.Text))
            {
                mStateInd = 2;
                SetFormControl(mState.Edit);
            }
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtCategoryCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    var cm = new MySqlCommand() { CommandText = "Delete From TblVendorFileUploadCategory Where CategoryCode=@CategoryCode" };
            //    Sm.CmParam<String>(ref cm, "@CategoryCode", TxtCategoryCode.Text);
            //    Sm.ExecCommand(cm);

            //    BtnCancelClick(sender, e);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            InsertData();
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CategoryCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CategoryCode", CategoryCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select CategoryCode, CategoryName, ActInd, MandatoryInd From TblVendorFileUploadCategory Where CategoryCode=@CategoryCode",
                        new string[] 
                        {
                            "CategoryCode", "CategoryName", "ActInd", "MandatoryInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCategoryCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCategoryName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            ChkMandatoryInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCategoryCode, "Category code", false) ||
                Sm.IsTxtEmpty(TxtCategoryName, "Category name", false) ||
                (mStateInd == 1 && IsCategoryCodeExisted()) ||
                (mStateInd == 2 && IsDataAlreadyInactive());
        }

        private bool IsDataAlreadyInactive()
        {
            if (Sm.IsDataExist("Select CategoryCode From TblVendorFileUploadCategory Where CategoryCode = @Param And ActInd = 'N'; ", TxtCategoryCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                TxtCategoryCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsCategoryCodeExisted()
        {
            if (!TxtCategoryCode.Properties.ReadOnly && Sm.IsDataExist("Select CategoryCode From TblVendorFileUploadCategory Where CategoryCode='" + TxtCategoryCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Category code ( " + TxtCategoryCode.Text + " ) is exists.");
                return true;
            }
            return false;
        }

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                if (mStateInd == 1)
                {
                    SQL.AppendLine("Insert Into TblVendorFileUploadCategory(CategoryCode, CategoryName, ActInd, MandatoryInd, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@CategoryCode, @CategoryName, @ActInd, @MandatoryInd, @UserCode2, CurrentDateTime()); ");
                }
                else if (mStateInd == 2)
                {
                    SQL.AppendLine("Update TblVendorFileUploadCategory Set ");
                    SQL.AppendLine("    CategoryName = @CategoryName, ActInd = @ActInd,  MandatoryInd = @MandatoryInd, LastUpBy = @UserCode2, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("Where CategoryCode = @CategoryCode And ActInd = 'Y'; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@CategoryCode", TxtCategoryCode.Text);
                Sm.CmParam<String>(ref cm, "@CategoryName", TxtCategoryName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@MandatoryInd", (ChkMandatoryInd.Checked) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtCategoryCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsVendorFileUploadCtUseMandatoryIndicator = Sm.GetParameterBoo("IsVendorFileUploadCtUseMandatoryIndicator");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsVendorFileUploadCtUseMandatoryIndicator' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsVendorFileUploadCtUseMandatoryIndicator": mIsVendorFileUploadCtUseMandatoryIndicator = ParValue == "Y"; break;

                            //string
                        }
                    }
                }
                dr.Close();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCategoryCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCategoryCode);
        }

        private void TxtCategoryName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCategoryName);
        }

        #endregion

        #endregion
    }
}
