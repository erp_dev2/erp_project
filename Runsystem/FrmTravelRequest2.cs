﻿#region Update
/*
    03/10/2018 [TKG] Travel Request berdasarkan status jabatan + Travel Request bisa mengakomodir untuk input currency lain selain IDR
    30/10/2018 [HAR] printout travel request luar negri   
    17/12/2018 [WED] LueCity diganti MeeCity
    22/01/2019 [MEY] menambah Tab Approval
    11/02/2019 [TKG] approval menggunakan validasi approval group
    15/03/2019 [MEY] error query saat save data
    18/03/2019 [MEY] error saat print data
    21/03/2019 [WED] bug print out SPPD KIM, format nya masih pakai yg punya HIN
    16/04/2019 [WED] bug : kalau cancel dokumen, walaupun create nya dari desktop (WebInd = 'D'), tetep bisa cancel
    07/05/2019 [DITA] Ubah nama jabatan di print out travel request berdasarkan parameter
    18/06/2019 [DITA] Ubah nama jabatan di print out Printout Travel Management diganti labelnya untuk kantor pusat berdasarkan parameter
    11/11/2019 [WED/SRN] TblDocumentApprovalGroupHdr --> TblDocApprovalGroupHdr; TblDocumentApprovalGroupDtl --> TblDocApprovalGroupDtl
    23/07/2020 [WED/SIER] Employee yg position status nya inactive, tidak bisa di proses berdasarkan parameter IsPositionStatusUseActInd
    27/10/2020 [WED/PHT] ada informasi arrival status, berdasarkan parameter IsTravelRequestUseArrivalStatus
    11/12/2020 [ICA/PHT] Menambah Printout SPPD PHT based on parameter TravelRequestPrintoutFormat
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    28/01/2021 [ICA/PHT] Duration di printout +1
    03/02/2021 [HAR/PHT] point [WED] LueCity diganti MeeCity : city ess (berupa kode city) waktu dibuka di desktop diubah ke cityname
    15/02/2021 [DITA/PHT] berdasarkan parameter : TravelRequestWebFormat, PIC hanya bisa diinsert melalui desktop
    14/02/2021 [HAR/PHT] parameter baru : TravelAllowMultipleEmployee, TravelRequestDeptApprovalType
    30/08/2021 [TKG/ALL] ubah validasi saat cancel data andaikata sudah dibuat voucher, tidak bisa dicancel lagi.
    28/07/2022 [IBL/SIER] memunculkan tombol print ketika dipanggil dari docapproval & menu lain
    09/09/2022 [VIN/TWC] Saat Edit TVR insert ke Approval (sebelumnya insert via ess) 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTravelRequest2 : RunSystem.FrmBase1
    {
        #region Field, Property

        private string mMainCurCode = string.Empty;
        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mWebInd = string.Empty, 
            mDocTitle = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;
        iGCell fCell;
        bool fAccept;
        internal FrmTravelRequest2Find FrmFind;
        private bool 
            Detasering = false, 
            mIsApprovalByDept = false,
            mIsTravelRequestRateEnabled = false,
            mIsPositionStatusUseActInd = false,
            mIsTravelRequestUseArrivalStatus = false,
            mIsTravelRequestAllowMultipleEmployee = false,
            mIsVoucherDocSeqNoEnabled
            ;
        private string
          mPosNameTravelRequestForHO = string.Empty,
          mPosNameTravelRequestForNonHO = string.Empty,
          mSignNameTravelRequestForHO = string.Empty,
          mSignNameTravelRequestForNonHO = string.Empty,
          mTravelRequestPrintoutFormat = string.Empty,
          mTravelRequestWebFormat = string.Empty,
          mTravelRequestDeptApprovalType = string.Empty
          ;

        #endregion

        #region Constructor

        public FrmTravelRequest2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsTravelRequestUseArrivalStatus)
                {
                    tabControl1.TabPages.Remove(TpgArrival);
                }
                SetGrd();
                SetFormControl(mState.View);
                SetLueTransportTravel(ref LueTransport);
                Sl.SetLueRegionCode(ref LueRegionCode);
                LueAllowance.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if(mDocTitle != "SIER") BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mWebInd = "D";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsApprovalByDept = IsApprovalByDept();
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsTravelRequestRateEnabled = Sm.GetParameterBoo("IsTravelRequestRateEnabled");
            mPosNameTravelRequestForHO = Sm.GetParameter("PosNameTravelRequestForHO");
            mPosNameTravelRequestForNonHO = Sm.GetParameter("PosNameTravelRequestForNonHO");
            mSignNameTravelRequestForHO = Sm.GetParameter("SignNameTravelRequestForHO");
            mSignNameTravelRequestForNonHO = Sm.GetParameter("SignNameTravelRequestForNonHO");
            mIsPositionStatusUseActInd = Sm.GetParameterBoo("IsPositionStatusUseActInd");
            mIsTravelRequestUseArrivalStatus = Sm.GetParameterBoo("IsTravelRequestUseArrivalStatus");
            mTravelRequestPrintoutFormat = Sm.GetParameter("TravelRequestPrintoutFormat");
            mTravelRequestWebFormat = Sm.GetParameter("TravelRequestWebFormat");
            if (mTravelRequestPrintoutFormat.Length == 0) mTravelRequestPrintoutFormat = "1";
            if (mTravelRequestWebFormat.Length == 0) mTravelRequestWebFormat = "1";
            mIsTravelRequestAllowMultipleEmployee = Sm.GetParameterBoo("IsTravelRequestAllowMultipleEmployee");
            mTravelRequestDeptApprovalType = Sm.GetParameter("TravelRequestDeptApprovalType");
            mIsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "DNo",
                        "Code", 
                        "Employee's Name",
                        "Meal",
                        "Daily Allowance",

                        //6-10
                        "City Transport", 
                        "Transport",
                        "Accomodation", 
                        "Other Allowance",
                        "Detasering",

                        //11-13
                        "Total",
                        "Voucher Request#",
                        "Currency"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        0, 100, 200, 150, 150, 
                        
                        //6-10
                        150,  150, 150, 150, 150, 
                        //11-13
                        150, 150, 60
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 12 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd1.Cols[13].Move(11);

            #endregion
        
            #region Grid 2

            Grd2.Header.Rows.Count = 2;
            Grd2.Cols.Count = 20;
            Grd2.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd2, 0, 0, "Dno", 2, 0);
            SetGrdHdr(ref Grd2, 0, 1, "", 2, 0);
            SetGrdHdr(ref Grd2, 0, 2, "Code", 2, 100);
            SetGrdHdr(ref Grd2, 0, 3, "Employee's Name", 2, 200);
            SetGrdHdr2(ref Grd2, 1, 4, "Breakfast", 5);
            SetGrdHdr2(ref Grd2, 1, 9, "Lunch", 5);
            SetGrdHdr2(ref Grd2, 1, 14, "Dinner", 5);
            SetGrdHdr(ref Grd2, 0, 19, "Total", 2, 120);

            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 2, 4, 5, 9, 10, 14, 15 }, false);
            Sm.GrdFormatDec(Grd2, new int[] { 7, 8, 12, 13, 17, 18, 19 }, 0);
            if (mIsTravelRequestRateEnabled)
                Sm.GrdColReadOnly(true, true, Grd2, new int[] 
                { 
                    0, 
                    1, 2, 3, 4, 5, 6, 9, 10, 
                    11, 14, 15, 16, 19 
                });
            else
                Sm.GrdColReadOnly(true, true, Grd2, new int[] 
                { 
                    0, 
                    1, 2, 3, 4, 5, 6, 7, 9, 10, 
                    11, 12, 14, 15, 16, 17, 19 
                });
            Sm.SetGrdProperty(Grd2, false);

            #endregion
       
            #region Grid 3

            Grd3.Cols.Count = 10;
            Grd3.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5

                        "",
                        "Code", 
                        "Employee's Name",
                        "Position Status",
                        "AD",

                        //6-9
                        "Currency",
                        "Rate",
                        "Quantity",
                        "Total Allowance"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        0, 100, 200, 0, 0,
  
                        //6-9
                        60, 150, 100, 150
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 2, 4, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 });
            if (mIsTravelRequestRateEnabled)
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 9 });
            else
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 });

            #endregion
         
            #region Grid 4

            Grd4.Cols.Count = 11;
            Grd4.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Code", 
                        "Employee's Name",
                        "Position Status",
                        "AD",

                        //6-10
                        "Currency",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        0, 100, 200, 0, 0,  

                        //6-10
                        60, 150, 100, 150, 200
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColButton(Grd4, new int[] { 1 });
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 2, 4, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 9 });
            
            #endregion
         
            #region Grid 5

            Grd5.Cols.Count = 12;
            Grd5.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Code", 
                        "Employee's Name",
                        "Office",
                        "Position Status",

                        //6-10
                        "AD",
                        "Currency",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        0, 100, 200, 50, 0,  
                        
                        //6-10
                        0, 60, 150, 100, 150, 

                        //11
                        200
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 1 });
            Sm.GrdColCheck(Grd5, new int[] { 4 });
            Sm.GrdFormatDec(Grd5, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1, 2, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 5, 6, 7, 10 });
            
            #endregion
         
            #region Grid 6

            Grd6.Cols.Count = 12;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Code", 
                        "Employee's Name",
                        "Office",
                        "Position Status",

                        //6-10
                        "AD",
                        "Currency",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        0, 100, 200, 50, 0,  
                        
                        //6-10
                        0, 80, 150, 100, 150, 
                        
                        //11
                        200
                    }
                );
            Sm.GrdFormatDec(Grd6, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColCheck(Grd6, new int[] { 4 });
            Sm.GrdColButton(Grd6, new int[] { 1 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 2, 5, 6 }, false);
            if (mIsTravelRequestRateEnabled)
                Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 5, 6, 7, 10 });
            else
                Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 10 });

            #endregion

            #region Grid 7

            Grd7.Cols.Count = 12;
            Grd7.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Code", 
                        "Employee's Name",
                        "AD Code",
                        "Allowance",

                        //6-10
                        "Position Status",
                        "Currency",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        0, 100, 200, 0, 200,  
                        
                        //6-10
                        0, 60, 130, 100, 130,  
                        
                        //11
                        200
                    }
                );
            Sm.GrdColButton(Grd7, new int[] { 1 });
            Sm.GrdFormatDec(Grd7, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd7, new int[] { 0, 1, 2, 4, 6 }, false);
            if (mIsTravelRequestRateEnabled)
                Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 6, 7, 10 });
            else
                Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 10 });
            
            #endregion

            #region Grid 8

            Grd8.Cols.Count = 4;
            Grd8.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark",
                    },
                    new int[] 
                    {
                        200, 
                        100, 100, 500
                    }
                );
            Sm.GrdFormatDate(Grd8, new int[] { 2 });
            
            #endregion
         }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 120;

            SetGrdHdr(ref Grd, 0, col, "Position Status", 1, 0);
            SetGrdHdr(ref Grd, 0, col + 1, "AD", 1, 0);
            SetGrdHdr(ref Grd, 0, col + 2, "Currency", 1, 60);
            SetGrdHdr(ref Grd, 0, col + 3, "Rate", 1, 100);
            SetGrdHdr(ref Grd, 0, col + 4, "Quantity", 1, 80);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd7, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, MeeCityCode,
                        DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService, TxtPICCode, TxtPICName,
                        LueSite, MeeResult, LueTransport, LueSiteCode2, MeeRemark, 
                        LueRegionCode
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7, 8, 12, 13, 17, 18 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 8, 13, 18 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(true, true, Grd3, new int[] { 7, 8 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd3, new int[] { 8 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 4, 8, 9, 11 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(true, true, Grd6, new int[] { 4, 8, 9, 11 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd6, new int[] { 4, 9, 11 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(true, true, Grd7, new int[] { 5, 8, 9, 11 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd7, new int[] { 5, 9, 11 });
                    Sm.GrdColReadOnly(true, true, Grd8, new int[] { });
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt,  MeeCityCode,
                      DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService,
                      LueSite, MeeResult, LueTransport, LueSiteCode2, MeeRemark,
                      LueRegionCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7, 8, 12, 13, 17, 18 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 8, 13, 18 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 7, 8 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 8 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 7, 8 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 4, 8, 9, 11 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 8, 9, 11 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 9, 11 });
                    if (mIsTravelRequestRateEnabled)
                        Sm.GrdColReadOnly(false, true, Grd7, new int[] { 5, 8, 9, 11 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd7, new int[] { 5, 9, 11 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      MeeCancelReason
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (mWebInd == "W" && TxtStatus.Text != "Cancelled")
                    {
                        if (mTravelRequestWebFormat == "2")
                            BtnPIC.Enabled = true;
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                        if (mIsTravelRequestRateEnabled)
                            Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7, 8, 12, 13, 17, 18 });
                        else
                            Sm.GrdColReadOnly(false, true, Grd2, new int[] { 8, 13, 18 });
                        if (mIsTravelRequestRateEnabled)
                            Sm.GrdColReadOnly(false, true, Grd3, new int[] { 7, 8 });
                        else
                            Sm.GrdColReadOnly(false, true, Grd3, new int[] { 8 });
                        Sm.GrdColReadOnly(false, true, Grd4, new int[] { 7, 8 });
                        Sm.GrdColReadOnly(false, true, Grd5, new int[] { 4, 8, 9, 11 });
                        if (mIsTravelRequestRateEnabled)
                            Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 8, 9, 11 });
                        else
                            Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 9, 11 });
                        if (mIsTravelRequestRateEnabled)
                            Sm.GrdColReadOnly(false, true, Grd7, new int[] { 5, 8, 9, 11 });
                        else
                            Sm.GrdColReadOnly(false, true, Grd7, new int[] { 5, 9, 11 });
                   //     Sm.GrdColReadOnly(true, true, Grd8, new int[] {});
                    }
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, MeeCityCode, TxtStatus,
                DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService, 
                TxtPICCode, TxtPICName, LueSite, MeeResult, LueTransport,  
                LueRegionCode, MeeRemark, LueAllowance
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 8, 12, 13, 17, 18, 19 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 7, 8, 9 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 7, 8, 9 });
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 8, 9, 10 });
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 8, 9, 10 });
            Sm.ClearGrd(Grd7, true);
            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 8, 9, 10 });
            Sm.ClearGrd(Grd8, true);
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTravelRequest2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSite, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueSiteCode2(ref LueSiteCode2, string.Empty);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    {
                        if(mTravelRequestPrintoutFormat == "1") ParPrint(i + 1);
                        ParPrint3(Sm.GetGrdStr(Grd1, i, 1));
                    }
                }
                if(mTravelRequestPrintoutFormat == "1")ParPrint2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            IsDetaseringActive();

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TravelRequest", "TblTravelRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTravelRequestHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl7(DocNo, Row));
            
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl(DocNo, Row));
            
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl2(DocNo, Row));
            
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl3(DocNo, Row));
            
            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl4(DocNo, Row));
            
            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl5(DocNo, Row));
            
            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl6(DocNo, Row));
            
            Sm.ExecCommands(cml);
            InsertDataVR(DocNo);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                IsDateNotValid()||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                Sm.IsLueEmpty(LueSite, "Site") ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC", false) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty() ||
                IsGrdEmpty2() ||
                IsCurCodeInvalid() ||
                IsPositionStatusInactive()
                ;
        }

        private bool IsPositionStatusInactive()
        {
            if (!mIsPositionStatusUseActInd) return false;

            // for header
            if (Sm.IsDataExist("Select 1 From TblPositionStatus Where PositionStatusCode In (Select PositionStatusCode From TblEmployee Where EmpCode = @Param And PositionStatusCode Is not null) And ActInd = 'N';", TxtPICCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "His/her position status is inactive.");
                TxtPICName.Focus();
                return true;
            }

            // for detail
            var SQL = new StringBuilder();
            string mEmpCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mEmpCode.Length > 0) mEmpCode += ",";
                    mEmpCode += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            if (mEmpCode.Length > 0)
            {
                SQL.AppendLine("Select Group_Concat(Concat(A.EmpCode, ' - ', A.EmpName) Separator '\r\n') EmpCode ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Inner join TblPositionStatus B On A.PositionStatusCode = B.PositionStatusCode ");
                SQL.AppendLine("    And B.ActInd = 'N' ");
                SQL.AppendLine("    And A.PositionStatusCode Is Not Null ");
                SQL.AppendLine("    And Find_In_Set(A.EmpCode, @Param); ");

                string mIsDataExists = Sm.GetValue(SQL.ToString(), mEmpCode);

                if (mIsDataExists.Length > 0)
                {
                    var ms = new StringBuilder();

                    ms.AppendLine("This/these employee's position status is/are inactive.");
                    ms.AppendLine(mIsDataExists);

                    Sm.StdMsg(mMsgType.Warning, ms.ToString());
                    return true;
                }
            }

            return false;
        }

        private bool IsCurCodeInvalid()
        {
            string EmpCode = string.Empty, CurCode = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                if (EmpCode.Length > 0)
                {
                    CurCode = string.Empty;

                    for (int i = 0; i < Grd2.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, i, 2)))
                        {
                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd2, i, 6);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, i, 6)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : Meal (Breakfast)." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd2, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }

                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd2, i, 11);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, i, 11)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : Meal (Lunch)." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd2, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }

                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd2, i, 16);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, i, 16)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : Meal (Dinner)." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd2, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd2, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Grd3.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd3, i, 2)))
                        {
                            if (CurCode.Length == 0) 
                                CurCode = Sm.GetGrdStr(Grd3, i, 6);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd3, i, 6)))
                                {
                                    Sm.StdMsg(mMsgType.Warning, 
                                        "Allowance : Daily Allowance." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd3, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd3, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Grd4.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd4, i, 2)))
                        {
                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd4, i, 6);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd4, i, 6)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : City Transport." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd4, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd4, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Grd5.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd5, i, 2)))
                        {
                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd5, i, 7);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd5, i, 7)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : Transport." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd5, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd5, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Grd6.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd6, i, 2)))
                        {
                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd6, i, 7);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd6, i, 7)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : Accomodation." + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd6, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd6, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Grd7.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd7, i, 2)) && Sm.GetGrdStr(Grd7, i, 5).Length>0)
                        {
                            if (CurCode.Length == 0)
                                CurCode = Sm.GetGrdStr(Grd7, i, 7);
                            else
                            {
                                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd7, i, 7)))
                                {
                                    Sm.StdMsg(mMsgType.Warning,
                                        "Allowance : " + Sm.GetGrdStr(Grd7, i, 5) + Environment.NewLine +
                                        "Employee's Code : " + Sm.GetGrdStr(Grd7, i, 2) + Environment.NewLine +
                                        "Employee's Name : " + Sm.GetGrdStr(Grd7, i, 3) + Environment.NewLine + Environment.NewLine +
                                        "One travel request document can only use 1 currency.");
                                    return true;
                                }
                            }
                        }
                    }
                }
                if (CurCode.Length > 0)
                    Grd1.Cells[r, 13].Value = CurCode;
                else
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Employee's Code : " + Sm.GetGrdStr(Grd7, r, 2) + Environment.NewLine +
                        "Employee's Name : " + Sm.GetGrdStr(Grd7, r, 3) + Environment.NewLine + Environment.NewLine +
                        "Currency is empty.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDetaseringActive()
        {
            //decimal dt1 = Decimal.Parse(Sm.GetDte(DteEndDt).Substring(0, 8));
            //decimal dt2 = Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8));
            double Rangedt = (DteEndDt.DateTime - DteStartDt.DateTime).TotalDays + 1;
            if (Rangedt >= 21)
            {
                Sm.StdMsg(mMsgType.Warning, "Travel request exceed 21 days, detasering was active.");
                Detasering = true;
                UpdateDetaseringColumn();
                ComputeSummaryAllowance();
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            decimal dt1 = Decimal.Parse(Sm.GetDte(DteEndDt).Substring(0, 8));
            decimal dt2 = Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8));

            if (dt2>dt1)
            {
                Sm.StdMsg(mMsgType.Warning, "date not valid.");
                return true;
            }
            return false;
        }

        private void UpdateDetaseringColumn()
        {
            string salaryStr =  string.Empty; 
            decimal SalaryDec = 0;
            string empCode = string.Empty;

            //menginap dalam unit kali 50%
            if (Detasering && Sm.GetLue(LueSiteCode2).Length > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.CompareStr(mMainCurCode, Sm.GetGrdStr(Grd1, Row, 13)))
                    {
                        empCode = Sm.GetGrdStr(Grd1, Row, 2);
                        salaryStr = Sm.GetValue("Select C.Amt "+
                           "from TblEmpsalaryHdr A "+
                           "Inner Join  "+
                           "( "+
                           "    Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  "+
                           "    From TblEmpSalaryHdr "+
                           "    Where EmpCode = '"+empCode+"' "+
                           "    Group BY EmpCode "+
                           ")B On Concat(A.DocDt, A.DocNo) = B.KeyCode  "+
                           "Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo "+
                           "Inner Join "+
                           "( "+
                           "    Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl "+
                           "    Group BY DocNo "+
                           ")D on C.DocNo = D.DocNo And C.Dno = D.Dno "+
                           "Where A.Status = 'A' And  A.EmpCode = '"+empCode+"' ");
                        if(salaryStr.Length>0)
                            SalaryDec = Decimal.Parse(salaryStr);
                        Grd1.Cells[Row, 10].Value = (SalaryDec * 50 /100);
                    }
                }
            }

            //menginap diluar unit 75%
            if (Detasering && Sm.GetLue(LueSiteCode2).Length == 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.CompareStr(mMainCurCode, Sm.GetGrdStr(Grd1, Row, 13)))
                    {
                        empCode = Sm.GetGrdStr(Grd1, Row, 2);
                        salaryStr = Sm.GetValue("Select C.Amt " +
                         "from TblEmpsalaryHdr A " +
                         "Inner Join  " +
                         "( " +
                         "    Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  " +
                         "    From TblEmpSalaryHdr " +
                         "    Where EmpCode = '" + empCode + "'" +
                         "    Group BY EmpCode " +
                         ")B On Concat(A.DocDt, A.DocNo) = B.KeyCode  " +
                         "Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo " +
                         "Inner Join " +
                         "( " +
                         "    Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl " +
                         "    Group BY DocNo " +
                         ")D on C.DocNo = D.DocNo And C.Dno = D.Dno " +
                         "Where A.Status = 'A' And A.EmpCode = '" + empCode + "' ");
                        if (salaryStr.Length > 0)
                            SalaryDec = Decimal.Parse(salaryStr);
                        Grd1.Cells[Row, 10].Value = (SalaryDec * 75 / 100);
                    }
                }
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (!mIsTravelRequestAllowMultipleEmployee)
            {
                if (Grd1.Rows.Count > 2)
                {
                    Sm.StdMsg(mMsgType.Warning, "You can't input more than one employee");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveTravelRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, WebInd, CancelInd, ");
            SQL.AppendLine("CancelReason, CityCode, StartDt, EndDt, StartTm, ");
            SQL.AppendLine("EndTm, TravelService, PICCode, SiteCode, Transportation, ");
            SQL.AppendLine("Result, SiteCode2, RegionCode, Remark, CreateBy, ");
            SQL.AppendLine("CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O', 'D', 'N', ");
            SQL.AppendLine("@CancelReason, @CityCode, @StartDt, @EndDt, @StartTm, ");
            SQL.AppendLine("@EndTm, @TravelService, @PICCode, @SiteCode, @Transportation, ");
            SQL.AppendLine("@Result, @SiteCode2, @RegionCode, @Remark, @UserCode, ");
            SQL.AppendLine("CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='TravelRequest' ");
            if (mIsApprovalByDept)
            {
                SQL.AppendLine("And T.DeptCode Is Not Null ");
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=@EmpCode ");
                SQL.AppendLine("    And DeptCode Is Not Null ");
                SQL.AppendLine("    ) ");
            }

            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='TravelRequest' And DAGCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@EmpCode ");
                SQL.AppendLine("))) ");
            }

            SQL.AppendLine(";");

            SQL.AppendLine("Update TblTravelRequestHdr Set ");
            SQL.AppendLine("    Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(  ");
            SQL.AppendLine("    Select 1 From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='TravelRequest' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine(");  ");

            if (mIsTravelRequestUseArrivalStatus)
            {
                SQL.AppendLine("Update TblTravelRequestHdr Set ArrivalStatus = 'O' Where DocNo = @DocNo; ");

                SQL.AppendLine("Update TblTravelRequestHdr Set ArrivalStatus = 'A' ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And SiteCode2 Is Null; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", MeeCityCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStart));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEnd));
            Sm.CmParam<String>(ref cm, "@TravelService", MeeTravelService.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            if (mTravelRequestDeptApprovalType.Length > 0)
            {
                if (mTravelRequestDeptApprovalType == "1")
                    Sm.CmParam<String>(ref cm, "@EmpCode", TxtPICCode.Text);
                else
                    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, 0, 2));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtPICCode.Text);
            }
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSite));
            Sm.CmParam<String>(ref cm, "@Transportation", Sm.GetLue(LueTransport));
            Sm.CmParam<String>(ref cm, "@Result", MeeResult.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode2", Sm.GetLue(LueSiteCode2));
            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, ");
            SQL.AppendLine("PositionStatusCode, ADCode, CurCode, Rate, Qty, ");
            SQL.AppendLine("PositionStatusCode2, ADCode2, CurCode2, Rate2, Qty2, ");
            SQL.AppendLine("PositionStatusCode3, ADCode3, CurCode3, Rate3, Qty3, ");
            SQL.AppendLine("Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @EmpCode, ");
            SQL.AppendLine("@PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, ");
            SQL.AppendLine("@PositionStatusCode2, @ADCode2, @CurCode2, @Rate2, @Qty2, ");
            SQL.AppendLine("@PositionStatusCode3, @ADCode3, @CurCode3, @Rate3, @Qty3, ");
            SQL.AppendLine("@Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode2", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@ADCode2", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetGrdStr(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Rate2", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode3", Sm.GetGrdStr(Grd2, Row, 14));
            Sm.CmParam<String>(ref cm, "@ADCode3", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CurCode3", Sm.GetGrdStr(Grd2, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Rate3", Sm.GetGrdDec(Grd2, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 19));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl2 ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, PositionStatusCode, ADCode, CurCode, Rate, Qty, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @EmpCode, @PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, @Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl3 ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, PositionStatusCode, ADCode, CurCode, Rate, Qty, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd4, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd4, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd4, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl4 ");
            SQL.AppendLine("(DocNo, DNo, OfficeInd, EmpCode, PositionStatusCode, ADCode, CurCode, Rate, Qty, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @OfficeInd, @EmpCode, @PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@OfficeInd", Sm.GetGrdBool(Grd5, Row, 4) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd5, Row, 5));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd5, Row, 6));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd5, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd5, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd5, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd5, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl5(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl5 ");
            SQL.AppendLine("(DocNo, DNo, OfficeInd, EmpCode, PositionStatusCode, ADCode, CurCode, Rate, Qty, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @OfficeInd, @EmpCode, @PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@OfficeInd", Sm.GetGrdBool(Grd6, Row, 4) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd6, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd6, Row, 5));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd6, Row, 6));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd6, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl6(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl6 ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, PositionStatusCode, ADCode, CurCode, Rate, Qty, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @PositionStatusCode, @ADCode, @CurCode, @Rate, @Qty, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetGrdStr(Grd7, Row, 6));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd7, Row, 4));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd7, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd7, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd7, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd7, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd7, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl7(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            if (Detasering)
            {
                SQL.AppendLine("Insert Into TblTravelRequestDtl7 ");
                SQL.AppendLine("(DocNo, DNo, EmpCode, CurCode, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Detasering, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @CurCode, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, @Detasering, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("    On Duplicate Key Update ");
                SQL.AppendLine("        EmpCode=@EmpCode, ");
                SQL.AppendLine("        CurCode=@CurCode, ");
                SQL.AppendLine("        Amt1=@Amt1, ");
                SQL.AppendLine("        Amt2=@Amt2, ");
                SQL.AppendLine("        Amt3=@Amt3, ");
                SQL.AppendLine("        Amt4=@Amt4, ");
                SQL.AppendLine("        Amt5=@Amt5, ");
                SQL.AppendLine("        Amt6=@Amt6, ");
                SQL.AppendLine("        Detasering=@Detasering ");
                SQL.AppendLine("; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblTravelRequestDtl7 ");
                SQL.AppendLine("(DocNo, DNo, EmpCode, CurCode, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Detasering, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @CurCode, @Amt1, @Amt2, @Amt3, @Amt4, @Amt5, @Amt6, @Detasering, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("    On Duplicate Key Update ");
                SQL.AppendLine("        EmpCode=@EmpCode, ");
                SQL.AppendLine("        CurCode=@CurCode, ");
                SQL.AppendLine("        Amt1=@Amt1, ");
                SQL.AppendLine("        Amt2=@Amt2, ");
                SQL.AppendLine("        Amt3=@Amt3, ");
                SQL.AppendLine("        Amt4=@Amt4, ");
                SQL.AppendLine("        Amt5=@Amt5, ");
                SQL.AppendLine("        Amt6=@Amt6, ");
                SQL.AppendLine("        Detasering=@Detasering ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt4", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt5", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt6", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Detasering", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void InsertDataVR(string TRVDocNo)
        {
            if (Grd1.Rows.Count > 1)
            {
                var VoucherRequestDocNoTR = string.Empty;
                var VoucherRequestDocNo = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    var cml = new List<MySqlCommand>();
                    string DNo = Sm.Right("00" + (Row + 1).ToString(), 3);
                    VoucherRequestDocNoTR = Sm.GetValue("Select VoucherRequestDocNo From tbltravelrequestDtl7 Where DocNo = '" + TRVDocNo + "' And Dno = '"+DNo+"' ");
                    VoucherRequestDocNo = VoucherRequestDocNoTR.Length >0 ? VoucherRequestDocNoTR : GenerateVoucherRequestDocNo();
                    cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, TRVDocNo, Row));
                    cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, TRVDocNo, Row));
                    Sm.ExecCommands(cml);
                }
            }
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || mIsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //save vr
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string TRVDocNo, int Rowx)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', @DeptCode, '23', Null, ");
            SQL.AppendLine("'C', @PaymentType, Null, @BankCode, null, null, null, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, Concat('Travel Request ', @TRVDocNo, '. ', @Remark),");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("    ON DUPLICATE KEY UPDATE Amt=@Amt, Remark=Concat('Travel Request ', @TRVDocNo, '. ', @Remark) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='TravelRequest' ");
            SQL.AppendLine("    And DocNo=@TRVDocNo ");
            SQL.AppendLine("    ); ");

            if (TxtDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Update tblvoucherrequesthdr A ");
                SQL.AppendLine("inner join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    select 1  ");
                SQL.AppendLine("    from tbldocapproval T1 ");
                SQL.AppendLine("    inner join tbltravelrequesthdr T2 On T1.DocNo = T2.DocNo and T1.DocNo = @TRVDocNo and T2.Status = 'A'  ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set A.Status = 'A' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ; ");


                SQL.AppendLine("update tblvoucherrequesthdr A ");
                SQL.AppendLine("inner join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    select 1 ");
                SQL.AppendLine("    from tbldocapproval T1 ");
                SQL.AppendLine("    inner join tbltravelrequesthdr T2 On T1.DocNo = T2.Docno and T1.DocNo = @TRVDocNo and T2.Status = 'C' ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set A.Status = 'C' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ; ");
            }

            SQL.AppendLine("Update TblTravelRequestDtl7 Set VoucherRequestDocNo=@DocNo ");
            SQL.AppendLine("Where DocNo=@TRVDocNo And Dno=@Rowx ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@TRVDocNo", TRVDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetValue("Select DeptCode From TblEmployee Where EmpCode='" + TxtPICCode.Text + "'"));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetParameter("BankAcCodeTravelRequest"));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetValue("Select UserCode From tblEmployee Where EmpCode = '"+TxtPICCode.Text+"'"));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetParameter("MainCurCode"));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Rowx, 11));
            Sm.CmParam<String>(ref cm, "@Rowx", Sm.Right("00" + (Rowx + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo, string TRVDocNo, int Rowx)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, Concat('Travel Request# : ', @TRVDocNo, ' - ', @EmpCode), @Amt, Null, @CreateBy, CurrentDateTime()) "+
                    "   On Duplicate key Update Description = Concat('Travel Request# : ', @TRVDocNo, ' - ', @EmpCode), Amt=@Amt ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@TRVDocNo", TRVDocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Rowx, 2));
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Rowx, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTravelRequestHdr(TxtDocNo.Text));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl7(TxtDocNo.Text, Row));
            
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl(TxtDocNo.Text, Row));
            
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl2(TxtDocNo.Text, Row));
            
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl3(TxtDocNo.Text, Row));
            
            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl4(TxtDocNo.Text, Row));
            
            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl5(TxtDocNo.Text, Row));

            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl6(TxtDocNo.Text, Row));
            
            Sm.ExecCommands(cml);

            if (!ChkCancelInd.Checked) InsertDataVR(TxtDocNo.Text);
            
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDateNotValid() ||
                IsDataCancelledAlready() ||
                IsDocAlreadyProcessToVoucher() ||
                (!ChkCancelInd.Checked && IsDataCreateByDesktop()) ||
                (mWebInd=="W" && IsCurCodeInvalid()) ||
                (mWebInd == "W" && !ChkCancelInd.Checked && IsPositionStatusInactive()) ||
                (mWebInd == "W" && mTravelRequestWebFormat == "2" && Sm.IsTxtEmpty(TxtPICCode, "PIC", false))
                ;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblTravelRequestHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
            
        }

        private bool IsDataCreateByDesktop()
        {
            var f = Sm.IsDataExist(
                "Select 1 From TblTravelRequestHdr Where WebInd='D' And DocNo=@Param;",
                TxtDocNo.Text,
                "You can only edit documents created in web application."
                );
            return f;
        }

        private bool IsDocAlreadyProcessToVoucher()
        {
            return Sm.IsDataExist(
                "Select 1 From TblTravelRequestDtl7 A, TblVoucherHdr B " +
                "Where A.DocNo=@Param And A.VoucherRequestDocNo Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' Limit 1;",
                TxtDocNo.Text, "This document already processed to voucher.");
        }

        private MySqlCommand CancelTravelRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            if (ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblTravelRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
                SQL.AppendLine("Where CancelInd='N' And Status In ('O', 'A') ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select VoucherRequestDocNo ");
                SQL.AppendLine("    From TblTravelrequestDtl7 ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And VoucherRequestDocNo Is Not Null ");
                SQL.AppendLine("    ); ");
            }

            if (mTravelRequestWebFormat == "2")
            {
                SQL.AppendLine("Update TblTravelRequestHdr Set ");
                SQL.AppendLine("    PICCode=@PICCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo; ");

                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime()  ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='TravelRequest' ");
                if (mIsApprovalByDept)
                {
                    SQL.AppendLine("And T.DeptCode Is Not Null ");
                    SQL.AppendLine("And T.DeptCode In ( ");
                    SQL.AppendLine("    Select DeptCode From TblEmployee ");
                    SQL.AppendLine("    Where EmpCode=@PICCode ");
                    SQL.AppendLine("    And DeptCode Is Not Null ");
                    SQL.AppendLine("    ) ");
                }

                if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='TravelRequest' And DAGCode Is Not Null Limit 1;"))
                {
                    SQL.AppendLine("And (T.DAGCode Is Null Or ");
                    SQL.AppendLine("(T.DAGCode Is Not Null ");
                    SQL.AppendLine("And T.DAGCode In ( ");
                    SQL.AppendLine("    Select A.DAGCode ");
                    SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                    SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                    SQL.AppendLine("    And A.ActInd='Y' ");
                    SQL.AppendLine("    And B.EmpCode=@PICCode ");
                    SQL.AppendLine("))) ");
                }

                SQL.AppendLine(";");

                SQL.AppendLine("Update TblTravelRequestHdr Set ");
                SQL.AppendLine("    Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists(  ");
                SQL.AppendLine("    Select 1 From TblDocApproval  ");
                SQL.AppendLine("    Where DocType='TravelRequest' ");
                SQL.AppendLine("    And DocNo=@DocNo  ");
                SQL.AppendLine(");  ");

            }
            else
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime()  ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='TravelRequest' ");
                if (mIsApprovalByDept)
                {
                    SQL.AppendLine("And T.DeptCode Is Not Null ");
                    SQL.AppendLine("And T.DeptCode In ( ");
                    SQL.AppendLine("    Select DeptCode From TblEmployee ");
                    SQL.AppendLine("    Where EmpCode=@PICCode ");
                    SQL.AppendLine("    And DeptCode Is Not Null ");
                    SQL.AppendLine("    ) ");
                }

                if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='TravelRequest' And DAGCode Is Not Null Limit 1;"))
                {
                    SQL.AppendLine("And (T.DAGCode Is Null Or ");
                    SQL.AppendLine("(T.DAGCode Is Not Null ");
                    SQL.AppendLine("And T.DAGCode In ( ");
                    SQL.AppendLine("    Select A.DAGCode ");
                    SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                    SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                    SQL.AppendLine("    And A.ActInd='Y' ");
                    SQL.AppendLine("    And B.EmpCode=@PICCode ");
                    SQL.AppendLine("))) ");
                }
                SQL.AppendLine("And Not Exists(  ");
                SQL.AppendLine("    Select 1 From TblDocApproval  Where DocType='TravelRequest' And DocNo=@DocNo  ");
                SQL.AppendLine(")  ");

                SQL.AppendLine(";");

                SQL.AppendLine("Update TblTravelRequestHdr Set ");
                SQL.AppendLine("    Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists(  ");
                SQL.AppendLine("    Select 1 From TblDocApproval  ");
                SQL.AppendLine("    Where DocType='TravelRequest' ");
                SQL.AppendLine("    And DocNo=@DocNo  ");
                SQL.AppendLine(");  ");
            }
                
            SQL.AppendLine("Update TblTravelRequestHdr SET WebInd='D' Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblTravelRequestDtl Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl2 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl3 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl4 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl5 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl6 Where DocNo=@DocNo ;");

            //SQL.AppendLine("Delete From TblTravelRequestDtl7 Where DocNo=@DocNo ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                ClearData();
                string WebInd = Sm.GetValue("Select WebInd From TblTravelRequestHdr Where DocNo=@Param;", DocNo);
                ShowTravelRequestHdr(DocNo);
                if (WebInd == "D")
                {
                    ShowTravelRequestDtl(DocNo);
                    ShowTravelRequestDtl2(DocNo);
                    ShowTravelRequestDtl3(DocNo);
                    ShowTravelRequestDtl4(DocNo);
                    ShowTravelRequestDtl5(DocNo);
                    ShowTravelRequestDtl6(DocNo);
                    ShowTravelRequestDtl7(DocNo);
                    ShowTravelRequestDtl8(DocNo);
                }
                else
                {
                    ShowTravelRequestDtl7(DocNo);
                    ShowEmployeeInfo();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTravelRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("When 'A' Then 'Approved' ");
            SQL.AppendLine("When 'O' Then 'Outstanding' ");
            SQL.AppendLine("when 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As DocStatus, ");
            if (mIsTravelRequestUseArrivalStatus)
            {
                SQL.AppendLine("Case A.ArrivalStatus ");
                SQL.AppendLine("When 'A' Then 'Arrived' ");
                SQL.AppendLine("When 'O' Then 'Ongoing' ");
                SQL.AppendLine("When 'C' Then 'Cancelled' ");
                SQL.AppendLine("End As ArrivalStatus, ");
                SQL.AppendLine("(Select UserName From TblUser Where UserCode In (Select ArrivalChecker From TblTravelRequestHdr Where Docno = @DocNo)) As ArrivalChecker, ");
                SQL.AppendLine("IfNull(Date_Format(A.ArrivalDtTm, '%d %b %Y %H:%i'), '') As ArrivalDtTm, ");
            }
            else
                SQL.AppendLine("Null As ArrivalStatus, Null As ArrivalChecker, Null As ArrivalDtTm, ");
            SQL.AppendLine("A.CancelInd, A.CancelReason, ifnull(C.CityName, A.CityCode) As CityCode, ");
            SQL.AppendLine("A.StartDt, A.EndDt, A.StartTm, A.EndTm, A.TravelService, A.PICCode, B.EmpName, ");
            SQL.AppendLine("A.SiteCode, A.Transportation, A.Result, A.SiteCode2, A.WebInd, A.RegionCode, A.Remark ");
            SQL.AppendLine("From TblTravelRequesthdr A ");
            SQL.AppendLine("Left Join TblEmployee B On A.PICCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblCity C On A.CityCode = C.CityCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "DocStatus", "CancelReason", "CancelInd", "CityCode",  
                        //6-10
                        "StartDt", "EndDt", "StartTm", "EndTm", "TravelService",
                        //11-15
                        "PICCode", "Empname", "SiteCode", "Transportation", "Result",
                        //16-20
                        "SiteCode2", "WebInd", "RegionCode", "Remark", "ArrivalStatus",
                        //21-22
                        "ArrivalChecker", "ArrivalDtTm"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = DocNo;
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeCityCode.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[7]));
                        Sm.SetTme(TmeStart, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(TmeEnd, Sm.DrStr(dr, c[9]));
                        MeeTravelService.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[12]);
                        Sl.SetLueSiteCode(ref LueSite, Sm.DrStr(dr, c[13]), string.Empty);
                        Sm.SetLue(LueTransport, Sm.DrStr(dr, c[14]));
                        MeeResult.EditValue = Sm.DrStr(dr, c[15]);
                        Sl.SetLueSiteCode(ref LueSiteCode2, Sm.DrStr(dr, c[16]), string.Empty);
                        //Sm.SetLue(LueSiteCode2, Sm.DrStr(dr, c[16]));
                        mWebInd = Sm.DrStr(dr, c[17]);
                        Sm.SetLue(LueRegionCode, Sm.DrStr(dr, c[18]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                        if (mIsTravelRequestUseArrivalStatus)
                        {
                            TxtArrivalStatus.EditValue = Sm.DrStr(dr, c[20]);
                            TxtArrivalChecker.EditValue = Sm.DrStr(dr, c[21]);
                            TxtArrivalDtTm.EditValue = Sm.DrStr(dr, c[22]);
                        }
                    }, true
                );
        }

        private void ShowTravelRequestDtl(string DocNo)
        {
            //Meal
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, ");
            SQL.AppendLine("A.PositionStatusCode, A.ADCode, A.CurCode, A.Rate, A.Qty, ");
            SQL.AppendLine("A.PositionStatusCode2, A.ADCode2, A.CurCode2, A.Rate2, A.Qty2, ");
            SQL.AppendLine("A.PositionStatusCode3, A.ADCode3, A.CurCode3, A.Rate3, A.Qty3, ");
            SQL.AppendLine("Amt ");
            SQL.AppendLine("From TblTravelRequestDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "EmpCode", "EmpName", "PositionStatusCode", "ADCode", "CurCode", 

                    //6-10
                    "Rate", "Qty", "PositionStatusCode2", "ADCode2", "CurCode2", 

                    //11-15
                    "Rate2", "Qty2", "PositionStatusCode3", "ADCode3", "CurCode3", 

                    //16-18
                    "Rate3", "Qty3", "Amt" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);                    
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 7, 8, 12, 13, 17, 18, 19 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowTravelRequestDtl2(string DocNo)
        {
            //Uang saku
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.PositionStatusCode, A.ADCode, A.CurCode, A.Rate, A.Qty, A.Amt ");
            SQL.AppendLine("From TblTravelRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "EmpCode", "EmpName", "PositionStatusCode", "ADCode", "CurCode", 

                    //6-8
                    "Rate", "Qty", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8, 9 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowTravelRequestDtl3(string DocNo)
        {
            //city transport

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.PositionStatusCode, A.ADCode, A.CurCode, A.Rate, A.Qty, A.Amt ");
            SQL.AppendLine("From TblTravelRequestDtl3 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",
 
                    //1-5
                    "EmpCode", "EmpName", "PositionStatusCode", "ADCode", "CurCode", 

                    //6-8
                    "Rate", "Qty", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8, 9 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowTravelRequestDtl4(string DocNo)
        {
            //Transport

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.OfficeInd, A.PositionStatusCode, A.ADCode, A.CurCode, A.Rate, A.Qty, A.Amt, A.Remark ");
            SQL.AppendLine("From TblTravelRequestDtl4 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "EmpCode", "EmpName", "OfficeInd", "PositionStatusCode", "ADCode", 
                    
                    //6-10
                    "CurCode", "Rate", "Qty", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd5, Grd5.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowTravelRequestDtl5(string DocNo)
        {
            //Accomodation
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.OfficeInd, A.PositionStatusCode, A.ADCode, A.CurCode, A.Rate, A.Qty, A.Amt, A.Remark ");
            SQL.AppendLine("From TblTravelRequestDtl5 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "EmpCode", "EmpName", "OfficeInd", "PositionStatusCode", "ADCode", 
                    
                    //6-10
                    "CurCode", "Rate", "Qty", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd6, Grd6.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowTravelRequestDtl6(string DocNo)
        {
            //Others
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.PositionStatusCode, A.ADCode, C.ADName, A.CurCode, A.Rate, A.Qty, A.Amt, A.Remark ");
            SQL.AppendLine("From TblTravelRequestDtl6 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction C On A.ADCode=C.ADCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "EmpCode", "EmpName", "ADCode", "ADName", "PositionStatusCode",  
                    
                    //6-10
                    "CurCode", "Rate", "Qty", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd7, Grd7.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowTravelRequestDtl7(string DocNo)
        {
            //Summary
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, ");
            SQL.AppendLine("A.Amt1, A.Amt2, A.Amt3, A.Amt4, A.Amt5, A.Amt6, A.Detasering, ");
            SQL.AppendLine("(A.Amt1+A.Amt2+A.Amt3+A.Amt4+A.Amt5+A.Amt6+A.Detasering) As Total, ");
            SQL.AppendLine("A.VoucherRequestDocNo, A.CurCode ");
            SQL.AppendLine("From TblTravelRequestDtl7 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "Amt1", "Amt2", "Amt3",

                    //6-10
                    "Amt4", "Amt5", "Amt6", "Detasering", "Total", 

                    //11-12
                    "VoucherRequestDocNo", "CurCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9); 
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowTravelRequestDtl8(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='TravelRequest' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd8, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        #endregion      

        #endregion

        #region Additional Method

        private bool IsApprovalByDept()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='TravelRequest' And DeptCode Is Not Null Limit 1;");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void SetLueADCode(ref DXE.LookUpEdit Lue, string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ADCode As Col1, C.ADName As Col2, B.CurCode As Col3 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode Not In (");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParValue Is Not Null ");
            SQL.AppendLine("        And ParCode In ( ");
            SQL.AppendLine("        'ADCodeBreakfast', 'ADCodeLunch', 'ADCodeDinner', 'ADCodeDailyAllowance', 'ADCodeCityTransport',  'ADCodeAccomodation'");
            if (mDocTitle == "TWC") 
                SQL.AppendLine(",'ADCodeTransport2'");
            else
                SQL.AppendLine(",'ADCodeTransport'");
            SQL.AppendLine("    )) ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (StartDt Is Null And EndDt Is Null) Or ");
            SQL.AppendLine("    (StartDt Is Not Null And EndDt Is Null And StartDt<=@Dt) Or ");
            SQL.AppendLine("    (StartDt Is Null And EndDt Is Not Null And @Dt<=EndDt) Or ");
            SQL.AppendLine("    (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@Dt And @Dt<=EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction C On B.ADCode=C.ADCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            try
            {
                Sm.SetLue3(ref Lue, ref cm, 0, 35, 0, false, true, false, "Code", "Allowance", "Currency", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueCompanyCity(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sm.SetLue2(ref Lue, "Select CityCode Col1, CityName Col2 From TblCompanyCity Order By CityName;", 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueTransportTravel(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sl.SetLueOption(ref Lue, "TransportTravel");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeTotalQty(int mRow, string EmpCode, iGrid mGrd)
        {
            if (mGrd.Name == "Grd2")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 4].Value = Sm.GetGrdDec(mGrd, mRow, 19);
            }

            if (mGrd.Name == "Grd3")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 5].Value = Sm.GetGrdDec(mGrd, mRow, 9);
            }

            if (mGrd.Name == "Grd4")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 6].Value = Sm.GetGrdDec(mGrd, mRow, 9);
            }

            if (mGrd.Name == "Grd5")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 7].Value = Sm.GetGrdDec(mGrd, mRow, 10);
            }
            
            if (mGrd.Name == "Grd6")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 8].Value = Sm.GetGrdDec(mGrd, mRow, 10);
            }

            if (mGrd.Name == "Grd7")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, row, 2)))
                        Grd1.Cells[row, 9].Value = Sm.GetGrdDec(mGrd, mRow, 10);
            }
            
            ComputeSummaryAllowance();
        }

        internal void ComputeTotalMeal(int r)
        {
            decimal Rate1 = 0m, Qty1 = 0m, Rate2 = 0m, Qty2 = 0m, Rate3 = 0m, Qty3 = 0m;

            if (Sm.GetGrdStr(Grd2, r, 7).Length > 0) Rate1 = Sm.GetGrdDec(Grd2, r, 7);
            if (Sm.GetGrdStr(Grd2, r, 8).Length > 0) Qty1 = Sm.GetGrdDec(Grd2, r, 8);
            if (Sm.GetGrdStr(Grd2, r, 12).Length > 0) Rate2 = Sm.GetGrdDec(Grd2, r, 12);
            if (Sm.GetGrdStr(Grd2, r, 13).Length > 0) Qty2 = Sm.GetGrdDec(Grd2, r, 13);
            if (Sm.GetGrdStr(Grd2, r, 17).Length > 0) Rate3 = Sm.GetGrdDec(Grd2, r, 17);
            if (Sm.GetGrdStr(Grd2, r, 18).Length > 0) Qty3 = Sm.GetGrdDec(Grd2, r, 18);

            Grd2.Cells[r, 19].Value = (Rate1 * Qty1) + (Rate2 * Qty2) + (Rate3 * Qty3);
        }

        internal void ComputeTotalDaily(int r)
        {
            if (Sm.GetGrdStr(Grd3, r, 7).Length > 0 && Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                Grd3.Cells[r, 9].Value = Sm.GetGrdDec(Grd3, r, 7) * Sm.GetGrdDec(Grd3, r, 8);
            else
                Grd3.Cells[r, 9].Value = 0m;
        }

        internal void ComputeTotalCityTransport(int r)
        {
            if (Sm.GetGrdStr(Grd4, r, 7).Length > 0 && Sm.GetGrdStr(Grd4, r, 8).Length > 0) 
                Grd4.Cells[r, 9].Value = Sm.GetGrdDec(Grd4, r, 7) * Sm.GetGrdDec(Grd4, r, 8);
            else
                Grd4.Cells[r, 9].Value = 0m;
        }

        internal void ComputeTotalTransport(int r)
        {
            if (Sm.GetGrdStr(Grd5, r, 8).Length > 0 && Sm.GetGrdStr(Grd5, r, 9).Length > 0) 
                Grd5.Cells[r, 10].Value = Sm.GetGrdDec(Grd5, r, 8) * Sm.GetGrdDec(Grd5, r, 9);
            else
                Grd5.Cells[r, 10].Value = 0m;
        }

        internal void ComputeTotalAccomodation(int r)
        {
            if (Sm.GetGrdStr(Grd6, r, 8).Length > 0 && Sm.GetGrdStr(Grd6, r, 9).Length > 0)
                Grd6.Cells[r, 10].Value = Sm.GetGrdDec(Grd6, r, 8) * Sm.GetGrdDec(Grd6, r, 9);
            else
                Grd6.Cells[r, 10].Value = 0m;   
        }

        internal void ComputeTotalOthers(int r)
        {
            if (Sm.GetGrdStr(Grd7, r, 8).Length > 0 && Sm.GetGrdStr(Grd7, r, 9).Length > 0)
                Grd7.Cells[r, 10].Value = Sm.GetGrdDec(Grd7, r, 8) * Sm.GetGrdDec(Grd7, r, 9);
            else
                Grd7.Cells[r, 10].Value = 0m;
        }

        internal void ComputeSummaryAllowance()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (Detasering)
                        Grd1.Cells[r, 11].Value = Sm.GetGrdDec(Grd1, r, 10);
                    else
                        Grd1.Cells[r, 11].Value = 
                            Sm.GetGrdDec(Grd1, r, 4) + 
                            Sm.GetGrdDec(Grd1, r, 5) + 
                            Sm.GetGrdDec(Grd1, r, 6) +
                            Sm.GetGrdDec(Grd1, r, 7) + 
                            Sm.GetGrdDec(Grd1, r, 8) + 
                            Sm.GetGrdDec(Grd1, r, 9) +
                            Sm.GetGrdDec(Grd1, r, 10);
                }
            }
        }

        private void ParPrint(int i)
        {
            var l = new List<TravelRequest>();


            string[] TableName = { "TravelRequest" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.TravelService, Date_Format(A.StartDt, '%d %M %Y')As StartDt, ");
            SQL.AppendLine("Date_Format(A.EndDt,'%d %M %Y')As EndDt, Concat(Left(A.StartTm,2),':',Right(A.StartTm,2))As StartTm, ");
            SQL.AppendLine("Concat(Left(A.EndTm,2),':',Right(A.EndTm,2)) As EndTm, B.Amt1 As MealAllowance, B.Amt2 As DailyAllowance, ");
            SQL.AppendLine("B.Amt3 As CityTransport, B.Amt4 As Transport, B.Amt5 As Accomadation, B.Amt6 As OtherAllowance, C.EmpName As PICName, ");
            SQL.AppendLine("D.PosName, DATEDIFF(A.EndDt, A.StartDt)As TotDt, (B.Amt1+ B.Amt2+ B.Amt3+ B.Amt4+ B.Amt5+ B.Amt6 +B.Detasering)As Total, E.CityName, A.CityCode, ");
            SQL.AppendLine("if(length(A.RegionCode) <=0, (Select ParValue From TblParameter Where Parcode='MainCurCode'), B.CurCode) As MainCur, A.Result, F.OptDesc As Transportation, G.HOInd  ");
            SQL.AppendLine("From tbltravelrequesthdr A ");
            SQL.AppendLine("Inner Join tbltravelrequestdtl7 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join tblposition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join tblcity E On A.CityCode = E.CityCode ");
            SQL.AppendLine("Left Join tbloption F On F.OptCat='TransportTravel' And A.Transportation = F.OptCode ");
            SQL.AppendLine("Left Join Tblsite G On A.SiteCode = G.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.DNo=@DNo ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", i.ToString()), 3));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "Docno",
                         "DocDt",
                         "TravelService",
                         "StartDt",
                         "EndDt",
                         
                         //11-15
                         "StartTm",
                         "EndTm",
                         "MealAllowance",
                         "DailyAllowance",
                         "CityTransport",
                        
                         //16-20
                         "Transport",
                         "Accomadation",
                         "OtherAllowance",
                         "PICName",
                         "PosName",

                         //21-25
                         "TotDt",
                         "Total",
                         "CityName",
                         "MainCur",
                         "Result",

                         //26-28
                         "Transportation",
                         "HOInd",
                         "CityCode"
                        
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TravelRequest()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            TravelService = Sm.DrStr(dr, c[8]),
                            StartDt = Sm.DrStr(dr, c[9]),
                            EndDt = Sm.DrStr(dr, c[10]),

                            StartTm = Sm.DrStr(dr, c[11]),
                            EndTm = Sm.DrStr(dr, c[12]),
                            MealAllowance = Sm.DrDec(dr, c[13]),
                            DailyAllowance = Sm.DrDec(dr, c[14]),
                            CityTransport = Sm.DrDec(dr, c[15]),

                            Transport = Sm.DrDec(dr, c[16]),
                            Accomadation = Sm.DrDec(dr, c[17]),
                            OtherAllowance = Sm.DrDec(dr, c[18]),
                            PICName = Sm.DrStr(dr, c[19]),
                            PosName = Sm.DrStr(dr, c[20]),

                            TotDt = Sm.DrStr(dr, c[21]),
                            Terbilang = Sm.Terbilang2(Sm.DrDec(dr, c[21])),
                            Total = Sm.DrDec(dr, c[22]),
                            CityName = Sm.DrStr(dr, c[23]),
                            MainCur = Sm.DrStr(dr, c[24]),
                            Result = Sm.DrStr(dr, c[25]),

                            Transportation = Sm.DrStr(dr, c[26]),
                            HOInd = Sm.DrStr(dr, c[27]),
                            CityCode = Sm.DrStr(dr, c[28]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PosName2 = (Sm.DrStr(dr, c[27]) == "Y" ? mPosNameTravelRequestForHO : mPosNameTravelRequestForNonHO),
                            SignName = (Sm.DrStr(dr, c[27]) == "Y" ? mSignNameTravelRequestForHO : mSignNameTravelRequestForNonHO)
                          
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            Sm.PrintReport("HINRbpd", myLists, TableName, false);

        }

        private void ParPrint2()
        {
            var l2 = new List<TravelRequest2>();
            var ldtl = new List<VoucherReqDtl>();

            string[] TableName = { "TravelRequest2", "VoucherReqDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL2.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y')As DocDt, C.EmpName As PICName, A.TravelService, ");
            SQL2.AppendLine("Date_Format(A.StartDt, '%d %M %Y')As StartDt, Date_Format(A.EndDt,'%d %M %Y')As EndDt, ");
            SQL2.AppendLine("Concat(Left(A.StartTm,2),':',Right(A.StartTm,2))As StartTm, ");
            SQL2.AppendLine("Concat(Left(A.EndTm,2),':',Right(A.EndTm,2)) As EndTm, A.Result, D.PosName, E.CityName, A.CityCode, ");
            SQL2.AppendLine("F.OptDesc As Transportation, G.HOInd  ");
            SQL2.AppendLine("From tbltravelrequesthdr A ");
            SQL2.AppendLine("Inner Join tbltravelrequestdtl7 B On A.DocNo=B.DocNo ");
            SQL2.AppendLine("Inner Join TblEmployee C On A.PICCode=C.EmpCode ");
            SQL2.AppendLine("Left Join tblposition D On C.PosCode=D.PosCode ");
            SQL2.AppendLine("Left Join tblcity E On A.CityCode = E.CityCode ");
            SQL2.AppendLine("Left Join tbloption F On F.OptCat='TransportTravel' And A.Transportation = F.OptCode ");
            SQL2.AppendLine("Left join TblSite G On A.SiteCode = G.SiteCode ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");


            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "Docno",
                         "DocDt",
                         "PicName",
                         "TravelService",
                         "StartDt",
                         
                         //11-15
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "Result",
                         "PosName",
                         
                         //16-19
                         "CityName",
                         "Transportation",
                         "HOInd",
                         "CityCode"

                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new TravelRequest2()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyAddressCity = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            CompanyFax = Sm.DrStr(dr2, c2[5]),

                            DocNo = Sm.DrStr(dr2, c2[6]),
                            DocDt = Sm.DrStr(dr2, c2[7]),
                            PICName = Sm.DrStr(dr2, c2[8]),
                            TravelService = Sm.DrStr(dr2, c2[9]),
                            StartDt = Sm.DrStr(dr2, c2[10]),

                            EndDt = Sm.DrStr(dr2, c2[11]),
                            StartTm = Sm.DrStr(dr2, c2[12]),
                            EndTm = Sm.DrStr(dr2, c2[13]),
                            Result = Sm.DrStr(dr2, c2[14]),
                            PosName = Sm.DrStr(dr2, c2[15]),

                            CityName = Sm.DrStr(dr2, c2[16]),
                            Transportation = Sm.DrStr(dr2, c2[17]),
                            HOInd = Sm.DrStr(dr2, c2[18]),
                            CityCode= Sm.DrStr(dr2, c2[19]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            SignName = (Sm.DrStr(dr2, c2[18]) == "Y" ? mSignNameTravelRequestForHO : mSignNameTravelRequestForNonHO)
                          
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select A.voucherRequestDocNo, A.DocNo, group_concat(B.EmpName)As EmpName From TblTravelRequestDtl7 A " +
                                     "Inner Join TblEmployee B On A.EmpCode=B.EmpCode Where A.DocNo=@DocNo Group by A.DocNo ";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo",

                         //1
                         "EmpName",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherReqDtl()
                        {
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),
                            EmpNAme = Sm.DrStr(drDtl, cDtl[1]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("HINRpd", myLists, TableName, false);

        }

        private void ParPrint3(string Dno)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "VoucherReq2", "VoucherReqDtl2", "VoucherReqDtl3", "VoucherReqDtl4" };

            var l2 = new List<VoucherReqHdr2>();
            var ldtl2 = new List<VoucherReqDtl2>();
            var ldtl3 = new List<VoucherReqDtl3>();
            var ldtl4 = new List<VoucherReqDtl4>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine(" Select @CompanyLogo As CompanyLogo, (Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL2.AppendLine(" Date_Format(A.DocDt, '%d %M %Y')As DocDt, ");
            SQL2.AppendLine(" A.DocNo As DocNoVR, B.DocNo, B.DNo, D.EmpName As PICName, E.PosName As PICPosName, F.GrdLvlName As PICGrdLvlName,  C.TravelService, G.OptDesc As Transportation, ");
            SQL2.AppendLine(" Date_Format(C.StartDt, '%d %M %Y')As StartDt, Date_Format(C.EndDt,'%d %M %Y')As EndDt, DateDiff(C.EndDt, C.StartDt)+1 as Duration, ");
            SQL2.AppendLine(" if(length(C.StartTm)>0, Concat(Left(C.StartTm,2),':',Right(C.StartTm,2)), '')As StartTm, if(Length(C.EndTm)>0, Concat(Left(C.EndTm,2),':',Right(C.EndTm,2)),'') As EndTm, C.result, ifnull(H.CityName, C.CityCode) As CityName, ");
            SQL2.AppendLine(" @CompanyLogo As CompanyLogo, I.Empname, J.PosName, K.GrdLvlName, C.Result, C.Remark As Remarkhdr, M.ProfitcenterName, N.HoInd, ifnull(H.CityName, C.CityCode) As CityCode, N.SiteName as SiteName1, O.SiteName as SiteName2 ");
            SQL2.AppendLine(" From Tblvoucherrequesthdr A ");
            SQL2.AppendLine(" Inner Join tbltravelrequestdtl7 B On A.DocNo=B.VoucherRequestDocNo And Dno =@Dno ");
            SQL2.AppendLine(" Left Join TbltravelRequestHdr C On B.DocNo=C.DocNo ");
            if(Doctitle == "PHT")
                SQL2.AppendLine(" Left Join TblEmployee D On C.PICCode=D.EmpCode ");
            else
                SQL2.AppendLine(" Left Join TblEmployee D On A.PIC=D.UserCode ");
            SQL2.AppendLine(" Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr F On D.GrdLvlCode=F.GrdLvlCode ");
            SQL2.AppendLine(" Left Join TblOption G On G.OptCat='TransportTravel' And C.Transportation = G.OptCode ");
            SQL2.AppendLine(" Left Join tblcity H On C.CityCode = H.CityCode ");
            SQL2.AppendLine(" Inner Join TblEmployee I On B.EmpCode = I.EmpCode ");
            SQL2.AppendLine(" Left Join TblPosition J On I.PosCode=J.PosCode  ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr K On I.GrdLvlCode=K.GrdLvlCode  ");
            SQL2.AppendLine(" left Join TblSite L On D.SiteCode = L.SiteCode ");
            SQL2.AppendLine(" left Join TblProfitCenter M On L.ProfitCenterCode = M.ProfitCenterCode ");
            SQL2.AppendLine(" left Join TblSite N On C.SiteCode = N.SiteCode ");
            SQL2.AppendLine(" left Join TblSite O On C.SiteCode2 = O.SiteCode ");
            SQL2.AppendLine(" Where C.DocNo=@DocNo And A.DocType='23' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@DNo", Dno);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "DocNoVR",
                         "DocNo",
                         "DocDt",
                         "DNo",
                         "PICName",
                        
                         //6-10
                         "PICPosName",
                         "PICGrdLvlName",
                         "TravelService",
                         "Transportation",
                         "StartDt",
                         
                         //11-15
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "Result",
                         "CityNAme",

                         //16-20
                         "CompanyLogo",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "Empname", 
                         
                         //21-25
                         "PosName", 
                         "GrdLvlName", 
                         "Result", 
                         "Remarkhdr",
                         "ProfitcenterName",

                         //26-30
                         "HOInd",
                         "CityCode",
                         "SiteName1",
                         "SiteName2", 
                         "Duration",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new VoucherReqHdr2()
                        {
                            CompanyName = Sm.DrStr(dr2, c2[0]),

                            DocNoVR = Sm.DrStr(dr2, c2[1]),
                            DocNo = Sm.DrStr(dr2, c2[2]),
                            DocDt = Sm.DrStr(dr2, c2[3]),
                            DNo = Sm.DrStr(dr2, c2[4]),
                            PICEmpName = Sm.DrStr(dr2, c2[5]),
                            PICPosName = Sm.DrStr(dr2, c2[6]),
                            PICGrdLvlName = Sm.DrStr(dr2, c2[7]),
                            TravelService = Sm.DrStr(dr2, c2[8]),
                            Transportation = Sm.DrStr(dr2, c2[9]),
                            StartDt = Sm.DrStr(dr2, c2[10]),
                            EndDt = Sm.DrStr(dr2, c2[11]),
                            StartTm = Sm.DrStr(dr2, c2[12]),
                            EndTm = Sm.DrStr(dr2, c2[13]),
                            result = Sm.DrStr(dr2, c2[14]),
                            CityName = Sm.DrStr(dr2, c2[15]),
                            CompanyLogo = Sm.DrStr(dr2, c2[16]),
                            CompanyAddress = Sm.DrStr(dr2, c2[17]),
                            CompanyPhone = Sm.DrStr(dr2, c2[18]),
                            CompanyFax = Sm.DrStr(dr2, c2[19]),
                            EmpName = Sm.DrStr(dr2, c2[20]),
                            PosName = Sm.DrStr(dr2, c2[21]),
                            GrdLvlName = Sm.DrStr(dr2, c2[22]),
                            Result = Sm.DrStr(dr2, c2[23]),
                            RemarkHdr = Sm.DrStr(dr2, c2[24]),
                            ProfitCenterName = Sm.DrStr(dr2, c2[25]),
                            HOInd = Sm.DrStr(dr2, c2[26]),
                            CityCode = Sm.DrStr(dr2, c2[27]),
                            SiteName1 = Sm.DrStr(dr2, c2[28]),
                            SiteName2 = Sm.DrStr(dr2, c2[29]),
                            Duration = Sm.DrStr(dr2, c2[30]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PrintDt = String.Format("{0:dd-MM-yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select A.voucherRequestDocNo, A.DocNo, group_concat(B.EmpName)As EmpName, ");
            SQL3.AppendLine("B.BankAcNo, B.BankAcName, C.PosName, D.POsitionStatusName, E.GrdLvlName ");
            SQL3.AppendLine("From TblTravelRequestDtl7 A ");
            SQL3.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL3.AppendLine("LEFT JOIN tblPosition C ON B.PosCode = C.PosCode ");
            SQL3.AppendLine("LEFT JOIN tblPositionStatus D ON B.PositionStatusCode = D.PositionStatusCode ");
            SQL3.AppendLine("LEFT JOIN tblGradeLevelHdr E ON B.GrdLvlCode = E.GrdLvlCode ");
            SQL3.AppendLine("Where A.DocNo=@DocNo And A.Dno = @Dno ");
            SQL3.AppendLine("Group by A.DocNo ");
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                cmDtl2.CommandText = SQL3.ToString();

                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtl2, "@DNo", Dno);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "voucherRequestDocNo",

                         //1-5
                         "DocNo",
                         "EmpName",
                         "PosName", 
                         "PositionStatusName", 
                         "GrdLvlName",

                         //6
                         "BankAcNo",
                         "BankAcName"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new VoucherReqDtl2()
                        {
                            VoucherRequestDocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpNAme = Sm.DrStr(drDtl2, cDtl2[2]),
                            EmpPosName = Sm.DrStr(drDtl2, cDtl2[3]),
                            EmpPosStatusName = Sm.DrStr(drDtl2, cDtl2[4]),
                            EmpGrdLvlName = Sm.DrStr(drDtl2, cDtl2[5]),
                            EmpBankAcNo = Sm.DrStr(drDtl2, cDtl2[6]),
                            EmpBankAcName = Sm.DrStr(drDtl2, cDtl2[7]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail3

            var cmDtl4 = new MySqlCommand();

            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;
                cmDtl4.CommandText =
                    "Select A.DocNo, A.EmpCode, SUBSTRING_INDEX(C.ADName, ' ', 1) As AdName,  (A.Rate+A.Rate2+A.Rate3) As Amt, (A.Qty+A.Qty2+A.Qty3) As Qty,  " +
                    "((A.Rate*A.Qty) +(A.Rate2*A.Qty2) + (A.Rate3*A.Qty3)) As Total " +
                    "From tbltravelrequestdtl A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Inner Join TblAllowanceDeduction C On A.ADCode = C.AdCode " +
                    "Inner Join TblAllowanceDeduction D On A.ADCode2 = D.AdCode " +
                    "Inner Join TblAllowanceDeduction E On A.ADCode3 = E.AdCode " +
                    "Where Concat(A.DocNO, A.Dno) = @DocNo " +

                    "Union All " +
                    "Select A.DocNo, A.EmpCode, C.AdName, A.Amt, A.Qty, (A.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl2 A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Inner Join TblAllowanceDeduction C On A.ADCode = C.AdCode " +
                    "Where A.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +

                    "Union All " +
                    "Select A.DocNO, A.EmpCode, 'City Transport' As ADName,   " +
                    "A.Amt, A.Qty, (A.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl3 A  " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode  " +
                    "Inner Join TblAllowanceDeduction C On A.ADCode = C.AdCode " +
                    "Where A.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +

                    "union all " +
                    "Select A.DocNo, A.EmpCode, 'Transport' As ADName,  " +
                    "A.Amt, A.Qty, if(OfficeInd='N', (A.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl4 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Inner Join TblAllowanceDeduction C On A.ADCode = C.AdCode " +
                    "Where A.Amt <> 0 And Concat(A.DocNO, A.Dno) = @DocNo " +

                    "Union all " +
                    "Select A.DocNo, A.EmpCode, C.AdName,  " +
                    "A.Amt, A.Qty, if(OfficeInd='N', (A.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl5 A   " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode   " +
                    "Inner Join TblAllowanceDeduction C On A.ADCode = C.AdCode  " +
                    "Where A.Amt <> 0  And Concat(A.DocNO, A.Dno) = @DocNo " +

                    "union all " +
                    "Select A.DocNo, A.EmpCode, D.ADName, A.Amt, A.Qty, (A.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl6 A   " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode   " +
                    "Left Join TblAllowanceDeduction D On A.AdCode=D.AdCode " +
                    "Where A.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo ";

                Sm.CmParam<String>(ref cmDtl4, "@DocNo", string.Concat(TxtDocNo.Text, Dno));
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "EmpCode",
                         "ADName",
                         "Amt",
                         "Qty",
                         "Total"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new VoucherReqDtl4()
                        {
                            DocNo = Sm.DrStr(drDtl4, cDtl4[0]),
                            EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                            AdName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                            Qty = Sm.DrDec(drDtl4, cDtl4[4]),
                            Total = Sm.DrDec(drDtl4, cDtl4[5]),
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            if (Doctitle == "HIN")
            {
                Sm.PrintReport("HINSppd", myLists, TableName, false);
            }

            if (Doctitle == "KIM")
            {
                Sm.PrintReport("KIMSppd", myLists, TableName, false);
            }

            if (Doctitle == "PHT")
            {
                Sm.PrintReport("PHTSppd", myLists, TableName, false);
            }
        }

        internal void ShowEmployeeInfo()
        {
            ShowEmployeeInfoMeal();
            ShowEmployeeInfoDaily();
            ShowEmployeeInfoCityTransport();
            ShowEmployeeInfoTransport();
            ShowEmployeeInfoAccomodation();
            ShowEmployeeInfoOther();
        }

        private void ShowEmployeeInfoMeal()
        {
            Sm.ClearGrd(Grd2, true);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;
            string
                ADCodeBreakfast = Sm.GetParameter("ADCodeBreakfast"),
                ADCodeLunch = Sm.GetParameter("ADCodeLunch"),
                ADCodeDinner = Sm.GetParameter("ADCodeDinner");
            string
                EmpName = string.Empty,
                PositionStatusCode = string.Empty,
                ADCode = string.Empty,
                CurCode = string.Empty;
            decimal Amt = 0m;
            int Row = -1;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            EmpCode = string.Empty;

            SQL.AppendLine("Select Distinct A.EmpCode, A.EmpName, A.PositionStatusCode, B.ADCode, B.CurCode, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode In ('ADCodeBreakfast', 'ADCodeLunch', 'ADCodeDinner'))  ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Null) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Null And B.StartDt<=@Dt) Or ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Not Null And @Dt<=B.EndDt) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Not Null And B.StartDt<=@Dt And @Dt<=B.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "PositionStatusCode", "ADCode", "CurCode", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        EmpName = Sm.DrStr(dr, c[1]);
                        PositionStatusCode = Sm.DrStr(dr, c[2]);
                        ADCode = Sm.DrStr(dr, c[3]);
                        CurCode = Sm.DrStr(dr, c[4]);
                        Amt = Sm.DrDec(dr, c[5]);

                        Row = -1;
                        if (Grd2.Rows.Count > 0)
                        {
                            for (int r = 0; r < Grd2.Rows.Count; r++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, r, 2), EmpCode))
                                {
                                    Row = r;
                                    break;
                                }
                            }
                        }
                        if (Row == -1)
                        {
                            Row = Grd2.Rows.Count - 1;
                            Grd2.Cells[Row, 2].Value = EmpCode;
                            Grd2.Cells[Row, 3].Value = EmpName;
                            Sm.SetGrdNumValueZero(Grd2, Row, new int[] { 7, 8, 12, 13, 17, 18, 19 });
                            Grd2.Rows.Add();
                        }

                        if (Sm.CompareStr(ADCode, ADCodeBreakfast))
                        {
                            Grd2.Cells[Row, 4].Value = PositionStatusCode;
                            Grd2.Cells[Row, 5].Value = ADCode;
                            Grd2.Cells[Row, 6].Value = CurCode;
                            Grd2.Cells[Row, 7].Value = Amt;
                        }

                        if (Sm.CompareStr(ADCode, ADCodeLunch))
                        {
                            Grd2.Cells[Row, 9].Value = PositionStatusCode;
                            Grd2.Cells[Row, 10].Value = ADCode;
                            Grd2.Cells[Row, 11].Value = CurCode;
                            Grd2.Cells[Row, 12].Value = Amt;
                        }

                        if (Sm.CompareStr(ADCode, ADCodeDinner))
                        {
                            Grd2.Cells[Row, 14].Value = PositionStatusCode;
                            Grd2.Cells[Row, 15].Value = ADCode;
                            Grd2.Cells[Row, 16].Value = CurCode;
                            Grd2.Cells[Row, 17].Value = Amt;
                        }
                    }
                }
                dr.Close();
            }
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 7, 8, 12, 13, 17, 18, 19 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowEmployeeInfoDaily()
        {
            Sm.ClearGrd(Grd3, true);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.PositionStatusCode, B.ADCode, B.CurCode, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeDailyAllowance')  ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Null) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Null And B.StartDt<=@Dt) Or ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Not Null And @Dt<=B.EndDt) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Not Null And B.StartDt<=@Dt And @Dt<=B.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "PositionStatusCode", "ADCode", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = 0m;
                    Grd.Cells[Row, 9].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8, 9 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowEmployeeInfoCityTransport()
        {
            Sm.ClearGrd(Grd4, true);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.PositionStatusCode, B.ADCode, B.CurCode, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeCityTransport')  ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Null) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Null And B.StartDt<=@Dt) Or ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Not Null And @Dt<=B.EndDt) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Not Null And B.StartDt<=@Dt And @Dt<=B.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "PositionStatusCode", "ADCode", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = 0m;
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8, 9 });
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowEmployeeInfoTransport()
        {
            Sm.ClearGrd(Grd5, true);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.PositionStatusCode, B.ADCode, B.CurCode, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='" + (mDocTitle == "TWC" ? "ADCodeTransport2" : "ADCodeTransport") + "')  ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Null) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Null And B.StartDt<=@Dt) Or ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Not Null And @Dt<=B.EndDt) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Not Null And B.StartDt<=@Dt And @Dt<=B.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "PositionStatusCode", "ADCode", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Grd.Cells[Row, 4].Value = false;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Grd5.Cells[Grd5.Rows.Count - 1, 4].Value = false;
            Sm.SetGrdNumValueZero(Grd5, Grd5.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowEmployeeInfoAccomodation()
        {
            Sm.ClearGrd(Grd6, true);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.PositionStatusCode, B.ADCode, B.CurCode, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblPositionStatusAllowanceDeduction B ");
            SQL.AppendLine("    On A.PositionStatusCode=B.PositionStatusCode ");
            SQL.AppendLine("    And B.RegionCode=@RegionCode ");
            SQL.AppendLine("    And B.ADCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeAccomodation')  ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Null) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Null And B.StartDt<=@Dt) Or ");
            SQL.AppendLine("        (B.StartDt Is Null And B.EndDt Is Not Null And @Dt<=B.EndDt) Or ");
            SQL.AppendLine("        (B.StartDt Is Not Null And B.EndDt Is Not Null And B.StartDt<=@Dt And @Dt<=B.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "PositionStatusCode", "ADCode", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Grd.Cells[Row, 4].Value = false;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Grd6.Cells[Grd6.Rows.Count - 1, 4].Value = false;
            Sm.SetGrdNumValueZero(Grd6, Grd6.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowEmployeeInfoOther()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select EmpCode, EmpName, PositionStatusCode ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By EmpName;");

            Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-2
                    "EmpName", "PositionStatusCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Grd.Cells[Row, 4].Value = null;
                    Grd.Cells[Row, 5].Value = null;
                    Grd.Cells[Row, 7].Value = null;
                    Grd.Cells[Row, 8].Value = 0m;
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd7, Grd7.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd7, 0, 0);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTravelRequest2Dlg2(this));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueTransport_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTransport, new Sm.RefreshLue1(SetLueTransportTravel));
        }

        private void LueSite_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSite, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueSiteCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode2, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
        }

        private void LueRegionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueRegionCode, new Sm.RefreshLue1(Sl.SetLueRegionCode));
        }

        private void LueAllowance_Leave(object sender, EventArgs e)
        {
            if (LueAllowance.Visible && fAccept && fCell.ColIndex == 5 && Sm.GetGrdStr(Grd7, fCell.RowIndex, 2).Length>0)
            {
                int r = fCell.RowIndex;
                if (Sm.GetLue(LueAllowance).Length == 0)
                {
                    Grd7.Cells[r, 4].Value = null;
                    Grd7.Cells[r, 5].Value = null;
                    Grd7.Cells[r, 7].Value = null;
                    Grd7.Cells[r, 8].Value = 0m;
                }
                else
                {
                    Grd7.Cells[r, 4].Value = Sm.GetLue(LueAllowance);
                    Grd7.Cells[r, 5].Value = LueAllowance.GetColumnValue("Col2");
                    Grd7.Cells[r, 7].Value = LueAllowance.GetColumnValue("Col3");
                    
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Amt From TblPositionStatusAllowanceDeduction ");
                    SQL.AppendLine("Where PositionStatusCode=@Param1 ");
                    SQL.AppendLine("And ADCode=@Param2 ");
                    SQL.AppendLine("And RegionCode=@Param3 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("    (StartDt Is Null And EndDt Is Null) Or ");
                    SQL.AppendLine("    (StartDt Is Not Null And EndDt Is Null And StartDt<=@Param4) Or ");
                    SQL.AppendLine("    (StartDt Is Null And EndDt Is Not Null And @Param4<=EndDt) Or ");
                    SQL.AppendLine("    (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@Param4 And @Param4<=EndDt) ");
                    SQL.AppendLine("); ");

                    Grd7.Cells[r, 8].Value = Sm.GetValueDec(SQL.ToString(),
                        Sm.GetGrdStr(Grd7, r, 6),
                        Sm.GetGrdStr(Grd7, r, 4),
                        Sm.GetLue(LueRegionCode),
                        Sm.GetDte(DteStartDt).Substring(0, 8)
                        );
                }
                Grd7.Cells[r, 9].Value = 0m;
                Grd7.Cells[r, 10].Value = 0m;
            }
            LueAllowance.Visible = false;
        }

        private void LueAllowance_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAllowance, new Sm.RefreshLue2(SetLueADCode), Sm.GetGrdStr(Grd7, fCell.RowIndex, 2));
        }

        private void LueAllowance_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (!Sm.IsDteEmpty(DteStartDt, "Start date") && 
                    !Sm.IsLueEmpty(LueRegionCode, "Region") && 
                    !Sm.IsLueEmpty(LueSite, "Site")) 
                    Sm.FormShowDialog(new FrmTravelRequest2Dlg(this, Sm.GetLue(LueSite)));
                }
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && 
                Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex) && 
                Sm.GetGrdStr(Grd7, e.RowIndex, 2).Length>0)
            {
                Sm.LueRequestEdit(ref Grd7, ref LueAllowance, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                SetLueADCode(ref LueAllowance, Sm.GetGrdStr(Grd7, e.RowIndex, 2));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 &&
                !Sm.IsDteEmpty(DteStartDt, "Start date") && 
                !Sm.IsLueEmpty(LueRegionCode, "Region") && 
                !Sm.IsLueEmpty(LueSite, "Site")) 
                Sm.FormShowDialog(new FrmTravelRequest2Dlg(this, Sm.GetLue(LueSite)));
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 8, 12, 13, 17, 18 }, e.ColIndex)) ComputeTotalMeal(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd2, e.RowIndex, 2), Grd2);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 8 }, e.ColIndex)) ComputeTotalDaily(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd3, e.RowIndex, 2), Grd3);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 8 }, e.ColIndex)) ComputeTotalCityTransport(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd4, e.RowIndex, 2), Grd4);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8, 9 }, e.ColIndex) &&
                !Sm.GetGrdBool(Grd5, e.RowIndex, 4)) 
                ComputeTotalTransport(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd5, e.RowIndex, 2), Grd5);

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (Sm.GetGrdBool(Grd5, e.RowIndex, 4))
                    Grd5.Cells[e.RowIndex, 10].Value = 0m;   
                else
                    ComputeTotalTransport(e.RowIndex);
                ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd5, e.RowIndex, 2), Grd5);
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8, 9 }, e.ColIndex) && !Sm.GetGrdBool(Grd6, e.RowIndex, 4)) 
                ComputeTotalAccomodation(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd6, e.RowIndex, 2), Grd6);

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (Sm.GetGrdBool(Grd6, e.RowIndex, 4))
                    Grd6.Cells[e.RowIndex, 10].Value = 0m;
                else
                    ComputeTotalAccomodation(e.RowIndex);
                ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd6, e.RowIndex, 2), Grd6);
            }
        }

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8, 9 }, e.ColIndex)) ComputeTotalOthers(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd7, e.RowIndex, 2), Grd7);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!BtnSave.Enabled) return;

            string EmpCode = string.Empty; 
            int mRow = 0;

            if ((mWebInd == "W" || TxtDocNo.Text.Length == 0) &&
                e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 2);
                    mRow = Grd1.SelectedRows[Index].Index;
                    break;
                }
            }

            if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (mWebInd == "W" || TxtDocNo.Text.Length == 0)
                {
                    Grd1.Rows.RemoveAt(mRow);

                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), EmpCode)) Grd2.Rows.RemoveAt(Row);
                    }
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 2), EmpCode)) Grd3.Rows.RemoveAt(Row);
                    }
                    for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd4, Row, 2), EmpCode)) Grd4.Rows.RemoveAt(Row);
                    }
                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd5, Row, 2), EmpCode)) Grd5.Rows.RemoveAt(Row);
                    }
                    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd6, Row, 2), EmpCode)) Grd6.Rows.RemoveAt(Row);
                    }
                    for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd7, Row, 2), EmpCode)) Grd7.Rows.RemoveAt(Row);
                    }
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r= Grd2.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd2, Grd2.SelectedRows[r].Index, 2);
                    Row = Grd2.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd2.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 4].Value = 0m;
                    }
                }
                ComputeSummaryAllowance();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r = Grd3.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd3, Grd3.SelectedRows[r].Index, 2);
                    Row = Grd3.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd3.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 5].Value = 0m;
                }
                ComputeSummaryAllowance();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r = Grd4.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd4, Grd4.SelectedRows[r].Index, 2);
                    Row = Grd4.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd4.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 6].Value = 0m;
                }
                ComputeSummaryAllowance();
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r = Grd5.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd5, Grd5.SelectedRows[r].Index, 2);
                    Row = Grd5.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd5.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 7].Value = 0m;
                }
                ComputeSummaryAllowance();
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r = Grd6.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd6, Grd6.SelectedRows[r].Index, 2);
                    Row = Grd6.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd6.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 8].Value = 0m;
                }
                ComputeSummaryAllowance();
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string EmpCode = string.Empty;
                int Row = 0;
                for (int r = Grd7.SelectedRows.Count - 1; r >= 0; r--)
                {
                    EmpCode = Sm.GetGrdStr(Grd7, Grd7.SelectedRows[r].Index, 2);
                    Row = Grd7.SelectedRows[r].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd7.Rows.RemoveAt(Row);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                            Grd1.Cells[r, 9].Value = 0m;
                }
                ComputeSummaryAllowance();
            }
        }

        #endregion

        #endregion

        #region Report Class

        class TravelRequest
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string TravelService { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal MealAllowance { get; set; }
            public decimal DailyAllowance { get; set; }
            public decimal CityTransport { get; set; }
            public decimal Transport { get; set; }
            public decimal Accomadation { get; set; }
            public decimal OtherAllowance { get; set; }
            public string PICName { get; set; }
            public string PosName { get; set; }
            public string PrintBy { get; set; }
            public string TotDt { get; set; }
            public decimal Total { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string MainCur { get; set; }
            public string Result { get; set; }
            public string Transportation { get; set; }
            public string HOInd { get; set; }
            public string CityCode { get; set; }
            public string PosName2 { get; set; }
            public string SignName { get; set; }
        }

        class TravelRequest2
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string TravelService { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string PICName { get; set; }
            public string PosName { get; set; }
            public string PrintBy { get; set; }
            public string CityName { get; set; }
            public string Result { get; set; }
            public string Transportation { get; set; }
            public string HOInd { get; set; }
            public string CityCode { get; set; }
            public string SignName { get; set; }
        }

        private class VoucherReqDtl
        {
            public string DocNo { get; set; }
            public string EmpNAme { get; set; }
        }


        private class VoucherReqHdr2
        {
            public string CompanyName { get; set; }
            public string DocNo { get; set; }
            public string DocNoVR { get; set; }
            public string DocDt { get; set; }
            public string DNo { get; set; }
            public string PICEmpName { get; set; }
            public string PICPosName { get; set; }
            public string PICGrdLvlName { get; set; }
            public string TravelService { get; set; }
            public string Transportation { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string result { get; set; }
            public string CityName { get; set; }
            public string PrintBy { get; set; }
            public string PrintDt { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string GrdLvlName { get; set; }
            public string Result { get; set; }
            public string RemarkHdr { get; set; }
            public string ProfitCenterName { get; set; }
            public string HOInd { get; set; }
            public string CityCode { get; set; }
            public string SiteName1 { get; set; }
            public string SiteName2 { get; set; }
            public string Duration { get; set; }
        }

        private class VoucherReqDtl2
        {
            public string VoucherRequestDocNo { get; set; }
            public string DocNo { get; set; }
            public string EmpNAme { get; set; }
            public string EmpPosName { get; set; }
            public string EmpPosStatusName { get; set; }
            public string EmpGrdLvlName { get; set; }
            public string EmpBankAcNo { get; set; }
            public string EmpBankAcName { get; set; }
        }

        private class VoucherReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class VoucherReqDtl4
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string AdName { get; set; }
            public decimal Amt { get; set; }
            public decimal Qty { get; set; }
            public decimal Total { get; set; }

        }
        #endregion        
    }

}
