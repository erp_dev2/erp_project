﻿#region Update
// 27/04/2017 [TKG] bug fixing pada saat memanggil aplikasi make to stock
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmPR : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmPRFind FrmFind;
        private bool mIsRemarkForApprovalMandatory = false;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPR(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Purchase Requisition";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtMakeToStockDocNo, TxtBomDocNo, TxtDocName, TxtItCode, 
                    TxtItName, TxtQty, TxtPlanningUomCode, TxtMaterialRequestDocNo
                }, true);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueOption(ref LueReqType, "ReqType");

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsRemarkForApprovalMandatory = Sm.GetParameter("IsPRRemarkForApprovalMandatory") == "Y";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo#",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "MTS DNo",
                        "Item's Code",

                        //6-10
                        "",
                        "Item's Name",
                        "Local Code",
                        "Outstanding"+ Environment.NewLine + "MTS Quantity",
                        "Quantity" + Environment.NewLine + "(Planning)",
                        
                        //11-15
                        "Planning" + Environment.NewLine + "UoM",
                        "Stock",
                        "Inventory" + Environment.NewLine + "UoM",
                        "Quantity" + Environment.NewLine + "(Purchase)",
                        "Purchase" + Environment.NewLine + "UoM",
                        
                        //16-17
                        "Usage" + Environment.NewLine + "Date",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        //1-5
                        50, 0, 20, 0, 100, 
                        //6-10
                        20, 200, 100, 100, 100, 
                        //11-15
                        80, 100, 80, 100, 80, 
                        //16-17
                        100, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 16 });
            Sm.GrdColButton(Grd1, new int[] { 3, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 8, 12, 13 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 11, 12, 13, 15, 16 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueReqType, LueDeptCode, MeeRemark
                    }, true);
                    BtnMakeToStockDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 10, 14, 17 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueReqType, LueDeptCode, MeeRemark
                    }, false);
                    BtnMakeToStockDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 10, 14, 17 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueReqType, LueDeptCode, TxtMakeToStockDocNo, 
                TxtBomDocNo, TxtDocName, TxtItCode, TxtItName, TxtPlanningUomCode, 
                TxtMaterialRequestDocNo, MeeRemark, DteUsageDt
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 12, 14 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPRFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PR", "TblPRHdr"),
                MaterialRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePRHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SavePRDtl(DocNo, Row));

            cml.Add(SaveMaterialRequest(DocNo, MaterialRequestDocNo));
            cml.Add(UpdateMakeToStockProcessInfo());

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsTxtEmpty(TxtMakeToStockDocNo, "Make to stock#", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsMakeToStockAlreadyCancelled() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeOutstanding();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "Item is empty.")) return true;
                
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine;
                    
                if (Sm.GetGrdDec(Grd1, Row, 10) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Planning) should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 10) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Planning) should not be greater than outstanding quantity.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Purchase) should be greater than 0.");
                    return true;
                }

                if (Sm.IsGrdValueEmpty(Grd1, Row, 16, false, Msg + "Usage date is empty.")) return true;
                if (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 17, false, Msg + "Remark is empty.")) return true;
            }
            return false;
        }

        private bool IsMakeToStockAlreadyCancelled()
        {
            if (Sm.IsDataExist(
                    "Select DocNo From TblMakeToStockHdr " +
                    "Where CancelInd='Y' And DocNo=@Param;", 
                    TxtMakeToStockDocNo.Text, 
                    "Make to stock# already cancelled.")) 
                return true;
            return false;

        }

        private MySqlCommand SavePRHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PR */ ");
            SQL.AppendLine("Insert Into TblPRHdr(DocNo, DocDt, ReqType, DeptCode, MakeToStockDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ReqType, @DeptCode, @MakeToStockDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", TxtMakeToStockDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePRDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PR */ ");
            SQL.AppendLine("Insert Into TblPRDtl(DocNo, DNo, CancelInd, MakeToStockDNo, ItCode, Qty1, Qty2, UsageDt, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @MakeToStockDNo, @ItCode, @Qty1, @Qty2, @UsageDt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MakeToStockDNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequest(string PRDocNo, string MaterialRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PR */ ");

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, DocDt, ReqType, DeptCode, PRDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @MaterialRequestDocNo, DocDt, ReqType, DeptCode, @PRDocNo, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPRHdr Where DocNo=@PRDocNo;");

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, Status, ProcessInd, PRDNo, ItCode, Qty, UsageDt, ");
            SQL.AppendLine("QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @MaterialRequestDocNo, DNo, CancelInd, 'O', 'O', DNo, ItCode, Qty2, UsageDt, ");
            SQL.AppendLine("Null, Null, 0, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPRDtl Where DocNo=@PRDocNo;");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select C.DocType, A.DocNo, B.DNo, C.DNo, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting C On A.DeptCode=C.DeptCode And C.DocType='MaterialRequest' ");
            SQL.AppendLine("Where A.DocNo=@MaterialRequestDocNo; ");

            SQL.AppendLine("Update TblMaterialRequestDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@MaterialRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='MaterialRequest' ");
            SQL.AppendLine("    And DocNo=@MaterialRequestDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PRDocNo", PRDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            return cm;
        }

        private MySqlCommand UpdateMakeToStockProcessInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PR */ ");

            SQL.AppendLine("Update TblMakeToStockDtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.MakeToStockDNo As DNo, Sum(T2.Qty1) As Qty1 ");
            SQL.AppendLine("    From TblPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPRDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And Locate(Concat('##', T2.MakeToStockDNo, '##'), @SelectedMakeToStockDNo)>0 ");
            SQL.AppendLine("    Where T1.MakeToStockDocNo=@MakeToStockDocNo ");
            SQL.AppendLine("    Group By T2.MakeToStockDNo ");
            SQL.AppendLine(") B On A.DNo=B.DNo ");
            SQL.AppendLine("Set A.ProcessInd2 = ");
            SQL.AppendLine("    Case When A.Qty-IfNull(B.Qty1, 0)<=0 Then 'F' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(B.Qty1, 0)=0 Then 'O' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo=@MakeToStockDocNo ");
            SQL.AppendLine("And Locate(Concat('##', A.DNo, '##'), @SelectedMakeToStockDNo)>0;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", TxtMakeToStockDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SelectedMakeToStockDNo", GetSelectedMakeToStockDNo());
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            UpdateCancelledItem();

            string DNo = "##XXX##";
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (!Sm.CompareStr(DNo, "##XXX##"))
            {
                if (IsMaterialRequestNotCancelled()) return;

                var cml = new List<MySqlCommand>();
                cml.Add(EditPRDtl(DNo));
                cml.Add(UpdateMakeToStockProcessInfo());
                Sm.ExecCommands(cml);
            }
            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select DNo From TblPRDtl Where DocNo=@DocNo And CancelInd='Y';"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                Grd1.Cells[Row, 1].Value = true;
                                Grd1.Cells[Row, 2].Value = true;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsMaterialRequestNotCancelled()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2) && 
                    Sm.GetGrdStr(Grd1, Row, 4).Length > 0 &&
                    IsMaterialRequestNotCancelled(Row)) 
                        return true;
            }
            return false;
        }

        private bool IsMaterialRequestNotCancelled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblMaterialRequestDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And PRDNo=@PRDNo ");
            SQL.AppendLine("And (CancelInd='Y' Or Status='C');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRDNo", Sm.GetGrdStr(Grd1, Row, 0));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                    "You need to cancel the material request.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPRDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPRDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        internal void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPRHdr(DocNo);
                ShowPRDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPRHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ReqType, A.DeptCode, A.MakeToStockDocNo, ");
            SQL.AppendLine("B.BomDocNo, C.DocName, D.ItCode, E.ItName, B.Qty, E.PlanningUomCode, ");
            SQL.AppendLine("F.DocNo As MaterialRequestDocNo, A.Remark ");
            SQL.AppendLine("From TblPRHdr A ");
            SQL.AppendLine("Left Join TblMakeToStockHdr B On A.MakeToStockDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblBomHdr C On B.BomDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblBomDtl2 D On C.DocNo=D.DocNo And D.ItType='1' ");
            SQL.AppendLine("Left Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblMaterialRequestHdr F On A.DocNo=F.PRDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ReqType", "DeptCode", "MakeToStockDocNo", "BomDocNo", 
                        
                        //6-10
                        "DocName", "ItCode", "ItName", "Qty", "PlanningUomCode", 
                        
                        //11-12
                        "MaterialRequestDocNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        TxtMakeToStockDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtBomDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtDocName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[8]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtPlanningUomCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtMaterialRequestDocNo.EditValue = Sm.DrStr(dr, c[11]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowPRDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.MakeToStockDNo, A.ItCode, B.ItName, B.ItCodeInternal, ");
            SQL.AppendLine("A.Qty1, B.PlanningUomCode2, B.InventoryUomCode, A.Qty2, B.PurchaseUomCode, ");
            SQL.AppendLine("A.UsageDt, A.Remark ");
            SQL.AppendLine("From TblPRDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "MakeToStockDNo", "ItCode", "ItName", "ItCodeInternal", 
                    
                    //6-10
                    "Qty1", "PlanningUomCode2", "InventoryUomCode", "Qty2", "PurchaseUomCode",    
                    
                    //11-12
                    "UsageDt", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Grd.Cells[Row, 9].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 12, 14 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsTxtEmpty(TxtMakeToStockDocNo, "Make to stock#", false))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPRDlg2(this, TxtMakeToStockDocNo.Text));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 10, 14, 16, 17 }, e.ColIndex))
                    {
                        if (e.ColIndex == 16) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 12, 14 });
                    }
                }
                else
                {
                    if (
                        e.ColIndex == 1 &&
                        (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 5));
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtMakeToStockDocNo, "Make to stock#", false))
                Sm.FormShowDialog(new FrmPRDlg2(this, TxtMakeToStockDocNo.Text));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 5));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 10, 14 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 17 }, e);
            if (e.ColIndex == 10)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 11), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                    Grd1.Cells[e.RowIndex, 14].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 10);
                else
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 5, 10, 14, 11, 15);
            }
            if (e.ColIndex == 14)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 11), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                    Grd1.Cells[e.RowIndex, 10].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 14);
                else
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 5, 14, 10, 15, 11);
            }
        }

        #endregion

        #region Additional Method

        internal string GetSelectedMakeToStockDNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeOutstanding()
        {
            string DNo = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.Qty-IfNull(C.Qty1, 0) As Qty ");
            SQL.AppendLine("From TblMakeToStockHdr A ");
            SQL.AppendLine("Inner Join TblMakeToStockDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Locate(Concat('##', B.DNo, '##'), @SelectedMakeToStockDNo)>0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.MakeToStockDNo As DNo, Sum(T2.Qty1) As Qty1 ");
            SQL.AppendLine("    From TblPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPRDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.MakeToStockDocNo=@MakeToStockDocNo ");
            SQL.AppendLine("    Group By T2.MakeToStockDNo ");
            SQL.AppendLine(") C On B.DNo=C.DNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@MakeToStockDocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", TxtMakeToStockDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SelectedMakeToStockDNo", GetSelectedMakeToStockDNo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "Qty" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DNo = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(DNo, Sm.GetGrdStr(Grd1, Row, 4)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //internal void ComputeQtyBasedOnConvertionFormula(
        //   string ConvertType, iGrid Grd, int Row, int ColItCode,
        //   int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(Sl.SetLueOption), "ReqType");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #region Button Event

        private void BtnMakeToStockDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPRDlg(this));
        }

        private void BtnMakeToStockDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtMakeToStockDocNo, "Make To Stock#", false))
            {
                var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                bool IsMTSStandard = Sm.IsDataExist(
                    "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                    TxtMakeToStockDocNo.Text);
                var f1 = new FrmMakeToStock(mMenuCode);
                if (IsMTSStandard)
                {
                    if (MenuCodeForMakeToStockStandard.Length > 0)
                    {
                        f1.mMenuCode = MenuCodeForMakeToStockStandard;
                        f1.Tag = MenuCodeForMakeToStockStandard;
                    }
                }
                else
                {
                    if (MenuCodeForMakeToStockMaklon.Length > 0)
                    {
                        f1.mMenuCode = MenuCodeForMakeToStockMaklon;
                        f1.Tag = MenuCodeForMakeToStockMaklon;
                    }
                }
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtMakeToStockDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
