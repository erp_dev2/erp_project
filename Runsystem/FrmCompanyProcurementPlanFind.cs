﻿#region update

/*
 * 02/11/2021 [SET/PHT] find menu transaksi baru untuk RUP (Rencana Umum Pengadaan)
 * 24/11/2021 [SET/PHT] BUG : Pada menu CPP, ketika insert dan loop milih item, UoM nya tidak sesuai dengan master item
 * 13/12/2021 [VIN/PHT] BUG: item tidak terfilter
 * 05/01/2022 [RIS/PHT] Menambah field dan kolom site.. dan juga terfilter berdasarkan group site dan param isFilterBySite
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyProcurementPlanFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCompanyProcurementPlan mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCompanyProcurementPlanFind(FrmCompanyProcurementPlan FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            Sl.SetLueYr(LueYr, "");
            Sl.SetLueSiteCode(ref LueSite, string.Empty, mFrmParent.mIsFilterBySite ? "Y" : "N");
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Year",
                        "Cancel",
                        "Site",
                        

                        //6-10
                        "Item",
                        "Metode",
                        "Quantity",
                        "UoM",
                        "Price",
                        

                        //11-15
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        

                        //16-20
                        "6",
                        "7",
                        "8",
                        "9",
                        "10",
                        

                        //21-25
                        "11",
                        "12",
                        "Remark",
                        "Created By",
                        "Created Date",
                        

                        //26-27
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 100, 60, 130,  

                        //6-10
                        100, 100, 100, 50, 100, 

                        //11-15
                        100, 100, 100, 100, 100, 

                        //16-20
                        100, 100, 100, 100, 100, 

                        //21-25
                        100, 100, 150, 130, 130, 

                        //26-27
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 25, 28 });
            Sm.GrdFormatTime(Grd1, new int[] { 26, 29 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt,  A.Yr, A.CancelInd, B.ItCode, C.ItName, B.ProcurementType, D.OptDesc Metode, B.Qty, B.UoMCode, ");
            SQL.AppendLine("B.UPrice, B.`1`, B.`2`, B.`3`, B.`4`, B.`5`, B.`6`, B.`7`, B.`8`, B.`9`, B.`10`, B.`11`, B.`12`, A.Remark, SiteName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblcompanyprocurementplanhdr A ");
            SQL.AppendLine("INNER JOIN tblcompanyprocurementplandtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN tblitem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("LEFT JOIN tbloption D ON B.ProcurementType = D.OptCode AND D.OptCat = 'ProcurementType' ");
            SQL.AppendLine("INNER JOIN tblsite E On A.SiteCode = E.SiteCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=E.SiteCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItem.Text, "C.ItName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSite), "A.SiteCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode); 

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "Yr", "CancelInd", "ItCode", "ItName", 

                            //6-10
                            "ProcurementType", "Metode", "Qty", "UoMCode", "UPrice", 

                            //11-15
                            "1", "2", "3", "4", "5",   
                            
                            //16-20
                            "6", "7", "8", "9", "10",  

                            //21-25
                            "11", "12", "Remark", "CreateBy", "CreateDt", 

                            //26-28
                            "LastUpBy", "LastUpDt", "SiteName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 28);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 16);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 17);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 18);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 19);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 20);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 21);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 22);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 23);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 24);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 25, 25);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 29, 27);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void LueSite_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSite, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSite_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion


    }
}
