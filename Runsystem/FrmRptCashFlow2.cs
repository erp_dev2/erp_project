﻿#region Update
/*
    18/12/2019 [VIN/VIR] new apps
    19/12/2019 [DITA/VIR] isi data reporting
    12/02/2020 [WED/YK] ambil dari tabel TblFicoSettingJournalToCBP
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlow2 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mThisYear = string.Empty,
            mOneYearAgo = string.Empty;

        private int[] mDecimalCol = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

        #endregion

        #region Constructor

        public FrmRptCashFlow2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetTimeStampVariable();
                SetGrd();
                GetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        #region Standar Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT *FROM TblRptDescription where doctype = 'CashFlow' order by description1 ");
            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPHdr ");
            SQL.AppendLine("Where Doctype = 'CashFlow' ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("Order By Right(Concat('00000000', Sequence ), 8), Code ");

            mSQL = SQL.ToString();

            return mSQL;
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Uraian",
                    "Detail", 
                    "Tipe",
                    "RKAP"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "RKAP"+Environment.NewLine+"SEMESTER I "+Environment.NewLine+"TAHUN "+ mThisYear,

                    //6-10
                    "SEMESTER I"+Environment.NewLine+"TAHUN "+ mOneYearAgo,
                    "SEMESTER I"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "% (4:1)",
                    "% (4:2)", 
                    "% (4:3)", 

                    //11-15
                    "% (4:4)",
                    "RKAP"+Environment.NewLine+"SEMESTER II "+Environment.NewLine+"TAHUN "+ mThisYear,
                    "SEMESTER II"+Environment.NewLine+"TAHUN "+ mOneYearAgo,
                    "SEMESTER II"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "% (4:1)",

                    //16-20
                    "% (4:2)", 
                    "% (4:3)", 
                    "% (4:4)",
                    "DocType",
                    "Code"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    200, 200, 200, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150, 

                    //16-20
                    150, 150, 150, 0, 0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20 });
            Sm.GrdFormatDec(Grd1, mDecimalCol, 0);
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                var cm = new MySqlCommand();
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                SetTimeStampVariable();
                SetGrd();

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(),
                new string[]
                {
                    //0
                    "Description1",
                    //1-4
                    "Description2", "Description3", "DocType", "Code"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Grd.Cells[Row, 4].Value = 0m;
                        Grd.Cells[Row, 5].Value = 0m;
                        Grd.Cells[Row, 6].Value = 0m;
                        Grd.Cells[Row, 7].Value = 0m;
                        Grd.Cells[Row, 8].Value = 0m;
                        Grd.Cells[Row, 9].Value = 0m;
                        Grd.Cells[Row, 10].Value = 0m;
                        Grd.Cells[Row, 11].Value = 0m;
                        Grd.Cells[Row, 12].Value = 0m;
                        Grd.Cells[Row, 13].Value = 0m;
                        Grd.Cells[Row, 14].Value = 0m;
                        Grd.Cells[Row, 15].Value = 0m;
                        Grd.Cells[Row, 16].Value = 0m;
                        Grd.Cells[Row, 17].Value = 0m;
                        Grd.Cells[Row, 18].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 4);
                    }, true, false, false, false
                );
                ProcessData();
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, mDecimalCol);
        }

        #endregion 

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<CashFlow>();

            SQL.AppendLine("SELECT T.DocType, T.Code, SUM(T.Amt) Amt, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.JAmt1Now) JAmt1Now, ");
            SQL.AppendLine("SUM(T.JAmt1Old) JAmt1Old, SUM(T.JAmt2Now) JAmt2Now, SUM(T.JAmt2Old) JAmt2Old ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, B.Amt, (B.Amt01 + B.Amt02 + B.Amt03 + B.Amt04 + B.Amt05 + B.Amt06) Amt1, ");
            SQL.AppendLine("    (B.Amt07 + B.Amt08 + B.Amt09 + B.Amt10 + B.Amt11 + B.Amt12) Amt2, 0.00 AS JAmt1Now, 0.00 JAmt1Old, ");
            SQL.AppendLine("    0.00 JAmt2Now, 0.00 JAmt2Old ");
            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND A.Yr = @ThisYear ");
            SQL.AppendLine("        AND B.Amt != 0 ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'CashFlow' ");
            SQL.AppendLine("        AND C.AcNo = D.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, IF(C.AcType = 'C', B.CAmt, B.DAmt) JAmt1Now, 0.00 JAmt1Old, ");
            SQL.AppendLine("    0.00 JAmt2Now, 0.00 JAmt2Old ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND LEFT(A.DocDt, 4) = @ThisYear ");
            SQL.AppendLine("        AND SUBSTR(A.DocDt, 5, 2) IN ('01', '02', '03', '04', '05', '06') ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'CashFlow' ");
            SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 JAmt1Now, 0.00 JAmt1Old, ");
            SQL.AppendLine("    IF(C.AcType = 'C', B.CAmt, B.DAmt) JAmt2Now, 0.00 JAmt2Old ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND LEFT(A.DocDt, 4) = @ThisYear ");
            SQL.AppendLine("        AND SUBSTR(A.DocDt, 5, 2) IN ('07', '08', '09', '10', '11', '12') ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'CashFlow' ");
            SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 JAmt1Now, IF(C.AcType = 'C', B.CAmt, B.DAmt) JAmt1Old, ");
            SQL.AppendLine("    0.00 JAmt2Now, 0.00 JAmt2Old ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND LEFT(A.DocDt, 4) = @OneYearAgo ");
            SQL.AppendLine("        AND SUBSTR(A.DocDt, 5, 2) IN ('01', '02', '03', '04', '05', '06') ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'CashFlow' ");
            SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 JAmt1Now, 0.00 JAmt1Old, ");
            SQL.AppendLine("    0.00 JAmt2Now, IF(C.AcType = 'C', B.CAmt, B.DAmt) JAmt2Old ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND LEFT(A.DocDt, 4) = @OneYearAgo ");
            SQL.AppendLine("        AND SUBSTR(A.DocDt, 5, 2) IN ('07', '08', '09', '10', '11', '12') ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'CashFlow' ");
            SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.DocType, T.Code; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@OneYearAgo", mOneYearAgo);
                Sm.CmParam<String>(ref cm, "@ThisYear", mThisYear);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocType",
                    
                    //1-5
                    "Code",
                    "Amt",
                    "Amt1",
                    "Amt2",
                    "JAmt1Now",
                    
                    //6-8
                    "JAmt1Old",
                    "JAmt2Now",
                    "JAmt2Old"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CashFlow()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt1 = Sm.DrDec(dr, c[3]),
                            Amt2 = Sm.DrDec(dr, c[4]),
                            JAmt1Now = Sm.DrDec(dr, c[5]),
                            JAmt1Old = Sm.DrDec(dr, c[6]),
                            JAmt2Now = Sm.DrDec(dr, c[7]),
                            JAmt2Old = Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach(var x in l)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 19) == x.DocType &&
                            Sm.GetGrdStr(Grd1, i, 20) == x.Code)
                        {
                            Grd1.Cells[i, 4].Value = x.Amt;
                            Grd1.Cells[i, 5].Value = x.Amt1;
                            Grd1.Cells[i, 6].Value = x.JAmt1Old;
                            Grd1.Cells[i, 7].Value = x.JAmt1Now;
                            Grd1.Cells[i, 8].Value = (x.Amt != 0) ? (x.JAmt1Now / x.Amt) * 100m : 0m;
                            Grd1.Cells[i, 9].Value = (x.Amt1 != 0) ? (x.JAmt1Now / x.Amt1) * 100m : 0m;
                            Grd1.Cells[i, 10].Value = (x.JAmt1Old != 0) ? (x.JAmt1Now / x.JAmt1Old) * 100m : 0m;
                            Grd1.Cells[i, 11].Value = (x.JAmt1Now != 0) ? (x.JAmt1Now / x.JAmt1Now) * 100m : 0m;
                            Grd1.Cells[i, 12].Value = x.Amt2;
                            Grd1.Cells[i, 13].Value = x.JAmt2Old;
                            Grd1.Cells[i, 14].Value = x.JAmt2Now;
                            Grd1.Cells[i, 15].Value = (x.Amt != 0) ? (x.JAmt2Now / x.Amt) * 100m : 0m;
                            Grd1.Cells[i, 16].Value = (x.Amt2 != 0) ? (x.JAmt2Now / x.Amt2) * 100m : 0m;
                            Grd1.Cells[i, 17].Value = (x.JAmt2Old != 0) ? (x.JAmt2Now / x.JAmt2Old) * 100m : 0m;
                            Grd1.Cells[i, 18].Value = (x.JAmt2Now != 0) ? (x.JAmt2Now / x.JAmt2Now) * 100m : 0m;

                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
            
        }

        #endregion 

        #endregion

        #region Class

        private class CashFlow
        {
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal JAmt1Now { get; set; }
            public decimal JAmt1Old { get; set; }
            public decimal JAmt2Now { get; set; }
            public decimal JAmt2Old { get; set; }
        }

        #endregion
    }
}
