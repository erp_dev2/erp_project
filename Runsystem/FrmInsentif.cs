﻿#region Update
 // 12/07/2018 [HAR] tambah parameter untuk bisa choose employee
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInsentif : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal bool mIsInsentifcanChooseEmployee = false;
        internal FrmInsentifFind FrmFind;

        #endregion

        #region Constructor

        public FrmInsentif(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Incentive";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's"+Environment.NewLine+"Name",
                        "Position",
                        "Minimum Daily"+Environment.NewLine+"Wages",

                        //6-8
                        "Wages"+Environment.NewLine+"Amount",
                        "Minimum Daily"+Environment.NewLine+"Wages Insentif",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        20, 80, 200, 180, 130,  
                        
                        //6-8
                        130, 130, 250
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });

            if (mIsInsentifcanChooseEmployee)
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2 }, false);
        }

        override protected void HideInfoInGrd()
        {
           Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 8 });
                    BtnWages.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 8 });
                    BtnWages.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    BtnWages.Enabled = false;
                    DteDocDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, TxtWages, MeeRemark });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 2);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInsentifFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
                ComputeInsentif();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdDec(Grd1, 0, 5) != 0)
                {
                    var MinWages = Sm.GetGrdDec(Grd1, 0, 5);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0) Grd1.Cells[Row, 5].Value = MinWages;
                }
                ComputeInsentif();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtWages.Text.Length > 0 && TxtDocNo.Text.Length==0)
            {
                Sm.FormShowDialog(new FrmInsentifDlg2(this, TxtWages.Text));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Insentif", "TblInsentifHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveInsentifHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveInsentifDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(DteDocDt, "Date", false) ||
                IsGrdEmpty() ||
                IsPWGDocNoAlreadyCancelled()
                ;
        }

        private bool IsPWGDocNoAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblPWGHdr Where DocNo=@Param And CancelInd='Y';",
                    TxtWages.Text,
                    "Wages document already cancelled."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveInsentifHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInsentifHdr (DocNo, DocDt, PwgDocNo, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, @PwgDocNo, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PwgDocNo", TxtWages.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveInsentifDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInsentifDtl(DocNo, DNo, EmpCode, MinWages, InsentifAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @MinWages, @InsentifAmt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@MinWages", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@InsentifAmt", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelInsentifHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblInsentifHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelInsentifHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInsentifHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowInsentifHdr(DocNo);
                ShowInsentifDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInsentifHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
            ref cm,
            "Select DocNo, CancelInd, DocDt, PwgDocNo, Remark From TblInsentifHdr Where DocNo=@DocNo;",
            new string[] { "DocNo", "CancelInd", "DocDt", "PwgDocNo", "Remark" },
            (MySqlDataReader dr, int[] c) =>
                {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y") ? true : false;
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                     TxtWages.EditValue = Sm.DrStr(dr, c[3]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                }, true
            );
        }

        private void ShowInsentifDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, ");
            SQL.AppendLine("A.MinWages, ");
            SQL.AppendLine("IfNull(A.MinWages, 0)-IfNull(A.InsentifAmt, 0) As AmtWages, ");
            SQL.AppendLine("A.InsentifAmt, A.Remark ");
            SQL.AppendLine("From TblInsentifDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "EmpCode", "EmpName", "PosName", "MinWages", "AmtWages", 
                    
                        //6-7
                        "InsentifAmt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowEmpWages(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, A.DocNo, B.EmpCode, C.EmpName, D.PosName, ");
            SQL.AppendLine("E.ParValue As MinWages,");
            SQL.AppendLine("B.Wages*B.HolidayWagesindex As AmtWages ");
            SQL.AppendLine("From TblPwgHdr A");
            SQL.AppendLine("Inner Join TblPwgdtl5 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode And C.ResignDt Is Null ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode='MinimumDailyWages' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "EmpCode", "EmpName", "PosName", "MinWages", "AmtWages", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    }, false, false, true, false
            );
            ComputeInsentif();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional method

        private void GetParameter()
        {
            mIsInsentifcanChooseEmployee = Sm.GetParameterBoo("IsInsentifcanChooseEmployee");
        }

        internal void ComputeInsentif()
        {
            decimal MinWages = 0m, AmtWages = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                MinWages = Sm.GetGrdDec(Grd1, Row, 5);
                AmtWages = Sm.GetGrdDec(Grd1, Row, 6);
                if ( Sm.GetGrdStr(Grd1, Row, 2).Length != 0 )
                {
                    Grd1.Cells[Row, 7].Value = MinWages - AmtWages;
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnWages_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInsentifDlg(this));
        }

        private void BtnWages2_Click(object sender, EventArgs e)
        {
            if (TxtWages.Text.Length != 0)
            {
                var f = new FrmPWG(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWages.Text;
                f.ShowDialog();
            }
        }

        #endregion
    }
}
