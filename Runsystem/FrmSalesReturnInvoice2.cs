﻿#region Update
/*
    21/08/2018 [TKG] New application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoice2 : RunSystem.FrmBase15
    {
        #region Field

        private string mMenuCode = string.Empty, mMainCurCode = string.Empty;
        private bool mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmSalesReturnInvoice2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Received#",
                        "DNo",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Quantity",

                        //6-10
                        "UoM",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "UoM",
                        "Amount Before Tax",
                        
                        //11-15
                        "Tax Invoice#",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "Tax Rate",
                        "Tax",
                        "Amount After Tax"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        130, 0, 100, 200, 80, 

                        //6-10
                        60, 60, 120, 60, 130, 
                        
                        //11-15
                        150, 100, 100, 130, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 10, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                int i = 0;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        i++;
                        cml.Add(SaveSalesReturnInvoice(r, i));
                    }
                }

                Sm.ExecCommands(cml);
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                ClearData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ClearData()
        {
            Sm.SetDteCurrentDate(DteDocDt);
            SetLueCtCode(ref LueCtCode);
            TxtLocalDocNo.EditValue = null;
            TxtTaxInvoiceDocument.EditValue = null;
            DteTaxInvoiceDt.EditValue = null;
            MeeRemark.EditValue = null;
            Sm.ClearGrd(Grd1, true);        
        }

        private MySqlCommand SaveSalesReturnInvoice(int r, int i)
        {
            var SQL = new StringBuilder();
            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesReturnInvoice", "TblSalesReturnInvoiceHdr", i.ToString());
            var CurCode = Sm.GetGrdStr(Grd1, r, 7);
            var DOCtDocDt = string.Empty;

            SQL.AppendLine("Insert Into TblSalesReturnInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, IncomingPaymentInd, CtCode, LocalDocNo, CurCode, RecvCtDocno, TaxInvDocument, TaxInvoiceDt, AmtBefTax, TaxRate, TaxAmt, TotalAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, Null, 'N', 'O', @CtCode, @LocalDocNo, @CurCode, @RecvCtDocno, @TaxInvDocument, @TaxInvoiceDt, @AmtBefTax, @TaxRate, @TaxAmt, @TotalAmt, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblRecvCtHdr Set SalesReturnInvoiceInd='F' ");
            SQL.AppendLine("Where DocNo=@RecvCtDocNo; ");

            SQL.AppendLine("Insert Into TblSalesReturnInvoiceDtl ");
            SQL.AppendLine("(DocNo, DNo, RecvCtDocNo, RecvCtDNo, ItCode, Qty, InventoryUomCode, CurCode, UPrice, PriceUomCode, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, '001', @RecvCtDocNo, @RecvCtDNo, @ItCode, @Qty, @InventoryUomCode, @CurCode, @UPrice, @PriceUomCode, ");
            SQL.AppendLine("@TaxInvDocument, @TaxInvoiceDt, @TaxRate, @AmtBefTax, @UserCode, CurrentDateTime()); ");

            if (mIsAutoJournalActived)
            {
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                    DOCtDocDt =
                        Sm.GetValue(
                            "Select DocDt From (" +
                            "Select E.DocDt " +
                            "From TblSalesReturnInvoiceHdr A " +
                            "Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo " +
                            "Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo " +
                            "Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='1' " +
                            "Inner join TblDOCtHdr E On D.DOCtDocNo=E.DocNo " +
                            "Where A.DocNo=@Param " +
                            "Union All " +
                            "Select E.DocDt " +
                            "From TblSalesReturnInvoiceHdr A " +
                            "Inner Join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo " +
                            "Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo " +
                            "Inner Join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' " +
                            "Inner Join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo " +
                            "Where A.DocNo=@Param " +
                            ") T Limit 1;",
                            DocNo);

                SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
                SQL.AppendLine("    JournalDocNo=");
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                SQL.AppendLine("Where DocNo=@DocNo;");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
                SQL.AppendLine("Select JournalDocNo, DocDt, ");
                SQL.AppendLine("Concat('Sales Return Invoice : ', DocNo) As JnDesc, ");
                SQL.AppendLine("@MenuCode As MenuCode, ");
                SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                SQL.AppendLine("CreateBy, CreateDt ");
                SQL.AppendLine("From TblSalesReturnInvoiceHdr ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And JournalDocNo Is Not Null; ");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                SQL.AppendLine("        Select B.ParValue As AcNo, ");

                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.AmtBefTax As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForSalesReturnInvoice' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select B.ParValue As AcNo, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.TaxAmt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.TotalAmt As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblSalesReturnInvoiceHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@RecvCtDocno", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd1, r, 12));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, r, 13));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd1, r, 15));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@RecvCtDNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode", Sm.GetGrdStr(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd1, r, 9));
            if (mIsAutoJournalActived)
            {
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParam<String>(ref cm, "@DOCtDocDt", DOCtDocDt);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 received document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblRecvCtHdr A ");
            SQL.AppendLine("Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.DNo=@Param2 ");
            SQL.AppendLine("Where A.SalesReturnInvoiceInd='O' ");
            SQL.AppendLine("And A.DocNo=@Param1; ");

            var Query = SQL.ToString();

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Received# is empty.")) return true;
                if (!Sm.IsDataExist(Query, Sm.GetGrdStr(Grd1, r, 1), Sm.GetGrdStr(Grd1, r, 2), string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Received# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine + Environment.NewLine +
                        "Invalid data (data already cancelled/already processed to sales invoice).");
                    return true;
                }
            }
            return false;
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesReturnInvoice2Dlg(this, Sm.GetLue(LueCtCode)));
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter && Grd1.CurRow.Index != Grd1.Rows.Count - 1)
                Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, Grd1.CurCell.Col.Index);

            if (e.KeyCode == Keys.Tab && Grd1.CurCell != null && Grd1.CurCell.RowIndex == Grd1.Rows.Count - 1)
            {
                int LastVisibleCol = Grd1.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd1.Cols.Count - 1; Col++)
                    if (Grd1.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd1.CurCell.Col.Order == LastVisibleCol)
                    BtnSave.Focus();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesReturnInvoice2Dlg(this, Sm.GetLue(LueCtCode)));
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
            SQL.AppendLine("From TblCustomer ");
            SQL.AppendLine("Where CtCode In ( ");
            SQL.AppendLine("    Select Distinct A.CtCode ");
            SQL.AppendLine("    From TblRecvCtHdr A ");
            SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.SalesReturnInvoiceInd='O' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By CtName; ");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sm.ClearGrd(Grd1, true);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtTaxInvoiceDocument_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxInvoiceDocument);
        }

        #endregion

        #endregion
    }
}
