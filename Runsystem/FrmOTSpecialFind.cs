﻿#region Update
/*
    31/10/2019 [HAR/SRN]  
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOTSpecialFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOTSpecial mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmOTSpecialFind(FrmOTSpecial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt,");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'OutStanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancel' End ");
            SQL.AppendLine("As StatusDesc, A.DeptCode, A.SiteCode,  ");
            SQL.AppendLine("C.DeptName, D.SiteName, E.DivisionName, if(B.SpecialIndCode = '1', I.Levelname, G.PositionStatusName) As Source, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblOTSpecialHdr A ");
            SQL.AppendLine("Inner Join TblOTSpecialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C ");
            SQL.AppendLine("    On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join TblDivision E On  A.DivisionCode = E.DivisionCode ");
            SQL.AppendLine("Inner Join tblparameter F on 0=0 And F.parCode = 'OTSpecialSource' ");
            SQL.AppendLine("Left jOin TblPositionStatus G On  B.SourceCode = G.PositionStatusCode   ");
            SQL.AppendLine("Left jOin TblLevelHdr I On  B.SourceCode = I.LevelCode  ");
            SQL.AppendLine("Where 0=0 ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Department",
                        "Site",
                        
                        //6-10
                        "Division",
                        "Source",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 

                        //11-14
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 100, 150, 150, 
                        
                        //6-10
                        150, 150, 100, 100, 100, 

                        //11-13
                        100, 100, 100, 
                    }
                );
           Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSourceCode.Text, "T.Source", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.CreateDt;",
                        new string[]
                        {
                            //0
                            "DocNo",
                            
                            //1-5
                            "DocDt", "StatusDesc", "DeptName", "SiteName", "DivisionName", 
                            
                            //6-10
                            "Source",  "CreateBy", "CreateDt", "LastUpBy",  "LastUpDt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkSourceCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }


        private void TxtSourceCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }


        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }
      
        #endregion

        
    }
}
