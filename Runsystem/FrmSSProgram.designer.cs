﻿namespace RunSystem
{
    partial class FrmSSProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSSProgram));
            this.ChkFamilyInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtSSPName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtSSPCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCCGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAcDesc1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo1 = new System.Windows.Forms.Label();
            this.BtnAcNo1 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFamilyInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnAcNo1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueCCGrpCode);
            this.panel2.Controls.Add(this.TxtAcDesc1);
            this.panel2.Controls.Add(this.TxtAcNo1);
            this.panel2.Controls.Add(this.LblAcNo1);
            this.panel2.Controls.Add(this.ChkFamilyInd);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtSSPName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtSSPCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 160);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 160);
            this.panel3.Size = new System.Drawing.Size(772, 313);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 313);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // ChkFamilyInd
            // 
            this.ChkFamilyInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFamilyInd.Location = new System.Drawing.Point(143, 132);
            this.ChkFamilyInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFamilyInd.Name = "ChkFamilyInd";
            this.ChkFamilyInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFamilyInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFamilyInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFamilyInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFamilyInd.Properties.Appearance.Options.UseFont = true;
            this.ChkFamilyInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFamilyInd.Properties.Caption = "Can be used by family";
            this.ChkFamilyInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFamilyInd.Size = new System.Drawing.Size(153, 22);
            this.ChkFamilyInd.TabIndex = 24;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(143, 110);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 350;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(414, 20);
            this.MeeRemark.TabIndex = 22;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(92, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSSPName
            // 
            this.TxtSSPName.EnterMoveNextControl = true;
            this.TxtSSPName.Location = new System.Drawing.Point(143, 26);
            this.TxtSSPName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSPName.Name = "TxtSSPName";
            this.TxtSSPName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSSPName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSPName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSPName.Properties.Appearance.Options.UseFont = true;
            this.TxtSSPName.Properties.MaxLength = 80;
            this.TxtSSPName.Size = new System.Drawing.Size(414, 20);
            this.TxtSSPName.TabIndex = 15;
            this.TxtSSPName.Validated += new System.EventHandler(this.TxtSSPName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(101, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSSPCode
            // 
            this.TxtSSPCode.EnterMoveNextControl = true;
            this.TxtSSPCode.Location = new System.Drawing.Point(143, 5);
            this.TxtSSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSPCode.Name = "TxtSSPCode";
            this.TxtSSPCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSPCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSPCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSSPCode.Properties.MaxLength = 16;
            this.TxtSSPCode.Size = new System.Drawing.Size(159, 20);
            this.TxtSSPCode.TabIndex = 13;
            this.TxtSSPCode.Validated += new System.EventHandler(this.TxtSSPCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(104, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(22, 92);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 14);
            this.label6.TabIndex = 37;
            this.label6.Text = "Cost Center\'s Group";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCGrpCode
            // 
            this.LueCCGrpCode.EnterMoveNextControl = true;
            this.LueCCGrpCode.Location = new System.Drawing.Point(143, 89);
            this.LueCCGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCGrpCode.Name = "LueCCGrpCode";
            this.LueCCGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCGrpCode.Properties.DropDownRows = 30;
            this.LueCCGrpCode.Properties.NullText = "[Empty]";
            this.LueCCGrpCode.Properties.PopupWidth = 450;
            this.LueCCGrpCode.Size = new System.Drawing.Size(414, 20);
            this.LueCCGrpCode.TabIndex = 38;
            this.LueCCGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueCCGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCGrpCode.EditValueChanged += new System.EventHandler(this.LueCCGrpCode_EditValueChanged);
            // 
            // TxtAcDesc1
            // 
            this.TxtAcDesc1.EnterMoveNextControl = true;
            this.TxtAcDesc1.Location = new System.Drawing.Point(143, 68);
            this.TxtAcDesc1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc1.Name = "TxtAcDesc1";
            this.TxtAcDesc1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc1.Properties.MaxLength = 80;
            this.TxtAcDesc1.Size = new System.Drawing.Size(414, 20);
            this.TxtAcDesc1.TabIndex = 36;
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(143, 47);
            this.TxtAcNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.MaxLength = 20;
            this.TxtAcNo1.Size = new System.Drawing.Size(159, 20);
            this.TxtAcNo1.TabIndex = 17;
            // 
            // LblAcNo1
            // 
            this.LblAcNo1.AutoSize = true;
            this.LblAcNo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo1.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo1.Location = new System.Drawing.Point(8, 51);
            this.LblAcNo1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo1.Name = "LblAcNo1";
            this.LblAcNo1.Size = new System.Drawing.Size(131, 14);
            this.LblAcNo1.TabIndex = 16;
            this.LblAcNo1.Text = "COA Account# (Debt)";
            this.LblAcNo1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo1
            // 
            this.BtnAcNo1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo1.Appearance.Options.UseBackColor = true;
            this.BtnAcNo1.Appearance.Options.UseFont = true;
            this.BtnAcNo1.Appearance.Options.UseForeColor = true;
            this.BtnAcNo1.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo1.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo1.Image")));
            this.BtnAcNo1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo1.Location = new System.Drawing.Point(307, 46);
            this.BtnAcNo1.Name = "BtnAcNo1";
            this.BtnAcNo1.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo1.TabIndex = 39;
            this.BtnAcNo1.ToolTip = "Choose COA\'s Account";
            this.BtnAcNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo1.ToolTipTitle = "Run System";
            this.BtnAcNo1.Click += new System.EventHandler(this.BtnAcNo1_Click);
            // 
            // FrmSSProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmSSProgram";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFamilyInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkFamilyInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtSSPName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtSSPCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueCCGrpCode;
        private DevExpress.XtraEditors.TextEdit TxtAcDesc1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo1;
        private System.Windows.Forms.Label LblAcNo1;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo1;
    }
}