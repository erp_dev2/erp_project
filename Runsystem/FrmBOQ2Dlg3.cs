﻿﻿#region Update
/*
    14/10/2019 [WED/IMS] item nya ambil dari BOM with CO-Product, tab Result
    07/11/2019 [WED/IMS] tambah untuk kolom percentage di form master
    25/11/2019 [DITA/IMS] query ItType dari table BOM dihapus
    16/02/2020 [TKG/IMS] tidak menggunakan BOM, langsung menggunakan item 
    24/09/2020 [DITA/IMS] item yg sama, bisa dipilih lagi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOQ2 mFrmParent;
        private string mSQL = string.Empty;
        private string mItemType = string.Empty; //1=service 2=Inventory

        #endregion

        #region Constructor

        public FrmBOQ2Dlg3(FrmBOQ2 FrmParent, string ItemType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItemType = ItemType;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code", 
                    "Item's Name",
                    "Local Code",
                    "Purchase UoM",

                    //6-7
                    "Specification",
                    "Category"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 200, 100, 100, 
                    
                    //6-8
                    250, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ItCodeInternal, A.PurchaseUomCode, A.Specification, B.ItCtName ");
            SQL.AppendLine("From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.ItCtCode=B.ItCtCode ");
            if (mItemType=="1")
                SQL.AppendLine("And A.ServiceItemInd='Y' ");
            else
                SQL.AppendLine("And A.InventoryItemInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, ItCode = string.Empty;
                var cm = new MySqlCommand();

                //if (mItemType == "1")
                //{
                //    if (mFrmParent.Grd2.Rows.Count >= 1)
                //    {
                //        for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                //        {
                //            ItCode = Sm.GetGrdStr(mFrmParent.Grd2, r, 3);
                //            if (ItCode.Length != 0)
                //            {
                //                if (Filter.Length > 0) Filter += " And ";
                //                Filter += "(A.ItCode<>@ItCode0" + r.ToString() + ") ";
                //                Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    if (mFrmParent.Grd3.Rows.Count >= 1)
                //    {
                //        for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                //        {
                //            ItCode = Sm.GetGrdStr(mFrmParent.Grd3, r, 3);
                //            if (ItCode.Length != 0)
                //            {
                //                if (Filter.Length > 0) Filter += " And ";
                //                Filter += "(A.ItCode<>@ItCode0" + r.ToString() + ") ";
                //                Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                //            }
                //        }
                //    }
                //}

                if (Filter.Length != 0) 
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By A.ItName; ",
                    new string[] 
                    { 
                        "ItCode", 
                        "ItName", "ItCodeInternal", "PurchaseUomCode", "Specification", "ItCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) /*&& !IsItemAlreadyChosen(Row)*/ )
                    {
                       // if (IsChoose == false) IsChoose = true;

                        Row2 = Row;
                        if (mItemType == "1")
                        {
                            Row1 = mFrmParent.Grd2.Rows.Count - 1;
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 6);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19 });
                            mFrmParent.Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19 });
                        }
                        else
                        {
                            Row1 = mFrmParent.Grd3.Rows.Count - 1;
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 1, Grd1, Row2, 6);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, Row1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
                            mFrmParent.Grd3.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
                        }
                    }
                }
            }

           // if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItemAlreadyChosen(int Row)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Row, 2);

            if (mItemType == "1")
            {
                for (int r = 0; r < mFrmParent.Grd2.Rows.Count - 1; r++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, r, 3), ItCode)) return true;
            }
            if (mItemType == "2")
            {
                for (int r = 0; r < mFrmParent.Grd3.Rows.Count - 1; r++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 3), ItCode)) return true;
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        } 

        #endregion

        #endregion

    }
}
