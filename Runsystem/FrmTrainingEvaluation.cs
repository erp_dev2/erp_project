﻿#region Update
/*  
    03/01/2018 [HAR] Hapus informasi competence, UpdateEmployeeCompetence disable dulu
    29/03/2018 [WED] tambah kolom result, dari option TrainingEvaluationResult
    29/03/2018 [WED] employee yang muncul adalah dari Training Request (training type : E) dan Training Assignment (training type : I)
    15/08/2018 [WED] tambah inputan MinScore dari Training, Status di kolom detail nya secara default terisi berdasarkan MinScore Training
    16/11/2018 [HAR] bug : employee yang sdh di evaluasi masih muncul
    27/11/2018 [HAR] tambah remark detail
    14/04/2019 [TKG] otomatis update employee's training
    11/03/2020 [IBL/SIER] tambah menu upolad dan download
 *  19/05/2020 [HAR/HIN] tambah validasi filter site hr
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingEvaluation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool mIsFilterByDeptHR = false, mIsFilterBySiteHR = false;
        internal decimal mMinScore = 0m;
        internal FrmTrainingEvaluationFind FrmFind;

        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;

        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmTrainingEvaluation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Training Evaluation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueOption(ref LueTrainingEvaluationResult, "TrainingEvaluationResult");
                LueTrainingEvaluationResult.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd()
        {        
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Document"+Environment.NewLine+"Training Source",
                        "TSDno",
                        "Training Type",
                        "Employee Code",
                        "Employee",
                        
                        //6-10 
                        "Department",
                        "Position",
                        "Gender",
                        "Score",
                        "ResultCode",

                        //11-12
                        "Result",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 40, 120, 100, 180, 
                        
                        //6-10
                        180, 180, 100, 80, 0, 
                        
                        //11
                        120, 150
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, TxtTrainingCode, TxtTrainingName, MeeCancelReason, ChkCancelInd, MeeRemark, TxtFile, TxtFile2, TxtFile3, }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                    TxtDocNo.Focus();
                    BtnTrainingCode.Enabled = false;

                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                   
                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;

                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;

                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;

                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 9, 11, 12  });
                    DteDocDt.Focus();
                    BtnTrainingCode.Enabled = true;

                    BtnTrainingCode.Enabled = true;
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = true;
                    BtnFile2.Enabled = true;
                    BtnDownload2.Enabled = true;
                    BtnFile3.Enabled = true;
                    BtnDownload3.Enabled = true;
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;

                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason
                    }, false);
                    DteDocDt.Focus();
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            mMinScore = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtTrainingCode, TxtTrainingName,  MeeCancelReason, MeeRemark, 
                LueTrainingEvaluationResult, TxtFile, TxtFile2, TxtFile3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtMinScore }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();

            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;

            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            mMinScore = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTrainingCode, TxtTrainingName, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtMinScore }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9 });
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11 }, e.ColIndex) && BtnSave.Enabled)
            {
                LueRequestEdit(Grd1, LueTrainingEvaluationResult, ref fCell, ref fAccept, e);
                Sl.SetLueOption(ref LueTrainingEvaluationResult, "TrainingEvaluationResult");
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd1, 0, 11).Length != 0)
                {
                    var Value1 = Sm.GetGrdStr(Grd1, 0, 10);
                    var Value2 = Sm.GetGrdStr(Grd1, 0, 11);
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 4).Length != 0)
                        {
                            Grd1.Cells[r, 10].Value = Value1;
                            Grd1.Cells[r, 11].Value = Value2;
                        }
                    }
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 9)
            {
                decimal mScore = 0m;
                Grd1.Cells[e.RowIndex, 10].Value = null;
                Grd1.Cells[e.RowIndex, 11].Value = null;
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 9) != 0) mScore = Sm.GetGrdDec(Grd1, e.RowIndex, 9);
                if (mScore >= mMinScore)
                {
                    Grd1.Cells[e.RowIndex, 10].Value = "1";
                    Grd1.Cells[e.RowIndex, 11].Value = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'TrainingEvaluationResult' And OptCode = '1'; ");
                }
                else
                {
                    Grd1.Cells[e.RowIndex, 10].Value = "2";
                    Grd1.Cells[e.RowIndex, 11].Value = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'TrainingEvaluationResult' And OptCode = '2'; ");
                }
            }
        }

      
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainingEvaluationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TrainingEvaluation", "TblTrainingEvaluationHdr");

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveTrainingEvaluationHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveTrainingEvaluationDtl(DocNo, Row));
                    //diremark dulu
                    //cml.Add(UpdateEmployeeCompetence(Row));
                }
            }
            cml.Add(SaveEmployeeTraining(DocNo));
            Sm.ExecCommands(cml);

            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);
            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
            }

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsUploadFileNotValid() ||
                IsUploadFileNotValid2() ||
                IsUploadFileNotValid3() ||
                IsGrdEmpty() ||
                IsGrdValueInvalid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdValueInvalid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10).Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Result is empty.");
                        Sm.FocusGrd(Grd1, Row, 11);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 training.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested training data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveTrainingEvaluationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingEvaluationHdr ");
            SQL.AppendLine("(DocNo, DocDt, TrainingCode, CancelInd, CancelReason, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @TrainingCode, 'N', @CancelReason, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
            Sm.CmParam<String>(ref cm, "@CancelReasonRemark", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveTrainingEvaluationDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingEvaluationDtl");
            SQL.AppendLine("(DocNo, DNo, TrainingSourceDocNo, TrainingSourceDNo, TrainingType, Score, Result, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @TrainingSourceDocNo, @TrainingSourceDNo, @TrainingType, @Score, @Result, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainingSourceDocNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@TrainingSourceDNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@TrainingType", Sm.Left(Sm.GetGrdStr(Grd1, Row, 3), 1));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@EmpCode", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Result", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

         private MySqlCommand SaveEmployeeTraining(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeTraining ");
            SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Yr, TrainingEvaluationDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select C.EmpCode, Right(Concat('00', IfNull(J.DNo, 0)+1), 3) As DNo, A.TrainingCode,  ");
            SQL.AppendLine("Group_Concat(Distinct F.TrainerName Order By F.TrainerName Separator ', ') As TrainerName, ");
            SQL.AppendLine("I.SiteName, Left(C.TrainingSchDt, 4) As Yr, A.DocNo, D.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblTrainingEvaluationHdr A ");
            SQL.AppendLine("Inner Join TblTrainingEvaluationDtl B On A.DocNo=B.DocNo And B.Result='1' ");
            SQL.AppendLine("Inner Join TblTrainingAssignmentDtl C On B.TrainingSourceDocNo=C.DocNo And B.TrainingSourceDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblTrainingRequestHdr D On C.TrainingRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblTrainingRequestDtl E On C.TrainingRequestDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblTrainer F On E.TrainerCode=F.TrainerCode ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select T3.EmpCode, Max(T3.TrainingSchDt) TrainingSchDt ");
	        SQL.AppendLine("    From TblTrainingEvaluationHdr T1 ");
	        SQL.AppendLine("    Inner Join TblTrainingEvaluationDtl T2 On T1.DocNo=T2.DocNo And T2.Result='1' ");
	        SQL.AppendLine("    Inner Join TblTrainingAssignmentDtl T3 On T2.TrainingSourceDocNo=T3.DocNo And T2.TrainingSourceDNo=T3.DNo ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
	        SQL.AppendLine("    And T1.DocNo=@DocNo ");
	        SQL.AppendLine("    Group By T3.EmpCode ");
            SQL.AppendLine(") G On C.EmpCode=G.EmpCode And C.TrainingSchDt=G.TrainingSchDt ");
            SQL.AppendLine("Inner Join TblTrainingAssignmentHdr H On B.TrainingSourceDocNo=H.DocNo  ");
            SQL.AppendLine("Left Join TblSite I On H.SiteCode=I.SiteCode "); 
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select EmpCode, Max(DNo) As DNo ");
	        SQL.AppendLine("    From TblEmployeeTraining ");
	        SQL.AppendLine("    Where EmpCode In ( ");
		    SQL.AppendLine("        Select Distinct T3.EmpCode ");
		    SQL.AppendLine("        From TblTrainingEvaluationHdr T1 ");
		    SQL.AppendLine("        Inner Join TblTrainingEvaluationDtl T2 On T1.DocNo=T2.DocNo And T2.Result='1' ");
		    SQL.AppendLine("        Inner Join TblTrainingAssignmentDtl T3 On T2.TrainingSourceDocNo=T3.DocNo And T2.TrainingSourceDNo=T3.DNo ");
		    SQL.AppendLine("        Where T1.CancelInd='N' ");
		    SQL.AppendLine("        And T1.DocNo=@DocNo ");
	        SQL.AppendLine("    ) ");
	        SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") J On C.EmpCode=J.EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Group By C.EmpCode, J.DNo, A.TrainingCode, I.SiteName, Left(C.TrainingSchDt, 4), A.DocNo, D.Remark ");
            SQL.AppendLine("Order By C.EmpCode; "); 

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployeeCompetence(int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Select A.EmpCode, @DNo, @CompetenceCode, 'Training Evaluation', @Score, @CreateBy, CurrentDateTime() ");
            SQLDtl3.AppendLine("From TblEmployee A ");
            SQLDtl3.AppendLine("Where (A.ResignDt Is Null And A.EmpCode = @EmpCode) ");
            SQLDtl3.AppendLine("On Duplicate Key ");
            SQLDtl3.AppendLine("    Update ");
            SQLDtl3.AppendLine("    CompetenceCode = @CompetenceCode, Score = @Score, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetValue("Select CompetenceCode From TblTraining Where TrainingCode = '"+TxtTrainingCode.Text+"' "));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTrainingEvaluation());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation");
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblTrainingEvaluationHdr " +
                    "Where CancelInd='Y'  And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelTrainingEvaluation()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingEvaluationHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ; ");

            SQL.AppendLine("Delete From TblEmployeeTraining ");
            SQL.AppendLine("Where TrainingEvaluationDocNo Is Not Null ");
            SQL.AppendLine("And TrainingEvaluationDocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateTEFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingEvaluationHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateTEFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingEvaluationHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateTEFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingEvaluationHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTrainingEvaluationHdr(DocNo);
                ShowTrainingEvaluationDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTrainingEvaluationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TrainingCode, B.TrainingName, A.CancelInd, A.CancelReason, A.Remark, B.MinScore, ");
            SQL.AppendLine("A.FileName, A.FileName2, A.FileName3");
            SQL.AppendLine("From TblTrainingEvaluationHdr A ");
            SQL.AppendLine("Inner Join TblTraining B On A.TrainingCode = B.TrainingCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        "DocNo", 
                        "DocDt", "TrainingCode", "TrainingName",  "CancelInd","CancelReason",
                        "Remark", "MinScore", "FileName", "FileName2", "FileName3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtTrainingCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtTrainingName.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[4]) == "Y");
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                        TxtMinScore.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        mMinScore = Sm.DrDec(dr, c[7]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[9]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

      
        private void ShowTrainingEvaluationDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select X.Dno, X.TrainingSourceDocNo, X.TrainingSourceDNo, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("D.DeptName, C.PosName, E.OptDesc As Gender, X.Score, X.Result, F.OptDesc As TrainingEvaluationResult, ");
                SQL.AppendLine("Case X.TrainingType When 'I' Then 'Internal' When 'E' Then 'External' End As TrainingType, X.Remark ");
                SQL.AppendLine("From TblTrainingEvaluationDtl X ");
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select T1.DocNo, T1.DNo, T2.EmpCode ");
	            SQL.AppendLine("    From TblTrainingEvaluationDtl T1 ");
	            SQL.AppendLine("    Inner Join TblTrainingRequestDtl2 T2 On T1.TrainingSourceDocNo = T2.DocNo And T1.TrainingSourceDNo = T2.DNo ");
	            SQL.AppendLine("    Where T1.DocNo = @DocNo ");
	            SQL.AppendLine("    Union All ");
	            SQL.AppendLine("    Select T3.DocNo, T3.DNo, T4.EmpCode ");
	            SQL.AppendLine("    From TblTrainingEvaluationDtl T3 ");
	            SQL.AppendLine("    Inner Join TblTrainingAssignmentDtl T4 On T3.TrainingSourceDocNo = T4.DocNo And T3.TrainingSourceDNo = T4.DNo ");
	            SQL.AppendLine("    Where T3.DocNo = @DocNo ");
                SQL.AppendLine(") A On X.DocNo = A.DocNo And X.DNo = A.DNo ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQL.AppendLine("Inner Join TblOption E On B.gender=E.OptCode And E.Optcat = 'Gender' ");
                SQL.AppendLine("Inner Join TblOption F On X.Result = F.OptCode And F.OptCat = 'TrainingEvaluationResult' ");
                SQL.AppendLine("Order By X.DNo; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "TrainingSourceDocNo", "TrainingSourceDNo", "TrainingType", "EmpCode", "EmpName", 
                        //6-10
                        "DeptName", "PosName", "Gender", "Score", "Result", 
                        //11
                        "TrainingEvaluationResult", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowTrainingSourceData(string TrainingCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@TrainingCode", TrainingCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.* From ( ");
                SQL.AppendLine("Select X.DocNo, A.Dno, X.TrainingType, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("D.DeptName, C.PosName, E.OptDesc As Gender,  A.Remark ");
                SQL.AppendLine("From  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select T1.DocNo, ");
                SQL.AppendLine("    Case T2.TrainingType When 'I' Then 'Internal' When 'E' Then 'External' End As TrainingType ");
	            SQL.AppendLine("    From TblTrainingRequestHdr T1 ");
	            SQL.AppendLine("    Inner Join TblTraining T2 On T1.TrainingCode = T2.TrainingCode And T2.TrainingType = 'E' ");
	            SQL.AppendLine("    Where T1.CancelInd = 'N' And T1.TrainingCode = @TrainingCode ");
                SQL.AppendLine(") X ");
                SQL.AppendLine("Inner Join TblTrainingRequestDtl2 A On X.DocnO = A.DocnO ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQL.AppendLine("Inner Join TblOption E On B.gender=E.OptCode And E.Optcat = 'Gender' ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.DocNo, T1.DNo, T1.TrainingType, T1.EmpCode, T2.EmpName,  ");
                SQL.AppendLine("T3.DeptName, T4.PosName, T5.OptDesc As Gender, T1.Remark ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select A.DocNo, A.DNo, A.EmpCode, A.Remark, ");
                SQL.AppendLine("    Case C.TrainingType When 'I' Then 'Internal' When 'E' Then 'External' End As TrainingType ");
	            SQL.AppendLine("    From TblTrainingAssignmentDtl A ");
	            SQL.AppendLine("    Inner Join TblTrainingAssignmentHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblTraining C On A.TrainingCode = C.TrainingCode And C.TrainingType = 'I' And C.TrainingCode = @TrainingCode ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment T3 On T2.DeptCode = T3.DeptCode ");
                SQL.AppendLine("Left Join TblPosition T4 On T2.PosCode = T4.PosCode ");
                SQL.AppendLine("Inner Join TblOption T5 On T2.Gender = T5.OptCode And T5.OptCat = 'Gender' ");
                SQL.AppendLine(") T ");

                SQL.AppendLine("Where concat(T.DocNo, T.Dno, T.EmpCode) not in ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select Concat(ifnull(Z.TrainingSourceDocNo, ''), ifnull(Z.TrainingSourceDno, ''), ifnull(Z.EmpCode, '')) As kode ");
	            SQL.AppendLine("    From ( ");
		        SQL.AppendLine("        Select B.TrainingSourceDocNo, B.TrainingSourceDno, if(B.trainingtype = 'I',  C.empCode, D.EmpCode) As EmpCode ");
		        SQL.AppendLine("        From tbltrainingEvaluationhdr A ");
		        SQL.AppendLine("        Inner Join tbltrainingEvaluationDtl B On A.DocNo = b.DocnO ");
		        SQL.AppendLine("        Left Join tbltrainingAssignmentDtl C On B.TrainingSourceDocNO = C.DocNo ");
		        SQL.AppendLine("        And B.TrainingSourceDNo = C.DNo ");
		        SQL.AppendLine("        And B.TrainingType = 'I' ");
		        SQL.AppendLine("        left Join tbltrainingrequestDtl2 D On B.TrainingSourceDocNO = D.DocNo ");
		        SQL.AppendLine("        And B.TrainingSourceDNo = D.DNo ");
		        SQL.AppendLine("        And B.TrainingType = 'E' ");
		        SQL.AppendLine("        Where A.CancelInd = 'N' ");
	            SQL.AppendLine("    )Z ");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.TrainingType Desc, T.DocNo, T.DNo, T.EmpName ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "Dno", "TrainingType", "EmpCode", "EmpName", "DeptName", 
                        //6-7
                        "PosName", "Gender"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Grd1.Cells[Row, 9].Value = 0;
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateTEFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateTEFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateTEFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblTrainingEvaluationHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblTrainingEvaluationHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblTrainingEvaluationHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnTrainingCode_Click(object sender, EventArgs e)
        {
            ClearData2();
            Sm.FormShowDialog(new FrmTrainingEvaluationDlg(this));
        }

        private void LueTrainingEvaluationResult_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingEvaluationResult, new Sm.RefreshLue2(Sl.SetLueOption), "TrainingEvaluationResult");
        }

        private void LueTrainingEvaluationResult_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTrainingEvaluationResult_Leave(object sender, EventArgs e)
        {
            if (LueTrainingEvaluationResult.Visible && fAccept && fCell.ColIndex == 11)
            {
                if (Sm.GetLue(LueTrainingEvaluationResult).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 10].Value =
                    Grd1.Cells[fCell.RowIndex, 11].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 10].Value = Sm.GetLue(LueTrainingEvaluationResult);
                    Grd1.Cells[fCell.RowIndex, 11].Value = LueTrainingEvaluationResult.GetColumnValue("Col2");
                }
                LueTrainingEvaluationResult.Visible = false;
            }
        }

        private void TxtMinScore_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMinScore, 0);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        #endregion

    }
}
