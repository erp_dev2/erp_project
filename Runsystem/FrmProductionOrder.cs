﻿#region Update
/*
    27/04/2017 [TKG] bug fixing pada saat memanggil aplikasi make to stock.
    27/04/2017 [TKG] bug fixing production order masih bisa dicancel setelah diproses menjadi production planning.
    23/02/2018 [ARI] tambah printout
    12/03/2018 [ARI] feedback tambah TE
    20/03/2018 [TKG] tambah start date, end date, process group
    12/12/2019 [WED/MAI] otomatis isi item dan detail nya dari Production Routing for Item, berdasarkan parameter IsProductionOrderUseAutoRoutingItem
    29/01/2020 [VIN/MMM] tambah batch quantity
    25/08/2020 [VIN/MGI] tambah ProductionWorkGrp
    14/01/2021 [WED/KSM] tarik data dari Sales Contract berdasarkan parameter IsSalesContractEnabled
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrder : RunSystem.FrmBase3
    {
        #region Field, Property

        internal bool 
            mIsProductionOrderUseProcessGroup = false,
            mIsProductionOrderUseAutoRoutingItem = false,
            mIsBOM2UseQtyPerBatch = false,
            mIsUseProductionWorkGroup = false,
            mIsSalesContractEnabled = false
            ;
        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSODNo = string.Empty, mDocType = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal decimal 
            
            mPlanningUomCodeConvert12 = 0m;
        internal FrmProductionOrderFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmProductionOrder(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Order";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLuePGCode(ref LuePGCode);
                LuePGCode.Visible = false;
                DteStartDt.Visible = false;
                DteEndDt.Visible = false;
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                if (!mIsBOM2UseQtyPerBatch)
                {
                    LblBatchQty.Visible = false;
                    TxtBatchQuantity.Visible = false;
                }
                if (!mIsUseProductionWorkGroup)
                    WorkGroup.Visible = LueProductionWorkGroup.Visible = false;
                else
                    WorkGroup.Visible = LueProductionWorkGroup.Visible = true;
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsProductionOrderUseProcessGroup = Sm.GetParameterBoo("IsProductionOrderUseProcessGroup");
            mIsProductionOrderUseAutoRoutingItem = Sm.GetParameterBoo("IsProductionOrderUseAutoRoutingItem");
            mIsBOM2UseQtyPerBatch = Sm.GetParameterBoo("IsBOM2UseQtyPerBatch");
            mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            mIsSalesContractEnabled = Sm.GetParameterBoo("IsSalesContractEnabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Work Center#",
                        "",
                        "Work Center",
                        "Location",
                        "Capacity",
                        
                        //6-10
                        "UoM",
                        "Sequence",
                        "",
                        "BoM#"+Environment.NewLine+"WBS#",
                        "",
                        
                        //11-15
                        "BoM/WBS"+Environment.NewLine+"Name",
                        "Start Date",
                        "End Date",
                        "Process Group Code",
                        "Process Group"
                    },
                     new int[] 
                    {
                        20, 
                        130, 20, 200, 150, 80, 
                        80, 80, 20, 130, 20, 
                        250, 100, 100, 0, 200

                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 9, 11, 14 });
            Sm.GrdColButton(Grd1, new int[] { 2, 8, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4, 5, 6 }, false);
            if (mIsProductionOrderUseProcessGroup) 
                Grd1.Cols[15].Move(1);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, DteUsageDt, TxtQty, MeeRemark, TxtBatchQuantity, LueProductionWorkGroup
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 12, 13, 15 });
                    BtnSODocNo.Enabled = BtnProductionRoutingDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteUsageDt, TxtQty, MeeRemark, LueProductionWorkGroup
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12, 13, 15 });
                    BtnSODocNo.Enabled = BtnProductionRoutingDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mSODNo = string.Empty;
            mDocType = string.Empty;
            mPlanningUomCodeConvert12 = 0m;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteUsageDt, TxtSODocNo, TxtItCode, 
                TxtItName, TxtPlanningUomCode, TxtPlanningUomCode2, TxtProductionRoutingDocNo, MeeRemark,
                LueProductionWorkGroup, TxtSpecification
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtQty2, TxtBatchQuantity }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionOrderFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<ProductionOrder>();
            var l2 = new List<Company>();
            var l3 = new List<ItResult>();

            string[] TableName = { "ProductionOrder", "Company", "ItResult" };
            List<IList> myLists = new List<IList>();

            #region Header
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("Select A.DocNo, Date_Format(A.DocDt, '%d/%m/%Y')As DocDt, A.ItCode, B.ItName, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.CtContactPersonName When '3' Then D.CtContactPersonName Else Null End As CtContactPersonName, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.CtName When '3' Then D.CtName Else Null End As CtName, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.Address When '3' Then D.Address Else Null End As Address, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.Phone When '3' Then D.Phone Else Null End As Phone, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.Fax When '3' Then D.Fax Else Null End As Fax, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.Qty When '3' Then D.Qty Else 0.00 End As Qty, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.PriceUomCode When '3' Then D.PriceUomCode Else Null End As PriceUomCode, ");
                SQL.AppendLine("Substring_Index(Substring_Index(B.ItName, ' ', 4 ), ' ', -1)As Size, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.Remark When '3' Then D.Remark Else Null End As Remark ");
                SQL.AppendLine("From TblProductionOrderHdr A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("    And A.DocNo = @DocNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T2.CtContactPersonName, T3.CtName, T3.Address, ");
                SQL.AppendLine("    IfNull(T3.Phone, T3.Mobile) Phone, T3.Fax, T4.Qty, T4.PriceUomCode, T2.Remark ");
                SQL.AppendLine("    From TblProductionOrderHdr T1 ");
                SQL.AppendLine("    Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblCustomer T3 On T2.CtCode = T3.CtCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select X1.DocNo, Group_Concat(Distinct IfNull(X4.PriceUomCode, '')) PriceUomCode, Sum(X2.Qty) Qty ");
                SQL.AppendLine("        From TblSOHdr X1 ");
                SQL.AppendLine("        Inner Join TblSODtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("            And X1.DocNo = @SODocNo ");
                SQL.AppendLine("        Inner Join TblCtQtDtl X3 On X1.CtQtDocNo = X3.DocNo And X2.CtQtDNo = X3.DNo ");
                SQL.AppendLine("        Inner Join TblItemPriceHdr X4 On X3.ItemPriceDocNo = X4.DocNo ");
                SQL.AppendLine("        Group By X1.DocNo ");
                SQL.AppendLine("    ) T4 On T2.DocNo = T4.DocNo ");
                SQL.AppendLine(") C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T6.ContactPersonName As CtContactPersonName, T4.CtName, T4.Address, ");
                SQL.AppendLine("    IfNull(T4.Phone, T4.Mobile) Phone, T4.Fax, T7.Qty, T7.PriceUomCode, T2.Remark ");
                SQL.AppendLine("    From TblProductionOrderHdr T1 ");
                SQL.AppendLine("    Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblCustomer T4 On T3.CtCode = T4.CtCode ");
                SQL.AppendLine("    Left join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select CtCode, Min(DNo) MinDNo ");
                SQL.AppendLine("        From TblCustomerContactPerson ");
                SQL.AppendLine("        Group By CtCode ");
                SQL.AppendLine("    ) T5 On T4.CtCode = T5.CtCode ");
                SQL.AppendLine("    Left Join TblCustomerContactPerson T6 On T4.CtCode = T6.CtCode And T5.MinDNo = T6.DNo ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select X1.DocNo, Group_Concat(Distinct IfNull(X4.PurchaseUomCode, '')) PriceUomCode, Sum(X3.Qty) Qty ");
                SQL.AppendLine("        From TblSalesContract X1 ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr X2 On X1.SalesMemoDocNo = X2.DocNo ");
                SQL.AppendLine("            And X1.DOcNo = @SODocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl X3 On X1.SalesMemoDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblItem X4 On X3.ItCode = X4.ItCode ");
                SQL.AppendLine("        Group By X1.DocNo ");
                SQL.AppendLine("    ) T7 On T2.DocNo = T7.DocNo ");
                SQL.AppendLine(") D On A.DocNo = D.DocNo; ");
            }
            else
            {
                SQL.AppendLine("Select A.DocNo, Date_Format(A.DocDt, '%d/%m/%Y')As DocDt, A.ItCode, B.ItName, C.CtContactPersonName, D.CtName, D.Address, ");
                SQL.AppendLine("IfNuLL(D.Phone, D.Mobile)As Phone, D.Fax, Z.Qty, Z.PriceUomCode, ");
                SQL.AppendLine("Substring_Index(Substring_Index(B.ItName, ' ', 4 ), ' ', -1)As Size, C.Remark ");
                SQL.AppendLine("From TblProductionOrderHdr A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Left Join TblSOHdr C On A.SODocNo=C.DocNo And C.DocNo Is Not Null ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select A.DocNo, Sum(B.Qty)As Qty, Group_Concat(Distinct IfNull(D.PriceUomCode, '')) PriceUomCode ");
                SQL.AppendLine("    From TblSOHdr A ");
                SQL.AppendLine("    Inner Join TblSODtl B On A.DOcNo=B.DocNo ");
                SQL.AppendLine("    Left Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
                SQL.AppendLine("    Left Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
                SQL.AppendLine("    Group by A.DocNo ");
                SQL.AppendLine(")Z On C.DocNo=Z.DocNo ");
                SQL.AppendLine("Left join TblCustomer D On C.CtCode=D.CtCode ");
                SQL.AppendLine("where A.DocNo=@DocNo ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "DocDt",
                         "ItCode",
                         "ItName",
                         "CtContactPersonName",
                         "CtName",

                         //6-10
                         "Address",
                         "Phone",
                         "Fax",
                         "Qty",
                         "PriceUomCode",

                         //11-12
                         "Size",
                         "Remark"

                        });
                if (dr.HasRows)
                {
                    int nomor = 0;
                    while (dr.Read())
                    {
                        nomor = nomor + 1;
                        l.Add(new ProductionOrder()
                        {
                            nomor = nomor,
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            CtContactPersonName = Sm.DrStr(dr, c[4]),
                            CtName = Sm.DrStr(dr, c[5]),

                            Address = Sm.DrStr(dr, c[6]),
                            Phone = Sm.DrStr(dr, c[7]),
                            Fax = Sm.DrStr(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            PriceUomCode = Sm.DrStr(dr, c[10]),
                            Size = Sm.DrStr(dr, c[11]),
                            Remark = Sm.DrStr(dr, c[12]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Company

            var cm2 = new MySqlCommand();

            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1
                         "CompanyName",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Company()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),
                            CompanyName = Sm.DrStr(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Item Result
            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();
            SQL3.AppendLine("Select C.ItCode, D.itname ");
            SQL3.AppendLine("From tblproductionorderdtl A ");
            SQL3.AppendLine("Inner Join TblBomHdr B On A.BOMDocNo=B.DocNo ");
            SQL3.AppendLine("Inner Join TblBomDtl2 C On B.DocNo = C.DocNo ");
            SQL3.AppendLine("Left Join TblItem D On C.ItCode = D.ItCode ");
            SQL3.AppendLine("Where C.ItType='1' And A.DocNo=@DocNo ");
            SQL3.AppendLine("Group By A.DocNo limit 1 ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0-1
                         "ItCode",
                         "ItName",

                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new ItResult()
                        {
                            ItCode = Sm.DrStr(dr3, c3[0]),
                            ItName = Sm.DrStr(dr3, c3[1]),


                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            Sm.PrintReport("ProductionOrder", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmProductionOrderDlg3(this, e.RowIndex, Sm.GetGrdStr(Grd1, e.RowIndex, 1)));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && Sm.IsGrdColSelected(new int[] { 12, 13, 15 }, e.ColIndex))
            {
                if (e.ColIndex == 12) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 13) Sm.DteRequestEdit(Grd1, DteEndDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 15) LueRequestEdit(Grd1, LuePGCode, ref fCell, ref fAccept, e, 14);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                Sm.FormShowDialog(new FrmProductionOrderDlg3(this, e.RowIndex, Sm.GetGrdStr(Grd1, e.RowIndex, 1)));
            
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionOrder", "TblProductionOrderHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProductionOrderHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveProductionOrderDtl(DocNo, Row));

            cml.Add(UpdateSOProcessInd3());
            cml.Add(UpdateMakeToStockProcessInd());
            if (mIsSalesContractEnabled && mDocType == "3")
                cml.Add(UpdateSalesMemoProcessInd3());

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteUsageDt, "Usage date") ||
                Sm.IsTxtEmpty(TxtSODocNo, "Sales order# / Make to stock#", false) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                Sm.IsTxtEmpty(TxtProductionRoutingDocNo, "Production routing#", false) ||
                (mIsBOM2UseQtyPerBatch && Sm.IsTxtEmpty(TxtBatchQuantity, "Quantity Batch", true)) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsBomNotValid() ||
                (mDocType=="1" && IsSONotValid()) ||
                (mDocType=="2" && IsMakeToStockNotValid()) ||
                IsSCNotValid()
                ;
        }

        private bool IsSCNotValid()
        {
            if (!mIsSalesContractEnabled) return false;

            if (mDocType == "3")
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo ");
                SQL.AppendLine("From TblSalesContract A ");
                SQL.AppendLine("Inner Join TblSalesMemoDtl B On A.SalesMemoDocNo = B.DocNo ");
                SQL.AppendLine("    And A.DocNo = @DocNo ");
                SQL.AppendLine("    And B.DNo = @DNo ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And B.ProcessInd <> 'F' ");
                SQL.AppendLine("    And B.ProcessInd3 <> 'F' ");
                SQL.AppendLine("Inner Join TblSalesMemoHdr C On A.SalesMemoDocNo = C.DocNo ");
                SQL.AppendLine("    And C.Status Not In ('M', 'F') ");
                SQL.AppendLine("; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@DocNo", TxtSODocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", mSODNo);

                if (!Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "SC# : " + TxtSODocNo.Text + Environment.NewLine +
                        "Invalid Sales Contract."
                        );
                    return true;
                }
            }

            return false;
        }

        private bool IsSONotValid()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo From TblSOHdr A, TblSODtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And B.DNo=@DNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And B.ProcessInd<>'F' ");
            SQL.AppendLine("And B.ProcessInd3<>'F';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSODocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", mSODNo);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SO# : " + TxtSODocNo.Text + Environment.NewLine +
                    "Invalid SO."
                    );
                return true;
            }
            return false;
        }

        private bool IsMakeToStockNotValid()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo From TblMakeToStockHdr A, TblMakeToStockDtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And B.DNo=@DNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And B.ProcessInd<>'F';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSODocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", mSODNo);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Make To Stock# : " + TxtSODocNo.Text + Environment.NewLine +
                    "Invalid Make To Stock document."
                    );
                return true;
            }
            return false;
        }

        private bool IsBomNotValid()
        { 
            decimal MaxSeq = 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0 &&
                    MaxSeq < Sm.GetGrdDec(Grd1, Row, 7))
                    MaxSeq = Sm.GetGrdDec(Grd1, Row, 7);
            }

            string SelectedBomDocNo = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 7) == MaxSeq)
                {
                    if (SelectedBomDocNo.Length != 0) SelectedBomDocNo += ", ";
                    SelectedBomDocNo += "##" + Sm.GetGrdStr(Grd1, Row, 9) +"##";
                }
            }
            SelectedBomDocNo = (SelectedBomDocNo.Length == 0 ? "##XXX##" : SelectedBomDocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBomDtl2 ");
            SQL.AppendLine("Where ItCode<>@ItCode ");
            SQL.AppendLine("And ItType='1' ");
            SQL.AppendLine("And Locate(Concat('##', DocNo, '##'), @SelectedBomDocNo)>0 ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@SelectedBomDocNo", SelectedBomDocNo);

            var BomDocNo = Sm.GetValue(cm);

            if (BomDocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Bom# : " + BomDocNo + Environment.NewLine +
                    "Invalid product result."
                    );
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 work center.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Work center# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Bill of Material# is empty.") 
                    ) return true;
            }
            return false;
        }

        private MySqlCommand SaveProductionOrderHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProductionOrderHdr ");
            SQL.AppendLine("(DocNo, DocDt, DocType, CancelInd, ProcessInd, UsageDt, SODocNo, SODNo, MakeToStockDOcNo, MakeToStockDNo, ");
            if (mIsSalesContractEnabled && mDocType == "3")
                SQL.AppendLine("SCDocNo, SMDNo, ");
            SQL.AppendLine("ItCode, Qty, ProductionRoutingDocNo, BatchQty, ProductionWorkGroup, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @DocType, 'N', 'O', @UsageDt, @SODOcNo, @SODNo, @MakeToStockDOcNo, @MakeToStockDNo, ");
            if (mIsSalesContractEnabled && mDocType == "3")
                SQL.AppendLine("@SCDocNo, @SMDNo, ");
            SQL.AppendLine("@ItCode, @Qty, @ProductionRoutingDocNo, @BatchQty, @ProductionWorkGroup, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            if (mIsSalesContractEnabled && mDocType == "3")
            {
                Sm.CmParam<String>(ref cm, "@SCDocNo", TxtSODocNo.Text);
                Sm.CmParam<String>(ref cm, "@SMDNo", mSODNo);
                Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", "");
                Sm.CmParam<String>(ref cm, "@MakeToStockDNo", "");
                Sm.CmParam<String>(ref cm, "@SODocNo", "");
                Sm.CmParam<String>(ref cm, "@SODNo", "");
            }
            else
            {
                if (mDocType == "1")
                {
                    Sm.CmParam<String>(ref cm, "@SODocNo", TxtSODocNo.Text);
                    Sm.CmParam<String>(ref cm, "@SODNo", mSODNo);
                    Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", "");
                    Sm.CmParam<String>(ref cm, "@MakeToStockDNo", "");
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@SODocNo", "");
                    Sm.CmParam<String>(ref cm, "@SODNo", "");
                    Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", TxtSODocNo.Text);
                    Sm.CmParam<String>(ref cm, "@MakeToStockDNo", mSODNo);
                }
            }
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text); 
            Sm.CmParam<Decimal>(ref cm, "@Qty", decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDocNo", TxtProductionRoutingDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<Decimal>(ref cm, "@BatchQty", Decimal.Parse(TxtBatchQuantity.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionOrderDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionOrderDtl(DocNo, DNo, ProductionRoutingDNo, BomDocNo, StartDt, EndDt, PGCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ProductionRoutingDNo, @BomDocNo, @StartDt, @EndDt, @PGCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@BomDocNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, Row, 12));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateSOProcessInd3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select SODocNo, SODNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblProductionOrderHdr ");
            SQL.AppendLine("    Where DocType='1' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And SODocNo=@SODocNo ");
            SQL.AppendLine("    And SODNo=@SODNo ");
            SQL.AppendLine("    Group By SODocNo, SODNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("Set T1.ProcessInd3= ");
            SQL.AppendLine("    Case When IfNull(T2.Qty, 0)=0 Then 'O' Else ");
            SQL.AppendLine("        Case When IfNull(T2.Qty, 0)>=T1.Qty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where T1.DocNo=@SODocNo And T1.DNo=@SODNo; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", (mDocType=="1"?TxtSODocNo.Text:"XXX"));
            Sm.CmParam<String>(ref cm, "@SODNo", mSODNo);
            
            return cm;
        }

        private MySqlCommand UpdateSalesMemoProcessInd3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesMemoDtl T1 ");
            SQL.AppendLine("Inner Join TblSalesContract T2 On T2.SalesMemoDocNo = T1.DocNo ");
            SQL.AppendLine("    And T2.CancelInd = 'N' ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select SCDocNo, SMDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblProductionOrderHdr ");
            SQL.AppendLine("    Where DocType = '3' ");
            SQL.AppendLine("    And SCDocNo Is Not Null And SMDNo Is Not Null ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And SCDocNo = @SODocNo And SMDNo = @SODNo ");
            SQL.AppendLine("    Group By SCDocNo, SMDNo ");
            SQL.AppendLine(") T3 On T2.DocNo = T3.SCDocNo And T1.DNo = T3.SMDNo ");
            SQL.AppendLine("Set T1.ProcessInd3 =  ");
            SQL.AppendLine("    Case When IfNull(T3.Qty, 0.00) = 0.00 Then 'O' Else ");
            SQL.AppendLine("        Case When IfNull(T3.Qty, 0.00) >= T1.Qty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where T2.DocNo = @SODocNo And T1.DNo = @SODNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", (mDocType == "3" ? TxtSODocNo.Text : "XXX"));
            Sm.CmParam<String>(ref cm, "@SODNo", mSODNo);

            return cm;
        }

        private MySqlCommand UpdateMakeToStockProcessInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMakeToStockDtl T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select MakeToStockDocNo, MakeToStockDNo, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblProductionOrderHdr ");
            SQL.AppendLine("    Where DocType='2' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And MakeToStockDocNo=@MakeToStockDocNo ");
            SQL.AppendLine("    And MakeToStockDNo=@MakeToStockDNo ");
            SQL.AppendLine("    Group By MakeToStockDocNo, MakeToStockDNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.MakeToStockDocNo And T1.DNo=T2.MakeToStockDNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When IfNull(T2.Qty, 0)=0 Then 'O' Else ");
            SQL.AppendLine("        Case When IfNull(T2.Qty, 0)>=T1.Qty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where T1.DocNo=@MakeToStockDocNo And T1.DNo=@MakeToStockDNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", (mDocType == "2" ? TxtSODocNo.Text : "XXX"));
            Sm.CmParam<String>(ref cm, "@MakeToStockDNo", mSODNo);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditProductionOrderHdr());
            cml.Add(UpdateSOProcessInd3());
            cml.Add(UpdateMakeToStockProcessInd());
            if (mIsSalesContractEnabled && mDocType == "3")
                cml.Add(UpdateSalesMemoProcessInd3());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsEditedDataNotCancelled() ||
                IsEditedDataAlreadyCancelled() ||
                IsEditedDataAlreadyProcessed();
        }

        private bool IsEditedDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "This production order is not cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataAlreadyCancelled()
        {
            if (Sm.IsDataExist("Select DocNo From TblProductionOrderHdr Where DocNo=@Param And CancelInd='Y';", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This production order already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataAlreadyProcessed()
        {
            if (Sm.IsDataExist("Select DocNo From TblProductionOrderHdr Where DocNo=@Param And ProcessInd<>'O';", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This production order already processed to Production planning.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditProductionOrderHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionOrderHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProductionOrderHdr(DocNo);
                ShowProductionOrderDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProductionOrderHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.DocType, A.CancelInd, A.UsageDt, ");
                SQL.AppendLine("Case A.DocType When '1' Then A.SODocNo When '2' Then A.MakeToStockDocNo When '3' Then A.SCDocNo Else '' End As DocNoVal, ");
                SQL.AppendLine("Case A.DocType When '1' Then A.SODNo When '2' Then A.MakeToStockDNo When '3' Then A.SMDNo Else '' End As DNoVal, ");
                SQL.AppendLine("A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
                SQL.AppendLine("Case A.DocType When '1' Then C.CtItCode When '3' Then D.CtItCode Else Null End As CtItCode, ");
                SQL.AppendLine("A.Qty, (A.Qty * B.PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode, B.PlanningUomCode2, ");
                SQL.AppendLine("A.ProductionRoutingDocNo, A.BatchQty, A.ProductionWorkGroup, A.Remark ");
                SQL.AppendLine("From TblProductionOrderHdr A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("    And A.DocNo = @DocNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T3.CtItCode ");
                SQL.AppendLine("    From TblProductionOrderHdr T1 ");
                SQL.AppendLine("    Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo And T1.DocNo = @DocNo ");
                SQL.AppendLine("        And T1.SODocNo Is Not Null ");
                SQL.AppendLine("    Inner Join TblCustomerItem T3 On T2.CtCode = T3.CtCode And T1.ItCode = T3.ItCode ");
                SQL.AppendLine(") C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T4.CtItCode ");
                SQL.AppendLine("    From TblProductionOrderHdr T1 ");
                SQL.AppendLine("    Inner Join TblSalesContract T2 ON T1.SCDocNO = T2.DocNo And T1.DocNo = @DocNo ");
                SQL.AppendLine("        And T1.SCDocNo Is Not Null ");
                SQL.AppendLine("    Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblCustomerItem T4 On T3.CtCode = T4.CtCode And T1.ItCode =T4.ItCode ");
                SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            }
            else
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.DocType, A.CancelInd, A.UsageDt, ");
                SQL.AppendLine("if(A.DocType = '1', A.SODocNo, A.MakeToStockDocNo) As DocNoVal, ");
                SQL.AppendLine("if(A.DocType = '1', A.SODNo, A.MakeToStockDNo) As DNoVal, ");
                SQL.AppendLine("A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
                SQL.AppendLine("Case When A.DocType = '1' Then D.CtItCode Else Null End As CtItCode, ");
                SQL.AppendLine("A.Qty, (A.Qty*PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode, B.PlanningUomCode2, A.ProductionRoutingDocNo, A.BatchQty, A.ProductionWorkGroup, A.Remark ");
                SQL.AppendLine("From TblProductionOrderHdr A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Left Join TblSOHdr C On A.SODocNo=C.DocNo And C.DocNo Is Not Null ");
                SQL.AppendLine("Left Join TblCustomerItem D On A.ItCode=D.ItCode And C.ctCode=D.CtCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");
            }

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "DocType", "CancelInd", "UsageDt", "DocNoVal",    
                        
                        //6-10
                        "DNoVal", "ItCode", "ItName", "ItCodeInternal", "CtItCode", 
                        
                        //11-15
                        "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", "ProductionRoutingDocNo", 
                        
                        //16-19
                        "Remark" , "Specification", "BatchQty", "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        mDocType = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[4]));
                        TxtSODocNo.EditValue = Sm.DrStr(dr, c[5]);
                        mSODNo = Sm.DrStr(dr, c[6]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[8]);
                        TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[9]);
                        TxtCtItCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[11]), 0);
                        TxtPlanningUomCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtQty2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                        TxtPlanningUomCode2.EditValue = Sm.DrStr(dr, c[14]);
                        TxtProductionRoutingDocNo.EditValue = Sm.DrStr(dr, c[15]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        TxtSpecification.EditValue = Sm.DrStr(dr, c[17]);
                        TxtBatchQuantity.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[19]));

                    }, true
                );
        }

        private void ShowProductionOrderDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ProductionRoutingDNo, C.WorkCenterDocNo, D.DocName As WorkCenterDocName, ");
            SQL.AppendLine("D.Location, D.Capacity, D.UomCode, C.Sequence, B.BomDocNo, E.DocName As BomDocName, ");
            SQL.AppendLine("B.StartDt, B.EndDt , B.PGCode, Concat(F.PGCode, ' : ', F.PGName) As PGName ");
            SQL.AppendLine("From TblProductionOrderHdr A ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl C On A.ProductionRoutingDocNo=C.DocNo And B.ProductionRoutingDNo=C.DNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr D On C.WorkCenterDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblBomHdr E On B.BomDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblProcessGroup F On B.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ProductionRoutingDNo", 

                    //1-5
                    "WorkCenterDocNo", "WorkCenterDocName", "Location", "Capacity", "UomCode", 

                    //6-10
                    "Sequence", "BomDocNo", "BomDocName", "StartDt", "EndDt", 
                    
                    //11-12
                    "PGCode", "PGName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowProductionRoutingInfo(string DocNo)
        {
            var SQL = new StringBuilder();

             ClearGrd();

            SQL.AppendLine("Select A.DNo, A.WorkCenterDocNo, B.DocName, B.Location, B.Capacity, B.UomCode, A.Sequence ");
            SQL.AppendLine("From TblProductionRoutingDtl A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.Sequence;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",

                           //1-5
                           "WorkCenterDocNo", "DocName", "Location", "Capacity", "UomCode",

                           //6
                           "Sequence"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ComputeQty2()
        {
            decimal Qty = 0m;
            if (TxtQty.Text.Length > 0)
                Qty = decimal.Parse(TxtQty.Text) * mPlanningUomCodeConvert12;
            TxtQty2.EditValue = Sm.FormatNum(Qty, 0);
        }

        #endregion

        #region Additional Method

        internal void ShowRoutingItem(string ItCode)
        {
            string mPRIDocNo = Sm.GetValue("Select DocNo From TblProductionRoutingItemHdr Where ItCode = @Param And ActInd = 'Y' Order By CreateDt Desc Limit 1;", ItCode);

            if (mIsProductionOrderUseAutoRoutingItem)
            {
                if (mPRIDocNo.Length > 0)
                {
                    ShowRoutingHdr(mPRIDocNo);
                    ShowRoutingDtl(mPRIDocNo);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "This item has no routing item yet.");
                }
            }
        }

        private void ShowRoutingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ProductionRoutingDocNo ");
            SQL.AppendLine("From TblProductionRoutingItemHdr ");
            SQL.AppendLine("Where DocNo = @Param; ");

            TxtProductionRoutingDocNo.EditValue = Sm.GetValue(SQL.ToString(), DocNo);
        }

        private void ShowRoutingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ProductionRoutingDNo, C.WorkCenterDocNo, D.DocName As WorkCenterDocName, ");
            SQL.AppendLine("D.Location, D.Capacity, D.UomCode, C.Sequence, A.BomDocNo, E.DocName As BomDocName, ");
            SQL.AppendLine("A.PGCode, Concat(F.PGCode, ' : ', F.PGName) As PGName ");
            SQL.AppendLine("From TblProductionRoutingItemDtl A ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl C On A.ProductionRoutingDocNo = C.DocNo And A.ProductionRoutingDNo = C.DNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr D On C.WorkCenterDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblBomHdr E On A.BomDocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblProcessGroup F On A.PGCode = F.PGCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ProductionRoutingDNo", 

                    //1-5
                    "WorkCenterDocNo", "WorkCenterDocName", "Location", "Capacity", "UomCode", 

                    //6-10
                    "Sequence", "BomDocNo", "BomDocName", "PGCode", "PGName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLuePGCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, Concat(PGCode, ' : ', PGName) As Col2 From TblProcessGroup " +
                "Where PGCode Not In (Select Parent From TblProcessGroup Where Parent Is Not Null) " +
                "Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ComputeBatchQty()
        {
            decimal mTotal = 0; 
            decimal mQuantity = Sm.GetDecValue(TxtQty.Text);
            decimal mQtyperBatch = 0;
            decimal mSequence = 0;
            string BOMDocNo = string.Empty;
            string SelectedBOMDocNo = string.Empty;

            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdDec(Grd1, row, 7) > mSequence)
                {
                    mSequence = Sm.GetGrdDec(Grd1, row, 7);
                    //BOMDocNo = Sm.GetGrdStr(Grd1, row, 9);
                }                
            }

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 9).Length > 0 && Sm.GetGrdDec(Grd1, i, 7) == mSequence)
                {
                    if (BOMDocNo.Length > 0) BOMDocNo += ",";
                    BOMDocNo += Sm.GetGrdStr(Grd1, i, 9);
                }
            }

            if(BOMDocNo.Length > 0 && TxtItCode.Text.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct DocNo From TblBomDtl2 ");
                SQL.AppendLine("Where ItCode = @Param1 ");
                SQL.AppendLine("And ItType='1' ");
                SQL.AppendLine("And Find_In_set(DocNo, @Param2) ");
                SQL.AppendLine("Limit 1;");

                SelectedBOMDocNo = Sm.GetValue(SQL.ToString(), TxtItCode.Text, BOMDocNo, string.Empty);

                if (SelectedBOMDocNo.Length > 0)
                {
                    mQtyperBatch = Sm.GetValueDec("Select QtyperBatch From TblBOMHdr Where DocNo = @param", SelectedBOMDocNo);
                    if(mQtyperBatch != 0)
                        mTotal = mQuantity / mQtyperBatch;
                }
            }

            TxtBatchQuantity.EditValue = Sm.FormatNum(mTotal, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
            ComputeQty2();
            if (mIsBOM2UseQtyPerBatch)
            {
                Sm.FormatNumTxt(TxtBatchQuantity, 0);
                ComputeBatchQty();
            }
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePGCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePGCode_Leave(object sender, EventArgs e)
        {
            if (LuePGCode.Visible && fAccept)
            {
                if (Sm.GetLue(LuePGCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LuePGCode);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = LuePGCode.GetColumnValue("Col2");
                }
                LuePGCode.Visible = false;
            }
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(SetLuePGCode));
        }

        #endregion

        #region Button Event

        private void BtnSODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProductionOrderDlg(this));
        }

        private void BtnSODocNo2_Click(object sender, EventArgs e)
        {
            if (mDocType == "1")
            {
                if (!Sm.IsTxtEmpty(TxtSODocNo, "Sales order#", false))
                {
                    var f1 = new FrmSO2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSODocNo.Text;
                    f1.ShowDialog();                    
                }
            }
            if (mDocType == "2")
            {
                if (!Sm.IsTxtEmpty(TxtSODocNo, "MTS#", false))
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;", 
                        TxtSODocNo.Text);
                    
                    var f1 = new FrmMakeToStock(mMenuCode);
                    if (IsMTSStandard) 
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f1.mMenuCode = MenuCodeForMakeToStockStandard;
                            f1.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f1.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f1.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSODocNo.Text;
                    f1.ShowDialog();
                }
            }

            if (mIsSalesContractEnabled && mDocType == "3")
            {
                if (!Sm.IsTxtEmpty(TxtSODocNo, "SO#", false))
                {
                    var f1 = new FrmSalesContract(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSODocNo.Text;
                    f1.ShowDialog();
                }
            }
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item", false))
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = TxtItCode.Text;
                f1.ShowDialog();
            }
        }

        private void BtnProductionRoutingDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProductionOrderDlg2(this));
        }

        private void BtnProductionRouting2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtProductionRoutingDocNo, "Routing#", false))
            {
                var f1 = new FrmProductionRouting(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtProductionRoutingDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class ProductionOrder
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string CtContactPersonName { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public decimal Qty { get; set; }
            public string PriceUomCode { get; set; }
            public string Size { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }

        }

        private class Company
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { set; get; }
        }

        private class ItResult
        {
            public string ItCode { set; get; }
            public string ItName { set; get; }
        }

        #endregion



        
    }
}
