﻿#region Update
/*
    01/02/2023 [IBL/BBT] Menu baru
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
#endregion

namespace RunSystem
{
    public partial class FrmYearlyClosingJournalEntry : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mMaxAccountCategory = "";
        internal bool
            mInsert = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mProcessInd = false,
            mIsAutoJournalActived = false;
        private decimal mAmt = 0m;
        private List<ResultCostCenter> l1 = new List<ResultCostCenter>();

        internal FrmYearlyClosingJournalEntryFind FrmFind;

        #endregion

        #region Constructor

        public FrmYearlyClosingJournalEntry(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Yearly Closing Journal Entry";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        
                        //6
                        "Amount"
                    },
                    new int[]
                    {
                        //0
                        20,
 
                        //1-5
                        20, 150, 200, 80, 150,
                        
                        //6
                        130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            #endregion

            #region Grid2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        
                        //6
                        "Amount"
                    },
                    new int[]
                    {
                        //0
                        20,

                        //1-5
                        20, 150, 200, 80, 150,

                        //6
                        130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6 });

            #endregion

            #region Grid3
            Grd3.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Checked By",
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[]
                    {
                        100,
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 4 });
            if (!mIsTransactionUseDetailApprovalInformation)
                Sm.GrdColInvisible(Grd3, new int[] { 0, 2 }, false);
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, TxtStatus, DteDocDt, LueYr, TxtAcNo,
                        TxtAcDesc, TxtAmt, TxtDocNoCopy, MeeRemark, TxtIncome,
                        TxtExpenses
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    BtnProcess.Enabled = false;
                    BtnCopy.Enabled = false;
                    LblCopy.Visible = TxtDocNoCopy.Visible = BtnCopy.Visible = false;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    mProcessInd = false;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, LueYr, MeeRemark
                    }, false);
                    BtnAcNo.Enabled = true;
                    BtnProcess.Enabled = true;
                    BtnCopy.Enabled = true;
                    LblCopy.Visible = TxtDocNoCopy.Visible = BtnCopy.Visible = true;
                    TxtDocNoCopy.Visible = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, TxtStatus, DteDocDt, LueYr, TxtAcNo,
                TxtAcDesc, TxtDocNoCopy, MeeRemark
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtIncome, TxtExpenses }, 0);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, TxtStatus, TxtAcNo, TxtAcDesc, TxtDocNoCopy, MeeRemark
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtIncome, TxtExpenses }, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);

            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 6 });
        }

        #region Additional Method
        private void GetParameter()
        {
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
            mIsTransactionUseDetailApprovalInformation = Sm.GetParameterBoo("IsTransactionUseDetailApprovalInformation");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
        }

        internal void ShowCopyData(string DocNo)
        {
            ClearData2();

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.CurrentEarningAcNo, B.AcDesc, A.Remark " +
                    "From TblYearlyClosingJournalEntryHdr A " +
                    "Inner Join TblCOA B On A.CurrentEarningAcNo = B.AcNo " +
                    "Where A.DocNo=@DocNo;",
                    new string[] { "DocNo", "CurrentEarningAcno", "AcDesc", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        ChkCancelInd.Checked = false;
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[1]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[2]);
                        TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );

            ShowYearlyClosingJournalEntryDtl(DocNo);
            ShowYearlyClosingJournalEntryDtl2(DocNo);

            TxtDocNoCopy.EditValue = TxtDocNo.Text;
            mProcessInd = false;

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                Grd1.Cells[i, 6].Value = 0m;
            for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                Grd2.Cells[i, 6].Value = 0m;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtIncome, TxtExpenses }, 0);
        }

        private void ComputeCurrentEarningAmt()
        {
            decimal Amt1 = 0m, Amt2 = 0m, Amt3 = 0m;

            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                Amt1 += Sm.GetGrdDec(Grd1, row, 6);

            for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                Amt2 += Sm.GetGrdDec(Grd2, row, 6);

            Amt3 = Amt1 - Amt2;

            if (Amt1 < 0)
                TxtIncome.EditValue = "(" + (Sm.FormatNum(Amt1 * -1, 0)) + ")";
            else
                TxtIncome.EditValue = Sm.FormatNum(Amt1, 0);

            if (Amt2 < 0)
                TxtExpenses.EditValue = "(" + (Sm.FormatNum(Amt2 * -1, 0)) + ")";
            else
                TxtExpenses.EditValue = Sm.FormatNum(Amt2, 0);

            if (Amt3 < 0)
                TxtAmt.EditValue = "(" + (Sm.FormatNum(Amt3 * -1, 0)) + ")";
            else
                TxtAmt.EditValue = Sm.FormatNum(Amt3, 0);
            
            mAmt = Amt3;
            mProcessInd = true;
        }

        private void ComputeDetailAmt()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l1 = new List<COAAmt>();

            SQL.AppendLine("Select T.Grid, T.AcNo, IfNull(T.DAmt, 0.00) + IfNull(T.CAmt, 0.00) As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select 'Grd1' As Grid, X2.AcNo, ");
            SQL.AppendLine("    Sum(If(X3.AcType = 'D', DAmt, DAmt*-1)) As DAmt, ");
            SQL.AppendLine("    Sum(If(X3.AcType = 'C', CAmt, CAmt*-1)) As CAmt ");
            SQL.AppendLine("    From TblJournalHdr X1 ");
            SQL.AppendLine("    Inner Join TblJournalDtl X2 On X2.DocNo = X1.DocNo ");
            SQL.AppendLine("    Inner Join TblCOA X3 On X2.AcNo = X3.AcNo ");
            SQL.AppendLine("    Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Find_In_Set(X2.AcNo, @AcNo) ");
            SQL.AppendLine("    And X3.ActInd = 'Y' ");
            SQL.AppendLine("    Group By X2.AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 'Grd2' As Grid, X2.AcNo, ");
            SQL.AppendLine("    Sum(If(X3.AcType = 'D', DAmt, DAmt*-1)) As DAmt, ");
            SQL.AppendLine("    Sum(If(X3.AcType = 'C', CAmt, CAmt*-1)) As CAmt ");
            SQL.AppendLine("    From TblJournalHdr X1 ");
            SQL.AppendLine("    Inner Join TblJournalDtl X2 On X2.DocNo = X1.DocNo ");
            SQL.AppendLine("    Inner Join TblCOA X3 On X2.AcNo = X3.AcNo ");
            SQL.AppendLine("    Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Find_In_Set(X2.AcNo, @AcNo2) ");
            SQL.AppendLine("    And X3.ActInd = 'Y' ");
            SQL.AppendLine("    Group By X2.AcNo ");
            SQL.AppendLine(") T ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AcNo", GetAcNo(Grd1));
                Sm.CmParam<String>(ref cm, "@AcNo2", GetAcNo(Grd2));
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetLue(LueYr) + "0101");
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetLue(LueYr) + "1231");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "Grid",

                    //1-4
                    "AcNo",
                    "Amt",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new COAAmt()
                        {
                            Grid = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            ComputeDetailAmt(ref l1);
            l1.Clear();
        }

        private void ComputeDetailAmt(ref List<COAAmt> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                {
                    if (l[i].Grid == "Grd1" && l[i].AcNo == Sm.GetGrdStr(Grd1, j, 2))
                        Grd1.Cells[j, 6].Value = l[i].Amt;
                }

                for (int j = 0; j < Grd2.Rows.Count - 1; j++)
                {
                    if (l[i].Grid == "Grd2" && l[i].AcNo == Sm.GetGrdStr(Grd2, j, 2))
                        Grd2.Cells[j, 6].Value = l[i].Amt;
                }
            }
        }

        private void ComputeJnAmtByCC(ref List<ResultCostCenter> l)
        {
            l.Clear();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CCCode, Null As AcNo, ");
            SQL.AppendLine("IfNull(If(Amt < 0, Amt*-1, 0.00), 0.00) As DAmt, ");
            SQL.AppendLine("IfNull(If(Amt < 0, 0.00, Amt), 0.00) As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select T.CCCode, Sum(If(T.Type = '1', T.Amt, 0.00)) - Sum(if(T.Type = '2', T.Amt, 0.00)) As Amt ");
            SQL.AppendLine("	From ( ");
            SQL.AppendLine("		Select '1' As Type, A.CCCode, Sum(If(C.AcType = 'D', B.DAmt, B.DAmt*-1)) + Sum(If(C.AcType = 'C', B.CAmt, B.CAmt*-1)) As Amt ");
            SQL.AppendLine("		From TblJournalHdr A ");
            SQL.AppendLine("		Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("		Where Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("		And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("		Group By A.CCCode ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select '2' As Type, A.CCCode, Sum(If(C.AcType = 'D', B.DAmt, B.DAmt*-1)) + Sum(If(C.AcType = 'C', B.CAmt, B.CAmt*-1)) As Amt ");
            SQL.AppendLine("		From TblJournalHdr A ");
            SQL.AppendLine("		Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("		Where Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("		And Find_In_Set(B.AcNo, @AcNo2) ");
            SQL.AppendLine("		Group By A.CCCode ");
            SQL.AppendLine("	)T ");
            SQL.AppendLine("	Group By T.CCCode ");
            SQL.AppendLine(")X Where Amt <> 0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select CCCode, AcNo, IfNull(DAmt, 0.00) As DAmt, IfNull(CAmt, 0.00) As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select T1.CCCode, T1.AcNo, ");
            SQL.AppendLine("	IfNull( ");
            SQL.AppendLine("	Case ");
            SQL.AppendLine("		When T1.Amt < 0 Then ");
            SQL.AppendLine("			if(T2.AcType = 'C', 0.00, T1.Amt*-1) ");
            SQL.AppendLine("		Else ");
            SQL.AppendLine("			if(T2.AcType = 'C', T1.Amt, 0.00) ");
            SQL.AppendLine("	End, 0.00) As DAmt, ");
            SQL.AppendLine("	IfNull( ");
            SQL.AppendLine("	Case ");
            SQL.AppendLine("		When T1.Amt < 0 Then ");
            SQL.AppendLine("			if(T2.AcType = 'D', 0.00, T1.Amt*-1) ");
            SQL.AppendLine("		Else ");
            SQL.AppendLine("			if(T2.AcType = 'D', T1.Amt, 0.00) ");
            SQL.AppendLine("	End, 0.00) As CAmt ");
            SQL.AppendLine("	From ( ");
            SQL.AppendLine("		Select B.CCCode, A.AcNo, ");
            SQL.AppendLine("		Sum(If(C.AcType = 'D', A.DAmt, A.DAmt*-1)) + Sum(If(C.AcType = 'C', A.CAmt, A.CAmt*-1)) As Amt ");
            SQL.AppendLine("		From TblJournalDtl A ");
            SQL.AppendLine("		Inner Join TblJournalHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblCOA C On A.AcNo = C.AcNo ");
            SQL.AppendLine("		Where Left(B.DocDt, 4) = @Yr ");
            SQL.AppendLine("		And Find_In_Set(A.AcNo, @AcNo) ");
            SQL.AppendLine("		Group By A.AcNo, B.CCCode ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select B.CCCode, A.AcNo, ");
            SQL.AppendLine("		Sum(If(C.AcType = 'D', A.DAmt, A.DAmt*-1)) + Sum(If(C.AcType = 'C', A.CAmt, A.CAmt*-1)) As Amt ");
            SQL.AppendLine("		From TblJournalDtl A ");
            SQL.AppendLine("		Inner Join TblJournalHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblCOA C On A.AcNo = C.AcNo ");
            SQL.AppendLine("		Where Left(B.DocDt, 4) = @Yr ");
            SQL.AppendLine("		And Find_In_Set(A.AcNo, @AcNo2) ");
            SQL.AppendLine("		Group By A.AcNo, B.CCCode ");
            SQL.AppendLine("	)T1 ");
            SQL.AppendLine("	Inner Join TblCOA T2 On T1.AcNo = T2.AcNo ");
            SQL.AppendLine(")X Where X.DAmt <> 0 Or X.CAmt <> 0 ");
            SQL.AppendLine("Order By CCCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@AcNo", GetAcNo(Grd1));
                Sm.CmParam<String>(ref cm, "@AcNo2", GetAcNo(Grd2));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCode", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ResultCostCenter()
                        {
                            CCCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }

        }

        private string GetAcNo(iGrid mGrid)
        {
            string AcNo = string.Empty;
            for (int row = 0; row < mGrid.Rows.Count - 1; row++)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += Sm.GetGrdStr(mGrid, row, 2);
            }

            return AcNo;
        }

        private bool IsNoNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'YearlyClosingJournalEntry' ");

            if (Sm.IsDataExist(SQL.ToString()))
                return false;

            return true;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmYearlyClosingJournalEntryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            mInsert = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "YearlyClosingJournalEntry", "TblYearlyClosingJournalEntryHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveYearlyClosingJournalEntryHdr(DocNo));
            if (Grd1.Rows.Count > 1) cml.Add(SaveYearlyClosingJournalEntryDtl(DocNo));
            if (Grd2.Rows.Count > 1) cml.Add(SaveYearlyClosingJournalEntryDtl2(DocNo));
            cml.Add(SaveYearlyClosingJournalEntryDtl3(DocNo));
            
            Sm.ExecCommands(cml);
            if (mIsAutoJournalActived && IsNoNeedApproval()) SaveJournal(DocNo, Sm.GetLue(LueYr));

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Date") ||
                Sm.IsTxtEmpty(TxtAcNo, "Current Earning Acc No.", false) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty() ||
                IsDataCalculated() ||
                IsMonthlyClosingJnNotExists() ||
                IsDocumentAlreadyExists() ||
                IsOutstandingDocExists()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                Sm.FocusGrd(Grd2, 0, 1);
                return true;
            }
            return false;
        }

        private bool IsDataCalculated()
        {
            if (!mProcessInd)
            {
                Sm.StdMsg(mMsgType.Warning, "You have to click 'Process Data' to re-calculate the amount due to change of detail data.");
                BtnProcess.Focus();
                return true;
            }
            return false;
        }

        private bool IsMonthlyClosingJnNotExists()
        {
            bool IsExists = Sm.IsDataExist("Select 1 From TblClosingJournal A Where Left(ClosingDt, 6) = Concat(@Param, '12') And A.CancelInd ='N';", Sm.GetLue(LueYr));
            if (!IsExists)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no monthly accounting journal entries for December " + Sm.GetLue(LueYr) + "." + Environment.NewLine +
                    "You must create monthly accounting journal entries for December "+Sm.GetLue(LueYr)+" first.");
                return true;
            }
            return false;
        }

        private bool IsDocumentAlreadyExists()
        {
            bool IsExists = Sm.IsDataExist("Select 1 From TblYearlyClosingJournalEntryHdr Where Year = @Param And Status = 'A' And CancelInd = 'N' Limit 1;", Sm.GetLue(LueYr));
            if (IsExists)
            {
                Sm.StdMsg(mMsgType.Warning, "Yearly Closing Journal Entry ( " + Sm.GetLue(LueYr) + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsOutstandingDocExists()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select DocNo From TblYearlyClosingJournalEntryHdr ");
            SQL.AppendLine("Where Year = @Param And CancelInd ='N' And Status = 'O' Limit 1;");

            string DocNo = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueYr));
            if (DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can't create document. There is an outstanding Yearly Closing Journal Entry document." + Environment.NewLine + Environment.NewLine +
                    "Document# : " + DocNo + Environment.NewLine +
                    "Year : " + Sm.GetLue(LueYr));
                return true;
            }
            return false;
        }

        private MySqlCommand SaveYearlyClosingJournalEntryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblYearlyClosingJournalEntryHdr(DocNo, DocDt, CancelInd, Status, Year, CurrentEarningAcNo, CurrentEarningAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, 'O', @Year, @CurrentEarningAcNo, @CurrentEarningAmt, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='YearlyClosingJournalEntry'; ");

            SQL.AppendLine("Update TblYearlyClosingJournalEntryHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='YearlyClosingJournalEntry' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CurrentEarningAcNo", TxtAcNo.Text);
            Sm.CmParam<decimal>(ref cm, "@CurrentEarningAmt", mAmt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveYearlyClosingJournalEntryDtl(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblYearlyClosingJournalEntryDtl(DocNo, DNo, AcNo, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @AcNo_" + i.ToString() + ", @Amt_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 6));

                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveYearlyClosingJournalEntryDtl2(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblYearlyClosingJournalEntryDtl2(DocNo, DNo, AcNo, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd2.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 2).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @AcNo_" + i.ToString() + ", @Amt_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + i.ToString(), Sm.GetGrdStr(Grd2, i, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 6));

                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveYearlyClosingJournalEntryDtl3(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            SQL.AppendLine("Insert Into TblYearlyClosingJournalEntryDtl4(DocNo, DNo, CCCode, AcNo, DAmt, CAmt, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < l1.Count; i++)
            {
                if (l1[i].CCCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @CCCode_" + i.ToString() + ", @AcNo_" + i.ToString() + ", @DAmt_" + i.ToString() + ", @CAmt_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), l1[i].CCCode);
                    Sm.CmParam<String>(ref cm, "@AcNo_" + i.ToString(), (l1[i].AcNo.Length > 0 ? l1[i].AcNo : TxtAcNo.Text));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + i.ToString(), l1[i].DAmt);
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + i.ToString(), l1[i].CAmt);

                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private void ProcessJournal1(string DocNo, ref List<PrepJnCostCenter> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct(A.CCCode) As CCCodeOld, C.OptDesc As CCCodeNew ");
            SQL.AppendLine("From TblYearlyClosingJournalEntryDtl4 A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCat = 'CostCenterforYearlyClosing' And C.OptCode = B.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCodeOld", "CCCodeNew" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PrepJnCostCenter()
                        {
                            CCCodeOld = Sm.DrStr(dr, c[0]),
                            CCCodeNew = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessJournal2(string DocNo, int DNo, string Year, PrepJnCostCenter x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @JournalDocNo := ");
            SQL.AppendLine(Sm.GetNewJournalDocNo(string.Concat(Year,"1231"), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Set @Dt := CurrentDateTime(); ");
            SQL.AppendLine("Insert Into TblYearlyClosingJournalEntryDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, Right(Concat('00', " + DNo + "), 3), @JournalDocNo, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Concat(@Yr,'1231'), ");
            SQL.AppendLine("Concat('Yearly Closing Journal Entry : ', DocNo) As JnDesc, @MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeNew, Concat(@Yr,'99'), Remark, @UserCode, @Dt ");
            SQL.AppendLine("From TblYearlyClosingJournalEntryHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            SQL.AppendLine("Set @row:=0;");
            SQL.AppendLine("Set @AcNo:= (Select CurrentEarningAcNo From TblYearlyClosingJournalEntryHdr Where DocNo = @DocNo); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, '001', A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select IfNull(AcNo, @AcNo) As AcNo, IfNull(DAmt, 0.00) As DAmt, IfNull(CAmt, 0.00) As CAmt ");
            SQL.AppendLine("   From TblYearlyClosingJournalEntryDtl4 ");
            SQL.AppendLine("   Where DocNo = @DocNo ");
            SQL.AppendLine("   And CCCode = @CCCodeOld ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCodeNew", x.CCCodeNew);
            Sm.CmParam<String>(ref cm, "@CCCodeOld", x.CCCodeOld);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        internal void SaveJournal(string DocNo, string Year)
        {
            var l1 = new List<PrepJnCostCenter>();

            ProcessJournal1(DocNo, ref l1);
            if (l1.Count > 0)
            {
                int DNo = 1;
                foreach (var x in l1)
                {
                    ProcessJournal2(DocNo, DNo, Year, x);
                    DNo++;
                }

            }
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(EditYearlyClosingJournalEntry(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            if (mIsAutoJournalActived) SaveJournal2();

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand() {
                CommandText = "Select DocNo From TblYearlyClosingJournalEntryHdr Where DocNo=@DocNo And CancelInd = 'Y' "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditYearlyClosingJournalEntry(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblYearlyClosingJournalEntryHdr Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        private void ProcessCancelJournal1(ref List<PrepCancelJn> l)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = "Select DocNo, DNo, JournalDocNo From TblYearlyClosingJournalEntryDtl3 Where DocNo = @DocNo;";
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "JournalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PrepCancelJn()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            JournalDocNo = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCancelJournal2(PrepCancelJn x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = string.Concat(Sm.GetLue(LueYr), "1231");
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblYearlyClosingJournalEntryDtl3 Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And DNo = @DNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Period, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblYearlyClosingJournalEntryDtl3 Where DocNo=@DocNo And DNo = @DNo); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblYearlyClosingJournalEntryDtl3 Where DocNo=@DocNo And DNo = @DNo); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private void SaveJournal2()
        {
            var l1 = new List<PrepCancelJn>();

            ProcessCancelJournal1(ref l1);
            if (l1.Count > 0)
            {
                l1.ForEach(i => { ProcessCancelJournal2(i); });
            }
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowYearlyClosingJournalEntryHdr(DocNo);
                ShowYearlyClosingJournalEntryDtl(DocNo);
                ShowYearlyClosingJournalEntryDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "YearlyClosingJournalEntry", ref Grd3, mIsTransactionUseDetailApprovalInformation);
                ComputeCurrentEarningAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowYearlyClosingJournalEntryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.CancelInd, Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As Status, " +
                    "A.DocDt, A.Year, A.CurrentEarningAcNo, B.AcDesc, IfNull(A.CurrentEarningAmt,0.00) As CurrentEarningAmt, A.Remark " +
                    "From TblYearlyClosingJournalEntryHdr A " +
                    "Inner Join TblCOA B On A.CurrentEarningAcNo = B.AcNo " +
                    "Where A.DocNo=@DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "CancelInd", "Status", "DocDt", "Year", "CurrentEarningAcno", 

                        //6-8
                        "AcDesc", "CurrentEarningAmt", "Remark"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[1]) == "Y";
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                        Sl.SetLueYr(LueYr, string.Empty);
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[6]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowYearlyClosingJournalEntryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.AcNo, B.AcDesc, Case B.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, ");
            SQL.AppendLine("B.Alias, IfNull(A.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblYearlyClosingJournalEntryDtl A ");
            SQL.AppendLine("Left Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "AcType", "Alias", "Amt",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowYearlyClosingJournalEntryDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.AcNo, B.AcDesc, Case B.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, ");
            SQL.AppendLine("B.Alias, IfNull(A.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblYearlyClosingJournalEntryDtl2 A ");
            SQL.AppendLine("Left Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "AcType", "Alias", "Amt",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg(this, "1"));
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            if(!Sm.IsLueEmpty(LueYr, "Year"))
                Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg2(this));
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeJnAmtByCC(ref l1);
                ComputeDetailAmt();
                ComputeCurrentEarningAmt();
            }
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg(this, "2"));
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg(this, "3"));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeCurrentEarningAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeCurrentEarningAmt();
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg(this, "2"));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmYearlyClosingJournalEntryDlg(this, "3"));
            }
        }

        #endregion

        #endregion

        #region Class
        private class COAAmt
        {
            public string Grid { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class ResultCostCenter
        {
            public string CCCode { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class PrepJnCostCenter
        {
            public string CCCodeOld { get; set; }
            public string CCCodeNew { get; set; }
        }

        private class PrepCancelJn
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string JournalDocNo { get; set; }
        }

        #endregion
    }
}
