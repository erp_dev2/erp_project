﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmObjectType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmObjectTypeFind FrmFind;

        #endregion

        #region Constructor

        public FrmObjectType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Object Type";
            
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtObjectTypeCode, TxtObjectTypeName, ChkActInd }, true);
                    TxtObjectTypeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtObjectTypeCode, TxtObjectTypeName }, false);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    TxtObjectTypeCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtObjectTypeCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd, TxtObjectTypeName }, false);
                    TxtObjectTypeName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtObjectTypeCode, TxtObjectTypeName });
            ChkActInd.Checked = false;
        }
        

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmObjectTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtObjectTypeCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtObjectTypeCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblObjectType Where ObjectTypeCode=@ObjectTypeCode" };
                Sm.CmParam<String>(ref cm, "@ObjectTypeCode", TxtObjectTypeCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblObjectType(ObjectTypeCode, ObjectTypeName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ObjectTypeCode, @ObjectTypeName, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ObjectTypeName=@ObjectTypeName, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ObjectTypeCode", TxtObjectTypeCode.Text);
                Sm.CmParam<String>(ref cm, "@ObjectTypeName", TxtObjectTypeName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtObjectTypeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ObjectTypeCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ObjectTypeCode", ObjectTypeCode);

                var SQL = new StringBuilder();
                SQL.AppendLine("SELECT ObjectTypeCode, ObjectTypeName, ActInd From TblObjectType Where ObjectTypeCode=@ObjectTypeCode ");
                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        {
                            "ObjectTypeCode", "ObjectTypeName", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtObjectTypeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtObjectTypeName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtObjectTypeCode, "Object type code", false) ||
                Sm.IsTxtEmpty(TxtObjectTypeName, "Object type name", false) ||
                IsObjectTypeCodeExisted();
        }

        private bool IsObjectTypeCodeExisted()
        {
            if (!TxtObjectTypeCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select ObjectTypeCode From TblObjectType Where ObjectTypeCode=@ObjectTypeCode;";
                Sm.CmParam<String>(ref cm, "@ObjectTypeCode", TxtObjectTypeCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Object type code ( " + TxtObjectTypeCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

            #region Misc Control Events

            private void TxtObjectTypeCode_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtObjectTypeCode);
            }

            private void TxtObjectTypeName_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtObjectTypeName);
            }

            #endregion

        #endregion
    }
}
