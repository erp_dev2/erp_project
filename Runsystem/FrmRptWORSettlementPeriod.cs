﻿#region Update
// 28/11/2017 [HAR] tambah filter location
// 05/05/2018 [ARI] tambah filter dan kolom status
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWORSettlementPeriod : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool mIsFilterBySite = false;


        #endregion

        #region Constructor

        public FrmRptWORSettlementPeriod(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
                SetLueLocCode(ref LueLocCode);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetLueStatusCode(ref LueStatus);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo As DocNoWOR, B.DocDt, A.AssetCode, D.AssetName, D.DisplayName, A.LocCOde, E.LocName, B.DownDt, ");
            SQL.AppendLine("C.DocNO As DocNoWO, C.DocDt As DocDtWO, DateDiff(C.DocDt, B.DownDt)  As DiffDay, F.OptDesc As Status ");
            SQL.AppendLine("From TblTOhdr A  ");
            SQL.AppendLine("Inner Join TblWOR B On A.AssetCode=B.TOCode   ");
            SQL.AppendLine("Inner Join TblWOHdr C On B.DocNo=C.WORDocNo  ");
            SQL.AppendLine("Inner Join TblAsset D On A.AssetCode=D.AssetCode ");
            SQL.AppendLine("Left Join TblLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Left Join TblOption F On A.Status=F.OptCode And F.Optcat = 'MaintenanceStatus' ");
            SQL.AppendLine("Where B.cancelInd = 'N' And C.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Docment WO Request", 
                        "Document Date "+Environment.NewLine+"WO Request", 
                        "",
                        "Asset Code",
                        "Asset Name",
                        
                        //6-10
                        "Display Name",
                        "Location",
                        "Down Date",
                        "Document WO",
                        "Document"+Environment.NewLine+"Date WO",
                        //11
                        "",
                        "Difference Day",
                        "Status"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        180, 100, 20, 150, 200,
                        //6-10
                        200, 180, 100, 180, 100,  
                        //11-13
                        20, 130, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 10 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 12, 13 }, false);
            //Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "B.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo2.Text, "C.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "B.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "A.LocCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "A.Status", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By  B.DocDt, B.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNoWOR",  

                            //1-5
                            "DocDt", "AssetCode", "AssetName", "DisplayName", "LocName", 

                            //6-10
                            "DownDt", "DocNoWO", "DocDtWO", "DiffDay", "Status"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional method

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueStatusCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2  From TblOption Where Optcat = 'MaintenanceStatus' ");
            SQL.AppendLine("Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document WO Request");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkDocNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Work Order");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtDocNo2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatusCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

    }
}
