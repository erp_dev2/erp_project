﻿#region Update
    // 22/06/2017 [WED] bug fixing saat filter mechanic
    // 02/08/2017 [Ari] tambah kolom department
    // 23/11/2017 [HAR] validasi group site
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWODlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmWO mFrmParent;
        private string mSQL = string.Empty;
        private int mRowIndex = 0;
        string MechanicCode = string.Empty;
        string MechanicName = string.Empty;

        #endregion

        #region Constructor

        public FrmWODlg3(FrmWO FrmParent, int RowIndex)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRowIndex = RowIndex;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Mechanic";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-4
                        "",
                        "Mechanic Code",
                        "Mechanic",
                        "Department"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-4
                        20, 100, 250, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2}, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
             var SQL = new StringBuilder();

                SQL.AppendLine("Select A.MechanicCode, A.MechanicName, C.DeptName From TblMechanic A ");
                SQL.AppendLine("Inner Join tblemployee B on A.MechanicCode = B.EmpCode ");
                SQL.AppendLine("Left Join tbldepartment C on B.DeptCode = C.DeptCode ");
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("    And B.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }

                mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtMechanic.Text, new string[] { "MechanicCode", "MechanicName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.MechanicName Desc;",
                        new string[] 
                        { 
                             //0
                             "MechanicCode", 
                             
                             //1-2
                             "MechanicName",
                             "DeptName"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (MechanicCode.Length == 0)
                        {
                            MechanicCode = string.Concat(MechanicCode, Sm.GetGrdStr(Grd1, Row, 2), "#");
                            MechanicName = string.Concat(MechanicName, Sm.GetGrdStr(Grd1, Row, 3));
                        }
                        else
                        {
                            MechanicCode = string.Concat(MechanicCode, Sm.GetGrdStr(Grd1, Row, 2), "#");
                            MechanicName = string.Concat(MechanicName,", ", Sm.GetGrdStr(Grd1, Row, 3));
                        }
                    }
                }
                mFrmParent.Grd2.Cells[mRowIndex, 11].Value = MechanicCode;
                mFrmParent.Grd2.Cells[mRowIndex, 12].Value = MechanicName;
                MechanicCode = "";
                MechanicName = "";
            }
           
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkMechanic_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Mechanic");
        }

        private void TxtMechanic_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
