﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemBarcodeDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmItemBarcode mFrmParent;
        string mSQL;

        #endregion

        #region Constructor

        public FrmItemBarcodeDlg(FrmItemBarcode FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                ChkChooseRepeatedly.Checked = true;
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 7].Value = "'" + Sm.GetGrdStr(Grd1, Row, 7);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);
            
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 7].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row, 7).Length - 1);
            }
            Grd1.EndUpdate();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, B.ItCtName, C.ItGrpName, D.OptDesc As Size, E.OptDesc As Color ");
            SQL.AppendLine("From Tblitem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join TblItemGroup C On A.ItGrpCode=C.ItGrpCode ");
            SQL.AppendLine("Left Join TblOption D On D.OptCat='ItemInformation1' And A.Information1=D.OptCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCat='ItemInformation2' And A.Information2=E.OptCode ");
            SQL.AppendLine("Where A.ActInd='Y'  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item Code", 
                    "Item Name", 
                    "Foreign Name",
                    "Category",

                    //6-8
                    "Group",
                    "Size",
                    "Color"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 80, 300, 300, 150,
                    
                    //6-8
                    150, 150, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();


                Sm.FilterStr(ref Filter, ref cm, TxtItemCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-4
                            "ItName", "ForeignName", "ItCtName", "ItGrpName", "Size",

                            //6
                            "Color"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        private bool IsCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), ItCode))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        override protected void ChooseData()
        {
            int i = 1;
            if (ChkChooseRepeatedly.Checked)
            {
                i = GetChooseRepeatedly();
                if (!(i >= 1 && i <= 999)) return;
            }

            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;
                        for (int Index = 1; Index <= (ChkChooseRepeatedly.Checked ? i : 1); Index++)
                            InsertData(Row);
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private void InsertData(int Row)
        {
            int Row1 = 0, Row2 = 0;

            Row1 = mFrmParent.Grd1.Rows.Count - 1;
            Row2 = Row;

            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
            
            mFrmParent.Grd1.Rows.Add();

        }

        private int GetChooseRepeatedly()
        {
            int i = 0;
            try
            {
                string value = "1";
                if (Sm.InputBox("Run System", "Choose Repeatedly (1-999):", ref value) == DialogResult.OK)
                {
                    i = int.Parse(value);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return i;
        }

        #endregion

        #endregion

        #region Event

        private void TxtItemCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItemCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item Category");
        }
      
        #endregion


    }
}
