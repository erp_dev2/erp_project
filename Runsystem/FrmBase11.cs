﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase11 : Form
    {
        #region Constructor

        public FrmBase11()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {

        }

        virtual protected void FrmClosing(object sender, FormClosingEventArgs e)
        {

        }

        #endregion

        #region Button Method

        virtual protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase11_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        private void FrmBase11_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmClosing(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            BtnPrintClick(sender, e);
        }

        #endregion

        #endregion
    }
}
