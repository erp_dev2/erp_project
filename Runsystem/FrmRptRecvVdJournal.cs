﻿#region Update
/*
    27/07/2021 [WED/IMS] new apps
    14/12/2021 [VIN/IMS] Bug: SOContract terdouble , ambil dr journaldtl
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRecvVdJournal : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRecvVdJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.RecvVdDocNo, B.DocDt, C.VdName, D.SOContractDocNo, A.JournalDocNo, A.JournalDocNo2, ");
            SQL.AppendLine("If(E.DocType = 'RecvVd', G.MenuCode, F.ParValue) MenuCode ");
            SQL.AppendLine("From TblRecvVdJournal A ");
            SQL.AppendLine("Inner Join TblRecvVdHdr B On A.RecvVdDocNo = B.DocNo ");
            SQL.AppendLine("    And B.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblVendor C On B.VdCode = C.VdCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select Distinct T1.RecvVdDocNo, T6.SOCDocNo ");
            //SQL.AppendLine("    From TblRecvVdJournal T1 ");
            //SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.RecvVdDocNo = T2.DocNo And T1.RecvVdDNo = T2.DNo ");
            //SQL.AppendLine("    Inner Join TblPODtl T3 On T2.PODocNo = T3.DocNo And T2.PODNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl T4 ON T3.PORequestDocNo = T4.DocNo And T3.PORequestDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo = T5.DocNo And T4.MaterialRequestDNo = T5.DNo ");
            //SQL.AppendLine("        And T5.MaterialRequestServiceDocNo Is Null ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestHdr T6 On T5.DocNo = T6.DocNo And T6.SOCDocNo Is Not Null ");

            //SQL.AppendLine("    Union All ");

            //SQL.AppendLine("    Select Distinct T1.RecvVdDocNo, T6.SOCDocNo ");
            //SQL.AppendLine("    From TblRecvVdJournal T1 ");
            //SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.RecvVdDocNo = T2.DocNo And T1.RecvVdDNo = T2.DNo ");
            //SQL.AppendLine("    Inner Join TblPODtl T3 On T2.PODocNo = T3.DocNo And T2.PODNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl T4 ON T3.PORequestDocNo = T4.DocNo And T3.PORequestDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo = T5.DocNo And T4.MaterialRequestDNo = T5.DNo ");
            //SQL.AppendLine("        And T5.MaterialRequestServiceDocNo Is Not Null ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestServiceHdr T6 On T5.MaterialRequestServiceDocNo = T6.DocNo And T6.SOCDocNo Is Not Null ");
            //SQL.AppendLine(") D On A.RecvVdDocNo = D.RecvVdDocNo ");
            SQL.AppendLine("Left JOIN tbljournaldtl D ON A.JournalDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblDocAbbreviation E On E.DocType In ('RecvVd', 'RecvVdAutoDO') ");
            SQL.AppendLine("    And E.DocAbbr = Substring_Index(Substring_Index(A.RecvVdDocNo, '/', -3), '/', 1) ");
            SQL.AppendLine("Left Join TblParameter F On F.ParCode = 'MenuCodeForRecvVdAutoCreateDO' ");
            SQL.AppendLine("Left Join TblMenu G On G.Param = 'FrmRecvVd' And G.MenuCode != F.ParValue ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Received#",
                    "",
                    "Date", 
                    "Vendor",
                    "SO Contract#",

                    //6-10
                    "Journal#",
                    "",
                    "Cancel Journal#",
                    "",
                    "MenuCode"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 20, 80, 250, 180,

                    //6-10
                    180, 20, 180, 20, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 7, 9  });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 9, 10 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 8, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count > 0)
                Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 1=1 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.RecvVdDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.RecvVdDocNo;",
                new string[]
                    {
                        //0
                        "RecvVdDocNo", 

                        //1-5
                        "DocDt", 
                        "VdName", 
                        "SOContractDocNo", 
                        "JournalDocNo", 
                        "JournalDocNo2",

                        //6
                        "MenuCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmRecvVd(Sm.GetGrdStr(Grd1, e.RowIndex, 10));
                f.Tag = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param Limit 1 ", Sm.GetGrdStr(Grd1, e.RowIndex, 10));
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0)
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length > 0)
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
