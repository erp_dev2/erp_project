﻿#region Update
// 28/12/2020 [HAR] tambah inputan parent
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmItemSubCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemSubCategoryFind FrmFind;
  
        #endregion

        #region Constructor

        public FrmItemSubCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItScCode, TxtItScName, LueItCtCode, LueParent
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtItScCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItScCode, TxtItScName, LueItCtCode, LueParent
                    }, false);
                    TxtItScCode.Focus();
                    ChkActiveInd.Checked = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItScName, LueItCtCode, LueParent
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    TxtItScName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtItScCode, TxtItScName, LueItCtCode, LueParent });

            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemSubCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            SetLueItCtCode(ref LueItCtCode, "");
            SetLueItScCode(ref LueParent, "");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItScCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItScCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblItemSubCategory Where ItScCode=@ItScCode" };
                Sm.CmParam<String>(ref cm, "@ItScCode", TxtItScCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemSubCategory(ItScCode, ItScName, ItCtCode, ActInd, Parent, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItScCode, @ItScName, @ItCtCode, @ActInd, @Parent, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItScName=@ItScName, ItCtCode=@ItCtCode, ActInd=@ActInd, Parent=@Parent, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItScCode", TxtItScCode.Text);
                Sm.CmParam<String>(ref cm, "@ItScName", TxtItScName.Text);
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItScCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItScCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();


                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItScCode", ItScCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.ItScCode, A.ItScName, A.ItCtCode, A.ActInd, A.Parent " +
                        "From TblItemSubCategory A "+ 
                        "Where A.ItScCode=@ItScCode;",
                        new string[] 
                        {
                            "ItScCode", 
                            "ItScName", "ItCtCode", "ActInd", "Parent"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItScCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItScName.EditValue = Sm.DrStr(dr, c[1]);
                            SetLueItCtCode(ref LueItCtCode, Sm.DrStr(dr, c[0]));
                            Sm.SetLue(LueItCtCode, Sm.DrStr(dr, c[2]));
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                            SetLueItScCode(ref LueParent, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[4]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItScCode, "Item's sub category code", false) ||
                Sm.IsTxtEmpty(TxtItScName, "Item's sub category name", false) ||
                Sm.IsLueEmpty(LueItCtCode, "Item's category") ||
                IsItCtCodeExisted() ||
                IsItCtCodeNotValid();
        }

        private bool IsItCtCodeExisted()
        {
            if (!TxtItScCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select ItScCode From TblItemSubCategory Where ItScCode=@ItScCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@ItScCode", TxtItScCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item's sub category code ( " + TxtItScCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsItCtCodeNotValid()
        {
            if (TxtItScCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = 
                        "Select ItScCode From TblItem " +
                        "Where ItScCode=@ItScCode And ItCtCode<>@ItCtCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@ItScCode", TxtItScCode.Text);
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "You can't change item's category, " + Environment.NewLine +
                        "you already have item(s) with previous category." + Environment.NewLine +
                        "Please update sub category in master item."
                        );
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region SetLue
        private void SetLueItCtCode(ref LookUpEdit Lue, string ItScCode)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Where ActInd = 'Y'  ");
            if (TxtItScCode.Text.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.ItCtCode As Col1, A.ItCtName As Col2 From TblItemCategory A ");
                SQL.AppendLine("Inner Join TblItemSubCategory B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("Where B.ItScCode = '" + ItScCode + "' ");
            }
            SQL.AppendLine(")Tbl Order By Col2");



            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItScCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItScCode);
        }
        private void TxtItScName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItScName);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(SetLueItCtCode), TxtItScCode.Text);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue2(SetLueItScCode), Sm.GetLue(LueItCtCode));

        }


        #endregion

        private void SetLueItScCode(ref LookUpEdit Lue, string ItCtCode)
        {
            var SQL = new StringBuilder();

            
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "' Order By ItScName ");
           
            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion

    }
}
