﻿#region Update
/* 
    15/08/2017 [TKG] filter berdasarkan department optional.
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    07/09/2017 [TKG] data resignee tidak perlu dimunculkan.
    01/10/2021 [DEV/ALL] Menambahkan Filter SITE di Reporting Employee's Attendance untuk memudahkan filter kehadiran employee berdasarkan SITE seperti lokasi kantor/ unit/ toko/ dll.
    13/06/2018 [VIN/IMS] BUG Process4 belum terfilter berdasarkan filter Dt nya 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpAttendance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptEmpAttendance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueAGCode(ref LueAGCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Old Code", 
                        "Position",
                        "Department",
                     
                        //6-10
                        "Site",
                        "Group",
                        "Join",
                        "Resign",
                        "Date",
                        
                        //11-15
                        "Schedule",
                        "Schedule" + Environment.NewLine + "Date (In)",
                        "Schedule" + Environment.NewLine + "Time (In)",
                        "Log" + Environment.NewLine + "Date (In)",
                        "Log" + Environment.NewLine + "Time (In)",
                        
                        //16-20
                        "Actual" + Environment.NewLine + "Date (In)",
                        "Actual" + Environment.NewLine + "Time (In)",
                        "Schedule" + Environment.NewLine + "Date (Out)",
                        "Schedule" + Environment.NewLine + "Time (Out)",
                        "Log" + Environment.NewLine + "Date (Out)",
                        
                        //21-23
                        "Log" + Environment.NewLine + "Time (Out)",
                        "Actual" + Environment.NewLine + "Date (Out)",
                        "Actual" + Environment.NewLine + "Time (Out)"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 80, 150, 150, 
                        
                        //6-10
                        180, 180, 100, 100, 80, 

                        //11-15
                        200, 100, 100, 100, 100,

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-23
                        100, 100, 100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 7, 8, 9, 12, 14, 16, 18, 20, 22 }, false);
            Sm.GrdFormatDate(Grd1, new int[] { 8, 9, 10, 12, 14, 16, 18, 20, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 15, 17, 19, 21, 23 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 7, 8, 9, 12, 14, 16, 18, 20, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            var lWS = new List<WS>();
            var lLog = new List<Log>();
            var lAG = new List<AG>();
            var lActual = new List<Actual>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lWS);
                if (lWS.Count > 0)
                {
                    Process2(ref lLog);
                    Process3(ref lAG);
                    Process4(ref lActual);

                    if (lAG.Count > 0) Process5(ref lWS, ref lAG);
                    if (lActual.Count > 0) Process6(ref lWS, ref lActual);
                    Process7(ref lWS, ref lLog);
                    Process8(ref lWS);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lWS.Clear();
                lLog.Clear();
                lAG.Clear();
                lActual.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            }
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<WS> l) 
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL.AppendLine("Select A.Dt, A.EmpCode, B.EmpName, B.EmpCodeOld, D.PosName, C.DeptName, F.SiteName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, G.WSName, ");
            SQL.AppendLine("G.In1 As LogIn, G.bIn1 As bLogIn, G.aIn1 As aLogIn, ");
            SQL.AppendLine("Case When G.Out3 Is Null Then G.Out1 Else G.Out3 End As LogOut, ");
            SQL.AppendLine("Case When G.bOut3 Is Null Then G.bOut1 Else G.bOut3 End As bLogOut, ");
            SQL.AppendLine("Case When G.aOut3 Is Null Then G.aOut1 Else G.aOut3 End As aLogOut ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode " + Filter);
            SQL.AppendLine("    And B.JoinDt<=@EndDt ");
            SQL.AppendLine("    And (B.ResignDt is Null Or (B.ResignDt Is Not Null And B.ResignDt>@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Inner Join TblWorkSchedule G On A.WsCode=G.WsCode ");
            SQL.AppendLine("Left Join TblSite F On B.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",
 
                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName", 
                    
                    //6-10
                    "SiteName", "JoinDt", "ResignDt", "WSName", "LogIn", 
                    
                    //11-15
                    "bLogIn", "aLogIn", "LogOut", "bLogOut", "aLogOut"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WS()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            EmpName = Sm.DrStr(dr, c[2]),
                            EmpCodeOld = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            DeptName = Sm.DrStr(dr, c[5]),
                            SiteName = Sm.DrStr(dr, c[6]),
                            JoinDt = Sm.DrStr(dr, c[7]),
                            ResignDt = Sm.DrStr(dr, c[8]),
                            WSName = Sm.DrStr(dr, c[9]),
                            LogIn = Sm.DrStr(dr, c[10]),
                            bLogIn = Sm.DrStr(dr, c[11]),
                            aLogIn = Sm.DrStr(dr, c[12]),
                            LogOut = Sm.DrStr(dr, c[13]),
                            bLogOut = Sm.DrStr(dr, c[14]),
                            aLogOut = Sm.DrStr(dr, c[15]),

                            IsOneDay = true,
                            IsWarning = false,
                            Dt2 = string.Empty,
                            AGCode = string.Empty,
                            AGName = string.Empty,
                            InDtL = string.Empty,
                            InTmL = string.Empty,
                            OutDtL = string.Empty,
                            OutTmL = string.Empty,
                            InDtA = string.Empty,
                            InTmA = string.Empty,
                            OutDtA = string.Empty,
                            OutTmA = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Log> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt2)).AddDays(1)));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode " + Filter);
            SQL.AppendLine("    And B.JoinDt<=@EndDt ");
            SQL.AppendLine("    And (B.ResignDt is Null Or (B.ResignDt is Not Null And B.ResignDt>=@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt Order By A.EmpCode, A.Dt, A.Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Log()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DtTm = Sm.Left(Sm.DrStr(dr, c[1])+Sm.DrStr(dr, c[2]), 12)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<AG> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (Sm.GetLue(LueDeptCode).Length>0)
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpCodeOld", "C.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

            SQL.AppendLine("Select B.EmpCode, A.AGCode, A.AGname ");
            SQL.AppendLine("From TblAttendanceGrpHdr A ");
            SQL.AppendLine("Inner Join TblAttendanceGrpDtl B On A.AGCode=B.AGCode " );
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode " + Filter);
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.ActInd='Y' ");
            if (ChkAGCode.Checked)
            {
                SQL.AppendLine("And A.AGCode=@AGCode ");
                if (Sm.GetLue(LueDeptCode).Length > 0)
                    SQL.AppendLine("And (A.DeptCode Is Null Or (A.DeptCode Is Not Null And A.DeptCode=@DeptCode)) ");
                Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            }            
            SQL.AppendLine("Order By B.EmpCode;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "AGCode", "AGName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AG()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            AGCode = Sm.DrStr(dr, c[1]),
                            AGName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Actual> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpCodeOld", "C.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else B.InOutA1 End As Actual ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode " + Filter);
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And B.Dt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Order By B.EmpCode, B.Dt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Actual" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Actual()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            DtTm = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process5(ref List<WS> lWS, ref List<AG> lAG)
        {
            int Temp = 0;
            bool IsFind = false;
            for (var i = 0; i < lAG.Count; i++)
            {
                IsFind = false;
                for (var j = Temp; j < lWS.Count; j++)
                {
                    if (string.Compare(lAG[i].EmpCode, lWS[j].EmpCode) == 0)
                    {
                        lWS[j].AGCode = lAG[i].AGCode;
                        lWS[j].AGName = lAG[i].AGName;
                        Temp = j;
                        IsFind = true;
                    }
                    else
                    {
                        if (IsFind) break;
                    }
                }
            }

            if (ChkAGCode.Checked)
            {
                var AGCode = Sm.GetLue(LueAGCode);
                lWS.RemoveAll((x) => !(x.AGCode == AGCode && x.AGCode.Length>0));
            }
        }

        private void Process6(ref List<WS> lWS, ref List<Actual> lActual)
        {
            for (var i = 0; i < lActual.Count; i++)
            {
                for (var j = 0; j < lWS.Count; j++)
                {
                    if (string.Compare(lActual[i].EmpCode, lWS[j].EmpCode) == 0 &&
                        string.Compare(lActual[i].Dt, lWS[j].Dt) == 0)
                    {
                        if (lActual[i].DtTm.Length == 24)
                        {
                            lWS[j].InDtA = Sm.Left(lActual[i].DtTm, 8);
                            lWS[j].InTmA = lActual[i].DtTm.Substring(8, 4);
                            lWS[j].OutDtA = lActual[i].DtTm.Substring(12, 8);
                            lWS[j].OutTmA = Sm.Right(lActual[i].DtTm, 4);
                        }
                        if (lActual[i].DtTm.Length == 12)
                        {
                            lWS[j].InDtA = Sm.Left(lActual[i].DtTm, 8);
                            lWS[j].InTmA = lActual[i].DtTm.Substring(8, 4);
                        }
                        break;
                    }
                }
            }
        }

        private void Process7(ref List<WS> lWS, ref List<Log> lLog)
        {
            string 
                EmpCode= string.Empty,
                Dt0 = string.Empty,
                Dt = string.Empty,
                Dt2 = string.Empty,
                LogIn = string.Empty,
                bLogIn = string.Empty,
                aLogIn = string.Empty,
                LogOut = string.Empty,
                bLogOut = string.Empty,
                aLogOut = string.Empty
                ;
            bool IsOneDay = true;

            string Now = Sm.ServerCurrentDateTime();

            if (Now.Length > 0) Now = Sm.Left(Now, 12);

            for (var i = 0; i < lWS.Count; i++)
            {
                EmpCode = lWS[i].EmpCode;
                Dt = lWS[i].Dt;
                Dt0 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(-1)), 8);
                Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(1)), 8);

                IsOneDay = (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].LogOut) <= 0);

                lWS[i].Dt2 = Dt2;
                lWS[i].IsOneDay = IsOneDay;

                LogIn = Dt + lWS[i].LogIn;
                if (Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn) <= 0)
                    bLogIn = Dt + lWS[i].bLogIn;
                else
                    bLogIn = Dt0 + lWS[i].bLogIn;

                if (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].aLogIn) <= 0)
                    aLogIn = Dt+lWS[i].aLogIn;
                else
                    aLogIn = Dt2+lWS[i].aLogIn;

                if (IsOneDay)
                    LogOut = Dt+lWS[i].LogOut;
                else
                    LogOut = Dt2+lWS[i].LogOut;

                if (IsOneDay)
                    bLogOut = Dt + lWS[i].bLogOut;
                else
                {
                    if (Sm.CompareDtTm(lWS[i].bLogOut, lWS[i].LogOut) <= 0)
                        bLogOut = Dt2 + lWS[i].bLogOut;
                    else
                        bLogOut = Dt + lWS[i].bLogOut;
                }

                if (IsOneDay)
                {
                    if (Sm.CompareDtTm(lWS[i].LogOut, lWS[i].aLogOut) <= 0)
                        aLogOut = Dt+lWS[i].aLogOut;
                    else
                        aLogOut = Dt2 + lWS[i].aLogOut;
                }
                else
                    aLogOut = Dt2+lWS[i].aLogOut;

                if (lLog.Count > 0)
                {
                    foreach (var index in lLog
                        .Where(x =>
                                string.Compare(x.EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(x.DtTm, bLogIn) >= 0 &&
                                Sm.CompareDtTm(x.DtTm, aLogIn) <= 0
                            )
                        .OrderBy(o => o.DtTm)
                        .Take(1)
                        )
                    {
                        lWS[i].InDtL = Sm.Left(index.DtTm, 8);
                        lWS[i].InTmL = Sm.Right(index.DtTm, 4);
                    }

                    foreach (var index in lLog
                        .Where(x =>
                                string.Compare(x.EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(x.DtTm, bLogOut) >= 0 &&
                                Sm.CompareDtTm(x.DtTm, aLogOut) <= 0
                            )
                        .OrderByDescending(o => o.DtTm)
                        .Take(1)
                        )
                    {
                        lWS[i].OutDtL = Sm.Left(index.DtTm, 8);
                        lWS[i].OutTmL = Sm.Right(index.DtTm, 4);
                    }
                }

                if (LogIn.Length == 12 && 
                    Sm.CompareDtTm(LogIn, Now) < 0 &&
                    (lWS[i].InDtL.Length == 0 || lWS[i].InTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 &&
                    Sm.CompareDtTm(LogOut, Now) < 0 &&
                    (lWS[i].OutDtL.Length == 0 || lWS[i].OutTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogIn.Length == 12 && lWS[i].InDtL.Length > 0 && lWS[i].InTmL.Length > 0 &&
                    Sm.CompareDtTm(LogIn, lWS[i].InDtL+lWS[i].InTmL) < 0)
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 && lWS[i].OutDtL.Length > 0 && lWS[i].OutTmL.Length > 0 &&
                    Sm.CompareDtTm(lWS[i].OutDtL + lWS[i].OutTmL, LogOut) < 0)
                    lWS[i].IsWarning = true;
            }
        }

        private void Process8(ref List<WS> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {

                r = Grd1.Rows.Add();
                r.Cells[0].Value = i+1;
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].EmpCodeOld;
                r.Cells[4].Value = l[i].PosName;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = l[i].SiteName;
                r.Cells[7].Value = l[i].AGName;
                SetDt(r, 8, l[i].JoinDt);
                SetDt(r, 9, l[i].ResignDt);
                SetDt(r, 10, l[i].Dt);
                r.Cells[11].Value = l[i].WSName;
                SetDt(r, 12, l[i].Dt);
                SetTm(r, 13, l[i].LogIn);
                SetDt(r, 14, l[i].InDtL);
                SetTm(r, 15, l[i].InTmL);
                SetDt(r, 16, l[i].InDtA);
                SetTm(r, 17, l[i].InTmA);
                if (l[i].IsOneDay)
                    SetDt(r, 18, l[i].Dt);
                else
                    SetDt(r, 18, l[i].Dt2);
                SetTm(r, 19, l[i].LogOut);
                SetDt(r, 20, l[i].OutDtL);
                SetTm(r, 21, l[i].OutTmL);
                SetDt(r, 22, l[i].OutDtA);
                SetTm(r, 23, l[i].OutTmA);

                if (l[i].IsWarning) r.BackColor = Color.Red;
            }
            Grd1.EndUpdate();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetDt(iGRow r, int c, string Dt)
        {
            if (Dt.Length==0)
                r.Cells[c].Value = string.Empty;
            else
                r.Cells[c].Value = Sm.ConvertDate(Dt);
        }

        private void SetTm(iGRow r, int c, string Tm)
        {
            if (Tm.Length == 0)
                r.Cells[c].Value = string.Empty;
            else
                r.Cells[c].Value = Sm.Left(Tm, 2) + ":" + Tm.Substring(2, 2); 
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DteDocDt2.DateTime = DteDocDt1.DateTime;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
            string DeptCode = Sm.GetLue(LueDeptCode);
            LueAGCode.EditValue = null;
            SetLueAGCode(ref LueAGCode, DeptCode);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), DeptCode);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Attendance group");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class WS
        {
            public string Dt { get; set; }
            public string Dt2 { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public string PosName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string WSName { get; set; }
            public bool IsOneDay { get; set; }
            public string LogIn { get; set; }
            public string bLogIn { get; set; }
            public string aLogIn { get; set; }
            public string LogOut { get; set; }
            public string bLogOut { get; set; }
            public string aLogOut { get; set; }
            public string AGCode { get; set; }
            public string AGName { get; set; }
            public string InDtL { get; set; }
            public string InTmL { get; set; }
            public string OutDtL { get; set; }
            public string OutTmL { get; set; }
            public string InDtA { get; set; }
            public string InTmA { get; set; }
            public string OutDtA { get; set; }
            public string OutTmA { get; set; }
            public bool IsWarning { get; set; }
        }

        private class AG
        {
            public string AGCode { get; set; }
            public string AGName { get; set; }
            public string EmpCode { get; set; }
        }

        private class Log
        {
            public string EmpCode { get; set; }
            public string DtTm { get; set; }
        }

        private class Actual
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string DtTm { get; set; }
        }

        #endregion
    }
}
