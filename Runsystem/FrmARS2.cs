﻿#region Update
/*
    08/07/2020 [TKG/YK] AR Settlement For Project
    14/02/2022 [TKG/PHT] merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmARS2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCtCode = string.Empty, mMainCurCode = string.Empty,
            mProjectAcNoFormula = string.Empty,
            mAcNo = string.Empty;
        private bool
            mIsAutoJournalActived = false,
            mIsARSettlementJournalReverse = false;
        internal FrmARS2Find FrmFind;

        #endregion

        #region Constructor

        public FrmARS2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "AR Settlement";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark, MeeCancelReason }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnSalesInvoice.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5 });
                    DteDocDt.Focus();
                    BtnSalesInvoice.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason }, false);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAcNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtSalesInvoiceDocNo, TxtCtCode, DteTaxInvDt, TxtCurCode, TxtOutstandingInv,
                TxtTaxInvDocument, TxtAmountARS, MeeRemark, MeeCancelReason
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmountARS, TxtOutstandingInv
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmARS2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnSalesInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FrmARS2Dlg2(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnSalesInvoice2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSalesInvoiceDocNo, "Sales Invoice", false))
            {
                try
                {
                    if (TxtSalesInvoiceDocNo.Text.Contains("SIPR"))
                    {
                        var f1 = new FrmSalesInvoice5(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = TxtSalesInvoiceDocNo.Text;
                        f1.ShowDialog();
                    }
                    else
                    {
                        var f1 = new FrmSalesInvoice(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = TxtSalesInvoiceDocNo.Text;
                        f1.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "ARS", "ARSDtl" };

            var l = new List<ARSHdr>();
            var ldtl = new List<ARSDtl>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine("Date_Format(A.DocDt,'%d %M %Y')As DocDt ");
            SQL.AppendLine("From TblARSHdr A  ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocDt"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ARSHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = TxtDocNo.Text,//Sm.DrStr(dr, c[6]),

                            DocDt = Sm.DrStr(dr, c[5]),
                            SalesInvoiceDocNo = TxtSalesInvoiceDocNo.Text, //Sm.DrStr(dr, c[7]),
                            CtName = TxtCtCode.Text, //Sm.DrStr(dr, c[8]),
                            TaxInvDocument = TxtTaxInvDocument.Text, //Sm.DrStr(dr, c[9]),
                            TaxInvDt = DteTaxInvDt.Text, //Sm.DrStr(dr, c[10]),
                            CurCode = TxtCurCode.Text, //Sm.DrStr(dr, c[11]),
                            AmtSet = TxtAmountARS.Text, //Sm.DrStr(dr, c[12]),
                            Remark = MeeRemark.Text, //Sm.DrStr(dr, c[13]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DNo, A.ACNo, B.ACDesc, A.DAmt, A.CAmt, A.Remark  ");
                SQLDtl.AppendLine("From TblARSDtl A ");
                SQLDtl.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "ACNo",
                         "ACDesc",
                         "DAmt",
                         "CAmt",
                         "Remark"

                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new ARSDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            ACNo = Sm.DrStr(drDtl, cDtl[1]),
                            ACDesc = Sm.DrStr(drDtl, cDtl[2]),
                            DAmt = Sm.DrDec(drDtl, cDtl[3]),
                            CAmt = Sm.DrDec(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("ARS", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && 
                TxtDocNo.Text.Length == 0 && 
                !Sm.IsTxtEmpty(TxtSalesInvoiceDocNo, "Sales invoice#", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmARS2Dlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmtWithCOA();
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtSalesInvoiceDocNo, "Sales invoice#", false))
                Sm.FormShowDialog(new FrmARS2Dlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 4].Value = 0;
                ComputeAmtWithCOA();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 3].Value = 0;
                ComputeAmtWithCOA();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (!Sm.IsTxtEmpty(TxtSalesInvoiceDocNo, "Sales Invoice#", false))
                RecomputeOutstandingSI(TxtSalesInvoiceDocNo.Text);

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Ars", "TblARSHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveARSHdr(DocNo));
            cml.Add(UpdateProcessIndSI());
            cml.Add(SaveARSDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveARSDtl(DocNo, Row));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 Sm.IsTxtEmpty(TxtAmountARS, "Settlement Amount", true) ||
                 Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                 IsGrdEmpty() ||
                 IsGrdExceedMaxRecords() ||
                 IsJournalAmtNotBalanced() ||
                 IsSettlementBiggerThanOutstanding() ||
                 IsSettlementSmallerThanNull();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsSettlementBiggerThanOutstanding()
        {
            decimal outs = Decimal.Parse(TxtOutstandingInv.Text);
            decimal sett = Decimal.Parse(TxtAmountARS.Text);
            if (sett > outs)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Settlemet amount is bigger than outstanding amount.");
                return true;
            }
            return false;
        }

        private bool IsSettlementSmallerThanNull()
        {
            decimal sett = Decimal.Parse(TxtAmountARS.Text);
            if (sett <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Settlemet amount must be greater than 0.");
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd1, Row, 3);
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd1, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveARSHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblARSHdr(DocNo, DocDt, CancelInd, SalesInvoiceDocNo, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @SalesInvoiceDocNo, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SalesInvoiceDocNo", TxtSalesInvoiceDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmountARS.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveARSDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AR Settlement (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblARSDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveARSDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblARSDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateProcessIndSI()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set ");
            if (TxtDocNo.Text.Length == 0)
            {
                if ((Decimal.Parse(TxtOutstandingInv.Text) - Decimal.Parse(TxtAmountARS.Text)) == 0)
                {
                    SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
                else
                {
                    SQL.AppendLine("    ProcessInd='P', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
            }
            else
            {
                if (IsDataSIAlreadyExist())
                {
                    SQL.AppendLine("    ProcessInd='P', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
                else
                {
                    SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
            }
            SQL.AppendLine("Where DocNo=@DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSalesInvoiceDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDataSIAlreadyExist()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SalesInvoiceDocNo From TblARSHdr ");
            SQL.AppendLine("Where CancelInd='N' And SalesInvoiceDocNo=@SalesInvoiceDocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SalesInvoiceDocNo", TxtSalesInvoiceDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                return true;
            }

            return false;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARSHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('AR Settlement : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblARSHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, B.DNo, B.AcNo, ");
            if (mIsARSettlementJournalReverse)
            {
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                    SQL.AppendLine("B.DAmt, B.CAmt, ");
                else
                    SQL.AppendLine("B.DAmt*IfNull(C.Amt, 0) As DAmt, B.CAMt*IfNull(C.Amt, 0) As CAmt, ");
            }
            else
            {
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                    SQL.AppendLine("B.CAmt As DAmt, B.DAMt As CAmt, ");
                else
                    SQL.AppendLine("B.CAmt*IfNull(C.Amt, 0) As DAmt, B.DAMt*IfNull(C.Amt, 0) As CAmt, ");
            }

            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblARSDtl B On B.DocNo=@DocNo ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                SQL.AppendLine(") C On 0=0 ");
            }
            SQL.AppendLine("Where A.DocNo=@JournalDocNo Order By B.AcNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelARSHdr());
            cml.Add(UpdateProcessIndSI());
            if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblARSHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelARSHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARSHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblARSHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling AR Settlement : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblARSHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblARSHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowARSHdr(DocNo);
                ShowARSDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowARSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SalesInvoiceDocNo, C.CtName, Null As TaxInvDocument, B.DocDt As TaxInvDt, ");
            // kalau di Sales Invoice, Amt di TblSalesInvoiceHdr ==> Amt = Amt-DownPayment
            SQL.AppendLine("B.Amt-IfNull(D.Amt, 0)-IfNull(E.Amt, 0) As OutstandingInv, ");
            SQL.AppendLine("B.CurCode, A.Amt As AmtSet, A.Remark ");
            SQL.AppendLine("From TblARSHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoice5Hdr B On A.SalesInvoiceDocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("   Select Sum(A.Amt) AS Amt ");
            SQL.AppendLine("   From TblIncomingPaymentDtl A ");
            SQL.AppendLine("   Inner Join TblIncomingPaymentHdr B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status<>'C' ");
            SQL.AppendLine("   Where A.InvoiceDocNo In (Select SalesInvoiceDocNo From TblARSHdr Where DocNo=@DocNo) ");
            SQL.AppendLine(") D On 1=1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblARSHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo<>@DocNo ");
            SQL.AppendLine("    And SalesInvoiceDocNo In (Select SalesInvoiceDocNo From TblARSHdr Where DocNo=@DocNo) ");
            SQL.AppendLine(") E On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "CancelInd", "SalesInvoiceDocNo", "CtName", "TaxInvDocument",
                        "TaxInvDt", "OutstandingInv", "CurCode", "AmtSet", "Remark",
                        "CancelReason"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[11]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        TxtSalesInvoiceDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetDte(DteTaxInvDt, Sm.DrStr(dr, c[6]));
                        var a = Sm.DrDec(dr, c[7]);
                        TxtOutstandingInv.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAmountARS.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ShowARSDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ACNo, B.ACDesc, A.DAmt, A.CAmt, A.Remark  ");
            SQL.AppendLine("From TblARSDtl A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                }, false, false, true, false
        );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowDataSalesInvoice(string SalesInvoiceDocNo)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, NULL As TaxInvDocument, A.CtCode, C.CtName, A.CurCode, ");
            SQL.AppendLine("A.Amt-IfNull(B.Amt2, 0.00)-IfNull(D.Amt3, 0.00) As Outstanding, ");
            if (mProjectAcNoFormula == "2")
                SQL.AppendLine("Concat(E.ParValue, J.ProjectCode2) As AcNo ");
            else
                SQL.AppendLine("Concat(E.ParValue, J.ProjectCode) As AcNo ");
            SQL.AppendLine("From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.InvoiceDocNo, SUM(A.AMT) AS Amt2 ");
            SQL.AppendLine("    From TblIncomingPaymentDtl A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' And IfNull(B.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    Where A.InvoiceDocNo In (Select DocNo From TblSalesInvoice5Hdr Where CancelInd='N' And ProcessInd<>'F') ");
            SQL.AppendLine("    Group By InvoiceDocNo ");
            SQL.AppendLine(") B On B.InvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  A.SalesInvoiceDocNo, SUM(A.Amt) As Amt3 ");
            SQL.AppendLine("    From TblARSHdr A  ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.SalesInvoiceDocNo In (Select DocNo From TblSalesInvoice5Hdr Where CancelInd='N' And ProcessInd<>'F') ");
            SQL.AppendLine("    Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine(")D On D.SalesInvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("Left Join TblSalesInvoice5Dtl F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl G On F.ProjectImplementationDocNo=G.DocNo And F.ProjectImplementationDNo=G.DNo ");
            SQL.AppendLine("Left Join TblProjectImplementationHdr H On G.DocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblSOContractRevisionHdr I On H.SOContractDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr J On I.SOCDocNo=J.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.ProcessInd <> 'F' ");
            SQL.AppendLine("And A.Amt-IfNull(B.Amt2, 0.00)-IfNull(D.Amt3, 0.00)>0.00 ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine(") T ");
            

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", SalesInvoiceDocNo);
            Sm.ShowDataInCtrl(
               ref cm,
               SQL.ToString(),
               new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CtCode", "CtName", "TaxInvDocument", "CurCode", 
                        
                        //6-7
                        "Outstanding", "AcNo"
                    },
               (MySqlDataReader dr, int[] c) =>
               {
                   TxtSalesInvoiceDocNo.EditValue = Sm.DrStr(dr, c[0]);
                   Sm.SetDte(DteTaxInvDt, Sm.DrStr(dr, c[1]));
                   mCtCode = Sm.DrStr(dr, c[2]);
                   TxtCtCode.EditValue = Sm.DrStr(dr, c[3]);
                   TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[4]);
                   TxtCurCode.EditValue = Sm.DrStr(dr, c[5]);
                   TxtOutstandingInv.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                   mAcNo = Sm.DrStr(dr, c[7]);
               }, false
              );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'MainCurCode', 'ProjectAcNoFormula', 'IsARSettlementJournalReverse' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsARSettlementJournalReverse": mIsARSettlementJournalReverse = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;

                            //string
                            case "ProjectAcNoFormula": mProjectAcNoFormula = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        internal void ComputeAmtWithCOA()
        {
            var Amt = 0m;
            var AcNo = string.Empty;

            AcNo = mAcNo;
            
            var AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);
            if (AcType.Length > 0)
            {
                if (!mIsARSettlementJournalReverse)
                {
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            if (Sm.GetGrdDec(Grd1, r, 3) != 0m)
                            {
                                if (AcType == "C")
                                    Amt -= Sm.GetGrdDec(Grd1, r, 3);
                                else
                                    Amt += Sm.GetGrdDec(Grd1, r, 3);
                            }
                            if (Sm.GetGrdDec(Grd1, r, 4) != 0m)
                            {
                                if (AcType == "C")
                                    Amt += Sm.GetGrdDec(Grd1, r, 4);
                                else
                                    Amt -= Sm.GetGrdDec(Grd1, r, 4);
                            }
                        }
                    }
                }
                else
                {
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            if (Sm.GetGrdDec(Grd1, r, 3) != 0m)
                            {
                                if (AcType == "D")
                                    Amt -= Sm.GetGrdDec(Grd1, r, 3);
                                else
                                    Amt += Sm.GetGrdDec(Grd1, r, 3);
                            }
                            if (Sm.GetGrdDec(Grd1, r, 4) != 0m)
                            {
                                if (AcType == "D")
                                    Amt += Sm.GetGrdDec(Grd1, r, 4);
                                else
                                    Amt -= Sm.GetGrdDec(Grd1, r, 4);
                            }
                        }
                    }
                }
            }
            TxtAmountARS.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public void RecomputeOutstandingSI(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(T.Amt) Amt From ( ");
            SQL.AppendLine("    Select A.Amt- IfNull(B.AmtInvIP, 0) - IfNull(C.AmtARS, 0) As Amt ");
            SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.InvoiceDocNo, SUM(A.AMT) AS AmtInvIP ");
            SQL.AppendLine("        From TblIncomingPaymentDtl A ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("        Where B.Status <> 'C' ");
            SQL.AppendLine("        And A.InvoiceDocNo = @Param ");
            SQL.AppendLine("        Group By InvoiceDocNo ");
            SQL.AppendLine("    )B On B.InvoiceDocNo = A.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, SUM(A.Amt) As AmtARS ");
            SQL.AppendLine("        From TblARSHdr A  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("        And A.SalesInvoiceDocNo = @DocNo ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.SalesInvoiceDocNo ");
            SQL.AppendLine("    Where A.DocNo = @Param And A.CancelInd = 'N' And A.ProcessInd <> 'F' ");
            SQL.AppendLine(") T; ");

            var Amt = Sm.GetValue(SQL.ToString(), DocNo);
            if (Amt.Length>0)
                TxtOutstandingInv.EditValue = Sm.FormatNum(decimal.Parse(Amt), 0);
            else
                TxtOutstandingInv.EditValue = Sm.FormatNum(0m, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

        #region Report Class

        private class ARSHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SalesInvoiceDocNo { get; set; }
            public string CtName { get; set; }
            public string TaxInvDocument { get; set; }
            public string TaxInvDt { get; set; }
            public string CurCode { get; set; }
            public string AmtSet { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }

        }

        private class ARSDtl
        {
            public string DNo { get; set; }
            public string ACNo { get; set; }
            public string ACDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
        }

        #endregion

    }
}
