﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionPenaltyDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProductionPenalty mFrmParent;
        string mSQL = "", mShopFloorControlDocNo = "";

        #endregion

        #region Constructor

        public FrmProductionPenaltyDlg2(FrmProductionPenalty FrmParent, string ShopFloorControlDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mShopFloorControlDocNo = ShopFloorControlDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, ");
            SQL.AppendLine("A.Qty-IfNull(D.Qty, 0) As Qty, B.PlanningUomCode, A.Qty2-IfNull(D.Qty2, 0) As Qty2, B.PlanningUomCode2, ");
            SQL.AppendLine("IfNull(C.ItIndex, 0) As ItIndex ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDNo, Sum(Qty) Qty, Sum(Qty2) Qty2 ");
            SQL.AppendLine("    From TblProductionPenaltyHdr T1 ");
            SQL.AppendLine("    Inner Join TblProductionPenaltyDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.ShopFloorControlDocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DNo=D.ShopFloorControlDNo ");
            SQL.AppendLine("Where A.DocNo=@ShopFloorControlDocNo And A.ProcessInd2<>'F' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "DNo", 
                        "Item's Code", 
                        "",
                        "Item's Local Code", 
                        
                        //6-10
                        "Item's Name",
                        "Batch Number",
                        "Quantity",
                        "Uom",
                        "Quantity 2",

                        //11-12
                        "Uom 2",
                        "Index"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 80, 20, 80, 
                        
                        //6-10
                        150, 180, 80, 80, 80, 
                        
                        //11-12
                        80, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 12 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 10, 11 }, false);
            ShowPlanningUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowPlanningUomCode()
        {
            if (mFrmParent.mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And Position(Concat('##', A.DNo, '##') In @DNo)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DNo", mFrmParent.GetSelectedDNo());
                Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", mShopFloorControlDocNo);
                

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DNo;",
                        new string[] 
                        { 
                            //0
                            "DNo",
 
                            //1-5
                            "ItCode", "ItCodeInternal", "ItName", "BatchNo", "Qty", 
                            
                            //6-9
                            "PlanningUomCode", "Qty2", "PlanningUomCode2", "ItIndex"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            try
            {
                if (Grd1.Rows.Count != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDNoAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 12);

                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 14, 15 });

                            mFrmParent.ShowGrdInfo2();
                        }
                    }
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsDNoAlreadyChosen(int Row)
        {
            string Key1 = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Key1)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 3));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 3));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        #endregion

        #endregion

    }
}
