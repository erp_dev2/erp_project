﻿#region Update
/*
    13/12/2017 [HAR] tambah inputan account mutations cost category
    16/01/2018 [HAR] costcenter, costcategory, mutations account wajib di isi jika parameter journalactive = 'Y'
    17/01/2018 [HAR] merah mandatory saat form load
    10/08/2020 [TKG/MAI] validasi apakah warehouse sudah pernah digunakan di transaksi inventory atau setting warehouse group sebelum dihapus.
    26/01/2021 [VIN/PHT] tambah inputan site berdasarkan parameter IsWarehouseUseSite  
    05/05/2021 [HAR/SRN] tambah inputan payment type untuk setting COA POS
    26/07/2021 [TKG/IMS] tambah indikator MAP
 *  02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
 *  24/01/2023 [HAR/PHT] tambah informasi whs old code
 *  06/02/2023 [VIN/ALL] Lup Warehouse hilang
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmWarehouse : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mWhsCode = string.Empty; //if this application is called from other application
        internal FrmWarehouseFind FrmFind;
        internal bool
            mIsAutoJournalActived = false,
            mIsWarehouseUseSite = false,
            mIsWarehouseUsePOS = false,
            mIsMovingAvgEnabled = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmWarehouse(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Warehouse";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetGrd();

                TcWarehouse.SelectedTabPage = Tp2;
                Tp2.PageVisible = mIsWarehouseUsePOS;
                if (mIsWarehouseUsePOS)
                {    
                    Sl.SetLuePosPayCode(ref LuePaymentType);
                    LuePaymentType.Visible = false;
                }

                TcWarehouse.SelectedTabPage = Tp1;
                if (mIsAutoJournalActived) LblCC.ForeColor = LblCCt.ForeColor = LblMCCt.ForeColor = Color.Red;
                if (!mIsWarehouseUseSite) LblSite.Visible = LueSiteCode.Visible = false;
                ChkMovingAvgInd.Visible = mIsMovingAvgEnabled;

                Sl.SetLueWhsCtCode(ref LueWhsCtCode);
                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueCCCode(ref LueCCCode);
                Sl.SetLueSiteCode(ref LueSiteCode);

                //if this application is called from other application
                if (mWhsCode.Length != 0)
                {
                    ShowData(mWhsCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCode, TxtWhsName, LueWhsCtCode, MeeAddress, LueCityCode, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        MeeContactPerson, LueCCCode, LueCCtCode, TxtAcNo, TxtAcDesc,
                        MeeRemark, LueSiteCode, ChkMovingAvgInd, TxtWhsCodeOld
                    }, true);
                    TxtWhsCode.Focus();
                    Grd1.ReadOnly = true;                  
                    BtnAcNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCode, TxtWhsName, LueWhsCtCode, MeeAddress, LueCityCode, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        MeeContactPerson, LueCCCode, MeeRemark, LueSiteCode, ChkMovingAvgInd,
                        TxtWhsCodeOld
                    }, false);
                    TxtWhsCode.Focus();
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3 });
                    BtnAcNo.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtWhsCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsName, LueWhsCtCode, MeeAddress, LueCityCode, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeContactPerson, 
                        LueCCCode, MeeRemark, LueSiteCode, ChkMovingAvgInd,
                        TxtWhsCodeOld
                    }, false);
                    TxtWhsName.Focus();
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {2, 3 });
                    BtnAcNo.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWhsCode, TxtWhsName, LueWhsCtCode, MeeAddress, LueCityCode, 
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                MeeContactPerson, LueCCCode, LueCCtCode,  TxtAcNo, TxtAcDesc,  
                LueSiteCode, TxtWhsCodeOld, MeeRemark
            });
            ChkMovingAvgInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void GetParameter()
        {
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsWarehouseUseSite = Sm.GetParameterBoo("IsWarehouseUseSite");
            //mIsWarehouseUsePOS = Sm.GetParameterBoo("IsWarehouseUsePOS");
            //mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsWarehouseUseSite', 'IsWarehouseUsePOS', 'IsMovingAvgEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsWarehouseUseSite": mIsWarehouseUseSite = ParValue == "Y"; break;
                            case "IsWarehouseUsePOS": mIsWarehouseUsePOS = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Paytpno",
                    "Payment Type",
                    "",
                    "Account Number",
                    "Account Desc"
                },
                new int[] 
                {
                    50, 
                    80, 180, 20, 180, 250
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 1});
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0,1, 4,5   });

            #endregion
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWarehouseFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
          
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCode, string.Empty, false) || 
                Sm.StdMsgYN("Delete", string.Empty) == DialogResult.No ||
                IsWhsCodeAlreadyUsed()) 
                return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWarehouse Where WhsCode=@WhsCode" };
                Sm.CmParam<String>(ref cm, "@WhsCode", TxtWhsCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveWarehouse());
                if (mIsWarehouseUsePOS)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWarehouseDtl(Row));
                }

                Sm.ExecCommands(cml);

                ShowData(TxtWhsCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #region Show Data

        public void ShowData(string WhsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                var CCCode = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                
                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);

                SQL.AppendLine("Select A.WhsCode, A.WhsName, A.WhsCtCode, A.Address, A.CityCode, A.PostalCd, ");
                SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile, A.ContactPerson, ");
                SQL.AppendLine("A.CCCode, A.CCtCode, A.Acno, B.AcDesc, A.SiteCode, A.Remark, A.WhsCodeOld, ");
                if (mIsMovingAvgEnabled)
                    SQL.AppendLine("A.MovingAvgInd ");
                else
                    SQL.AppendLine("'N' As MovingAvgInd ");
                SQL.AppendLine("From TblWarehouse A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
                SQL.AppendLine("Where A.WhsCode=@WhsCode;");

                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "WhsCode", 
                            "WhsName", "WhsCtCode", "Address", "CityCode", "PostalCd",
                            "Phone", "Fax", "Email", "Mobile", "ContactPerson", 
                            "CCCode", "CCtCode", "AcNo", "AcDesc", "SiteCode",
                            "Remark", "MovingAvgInd", "WhsCodeOld"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWhsCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWhsName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueWhsCtCode, Sm.DrStr(dr, c[2]));
                            MeeAddress.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[4]));
                            TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                            TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                            TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                            MeeContactPerson.EditValue = Sm.DrStr(dr, c[10]);
                            CCCode = Sm.DrStr(dr, c[11]);
                            Sm.SetLue(LueCCCode, CCCode);
                            Sl.SetLueCCtCode(ref LueCCtCode, CCCode);
                            Sm.SetLue(LueCCtCode, Sm.DrStr(dr, c[12]));
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[13]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[14]);
                            Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[15]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                            ChkMovingAvgInd.Checked = Sm.DrStr(dr, c[17])=="Y";
                            TxtWhsCodeOld.EditValue = Sm.DrStr(dr, c[18]);
                        }, true
                    );

                if (mIsWarehouseUsePOS)
                    ShowWarehouseDtl(WhsCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWarehouseDtl(string WhsCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.Dno, A.PayTpNo, B.PayTpNm, A.Acno, C.AcDesc " +
                    "From TblWarehouseDtl A " +
                    "Inner Join TblPosPayByType B On A.PayTpNo = B.PayTpNo " +
                    "Left Join TblCoa C On A.Acno = C.AcNo " +
                    "Where A.whsCode=@WhsCode Order By A.Dno ;",
                    new string[] { "Dno", "PayTpNo", "PayTpNm", "Acno", "AcDesc" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }


        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWhsCode, "Warehouse code", false) ||
                Sm.IsTxtEmpty(TxtWhsName, "Warehouse name", false) ||
                Sm.IsLueEmpty(LueCityCode, "City") ||
                IsWhsCodeExisted()||
                IsCCCodeEmpty() ||
                IsCCtCodeEmpty() ||
                IsMCCtCodeEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsWhsCodeExisted()
        {
            if (!TxtWhsCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select 1 From TblWarehouse Where WhsCode='" + TxtWhsCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Warehouse code ( " + TxtWhsCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsCCCodeEmpty()
        {
            if (mIsAutoJournalActived && Sm.GetLue(LueCCCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input Costcenter.");
                return true;
            }
            return false;
        }

        private bool IsCCtCodeEmpty()
        {
            if (mIsAutoJournalActived && Sm.GetLue(LueCCtCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input Costcategory.");
                return true;
            }
            return false;
        }

        private bool IsMCCtCodeEmpty()
        {
            if (mIsAutoJournalActived && TxtAcNo.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input Mutations Cost Category Account.");
                return true;
            }
            return false;
        }

        private bool IsWhsCodeAlreadyUsed()
        {
            return Sm.IsDataExist(
                "Select 1 As Exist From TblWarehouse T " +
                "Where T.WhsCode=@Param " +
                "And (Exists(Select 1 From TblGroupWarehouse Where WhsCode=T.WhsCode Limit 1) " + 
                "Or Exists(Select 1 From TblStockSummary Where WhsCode=T.WhsCode Limit 1));",
                TxtWhsCode.Text, 
                "This Warehouse already used in group's warehouse setting or inventory transactions.");
        }

        private bool IsGrdValueNotValid()
        {
            if (mIsWarehouseUsePOS)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Payment Type is Empty.")) return true;
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdStr(Grd1, Row, 4).Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Account is empty");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveWarehouse()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWarehouse(WhsCode, WhsName, ");
            if (mIsMovingAvgEnabled) SQL.AppendLine("MovingAvgInd, ");
            SQL.AppendLine("WhsCtCode, Address, CityCode, PostalCd, Phone, fax, Email, Mobile, ContactPerson, CCCode, CCtCode, AcNo, SiteCode, WhsCodeOld, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WhsCode, @WhsName, ");
            if (mIsMovingAvgEnabled) SQL.AppendLine("@MovingAvgInd, ");
            SQL.AppendLine("@WhsCtCode, @Address, @CityCode, @PostalCd, @Phone, @fax, @Email, @Mobile, @ContactPerson, @CCCode, @CCtCode, @AcNo, @SiteCode, @WhsCodeOld, @Remark, @UserCode,  CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update ");
            SQL.AppendLine("   WhsName=@WhsName, ");
            if (mIsMovingAvgEnabled) SQL.AppendLine("MovingAvgInd=@MovingAvgInd, ");
            SQL.AppendLine("   WhsCtCode=@WhsCtCode, Address=@Address, CityCode=@CityCode, PostalCd=@PostalCd, Phone=@Phone, fax=@fax, Email=@Email, Mobile=@Mobile, ");
            SQL.AppendLine("   ContactPerson=@ContactPerson, CCCode=@CCCode, CCtCode=@CCtCode, AcNo=@AcNo, SiteCode=@SiteCode, Remark=@Remark, WhsCodeOld= @WhsCodeOld, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", TxtWhsCode.Text);
            Sm.CmParam<String>(ref cm, "@WhsName", TxtWhsName.Text);
            if (mIsMovingAvgEnabled) Sm.CmParam<String>(ref cm, "@MovingAvgInd", ChkMovingAvgInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@WhsCtCode", Sm.GetLue(LueWhsCtCode));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ContactPerson", MeeContactPerson.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetLue(LueCCtCode));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@WhsCodeOld", TxtWhsCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
           
            return cm;
        }

        private MySqlCommand SaveWarehouseDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWarehouseDtl(WhsCode, DNo, PayTpNo, Acno, CreateBy, CreateDt) " +
                    "Values(@WhsCode, @DNo, @PayTpNo, @Acno, @CreateBy, CurrentDateTime()) " +
                    "On Duplicate Key Update Acno = @Acno, LastUpBy = @LastUpBy, LastUpDt =CurrentDateTime() ;  "
            };
            Sm.CmParam<String>(ref cm, "@WhsCode", TxtWhsCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PayTpNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Acno", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWhsCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsCode);
        }

        private void TxtWhsName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsName);
        }

        private void LueWhsCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCtCode, new Sm.RefreshLue1(Sl.SetLueWhsCtCode));
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            LueCCtCode.EditValue = null;
            if (Sm.GetLue(LueCCCode).Length != 0)
            {
                Sm.SetControlReadOnly(LueCCtCode, false);
                Sl.SetLueCCtCode(ref LueCCtCode, Sm.GetLue(LueCCCode));
            }
            else
                Sm.SetControlReadOnly(LueCCtCode, true);
        }

        private void LueCCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCtCode, new Sm.RefreshLue2(Sl.SetLueCCtCode), Sm.GetLue(LueCCCode));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWarehouseDlg(this, true, 0));
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));

        }

       

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLuePosPayCode));
        }

        private void LuePaymentType_Leave(object sender, EventArgs e)
        {
            if (LuePaymentType.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LuePaymentType).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LuePaymentType);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LuePaymentType.GetColumnValue("Col2");
                }
                LuePaymentType.Visible = false;
            }
        }

        #endregion

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {

            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 3)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWarehouseDlg(this, false, e.RowIndex));
                }

                if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
                {
                    LueRequestEdit(Grd1, LuePaymentType, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sl.SetLuePosPayCode(ref LuePaymentType);
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtWhsCode.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmWarehouseDlg(this, false, e.RowIndex));
        }
        #endregion

      
    }
}
