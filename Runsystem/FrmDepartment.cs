﻿#region Update
/*
    24/08/2018 [WED] tambah AcNo untuk Travel Request
    18/09/2019 [TKG/IMS] tambah daftar position
    20/01/2020 [TKG/IMS] tambah department group
    12/03/2020 [VIN/SRN] tambah inputan COA
    04/05/2020 [TKG/TWC] tambah inputan COA utk THR
    30/06/2020 [VIN/IMS] tambah inputan COA utk Incentive
    23/07/2020 [WED/SIER] tambah inputan COA untuk debit Cash Advance Settlement. mandatory berdasarkan parameter CashAdvanceJournalDebitFormat
    28/10/2020 [TKG/IMS] tambah coa ss
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    12/04/2022 [TYO/PHT] merubah sourcing field group berdasarkan parameter GroupDepartemenSource
    20/06/2022 [RIS/PHT] Merubah param GroupDepartementSource menjadi internal supaya bisa di pakai di frm find
    17/01/2023 [MYA/VIR] Menambah digit, memandatorykan, dan menambahkan validasi tidak bisa edit pada field short code pada master department
    13/02/2023 [MAU/BBT] penyesuaian validasi cas ketika insert atau edit department berdasarkan validasi CashAdvanceJournalDebitFormat
    02/03/2023 [WED/BBT] tambah karakter di Department name jadi 255
    17/04/2023 [RDA/HEX] tambah field branch manager (non mandatory) berdasarkan param mIsDepartmentUseBranchManager
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmDepartment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDeptCode = string.Empty, //if this application is called from other application
            mGroupDepartementSource = string.Empty;
        internal FrmDepartmentFind FrmFind;
        internal string mIsDeptFilterByDivision;
        private bool 
            mIsDepartmentPositionEnabled = false,
            mIsDepartmentUseMandatoryShortCode = false,
            mIsDepartmentUseBranchManager = false;
        internal bool mIsCOAUseAlias = false;
        private string
            mCashAdvanceJournalDebitFormat = string.Empty;

        #endregion

        #region Constructor

        public FrmDepartment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Department";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                ExecQuery();
                GetParameter();
                if (mCashAdvanceJournalDebitFormat != "1" && mCashAdvanceJournalDebitFormat != "3")
                {
                    Tc1.SelectedTabPage = Tp3;
                    LblAcNo9.ForeColor = Color.Red;
                }
                Sl.SetLueDivisionCode(ref LueDivisionCode);
                Tc1.SelectedTabPage = Tp1;
                Tp2.PageVisible = mIsDepartmentPositionEnabled;

                if (mGroupDepartementSource == "1")
                    Sl.SetLueOption(ref LueDeptGrpCode, "DepartmentGroup");

                if (mGroupDepartementSource == "2")
                { 
                    SetLueGroup(ref LueDeptGrpCode);
                    LblGroup.ForeColor = Color.Red;
                }
                if (mIsDepartmentUseMandatoryShortCode)
                {
                    label50.ForeColor = Color.Red;
                }
                SetGrd();

                //if this application is called from other application
                if (mDeptCode.Length != 0)
                {
                    ShowData(mDeptCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                Sl.SetLueUserCode(ref LueBranchManager);
                if (!mIsDepartmentUseBranchManager) label5.Visible = LueBranchManager.Visible = false;

                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDepartmentPositionEnabled = Sm.GetParameterBoo("IsDepartmentPositionEnabled");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
            mCashAdvanceJournalDebitFormat = Sm.GetParameter("CashAdvanceJournalDebitFormat");
            mGroupDepartementSource = Sm.GetParameter("GroupDepartementSource");
            mIsDepartmentUseMandatoryShortCode = Sm.GetParameterBoo("IsDepartmentUseMandatoryShortCode");
            mIsDepartmentUseBranchManager = Sm.GetParameterBoo("IsDepartmentUseBranchManager");

            if (mCashAdvanceJournalDebitFormat.Length == 0) mCashAdvanceJournalDebitFormat = "1";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Site Code",
                        "Site",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-2
                        100, 180
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });

            Grd2.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                Grd2, new string[] { "", "Code", "Position" },
                new int[] { 20, 0, 300 }
            );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDeptCode, TxtDeptName, TxtShortCode, ChkActInd, ChkControlItemInd, 
                        LueDivisionCode, LueDeptGrpCode, LueBranchManager
                    }, true);
                    Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0 });
                    BtnAcNo.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    BtnAcNo3.Enabled = false;
                    BtnAcNo4.Enabled = false;
                    BtnAcNo5.Enabled = false;
                    BtnAcNo6.Enabled = false;
                    BtnAcNo7.Enabled = false;
                    BtnAcNo8.Enabled = false;
                    BtnAcNo9.Enabled = false;
                    BtnAcNo10.Enabled = false;
                    TxtDeptCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDeptCode, TxtDeptName, TxtShortCode, ChkActInd, ChkControlItemInd, 
                        LueDivisionCode, LueDeptGrpCode, LueBranchManager
                    }, false);
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;
                    BtnAcNo10.Enabled = true;
                    TxtDeptCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDeptCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDeptName, TxtShortCode, ChkActInd, ChkControlItemInd, LueDivisionCode,
                        LueDeptGrpCode, LueBranchManager
                    }, false);
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;
                    BtnAcNo10.Enabled = true;
                    TxtDeptName.Focus();
                    if (mIsDepartmentUseMandatoryShortCode)
                    {
                        Sm.SetControlReadOnly(TxtShortCode, true);
                    }
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDeptCode, TxtDeptName, TxtShortCode, LueDivisionCode, LueDeptGrpCode,  
                TxtAcNo, MeeAcDesc, TxtAcNo2, MeeAcDesc2, TxtAcNo3, 
                MeeAcDesc3, TxtAcNo4, MeeAcDesc4, TxtAcNo5, MeeAcDesc5, 
                TxtAcNo6, MeeAcDesc6, TxtAcNo7, MeeAcDesc7, TxtAcNo8, 
                MeeAcDesc8, TxtAcNo9, MeeAcDesc9, TxtAcNo10, MeeAcDesc10, LueBranchManager
            });
            ChkActInd.Checked = ChkControlItemInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDepartmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDeptCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDeptCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblDepartment Where DeptCode=@DeptCode" };
                Sm.CmParam<String>(ref cm, "@DeptCode", TxtDeptCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveDepartment());

                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SaveDeptSite(Row));
                }

                Sm.ExecCommands(cml);
                ShowData(TxtDeptCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDeptCode, "Department's code", false) ||
                Sm.IsTxtEmpty(TxtDeptName, "Department's name", false) ||
                IsDeptCodeExisted() ||
                IsDivisionMandatory() ||
                IsAcNo9Empty() ||
                (mIsDepartmentUseMandatoryShortCode && Sm.IsTxtEmpty(TxtShortCode, "Department's Short Code", false)) ||
                (mGroupDepartementSource == "2" && Sm.IsLueEmpty(LueDeptGrpCode, "Group"));
        }

        private bool IsAcNo9Empty()
        {
            if (mCashAdvanceJournalDebitFormat == "1" || mCashAdvanceJournalDebitFormat == "3") return false;

            if (TxtAcNo9.Text.Trim().Length == 0 )
            {
                Tc1.SelectedTabPage = Tp3;
                Sm.StdMsg(mMsgType.Warning, "Cash Advance Settlement's account should not be empty.");
                TxtAcNo9.Focus();
                return true;
            }

            return false;
        }

        private bool IsDeptCodeExisted()
        {
            if (!TxtDeptCode.Properties.ReadOnly)
            {
                return Sm.IsDataExist(
                    "Select 1 From TblDepartment Where DeptCode=@Param;",
                    TxtDeptCode.Text,
                    "Department code ( " + TxtDeptCode.Text + " ) already existed."
                    );
            }
            else
                return false;
        }

        private bool IsDivisionMandatory()
        {
            mIsDeptFilterByDivision = Sm.GetParameter("IsDeptFilterByDivision");
            if (mIsDeptFilterByDivision == "Y")
            {
                if (Sm.GetLue(LueDivisionCode).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Division still empty");
                    return true;
                }
            }
            return false;
        }

        internal string GetSelectedSite()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private MySqlCommand SaveDepartment()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var PosCode = string.Empty;

            SQL.AppendLine("Insert Into TblDepartment(DeptCode, DeptName, ShortCode, ActInd, ControlItemInd, DivisionCode, DeptGrpCode, AcNo, AcNo2, AcNo3, AcNo4, AcNo5, AcNo6, AcNo7, AcNo8, AcNo9, AcNo10, ");
            if (mIsDepartmentUseBranchManager) SQL.AppendLine("BranchUserCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DeptCode, @DeptName, @ShortCode, @ActInd, @ControlItemInd, @DivisionCode, @DeptGrpCode, @AcNo, @AcNo2, @AcNo3, @AcNo4, @AcNo5, @AcNo6, @AcNo7, @AcNo8, @AcNo9, @AcNo10, ");
            if (mIsDepartmentUseBranchManager) SQL.AppendLine("@BranchUserCode, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update DeptName=@DeptName, ShortCode=@ShortCode, ActInd=@ActInd, ControlItemInd=@ControlItemInd, DivisionCode=@DivisionCode, DeptGrpCode=@DeptGrpCode, AcNo=@AcNo, AcNo2=@AcNo2, ");
            SQL.AppendLine("   AcNo3=@AcNo3, AcNo4=@AcNo4, AcNo5=@AcNo5, AcNo6=@AcNo6, AcNo7=@AcNo7, AcNo8=@AcNo8, AcNo9=@AcNo9, AcNo10=@AcNo10, ");
            if (mIsDepartmentUseBranchManager)
                SQL.AppendLine("BranchUserCode=@BranchUserCode, ");
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            SQL.AppendLine("Delete From TblDepartmentBudgetSite Where DeptCode=@DeptCode; ");

            SQL.AppendLine("Delete From TblDepartmentPosition Where DeptCode=@DeptCode; ");

            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    PosCode = Sm.GetGrdStr(Grd2, r, 1);
                    if (PosCode.Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblDepartmentPosition(DeptCode, PosCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values (@DeptCode, @PosCode0" + r.ToString() + ", @UserCode, CurrentDateTime()); ");

                        Sm.CmParam<String>(ref cm, "@PosCode0" + r.ToString(), PosCode);
                    }
                }
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DeptCode", TxtDeptCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptName", TxtDeptName.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ControlItemInd", ChkControlItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivisionCode));
            Sm.CmParam<String>(ref cm, "@DeptGrpCode", Sm.GetLue(LueDeptGrpCode));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@AcNo3", TxtAcNo3.Text);
            Sm.CmParam<String>(ref cm, "@AcNo4", TxtAcNo4.Text);
            Sm.CmParam<String>(ref cm, "@AcNo5", TxtAcNo5.Text);
            Sm.CmParam<String>(ref cm, "@AcNo6", TxtAcNo6.Text);
            Sm.CmParam<String>(ref cm, "@AcNo7", TxtAcNo7.Text);
            Sm.CmParam<String>(ref cm, "@AcNo8", TxtAcNo8.Text);
            Sm.CmParam<String>(ref cm, "@AcNo9", TxtAcNo9.Text);
            Sm.CmParam<String>(ref cm, "@AcNo10", TxtAcNo10.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if(mIsDepartmentUseBranchManager) Sm.CmParam<String>(ref cm, "@BranchUserCode", Sm.GetLue(LueBranchManager));

            return cm;
        }

        private MySqlCommand SaveDeptSite(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDepartmentBudgetSite (DeptCode, SiteCode, CreateBy, CreateDt) " +
                    "Values (@DeptCode, @SiteCode, @UserCode, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@DeptCode", TxtDeptCode.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion 

        #region Show Data

        public void ShowData(string DeptCode)
        {
            try
            {
                ClearData();
                ShowDepartment(DeptCode);
                ShowDeptSite(DeptCode);
                ShowDepartmentPosition(DeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDepartment(string DeptCode)
        {
            Cursor.Current = Cursors.WaitCursor;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DeptCode, A.DeptName, A.ShortCode, A.ActInd, A.ControlItemInd, A.DivisionCode, A.DeptGrpCode, ");
            SQL.AppendLine("A.AcNo, B.AcDesc, ");
            SQL.AppendLine("A.AcNo2, C.AcDesc As AcDesc2,  ");
            SQL.AppendLine("A.AcNo3, D.AcDesc As AcDesc3,  ");
            SQL.AppendLine("A.AcNo4, E.AcDesc As AcDesc4,  ");
            SQL.AppendLine("A.AcNo5, F.AcDesc As AcDesc5,  ");
            SQL.AppendLine("A.AcNo6, G.AcDesc As AcDesc6, ");
            SQL.AppendLine("A.AcNo7, H.AcDesc As AcDesc7, ");
            SQL.AppendLine("A.AcNo8, I.AcDesc As AcDesc8, ");
            SQL.AppendLine("A.AcNo9, J.AcDesc As AcDesc9, ");
            SQL.AppendLine("A.AcNo10, K.AcDesc As AcDesc10, ");
            if(mIsDepartmentUseBranchManager)
                SQL.AppendLine("A.BranchUserCode ");
            else
                SQL.AppendLine("NULL as BranchUserCode ");
            SQL.AppendLine("From TblDepartment A ");
            SQL.AppendLine("Left Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("Left Join TblCOA C On A.AcNo2 = C.AcNo ");
            SQL.AppendLine("Left Join TblCOA D On A.AcNo3 = D.AcNo ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo4 = E.AcNo ");
            SQL.AppendLine("Left Join TblCOA F On A.AcNo5 = F.AcNo ");
            SQL.AppendLine("Left Join TblCOA G On A.AcNo6 = G.AcNo ");
            SQL.AppendLine("Left Join TblCOA H On A.AcNo7 = H.AcNo ");
            SQL.AppendLine("Left Join TblCOA I On A.AcNo8 = I.AcNo ");
            SQL.AppendLine("Left Join TblCOA J On A.AcNo9 = J.AcNo ");
            SQL.AppendLine("Left Join TblCOA K On A.AcNo10 = K.AcNo ");
            SQL.AppendLine("Where A.DeptCode = @DeptCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DeptCode", 
                    "DeptName", "ShortCode", "ActInd", "ControlItemInd", "DivisionCode",
                    "DeptGrpCode", "AcNo", "AcDesc", "AcNo2", "AcDesc2", 
                    "AcNo3", "AcDesc3", "AcNo4", "AcDesc4", "AcNo5",  
                    "AcDesc5", "AcNo6", "AcDesc6", "AcNo7", "AcDesc7", 
                    "AcNo8", "AcDesc8", "AcNo9", "AcDesc9", "AcNo10", 
                    "AcDesc10","BranchUserCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtDeptName.EditValue = Sm.DrStr(dr, c[1]);
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[2]);
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    ChkControlItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    Sm.SetLue(LueDivisionCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueDeptGrpCode, Sm.DrStr(dr, c[6]));
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[7]);
                    MeeAcDesc.EditValue = Sm.DrStr(dr, c[8]);
                    TxtAcNo2.EditValue = Sm.DrStr(dr, c[9]);
                    MeeAcDesc2.EditValue = Sm.DrStr(dr, c[10]);
                    TxtAcNo3.EditValue = Sm.DrStr(dr, c[11]);
                    MeeAcDesc3.EditValue = Sm.DrStr(dr, c[12]);
                    TxtAcNo4.EditValue = Sm.DrStr(dr, c[13]);
                    MeeAcDesc4.EditValue = Sm.DrStr(dr, c[14]);
                    TxtAcNo5.EditValue = Sm.DrStr(dr, c[15]);
                    MeeAcDesc5.EditValue = Sm.DrStr(dr, c[16]);
                    TxtAcNo6.EditValue = Sm.DrStr(dr, c[17]);
                    MeeAcDesc6.EditValue = Sm.DrStr(dr, c[18]);
                    TxtAcNo7.EditValue = Sm.DrStr(dr, c[19]);
                    MeeAcDesc7.EditValue = Sm.DrStr(dr, c[20]);
                    TxtAcNo8.EditValue = Sm.DrStr(dr, c[21]);
                    MeeAcDesc8.EditValue = Sm.DrStr(dr, c[22]);
                    TxtAcNo9.EditValue = Sm.DrStr(dr, c[23]);
                    MeeAcDesc9.EditValue = Sm.DrStr(dr, c[24]);
                    TxtAcNo10.EditValue = Sm.DrStr(dr, c[25]);
                    MeeAcDesc10.EditValue = Sm.DrStr(dr, c[26]);
                    Sm.SetLue(LueBranchManager, Sm.DrStr(dr, c[27]));
                }, true
            );
        }

        private void ShowDeptSite(string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SiteCode, B.SiteName ");
            SQL.AppendLine("From TblDepartmentBudgetSite  A ");
            SQL.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Where DeptCode=@DeptCode ");
            SQL.AppendLine("Order By B.SiteName ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] { "SiteCode", "SiteName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

         private void ShowDepartmentPosition(string DeptCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.PosCode, B.PosName From TblDepartmentPosition A, TblPosition B " +
                    "Where A.PosCode=B.PosCode And A.DeptCode=@DeptCode;",
                    new string[] { "PosCode", "PosName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
         }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("ALTER TABLE `tbldepartment` ");
            SQL.AppendLine("    CHANGE COLUMN `DeptName` `DeptName` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `DeptCode`; ");

            Sm.ExecQuery(SQL.ToString());
        }

        internal void SetLueGroup(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DirectorateGroupCode As Col1, Concat(DirectorateGroupName, ' ', DirectorateGroupCode) As Col2 ");
            SQL.AppendLine("From TblDirectorateGroup ");
            SQL.AppendLine("Where NotParentInd = 'Y' And ActInd = 'Y' ");
            SQL.AppendLine("Order By DirectorateGroupCode ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Grid Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmDepartmentDlg(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmDepartmentDlg(this));
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmDepartmentDlg3(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmDepartmentDlg3(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Misc Control Event

        private void TxtDeptCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDeptCode);
        }

        private void TxtDeptName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDeptName);
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtShortCode);
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
        }

        private void LueDeptGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "DepartmentGroup");
        }

        private void TxtAcNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (travel request) ?") == DialogResult.Yes)
                {
                    TxtAcNo.EditValue = null;
                    MeeAcDesc.EditValue = null;
                }
            }
        }

        private void TxtAcNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo2.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (bonus fee) ?") == DialogResult.Yes)
                {
                    TxtAcNo2.EditValue = null;
                    MeeAcDesc2.EditValue = null;
                }
            }
        }

        private void TxtAcNo3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo3.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (salary) ?") == DialogResult.Yes)
                {
                    TxtAcNo3.EditValue = null;
                    MeeAcDesc3.EditValue = null;
                }
            }
        }

        private void TxtAcNo4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo4.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (over time) ?") == DialogResult.Yes)
                {
                    TxtAcNo4.EditValue = null;
                    MeeAcDesc4.EditValue = null;
                }
            }
        }

        private void TxtAcNo5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo5.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (leave) ?") == DialogResult.Yes)
                {
                    TxtAcNo5.EditValue = null;
                    MeeAcDesc5.EditValue = null;
                }
            }
        }

        private void TxtAcNo6_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo6.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (tax allowance) ?") == DialogResult.Yes)
                {
                    TxtAcNo6.EditValue = null;
                    MeeAcDesc6.EditValue = null;
                }
            }
        }

        private void TxtAcNo7_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo7.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (religious holiday allowance) ?") == DialogResult.Yes)
                {
                    TxtAcNo7.EditValue = null;
                    MeeAcDesc7.EditValue = null;
                }
            }
        }

        private void TxtAcNo8_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo8.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (incentive) ?") == DialogResult.Yes)
                {
                    TxtAcNo8.EditValue = null;
                    MeeAcDesc8.EditValue = null;
                }
            }
        }

        private void TxtAcNo9_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo9.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (cash advance) ?") == DialogResult.Yes)
                {
                    TxtAcNo9.EditValue = null;
                    MeeAcDesc9.EditValue = null;
                }
            }
        }

        private void TxtAcNo10_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtAcNo10.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove COA's account (social security) ?") == DialogResult.Yes)
                {
                    TxtAcNo10.EditValue = null;
                    MeeAcDesc10.EditValue = null;
                }
            }
        }

        private void LueBranchManager_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBranchManager, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        #endregion

        #region Button

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 2));
        }
        private void BtnAcNo3_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 3));
        }
        private void BtnAcNo4_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 4));
        }

        private void BtnAcNo5_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 5));
        }

        private void BtnAcNo6_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 6));
        }

        private void BtnAcNo7_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 7));
        }

        private void BtnAcNo8_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 8));
        }

        private void BtnAcNo9_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 9));
        }

        private void BtnAcNo10_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDepartmentDlg2(this, 10));
        }

        #endregion

        #endregion
    }
}
