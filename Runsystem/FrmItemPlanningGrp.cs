﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemPlanningGrp : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmItemPlanningGrpFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemPlanningGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtItPlanningGrpCode, ChkActInd, TxtItPlanningGrpName }, true);
                    TxtItPlanningGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtItPlanningGrpCode, TxtItPlanningGrpName }, false);
                    TxtItPlanningGrpCode.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(TxtItPlanningGrpCode, true);
                    Sm.SetControlReadOnly(TxtItPlanningGrpName, false);
                    TxtItPlanningGrpName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtItPlanningGrpCode, TxtItPlanningGrpName });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemPlanningGrpFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItPlanningGrpCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtItPlanningGrpCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblItemPlanningGroup Where ItPlanningGrpCode=@ItPlanningGrpCode" };
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", TxtItPlanningGrpCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemPlanningGroup(ItPlanningGrpCode, ItPlanningGrpName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItPlanningGrpCode, @ItPlanningGrpName, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItPlanningGrpName=@ItPlanningGrpName, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", TxtItPlanningGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpName", TxtItPlanningGrpName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItPlanningGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CurCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", CurCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select ItPlanningGrpCode, ItPlanningGrpName, ActInd From TblItemPlanningGroup Where ItPlanningGrpCode=@ItPlanningGrpCode;",
                        new string[] { "ItPlanningGrpCode", "ItPlanningGrpName", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItPlanningGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItPlanningGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItPlanningGrpCode, "Planning group code", false) ||
                Sm.IsTxtEmpty(TxtItPlanningGrpName, "Planning group name", false) ||
                IsCurCodeExisted();
                IsDataInactiveAlready();
        }

        private bool IsCurCodeExisted()
        {
            if (!TxtItPlanningGrpCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select ItPlanningGrpCode From TblItemPlanningGroup Where ItPlanningGrpCode=@ItPlanningGrpCode;";
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", TxtItPlanningGrpCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Planning group code ( " + TxtItPlanningGrpCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select ItPlanningGrpCode From TblItemPlanningGroup " +
                    "Where ActInd='N' And ItPlanningGrpCode=@ItPlanningGrpCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", TxtItPlanningGrpCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion
    }
}
