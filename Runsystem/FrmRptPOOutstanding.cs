﻿#region Update
/* 
    04/04/2017 [WED] Penambahan kolom Discount %, Discount Amt, Rounding Value, Currency, Total, Grand Total per item PO
    04/04/2017 [WED] Perubahan nama dan isi kolom "Tax" menjadi "PPN"
    04/04/2017 [WED] Discount %, Discount Amt, Rounding Value, diambil dari PORevision yang sudah diapprove. Jika tidak ada, ambil dari PO
    18/10/2017 [HAR] harga sblm revisi dihapus yang ditamilkan harga stlh revisi saja
    26/10/2017 [ARI] tambah kolom tax
    29/05/2018 [TKG] tambah validasi status PO
    08/08/2018 [WED] tambah loop untuk melihat hasil file upload di QtHdr
    23/11/2018 [HAR] tjadi 5 file + format ftp
    25/01/2022 [ISD/PHT] tambah kolom estimated received date revision dengan parameter IsOutstandingPOListUsePORevision
    26/01/2022 [ISD/PHT] tambah kolom UPriceRev, DiscountRev, DiscountAmtRev, RoundingValueRev, TotalRev, GrandTotalRev dengan parameter IsOutstandingPOListUsePORevision
    02/03/2022 [ISD/PHT] bug kolom Cancelled Qty tidak muncul dan rename menjadi Cancelling PO Quantity
    02/03/2022 [ISD/PHT] tambah kolom POQty Revision dengan parameter IsOutstandingPOListUsePORevision
    14/04/2022 [RIS/PHT] Merubah parameter IsOutstandingPOListUsePORevision menjadi IsRptProcurementUsePORevision
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmRptPOOutstanding : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsSiteMandatory = false, mIsFilterBySite = false,
            mIsShowForeignName = false, mIsShowTax = false,
            mIsFilterByItCt = false, mIsQTAllowToUploadFile = false,
            mIsRptProcurementUsePORevision = false;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        
        #endregion

        #region Constructor

        public FrmRptPOOutstanding(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsShowTax = Sm.GetParameterBoo("IsShowTax");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsQTAllowToUploadFile = Sm.GetParameterBoo("IsQTAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
        }

        private string SetSQL(string Filter, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, F.VdName, J.DeptName, E.ItCode, E.ItName, E.ForeignName, E.PurchaseUomCode, B.Qty, B.EstRecvDt, ");
            if (mIsSiteMandatory)
                SQL.AppendLine("K.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("B.EstTimeArrival As EstRecvDtRev, ");
                SQL.AppendLine("IFNULL(M3.Qty, 0) as QtyRev, IFNULL(H.POQtyCancelQty, 0) as POQtyCancel, ");
                SQL.AppendLine("IFNULL(M2.UPrice, M.UPrice) AS UPrice, IFNULL(M3.UPrice, 0) AS UPriceRev,  ");
                SQL.AppendLine("IFNULL(M2.DiscountOld, B.Discount) AS Discount, IFNULL(M2.DiscountAmtOld, B.DiscountAmt) As DiscountAmt,  ");
                SQL.AppendLine("IFNULL(M2.RoundingValueOld, B.RoundingValue) As RoundingValue, "); // Before Revision
                SQL.AppendLine("IFNULL(M3.Discount, 0) AS DiscountRev, IFNULL(M3.DiscountAmt, 0) As DiscountAmtRev, IFNULL(M3.RoundingValue, 0) As RoundingValueRev, "); //After Revision

                SQL.AppendLine("((((IFNULL(M2.QtyOld, B.Qty)- IfNull(H.POQtyCancelQty, 0))*IfNull(IFNULL(M2.UPrice, M.UPrice), 0)) * ((100 - ifnull(IFNULL(M2.DiscountOld, B.Discount), 0)) / 100)) - IFNULL(IfNull(M2.DiscountAmtOld, B.DiscountAmt), 0) + ifnull(IFNULL(M2.RoundingValueOld, B.RoundingValue), 0)) As Total, ");
                SQL.AppendLine("((((IFNULL(M3.Qty, 0) - IfNull(H.POQtyCancelQty, 0))*IFNULL(M3.UPrice, 0)) * ((100-IFNULL(M3.Discount, 0))/100)) - IFNULL(M3.DiscountAmt, 0) + IFNULL(M3.RoundingValue, 0)) As TotalRev, ");

                SQL.AppendLine("( ");
                SQL.AppendLine("  ((((IFNULL(M2.QtyOld, B.Qty)- IfNull(H.POQtyCancelQty, 0))*IfNull(IFNULL(M2.UPrice, M.UPrice), 0)) * ((100 - ifnull(IFNULL(M2.DiscountOld, B.Discount), 0)) / 100)) - IFNULL(IfNull(M2.DiscountAmtOld, B.DiscountAmt), 0) + ifnull(IFNULL(M2.RoundingValueOld, B.RoundingValue), 0)) + ");
                SQL.AppendLine("  IfNull((If(A.TaxCode1='',0,((((IFNULL(M2.QtyOld, B.Qty)- IfNull(H.POQtyCancelQty, 0))*IfNull(IFNULL(M2.UPrice, M.UPrice), 0)) * ((100 - ifnull(IFNULL(M2.DiscountOld, B.Discount), 0)) / 100)) - IFNULL(IfNull(M2.DiscountAmtOld, B.DiscountAmt), 0) + ifnull(IFNULL(M2.RoundingValueOld, B.RoundingValue), 0)) * N1.TaxRate / 100)), 0) + ");
                SQL.AppendLine("  IfNull((If(A.TaxCode2='',0,((((IFNULL(M2.QtyOld, B.Qty)- IfNull(H.POQtyCancelQty, 0))*IfNull(IFNULL(M2.UPrice, M.UPrice), 0)) * ((100 - ifnull(IFNULL(M2.DiscountOld, B.Discount), 0)) / 100)) - IFNULL(IfNull(M2.DiscountAmtOld, B.DiscountAmt), 0) + ifnull(IFNULL(M2.RoundingValueOld, B.RoundingValue), 0)) * N2.TaxRate / 100)), 0) +  ");
                SQL.AppendLine("  IfNull((If(A.TaxCode3='',0,((((IFNULL(M2.QtyOld, B.Qty)- IfNull(H.POQtyCancelQty, 0))*IfNull(IFNULL(M2.UPrice, M.UPrice), 0)) * ((100 - ifnull(IFNULL(M2.DiscountOld, B.Discount), 0)) / 100)) - IFNULL(IfNull(M2.DiscountAmtOld, B.DiscountAmt), 0) + ifnull(IFNULL(M2.RoundingValueOld, B.RoundingValue), 0)) * N3.TaxRate / 100)), 0)  ");
                SQL.AppendLine(") ");
                SQL.AppendLine("AS GrandTotal, ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("  ((((IFNULL(M3.Qty, 0) - IfNull(H.POQtyCancelQty, 0))*IFNULL(M3.UPrice, 0)) * ((100-IFNULL(M3.Discount, 0))/100)) - IFNULL(M3.DiscountAmt, 0) + IFNULL(M3.RoundingValue, 0)) +  ");
                SQL.AppendLine("  IFNULL((If(A.TaxCode1='',0,((((IFNULL(M3.Qty, 0) - IfNull(H.POQtyCancelQty, 0))*IFNULL(M3.UPrice, 0)) * ((100-IFNULL(M3.Discount, 0))/100)) - IFNULL(M3.DiscountAmt, 0) + IFNULL(M3.RoundingValue, 0)) * N1.TaxRate / 100)), 0) +  ");
                SQL.AppendLine("  IFNULL((If(A.TaxCode2='',0,((((IFNULL(M3.Qty, 0) - IfNull(H.POQtyCancelQty, 0))*IFNULL(M3.UPrice, 0)) * ((100-IFNULL(M3.Discount, 0))/100)) - IFNULL(M3.DiscountAmt, 0) + IFNULL(M3.RoundingValue, 0)) * N2.TaxRate / 100)), 0) +  ");
                SQL.AppendLine("  IFNULL((If(A.TaxCode3='',0,((((IFNULL(M3.Qty, 0) - IfNull(H.POQtyCancelQty, 0))*IFNULL(M3.UPrice, 0)) * ((100-IFNULL(M3.Discount, 0))/100)) - IFNULL(M3.DiscountAmt, 0) + IFNULL(M3.RoundingValue, 0)) * N3.TaxRate / 100)), 0)  ");
                SQL.AppendLine(") As GrandTotalRev, ");
                SQL.AppendLine("IFNULL(M3.Qty, B.Qty)-IfNull(G.RecvVdQty, 0)-IfNull(H.POQtyCancelQty, 0) As Balance, ");
            }
            else
            {
                SQL.AppendLine("IfNull(M.UPrice, 0) As UPrice, ");
                SQL.AppendLine("IfNull(B.Discount, 0) AS Discount, IfNull(B.DiscountAmt, 0) As DiscountAmt, IfNull(B.RoundingValue, 0) As RoundingValue, "); //(B.Qty*IfNull(M.UPrice, 0)) As Total, ");
                SQL.AppendLine("((((B.Qty- IfNull(H.POQtyCancelQty, 0))*IfNull(M.UPrice, 0)) * ((100 - ifnull(B.Discount, 0)) / 100)) - IFNULL(B.DiscountAmt, 0) + ifnull(B.RoundingValue, 0)) As Total, ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  ((((B.Qty- IfNull(H.POQtyCancelQty, 0))*IfNull(M.UPrice, 0)) * ((100 - ifnull(B.Discount, 0)) / 100)) - IFNULL(B.DiscountAmt, 0) + ifnull(B.RoundingValue, 0)) + ");
                SQL.AppendLine("  IfNull((If(A.TaxCode1='',0,((((B.Qty- IfNull(H.POQtyCancelQty, 0))*IfNull(M.UPrice, 0)) * ((100 - ifnull(B.Discount, 0)) / 100)) - IFNULL(B.DiscountAmt, 0) + ifnull(B.RoundingValue, 0)) * N1.TaxRate / 100)), 0) + ");
                SQL.AppendLine("  IfNull((If(A.TaxCode2='',0,((((B.Qty- IfNull(H.POQtyCancelQty, 0))*IfNull(M.UPrice, 0)) * ((100 - ifnull(B.Discount, 0)) / 100)) - IFNULL(B.DiscountAmt, 0) + ifnull(B.RoundingValue, 0)) * N2.TaxRate / 100)), 0) + ");
                SQL.AppendLine("  IfNull((If(A.TaxCode3='',0,((((B.Qty- IfNull(H.POQtyCancelQty, 0))*IfNull(M.UPrice, 0)) * ((100 - ifnull(B.Discount, 0)) / 100)) - IFNULL(B.DiscountAmt, 0) + ifnull(B.RoundingValue, 0)) * N3.TaxRate / 100)), 0) ");
                SQL.AppendLine(") As GrandTotal, ");
                SQL.AppendLine("Null As EstRecvDtRev, Null As UPriceRev, Null As TotalRev, Null As DiscountRev, Null As DiscountAmtRev, Null As RoundingValueRev, Null As GrandTotalRev, Null As QtyRev, ");
                SQL.AppendLine("B.Qty-IfNull(G.RecvVdQty, 0)-IfNull(H.POQtyCancelQty, 0) As Balance, ");
            }
            SQL.AppendLine("IfNull(G.RecvVdQty, 0) As Qty2, IfNull(H.POQtyCancelQty, 0) As Qty3, L.CurCode, ");
            SQL.AppendLine("Case B.ProcessInd When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fulfilled' End As ProcessIndDesc, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  IfNull(If(A.TaxCode1='',0,(If(A.TaxCode1=O.ParValue,((B.Qty*IfNull(M.UPrice, 0))*0.1), 0))), 0) + ");
            SQL.AppendLine("  IfNull(If(A.TaxCode2='',0,(If(A.TaxCode2=O.ParValue,((B.Qty*IfNull(M.UPrice, 0))*0.1), 0))), 0) + ");
            SQL.AppendLine("  IfNull(If(A.TaxCode3='',0,(If(A.TaxCode3=O.ParValue,((B.Qty*IfNull(M.UPrice, 0))*0.1), 0))), 0) ");
            SQL.AppendLine(") As PPN, A.TaxAmt, L.FileName1, L.FileName2, L.FileName3, L.FileName4, L.FileName5 ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' " + Filter2.Replace("X.", "B."));
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode " + Filter3.Replace("X.", "E."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblVendor F On A.VdCode=F.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPOHdr T2 On T1.PODocNo=T2.DocNo " + Filter.Replace("X.", "T2."));
            SQL.AppendLine("    Inner Join TblPODtl T3 On T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo " + Filter2.Replace("X.", "T3."));
            SQL.AppendLine("    Inner Join TblItem T4 On T1.ItCode=T4.ItCode " + Filter3.Replace("X.", "T4."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") G On A.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As POQtyCancelQty ");
            SQL.AppendLine("    From TblPOQtyCancel T1 ");
            SQL.AppendLine("    Inner Join TblPOHdr T2 On T1.PODocNo=T2.DocNo " + Filter.Replace("X.", "T2."));
            SQL.AppendLine("    Inner Join TblPODtl T3 On T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo " + Filter2.Replace("X.", "T3."));
            SQL.AppendLine("    Inner Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo=T5.DocNo And T4.MaterialRequestDNo=T5.DNo ");
            SQL.AppendLine("    Inner Join TblItem T6 On T5.ItCode=T6.ItCode " + Filter3.Replace("X.", "T6."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") H On A.DocNo=H.DocNo And B.DNo=H.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr I On C.MaterialRequestDocNo=I.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment J On I.DeptCode=J.DeptCode ");
            if (mIsSiteMandatory)
                SQL.AppendLine("Left Join TblSite K On A.SiteCode=K.SiteCode ");
            SQL.AppendLine("Left Join TblQtHdr L On C.QtDocNo = L.DocNo ");
            SQL.AppendLine("Left Join TblQtDtl M On C.QtDocNo = M.DocNo And C.QtDNo = M.DNo ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("Left Join(  ");
                SQL.AppendLine("	SELECT X1.PODocNo, X1.PODNo, X1.DiscountOld, X1.DiscountAmtOld, X1.RoundingValueOld, X1.DocNo, X2.UPrice, X1.QtyOld, X1.AmtOld ");
                SQL.AppendLine("	FROM tblporevision X1 ");
                SQL.AppendLine("	Inner Join tblqtdtl X2 ON X1.QtDocNoOld = X2.DocNo AND X1.QtDNoOld = X2.DNo ");
                SQL.AppendLine("	Inner Join tblpohdr X3 On X1.PODocNo=X3.DocNo " + Filter.Replace("X.", "X3."));
                SQL.AppendLine("	WHERE X1.DocNo = (SELECT MIN(DocNo) FROM tblporevision WHERE PODocNo = X1.PODocNo) ");
                SQL.AppendLine(")M2 ON B.DocNo = M2.PODocNo AND B.DNo = M2.PODNo  "); //Before Revision
                SQL.AppendLine("Left Join( ");
                SQL.AppendLine("	SELECT X1.PODocNo, X1.PODNo, X1.Discount, X1.DiscountAmt, X1.RoundingValue, X1.DocNo, X2.UPrice, X1.Qty, X1.Amt ");
                SQL.AppendLine("	FROM tblporevision X1 ");
                SQL.AppendLine("	Inner Join tblqtdtl X2 ON X1.QtDocNo = X2.DocNo AND X1.QtDNo = X2.DNo ");
                SQL.AppendLine("	Inner Join tblpohdr X3 On X1.PODocNo=X3.DocNo " + Filter.Replace("X.", "X3."));
                SQL.AppendLine("	WHERE X1.DocNo = (SELECT MAX(DocNo) FROM tblporevision WHERE PODocNo = X1.PODocNo) ");
                SQL.AppendLine(")M3 ON B.DocNo = M3.PODocNo AND B.DNo = M3.PODNo   "); //After Revision
            }
            SQL.AppendLine("Left Join TblTax N1 On A.TaxCode1 = N1.TaxCode ");
            SQL.AppendLine("Left Join TblTax N2 On A.TaxCode2 = N2.TaxCode ");
            SQL.AppendLine("Left Join TblTax N3 On A.TaxCode3 = N3.TaxCode ");
            SQL.AppendLine("Inner Join TblParameter O On O.ParCode = 'PPNTaxCode' ");
            
            SQL.AppendLine("Where A.Status='A' " + Filter.Replace("X.", "A."));
            if (mIsSiteMandatory && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {

            Grd1.Cols.Count = 44;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Local Document",
                    "Status",
                    "Vendor",

                    //6-10
                    "Site",
                    "Department",
                    "Item's"+Environment.NewLine+"Code",
                    "Item's Name",
                    "Foreign Name",
                    
                    //11-15
                    "PO"+Environment.NewLine+"Quantity",
                    "Received"+Environment.NewLine+"Quantity",
                    "Cancellation"+Environment.NewLine+"PO Quantity",
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "UoM",

                    //16-20
                    "PPN",
                    "Estimated"+Environment.NewLine+"Date",
                    "Currency",
                    "Price",
                    "Discount %",

                    //21-25
                    "Discount"+Environment.NewLine+"Amount",
                    "Rounding Value",
                    "Total",
                    "Grand Total",
                    "Tax",

                    //26-29
                    "File 1",
                    "",
                    "File 2",
                    "",
                    "File 3",

                    //31-35
                    "",
                    "File 4",
                    "",
                    "File 5",
                    "",

                    //36-40
                    "Estimated Received"+Environment.NewLine+"Date Revision",
                    "Price"+Environment.NewLine+"After Revision",
                    "Total"+Environment.NewLine+"After Revision",
                    "Discount%"+Environment.NewLine+"After Revision",
                    "Discount Amount"+Environment.NewLine+"After Revision",

                    //41-44
                    "Rounding Value"+Environment.NewLine+"After Revision",
                    "Grand Total"+Environment.NewLine+"After Revision",
                    "PO Quantity"+Environment.NewLine+"After Revision"
                },
                new int[]
                {
                    //0
                    50,
                
                    //1-5
                    130, 80, 150, 80, 250,  
                    
                    //6-10
                    150, 200, 80, 300, 230,
                    
                    //11-15
                    80, 80, 80, 80, 80,
                    
                    //16-20
                    120, 80, 80, 150, 100,

                    //21-25
                    100, 100, 150, 150, 150,

                    //26-30
                    150, 20, 150, 20, 150, 

                    //31-35
                    20, 150, 20, 150, 20, 

                    //36
                    150, 150, 150, 150, 150,

                    //41-44
                    150, 150, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 27, 29, 31, 33, 35 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 14, 16, 19, 20, 21, 22, 23, 24, 25, 37, 38, 39, 40, 41, 42, 43 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 17, 36 });
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 12, 20, 21, 22 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 10, 12, 20, 21, 22 }, false);
            if (!mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            if (!mIsShowTax) Sm.GrdColInvisible(Grd1, new int[] { 16 }, false);
            Grd1.Cols[16].Move(22);
            Grd1.Cols[23].Move(19);

            if (!mIsQTAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29 });

            if (mIsRptProcurementUsePORevision) 
            {
                Grd1.Cols[36].Move(17);
                Grd1.Cols[37].Move(20); 
                Grd1.Cols[38].Move(22);
                Grd1.Cols[39].Move(24);
                Grd1.Cols[40].Move(26);
                Grd1.Cols[41].Move(28);
                Grd1.Cols[24].Move(29);
                Grd1.Cols[42].Move(30);
                Grd1.Cols[43].Move(12);
                
                Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41 }, false);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 36, 37, 38, 39, 40, 41, 42, 43 }, false);

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 30, 32, 34, 36, 37, 38, 39, 40, 41, 42, 43 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 11, 12, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);

            if(mIsRptProcurementUsePORevision) Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ", Filter2 = " And 0=0 ", Filter3 = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "X.DocNo", "X.LocalDocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);
                Sm.FilterStr(ref Filter3, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });

                switch (Sm.GetLue(LueStatus))
                {
                    case "OP":
                        Filter2 += " And X.ProcessInd In ('O', 'P') ";
                        break;
                    default:
                        Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueStatus), "X.ProcessInd", true);
                        break;
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(Filter, Filter2, Filter3) + " Order By F.VdName, A.DocDt, A.DocNo, E.ItName;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "ProcessIndDesc", "VdName", "SiteName", 
                            
                            //6-10
                            "DeptName", "ItCode", "ItName", "ForeignName", "Qty", 
                            
                            //11-15
                            "Qty2", "Qty3", "Balance", "PurchaseUomCode", "PPN",
                            
                            //16-20
                            "EstRecvDt", "CurCode", "UPrice", "Discount", "DiscountAmt",
                            
                            //21-25
                            "RoundingValue", "Total", "GrandTotal", "TaxAmt", "FileName1",

                            //26-30
                            "FileName2", "FileName3", "FileName4", "FileName5", "EstRecvDtRev", 
                            
                            //31-35
                            "UPriceRev", "TotalRev", "DiscountRev", "DiscountAmtRev", "RoundingValueRev",

                            //36-37
                            "GrandTotalRev", "QtyRev"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 36, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 37);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 27 && Sm.GetGrdStr(Grd1, e.RowIndex, 26).Length != 0 && mIsQTAllowToUploadFile)
            {
                e.DoDefault = false;
                DownloadFileKu(e.RowIndex, 26);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }

            if (e.ColIndex == 29 && Sm.GetGrdStr(Grd1, e.RowIndex, 28).Length != 0 && mIsQTAllowToUploadFile)
            {
                e.DoDefault = false;
                DownloadFileKu(e.RowIndex, 28);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 27 && Sm.GetGrdStr(Grd1, e.RowIndex, 26).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 26);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }

            if (e.ColIndex == 29 && Sm.GetGrdStr(Grd1, e.RowIndex, 28).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 28);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }

            if (e.ColIndex == 31 && Sm.GetGrdStr(Grd1, e.RowIndex, 30).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 30);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }

            if (e.ColIndex == 33 && Sm.GetGrdStr(Grd1, e.RowIndex, 32).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 32);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }

            if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 34).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 34);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptPOOutstanding"));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial Fulfilled' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void DownloadFileKu(int Row, int Cols)
        {
            string mFileName = Sm.GetGrdStr(Grd1, Row, Cols);
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, mFileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = mFileName;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsGrdValueEmpty(Grd1, Row, Cols, false, "File is empty") && downloadedData != null && downloadedData.Length != 0)
            {
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    Sm.StdMsg(mMsgType.Info, "Saved Successfully");
                }
            }
            else
                Sm.StdMsg(mMsgType.Warning, "No File was Downloaded Yet.");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion
    }
}
