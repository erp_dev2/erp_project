﻿#region Update
/*
    16/05/2018 [TKG] tambah entity
    24/06/2021 [TKG/IMS] department mengambil dari emp ss list
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSSList3 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mSSPCode = string.Empty;
        private bool mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptEmpSSList3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);

                var Dt = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));
                
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mSSPCode = Sm.GetParameter("SSProgramForHealth");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 12].Value = "'" + Sm.GetGrdStr(Grd1, Row, 12);
                Grd1.Cells[Row, 18].Value = "'" + Sm.GetGrdStr(Grd1, Row, 18);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 12].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 12), Sm.GetGrdStr(Grd1, Row, 12).Length - 1);
                Grd1.Cells[Row, 18].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 18), Sm.GetGrdStr(Grd1, Row, 18).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Case When B.EmpInd='Y' Then '1' Else '2' End As RecType, ");
            SQL.AppendLine("B.EmpCode, B.PName As EmpName, C.EmpCodeOld, D.PosName, C.DeptCode, E.DeptName, H.SiteName, J.EntName, I.OptDesc As PayrunPeriod, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, B.IDNo, B.BirthDt, B.Age, ");
            SQL.AppendLine("B.SSCode, G.SSName, B.CardNo, B.StartDt, ");
            SQL.AppendLine("Case When B.EmpInd='Y' Then 'Peserta' Else F.OptDesc End As Status, ");
            SQL.AppendLine("B.Salary, B.EmployerAmt, B.EmployeeAmt, B.TotalAmt ");
            SQL.AppendLine("From TblEmpSSListHdr A ");
            SQL.AppendLine("Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode " + Filter);
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblOption F On F.OptCat='FamilyStatus' And B.FamilyStatus=F.OptCode ");
            SQL.AppendLine("Inner Join TblSS G On B.SSCode=G.SSCode And G.SSPCode=@SSPCode ");
            SQL.AppendLine("Left Join TblSite H On C.SiteCode=H.SiteCode ");
            SQL.AppendLine("Left Join TblOption I On I.OptCat='PayrunPeriod' And C.PayrunPeriod=I.OptCode ");
            SQL.AppendLine("Left Join TblEntity J On A.EntCode=J.EntCode ");
            SQL.AppendLine("Where A.Yr=@Yr And A.Mth=@Mth And CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (A.SiteCode Is Null Or (");
                SQL.AppendLine("        A.SiteCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupSite ");
                SQL.AppendLine("            Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ))) ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "",
                        "Participants",
                        "Old"+Environment.NewLine+"Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Site",
                        "Entity",
                        "Payrun Period",
                        "Join"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Resign"+Environment.NewLine+"Date",
                        "Identity#",
                        "Birth Date",
                        "Age",
                        "Social Security Code",
                        
                        //16-20
                        "",
                        "Social Security",
                        "Card#",
                        "Start"+Environment.NewLine+"Date",
                        "Status",
                        
                        //21-24
                        "Salary",
                        "Employer"+Environment.NewLine+"(Amount)",
                        "Employee"+Environment.NewLine+"(Amount)",
                        "Total"+Environment.NewLine+"(Amount)"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 200, 80, 150, 
                        
                        //6-10
                        150, 150, 200, 150, 100, 
                        
                        //11-15
                        100, 150, 100, 80, 0, 
                        
                        //16-20
                        20, 150, 100, 100, 150, 
                        
                        //21-24
                        100, 100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 21, 22, 23, 24 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11, 13, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 10, 13, 15, 16 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 10, 13, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year")) return;
                if (Sm.IsLueEmpty(LueMth, "Month")) return;

                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1=1 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "C.PayrunPeriod", true);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SSPCode", mSSPCode);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Filter) + " Order By E.DeptName, H.SiteName, B.EmpCode, Case When B.EmpInd='Y' Then '1' Else '2' End, B.PName;",
                    new string[] 
                        { 
                            //0
                            "RecType",  

                            //1-5
                            "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName",  
                            
                            //6-10
                            "SiteName", "EntName", "PayrunPeriod", "JoinDt", "ResignDt", 
                            
                            //11-15
                            "IDNo", "BirthDt", "Age", "SSCode", "SSName", 
                            
                            //16-20
                            "CardNo", "StartDt", "Status", "Salary", "EmployerAmt", 
                            
                            //21-22
                            "EmployeeAmt", "TotalAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            if (Sm.DrStr(dr, 0) == "1")
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);

                            if (Sm.DrStr(dr, 0) == "1")
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                            }
                            else
                            {
                                for (int Col = 21; Col <= 24; Col++)
                                    Grd.Cells[Row, Col].Value = 0m;
                            }
                        }, false, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 22, 23, 24 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun Period");
        }

        #endregion

        #endregion
    }
}
