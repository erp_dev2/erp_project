﻿#region Update
/*
    07/10/2019 [WED/IMS] new apps
    06/11/2019 [WED/IMS] tambah kolom Qty, UPrice, dan Total. bisa dihapus dan ditambah item BOQ nya. kalau ada item baru BOQ yg sebelumnya belum ada di SOC, maka di SOC tambah data baru, dengan nominal contract nya 0
    07/11/2019 [WED/IMS] remark diisi di revision
    21/11/2019 [WED/IMS] tambah PO# dan update ke SO Contract. Delivery Date dibuka, dan update ke SO Contract
    21/11/2019 [WED/IMS] price di list of item ambil dari boq inventory ataupun boq service
    26/11/2019 [DITA/IMS] Attachment File untuk SOContractRevision
    29/11/2019 [WED/IMS] waktu klik delivery date, tidak meng 0 kan nominal nya
    17/12/2019 [DITA/IMS] Bug detail socontract ga tampil
    06/01/2020 [WED/IMS] tax minta dimunculkan
    20/02/2020 [WED/IMS] tidak bisa insert, save, cancel, edit
    12/05/2020 [WED/IMS] semua amount di hide (grid) dan dibuat 0 (header) berdasarkan parameter IsSOContract2AmtIsZero
    25/09/2020 [DITA/IMS] tambah informasi No dari SO Contract revision
    19/05/2021 [VIN/IMS] revisi source show SOC revision dtl
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmSOContractRevision2 : RunSystem.FrmBase3
    {
        #region Field, Property

        private string mSOCDocType = "2",
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty,
          mTxtFile = string.Empty,
          mTxtFile2 = string.Empty,
          mTxtFile3 = string.Empty;

        private byte[] downloadedData;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
           mCity = string.Empty, mCnt = string.Empty, mSADNo = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated =false,
            mIsSOContract2AmtIsZero = false;
        private bool IsInsert = false;
        internal string mIsSoUseDefaultPrintout;
        internal string mGenerateCustomerCOAFormat = string.Empty;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmSOContractRevision2Find FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContractRevision2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmSOContractRevision");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnInsert.Visible = BtnCancel.Visible = BtnSave.Visible = BtnCancel.Visible = false;
                SetNumberOfSalesUomCode();
                GetParameter();

                TcOutgoingPayment.SelectedTabPage = TpItem;
                TcOutgoingPayment.SelectedTabPage = TpInventory;
                TcOutgoingPayment.SelectedTabPage = TpService;

                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueTaxCode(ref LueTaxCode);
                SetLueStatus(ref SOCLueStatus);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {
            #region Item

            Grd3.Cols.Count = 31;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Agent Code",
                    "Agent",
                    "Item's"+Environment.NewLine+"Code",
                    "",

                    //6-10
                    "Item's Name",
                    "Packaging",
                    "Quantity"+Environment.NewLine+"Packaging",
                    "Quantity",
                    "UoM",

                    //11-15
                    "Price"+Environment.NewLine+"(Price List)",
                    "Discount"+Environment.NewLine+"%",
                    "Discount"+Environment.NewLine+"Amount",
                    "Price After"+Environment.NewLine+"Discount",                        
                    "Promo"+Environment.NewLine+"%",

                    //16-20
                    "Price"+Environment.NewLine+"Before Tax",
                    "Tax"+Environment.NewLine+"%",
                    "Tax"+Environment.NewLine+"Amount",
                    "Price"+Environment.NewLine+"After Tax",                        
                    "Total",

                    //21-25
                    "Delivery"+Environment.NewLine+"Date",
                    "Remark",
                    "CtQtDNo",
                    "Specification",
                    "Customer's"+Environment.NewLine+"Item Code",

                    //26-30
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Volume",
                    "Total Volume",
                    "Uom"+Environment.NewLine+"Volume",
                    "No"
                },
                new int[] 
                {
                    //0
                    40,

                    //1-5
                    20, 0, 180, 80, 20, 
                    
                    //6-10
                    200, 100, 100, 80, 80, 
                    
                    //11-15
                    100, 80, 100, 100, 80,
                    
                    //16-20
                    100, 80, 100, 100, 120,   
                    
                    //21-25
                    100, 400, 0, 120, 120,

                    //26-30
                    250, 100, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            Grd3.Cols[24].Move(7);
            Grd3.Cols[26].Move(7);
            Grd3.Cols[25].Move(7);
            Grd3.Cols[27].Move(14);
            Grd3.Cols[28].Move(15);
            Grd3.Cols[29].Move(16);
            Grd3.Cols[30].Move(5);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 23, 24, 25, 27, 28, 29 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd3, new int[] { 26 });

            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd3, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            }

            #endregion

            #region Grid 1 - ARDP

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3 });
            }

            #endregion

            #region BOQ Service

            Grd2.Cols.Count = 25;
            Grd2.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "BOM#",
                    "BOMDNo",
                    "Item Code",
                    "Item Name",
                    "Item Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Minimal"+Environment.NewLine+"Service",
                    "Material",
                    "Total",

                    //11-15
                    "After Sales",
                    "Total"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",
                    "SPH",
                    "Total Price",

                    //16-20
                    "Remark",
                    "Total Price"+Environment.NewLine+"Contract",
                    "Total Price"+Environment.NewLine+"Revision",
                    "Quantity Contract",
                    "Unit Price"+Environment.NewLine+"Contract",

                    //21-24
                    "Quantity Revision",
                    "Unit Price"+Environment.NewLine+"Revision",
                    "SOCDNo",
                    "No"
                },
                new int[] 
                {
                    20,
                    150, 100, 100, 180, 180, 
                    100, 100, 120, 120, 120, 
                    120, 120, 120, 120, 120, 
                    200, 120, 120, 120, 120,
                    120, 120, 0, 100
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 23, 24 });
            Grd2.Cols[20].Move(17);
            Grd2.Cols[19].Move(17);
            Grd2.Cols[22].Move(20);
            Grd2.Cols[21].Move(20);
            Grd2.Cols[24].Move(3);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 3, 23 });

            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 20, 22 });
            }

            #endregion

            #region BOQ Inventory

            Grd5.Cols.Count = 27;
            Grd5.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd5, new string[] 
                {
                    //0
                    "",

                    //1-5
                    "BOM#",
                    "BOMDNo",
                    "Item Code",
                    "Item Name",
                    "Item Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Cost"+Environment.NewLine+"of Goods",
                    "After Sales",
                    "COM",

                    //11-15
                    "OH",
                    "Margin",
                    "Design Cost",
                    "Unit Price"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",

                    //16-20
                    "Unit Price"+Environment.NewLine+"(SPH)",
                    "Total Price"+Environment.NewLine+"(SPH)",
                    "Remark",
                    "Total Price"+Environment.NewLine+"Contract",
                    "Total Price"+Environment.NewLine+"Revision",

                    //21-25
                    "Quantity Contract",
                    "Unit Price"+Environment.NewLine+"Contract",
                    "Quantity Revision",
                    "Unit Price"+Environment.NewLine+"Revision",
                    "SOCDNo",

                    //26
                    "No"
                },
                new int[] 
                {
                    20,
                    150, 80, 100, 180, 180, 
                    120, 100, 120, 120, 120, 
                    120, 120, 120, 120, 120,
                    120, 120, 200, 120, 120,
                    120, 120, 120, 120, 0,
                    100
                }
            );
            Sm.GrdColButton(Grd5, new int[] { 0 });
            Sm.GrdFormatDec(Grd5, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColReadOnly(Grd5, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 25, 26 });
            Grd5.Cols[22].Move(19);
            Grd5.Cols[21].Move(19);
            Grd5.Cols[24].Move(22);
            Grd5.Cols[23].Move(22);
            Grd5.Cols[26].Move(3);
            Sm.GrdColInvisible(Grd5, new int[] { 1, 2, 3 });

            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd5, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 22, 24 });
            }

            #endregion

        }
        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtFile, TxtFile2, TxtFile3, ChkFile, ChkFile2, ChkFile3, TxtPONo
                    }, true);
                    SOCChkCancelInd.Properties.ReadOnly = true;
                    BtnSOCDocNo.Enabled = false;
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = false;
                    BtnDownload.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 16, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 18, 23, 24 });
                    Grd3.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, TxtPONo, DteDeliveryDt
                    }, false);
                    BtnSOCDocNo.Enabled = true;
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = true;
                    BtnDownload.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = false;
                    SOCDteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 16, 21, 22 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 18, 23, 24 });
                    Grd3.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            IsInsert = false;
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            mTxtFile = mTxtFile2 = mTxtFile3 =  string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, TxtStatus, DteDocDt, TxtSOCDocNo,
                  TxtFile, TxtFile3, TxtFile2, DteDeliveryDt, TxtPONo, LuePtCode, LueTaxCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtServiceAmt }, 0);
            SOCChkCancelInd.Checked = false;
            ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
            PbUpload.Value = PbUpload2.Value = PbUpload3.Value = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 });
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearData3()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            mTxtFile = mTxtFile2 = mTxtFile3 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, TxtFile, TxtFile3, TxtFile2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtServiceAmt }, 0);
            ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
            PbUpload.Value = PbUpload2.Value = PbUpload3.Value = 0;
            SOCChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractRevision2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            //try
            //{
            //    ClearData();
            //    SetFormControl(mState.Insert);
            //    Sm.SetDteCurrentDate(DteDocDt);
            //    TxtStatus.EditValue = "Outstanding";
            //    IsInsert = true;
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            //try
            //{
            //    if (TxtDocNo.Text.Length == 0)
            //        InsertData();
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            //if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            //ClearData();
            //SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (SOCChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Grid Methods

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 21, 22 }, e.ColIndex))
            {
                decimal mQty = 0m, mUPrice = 0m, mTotal = 0m;
                mQty = Sm.GetGrdDec(Grd2, e.RowIndex, 21);
                mUPrice = Sm.GetGrdDec(Grd2, e.RowIndex, 22);
                mTotal = mQty * mUPrice;
                Grd2.Cells[e.RowIndex, 18].Value = mTotal;
                ComputeTotalBOQService();
                ComputeItem();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && IsInsert)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeTotalBOQService();
                ComputeItem();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtSOCDocNo, "SOC Document#", false) && TxtDocNo.Text.Length == 0)
                {
                    Sm.FormShowDialog(new FrmSOContractRevision2Dlg2(this, TxtBOQDocNo.Text));
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd3.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 21 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 21) Sm.DteRequestEdit(Grd3, DteDeliveryDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    //Sm.SetGrdNumValueZero(ref Grd3, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
                }
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtSOCDocNo, "SOC Document#", false) && TxtDocNo.Text.Length == 0)
                {
                    Sm.FormShowDialog(new FrmSOContractRevision2Dlg3(this, TxtBOQDocNo.Text));
                }
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && IsInsert)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
                ComputeTotalBOQInventory();
                ComputeItem();
            }
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 23, 24 }, e.ColIndex))
            {
                decimal mQty = 0m, mUPrice = 0m, mTotal = 0m;
                mQty = Sm.GetGrdDec(Grd5, e.RowIndex, 23);
                mUPrice = Sm.GetGrdDec(Grd5, e.RowIndex, 24);
                mTotal = mQty * mUPrice;
                Grd5.Cells[e.RowIndex, 20].Value = mTotal;
                ComputeTotalBOQInventory();
                ComputeItem();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string mIndexDocNo = string.Empty;
            string DocNo = string.Concat(TxtSOCDocNo.Text, "/", GenerateDocNo());
            string mDNo4 = string.Empty, mDNo5 = string.Empty;
            int mDNo4x = 0, mDNo5x = 0;

            mDNo4 = Sm.GetValue("Select Max(DNo) From TblSOContractDtl4 Where DocNo = @Param;", TxtSOCDocNo.Text);
            mDNo5 = Sm.GetValue("Select Max(DNo) From TblSOContractDtl5 Where DocNo = @Param;", TxtSOCDocNo.Text);

            if (mDNo4.Length == 0) mDNo4 = "0";
            if (mDNo5.Length == 0) mDNo5 = "0";

            mDNo4x = Int32.Parse(mDNo4); mDNo5x = Int32.Parse(mDNo5);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractRevisionHdr(DocNo));
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                {
                    cml.Add(SaveSOContractRevisionDtl(DocNo, Row));
                    cml.Add(SaveSOContractDtl(TxtSOCDocNo.Text, Row));
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 23).Length == 0)
                    {
                        mDNo4x += 1;
                        cml.Add(SaveSOContractRevisionDtl4(DocNo, Row, mDNo4x, true));
                        cml.Add(SaveSOContractDtl4(TxtSOCDocNo.Text, Row, mDNo4x));
                    }
                    else
                    {
                        cml.Add(SaveSOContractRevisionDtl4(DocNo, Row, Row, false));
                    }
                }
            }

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 25).Length == 0)
                    {
                        mDNo5x += 1;
                        cml.Add(SaveSOContractRevisionDtl5(DocNo, Row, mDNo5x, true));
                        cml.Add(SaveSOContractDtl5(TxtSOCDocNo.Text, Row, mDNo5x));
                    }
                    else
                    {
                        cml.Add(SaveSOContractRevisionDtl5(DocNo, Row, Row, false));
                    }
                }
            }

            cml.Add(UpdatePrevAmt(DocNo));

            cml.Add(UpdateSOContractHdr(TxtSOCDocNo.Text));

            for (int i = 0; i < Grd3.Rows.Count; ++i)
                if (Sm.GetGrdStr(Grd3, i, 4).Length > 0)
                    cml.Add(UpdateSOContractDtl(TxtSOCDocNo.Text, i));

            //cml.Add(UpdateSOCAmt(DocNo));

            Sm.ExecCommands(cml);
            if (TxtFile.Text.Length > 0 && TxtFile.Text != mTxtFile)
                UploadFile();
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != mTxtFile2)
                UploadFile2();
            if (TxtFile3.Text.Length > 0 && TxtFile3.Text != mTxtFile3)
                UploadFile3();
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSOCDocNo, "SOC Document#", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDataAlreadyProcessedToDR() ||
                IsUploadFileNotValid();
        }

        private bool IsDataAlreadyProcessedToDR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblDRDtl A ");
            SQL.AppendLine("Inner Join TblDRHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.SODocNo = @Param ");
            SQL.AppendLine("Limit 1; ");
            
            if(Sm.IsDataExist(SQL.ToString(), TxtSOCDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to DR #" + Sm.GetValue(SQL.ToString(), TxtSOCDocNo.Text));
                return true;
            }

            return false;
        }

        private MySqlCommand SaveSOContractRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, Status, Amt, Amt2, DocType, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, 'O', @Amt, @Amt2, @DocType, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='SOCRev'; ");
            }

            SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Delete From TblSOContractDtl Where DocNo = @SOCDocNo; ");
                
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mSOCDocType);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtServiceAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtInventoryAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSOContractDtl(DocNo, DNo, ItCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, UPrice, UPriceBefTax, UPriceAfTax, TaxRate, TaxAmt, Amt, DeliveryDt,  Remark, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @ItCode, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, @UPrice, @UPriceBefTax, @UPriceAfTax, @TaxRate, @TaxAmt, @Amt, @DeliveryDt,  @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl4(string DocNo, int Row, int DNo, bool IsNew)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl4(DocNo, SOCDocNo, DNo, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Qty, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            if (IsNew) Sm.CmParam<String>(ref cm, "@DNo", DNo.ToString());
            else Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 22));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl5(string DocNo, int Row, int DNo, bool IsNew)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl5(DocNo, SOCDocNo, DNo, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Qty, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            if (IsNew) Sm.CmParam<String>(ref cm, "@DNo", DNo.ToString());
            else Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd5, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd5, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd5, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd5, Row, 18));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl4(string DocNo, int Row, int DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractDtl4(DocNo, DNo, BOQDocNo, BOMDocNo, BOMDNo, Amt, Qty, UPrice, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BOQDocNo, @BOMDocNo, @BOMDNo, @Amt, @Qty, @UPrice, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo.ToString());
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", 0m);
            Sm.CmParam<Decimal>(ref cm, "@Qty", 0m);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", 0m);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl5(string DocNo, int Row, int DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractDtl5(DocNo, DNo, BOQDocNo, BOMDocNo, BOMDNo, Amt, Qty, UPrice, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BOQDocNo, @BOMDocNo, @BOMDNo, @Amt, @Qty, @UPrice, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo.ToString());
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", 0m);
            Sm.CmParam<Decimal>(ref cm, "@Qty", 0m);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", 0m);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePrevAmt(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractRevisionHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt, A.PrevAmt2 = B.AmtBOM; ");
            
            SQL.AppendLine("Update TblSOContractRevisionDtl4 A ");
            SQL.AppendLine("Inner Join TblSOContractDtl4 B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

            SQL.AppendLine("Update TblSOContractRevisionDtl5 A ");
            SQL.AppendLine("Inner Join TblSOContractDtl5 B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand UpdateSOContractHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine(" PONo = @PONo ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PONo", TxtPONo.Text);

            return cm;
        }

        private MySqlCommand UpdateSOContractDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblSOContractDtl Set ");
            SQL.AppendLine("  DeliveryDt = @DeliveryDt ");
            SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));

            return cm;
        }

        private MySqlCommand UpdateSOContractFile(string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateSOContractFile2(string FileName2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName2=@FileName2 ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@FileName2", FileName2);

            return cm;
        }

        private MySqlCommand UpdateSOContractFile3(string FileName3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName3=@FileName3 ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@FileName3", FileName3);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOContractRevision(DocNo);
                ShowSOContractRevisionHdr(DocNo);
                ShowSOContractRevisionDtl(DocNo);
                ShowSOContractRevisionDtl4(DocNo);
                ShowSOContractRevisionDtl5(DocNo);
                ShowARDownPaymentRevision(DocNo);
                ComputeTotalBOQService();
                ComputeTotalBOQInventory();

                if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
                {
                    TxtServiceAmt.EditValue = Sm.FormatNum(0m, 0);
                    TxtInventoryAmt.EditValue = Sm.FormatNum(0m, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.SOCDocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.Amt ");
            SQL.AppendLine("From TblSOContractRevisionHdr A  ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-4
                    "SOCDocNo", "DocDt", "StatusDesc", "Amt",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                    TxtServiceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                }, true
            );
        }

        private void ShowSOContractRevisionHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, T.Amt, T.Amt2, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname, A.FileName, A.FileName2, A.FileName3, A.PONo, T.PtCode, A.TaxCode ");
            SQL.AppendLine("From TblSOContractRevisionHdr T ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On T.SOCDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Where T.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                       "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-30
                        "ProjectName", "Amt2", "FileName", "FileName2", "FileName3",

                        //31-33
                        "PONo", "PtCode", "TaxCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtServiceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueSPCode(ref LueSPCode);
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        TxtInventoryAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                        TxtFile.EditValue = Sm.DrStr(dr, c[28]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[29]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[30]);
                        if (TxtFile.Text.Length > 0) ChkFile.Checked = true;
                        if (TxtFile2.Text.Length > 0) ChkFile2.Checked = true;
                        if (TxtFile3.Text.Length > 0) ChkFile3.Checked = true;
                        TxtPONo.EditValue = Sm.DrStr(dr, c[31]);
                        Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[33]));
                    }, true
                );
        }

        private void ShowSOContractRevisionDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, A1.Qty QtyPackagingUnit, A1.Qty, ");
            SQL.AppendLine("   A1.Uprice, 0 As Discount, 0 As DiscountAmt, ");
            SQL.AppendLine("   A1.UPrice As UPriceAfterDiscount, ");
            SQL.AppendLine("   0.00 As PromoRate, ");
            SQL.AppendLine("   A1.UPriceBefTax As UPriceBefore, ");
            SQL.AppendLine("   (A1.UPriceAfTax+(B.TaxRate * A1.Uprice)) As UPriceAfterTax, ");
            SQL.AppendLine("   A1.Amt As Total, ");
            SQL.AppendLine("   B.TaxRate, ");
            SQL.AppendLine("   (B.TaxRate * A1.Uprice) TaxAmt, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  ");
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom, A1.No  ");
            SQL.AppendLine("   From TblSOContractRevisionDtl A1 ");
            SQL.AppendLine("   Inner Join TblSOContractHdr A On A1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo And A1.DNo = B.DNo ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Where A1.DocNo = @DocNo ");
            SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-23
                    "TotalVolume", "VolUom", "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractRevisionDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BOMDocNo, A.BOMDNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.MinimalServiceAmt, A.MaterialAmt, A.Amt, A.AfterSalesAmt, ");
            SQL.AppendLine("A.TotalPPHAmt, A.SpaceNegoAmt, A.SPHAmt, A.TotalAmt, T0.Remark, T.Amt As ContractAmt, T0.Amt As RevisionAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, T0.Qty RevisionQty, T0.UPrice RevisionUPrice, T0.No ");
            SQL.AppendLine("From TblSOContractRevisionDtl4 T0 ");
            SQL.AppendLine("Inner Join TblSOContractDtl4 T On T0.SOCDocNo = T.DocNo And T0.DNo = T.DNo And T0.DOcNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl2 A On T.BOQDocNo = A.DocNo And T.ItCode = A.ItCode ");
            //SQL.AppendLine("Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On T.ItCode = C.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOMDocNo",

                    //1-5
                    "BOMDNo", "ItCode", "ItName", "ItCodeInternal", "Qty", 

                    //6-10
                    "PurchaseUomCode", "MinimalServiceAmt", "MaterialAmt", "Amt", "AfterSalesAmt", 

                    //11-15
                    "TotalPPHAmt", "SpaceNegoAmt", "SPHAmt", "TotalAmt", "Remark",

                    //16-20
                    "ContractAmt", "RevisionAmt", "ContractQty", "ContractUPrice", "RevisionQty",

                    //21-22
                    "RevisionUPrice", "No"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void ShowSOContractRevisionDtl5(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BOMDocNo, A.BOMDNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQL.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, T0.Remark, T.Amt As ContractAmt, T0.Amt RevisionAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, T0.Qty RevisionQty, T0.UPrice RevisionUPrice, T0.No ");
            SQL.AppendLine("From TblSOContractRevisionDtl5 T0 ");
            SQL.AppendLine("Inner Join TblSOContractDtl5 T On T0.SOCDocNo = T.DocNo And T0.DNo = T.DNo And T0.DOcNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl3 A On T.BOQDocNo = A.DocNo And T.ItCode = A.ItCode ");
            //SQL.AppendLine("Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On T.ItCode = C.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOMDocNo",

                    //1-5
                    "BOMDNo", "ItCode", "ItName", "ItCodeInternal", "Qty", 

                    //6-10
                    "PurchaseUomCode", "CostOfGoodsAmt", "AfterSalesAmt", "COMAmt", "OHAmt", 

                    //11-15
                    "MarginAmt", "DesignCostAmt", "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", 

                    //16-20
                    "TotalPriceSPHAmt", "Remark", "ContractAmt", "RevisionAmt", "ContractQty", 

                    //21-24
                    "ContractUPrice", "RevisionQty", "RevisionUPrice", "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);

        }

        private void ShowARDownPaymentRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo In (Select SOCDocNo From TblSOContractRevisionHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname, A.FileName, A.FileName2, A.FileName3, A.PONo ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                       "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-30
                        "ProjectName", "FileName", "FileName2", "FileName3", "PONo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtServiceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueSPCode(ref LueSPCode);
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[27]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[28]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[29]);
                        if (TxtFile.Text.Length > 0) ChkFile.Checked = true;
                        if (TxtFile2.Text.Length > 0) ChkFile2.Checked = true;
                        if (TxtFile3.Text.Length > 0) ChkFile3.Checked = true;
                        mTxtFile= TxtFile.Text ;
                        mTxtFile2= TxtFile2.Text ;
                        mTxtFile3=TxtFile3.Text ;
                        TxtPONo.EditValue = Sm.DrStr(dr, c[30]);
                    }, true
                );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   B.Uprice, 0 As Discount, 0 As DiscountAmt, ");
            SQL.AppendLine("   B.UPrice As UPriceAfterDiscount, ");
            SQL.AppendLine("   0.00 As PromoRate, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceBefore, ");
            SQL.AppendLine("   B.UPriceAfTax As UPriceAfterTax, ");
            SQL.AppendLine("   B.Amt As Total, ");
            SQL.AppendLine("   B.TaxRate, ");
            SQL.AppendLine("   0 As TaxAmt, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  "); 
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom  ");
            SQL.AppendLine("   From TblSOContractHdr A ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Where A.DocNo = @DocNo ");
            SQL.AppendLine(") T Order By DNo; ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DNo SOCDNo, A.BOMDocNo, A.BOMDNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.MinimalServiceAmt, A.MaterialAmt, A.Amt, A.AfterSalesAmt, ");
            SQL.AppendLine("A.TotalPPHAmt, A.SpaceNegoAmt, A.SPHAmt, A.TotalAmt, T2.Remark, T.Amt As ContractAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, T2.Qty RevisionQty, T2.UPrice RevisionUPrice, T2.Amt RevisionAmt ");
            SQL.AppendLine("From (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) T0 ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr T1 On T0.DocNo = T1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionDtl4 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl4 T On T2.SOCDocNo = T.DocNo And T2.DNo = T.DNo ");
            SQL.AppendLine("Inner Join TblBOQDtl2 A On T.BOQDocNo = A.DocNo And T.BOMDocNo = A.BOMDocNo And T.BOMDNo = A.BOMDNo ");
            SQL.AppendLine("    And T.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOMDocNo",

                    //1-5
                    "BOMDNo", "ItCode", "ItName", "ItCodeInternal", "Qty", 

                    //6-10
                    "PurchaseUomCode", "MinimalServiceAmt", "MaterialAmt", "Amt", "AfterSalesAmt", 

                    //11-15
                    "TotalPPHAmt", "SpaceNegoAmt", "SPHAmt", "TotalAmt", "Remark",

                    //16-20
                    "ContractAmt", "ContractQty", "ContractUPrice", "RevisionQty", "RevisionUPrice", 
                    
                    //21-22
                    "RevisionAmt", "SOCDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowSOContractDtl5(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DNo SOCDNo, A.BOMDocNo, A.BOMDNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQL.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, T2.Remark, T.Amt As ContractAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, T2.Qty RevisionQty, T2.UPrice RevisionUPrice, T2.Amt RevisionAmt ");
            SQL.AppendLine("From (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) T0 ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr T1 On T0.DocNo = T1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionDtl5 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl5 T On T2.SOCDocNo = T.DocNo And T2.DNo = T.DNo ");
            SQL.AppendLine("Inner Join TblBOQDtl3 A On T.BOQDocNo = A.DocNo And T.BOMDocNo = A.BOMDocNo And T.BOMDNo = A.BOMDNo ");
            SQL.AppendLine("    And T.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOMDocNo",

                    //1-5
                    "BOMDNo", "ItCode", "ItName", "ItCodeInternal", "Qty", 

                    //6-10
                    "PurchaseUomCode", "CostOfGoodsAmt", "AfterSalesAmt", "COMAmt", "OHAmt", 

                    //11-15
                    "MarginAmt", "DesignCostAmt", "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", 

                    //16-20
                    "TotalPriceSPHAmt", "Remark", "ContractAmt", "ContractQty", "ContractUPrice",

                    //21-24
                    "RevisionQty", "RevisionUPrice", "RevisionAmt", "SOCDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        private void UploadFile()
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile(toUpload.Name));
            Sm.ExecCommands(cml);
          
        }

        private void UploadFile2()
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

           
            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile2(toUpload.Name));
            Sm.ExecCommands(cml);
            
        }

        private void UploadFile3()
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

           
            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile3(toUpload.Name));
            Sm.ExecCommands(cml);
          
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFile2SizeNotvalid() ||
                IsFile3SizeNotvalid() ||
                IsFileNameAlreadyExisted() ||
                IsFileName2AlreadyExisted() ||
                IsFileName3AlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 1 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFile2SizeNotvalid()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 2 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFile3SizeNotvalid()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 3 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileName2AlreadyExisted()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileName3AlreadyExisted()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        internal string GetSelectedBOMData()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd2, Row, 1) + Sm.GetGrdStr(Grd2, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOMData2()
        {
            var SQL = string.Empty;
            if (Grd5.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd5, Row, 1) + Sm.GetGrdStr(Grd5, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'SOCRev' Limit 1; ");
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblSOContractRevisionHdr ");
            SQL.AppendLine("       Where SOCDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtSOCDocNo.Text);
        }

        internal void ShowSOContract(string DocNo)
        {
            ClearData3();
            ShowSOContractHdr(DocNo);
            ShowSOContractDtl(DocNo);
            ShowSOContractDtl4(DocNo);
            ShowSOContractDtl5(DocNo);
            ShowARDownPayment(DocNo);
        }

        internal void ComputeTotalBOQService()
        {
            decimal Amt = 0m;

            for (int i = 0; i < Grd2.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 3).Length > 0)
                {
                    Amt += Sm.GetGrdDec(Grd2, i, 18);
                }
            }
        
            TxtServiceAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeTotalBOQInventory()
        {
            decimal Amt = 0m;

            for (int i = 0; i < Grd5.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd5, i, 3).Length > 0)
                {
                    Amt += Sm.GetGrdDec(Grd5, i, 20);
                }
            }

            TxtInventoryAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeItem()
        {
            // old formula
            // Price (Before Tax) di list of item, ambil dari Contract Price nya boq
            // Total = Price (Before Tax) * quantity
            // new formula
            // Price (After Tax) di list of item, ambil dari Contract Price nya boq
            // Total = Price (After Tax) * quantity

            for (int i = 0; i < Grd3.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd3, i, 4).Length > 0)
                {
                    for (int j = 0; j < Grd2.Rows.Count; ++j)
                    {
                        if (Sm.GetGrdStr(Grd3, i, 4) == Sm.GetGrdStr(Grd2, j, 3))
                        {
                            //Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd2, j, 19);
                            Grd3.Cells[i, 19].Value = Sm.GetGrdDec(Grd2, j, 22);
                            Grd3.Cells[i, 9].Value = Sm.GetGrdDec(Grd2, j, 21);
                            Grd3.Cells[i, 8].Value = Sm.GetGrdDec(Grd2, j, 21);
                        }
                    }

                    for (int k = 0; k < Grd5.Rows.Count; ++k)
                    {
                        if (Sm.GetGrdStr(Grd3, i, 4) == Sm.GetGrdStr(Grd5, k, 3))
                        {
                            //Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd6, k, 21);
                            Grd3.Cells[i, 19].Value = Sm.GetGrdDec(Grd5, k, 24);
                            Grd3.Cells[i, 9].Value = Sm.GetGrdDec(Grd5, k, 23);
                            Grd3.Cells[i, 8].Value = Sm.GetGrdDec(Grd5, k, 23);
                        }
                    }

                    decimal mPriceAfTax = Sm.GetGrdDec(Grd3, i, 19);
                    decimal mTaxRate = Math.Round((((Sm.GetGrdDec(Grd3, i, 19) - Sm.GetGrdDec(Grd3, i, 16)) / Sm.GetGrdDec(Grd3, i, 16)) * 100m), 4);
                    decimal mTaxAmt = Sm.GetGrdDec(Grd3, i, 19) - Sm.GetGrdDec(Grd3, i, 16);
                    decimal mQty = Sm.GetGrdDec(Grd3, i, 9);
                    decimal mTotal = mPriceAfTax * mQty;

                    Grd3.Cells[i, 17].Value = Sm.FormatNum(mTaxRate, 0);
                    Grd3.Cells[i, 18].Value = Sm.FormatNum(mTaxAmt, 0);
                    Grd3.Cells[i, 20].Value = mTotal;
                }
            }

            //decimal Qty = 0m, TotalAmt = 0m, PriceGrid3 = 0m;
            //Qty = Sm.GetGrdDec(Grd3, 0, 8);
            //TotalAmt = Decimal.Parse(TxtServiceAmt.Text);

            //if (TotalAmt > 0 && Qty>0)
            //{
            //    PriceGrid3 = TotalAmt / Qty;
            //}

            //Grd3.Cells[0, 11].Value = PriceGrid3;
            //Grd3.Cells[0, 14].Value = PriceGrid3;
            //Grd3.Cells[0, 19].Value = PriceGrid3;
            //Grd3.Cells[0, 20].Value = PriceGrid3;
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsSOUseARDPValidated = Sm.GetParameter("IsSOUseARDPValidated") == "Y";
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsSOContract2AmtIsZero = Sm.GetParameterBoo("IsSOContract2AmtIsZero");
        }

        private void ParPrint()
        {
            
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }

            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }

        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueSPCode(ref LueSPCode);
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void TxtInventoryAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInventoryAmt, 0);
        }

        private void TxtServiceAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtServiceAmt, 0);
        }

        private void DteDeliveryDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd3, ref fAccept, e);
        }

        private void DteDeliveryDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeliveryDt, ref fCell, ref fAccept);
        }

        #endregion

        #region Button Event

        private void BtnSOCDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
                Sm.FormShowDialog(new FrmSOContractRevision2Dlg(this));
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #endregion

    }
}