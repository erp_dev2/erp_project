﻿#region Update
/*
    04/12/2020 [WED/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt9Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOCt9 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmDOCt9Find(FrmDOCt9 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, B.WhsName, C.CtName, A.DRDocNo, A.PLDocNo, A.Cnt, ");
            SQL.AppendLine("D.CancelInd, D.ItCode, E.ItName, D.BatchNo, D.Source, ");
            SQL.AppendLine("D.Qty, E.InventoryUomCode, D.Qty2, E.InventoryUomCode2, D.Qty3, E.InventoryUomCode3, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("A.KBRegistrationNo, A.KBRegistrationDt, A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=B.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Local"+Environment.NewLine+"Document#",
                        "Warehouse",
                        "Customer",
                        
                        //6-10
                        "DR#",
                        "PL#",
                        "Container#",
                        "Cancel",
                        "Item's"+Environment.NewLine+"Code",

                        //11-15
                        "Item's Name",
                        "Batch#",
                        "Source",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Created"+Environment.NewLine+"By",

                        //21-25
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //26-30
                        "Registration#",
                        "Registration Date",
                        "Contract#",
                        "Contract Date",
                        "Packing List#",

                        //31
                        "Packing List Date"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 130, 200, 250, 
                        
                        //6-10
                        140, 140, 150, 60, 100, 
                        
                        //11-15
                        200, 180, 180, 100, 80, 

                        //16-20
                        100, 80, 100, 80, 100,

                        //21-25
                        100, 100, 100, 100, 100,

                        //26-30
                        120, 120, 120, 120, 120,

                        //31
                        120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24, 27, 29, 31 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            if (!mFrmParent.mIsKawasanBerikatEnabled) Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29, 30, 31 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 13, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And D.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "WhsName", "CtName", "DRDocNo", 
                            
                            //6-10
                            "PLDocNo", "Cnt", "CancelInd", "ItCode", "ItName", 
                            
                            //11-15
                            "BatchNo", "Source", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CreateBy", "CreateDt", 
                            
                            //21-25
                            "LastUpBy", "LastUpDt", "KBRegistrationNo", "KBRegistrationDt", "KBContractNo", 
                            
                            //26-28
                            "KBContractDt", "KBPLNo", "KBPLDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 28);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
