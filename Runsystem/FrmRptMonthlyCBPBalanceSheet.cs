﻿#region Update
/*
  23/12/2020 [DITA/PHT] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyCBPBalanceSheet : RunSystem.FrmBase6
    {
        #region Field

        private string
      mMenuCode = string.Empty,
      mAccessInd = string.Empty,
      mSQL = string.Empty;
        private bool
            mIsAccountingRptUseJournalPeriod = false;

        #endregion

        #region Constructor

        public FrmRptMonthlyCBPBalanceSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth1);
                Sl.SetLueMth(LueMth2);
                Sm.SetLue(LueMth1, "01");
                Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
                Sl.SetLueProfitCenterCode(ref LueProfitCenter);
                SetLueCCCode(ref LueCCCode, string.Empty);
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Cols[0].Width = 30;

            Grd1.Header.Cells[0, 1].Value = "Account#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;
            Grd1.Cols[1].Width = 250;

            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;
            Grd1.Cols[2].Width = 200;

            for (int c = 1; c <= 12; c++)
            {
                Grd1.Header.Cells[1, 1 + (2 * c)].Value = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(c);
                Grd1.Header.Cells[1, 1 + (2 * c)].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[1, 1 + (2 * c)].SpanCols = 2;
                Grd1.Cols[1 + (2 * c)].Width = 100;
                Grd1.Header.Cells[0, 1 + (2 * c)].Value = "Total RKAP";
                Grd1.Header.Cells[0, 1 + (2 * c)].SpanRows = 1;
                Grd1.Header.Cells[0, 1 + (2 * c)].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[1 + (2 * c)].Width = 150;
                Grd1.Header.Cells[0, 2 + (2 * c)].Value = "Total Journal";
                Grd1.Header.Cells[0, 2 + (2 * c)].SpanRows = 1;
                Grd1.Header.Cells[0, 2 + (2 * c)].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[2 + (2 * c)].Width = 150;
            }


            Sm.GrdFormatDec(Grd1, new int[] { 
                3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
            }, 0);
            //Grd1.Cols.AutoWidth();
        }

        override protected void HideInfoInGrd()
        {
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth1, "Start Month")||
                Sm.IsLueEmpty(LueMth2, "End Month")
                ) return;
            var Yr = Sm.GetLue(LueYr);
            var Mth1 = Sm.GetLue(LueMth1);
            var Mth2 = Sm.GetLue(LueMth2);
            string SelectedCOA = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<CBP>();
                var l2 = new List<COACBP>();
                var l3 = new List<COAJournal>();
                var l4 = new List<JournalTemp>();
                var l5 = new List<MonthCols>();

                PrepDataCBP(ref l, Yr);
                if (l.Count > 0)
                {
                    SelectedCOA = GetSelectedCOA(ref l);
                    PrepCOACBP(ref l2, SelectedCOA);

                    //proses total nilai RKAP
                    if (l2.Count > 0)
                    {
                        Process1(ref l, ref l2, Yr);
                    }
                    PrepCOAJournal(ref l3, SelectedCOA, Yr);
                    PrepJournalTemp(ref l4, SelectedCOA, Yr, Mth1, Mth2);
                    Process2(ref l3, ref l4);
                    Process3(ref l2, ref l3);
                    Process4(ref l2);
                    Process5(ref l2);
                    Process6(ref l5);
                    Process7(ref l5, Mth1, Mth2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueCCCode(ref LookUpEdit Lue, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter Where CBPInd = 'Y' And ProfitCenterCode = @ProfitCenterCode Order By CCName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

        }

        private void PrepDataCBP(ref List<CBP> l, string Yr)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("SELECT A.Yr, B.AcNo, SUM(B.Amt01) Amt01, SUM(B.Amt02) Amt02, SUM(B.Amt03) Amt03, SUM(B.Amt04) Amt04,  ");
            SQL.AppendLine("SUM(B.Amt05) Amt05, SUM(B.Amt06) Amt06,SUM(B.Amt07) Amt07, SUM(B.Amt08) Amt08,  ");
            SQL.AppendLine("SUM(B.Amt09) Amt09, SUM(B.Amt10) Amt10,SUM(B.Amt11) Amt11, SUM(B.Amt12) Amt12  ");
            SQL.AppendLine("FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo=B.DocNo AND A.CancelInd != 'Y'  ");
            if (Sm.GetLue(LueProfitCenter).Length != 0)
                SQL.AppendLine("INNER JOIN TblCostCenter C On A.CCCode = C.CCCode And C.ProfitCenterCode=@ProfitCenterCode ");
            if (Sm.GetLue(LueCCCode).Length != 0)
                SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("WHERE A.Yr = @Yr ");
            SQL.AppendLine("And A.DocType = '3' ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("; ");


            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenter));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "Yr", 

                    //1-5
                    "AcNo", 
                    "Amt01",
                    "Amt02",
                    "Amt03",
                    "Amt04",

                    //6-10
                    "Amt05",
                    "Amt06",
                    "Amt07",
                    "Amt08",
                    "Amt09",

                    //11-13
                    "Amt10",
                    "Amt11",
                    "Amt12",

                   
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CBP()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt01 = Sm.DrDec(dr, c[2]),
                            Amt02 = Sm.DrDec(dr, c[3]),
                            Amt03 = Sm.DrDec(dr, c[4]),
                            Amt04 = Sm.DrDec(dr, c[5]),
                            Amt05 = Sm.DrDec(dr, c[6]),
                            Amt06 = Sm.DrDec(dr, c[7]),
                            Amt07 = Sm.DrDec(dr, c[8]),
                            Amt08 = Sm.DrDec(dr, c[9]),
                            Amt09 = Sm.DrDec(dr, c[10]),
                            Amt10 = Sm.DrDec(dr, c[11]),
                            Amt11 = Sm.DrDec(dr, c[12]),
                            Amt12 = Sm.DrDec(dr, c[13]),


                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOACBP(ref List<COACBP> l2, string SelectedCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcDesc, LEVEL, Parent ");
            SQL.AppendLine("FROM TblCOA ");
            SQL.AppendLine("WHERE Find_In_Set(AcNo, @SelectedCOA); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Level", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COACBP()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrDec(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            AmtCBP01 = 0m,
                            AmtJN01 = 0m,
                            AmtCBP02 = 0m,
                            AmtJN02 = 0m,
                            AmtCBP03 = 0m,
                            AmtJN03 = 0m,
                            AmtCBP04 = 0m,
                            AmtJN04 = 0m,
                            AmtCBP05 = 0m,
                            AmtJN05 = 0m,
                            AmtCBP06 = 0m,
                            AmtJN06 = 0m,
                            AmtCBP07 = 0m,
                            AmtJN07 = 0m,
                            AmtCBP08 = 0m,
                            AmtJN08 = 0m,
                            AmtCBP09 = 0m,
                            AmtJN09 = 0m,
                            AmtCBP10 = 0m,
                            AmtJN10 = 0m,
                            AmtCBP11 = 0m,
                            AmtJN11 = 0m,
                            AmtCBP12 = 0m,
                            AmtJN12 = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<CBP> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        //Total RKAP
        private void Process1(ref List<CBP> l, ref List<COACBP> l2, string Yr)
        {
            foreach (var x in l)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtCBP01 += x.Amt01;
                    y.AmtCBP02 += x.Amt02;
                    y.AmtCBP03 += x.Amt03;
                    y.AmtCBP04 += x.Amt04;
                    y.AmtCBP05 += x.Amt05;
                    y.AmtCBP06 += x.Amt06;
                    y.AmtCBP07 += x.Amt07;
                    y.AmtCBP08 += x.Amt08;
                    y.AmtCBP09 += x.Amt09;
                    y.AmtCBP10 += x.Amt10;
                    y.AmtCBP11 += x.Amt11;
                    y.AmtCBP12 += x.Amt12;
                }
            }
        }

        private void PrepCOAJournal(ref List<COAJournal> l3, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcType ");
            SQL.AppendLine("FROM TblCOA  ");
            SQL.AppendLine("WHERE AcNo IN( ");
            SQL.AppendLine("	SELECT DISTINCT B.AcNo ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else
            {
                SQL.AppendLine("	AND LEFT(A.DocDt, 4) = @Yr ");
            }
            SQL.AppendLine(") ");

            SQL.AppendLine("AND ActInd = 'Y' ");
            SQL.AppendLine("AND Parent IS NOT NULL  ");
            SQL.AppendLine("AND AcNo NOT IN ( ");
            SQL.AppendLine("SELECT Parent FROM TblCOA WHERE Parent IS NOT null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DAmt01 = 0m,
                            CAmt01 = 0m,
                            DAmt02 = 0m,
                            CAmt02 = 0m,
                            DAmt03 = 0m,
                            CAmt03 = 0m,
                            DAmt04 = 0m,
                            CAmt04 = 0m,
                            DAmt05 = 0m,
                            CAmt05 = 0m,
                            DAmt06 = 0m,
                            CAmt06 = 0m,
                            DAmt07 = 0m,
                            CAmt07 = 0m,
                            DAmt08 = 0m,
                            CAmt08 = 0m,
                            DAmt09 = 0m,
                            CAmt09 = 0m,
                            DAmt10 = 0m,
                            CAmt10 = 0m,
                            DAmt11 = 0m,
                            CAmt11 = 0m,
                            DAmt12 = 0m,
                            CAmt12 = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepJournalTemp(ref List<JournalTemp> l4, string SelectedCOA, string Yr, string Mth1, string Mth2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("SELECT A.AcNo, SUM(A.DAmt) DAmt, SUM(A.CAmt) CAmt, ");
            if (mIsAccountingRptUseJournalPeriod)
                SQL.AppendLine("Substring(B.Period, 5, 2) As Mth ");
            else
                SQL.AppendLine("Substring(B.DocDt, 5, 2) As Mth ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, @Mth1) ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, @Mth2) ");
            }
            else
            {
                SQL.AppendLine("AND LEFT(B.DocDt, 6) Between CONCAT(@Yr, @Mth1) AND CONCAT(@Yr, @Mth2) ");
            }

            SQL.AppendLine(")T ");
            SQL.AppendLine("GROUP BY T.AcNo, T.Mth; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth1", Mth1);
                Sm.CmParam<String>(ref cm, "@Mth2", Mth2);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "AcNo", 
                    "DAmt", "CAmt", "Mth"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new JournalTemp()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = Sm.DrDec(dr, c[1]),
                            CAmt = Sm.DrDec(dr, c[2]),
                            Mth = Sm.DrStr(dr, c[3]),

                        });
                    }
                }
                dr.Close();
            }
        }

        // ceplokin DAmt & CAmt ke list
        private void Process2(ref List<COAJournal> l3, ref List<JournalTemp> l4)
        {

            foreach (var x in l4)
            {
                foreach (var y in l3.Where(w => w.AcNo == x.AcNo))
                {
                    if (x.Mth == "01")
                    {
                        y.CAmt01 += x.CAmt;
                        y.DAmt01 += x.DAmt;
                    }
                    if (x.Mth == "02")
                    {
                        y.CAmt02 += x.CAmt;
                        y.DAmt02 += x.DAmt;
                    }
                    if (x.Mth == "03")
                    {
                        y.CAmt03 += x.CAmt;
                        y.DAmt03 += x.DAmt;
                    }
                    if (x.Mth == "04")
                    {
                        y.CAmt04 += x.CAmt;
                        y.DAmt04 += x.DAmt;
                    }
                    if (x.Mth == "05")
                    {
                        y.CAmt05 += x.CAmt;
                        y.DAmt05 += x.DAmt;
                    }
                    if (x.Mth == "06")
                    {
                        y.CAmt06 += x.CAmt;
                        y.DAmt06 += x.DAmt;
                    }
                    if (x.Mth == "07")
                    {
                        y.CAmt07 += x.CAmt;
                        y.DAmt07 += x.DAmt;
                    }
                    if (x.Mth == "08")
                    {
                        y.CAmt08 += x.CAmt;
                        y.DAmt08 += x.DAmt;
                    }
                    if (x.Mth == "09")
                    {
                        y.CAmt09 += x.CAmt;
                        y.DAmt09 += x.DAmt;
                    }
                    if (x.Mth == "10")
                    {
                        y.CAmt10 += x.CAmt;
                        y.DAmt10 += x.DAmt;
                    }
                    if (x.Mth == "11")
                    {
                        y.CAmt11 += x.CAmt;
                        y.DAmt11 += x.DAmt;
                    }
                    if (x.Mth == "12")
                    {
                        y.CAmt12 += x.CAmt;
                        y.DAmt12 += x.DAmt;
                    }
                    
                }
            }
        }

        // Total Journal
        private void Process3(ref List<COACBP> l2, ref List<COAJournal> l3)
        {
            foreach (var x in l3)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtJN01 += (x.AcType == "C") ? (x.CAmt01 - x.DAmt01) : (x.DAmt01 - x.CAmt01);
                    y.AmtJN02 += (x.AcType == "C") ? (x.CAmt02 - x.DAmt02) : (x.DAmt02 - x.CAmt02);
                    y.AmtJN03 += (x.AcType == "C") ? (x.CAmt03 - x.DAmt03) : (x.DAmt03 - x.CAmt03);
                    y.AmtJN04 += (x.AcType == "C") ? (x.CAmt04 - x.DAmt04) : (x.DAmt04 - x.CAmt04);
                    y.AmtJN05 += (x.AcType == "C") ? (x.CAmt05 - x.DAmt05) : (x.DAmt05 - x.CAmt05);
                    y.AmtJN06 += (x.AcType == "C") ? (x.CAmt06 - x.DAmt06) : (x.DAmt06 - x.CAmt06);
                    y.AmtJN07 += (x.AcType == "C") ? (x.CAmt07 - x.DAmt07) : (x.DAmt07 - x.CAmt07);
                    y.AmtJN08 += (x.AcType == "C") ? (x.CAmt08 - x.DAmt08) : (x.DAmt08 - x.CAmt08);
                    y.AmtJN09 += (x.AcType == "C") ? (x.CAmt09 - x.DAmt09) : (x.DAmt09 - x.CAmt09);
                    y.AmtJN10 += (x.AcType == "C") ? (x.CAmt10 - x.DAmt10) : (x.DAmt10 - x.CAmt10);
                    y.AmtJN11 += (x.AcType == "C") ? (x.CAmt11 - x.DAmt11) : (x.DAmt11 - x.CAmt11);
                    y.AmtJN12 += (x.AcType == "C") ? (x.CAmt12 - x.DAmt12) : (x.DAmt12 - x.CAmt12);
                }
            }

        }

        // proses masukin amount journal ke parent dari si amount bontotnya
        private void Process4(ref List<COACBP> l2)
        {
            int MaxLvlCOA = Int32.Parse(Sm.GetValue("SELECT MAX(LEVEL) FROM TblCOA"));

            for (int i = MaxLvlCOA; i > 0; i--)
            {
                foreach (var x in l2.Where(w => w.Level == i).OrderBy(o => o.Parent))
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.Parent))
                    {
                        y.AmtJN01 += x.AmtJN01;
                        y.AmtJN02 += x.AmtJN02;
                        y.AmtJN03 += x.AmtJN03;
                        y.AmtJN04 += x.AmtJN04;
                        y.AmtJN05 += x.AmtJN05;
                        y.AmtJN06 += x.AmtJN06;
                        y.AmtJN07 += x.AmtJN07;
                        y.AmtJN08 += x.AmtJN08;
                        y.AmtJN09 += x.AmtJN09;
                        y.AmtJN10 += x.AmtJN10;
                        y.AmtJN11 += x.AmtJN11;
                        y.AmtJN12 += x.AmtJN12;
                      
                    }
                }
            }
        }

        // ceplokin data ke grid
        private void Process5(ref List<COACBP> l2)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l2)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.AmtCBP01, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.AmtJN01, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.AmtCBP02, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.AmtJN02, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = Sm.FormatNum(x.AmtCBP03, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.AmtJN03, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.FormatNum(x.AmtCBP04, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.FormatNum(x.AmtJN04, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = Sm.FormatNum(x.AmtCBP05, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = Sm.FormatNum(x.AmtJN05, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 13].Value = Sm.FormatNum(x.AmtCBP06, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 14].Value = Sm.FormatNum(x.AmtJN06, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 15].Value = Sm.FormatNum(x.AmtCBP07, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 16].Value = Sm.FormatNum(x.AmtJN07, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 17].Value = Sm.FormatNum(x.AmtCBP08, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 18].Value = Sm.FormatNum(x.AmtJN08, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 19].Value = Sm.FormatNum(x.AmtCBP09, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 20].Value = Sm.FormatNum(x.AmtJN09, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 21].Value = Sm.FormatNum(x.AmtCBP10, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 22].Value = Sm.FormatNum(x.AmtJN10, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 23].Value = Sm.FormatNum(x.AmtCBP11, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 24].Value = Sm.FormatNum(x.AmtJN11, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 25].Value = Sm.FormatNum(x.AmtCBP12, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 26].Value = Sm.FormatNum(x.AmtJN12, 0);

                Row += 1;
            }

            Grd1.EndUpdate();
        }

        //masukin index kolom dan bulan ke list MonthCols
        private void Process6(ref List<MonthCols> l5)
        {
            int Cols = 3;
            for (int i = 1; i < 13; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    l5.Add(new MonthCols()
                    {
                        Mth = string.Concat("00", i.ToString()),
                        Cols = Cols
                    });
                    Cols += 1;
                }
            }
        }

         //hide kolom sesuai filter
        private void Process7(ref List<MonthCols> l5, string Mth1, string Mth2)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 }, true);
            foreach (var x in l5.Where(w => Int32.Parse(w.Mth) < Int32.Parse(Mth1) || Int32.Parse(w.Mth) > Int32.Parse(Mth2)))
            {
                Sm.GrdColInvisible(Grd1, new int[] { x.Cols }, false);
            }
        }



        #endregion

        #endregion

        #region Events

        #region Misc Control Event

        private void LueProfitCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenter, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
            SetLueCCCode(ref LueCCCode, Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProfitCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Profit Center");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion

        #region Class

        private class CBP
        {
            public string Yr { get; set; }
            public string AcNo { get; set; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }


        }

        private class COACBP
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public decimal Level { get; set; }

            public decimal AmtCBP01 { get; set; }
            public decimal AmtCBP02 { get; set; }
            public decimal AmtCBP03 { get; set; }
            public decimal AmtCBP04 { get; set; }
            public decimal AmtCBP05 { get; set; }
            public decimal AmtCBP06 { get; set; }
            public decimal AmtCBP07 { get; set; }
            public decimal AmtCBP08 { get; set; }
            public decimal AmtCBP09 { get; set; }
            public decimal AmtCBP10 { get; set; }
            public decimal AmtCBP11 { get; set; }
            public decimal AmtCBP12 { get; set; }

            public decimal AmtJN01 { get; set; }
            public decimal AmtJN02 { get; set; }
            public decimal AmtJN03 { get; set; }
            public decimal AmtJN04 { get; set; }
            public decimal AmtJN05 { get; set; }
            public decimal AmtJN06 { get; set; }
            public decimal AmtJN07 { get; set; }
            public decimal AmtJN08 { get; set; }
            public decimal AmtJN09 { get; set; }
            public decimal AmtJN10 { get; set; }
            public decimal AmtJN11 { get; set; }
            public decimal AmtJN12 { get; set; }
        }

        private class COAJournal
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt01 { get; set; }
            public decimal DAmt02 { get; set; }
            public decimal DAmt03 { get; set; }
            public decimal DAmt04 { get; set; }
            public decimal DAmt05 { get; set; }
            public decimal DAmt06 { get; set; }
            public decimal DAmt07 { get; set; }
            public decimal DAmt08 { get; set; }
            public decimal DAmt09 { get; set; }
            public decimal DAmt10 { get; set; }
            public decimal DAmt11 { get; set; }
            public decimal DAmt12 { get; set; }

            public decimal CAmt01 { get; set; }
            public decimal CAmt02 { get; set; }
            public decimal CAmt03 { get; set; }
            public decimal CAmt04 { get; set; }
            public decimal CAmt05 { get; set; }
            public decimal CAmt06 { get; set; }
            public decimal CAmt07 { get; set; }
            public decimal CAmt08 { get; set; }
            public decimal CAmt09 { get; set; }
            public decimal CAmt10 { get; set; }
            public decimal CAmt11 { get; set; }
            public decimal CAmt12 { get; set; }

        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Mth { get; set; }
        }

        private class MonthCols
        {
            public string Mth { get; set; }
            public int Cols { get; set; }
        }
        #endregion
    }
}
