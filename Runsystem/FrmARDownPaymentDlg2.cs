﻿#region Update
/*
    06/06/2018 [TKG] Tambah filetr hanya nomor rekening coa yg aktif saja yg muncul.
    16/08/2022 [MAU/PRODUCT] Membuat validasi COA berdasarkan apa yang telah terpasang di Group (system tools).

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmARDownPaymentDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmARDownpayment mFrmParent;
        private string mSQL = string.Empty;



        #endregion

        #region Constructor

        public FrmARDownPaymentDlg2(FrmARDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method
        //private void GetParameter()
        //{
            

        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    string ParCode = string.Empty, ParValue = string.Empty;

        //    SQL.AppendLine(" Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");

        //    SQL.AppendLine(" 'IsCOAFilteredByGroup' ");

        //    SQL.AppendLine(" );");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                ParCode = Sm.DrStr(dr, c[0]);
        //                ParValue = Sm.DrStr(dr, c[1]).Trim();
        //                switch (ParCode)
        //                {
        //                    //boolean
        //                    case "IsCOAFilteredByGroup": mFrmParent.mIsCOAFilteredByGroup = ParValue == "Y"; break;


        //                    //string



        //                }
        //            }
        //        }
        //        dr.Close();
        //    }

        //}

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueAcCtCode(ref LueAcCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 300, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcTypeDesc, C.AcCtName ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");

            if (mFrmParent.mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("And Locate(Concat('##', A.AcNo, '##'), @SelectedAcNo)<1 ");

            SQL.AppendLine("And A.ActInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedAcNo", mFrmParent.GetSelectedAcNo());
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "AcTypeDesc", "AcCtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAcNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 2, Grd1, Row2, 3);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, Row1, new int[] { 3, 4 });

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsAcNoAlreadyChosen(int Row)
        {
            string AcNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 1), AcNo)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
