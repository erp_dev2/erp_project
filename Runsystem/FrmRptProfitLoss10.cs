﻿#region Update
/*
    13/12/ [IBL/PHT] new reporting profit loss product based on setting. Based on RptProfitLoss8
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss10 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mAccountingRptStartFrom = string.Empty;
        private bool
            mIsEntityMandatory = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptFinancialNotUseStartFromFilter = false;

        #endregion

        #region Constructor

        public FrmRptProfitLoss10(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueEntCode(ref LueEntCode);
                Sl.SetLueOption(ref LueCCtProductCode, "CCtProduct");
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                SetGrd();
              
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Product Code",
                    "Product", 
                    "Category",
                    "Balance",
                    
                },
                new int[] 
                {
                    //0
                    25,

                    //1-4
                    180, 200, 150, 150
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4});
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month")||
                Sm.IsLueEmpty(LueEntCode, "Entity")
                ) return;
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueStartFrom);
            string SelectedCOA = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<RowData>();
                var l2 = new List<COACCt>();
                var l3 = new List<Product>();
                var l4 = new List<COA4>();

                PrepData(ref l);
                if (l.Count > 0)
                {
                    PrepCOACCt(ref l2);
                    if (l2.Count > 0)
                    {
                        SelectedCOA = GetSelectedCOA(ref l2);
                        GetCOA4(ref l4);

                        //proses nilai balance untuk kepala 4
                        if (l4.Count > 0)
                        {
                            if (StartFrom.Length > 0)
                            {
                                Process1(ref l2, string.Empty, StartFrom, true, ref l4);
                                if (mIsRptAccountingShowJournalMemorial)
                                    Process2_JournalMemorial(ref l2, string.Empty, Yr, Mth, StartFrom, true, ref l4);
                                else
                                    Process2(ref l2, string.Empty, Yr, Mth, StartFrom, true, ref l4);
                            }
                            else
                            {
                                Process1(ref l2, string.Empty, StartFrom, true, ref l4);
                                if (mIsRptAccountingShowJournalMemorial)
                                    Process2_JournalMemorial(ref l2, string.Empty, Yr, Mth, string.Empty, true, ref l4);
                                else
                                    Process2(ref l2, string.Empty, Yr, Mth, string.Empty, true, ref l4);
                            }
                        }

                        if (StartFrom.Length > 0)
                        {
                            Process1(ref l2, SelectedCOA, StartFrom, false, ref l4);
                            Process2(ref l2, SelectedCOA, Yr, Mth, StartFrom, false, ref l4);
                        }
                        else
                        {
                            Process1(ref l2, SelectedCOA, StartFrom, false, ref l4);
                            Process2(ref l2, SelectedCOA, Yr, Mth, string.Empty, false, ref l4);
                        }
                        Process3(ref l2, ref l3);
                        Process4(ref l3, ref l4);
                        Process5(ref l, ref l3);
                    }
                    Process6(ref l);
                    Process7(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] {4});

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        //untuk ngedapetin account number kepala 4
        private void GetCOA4(ref List<COA4> l4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("And AcNo Like '4%' ");
            SQL.AppendLine("And Parent Is not Null ");
            SQL.AppendLine("And AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not null) ");
            SQL.AppendLine("Order By AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new COA4()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = 0m,
                            CAmt = 0m
                        });
                    }
                }
                dr.Close();
            }

        }

        private void GetParameter()
        {
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
           
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y';");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void PrepData(ref List<RowData> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.CCtProductCode, B.OptDesc CCtProductName, ");
            SQL.AppendLine("Case When JoinCostInd = 'Y' Then 'Join Cost' ELSE 'Revenue' End  Category ");
            SQL.AppendLine("FROM TblCostCategory A ");
            SQL.AppendLine("INNER JOIN TblOption B ON A.CCtProductCode = B.OptCode AND optcat = 'CCtProduct' ");
            SQL.AppendLine("WHERE A.CCtProductCode IS NOT NULL  ");
            SQL.AppendLine("AND (A.JoinCostInd != 'N' OR A.RevenueInd != 'N') ");
            if (ChkCCtProductCode.Checked) SQL.AppendLine("AND A.CCtProductCode=@CCtProductCode ");
            SQL.AppendLine("Order BY A.CCtProductCode, Category; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkCCtProductCode.Checked) Sm.CmParam<String>(ref cm, "@CCtProductCode", Sm.GetLue(LueCCtProductCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCtProductCode", "CCtProductName", "Category" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RowData()
                        {
                            CCtProductCode = Sm.DrStr(dr, c[0]),
                            CCtProductName = Sm.DrStr(dr, c[1]),
                            Category = Sm.DrStr(dr, c[2]),
                            Balance = 0m,
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOACCt(ref List<COACCt> l2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT Distinct A.AcNo ");
            SQL.AppendLine("FROM TblCostCategory A  ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.AcNo ");
            SQL.AppendLine("    From TblProfitLossSettingHdr T1 ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl T2 On T1.SettingCode = T2.SettingCode ");
            SQL.AppendLine("    Where T1.Yr = @Yr And T1.ActInd = 'Y' And T1.ProfitLossInd = 'Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.AcNo ");
            SQL.AppendLine("    From TblProfitLossSettingHdr T1 ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl2 T2 On T1.SettingCode = T2.SettingCode ");
            SQL.AppendLine("    Where T1.Yr = @Yr And T1.ActInd = 'Y' And T1.ProfitLossInd = 'Y' ");
            SQL.AppendLine(")C On A.AcNo = C.AcNo ");
            SQL.AppendLine("INNER JOIN TblCOA B ON C.AcNo = B.AcNo And B.ActInd = 'Y' ");
            SQL.AppendLine("WHERE A.CCtProductCode IS NOT NULL   ");
            if (ChkCCtProductCode.Checked) SQL.AppendLine("AND A.CCtProductCode=@CCtProductCode ");
            SQL.AppendLine("AND (A.JoinCostInd != 'N' OR A.RevenueInd != 'N')  ");
            SQL.AppendLine("ORDER BY A.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString(); 
                if (ChkCCtProductCode.Checked) Sm.CmParam<String>(ref cm, "@CCtProductCode", Sm.GetLue(LueCCtProductCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COACCt()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = 0m,
                            CAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<COACCt> l2)
        {
            string AcNo = string.Empty;

            foreach (var x in l2)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        // Opening Balance
        private void Process1(ref List<COACCt> l2, string SelectedCOA, string Yr, bool IsPercentage, ref List<COA4> l4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var lJournal = new List<Journal>();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            if (IsPercentage)//dapetin debit/credit kepala 4
                SQL.AppendLine("    And C.AcNo Like '4%' ");
            else
                SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            if (mIsEntityMandatory && ((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")))
            {
              SQL.AppendLine("    And IfNull(A.EntCode, '') = IfNull(@EntCode, '') ");
            }
            SQL.AppendLine("Order By B.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    if (IsPercentage)
                    {
                        for (var j = Temp; j < l4.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l4[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l4[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l4[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l4[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l4[j].DAmt += lJournal[i].DAmt;
                                l4[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l4[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                    else
                    {
                        for (var j = Temp; j < l2.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l2[j].DAmt += lJournal[i].DAmt;
                                l2[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        // Journal 
        private void Process2(ref List<COACCt> l2, string SelectedCOA, string Yr, string Mth, string StartFrom, bool IsPercentage, ref List<COA4> l4)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsEntityMandatory && ((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")))
                SQL.AppendLine("    And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            if (IsPercentage) //dapetin debit/credit kepala 4
                SQL.AppendLine("    And C.AcNo Like '4%' ");
            else
                SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.Period, 6)<=@YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    if (IsPercentage)
                    {
                        for (var j = Temp; j < l4.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l4[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l4[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l4[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l4[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l4[j].DAmt += lJournal[i].DAmt;
                                l4[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l4[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                    else
                    {
                        for (var j = Temp; j < l2.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l2[j].DAmt += lJournal[i].DAmt;
                                l2[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process2_JournalMemorial(ref List<COACCt> l2, string SelectedCOA, string Yr, string Mth, string StartFrom, bool IsPercentage, ref List<COA4> l4)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From (");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsEntityMandatory && ((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")))
                SQL.AppendLine("    And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            if (IsPercentage) //dapetin debit/credit kepala 4
                SQL.AppendLine("    And C.AcNo Like '4%' ");
            else
                SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.Period, 6)<=@YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (mIsEntityMandatory && ((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")))
                SQL.AppendLine("    And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            if (IsPercentage) //dapetin debit/credit kepala 4
                SQL.AppendLine("    And C.AcNo Like '4%' ");
            else
                SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("And A.Period Is Not Null ");
                    SQL.AppendLine("And Left(A.Period, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.Period, 6)<=@YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    if (IsPercentage)
                    {
                        for (var j = Temp; j < l4.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l4[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l4[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l4[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l4[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l4[j].DAmt += lJournal[i].DAmt;
                                l4[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l4[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                    else
                    {
                        for (var j = Temp; j < l2.Count; j++)
                        {
                            if (
                                lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                                lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                                )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                l2[j].DAmt += lJournal[i].DAmt;
                                l2[j].CAmt += lJournal[i].CAmt;
                                if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        // ceplokin Debit dan Credit ke Product
        private void Process3(ref List<COACCt> l2, ref List<Product> l3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.CCtProductCode, A.AcNo, ");
            SQL.AppendLine("Case When JoinCostInd = 'Y' Then 'Join Cost' ELSE 'Revenue' End  Category  ");
            SQL.AppendLine("FROM TblCostCategory A  ");
            SQL.AppendLine("INNER JOIN TblOption B ON A.CCtProductCode = B.OptCode AND optcat = 'CCtProduct'  ");
            SQL.AppendLine("WHERE A.CCtProductCode IS NOT NULL   ");
            SQL.AppendLine("AND (A.JoinCostInd != 'N' OR A.RevenueInd != 'N') ");
            if (ChkCCtProductCode.Checked) SQL.AppendLine("AND A.CCtProductCode=@CCtProductCode ");
            SQL.AppendLine("Order BY A.CCtProductCode, Category; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkCCtProductCode.Checked) Sm.CmParam<String>(ref cm, "@CCtProductCode", Sm.GetLue(LueCCtProductCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCtProductCode", "AcNo", "Category" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new Product()
                        {
                            CCtProductCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Category = Sm.DrStr(dr, c[2]),
                            DAmt = 0m,
                            CAmt = 0m,

                        });
                    }
                }
                dr.Close();
            }
            if (l3.Count > 0 && l2.Count > 0)
            {
                l3.OrderBy(x => x.AcNo);
                for (var i = 0; i < l2.Count; i++)
                {
                    for (var j = 0; j < l3.Count; j++)
                    {
                        if (Sm.CompareStr(l3[j].AcNo, l2[i].AcNo))
                        {
                            l3[j].DAmt = l2[i].DAmt;
                            l3[j].CAmt = l2[i].CAmt;
                        }
                    }
                   
                }
            } 
        }

        // proses nilai debit credit dengan category join cost
        private void Process4(ref List<Product> l3, ref List<COA4> l4)
        {
            decimal TotalDAmt4 = 0m, TotalCAmt4 = 0m;

            foreach(var y in l4)
            {
                //total nilai D/C kepala 4 dari COA
                TotalDAmt4 += y.DAmt;
                TotalCAmt4 += y.CAmt;
            }

            foreach(var x in l3.Where(w => w.Category != "Revenue"))
            {
                //nilai Debit = DebitAmt * (DebitAmt/Total Debit Kepala 4)
                //nilai Credit = CreditAmt * (CreditAmt/Total Credit Kepala 4)
                if (TotalDAmt4 == 0m) x.DAmt = 0m; else x.DAmt = x.DAmt * (x.DAmt / TotalDAmt4);
                if (TotalCAmt4 == 0m) x.CAmt = 0m; else x.CAmt = x.CAmt * (x.CAmt / TotalCAmt4);
            }
        }

        // ceplokin nilai Balance ke Row Data (dari Product)
        private void Process5(ref List<RowData> l, ref List<Product> l3)
        {
            foreach (var x in l)
            {
                foreach (var y in l3.Where(w => w.CCtProductCode == x.CCtProductCode && w.Category == x.Category))
                {
                    x.Balance += (y.Category == "Revenue") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                }
            }
        }

        // ceplokin data ke grid
        private void Process6(ref List<RowData> l)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.CCtProductCode;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.CCtProductName;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.Category;
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.Balance, 0);
               
                Row += 1;
            }

            Grd1.EndUpdate();
        }

        private void Process7(ref List<RowData> l)
        {
            decimal RevenueAmt = 0m, JoinCostAmt = 0m, ProfitLossAmt = 0m;

            foreach (var x in l)
            {
                if (x.Category == "Revenue")
                    RevenueAmt += x.Balance;
                if (x.Category == "Join Cost")
                    JoinCostAmt += x.Balance;
            }
            ProfitLossAmt = RevenueAmt - JoinCostAmt;
            TxtProfitLoss.EditValue = Sm.FormatNum(ProfitLossAmt, 0);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
        }

        private void LueCCtProductCode_EditValueChanged(object sender, EventArgs e)
        {
           Sm.RefreshLookUpEdit(LueCCtProductCode, new Sm.RefreshLue2(Sl.SetLueOption), "CCtProduct");
           Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCtProductCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Product");
        }

        #endregion

        #endregion

        #region Class

        private class RowData
        {
            public string CCtProductCode { get; set; }
            public string CCtProductName { get; set; }
            public string Category { get; set; }
            public decimal Balance { get; set; }
           
        }

        private class COACCt
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COA4
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Product
        {
            public string CCtProductCode { get; set; }
            public string AcNo { get; set; }
            public string Category { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion
    }
}
