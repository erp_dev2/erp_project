﻿#region Update
/*
    22/08/2017 [TKG] menggunakan tax group
    02/09/2017 [TKG] bug fixing saat menampilkan memilih data purchase invoice
    03/09/2017 [TKG] tambah informasi outgoing payment dan date, incoming payment# dan date
    03/09/2017 [TKG] vr tax hanya yg sudah divoucherkan
    12/11/2017 [TKG] VR Tax bisa in atau out berdasarkan debit/creditnya.
    28/11/2017 [TKG] perubahan perhitungan vr tax
    13/05/2019 [TKG] tambah PI raw material
    06/05/2022 [TYO/ALL] filter Tax Invoice# sesuai dengan tax groupnya
    07/06/2022 [DITA/IOK] tambah taxcode PI untuk keperluan jika ada update tax di PI 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVATSettlementDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVATSettlement mFrmParent;
        private string mCurCode = string.Empty, mPPNTaxCode = string.Empty, mDocType = string.Empty;
        private bool mIsPIRawMaterialShown = false;

        #endregion

        #region Constructor

        public FrmVATSettlementDlg(FrmVATSettlement FrmParent, string DocType, string CurCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
            mCurCode = CurCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Document";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                mIsPIRawMaterialShown = Sm.CompareStr(Sm.GetLue(mFrmParent.LueTaxGrpCode), mFrmParent.mPurchaseInvoiceRawMaterialTaxGrpCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter, string Filter2, string Filter3, string Filter4, string Filter5, string Filter6, string Filter7, string Filter8)
        {
            var SQL = new StringBuilder();

            if (mDocType == "I")
            {
                SQL.AppendLine("Select '1' As DocType, 'Purchase Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("E.VdName As BusinessPartner, F.PODocNo As DocumentNo, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
                SQL.AppendLine("End As TaxInvoiceNo, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt3 ");
                SQL.AppendLine("End As TaxInvoiceDt, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode1 ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode3 ");
                SQL.AppendLine("End As TaxCode, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then B.TaxRate*A.Amt*0.01 ");
                SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then C.TaxRate*A.Amt*0.01 ");
                SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then D.TaxRate*A.Amt*0.01 ");
                SQL.AppendLine("Else 0.00 End As Amt, Null As Remark ");
                SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("Left Join TblTax B On A.TaxCode1=B.TaxCode ");
                SQL.AppendLine("Left Join TblTax C On A.TaxCode2=C.TaxCode ");
                SQL.AppendLine("Left Join TblTax D On A.TaxCode3=D.TaxCode ");
                SQL.AppendLine("Inner Join TblVendor E On A.VdCode=E.VdCode ");
                SQL.AppendLine("Left Join ( ");

                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.PODocNo, ");
                SQL.AppendLine("    Case When Tbl2.OP Is Null Then '' Else Concat(', ', Tbl2.OP) End ");
                SQL.AppendLine("    ) As PODocNo From ( ");

                SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T3.PODocNo Order By T3.PODocNo ASC Separator ', ') As PODocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
                SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.CurCode = @CurCode ");
                SQL.AppendLine("And ( ");

                SQL.AppendLine("(T1.TaxCode1 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(T1.TaxCode2 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo2 Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(T1.TaxCode3 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo3 Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine(") ");
                SQL.AppendLine("    Group By T1.DocNo ");

                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As OP ");
                SQL.AppendLine("        From TblOutgoingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
                SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                
                SQL.AppendLine(") F On A.DocNo=F.DocNo ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.CurCode = @CurCode ");
                SQL.AppendLine("And ( ");

                SQL.AppendLine("(A.TaxCode1 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(A.TaxCode2 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo2 Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(A.TaxCode3 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo3 Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter2);

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '2' As DocType, 'Purchase Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.VdName As BusinessPartner, C.PODocNo As DocumentNo,  ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
                SQL.AppendLine("End As TaxInvoiceNo, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt3 ");
                SQL.AppendLine("End As TaxInvoiceDt, ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode1 ");
                SQL.AppendLine("    When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode2 ");
                SQL.AppendLine("    When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode3 ");
                SQL.AppendLine("End As TaxCode, ");
                SQL.AppendLine("Case When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt1 ");
                SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt2 ");
                SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt3 ");
                SQL.AppendLine("Else 0.00 End As Amt, Null As Remark ");
                SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
                SQL.AppendLine("Left Join ( ");

                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.PODocNo, ");
                SQL.AppendLine("    Case When Tbl2.OP Is Null Then '' Else Concat(', ', Tbl2.OP) End ");
                SQL.AppendLine("    ) As PODocNo From ( ");

                SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T4.PODocNo Order By T4.PODocNo ASC Separator ', ') As PODocNo ");
                SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblDOVdDtl T3 On T2.DOVdDocNo=T3.DocNo And T2.DOVdDNo=T3.DNo ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T4 On T3.RecvVdDocNo=T4.DocNo And T3.RecvVdDNo=T4.DNo ");
                SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.CurCode = @CurCode ");
                SQL.AppendLine("And ( ");

                SQL.AppendLine("(T1.TaxCode1 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(T1.TaxCode2 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo2 Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(T1.TaxCode3 Is Not Null ");
                SQL.AppendLine("And T1.VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And T1.VATSettlementDocNo3 Is Null ");
                SQL.AppendLine("And IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine(") ");
                SQL.AppendLine("    Group By T1.DocNo ");

                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As OP ");
                SQL.AppendLine("        From TblOutgoingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='2' ");
                SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");

                SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.CurCode=@CurCode ");
                SQL.AppendLine("And ( ");

                SQL.AppendLine("(A.TaxCode1 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(A.TaxCode2 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo2 Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine("Or ");
                SQL.AppendLine("(A.TaxCode3 Is Not Null ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo3 Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine(") ");

                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter3);

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '3' As DocType, 'Voucher Request Tax' As DocTypeDesc, DocNo, DocDt, ");
                SQL.AppendLine("Null As BusinessPartner, Null As DocumentNo, TaxInvNo, TaxInvDt, A.TaxCode,  ");
                SQL.AppendLine("Case When IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("Then Case When AcType='D' Then -1.00 Else 1.00 End*Amt ");
                SQL.AppendLine("Else 0.00 End As Amt, ");
                SQL.AppendLine("Null As Remark ");
                SQL.AppendLine("From TblVoucherRequestTax A ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblVoucherHdr ");
                SQL.AppendLine("    Where DocType='21' ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine("    And VoucherRequestDocNo=A.VoucherRequestDocNo ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And CurCode=@CurCode ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                if (ChkDocNo.Checked) SQL.AppendLine(" And Upper(A.DocNo) Like @DocNo1 ");
                SQL.AppendLine(Filter4);

                if (mIsPIRawMaterialShown)
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select '7' As DocType, 'Purchase Invoice Raw Material' As DocTypeDesc, A.DocNo, A.DocDt, ");
                    SQL.AppendLine("D.VdName As BusinessPartner, Null As DocumentNo, Null As TaxInvoiceNo, Null As TaxInvoiceDt,Null As TaxCode, ");
                    SQL.AppendLine("A.Tax As Amt, Null As Remark ");
                    SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
                    SQL.AppendLine("Inner Join TblRawMaterialVerify B On A.RawMaterialVerifyDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblLegalDocVerifyHdr C On B.LegalDocVerifyDocNo=C.DocNo ");
                    SQL.AppendLine("Inner Join TblVendor D On C.VdCode=D.VdCode ");
                    SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                    SQL.AppendLine("And A.CancelInd='N' ");
                    SQL.AppendLine("And A.TaxInd='Y' ");
                    SQL.AppendLine("And A.CurCode=@CurCode ");
                    SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                    SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                    if (ChkDocNo.Checked) SQL.AppendLine(" And Upper(A.DocNo) Like @DocNo1 ");
                    SQL.AppendLine(Filter8);
                }
            }

            if (mDocType == "O")
            {
                SQL.AppendLine("Select '4' As DocType, 'Sales Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName As BusinessPartner, C.SODocNo As DocumentNo, A.TaxInvDocument As TaxInvoiceNo, A.DocDt As TaxInvoiceDt, Null As TaxCode, ");
                SQL.AppendLine("A.TotalTax As Amt, Null As Remark ");
                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join ( ");

                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                SQL.AppendLine("    ) As SODocNo From ( ");

                SQL.AppendLine("    Select T.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                SQL.AppendLine("        Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And T1.CurCode = @CurCode ");
                SQL.AppendLine("        And T1.TotalTax<>0 ");
                SQL.AppendLine("        And T1.SODocNo Is Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                SQL.AppendLine("        Inner Join TblPLDtl T5 On T3.PLDocNo=T5.DocNo And T4.PLDNo=T5.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And T1.CurCode = @CurCode ");
                SQL.AppendLine("        And T1.TotalTax<>0 ");
                SQL.AppendLine("        And T1.SODocNo Is Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct DocNo, SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr ");
                SQL.AppendLine("        Where (DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("        And VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And CurCode = @CurCode ");
                SQL.AppendLine("        And TotalTax<>0 ");
                SQL.AppendLine("        And SODocNo Is Not Null ");
                SQL.AppendLine("    ) T Group By T.DocNo ");
                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And A.CurCode = @CurCode ");
                SQL.AppendLine("And A.TotalTax<>0 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter5);

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '5' As DocType, 'Sales Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName As BusinessPartner, C.SODocNo As DocumentNo, A.TaxInvDocument As TaxInvoiceNo, A.TaxInvoiceDt, Null As TaxCode,  ");
                SQL.AppendLine("A.TaxAmt As Amt, Null As Remark ");
                SQL.AppendLine("From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join ( ");

                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                SQL.AppendLine("    ) As SODocNo From ( ");

                SQL.AppendLine("    Select T.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                SQL.AppendLine("        Inner Join TblDRDtl T6 On T4.DRDocNo=T6.DocNo And T5.DRDNo=T6.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("        And T1.CurCode=@CurCode ");
                SQL.AppendLine("        And T1.TaxAmt<>0 ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                SQL.AppendLine("        Inner Join TblPLDtl T6 On T4.PLDocNo=T6.DocNo And T5.PLDNo=T6.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And T1.CurCode=@CurCode ");
                SQL.AppendLine("        And T1.TaxAmt<>0 ");
                SQL.AppendLine("    ) T Group By T.DocNo ");

                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='2' ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");

                SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And A.CurCode=@CurCode ");
                SQL.AppendLine("And A.TaxAmt<>0 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter6);

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '6' As DocType, 'Point Of Sale' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName As BusinessPartner, Null As DocumentNo, A.TaxInvoiceNo, A.TaxInvoiceDt, Null As TaxCode,  ");
                SQL.AppendLine("A.Amt, ");
                SQL.AppendLine("Concat(IfNull(C.OptDesc, ''), ' (', ");
                SQL.AppendLine("    Concat( ");
                SQL.AppendLine("        Case When E.BankName Is Not Null Then Concat(E.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("        Case When D.BankAcNo Is Not Null  ");
                SQL.AppendLine("        Then Concat(D.BankAcNo, ' [', IfNull(D.BankAcNm, ''), ']') ");
                SQL.AppendLine("        Else IfNull(D.BankAcNm, '') End ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(", ')') As Remark ");
                SQL.AppendLine("From TblPosVat A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
                SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
                SQL.AppendLine("Left Join TblBank E On D.BankCode=E.BankCode ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And A.CurCode=@CurCode ");
                SQL.AppendLine("And A.Amt<>0 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter7);
            }
            SQL.AppendLine(" Order By DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Type",
                        "Type",
                        "Document#", 
                        "Date",

                        //6-10
                        "Vendor/Customer",
                        "PO/Outgoing Payment/"+Environment.NewLine+"SO/Incoming Payment",
                        "Tax Invoice#",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "Amount",

                        //11-12
                        "Remark", 
                        "TaxCode"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 180, 130, 80,

                        //6-10
                        180, 400, 130, 100, 120,

                        //11-12
                        300, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", 
                    Filter2 = string.Empty, 
                    Filter3 = string.Empty, 
                    Filter4 = string.Empty,
                    Filter5 = string.Empty, 
                    Filter6 = string.Empty, 
                    Filter7 = string.Empty,
                    Filter8 = string.Empty;

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    string DocType = string.Empty, DocNo = string.Empty;

                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocType = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 4);
                        if (DocNo.Length != 0)
                        {
                            if (mDocType=="I")
                            {
                                if (DocType == "1")
                                {
                                    if (Filter2.Length > 0) Filter2 += " And ";
                                    Filter2 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                                if (DocType == "2")
                                {
                                    if (Filter3.Length > 0) Filter3 += " And ";
                                    Filter3 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                                if (DocType == "3")
                                {
                                    if (Filter4.Length > 0) Filter4 += " And ";
                                    Filter4 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                                if (DocType == "7")
                                {
                                    if (Filter8.Length > 0) Filter8 += " And ";
                                    Filter8 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                            }

                            if (mDocType == "O")
                            {
                                if (DocType == "4")
                                {
                                    if (Filter5.Length > 0) Filter5 += " And ";
                                    Filter5 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                                if (DocType == "5")
                                {
                                    if (Filter6.Length > 0) Filter6 += " And ";
                                    Filter6 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                                if (DocType == "6")
                                {
                                    if (Filter7.Length > 0) Filter7 += " And ";
                                    Filter7 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                }
                            }
                        }
                    }
                }

                if (mDocType == "I")
                {
                    if (Filter2.Length > 0) Filter2 = " And ( " + Filter2 + ") ";
                    if (Filter3.Length > 0) Filter3 = " And ( " + Filter3 + ") ";
                    if (Filter4.Length > 0) Filter4 = " And ( " + Filter4 + ") ";
                    if (Filter8.Length > 0) Filter8 = " And ( " + Filter8 + ") ";
                }

                if (mDocType == "O")
                {
                    if (Filter5.Length > 0) Filter5 = " And ( " + Filter5 + ") ";
                    if (Filter6.Length > 0) Filter6 = " And ( " + Filter6 + ") ";
                    if (Filter7.Length > 0) Filter7 = " And ( " + Filter7 + ") ";
                }

                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(mFrmParent.LueCurCode));
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(mFrmParent.LueTaxGrpCode));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter, Filter2, Filter3, Filter4, Filter5, Filter6, Filter7, Filter8),
                        new string[] 
                        { 
                            //0
                            "DocType",
                            
                            //1-5
                            "DocTypeDesc", "DocNo", "DocDt", "BusinessPartner", "DocumentNo", 
                            
                            //6-10
                            "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "Remark", "TaxCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        mFrmParent.Grd1.Cells[Row1, 6].Value = Sm.GetGrdStr(Grd1, Row2, 6) + " - " + Sm.GetGrdStr(Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9 });
                        mFrmParent.ComputeAmt();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string
                DocType = Sm.GetGrdStr(Grd1, Row, 2),
                DocNo = Sm.GetGrdStr(Grd1, Row, 4);

            for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 2), DocType) && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 4), DocNo)) return true;

            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Method

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
