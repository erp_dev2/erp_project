﻿#region Update
/*
    17/11/2020 [TKG/PHT] bug saat save (parameter @EntCode)
    12/01/2021 [IBL/PHT] menambah inputan COA Inter Office berdasarkan parameter IsProfitCenterUseInterOffice
    04/03/2021 [HAR/PHT] menambah validasi saat generate proficenter
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    01/03/2022 [TKG/PHT] ubah GetParameter()
    02/03/2022 [TKG/PHT] tambah level
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProfitCenter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mProfitCenterCode = string.Empty;//if this application is called from other application
        internal FrmProfitCenterFind FrmFind;
        internal bool
            mIsProfitCenterUseInterOffice = false,
            mIsCOAUseAlias = false,
            mIsCostCategoryFilterByEntity = false,
            mIsOneAccountForOneCostCategory = false,
            mIsProfitCenterLevelEnabled = false;

        #endregion

        #region Constructor

        public FrmProfitCenter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Profit Center";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetFormControl(mState.View);

            Sl.SetLueProfitCenterCode(ref LueParent);
            Sl.SetLueEntCode(ref LueEntCode);

            //if this application is called from other application
            if (mProfitCenterCode.Length != 0)
            {
                ShowData(mProfitCenterCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }

            LblAcNo.Visible = TxtAcNo.Visible = TxtAcDesc.Visible = BtnAcNo.Visible = mIsProfitCenterUseInterOffice;
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProfitCenterCode,TxtProfitCenterName, LueParent, LueEntCode,
                        TxtAcNo, TxtAcDesc, TxtLevel
                    }, true);
                    BtnAcNo.Enabled = false;
                    TxtProfitCenterCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtProfitCenterName, LueParent, LueEntCode }, false);
                    if (mIsProfitCenterLevelEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLevel }, false);
                    BtnAcNo.Enabled = true;
                    TxtProfitCenterName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtProfitCenterCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtProfitCenterName, LueParent }, false);
                    if (mIsProfitCenterLevelEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLevel }, false);
                    BtnAcNo.Enabled = true;
                    TxtProfitCenterName.Focus();
                    break;
                default:
                    break;
            }
        }
        #endregion

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProfitCenterCode, TxtProfitCenterName, LueParent, LueEntCode, TxtAcNo,
                TxtAcDesc, TxtLevel
            });
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProfitCenterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProfitCenterCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblProfitCenter Where ProfitCenterCode=@ProfitCenterCode And ProfitCenterName=@ProfitCenterName And Parent=@Parent " };
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", TxtProfitCenterCode.Text);
                Sm.CmParam<String>(ref cm, "@ProfitCenterName", TxtProfitCenterName.Text);
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                if (TxtProfitCenterCode.Text.Length == 0)
                    TxtProfitCenterCode.EditValue = GenerateProfitCenterCode();

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblProfitCenter(ProfitCenterCode, ProfitCenterName, Parent, EntCode, AcNo, ");
                if (mIsProfitCenterLevelEnabled) SQL.AppendLine("Level, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ProfitCenterCode, @ProfitCenterName, @Parent, @EntCode, @AcNo, ");
                if (mIsProfitCenterLevelEnabled) SQL.AppendLine("@Level, ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update ProfitCenterName=@ProfitCenterName,Parent=@Parent, EntCode=@EntCode, AcNo=@AcNo, ");
                if (mIsProfitCenterLevelEnabled) SQL.AppendLine("    Level=@Level, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", TxtProfitCenterCode.Text);
                Sm.CmParam<String>(ref cm, "@ProfitCenterName", TxtProfitCenterName.Text);
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                if (mIsProfitCenterLevelEnabled) Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtProfitCenterCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProfitCenterCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.ProfitCenterCode, A.ProfitCenterName, A.Parent, A.EntCode, A.AcNo, ");
                if (mIsCOAUseAlias)
                    SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc, ");
                else
                    SQL.AppendLine("B.AcDesc, ");
                if (mIsProfitCenterLevelEnabled)
                    SQL.AppendLine("A.Level ");
                else
                    SQL.AppendLine("Null As Level ");
                SQL.AppendLine("From TblProfitCenter A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Where A.ProfitCenterCode = @ProfitCenterCode");

                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);

                Sm.ShowDataInCtrl(
                        ref cm,SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "ProfitCenterCode",
                            
                            //1-5
                            "ProfitCenterName", "Parent", "EntCode", "AcNo", "AcDesc",

                            //6
                            "Level"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtProfitCenterCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtProfitCenterName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[3]));
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[5]);
                            TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 11);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProfitCenterName, "Profit Center Name", false) ||
                Sm.IsLueEmpty(LueEntCode, "Entity");
        }

        private string GenerateProfitCenterCode()
        {
            var SQL = new StringBuilder();
            int lenPCCode = 5;

            var MaxPCCode = Sm.GetValue("SELECT MAX(LENGTH(ProfitcenterCode)) FROM tblProfitcenter");
            if (MaxPCCode.Length > 0)
                lenPCCode = Convert.ToInt32(MaxPCCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            //SQL.AppendLine("    Select Convert(ProfitCenterCode, Decimal)+1 As Value ");
            SQL.AppendLine("    Select Cast(IfNull(Max(ProfitCenterCode), '00000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    WHERE LENGTH(ProfitcenterCode) = "+lenPCCode+" ");
            SQL.AppendLine("    Order By ProfitCenterCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), " + lenPCCode + ") As ProfitCenterCode");

            return Sm.GetValue(SQL.ToString());

            //return Sm.GetValue(
            //    "Select Right(Concat('00000', ProfitCenterCodeMax) , 5) As ProfitCenterCodeTemp " +
            //    "From (Select Max(Cast(Trim(ProfitCenterCode) As Decimal))+1 ProfitCenterCodeMax From TblProfitCenter) T;"
            //    );

        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsProfitCenterUseInterOffice', 'IsCOAUseAlias', 'IsCostCategoryFilterByEntity', 'IsOneAccountForOneCostCategory', 'IsProfitCenterLevelEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsProfitCenterUseInterOffice": mIsProfitCenterUseInterOffice = ParValue == "Y"; break;
                            case "IsCOAUseAlias": mIsCOAUseAlias = ParValue == "Y"; break;
                            case "IsCostCategoryFilterByEntity": mIsCostCategoryFilterByEntity = ParValue == "Y"; break;
                            case "IsOneAccountForOneCostCategory": mIsOneAccountForOneCostCategory = ParValue == "Y"; break;
                            case "IsProfitCenterLevelEnabled": mIsProfitCenterLevelEnabled = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
        }

        private void LueEntity_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProfitCenterDlg(this));
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtLevel, 11);
        }

        #endregion

        #endregion
    }
}
