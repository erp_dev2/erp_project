﻿#region Update
/*
    01/08/2017 [ARI] tambah kolom Location, filter Location
    14/08/2017 [Ari] tambah Otorisasi di filter location Saat Find TO yang tampil hanya sesuai setingan pada Site Group
    31/01/2018 [WED] rubah nama kolom Description -> Asset Display Name; Asset Name -> Description
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmTOFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmTO mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmTOFind(FrmTO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
                SetGrd();
                SetSQL();
                SetLueLocCode(ref LueLocCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, B.Assetname, B.DisplayName, C.AssetCategoryname, A.StartDt, A.EndDt, A.ActInd, ");
            SQL.AppendLine("D.OptDesc As StatusTO, E.LocName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt From tblTOHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("Left Join TblAssetCategory C On B.AssetCategoryCode = C.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblOption D On A.Status = D.OptCode And D.OptCat = 'MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Where 0=0 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=E.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset Code", 
                        "Description",
                        "Status",
                        "Active",
                        "Location",
                        
                        //6-10
                        "Asset Display Name",
                        "Category",
                        "Valid From",
                        "Valid To",
                        "Created"+Environment.NewLine+"By",
                       
                        //11-15
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 180, 120, 80, 150, 
                        
                        //6-10
                        250, 150, 150, 150, 100,    
                        
                        //11-15
                        120, 100, 100, 120, 100

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] {8,9,11,14});
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 7, 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 7, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "B.Assetname", "B.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "A.LocCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "AssetCode",

                            //1-5
                            "Assetname", "StatusTO", "ActInd", "LocName", "DisplayName", 

                            //6-10
                            "AssetCategoryname", "StartDt", "EndDt", "CreateBy", "CreateDt",  

                           //11-12
                            "LastUpBy", "LastUpDt"                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional Method

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }
      
        #endregion

    }
}
