﻿#region Update
/*
    18/05/2019 [TKG] memperpanjang DNo karena tidak cukup
    20/06/2019 [TKG] panjang local document tambah menjadi 80 karakter
    07/08/2019 [WED] tidak meng update data SO Contract asli nya
    08/05/2020 [DITA/YK] ubah amount ke amount2 dan sebaliknya, tambah amt2 -> contract amount
    11/05/2020 [IBL/YK] tambah addendum date di header
    03/06/2020 [VIN/YK] tambah remark bisa di edit
    05/06/2020 [WED/YK] amt2 --> item tanpa pajak, amt = item + pajak
    23/07/2020 [VIN/YK] show data SO Contract saat insert ambil dari SO contract revision yang terakhir  
    24/08/2020 [VIN/YK] Delivery Date dapat diedit dan meng-update SOC detail
    27/04/2022 [TRI/YK] angka contract amount dibulatkan cth 123,5 -> 124,00
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmSOContractRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
           mCity = string.Empty, mCnt = string.Empty, mSADNo = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated =false;
        private bool IsInsert = false;
        internal string mIsSoUseDefaultPrintout;
        internal string mGenerateCustomerCOAFormat = string.Empty;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmSOContractRevisionFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContractRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmSOContractRevision");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfSalesUomCode();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueStatus(ref SOCLueStatus);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {
            Grd3.Cols.Count = 30;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Agent Code",
                    "Agent",
                    "Item's"+Environment.NewLine+"Code",
                    "",

                    //6-10
                    "Item's Name",
                    "Packaging",
                    "Quantity"+Environment.NewLine+"Packaging",
                    "Quantity",
                    "UoM",

                    //11-15
                    "Price"+Environment.NewLine+"(Price List)",
                    "Discount"+Environment.NewLine+"%",
                    "Discount"+Environment.NewLine+"Amount",
                    "Price After"+Environment.NewLine+"Discount",                        
                    "Promo"+Environment.NewLine+"%",

                    //16-20
                    "Price"+Environment.NewLine+"Before Tax",
                    "Tax"+Environment.NewLine+"%",
                    "Tax"+Environment.NewLine+"Amount",
                    "Price"+Environment.NewLine+"After Tax",                        
                    "Total",

                    //21-25
                    "Delivery"+Environment.NewLine+"Date",
                    "Remark",
                    "CtQtDNo",
                    "Specification",
                    "Customer's"+Environment.NewLine+"Item Code",

                    //26-29
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Volume",
                    "Total Volume",
                    "Uom"+Environment.NewLine+"Volume"
                },
                new int[] 
                {
                    //0
                    40,

                    //1-5
                    20, 0, 180, 80, 20, 
                    
                    //6-10
                    200, 100, 100, 80, 80, 
                    
                    //11-15
                    100, 80, 100, 100, 80,
                    
                    //16-20
                    100, 80, 100, 100, 120,   
                    
                    //21-25
                    100, 400, 0, 120, 120,

                    //26-29
                    250, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Grd3.Cols[24].Move(7);
            Grd3.Cols[26].Move(7);
            Grd3.Cols[25].Move(7);
            Grd3.Cols[27].Move(14);
            Grd3.Cols[28].Move(15);
            Grd3.Cols[29].Move(16);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 17, 18, 23, 24, 25, 27, 28, 29 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd3, new int[] { 26 });

            #region Grid 1

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            #endregion

           
            Grd2.Cols.Count = 11;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "BOQ Document", 
                        
                        //1-5
                        "BOQ Date",
                        "LOP Document",
                        "Project",
                        "BOQ Item Code",
                        "BOQ Item Name",
                        //6-10
                        "BOQ Amount",
                        "Amount",
                        "Allow",
                        "Amt",
                        "parentInd"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        120, 150, 250, 100, 250,
                        //6-8
                        150, 150, 80, 150, 80
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 });

        }
        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteAddendumDt, MeeRemark, MeeRemark2
                    }, true);
                    SOCChkCancelInd.Properties.ReadOnly = true;
                    BtnSOCDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 21 });

                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, DteAddendumDt, MeeRemark2
                    }, false);
                    BtnSOCDocNo.Enabled = true;
                    SOCDteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 21 });

                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark2 }, false);
                    MeeRemark2.Properties.ReadOnly = false;
                    MeeRemark2.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, TxtStatus, DteDocDt, TxtSOCDocNo,
                  DteAddendumDt, MeeRemark2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtContractAmt, TxtAmt }, 0);
            SOCChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile, MeeRemark2
            });
        }

        private void ClearData3()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, MeeRemark2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtContractAmt, TxtAmt }, 0);
            SOCChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (SOCChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Grid Methods

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalBOQ();
                //ComputeItem();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string mIndexDocNo = string.Empty;
            string DocNo = string.Concat(TxtSOCDocNo.Text, "/", GenerateDocNo());

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractRevisionHdr(DocNo));
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) cml.Add(SaveSOContractRevisionDtl(DocNo, Row));
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveSOContractRevisionDtl2(DocNo, Row));

            cml.Add(UpdatePrevAmt(DocNo));

            //cml.Add(UpdateSOCAmt(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSOCDocNo, "SOC Document#", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ;
        }

        private MySqlCommand SaveSOContractRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, Status, Amt, Amt2, AddendumDt, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, 'O', @Amt, @Amt2, @AddendumDt, @Remark,");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='SOCRev'; ");
            }

            SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A', Remark=@Remark ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");
                
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            var ContractAmt = Decimal.Round(Decimal.Parse(TxtContractAmt.Text)); 
            Sm.CmParam<Decimal>(ref cm, "@Amt", ContractAmt);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text));
            Sm.CmParamDt(ref cm, "@AddendumDt", Sm.GetDte(DteAddendumDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark2.Text);


            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, Amt, DeliveryDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @DeliveryDt, @CreateBy, CurrentDateTime()); ");
            SQL.AppendLine("UPDATE tblsocontractdtl SET DeliveryDt=@DeliveryDt ");
            SQL.AppendLine("WHERE DocNo=@SOCDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl2(DocNo, SOCDocNo, DNo, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePrevAmt(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractRevisionHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt; ");
            
            SQL.AppendLine("Update TblSOContractRevisionDtl A ");
            SQL.AppendLine("Inner Join TblSOContractDtl B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

            SQL.AppendLine("Update TblSOContractRevisionDtl2 A ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }
        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSOContractRevisionHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditSOContractRevisionHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractRevisionHdr Set ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        //private MySqlCommand UpdateSOCAmt(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblSOContractHdr A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And B.DocNo = @DocNo And B.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    SQL.AppendLine("Update TblSOContractDtl A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionDtl B On A.DocNo = B.SOCDocNo And A.DNo = B.DNo And B.DocNo = @DocNo ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.DocNo = C.DocNo And C.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    SQL.AppendLine("Update TblSOContractDtl2 A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionDtl2 B On A.DocNo = B.SOCDocNo And A.DNo = B.DNo And B.DocNo = @DocNo ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.DocNo = C.DocNo And C.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //    return cm;
        //}

        private void InputAmount(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 9);
                if (Sm.GetGrdStr(Grd2, RowIndex, 8) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 4);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 4) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 7));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 7].Value = Qty;
                                    Grd2.Cells[y, 9].Value = Qty;
                                    Grd2.Cells[RowIndex, 9].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }           
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOContractRevision(DocNo);
                ShowSOContractRevisionHdr(DocNo);
                ShowSOContractRevisionDtl(DocNo);
                ShowSOContractRevisionDtl2(DocNo);
                ShowARDownPaymentRevision(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.SOCDocNo, A.DocDt, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.Amt, A.Amt2 ");
            SQL.AppendLine("From TblSOContractRevisionHdr A  ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "SOCDocNo", "DocDt", "StatusDesc", "Amt", "Amt2",

                    "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                    TxtContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    MeeRemark2.EditValue = Sm.DrStr(dr, c[6]);

                }, true
            );
        }

        private void ShowSOContractRevisionHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, T.Amt, T.Amt2, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, T.Remark As Remark2,");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname, T.AddendumDt ");
            SQL.AppendLine("From TblSOContractRevisionHdr T ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On T.SOCDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Where T.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                    //6-10
                    "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                    
                    //11-15
                   "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                    
                    //16-20
                    "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                    
                    //21-25
                    "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                    
                    //27
                    "ProjectName", "AddendumDt", "Remark2", "Amt2"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                    TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 0);
                    TxtContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                    SetLueSPCode(ref LueSPCode);
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                    SetLueDTCode(ref LueShpMCode);
                    Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    mSADNo = Sm.DrStr(dr, c[13]);
                    TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                    mCity = Sm.DrStr(dr, c[15]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                    mCnt = Sm.DrStr(dr, c[17]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                    SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                    SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                    Sm.SetDte(DteAddendumDt, Sm.DrStr(dr, c[27]));
                    MeeRemark2.EditValue = Sm.DrStr(dr, c[28]);
                }, true
            );
        }

        private void ShowSOContractRevisionDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   A2.Amt2 As Uprice, 0 As Discount, 0 As DiscountAmt, ");
            SQL.AppendLine("   A2.Amt2 As UPriceAfterDiscount, ");
            SQL.AppendLine("   0 As PromoRate, ");
            SQL.AppendLine("   A2.Amt2 As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate, ");
            SQL.AppendLine("   (B.TaxRate * 0.01 * A.Amt2) As TaxAmt, A2.Amt UPriceAfterTax, A2.Amt Total, ");
            SQL.AppendLine("   A1.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume, ");
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom ");
            SQL.AppendLine("   From TblSOContractRevisionDtl A1 ");
            SQL.AppendLine("   Inner Join TblSOContractRevisionHdr A2 On A1.DocNo = A2.DocNo And A1.DocNo = @DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractHdr A On A1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo And A1.DNo = B.DNo ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractRevisionDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A11.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From TblSOContractRevisionDtl2 A11 ");
            SQL.AppendLine("Inner Join tblSOContracthdr A1 On A11.SOCDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.DocNo = A2.Docno And A11.DNo = A2.DNo ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A1.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A11.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.ItBOQCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void ShowARDownPaymentRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo In (Select SOCDocNo From TblSOContractRevisionHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, I.Amt, I.Amt2, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Left Join TblSOContractRevisionHdr I On I.SOCDocNo = A.DocNO ");

            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                       "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-27
                        "ProjectName", "Amt2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueSPCode(ref LueSPCode);
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                    }, true
                );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*  ");
            SQL.AppendLine("From (   ");
            SQL.AppendLine("   SELECT A4.DocNO, ");
	        SQL.AppendLine("   B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, "); 
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty,  ");
            SQL.AppendLine("   A5.Amt2 As Uprice, 0 As Discount, 0 As DiscountAmt,   ");
            SQL.AppendLine("   A5.Amt2 As UPriceAfterDiscount,  ");
            SQL.AppendLine("   0 As PromoRate,  ");
            SQL.AppendLine("   A5.Amt2 As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate,  ");
	        SQL.AppendLine("    (B.TaxRate * 0.01 * A.Amt2) As TaxAmt, A5.Amt UPriceAfterTax, A5.Amt Total,  ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,   "); 
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom   ");
            SQL.AppendLine("   From TblSOContractHdr A  ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode   ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode   ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  "); 
            SQL.AppendLine("   inner JOIN tblsocontractrevisiondtl A3 ON A3.SOCDocNo=A.DocNo  AND B.DNo=A3.DNo ");
            SQL.AppendLine("   inner JOIN  ");
	        SQL.AppendLine("    (   ");
	        SQL.AppendLine("        SELECT MAX(A.DocNo) DocNo   ");
	        SQL.AppendLine("        FROM tblsocontractrevisiondtl A   ");
	        SQL.AppendLine("        INNER JOIN tblsocontracthdr B ON A.SOCDocNo=B.DocNo   ");
	        SQL.AppendLine("        WHERE A.SOCDocNo=@DocNO   ");
	        SQL.AppendLine("    ) A4 ON A4.DocNo= A3.DocNo   ");
	        SQL.AppendLine("    INNER JOIN tblsocontractrevisionhdr A5 ON A3.DocNo=A5.DocNo AND A3.DocNo=A4.DocNO ");
            SQL.AppendLine("   Where A.DocNo = @DocNo  ");
            SQL.AppendLine(") T Order By DNo; ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A3.DocNo, A3.DNo, A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A3.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From tblSOContracthdr A1 ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.DocNo = A2.Docno ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A1.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("INNER JOIN tblsocontractrevisiondtl2 A3 ON A3.SOCDocNo=A1.DocNo  AND A2.DNo=A3.DNo  "); 
            SQL.AppendLine("inner JOIN   ");
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    SELECT MAX(A.DocNo) DocNo  ");
	        SQL.AppendLine("    FROM tblsocontractrevisiondtl2 A  ");
	        SQL.AppendLine("    INNER JOIN tblsocontracthdr B ON A.SOCDocNo=B.DocNo  ");
	        SQL.AppendLine("    WHERE A.SOCDocNo=@DocNO  ");
            SQL.AppendLine(") A4 ON A4.DocNo= A3.DocNo  ");
            SQL.AppendLine("Where A1.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.ItBOQCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                  if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                  {
                      Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                      Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                      Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                      Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                  }

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ShowBOQ(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("C.ItBoqCode, D.ItBOQName, C.totalAMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("from tblBOQhdr A ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
	        SQL.AppendLine("    From TblItemBOQ ");
	        SQL.AppendLine("    Where ItBOQCode not in ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
		    SQL.AppendLine("        Where parent is not null ");
	        SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo, C.ItBOQCode ;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "TotalAmt", "Allow", "parentInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                    {
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                        Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                        Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }
                    else
                    {
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'SOCRev' Limit 1; ");
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblSOContractRevisionHdr ");
            SQL.AppendLine("       Where SOCDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtSOCDocNo.Text);
        }

        internal void ShowSOContract(string DocNo)
        {
            ClearData3();
            ShowSOContractHdr(DocNo);
            ShowSOContractDtl(DocNo);
            ShowSOContractDtl2(DocNo);
            ShowARDownPayment(DocNo);
        }

        internal void ComputeTotalBOQ()
        {
            decimal Amt = 0m;
            string BOQDocNo = TxtBOQDocNo.Text;

            for (int i = 0; i < Grd2.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 10).Length > 0 && Sm.GetGrdStr(Grd2, i, 10) == "Y")
                {
                    Amt += Sm.GetGrdDec(Grd2, i, 9);
                }
            }
        
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);

            ComputeItem();
        }


        internal void ComputeItem()
        {
            decimal Qty = 0m, TotalAmt = 0m, PriceGrid3 = 0m, TaxAmt = 0m, TaxPercentage = 0m;
            Qty = Sm.GetGrdDec(Grd3, 0, 8);
            TaxPercentage = Sm.GetGrdDec(Grd3, 0, 17);
            TotalAmt = Decimal.Parse(TxtAmt.Text);

            if (TotalAmt > 0 && Qty>0)
            {
                PriceGrid3 = TotalAmt * Qty;
                TaxAmt = PriceGrid3 * TaxPercentage * 0.01m;
                TotalAmt = PriceGrid3;
            }

            Grd3.Cells[0, 11].Value = PriceGrid3; // Price List
            Grd3.Cells[0, 14].Value = PriceGrid3; // Price After Discount
            Grd3.Cells[0, 16].Value = PriceGrid3; // Price Before Tax
            Grd3.Cells[0, 18].Value = TaxAmt; // Tax Amount
            Grd3.Cells[0, 19].Value = PriceGrid3 + TaxAmt; // Price After Tax
            Grd3.Cells[0, 20].Value = PriceGrid3 + TaxAmt; // Total

            TxtAmt.EditValue = Sm.FormatNum(TotalAmt, 0);
            TxtContractAmt.EditValue = Sm.FormatNum((PriceGrid3 + TaxAmt), 0);
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsSOUseARDPValidated = Sm.GetParameter("IsSOUseARDPValidated") == "Y";
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");            
        }

        private void ParPrint()
        {
            
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }



        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueSPCode(ref LueSPCode);
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void SOCDteDocDt_DateTimeChanged(object sender, EventArgs e)
        {
            DteAddendumDt.EditValue = SOCDteDocDt.Text;
        }

        #endregion

        #region Button Event

        private void BtnSOCDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
                Sm.FormShowDialog(new FrmSOContractRevisionDlg(this));
        }

        #endregion

        private void DteDeliveryDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeliveryDt, ref fCell, ref fAccept);

        }

        private void DteDeliveryDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd3, ref fAccept, e);

        }
        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 21) Sm.DteRequestEdit(Grd3, DteDeliveryDt, ref fCell, ref fAccept, e);

        }
        #endregion

       

        

       

    }
}
