﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAnnualPurchasing : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAnnualPurchasing(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            Sl.SetLueYr(LueYr, "");
            Sl.SetLueVdCode(ref LueVdCode);
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select xx.Vdcode, xx.Vdname, xx.SiteName, xx.Curcode, xx.Total, xx.Mth01, xx.Mth02, xx.Mth03, xx.Mth04, xx.Mth05, xx.Mth06, xx.Mth07, xx.Mth08,"); 
            SQL.AppendLine("xx.Mth09, xx.Mth10,xx.Mth11, xx.Mth12, xx.Year from ( ");
            SQL.AppendLine("Select * From ");
            SQL.AppendLine("(Select VdCode, VdName, SiteName, Curcode, ");
            SQL.AppendLine("(Sum(Mth01)+Sum(Mth02)+Sum(Mth03)+Sum(Mth04)+Sum(Mth05)+Sum(Mth06)+ ");
            SQL.AppendLine("Sum(Mth07)+Sum(Mth08)+Sum(Mth09)+Sum(Mth10)+Sum(Mth11)+Sum(Mth12)) As Total, ");
            SQL.AppendLine("Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            SQL.AppendLine("Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            SQL.AppendLine("Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12, Year ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select VdCode, VdName, SiteName, Curcode, ");
            SQL.AppendLine("Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            SQL.AppendLine("Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            SQL.AppendLine("Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            SQL.AppendLine("Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            SQL.AppendLine("Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
            SQL.AppendLine("Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            SQL.AppendLine("Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
            SQL.AppendLine("Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
            SQL.AppendLine("Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            SQL.AppendLine("Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            SQL.AppendLine("Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            SQL.AppendLine("Case Mth When '12' Then Amt Else 0.0000 End As Mth12, Year ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("     Select A.VdCode, D.VdName, F.SiteName, A.Curcode, sum(A.Amt)As Amt, Substring(C.DocDt, 5, 2) As Mth, Left(C.DocDt,4)As Year	");	 
            SQL.AppendLine("		From tblpohdr A ");
            SQL.AppendLine("		inner join TblRecvVddtl B On A.DocNo=B.PODocNo ");
            SQL.AppendLine("		inner join TblRecvVdHdr C On B.DocNo=C.DocNo ");
            SQL.AppendLine("		Inner Join TblVendor D On A.VdCode=D.VdCode ");
            SQL.AppendLine("		Left Join ( ");
            SQL.AppendLine("			Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("			Select convert('02' using latin1) Union All ");
            SQL.AppendLine("			Select convert('03' using latin1) Union All ");
            SQL.AppendLine("			Select convert('04' using latin1) Union All ");
            SQL.AppendLine("			Select convert('05' using latin1) Union All ");
            SQL.AppendLine("			Select convert('06' using latin1) Union All ");
            SQL.AppendLine("			Select convert('07' using latin1) Union All ");
            SQL.AppendLine("			Select convert('08' using latin1) Union All ");
            SQL.AppendLine("			Select convert('09' using latin1) Union All ");
            SQL.AppendLine("			Select convert('10' using latin1) Union All ");
            SQL.AppendLine("			Select convert('11' using latin1) Union All ");
            SQL.AppendLine("			Select convert('12' using latin1) ");
            SQL.AppendLine("        ) E On Substring(C.DocDt, 5, 2)=convert(E.Mth using latin1) ");
            SQL.AppendLine("        Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("        Where B.CancelInd='N'");
            SQL.AppendLine("     Group by A.VdCode, D.VdName, F.SiteName, A.Curcode, Substring(C.DocDt, 5, 2), Left(C.DocDt,4) Having Amt<>0 ");   
            SQL.AppendLine("	)X ");
            SQL.AppendLine(")Z ");
            SQL.AppendLine("Group By  Z.VdCode, Z.VdName, Z.SiteName, Z.Curcode, Z.Year ");
            SQL.AppendLine("Having "); 
            SQL.AppendLine("    Sum(Mth01)<>0 Or  Sum(Mth02)<>0 Or Sum(Mth03)<>0 Or Sum(Mth04)<>0 Or Sum(Mth05)<>0 Or Sum(Mth06)<>0 Or ");
            SQL.AppendLine("    Sum(Mth07)<>0 Or Sum(Mth08)<>0 Or Sum(Mth09)<>0 Or Sum(Mth10)<>0 Or Sum(Mth11)<>0 Or Sum(Mth12)<>0 ");
            SQL.AppendLine(") Z1 ");
            SQL.AppendLine("Order by VdCode, VdName, SiteName, Year ");
            SQL.AppendLine(")xx ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Vendor"+Environment.NewLine+"Code",
                        "Vendor Name ",
                        "Site",
                        "Currency",
                        "Total",
                       
                        //6-10
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        
                        //11-15
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",

                        //16-17
                        "November",
                        "December"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 150, 80, 150,   

                        //6-10
                        140, 140, 140, 140, 140,    
                        
                        //11-15
                        140, 140, 140, 140, 140,
                        
                        //16-17
                        140, 140,
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
          
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " Where 0=0";

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "xx.VdCode", true);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "xx.Year", true);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter,
                            new string[]
                            { 
                                //0
                                "VdCode", 

                                //1-5
                                "VdName", "SiteName", "Curcode", "Total", "Mth01",  
                                
                                //6-10
                                "Mth02", "Mth03", "Mth04", "Mth05", "Mth06", 
                                
                                //11-15
                                "Mth07", "Mth08", "Mth09", "Mth10", "Mth11",
                                
                                //16
                                "Mth12", 
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);

                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);

                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 16);

                            }, true, false, false, false
                        );
                    Grd1.GroupObject.Add(2);
                    Grd1.Group();
                    AdjustSubtotals();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    //if (Grd1.Rows.Count > 1)
                    //    Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 5 : 3);
                    //else
                    Sm.FocusGrd(Grd1, 0, 0);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }
       
        #endregion

        
    }
}
