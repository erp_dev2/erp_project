﻿#region Update
/*
    14/04/2022 [ICA/YK] New Apps
    25/04/2022 [MYA/YK] Membuat Printout reporting laba rugi nasional
    26/04/2022 [MYA/YK] Membuat Printout reporting neraca nasional
    17/06/2022 [MYA/YK] BUG : Membuat Printout reporting neraca nasional
    20/06/2022 [MYA/YK] BUG : Printout Reporting National Balance Sheet
    27/06/2022 [MYA/YK] Menambah coa untuk akumulasi per dua digit pertama COA di Printout National Profit Loss.  
    27/06/2022 [MYA/YK] Menambah rumus akumulasi per dua digit pertama COA di Printout National Profit Loss.
    29/06/2022 [MYA/YK] Menambah coa untuk akumulasi per dua digit pertama COA di Printout National Balance Sheet. 
    29/06/2022 [MYA/YK] Menambah akumulasi per dua digit pertama COA di Printout National Balance Sheet. 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss11 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string
            mMenuCodeForReportingNationalBalanceSheet = string.Empty,
            mSiteCodeForNationalProfitLoss = string.Empty,
            mCentralSiteCode = string.Empty;
        private bool IsNBS = false;
        private int mLatestCol = 6;
        private List<COA> lCOA;
        private List<Site> lSite;
        private List<Journal> lJournal;

        #endregion

        #region Constructor

        public FrmRptProfitLoss11(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {

                lCOA = new List<COA>();
                lSite = new List<Site>();
                lJournal = new List<Journal>();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                if (!IsNBS)
                {
                    LueMth.Visible = label1.Visible = false;
                    label2.Top -= 21;
                    label3.Top -= 21;
                    DteDocDt1.Top -= 21;
                    DteDocDt2.Top -= 21;
                    panel2.Height -= 21;
                }
                else
                {
                    DteDocDt1.Visible = DteDocDt2.Visible = label2.Visible = label3.Visible = false;
                    panel2.Height -= 42;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",
                    
                    //1-5
                    "KETERANGAN",
                    "JUMLAH BESAR",
                    "JUMLAH LUAR PUSAT",
                    "Group",
                    "Type",

                    //6
                    "AcNo",
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    250, 150, 150, 50, 50,

                    //6
                    50
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        protected override void ShowData()
        {
            if (IsShowDataNotValid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //var lCOA = new List<COA>();
                //var lSite = new List<Site>();
                //var lJournal = new List<Journal>();
                lCOA.Clear(); lSite.Clear(); lJournal.Clear();

                Process1(ref lSite);
                Process2(ref lSite, ref lCOA, ref lJournal);
                Process3(ref lJournal);
                Process4(ref lSite, ref lCOA, ref lJournal);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint(ref lSite);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint(ref List<Site> lSite)
        {
            var lJH = new List<ProfitLoss>();
            var lJP = new List<JournalPrint>();
            var lJT = new List<TotalNBS>();
            var lJATA = new List<TotalAllNBSA>();
            var lJATP = new List<TotalAllNBSP>();

            List<IList> myLists = new List<IList>();

            string[] TableName = { "ProfitLoss","JournalPrint","TotalNBS","TotalAllNBSA","TotalAllNBSP" };

            if (IsNBS)
            {
                #region Header

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                string Mth =  (Sm.ServerCurrentDate()).Substring(4, 2) ;
                string Date = string.Empty;
                
                if(Sm.GetLue(LueMth) == Mth)
                {
                    Date = string.Format("{0:dd/MM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))); 
                }
                else
                {
                    Date = Sm.GetValue("select LAST_DAY('"+ Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01')").Substring(0, 10);
                }

                SQL.AppendLine("Select ParValue AS CompanyName From TblParameter Where Parcode = 'ReportTitle1' ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();


                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                            {
                        //0
                         "CompanyName",

                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lJH.Add(new ProfitLoss()
                            {
                                CompanyName = Sm.DrStr(dr, c[0]),

                                DocDt1 = Date,
                                //DocDt1 = string.Format("{0:dd/MM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))),
                                DocDt2 = string.Format("{0:dd/MM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2))),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(lJH);

                #endregion

                #region Detail

                int
                    ColSite1 = 0,
                    ColSite2 = 0,
                    ColSite3 = 0,
                    ColSite4 = 0,
                    ColSite5 = 0,
                    ColSite6 = 0,
                    ColSite7 = 0,
                    ColSite8 = 0,
                    ColSite9 = 0,
                    ColSite10 = 0,
                    ColSite11 = 0,
                    ColSite12 = 0;

                foreach (var y in lSite)
                {
                    if (y.SiteCode == "000")
                        ColSite1 = y.Col;
                    if (y.SiteCode == "010")
                        ColSite2 = y.Col;
                    if (y.SiteCode == "020")
                        ColSite3 = y.Col;
                    if (y.SiteCode == "070")
                        ColSite4 = y.Col;
                    if (y.SiteCode == "040")
                        ColSite5 = y.Col;
                    if (y.SiteCode == "050")
                        ColSite6 = y.Col;
                    if (y.SiteCode == "060")
                        ColSite7 = y.Col;
                    if (y.SiteCode == "030")
                        ColSite8 = y.Col;
                    if (y.SiteCode == "080")
                        ColSite9 = y.Col;
                    if (y.SiteCode == "090")
                        ColSite10 = y.Col;
                    if (y.SiteCode == "100")
                        ColSite11 = y.Col;
                    if (y.SiteCode == "110")
                        ColSite12 = y.Col;
                }

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {

                        lJP.Add(new JournalPrint()
                        {
                            AcDesc = Sm.GetGrdStr(Grd1, i, 1),
                            JumlahBesar = Sm.GetGrdDec(Grd1, i, 2),
                            JumlahLuarPusat = Sm.GetGrdDec(Grd1, i, 3),
                            Group = Sm.GetGrdStr(Grd1, i, 4),
                            Type = Sm.GetGrdStr(Grd1, i, 5),
                            Pusat = ColSite1 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite1) : 0m,
                            Wil1 = ColSite2 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite2) : 0m,
                            Wil2 = ColSite3 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite3) : 0m,
                            Wil3 = ColSite4 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite4) : 0m,
                            Wil4 = ColSite5 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite5) : 0m,
                            Wil5 = ColSite6 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite6) : 0m,
                            Wil6 = ColSite7 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite7) : 0m,
                            DivGed = ColSite8 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite8) : 0m,
                            DivInf = ColSite9 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite9) : 0m,
                            DivKim = ColSite10 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite10) : 0m,
                            DivEng = ColSite11 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite11) : 0m,
                            DivPengBis = ColSite12 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite12) : 0m,
                            AcNo = Sm.GetGrdStr(Grd1, i, 6),
                            AcNoParent = Sm.GetGrdStr(Grd1, i, 6).Substring(0, 3),
                        });
                    }
                }
                myLists.Add(lJP);


                #endregion

                #region Total NBS

                string mGroup = string.Empty, mType = string.Empty;
                int TotalGroupA = 0, TotalGroupP = 0;

                TotalGroupA = Convert.ToInt32(Sm.GetValue("SELECT COUNT(DISTINCT(A.`Group`))" +
                                                         "FROM tblrptprofitlosscoasetting A " +
                                                         "WHERE A.`Type`='A' AND A.RptType = 'NBS'"));

                TotalGroupP = Convert.ToInt32(Sm.GetValue("SELECT COUNT(DISTINCT(A.`Group`))" +
                                                         "FROM tblrptprofitlosscoasetting A " +
                                                         "WHERE A.`Type`='P' AND A.RptType = 'NBS'"));


                #region Group Aktiva

                for (int i = 1; i <= TotalGroupA; i++)
                {

                    decimal
                        mTotJumlahBesar = 0m,
                        mTotJumlahLuarPusat = 0m,
                        mTotPusat = 0m,
                        mTotWil1 = 0m,
                        mTotWil2 = 0m,
                        mTotWil3 = 0m,
                        mTotWil4 = 0m,
                        mTotWil5 = 0m,
                        mTotWil6 = 0m,
                        mTotDivGed = 0m,
                        mTotDivInf = 0m,
                        mTotDivKim = 0m,
                        mTotDivEng = 0m,
                        mTotDivPengBis = 0m;
                    string
                        mAcNoParent = string.Empty;

                    foreach (var a in lJP.Where(a => a.Group == Convert.ToString(i) && a.Type == "A"))
                    {
                        mTotJumlahBesar += a.JumlahBesar;
                        mTotJumlahLuarPusat += a.JumlahLuarPusat;
                        mTotPusat += a.Pusat;
                        mTotWil1 += a.Wil1;
                        mTotWil2 += a.Wil2;
                        mTotWil3 += a.Wil3;
                        mTotWil4 += a.Wil4;
                        mTotWil5 += a.Wil5;
                        mTotWil6 += a.Wil6;
                        mTotDivGed += a.DivGed;
                        mTotDivInf += a.DivInf;
                        mTotDivKim += a.DivKim;
                        mTotDivEng += a.DivEng;
                        mTotDivPengBis += a.DivPengBis;
                        mGroup = a.Group;
                        mType = a.Type;
                        mAcNoParent = a.AcNo;
                    }

                    lJT.Add(new TotalNBS()
                    {
                        Group = mGroup,
                        Type = mType,
                        TotJumlahBesar = mTotJumlahBesar,
                        TotJumlahLuarPusat = mTotJumlahLuarPusat,
                        TotPusat = mTotPusat,
                        TotWil1 = mTotWil1,
                        TotWil2 = mTotWil2,
                        TotWil3 = mTotWil3,
                        TotWil4 = mTotWil4,
                        TotWil5 = mTotWil5,
                        TotWil6 = mTotWil6,
                        TotDivGed = mTotDivGed,
                        TotDivInf = mTotDivInf,
                        TotDivKim = mTotDivKim,
                        TotDivEng = mTotDivEng,
                        TotDivPengBis = mTotDivPengBis,
                        AcNoParent = mAcNoParent.Substring(0, 3),
                    });
                }

                #endregion

                #region Group Passiva

                for (int i = 1; i <= TotalGroupP; i++)
                {

                    decimal
                        mTotJumlahBesar = 0m,
                        mTotJumlahLuarPusat = 0m,
                        mTotPusat = 0m,
                        mTotWil1 = 0m,
                        mTotWil2 = 0m,
                        mTotWil3 = 0m,
                        mTotWil4 = 0m,
                        mTotWil5 = 0m,
                        mTotWil6 = 0m,
                        mTotDivGed = 0m,
                        mTotDivInf = 0m,
                        mTotDivKim = 0m,
                        mTotDivEng = 0m,
                        mTotDivPengBis = 0m;
                    string
                        mAcNoParent = string.Empty;

                    foreach (var a in lJP.Where(a => a.Group == Convert.ToString(i) && a.Type == "P"))
                    {
                        mTotJumlahBesar += a.JumlahBesar;
                        mTotJumlahLuarPusat += a.JumlahLuarPusat;
                        mTotPusat += a.Pusat;
                        mTotWil1 += a.Wil1;
                        mTotWil2 += a.Wil2;
                        mTotWil3 += a.Wil3;
                        mTotWil4 += a.Wil4;
                        mTotWil5 += a.Wil5;
                        mTotWil6 += a.Wil6;
                        mTotDivGed += a.DivGed;
                        mTotDivInf += a.DivInf;
                        mTotDivKim += a.DivKim;
                        mTotDivEng += a.DivEng;
                        mTotDivPengBis += a.DivPengBis;
                        mGroup = a.Group;
                        mType = a.Type;
                        mAcNoParent = a.AcNo;
                    }

                    lJT.Add(new TotalNBS()
                    {
                        Group = mGroup,
                        Type = mType,
                        TotJumlahBesar = mTotJumlahBesar,
                        TotJumlahLuarPusat = mTotJumlahLuarPusat,
                        TotPusat = mTotPusat,
                        TotWil1 = mTotWil1,
                        TotWil2 = mTotWil2,
                        TotWil3 = mTotWil3,
                        TotWil4 = mTotWil4,
                        TotWil5 = mTotWil5,
                        TotWil6 = mTotWil6,
                        TotDivGed = mTotDivGed,
                        TotDivInf = mTotDivInf,
                        TotDivKim = mTotDivKim,
                        TotDivEng = mTotDivEng,
                        TotDivPengBis = mTotDivPengBis,
                        AcNoParent = mAcNoParent.Substring(0, 3),
                    });
                }

                #endregion

                myLists.Add(lJT);


                #endregion

                #region Total All NBS


                #region Group Aktiva

                for (int i = 1; i <= TotalGroupA; i++)
                {

                    decimal
                        mTotAllJumlahBesar = 0m,
                        mTotAllJumlahLuarPusat = 0m,
                        mTotAllPusat = 0m,
                        mTotAllWil1 = 0m,
                        mTotAllWil2 = 0m,
                        mTotAllWil3 = 0m,
                        mTotAllWil4 = 0m,
                        mTotAllWil5 = 0m,
                        mTotAllWil6 = 0m,
                        mTotAllDivGed = 0m,
                        mTotAllDivInf = 0m,
                        mTotAllDivKim = 0m,
                        mTotAllDivEng = 0m,
                        mTotAllDivPengBis = 0m;

                    foreach (var a in lJP.Where(a => a.Type == "A"))
                    {
                        mTotAllJumlahBesar += a.JumlahBesar;
                        mTotAllJumlahLuarPusat += a.JumlahLuarPusat;
                        mTotAllPusat += a.Pusat;
                        mTotAllWil1 += a.Wil1;
                        mTotAllWil2 += a.Wil2;
                        mTotAllWil3 += a.Wil3;
                        mTotAllWil4 += a.Wil4;
                        mTotAllWil5 += a.Wil5;
                        mTotAllWil6 += a.Wil6;
                        mTotAllDivGed += a.DivGed;
                        mTotAllDivInf += a.DivInf;
                        mTotAllDivKim += a.DivKim;
                        mTotAllDivEng += a.DivEng;
                        mTotAllDivPengBis += a.DivPengBis;
                        mType = a.Type;
                    }

                    lJATA.Add(new TotalAllNBSA()
                    {
                        Group = mGroup,
                        Type = mType,
                        TotAllJumlahBesar = mTotAllJumlahBesar,
                        TotAllJumlahLuarPusat = mTotAllJumlahLuarPusat,
                        TotAllPusat = mTotAllPusat,
                        TotAllWil1 = mTotAllWil1,
                        TotAllWil2 = mTotAllWil2,
                        TotAllWil3 = mTotAllWil3,
                        TotAllWil4 = mTotAllWil4,
                        TotAllWil5 = mTotAllWil5,
                        TotAllWil6 = mTotAllWil6,
                        TotAllDivGed = mTotAllDivGed,
                        TotAllDivInf = mTotAllDivInf,
                        TotAllDivKim = mTotAllDivKim,
                        TotAllDivEng = mTotAllDivEng,
                        TotAllDivPengBis = mTotAllDivPengBis
                    });
                }

                myLists.Add(lJATA);

                #endregion

                #region Group Passiva

                for (int i = 1; i <= TotalGroupP; i++)
                {

                    decimal
                        mTotAllJumlahBesar = 0m,
                        mTotAllJumlahLuarPusat = 0m,
                        mTotAllPusat = 0m,
                        mTotAllWil1 = 0m,
                        mTotAllWil2 = 0m,
                        mTotAllWil3 = 0m,
                        mTotAllWil4 = 0m,
                        mTotAllWil5 = 0m,
                        mTotAllWil6 = 0m,
                        mTotAllDivGed = 0m,
                        mTotAllDivInf = 0m,
                        mTotAllDivKim = 0m,
                        mTotAllDivEng = 0m,
                        mTotAllDivPengBis = 0m;

                    foreach (var a in lJP.Where(a => a.Type == "P"))
                    {
                        mTotAllJumlahBesar += a.JumlahBesar;
                        mTotAllJumlahLuarPusat += a.JumlahLuarPusat;
                        mTotAllPusat += a.Pusat;
                        mTotAllWil1 += a.Wil1;
                        mTotAllWil2 += a.Wil2;
                        mTotAllWil3 += a.Wil3;
                        mTotAllWil4 += a.Wil4;
                        mTotAllWil5 += a.Wil5;
                        mTotAllWil6 += a.Wil6;
                        mTotAllDivGed += a.DivGed;
                        mTotAllDivInf += a.DivInf;
                        mTotAllDivKim += a.DivKim;
                        mTotAllDivEng += a.DivEng;
                        mTotAllDivPengBis += a.DivPengBis;
                        mType = a.Type;
                    }

                    lJATP.Add(new TotalAllNBSP()
                    {
                        Group = mGroup,
                        Type = mType,
                        TotAllJumlahBesar = mTotAllJumlahBesar,
                        TotAllJumlahLuarPusat = mTotAllJumlahLuarPusat,
                        TotAllPusat = mTotAllPusat,
                        TotAllWil1 = mTotAllWil1,
                        TotAllWil2 = mTotAllWil2,
                        TotAllWil3 = mTotAllWil3,
                        TotAllWil4 = mTotAllWil4,
                        TotAllWil5 = mTotAllWil5,
                        TotAllWil6 = mTotAllWil6,
                        TotAllDivGed = mTotAllDivGed,
                        TotAllDivInf = mTotAllDivInf,
                        TotAllDivKim = mTotAllDivKim,
                        TotAllDivEng = mTotAllDivEng,
                        TotAllDivPengBis = mTotAllDivPengBis
                    });
                }

                myLists.Add(lJATP);

                #endregion



                #endregion

                Sm.PrintReport("NationalBalanceSheetYK", myLists, TableName, false);
            }
            else
            {
                #region Header

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ParValue AS CompanyName From TblParameter Where Parcode = 'ReportTitle1' ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();


                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                            {
                        //0
                         "CompanyName",

                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lJH.Add(new ProfitLoss()
                            {
                                CompanyName = Sm.DrStr(dr, c[0]),

                                DocDt1 = string.Format("{0:dd/MM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))),
                                DocDt2 = string.Format("{0:dd/MM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2))),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(lJH);

                #endregion

                #region Detail

                int
                    ColSite1 = 0,
                    ColSite2 = 0,
                    ColSite3 = 0,
                    ColSite4 = 0,
                    ColSite5 = 0,
                    ColSite6 = 0,
                    ColSite7 = 0,
                    ColSite8 = 0,
                    ColSite9 = 0,
                    ColSite10 = 0,
                    ColSite11 = 0,
                    ColSite12 = 0;

                foreach (var y in lSite)
                {
                    if (y.SiteCode == "000")
                        ColSite1 = y.Col;
                    if (y.SiteCode == "010")
                        ColSite2 = y.Col;
                    if (y.SiteCode == "020")
                        ColSite3 = y.Col;
                    if (y.SiteCode == "070")
                        ColSite4 = y.Col;
                    if (y.SiteCode == "040")
                        ColSite5 = y.Col;
                    if (y.SiteCode == "050")
                        ColSite6 = y.Col;
                    if (y.SiteCode == "060")
                        ColSite7 = y.Col;
                    if (y.SiteCode == "030")
                        ColSite8 = y.Col;
                    if (y.SiteCode == "080")
                        ColSite9 = y.Col;
                    if (y.SiteCode == "090")
                        ColSite10 = y.Col;
                    if (y.SiteCode == "100")
                        ColSite11 = y.Col;
                    if (y.SiteCode == "110")
                        ColSite12 = y.Col;
                }

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {

                        lJP.Add(new JournalPrint()
                        {
                            AcDesc = Sm.GetGrdStr(Grd1, i, 1),
                            JumlahBesar = Sm.GetGrdDec(Grd1, i, 2),
                            JumlahLuarPusat = Sm.GetGrdDec(Grd1, i, 3),
                            Group = Sm.GetGrdStr(Grd1, i, 4),
                            Pusat = ColSite1 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite1) : 0m,
                            Wil1 = ColSite2 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite2) : 0m,
                            Wil2 = ColSite3 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite3) : 0m,
                            Wil3 = ColSite4 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite4) : 0m,
                            Wil4 = ColSite5 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite5) : 0m,
                            Wil5 = ColSite6 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite6) : 0m,
                            Wil6 = ColSite7 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite7) : 0m,
                            DivGed = ColSite8 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite8) : 0m,
                            DivInf = ColSite9 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite9) : 0m,
                            DivKim = ColSite10 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite10) : 0m,
                            DivEng = ColSite11 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite11) : 0m,
                            DivPengBis = ColSite12 > 0 ? Sm.GetGrdDec(Grd1, i, ColSite12) : 0m,
                            AcNo = Sm.GetGrdStr(Grd1, i, 6),
                            AcNoParent = Sm.GetGrdStr(Grd1, i, 6).Substring(0, 3),
                        });
                    }
                }

                myLists.Add(lJP);


                #endregion

                Sm.PrintReport("NationalProfitLossYK", myLists, TableName, false);
            }

        }

        #endregion

        #region Additional Method

        private bool IsShowDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                (!IsNBS && Sm.IsDteEmpty(DteDocDt1, "Date From")) ||
                (!IsNBS && Sm.IsDteEmpty(DteDocDt2, "Date To")) ||
                (!IsNBS && IsDateNotValid()) ||
                (IsNBS && Sm.IsLueEmpty(LueMth, "Month"));
        }

        private bool IsDateNotValid()
        {
            if(Sm.Left(Sm.GetDte(DteDocDt1), 4) != Sm.GetLue(LueYr))
            {
                Sm.StdMsg(mMsgType.Warning, "Date From should in " + Sm.GetLue(LueYr) + ".");
                return true;
            }
            if (Sm.Left(Sm.GetDte(DteDocDt2), 4) != Sm.GetLue(LueYr))
            {
                Sm.StdMsg(mMsgType.Warning, "Date To should in " + Sm.GetLue(LueYr) + ".");
                return true;
            }
            if(Decimal.Parse(Sm.GetDte(DteDocDt1)) > Decimal.Parse(Sm.GetDte(DteDocDt2)))
            {
                Sm.StdMsg(mMsgType.Warning, "Date To should more then Date From. ");
                return true;
            }

            return false;
        }

        private void GetParameter()
        {
            mMenuCodeForReportingNationalBalanceSheet = Sm.GetParameter("MenuCodeForReportingNationalBalanceSheet");
            mSiteCodeForNationalProfitLoss = Sm.GetParameter("SiteCodeForNationalProfitLoss");
            mCentralSiteCode = Sm.GetParameter("CentralSiteCode");


            if (mMenuCode == mMenuCodeForReportingNationalBalanceSheet) IsNBS = true;
        }

        //get SiteCode
        private void Process1(ref List<Site> lSite)
        {
            int TempCol = 0;
            Grd1.Cols.Count = mLatestCol + 1;
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT SiteCode, SiteName, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When SiteCode = '"+ mCentralSiteCode + "' Then '1' ");
            SQL.AppendLine("    else '0' ");
            SQL.AppendLine("End CentralSiteInd ");
            SQL.AppendLine("FROM TblSite ");
            SQL.AppendLine("Where Find_In_Set(SiteCode, @SiteCode) ");
            SQL.AppendLine("    And ActInd = 'Y' ");
            SQL.AppendLine("Order By SiteCode; ");
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCodeForNationalProfitLoss);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName", "CentralSiteInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lSite.Add(new Site()
                        {
                            SiteCode = dr.GetString(0),
                            SiteName = dr.GetString(1),
                            CentaralSiteInd = dr.GetString(2) == "1" ? true : false
                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in lSite)
            {
                Grd1.Cols.Count += 1;
                TempCol = Grd1.Cols.Count;

                x.Col = TempCol - 1;
                Grd1.Cols[TempCol - 1].Text = x.SiteName;
                Grd1.Cols[TempCol - 1].Width = 150;
                Grd1.Header.Cells[0, TempCol - 1].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[TempCol - 1].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[TempCol - 1].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
                Grd1.Cols[TempCol - 1].CellStyle.ValueType = typeof(decimal);
            }
        }

        //Get COA
        private void Process2(ref List<Site> lSite, ref List<COA> lCOA, ref List<Journal> lJournal)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AcNo, B.AcDesc, A.Type, IFNULL(A.Group, 0) as 'Group' ");
            SQL.AppendLine("FROM TblRptProfitLossCOASetting A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
            if (!IsNBS)
                SQL.AppendLine("    And RptType = 'NPL' ");
            else
                SQL.AppendLine("    And RptType = 'NBS' ");
            SQL.AppendLine("Order By AcNo; ");
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Type", "Group" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = dr.GetString(0),
                            AcDesc = dr.GetString(1),
                            Type = dr.GetString(2),
                            Group = dr.GetString(3),
                        });
                    }
                }
                dr.Close();
            }

            foreach(var x in lCOA)
            {
                foreach(var y in lSite)
                {
                    lJournal.Add(new Journal()
                    {
                        AcNo = x.AcNo,
                        AcNo2 = x.AcNo + "." + y.SiteCode,
                        Amt = 0m
                    });
                }
            }
        }

        //Get Site Journal Amt
        private void Process3(ref List<Journal> lJournal)
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            int i = 0;
            bool IsFirst = true;

            if (IsNBS)
                Filter = "  And Left(A.DocDt, 4) = @Yr And Left(A.DocDt, 6) <= Concat(@Yr, @Mth) ";
            else
                Filter = "  And A.DocDt Between @DocDt1 And @DocDt2 ";

            foreach (var x in lJournal)
            {
                if (IsFirst)
                {
                    SQL.AppendLine("SELECT A.AcNo, '"+x.AcNo2+"' AcNo2, IfNull(C_" + i + ".Amt, 0.00) Amt ");
                    SQL.AppendLine("FROM TblRptProfitLossCOASetting A ");
                    SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
                    SQL.AppendLine("LEFT JOIN ( ");
                    SQL.AppendLine("	SELECT '" + x.AcNo2 + "' AcNo, SUM(A.Amt) Amt ");
                    SQL.AppendLine("	FROM ( ");
                    SQL.AppendLine("		SELECT ");
                    SQL.AppendLine("		Case ");
                    SQL.AppendLine("			When C.AcType = 'D' then B.DAmt - B.CAmt ");
                    SQL.AppendLine("			ELSE B.CAmt - B.DAmt ");
                    SQL.AppendLine("		END Amt ");
                    SQL.AppendLine("		FROM TblJournalHdr A ");
                    SQL.AppendLine("		INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("		INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                    SQL.AppendLine("            And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                    SQL.AppendLine("		WHERE B.AcNo LIKE '" + x.AcNo2 + "%' ");

                    if (IsNBS)
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Sum(B.Amt) Amt ");
                        SQL.AppendLine("        From TblCOAOpeningBalanceHdr A ");
                        SQL.AppendLine("        Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("            And A.Yr = @Yr ");
                        SQL.AppendLine("            And A.CancelInd = 'N' ");
                        SQL.AppendLine("        Inner Join TblCOA C On B.AcNo = C.AcNo ");
                        SQL.AppendLine("            And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                        SQL.AppendLine("		WHERE B.AcNo LIKE '" + x.AcNo2 + "%' ");
                    }
                    SQL.AppendLine("	)A ");
                    SQL.AppendLine("	GROUP BY AcNo ");
                    SQL.AppendLine(")C_" + i + " ON Left(C_" + i + ".AcNo, Length(A.AcNo)) = A.AcNo ");
                    SQL.AppendLine("Where A.AcNo = '"+x.AcNo+"' ");
                    if (!IsNBS)
                        SQL.AppendLine("    And RptType = 'NPL' ");
                    else
                        SQL.AppendLine("    And RptType = 'NBS' ");

                    IsFirst = false;
                }
                else
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("SELECT A.AcNo, '" + x.AcNo2 + "' AcNo2, IfNull(C_" + i + ".Amt, 0.00) Amt ");
                    SQL.AppendLine("FROM TblRptProfitLossCOASetting A ");
                    SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
                    SQL.AppendLine("LEFT JOIN ( ");
                    SQL.AppendLine("	SELECT '" + x.AcNo2 + "' AcNo, SUM(A.Amt) Amt ");
                    SQL.AppendLine("	FROM ( ");
                    SQL.AppendLine("		SELECT ");
                    SQL.AppendLine("		Case ");
                    SQL.AppendLine("			When C.AcType = 'D' then B.DAmt - B.CAmt ");
                    SQL.AppendLine("			ELSE B.CAmt - B.DAmt ");
                    SQL.AppendLine("		END Amt ");
                    SQL.AppendLine("		FROM TblJournalHdr A ");
                    SQL.AppendLine("		INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("		INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                    SQL.AppendLine("            And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                    SQL.AppendLine("		WHERE B.AcNo LIKE '" + x.AcNo2 + "%' ");

                    if (IsNBS)
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Sum(B.Amt) Amt ");
                        SQL.AppendLine("        From TblCOAOpeningBalanceHdr A ");
                        SQL.AppendLine("        Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("            And A.Yr = @Yr ");
                        SQL.AppendLine("            And A.CancelInd = 'N' ");
                        SQL.AppendLine("        Inner Join TblCOA C On B.AcNo = C.AcNo ");
                        SQL.AppendLine("            And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                        SQL.AppendLine("		WHERE B.AcNo LIKE '" + x.AcNo2 + "%' ");
                    }
                    SQL.AppendLine("	)A ");
                    SQL.AppendLine("	GROUP BY AcNo ");
                    SQL.AppendLine(")C_" + i + " ON Left(C_" + i + ".AcNo, Length(A.AcNo)) = A.AcNo ");
                    SQL.AppendLine("Where A.AcNo = '" + x.AcNo + "' ");
                    if (!IsNBS)
                        SQL.AppendLine("    And RptType = 'NPL' ");
                    else
                        SQL.AppendLine("    And RptType = 'NBS' ");
                }

                i += 1;
            }

            var cm = new MySqlCommand();
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<string>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParamDt(ref cm, "@DOcDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DOcDt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcNo2", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in lJournal.Where(w => w.AcNo == dr.GetString(0) && w.AcNo2 == dr.GetString(1)))
                        {
                            x.Amt = dr.GetDecimal(2);
                        }
                    }
                }
                dr.Close();
            }
        }

        //set data in grid
        private void Process4(ref List<Site> lSite, ref List<COA> lCOA, ref List<Journal> lJournal)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            int mNo = 0;
            string AcNo = string.Empty;
            for (int i = 0; i < lCOA.Count; i++)
            {
                decimal TotAmt = 0m, AmtPusat = 0m;
                int SiteCol = mLatestCol + 1;
                AcNo = lCOA[i].AcNo;

                r = Grd1.Rows.Add();
                r.Cells[0].Value = ++mNo;
                r.Cells[1].Value = lCOA[i].AcDesc;

                foreach(var x in lSite)
                {
                    foreach(var y in lJournal.Where(w=> w.AcNo == AcNo && w.AcNo2 == AcNo+"."+x.SiteCode))
                    {
                        r.Cells[SiteCol].Value = y.Amt;

                        if (x.SiteCode == mCentralSiteCode)
                            AmtPusat = y.Amt;
                        TotAmt += y.Amt;

                        SiteCol++;
                    }
                }

                r.Cells[2].Value = TotAmt;
                r.Cells[3].Value = TotAmt - AmtPusat;
                r.Cells[4].Value = lCOA[i].Group;
                r.Cells[5].Value = lCOA[i].Type;
                r.Cells[6].Value = lCOA[i].AcNo;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true; 
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3 });
            for (int Col = mLatestCol + 1; Col < Grd1.Cols.Count; Col++)
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { Col });
            Grd1.EndUpdate();
        }

        #endregion

        #region Event

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
           if(Sm.GetLue(LueYr).Length > 0)
            {
                Sm.SetDte(DteDocDt1, Sm.GetLue(LueYr) + DateTime.Now.ToString("MMdd"));
                Sm.SetDte(DteDocDt2, Sm.GetLue(LueYr) + DateTime.Now.ToString("MMdd"));
            }
        }

        #endregion

        #region Class 

        private class Site
        {
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public bool CentaralSiteInd { get; set; }
            public int Col { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Type { get; set; }
            public string Group { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public string AcNo2 { get; set; }
            public decimal Amt { get; set; }
        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public string AcNo2 { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProfitLoss
        {
            public string CompanyName { set; get; }
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
            public string PrintBy { get; set; }
        }

        private class JournalPrint
        {
            public string AcDesc { get; set; }
            public decimal JumlahBesar { get; set; }
            public decimal JumlahLuarPusat { get; set; }
            public string Group { get; set; }
            public string Type { get; set; }
            public decimal Pusat { get; set; }
            public decimal Wil1 { get; set; }
            public decimal Wil2 { get; set; }
            public decimal Wil3 { get; set; }
            public decimal Wil4 { get; set; }
            public decimal Wil5 { get; set; }
            public decimal Wil6 { get; set; }
            public decimal DivGed { get; set; }
            public decimal DivInf { get; set; }
            public decimal DivKim { get; set; }
            public decimal DivEng { get; set; }
            public decimal DivPengBis { get; set; }
            public string AcNo { get; set; }
            public string AcNoParent { get; set; }
        }

        private class TotalNBS
        {
            public string Group { get; set; }
            public string Type { get; set; }
            public decimal TotJumlahBesar { get; set; }
            public decimal TotJumlahLuarPusat { get; set; }
            public decimal TotPusat { get; set; }
            public decimal TotWil1 { get; set; }
            public decimal TotWil2 { get; set; }
            public decimal TotWil3 { get; set; }
            public decimal TotWil4 { get; set; }
            public decimal TotWil5 { get; set; }
            public decimal TotWil6 { get; set; }
            public decimal TotDivGed { get; set; }
            public decimal TotDivInf { get; set; }
            public decimal TotDivKim { get; set; }
            public decimal TotDivEng { get; set; }
            public decimal TotDivPengBis { get; set; }
            public string AcNoParent { get; set; }
        }

        private class TotalAllNBSA
        {
            public string Group { get; set; }
            public string Type { get; set; }
            public decimal TotAllJumlahBesar { get; set; }
            public decimal TotAllJumlahLuarPusat { get; set; }
            public decimal TotAllPusat { get; set; }
            public decimal TotAllWil1 { get; set; }
            public decimal TotAllWil2 { get; set; }
            public decimal TotAllWil3 { get; set; }
            public decimal TotAllWil4 { get; set; }
            public decimal TotAllWil5 { get; set; }
            public decimal TotAllWil6 { get; set; }
            public decimal TotAllDivGed { get; set; }
            public decimal TotAllDivInf { get; set; }
            public decimal TotAllDivKim { get; set; }
            public decimal TotAllDivEng { get; set; }
            public decimal TotAllDivPengBis { get; set; }
        }

        private class TotalAllNBSP
        {
            public string Group { get; set; }
            public string Type { get; set; }
            public decimal TotAllJumlahBesar { get; set; }
            public decimal TotAllJumlahLuarPusat { get; set; }
            public decimal TotAllPusat { get; set; }
            public decimal TotAllWil1 { get; set; }
            public decimal TotAllWil2 { get; set; }
            public decimal TotAllWil3 { get; set; }
            public decimal TotAllWil4 { get; set; }
            public decimal TotAllWil5 { get; set; }
            public decimal TotAllWil6 { get; set; }
            public decimal TotAllDivGed { get; set; }
            public decimal TotAllDivInf { get; set; }
            public decimal TotAllDivKim { get; set; }
            public decimal TotAllDivEng { get; set; }
            public decimal TotAllDivPengBis { get; set; }
        }

        #endregion
    }
}
