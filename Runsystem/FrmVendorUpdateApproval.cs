﻿#region Update
/*
    02/07/2018 [WED] tambah ID Number untuk Contact Person
    03/07/2018 [WED] tambah Head Office
    24/09/2018 [WED] tambah Establishment Year
    30/11/2021 [NJP/RM] VendorSectorUpdate save subsector bukan qualification
    20/06/2022 [VIN/TWC] berdasarkan mIsUseECatalog=N yg di save qualification, Y -> subsector
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorUpdateApproval : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private iGRichTextManager fManager;
        internal bool mIsUseECatalog = false;

        #endregion

        #region Constructor

        public FrmVendorUpdateApproval(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetLue(LueFontSize, "10");
                SetGrd();
                GetParameter();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "Approve",

                    //1-5
                    "Cancel",
                    "",
                    "Vendor",
                    "Notes",
                    "Approver's Remark (Reason to cancel, etc)",
                }
            );

            fManager = new iGRichTextManager(Grd1);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 0, 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);

            Grd1.Cols[3].CellStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.SetGrdColHdrAutoAlign(Grd1);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.VdCode, Concat( ");
            SQL.AppendLine("  'Vendor Name : ', T.VdName, '\n', ");
            SQL.AppendLine("  'Short Name  : ', T.ShortName, '\n', ");
            SQL.AppendLine("  'City        : ', T.CityName ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.VdCode, T1.VdName, T1.ShortName, T2.CityName ");
            SQL.AppendLine("    From tblvendorupdate T1 ");
            SQL.AppendLine("    Inner Join TblCity T2 On T1.CityCode = T2.CityCode ");
            SQL.AppendLine("    Where T1.ApproveUserCode Is Null And T1.ApproveStatus Is Null ");

            if (TxtVdCode.Text.Length > 0)
            {
                SQL.AppendLine("    And (T1.VdCode Like '%" + TxtVdCode.Text + "%' Or  ");
                SQL.AppendLine("      T1.VdName Like '%" + TxtVdCode.Text + "%' Or ");
                SQL.AppendLine("      T1.ShortName Like '%" + TxtVdCode.Text + "%' ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ParValue From TblParameter Where ParCode = 'UserCodeForVendorUpdateApproval' ");
            SQL.AppendLine("    And ParValue Like '%#" + Gv.CurrentUserCode + "#%' Limit 1 ");
            SQL.AppendLine(") ");
            
            SQL.AppendLine(")T ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[] { "VdCode", "Remarks" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = ChkAutoChoose.Checked;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                        Grd.Cells[Row, 4].Value = Sm.DrStr(dr, 1);
                        Grd.Cells[Row, 4].Font = new Font("Lucida Console", 9);
                        Grd.Cells[Row, 5].Value = null;
                    }, true, false, true, true
                );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                var SQL = new StringBuilder();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0 && (Sm.GetGrdBool(Grd1, Row, 0) || Sm.GetGrdBool(Grd1, Row, 1)))
                    {
                        string mOldVdName = Sm.GetValue("Select VdName From TblVendor Where VdCode = '" + Sm.GetGrdStr(Grd1, Row, 3) + "'; ");
                        string mNewVdName = Sm.GetValue("Select VdName From TblVendorUpdate Where VdCode = '" + Sm.GetGrdStr(Grd1, Row, 3) + "'; ");

                        bool IsVdNameEdited = !Sm.CompareStr(mOldVdName, mNewVdName);
                        cml.Add(UpdateVendor(Row, IsVdNameEdited, mNewVdName));
                    }
                }

                Sm.ExecCommands(cml);
                Sm.ClearGrd(Grd1, true);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && BtnSave.Enabled)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVendorUpdateApprovalDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmVendorUpdateApprovalDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if ((e.ColIndex == 0 || e.ColIndex == 1))
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex))
                    Grd1.Cells[e.RowIndex, (e.ColIndex == 0) ? 1 : 0].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 5].Value = null;
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsUseECatalog = Sm.GetParameterBoo("IsUseECatalog");
        }


        private MySqlCommand UpdateVendor(int Row, bool IsVdNameEdited, string NewVdName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVendorUpdate ");
            SQL.AppendLine("  Set ApproveUserCode = @UserCode, ApproveStatus = @ApproveStatus, ApproveRemark = @ApproveRemark, ");
            SQL.AppendLine("  LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where VdCode = @VdCode And ApproveUserCode Is Null And ApproveStatus Is Null; ");

            if(Sm.GetGrdBool(Grd1, Row, 0))
            {
                SQL.AppendLine("Update TblVendor A ");
                SQL.AppendLine("Inner Join TblVendorUpdate B On A.VdCode = B.VdCode ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("        A.VdName=B.VdName, A.ShortName=B.ShortName, A.VdCtCode=B.VdCtCode, A.Address=B.Address, ");
                SQL.AppendLine("        A.VilCode=B.VilCode, A.SDCode=B.SDCode, A.CityCode=B.CityCode, A.PostalCd=B.PostalCd, ");
                SQL.AppendLine("        A.IdentityNo=B.IdentityNo, A.TIN=B.TIN, A.TaxInd=B.TaxInd, A.Phone=B.Phone, A.Fax=B.Fax, ");
                SQL.AppendLine("        A.Email=B.Email, A.Mobile=B.Mobile, A.CreditLimit=B.CreditLimit, A.Remark=B.Remark, A.Website=B.Website, ");
                SQL.AppendLine("        A.Parent=B.Parent, A.EstablishedYr = B.EstablishedYr, ");
                SQL.AppendLine("        A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where B.VdCode = @VdCode; ");

                SQL.AppendLine("Delete From TblVendorContactPerson Where VdCode=@VdCode; ");
                SQL.AppendLine("Delete From TblVendorBankAccount Where VdCode=@VdCode; ");
                SQL.AppendLine("Delete From TblVendorItemCategory Where VdCode=@VdCode; ");
                SQL.AppendLine("Delete From TblVendorSector Where VdCode=@VdCode; ");

                if (IsVdNameEdited)
                {
                    UpdateCOAVendorDesc(ref SQL, "VendorAcNoDownPayment", "VendorAcDescDownPayment");
                    UpdateCOAVendorDesc(ref SQL, "VendorAcNoAP", "VendorAcDescAP");
                    UpdateCOAVendorDesc(ref SQL, "VendorAcNoUnInvoiceAP", "VendorAcDescUnInvoiceAP");
                    UpdateCOAVendorDesc(ref SQL, "AcNoForGiroAP", "AcDescForGiroAP");
                }

                SQL.AppendLine("Insert Into TblVendorContactPerson(VdCode, DNo, ContactPersonName, IDNumber, Position, ContactNumber, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.VdCode, A.DNo, A.ContactPersonName, A.IDNumber, A.Position, A.ContactNumber, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblVendorContactPersonUpdate A ");
                SQL.AppendLine("Where A.VdCode = @VdCode; ");
                
                SQL.AppendLine("Insert Into TblVendorBankAccount(VdCode, DNo, BankCode, BankBranch, BankAcNo, BankAcName, CreateBy, CreateDt) ");
                SQL.AppendLine("Select B.VdCode, B.DNo, B.BankCode, B.BankBranch, B.BankAcNo, B.BankAcName, B.CreateBy, B.CreateDt ");
                SQL.AppendLine("From TblVendorBankAccountUpdate B ");
                SQL.AppendLine("Where B.VdCode = @VdCode; ");

                SQL.AppendLine("Insert Into TblVendorItemCategory(VdCode, DNo, ItCtCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select C.VdCode, C.DNo, C.ItCtCode, C.CreateBy, C.CreateDt ");
                SQL.AppendLine("From TblVendorItemCategoryUpdate C ");
                SQL.AppendLine("Where C.VdCode = @VdCode; ");

                SQL.AppendLine("Insert Into TblVendorSector(VdCode, DNo, SectorCode, ");
                if (mIsUseECatalog) SQL.AppendLine("SubSectorCode, ");
                else SQL.AppendLine("QualificationCode, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select D.VdCode, D.DNo, D.SectorCode, ");
                if (mIsUseECatalog) SQL.AppendLine("D.SubSectorCode, ");
                else SQL.AppendLine("D.QualificationCode, ");
                SQL.AppendLine("D.CreateBy, D.CreateDt ");
                SQL.AppendLine("From TblVendorSectorUpdate D ");
                SQL.AppendLine("Where D.VdCode = @VdCode; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@VdName", NewVdName);
            Sm.CmParam<String>(ref cm, "@ApproveStatus", Sm.GetGrdBool(Grd1, Row, 0) ? "A" : "C");
            Sm.CmParam<String>(ref cm, "@ApproveRemark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void UpdateCOAVendorDesc(ref StringBuilder SQL, string AcNo, string AcDesc)
        {
            SQL.AppendLine("Update TblCOA A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='" + AcNo + "' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='" + AcDesc + "' And C.ParValue Is Not Null ");
            SQL.AppendLine("Set A.AcDesc=Concat(C.ParValue, ' ', @VdName), A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.AcNo=Concat(B.ParValue, @VdCode); ");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        private void ChkAutoChoose_CheckedChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < Grd1.Rows.Count; row++)
                if (Sm.GetGrdStr(Grd1, row, 3).Length != 0)
                    Grd1.Cells[row, 0].Value = ChkAutoChoose.Checked;
        }

        #endregion

        #endregion

    }
}
