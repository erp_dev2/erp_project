﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptLoadingQueue : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptLoadingQueue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            //Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -4);
            SetLueStatus(ref LueStatus);
            Sm.SetLue(LueStatus, "I");
            Sl.SetLueLoadingItemCat(ref LueItem);
            Sl.SetLueLoadingArea(ref LueAreaBongkar);

            base.FrmLoad(sender, e);
            BtnChart.Visible = (Sm.GetValue("Select IfNull(ParValue, '') from tblparameter where ParCode='ChartAvailable'") == "Yes");
        }

        public static void SetLueStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'I' As Col1, 'Timbangan ke-1' As Col2 " +
                "Union All " +
                "Select 'O' As Col1, 'Timbangan ke-2' As Col2 ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Case When A.Status='I' Then 'In' When A.Status='O' then 'Out' ");
            SQL.AppendLine("When A.Status='C' then 'Cancelled' end As StatusDesc, A.DocDateBf, ");
            SQL.AppendLine("Case When A.Status='I' Then 'Timbangan ke-1' When A.Status='O' then 'Timbangan ke-2' When A.Status='C' then 'Cancelled' end As QueueType,  ");
            SQL.AppendLine("D.TTName, A.LicenceNo, A.DrvName, C.ItName, B.LoadAreaName, A.WeightBf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateBf, 8), 2), '/', Right(Left(A.DocDateBf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateBf, 4), ' ', Left(A.DocTimeBf, 2), ':', Right(A.DocTimeBf, 2)) As WeightBfDt, ");
            SQL.AppendLine("A.WeightAf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateAf, 8), 2), '/', Right(Left(A.DocDateAf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateAf, 4), ' ', Left(A.DocTimeAf, 2), ':', Right(A.DocTimeAf, 2)) As WeightAfDt, ");
            SQL.AppendLine("A.Status, A.Netto, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("Case When (A.DocDateAf Is Null Or A.DocTimeAf Is Null) Then Null Else ");
            SQL.AppendLine("CONCAT( ");
            SQL.AppendLine("    Case When FLOOR(HOUR(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)), Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) ) ) / 24) = 0 Then '' Else ");
            SQL.AppendLine("        Concat(FLOOR(HOUR(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)), Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) ) ) / 24), ' hari ') End, ");
            SQL.AppendLine("    Case When MOD(HOUR(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)),Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) )), 24) = 0 Then '' Else ");
            SQL.AppendLine("        Concat(MOD(HOUR(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)),Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) )), 24), ' jam ') End, ");
            SQL.AppendLine("    Case When MINUTE(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)),Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) ))=0 Then '' Else ");
            SQL.AppendLine("        Concat(MINUTE(TIMEDIFF(Concat(Left(DocDateAf, 4), '-', Substring(DocDateAf, 5, 2), '-', Right(DocDateAf, 2), ' ', Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)),Concat(Left(DocDateBf, 4), '-', Substring(DocDateBf, 5, 2), '-', Right(DocDateBf, 2), ' ', Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) )), ' menit') End) ");
            SQL.AppendLine("End As TimeDifference ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join (Select OptCode As LoadArea, OptDesc As LoadAreaName from tblOption where OptCat='LoadingArea') B On A.LoadArea=B.LoadArea ");
            SQL.AppendLine("Left Join (Select OptCode As ItCat, OptDesc As ItName from tblOption where OptCat='LoadingItemCat') C On A.ItCat=C.ItCat ");
            SQL.AppendLine("Left Join TblTransportType D On A.TTCode=D.TTCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Jenis"+Environment.NewLine+"Antrian",  
                        "Nomor"+Environment.NewLine+"Antrian", 
                        "Status", 
                        "Status"+Environment.NewLine+"Antrian",   
                        "Tipe"+Environment.NewLine+"Ttransportasi",   
                           
                        
                        //6-10
                        "Nomor"+Environment.NewLine+"Polisi",
                        "Pengemudi",   
                        "Area"+Environment.NewLine+"Bongkar", 
                        "Berat Sebelum"+Environment.NewLine+"Bongkar",   
                        "Tanggal/Waktu Sebelum"+Environment.NewLine+"Bongkar",  
                        
                        //11-15
                        "Berat Setelah"+Environment.NewLine+"Bongkar",   
                        "Tanggal/Waktu Setelah"+Environment.NewLine+"Bongkar",  
                        "Selisih Waktu"+Environment.NewLine+"Sebelum Dan Setelah Bongkar",
                        "Berat"+Environment.NewLine+"Bersih",   
                        "Created"+Environment.NewLine+"By", 
                        
                        //16-20
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 12, 14, 15, 16, 17, 18, 19, 20 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12, 14, 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDateBf");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "A.Status", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLicenceNo.Text, "A.LicenceNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAreaBongkar), "A.LoadArea", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItem), "A.ItCat", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "StatusDesc", "QueueType", "TTName", "LicenceNo", "DrvName", 
                            
                            //6-10
                            "ItName", "LoadAreaName", "WeightBf", "WeightBfDt", "WeightAf", 
                            
                            //11-15
                            "WeightAfDt", "TimeDifference", "Netto", "CreateBy", "CreateDt",   
                            
                            //16-18
                            "LastUpBy", "LastUpDt", "Status"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            if (Sm.GetGrdStr(Grd1, Row, 3) == "Cancelled")
                                Grd1.Rows[Row].ForeColor = Color.Red;
                            else
                                if (Sm.GetGrdStr(Grd1, Row, 3) == "In")
                                    Grd1.Rows[Row].ForeColor = Color.Purple;
                                else
                                    Grd1.Rows[Row].ForeColor = Color.Black;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 20, 17);
                        }, true, true, false, true
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                    9,11,13
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Antrian");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tanggal Antrian");
        }

        private void ChkAreaBongkar_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Area Bongkar");
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void ChkLicenceNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Polisi");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtLicenceNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueAreaBongkar_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAreaBongkar, new Sm.RefreshLue1(Sl.SetLueLoadingArea));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItem, new Sm.RefreshLue1(Sl.SetLueLoadingItemCat));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Jenis Antrian");
        }

        #endregion
    }
}
