﻿#region Update
/*
    12/07/2020 [TKG/IMS] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptGLProject : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptGLProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueAcNo(ref LueCOAAc);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, A.JnDesc, C.Parent, D.AcDesc As ParentDesc,  ");
            SQL.AppendLine("B.AcNo, C.AcDesc, C.Alias, C.AcType, ");
            SQL.AppendLine("B.DAmt, B.CAmt, B.DAmt+B.CAmt As Amt, ");
            SQL.AppendLine("E.DocNo As SOContractDocNo, H.ProjectCode, H.ProjectName, E.PONo, ");
            SQL.AppendLine("A.MenuCode, A.MenuDesc, A.Remark As RemarkH, B.Remark As RemarkD ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Left Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Left Join TblCOA D On C.Parent=D.AcNo ");
            SQL.AppendLine("Left Join TblSOContractHdr E ");
            SQL.AppendLine("    On Case When Length(IfNull(A.Remark, ''))<18 Then '' Else Right(A.Remark, 18) End=E.DocNo ");
            SQL.AppendLine("Left Join TblBOQHdr F On E.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr G On F.LOPDocNo=G.DocNO ");
            SQL.AppendLine("Left Join TblProjectGroup H On G.PGCode=H.PGCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.DocDt, A.DocNo, B.AcNo; ");


            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Date",
                        "Document#", 
                        "Description",
                        "Parent#",
                        "Parent Description",

                        //6-10
                        "Account#",
                        "Account Description",
                        "Alias",
                        "Type",
                        "Debit",
                        
                        //11-15
                        "Credit",
                        "Amount",
                        "Balance",
                        "SO Contract#",
                        "Project's Code",

                        //16-20
                        "Project's Name",
                        "Customer's PO#",
                        "Menu Code",
                        "Menu Description",
                        "Remark Header",

                        //21
                        "Remark Detail"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        80, 150, 200, 120, 200, 
                        //6-10
                        130, 200, 130, 100, 130, 
                        //11-15
                        130, 130, 130, 130, 100, 
                        //16-20
                        200, 130, 120, 250, 250, 
                        //21
                        250
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                if (TxtAcNo.Text.Length != 0 && Sm.GetLue(LueCOAAc).Length != 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Data can't be filtered by account# and COA's account simultaneously.");
                    TxtAcNo.Focus();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "C.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "H.ProjectCode", "H.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, "E.PONo", false);
                if (ChkSOContractDocNo.Checked) Filter += " And E.DocNo Like '%" + TxtSOContractDocNo.Text.Replace("'", "") + "%' ";
                if (ChkCOAAc.Checked) Filter += " And B.AcNo Like '" + Sm.GetLue(LueCOAAc).Replace("'", "") + "%' ";
                if (ChkAlias.Checked) Filter += " And C.Alias Like '" + TxtAlias.Text.Replace("'", "") + "%' ";
                if (ChkAcNo.Checked) Filter += " And B.AcNo Like '" + TxtAcNo.Text.Replace("'", "") + "%' ";

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[]
                    {
                        //0
                        "DocDt",

                        //1-5
                        "DocNo", "JnDesc", "Parent", "ParentDesc", "AcNo", 

                        //6-10
                        "AcDesc", "Alias", "AcType", "DAmt", "CAmt", 
                        
                        //11-15
                        "Amt", "SOContractDocNo", "ProjectCode", "ProjectName", "PONo", 
                        
                        //16-19
                        "MenuCode", "MenuDesc", "RemarkH", "RemarkD"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                            Grd1.Cells[Row, 13].Value = Sm.GetGrdDec(Grd1, Row, 10) - Sm.GetGrdDec(Grd1, Row, 11);
                        else
                            Grd1.Cells[Row, 13].Value = Sm.GetGrdDec(Grd1, Row, 11) - Sm.GetGrdDec(Grd1, Row, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    }, true, false, false, false
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 13 });
                Grd1.EndUpdate();
             }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
  
        }

        private void Bal()
        {
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
            {
                if (Grd1.Rows[row].Type == iGRowType.Normal)
                    Grd1.Cells[row, 12].ForeColor = Color.Transparent;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }        

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account Description");
        }

        private void LueCOAAc_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCOAAc, new Sm.RefreshLue1(Sl.SetLueAcNo));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCOAAc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "COA's account");
        }

        private void ChkAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Alias");
        }

        private void TxtAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO contract#");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer's PO#");
        }

        #endregion

        #endregion
    }
}
