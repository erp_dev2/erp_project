﻿#region Update
/*
    28/01/2018 [TKG] Monthly movement
    18/01/2021 [DITA/IMS] tambah kolom specification + local code berdasarkan param : IsBOMShowSpecifications
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem   
{
    public partial class FrmRptMonthlyStockMovement : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;
        int mNumberOfInventoryUomCode = 1, mCol = 20;
        List<TransType> mlTransType = new List<TransType>();
        internal bool mIsBOMShowSpecifications = false;
   
        #endregion

        #region Constructor

        public FrmRptMonthlyStockMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();

                var CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueItCtCode(ref LueItCtCode);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.Header.Rows.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Category",
                        "Group",
                        
                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "UoM",
                        
                        //11-12
                        "UoM",
                        "UoM",
                        "Item's Local"+Environment.NewLine+"Code",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        180, 90, 200, 180, 180, 
                        
                        //6-10
                        150, 180, 60, 60, 80, 

                        //11-14
                        80, 80, 100, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 11, 12 }, false);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 13, 14});
            Grd1.Cols[13].Move(3);
            Grd1.Cols[14].Move(5);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);

            for (int C=0; C<=mCol-6; C++)
                Grd1.Header.Cells[0, C].SpanRows = 3;

            SetGrdCol("Opening", 15);
            SetGrdCol("Balance", 18);

            ProcessTransType(ref mlTransType);

            if (mlTransType.Count > 0)
            {
                Grd1.Cols.Count += (mlTransType.Count * 3);

                int c = mCol + 1;
                Grd1.Header.Cells[2, c].Value = "In";
                Grd1.Header.Cells[2, c].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[2, c].SpanCols = (mlTransType.Count / 2) * 3;
                Grd1.Header.Cells[2, c].BackColor = Color.LightGreen;

                c = mCol + ((mlTransType.Count / 2) * 3) + 1;
                Grd1.Header.Cells[2, c].Value = "Out";
                Grd1.Header.Cells[2, c].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[2, c].SpanCols = (mlTransType.Count / 2) * 3;
                Grd1.Header.Cells[2, c].BackColor = Color.Pink;

                c = mCol + 1;
                for (int i = 0; i < mlTransType.Count; i++)
                {
                    Grd1.Header.Cells[1, c].Value = mlTransType[i].Name;
                    Grd1.Header.Cells[1, c].TextAlign = iGContentAlignment.TopCenter;
                    Grd1.Header.Cells[1, c].SpanCols = 3;
                    for (int j = 0; j < 3; j++)
                    {
                        Grd1.Header.Cells[0, c + j].Value = "Quantity";
                        Grd1.Header.Cells[0, c + j].TextAlign = iGContentAlignment.TopCenter;

                        if (mNumberOfInventoryUomCode == 1)
                        {
                            if (j == 1 || j == 2) Grd1.Cols[c + j].Visible = false;
                        }

                        if (mNumberOfInventoryUomCode == 2)
                        {
                            if (j == 2) Grd1.Cols[c + j].Visible = false;
                        }
                    }
                    c += 3;
                }
            }
            string FormatDec = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            for (int i = mCol-5; i < Grd1.Cols.Count; i++)
                GrdFormatDec(i, FormatDec);

            if (mNumberOfInventoryUomCode == 1)
            {
                Grd1.Cols[16].Visible = false;
                Grd1.Cols[17].Visible = false;
                Grd1.Cols[19].Visible = false;
                Grd1.Cols[20].Visible = false;
            }

            if (mNumberOfInventoryUomCode == 2)
            {
                Grd1.Cols[17].Visible = false;
                Grd1.Cols[20].Visible = false;
            }
            Sm.SetGrdProperty(Grd1, true);
        }

        private void GrdFormatDec(int c, string FormatDec)
        {
            Grd1.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
            Grd1.Cols[c].CellStyle.FormatString = FormatDec;
            Grd1.Cols[c].CellStyle.ValueType = typeof(decimal);
        }
        
        private void SetGrdCol(string Title, int c)
        {
            Grd1.Header.Cells[2, c].Value = Title;
            Grd1.Header.Cells[2, c].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[2, c].SpanCols = 3;
            for (int i=c;i<=c+2; i++)
            {
                Grd1.Header.Cells[0, i].Value = "Qty";
                Grd1.Header.Cells[0, i].SpanRows = 2;
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueMth, "Month")) return;
            if (Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var lItem = new List<Item>();
            var lMovement = new List<Movement>();
            var lItemList = new List<ItemList>();

            try
            {
                Process1(ref lItem);
                Process2(ref lMovement, ref lItem);
                if (lMovement.Count > 0)
                {
                    Process3(ref lMovement, ref lItem, ref lItemList);
                    Process4(ref lMovement);
                    Process5(ref lMovement);
                    Process6();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lItem.Clear();
                lMovement.Clear();
                lItemList.Clear();

                Grd1.Cols.AutoWidth();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessTransType(ref List<TransType> l)
        {
            l.Clear();

            byte x = 0;
            int C = mCol+1;
            var l2 = new List<TransType>();
            var cm = new MySqlCommand();
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select OptCode, OptDesc From TblOption Where OptCat='InventoryTransType' Order By OptCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l2.Add(new TransType()
                        {
                            Code = Sm.DrStr(dr, c[0]),
                            Name = Sm.DrStr(dr, c[1])
                        });
                }
                dr.Close();
            }
            if (l2.Count > 0)
            {
                while (x < 2)
                {
                    for (int i = 0; i < l2.Count - 1; i++)
                    {
                        l.Add(new TransType()
                        {
                            Code = l2[i].Code,
                            Name = l2[i].Name,
                            MinusInd = x,
                            Col = C
                        });
                        C += 3;
                    }
                    x += 1;
                }
                l2.Clear();
            }
        }

        private void Process1(ref List<Item> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;

            if (TxtItCode.Text.Length > 0)
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItCtname, Concat(C.ItGrpName, ' (', C.ItGrpCode, ')') As ItGrpName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItCodeInternal, A.Specification  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join TblItemGroup C On A.ItGrpCode=C.ItGrpCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "ItCode", 
                    "ItName", "ItCtName", "ItGrpName", "InventoryUomCode", "InventoryUomCode2", 
                    "InventoryUomCode3" , "ItCodeInternal", "Specification"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Item()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItName = Sm.DrStr(dr, c[1]),
                            ItCtName = Sm.DrStr(dr, c[2]),
                            ItGrpName = Sm.DrStr(dr, c[3]),
                            InventoryUomCode = Sm.DrStr(dr, c[4]),
                            InventoryUomCode2 = Sm.DrStr(dr, c[5]),
                            InventoryUomCode3 = Sm.DrStr(dr, c[6]),
                            ItCodeInternal = Sm.DrStr(dr, c[7]),
                            Specification = Sm.DrStr(dr, c[8]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Movement> l, ref List<Item> l2)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var Dt1 = string.Empty;
            var Dt2 = string.Empty;
            var Mth = Sm.GetLue(LueMth);
            var Yr = Sm.GetLue(LueYr);
            int[] c;

            Dt1 = string.Concat(Yr, Mth, "01");
            if (Mth == "12")
            {
                Mth = "01";
                Yr = (int.Parse(Yr) + 1).ToString();
            }
            else
                Mth = Sm.Right("0"+(int.Parse(Mth) + 1).ToString(), 2);

            Dt2 = string.Concat(Yr, Mth, "01");

            
            if (l2.Count>0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(ItCode=@ItCode" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode" + i.ToString(), l2[i].ItCode);
                }
                if (Filter.Length > 0) Filter = " And (" + Filter + ") ";
            }
            else
                Filter = " And 0=1 ";
        

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);

            SQL.AppendLine("Select T1.Type, T1.DocType, T1.Source, T1.Lot, T1.Bin, ");
            SQL.AppendLine("T1.Qty, ");
            if (mNumberOfInventoryUomCode >= 2)
                SQL.AppendLine("T1.Qty2, ");
            if (mNumberOfInventoryUomCode == 3)
                SQL.AppendLine("T1.Qty3, ");
            SQL.AppendLine("T2.ItCode, T2.BatchNo ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select '1' As Type, '00' As DocType, Source, Lot, Bin, ");
            SQL.AppendLine("Sum(Qty) As Qty ");
            if (mNumberOfInventoryUomCode>=2)
                SQL.AppendLine(", Sum(Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode==3)
                SQL.AppendLine(", Sum(Qty3) As Qty3 ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And DocDt<@Dt1 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By Source, Lot, Bin ");
            SQL.AppendLine("Having Sum(Qty)<>0 ");
            SQL.AppendLine("union All ");
            SQL.AppendLine("Select '2' As Type, DocType, Source, Lot, Bin, ");
            SQL.AppendLine("Sum(Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2)
                SQL.AppendLine(", Sum(Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode == 3)
                SQL.AppendLine(", Sum(Qty3) As Qty3 ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And DocDt>=@Dt1 And DocDt<@Dt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By DocType, Source, Lot, Bin ");
            SQL.AppendLine("Having Sum(Qty)<>0 ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            SQL.AppendLine("Order By T1.Type, T1.DocType, T2.ItCode, T2.BatchNo, T1.Lot, T1.Bin;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();

                if (mNumberOfInventoryUomCode == 1)
                {
                    c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Type", 
                        //1-5
                        "DocType", "ItCode", "BatchNo", "Source", "Lot", 
                        //6-7
                        "Bin", "Qty"
                    });
                }
                else
                {
                    if (mNumberOfInventoryUomCode == 2)
                    {
                        c = Sm.GetOrdinal(dr, new string[] 
                        { 
                            //0
                            "Type", 
                            //1-5
                            "DocType", "ItCode", "BatchNo", "Source", "Lot", 
                            //6-8
                            "Bin", "Qty", "Qty2"
                        });
                    }
                    else
                    {
                        c = Sm.GetOrdinal(dr, new string[] 
                        { 
                            //0
                            "Type", 
                            //1-5
                            "DocType", "ItCode", "BatchNo", "Source", "Lot", 
                            //6-9
                            "Bin", "Qty", "Qty2", "Qty3" 
                        });
                    }
                }
                if (dr.HasRows)
                {
                    if (mNumberOfInventoryUomCode == 1)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Movement()
                            {
                                Type = Sm.DrStr(dr, c[0]),
                                TransType = Sm.DrStr(dr, c[1]),
                                ItCode = Sm.DrStr(dr, c[2]),
                                BatchNo = Sm.DrStr(dr, c[3]),
                                Source = Sm.DrStr(dr, c[4]),
                                Lot = Sm.DrStr(dr, c[5]),
                                Bin = Sm.DrStr(dr, c[6]),
                                Qty = Sm.DrDec(dr, c[7])
                            });
                        }
                    }

                    if (mNumberOfInventoryUomCode == 2)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Movement()
                            {
                                Type = Sm.DrStr(dr, c[0]),
                                TransType = Sm.DrStr(dr, c[1]),
                                ItCode = Sm.DrStr(dr, c[2]),
                                BatchNo = Sm.DrStr(dr, c[3]),
                                Source = Sm.DrStr(dr, c[4]),
                                Lot = Sm.DrStr(dr, c[5]),
                                Bin = Sm.DrStr(dr, c[6]),
                                Qty = Sm.DrDec(dr, c[7]),
                                Qty2 = Sm.DrDec(dr, c[8])
                            });
                        }
                    }

                    if (mNumberOfInventoryUomCode == 3)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Movement()
                            {
                                Type = Sm.DrStr(dr, c[0]),
                                TransType = Sm.DrStr(dr, c[1]),
                                ItCode = Sm.DrStr(dr, c[2]),
                                BatchNo = Sm.DrStr(dr, c[3]),
                                Source = Sm.DrStr(dr, c[4]),
                                Lot = Sm.DrStr(dr, c[5]),
                                Bin = Sm.DrStr(dr, c[6]),
                                Qty = Sm.DrDec(dr, c[7]),
                                Qty2 = Sm.DrDec(dr, c[8]),
                                Qty3 = Sm.DrDec(dr, c[9])
                            });
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Movement> l, ref List<Item> l2, ref List<ItemList> l3)
        {
            l3.Clear();
            foreach (var m in l.Select(x => new { x.ItCode, x.BatchNo, x.Source, x.Lot, x.Bin })
                .Distinct()
                .OrderBy(x => x.ItCode)
                .ThenBy(x => x.BatchNo)
                .ThenBy(x => x.Source)
                .ThenBy(x => x.Lot)
                .ThenBy(x => x.Bin)
                )
                l3.Add(new ItemList()
                    {
                        ItCode = m.ItCode,
                        BatchNo = m.BatchNo,
                        Source = m.Source,
                        Lot = m.Lot,
                        Bin = m.Bin
                    });

            int Index = 0;
            for (int i = 0; i < l3.Count; i++)
            {
                for (int j = Index; j < l2.Count; j++)
                {
                    if (Sm.CompareStr(l3[i].ItCode, l2[j].ItCode))
                    {
                        l3[i].ItName = l2[j].ItName;
                        l3[i].ItCtName = l2[j].ItCtName;
                        l3[i].ItGrpName = l2[j].ItGrpName;
                        l3[i].InventoryUomCode = l2[j].InventoryUomCode;
                        l3[i].InventoryUomCode2 = l2[j].InventoryUomCode2;
                        l3[i].InventoryUomCode3 = l2[j].InventoryUomCode3;
                        l3[i].ItCodeInternal = l2[j].ItCodeInternal;
                        l3[i].Specification = l2[j].Specification;
                        Index = j;
                        break;
                    }
                }
            }

            int r = 0;
            var WhsName = LueWhsCode.GetColumnValue("Col2");

            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            foreach (ItemList i in l3
                .OrderBy(x => x.ItCode)
                .ThenBy(x => x.Lot)
                .ThenBy(x => x.Bin)
                )
            {
                Grd1.Rows.Add();
                Grd1.Cells[r, 0].Value = r + 1;
                Grd1.Cells[r, 1].Value = WhsName;
                Grd1.Cells[r, 2].Value = i.ItCode;
                Grd1.Cells[r, 3].Value = i.ItName;
                Grd1.Cells[r, 4].Value = i.ItCtName;
                Grd1.Cells[r, 5].Value = i.ItGrpName;
                Grd1.Cells[r, 6].Value = i.BatchNo;
                Grd1.Cells[r, 7].Value = i.Source;
                Grd1.Cells[r, 8].Value = i.Lot;
                Grd1.Cells[r, 9].Value = i.Bin;
                Grd1.Cells[r, 10].Value = i.InventoryUomCode;
                Grd1.Cells[r, 11].Value = i.InventoryUomCode2;
                Grd1.Cells[r, 12].Value = i.InventoryUomCode3;
                Grd1.Cells[r, 13].Value = i.ItCodeInternal;
                Grd1.Cells[r, 14].Value = i.Specification;
                for (int c = mCol - 5; c < Grd1.Cols.Count; c++)
                    Grd1.Cells[r, c].Value = 0m;
                r++;
            }
            Grd1.EndUpdate();
        }

        private void Process4(ref List<Movement> l)
        {
            int Index = 0;
            
            Grd1.BeginUpdate();
            foreach (var x in l.Where(m => string.Compare(m.Type, "1") == 0)
                .OrderBy(o => o.ItCode)
                .ThenBy(o => o.BatchNo)
                .ThenBy(o => o.Source)
                .ThenBy(o => o.Lot)
                .ThenBy(o => o.Bin)
                )
            {
                for (int i = Index; i < Grd1.Rows.Count; i++)
                {
                    if (
                        Sm.CompareStr(x.ItCode, Sm.GetGrdStr(Grd1, i, 2)) &&
                        Sm.CompareStr(x.BatchNo, Sm.GetGrdStr(Grd1, i, 6)) &&
                        Sm.CompareStr(x.Source, Sm.GetGrdStr(Grd1, i, 7)) &&
                        Sm.CompareStr(x.Lot, Sm.GetGrdStr(Grd1, i, 8)) &&
                        Sm.CompareStr(x.Bin, Sm.GetGrdStr(Grd1, i, 9))
                        )
                    {
                        Grd1.Cells[i, 15].Value = x.Qty;
                        Grd1.Cells[i, 16].Value = x.Qty2;
                        Grd1.Cells[i, 17].Value = x.Qty3;
                        Index = i;
                        break;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void Process5(ref List<Movement> l)
        {
            int c = 0;
            string 
                ItCode = string.Empty, 
                BatchNo = string.Empty, 
                Source = string.Empty, 
                Lot = string.Empty, 
                Bin = string.Empty;

            Grd1.BeginUpdate();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 2);
                BatchNo = Sm.GetGrdStr(Grd1, r, 6);
                Source = Sm.GetGrdStr(Grd1, r, 7);
                Lot = Sm.GetGrdStr(Grd1, r, 8);
                Bin = Sm.GetGrdStr(Grd1, r, 9);
                foreach (var x in l
                    .Where(m => 
                        string.Compare(m.Type, "2") == 0 &&
                        string.Compare(m.ItCode, ItCode) == 0 &&
                        string.Compare(m.BatchNo, BatchNo) == 0 &&
                        string.Compare(m.Source, Source) == 0 &&
                        string.Compare(m.Lot, Lot) == 0 &&
                        string.Compare(m.Bin, Bin) == 0 
                        ))
                {
                    c = -1;
                    if (x.Qty>0)
                    {
                        foreach (var t in mlTransType.Where(w => w.Code == x.TransType && w.MinusInd == 0))
                        {
                            c = t.Col;
                            break;
                        }
                        if (c != -1)
                        {
                            Grd1.Cells[r, c].Value = x.Qty;
                            Grd1.Cells[r, c+1].Value = x.Qty2;
                            Grd1.Cells[r, c+2].Value = x.Qty3;
                        }
                    }
                    if (x.Qty<0)
                    {
                        foreach (var t in mlTransType.Where(w => w.Code == x.TransType && w.MinusInd == 1))
                        {
                            c = t.Col;
                            break;
                        }
                        if (c != -1)
                        {
                            Grd1.Cells[r, c].Value = x.Qty;
                            Grd1.Cells[r, c+1].Value = x.Qty2;
                            Grd1.Cells[r, c+2].Value = x.Qty3;
                        }
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void Process6()
        {
            var Balance = 0m;
            int c = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    c = mCol + j;
                    Balance = Sm.GetGrdDec(Grd1, i, mCol-6+j);
                    while (c < Grd1.Cols.Count)
                    {
                        Balance += Sm.GetGrdDec(Grd1, i, c);
                        c += 3;
                    }
                    Grd1.Cells[i, mCol-3+j].Value = Balance;
                }
            }

            bool IsExist = false;
            for (int i = mCol + 1; i < Grd1.Cols.Count; i++)
            {
                IsExist = false;

                for (int j = 0; j < Grd1.Rows.Count; j++)
                {
                    if (Sm.GetGrdDec(Grd1, j, i) != 0)
                    {
                        IsExist = true;
                        break;
                    }
                }
                Grd1.Cols[i].Visible = IsExist;
            }
        }

        #endregion
       
        #endregion

        #region Event

        private void FrmRptMonthlyStockMovement_FormClosing(object sender, FormClosingEventArgs e)
        {
            mlTransType.Clear();
        }

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion
       
        #endregion

        #region Class

        private class TransType
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public byte MinusInd { get; set; }
            public int Col { get; set; }
        }

        private class Movement
        {
            public string Type { get; set; }
            public string TransType { get; set; }
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
        }

        private class ItemList
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCtName { get; set; }
            public string ItGrpName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
        }

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCtName { get; set; }
            public string ItGrpName { get; set; } 
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
        }

        #endregion
    }
}
