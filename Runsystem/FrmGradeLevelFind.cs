﻿#region Update
/*
    10/09/2017 [TKG] tambah level
    23/07/2020 [WED/SIER] tambah ActInd berdasarkan parameter IsGradeLevelUseActInd
    12/08/2021 [IBL/PHT] tambah Sequence berdasarkan parameter IsGradeLevelUseSequence
    12/01/2022 [TKG/IMS] tambah other ss salary
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmGradeLevelFind : RunSystem.FrmBase2
    {
        #region Field

       private FrmGradeLevel mFrmParent;
       private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGradeLevelFind(FrmGradeLevel FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.GrdLvlCode, A.GrdLvlName, C.GrdLvlGrpName, A.ActInd, E.LevelName, ");
            SQL.AppendLine("A.BasicSalary, A.BasicSalary2, A.HealthSSSalary, A.EmploymentSSSalary, ");
            if (mFrmParent.mIsGradeLevelOtherSSSalaryEnabled)
                SQL.AppendLine("A.OtherSSSalary, ");
            else
                SQL.AppendLine("Null As OtherSSSalary, ");
            SQL.AppendLine("A.TaxSalary, D.ADName, A.SeqNo, B.Amt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblGradeLevelHdr A ");
            SQL.AppendLine("Left Join TblGradeLevelDtl B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Left Join TblGradeLevelGroup C On A.GrdLvlGrpCode=C.GrdLvlGrpCode ");
            SQL.AppendLine("Left Join TblAllowanceDeduction D On B.ADCode=D.ADCode ");
            SQL.AppendLine("Left Join TblLevelHdr E On A.LevelCode=E.LevelCode ");

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5 
                        "Code",
                        "Name",
                        "Active",
                        "Group",
                        "Level",                        
                        
                        //6-10
                        "Monthly"+Environment.NewLine+"Salary",
                        "Daily"+Environment.NewLine+"Salary",
                        "Health SS",
                        "Employment SS",
                        "Other SS", 
                        
                        //11-15
                        "Tax",
                        "Allowance/Deduction",
                        "Amount",
                        "Sequence",
                        "Created By",
                        
                        //16-20
                        "Created Date",
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 180, 80, 180, 180, 
                        
                        //6-10
                        120, 120, 120, 120, 150, 
                        
                        //11-15
                        150, 180, 150, 100, 200, 150,
                        
                        //16-20
                        150, 200, 150, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, false);
            if (!mFrmParent.mIsGradeLevelUseActInd) Sm.GrdColInvisible(Grd1, new int[] { 3 });
            if (!mFrmParent.mIsGradeLevelUseSequence) Sm.GrdColInvisible(Grd1, new int[] { 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);  
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtGrdLvlCode.Text, new string[] { "A.GrdLvlCode", "A.GrdLvlName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.GrdLvlName;",
                        new string[]
                        {
                              //0
                             "GrdLvlCode",
                             
                             //1-5
                             "GrdLvlName",
                             "ActInd",
                             "GrdLvlGrpName",
                             "LevelName",
                             "BasicSalary",
                             
                             //6-10
                             "BasicSalary2",
                             "HealthSSSalary",
                             "EmploymentSSSalary",
                             "OtherSSSalary",
                             "TaxSalary",

                             //11-15
                             "ADName",
                             "Amt",
                             "SeqNo",
                             "CreateBy",
                             "CreateDt",

                             //16-17
                             "LastUpBy",
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 5].Value = 0;
                this.Hide();
            }
        }

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtGrdLvlCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGrdLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Grade");
        }
        #endregion

        #endregion
       
    }
}
