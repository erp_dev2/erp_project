﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptItemPnt : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptItemPnt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-14);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T5.UserName, T1.DocDt, T2.DocName, T3.DeptName, ");
            SQL.AppendLine("T1.ItCode, T4.ItName, T1.BatchNo, T1.Qty, T4.PlanningUomCode, T1.Qty2, T4.PlanningUomCode2 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.CreateBy, T.DocDt, T.WorkCenterDocNo, T.ItCode, T.BatchNo, ");
            SQL.AppendLine("    Sum(T.Qty) Qty, Sum(T.Qty2) Qty2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.CreateBy, A.DocDt, A.WorkCenterDocNo, C.ItCode, C.BatchNo, B.Qty, B.Qty2 ");
            SQL.AppendLine("        From TblPntHdr A ");
            SQL.AppendLine("        Inner Join TblPntDtl5 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("        Where A.CancelInd='N' And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.CreateBy, A.DocDt, A.WorkCenterDocNo, C.ItCode, C.BatchNo, B.Qty, B.Qty2 ");
            SQL.AppendLine("        From TblPnt2Hdr A ");
            SQL.AppendLine("        Inner Join TblPnt2Dtl6 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("        Where A.CancelInd='N' And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.CreateBy, T.DocDt, T.WorkCenterDocNo, T.ItCode, T.BatchNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr T2 On T1.WorkCenterDocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Left Join TblDepartment T3 On T2.DeptCode=T3.DeptCode ");
            SQL.AppendLine("Inner Join TblItem T4 On T1.ItCode=T4.ItCode ");
            SQL.AppendLine("Left Join TblUser T5 On T1.CreateBy=T5.UserCode ");
            SQL.AppendLine("Order By T1.DocDt, T2.DocName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Work Center",
                        "Department",
                        "Item's Code",
                        "Item's Name",
                        
                        //6-10
                        "Batch#",
                        "Quantity",
                        "Uom", 
                        "Quantity",
                        "Uom",

                        //11
                        "Created By"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 250, 200, 100, 200,
                        
                        //6-10
                        200, 100, 60, 100, 60,

                        //11
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsFilterByDateInvalid()
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "T2.DocNo", "T2.DocName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T2.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                        { 
                            //0
                            "DocDt",  

                            //1-5
                            "DocName", "DeptName", "ItCode", "ItName", "BatchNo", 
                            
                            //6-10
                            "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", "UserName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row+1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
