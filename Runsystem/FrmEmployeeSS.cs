﻿#region Update
/*
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeSS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal FrmEmployeeSSFind FrmFind;
        private bool mIsSSCOAInfoMandatory = false;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmployeeSS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Social Security Amendment";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                LueSSCode.Visible = false;
                DteStartDt.Visible = false;
                DteEndDt.Visible = false;
                DteStartDt2.Visible = false;
                DteEndDt2.Visible = false;
                DteBirthDt.Visible = false;
                LueEmployeeSSProblem.Visible = false;
                LueEmployeeFamilySSProblem.Visible = false;

                xtraTabControl1.SelectedTabPage = xtraTabPage2;
                Sl.SetLueOption(ref LueEmployeeFamilySSProblem, "EmployeeSSProblem");
                Sl.SetLueSSCode(ref LueSSCode);

                xtraTabControl1.SelectedTabPage = xtraTabPage1;
                Sl.SetLueOption(ref LueEmployeeSSProblem, "EmployeeSSProblem");

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mEmpCode.Length != 0)
                {
                    ShowData(mEmpCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Social"+Environment.NewLine+"Security Code",
                        "",
                        "Social"+Environment.NewLine+"Security Name",
                        "Card#",

                        //6-10
                        "Health Facility",
                        "Start Date",
                        "End Date",
                        "Problem",
                        "Problem",

                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 80, 20, 200, 120, 
                        
                        //6-10
                        300, 100, 100, 0, 200, 
                        
                        //11
                        250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 9 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 20;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                       //0
                        "DNo",

                        //1-5
                        "",
                        "Family Name",
                        "Status Code",
                        "Status",
                        "Gender Code",
                        
                        //6-10
                        "Gender",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",
                        "Social"+Environment.NewLine+"Security Code",
                        "",

                        //11-15
                        "Social"+Environment.NewLine+"Security Name",
                        "Card#",
                        "Health Facility",
                        "Start Date",
                        "End Date",

                        //16-19
                        "Birth Date",
                        "Problem",
                        "Problem",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 200, 0, 120, 0,

                        //6-10
                        120, 120, 120, 100, 20,

                        //11-15
                        200, 120, 300, 100, 100,
                        
                        //16-19
                        100, 0, 200, 250
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 17 });
            Sm.GrdColButton(Grd2, new int[] { 1, 10 });
            Sm.GrdFormatDate(Grd2, new int[] { 14, 15, 16 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 3, 5, 7, 8, 9, 10, 17 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    xtraTabControl1.SelectedTabPage = xtraTabPage2;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 11, 12, 13, 14, 15, 16, 18, 19 });
                    xtraTabControl1.SelectedTabPage = xtraTabPage1;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 6, 7, 8, 10, 11 });
                    TxtEmpCode.Focus();
                    break;
                case mState.Edit:
                    xtraTabControl1.SelectedTabPage = xtraTabPage2;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 11, 12, 13, 14, 15, 16, 18, 19 });
                    xtraTabControl1.SelectedTabPage = xtraTabPage1;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 6, 7, 8, 10, 11 });
                    TxtEmpCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, 
                LueSSCode, DteStartDt, DteEndDt, DteStartDt2, DteEndDt2, DteBirthDt, LueEmployeeSSProblem,
                LueEmployeeFamilySSProblem 
            });
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmployeeSSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(DelEmployeeSS());
            cml.Add(DelEmployeeFamilySS());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(InsertEmployeeSS(Row));
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(InsertEmployeeFamilySS(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtEmpCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                IsGrdExceedMaxRecords() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee's social security.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's social security entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's social security entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Social security is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Card# is empty.")) return true;
                }
            }
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Family name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 9, false, "Social security is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 12, false, "Card# is empty.")) return true;

                    for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
                    {
                        if (
                            Row != Row2 &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), Sm.GetGrdStr(Grd2, Row2, 2)) &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 9), Sm.GetGrdStr(Grd2, Row2, 9))
                            )
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Family Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                                "Social security : " + Sm.GetGrdStr(Grd2, Row, 11) + Environment.NewLine + Environment.NewLine +
                                "Duplicate entry."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand DelEmployeeSS()
        {
            var cm = new MySqlCommand(){ CommandText = "Delete From TblEmployeeSS Where EmpCode=@EmpCode;" };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        private MySqlCommand DelEmployeeFamilySS()
        {
            var cm = new MySqlCommand() { CommandText = "Delete From TblEmployeeFamilySS Where EmpCode=@EmpCode;" };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        private MySqlCommand InsertEmployeeSS(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeSS(EmpCode, DNo, SSCode, CardNo, HealthFacility, StartDt, EndDt, Problem, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @SSCode, @CardNo, @HealthFacility, @StartDt, @EndDt, @Problem, @Remark, @CreateBy, CurrentDateTime()); ");
            if (mIsSSCOAInfoMandatory)
            {
                SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                SQL.AppendLine("Select Concat(AcNo1, @EmpCode) As AcNo, ");
                SQL.AppendLine("Trim(Concat(AcDesc1, ' ', @EmpCode, ' - ', @EmpName)) As AcDesc, ");
                SQL.AppendLine("If(Length(AcNo1)<=0, '', Left(AcNo1, Length(AcNo1)-1)) As Parent, ");
                SQL.AppendLine("If(Length(AcNo1)<=0, '', Length(AcNo1)-Length(Replace(AcNo1, '.', ''))+1) As Level, ");
                SQL.AppendLine("'C', @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblSS ");
                SQL.AppendLine("Where SSCode=@SSCode ");
                SQL.AppendLine("And Concat(AcNo1, @EmpCode) Not In (Select AcNo from TblCOA) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat(AcNo2, @EmpCode) As AcNo, ");
                SQL.AppendLine("Trim(Concat(AcDesc2, ' ', @EmpCode, ' - ', @EmpName)) As AcDesc, ");
                SQL.AppendLine("If(Length(AcNo2)<=0, '', Left(AcNo2, Length(AcNo2)-1)) As Parent, ");
                SQL.AppendLine("If(Length(AcNo2)<=0, '', Length(AcNo2)-Length(Replace(AcNo2, '.', ''))+1) As Level, ");
                SQL.AppendLine("'D', @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblSS ");
                SQL.AppendLine("Where SSCode=@SSCode ");
                SQL.AppendLine("And Concat(AcNo2, @EmpCode) Not In (Select AcNo from TblCOA); ");
            }
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpName", TxtEmpName.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SSCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CardNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@HealthFacility", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, Row, 7));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Problem", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertEmployeeFamilySS(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmployeeFamilySS(EmpCode, DNo, FamilyName, Status, Gender, IDNo, NIN, SSCode, CardNo, HealthFacility, StartDt, EndDt, BirthDt, Problem, Remark, CreateBy, CreateDt) " +
                    "Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @IDNo, @NIN, @SSCode, @CardNo, @HealthFacility, @StartDt, @EndDt, @BirthDt, @Problem, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FamilyName", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@IDNo", Sm.GetGrdStr(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@NIN", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@SSCode", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@CardNo", Sm.GetGrdStr(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@HealthFacility", Sm.GetGrdStr(Grd2, Row, 13));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd2, Row, 14));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd2, Row, 15));
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetGrdDate(Grd2, Row, 16));
            Sm.CmParam<String>(ref cm, "@Problem", Sm.GetGrdStr(Grd2, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 19));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string EmpCode)
        {
            try
            {
                ClearData();
                ShowEmployee(EmpCode);
                ShowEmployeeSS(EmpCode);
                ShowEmployeeFamilySS(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployee(string EmpCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 
                        
                        //1-4
                        "EmpName", "EmpCodeOld", "PosName", "DeptName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowEmployeeSS(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.SSCode, B.SSName, A.CardNo, A.HealthFacility, A.StartDt, A.EndDt, A.Problem, C.OptDesc As ProblemDesc, A.Remark ");
            SQL.AppendLine("From TblEmployeeSS A ");
            SQL.AppendLine("Left Join TblSS B On A.SSCode=B.SSCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='EmployeeSSProblem' And A.Problem=C.OptCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By B.SSName;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "SSCode", "SSName", "CardNo", "HealthFacility", "StartDt", 
                        
                        //6-9
                        "EndDt", "Problem", "ProblemDesc", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowEmployeeFamilySS(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, A.Status, B.OptDesc as StatusDesc, A.Gender, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.IDNo, A.NIN, A.SSCode, D.SSName, A.CardNo, A.HealthFacility, A.StartDt, A.EndDt, A.BirthDt, A.Problem, E.OptDesc As ProblemDesc, A.Remark ");
            SQL.AppendLine("From TblEmployeeFamilySS A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblSS D On A.SSCode=D.SSCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCat='EmployeeSSProblem' And A.Problem=E.OptCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.FamilyName, D.SSName;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "Status", "StatusDesc", "Gender", "GenderDesc", 
                        
                        //6-10
                        "IDNo", "NIN", "SSCode", "SSName", "CardNo", 
                        
                        //11-15
                        "HealthFacility", "StartDt", "EndDt", "BirthDt", "Problem", 
                        
                        //16-17
                        "ProblemDesc", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedSSCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSSCOAInfoMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSSCOAInfoMandatory": mIsSSCOAInfoMandatory = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 7, 8, 10, 16 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueSSCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSSCode, new Sm.RefreshLue1(Sl.SetLueSSCode));
        }

        private void LueSSCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueSSCode_Leave(object sender, EventArgs e)
        {
            if (LueSSCode.Visible && fAccept && fCell.ColIndex == 11)
            {
                if (Sm.GetLue(LueSSCode).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 9].Value = null;
                    Grd2.Cells[fCell.RowIndex, 11].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueSSCode);
                    Grd2.Cells[fCell.RowIndex, 11].Value = LueSSCode.GetColumnValue("Col2");
                }
                LueSSCode.Visible = false;
            }
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteStartDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt2, ref fCell, ref fAccept);
        }

        private void DteStartDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
        }

        private void DteEndDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt2, ref fCell, ref fAccept);
        }

        private void DteEndDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteBirthDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteBirthDt, ref fCell, ref fAccept);
        }

        private void DteBirthDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void LueEmployeeSSProblem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmployeeSSProblem, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeSSProblem");
        }

        private void LueEmployeeSSProblem_Leave(object sender, EventArgs e)
        {
            if (LueEmployeeSSProblem.Visible && fAccept)
            {
                if (Sm.GetLue(LueEmployeeSSProblem).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LueEmployeeSSProblem);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueEmployeeSSProblem.GetColumnValue("Col2");
                }
                LueEmployeeSSProblem.Visible = false;
            }
        }

        private void LueEmployeeSSProblem_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEmployeeFamilySSProblem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmployeeFamilySSProblem, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeSSProblem");
        }

        private void LueEmployeeFamilySSProblem_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueEmployeeFamilySSProblem_Leave(object sender, EventArgs e)
        {
            if (LueEmployeeFamilySSProblem.Visible && fAccept)
            {
                if (Sm.GetLue(LueEmployeeFamilySSProblem).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LueEmployeeFamilySSProblem);
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueEmployeeFamilySSProblem.GetColumnValue("Col2");
                }
                LueEmployeeFamilySSProblem.Visible = false;
            }
        }

        #endregion

        #region Grid Event

        #region Grid 1
        
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmployeeSSDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 10, 11 }, e.ColIndex))
                {
                    if (e.ColIndex == 7) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 8) Sm.DteRequestEdit(Grd1, DteEndDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 10) LueRequestEdit(Grd1, LueEmployeeSSProblem, ref fCell, ref fAccept, e, 9);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmEmployeeSSDlg(this));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 5, 6, 11 }, e);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 7 || e.ColIndex == 8)
            {
                if (Sm.GetGrdStr(Grd1, 0, e.ColIndex).Length != 0)
                {
                    var Dt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, e.ColIndex));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length == 0) Grd1.Cells[Row, e.ColIndex].Value = Dt;
                }
            }
            if (Sm.IsGrdColSelected(new int[] { 5, 11 }, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd1, 0, e.ColIndex).Length != 0)
                {
                    var Value = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length == 0) Grd1.Cells[Row, e.ColIndex].Value = Value;
                }
            }
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmployeeSSDlg2(this, TxtEmpCode.Text));
                }

                if (Sm.IsGrdColSelected(new int[] { 11, 12, 13, 14, 15, 16, 18, 19 }, e.ColIndex))
                {
                    if (e.ColIndex == 11) LueRequestEdit(Grd2, LueSSCode, ref fCell, ref fAccept, e, 9);
                    if (e.ColIndex == 14) Sm.DteRequestEdit(Grd2, DteStartDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 15) Sm.DteRequestEdit(Grd2, DteEndDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 16) Sm.DteRequestEdit(Grd2, DteBirthDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 18) LueRequestEdit(Grd2, LueEmployeeFamilySSProblem, ref fCell, ref fAccept, e, 17);
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                }
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd2, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd2, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 12, 13, 19 }, e);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmEmployeeSSDlg2(this, TxtEmpCode.Text));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd2, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mSSCode = Sm.GetGrdStr(Grd2, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 14 || e.ColIndex == 15)
            {
                if (Sm.GetGrdStr(Grd2, 0, e.ColIndex).Length != 0)
                {
                    var Dt = Sm.ConvertDate(Sm.GetGrdDate(Grd2, 0, e.ColIndex));
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, e.ColIndex).Length == 0) Grd2.Cells[Row, e.ColIndex].Value = Dt;
                }
            }
            if (Sm.IsGrdColSelected(new int[] { 13, 19 }, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd2, 0, e.ColIndex).Length != 0)
                {
                    var Value = Sm.GetGrdStr(Grd2, 0, e.ColIndex);
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, e.ColIndex).Length == 0) Grd2.Cells[Row, e.ColIndex].Value = Value;
                }
            }
        }

        #endregion

        #endregion

        #endregion
    }
}
