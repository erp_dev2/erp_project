﻿#region Update
//09/02/2018 [HAR] tambah informasi evidence, indicator dan level
//14/03/2018 [HAR] Score detail hapus,  pindah ke samping competence, ada thickbox evidence di detail
//24/03/2018 [HAR] BUG tidak bisa save
//27/03/2018 [HAR] tab jadi 20
//17/05/2018 [HAR] BUG saat update employee competence bukan berdasarkan key, tapi employee dan competence
//06/06/2018 [HAR] tambah button buat ke form training request
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmAssesmentProcess : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmAssesmentProcessFind FrmFind;
        internal int BtnCode = 0;
        int MaxDno = 0;

        #endregion

        #region Constructor

        public FrmAssesmentProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Assesment";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);

                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();

                SetTabControls("2", ref LueComp2);
                SetTabControls("3", ref LueComp3);
                SetTabControls("4", ref LueComp4);
                SetTabControls("5", ref LueComp5);
                SetTabControls("6", ref LueComp6);
                SetTabControls("7", ref LueComp7);
                SetTabControls("8", ref LueComp8);
                SetTabControls("9", ref LueComp9);
                SetTabControls("10", ref LueComp10);
                SetTabControls("11", ref LueComp11);
                SetTabControls("12", ref LueComp12);
                SetTabControls("13", ref LueComp13);
                SetTabControls("14", ref LueComp14);
                SetTabControls("15", ref LueComp15);
                SetTabControls("16", ref LueComp16);
                SetTabControls("17", ref LueComp17);
                SetTabControls("18", ref LueComp18);
                SetTabControls("19", ref LueComp19);
                SetTabControls("20", ref LueComp20);
                SetTabControls("1", ref LueComp1);

                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetTabControls(string TabPage, ref DXE.LookUpEdit LueComp)
        {
            tabControl1.SelectTab("tabPage" + TabPage);
            SetLueComp(ref LueComp, "");
        }

        private void SetLueComp(ref DXE.LookUpEdit Lue, string Bin)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CompetenceCode Col1, Competencename Col2 From TblCompetence Order By Col1;");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(), 0, 50, false, true, "Competence Code", "Competence Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtAsmCode, TxtAsmName, DteDocDt,  
                        LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                        LueComp6, LueComp7, LueComp8, LueComp9, LueComp10, MeeCancelReason,
                        LueComp11, LueComp12, LueComp13, LueComp14, LueComp15, 
                        LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                        TxtDeptName, TxtDeptName2,
                        TxtLevel1, TxtLevel2, TxtLevel3, TxtLevel4, TxtLevel5, 
                        TxtLevel6, TxtLevel7, TxtLevel8,TxtLevel9, TxtLevel10, 
                        TxtLevel11, TxtLevel12, TxtLevel13, TxtLevel14, TxtLevel15, 
                        TxtLevel16, TxtLevel17, TxtLevel18, TxtLevel19, TxtLevel20, 
                        TxtEmpCode, TxtEmpCode2, TxtEmpName, TxtEmpName2, TxtPosition, TxtPosition2, TxtCompetenceLevelName, DteJoinDt,
                        TxtAsmCode, TxtAsmName, MeeRemark,
                        TxtScore1, TxtScore2, TxtScore3, TxtScore4, TxtScore5, 
                        TxtScore6, TxtScore7, TxtScore8,TxtScore9, TxtScore10, 
                        TxtScore11, TxtScore12, TxtScore13, TxtScore14, TxtScore15,
                        TxtScore16, TxtScore17, TxtScore18,TxtScore19, TxtScore20,
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd9, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd10, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    BtnAssement.Enabled = false;
                    BtnAssesment2.Enabled = false;
                    BtnEmpCode.Enabled = false;
                    BtnEmpCode2.Enabled = false;
                    BtnTraining.Enabled = false;
                    ChkCancelInd.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark,
                        TxtScore1, TxtScore2, TxtScore3, TxtScore4, TxtScore5, 
                        TxtScore6, TxtScore7, TxtScore8, TxtScore9, TxtScore10, 
                        TxtScore11, TxtScore12, TxtScore13, TxtScore14, TxtScore15,
                        TxtScore16, TxtScore17, TxtScore18, TxtScore19, TxtScore20,
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd8, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd9, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd10, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 6 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6 });
                    BtnAssement.Enabled = true;
                    BtnAssesment2.Enabled = true;
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    BtnTraining.Enabled = true;
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason
                    }, false);
                    ChkCancelInd.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            SetGrdCriteria(new List<iGrid> 
                { 
                    Grd1, Grd2, Grd3, Grd4, Grd5,
                    Grd6, Grd7, Grd8, Grd9, Grd10,
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                });

        }

        private void SetGrdCriteria(List<iGrid> ListofGrd)
        {
            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 8;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                            //0
                            "Dno",
                            //1-5
                            "AsmDno",
                            "SectionNo",
                            "Competence Detail",
                            "Indicator",
                            "Evidence",
                            //6-7
                            "",
                            "Remark",
                        },
                        new int[] 
                        { 
                            20, 
                            20, 20, 250, 250, 250, 
                            20, 250
                        }
                    );
                Sm.GrdColCheck(Grd, new int []{6});
                Sm.GrdColInvisible(Grd, new int[] { 0, 1, 2});
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssesmentProcessFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                BtnCode = 0;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAsmCode, "", false)) return;
            SetFormControl(mState.Edit);
            BtnCode = 1;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData(sender, e);
                }
                else
                {
                    EditData(sender, e);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAsmCode, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(TxtAsmCode.Text);
        }

        private void PrintData(string DocNo)
        {           

        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtAsmCode, TxtAsmName, DteDocDt,  
                LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                LueComp6, LueComp7, LueComp8, LueComp9, LueComp10, MeeCancelReason,
                LueComp11, LueComp12, LueComp13, LueComp14, LueComp15, 
                LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                TxtDeptName, TxtDeptName2,
                TxtEmpCode, TxtEmpCode2, TxtEmpName, TxtEmpName2, TxtPosition, TxtPosition2, TxtCompetenceLevelName, DteJoinDt,
                TxtAsmCode, TxtAsmName, MeeRemark, 
                TxtLevel1, TxtLevel2, TxtLevel3, TxtLevel4, TxtLevel5, 
                TxtLevel6, TxtLevel7, TxtLevel8, TxtLevel9, TxtLevel10, 
                TxtLevel11, TxtLevel12, TxtLevel13, TxtLevel14, TxtLevel15,
                TxtLevel16, TxtLevel17, TxtLevel18, TxtLevel19, TxtLevel20, 
            });

            Sm.SetControlNumValueZero(new List<TextEdit>
            {
                TxtScore1, TxtScore2, TxtScore3, TxtScore4, TxtScore5, 
                TxtScore6, TxtScore7, TxtScore8, TxtScore9, TxtScore10, 
                TxtScore11, TxtScore12, TxtScore13, TxtScore14, TxtScore15,
                TxtScore16, TxtScore17, TxtScore18, TxtScore19, TxtScore20, 
            }, 0);

            ClearGrd(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
            });

            ChkCancelInd.Checked = false;
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAsmCode, TxtAsmName, TxtCompetenceLevelName, 
                LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                LueComp6, LueComp7, LueComp8, LueComp9, LueComp10, MeeCancelReason,
                LueComp11, LueComp12, LueComp13, LueComp14, LueComp15,
                LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                TxtLevel1, TxtLevel2, TxtLevel3, TxtLevel4, TxtLevel5, 
                TxtLevel6, TxtLevel7, TxtLevel8, TxtLevel9, TxtLevel10, 
                TxtLevel11, TxtLevel12, TxtLevel13, TxtLevel14, TxtLevel15, 
                TxtLevel16, TxtLevel17, TxtLevel18, TxtLevel19, TxtLevel20, 
            });

            Sm.SetControlNumValueZero(new List<TextEdit>
            {
                TxtScore1, TxtScore2, TxtScore3, TxtScore4, TxtScore5, 
                TxtScore6, TxtScore7, TxtScore8, TxtScore9, TxtScore10, 
                TxtScore11, TxtScore12, TxtScore13, TxtScore14, TxtScore15,
                TxtScore16, TxtScore17, TxtScore18, TxtScore19, TxtScore20,
            }, 0);


            ClearGrd(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
            });
        }


        private void ClearGrd(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.FocusGrd(Grd, 0, 0);
            });
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AssesmentProcess", "TblAssesmentProcessHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAssesmentProcessHdr(DocNo));

            int DNo = 0;

            string max = Sm.GetValue("Select DNo From TblEmployeeCompetence Where EmpCode = '" + TxtEmpCode.Text + "' Group By EmpCode; ");
            if (max.Length == 0)
                MaxDno = 0;
            else
                MaxDno = Convert.ToInt32(Sm.GetValue("Select Cast(Max(Dno) As Integer) As MaxDno From TblEmployeeCompetence Where EmpCode = '" + TxtEmpCode.Text + "' Group By EmpCode"));

            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd1, LueComp1, MaxDno + 1, TxtScore1, TxtLevel1);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd2, LueComp2, MaxDno + 2, TxtScore2, TxtLevel2);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd3, LueComp3, MaxDno + 3, TxtScore3, TxtLevel3);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd4, LueComp4, MaxDno + 4, TxtScore4, TxtLevel4);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd5, LueComp5, MaxDno + 5, TxtScore5, TxtLevel5);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd6, LueComp6, MaxDno + 6, TxtScore6, TxtLevel6);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd7, LueComp7, MaxDno + 7, TxtScore7, TxtLevel7);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd8, LueComp8, MaxDno + 8, TxtScore8, TxtLevel8);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd9, LueComp9, MaxDno + 9, TxtScore9, TxtLevel9);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd10, LueComp10, MaxDno + 10, TxtScore10, TxtLevel10);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd11, LueComp11, MaxDno + 11, TxtScore11, TxtLevel11);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd12, LueComp12, MaxDno + 12, TxtScore12, TxtLevel12);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd13, LueComp13, MaxDno + 13, TxtScore13, TxtLevel13);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd14, LueComp14, MaxDno + 14, TxtScore14, TxtLevel14);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd15, LueComp15, MaxDno + 15, TxtScore15, TxtLevel15);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd16, LueComp16, MaxDno + 16, TxtScore16, TxtLevel16);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd17, LueComp17, MaxDno + 17, TxtScore17, TxtLevel17);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd18, LueComp18, MaxDno + 18, TxtScore18, TxtLevel18);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd19, LueComp19, MaxDno + 19, TxtScore19, TxtLevel19);
            SaveAssesmentProcessDtlX(ref cml, DocNo, ref DNo, Grd20, LueComp20, MaxDno + 20, TxtScore20, TxtLevel20);

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private void SaveAssesmentProcessDtlX(ref List<MySqlCommand> cml, string DocNo, ref int DNo, iGrid Grd, LookUpEdit Lue, int DNoEmp, TextEdit TxtScore, TextEdit TxtLevel)
        {
            for (int Row = 0; Row < Grd.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd, Row, 3).Length > 0)
                    cml.Add(SaveAssesmentProcessDtl(DocNo, ref DNo, Grd, Row));
            }
            if (Sm.GetLue(Lue).Length > 0)
            {
                cml.Add(UpdateScoreEmployee(DNo, Lue, TxtScore, TxtLevel));
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpName, "Employee", false) ||
                Sm.IsTxtEmpty(TxtAsmName, "Assesment", false) ||
                Sm.IsTxtEmpty(TxtEmpName2, "Assesor", false) ||
                IsGrdScoreNotValid()||
                IsGrdEmpty() 
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 competence.");
                return true;
            }
            return false;
        }

        private bool IsGrdScoreNotValid()
        {
            if (IsScoreNotValid(LueComp1, TxtScore1) ||
                IsScoreNotValid(LueComp2, TxtScore2) ||
                IsScoreNotValid(LueComp3, TxtScore3) ||
                IsScoreNotValid(LueComp4, TxtScore4) ||
                IsScoreNotValid(LueComp5, TxtScore5) ||
                IsScoreNotValid(LueComp6, TxtScore6) ||
                IsScoreNotValid(LueComp7, TxtScore7) ||
                IsScoreNotValid(LueComp8, TxtScore8) ||
                IsScoreNotValid(LueComp9, TxtScore9) ||
                IsScoreNotValid(LueComp10, TxtScore10) ||
                IsScoreNotValid(LueComp11, TxtScore11) ||
                IsScoreNotValid(LueComp12, TxtScore12) ||
                IsScoreNotValid(LueComp13, TxtScore13) ||
                IsScoreNotValid(LueComp14, TxtScore14) ||
                IsScoreNotValid(LueComp15, TxtScore15) ||
                IsScoreNotValid(LueComp16, TxtScore16) ||
                IsScoreNotValid(LueComp17, TxtScore17) ||
                IsScoreNotValid(LueComp18, TxtScore18) ||
                IsScoreNotValid(LueComp19, TxtScore19) ||
                IsScoreNotValid(LueComp20, TxtScore20)
                ) return true;

            return false;
        }

        private bool IsScoreNotValid(LookUpEdit LueCom, TextEdit TxtScore)
        {
            if (Decimal.Parse(TxtScore.Text)>10)
            {
                Sm.StdMsg(mMsgType.Warning, "Score criteria '"+Sm.GetLue(LueCom)+"' not valid, maximum score 10.");
                return true;
            }
            return false;
        }
        
        private MySqlCommand SaveAssesmentProcessHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssesmentProcessHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, AsmCode, EmpCode, EmpCode2, ");
            SQL.AppendLine("Score1, Score2, Score3, Score4, Score5, ");
            SQL.AppendLine("Score6, Score7, Score8, Score9, Score10, ");
            SQL.AppendLine("Score11, Score12, Score13, Score14, Score15, ");
            SQL.AppendLine("Score16, Score17, Score18, Score19, Score20, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @AsmCode, @EmpCode, @EmpCode2, ");
            SQL.AppendLine("@Score1, @Score2, @Score3, @Score4, @Score5, ");
            SQL.AppendLine("@Score6, @Score7, @Score8, @Score9, @Score10, ");
            SQL.AppendLine("@Score11, @Score12, @Score13, @Score14, @Score15, ");
            SQL.AppendLine("@Score16, @Score17, @Score18, @Score19, @Score20, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()) ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode2", TxtEmpCode2.Text);
            Sm.CmParam<Decimal>(ref cm, "@Score1", Decimal.Parse(TxtScore1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score2", Decimal.Parse(TxtScore2.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score3", Decimal.Parse(TxtScore3.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score4", Decimal.Parse(TxtScore4.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score5", Decimal.Parse(TxtScore5.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score6", Decimal.Parse(TxtScore6.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score7", Decimal.Parse(TxtScore7.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score8", Decimal.Parse(TxtScore8.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score9", Decimal.Parse(TxtScore9.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score10", Decimal.Parse(TxtScore10.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score11", Decimal.Parse(TxtScore11.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score12", Decimal.Parse(TxtScore12.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score13", Decimal.Parse(TxtScore13.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score14", Decimal.Parse(TxtScore14.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score15", Decimal.Parse(TxtScore15.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score16", Decimal.Parse(TxtScore16.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score17", Decimal.Parse(TxtScore17.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score18", Decimal.Parse(TxtScore18.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score19", Decimal.Parse(TxtScore19.Text));
            Sm.CmParam<Decimal>(ref cm, "@Score20", Decimal.Parse(TxtScore20.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAssesmentProcessDtl(string DocNo, ref int DNo, iGrid Grd, int Row)
        {
            var SQL = new StringBuilder();

            DNo += 1;

            SQL.AppendLine("Insert Into TblAssesmentProcessDtl ");
            SQL.AppendLine("(DocNo, Dno, AsmCode, AsmDNo, AsmSectionNo, EvidenceInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @Dno, @AsmCode, @AsmDNo, @AsmSectionNo, @EvidenceInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@AsmDno", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<String>(ref cm, "@AsmSectionNo", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParam<String>(ref cm, "@EvidenceInd", Sm.GetGrdBool(Grd, Row, 6) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateScoreEmployee(int DNo, LookUpEdit Lue, TextEdit TxtScore, TextEdit TxtLevel)
        {
            var cm2 = new MySqlCommand()
            {
                CommandText =
                    "Select EmpCode From TblEmployeeCompetence " +
                    "Where EmpCode=@EmpCode And CompetenceCode = @CompetenceCode "
            };
            Sm.CmParam<String>(ref cm2, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm2, "@CompetenceCode", Sm.GetLue(Lue));

            var SQL = new StringBuilder();

            if (Sm.IsDataExist(cm2))
            {
                SQL.AppendLine("    Update TblEmployeeCompetence SET ");
                SQL.AppendLine("    Description=@Description, Score=@Score, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("    Where EmpCode=@EmpCode And CompetenceCode =@CompetenceCode ; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, Dno, CompetenceCode, Description, Score, Level, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@EmpCode, @DNo, @CompetenceCode, @Description, @Score, @Level, @UserCode, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo",  Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetLue(Lue));
            Sm.CmParam<String>(ref cm, "@Description", Lue.Text);
            Sm.CmParam<Decimal>(ref cm, "@Score", Decimal.Parse(TxtScore.Text));
            Sm.CmParam<String>(ref cm, "@Level", TxtLevel.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Edit Data

        private void EditData(object sender, EventArgs e)
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAssesmentProcess());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblAssesmentProcessHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditAssesmentProcess()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAssesmentProcessHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ; ");
            SQL.AppendLine("Delete From TblEmployeeCompetence ");
            SQL.AppendLine("Where EmpCode=@EmpCode And CompetenceCode is not null ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowDataAsm(string AsmCode)
        {
            try
            {
                ClearData2();
                ShowAssesmentHdr(AsmCode);
                ShowAssesmentDtl(AsmCode, "1", ref Grd1);
                ShowAssesmentDtl(AsmCode, "2", ref Grd2);
                ShowAssesmentDtl(AsmCode, "3", ref Grd3);
                ShowAssesmentDtl(AsmCode, "4", ref Grd4);
                ShowAssesmentDtl(AsmCode, "5", ref Grd5);
                ShowAssesmentDtl(AsmCode, "6", ref Grd6);
                ShowAssesmentDtl(AsmCode, "7", ref Grd7);
                ShowAssesmentDtl(AsmCode, "8", ref Grd8);
                ShowAssesmentDtl(AsmCode, "9", ref Grd9);
                ShowAssesmentDtl(AsmCode, "10", ref Grd10);
                ShowAssesmentDtl(AsmCode, "11", ref Grd11);
                ShowAssesmentDtl(AsmCode, "12", ref Grd12);
                ShowAssesmentDtl(AsmCode, "13", ref Grd13);
                ShowAssesmentDtl(AsmCode, "14", ref Grd14);
                ShowAssesmentDtl(AsmCode, "15", ref Grd15);
                ShowAssesmentDtl(AsmCode, "16", ref Grd16);
                ShowAssesmentDtl(AsmCode, "17", ref Grd17);
                ShowAssesmentDtl(AsmCode, "18", ref Grd18);
                ShowAssesmentDtl(AsmCode, "19", ref Grd19);
                ShowAssesmentDtl(AsmCode, "20", ref Grd20);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssesmentHdr(string AsmCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AsmCode, A.AsmName, B.LevelName, ");
            SQL.AppendLine("A.CompetenceCode1, A.CompetenceCode2, A.CompetenceCode3, A.CompetenceCode4, A.CompetenceCode5, ");
            SQL.AppendLine("A.CompetenceCode6, A.CompetenceCode7, A.CompetenceCode8, A.CompetenceCode9, A.CompetenceCode10, ");
            SQL.AppendLine("A.CompetenceCode11, A.CompetenceCode12, A.CompetenceCode13, A.CompetenceCode14, A.CompetenceCode15, ");
            SQL.AppendLine("A.CompetenceCode16, A.CompetenceCode17, A.CompetenceCode18, A.CompetenceCode19, A.CompetenceCode20, ");
            SQL.AppendLine("A.Level1, A.Level2, A.Level3, A.Level4, A.Level5, ");
            SQL.AppendLine("A.Level6, A.Level7, A.Level8, A.Level9, A.Level10, ");
            SQL.AppendLine("A.Level11, A.Level12, A.Level13, A.Level14, A.Level15, ");
            SQL.AppendLine("A.Level16, A.Level17, A.Level18, A.Level19, A.Level20 ");
            SQL.AppendLine("From TblAssesmentHdr A ");
            SQL.AppendLine("Left Join Tbllevelhdr B On A.CompetenceLevel = B.levelCode ");
            SQL.AppendLine("Where AsmCode=@AsmCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "AsmCode", 
                        //1-5
                        "AsmName", "levelName", "CompetenceCode1", "CompetenceCode2", "CompetenceCode3", 
                        //6-10
                        "CompetenceCode4", "CompetenceCode5", "CompetenceCode6", "CompetenceCode7", "CompetenceCode8", 
                        //11-15
                        "CompetenceCode9", "CompetenceCode10", "CompetenceCode11", "CompetenceCode12", "CompetenceCode13", 
                        //16-18
                        "CompetenceCode14", "CompetenceCode15", "CompetenceCode16", "CompetenceCode17", "CompetenceCode18",  
                        //21-25
                        "CompetenceCode19", "CompetenceCode20", "Level1", "Level2", "Level3", 
                        //26-30
                        "Level4", "Level5", "Level6", "Level7", "Level8", 
                        //31-35
                        "Level9", "Level10", "Level11", "Level12", "Level13", 
                        //36-40
                        "Level14", "Level15", "Level6", "Level17", "Level18", 
                        //41-42
                        "Level19", "Level20", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAsmCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAsmName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtCompetenceLevelName.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueComp1, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueComp2, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueComp3, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueComp4, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueComp5, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueComp6, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueComp7, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueComp8, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueComp9, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueComp10, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueComp11, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueComp12, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueComp13, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueComp14, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueComp15, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueComp16, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueComp17, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueComp18, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueComp19, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LueComp20, Sm.DrStr(dr, c[22]));
                        TxtLevel1.EditValue = Sm.DrStr(dr, c[23]);
                        TxtLevel2.EditValue = Sm.DrStr(dr, c[24]);
                        TxtLevel3.EditValue = Sm.DrStr(dr, c[25]);
                        TxtLevel4.EditValue = Sm.DrStr(dr, c[26]);
                        TxtLevel5.EditValue = Sm.DrStr(dr, c[27]);
                        TxtLevel6.EditValue = Sm.DrStr(dr, c[28]);
                        TxtLevel7.EditValue = Sm.DrStr(dr, c[29]);
                        TxtLevel8.EditValue = Sm.DrStr(dr, c[30]);
                        TxtLevel9.EditValue = Sm.DrStr(dr, c[31]);
                        TxtLevel10.EditValue = Sm.DrStr(dr, c[32]);
                        TxtLevel11.EditValue = Sm.DrStr(dr, c[33]);
                        TxtLevel12.EditValue = Sm.DrStr(dr, c[34]);
                        TxtLevel13.EditValue = Sm.DrStr(dr, c[35]);
                        TxtLevel14.EditValue = Sm.DrStr(dr, c[36]);
                        TxtLevel15.EditValue = Sm.DrStr(dr, c[37]);
                        TxtLevel16.EditValue = Sm.DrStr(dr, c[38]);
                        TxtLevel17.EditValue = Sm.DrStr(dr, c[39]);
                        TxtLevel18.EditValue = Sm.DrStr(dr, c[40]);
                        TxtLevel19.EditValue = Sm.DrStr(dr, c[41]);
                        TxtLevel20.EditValue = Sm.DrStr(dr, c[42]);
                    }, true
                );
        }

        private void ShowAssesmentDtl(string AsmCode, string SectionNo, ref iGrid GrdXX)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Dno, SectionNo, Description, Indicator, Evidence, Remark");
            SQL.AppendLine("From TblAssesmentDtl ");
            SQL.AppendLine("Where AsmCode=@AsmCode ");
            SQL.AppendLine("And SectionNo=@SectionNo ");
            SQL.AppendLine("Order By DNo ; ");

            Sm.ShowDataInGrid(
                ref GrdXX, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "SectionNo", "Description", "Indicator", "Evidence", "Remark", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4); 
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(GrdXX, 0, 0);
        }

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAssesmentProcessHdr(DocNo);
                ShowAssesmentProcessDtl(DocNo, "1", ref Grd1);
                ShowAssesmentProcessDtl(DocNo, "2", ref Grd2);
                ShowAssesmentProcessDtl(DocNo, "3", ref Grd3);
                ShowAssesmentProcessDtl(DocNo, "4", ref Grd4);
                ShowAssesmentProcessDtl(DocNo, "5", ref Grd5);
                ShowAssesmentProcessDtl(DocNo, "6", ref Grd6);
                ShowAssesmentProcessDtl(DocNo, "7", ref Grd7);
                ShowAssesmentProcessDtl(DocNo, "8", ref Grd8);
                ShowAssesmentProcessDtl(DocNo, "9", ref Grd9);
                ShowAssesmentProcessDtl(DocNo, "10", ref Grd10);
                ShowAssesmentProcessDtl(DocNo, "11", ref Grd11);
                ShowAssesmentProcessDtl(DocNo, "12", ref Grd12);
                ShowAssesmentProcessDtl(DocNo, "13", ref Grd13);
                ShowAssesmentProcessDtl(DocNo, "14", ref Grd14);
                ShowAssesmentProcessDtl(DocNo, "15", ref Grd15);
                ShowAssesmentProcessDtl(DocNo, "16", ref Grd16);
                ShowAssesmentProcessDtl(DocNo, "17", ref Grd17);
                ShowAssesmentProcessDtl(DocNo, "18", ref Grd18);
                ShowAssesmentProcessDtl(DocNo, "19", ref Grd19);
                ShowAssesmentProcessDtl(DocNo, "20", ref Grd20);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssesmentProcessHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.EmpCode As EmpCode1, B.EmpName As Empname1, ");
            SQL.AppendLine("E.Posname As posName1, G.DeptName As DeptName1, B.JoinDt, A.AsmCode, C.AsmName, H.Posname As posname3, "); 
            SQL.AppendLine("A.EmpCode2, D.EmpName As EmpName2, H.PosName As PosName2, I.DeptName As DeptName2, A.Remark,  ");
            SQL.AppendLine("C.CompetenceCode1, C.CompetenceCode2, C.CompetenceCode3, C.CompetenceCode4, C.CompetenceCode5, ");
            SQL.AppendLine("C.CompetenceCode6, C.CompetenceCode7, C.CompetenceCode8, C.CompetenceCode9, C.CompetenceCode10, ");
            SQL.AppendLine("C.CompetenceCode11, C.CompetenceCode12, C.CompetenceCode13, C.CompetenceCode14, C.CompetenceCode15, ");
            SQL.AppendLine("C.CompetenceCode16, C.CompetenceCode17, C.CompetenceCode18, C.CompetenceCode19, C.CompetenceCode20, ");
            SQL.AppendLine("C.Level1, C.Level2, C.Level3, C.Level4, C.Level5, ");
            SQL.AppendLine("C.Level6, C.Level7, C.Level8, C.Level9, C.Level10, ");
            SQL.AppendLine("C.Level11, C.Level12, C.Level13, C.Level14, C.Level15,");
            SQL.AppendLine("C.Level16, C.Level17, C.Level18, C.Level19, C.Level20, ");
            SQL.AppendLine("A.Score1, A.Score2, A.Score3, A.Score4, A.Score5, ");
            SQL.AppendLine("A.Score6, A.Score7, A.Score8, A.Score9, A.Score10, ");
            SQL.AppendLine("A.Score11, A.Score12, A.Score13, A.Score14, A.Score15, ");
            SQL.AppendLine("A.Score16, A.Score17, A.Score18, A.Score19, A.Score20  ");
            SQL.AppendLine("From TblAssesmentProcessHdr A  ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblAssesmentHdr C On A.AsmCode = C.AsmCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode2 = D.EmpCode ");
            SQL.AppendLine("Left Join TblPosition E On B.PosCode = E.PosCode ");
            SQL.AppendLine("Left Join TblPosition F On D.PosCode = F.PosCode ");
            SQL.AppendLine("Inner Join tblDepartment G On B.DeptCode = G.DeptCode ");
            SQL.AppendLine("Left Join TblPosition H On D.PosCode = H.PosCode ");
            SQL.AppendLine("Inner Join tblDepartment I On D.DeptCode = I.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelInd", "CancelReason", "EmpCode1", "Empname1", 
                        //6-10
                        "posName1", "DeptName1", "JoinDt", "AsmCode", "AsmName", 
                        //11-15
                        "posname3", "EmpCode2", "EmpName2", "PosName2", "DeptName2", 
                        //16-20
                        "Remark",  "CompetenceCode1", "CompetenceCode2", "CompetenceCode3", "CompetenceCode4", 
                        //21-25
                        "CompetenceCode5", "CompetenceCode6", "CompetenceCode7","CompetenceCode8", "CompetenceCode9",
                        //26-30
                        "CompetenceCode10", "CompetenceCode11","CompetenceCode12", "CompetenceCode13", "CompetenceCode14", 
                        //31-35
                        "CompetenceCode15", "CompetenceCode16", "CompetenceCode17","CompetenceCode18", "CompetenceCode19",    
                        //36-40
                        "CompetenceCode20", "Level1", "Level2",  "Level3", "Level4",
                        //41-45
                        "Level5", "Level6", "Level7", "Level8", "Level9", 
                        //46-50
                        "Level10", "Level11", "Level12", "Level13", "Level14", 
                        //51-55
                        "Level15", "Level16", "Level17", "Level18", "Level19",  
                        //56-60
                        "Level20", "Score1", "Score2",  "Score3", "Score4",    
                        //61-65
                        "Score5", "Score6", "Score7", "Score8", "Score9", 
                        //66-70
                        "Score10", "Score11", "Score12", "Score13", "Score14", 
                        //71-75
                        "Score15", "Score16", "Score17", "Score18", "Score19", 
                        //76
                        "Score20"
            
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPosition.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptName.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[8]));
                        TxtAsmCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAsmName.EditValue = Sm.DrStr(dr, c[10]);
                        TxtCompetenceLevelName.EditValue = Sm.DrStr(dr, c[11]);
                        TxtEmpCode2.EditValue = Sm.DrStr(dr, c[12]);
                        TxtEmpName2.EditValue = Sm.DrStr(dr, c[13]);
                        TxtPosition2.EditValue = Sm.DrStr(dr, c[14]);
                        TxtDeptName2.EditValue = Sm.DrStr(dr, c[15]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sm.SetLue(LueComp1, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueComp2, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueComp3, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueComp4, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueComp5, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LueComp6, Sm.DrStr(dr, c[22]));
                        Sm.SetLue(LueComp7, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueComp8, Sm.DrStr(dr, c[24]));
                        Sm.SetLue(LueComp9, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueComp10, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueComp11, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueComp12, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueComp13, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueComp14, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueComp15, Sm.DrStr(dr, c[31]));
                        Sm.SetLue(LueComp16, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueComp17, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueComp18, Sm.DrStr(dr, c[34]));
                        Sm.SetLue(LueComp19, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueComp20, Sm.DrStr(dr, c[36]));
                        TxtLevel1.EditValue = Sm.DrStr(dr, c[37]);
                        TxtLevel2.EditValue = Sm.DrStr(dr, c[38]);
                        TxtLevel3.EditValue = Sm.DrStr(dr, c[39]);
                        TxtLevel4.EditValue = Sm.DrStr(dr, c[40]);
                        TxtLevel5.EditValue = Sm.DrStr(dr, c[41]);
                        TxtLevel6.EditValue = Sm.DrStr(dr, c[42]);
                        TxtLevel7.EditValue = Sm.DrStr(dr, c[43]);
                        TxtLevel8.EditValue = Sm.DrStr(dr, c[44]);
                        TxtLevel9.EditValue = Sm.DrStr(dr, c[45]);
                        TxtLevel10.EditValue = Sm.DrStr(dr, c[46]);
                        TxtLevel11.EditValue = Sm.DrStr(dr, c[47]);
                        TxtLevel12.EditValue = Sm.DrStr(dr, c[48]);
                        TxtLevel13.EditValue = Sm.DrStr(dr, c[49]);
                        TxtLevel14.EditValue = Sm.DrStr(dr, c[50]);
                        TxtLevel15.EditValue = Sm.DrStr(dr, c[51]);
                        TxtLevel16.EditValue = Sm.DrStr(dr, c[52]);
                        TxtLevel17.EditValue = Sm.DrStr(dr, c[53]);
                        TxtLevel18.EditValue = Sm.DrStr(dr, c[54]);
                        TxtLevel19.EditValue = Sm.DrStr(dr, c[55]);
                        TxtLevel20.EditValue = Sm.DrStr(dr, c[56]);
                        TxtScore1.EditValue = Sm.DrDec(dr, c[57]);
                        TxtScore2.EditValue = Sm.DrDec(dr, c[58]);
                        TxtScore3.EditValue = Sm.DrDec(dr, c[59]);
                        TxtScore4.EditValue = Sm.DrDec(dr, c[60]);
                        TxtScore5.EditValue = Sm.DrDec(dr, c[61]);
                        TxtScore6.EditValue = Sm.DrDec(dr, c[62]);
                        TxtScore7.EditValue = Sm.DrDec(dr, c[63]);
                        TxtScore8.EditValue = Sm.DrDec(dr, c[64]);
                        TxtScore9.EditValue = Sm.DrDec(dr, c[65]);
                        TxtScore10.EditValue = Sm.DrDec(dr, c[66]);
                        TxtScore11.EditValue = Sm.DrDec(dr, c[67]);
                        TxtScore12.EditValue = Sm.DrDec(dr, c[68]);
                        TxtScore13.EditValue = Sm.DrDec(dr, c[69]);
                        TxtScore14.EditValue = Sm.DrDec(dr, c[70]);
                        TxtScore15.EditValue = Sm.DrDec(dr, c[71]);
                        TxtScore16.EditValue = Sm.DrDec(dr, c[72]);
                        TxtScore17.EditValue = Sm.DrDec(dr, c[73]);
                        TxtScore18.EditValue = Sm.DrDec(dr, c[74]);
                        TxtScore19.EditValue = Sm.DrDec(dr, c[75]);
                        TxtScore20.EditValue = Sm.DrDec(dr, c[76]);
                    }, true
                );
        }

        private void ShowAssesmentProcessDtl(string DocNo, string SectionNo, ref iGrid GrdXX)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.AsmCode, A.AsmDno, A.AsmSectionNo, B.Description, B.Indicator, B.Evidence, A.EvidenceInd, B.Remark ");
            SQL.AppendLine("From TblAssesmentProcessDtl A ");
            SQL.AppendLine("Inner Join TblAssesmentDtl B On A.AsmCode = B.AsmCode And A.AsmDno = B.Dno ");
            SQL.AppendLine("And A.AsmSectionNo=B.SectionNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.DNo ");

            Sm.ShowDataInGrid(
                ref GrdXX, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "AsmDno", "AsmSectionNo", "Description", "Indicator", "Evidence", 
                    //6-7
                    "EvidenceInd", "Remark",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(GrdXX, 0, 0);
        }

        #endregion

        #region Additional Method

        
        #endregion

        #endregion

        #region Event

        #region Grid Event
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
            }
        }

        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
            }
        }

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
            }
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
            }
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
            }
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
            }
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
            }
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
        }

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
        }

        private void Grd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
        }

        private void tabControl1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
        }

        private void Grd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd17, e, BtnSave);
        }

        private void Grd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd18, e, BtnSave);
        }

        private void Grd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd19, e, BtnSave);
        }

        private void Grd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd20, e, BtnSave);
        }

        #endregion

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAssesmentProcessDlg(this, Sm.GetDte(DteDocDt).Substring(0, 8), 1));
        }

        private void BtnEmpCode2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAssesmentProcessDlg(this, Sm.GetDte(DteDocDt).Substring(0, 8), 2));
        }

        private void BtnAssement_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAssesmentProcessDlg2(this));
        }

        private void LueComp1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp1, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp2, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp3, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp4, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp5, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp6, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp7, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp8, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp9, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp10, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp11, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp12, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp13, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp14, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueComp15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueComp15, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void BtnAssesment2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtAsmCode, "Assesment Code", false))
            {
                var f = new FrmAssesment2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAsmCode = TxtAsmCode.Text;
                f.ShowDialog();
            }
        }
       
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void TxtScore1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore1, 0);
        }

        private void TxtScore2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore2, 0);
        }

        private void TxtScore3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore3, 0);
        }

        private void TxtScore4_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore4, 0);
        }

        private void TxtScore5_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore5, 0);
        }

        private void TxtScore6_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore6, 0);
        }

        private void TxtScore7_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore7, 0);
        }

        private void TxtScore8_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore8, 0);
        }

        private void TxtScore9_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore9, 0);
        }

        private void TxtScore10_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore10, 0);
        }

        private void TxtScore11_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore11, 0);
        }

        private void TxtScore12_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore12, 0);
        }

        private void TxtScore13_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore13, 0);
        }

        private void TxtScore14_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore14, 0);
        }

        private void TxtScore15_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore15, 0);
        }

        private void TxtScore16_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore16, 0);
        }

        private void TxtScore17_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore17, 0);
        }

        private void TxtScore18_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore18, 0);
        }

        private void TxtScore19_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore19, 0);
        }

        private void TxtScore20_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtScore20, 0);
        }

        private void BtnTraining_Click(object sender, EventArgs e)
        {
            var f = new FrmTrainingRequest(mMenuCode);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }

        #endregion

        
      
    }
}
