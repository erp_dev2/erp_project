﻿#region Update
/*
    04/07/2017 (har) 1. month berdasarkan economic life year 2. tahun terakhir full 12 bulan  3. bulan terakhir di tahun terakhir, anual cost = 0,  depresiasi = sisa hasil depresiasi
                     4. cut depresiasi tiap pergantian tahun
    05/09/2017 (har) recompute saat show data dihilangkan, lagsung ambil data detail
    29/07/2017 [TKG] tambah journal# di dtl
    23/01/2018 [WED] label Date of Purchase --> Depreciation Date
    23/01/2018 [WED] Depreciation Date bisa dirubah waktu insert, sehingga ada kolom baru di tabel depreciation asset untuk simpan DepreciationDate
    17/04/2018 [HAR] tambah parameter buat nentuin jenis depresiasi garis lurus 
    19/04/2018 [HAR] bug fixing Depresiasi asset nilai balance dan annual value KMI garislurus
    25/04/2018 [HAR] Economic life,  Depreciation date, depreciation method bisa di ubah saat insert
    07/05/2018 [HAR] tambah parameter buat nentuin jenis depresiasi saldo menurun
    10/05/2018 [HAR] bug fixing nilai depresiasi di tahun terakhir untuk saldo menurun non Runsystem
    17/07/2018 [TKG] tambah cost center saat journal
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.\
    27/11/2019 [HAR/KBN] nilai 1 rupiah d akhir depresiasi berdasarkan parameter
    09/06/2021 [IBL/ALL] tambah validasi setting journal
    10/11/2021 [RIS/AMKA] menambahkan parameter IsDBACCFilteredByGroup
    22/01/2021 [TKG/PHT] ubah GetParameter() dan proses save
    28/01/2022 [ICA/ALL] bug ketika show data, jumlah grid double
    03/02/2022 [SET/PHT] tambah parameter IsFicoUseMultiProfitCenterFilter
    01/03/2022 [TRI/AMKA] bug saldo balance ketika month hanya 1, saldo belum sesuai dengan param mDepreciationSalvageValue
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    08/04/2022 [DITA/PHT] saat save journal cancel tambah validasi currendt based on param IsClosingJournalBasedOnMultiProfitCenter

 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAsset : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmDepreciationAssetFind FrmFind;
        bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAutoJournalActived = false,
            mIsJournalValidationDepreciationAssetEnabled = false;
        internal bool 
            mIsDBACCFilteredByGroup = false,
            mIsFicoUseMultiProfitCenterFilter = false;
        internal string mDepreciationTypeGarisLurus = string.Empty;
        internal string mDepreciationTypeSaldoMenurun = string.Empty;
        internal string LastYr = string.Empty;
        internal string mDepreciationSalvageValue = string.Empty;
        #endregion

        #region Constructor

        public FrmDepreciationAsset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Depreciation Asset";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLueDepreciationCode(ref LueDepreciationCode);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5 
                        "Active",
                        "Old Active",
                        "Month"+Environment.NewLine+"(Purchase)",
                        "Month",
                        "Year",
                        
                        //6-10
                        "Depreciation"+Environment.NewLine+"Value",
                        "Costcenter Code",
                        "Costcenter",
                        "Balance",
                        "Annual Cost Value",

                        //11
                        "Journal#"
                    },
                     new int[] 
                    {
                        20, 
                        60, 40, 100, 80, 80, 
                        150, 100, 200, 200, 200,
                        130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] {6, 9, 10}, 0);     
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1,  2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtAssetCode, TxtAssetName, TxtDisplayName, TxtEcoLifeYr, TxtEcoLife, TxtItCode, 
                        TxtItName, DteDepreciationDt, TxtAssetValue, LueDepreciationCode, TxtPercentageAnnualDepreciation,
                        TxtCC, MeeRemark
                    }, true);
                    BtnAssetCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1,2,3,4,5,6,7,8,9,10 });
                    ChkCancelInd.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, DteDepreciationDt, TxtEcoLifeYr, TxtEcoLife, LueDepreciationCode, TxtPercentageAnnualDepreciation,
                    }, false);
                    BtnAssetCode.Enabled = true;
                    break;
                case mState.Edit:
                    ChkCancelInd.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtAssetCode, TxtAssetName, TxtDisplayName, 
                TxtItCode, TxtItName, DteDepreciationDt, LueDepreciationCode, TxtCC, 
                MeeRemark
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtEcoLifeYr, TxtEcoLife, TxtAssetValue, TxtPercentageAnnualDepreciation }, 0);

            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDepreciationAssetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DepreciationAsset", "TblDepreciationAssetHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDepAssetHdr(DocNo));
            cml.Add(SaveDepAssetDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
            //        cml.Add(SaveDepAssetDtl(DocNo, Row));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset", false) ||
                Sm.IsDteEmpty(DteDepreciationDt, "Depreciation Date")||
                Sm.IsTxtEmpty(TxtAssetValue, "Asset value", true) ||
                Sm.IsTxtEmpty(TxtEcoLifeYr, "Economic Life Year", true) ||
                Sm.IsTxtEmpty(TxtEcoLife, "Economic Life Month", true) ||
                Sm.IsLueEmpty(LueDepreciationCode, "Depreciation Code")||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDepreciationDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDepreciationDt))) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDepreciationDt)) ||
                IsJournalSettingInvalid() ||
                IsGrdEmpty()
                ;
        }

        private string GetProfitCenterCode()
        {
            var Value = TxtAssetCode.Text;
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select B.ProfitCenterCode From TblAsset A, TblCostCenter B " +
                    "Where A.CCCode=B.CCCode And A.CCCode Is Not Null And B.ProfitCenterCode Is Not Null And A.AssetCode=@Param;",
                    Value);
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationDepreciationAssetEnabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Table
            if (IsJournalSettingInvalid_Asset(Msg)) return true;
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_Asset(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Asset = string.Empty;

            SQL.AppendLine("Select Concat(AssetCode, ' - ', AssetName) ");
            SQL.AppendLine("From TblAsset ");
	        SQL.AppendLine("Where AcNo Is Null ");
	        SQL.AppendLine("And ActiveInd = 'Y' ");
	        SQL.AppendLine("And AssetCode = @AssetCode; ");

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            cm.CommandText = SQL.ToString();
            Asset = Sm.GetValue(cm);
            if (Asset.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Asset's COA account# (" + Asset + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ItCtName = string.Empty;

            SQL.AppendLine("Select C.ItCtName ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode And A.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode = C.ItCtCode And C.AcNo Is Null ");
            SQL.AppendLine("    And A.AssetCode = @AssetCode; ");

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No depreciation value.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDepAssetHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "/* Depreciation Asset-Hdr */ " +
                    "Insert Into TblDepreciationAssetHdr(DocNo, DocDt, CancelInd, AssetCode, AssetValue, DepreciationDt, EcoLifeYr, EcoLife, DepreciationCode, PercentageAnnualDepreciation, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @CancelInd, @AssetCode, @AssetValue, @DepreciationDt, @EcoLifeYr, @EcoLife, @DepreciationCode, @PercentageAnnualDepreciation, @Remark, @UserCode, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N"); 
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", Decimal.Parse(TxtAssetValue.Text));
            Sm.CmParamDt(ref cm, "@DepreciationDt", Sm.GetDte(DteDepreciationDt));
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", Decimal.Parse(TxtEcoLifeYr.Text)); 
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", Decimal.Parse(TxtEcoLife.Text));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode)); 
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", Decimal.Parse(TxtPercentageAnnualDepreciation.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDepAssetDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Depreciation Asset-Dtl */ ");

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");


            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDepreciationAssetDtl ");
                        SQL.AppendLine("(DocNo, DNo, ActiveInd, MthPurchase, Mth, Yr, DepreciationValue, CCCode, CostValue, AnnualCostValue, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        "(@DocNo, @DNo_"+r.ToString() +
                        ", 'Y', @MthPurchase_" + r.ToString() +
                        ", @Mth_" + r.ToString() + 
                        ", @Yr_" + r.ToString() +
                        ", @DepreciationValue_" + r.ToString() +
                        ", @CCCode_" + r.ToString() +
                        ", @CostValue_" + r.ToString() +
                        ", @AnnualCostValue_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@MthPurchase_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@Mth_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@DepreciationValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@CCCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@CostValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@AnnualCostValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                }
            }


            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();

            return cm;
        }

        //private MySqlCommand SaveDepAssetDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblDepreciationAssetDtl(DocNo, DNo, ActiveInd, MthPurchase, Mth, Yr, DepreciationValue, CCCode, CostValue, AnnualCostValue, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @ActiveInd, @MthPurchase, @Mth, @Yr, @DepreciationValue, @CCCode, @CostValue, @AnnualCostValue, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ActiveInd", "Y");
        //    Sm.CmParam<String>(ref cm, "@MthPurchase", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@DepreciationValue", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@CostValue", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@AnnualCostValue", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* Depreciation Asset-Journal */ ");

            SQL.AppendLine("Update TblDepreciationAssetHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, IfNull(A.DepreciationDt, B.AssetDt) As JournalDt, ");
            SQL.AppendLine("Concat('Depreciation of Business Assets : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.AssetCode = B.AssetCode; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, EntCode From (");
            SQL.AppendLine("        Select B.AcNo, 'D' As ACtype, B.AssetValue As DAmt, 0 As CAmt, D.EntCode ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblAsset B ");
            SQL.AppendLine("        On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("        And B.AcNo Is Not Null ");
            SQL.AppendLine("        And B.ActiveInd='Y' ");
            SQL.AppendLine("        Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("        Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, 'C' As ACtype, 0 As DAmt, B.AssetValue As CAmt, F.EntCode ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblAsset B On A.AssetCode=B.AssetCode And B.ActiveInd='Y' ");
            SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("        Left Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode  ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo, Actype ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDepreciationDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();
            cml.Add(EditDepAssetHdr());
            if (mIsAutoJournalActived && IsJournalDataExisted()) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);
            
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            var Query = new StringBuilder();

            Query.AppendLine("Select IfNull(A.DepreciationDt, B.AssetDt) As DocDt ");
            Query.AppendLine("From TblDepreciationAssetHdr A ");
            Query.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            Query.AppendLine("Where A.DocNo=@Param ");
            Query.AppendLine("And A.AssetCode = B.AssetCode; ");

            var DocDt = Sm.GetValue(Query.ToString(), TxtDocNo.Text);

            return
                Sm.IsTxtEmpty(TxtDocNo, "Depreciation", false) ||

                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, DocDt, GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, DocDt)) ||
                // Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, DocDt) ||
                IsJournalSettingInvalid() ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblDepreciationAssetHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblDepreciationAssetHdr " +
                    "Where JournalDocNo Is Not Null And DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        private MySqlCommand EditDepAssetHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblDepreciationAssetHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var Query = new StringBuilder();

            Query.AppendLine("Select IfNull(A.DepreciationDt, B.AssetDt) As DocDt ");
            Query.AppendLine("From TblDepreciationAssetHdr A ");
            Query.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            Query.AppendLine("Where A.DocNo=@Param ");
            Query.AppendLine("And A.AssetCode = B.AssetCode; ");

            var DocDt = Sm.GetValue(Query.ToString(), TxtDocNo.Text);
            var CurrentDt = Sm.ServerCurrentDate();
            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblDepreciationAssetHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, ");
            SQL.AppendLine("JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDepreciationAssetHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblDepreciationAssetHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDepAssetHdr(DocNo);
                ShowDepAssetDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDepAssetHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.AssetCode, B.Assetname, B.DisplayName, B.ItCode, C.Itname, ");
            SQL.AppendLine("A.DepreciationDt, IfNull(A.AssetValue, B.AssetValue)As AssetValue, IFnull(A.EcolifeYr, B.EcoLifeYr) As EcoLifeYr, ");
            SQL.AppendLine("ifnull(A.Ecolife, B.Ecolife) As EcoLife, ifnull(A.DepreciationCode, B.DepreciationCode) DepreciationCode, ");
            SQL.AppendLine("ifnull(A.PercentageAnnualDepreciation, B.PercentageAnnualDepreciation) PercentageAnnualDepreciation, E.CCname, A.remark "); 
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("left Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On B.DepreciationCOde = D.OptCode And D.OptCat = 'DepreciationMethod'  ");
            SQL.AppendLine("Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("Where DocNo=@DocNo;"); 

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt",  "CancelInd", "AssetCode", "AssetName", "DisplayName",   
                        //6-10
                        "ItCode", "ItName", "DepreciationDt", "AssetValue", "EcoLifeYr",    
                        //11-15
                        "EcoLife", "DepreciationCode", "PercentageAnnualdepreciation", "CCname", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtAssetCode.EditValue =  Sm.DrStr(dr, c[3]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItCode.EditValue =  Sm.DrStr(dr, c[6]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[7]); 
                        Sm.SetDte(DteDepreciationDt, Sm.DrStr(dr, c[8]));
                        TxtAssetValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[9]), 0);
                        TxtEcoLifeYr.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                        TxtEcoLife.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[11]), 0);
                        Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[12]));
                        TxtPercentageAnnualDepreciation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                        TxtCC.EditValue =  Sm.DrStr(dr, c[14]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    }, true
                );
        }

        private void ShowDepAssetDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.ActiveInd, A.MthPurchase, A.Mth, A.Yr, A.depreciationvalue, A.CCCode, B.CCName,  A.CostValue, Round(A.AnnualCostValue, 2) As AnnualCostValue, A.JournalDocNo ");
            SQL.AppendLine("From TblDepreciationAssetDtl A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ActiveInd", "MthPurchase", "Mth", "Yr", "depreciationvalue", 
                    
                    //6-10
                    "CCCode", "CCName", "CostValue", "AnnualCostValue", "JournalDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    if (Sm.GetGrdDec(Grd1, Row, 3) % 12 == 1)
                        Grd1.Rows[Row].BackColor = Color.Aqua;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            //ClearGrd();
            //SetMthGrd();
            //SetCostValue();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowAssetInfo(string AssetCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.AssetCode, A.AssetName, A.DisplayName, A.ItCode, B.Itname, "+
                    "A.AssetDt, A.AssetValue, A.EcoLifeYr, A.Ecolife, A.DepreciationCode, A.PercentageAnnualdepreciation, D.CCname " +
                    "from TblAsset A "+
                    "left Join TblItem B On A.ItCode = B.ItCode "+
                    "Left Join TblOption C On A.DepreciationCOde = C.OptCode And C.OptCat = 'DepreciationMethod' "+
                    "Left Join TblCostCenter D On A.CCCode = D.CCCode "+
                    "Where A.AssetCode = '"+AssetCode+"' ",
                    new string[] 
                    { 
                        "AssetCode", 
                        "AssetName", "DisplayName", "ItCode", "ItName", "AssetDt",
                        "AssetValue", "EcoLifeYr", "EcoLife", "DepreciationCode", "PercentageAnnualdepreciation", 
                        "CCName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAssetCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetDte(DteDepreciationDt, Sm.DrStr(dr, c[5]));
                        TxtAssetValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[6]), 0);
                        TxtEcoLifeYr.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[7]), 0);
                        TxtEcoLife.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                        Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[9]));
                        TxtPercentageAnnualDepreciation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                        TxtCC.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }


        #endregion

        #endregion

        #region Additional Method

        public static void SetLueDepreciationCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'DepreciationMethod' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsJournalValidationDepreciationAssetEnabled', 'IsDBACCFilteredByGroup', 'DepreciationSalvageValue', 'DepreciationTypeSaldoMenurun', ");
            SQL.AppendLine("'DepreciationTypeGarisLurus', 'IsFicoUseMultiProfitCenterFilter', 'IsClosingJournalBasedOnMultiProfitCenter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsJournalValidationDepreciationAssetEnabled": mIsJournalValidationDepreciationAssetEnabled = ParValue == "Y"; break;
                            case "IsDBACCFilteredByGroup": mIsDBACCFilteredByGroup = ParValue == "Y"; break;
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            
                            //string
                            case "DepreciationSalvageValue": mDepreciationSalvageValue = ParValue; break;
                            case "DepreciationTypeSaldoMenurun": mDepreciationTypeSaldoMenurun = ParValue; break;
                            case "DepreciationTypeGarisLurus": mDepreciationTypeGarisLurus = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        public void SetMthGrd()
        {
            ClearGrd();
            try
            {
                decimal 
                    StartMth = 0m,
                    Year = 0m,
                    EndMth = 0m;
                string CCName = TxtCC.Text;

                string DepMethod = Sm.GetValue("Select DepreciationCode From TblAsset Where AssetCode ='" + TxtAssetCode.Text + "'");

                if (Sm.GetDte(DteDepreciationDt).Length > 0)
                {
                    StartMth = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(4, 2)) - 1;
                    Year = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(0, 4));
                }

                if (mDepreciationTypeGarisLurus == "1" && DepMethod == "1")
                {
                    EndMth = decimal.Parse(TxtEcoLife.Text);
                }
                else if (mDepreciationTypeSaldoMenurun == "1" && DepMethod == "2")
                {
                    EndMth = decimal.Parse(TxtEcoLife.Text);
                }
                else
                {
                    EndMth = 12 - StartMth;
                    if (TxtEcoLifeYr.Text.Length > 0)
                        EndMth += ((decimal.Parse(TxtEcoLifeYr.Text) - 1) * 12);
                }

                var cm = new MySqlCommand()
                {
                    CommandText = "Select CCCode From TblAsset Where AssetCode = @AssetCode "
                };
                Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

                string CCCode = Sm.GetValue(cm);

               
                    for (int x = 0; x < EndMth; x++)
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[x, 3].Value = x + 1;
                        if (StartMth == 12)
                        {
                            StartMth = 1;
                            Year = Year + 1;
                        }
                        else
                        {
                            StartMth += 1;
                        }
                        Grd1.Cells[x, 1].Value = true;
                        if (StartMth.ToString().Length > 1)
                        {
                            Grd1.Cells[x, 4].Value = StartMth;
                        }
                        else
                        {
                            Grd1.Cells[x, 4].Value = String.Concat('0', StartMth);
                        }
                        Grd1.Cells[x, 5].Value = Year;
                        Grd1.Cells[x, 7].Value = CCCode;
                        Grd1.Cells[x, 8].Value = CCName;
                    }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        public void  SetCostValue()
        {
                decimal 
                    StartMth = 0m,
                    CostValue = 0m,
                    Prosen = 0m,
                    EcoLife = 0m,
                    ResidualMonth = 0m;

                int hitungLastYr = 0;
                string Year = string.Empty;
                string DepMethod = string.Empty;

                if (Sm.GetDte(DteDepreciationDt).Length>0) 
                {
                    StartMth = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(4, 2)) - 1;
                    Year = Sm.GetDte(DteDepreciationDt).Substring(0, 4);
                }

                if (TxtAssetValue.Text.Length > 0) 
                    CostValue = Decimal.Parse(TxtAssetValue.Text);
                if (TxtPercentageAnnualDepreciation.Text.Length > 0) 
                    Prosen = Decimal.Parse(TxtPercentageAnnualDepreciation.Text);

                EcoLife = 12 - StartMth;
                ResidualMonth = 12 - StartMth;
                if (TxtEcoLifeYr.Text.Length > 0) 
                    EcoLife += ((decimal.Parse(TxtEcoLifeYr.Text) - 1) * 12);

                if (mDepreciationTypeGarisLurus == "1")
                {
                    if (TxtEcoLife.Text.Length > 0)
                        EcoLife = decimal.Parse(TxtEcoLife.Text);
                    else
                        EcoLife = 0;
                }

                //var cm = new MySqlCommand()
                //{ CommandText = "Select DepreciationCode From TblAsset Where AssetCode =@AssetCode ;"};
                //Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

                //DepMethod = Sm.GetValue(cm);

                DepMethod = Sm.GetLue(LueDepreciationCode);

                #region depmethod = 2 saldo menurun
                if (DepMethod == "2")
                {
                    #region deptype = 0  saldo menurun Runsystem
                    if (mDepreciationTypeSaldoMenurun == "0")
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdDec(Grd1, Row, 4) % 12 == 0)
                            {
                                Grd1.Rows[Row].BackColor = Color.Aqua;
                            }

                            if (Row == 0)
                            {
                                Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                                Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                                Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 10) / ResidualMonth, 2), 0);
                            }
                            else
                            {
                                #region Old

                                if (Row == (EcoLife - 1))
                                {
                                    if (mDepreciationSalvageValue == "0")
                                    {
                                        Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(0m, 0);
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 9].Value = Sm.FormatNum(1m, 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(1m, 0);
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)))-1, 2), 0);
                                    }
                                }
                                else
                                {
                                    if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                                    {
                                        if (Row == 0)
                                        {
                                            Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                                        }
                                        else
                                        {
                                            if (Row == 0)
                                                Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                            else
                                                Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                        }
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 10) / CekRow(Sm.GetGrdStr(Grd1, Row, 5)), 2), 0);
                                    }
                                    else
                                    {
                                        Year = Sm.GetGrdStr(Grd1, Row, 5);
                                        if (Row == 0)
                                            CostValue = 0;
                                        else
                                            CostValue = (Sm.GetGrdDec(Grd1, (Row - 1), 9) - Sm.GetGrdDec(Grd1, (Row - 1), 6));

                                        if (Row == 0)
                                            Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                        else
                                            Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 10) / CekRow(Sm.GetGrdStr(Grd1, Row, 5)), 2), 0);
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion

                    #region deptype = 1 saldo menurun Non runsystem
                    else
                    {
                        int CountData = 0;
                        int lastRow = Grd1.Rows.Count - 2;
                        if (Grd1.Rows.Count > 1)
                            LastYr = Sm.GetGrdStr(Grd1, lastRow, 5);

                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            Year = Sm.GetGrdStr(Grd1, Row, 5);
                            CountData = CekRow(Year);
                          
                            if (Row == 0)
                            {
                                Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((((CountData * (Prosen / 100) * CostValue) / 12) / CountData), 2), 0);
                                Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round(CostValue - (((CountData * (Prosen / 100) * CostValue) / 12) / CountData), 2), 0);
                                Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((((CountData * (Prosen / 100) * CostValue) / 12)), 2), 0);
                            }
                            else
                            {
                                if (Row == Grd1.Rows.Count - 2)
                                {
                                    if (mDepreciationSalvageValue == "0")
                                    {
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 6), 2), 0);
                                        Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 10), 2), 0);
                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 6), 2)-1, 0);
                                        Grd1.Cells[Row, 9].Value = Sm.FormatNum(1m, 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 10), 2)-1, 0);
                                    }
                                }
                                else
                                {
                                    if (Sm.GetGrdStr(Grd1, Row, 5) == Sm.GetGrdStr(Grd1, (Row - 1), 5))
                                    {
                                        Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 6), 2), 0);
                                        Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, Row - 1, 9) - Sm.GetGrdDec(Grd1, Row - 1, 6)), 2), 0);
                                        Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row - 1, 10), 2), 0);
                                    }
                                    else
                                    {
                                        if (LastYr == Sm.GetGrdStr(Grd1, Row, 5) && Row != Grd1.Rows.Count - 2)
                                        {
                                            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round(((Sm.GetGrdDec(Grd1, (Row - 1), 9)) / CountData), 2), 0);
                                            decimal Balance = (Sm.GetGrdDec(Grd1, Row - 1, 9) - ((Sm.GetGrdDec(Grd1, (Row - 1), 9)) / CountData));
                                            Grd1.Cells[Row, 9].Value = Sm.FormatNum(Balance <= 0 ? 0 : Balance, 0);
                                            Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 9), 2), 0);
                                        }
                                        else
                                        {
                                            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9) * (Prosen / 100) / CountData), 2), 0);
                                            decimal Balance = Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 9) - (Sm.GetGrdDec(Grd1, (Row - 1), 9) * (Prosen / 100) / CountData), 2);
                                            Grd1.Cells[Row, 9].Value = Sm.FormatNum(Balance <= 0 ? 0 : Balance, 0);
                                            Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9) * (Prosen / 100)), 2), 0);
                                        }
                                    }
                                }                               
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #region depmethod = 1 garis lurus
                else
                {
                    #region depttype = 1 garis lurus non runsystem 
                    if (mDepreciationTypeGarisLurus == "1")
                    {
                        int CountData = 0;
                        int lastRow = Grd1.Rows.Count - 2;
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            Year = Sm.GetGrdStr(Grd1, Row, 5);
                            CountData = CekRow(Year);
                            
                            if (Row == 0)
                            {
                            if (lastRow == 0) Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((CostValue - (CostValue / EcoLife) + decimal.Parse(mDepreciationSalvageValue)), 2), 0);
                            else Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((CostValue - (CostValue / EcoLife)), 2), 0);
                            }
                            else if (Row == lastRow)
                            {
                                if (mDepreciationSalvageValue == "0")
                                    Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                else
                                    Grd1.Cells[Row, 9].Value = Sm.FormatNum(1m, 0);
                            }
                            else
                            {
                                Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                            } 
                            if (Row == lastRow)
                            {
                                if (mDepreciationSalvageValue == "0")
                                {
                                    Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                                    Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData), 2), 0);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife) - 1, 2), 0);
                                    Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData) - 1, 2), 0);
                                }
                            }
                            else
                            {
                                Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                                Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData), 2), 0);
                            }
                        }
                    }
                    #endregion 

                    #region depttype = 0 garis lurus runsystem
                    else
                    {
                        int CountData = 0;
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                            {
                                if (Row == 0)
                                {
                                    CountData = CekRow(Sm.GetGrdStr(Grd1, Row, 5));
                                    Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                }

                                Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2), 0);
                                Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                            }
                            else
                            {
                                CountData = CountData + CekRow(Sm.GetGrdStr(Grd1, Row, 5));
                                if (Row == 0)
                                {
                                     Grd1.Cells[Row, 9].Value = Sm.FormatNum(0m, 0);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 9].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 9)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                }
                                Grd1.Cells[Row, 10].Value = Sm.FormatNum(Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2), 0);
                                Grd1.Cells[Row, 6].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                            }
                        }
                    }
                    #endregion
                }
                #endregion
        }

        private int CekRow(string Year)
        {
            int Count=0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                {
                    Count = Count + 1;
                }
            }
            return Count;
        }

        private int CekRow2(int Row)
        {
            int Count = 0;
            if (Row % 12 == 0)
            {
                Count = 12;
            }
            else
            {
                Count = Row % 12;
            }
            return Count;
        }

        #endregion

        #region Event

        private void BtnAssetCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDepreciationAssetDlg(this));
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAssetValue }, false);
            TxtAssetValue.Focus();
        }

        private void TxtEcoLife_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEcoLife, 0);
            ClearGrd();
            SetMthGrd();
            SetCostValue();
        }

        private void TxtAssetValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAssetValue, 0);
            ClearGrd();
            SetMthGrd();
            SetCostValue();
        }

        private void DteDepreciationDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            SetMthGrd();
            SetCostValue();
        }

        private void TxtEcoLifeYr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEcoLifeYr, 0);
            ClearGrd();
            SetMthGrd();
            SetCostValue();
        }

        private void LueDepreciationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDepreciationCode, new Sm.RefreshLue1(SetLueDepreciationCode));
            if (Sm.GetLue(LueDepreciationCode).Length > 0)
            {
                ClearGrd();
                SetMthGrd();
                SetCostValue();
            }
        }

        private void TxtPercentageAnnualDepreciation_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPercentageAnnualDepreciation, 0);
            ClearGrd();
            SetMthGrd();
            SetCostValue();
        }

        #endregion 
           
    }
}
