﻿#region Update

/*
    22/04/2020 [IBL/TWC] New Reporting
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAssetDepreciationSettingControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAssetDepreciationSettingControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueAssetCategoryCode(ref LueAssetCtCode);
                Sl.SetLueCCCode(ref LueCCCode);
                ChkExcludedSettedAsset.Checked = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();
            
            //SQL.AppendLine("Select A.AssetCode, A.AssetName, C.AssetCategoryName, D.CCName ");
            //SQL.AppendLine("From TblAsset A ");
            //SQL.AppendLine("Inner Join TblCostCenterDtl B  ");
            //SQL.AppendLine("    On A.CCCode=B.CCCode  ");
            //SQL.AppendLine("    And A.AssetCategoryCode=B.AssetCategoryCode ");
            //SQL.AppendLine("    And B.AcNo Is Null ");
            //SQL.AppendLine("Inner Join TblAssetCategory C On A.AssetCategoryCode=C.AssetCategoryCode ");
            //SQL.AppendLine("Inner Join TblCostCenter D On A.CCCode=D.CCCode ");

            SQL.AppendLine("Select * ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.AssetCode, A.AssetName, A.AssetCategoryCode, C.AssetCategoryName, ");
            SQL.AppendLine("        A.CCCode, D.CCName, B.AcNo ");
            SQL.AppendLine("        From TblAsset A ");
            SQL.AppendLine("        Left Join TblCostCenterDtl B On A.CCCode = B.CCCode And A.AssetCategoryCode = B.AssetCategoryCode ");
            SQL.AppendLine("        Left Join tblAssetCategory C On A.AssetCategoryCode = C.AssetCategoryCode ");
            SQL.AppendLine("        Left Join TblCostCenter D on A.CCCode = D.CCCode ");
            SQL.AppendLine("    )T ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new String[]
                {
                    //0
                    "No",

                    //1-4
                    "Asset's Code",
                    "Asset's Name",
                    "Asset's Category",
                    "Cost Center",
                },
                new int[]
                {
                    //0
                    40,

                    //1-4
                    120, 180, 160, 250
                }
            );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
        }

        protected override void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                if (ChkExcludedSettedAsset.Checked) Filter = " Where T.AcNo IS NULL ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "T.AssetCode", "T.AssetName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCtCode), "T.AssetCategoryCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "T.CCCode", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + "Order By T.AssetName",
                        new string[]
                        { 
                            //0
                            "AssetCode", 

                            //1-3
                            "AssetName", "AssetCategoryName", "CCName",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Additional Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueAssetCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCtCode, new Sm.RefreshLue1(Sl.SetLueAssetCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCtCode_ChackedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's Category");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        private void label2_Click(object sender, EventArgs e)
        {

        }

        #endregion
    }
}
