﻿#region Update
/*
    12/12/2019 [WED/MAI] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionRoutingItem : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private bool mIsProductionOrderUseProcessGroup = false;
        internal FrmProductionRoutingItemFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmProductionRoutingItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Production Routing for Item";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            GetParameter();
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            SetGrd();
            LuePGCode.Visible = false;
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Work Center"+Environment.NewLine+"Document#",
                    "",
                    "Work Center"+Environment.NewLine+"Document Name",
                    "Location",

                    //6-10
                    "Capacity",
                    "UoM",
                    "Sequence",
                    "",
                    "BoM#"+Environment.NewLine+"WBS#",
                    
                    //11-14
                    "",
                    "BoM/WBS"+Environment.NewLine+"Name",
                    "Process Group Code",
                    "Process Group"
                },
                 new int[] 
                {
                    100,
                    20, 150, 20, 250, 100,
                    100, 100, 100, 20, 130, 
                    20, 250, 0, 200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 10, 12, 13 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3, 9, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 2);
            if (mIsProductionOrderUseProcessGroup)
                Grd1.Cols[14].Move(1);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5, 6, 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo,DteDocDt,TxtDocName,MeeRemark,ChkActInd, TxtSource
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnProductionRouting.Enabled = false;
                    BtnItCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    ChkActInd.Checked = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt,TxtDocName,MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnProductionRouting.Enabled = true;
                    BtnItCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(ChkActInd, false);
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtDocName, MeeRemark, TxtSource, ChkActInd,
                TxtItCode, TxtItCodeInternal, TxtItName, TxtProductionRoutingDocNo
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionRoutingItemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 1)
                {
                    Sm.FormShowDialog(new FrmProductionRoutingItemDlg3(this));
                }
                if (e.ColIndex == 9 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 2, false, "Work Center is empty."))
                    Sm.FormShowDialog(new FrmProductionRoutingItemDlg4(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), e.RowIndex));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && Sm.IsGrdColSelected(new int[] { 14 }, e.ColIndex))
            {
                if (e.ColIndex == 14) LueRequestEdit(Grd1, LuePGCode, ref fCell, ref fAccept, e, 13);
            }

            if (BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                !IsWorkCenterEmpty(e) &&
                Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
                Sm.GrdRequestEdit(Grd1, e.RowIndex);                
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1) Sm.FormShowDialog(new FrmProductionRoutingItemDlg3(this));
                if (e.ColIndex == 9 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 2, false, "Work Center is empty.")) 
                    Sm.FormShowDialog(new FrmProductionRoutingItemDlg4(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), e.RowIndex));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProductionRoutingItemHdr(DocNo);
                ShowProductionRoutingItemDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        public void ShowData2(string DocNo)
        {
            try
            {
                Sm.ClearGrd(Grd1, true);
                ShowProductionRoutingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowProductionRoutingItemHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, B.DocName, A.ProductionRoutingDocNo, ");
            SQL.AppendLine("A.ItCode, C.ItName, C.ItCodeInternal, C.Specification, A.Remark ");
            SQL.AppendLine("From TblProductionRoutingItemHdr A ");
            SQL.AppendLine("Inner Join TblProductionRoutingHdr B On A.ProductionRoutingDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
            ref cm, SQL.ToString(),
            new string[] 
            {
                "DocNo",
                "DocDt", "ActInd", "DocName", "ProductionRoutingDocNo", "ItCode", 
                "ItName", "ItCodeInternal", "Specification", "Remark"
            },
            (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtDocName.EditValue = Sm.DrStr(dr, c[3]);
                    TxtProductionRoutingDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[7]);
                    TxtSpecification.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowProductionRoutingItemDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, B.WorkCenterDocNo, C.DocName, C.Location, C.Capacity, E.UomName, B.Sequence, ");
            SQL.AppendLine("A.BOMDocNo, D.DocName BOMDocName, A.PGCode, Concat(F.PGCode, ' : ', F.PGName) As PGName ");
            SQL.AppendLine("From TblProductionRoutingItemDtl A ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl B On A.ProductionRoutingDocNo = B.DocNo And A.ProductionRoutingDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr C On B.WorkCenterDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOMHdr D On A.BOMDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblUom E On C.UomCode = E.UomCode ");
            SQL.AppendLine("Left Join TblProcessGroup F On A.PGCode = F.PGCode ");
            SQL.AppendLine("Order by A.DNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "WorkCenterDocNo", "DocName", "Location", "Capacity", "UomName", 

                    //6-10
                    "Sequence", "BOMDocNo", "BOMDocName", "PGCode", "PGName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 10);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowProductionRoutingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkCenterDocNo, B.DocName, B.Location, B.Capacity, C.UomName, A.Sequence ");
            SQL.AppendLine("From TblProductionRoutingDtl A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblUom C On B.UomCode=C.UomCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.Sequence");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "WorkCenterDocNo","DocName","Location","Capacity","UomName",

                    //6
                    "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (IsDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionRoutingItem", "TblProductionRoutingItemHdr");
            string PRDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionRouting", "TblProductionRoutingHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProductionRoutingItemHdr(DocNo, PRDocNo));
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProductionRoutingItemDtl(DocNo, PRDocNo, Row));
            }

            cml.Add(SaveProductionRoutingHdr(PRDocNo));
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProductionRoutingDtl(PRDocNo, Row));
            }

            cml.Add(DeactivateOtherRoutingItem(DocNo, PRDocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                Sm.IsTxtEmpty(TxtDocName, "Document Name", false) ||
                Sm.IsTxtEmpty(TxtItCode, "Item", false) ||
                IsGrd1Empty() ||
                IsGrd1ValueNotValid() ||
                IsBomNotValid()
                ;
        }

        private bool IsBomNotValid()
        {
            decimal MaxSeq = 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0 &&
                    MaxSeq < Sm.GetGrdDec(Grd1, Row, 8))
                    MaxSeq = Sm.GetGrdDec(Grd1, Row, 8);
            }

            string SelectedBomDocNo = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 10).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 8) == MaxSeq)
                {
                    if (SelectedBomDocNo.Length != 0) SelectedBomDocNo += ", ";
                    SelectedBomDocNo += "##" + Sm.GetGrdStr(Grd1, Row, 10) + "##";
                }
            }
            SelectedBomDocNo = (SelectedBomDocNo.Length == 0 ? "##XXX##" : SelectedBomDocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBomDtl2 ");
            SQL.AppendLine("Where ItCode<>@ItCode ");
            SQL.AppendLine("And ItType='1' ");
            SQL.AppendLine("And Locate(Concat('##', DocNo, '##'), @SelectedBomDocNo)>0 ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@SelectedBomDocNo", SelectedBomDocNo);

            var BomDocNo = Sm.GetValue(cm);

            if (BomDocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bom# : " + BomDocNo + Environment.NewLine +
                    "Invalid product result."
                    );
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Work center Name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Sequance is zero.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 10, false, "BOM is empty.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveProductionRoutingItemHdr(string DocNo, string PRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into tblProductionRoutingItemHdr(DocNo, DocDt, ActInd, ProductionRoutingDocNo, ItCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'Y', @ProductionRoutingDocNo, @ItCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDocNo", PRDocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionRoutingItemDtl(string DocNo, string PRDocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProductionRoutingItemDtl(DocNo, DNo, ProductionRoutingDocNo, ProductionRoutingDNo, BOMDocNo, PGCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @ProductionRoutingDocNo, @ProductionRoutingDNo, @BOMDocNo, @PGCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDocNo", PRDocNo);
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionRoutingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into tblProductionRoutinghdr(DocNo,DocDt,DocName,ActiveInd,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo,@DocDt,@DocName,@ActiveInd,@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionRoutingDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProductionRoutingDtl(DocNo,DNo,WorkCenterDocNo, Sequence, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo,@DNo,@WorkCenterDocNo,@Sequence, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeactivateOtherRoutingItem(string DocNo, string PRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionRoutingItemHdr A ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.ActInd = 'N', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo <> @DocNo ");
            SQL.AppendLine("And A.ItCode = @ItCode ");
            SQL.AppendLine("And A.ActInd = 'Y'; ");

            SQL.AppendLine("Update TblProductionRoutingHdr A ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.ActiveInd = 'N', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct ProductionRoutingDocNo ");
            SQL.AppendLine("    From TblProductionRoutingItemHdr ");
            SQL.AppendLine("    Where ActInd = 'N' ");
            SQL.AppendLine("    And DocNo <> @DocNo ");
            SQL.AppendLine("    And ProductionRoutingDocNo <> @ProductionRoutingDocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ActiveInd = 'Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDocNo", PRDocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelProductionRoutingItemHdr());
            cml.Add(CancelProductionRoutingHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            string a = ChkActInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Select DocNo From TblProductionRoutingItemHdr ");
                SQL.AppendLine("Where ActiveInd='N' And DocNo=@DocNo ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is no actived already.");
                    return true;
                }

            }
            else if (a == "Y")
            {
                SQL.AppendLine("Select DocNo From TblProductionRoutingItemHdr ");
                SQL.AppendLine("Where ActiveInd='Y' And DocNo=@DocNo ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is actived already.");
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand CancelProductionRoutingItemHdr()
        {
            string a = ChkActInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Update TblProductionRoutingItemHdr Set ");
                SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActInd='Y' ");
            }
            else if (a == "Y")
            {
                SQL.AppendLine("Update TblProductionRoutingItemHdr Set ");
                SQL.AppendLine("    ActInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActInd='N' ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelProductionRoutingHdr()
        {
            string a = ChkActInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Update TblProductionRoutingHdr Set ");
                SQL.AppendLine("    ActiveInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActiveInd='Y' ");
            }
            else if (a == "Y")
            {
                SQL.AppendLine("Update TblProductionRoutingHdr Set ");
                SQL.AppendLine("    ActiveInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActiveInd='N' ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtProductionRoutingDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLuePGCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, Concat(PGCode, ' : ', PGName) As Col2 From TblProcessGroup " +
                "Where PGCode Not In (Select Parent From TblProcessGroup Where Parent Is Not Null) " +
                "Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsProductionOrderUseProcessGroup = Sm.GetParameterBoo("IsProductionOrderUseProcessGroup");
        }

        private bool IsWorkCenterEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                //e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(SetLuePGCode));
        }

        private void LuePGCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePGCode_Leave(object sender, EventArgs e)
        {
            if (LuePGCode.Visible && fAccept)
            {
                if (Sm.GetLue(LuePGCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LuePGCode);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = LuePGCode.GetColumnValue("Col2");
                }
                LuePGCode.Visible = false;
            }
        }

        #endregion

        #region Button Click

        private void BtnProductionRouting_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProductionRoutingItemDlg(this));
            }
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProductionRoutingItemDlg2(this));
            }
        }

        private void BtnItCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item", false))
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = TxtItCode.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
