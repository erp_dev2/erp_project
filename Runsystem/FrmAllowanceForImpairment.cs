﻿#region Update
/*
    02/02/2022 [TYO + RDA + WED/PRODUCT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAllowanceForImpairment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string[] mProductPeriodFrom = { "0", "31", "61", "91", "181", "366" };
        private string[] mProductPeriodTo = { "30", "60", "90", "180", "365", "∞" };

        private string[] mNonProductPeriodFrom = { "0", "31", "61", "91", "181", "366", "731", "1096", "1461" };
        private string[] mNonProductPeriodTo = { "30", "60", "90", "180", "365", "730", "1095", "1460", "∞" };

        private string mAgingARDaysFormat = string.Empty;

        internal FrmAllowanceForImpairmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmAllowanceForImpairment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Allowance for Impairment of AR Setting";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                SetGrd();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            //Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",

                    //1-4
                    "Aging Days"+Environment.NewLine+"Period",
                    "From",
                    "To",
                    "Allowance for"+Environment.NewLine+"Impairment of"+Environment.NewLine+"Receivable Percentage"+Environment.NewLine+"(%)",
                },
                 new int[]
                {
                    //0
                    20,

                    //1-4
                    120, 100, 100, 180,
                    
                }
            );

            //for (int Row = 0; Row < 6; Row++)
            //{
            //    Grd1.Rows.Add();
            //    Grd1.Cells[Row, 0].Value = "00" + (Row + 1);
            //    Grd1.Cells[Row, 1].Value = "Period " + (Row + 1);
            //}

            //Grd1.Cells[0, 2].Value = "0";
            //Grd1.Cells[1, 2].Value = "31";
            //Grd1.Cells[2, 2].Value = "61";
            //Grd1.Cells[3, 2].Value = "91";
            //Grd1.Cells[4, 2].Value = "181";
            //Grd1.Cells[5, 2].Value = "366";

            //Grd1.Cells[0, 3].Value = "30";
            //Grd1.Cells[1, 3].Value = "60";
            //Grd1.Cells[2, 3].Value = "90";
            //Grd1.Cells[3, 3].Value = "180";
            //Grd1.Cells[4, 3].Value = "365";
            //Grd1.Cells[5, 3].Value = "∞";

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            SetGrdColRowHdrAutoAlign(Grd1);

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, TxtTitle, DteDocDt, MeeRemark, ChkCancelInd
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtTitle, DteDocDt, MeeRemark
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, TxtTitle, DteDocDt, MeeRemark, ChkCancelInd
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtDocNo, TxtTitle, DteDocDt, MeeRemark
            });

            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, false);
        }

        private void GetParameter()
        {
            mAgingARDaysFormat = Sm.GetParameter("AgingARDaysFormat");

            if (mAgingARDaysFormat.Length == 0) mAgingARDaysFormat = "1"; // 1 = Product, 2 = Non Product
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAllowanceForImpairmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                InitDetailData(1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AllowanceForImpairment", "TblAllowanceForImpairmentHdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveAFIHdr(DocNo));
                cml.Add(SaveAFIDtl(DocNo));
                cml.Add(CancelOtherAFIDocument(DocNo));

                Sm.ExecCommands(cml);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand SaveAFIHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/*Save Allowance Impairment Hdr*/");
            SQL.AppendLine("Insert Into TblAllowanceForImpairmentHdr(DocNo, DocDt, Title, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Title, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Title", TxtTitle.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);

            return cm;
        }

        private MySqlCommand SaveAFIDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;

            SQL.AppendLine("/*Save Allowance Impairment Dtl*/");
            SQL.AppendLine("Insert Into TblAllowanceForImpairmentDtl(DocNo, DNo, Percentage, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (i != 0) SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo, @DNo__" + i.ToString() + ", @Percentage__" + i.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo__" + i.ToString(), Sm.Right(string.Concat("000", (Row + 1).ToString()), 3));
                Sm.CmParam<Decimal>(ref cm, "@Percentage__" + i.ToString(), Sm.GetGrdDec(Grd1, Row, 4));

                i += 1;
            }
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelOtherAFIDocument(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAllowanceForImpairmentHdr Set ");
            SQL.AppendLine("    CancelInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo != @DocNo ");
            SQL.AppendLine("And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtTitle, "Title", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 record.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdDec(Grd1, i, 4) < 0m || Sm.GetGrdDec(Grd1, i, 4) > 100m)
                {
                    Sm.StdMsg(mMsgType.Warning, "Valid percentage is between 0 and 100.");
                    Sm.FocusGrd(Grd1, i, 4);
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                EditAFI();
                ShowData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtTitle, "Title", false) ||
                IsGrdEmpty() ||
                IsDocNoAlreadyCancelled()
                ;
        }

        private bool IsDocNoAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblAllowanceForImpairmentHdr " +
                "Where DocNo=@DocNo And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already canceled.");
        }

        private void EditAFI()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" /*Update Allowance For Impairment */ ");
            SQL.AppendLine("Update TblAllowanceForImpairmentHdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo =@DocNo ");
            SQL.AppendLine("And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                ShowAllowanceForImpairmentHdr(DocNo);
                ShowAllowanceForImpairmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAllowanceForImpairmentHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, A.Title, A.Remark ");
            SQL.AppendLine("FROM tblallowanceforimpairmenthdr A ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "CancelInd", "Title", "Remark",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtTitle.EditValue = Sm.DrStr(dr, c[3]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                }, true
            );
        }

        private void ShowAllowanceForImpairmentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.Percentage ");
            SQL.AppendLine("FROM tblallowanceforimpairmentdtl A ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo",
                    
                    //1
                    "Percentage",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0); //DNo
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 1); //Percentage
                }, false, false, false, false
            );

            InitDetailData(2);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 4) > 100m || Sm.GetGrdDec(Grd1, e.RowIndex, 4) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 1) +
                        " allowance percentage must between 0 - 100 "
                        );
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly) Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void SetGrdColRowHdrAutoAlign(iGrid Grd)
        {
            for (int Index = 0; Index < Grd.Cols.Count; Index++)
            {
                Grd.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            }

            for (int Index = 0; Index < Grd.Rows.Count; Index++)
            {
                Grd.Rows[Index].TextAlign = iGContentAlignment.MiddleCenter;
            }
        }

        #endregion

        #region Additional Method

        private void InitDetailData(byte state)
        {
            if (mAgingARDaysFormat == "1")
            {
                if (state == 1) // insert
                {
                    Grd1.BeginUpdate();

                    for (int i = 0; i < mProductPeriodFrom.Count(); ++i)
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = string.Concat("Period ", (i + 1).ToString());
                        Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = mProductPeriodFrom[i];
                        Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = mProductPeriodTo[i];
                        Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = 0m;
                    }

                    Grd1.EndUpdate();
                }
                else
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                        {
                            Grd1.Cells[i, 1].Value = string.Concat("Period ", (i + 1).ToString());
                            Grd1.Cells[i, 2].Value = mProductPeriodFrom[i];
                            Grd1.Cells[i, 3].Value = mProductPeriodTo[i];
                        }
                    }
                }
            }
            else
            {
                if (state == 1) // insert
                {
                    Grd1.BeginUpdate();

                    for (int i = 0; i < mNonProductPeriodFrom.Count(); ++i)
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = string.Concat("Period ", (i + 1).ToString());
                        Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = mNonProductPeriodFrom[i];
                        Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = mNonProductPeriodTo[i];
                        Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = 0m;
                    }

                    Grd1.EndUpdate();
                }
                else
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                        {
                            Grd1.Cells[i, 1].Value = string.Concat("Period ", (i + 1).ToString());
                            Grd1.Cells[i, 2].Value = mNonProductPeriodFrom[i];
                            Grd1.Cells[i, 3].Value = mNonProductPeriodTo[i];
                        }
                    }
                }
            }
        }

        #endregion

        #endregion


    }
}
