﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPenalty : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mPenaltyCode="" ;
        internal FrmPenaltyFind FrmFind;

        #endregion

        #region Constructor

        public FrmPenalty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Penalty";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            SetLuePenaltyCtCode(ref LuePenaltyCtCode);

            if (mPenaltyCode.Length != 0)
            {
                ShowData(mPenaltyCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPenaltyCode, ChkActInd, TxtPenaltyName, TxtAmt, LuePenaltyCtCode
                    }, true);
                    TxtPenaltyCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtPenaltyCode, TxtPenaltyName, TxtAmt, LuePenaltyCtCode
                    }, false);
                    TxtPenaltyCode.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    TxtPenaltyCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtPenaltyCode, TxtPenaltyName, TxtAmt, LuePenaltyCtCode
            });
            ChkActInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPenaltyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPenaltyCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into Tblpenalty(PenaltyCode, PenaltyName, ActInd, Amt, PenaltyCtCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PenaltyCode, @PenaltyName, @ActInd, @Amt, @PenaltyCtCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update PenaltyName=@PenaltyName, ActInd=@ActInd, Amt=@Amt, PenaltyCtCode=@PenaltyCtCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PenaltyCode", TxtPenaltyCode.Text);
                Sm.CmParam<String>(ref cm, "@PenaltyName", TxtPenaltyName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@PenaltyCtCode", Sm.GetLue(LuePenaltyCtCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPenaltyCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PenaltyCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PenaltyCode", PenaltyCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select PenaltyCode, PenaltyName, ActInd, Amt, PenaltyCtCode From TblPenalty Where PenaltyCode=@PenaltyCode ",
                        new string[] 
                        {
                            "PenaltyCode", "PenaltyName", "ActInd", "Amt", "PenaltyCtCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPenaltyCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPenaltyName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            TxtAmt.EditValue = Sm.DrDec(dr, c[3]);
                            Sm.SetLue(LuePenaltyCtCode, Sm.DrStr(dr, c[4]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPenaltyCode, "Penalty code", false) ||
                Sm.IsTxtEmpty(TxtPenaltyName, "Penalty name", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePenaltyCtCode, "Penalty Category")||
                IsCityCodeExisted()||
                IsDataInactiveAlready();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtPenaltyCode.Properties.ReadOnly && Sm.IsDataExist("Select PenaltyCode From TblPenalty Where PenaltyCode='" + TxtPenaltyCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Penalty code ( " + TxtPenaltyCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select PenaltyCode From Tblpenalty " +
                    "Where ActInd='N' And PenaltyCode=@PenaltyCode;"
            };
            Sm.CmParam<String>(ref cm, "@PenaltyCode", TxtPenaltyCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLuePenaltyCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PenaltyCtCode As Col1, PenaltyCtName As Col2 From TblPenaltyCategory",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtPenaltyCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPenaltyCode);
        }

        private void TxtPenaltyName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPenaltyName);
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePenaltyCtCode, new Sm.RefreshLue1(SetLuePenaltyCtCode));
        }

        #endregion

    }
}
