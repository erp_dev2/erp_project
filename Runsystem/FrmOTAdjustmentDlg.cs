﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTAdjustmentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmOTAdjustment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmOTAdjustmentDlg(FrmOTAdjustment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Employee's"+Environment.NewLine+"Old Code",
                        "Position",
                        "Department",
                        
                        //6-10
                        "Requested#",
                        "Requested D#",
                        "OT"+Environment.NewLine+"Date",
                        "Start"+Environment.NewLine+"Time",
                        "End"+Environment.NewLine+"Time",

                        //11
                        "DeptCode"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        100, 250, 80, 150, 150, 
                        
                        //6-10
                        130, 0, 100, 100, 100,

                        //11
                        0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 7, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, ");
            SQL.AppendLine("A.DocNo, B.DNo, A.OTDt, B.OTStartTm, B.OTEndTm, C.DeptCode ");
            SQL.AppendLine("From TblOTRequestHdr A ");
            SQL.AppendLine("Inner Join TblOTRequestDtl B On A.DocNo=B.DocNo And B.AdjustedInd='N' ");
            SQL.AppendLine("Inner Join TblEmployee C ");
            SQL.AppendLine("    On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment E On C.DeptCode=E.DeptCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName", "C.EmpCodeOld" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.EmpName;",
                        new string[] 
                        { 
                             //0
                             "EmpCode", 
                             
                             //1-5
                             "EmpName", 
                             "EmpCodeOld", 
                             "PosName",
                             "DeptName",
                             "DocNo",
                             
                             //6-10
                             "DNo",
                             "OTDt",
                             "OTStartTm",
                             "OTEndTm",
                             "DeptCode"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtEmpCodeOld.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtPosCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtOTRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.mOTRequestDNo = Sm.GetGrdStr(Grd1, Row, 7);
                Sm.SetDte(mFrmParent.DteOTDt, Sm.GetGrdDate(Grd1, Row, 8));
                Sm.SetTme(mFrmParent.TmeRequestedStartTm, Sm.GetGrdStr(Grd1, Row, 9));
                Sm.SetTme(mFrmParent.TmeRequestedEndTm, Sm.GetGrdStr(Grd1, Row, 10));
                Sm.SetTme(mFrmParent.TmeStartTm, Sm.GetGrdStr(Grd1, Row, 9));
                Sm.SetTme(mFrmParent.TmeEndTm, Sm.GetGrdStr(Grd1, Row, 10));
                mFrmParent.mDeptCode = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.ShowAttendanceLog();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
