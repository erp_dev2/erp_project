﻿namespace RunSystem
{
    partial class FrmItemBarcode2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueBin = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtProdCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtProdDesc = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueSellerCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkProdCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkProdDesc = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSellerCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkBin = new DevExpress.XtraEditors.CheckEdit();
            this.ChkWhsCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSellerCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSellerCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkWhsCode);
            this.panel2.Controls.Add(this.ChkBin);
            this.panel2.Controls.Add(this.ChkSellerCode);
            this.panel2.Controls.Add(this.ChkProdDesc);
            this.panel2.Controls.Add(this.ChkProdCode);
            this.panel2.Controls.Add(this.LueSellerCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtProdDesc);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueBin);
            this.panel2.Controls.Add(this.TxtProdCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            // 
            // LueBin
            // 
            this.LueBin.EnterMoveNextControl = true;
            this.LueBin.Location = new System.Drawing.Point(176, 95);
            this.LueBin.Margin = new System.Windows.Forms.Padding(5);
            this.LueBin.Name = "LueBin";
            this.LueBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.Appearance.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBin.Properties.DropDownRows = 25;
            this.LueBin.Properties.NullText = "[Empty]";
            this.LueBin.Properties.PopupWidth = 500;
            this.LueBin.Size = new System.Drawing.Size(320, 20);
            this.LueBin.TabIndex = 24;
            this.LueBin.ToolTip = "F4 : Show/hide list";
            this.LueBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBin.EditValueChanged += new System.EventHandler(this.LueBin_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(144, 98);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Bin";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProdCode
            // 
            this.TxtProdCode.EnterMoveNextControl = true;
            this.TxtProdCode.Location = new System.Drawing.Point(177, 25);
            this.TxtProdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProdCode.Name = "TxtProdCode";
            this.TxtProdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProdCode.Properties.MaxLength = 400;
            this.TxtProdCode.Size = new System.Drawing.Size(319, 20);
            this.TxtProdCode.TabIndex = 20;
            this.TxtProdCode.Validated += new System.EventHandler(this.TxtProdCode_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(85, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Product Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProdDesc
            // 
            this.TxtProdDesc.EnterMoveNextControl = true;
            this.TxtProdDesc.Location = new System.Drawing.Point(177, 48);
            this.TxtProdDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProdDesc.Name = "TxtProdDesc";
            this.TxtProdDesc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProdDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProdDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProdDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtProdDesc.Properties.MaxLength = 255;
            this.TxtProdDesc.Size = new System.Drawing.Size(546, 20);
            this.TxtProdDesc.TabIndex = 26;
            this.TxtProdDesc.Validated += new System.EventHandler(this.TxtProdDesc_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(53, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 14);
            this.label2.TabIndex = 25;
            this.label2.Text = "Product Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(177, 119);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(320, 20);
            this.LueWhsCode.TabIndex = 28;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(98, 122);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 27;
            this.label1.Text = "Warehouse";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSellerCode
            // 
            this.LueSellerCode.EnterMoveNextControl = true;
            this.LueSellerCode.Location = new System.Drawing.Point(177, 71);
            this.LueSellerCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSellerCode.Name = "LueSellerCode";
            this.LueSellerCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSellerCode.Properties.Appearance.Options.UseFont = true;
            this.LueSellerCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSellerCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSellerCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSellerCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSellerCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSellerCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSellerCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSellerCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSellerCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSellerCode.Properties.DropDownRows = 25;
            this.LueSellerCode.Properties.NullText = "[Empty]";
            this.LueSellerCode.Properties.PopupWidth = 500;
            this.LueSellerCode.Size = new System.Drawing.Size(320, 20);
            this.LueSellerCode.TabIndex = 30;
            this.LueSellerCode.ToolTip = "F4 : Show/hide list";
            this.LueSellerCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSellerCode.EditValueChanged += new System.EventHandler(this.LueSellerCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(131, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "Seller";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkProdCode
            // 
            this.ChkProdCode.Location = new System.Drawing.Point(504, 23);
            this.ChkProdCode.Name = "ChkProdCode";
            this.ChkProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProdCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProdCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProdCode.Properties.Caption = " ";
            this.ChkProdCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdCode.Size = new System.Drawing.Size(20, 22);
            this.ChkProdCode.TabIndex = 31;
            this.ChkProdCode.ToolTip = "Remove filter";
            this.ChkProdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProdCode.ToolTipTitle = "Run System";
            this.ChkProdCode.CheckedChanged += new System.EventHandler(this.ChkProdCode_CheckedChanged);
            // 
            // ChkProdDesc
            // 
            this.ChkProdDesc.Location = new System.Drawing.Point(731, 46);
            this.ChkProdDesc.Name = "ChkProdDesc";
            this.ChkProdDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdDesc.Properties.Appearance.Options.UseFont = true;
            this.ChkProdDesc.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProdDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProdDesc.Properties.Caption = " ";
            this.ChkProdDesc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdDesc.Size = new System.Drawing.Size(20, 22);
            this.ChkProdDesc.TabIndex = 32;
            this.ChkProdDesc.ToolTip = "Remove filter";
            this.ChkProdDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProdDesc.ToolTipTitle = "Run System";
            this.ChkProdDesc.CheckedChanged += new System.EventHandler(this.ChkProdDesc_CheckedChanged);
            // 
            // ChkSellerCode
            // 
            this.ChkSellerCode.Location = new System.Drawing.Point(504, 72);
            this.ChkSellerCode.Name = "ChkSellerCode";
            this.ChkSellerCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSellerCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSellerCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSellerCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSellerCode.Properties.Caption = " ";
            this.ChkSellerCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSellerCode.Size = new System.Drawing.Size(20, 22);
            this.ChkSellerCode.TabIndex = 33;
            this.ChkSellerCode.ToolTip = "Remove filter";
            this.ChkSellerCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSellerCode.ToolTipTitle = "Run System";
            this.ChkSellerCode.CheckedChanged += new System.EventHandler(this.ChkSellerCode_CheckedChanged);
            // 
            // ChkBin
            // 
            this.ChkBin.Location = new System.Drawing.Point(504, 96);
            this.ChkBin.Name = "ChkBin";
            this.ChkBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBin.Properties.Appearance.Options.UseFont = true;
            this.ChkBin.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBin.Properties.Caption = " ";
            this.ChkBin.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBin.Size = new System.Drawing.Size(20, 22);
            this.ChkBin.TabIndex = 34;
            this.ChkBin.ToolTip = "Remove filter";
            this.ChkBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBin.ToolTipTitle = "Run System";
            this.ChkBin.CheckedChanged += new System.EventHandler(this.ChkBin_CheckedChanged);
            // 
            // ChkWhsCode
            // 
            this.ChkWhsCode.Location = new System.Drawing.Point(504, 117);
            this.ChkWhsCode.Name = "ChkWhsCode";
            this.ChkWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWhsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkWhsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWhsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWhsCode.Properties.Caption = " ";
            this.ChkWhsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWhsCode.Size = new System.Drawing.Size(20, 22);
            this.ChkWhsCode.TabIndex = 35;
            this.ChkWhsCode.ToolTip = "Remove filter";
            this.ChkWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWhsCode.ToolTipTitle = "Run System";
            this.ChkWhsCode.CheckedChanged += new System.EventHandler(this.ChkWhsCode_CheckedChanged);
            // 
            // FrmItemBarcode2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmItemBarcode2";
            this.Text = "Product\'s Barcode";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSellerCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSellerCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueBin;
        private DevExpress.XtraEditors.TextEdit TxtProdCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtProdDesc;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueSellerCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkProdCode;
        private DevExpress.XtraEditors.CheckEdit ChkProdDesc;
        private DevExpress.XtraEditors.CheckEdit ChkWhsCode;
        private DevExpress.XtraEditors.CheckEdit ChkBin;
        private DevExpress.XtraEditors.CheckEdit ChkSellerCode;
    }
}