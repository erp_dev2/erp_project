﻿#region Update
/*
    18/03/2020 [WED/YK] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDetailContractRealization : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mJointOperationTypeForKSOLead = string.Empty,
            mJointOperationTypeForKSOMember = string.Empty,
            mJointOperationTypeForNonKSO = string.Empty,
            mSiteCode = string.Empty,
            mTypeForDetailRealization = string.Empty,
            mDescForDetailRealization = string.Empty;

        private int[] mColDec = { 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43 };

        #endregion

        #region Constructor

        public FrmRptDetailContractRealization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 45;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "KSO/Non-KSO",
                    "Carry Over/New",
                    "NAS",
                    "UP",
                    "Project Code",
                    
                    //6-10
                    "Project Name", 
                    "Year",
                    "Resource", 
                    "Scope of Work",
                    "Type",

                    //11-15
                    "Item Name",
                    "Customer",
                    "Contract Date",
                    "Non-PPN",
                    "Position : LF",

                    //16-20
                    "Position : M",
                    "Joint Operation"+Environment.NewLine+"Member",
                    "Percentage",
                    "Joint Operation"+Environment.NewLine+"NPWP",
                    "RKAP Amount",

                    //21-25
                    "Opening Balance"+Environment.NewLine+"Contract Amount",
                    "Carry Over",
                    "Realization"+Environment.NewLine+"January",
                    "Realization"+Environment.NewLine+"February",
                    "Realization"+Environment.NewLine+"March",

                    //26-30
                    "Total 1st Quarterly",
                    "Realization"+Environment.NewLine+"April",
                    "Realization"+Environment.NewLine+"May",
                    "Realization"+Environment.NewLine+"June",
                    "Total 2nd Quarterly",

                    //31-35
                    "Realization"+Environment.NewLine+"July",
                    "Realization"+Environment.NewLine+"August",
                    "Realization"+Environment.NewLine+"September",
                    "Total 3rd Quarterly",
                    "Realization"+Environment.NewLine+"October",

                    //36-40
                    "Realization"+Environment.NewLine+"November",
                    "Realization"+Environment.NewLine+"December",
                    "Total 4th Quarterly",
                    "Total New Contract",
                    "Total Contract",

                    //41-44
                    "Total Co +"+Environment.NewLine+"New Contract",
                    "Percentage"+Environment.NewLine+"towards Target",
                    "Balance",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 60, 60, 150, 
                    
                    //6-10
                    150, 150, 150, 150, 150, 

                    //11-15
                    150, 150, 150, 150, 150, 

                    //16-20
                    150, 150, 150, 150, 150, 

                    //21-25
                    150, 150, 150, 150, 150, 

                    //26-30
                    150, 150, 150, 150, 150, 

                    //31-35
                    150, 150, 150, 150, 150, 

                    //36-40
                    150, 150, 150, 150, 150, 
                    
                    //41-44
                    150, 150, 150, 200
                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ContractRealization>();
                var l2 = new List<AuctionBranchToSite>();
                var l3 = new List<DummyCounter>();
                var l4 = new List<SiteCounter>();

                mSiteCode = Sm.GetLue(LueSiteCode);

                ProcessSiteCounter(ref l3);
                ProcessBranchToSite(ref l2);

                if (l2.Count > 0 && l3.Count > 0)
                {
                    ProcessSiteCounter2(ref l2, ref l3, ref l4);
                }

                Process1(ref l, ref l4);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mTypeForDetailRealization = Sm.GetParameter("TypeForDetailRealization");
            mDescForDetailRealization = Sm.GetParameter("DescForDetailRealization");
            mJointOperationTypeForKSOLead = Sm.GetParameter("JointOperationTypeForKSOLead");
            mJointOperationTypeForKSOMember = Sm.GetParameter("JointOperationTypeForKSOMember");
            mJointOperationTypeForNonKSO = Sm.GetParameter("JointOperationTypeForNonKSO");

            if (mTypeForDetailRealization.Length == 0) mTypeForDetailRealization = "Carry Over,Proyek Baru";
            if (mDescForDetailRealization.Length == 0) mDescForDetailRealization = "PROYEK-PROYEK NON-KSO,PROYEK-PROYEK KSO,PROYEK-PROYEK KSO (Member) ";
        }

        private void ProcessSiteCounter(ref List<DummyCounter> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SiteCode, Count(A.DocNo) Counter ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("    And C.Status = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    And Left(C.DocDt, 4) = @Yr ");
            SQL.AppendLine("Group By A.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "Counter" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DummyCounter()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            No = Sm.DrInt(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSiteCounter2(ref List<AuctionBranchToSite> l, ref List<DummyCounter> l2, ref List<SiteCounter> l3)
        {
            foreach(var x in l2)
            {
                foreach(var y in l)
                {
                    if (x.SiteCode == y.BranchCode)
                    {
                        x.SiteCode = y.SiteCode;
                        break;
                    }
                }
            }

            l3 = l2.GroupBy(x => x.SiteCode)
                .Select(t => new SiteCounter()
                {
                    SiteCode = t.Key,
                    No = 0,
                    No2 = t.Sum(s => s.No)
                }).ToList();

            for (int i = 0; i < l3.Count; ++i)
            {
                if (i != 0)
                {
                    l3[i].No = l3[i - 1].No2 + 1;
                    l3[i].No2 += l3[i - 1].No2;
                }
                else
                {
                    l3[i].No = 1;
                }
            }
        }

        private void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT SiteCode Col1, SiteName Col2 ");
            SQL.AppendLine("FROM tblsite ");
            SQL.AppendLine("WHERE FIND_IN_SET(sitecode, (SELECT parvalue FROM tblparameter WHERE parcode = 'SiteCodeForAuctionInfo')) ");
            SQL.AppendLine("AND sitecode NOT IN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT optcode ");
            SQL.AppendLine("    FROM tbloption ");
            SQL.AppendLine("    WHERE optcat = 'AuctionBranchToSite' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By SiteCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ProcessBranchToSite(ref List<AuctionBranchToSite> l)
        {
            string sSQL = "Select OptCode, OptDesc From TblOption Where OptCat = 'AuctionBranchToSite';";
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = sSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionBranchToSite()
                        {
                            BranchCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach(var x in l)
                {
                    if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                    {
                        if (mSiteCode.Length > 0) mSiteCode += ",";
                        mSiteCode += x.BranchCode;
                    }
                }

                if (mSiteCode.Length == 0) mSiteCode = Sm.GetLue(LueSiteCode);
            }
        }

        private void Process1(ref List<ContractRealization> l, ref List<SiteCounter> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int mUP = 1;
            int mNas = 1;

            foreach (var x in l2)
            {
                if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                {
                    mNas = x.No;
                    break;
                }
            }

            string[] mProjectDesc = mDescForDetailRealization.Split(',');
            string[] mProjectType = mTypeForDetailRealization.Split(',');

            SQL.AppendLine("SELECT T1.*, T2.ItName, T3.CtName FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                for (int j = 0; j < mProjectType.Count(); ++j)
                {
                    SQL.AppendLine("SELECT '" + mProjectDesc[i] + "' AS ProjectDesc, '" + mProjectType[j] + "' AS ProjectDesc2, If(J1.ParValue = '2', D.ProjectCode2, D.ProjectCode) ProjectCode2, B.ProjectName, LEFT(F.DocDt, 4) Yr, G.OptDesc ProjectResource, H.OptDesc ProjectScope, ");
                    SQL.AppendLine("I.OptDesc ProjectType, J.ItCode, B.CtCode, F.DocDt, If(C.TaxInd = 'N', '1', NULL) NonPPN,  ");
                    SQL.AppendLine("If(D.JOType = @JointOperationTypeForKSOLead, '1', NULL) LF, If(D.JOType = @JointOperationTypeForKSOMember, '1', NULL) M, ");
                    SQL.AppendLine("K.JOName, 0.00 AS JOPercentage, NULL AS JONPWP, IFNULL(L.Amt, 0.00) RKAPAmt, IFNULL(N.Amt, 0.00) OldContractAmt, ");
                    SQL.AppendLine("IFNULL(M.Amt, 0.00) CarryOverAmt, IFNULL(O.Amt1, 0.00) Amt1, IFNULL(O.Amt2, 0.00) Amt2, IFNULL(O.Amt3, 0.00) Amt3, IFNULL(O.Amt4, 0.00) Amt4, ");
                    SQL.AppendLine("IFNULL(O.Amt5, 0.00) Amt5, IFNULL(O.Amt6, 0.00) Amt6, IFNULL(O.Amt7, 0.00) Amt7, IFNULL(O.Amt8, 0.00) Amt8, IFNULL(O.Amt9, 0.00) Amt9, IFNULL(O.Amt10, 0.00) Amt10, ");
                    SQL.AppendLine("IFNULL(O.Amt11, 0.00) Amt11, IFNULL(O.Amt12, 0.00) Amt12, D.Remark ");
                    SQL.AppendLine("FROM TblSite A ");
                    SQL.AppendLine("INNER JOIN TblLOPHdr B ON A.SiteCode = B.SiteCode ");
                    SQL.AppendLine("    AND Find_In_Set(A.SiteCode, @SiteCode) ");
                    SQL.AppendLine("INNER JOIN TblBOQHdr C ON B.DocNo = C.LOPDocNo ");
                    SQL.AppendLine("INNER JOIN TblSOContractHdr D ON C.DocNo = D.BOQDocNo ");
                    SQL.AppendLine("    AND D.Status = 'A' ");
                    SQL.AppendLine("    AND D.CancelInd = 'N' ");

                    if (i == 0)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOMember ");
                    
                    if (j == 0)
                        SQL.AppendLine("INNER JOIN (SELECT MAX(DocNo) DocNo, SOCDocNo FROM TblSOContractRevisionHdr WHERE LEFT(DocDt, 4) < @Yr GROUP BY SOCDocNo) E ON D.Docno = E.SOCDocNo ");
                    else
                        SQL.AppendLine("INNER JOIN (SELECT MAX(DocNo) DocNo, SOCDocNo FROM TblSOContractRevisionHdr WHERE LEFT(DocDt, 4) = @Yr GROUP BY SOCDocNo) E ON D.Docno = E.SOCDocNo ");
                    
                    SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr F ON E.DocNo = F.DocNo ");
                    SQL.AppendLine("INNER JOIN TblOption G ON G.OptCat = 'ProjectResource' AND G.OptCode = B.ProjectResource ");
                    SQL.AppendLine("INNER JOIN TblOption H ON H.OptCat = 'ProjectScope' AND H.OptCode = B.ProjectScope ");
                    SQL.AppendLine("INNER JOIN TblOption I ON I.OptCat = 'ProjectType' AND I.OptCode = B.ProjectType ");
                    SQL.AppendLine("INNER JOIN TblSOContractDtl J ON D.DocNo = J.DocNo ");
                    SQL.AppendLine("Left Join TblParameter J1 On J1.ParCode = 'ProjectAcNoFormula' ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(Distinct T2.JOName SEPARATOR '; ') JOName ");
                    SQL.AppendLine("    FROM TblSOContractDtl3 T1 ");
                    SQL.AppendLine("    INNER JOIN TblJointOperationHdr T2 ON T1.JOCode = T2.JOCode ");
                    SQL.AppendLine("    INNER JOIN TblSOContractHdr T3 ON T1.DocNo = T3.DocNo ");
                    SQL.AppendLine("        AND T3.Status = 'A' ");
                    SQL.AppendLine("        AND T3.CancelInd = 'N' ");
                    if (i == 0)
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForNonKSO ");
                    else if(i == 1)
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForKSOMember ");

                    SQL.AppendLine("    INNER JOIN TblBOQHdr T4 ON T3.BOQDocNo = T4.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T5 ON T4.LOPDocno = T5.DocNo ");
                    SQL.AppendLine("        AND FIND_IN_SET(T5.SiteCode, @SiteCode) ");
                    SQL.AppendLine("    GROUP BY T1.DocNo ");
                    SQL.AppendLine(") K ON D.DocNo = K.DocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT SUM(Amt) Amt ");
                    SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
                    SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("        AND A.Yr = @Yr ");
                    SQL.AppendLine("        AND A.CancelInd = 'N' ");
                    SQL.AppendLine("        AND B.Amt != 0 ");
                    SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                    SQL.AppendLine("        AND C.ActInd = 'Y' ");
                    SQL.AppendLine("        AND C.Level = @COALevelFicoSettingJournalToCBP + 1 ");
                    SQL.AppendLine("        AND FIND_IN_SET(RIGHT(C.AcNo, 3), @SiteCode) ");
                    SQL.AppendLine(") L ON 0 = 0 ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T3.SOCDocNo, SUM(T2.Amt) Amt ");
                    SQL.AppendLine("    FROM TblProjectImplementationHdr T1 ");
                    SQL.AppendLine("    INNER JOIN TblProjectImplementationDtl T2 ON T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T1.Status = 'A' ");
                    SQL.AppendLine("        AND T2.SettledInd = 'N' ");
                    SQL.AppendLine("        AND T2.Amt != 0 ");
                    SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOContractDocNo = T3.DocNo ");
                    SQL.AppendLine("        AND LEFT(T3.DocDt, 4) < @Yr ");
                    SQL.AppendLine("    INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo ");
                    SQL.AppendLine("        AND T4.Status = 'A' ");
                    SQL.AppendLine("        AND T4.CancelInd = 'N' ");
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if(i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    SQL.AppendLine("    INNER JOIN TblBOQHdr T5 ON T4.BOQDocNo = T5.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo ");
                    SQL.AppendLine("        AND Find_In_Set(T6.SiteCode, @SiteCode) ");
                    SQL.AppendLine("    GROUP BY T3.SOCDocNo ");
                    SQL.AppendLine(") M ON D.DocNo = M.SOCDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T2.SOCDocNo, SUM(T2.Amt) Amt ");
                    SQL.AppendLine("    FROM (SELECT MAX(DocNo) DocNo, SOCDocNo FROM TblSOContractRevisionHdr WHERE LEFT(DocDt, 4) = (@Yr - 1) GROUP BY SOCDocNo) T1 ");
                    SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T2 ON T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblSOContractHdr T3 ON T2.SOCDocNo = T3.DocNo ");
                    SQL.AppendLine("        And T3.Status = 'A' ");
                    SQL.AppendLine("        AND T3.CancelInd = 'N' ");
                    if (i == 0)
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForNonKSO ");
                    else if(i == 1)
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T3.JOType = @JointOperationTypeForKSOMember ");

                    SQL.AppendLine("    INNER JOIN TblBOQHdr T4 ON T3.BOQDocNo = T4.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo ");
                    SQL.AppendLine("        AND FIND_IN_SET(T5.SiteCode, @SiteCode) ");
                    SQL.AppendLine("    GROUP BY T2.SOCDocNo ");
                    SQL.AppendLine(") N ON D.DocNo = N.SOCDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT X1.SOCDocNo, SUM(X1.Amt1) Amt1, SUM(X1.Amt2) Amt2, SUM(X1.Amt3) Amt3, SUM(X1.Amt4) Amt4,  ");
                    SQL.AppendLine("    SUM(X1.Amt5) Amt5, SUM(X1.Amt6) Amt6, SUM(X1.Amt7) Amt7, SUM(X1.Amt8) Amt8, SUM(X1.Amt9) Amt9,  ");
                    SQL.AppendLine("    SUM(X1.Amt10) Amt10, SUM(X1.Amt11) Amt11, SUM(X1.Amt12) Amt12 ");
                    SQL.AppendLine("    FROM ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T.SOCDocNo, If(T.Mth = '01', T.Amt, 0.00) Amt1, If(T.Mth = '02', T.Amt, 0.00) Amt2, ");
                    SQL.AppendLine("        If(T.Mth = '03', T.Amt, 0.00) Amt3, If(T.Mth = '04', T.Amt, 0.00) Amt4, If(T.Mth = '05', T.Amt, 0.00) Amt5, ");
                    SQL.AppendLine("        If(T.Mth = '06', T.Amt, 0.00) Amt6, If(T.Mth = '07', T.Amt, 0.00) Amt7, If(T.Mth = '08', T.Amt, 0.00) Amt8, ");
                    SQL.AppendLine("        If(T.Mth = '09', T.Amt, 0.00) Amt9, If(T.Mth = '10', T.Amt, 0.00) Amt10, If(T.Mth = '11', T.Amt, 0.00) Amt11, ");
                    SQL.AppendLine("        If(T.Mth = '12', T.Amt, 0.00) Amt12 ");
                    SQL.AppendLine("        FROM ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("             SELECT T2.SOCDocNo, SUBSTR(T2.DocDt, 5, 2) Mth, T2.Amt ");
                    SQL.AppendLine("            FROM (SELECT MAX(DocNo) DocNo, SOCDocNo FROM TblSOContractRevisionHdr WHERE LEFT(DocDt, 4) = @Yr GROUP BY SOCDocNo) T1 ");
                    SQL.AppendLine("            INNER JOIN TblSOContractRevisionHdr T2 ON T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("            INNER JOIN TblSOContractHdr T3 ON T2.SOCDocNo = T3.DocNo ");
                    SQL.AppendLine("                AND T3.CancelInd = 'N' ");
                    SQL.AppendLine("                AND T3.Status = 'A' ");
                    if (i == 0)
                        SQL.AppendLine("                AND T3.JOType = @JointOperationTypeForNonKSO ");
                    else if(i == 1)
                        SQL.AppendLine("                AND T3.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("                AND T3.JOType = @JointOperationTypeForKSOMember ");

                    SQL.AppendLine("            INNER JOIN TblBOQHdr T4 ON T3.BOQDocNo = T4.DocNo ");
                    SQL.AppendLine("            INNER JOIN TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo ");
                    SQL.AppendLine("                AND FIND_IN_SET(T5.SiteCode, @SiteCode) ");
                    SQL.AppendLine("        ) T ");
                    SQL.AppendLine("    ) X1 ");
                    SQL.AppendLine("    GROUP BY X1.SOCDocNo ");
                    SQL.AppendLine(") O ON D.DocNo = O.SOCDocNo ");

                    if (i != (mProjectDesc.Count() - 1) || j != (mProjectType.Count() - 1))
                        SQL.AppendLine("    UNION ALL ");
                }
            }

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            SQL.AppendLine("Inner Join TblCustomer T3 On T1.CtCode = T3.CtCode ");
            SQL.AppendLine("Order By T1.ProjectDesc, T1.ProjectDesc2; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOLead", mJointOperationTypeForKSOLead);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOMember", mJointOperationTypeForKSOMember);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForNonKSO", mJointOperationTypeForNonKSO);
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-5
                    "ProjectDesc2", "ProjectCode2", "ProjectName", "Yr", "ProjectResource", 
                    //6-10
                    "ProjectScope", "ProjectType", "ItName", "CtName", "DocDt", 
                    //11-15
                    "NonPPN", "LF", "M", "JOName", "JOPercentage", 
                    //16-20
                    "RKAPAmt", "OldContractAmt", "CarryOverAmt", "Amt1", "Amt2", 
                    //21-25
                    "Amt3", "Amt4", "Amt5", "Amt6", "Amt7", 
                    //26-30
                    "Amt8", "Amt9", "Amt10", "Amt11", "Amt12", 
                    //31-32
                    "Remark", "JONPWP"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ContractRealization()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            ProjectDesc2 = Sm.DrStr(dr, c[1]),
                            NAS = mNas.ToString(),
                            UP = mUP.ToString(),
                            ProjectCode2 = Sm.DrStr(dr, c[2]),
                            ProjectName = Sm.DrStr(dr, c[3]),
                            Yr = Sm.DrStr(dr, c[4]),
                            ProjectResource = Sm.DrStr(dr, c[5]),
                            ProjectScope = Sm.DrStr(dr, c[6]),
                            ProjectType = Sm.DrStr(dr, c[7]),
                            ItName = Sm.DrStr(dr, c[8]),
                            CtName = Sm.DrStr(dr, c[9]),
                            DocDt = Sm.DrStr(dr, c[10]),
                            NonPPN = Sm.DrStr(dr, c[11]),
                            LF = Sm.DrStr(dr, c[12]),
                            M = Sm.DrStr(dr, c[13]),
                            JOName = Sm.DrStr(dr, c[14]),
                            JOPercentage = Sm.DrDec(dr, c[15]),
                            RKAPAmt = Sm.DrDec(dr, c[16]),
                            OldContractAmt = Sm.DrDec(dr, c[17]),
                            CarryOverAmt = Sm.DrDec(dr, c[18]),
                            Amt1 = Sm.DrDec(dr, c[19]),
                            Amt2 = Sm.DrDec(dr, c[20]),
                            Amt3 = Sm.DrDec(dr, c[21]),
                            Total1 = Sm.DrDec(dr, c[19]) + Sm.DrDec(dr, c[20]) + Sm.DrDec(dr, c[21]),
                            Amt4 = Sm.DrDec(dr, c[22]),
                            Amt5 = Sm.DrDec(dr, c[23]),
                            Amt6 = Sm.DrDec(dr, c[24]),
                            Total2 = Sm.DrDec(dr, c[22]) + Sm.DrDec(dr, c[23]) + Sm.DrDec(dr, c[24]),
                            Amt7 = Sm.DrDec(dr, c[25]),
                            Amt8 = Sm.DrDec(dr, c[26]),
                            Amt9 = Sm.DrDec(dr, c[27]),
                            Total3 = Sm.DrDec(dr, c[25]) + Sm.DrDec(dr, c[26]) + Sm.DrDec(dr, c[27]),
                            Amt10 = Sm.DrDec(dr, c[28]),
                            Amt11 = Sm.DrDec(dr, c[29]),
                            Amt12 = Sm.DrDec(dr, c[30]),
                            Total4 = Sm.DrDec(dr, c[28]) + Sm.DrDec(dr, c[29]) + Sm.DrDec(dr, c[30]),
                            TotalNewContract = 0m,
                            TotalContract = 0m,
                            TotalCoNewContract = 0m,
                            PercentageTowardsTarget = 0m,
                            Balance = 0m,
                            Remark = Sm.DrStr(dr, c[31]),
                            JONPWP = Sm.DrStr(dr, c[32])
                        });

                        mUP += 1;
                        mNas += 1;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ContractRealization> l)
        {
            foreach(var x in l)
            {
                x.TotalNewContract = x.Total1 + x.Total2 + x.Total3 + x.Total4;
                x.TotalContract = x.OldContractAmt + x.TotalNewContract;
                x.TotalCoNewContract = x.RKAPAmt + x.TotalNewContract;
                if (x.RKAPAmt != 0m) x.PercentageTowardsTarget = (x.TotalCoNewContract / x.RKAPAmt) * 100m;
                x.Balance = x.RKAPAmt - x.TotalCoNewContract;
            }
        }

        private void Process3(ref List<ContractRealization> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.ProjectDesc2;
                Grd1.Cells[Row, 3].Value = x.NAS;
                Grd1.Cells[Row, 4].Value = x.UP;
                Grd1.Cells[Row, 5].Value = x.ProjectCode2;
                Grd1.Cells[Row, 6].Value = x.ProjectName;
                Grd1.Cells[Row, 7].Value = x.Yr;
                Grd1.Cells[Row, 8].Value = x.ProjectResource;
                Grd1.Cells[Row, 9].Value = x.ProjectScope;
                Grd1.Cells[Row, 10].Value = x.ProjectType;
                Grd1.Cells[Row, 11].Value = x.ItName;
                Grd1.Cells[Row, 12].Value = x.CtName;
                Grd1.Cells[Row, 13].Value = x.DocDt;
                Grd1.Cells[Row, 14].Value = x.NonPPN;
                Grd1.Cells[Row, 15].Value = x.LF;
                Grd1.Cells[Row, 16].Value = x.M;
                Grd1.Cells[Row, 17].Value = x.JOName;
                Grd1.Cells[Row, 18].Value = x.JOPercentage;
                Grd1.Cells[Row, 19].Value = x.JONPWP;
                Grd1.Cells[Row, 20].Value = x.RKAPAmt;
                Grd1.Cells[Row, 21].Value = x.OldContractAmt;
                Grd1.Cells[Row, 22].Value = x.CarryOverAmt;
                Grd1.Cells[Row, 23].Value = x.Amt1;
                Grd1.Cells[Row, 24].Value = x.Amt2;
                Grd1.Cells[Row, 25].Value = x.Amt3;
                Grd1.Cells[Row, 26].Value = x.Total1;
                Grd1.Cells[Row, 27].Value = x.Amt4;
                Grd1.Cells[Row, 28].Value = x.Amt5;
                Grd1.Cells[Row, 29].Value = x.Amt6;
                Grd1.Cells[Row, 30].Value = x.Total2;
                Grd1.Cells[Row, 31].Value = x.Amt7;
                Grd1.Cells[Row, 32].Value = x.Amt8;
                Grd1.Cells[Row, 33].Value = x.Amt9;
                Grd1.Cells[Row, 34].Value = x.Total3;
                Grd1.Cells[Row, 35].Value = x.Amt10;
                Grd1.Cells[Row, 36].Value = x.Amt11;
                Grd1.Cells[Row, 37].Value = x.Amt12;
                Grd1.Cells[Row, 38].Value = x.Total4;
                Grd1.Cells[Row, 39].Value = x.TotalNewContract;
                Grd1.Cells[Row, 40].Value = x.TotalContract;
                Grd1.Cells[Row, 41].Value = x.TotalCoNewContract;
                Grd1.Cells[Row, 42].Value = x.PercentageTowardsTarget;
                Grd1.Cells[Row, 43].Value = x.Balance;
                Grd1.Cells[Row, 44].Value = x.Remark;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.GroupObject.Add(2);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Class

        private class ContractRealization
        {
            public string ProjectDesc { get; set; }
            public string ProjectDesc2 { get; set; }
            public string NAS { get; set; }
            public string UP { get; set; }
            public string ProjectCode2 { get; set; }
            public string ProjectName { get; set; }
            public string Yr { get; set; }
            public string ProjectResource { get; set; }
            public string ProjectScope { get; set; }
            public string ProjectType { get; set; }
            public string ItName { get; set; }
            public string CtName { get; set; }
            public string DocDt { get; set; }
            public string NonPPN { get; set; }
            public string LF { get; set; }
            public string M { get; set; }
            public string JOName { get; set; }
            public decimal JOPercentage { get; set; }
            public decimal RKAPAmt { get; set; }
            public decimal OldContractAmt { get; set; }
            public decimal CarryOverAmt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Total1 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Total2 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Total3 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal Total4 { get; set; }
            public decimal TotalNewContract { get; set; }
            public decimal TotalContract { get; set; }
            public decimal TotalCoNewContract { get; set; }
            public decimal PercentageTowardsTarget { get; set; }
            public decimal Balance { get; set; }
            public string Remark { get; set; }
            public string JONPWP { get; set; }
        }

        private class AuctionBranchToSite
        {
            public string BranchCode { get; set; }
            public string SiteCode { get; set; }
        }

        private class SiteCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
            public int No2 { get; set; }
        }

        private class DummyCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
        }

        #endregion
    }
}
