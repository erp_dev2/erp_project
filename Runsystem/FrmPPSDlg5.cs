﻿# region Update
/*
   04/07/2022 [MYA/PHT] Work periode di pps menjadi mandatory 
   06/07/2022 [MYA/PHT] Work periode dan remark di pps menjadi editable, di sebelah work periode ada loop yang mengcapture history edit dari work periode 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPSDlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPPS mFrmParent;
        private string mSQL = string.Empty, mDocNo = string.Empty, mDNo = string.Empty;

        #endregion

        #region Constructor

        public FrmPPSDlg5(FrmPPS FrmParent, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Work Periode History";
                base.FrmLoad(sender, e);
                BtnRefresh.Visible = BtnChoose.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                panel2.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",
                        
                        //1-5
                        "DocNo",
                        "DNo",
                        "Work Periode",
                        "Remark",
                        "Last Update By",

                        //6-7
                        "Last Update",
                        "Last Update Time",
                    },
                    new int[]
                    {
                        //0
                        20,

                        //1-5
                        200, 50, 100, 250, 200,

                        //6-7
                        100, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatTime(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, CONCAT(A.WorkPeriodYr, '|', A.WorkPeriodMth) AS WorkPeriod, Remark, LastUpBy, LastUpDt  ");
            SQL.AppendLine("From tblppsdtl A ");
            SQL.AppendLine("Where DocNo=@DocNo");
            SQL.AppendLine("Order By LastUpDt Desc ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL,
                        new string[]
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DNo",
                             "WorkPeriod",
                             "Remark",
                             "LastUpBy",
                             "LastUpDt",
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

    }
}
