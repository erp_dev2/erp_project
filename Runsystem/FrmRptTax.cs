﻿#region Update
/*
    29/12/2017 [TKG] tambah informasi dan filter payrun period 
    04/01/2018 [TKG] perhitungan brutto untuk HIN
    09/04/2018 [TKG] tampilan untuk HIN
    26/04/2018 [TKG] ubah perhitungan brutto utk HIN
    14/05/2018 [TKG] untuk ho brutto ditambah sci
    10/07/2018 [TKG] ubah rumus brutto
    02/11/2022 [TYO/SKI] tambah kolom Variablle allowance, BPJS Kesehatan (Employer), BPJS JK (Employer), BPJS KK (Employer) berdasarkan parameter IsEmployeeTaxRptUseSSTab
    02/11/2022 [TYO/SKI] merubah rumus bruto berdasarkan parameter BrutoFormula
    11/11/2022 [TYO/SKI] penyesuaian query SetSQL
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptTax : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mSalaryInd = "1",
            mRptTaxType = "1",
            mBrutoFormula = string.Empty;
        private bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false,
            mIsEmployeeTaxRptUseSSTab = false;

        #endregion

        #region Constructor

        public FrmRptTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr, string.Empty);

                var Dt = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));

                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mRptTaxType = Sm.GetParameter("RptTaxType");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mBrutoFormula = Sm.GetParameter("BrutoFormula");
            mIsEmployeeTaxRptUseSSTab = Sm.GetParameterBoo("IsEmployeeTaxRptUseSSTab");

            if (mBrutoFormula.Length == 0) mBrutoFormula = "1";

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.PayrunCode, E.PayrunName, A.EmpCode, B.EmpName, B.DeptCode, B.SiteCode, C.DeptName, D.SiteName, ");
            SQL.AppendLine("Concat(Left(E.EndDt, 4), '-', Substring(E.EndDt, 5, 2)) As Period,  ");
            SQL.AppendLine("A.NPWP, A.PTKP, E.PayrunPeriod, G.OptDesc As PayrunPeriodDesc, A.Salary, A.FixAllowance, A.OT1Amt+A.OT2Amt+A.OTHolidayAmt As OT, A.IncProduction, ");

            if (mBrutoFormula == "1")
            {
                if (mSalaryInd == "2")
                {
                    SQL.AppendLine(" (IfNull(A.Salary,0)+IfNull(A.IncEmployee,0)+IfNull(A.TaxableFixAllowance,0)+IfNull(A.OT1Amt,0)+IfNull(A.OT2Amt,0)+IfNull(A.OTHolidayAmt,0)+ ");
                    SQL.AppendLine(" IfNull(A.Transport,0)+IfNull(A.Meal,0)+IfNull(A.FieldAssignment,0)+IfNull(A.SSemployeremployment,0)-IfNull(A.SSemployerpension,0)+IfNull(A.SalaryAdjustment,0)) As Bruto, ");
                }
                if (mSalaryInd == "1")
                {
                    SQL.AppendLine("(IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)-IfNull(A.ProcessUPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
                    SQL.AppendLine("IfNull(A.OTHolidayAmt,0)+ IfNull(A.TaxableFixAllowance,0)+ IfNull(A.EmploymentPeriodAllowance,0)+ IfNull(A.IncEmployee,0)+ ");
                    SQL.AppendLine("IfNull(A.IncMinWages,0)+ IfNull(A.IncProduction,0)+ IfNull(A.IncPerformance,0)+ IfNull(A.PresenceReward,0)+ IfNull(A.HolidayEarning,0)+ ");
                    SQL.AppendLine("IfNull(A.ExtraFooding,0)+ IfNull(A.SSEmployerHealth,0)+ IfNull(A.SSEmployerEmployment,0)+ IfNull(A.SalaryAdjustment,0)- ");
                    SQL.AppendLine("IfNull(A.TaxableFixDeduction,0) - IfNull(A.DedEmployee,0)- IfNull(A.DedProduction,0)- IfNull(A.DedProdLeave,0)- IfNull(A.SSEmployeeEmployment,0)) As Bruto, ");
                }
                if (mSalaryInd == "3")
                {
                    if (Sm.GetParameter("DocTitle") == "HIN")
                    {
                        //SQL.AppendLine("Case When D.HOInd='Y' Then ");
                        //SQL.AppendLine("    A.Salary ");
                        //SQL.AppendLine("    +A.TaxableFixAllowance ");
                        //SQL.AppendLine("    -A.TaxableFixDeduction ");
                        //SQL.AppendLine("    +A.Transport ");
                        //SQL.AppendLine("    +A.Meal ");
                        //SQL.AppendLine("    +A.ADLeave ");
                        //SQL.AppendLine("    +A.SalaryAdjustment ");
                        //SQL.AppendLine("    +A.ServiceChargeIncentive ");
                        //SQL.AppendLine("    - (A.SSEmployeeHealth ");
                        //SQL.AppendLine("    + A.SSEmployeeEmployment ");
                        //SQL.AppendLine("    + A.SSEePension ");
                        //SQL.AppendLine("    + A.SSEmployeePension ");
                        //SQL.AppendLine("    + A.SSEmployeePension2 ");
                        //SQL.AppendLine("    ) ");
                        //SQL.AppendLine("Else ");
                        //SQL.AppendLine("    A.Salary ");
                        //SQL.AppendLine("    +A.TaxableFixAllowance ");
                        //SQL.AppendLine("    -A.TaxableFixDeduction ");
                        //SQL.AppendLine("    +A.Transport ");
                        //SQL.AppendLine("    +A.Meal ");
                        //SQL.AppendLine("    +A.ADLeave ");
                        //SQL.AppendLine("    +A.SalaryAdjustment ");
                        //SQL.AppendLine("    +A.ServiceChargeIncentive ");
                        //SQL.AppendLine("    - (A.SSEmployeeHealth ");
                        //SQL.AppendLine("    + A.SSEmployeeEmployment ");
                        //SQL.AppendLine("    + A.SSEePension ");
                        //SQL.AppendLine("    + A.SSEmployeePension ");
                        //SQL.AppendLine("    + A.SSEmployeePension2 ");
                        //SQL.AppendLine("    ) ");
                        //SQL.AppendLine("End As Bruto, ");
                        SQL.AppendLine("A.Salary + ");
                        SQL.AppendLine("A.TaxableFixAllowance + ");
                        SQL.AppendLine("A.Transport + ");
                        SQL.AppendLine("A.Meal + ");
                        SQL.AppendLine("A.ADLeave + ");
                        SQL.AppendLine("A.SalaryAdjustment + ");
                        SQL.AppendLine("A.ServiceChargeIncentive + ");
                        SQL.AppendLine("A.SSEmployerHealth + ");
                        SQL.AppendLine("A.SSErLifeInsurance + ");
                        SQL.AppendLine("A.SSErWorkingAccident ");
                        SQL.AppendLine("As Bruto, ");
                    }
                    else
                    {
                        SQL.AppendLine("IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
                        SQL.AppendLine("IfNull(A.OTHolidayAmt,0)+IfNull(A.TaxableFixAllowance,0)-IfNull(A.TaxableFixDeduction,0)+");
                        SQL.AppendLine("IfNull(A.Transport,0)+ IfNull(A.Meal,0)+ IfNull(A.SalaryAdjustment,0)-IfNull(A.EmpAdvancePayment,0)+IfNull(A.ServiceChargeIncentive,0) ");
                        SQL.AppendLine("As Bruto, ");
                    }
                }
            }
            else
                SQL.AppendLine("IfNull(A.Salary, 0) + IfNull(A.FixAllowance, 0) + IfNull(A.VarAllowance, 0) + IfNull(H1.BPJSKesAmt, 0) + IfNull(H2.BPJSJkkAmt, 0) + IfNull(H3.BPJSKmAmt, 0) As Bruto, ");
            SQL.AppendLine("A.EOYTax+A.Tax As Tax, ");
            SQL.AppendLine("A.VarAllowance, H1.BPJSKesAmt, H2.BPJSJkkAmt, H3.BPJSKmAmt ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun E ");
            SQL.AppendLine("    On A.PayrunCode=E.PayrunCode ");
            SQL.AppendLine("    And E.CancelInd='N' ");
            SQL.AppendLine("    And Substring(E.EndDt,5,2)=@Mth ");
            SQL.AppendLine("    And Left(E.EndDt, 4)=@Yr ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("    From TblAllowancededuction A ");
            SQL.AppendLine("    Inner Join TblEmployeeAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("    Where A.TaxInd='Y' ");
            SQL.AppendLine("    Group by B.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Left Join TblOption G On E.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   SELECT A.PayrunCode,B.DocNo, A.EmpCode, B.EmployerAmt BPJSKesAmt ");
            SQL.AppendLine("	FROM tblpayrollprocess1 A ");
            SQL.AppendLine("	INNER JOIN tblempsslistdtl B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("	WHERE B.SSCode = 'BPJS-KES' ");
            SQL.AppendLine("	GROUP BY B.EmpCode ");
            SQL.AppendLine(") H1 ON A.EmpCode = H1.EmpCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   SELECT A.PayrunCode,B.DocNo, A.EmpCode, B.EmployerAmt BPJSJkkAmt ");
            SQL.AppendLine("	FROM tblpayrollprocess1 A ");
            SQL.AppendLine("	INNER JOIN tblempsslistdtl B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("	WHERE B.SSCode = 'JKK' ");
            SQL.AppendLine("	GROUP BY B.EmpCode ");
            SQL.AppendLine(") H2 ON A.EmpCode = H2.EmpCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   SELECT A.PayrunCode,B.DocNo, A.EmpCode, B.EmployerAmt BPJSKmAmt ");
            SQL.AppendLine("	FROM tblpayrollprocess1 A ");
            SQL.AppendLine("	INNER JOIN tblempsslistdtl B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("	WHERE B.SSCode = 'JKM' ");
            SQL.AppendLine("	GROUP BY B.EmpCode ");
            SQL.AppendLine(") H3 ON A.EmpCode = H3.EmpCode ");

            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(" )X ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Year",
                        "Month",
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Employee's Name",
                        "Department",
                        "Site",
                        "Period",
                        "NPWP",
                       
                        //11-15
                        "PTKP",
                        "Payrun Period",
                        "Salary",
                        "Allowance",
                        "Overtime",

                        //16-20
                        "Production"+Environment.NewLine+"Incentive",
                        "Bruto",
                        "Tax",
                        "Variable Allowance",
                        "BPJS Kesehatan (Employer)",

                        //21-22
                        "BPJS Jaminan Kematian"+Environment.NewLine+"(Employer)",
                        "BPJS Jaminan Kecelakaan Kerja"+Environment.NewLine+"(Employer)"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        60, 60, 100, 170, 100, 
                        
                        //6-10
                        200, 150, 100, 0, 140, 
                        
                        //11-15
                        100, 150, 100, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 150, 200,

                        //21-22
                        200, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, 0);
            if (mRptTaxType=="1")
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 9 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22}, mIsEmployeeTaxRptUseSSTab);
            Grd1.Cols[19].Move(15);
            Grd1.Cols[20].Move(16);
            Grd1.Cols[21].Move(17);
            Grd1.Cols[22].Move(18);
        }

        override protected void HideInfoInGrd()
        {
            if (mRptTaxType == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 9 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueMth, "Month")) return;
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " where 1=1 ", Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "X.EmpCode", "X.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPayCode.Text, new string[] { "X.PayrunCode", "X.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "X.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "X.PayrunPeriod", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "PayrunCode",

                            //1-5
                            "PayrunName", "EmpCode", "EmpName","DeptName", "SiteName",
                            
                            //6-10
                            "Period", "NPWP", "PTKP", "PayrunPeriodDesc", "Salary", 
                            
                            //11-15
                            "FixAllowance",  "OT", "IncProduction", "Bruto", "Tax",

                            //16-19
                            "VarAllowance", "BPJSKesAmt", "BPJSJkkAmt", "BPJSKmAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = Yr;
                            Grd.Cells[Row, 2].Value = Mth;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 13, 14, 15, 16, 17, 18 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
 
        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtPayCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }
        
        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion
    }
}
