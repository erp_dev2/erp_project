﻿namespace RunSystem
{
    partial class FrmPropertyInventoryTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPropertyInventoryTransfer));
            this.label3 = new System.Windows.Forms.Label();
            this.LuePropertyCtTo = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LuePropertyCtFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LblSiteTo = new System.Windows.Forms.Label();
            this.LueSiteCodeTo = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSiteFrom = new System.Windows.Forms.Label();
            this.LueSiteCodeFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCCCodeTo = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCCCodeFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.Tc = new System.Windows.Forms.TabControl();
            this.TpGeneral = new System.Windows.Forms.TabPage();
            this.TpCertificationInformation = new System.Windows.Forms.TabPage();
            this.MeeRegistrationBasis = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCertExpDt = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAreaBased = new DevExpress.XtraEditors.TextEdit();
            this.LblAreaBased = new System.Windows.Forms.Label();
            this.DteSurveyDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LblSurveyDocDt = new System.Windows.Forms.Label();
            this.TxtSurveyDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblSurveyDocNo = new System.Windows.Forms.Label();
            this.DteBasisDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LblBasisDocDt = new System.Windows.Forms.Label();
            this.TxtRegistrationBasis = new DevExpress.XtraEditors.TextEdit();
            this.LblRegistrationBasis = new System.Windows.Forms.Label();
            this.TxtNIB = new DevExpress.XtraEditors.TextEdit();
            this.LblNIB = new System.Windows.Forms.Label();
            this.TxtCertIssuedLocation = new DevExpress.XtraEditors.TextEdit();
            this.LblCertIssuedLoc = new System.Windows.Forms.Label();
            this.DteCertExpDt = new DevExpress.XtraEditors.DateEdit();
            this.LblCertExpDt = new System.Windows.Forms.Label();
            this.DteCertIssuedDt = new DevExpress.XtraEditors.DateEdit();
            this.LblCertIssuedDt = new System.Windows.Forms.Label();
            this.TxtCertificateNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCertNo = new System.Windows.Forms.Label();
            this.LblCertificateType = new System.Windows.Forms.Label();
            this.LueCertificateType = new DevExpress.XtraEditors.LookUpEdit();
            this.TpUploadFile = new System.Windows.Forms.TabPage();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile1 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload1 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload1 = new System.Windows.Forms.ProgressBar();
            this.TxtFile1 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnFile1 = new DevExpress.XtraEditors.SimpleButton();
            this.TpApprovalInformation = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeFrom.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCodeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCodeFrom.Properties)).BeginInit();
            this.Tc.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.TpCertificationInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRegistrationBasis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertExpDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBased.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSurveyDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSurveyDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBasisDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBasisDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegistrationBasis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertIssuedLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertExpDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertExpDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertIssuedDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertIssuedDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCertificateType.Properties)).BeginInit();
            this.TpUploadFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).BeginInit();
            this.TpApprovalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1035, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Size = new System.Drawing.Size(80, 597);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(80, 31);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LuePropertyCtTo);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LuePropertyCtFrom);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Size = new System.Drawing.Size(1035, 176);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Tc);
            this.panel3.Location = new System.Drawing.Point(0, 176);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Size = new System.Drawing.Size(1035, 421);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 574);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(80, 23);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(3, 4);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(1021, 379);
            this.Grd1.TabIndex = 29;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Size = new System.Drawing.Size(80, 31);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 145);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Property Category To";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePropertyCtTo
            // 
            this.LuePropertyCtTo.EnterMoveNextControl = true;
            this.LuePropertyCtTo.Location = new System.Drawing.Point(163, 141);
            this.LuePropertyCtTo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LuePropertyCtTo.Name = "LuePropertyCtTo";
            this.LuePropertyCtTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtTo.Properties.Appearance.Options.UseFont = true;
            this.LuePropertyCtTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropertyCtTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropertyCtTo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtTo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropertyCtTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropertyCtTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropertyCtTo.Properties.DropDownRows = 20;
            this.LuePropertyCtTo.Properties.NullText = "[Empty]";
            this.LuePropertyCtTo.Properties.PopupWidth = 500;
            this.LuePropertyCtTo.Size = new System.Drawing.Size(328, 24);
            this.LuePropertyCtTo.TabIndex = 22;
            this.LuePropertyCtTo.ToolTip = "F4 : Show/hide list";
            this.LuePropertyCtTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropertyCtTo.EditValueChanged += new System.EventHandler(this.LuePropertyCtTo_EditValueChanged);
            this.LuePropertyCtTo.Validated += new System.EventHandler(this.LuePropertyCtTo_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(5, 118);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "Property Category From";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePropertyCtFrom
            // 
            this.LuePropertyCtFrom.EnterMoveNextControl = true;
            this.LuePropertyCtFrom.Location = new System.Drawing.Point(163, 114);
            this.LuePropertyCtFrom.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LuePropertyCtFrom.Name = "LuePropertyCtFrom";
            this.LuePropertyCtFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtFrom.Properties.Appearance.Options.UseFont = true;
            this.LuePropertyCtFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropertyCtFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropertyCtFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropertyCtFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCtFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropertyCtFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropertyCtFrom.Properties.DropDownRows = 20;
            this.LuePropertyCtFrom.Properties.NullText = "[Empty]";
            this.LuePropertyCtFrom.Properties.PopupWidth = 500;
            this.LuePropertyCtFrom.Size = new System.Drawing.Size(328, 24);
            this.LuePropertyCtFrom.TabIndex = 20;
            this.LuePropertyCtFrom.ToolTip = "F4 : Show/hide list";
            this.LuePropertyCtFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropertyCtFrom.EditValueChanged += new System.EventHandler(this.LuePropertyCtFrom_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(154, 116);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(347, 24);
            this.MeeRemark.TabIndex = 33;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(98, 120);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 32;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(163, 60);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 24);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(123, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 18);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(163, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(246, 24);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(79, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteTo
            // 
            this.LblSiteTo.AutoSize = true;
            this.LblSiteTo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteTo.ForeColor = System.Drawing.Color.Black;
            this.LblSiteTo.Location = new System.Drawing.Point(98, 37);
            this.LblSiteTo.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblSiteTo.Name = "LblSiteTo";
            this.LblSiteTo.Size = new System.Drawing.Size(54, 18);
            this.LblSiteTo.TabIndex = 26;
            this.LblSiteTo.Text = "Site To";
            this.LblSiteTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCodeTo
            // 
            this.LueSiteCodeTo.EnterMoveNextControl = true;
            this.LueSiteCodeTo.Location = new System.Drawing.Point(154, 33);
            this.LueSiteCodeTo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueSiteCodeTo.Name = "LueSiteCodeTo";
            this.LueSiteCodeTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeTo.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCodeTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCodeTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCodeTo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeTo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCodeTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCodeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCodeTo.Properties.DropDownRows = 20;
            this.LueSiteCodeTo.Properties.NullText = "[Empty]";
            this.LueSiteCodeTo.Properties.PopupWidth = 500;
            this.LueSiteCodeTo.Size = new System.Drawing.Size(347, 24);
            this.LueSiteCodeTo.TabIndex = 27;
            this.LueSiteCodeTo.ToolTip = "F4 : Show/hide list";
            this.LueSiteCodeTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCodeTo.EditValueChanged += new System.EventHandler(this.LueSiteCode2_EditValueChanged);
            // 
            // LblSiteFrom
            // 
            this.LblSiteFrom.AutoSize = true;
            this.LblSiteFrom.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteFrom.ForeColor = System.Drawing.Color.Black;
            this.LblSiteFrom.Location = new System.Drawing.Point(83, 10);
            this.LblSiteFrom.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblSiteFrom.Name = "LblSiteFrom";
            this.LblSiteFrom.Size = new System.Drawing.Size(70, 18);
            this.LblSiteFrom.TabIndex = 24;
            this.LblSiteFrom.Text = "Site From";
            this.LblSiteFrom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCodeFrom
            // 
            this.LueSiteCodeFrom.EnterMoveNextControl = true;
            this.LueSiteCodeFrom.Location = new System.Drawing.Point(154, 6);
            this.LueSiteCodeFrom.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueSiteCodeFrom.Name = "LueSiteCodeFrom";
            this.LueSiteCodeFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFrom.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCodeFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCodeFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCodeFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCodeFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCodeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCodeFrom.Properties.DropDownRows = 20;
            this.LueSiteCodeFrom.Properties.NullText = "[Empty]";
            this.LueSiteCodeFrom.Properties.PopupWidth = 500;
            this.LueSiteCodeFrom.Size = new System.Drawing.Size(347, 24);
            this.LueSiteCodeFrom.TabIndex = 25;
            this.LueSiteCodeFrom.ToolTip = "F4 : Show/hide list";
            this.LueSiteCodeFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCodeFrom.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LueCCCodeTo);
            this.panel5.Controls.Add(this.LueCCCodeFrom);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.LblSiteTo);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LueSiteCodeTo);
            this.panel5.Controls.Add(this.LueSiteCodeFrom);
            this.panel5.Controls.Add(this.LblSiteFrom);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(521, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(514, 176);
            this.panel5.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(48, 93);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 18);
            this.label4.TabIndex = 30;
            this.label4.Text = "Cost Center To";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCCodeTo
            // 
            this.LueCCCodeTo.EnterMoveNextControl = true;
            this.LueCCCodeTo.Location = new System.Drawing.Point(154, 89);
            this.LueCCCodeTo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueCCCodeTo.Name = "LueCCCodeTo";
            this.LueCCCodeTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeTo.Properties.Appearance.Options.UseFont = true;
            this.LueCCCodeTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCodeTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCodeTo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeTo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCodeTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCodeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCodeTo.Properties.DropDownRows = 20;
            this.LueCCCodeTo.Properties.NullText = "[Empty]";
            this.LueCCCodeTo.Properties.PopupWidth = 500;
            this.LueCCCodeTo.Size = new System.Drawing.Size(347, 24);
            this.LueCCCodeTo.TabIndex = 31;
            this.LueCCCodeTo.ToolTip = "F4 : Show/hide list";
            this.LueCCCodeTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCodeTo.EditValueChanged += new System.EventHandler(this.LueCCCode2_EditValueChanged);
            // 
            // LueCCCodeFrom
            // 
            this.LueCCCodeFrom.EnterMoveNextControl = true;
            this.LueCCCodeFrom.Location = new System.Drawing.Point(154, 62);
            this.LueCCCodeFrom.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueCCCodeFrom.Name = "LueCCCodeFrom";
            this.LueCCCodeFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeFrom.Properties.Appearance.Options.UseFont = true;
            this.LueCCCodeFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCodeFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCodeFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCodeFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCodeFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCodeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCodeFrom.Properties.DropDownRows = 20;
            this.LueCCCodeFrom.Properties.NullText = "[Empty]";
            this.LueCCCodeFrom.Properties.PopupWidth = 500;
            this.LueCCCodeFrom.Size = new System.Drawing.Size(347, 24);
            this.LueCCCodeFrom.TabIndex = 29;
            this.LueCCCodeFrom.ToolTip = "F4 : Show/hide list";
            this.LueCCCodeFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCodeFrom.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(33, 66);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 18);
            this.label6.TabIndex = 28;
            this.label6.Text = "Cost Center From";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc
            // 
            this.Tc.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tc.Controls.Add(this.TpGeneral);
            this.Tc.Controls.Add(this.TpCertificationInformation);
            this.Tc.Controls.Add(this.TpUploadFile);
            this.Tc.Controls.Add(this.TpApprovalInformation);
            this.Tc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc.Location = new System.Drawing.Point(0, 0);
            this.Tc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tc.Name = "Tc";
            this.Tc.SelectedIndex = 0;
            this.Tc.Size = new System.Drawing.Size(1035, 421);
            this.Tc.TabIndex = 30;
            // 
            // TpGeneral
            // 
            this.TpGeneral.Controls.Add(this.Grd1);
            this.TpGeneral.Location = new System.Drawing.Point(4, 30);
            this.TpGeneral.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpGeneral.Size = new System.Drawing.Size(1027, 387);
            this.TpGeneral.TabIndex = 0;
            this.TpGeneral.Text = "General";
            this.TpGeneral.UseVisualStyleBackColor = true;
            // 
            // TpCertificationInformation
            // 
            this.TpCertificationInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpCertificationInformation.Controls.Add(this.MeeRegistrationBasis);
            this.TpCertificationInformation.Controls.Add(this.MeeCertExpDt);
            this.TpCertificationInformation.Controls.Add(this.TxtAreaBased);
            this.TpCertificationInformation.Controls.Add(this.LblAreaBased);
            this.TpCertificationInformation.Controls.Add(this.DteSurveyDocDt);
            this.TpCertificationInformation.Controls.Add(this.LblSurveyDocDt);
            this.TpCertificationInformation.Controls.Add(this.TxtSurveyDocNo);
            this.TpCertificationInformation.Controls.Add(this.LblSurveyDocNo);
            this.TpCertificationInformation.Controls.Add(this.DteBasisDocDt);
            this.TpCertificationInformation.Controls.Add(this.LblBasisDocDt);
            this.TpCertificationInformation.Controls.Add(this.TxtRegistrationBasis);
            this.TpCertificationInformation.Controls.Add(this.LblRegistrationBasis);
            this.TpCertificationInformation.Controls.Add(this.TxtNIB);
            this.TpCertificationInformation.Controls.Add(this.LblNIB);
            this.TpCertificationInformation.Controls.Add(this.TxtCertIssuedLocation);
            this.TpCertificationInformation.Controls.Add(this.LblCertIssuedLoc);
            this.TpCertificationInformation.Controls.Add(this.DteCertExpDt);
            this.TpCertificationInformation.Controls.Add(this.LblCertExpDt);
            this.TpCertificationInformation.Controls.Add(this.DteCertIssuedDt);
            this.TpCertificationInformation.Controls.Add(this.LblCertIssuedDt);
            this.TpCertificationInformation.Controls.Add(this.TxtCertificateNo);
            this.TpCertificationInformation.Controls.Add(this.LblCertNo);
            this.TpCertificationInformation.Controls.Add(this.LblCertificateType);
            this.TpCertificationInformation.Controls.Add(this.LueCertificateType);
            this.TpCertificationInformation.Location = new System.Drawing.Point(4, 30);
            this.TpCertificationInformation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpCertificationInformation.Name = "TpCertificationInformation";
            this.TpCertificationInformation.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpCertificationInformation.Size = new System.Drawing.Size(1027, 387);
            this.TpCertificationInformation.TabIndex = 1;
            this.TpCertificationInformation.Text = "Certificate Information";
            // 
            // MeeRegistrationBasis
            // 
            this.MeeRegistrationBasis.EnterMoveNextControl = true;
            this.MeeRegistrationBasis.Location = new System.Drawing.Point(226, 170);
            this.MeeRegistrationBasis.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MeeRegistrationBasis.Name = "MeeRegistrationBasis";
            this.MeeRegistrationBasis.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.Appearance.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRegistrationBasis.Properties.MaxLength = 1000;
            this.MeeRegistrationBasis.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRegistrationBasis.Properties.ShowIcon = false;
            this.MeeRegistrationBasis.Size = new System.Drawing.Size(312, 24);
            this.MeeRegistrationBasis.TabIndex = 53;
            this.MeeRegistrationBasis.ToolTip = "F4 : Show/hide text";
            this.MeeRegistrationBasis.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRegistrationBasis.ToolTipTitle = "Run System";
            // 
            // MeeCertExpDt
            // 
            this.MeeCertExpDt.EnterMoveNextControl = true;
            this.MeeCertExpDt.Location = new System.Drawing.Point(226, 87);
            this.MeeCertExpDt.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MeeCertExpDt.Name = "MeeCertExpDt";
            this.MeeCertExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.Appearance.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCertExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCertExpDt.Properties.MaxLength = 1000;
            this.MeeCertExpDt.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCertExpDt.Properties.ShowIcon = false;
            this.MeeCertExpDt.Size = new System.Drawing.Size(312, 24);
            this.MeeCertExpDt.TabIndex = 38;
            this.MeeCertExpDt.ToolTip = "F4 : Show/hide text";
            this.MeeCertExpDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCertExpDt.ToolTipTitle = "Run System";
            // 
            // TxtAreaBased
            // 
            this.TxtAreaBased.EnterMoveNextControl = true;
            this.TxtAreaBased.Location = new System.Drawing.Point(226, 278);
            this.TxtAreaBased.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtAreaBased.Name = "TxtAreaBased";
            this.TxtAreaBased.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAreaBased.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAreaBased.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAreaBased.Properties.Appearance.Options.UseFont = true;
            this.TxtAreaBased.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAreaBased.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAreaBased.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TxtAreaBased.Properties.MaxLength = 200;
            this.TxtAreaBased.Size = new System.Drawing.Size(312, 24);
            this.TxtAreaBased.TabIndex = 52;
            // 
            // LblAreaBased
            // 
            this.LblAreaBased.AutoSize = true;
            this.LblAreaBased.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAreaBased.ForeColor = System.Drawing.Color.Black;
            this.LblAreaBased.Location = new System.Drawing.Point(3, 282);
            this.LblAreaBased.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblAreaBased.Name = "LblAreaBased";
            this.LblAreaBased.Size = new System.Drawing.Size(236, 18);
            this.LblAreaBased.TabIndex = 51;
            this.LblAreaBased.Text = "Luas Berdasarkan Surat Ukur (m2)";
            this.LblAreaBased.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteSurveyDocDt
            // 
            this.DteSurveyDocDt.EditValue = null;
            this.DteSurveyDocDt.EnterMoveNextControl = true;
            this.DteSurveyDocDt.Location = new System.Drawing.Point(226, 251);
            this.DteSurveyDocDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteSurveyDocDt.Name = "DteSurveyDocDt";
            this.DteSurveyDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSurveyDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteSurveyDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSurveyDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteSurveyDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteSurveyDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteSurveyDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSurveyDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteSurveyDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSurveyDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteSurveyDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteSurveyDocDt.Size = new System.Drawing.Size(312, 24);
            this.DteSurveyDocDt.TabIndex = 50;
            // 
            // LblSurveyDocDt
            // 
            this.LblSurveyDocDt.AutoSize = true;
            this.LblSurveyDocDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSurveyDocDt.ForeColor = System.Drawing.Color.Black;
            this.LblSurveyDocDt.Location = new System.Drawing.Point(96, 255);
            this.LblSurveyDocDt.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblSurveyDocDt.Name = "LblSurveyDocDt";
            this.LblSurveyDocDt.Size = new System.Drawing.Size(134, 18);
            this.LblSurveyDocDt.TabIndex = 49;
            this.LblSurveyDocDt.Text = "Tanggal Surat Ukur";
            this.LblSurveyDocDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSurveyDocNo
            // 
            this.TxtSurveyDocNo.EnterMoveNextControl = true;
            this.TxtSurveyDocNo.Location = new System.Drawing.Point(226, 224);
            this.TxtSurveyDocNo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtSurveyDocNo.Name = "TxtSurveyDocNo";
            this.TxtSurveyDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSurveyDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSurveyDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSurveyDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSurveyDocNo.Properties.MaxLength = 200;
            this.TxtSurveyDocNo.Size = new System.Drawing.Size(312, 24);
            this.TxtSurveyDocNo.TabIndex = 48;
            // 
            // LblSurveyDocNo
            // 
            this.LblSurveyDocNo.AutoSize = true;
            this.LblSurveyDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSurveyDocNo.ForeColor = System.Drawing.Color.Black;
            this.LblSurveyDocNo.Location = new System.Drawing.Point(123, 228);
            this.LblSurveyDocNo.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblSurveyDocNo.Name = "LblSurveyDocNo";
            this.LblSurveyDocNo.Size = new System.Drawing.Size(105, 18);
            this.LblSurveyDocNo.TabIndex = 47;
            this.LblSurveyDocNo.Text = "No. Surat Ukur";
            this.LblSurveyDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBasisDocDt
            // 
            this.DteBasisDocDt.EditValue = null;
            this.DteBasisDocDt.EnterMoveNextControl = true;
            this.DteBasisDocDt.Location = new System.Drawing.Point(226, 197);
            this.DteBasisDocDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteBasisDocDt.Name = "DteBasisDocDt";
            this.DteBasisDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBasisDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteBasisDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBasisDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBasisDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBasisDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBasisDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBasisDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBasisDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBasisDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBasisDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBasisDocDt.Size = new System.Drawing.Size(312, 24);
            this.DteBasisDocDt.TabIndex = 46;
            // 
            // LblBasisDocDt
            // 
            this.LblBasisDocDt.AutoSize = true;
            this.LblBasisDocDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBasisDocDt.ForeColor = System.Drawing.Color.Black;
            this.LblBasisDocDt.Location = new System.Drawing.Point(49, 201);
            this.LblBasisDocDt.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblBasisDocDt.Name = "LblBasisDocDt";
            this.LblBasisDocDt.Size = new System.Drawing.Size(187, 18);
            this.LblBasisDocDt.TabIndex = 45;
            this.LblBasisDocDt.Text = "Tanggal Dasar Pendaftaran";
            this.LblBasisDocDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRegistrationBasis
            // 
            this.TxtRegistrationBasis.EnterMoveNextControl = true;
            this.TxtRegistrationBasis.Location = new System.Drawing.Point(603, 170);
            this.TxtRegistrationBasis.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtRegistrationBasis.Name = "TxtRegistrationBasis";
            this.TxtRegistrationBasis.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRegistrationBasis.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRegistrationBasis.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRegistrationBasis.Properties.Appearance.Options.UseFont = true;
            this.TxtRegistrationBasis.Properties.MaxLength = 16;
            this.TxtRegistrationBasis.Size = new System.Drawing.Size(312, 24);
            this.TxtRegistrationBasis.TabIndex = 44;
            this.TxtRegistrationBasis.Visible = false;
            // 
            // LblRegistrationBasis
            // 
            this.LblRegistrationBasis.AutoSize = true;
            this.LblRegistrationBasis.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRegistrationBasis.ForeColor = System.Drawing.Color.Black;
            this.LblRegistrationBasis.Location = new System.Drawing.Point(103, 174);
            this.LblRegistrationBasis.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblRegistrationBasis.Name = "LblRegistrationBasis";
            this.LblRegistrationBasis.Size = new System.Drawing.Size(130, 18);
            this.LblRegistrationBasis.TabIndex = 43;
            this.LblRegistrationBasis.Text = "Dasar Pendaftaran";
            this.LblRegistrationBasis.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNIB
            // 
            this.TxtNIB.EnterMoveNextControl = true;
            this.TxtNIB.Location = new System.Drawing.Point(226, 143);
            this.TxtNIB.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtNIB.Name = "TxtNIB";
            this.TxtNIB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNIB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNIB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNIB.Properties.Appearance.Options.UseFont = true;
            this.TxtNIB.Properties.MaxLength = 200;
            this.TxtNIB.Size = new System.Drawing.Size(312, 24);
            this.TxtNIB.TabIndex = 42;
            // 
            // LblNIB
            // 
            this.LblNIB.AutoSize = true;
            this.LblNIB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNIB.ForeColor = System.Drawing.Color.Black;
            this.LblNIB.Location = new System.Drawing.Point(14, 147);
            this.LblNIB.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblNIB.Name = "LblNIB";
            this.LblNIB.Size = new System.Drawing.Size(223, 18);
            this.LblNIB.TabIndex = 41;
            this.LblNIB.Text = "Nomor Indentifikasi Bidang (NIB)";
            this.LblNIB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertIssuedLocation
            // 
            this.TxtCertIssuedLocation.EnterMoveNextControl = true;
            this.TxtCertIssuedLocation.Location = new System.Drawing.Point(226, 116);
            this.TxtCertIssuedLocation.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtCertIssuedLocation.Name = "TxtCertIssuedLocation";
            this.TxtCertIssuedLocation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCertIssuedLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertIssuedLocation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertIssuedLocation.Properties.Appearance.Options.UseFont = true;
            this.TxtCertIssuedLocation.Properties.MaxLength = 200;
            this.TxtCertIssuedLocation.Size = new System.Drawing.Size(312, 24);
            this.TxtCertIssuedLocation.TabIndex = 40;
            // 
            // LblCertIssuedLoc
            // 
            this.LblCertIssuedLoc.AutoSize = true;
            this.LblCertIssuedLoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertIssuedLoc.ForeColor = System.Drawing.Color.Black;
            this.LblCertIssuedLoc.Location = new System.Drawing.Point(138, 120);
            this.LblCertIssuedLoc.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblCertIssuedLoc.Name = "LblCertIssuedLoc";
            this.LblCertIssuedLoc.Size = new System.Drawing.Size(90, 18);
            this.LblCertIssuedLoc.TabIndex = 39;
            this.LblCertIssuedLoc.Text = "Letak Tanah";
            this.LblCertIssuedLoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteCertExpDt
            // 
            this.DteCertExpDt.EditValue = null;
            this.DteCertExpDt.EnterMoveNextControl = true;
            this.DteCertExpDt.Location = new System.Drawing.Point(603, 87);
            this.DteCertExpDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteCertExpDt.Name = "DteCertExpDt";
            this.DteCertExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertExpDt.Properties.Appearance.Options.UseFont = true;
            this.DteCertExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteCertExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteCertExpDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteCertExpDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertExpDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteCertExpDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertExpDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteCertExpDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteCertExpDt.Size = new System.Drawing.Size(312, 24);
            this.DteCertExpDt.TabIndex = 38;
            this.DteCertExpDt.Visible = false;
            // 
            // LblCertExpDt
            // 
            this.LblCertExpDt.AutoSize = true;
            this.LblCertExpDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertExpDt.ForeColor = System.Drawing.Color.Black;
            this.LblCertExpDt.Location = new System.Drawing.Point(64, 90);
            this.LblCertExpDt.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblCertExpDt.Name = "LblCertExpDt";
            this.LblCertExpDt.Size = new System.Drawing.Size(171, 18);
            this.LblCertExpDt.TabIndex = 37;
            this.LblCertExpDt.Text = "Tanggal Berakhirnya Hak";
            this.LblCertExpDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteCertIssuedDt
            // 
            this.DteCertIssuedDt.EditValue = null;
            this.DteCertIssuedDt.EnterMoveNextControl = true;
            this.DteCertIssuedDt.Location = new System.Drawing.Point(226, 59);
            this.DteCertIssuedDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteCertIssuedDt.Name = "DteCertIssuedDt";
            this.DteCertIssuedDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertIssuedDt.Properties.Appearance.Options.UseFont = true;
            this.DteCertIssuedDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertIssuedDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteCertIssuedDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteCertIssuedDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteCertIssuedDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertIssuedDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteCertIssuedDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertIssuedDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteCertIssuedDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteCertIssuedDt.Size = new System.Drawing.Size(312, 24);
            this.DteCertIssuedDt.TabIndex = 36;
            // 
            // LblCertIssuedDt
            // 
            this.LblCertIssuedDt.AutoSize = true;
            this.LblCertIssuedDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertIssuedDt.ForeColor = System.Drawing.Color.Black;
            this.LblCertIssuedDt.Location = new System.Drawing.Point(86, 63);
            this.LblCertIssuedDt.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblCertIssuedDt.Name = "LblCertIssuedDt";
            this.LblCertIssuedDt.Size = new System.Drawing.Size(139, 18);
            this.LblCertIssuedDt.TabIndex = 35;
            this.LblCertIssuedDt.Text = "Penerbitan Sertipikat";
            this.LblCertIssuedDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateNo
            // 
            this.TxtCertificateNo.EnterMoveNextControl = true;
            this.TxtCertificateNo.Location = new System.Drawing.Point(226, 31);
            this.TxtCertificateNo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtCertificateNo.Name = "TxtCertificateNo";
            this.TxtCertificateNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCertificateNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateNo.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateNo.Properties.MaxLength = 200;
            this.TxtCertificateNo.Size = new System.Drawing.Size(312, 24);
            this.TxtCertificateNo.TabIndex = 34;
            // 
            // LblCertNo
            // 
            this.LblCertNo.AutoSize = true;
            this.LblCertNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertNo.ForeColor = System.Drawing.Color.Black;
            this.LblCertNo.Location = new System.Drawing.Point(131, 35);
            this.LblCertNo.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblCertNo.Name = "LblCertNo";
            this.LblCertNo.Size = new System.Drawing.Size(94, 18);
            this.LblCertNo.TabIndex = 33;
            this.LblCertNo.Text = "No. Sertipikat";
            this.LblCertNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblCertificateType
            // 
            this.LblCertificateType.AutoSize = true;
            this.LblCertificateType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateType.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateType.Location = new System.Drawing.Point(123, 8);
            this.LblCertificateType.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblCertificateType.Name = "LblCertificateType";
            this.LblCertificateType.Size = new System.Drawing.Size(102, 18);
            this.LblCertificateType.TabIndex = 31;
            this.LblCertificateType.Text = "Jenis Sertipikat";
            this.LblCertificateType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCertificateType
            // 
            this.LueCertificateType.EnterMoveNextControl = true;
            this.LueCertificateType.Location = new System.Drawing.Point(226, 4);
            this.LueCertificateType.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueCertificateType.Name = "LueCertificateType";
            this.LueCertificateType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCertificateType.Properties.Appearance.Options.UseFont = true;
            this.LueCertificateType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCertificateType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCertificateType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCertificateType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCertificateType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCertificateType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCertificateType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCertificateType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCertificateType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCertificateType.Properties.DropDownRows = 30;
            this.LueCertificateType.Properties.NullText = "[Empty]";
            this.LueCertificateType.Properties.PopupWidth = 300;
            this.LueCertificateType.Size = new System.Drawing.Size(312, 24);
            this.LueCertificateType.TabIndex = 32;
            this.LueCertificateType.ToolTip = "F4 : Show/hide list";
            this.LueCertificateType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCertificateType.EditValueChanged += new System.EventHandler(this.LueCertificateType_EditValueChanged);
            // 
            // TpUploadFile
            // 
            this.TpUploadFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpUploadFile.Controls.Add(this.ChkFile2);
            this.TpUploadFile.Controls.Add(this.BtnDownload2);
            this.TpUploadFile.Controls.Add(this.PbUpload2);
            this.TpUploadFile.Controls.Add(this.TxtFile2);
            this.TpUploadFile.Controls.Add(this.label7);
            this.TpUploadFile.Controls.Add(this.BtnFile2);
            this.TpUploadFile.Controls.Add(this.ChkFile1);
            this.TpUploadFile.Controls.Add(this.BtnDownload1);
            this.TpUploadFile.Controls.Add(this.PbUpload1);
            this.TpUploadFile.Controls.Add(this.TxtFile1);
            this.TpUploadFile.Controls.Add(this.label22);
            this.TpUploadFile.Controls.Add(this.BtnFile1);
            this.TpUploadFile.Location = new System.Drawing.Point(4, 30);
            this.TpUploadFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpUploadFile.Name = "TpUploadFile";
            this.TpUploadFile.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpUploadFile.Size = new System.Drawing.Size(874, 398);
            this.TpUploadFile.TabIndex = 2;
            this.TpUploadFile.Text = "Upload File";
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(295, 66);
            this.ChkFile2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(23, 23);
            this.ChkFile2.TabIndex = 69;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.Click += new System.EventHandler(this.ChkFile2_Click);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(346, 66);
            this.BtnDownload2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(27, 27);
            this.BtnDownload2.TabIndex = 71;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(38, 96);
            this.PbUpload2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(336, 22);
            this.PbUpload2.TabIndex = 72;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(38, 69);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(253, 24);
            this.TxtFile2.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(6, 72);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 18);
            this.label7.TabIndex = 67;
            this.label7.Text = "File";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(315, 66);
            this.BtnFile2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(27, 27);
            this.BtnFile2.TabIndex = 70;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile1
            // 
            this.ChkFile1.Location = new System.Drawing.Point(295, 6);
            this.ChkFile1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkFile1.Name = "ChkFile1";
            this.ChkFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile1.Properties.Appearance.Options.UseFont = true;
            this.ChkFile1.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile1.Properties.Caption = " ";
            this.ChkFile1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile1.Size = new System.Drawing.Size(23, 23);
            this.ChkFile1.TabIndex = 63;
            this.ChkFile1.ToolTip = "Remove filter";
            this.ChkFile1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile1.ToolTipTitle = "Run System";
            this.ChkFile1.CheckedChanged += new System.EventHandler(this.ChkFile1_CheckedChanged);
            // 
            // BtnDownload1
            // 
            this.BtnDownload1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload1.Appearance.Options.UseBackColor = true;
            this.BtnDownload1.Appearance.Options.UseFont = true;
            this.BtnDownload1.Appearance.Options.UseForeColor = true;
            this.BtnDownload1.Appearance.Options.UseTextOptions = true;
            this.BtnDownload1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload1.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload1.Image")));
            this.BtnDownload1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload1.Location = new System.Drawing.Point(346, 6);
            this.BtnDownload1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnDownload1.Name = "BtnDownload1";
            this.BtnDownload1.Size = new System.Drawing.Size(27, 27);
            this.BtnDownload1.TabIndex = 65;
            this.BtnDownload1.ToolTip = "DownloadFile";
            this.BtnDownload1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload1.ToolTipTitle = "Run System";
            this.BtnDownload1.Click += new System.EventHandler(this.BtnDownload1_Click);
            // 
            // PbUpload1
            // 
            this.PbUpload1.Location = new System.Drawing.Point(38, 37);
            this.PbUpload1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PbUpload1.Name = "PbUpload1";
            this.PbUpload1.Size = new System.Drawing.Size(336, 22);
            this.PbUpload1.TabIndex = 66;
            // 
            // TxtFile1
            // 
            this.TxtFile1.EnterMoveNextControl = true;
            this.TxtFile1.Location = new System.Drawing.Point(38, 10);
            this.TxtFile1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtFile1.Name = "TxtFile1";
            this.TxtFile1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile1.Properties.Appearance.Options.UseFont = true;
            this.TxtFile1.Properties.MaxLength = 16;
            this.TxtFile1.Size = new System.Drawing.Size(253, 24);
            this.TxtFile1.TabIndex = 62;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 13);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 18);
            this.label22.TabIndex = 61;
            this.label22.Text = "File";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile1
            // 
            this.BtnFile1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile1.Appearance.Options.UseBackColor = true;
            this.BtnFile1.Appearance.Options.UseFont = true;
            this.BtnFile1.Appearance.Options.UseForeColor = true;
            this.BtnFile1.Appearance.Options.UseTextOptions = true;
            this.BtnFile1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile1.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile1.Image")));
            this.BtnFile1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile1.Location = new System.Drawing.Point(315, 6);
            this.BtnFile1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnFile1.Name = "BtnFile1";
            this.BtnFile1.Size = new System.Drawing.Size(27, 27);
            this.BtnFile1.TabIndex = 64;
            this.BtnFile1.ToolTip = "BrowseFile";
            this.BtnFile1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile1.ToolTipTitle = "Run System";
            this.BtnFile1.Click += new System.EventHandler(this.BtnFile1_Click);
            // 
            // TpApprovalInformation
            // 
            this.TpApprovalInformation.Controls.Add(this.Grd2);
            this.TpApprovalInformation.Location = new System.Drawing.Point(4, 30);
            this.TpApprovalInformation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpApprovalInformation.Name = "TpApprovalInformation";
            this.TpApprovalInformation.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TpApprovalInformation.Size = new System.Drawing.Size(874, 398);
            this.TpApprovalInformation.TabIndex = 3;
            this.TpApprovalInformation.Text = "Approval Information";
            this.TpApprovalInformation.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 22;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(3, 4);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(868, 390);
            this.Grd2.TabIndex = 56;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EditValue = "";
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(163, 87);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 1000;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(246, 24);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(62, 91);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(103, 18);
            this.label16.TabIndex = 16;
            this.label16.Text = "Cancel Reason";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(410, 85);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(70, 23);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(163, 33);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(246, 24);
            this.TxtStatus.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(113, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 18);
            this.label9.TabIndex = 12;
            this.label9.Text = "Status";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmPropertyInventoryTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 597);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmPropertyInventoryTransfer";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeFrom.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCodeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCodeFrom.Properties)).EndInit();
            this.Tc.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpCertificationInformation.ResumeLayout(false);
            this.TpCertificationInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRegistrationBasis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertExpDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBased.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSurveyDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSurveyDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBasisDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBasisDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegistrationBasis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertIssuedLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertExpDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertExpDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertIssuedDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertIssuedDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCertificateType.Properties)).EndInit();
            this.TpUploadFile.ResumeLayout(false);
            this.TpUploadFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).EndInit();
            this.TpApprovalInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LuePropertyCtFrom;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblSiteTo;
        private System.Windows.Forms.Label LblSiteFrom;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteCodeFrom;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueCCCodeFrom;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.LookUpEdit LuePropertyCtTo;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteCodeTo;
        internal DevExpress.XtraEditors.LookUpEdit LueCCCodeTo;
        private System.Windows.Forms.TabControl Tc;
        private System.Windows.Forms.TabPage TpGeneral;
        private System.Windows.Forms.TabPage TpCertificationInformation;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.TabPage TpUploadFile;
        internal DevExpress.XtraEditors.TextEdit TxtNIB;
        private System.Windows.Forms.Label LblNIB;
        internal DevExpress.XtraEditors.TextEdit TxtCertIssuedLocation;
        private System.Windows.Forms.Label LblCertIssuedLoc;
        internal DevExpress.XtraEditors.DateEdit DteCertExpDt;
        private System.Windows.Forms.Label LblCertExpDt;
        internal DevExpress.XtraEditors.DateEdit DteCertIssuedDt;
        private System.Windows.Forms.Label LblCertIssuedDt;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateNo;
        private System.Windows.Forms.Label LblCertNo;
        private System.Windows.Forms.Label LblCertificateType;
        internal DevExpress.XtraEditors.LookUpEdit LueCertificateType;
        internal DevExpress.XtraEditors.TextEdit TxtRegistrationBasis;
        private System.Windows.Forms.Label LblRegistrationBasis;
        internal DevExpress.XtraEditors.TextEdit TxtAreaBased;
        private System.Windows.Forms.Label LblAreaBased;
        internal DevExpress.XtraEditors.DateEdit DteSurveyDocDt;
        private System.Windows.Forms.Label LblSurveyDocDt;
        internal DevExpress.XtraEditors.TextEdit TxtSurveyDocNo;
        private System.Windows.Forms.Label LblSurveyDocNo;
        internal DevExpress.XtraEditors.DateEdit DteBasisDocDt;
        private System.Windows.Forms.Label LblBasisDocDt;
        private DevExpress.XtraEditors.CheckEdit ChkFile1;
        public DevExpress.XtraEditors.SimpleButton BtnDownload1;
        private System.Windows.Forms.ProgressBar PbUpload1;
        internal DevExpress.XtraEditors.TextEdit TxtFile1;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.SimpleButton BtnFile1;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.TabPage TpApprovalInformation;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.MemoExEdit MeeCertExpDt;
        private DevExpress.XtraEditors.MemoExEdit MeeRegistrationBasis;
    }
}