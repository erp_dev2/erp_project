﻿namespace RunSystem
{
    partial class FrmRptRawMaterialCompare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteUnloadingDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteUnloadingDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkInspectionDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteInspectionDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteInspectionDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtQueueNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkQueueNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInspectionDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQueueNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtQueueNo);
            this.panel2.Controls.Add(this.ChkQueueNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkInspectionDt);
            this.panel2.Controls.Add(this.DteInspectionDt2);
            this.panel2.Controls.Add(this.DteInspectionDt1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteUnloadingDt2);
            this.panel2.Controls.Add(this.DteUnloadingDt1);
            this.panel2.Size = new System.Drawing.Size(772, 76);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 397);
            this.Grd1.TabIndex = 20;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(211, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 10;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Unloading Date";
            // 
            // DteUnloadingDt2
            // 
            this.DteUnloadingDt2.EditValue = null;
            this.DteUnloadingDt2.EnterMoveNextControl = true;
            this.DteUnloadingDt2.Location = new System.Drawing.Point(226, 4);
            this.DteUnloadingDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUnloadingDt2.Name = "DteUnloadingDt2";
            this.DteUnloadingDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadingDt2.Properties.Appearance.Options.UseFont = true;
            this.DteUnloadingDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadingDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUnloadingDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUnloadingDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUnloadingDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadingDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUnloadingDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadingDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUnloadingDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUnloadingDt2.Size = new System.Drawing.Size(102, 20);
            this.DteUnloadingDt2.TabIndex = 11;
            // 
            // DteUnloadingDt1
            // 
            this.DteUnloadingDt1.EditValue = null;
            this.DteUnloadingDt1.EnterMoveNextControl = true;
            this.DteUnloadingDt1.Location = new System.Drawing.Point(104, 4);
            this.DteUnloadingDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUnloadingDt1.Name = "DteUnloadingDt1";
            this.DteUnloadingDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadingDt1.Properties.Appearance.Options.UseFont = true;
            this.DteUnloadingDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadingDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUnloadingDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUnloadingDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUnloadingDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadingDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUnloadingDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadingDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUnloadingDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUnloadingDt1.Size = new System.Drawing.Size(102, 20);
            this.DteUnloadingDt1.TabIndex = 9;
            this.DteUnloadingDt1.EditValueChanged += new System.EventHandler(this.DteUnloadingDt1_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(211, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Inspection Date";
            // 
            // ChkInspectionDt
            // 
            this.ChkInspectionDt.Location = new System.Drawing.Point(330, 26);
            this.ChkInspectionDt.Name = "ChkInspectionDt";
            this.ChkInspectionDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInspectionDt.Properties.Appearance.Options.UseFont = true;
            this.ChkInspectionDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInspectionDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInspectionDt.Properties.Caption = " ";
            this.ChkInspectionDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInspectionDt.Size = new System.Drawing.Size(22, 22);
            this.ChkInspectionDt.TabIndex = 16;
            this.ChkInspectionDt.ToolTip = "Remove filter";
            this.ChkInspectionDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInspectionDt.ToolTipTitle = "Run System";
            this.ChkInspectionDt.CheckedChanged += new System.EventHandler(this.ChkInspectionDt_CheckedChanged);
            // 
            // DteInspectionDt2
            // 
            this.DteInspectionDt2.EditValue = null;
            this.DteInspectionDt2.EnterMoveNextControl = true;
            this.DteInspectionDt2.Location = new System.Drawing.Point(226, 26);
            this.DteInspectionDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteInspectionDt2.Name = "DteInspectionDt2";
            this.DteInspectionDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInspectionDt2.Properties.Appearance.Options.UseFont = true;
            this.DteInspectionDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInspectionDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteInspectionDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteInspectionDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteInspectionDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInspectionDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteInspectionDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInspectionDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteInspectionDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteInspectionDt2.Size = new System.Drawing.Size(102, 20);
            this.DteInspectionDt2.TabIndex = 15;
            this.DteInspectionDt2.EditValueChanged += new System.EventHandler(this.DteInspectionDt2_EditValueChanged);
            // 
            // DteInspectionDt1
            // 
            this.DteInspectionDt1.EditValue = null;
            this.DteInspectionDt1.EnterMoveNextControl = true;
            this.DteInspectionDt1.Location = new System.Drawing.Point(104, 26);
            this.DteInspectionDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteInspectionDt1.Name = "DteInspectionDt1";
            this.DteInspectionDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInspectionDt1.Properties.Appearance.Options.UseFont = true;
            this.DteInspectionDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInspectionDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteInspectionDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteInspectionDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteInspectionDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInspectionDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteInspectionDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInspectionDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteInspectionDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteInspectionDt1.Size = new System.Drawing.Size(102, 20);
            this.DteInspectionDt1.TabIndex = 13;
            this.DteInspectionDt1.EditValueChanged += new System.EventHandler(this.DteInspectionDt1_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(48, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Queue#";
            // 
            // TxtQueueNo
            // 
            this.TxtQueueNo.EnterMoveNextControl = true;
            this.TxtQueueNo.Location = new System.Drawing.Point(104, 48);
            this.TxtQueueNo.Name = "TxtQueueNo";
            this.TxtQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQueueNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQueueNo.Properties.MaxLength = 30;
            this.TxtQueueNo.Size = new System.Drawing.Size(224, 20);
            this.TxtQueueNo.TabIndex = 18;
            this.TxtQueueNo.Validated += new System.EventHandler(this.TxtQueueNo_Validated);
            // 
            // ChkQueueNo
            // 
            this.ChkQueueNo.Location = new System.Drawing.Point(330, 47);
            this.ChkQueueNo.Name = "ChkQueueNo";
            this.ChkQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkQueueNo.Properties.Appearance.Options.UseFont = true;
            this.ChkQueueNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkQueueNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkQueueNo.Properties.Caption = " ";
            this.ChkQueueNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkQueueNo.Size = new System.Drawing.Size(22, 22);
            this.ChkQueueNo.TabIndex = 19;
            this.ChkQueueNo.ToolTip = "Remove filter";
            this.ChkQueueNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkQueueNo.ToolTipTitle = "Run System";
            this.ChkQueueNo.CheckedChanged += new System.EventHandler(this.ChkQueueNo_CheckedChanged);
            // 
            // FrmRptRawMaterialCompare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptRawMaterialCompare";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadingDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInspectionDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInspectionDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQueueNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkInspectionDt;
        internal DevExpress.XtraEditors.DateEdit DteInspectionDt2;
        internal DevExpress.XtraEditors.DateEdit DteInspectionDt1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteUnloadingDt2;
        internal DevExpress.XtraEditors.DateEdit DteUnloadingDt1;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtQueueNo;
        private DevExpress.XtraEditors.CheckEdit ChkQueueNo;

    }
}