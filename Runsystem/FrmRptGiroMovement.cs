﻿#region Update
/*
    18/07/2018 [TKG] tambah informasi site PO
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptGiroMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "";
        private bool mIsSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmRptGiroMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueBankCode(ref LueBankCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        private string GetSQL(string filter1, string filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select E.OptDesc As DocTypeDesc, A.DocNo, B.VdName As Name, 'Vendor' As Type, A.BankCode, C.BankName, A.GiroNo, D.OpeningDt, D.DueDt, D.CurCode, D.Amt, F.PODocNo ");
            SQL.AppendLine("    from TblGiroMovement A ");
            SQL.AppendLine("    Inner Join TblVendor B On A.BusinessPartnerCode=B.VdCode " + filter1);
            SQL.AppendLine("    Left Join TblBank C On A.BankCode=C.BankCode ");
            SQL.AppendLine("    Left Join TblGiroSummary D On A.BusinessPartnerCode=D.BusinessPartnerCode And A.BusinessPartnerType=D.BusinessPartnerType And A.BankCode=D.BankCode And A.GiroNo=D.GiroNo ");
            SQL.AppendLine("    Left Join TblOption E On A.DocType=E.OptCode And E.OptCat='GiroDocType' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.DocNo, ");
            if (mIsSiteMandatory)
                SQL.AppendLine("        Group_Concat(Distinct Concat(T.PODocNo, Case When T.SiteName Is Null Then '' Else Concat(' (', T.SiteName, ')') End) Order By T.SiteName, T.PODocNo Separator ', ') As PODocNo ");
            else
                SQL.AppendLine("        Group_Concat(Distinct T.PODocNo Order By T.PODocNo Separator ', ') As PODocNo ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T1.DocNo, T2.PODocNo, T4.SiteName ");
            SQL.AppendLine("            From TblVoucherHdr T1 ");
            SQL.AppendLine("            Inner Join TblAPDownpayment T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo And T2.PODocNo Is Not Null ");
            SQL.AppendLine("            Inner Join TblPOHdr T3 On T2.PODocNo=T3.DocNo ");
            SQL.AppendLine("            Left Join TblSite T4 On T3.SiteCode=T4.SiteCode ");
            SQL.AppendLine("            Where T1.GiroNo Is Not Null And T1.DocType='04' ");
            SQL.AppendLine("            union All ");
            SQL.AppendLine("            Select T1.DocNo, T4.PODocNo, T6.SiteName ");
            SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceDtl T3 On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            Inner Join TblRecvVdDtl T4 On T3.RecvVdDocNo=T4.DocNo And T3.RecvVdDNo=T4.DNo ");
            SQL.AppendLine("            Left Join TblPOHdr T5 On T4.PODocNo=T5.DocNo ");
            SQL.AppendLine("            Left Join TblSite T6 On T5.SiteCode=T6.SiteCode ");
            SQL.AppendLine("            Where Exists(Select 1 From TblOutgoingPaymentDtl3 Where DocNo=T1.DocNo Limit 1) ");
            SQL.AppendLine("        ) T Group By T.DocNo ");
            SQL.AppendLine("    ) F On A.DocNo=F.DocNo ");
            SQL.AppendLine("    Where A.BusinessPartnerType='1' ");
            SQL.AppendLine("Union all ");
            SQL.AppendLine("    Select E.OptDesc As DocTypeDesc, A.DocNo, B.CtName As Name, 'Customer' As Type, A.BankCode, C.BankName, A.GiroNo, D.OpeningDt, D.DueDt, D.CurCode, D.Amt, Null As PODocNo ");
            SQL.AppendLine("    from tblgiromovement A ");
            SQL.AppendLine("    Inner Join TblCustomer B On A.BusinessPartnerCode=B.CtCode " + filter2);
            SQL.AppendLine("    Left Join TblBank C On A.BankCode=C.BankCode ");
            SQL.AppendLine("    Left Join Tblgirosummary D On A.BusinessPartnerCode=D.BusinessPartnerCode And A.BusinessPartnerType=D.BusinessPartnerType And A.BankCode=D.BankCode And A.GiroNo=D.GiroNo ");
            SQL.AppendLine("    Left Join TblOption E On A.DocType=E.OptCode And E.OptCat='GiroDocType' ");
            SQL.AppendLine("    Where A.BusinessPartnerType='2' " );
            SQL.AppendLine(") Tbl ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Transaction",
                        "Document#", 
                        "Name", 
                        "Type",
                        "Bank",
                       
                        //6-10
                        "Giro#",
                        "Opening Date",
                        "Due Date",
                        "Currency",
                        "Amount",

                        //11
                        "PO#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        250, 130, 200, 100, 250,
                        
                        //6-10
                        150, 100, 100, 60, 130,

                        //11
                        350
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "", Filter1 = " ", Filter2 = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBankCode), "Tbl.BankCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtGiroNo.Text, "Tbl.GiroNo", false);

                if (!(Filter1 == " " && Filter2 == " "))
                {
                    if (Filter1 == " ") Filter1 = " And 1=0 ";
                    if (Filter2 == " ") Filter2 = " And 1=0 ";
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter1, Filter2) + Filter + 
                        " Order By Tbl.Type, Tbl.Name, Tbl.BankName, Tbl.GiroNo;",
                        new string[]
                        {
                            //0
                            "DocTypeDesc",

                            //1-5
                            "DocNo", "Name", "Type", "BankName", "GiroNo", 
                            
                            //6-10
                            "OpeningDt", "DueDt", "CurCode", "Amt", "PODocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGiroNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Giro#");
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bank");
        }

        #endregion

    }
}
