﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPNTDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPNT mFrmParent;
        private string mSQL = string.Empty, mWorkCenterDocNo = string.Empty;
        private decimal mHolidayWagesIndex = 1m, mNotHolidayWagesIndex = 1m;

        #endregion

        #region Constructor

        public FrmPNTDlg(FrmPNT FrmParent, string WorkCenterDocNo, decimal HolidayWagesIndex, decimal NotHolidayWagesIndex)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWorkCenterDocNo = WorkCenterDocNo;
            mHolidayWagesIndex = HolidayWagesIndex;
            mNotHolidayWagesIndex = NotHolidayWagesIndex;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -45);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "SFC#",
                        "SFC DNo",
                        "Date",
                        "Shift",

                        //6-10
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Item's Code",

                        //11-15
                        "Item's Name",
                        "Index",
                        "Batch#",
                        "UoM",
                        "Uom 2",

                        //16-18
                        "Holiday",
                        "Holiday"+Environment.NewLine+"Index",
                        "Shift Code"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 130, 0, 100, 150, 
                    
                        //6-10
                        100, 200, 150, 150, 80, 

                        //11-15
                        200, 70, 180, 80, 80,  

                        //16-18
                        150, 80, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 8, 9, 10, 14, 15, 16, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 8, 9, 10, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, C.ProductionShiftName, ");
            SQL.AppendLine("B.ItCode, D.ItName, B.BatchNo, D.PlanningUomCode, D.PlanningUomCode2, ");
            SQL.AppendLine("F.HolName, if(IfNull(F.HolName, '')='', @NotHolidayWagesIndex, @HolidayWagesIndex) As HolidayWagesIndex, IfNull(E.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("G.BomCode As EmpCode, H.EmpName, I.PosName, J.DeptName, A.ProductionShiftCode ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Left Join TblShopFloorControlDtl B On A.DocNo=B.DocNo And B.ProcessInd2<>'F' ");
            SQL.AppendLine("Left Join TblProductionShift C On A.ProductionShiftCode=C.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblHoliday F On A.DocDt=F.HolDt ");
            SQL.AppendLine("Left Join TblShopFloorControl2Dtl G On B.DocNo=G.DocNo And G.CoordinatorInd='N' And G.BomType='3' ");
            SQL.AppendLine("Left Join TblEmployee H On G.BomCode=H.EmpCode ");
            SQL.AppendLine("Left Join TblPosition I On H.PosCode=I.PosCode ");
            SQL.AppendLine("Left Join TblDepartment J On H.DeptCode=J.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.WorkCenterDocNo=@WorkCenterDocNo ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, B.DNo, IfNull(G.BomCode, ''), '##'), @SelectedData)<1 ");
           
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "G.BomCode", "H.EmpName" });

                Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);
                Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", mHolidayWagesIndex);
                Sm.CmParam<Decimal>(ref cm, "@NotHolidayWagesIndex", mNotHolidayWagesIndex);
                Sm.CmParam<String>(ref cm, "@SelectedData", mFrmParent.GetSelectedData());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo, H.EmpName;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                             
                            //1-5
                            "DNo",
                            "DocDt", 
                            "ProductionShiftName",
                            "EmpCode",
                            "EmpName",

                            //6-10
                            "PosName",
                            "DeptName",
                            "ItCode",
                            "ItName",
                            "ItIndex",

                            //11-15
                            "BatchNo",
                            "PlanningUomCode",
                            "PlanningUomCode2",
                            "HolName",
                            "HolidayWagesIndex",

                            //16
                            "ProductionShiftCode"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        mFrmParent.Grd1.Cells[Row1, 18].Value = Sm.GetGrdStr(Grd1, Row2, 16).Length > 0;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);

                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, Row1, new int[] { 14, 16, 20 });
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 18 });
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12, 14, 16, 20 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
            else
                mFrmParent.ShowDocInfo();
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 2) +
                Sm.GetGrdStr(Grd1, Row, 3) +
                Sm.GetGrdStr(Grd1, Row, 6);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key,
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 3) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 6)
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee#");
        }

        #endregion

        #endregion
    }
}
