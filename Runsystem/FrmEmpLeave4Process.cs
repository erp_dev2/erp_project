﻿#region Update
/* 
  17/02/2020 [HAR/IMS] : leave hour ambil dari leeave yg type nya cuti ijin pribadi, tambah info break 
 * 19/02/2020 [HAR/IMS] : tambah informasi break 2 .
 * 22/02/2020 [HAR/IMS] : bug insert leave dtl.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave4Process : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmEmpLeave4ProcessFind FrmFind;
        internal decimal mToleranceLeaveOnMinute = 0m;
        internal decimal mDurationForWastedTimeLeave = 0m;
        internal string mWastedTimeLeaveCode = string.Empty;
        internal string mAnnualLeaveCode = string.Empty;
        internal string mLeaveCodePermisionPersonal = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpLeave4Process(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Employee Leave Process";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mToleranceLeaveOnMinute = Sm.GetParameterDec("ToleranceLeaveOnMinute");
            mWastedTimeLeaveCode = Sm.GetParameter("WastedTimeLeaveCode");
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mDurationForWastedTimeLeave = Sm.GetParameterDec("DurationForWastedTimeLeave");
            mLeaveCodePermisionPersonal = Sm.GetParameter("LeaveCodePermisionPersonal");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo#",
                        
                        //1-5
                        "",
                        "Document",
                        "Document"+Environment.NewLine+"Date",
                        "Employee"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Old Code",

                        //6-10
                        "Employee",
                        "Department",
                        "Position",
                        "Site",
                        "Start Leave"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Req Start" + Environment.NewLine + "Time",
                        "Req End"+Environment.NewLine+"Time",
                        "Log Start" + Environment.NewLine + "Time",
                        "Log End" + Environment.NewLine + "Time",
                        "Actual Start" + Environment.NewLine + "Time",

                        //16-20
                        "Actual End" + Environment.NewLine + "Time",
                        "Duration",
                        "Annual" + Environment.NewLine + "Leave",
                        "Type",
                        "Leave" + Environment.NewLine + "Document",
                        //21-25
                        "",
                        "Break",
                        "Break" + Environment.NewLine + "Time Out",
                        "Break" + Environment.NewLine + "Time In",
                        "Break" + Environment.NewLine + "Time 2 Out",
                        //26
                        "Break" + Environment.NewLine + "Time 2 In",
                        "LeaveCode",
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        //1-5
                        20, 120, 80, 100, 100,  
                        //6-10
                        120, 120, 120, 120, 100,  
                        //11-15
                        80, 80, 80, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 120,
                        //21-25
                        20, 80, 80, 80, 80,
                        //26
                        80, 80
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 18}, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 23, 24, 25, 26 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 23, 24, 25, 26, 27}, false);
            Sm.GrdColCheck(Grd1, new int[] { 22 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtDocNo, DteDocDt, MeeRemark, ChkCancelInd, LueYr, LueMth
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 15, 16 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueYr, LueMth
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 15, 16 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd
                    }, false);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark, LueYr, LueMth
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLeave4ProcessFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count > 0 && BtnSave.Enabled)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Sm.SetGrdValueNull(ref Grd1, Row, new int[] { 13, 14, 15, 16, 17, 19, 20 });
                }
                Process1();
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;
            int RowDocNo = 0;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeaveHourProcess", "TblEmpLeaveHourProcessHdr");
            string DocNoLeave = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeave", "TblEmpLeaveHdr");
             
            var cml = new List<MySqlCommand>();

            cml.Add(SaveELHPHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveELHPDtl(DocNo, Row, DocNoLeave));

                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 27).Length > 0)
                {
                    RowDocNo = RowDocNo + 1;
                
                    if (RowDocNo == 1)
                    {
                        cml.Add(SaveELDtl(DocNo, Row, DocNoLeave));
                    }
                    else
                    {
                        string DocNoLeave2 = String.Concat(Sm.Right(String.Concat("0000", Convert.ToString(Convert.ToInt32(Sm.Left(DocNoLeave, 4)) + RowDocNo)), 4), DocNoLeave.Substring(4, (DocNoLeave.Length - 4)));
                        cml.Add(SaveELDtl(DocNo, Row, DocNoLeave2));
                    }
                }
            }
                

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords()||
                IsGrdTimeEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdTimeEmpty()
        {
            if (Grd1.Rows.Count > 1)
            {
                for(int Row=0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 15).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Employee : "+Sm.GetGrdStr(Grd1, Row, 6)+ " '"+Environment.NewLine+"' Actual start time not valid !.");
                        return true;
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 16).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Employee : " + Sm.GetGrdStr(Grd1, Row, 6) + " '" + Environment.NewLine + "' Actual end time not valid !.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Document data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveELHPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeaveHourProcessHdr(DocNo, DocDt, Yr, Mth, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, @Mth, 'N', @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveELHPDtl(string DocNo, int Row, string DocNoLeave)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeaveHourProcessDtl(DocNo, DNo, LeaveHourDocNo,  ");
            SQL.AppendLine("LogStartTm, LogEndTm, ActualStartTm, ActualEndTm, Duration, LeaveAmt, LeaveCode, LeaveDocNo, BTm1, BTm2, BTm3, BTm4, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @LeaveHourDocNo,  ");
            SQL.AppendLine("@LogStartTm, @LogEndTm, @ActualStartTm, @ActualEndTm, @Duration, @LeaveAmt, @LeaveCode, null, @BTm1, @BTm2, @BTm3, @BTm4, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNoLeave", DocNoLeave);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@LeaveHourDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@LogStartTm", Sm.GetGrdStr(Grd1, Row, 13).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@LogEndTm", Sm.GetGrdStr(Grd1, Row, 14).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@ActualStartTm", Sm.GetGrdStr(Grd1, Row, 15).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@ActualEndTm", Sm.GetGrdStr(Grd1, Row, 16).Replace(":", ""));
            Sm.CmParam<Decimal>(ref cm, "@Duration", Decimal.Parse(Sm.Left(Sm.GetGrdStr(Grd1, Row, 17).Replace(":", ""), 2)) + (Decimal.Parse(Sm.Right(Sm.GetGrdStr(Grd1, Row, 17).Replace(":", ""), 2)) / 10));
            Sm.CmParam<Decimal>(ref cm, "@LeaveAmt", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetValue("Select LeaveCode From TblLeave Where leaveName = '" + Sm.GetGrdStr(Grd1, Row, 19) + "' "));
            Sm.CmParam<String>(ref cm, "@BTm1", Sm.GetGrdStr(Grd1, Row, 23).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@BTm2", Sm.GetGrdStr(Grd1, Row, 24).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@BTm3", Sm.GetGrdStr(Grd1, Row, 25).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@BTm4", Sm.GetGrdStr(Grd1, Row, 26).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveELDtl(string DocNo, int Row, string DocNoLeave)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeaveHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, EmpCode, LeaveCode, ");
            SQL.AppendLine("LeaveType, LeaveStartDt, StartDt, EndDt, DurationDay, StartTm, EndTm, DurationHour, BreakInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNoLeave, DocDt, 'N', 'A', EmpCode, @LeaveCode,  ");
            SQL.AppendLine("'F', LeaveStartDt, StartDt, StartDt, '1', @ActualStartTm, @ActualEndTm, @Duration, BreakInd, null,   ");
            SQL.AppendLine("@CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblEmpleaveHdr ");
            SQL.AppendLine("Where DocNo = @LeaveHourDocNo ;");

            SQL.AppendLine("Insert Into TblEmpLeaveDtl ");
            SQL.AppendLine("(DocNO, Dno, LeaveDt, CreateBy, CreateDt  )");
            SQL.AppendLine("Select @DocNoLeave, '001', LeaveDt, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblEmpleaveDtl ");
            SQL.AppendLine("Where DocNo = @LeaveHourDocNo ;");


            if (Sm.GetGrdStr(Grd1, Row, 27).Length > 0)
            {
                SQL.AppendLine("Update TblEmpleavehdr ");
                SQL.AppendLine("SET  CancelInd = 'Y', ");
                SQL.AppendLine("CancelReason = 'cancel by system, this document already changed to annual leave or wasted time leave ', ");
                SQL.AppendLine("Remark = 'cancel by system, this document already changed to annual leave or wasted time leave ', ");
                SQL.AppendLine("LastUpBy = @CreateBy, ");
                SQL.AppendLine("LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@LeaveHourDocNo; ");
            }

            if (Sm.GetGrdStr(Grd1, Row, 19) == Sm.GetValue("Select LeaveName From TblLeave Where leaveCode = '" + mAnnualLeaveCode + "' "))
            {
                SQL.AppendLine("Update TblleaveSummary ");
                SQL.AppendLine("SET  NoOfDays2 = NoOfDays2+1, ");
                SQL.AppendLine("balance = balance-1, ");
                SQL.AppendLine("LastUpBy = @CreateBy, ");
                SQL.AppendLine("LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And Yr= @Yr And LeaveCode = @LeaveCode ; ");
            }

                SQL.AppendLine("Update tblempleavehourprocessdtl ");
                SQL.AppendLine("SET LeaveDocNo = @DocNoLeave ");
                SQL.AppendLine("Where DocNo=@DocNo And LeavehourDocNo = @LeaveHourDocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNoLeave", DocNoLeave);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@LeaveHourDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@LogStartTm", Sm.GetGrdStr(Grd1, Row, 13).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@LogEndTm", Sm.GetGrdStr(Grd1, Row, 14).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@ActualStartTm", Sm.GetGrdStr(Grd1, Row, 15).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@ActualEndTm", Sm.GetGrdStr(Grd1, Row, 16).Replace(":", ""));
            Sm.CmParam<Decimal>(ref cm, "@Duration", Decimal.Parse(Sm.Left(Sm.GetGrdStr(Grd1, Row, 17).Replace(":", ""), 2)) + (Decimal.Parse(Sm.Right(Sm.GetGrdStr(Grd1, Row, 17).Replace(":", ""), 2)) / 10));
            Sm.CmParam<Decimal>(ref cm, "@LeaveAmt", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetGrdStr(Grd1, Row, 27).Length>0 ? Sm.GetGrdStr(Grd1, Row, 27): null);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetGrdDate(Grd1, Row, 10), 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelELHPHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready()||
                IsDataLeaveActive();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpLeaveHourProcessHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }


        private bool IsDataLeaveActive()
        {
            string DocNoLeave = string.Empty;
            var SQL = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 20).Length > 0)
                {
                    DocNoLeave = Sm.GetGrdStr(Grd1, Row, 20);

                    SQL.AppendLine("Select DocNo From TblEmpLeaveHdr ");
                    SQL.AppendLine("Where CancelInd='N' And DocNo=@DocNo ; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNoLeave);

                    if (Sm.IsDataExist(cm))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Document Leave Request : " + DocNoLeave + " must be cancel.");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand CancelELHPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpLeaveHourProcessHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion
        #endregion

        #region  Show Data

        internal void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowELHPHdr(DocNo);
                ShowELHPDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowELHPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.Mth, A.CancelInd, A.Remark ");
            SQL.AppendLine("From TblEmpLeaveHourProcessHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Yr", "Mth", "CancelInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueYr(LueYr, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        Sl.SetLueMth(LueMth);
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[3]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowELHPDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.leavehourDocNo, B.DocDt,  B.EmpCode, C.empCodeOld, C.Empname, ");
            SQL.AppendLine("D.Deptname, E.Posname, F.SiteName, B.StartDt, B.StartTm, B.EndTm,  ");
            SQL.AppendLine("A.LogStartTm, A.LogEndTm, A.ActualStartTm, A.ActualEndTm, A.Duration,  ");
            SQL.AppendLine("A.leaveAmt, G.Leavename, A.LeaveDocNo, A.BTm1, A.BTm2, A.BTm3, A.BTm4, B.BreakInd ");
            SQL.AppendLine("From tblEmpleaveHourProcessDtl A ");
            SQL.AppendLine("Left Join TblEmpleaveHdr B On A.LeaveHourDocNo = B.DocNO ");
            SQL.AppendLine("Inner Join Tblemployee C On B.EmpCode = C.EmpCode ");
            SQL.AppendLine("Inner Join Tbldepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode ");
            SQL.AppendLine("Left join TblSite F On C.SiteCode = F.SiteCode ");
            SQL.AppendLine("Left Join Tblleave G On A.LeaveCode = G.LeaveCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "leavehourDocNo", "DocDt", "EmpCode", "empCodeOld", "Empname", 
                    
                    //6-10
                    "Deptname", "Posname", "SiteName", "StartDt", "StartTm",    
                    
                    //11-15
                    "EndTm", "LogStartTm", "LogEndTm", "ActualStartTm", "ActualEndTm", 
 
                    //16-20
                    "Duration", "leaveAmt", "Leavename", "LeaveDocNo", "BTm1",

                    //21-24
                    "BTm2", "BTm3", "BTm4", "BreakInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 24);
                    //Grd1.Cells[Row, 22].Value = Sm.GetGrdStr(Grd, Row, 23).Length>0 ? true : false;                    
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) 
                            Sm.FormShowDialog(new FrmEmpLeave4ProcessDlg(this, string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth))));
                    }
                }
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length>0)
            {
                 var f = new FrmEmpLeave(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                    f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 )
                Sm.FormShowDialog(new FrmEmpLeave4ProcessDlg(this, string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth))));

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length > 0)
            {
                var f = new FrmEmpLeave(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 15 || e.ColIndex == 16)
            {
                if (BtnSave.Enabled)
                {
                    string BtTm1 = string.Empty;
                    string BtTm2 = string.Empty;
                    string BtTm3 = string.Empty;
                    string BtTm4 = string.Empty;

                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length>0)
                        BtTm1 = Sm.GetGrdStr(Grd1, e.RowIndex, 23).Replace(":", "");
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length > 0)
                        BtTm2 = Sm.GetGrdStr(Grd1, e.RowIndex, 24).Replace(":", "");

                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 25).Length > 0)
                        BtTm3 = Sm.GetGrdStr(Grd1, e.RowIndex, 25).Replace(":", "");
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 26).Length > 0)
                        BtTm4 = Sm.GetGrdStr(Grd1, e.RowIndex, 26).Replace(":", "");

                    if(IsGrdTimeNotValid(Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex))==false)
                        ComputeHour(Sm.GetGrdDate(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 15).Replace(":", ""), Sm.GetGrdStr(Grd1, e.RowIndex, 16).Replace(":", ""), e.RowIndex,
                          BtTm1, BtTm2, BtTm3, BtTm4, Sm.GetGrdBool(Grd1, e.RowIndex, 22));
                }
                SetTime(e.RowIndex, e.ColIndex);
            }
        }

        #endregion

        #region Additional Method

        public void SetTime(int RowXXX, int ColEdit)
        {
            try
            {
                if (Sm.GetGrdStr(Grd1, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(Grd1, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 2)) <= 59)
                {
                    Grd1.Cells[RowXXX, ColEdit].Value = String.Concat(Sm.Left(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 2));
                }
                if (Sm.GetGrdStr(Grd1, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(Grd1, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 2)) <= 59)
                {
                    Grd1.Cells[RowXXX, ColEdit].Value = String.Concat("0", Sm.Left(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(Grd1, RowXXX, ColEdit), 2));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }


        }

        private void Process1()//actual in out
        {
            try
            {
                var lAtd = new List<Atd>();
                ProcessAtdLog(ref lAtd);

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    double mTotalHourBreak = 0;
                    var dtTmTempStart = string.Empty;
                    var dtTmTempEnd = string.Empty;

                    string EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 4);
                    string DteStartDt = Sm.GetGrdDate(Grd1, Row, 10);
                    string LogStart = string.Empty;
                    string LogEnd = string.Empty;

                    string Tm1 = Sm.GetGrdStr(Grd1, Row, 11).Replace(":", "");
                    string Tm2 = Sm.GetGrdStr(Grd1, Row, 12).Replace(":", ""); ;

                    DateTime DtTm1 = new DateTime(
                       Int32.Parse(DteStartDt.Substring(0, 4)),
                       Int32.Parse(DteStartDt.Substring(4, 2)),
                       Int32.Parse(DteStartDt.Substring(6, 2)),
                       Int32.Parse(Sm.Left(Tm1, 2)), Int32.Parse(Sm.Right(Tm1, 2)), 0
                       );

                    DateTime DtTm2 = new DateTime(
                        Int32.Parse(DteStartDt.Substring(0, 4)),
                        Int32.Parse(DteStartDt.Substring(4, 2)),
                        Int32.Parse(DteStartDt.Substring(6, 2)),
                        Int32.Parse(Sm.Left(Tm2, 2)), Int32.Parse(Sm.Right(Tm2, 2)), 0
                        );

                    if (Sm.GetGrdBool(Grd1, Row, 22) && Sm.GetGrdStr(Grd1, Row, 23).Length > 0 && Sm.GetGrdStr(Grd1, Row, 24).Length > 0)
                    {
                        string Tm3 = Sm.GetGrdStr(Grd1, Row, 23).Replace(":", "");
                        string Tm4 = Sm.GetGrdStr(Grd1, Row, 24).Replace(":", ""); ;

                        DateTime DtTm3 = new DateTime(
                           Int32.Parse(DteStartDt.Substring(0, 4)),
                           Int32.Parse(DteStartDt.Substring(4, 2)),
                           Int32.Parse(DteStartDt.Substring(6, 2)),
                           Int32.Parse(Sm.Left(Tm3, 2)), Int32.Parse(Sm.Right(Tm3, 2)), 0
                           );

                        DateTime DtTm4 = new DateTime(
                            Int32.Parse(DteStartDt.Substring(0, 4)),
                            Int32.Parse(DteStartDt.Substring(4, 2)),
                            Int32.Parse(DteStartDt.Substring(6, 2)),
                            Int32.Parse(Sm.Left(Tm4, 2)), Int32.Parse(Sm.Right(Tm4, 2)), 0
                            );
                        mTotalHourBreak = (DtTm4 - DtTm3).TotalMinutes;
                    }

                    if (Sm.GetGrdBool(Grd1, Row, 22) && Sm.GetGrdStr(Grd1, Row, 25).Length > 0 && Sm.GetGrdStr(Grd1, Row, 26).Length > 0)
                    {
                        string Tm5 = Sm.GetGrdStr(Grd1, Row, 25).Replace(":", "");
                        string Tm6 = Sm.GetGrdStr(Grd1, Row, 26).Replace(":", ""); ;

                        DateTime DtTm5 = new DateTime(
                           Int32.Parse(DteStartDt.Substring(0, 4)),
                           Int32.Parse(DteStartDt.Substring(4, 2)),
                           Int32.Parse(DteStartDt.Substring(6, 2)),
                           Int32.Parse(Sm.Left(Tm5, 2)), Int32.Parse(Sm.Right(Tm5, 2)), 0
                           );

                        DateTime DtTm6 = new DateTime(
                            Int32.Parse(DteStartDt.Substring(0, 4)),
                            Int32.Parse(DteStartDt.Substring(4, 2)),
                            Int32.Parse(DteStartDt.Substring(6, 2)),
                            Int32.Parse(Sm.Left(Tm6, 2)), Int32.Parse(Sm.Right(Tm6, 2)), 0
                            );
                        mTotalHourBreak = mTotalHourBreak+((DtTm6 - DtTm5).TotalMinutes);
                    }

                    DateTime Dt1Tolerance = DtTm1.AddMinutes(Convert.ToDouble(mToleranceLeaveOnMinute));
                    DateTime Dt2Tolerance = DtTm2.AddMinutes(Convert.ToDouble(mToleranceLeaveOnMinute));
                    DateTime Dt3Tolerance = DtTm1.AddMinutes(Convert.ToDouble(mDurationForWastedTimeLeave) + mTotalHourBreak);
                    DateTime Dt4Tolerance = DtTm1.AddMinutes(Convert.ToDouble(mToleranceLeaveOnMinute + mDurationForWastedTimeLeave + 5) + mTotalHourBreak);//jika melebihi 4 jam lgsg diset 4 jam plus toleransi plus istirahta

                    foreach (var i in lAtd.Where(Index => Index.EmpCode == EmpCodeTemp && Index.Dt == DteStartDt.Substring(0, 8))
                                        .OrderByDescending(x => x.EmpCode))
                    {
                        DateTime DtLogTm = new DateTime(
                        Int32.Parse(i.Dt.Substring(0, 4)),
                        Int32.Parse(i.Dt.Substring(4, 2)),
                        Int32.Parse(i.Dt.Substring(6, 2)),
                        Int32.Parse(Sm.Left(i.Tm, 2)), Int32.Parse(i.Tm.Substring(2, 2)), 0
                        );

                        //In artinya dia keluar kantor
                        //Out artinya dia balik kantor

                        int result1 = DateTime.Compare(DtLogTm, DtTm1);//bandingin log ama request in
                        int result2 = DateTime.Compare(DtLogTm, Dt1Tolerance);//bandingin log dengan request in+toleranci

                        int result3 = DateTime.Compare(DtLogTm, DtTm2);//bandingin log ama time request out
                        int result4 = DateTime.Compare(DtLogTm, Dt3Tolerance);//bandingin log ama 4 jam+tolorance 

                        //1 = val1>val2
                        //0 = val1=val2
                        //-1 = val1<val2

                        //ceklok lbh besar dari jam request berangkat, dan ceklok kurang dari request+toleransi
                        if (result1 >= 1 && result2 <= 0)
                        {
                            if (dtTmTempStart.Length == 0)
                            {
                                dtTmTempStart = DtLogTm.ToString();
                            }
                            else
                                dtTmTempStart = dtTmTempStart.ToString();
                        }

                        //if (dtTmTempStart.Length == 0)
                        //    dtTmTempStart = DtTm1.ToString();

                        //ceklok lbh besar dari jam req balik, dan ceklok kurang dari request+toleransi
                        if (result3 >= 1 && result4 < 0)
                        {
                            if (dtTmTempEnd.Length == 0)
                            {
                                dtTmTempEnd = DtLogTm.ToString();
                            }
                            else
                                dtTmTempEnd = dtTmTempEnd.ToString();
                        }

                        //if (dtTmTempEnd.Length == 0)
                        //    dtTmTempEnd = Dt4Tolerance.ToString();
                    }

                    if (dtTmTempStart.Length > 0)
                    {
                        Grd1.Cells[Row, 13].Value = 
                            Grd1.Cells[Row, 15].Value = Convert.ToDateTime(dtTmTempStart.ToString()).ToString("HH:mm");
                    }
                    else
                    {
                        dtTmTempStart = DtTm1.ToString();
                        Grd1.Cells[Row, 13].Value = string.Empty;
                            Grd1.Cells[Row, 15].Value = Convert.ToDateTime(dtTmTempStart.ToString()).ToString("HH:mm");
                    }
                    if (dtTmTempEnd.Length > 0)
                    {
                        Grd1.Cells[Row, 14].Value =
                            Grd1.Cells[Row, 16].Value = Convert.ToDateTime(dtTmTempEnd.ToString()).ToString("HH:mm");
                    }
                    else
                    {
                        dtTmTempEnd = Dt4Tolerance.ToString();
                        Grd1.Cells[Row, 14].Value = string.Empty;
                            Grd1.Cells[Row, 16].Value = Convert.ToDateTime(dtTmTempEnd.ToString()).ToString("HH:mm");
                    }
                    if (dtTmTempStart.Length > 0 && dtTmTempEnd.Length > 0)
                    {
                        string BTm1 = string.Empty;
                        string BTm2 = string.Empty;
                        string BTm3 = string.Empty;
                        string BTm4 = string.Empty;

                        if (Sm.GetGrdStr(Grd1, Row, 23).Length > 0)
                            BTm1 = Sm.GetGrdStr(Grd1, Row, 23).Replace(":", "");
                        if (Sm.GetGrdStr(Grd1, Row, 24).Length > 0)
                            BTm2 = Sm.GetGrdStr(Grd1, Row, 24).Replace(":", "");
                        if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                            BTm3 = Sm.GetGrdStr(Grd1, Row, 25).Replace(":", "");
                        if (Sm.GetGrdStr(Grd1, Row, 26).Length > 0)
                            BTm4 = Sm.GetGrdStr(Grd1, Row, 26).Replace(":", "");

                        ComputeHour(DteStartDt, Sm.GetGrdStr(Grd1, Row, 15).Replace(":", ""), Sm.GetGrdStr(Grd1, Row, 16).Replace(":", ""), Row,
                            BTm1, BTm2, BTm3, BTm4, Sm.GetGrdBool(Grd1, Row, 22)
                            );
                    }
                }

                lAtd.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeHour(string ActDt, string ActStartTm, string ActEndTm, int Row, string BTm1, string BTm2, string BTm3, string BTm4, bool BreakInd)
        {
            try
            {
                double TotalHourBreak = 0;

                DateTime Dt1 = new DateTime(
                   Int32.Parse(ActDt.Substring(0, 4)),
                   Int32.Parse(ActDt.Substring(4, 2)),
                   Int32.Parse(ActDt.Substring(6, 2)),
                   Int32.Parse(ActStartTm.Substring(0, 2)),
                   Int32.Parse(ActStartTm.Substring(2, 2)),
                   0
                   );

               
                DateTime Dt2 = new DateTime(
                    Int32.Parse(ActDt.Substring(0, 4)),
                    Int32.Parse(ActDt.Substring(4, 2)),
                    Int32.Parse(ActDt.Substring(6, 2)),
                    Int32.Parse(ActEndTm.Substring(0, 2)),
                    Int32.Parse(ActEndTm.Substring(2, 2)),
                    0
                    );

                if (BreakInd && BTm1.Length>0 && BTm2.Length >0)
                {
                    DateTime Dt3 = new DateTime(
                       Int32.Parse(ActDt.Substring(0, 4)),
                       Int32.Parse(ActDt.Substring(4, 2)),
                       Int32.Parse(ActDt.Substring(6, 2)),
                       Int32.Parse(BTm1.Substring(0, 2)),
                       Int32.Parse(BTm1.Substring(2, 2)),
                       0
                       );

                    DateTime Dt4 = new DateTime(
                        Int32.Parse(ActDt.Substring(0, 4)),
                        Int32.Parse(ActDt.Substring(4, 2)),
                        Int32.Parse(ActDt.Substring(6, 2)),
                        Int32.Parse(BTm2.Substring(0, 2)),
                        Int32.Parse(BTm2.Substring(2, 2)),
                        0
                        );
                    int CompareActualBreak1 = DateTime.Compare(Dt2, Dt3);
                    if (CompareActualBreak1 >= 1)
                    TotalHourBreak  = (Dt4 - Dt3).TotalMinutes;
                }

                if (BreakInd && BTm3.Length > 0 && BTm4.Length > 0)
                {
                    DateTime Dt5 = new DateTime(
                       Int32.Parse(ActDt.Substring(0, 4)),
                       Int32.Parse(ActDt.Substring(4, 2)),
                       Int32.Parse(ActDt.Substring(6, 2)),
                       Int32.Parse(BTm3.Substring(0, 2)),
                       Int32.Parse(BTm3.Substring(2, 2)),
                       0
                       );

                    DateTime Dt6 = new DateTime(
                        Int32.Parse(ActDt.Substring(0, 4)),
                        Int32.Parse(ActDt.Substring(4, 2)),
                        Int32.Parse(ActDt.Substring(6, 2)),
                        Int32.Parse(BTm4.Substring(0, 2)),
                        Int32.Parse(BTm4.Substring(2, 2)),
                        0
                        );

                    int CompareActualBreak2 = DateTime.Compare(Dt2, Dt5);
                    if (CompareActualBreak2 >= 1)
                        TotalHourBreak = TotalHourBreak+((Dt6 - Dt5).TotalMinutes);
                }

                double TotalHours = (Dt2 - Dt1).TotalMinutes;

                if (TotalHours < 0) TotalHours = 0;

                if (TotalHourBreak > 0)
                    TotalHours = TotalHours - TotalHourBreak; 

                double hours = Math.Truncate(TotalHours / 60);
                double minutes = TotalHours % 60;
                var time = string.Format("{0} : {1}", (hours.ToString().Length == 1 ? string.Concat("0", hours.ToString()) : hours.ToString()), (minutes.ToString().Length == 1 ? string.Concat("0", minutes.ToString()) : minutes.ToString()));
                Grd1.Cells[Row, 17].Value = time;
                if (TotalHours >= Convert.ToDouble(mDurationForWastedTimeLeave))
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) > 0)
                    {
                        Grd1.Cells[Row, 19].Value = Sm.GetValue("Select Leavename From tblLeave Where leaveCode = '" + mAnnualLeaveCode + "' ");
                        Grd1.Cells[Row, 27].Value = mAnnualLeaveCode;
                    }
                    else
                    {
                        Grd1.Cells[Row, 19].Value = Sm.GetValue("Select Leavename From tblLeave Where leaveCode = '" + mWastedTimeLeaveCode + "' ");
                        Grd1.Cells[Row, 27].Value = mWastedTimeLeaveCode;
                    }
                }
                else
                {
                    Grd1.Cells[Row, 19].Value = Sm.GetValue("Select Leavename From tblLeave Where leaveCode = '" + mLeaveCodePermisionPersonal + "' ");
                    Grd1.Cells[Row, 27].Value = null;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

       
        private void ProcessAtdLog(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 4);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }


            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Where left(A.Dt, 6) = @YrMth  ");
            SQL.AppendLine(Filter);

            Sm.CmParam<String>(ref cm, "@YrMth", String.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + "order by dt, tm Asc ;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "Tm"                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Tm = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private bool IsGrdTimeNotValid(string ActStartTm)
        {
            string Msg = "";

            if (ActStartTm.Length != 0)
            {
                if (Decimal.Parse(ActStartTm.Replace(":", "")) > 2359)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Actual Time not Valid");
                    return true;
                }
            }
            return false;
        }     


        #endregion

        #endregion

        #region Class

        private class Result1
        {
           public string Document  { get; set; }
           public string EmpCode  { get; set; }
           public string EmpCodeOld  { get; set; }
           public string EmpName  { get; set; }

            //6-10
           public string DeptName  { get; set; }
           public string PosName  { get; set; }
           public string SiteName  { get; set; }
           public string StartLeaveDt  { get; set; }
           public string ReqStartTm  { get; set; }

            //11-13
           public string ReqEndTm  { get; set; }
           public string LogStartTm  { get; set; }
           public string LogEndTm  { get; set; }
           public string ActualStartTm  { get; set; }
           public string ActualEndTm  { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        #endregion

       
    }
}
