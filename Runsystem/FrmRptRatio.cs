﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRatio : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mRatioAcNoAsset = string.Empty,
            mRatioAcNoReturnOnAsset1 = string.Empty,
            mRatioAcNoReturnOnAsset2 = string.Empty,
            mRatioAcNoReturnOnAsset3 = string.Empty,
            mRatioAcNoReturnOnAsset4 = string.Empty,
            mRatioAcNoReturnOnAsset5 = string.Empty,
            mRatioAcNoCurrentRatio1 = string.Empty,
            mRatioAcNoCurrentRatio2 = string.Empty;

        private bool mIsReportingFilterByEntity = false;

        #endregion

        #region Constructor

        public FrmRptRatio(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Nama Rasio",
                    "Formula 1",
                    "Nilai 1",
                    "Formula 2",
                    "Nilai 2",

                    //6-7
                    "Rumus",
                    "Hasil",
                },
                new int[] 
                {
                    30,
                    200, 180, 180, 180, 180, 
                    350, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 5, 7 }, 2);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                PrepareColumn();
                InsertFormulaValue();
                ComputeFinalValue();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mRatioAcNoAsset = Sm.GetParameter("RatioAcNoAsset");
            mRatioAcNoReturnOnAsset1 = Sm.GetParameter("RatioAcNoReturnOnAsset1");
            mRatioAcNoReturnOnAsset2 = Sm.GetParameter("RatioAcNoReturnOnAsset2");
            mRatioAcNoReturnOnAsset3 = Sm.GetParameter("RatioAcNoReturnOnAsset3");
            mRatioAcNoReturnOnAsset4 = Sm.GetParameter("RatioAcNoReturnOnAsset4");
            mRatioAcNoReturnOnAsset5 = Sm.GetParameter("RatioAcNoReturnOnAsset5");
            mRatioAcNoCurrentRatio1 = Sm.GetParameter("RatioAcNoCurrentRatio1");
            mRatioAcNoCurrentRatio2 = Sm.GetParameter("RatioAcNoCurrentRatio2");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
        }

        private void PrepareColumn()
        {
            int mTotalCols = 8;
            string[] mNamaRasio = { 
                                      "Sale To Average Asset", 
                                      "Average Asset to Average Equity", "EBITDA Margin", "Net Profit Margin", 
                                      "Return On Equity", "Return On Asset", "Market Share Growth", "Current Ratio" 
                                  };
            string[] mFormula1 = { 
                                      "Pendapatan", 
                                      "Rata-Rata Asset", "Laba Operasi", "Laba Bersih", 
                                      "Laba Setelah Pajak", "Laba Setelah Pajak", "Pengunjung Wisman", "Total Aktiva Lancar" 
                                  };

            string[] mFormula2 = { 
                                      "Rata-Rata Asset", 
                                      "Kenaikan Modal", "Pendapatan", "Pendapatan", 
                                      "Total Asset", "Total Asset", "Total Wisman Ke Indonesia", "Total Hutang Lancar" 
                                  };
            Grd1.Rows.Count = mTotalCols;
            for (int i = 0; i < mTotalCols; i++)
            {
                Grd1.Cells[i, 0].Value = string.Concat((i + 1).ToString(), ".");
                Grd1.Cells[i, 1].Value = mNamaRasio[i];
                Grd1.Cells[i, 2].Value = mFormula1[i];
                Grd1.Cells[i, 4].Value = mFormula2[i];
                Grd1.Cells[i, 6].Value = string.Format("( {0} / {1} ){2}", mFormula1[i], mFormula2[i], " x 100%");
                Grd1.Cells[i, 3].Value = Grd1.Cells[i, 5].Value = Grd1.Cells[i, 7].Value = 0m;
            }
        }

        private decimal ComputeFormula(string AcNo, string DocDt)
        {
            decimal mFormulaValue = 0m;
            string mDocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string mDocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            string mStartFrom = Sm.GetLue(LueStartFrom);
            decimal mOB = 0m, mOBDb = 0m, mOBCr = 0m, mTotDebit = 0m, mTotCredit = 0m;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();
            var SQL3 = new StringBuilder();
            var cm3 = new MySqlCommand();

            #region nilai Opening Balance

            #region dari TblCOAOpeningBalanceDtl

            SQL.AppendLine("Select IfNull(B.Amt, 0) Amt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo = @AcNo ");

            if (mStartFrom.Length > 0)
                SQL.AppendLine("    And A.Yr = @StartFrom ");
            else
            {
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.DocDt < @DocDt2 ");
            }
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParam<String>(ref cm, "@DocDt1", mDocDt1);
            Sm.CmParam<String>(ref cm, "@DocDt2", mDocDt2);
            if (mStartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", mStartFrom);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mOB = dr.GetDecimal(0);
                    }
                }
                dr.Close();
            }

            #endregion

            #region dari TblJournalDtl

            SQL2.AppendLine("Select IfNull(Sum(T.DAmt), 0) DAmt, IfNull(Sum(T.CAmt), 0) CAmt ");
            SQL2.AppendLine("From ( ");
            SQL2.AppendLine("    Select IfNull(B.DAmt, 0) DAmt, IfNull(B.CAmt, 0) CAmt ");
            SQL2.AppendLine("    From TblJournalHdr A ");
            SQL2.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mStartFrom.Length <= 0)
            {
                SQL2.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL2.AppendLine("        And A.DocDt < @DocDt1 ");
            }
            else
            {
                SQL2.AppendLine("        And Left(A.DocDt, 6) >= @StartFrom ");
                SQL2.AppendLine("        And A.DocDt < @DocDt1 ");
            }
            SQL2.AppendLine("        And B.AcNo Like '" + AcNo + "%' ");
            SQL2.AppendLine("    Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL2.AppendLine(") T; ");

            Sm.CmParam<String>(ref cm2, "@Yr", Sm.Left(mDocDt1, 4));
            Sm.CmParam<String>(ref cm2, "@DocDt1", mDocDt1);
            if (mStartFrom.Length > 0)
                Sm.CmParam<String>(ref cm2, "@StartFrom", string.Concat(mStartFrom, "01"));
            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandTimeout = 600;
                cm2.CommandText = SQL2.ToString();
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] { "DAmt", "CAMt" });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        mOBDb = dr2.GetDecimal(0);
                        mOBCr = dr2.GetDecimal(1);
                    }
                }
                dr2.Close();
            }

            #endregion

            if (Sm.Left(AcNo, 1) == "2" || Sm.Left(AcNo, 1) == "3")
                mOB = mOB + mOBCr - mOBDb;
            else
                mOB = mOB + mOBDb - mOBCr;

            #endregion

            #region Total Debit Kredit periode berjalan

            SQL3.AppendLine("Select IfNull(Sum(T.DAmt), 0) DAmt, IfNull(Sum(T.CAmt), 0) CAmt ");
            SQL3.AppendLine("From ( ");
            SQL3.AppendLine("    Select IfNull(B.DAmt, 0) DAmt, IfNull(B.CAmt, 0) CAmt ");
            SQL3.AppendLine("    From TblJournalHdr A ");
            SQL3.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mStartFrom.Length <= 0)
            {
                SQL3.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                if(DocDt.Length > 0)
                    SQL3.AppendLine("        And A.DocDt = @DocDt1 ");
                else
                    SQL3.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            }
            else
            {
                SQL3.AppendLine("        And Left(A.DocDt, 6) >= @StartFrom ");
                if (DocDt.Length > 0)
                    SQL3.AppendLine("        And A.DocDt = @DocDt1 ");
                else
                    SQL3.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            }
            SQL3.AppendLine("        And B.AcNo Like '" + AcNo + "%' ");
            SQL3.AppendLine("    Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL3.AppendLine(") T; ");

            Sm.CmParam<String>(ref cm3, "@Yr", Sm.Left(mDocDt1, 4));
            Sm.CmParam<String>(ref cm3, "@DocDt1", mDocDt1);
            Sm.CmParam<String>(ref cm3, "@DocDt2", mDocDt2);
            if (mStartFrom.Length > 0)
                Sm.CmParam<String>(ref cm3, "@StartFrom", string.Concat(mStartFrom, "01"));
            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandTimeout = 600;
                cm3.CommandText = SQL3.ToString();
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] { "DAmt", "CAMt" });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        mTotDebit = dr3.GetDecimal(0);
                        mTotCredit = dr3.GetDecimal(1);
                    }
                }
                dr3.Close();
            }

            #endregion

            if (Sm.Left(AcNo, 1) == "2" || Sm.Left(AcNo, 1) == "3")
                mFormulaValue = mOB + mTotCredit - mTotDebit;
            else
                mFormulaValue = mOB + mTotDebit - mTotCredit;

            return mFormulaValue;
        }

        private void InsertFormulaValue()
        {
            // variabel Formula[baris][formula ke-]
            decimal
                mF11 = 0m, mF12 = 0m, mF21 = 0m, mF22 = 0m, mF31 = 0m, mF32 = 0m, mF41 = 0m, mF42 = 0m,
                mF51 = 0m, mF52 = 0m, mF61 = 0m, mF62 = 0m, mF71 = 0m, mF72 = 0m, mF81 = 0m, mF82 = 0m;
            mF11 = ComputeFormula("4", string.Empty);
            mF12 = (ComputeFormula(mRatioAcNoAsset, Sm.Left(Sm.GetDte(DteDocDt1), 8)) + ComputeFormula(mRatioAcNoAsset, string.Empty)) / 2;
            mF21 = (ComputeFormula(mRatioAcNoAsset, Sm.Left(Sm.GetDte(DteDocDt1), 8)) + ComputeFormula(mRatioAcNoAsset, string.Empty)) / 2;
            mF22 = (ComputeFormula("3", string.Empty) - ComputeFormula("3", Sm.Left(Sm.GetDte(DteDocDt1), 8)));
            mF31 = (ComputeFormula("4", string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset1, string.Empty));
            mF32 = ComputeFormula("4", string.Empty);
            mF41 = (ComputeFormula("4", string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset1, string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset2, string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset3, string.Empty) + ComputeFormula(mRatioAcNoReturnOnAsset4, string.Empty));
            mF42 = ComputeFormula("4", string.Empty);
            mF51 = (ComputeFormula("4", string.Empty) - ComputeFormula("5", string.Empty));
            mF52 = ComputeFormula("1", string.Empty);
            mF61 = (ComputeFormula("4", string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset1, string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset2, string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset3, string.Empty) + ComputeFormula(mRatioAcNoReturnOnAsset4, string.Empty) - ComputeFormula(mRatioAcNoReturnOnAsset5, string.Empty));
            mF62 = ComputeFormula(mRatioAcNoAsset, string.Empty);

            //wisman dari master Foreign Visitors
            mF71 = GetForeignVisitors();
            mF72 = GetTotalForeignVisitors();

            mF81 = ComputeFormula(mRatioAcNoCurrentRatio1, string.Empty);
            mF82 = ComputeFormula(mRatioAcNoCurrentRatio2, string.Empty);

            Grd1.Cells[0, 3].Value = mF11;
            Grd1.Cells[0, 5].Value = mF12;
            Grd1.Cells[1, 3].Value = mF21;
            Grd1.Cells[1, 5].Value = mF22;
            Grd1.Cells[2, 3].Value = mF31;
            Grd1.Cells[2, 5].Value = mF32;
            Grd1.Cells[3, 3].Value = mF41;
            Grd1.Cells[3, 5].Value = mF42;
            Grd1.Cells[4, 3].Value = mF51;
            Grd1.Cells[4, 5].Value = mF52;
            Grd1.Cells[5, 3].Value = mF61;
            Grd1.Cells[5, 5].Value = mF62;
            Grd1.Cells[6, 3].Value = mF71;
            Grd1.Cells[6, 5].Value = mF72;
            Grd1.Cells[7, 3].Value = mF81;
            Grd1.Cells[7, 5].Value = mF82;
        }

        private void ComputeFinalValue()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 5) != 0 && Sm.GetGrdDec(Grd1, Row, 3) != 0)
                    Grd1.Cells[Row, 7].Value = (Sm.GetGrdDec(Grd1, Row, 3) / Sm.GetGrdDec(Grd1, Row, 5)) * 100m;
            }
        }

        private decimal GetForeignVisitors()
        {
            decimal mFV = 0m;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(ForeignVisitors, 0) ForeignVisitors ");
            SQL.AppendLine("From TblForeignVisitors ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By DocNo Desc Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString()))
            {
                mFV = Decimal.Parse(Sm.GetValue(SQL.ToString()));
            }

            return mFV;
        }

        private decimal GetTotalForeignVisitors()
        {
            decimal mTFV = 0m;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(TotalForeignVisitors, 0) ForeignVisitors ");
            SQL.AppendLine("From TblForeignVisitors ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By DocNo Desc Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString()))
            {
                mTFV = Decimal.Parse(Sm.GetValue(SQL.ToString()));
            }

            return mTFV;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("X", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo("X", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion
    }
}
