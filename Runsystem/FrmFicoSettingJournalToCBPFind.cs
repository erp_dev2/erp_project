﻿#region Update
/*
    11/02/2020 [WED/YK] new apps
    12/04/2020 [VIN] bug nama kolom
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmFicoSettingJournalToCBPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmFicoSettingJournalToCBP mFrmParent;

        #endregion

        #region Constructor

        public FrmFicoSettingJournalToCBPFind(FrmFicoSettingJournalToCBP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);

                if (!mFrmParent.mMenuCodeForCustomProfitLoss)
                {
                    LblSiteCode.Visible = LueSiteCode.Visible = ChkSiteCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Type", 
                    "Code",
                    "Active",
                    "Sequence",
                    "Site",
                    
                    //6-10
                    "Description 1",
                    "Description 2",
                    "Description 3",
                    "Description 4",
                    "Created"+Environment.NewLine+"By", 
                    
                    //11-15
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By",
                    "Last"+Environment.NewLine+"Updated Date",
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 100, 60, 100, 180, 
                    
                    //6-10
                    200, 200, 200, 200, 100, 

                    //11-15
                    100, 100, 100, 100, 100
                }
            );

            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 10, 11, 12, 13, 14, 15 }, false);
            if (mFrmParent.mMenuCodeForCustomCashFlow) Sm.GrdColInvisible(Grd1, new int[] { 9 });
            if (mFrmParent.mMenuCodeForCustomEquity) Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9 });
            if (mFrmParent.mMenuCodeForCustomProfitLoss) Sm.GrdColInvisible(Grd1, new int[] { 8, 9 });
            if (!mFrmParent.mMenuCodeForCustomProfitLoss) Sm.GrdColInvisible(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DocType, A.Code, A.ActInd, A.Sequence, A.Description1, A.Description2, A.Description3, A.Description4, ");
                if (mFrmParent.mMenuCodeForCustomProfitLoss)
                    SQL.AppendLine("B.SiteName, ");
                else
                    SQL.AppendLine("null As SiteName, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblFicoSettingJournalToCBPHdr A ");
                if(mFrmParent.mMenuCodeForCustomProfitLoss)
                    SQL.AppendLine("Left Join TblSite B On A.SiteCode = B.SiteCode ");
                SQL.AppendLine("Where A.DocType = @DocType ");

                if (mFrmParent.mMenuCodeForCustomBalanceSheet)
                    Sm.CmParam<String>(ref cm, "@DocType", "BalanceSheet");
                if (mFrmParent.mMenuCodeForCustomProfitLoss)
                    Sm.CmParam<String>(ref cm, "@DocType", "ProfitLoss");
                if (mFrmParent.mMenuCodeForCustomCashFlow)
                    Sm.CmParam<String>(ref cm, "@DocType", "CashFlow");
                if (mFrmParent.mMenuCodeForCustomEquity)
                    Sm.CmParam<String>(ref cm, "@DocType", "Equity");

                Sm.FilterStr(ref Filter, ref cm, TxtDescription.Text, new string[] { "A.Description1", "A.Description2", "A.Description3", "A.Description4" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);  

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString() + Filter,
                    new string[]
                    {
                        //0
                        "DocType",

                        //1-5
                        "Code",
                        "ActInd",
                        "Sequence",
                        "SiteName",
                        "Description1",
                        
                        //6-10
                        "Description2",
                        "Description3",
                        "Description4", 
                        "CreateBy",
                        "CreateDt",

                        //11-12
                        "LastUpBy",
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDescription_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDescription_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
