﻿namespace RunSystem
{
    partial class FrmAssesment2Find
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAsmName = new DevExpress.XtraEditors.TextEdit();
            this.ChkAsmName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkCompetenceType = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCompetenceType = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAsmName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompetenceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkCompetenceType);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCompetenceType);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtAsmName);
            this.panel2.Controls.Add(this.ChkAsmName);
            this.panel2.Size = new System.Drawing.Size(672, 60);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 413);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "Assesment";
            // 
            // TxtAsmName
            // 
            this.TxtAsmName.EnterMoveNextControl = true;
            this.TxtAsmName.Location = new System.Drawing.Point(78, 8);
            this.TxtAsmName.Name = "TxtAsmName";
            this.TxtAsmName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsmName.Properties.Appearance.Options.UseFont = true;
            this.TxtAsmName.Properties.MaxLength = 40;
            this.TxtAsmName.Size = new System.Drawing.Size(251, 20);
            this.TxtAsmName.TabIndex = 13;
            this.TxtAsmName.Validated += new System.EventHandler(this.TxtAsmName_Validated);
            // 
            // ChkAsmName
            // 
            this.ChkAsmName.Location = new System.Drawing.Point(337, 7);
            this.ChkAsmName.Name = "ChkAsmName";
            this.ChkAsmName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAsmName.Properties.Appearance.Options.UseFont = true;
            this.ChkAsmName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAsmName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAsmName.Properties.Caption = " ";
            this.ChkAsmName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAsmName.Size = new System.Drawing.Size(31, 22);
            this.ChkAsmName.TabIndex = 14;
            this.ChkAsmName.ToolTip = "Remove filter";
            this.ChkAsmName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAsmName.ToolTipTitle = "Run System";
            this.ChkAsmName.CheckedChanged += new System.EventHandler(this.ChkAsmName_CheckedChanged);
            // 
            // ChkCompetenceType
            // 
            this.ChkCompetenceType.Location = new System.Drawing.Point(337, 30);
            this.ChkCompetenceType.Name = "ChkCompetenceType";
            this.ChkCompetenceType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCompetenceType.Properties.Appearance.Options.UseFont = true;
            this.ChkCompetenceType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCompetenceType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCompetenceType.Properties.Caption = " ";
            this.ChkCompetenceType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCompetenceType.Size = new System.Drawing.Size(19, 22);
            this.ChkCompetenceType.TabIndex = 26;
            this.ChkCompetenceType.ToolTip = "Remove filter";
            this.ChkCompetenceType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCompetenceType.ToolTipTitle = "Run System";
            this.ChkCompetenceType.CheckedChanged += new System.EventHandler(this.ChkCompetenceType_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(36, 33);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // LueCompetenceType
            // 
            this.LueCompetenceType.EnterMoveNextControl = true;
            this.LueCompetenceType.Location = new System.Drawing.Point(78, 30);
            this.LueCompetenceType.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceType.Name = "LueCompetenceType";
            this.LueCompetenceType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceType.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceType.Properties.DropDownRows = 20;
            this.LueCompetenceType.Properties.NullText = "[Empty]";
            this.LueCompetenceType.Properties.PopupWidth = 500;
            this.LueCompetenceType.Size = new System.Drawing.Size(251, 20);
            this.LueCompetenceType.TabIndex = 25;
            this.LueCompetenceType.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceType.EditValueChanged += new System.EventHandler(this.LueCompetenceType_EditValueChanged_1);
            // 
            // FrmAssesment2Find
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmAssesment2Find";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAsmName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompetenceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtAsmName;
        private DevExpress.XtraEditors.CheckEdit ChkAsmName;
        private DevExpress.XtraEditors.CheckEdit ChkCompetenceType;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceType;
    }
}