﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmInvoiceDocReceiptsFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvoiceDocReceipts mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInvoiceDocReceiptsFind(FrmInvoiceDocReceipts FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            Sl.SetLueVdCode(ref LueVdCode);

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, D.VdName,");
            SQL.AppendLine("B.PurchaseInvoiceDocNo, C.DocDt As PurchaseInvoiceDocDt, ");
            SQL.AppendLine("C.VdInvNo, C.VdInvDt, C.Amt, ");
            SQL.AppendLine("C.TaxInvoiceNo, (C.Amt+C.TaxAmt) As AmtWithTax, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt,A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblInvoiceDocReceiptsHdr A ");
            SQL.AppendLine("inner Join TblInvoiceDocReceiptsDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr C On B.PurchaseInvoiceDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblVendor D On A.VdCode = D.VdCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document Number", 
                    "Document"+Environment.NewLine+"Date",
                    "Cancel",
                    "Vendor",
                    "Purchase"+Environment.NewLine+"Invoice Number",

                    //6-10
                    "",
                    "Purchase Invoice"+Environment.NewLine+"Invoice Date",
                    "Vendor's"+Environment.NewLine+"Invoice Number",
                    "Vendor's"+Environment.NewLine+"Invoice Date",
                    "Amount"+Environment.NewLine+"Without Tax",
                    
                    //11-15
                    "Tax"+Environment.NewLine+"Invoice Number",
                    "Amount"+Environment.NewLine+"With Tax",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    
                    //16-18
                    "Last"+Environment.NewLine+"Updated By",
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                   }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 7, 9, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 13, 14, 15, 16, 17, 18 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, B.DNo",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "VdName", "PurchaseInvoiceDocNo", "PurchaseInvoiceDocDt", 

                            //6-10
                            "VdInvNo", "VdInvDt", "Amt", "TaxInvoiceNo", "AmtWithTax",  
                            
                            //11-14
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 18, 14);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
