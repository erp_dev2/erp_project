﻿#region Update
/*
    21/06/2019 [WED] new apps
    27/06/2019 [WED] PRJI ambil dari RBP. Mth dan Yr harus diisi dulu baru bisa pilih detail Project maupun Department
    28/06/2019 [WED] Dropping Request Direct Cost tidak boleh 0
    28/06/2019 [WED] Approval Dropping Request
    01/07/2019 [WED] Amt di Header tidak boleh 0. Dropping Request atau Remuneration Cost tidak boleh 0
    01/07/2019 [WED] Amount di Department otomatis diisi dari amount budget request nya
    08/07/2019 [WED] BUG Budget Category yg muncul masih belom sesuai dengan Year dan Month nya
    09/07/2019 [WED] BUG saat cancel dokumen yg fulfilled karena Direct Cost nya 0 semua
    09/07/2019 [WED] Feedback : Remark Budget Category juga inline dari Budget Request
    15/07/2019 [WED] tambah item dan qty untuk Dropping Request Department
    15/07/2019 [WED] tambah vendor dan bank account#
    15/07/2019 [WED] Budget Category yang Budget Request Amount nya <= 0, tidak dimunculkan
    18/07/2019 [WED] tambah qty untuk DroppingRequestDtl
    08/08/2019 [TKG] saat memilih project implementation, month dan year harus diisi. kalau month danyear diubah, project implementation dan datanya harus dikosongkan.
    22/08/2019 [WED] ubah link ke SO Contract Revision
    02/09/2019 [WED] approval nya cek sampai ke site LOP
    11/10/2019 [WED/YK] cancel nya ada bug
    11/10/2019 [WED/YK] approval masih salah
    05/12/2019 [WED/VIR] tidak boleh cancel kalau ada MR aktif
    28/01/2020 [WED/VIR] remuneration bisa di dropping payment, berdasarkan parameter IsDroppingRequestRemunerationProceedToPayment
    18/02/2020 [HAR/VIR] amount budget ambil dari budget maintenance sblmnya ambil dari budget request
    03/03/2020 [IBL/VIR] Tambah field PIC di dropping request untuk pic non LOP --> ambil dari employee
    09/03/2020 [DITA/VIR] print out
    19/03/2020 [HAR/VIR] printout RKAP ambil dari budget request
    01/04/2020 [VIN/VIR] bug printout sd yang lalu
    13/04/2020 [WED/YK]  Dropping Request menyimpan amount draft akhir dan final (savedroppingrequestHdr & EditDroppingRequesthdr)
    13/04/2020 [HAR/VIR] printout, nilainya tidak sama dengan Budget Maintenance dan validasi jika budget category sdh dipakai, dia hanya bisa memakai sesuai outstanding amount  
    17/04/2020 [WED/VIR] tambah grouping di print out, perbaikan nilai RAP print out, perbaikan site di print out
    30/04/2020 [WED/VIR] penyesuaian print out
    05/05/2020 [WED/VIR] total amount di kali dengan qty, berdasarkan parameter IsDroppingRequestDeptUseQtyCalculation
    06/05/2020 [WED/VIR] validasi limit di department masih bermasalah saat kena parameter IsDroppingRequestUseQtyCalculation
    12/05/2020 [DITA/SIER] Pengaturan Budget berdasarkan tahun dengan parametr baru mIsBudget2YearlyFormat
    01/07/2020 [DITA/YK] untuk dropping request pada bulan dan tahun tertentu yg belum di voucher kan, tidak bisa bikin dropping request pada bulan dan tahun tersebut param --> IsDroppingRequestProcessedByMonthAndYear
    01/07/2020 [DITA/YK] RALAT --> jadinya dropping request untuk bulan dan tahun sebelumnya dengan department/site yg sama dan directcost != 0 jika belum divoucherkan maka tidak bisa save
    06/07/2020 [DITA/YK] saat show data detail yg PRJI RBPRemunerationAmt dan RBPDirectCostAmt ambil dari total yg sudah dijumlahkan dengan ssemployee, tax, dll
    07/07/2020 [DITA/YK] saat show data detail RBPDirectCostAmt masih bug ambil dari RemunerationAmt
    22/12/2021 [TYO/YK] Menambahkan field "Approval's Remark" berdasarkan param IsPSModuleShowApproverRemarkInfo
    22/03/2022 [VIN/YK] BUG show hdr belum di group by
    18/07/2022 [TYO/PRODUCT] Menambah Tab Control Approval Information
    20/09/2022 [RDA/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (RCV)
    27/09/2022 [MAU/VIR] Memunculkan Item Code,  local code dan code budget category pada menu Dropping Request dan fitur hide 
    28/09/2022 [BRI/VIR] penarikan dropping request dalam bulan yang sama berdasarkan param IsDroppingRequestWithdraw
    28/09/2022 [BRI/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (MR)
    03/10/2022 [BRI/VIR] menarik prji di bulan dan tahun yang sama berdasarkan param IsDroppingRequestWithdraw
    10/10/2022 [ICA/VIR] Unhide kolom budget category code, item's code, local code ketika uncheck hide
    03/11/2022 [HPH/VIR] Membuat Validasi dimana Dropping Request kedua hanya bisa dibuat ketika Dropping Request pertama pada field process sudah final pada PRJI, bulan dan tahun yang sama  dengan param IsDroppingRequestProcessDraft
    04/11/2022 [HPH/VIR] Merubah source parameter IsDroppingRequestPRJIMultipleDraftEnabled
    04/01/2023 [BRI/MNET] tambah type berdasarkan param IsDroppingRequestUseType
    04/01/2023 [BRI/MNET] format upload di detail berdasarkan param IsDroppingReqAllowToUploadFile & IsDroppingReqUploadFileMandatory
    17/01/2023 [BRI/MNET] Bug : validasi insert
    25/01/2023 [BRI/MNET] Bug : insert upload file
    31/01/2023 [BRI/MNET] bug button download
    06/02/2023 [RDA/MNET] tambah parameter IsFindMRinformationDroppingRequestActive untuk frm find
    23/02/2023 [MYA/MNET] Menambahkan validasi dropping request tidak dapat di cancel ketika sudah terpakai di transaksi untuk realisasi
    27/02/2023 [RDA/MNET] tambah field usage date dan purpose berdasarkan parameter IsDroppingRequestUseUsageDate dan IsDroppingRequestUsePurpose
    28/02/2023 [RDA/MNET] tambah printout dropping request MNET
    26/04/2023 [BRI/YK] Bug edit data
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mListItCtCodeForResource = string.Empty,
            mStateInd = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;
        internal FrmDroppingRequestFind FrmFind;
        private string mBankAccountFormat = string.Empty;
        private bool
            mIsDroppingRequestForProjectCheckMRExists = false,
            mIsDroppingRequestRemunerationProceedToPayment = false,
            mIsDroppingRequestDeptUseQtyCalculation = false,
            mIsBudget2YearlyFormat = false,
            mIsDroppingRequestProcessedByMonthAndYear = false,
            mIsPSModuleShowApproverRemarkInfo = false;

        internal bool
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsDroppingRequestUseMRReceivingPartial = false,
            mIsDroppingRequestWithdraw = false,
            mIsDroppingRequestPRJIMultipleDraftEnabled = false,
            mIsDroppingRequestUseType = false,
            mIsDroppingReqAllowToUploadFile = false,
            mIsDroppingReqUploadFileMandatory = false,
            mIsFindMRinformationDroppingRequestActive = false,
            mIsDroppingRequestUseUsageDate = false,
            mIsDroppingRequestUsePurpose = false
            ;
        private int mGrd4 = 0;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmDroppingRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmDroppingRequest");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                TcDroppingRequest.SelectTab("TpUploadFile");
                SetGrd4();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");
                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueBankAcCode(ref LueBankAcCode2, string.Empty);
                Sl.SetLueOption(ref LueDocType, "DroppingRequestDocType");
                TxtDocType.Visible = LueDocType.Visible = mIsDroppingRequestUseType;
                if (mIsDroppingRequestUseType)
                {
                    label7.ForeColor = label9.ForeColor = Color.Black;
                }

                TcDroppingRequest.SelectTab("TpgDept");
                SetLueBCCode(ref LueBCCode, string.Empty, string.Empty);
                Sl.SetLueVdCode2(ref LueVdCode2, string.Empty);
                LueVdCode2.Visible = LueVdBankAcDNo.Visible = LueBCCode.Visible = false;
                SetGrd2();
                SetGrd3();

                if (!mIsDroppingReqAllowToUploadFile)
                    TcDroppingRequest.TabPages.Remove(TpUploadFile);

                TcDroppingRequest.SelectTab("TpgPRJI");
                Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                LueVdCode.Visible = LueVdBankAcDNo2.Visible = false;
                SetGrd();

                SetFormControl(mState.View);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                if (!mIsPSModuleShowApproverRemarkInfo)
                {
                    int ypoint = 21;
                    LblApprovalRemark.Visible = MeeApprovalRemark.Visible = false;
                    label11.Top = label11.Top - ypoint;
                    LueProcessInd.Top = LueProcessInd.Top - ypoint;
                    label13.Top = label13.Top - ypoint;
                    MeeCancelReason.Top = MeeCancelReason.Top - ypoint;
                    ChkCancelInd.Top = ChkCancelInd.Top - ypoint;
                    label4.Top = label4.Top - ypoint;
                    TxtPRJIDocNo.Top = TxtPRJIDocNo.Top - ypoint;
                    BtnPRJIDocNo.Top = BtnPRJIDocNo.Top - ypoint;
                    BtnPRJIDocNo2.Top = BtnPRJIDocNo2.Top - ypoint;
                    label3.Top = label3.Top - ypoint;
                    LueDeptCode.Top = LueDeptCode.Top - ypoint;
                    TxtDocType.Top = TxtDocType.Top - ypoint;
                    LueDocType.Top = LueDocType.Top - ypoint;
                    Grd1.Height = Grd1.Height + ypoint;
                }
                if (mIsDroppingRequestUseType)
                {
                    int ypoint = 42;
                    label7.Visible = label9.Visible = false;
                    LueBankAcCode.Visible = LueBankAcCode2.Visible = false;
                    label8.Top = label8.Top - ypoint;
                    label28.Top = label28.Top - ypoint;
                    label14.Top = label14.Top - ypoint;
                    label12.Top = label12.Top - ypoint;
                    TxtAmt.Top = TxtAmt.Top - ypoint;
                    MeeRemark.Top = MeeRemark.Top - ypoint;
                    TxtPICCode.Top = TxtPICCode.Top - ypoint;
                    TxtPICName.Top = TxtPICName.Top - ypoint;
                    BtnPICCode.Top = BtnPICCode.Top - ypoint;
                }

                DteUsageDate.Visible = label15.Visible = mIsDroppingRequestUseUsageDate ? true : false;
                MeePurpose.Visible = label16.Visible = mIsDroppingRequestUsePurpose ? true : false;
                 
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd2.Font = new Font(
                    Grd2.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd3.Font = new Font(
                    Grd3.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd4.Font = new Font(
                    Grd4.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                //Sm.SetGrdAutoSize(Grd1);
                //Sm.SetGrdAutoSize(Grd2);
                //Sm.SetGrdAutoSize(Grd3);
            }
        }

        #endregion

        #region Standard Method

        #region Grid 1 - Project

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "",
                    //1-5
                    "PRBPDNo"+Environment.NewLine+"Check", "ResourceItCode", "Resource", "VdCode", "Vendor",
                    //6-10
                    "Month", "Year", "Remuneration", "Direct Cost", "Remark",
                    //11-15
                    "", "MR#", "RBP Remuneration", "RBP Direct Cost", "PRBPDocNo",
                    //16-19
                    "VdBankAcDNo", "Bank Account", "Quantity", "PRJI Total With Tax"
                    /*"Quantity 1"+Environment.NewLine+"(Unit)", "Quantity 2"+Environment.NewLine+"(Time)", "Unit Price", "Tax", "Total Without Tax", 
                    "Total Tax", "Total With Tax", "Remark", "", "MR#", 
                    "Project Amount"*/
                },
                new int[] 
                { 
                    20, 
                    20, 100, 200, 100, 250,
                    80, 80, 120, 120, 200,
                    20, 180, 180, 180, 0,
                    100, 200, 100, 0
                    /*150, 150, 180, 180, 200, 
                    200, 200, 150, 20, 160,
                    200*/
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 13, 14, 18, 19 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 12, 13, 14, 15, 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 11, 15, 16, 19 });
            Grd1.Cols[17].Move(6);
            Grd1.Cols[18].Move(4);
        }

        #endregion

        #region Grid 2 - Department

        private void SetGrd2()
        {
            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                { 
                    //0
                    "",

                    //1-5
                    "Code Budget Category", 
                    "Budget Category", 
                    "Amount", 
                    "Remark", 
                    "MR#",

                    //6-10
                    "", 
                    "Item's Code", 
                    "Item", 
                    "Quantity", 
                    "VdCode",

                    //11-15
                    "Vendor", 
                    "VdBankAcDNo", 
                    "Bank Account" , 
                    "Local Code Budget Category"
                },
                new int[] 
                { 
                    20, 
                    100, 200, 200, 200, 180,
                    20, 100, 200, 100, 100,
                    200, 100, 200, 120
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 3, 9 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0, 6 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 5, 7, 8, 10, 12 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 7, 10, 12,14 });
            Grd2.Cols[5].Move(13);
            Grd2.Cols[4].Move(13);
            Grd2.Cols[14].Move(2);
        }

        #endregion

        #region Grid3 - Approval Information
        private void SetGrd3()
        {
            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 2, 3, 4 });      
        }

        #endregion

        #region Grid4 - Upload File
        private void SetGrd4()
        {
            Grd4.Cols.Count = 8;
            Grd4.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "DNo",
                    
                        //1-5
                        "U",
                        "File Name",
                        "D",
                        "Upload By",
                        "Date",
                    
                        //6-7
                        "Time",
                        "File Name2"
                    },
                    new int[]
                    {
                        //0
                        0,
                        //1-5
                        20, 250, 20, 150, 80,
                        //6-7
                        80, 250
                    }
                );
            Sm.GrdColInvisible(Grd4, new int[] { 0, 7 }, false);
            Sm.GrdColButton(Grd4, new int[] { 1 }, "1");
            Sm.GrdColButton(Grd4, new int[] { 3 }, "2");
            Sm.GrdFormatDate(Grd4, new int[] { 5 });
            Sm.GrdFormatTime(Grd4, new int[] { 6 });
            Sm.GrdColReadOnly(Grd4, new int[] { 0, 2, 4, 5, 6, 7 });
        }

        #endregion

        protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1, 7, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, LueProcessInd, MeeCancelReason, TxtPRJIDocNo,
                        LueDeptCode, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, TxtAmt, MeeRemark,
                        LueVdCode, LueBCCode, TxtPICCode, TxtPICName, MeeApprovalRemark, LueDocType,
                        DteUsageDate, MeePurpose
                    }, true);
                    BtnPRJIDocNo.Enabled = false;
                    BtnPICCode.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = true;
                    Sm.GrdColReadOnly(true, false, Grd4, new int[] { 1 });
                    //if (TxtDocNo.Text.Length > 0)
                    //{
                    //    Grd2.ReadOnly = false;
                    //    Sm.GrdColReadOnly(Grd2, new int[] { 4, 5, 6, 7, 8 });
                    //}
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, MeeRemark,
                        LueVdCode, LueBCCode, LueDocType, DteUsageDate, MeePurpose
                    }, false);
                    if (mIsDroppingRequestUseType) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBankAcCode, LueBankAcCode2 }, true);
                    BtnPRJIDocNo.Enabled = true;
                    BtnPICCode.Enabled = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = false;
                    Sm.GrdColReadOnly(false, false, Grd4, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (Sm.GetLue(LueProcessInd) == "D") 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, false);
                    else 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, true);

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    BtnPICCode.Enabled = false;
                    ChkCancelInd.Focus();
                    SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            LueProcessInd, LueVdCode, LueBCCode
                        }, false);
                        Grd1.ReadOnly = Grd2.ReadOnly = false;
                        Sm.GrdColReadOnly(false, false, Grd4, new int[] { 1 });
                        mGrd4 = 0;
                        mGrd4 = Grd4.Rows.Count - 1;
                    }
                    else
                    {
                        Grd1.ReadOnly = Grd2.ReadOnly = true;
                        Sm.GrdColReadOnly(true, false, Grd4, new int[] { 1 });
                    }
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtStatus, LueProcessInd, TxtPRJIDocNo,
                LueDeptCode, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, MeeRemark, LueVdCode, LueBCCode,
                TxtPICCode, TxtPICName, MeeApprovalRemark, LueDocType, DteUsageDate, MeePurpose
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            mStateInd = string.Empty;
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            ClearGrdProject();
            ClearGrdDept();
            Grd3.Rows.Clear();
            Sm.ClearGrd(Grd4, true);
        }

        private void ClearGrdProject()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 13, 14, 18 });
        }

        private void ClearGrdDept()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 9 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDroppingRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetLue(LueProcessInd,"D");
                mStateInd = "I";
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateInd = "E";
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                ComputePRJIAmt();
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            if (Sm.IsDataExist("Select 1 From TblDroppingRequestHdr Where DocNo = @Param And Status In ('O', 'A') And CancelInd = 'N'; ", TxtDocNo.Text))
            {
                ParPrint();
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "You cannot print cancelled data.");
                return;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 8, 9, 13, 14, 18 }, e);
                if (Sm.IsGrdColSelected(new int[] { 8, 9 }, e.ColIndex))
                {
                    ComputeTotalAmt();
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 0, 11 }, e.ColIndex))
                {
                    if (e.ColIndex == 0 && BtnSave.Enabled) 
                    {
                        if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project Implementation#", false) && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                        {
                            if (mStateInd == "I") Sm.FormShowDialog(new FrmDroppingRequestDlg2(this, TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                            //Sm.GrdRequestEdit(Grd1, e.RowIndex);
                            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10, 11, 12 });
                        }
                    }

                    if (e.ColIndex == 11)
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                        {
                            var f = new FrmMaterialRequest(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                            f.ShowDialog();
                        }
                    }
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 0, 5, 11, 17 }, e.ColIndex))
                {
                    if (e.ColIndex == 0 && BtnSave.Enabled)
                    {
                        if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project Implementation#", false) && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                        {
                            if (mStateInd == "I") Sm.FormShowDialog(new FrmDroppingRequestDlg2(this, TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                            //Sm.GrdRequestEdit(Grd1, e.RowIndex);
                            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10, 11, 12 });
                        }
                    }

                    if (e.ColIndex == 5) LueRequestEdit(Grd1, LueVdCode, ref fCell, ref fAccept, e);

                    if (e.ColIndex == 17 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 5, false, "Vendor is empty.")) LueRequestEdit2(Grd1, LueVdBankAcDNo2, ref fCell, ref fAccept, e);

                    if (e.ColIndex == 11)
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                        {
                            var f = new FrmMaterialRequest(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                            f.ShowDialog();
                        }
                    }
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                if (mStateInd == "I") Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (!(mStateInd == "E" && e.KeyCode == Keys.Delete)) ComputeTotalAmt();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 3, 9 }, e);
                if (Sm.IsGrdColSelected(new int[] { 3, 9 }, e.ColIndex))
                {
                    ValidateOutstandingBudgetCategory(Sm.GetGrdStr(Grd2, e.RowIndex, 1), e.RowIndex);
                    ComputeTotalDept();
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd2.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 2 && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                {
                    LueRequestEdit(Grd2, LueBCCode, ref fCell, ref fAccept, e);
                    if (mStateInd == "I") Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 9 });
                }
                if (e.ColIndex == 11 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 2, false, "Budget Category is empty"))
                {
                    LueRequestEdit(Grd2, LueVdCode2, ref fCell, ref fAccept, e);
                    if (mStateInd == "I") Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 9 });
                }
                if (e.ColIndex == 13 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 11, false, "Vendor is empty"))
                {
                    LueRequestEdit(Grd2, LueVdBankAcDNo, ref fCell, ref fAccept, e);
                    if (mStateInd == "I") Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 9 });
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 2, false, "Budget Category is empty"))
            {
                Sm.FormShowDialog(new FrmDroppingRequestDlg3(this, e.RowIndex));
                if (mStateInd == "I") Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 9 });
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                if (mStateInd == "I") Sm.GrdRemoveRow(Grd2, e, BtnSave);
                if (!(mStateInd == "E" && e.KeyCode == Keys.Delete)) ComputeTotalDept();
            }
           
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 2).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 1)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 2].Value = OD.FileName;
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 2).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedRow = 0;
            for (int Index = Grd4.SelectedRows.Count - 1; Index >= 0; Index--)
                SelectedRow = Grd4.SelectedRows[Index].Index;
            if (SelectedRow >= mGrd4)
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.GrdEnter(Grd4, e);
                Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DroppingRequest", "TblDroppingRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDroppingRequestHdr(DocNo));

            if (TxtPRJIDocNo.Text.Length > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveDroppingRequestDtl(DocNo, Row));
            }

            if (Sm.GetLue(LueDeptCode).Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                        cml.Add(SaveDroppingRequestDtl2(DocNo, Row));
            }

            if (mIsDroppingReqAllowToUploadFile) cml.Add(SaveDroppingRequestFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0)
                {
                    if (mIsDroppingReqAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 2).Length > 0 && Sm.GetGrdStr(Grd4, Row, 2) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd4, Row, 2) != Sm.GetGrdStr(Grd4, Row, 7))
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd4, Row, 2));
                        }
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                IsPRJIOrDeptEmpty() ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                (!mIsDroppingRequestWithdraw && IsPRJIAlreadyProcessed()) ||
                (!mIsDroppingRequestWithdraw && IsDeptAlreadyProcessed()) ||
                (mIsDroppingRequestProcessedByMonthAndYear && IsDroppingRequestHasNotBeenProcessedToVoucher()) ||
                (!mIsDroppingRequestUseType && Sm.IsLueEmpty(LueBankAcCode, "Account From")) ||
                (!mIsDroppingRequestUseType && Sm.IsLueEmpty(LueBankAcCode2, "Account To")) ||
                (mIsDroppingRequestUseType && Sm.IsLueEmpty(LueDocType, "Dropping Payment Type")) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsPICCodeEmpty() ||
                (mIsDroppingRequestUseUsageDate && Sm.IsDteEmpty(DteUsageDate, "Usage Date")) ||
                (mIsDroppingRequestUsePurpose && Sm.IsMeeEmpty(MeePurpose, "Purpose")) ||
                (mIsDroppingRequestForProjectCheckMRExists && TxtPRJIDocNo.Text.Length > 0 && IsMRForPRJINotSet()) ||
                IsGrdEmpty() ||
                IsBothGrdIsFilled() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid("I")||
                (mIsDroppingRequestPRJIMultipleDraftEnabled && TxtPRJIDocNo.Text.Length > 0 && IsDroppingRequestPRJIMultipleDraftEnabled()) ||
                (mIsDroppingReqUploadFileMandatory && IsGrd4ValueNotValid());

        }

        private bool IsPICCodeEmpty()
        {
            if (Sm.GetLue(LueDeptCode).Length != 0 && TxtPICCode.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "PIC can not be empty.");
                TxtPICCode.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcDroppingRequest.SelectTab("TpgPRJI");
                return true;
            }

            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcDroppingRequest.SelectTab("TpgDept");
                return true;
            }

            return false;
        }

        private bool IsDroppingRequestHasNotBeenProcessedToVoucher()
        {
            var SQL = new StringBuilder();

            string mSiteDeptCode = string.Empty;

            if (Sm.GetLue(LueDeptCode).Length > 0) mSiteDeptCode = Sm.GetLue(LueDeptCode);
            else
            {
                var ms = new StringBuilder();

                ms.AppendLine("Select E.SiteCode ");
                ms.AppendLine("From TblProjectImplementationHdr A ");
                ms.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo And A.DocNo = @Param ");
                ms.AppendLine("Inner Join TblSOCOntractHdr C On B.SOCDocNo = C.DocNo ");
                ms.AppendLine("Inner Join TblBOQHDr D On C.BOQDocNo = D.DOcNo ");
                ms.AppendLine("Inner Join TblLOPHDr E On D.LOPDocNo = E.DocNo; ");

                mSiteDeptCode = Sm.GetValue(ms.ToString(), TxtPRJIDocNo.Text);
            }

            SQL.AppendLine("Select D.DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr D ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    Select A.DocNo, D.SiteCode From ");
            SQL.AppendLine("    TblProjectImplementationHdr A  ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocno = A1.Docno ");
            SQL.AppendLine("    INNER JOIN TblSOContractHdr B On A1.SOCDocNo = B.DocNo  ");
            SQL.AppendLine("    INNER JOIN TblBOQHdr C ON B.BOQDocNo = C.DocNo");
            SQL.AppendLine("    INNER JOIN TblLOPHdr D ON C.LOPDocNo = D.DocNo");
            SQL.AppendLine(")F On D.PRJIDocNo = F.DocNo ");
            SQL.AppendLine("Where CONCAT(@Param2, @Param1) > CONCAT(D.Yr, D.Mth) ");
            SQL.AppendLine("And (D.DeptCode = @Param3 Or F.SiteCode = @Param3)");
            SQL.AppendLine("And D.CancelInd = 'N' ");
            SQL.AppendLine("And D.Status In ('A', 'O') ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl ");
            SQL.AppendLine("    Where DocNo = D.DocNo ");
            SQL.AppendLine("    And DirectCostAmt != 0.00 ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And D.DocNo Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.DRQDocNo ");
            SQL.AppendLine("    From TblVoucherHdr T1 ");
            SQL.AppendLine("    inner Join TblDroppingPaymentHdr T2 On T1.VoucherRequestDocNo = T2.VoucherRequestDocNo ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' And T2.CancelInd = 'N' And T2.Status In ('O', 'A') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Limit 1; ");

            if (LueMth.Text.Length > 0 && LueYr.Text.Length > 0 && mSiteDeptCode.Length > 0)
            {
                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueMth), Sm.GetLue(LueYr), mSiteDeptCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to voucher this Dropping Request document : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueMth), Sm.GetLue(LueYr), mSiteDeptCode));
                    return true;
                }
            }

            return false;
        }

        private bool IsPRJIAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Where PRJIDocNo = @Param1 ");
            SQL.AppendLine("And Mth = @Param2 ");
            SQL.AppendLine("And Yr = @Param3 ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A', 'O') ");
            SQL.AppendLine("Limit 1; ");

            if (TxtPRJIDocNo.Text.Length > 0)
            {
                if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)))
                {
                    Sm.StdMsg(mMsgType.Warning, "This project implementation already created on : " + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                    TxtPRJIDocNo.Focus();
                    return true;
                }
            }

            return false;
        }

       
        private bool IsDroppingRequestPRJIMultipleDraftEnabled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DocNo FROM tbldroppingrequesthdr ");
            SQL.AppendLine("WHERE PRJIDocNo = @param1 ");
            SQL.AppendLine("AND Mth = @param2 ");
            SQL.AppendLine("AND Yr = @param3 ");
            SQL.AppendLine("AND CancelInd = 'N' ");
            SQL.AppendLine("AND ProcessInd = 'D' ");
            SQL.AppendLine("Limit 1; ");

            if (TxtPRJIDocNo.Text.Length > 0)
            {
                if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)))
                {
                    Sm.StdMsg(mMsgType.Warning, "Previous Dropping Request process still on draft : " + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                    TxtPRJIDocNo.Focus();
                    return true;
                }
            }

            return false;
        }

   

        private bool IsDeptAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Where DeptCode = @Param1 ");
            SQL.AppendLine("And Mth = @Param2 ");
            SQL.AppendLine("And Yr = @Param3 ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A', 'O') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.GetLue(LueDeptCode).Length > 0)
            {
                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)))
                {
                    Sm.StdMsg(mMsgType.Warning, "This department already created on : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                    TxtPRJIDocNo.Focus();
                    return true;
                }
            }

            return false;
        }

        private bool IsMRForPRJINotSet()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblDroppingRequestDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And A.PRJIDocNo = @Param ");
            SQL.AppendLine("    And B.MRDocNo Is Null ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This Project Implementation# has outstanding MR in : " + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text));
                TxtPRJIDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsPRJIOrDeptEmpty()
        {
            if (TxtPRJIDocNo.Text.Length > 0 && Sm.GetLue(LueDeptCode).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only choose either Project Implementation# or Department.");
                LueDeptCode.Focus();
                return true;
            }
            else if (TxtPRJIDocNo.Text.Length <= 0 && Sm.GetLue(LueDeptCode).Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose either Project Implementation# or Department.");
                LueDeptCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (TxtPRJIDocNo.Text.Length > 0 && Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Resource.");
                TcDroppingRequest.SelectTab("TpgPRJI");
                return true;
            }
            if (Sm.GetLue(LueDeptCode).Length > 0 && Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Budget Category.");
                TcDroppingRequest.SelectTab("TpgDept");
                return true;
            }
            return false;
        }

        private bool IsBothGrdIsFilled()
        {
            if (Grd1.Rows.Count > 1 && Grd2.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Only fill either Project or Department data.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid(string mStateInd)
        {
            if (TxtPRJIDocNo.Text.Length > 0 && MeeCancelReason.Text.Length <= 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, i, 3, false, "Resource is empty.")) { TcDroppingRequest.SelectTab("TpgPRJI"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, i, 18, true, "Quantity is zero.")) { TcDroppingRequest.SelectTab("TpgPRJI"); return true; }

                    if (Sm.GetGrdDec(Grd1, i, 8) > Sm.GetGrdDec(Grd1, i, 13))
                    {
                        var mMsgs = new StringBuilder();

                        mMsgs.AppendLine("RBP Remuneration     : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 13), 0));
                        mMsgs.AppendLine("Requested Remuneration : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 8), 0));
                        mMsgs.AppendLine("Requested Remuneration shold be less than or equal to RBP Remuneration.");

                        Sm.StdMsg(mMsgType.Warning, mMsgs.ToString());
                        TcDroppingRequest.SelectTab("TpgPRJI");
                        Sm.FocusGrd(Grd1, i, 8);
                        return true;
                    }
                    
                    if (Sm.GetGrdDec(Grd1, i, 9) > Sm.GetGrdDec(Grd1, i, 14))
                    {
                        var mMsgs = new StringBuilder();

                        mMsgs.AppendLine("RBP Direct Cost     : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 14), 0));
                        mMsgs.AppendLine("Requested Direct Cost : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 9), 0));
                        mMsgs.AppendLine("Requested Direct Cost shold be less than or equal to RBP Direct Cost.");

                        Sm.StdMsg(mMsgType.Warning, mMsgs.ToString());
                        TcDroppingRequest.SelectTab("TpgPRJI");
                        Sm.FocusGrd(Grd1, i, 9);
                        return true;
                    }

                    if ((Sm.GetGrdDec(Grd1, i, 13) + Sm.GetGrdDec(Grd1, i, 14)) > Sm.GetGrdDec(Grd1, i, 19)) // lebih besar dari PRJI Revision, harus RBP Revision dulu
                    {
                        var mMsgs = new StringBuilder();

                        mMsgs.AppendLine("RBP Amount             : " + Sm.FormatNum((Sm.GetGrdDec(Grd1, i, 13) + Sm.GetGrdDec(Grd1, i, 14)), 0));
                        mMsgs.AppendLine("Requested Amount : " + Sm.FormatNum((Sm.GetGrdDec(Grd1, i, 8) + Sm.GetGrdDec(Grd1, i, 9)), 0));
                        mMsgs.AppendLine("PRJI Amount             : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 19), 0));
                        mMsgs.AppendLine("RBP Amount could not bigger than PRJI Amount.");

                        Sm.StdMsg(mMsgType.Warning, mMsgs.ToString());
                        TcDroppingRequest.SelectTab("TpgPRJI");
                        if (Sm.GetGrdDec(Grd1, i, 8) > 0m) Sm.FocusGrd(Grd1, i, 8);
                        else Sm.FocusGrd(Grd1, i, 9);
                        return true;
                    }
                }

                if (mStateInd == "I")
                {
                    for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
                    {
                        for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                        {
                            if ((Sm.GetGrdStr(Grd1, r1, 2) == Sm.GetGrdStr(Grd1, r2, 2)))
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate resource found : " + Sm.GetGrdStr(Grd1, r2, 3));
                                TcDroppingRequest.SelectTab("TpgPRJI");
                                Sm.FocusGrd(Grd1, r2, 3);
                                return true;
                            }
                        }
                    }
                }
            }

            if (Sm.GetLue(LueDeptCode).Length > 0 && MeeCancelReason.Text.Length <= 0)
            {
                var l = new List<BudgetDepartment>();

                for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, i, 3, true, "Amount is zero.")) { TcDroppingRequest.SelectTab("TpgDept"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, i, 8, false, "Item is empty.")) { TcDroppingRequest.SelectTab("TpgDept"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, i, 9, true, "Quantity is zero.")) { TcDroppingRequest.SelectTab("TpgDept"); return true; }

                    l.Add(new BudgetDepartment()
                    {
                        BCCode = Sm.GetGrdStr(Grd2, i, 1),
                        DroppingAmt = Sm.GetGrdDec(Grd2, i, 3),
                        Qty = Sm.GetGrdDec(Grd2, i, 9)
                    });
                }

                if (l.Count > 0)
                {
                    var l2 = l.GroupBy(x => x.BCCode)
                        .Select
                        (
                            o => new BudgetDepartment2()
                            {
                                BCCode = o.Key,
                                RequestAmt = 0m,
                                DroppingAmt = o.Sum(p => mIsDroppingRequestDeptUseQtyCalculation ? (p.DroppingAmt * p.Qty) : p.DroppingAmt)
                            }
                        ).ToList();

                    if (l2.Count > 0)
                    {
                        string mBCCode = string.Empty;
                        var SQL = new StringBuilder();
                        var cm = new MySqlCommand();
                        int p = 0;

                        for (int i = 0; i < l2.Count; i++)
                        {
                            if (mBCCode.Length > 0) mBCCode += ",";
                            mBCCode += l2[i].BCCode;
                        }

                        #region Old Code
                        //SQL.AppendLine("Select C.BCCode, Sum(B.Amt) Amt ");
                        //SQL.AppendLine("From TblBudgetHdr A  ");
                        //SQL.AppendLine("Inner Join TblBudgetDtl B On A.DocNo = B.DocNo  ");
                        //SQL.AppendLine("Inner Join TblBudgetRequestDtl C On C.DocNo = B.BudgetRequestDocNo And C.DNo = B.BudgetRequestDNo ");
                        //SQL.AppendLine("Inner Join TblBudgetRequesthdr D On C.DocNo = D.DocNo  ");
                        //SQL.AppendLine("Where D.Mth = @Mth  ");
                        //SQL.AppendLine("And D.Yr = @Yr  ");
                        //SQL.AppendLine("And D.CancelInd = 'N'  ");
                        //SQL.AppendLine("And D.DeptCode = @DeptCode  ");
                        //SQL.AppendLine("And Find_In_Set(C.BCCode, @BCCode) ");
                        //SQL.AppendLine("Group By C.BCCode; ");
                        #endregion

                        SQL.AppendLine("Select BCCode, Sum(Amt2) Amt ");
                        SQL.AppendLine("From TblBudgetSummary ");
                        SQL.AppendLine("Where DeptCode = @DeptCode ");
                        if(!mIsBudget2YearlyFormat)
                             SQL.AppendLine("And Mth = @Mth ");
                        SQL.AppendLine("And Yr = @Yr ");
                        SQL.AppendLine("And Find_In_Set(BCCode, @BCCode) ");
                        SQL.AppendLine("Group By BCCode; ");

                        using (var cn = new MySqlConnection(Gv.ConnectionString))
                        {
                            cn.Open();
                            cm.Connection = cn;
                            cm.CommandText = SQL.ToString();
                            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                            Sm.CmParam<String>(ref cm, "@BCCode", mBCCode);
                            var dr = cm.ExecuteReader();
                            var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Amt" });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    for (int i = 0; i < l2.Count; i++)
                                    {
                                        if (l2[i].BCCode == Sm.DrStr(dr, c[0]))
                                        {
                                            l2[i].RequestAmt = Sm.DrDec(dr, c[1]);
                                            break;
                                        }
                                    }
                                }
                            }
                            dr.Close();
                        }

                        for (int i = 0; i < l2.Count; i++)
                        {
                            if (l2[i].DroppingAmt > l2[i].RequestAmt)
                            {
                                var mMsg = new StringBuilder();

                                mMsg.AppendLine("Budget Category : " + Sm.GetValue("Select BCName From TblBudgetCategory Where BCCode = @Param Limit 1; ", l2[i].BCCode));
                                mMsg.AppendLine("Total Dropping Amount : " + Sm.FormatNum(l2[i].DroppingAmt, 0));
                                mMsg.AppendLine("Request Amount : " + Sm.FormatNum(l2[i].RequestAmt, 0));
                                mMsg.AppendLine("Your dropping amount should be less than or equal to budget request amount.");

                                Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                                TcDroppingRequest.SelectTab("TpgDept");
                                Sm.FocusGrd(Grd2, p, 2);
                                l2.Clear();

                                return true;
                            }

                            p++;
                        }
                    }
                }
                l.Clear();
            }

            return false;
        }

        private MySqlCommand SaveDroppingRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDroppingRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, CancelInd, PRJIDocNo, DeptCode,PICCode, Mth, Yr, ");
            if (mIsDroppingRequestUseType)
                SQL.AppendLine("DocType, ");
            SQL.AppendLine("BankAcCode, BankAcCode2, Amt, DraftAmt, PaidInd, Remark, UsageDt, Purpose, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O', 'D', 'N', @PRJIDocNo, @DeptCode, @PICCode, @Mth, @Yr, ");
            if (mIsDroppingRequestUseType)
                SQL.AppendLine("@DocType, ");
            SQL.AppendLine("@BankAcCode, @BankAcCode2, @Amt, @Amt, 'O', @Remark, @UsageDt, @Purpose, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDate));
            Sm.CmParam<String>(ref cm, "@Purpose", MeePurpose.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetDecValue(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            return cm;
        }

        private MySqlCommand SaveDroppingRequestDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblDroppingRequestDtl(DocNo, DNo, PRBPDocNo, PRBPDNo, VdCode, VdBankAcDNo, ");
            SQLDtl.AppendLine("RemunerationAmt, DirectCostAmt, OutstandingAmt, PaidInd, Qty, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @PRBPDocNo, @PRBPDNo, @VdCode, @VdBankAcDNo, ");
            SQLDtl.AppendLine("@RemunerationAmt, @DirectCostAmt, @OutstandingAmt, 'O', @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PRBPDocNo", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@PRBPDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@VdBankAcDNo", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@OutstandingAmt", Sm.GetGrdDec(Grd1, Row, 9) + Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDroppingRequestDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblDroppingRequestDtl2(DocNo, DNo, BCCode, ItCode, Qty, VdCode, VdBankAcDNo, Amt, OutstandingAmt, PaidInd, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @BCCode, @ItCode, @Qty, @VdCode, @VdBankAcDNo, @Amt, @OutstandingAmt, 'O', @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@VdBankAcDNo", Sm.GetGrdStr(Grd2, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@OutstandingAmt", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDroppingRequestFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            if (DocNo == "Edit") DocNo = Sm.GetValue("Select DocNo From TblDroppingRequestHdr Where DocNo = @Param; ", TxtDocNo.Text);

            SQL.AppendLine("/* DroppingRequest - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDroppingRequestFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 2));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditDroppingRequestHdr());
            if (MeeCancelReason.Text.Length <= 0)
            {
                //if (Sm.GetLue(LueProcessInd) == "D")
                //{
                    cml.Add(DeleteDroppingRequestDtl12());

                    if (Sm.IsDataExist("Select 1 From TblDroppingRequestHdr Where DocNo = @Param And PRJIDocNo Is Not Null", TxtDocNo.Text))
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                                cml.Add(SaveDroppingRequestDtl(TxtDocNo.Text, Row));
                    }

                    if (Sm.IsDataExist("Select 1 From TblDroppingRequestHdr Where DocNo = @Param And DeptCode Is Not Null", TxtDocNo.Text))
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                                cml.Add(SaveDroppingRequestDtl2(TxtDocNo.Text, Row));
                    }

                if (mIsDroppingReqAllowToUploadFile) cml.Add(SaveDroppingRequestFile("Edit"));

                cml.Add(UpdatePaidInd());
                //}
            }

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0)
                {
                    if (mIsDroppingReqAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 2).Length > 0 && Sm.GetGrdStr(Grd4, Row, 2) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd4, Row, 2) != Sm.GetGrdStr(Grd4, Row, 7))
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd4, Row, 2));
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                IsDataCancelledAlready() ||
                IsDataAlreadyPaid() ||
                IsGrdValueNotValid("I") ||
                //IsPICCodeEmpty() ||
                IsDataAlreadyProcessedToMR() ||
                (mIsDroppingRequestUseType && IsDataAlreadyProcessed())
                ;
        }

        private bool IsDataAlreadyProcessedToMR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.DroppingRequestDocNo Is Not Null ");
            SQL.AppendLine("    And A.DroppingRequestDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to this MR#" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT Tbl.DocNo ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblDroppingPaymenthdr A ");
            SQL.AppendLine("Inner Join TblDroppingPaymentdtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.DRQDocNo Is Not Null ");
            SQL.AppendLine("    And A.DRQDocNo = @Param ");
            SQL.AppendLine(" ");
            SQL.AppendLine("UNION ALL  ");
            SQL.AppendLine(" ");
            SQL.AppendLine("SELECT A.DocNo ");
            SQL.AppendLine("From tblpettycashdisbursementhdr A ");
            SQL.AppendLine("WHERE A.CancelInd = 'N' And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.DroppingRequestDocNo Is Not Null ");
            SQL.AppendLine("    And A.DroppingRequestDocNo = @Param ");
            SQL.AppendLine(" ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine(" ");
            SQL.AppendLine("SELECT A.DocNo ");
            SQL.AppendLine("FROM TblVoucherRequestExternalPayrollHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherRequestExternalPayrollDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	And B.DroppingRequestDocNo Is Not Null ");
            SQL.AppendLine("   And B.DroppingRequestDocNo = @Param ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("LIMIT 1 ");



            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't cancel document because Dropping Request has been used");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyPaid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And PaidInd In ('F', 'P') ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl ");
            SQL.AppendLine("    Where DocNo = @Param ");
            SQL.AppendLine("    And DirectCostAmt = 0.00 ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been paid.");
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDroppingRequestHdr ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditDroppingRequestHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDroppingRequestHdr Set ");
            SQL.AppendLine("    Amt = @Amt, ProcessInd=@ProcessInd, CancelReason=@CancelReason, PICCode=@PICCode, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status <> 'C'; ");

            //if (IsNeedApproval())
            //{
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='DroppingRequest' ");
                if (TxtPRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("And T.SiteCode Is Not Null ");
                    SQL.AppendLine("And T.SiteCode In ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Distinct D.SiteCode ");
                    SQL.AppendLine("    From TblProjectImplementationHdr A ");
                    SQL.AppendLine("    Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.Docno ");
                    SQL.AppendLine("    Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo And A.DocNo = @PRJIDocNo ");
                    SQL.AppendLine("    Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
                    SQL.AppendLine("    Inner Join TblLOPHdr D On C.LOPDocNo = D.DocNo ");
                    SQL.AppendLine(") ");
                }

                if (Sm.GetLue(LueDeptCode).Length > 0)
                {
                    SQL.AppendLine("And T.DeptCode Is Not Null ");
                    SQL.AppendLine("And T.DeptCode = @DeptCode ");
                }
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblDroppingRequestHdr ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    And ProcessInd = 'F' ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("; ");
            //}

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'DroppingRequest' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblDroppingRequestHdr Set ");
            SQL.AppendLine("DraftAmt = @Amt ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And ProcessInd = 'D'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteDroppingRequestDtl12()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblDroppingRequestDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblDroppingRequestHdr Where DocNo = @DocNo And CancelInd = 'N'); ");

            SQL.AppendLine("Delete From TblDroppingRequestDtl2 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblDroppingRequestHdr Where DocNo = @DocNo And CancelInd = 'N'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand UpdatePaidInd() // langsung Fulfilled jika direct cost = 0
        {
            var SQL = new StringBuilder();

            if (!mIsDroppingRequestRemunerationProceedToPayment)
            {
                SQL.AppendLine("Update TblDroppingRequestDtl Set ");
                SQL.AppendLine("    PaidInd = 'F' ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And DirectCostAmt = 0 ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 From TblDroppingRequestHdr Where DocNo = @DocNo And ProcessInd = 'F' And CancelInd = 'N' ");
                SQL.AppendLine("); ");
            }

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'O' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'P' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'F' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'O') ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDroppingRequestHdr(DocNo);
                ShowDroppingRequestDtl(DocNo);
                ShowDroppingRequestDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "DroppingRequest", ref Grd3);
                if (mIsDroppingReqAllowToUploadFile) ShowDroppingRequestFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDroppingRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.ProcessInd, A.CancelReason, A.CancelInd, A.PRJIDocNo, A.DeptCode, A.Mth, A.Yr, A.BankAcCode, A.BankAcCode2, A.Amt, A.Remark, ");
            SQL.AppendLine("B.EmpCode, B.EmpName ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", D.Remark ApprovalRemark ");
            else
                SQL.AppendLine(", NULL as ApprovalRemark ");

            if (mIsDroppingRequestUseType)
                SQL.AppendLine(", A.DocType ");
            else
                SQL.AppendLine(", NULL as DocType ");

            if (mIsDroppingRequestUseUsageDate)
                SQL.AppendLine(", A.UsageDt ");
            else
                SQL.AppendLine(", NULL as UsageDt ");

            if (mIsDroppingRequestUsePurpose)
                SQL.AppendLine(", A.Purpose ");
            else
                SQL.AppendLine(", NULL as Purpose ");
            SQL.AppendLine("From TblDroppingRequestHdr A");
            SQL.AppendLine("Left Join TblEmployee B on A.PICCode = B.EmpCode ");
            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'DroppingRequest' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	Group By T1.DocNo ");
                SQL.AppendLine(") C ON A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(   ");
                SQL.AppendLine("  	SELECT B.`Level` LV , A.DocNo, A.Remark, A.UserCode, C.UserName");
                SQL.AppendLine("	FROM tbldocapproval A");
                SQL.AppendLine("	INNER JOIN tbldocapprovalsetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo");
                SQL.AppendLine("	INNER JOIN tbluser C ON A.UserCode = C.UserCode");
                SQL.AppendLine("	WHERE A.DocNo = @DocNo");
                SQL.AppendLine("	And (A.UserCode Is Not Null And A.UserCode <> '')   ");
                SQL.AppendLine("	And A.Status Is Not Null ");

                SQL.AppendLine(") D On C.DocNo = D.DocNo And C.LV = D.LV ");
            }
            if (mIsDroppingRequestUseType)
                SQL.AppendLine("Left Join TblOption E On A.DocType = E.OptCode And E.OptCat = 'DroppingRequestDocType' ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "StatusDesc", "ProcessInd", "CancelReason", "CancelInd", 
                    
                    //6-10
                    "PRJIDocNo", "DeptCode", "Mth", "Yr", "BankAcCode", 
                    
                    //11-15
                    "BankAcCode2", "Amt", "Remark", "EmpCode", "EmpName",

                    //16-19
                    "ApprovalRemark", "DocType", "UsageDt", "Purpose"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[3]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[5]) == "Y";
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueBankAcCode2, Sm.DrStr(dr, c[11]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[7]), "N");
                    TxtPICCode.EditValue = Sm.DrStr(dr, c[14]);
                    TxtPICName.EditValue = Sm.DrStr(dr, c[15]);
                    if (mIsPSModuleShowApproverRemarkInfo)
                        MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[16]);
                    if (mIsDroppingRequestUseType)
                        Sm.SetLue(LueDocType, Sm.DrStr(dr, c[17]));
                    if (mIsDroppingRequestUseUsageDate)
                        Sm.SetDte(DteUsageDate, Sm.DrStr(dr, c[18]));
                    if (mIsDroppingRequestUsePurpose)
                        MeePurpose.EditValue = Sm.DrStr(dr, c[19]);
                }, true
            );
        }

        private void ShowDroppingRequestDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.DocNo, A.DNo, A.PRBPDocNo, A.PRBPDNo, B.ResourceItCode, D.ItName, A.VdCode, ");
            SQLDtl.AppendLine("E.VdName, C.Mth, C.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, ");
            if (mIsDroppingRequestUseMRReceivingPartial)
                SQLDtl.AppendLine("H.MRDocNo, ");
            else
                SQLDtl.AppendLine("A.MRDocNo, ");
            SQLDtl.AppendLine("if(C.RemunerationAmt > 0, C.RemunerationAmt+ C.TaxAmt + C.SSHealthAmt + C.SSEmploymentAmt + C.OthersAmt, 0.00) As RBPRemunerationAmt, ");
            SQLDtl.AppendLine("if(C.DirectCostAmt > 0, C.DirectCostAmt+ C.TaxAmt + C.SSHealthAmt + C.SSEmploymentAmt + C.OthersAmt, 0.00) As RBPDirectCostAmt, ");
            SQLDtl.AppendLine("A.VdBankAcDNo, Concat('[', F.BankAcNo, '] ', F.BankAcName, ' ', G.BankName) BankAcNo, A.Qty ");
            SQLDtl.AppendLine("From TblDroppingRequestDtl A ");
            SQLDtl.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQLDtl.AppendLine("Inner Join TblProjectImplementationRBPDtl C On B.DocNo = C.DocNo And A.PRBPDNo = C.DNo ");
            SQLDtl.AppendLine("Inner Join TblItem D On B.ResourceItCode = D.ItCode ");
            SQLDtl.AppendLine("Left Join TblVendor E On A.VdCode = E.VdCode ");
            SQLDtl.AppendLine("Left Join TblVendorBankAccount F On E.VdCode = F.VdCode And A.VdBankAcDNo = F.DNo ");
            SQLDtl.AppendLine("Left Join TblBank G On F.BankCode = G.BankCode ");
            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                SQLDtl.AppendLine("LEFT JOIN ( ");
                SQLDtl.AppendLine("	SELECT GROUP_CONCAT(A.DocNo) MRDocNo, I.DocNo AS DroppingReqDocNo, ");
                SQLDtl.AppendLine("	M.ItCode, M.ItName ");
                SQLDtl.AppendLine("	FROM tblmaterialrequesthdr A ");
                SQLDtl.AppendLine("	INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                SQLDtl.AppendLine("	LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                SQLDtl.AppendLine("	LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                SQLDtl.AppendLine("	LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                SQLDtl.AppendLine("	LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                SQLDtl.AppendLine("	LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                SQLDtl.AppendLine("	LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                SQLDtl.AppendLine("	INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                SQLDtl.AppendLine("	INNER JOIN tbldroppingrequestdtl J ON I.DocNo = J.DocNo ");
                SQLDtl.AppendLine("	INNER JOIN TblProjectImplementationRBPHdr K On J.PRBPDocNo = K.DocNo AND B.ItCode = K.ResourceItCode ");
                SQLDtl.AppendLine("	INNER JOIN TblProjectImplementationRBPDtl L On K.DocNo = L.DocNo And J.PRBPDNo = L.DNo  ");
                SQLDtl.AppendLine("	LEFT JOIN tblitem M ON K.ResourceItCode = M.ItCode ");
                SQLDtl.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                SQLDtl.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A' ");
                SQLDtl.AppendLine("	AND A.DroppingRequestDocNo = @DocNo ");
                SQLDtl.AppendLine("	GROUP BY I.DocNo, M.ItCode, M.ItName ");
                SQLDtl.AppendLine(")H ON A.DocNo = H.DroppingReqDocNo AND B.ResourceItCode = H.ItCode ");
            }
            SQLDtl.AppendLine("Order By A.DNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "PRBPDNo", 
                    "ResourceItCode", "ItName", "VdCode", "VdName", "Mth", 
                    "Yr", "RemunerationAmt", "DirectCostAmt", "Remark", "MRDocNo", 
                    "RBPRemunerationAmt", "RBPDirectCostAmt", "PRBPDocNo", "VdBankAcDNo", "BankAcNo",
                    "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 13, 14 });
        }

        private void ShowDroppingRequestDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.DocNo, A.DNo, A.BCCode, B.BCName, A.ItCode, C.ItName, A.Qty, ");
            SQLDtl2.AppendLine("A.VdCode, D.VdName, A.VdBankAcDNo, ");
            SQLDtl2.AppendLine("Concat('[', E.BankAcNo, '] ', E.BankAcName, ' ', F.BankName) BankAcNo, A.Amt, A.Remark, B.LocalCode,  ");
            if (mIsDroppingRequestUseMRReceivingPartial)
                SQLDtl2.AppendLine(" G.MRDocNo ");
            else
                SQLDtl2.AppendLine(" A.MRDocNo ");
            SQLDtl2.AppendLine("From TblDroppingRequestDtl2 A ");
            SQLDtl2.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode And A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Left Join TblItem C On A.ItCode = C.ItCode ");
            SQLDtl2.AppendLine("Left Join TblVendor D On A.VdCode = D.VdCode ");
            SQLDtl2.AppendLine("Left Join TblVendorBankAccount E On A.VdCode = E.VdCode And A.VdBankAcDNo = E.DNo ");
            SQLDtl2.AppendLine("Left join TblBank F On E.BankCode = F.BankCode ");
            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                SQLDtl2.AppendLine("LEFT JOIN ( ");
                SQLDtl2.AppendLine("	SELECT GROUP_CONCAT(A.DocNo) MRDocNo, I.DocNo AS DroppingReqDocNo, ");
                SQLDtl2.AppendLine("	K.ItCode, K.ItName ");
                SQLDtl2.AppendLine("	FROM tblmaterialrequesthdr A ");
                SQLDtl2.AppendLine("	INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("	LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                SQLDtl2.AppendLine("	LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                SQLDtl2.AppendLine("	LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                SQLDtl2.AppendLine("	LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                SQLDtl2.AppendLine("	LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                SQLDtl2.AppendLine("	LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                SQLDtl2.AppendLine("	INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                SQLDtl2.AppendLine("	INNER JOIN tbldroppingrequestdtl2 J ON I.DocNo = J.DocNo AND A.DroppingRequestBCCode = J.BCCode AND B.ItCode = J.ItCode ");
                SQLDtl2.AppendLine("	LEFT JOIN tblitem K ON J.ItCode = K.ItCode ");
                SQLDtl2.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                SQLDtl2.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A' ");
                SQLDtl2.AppendLine("	AND A.DroppingRequestDocNo = @DocNo ");
                SQLDtl2.AppendLine("	GROUP BY I.DocNo, K.ItCode, K.ItName ");
                SQLDtl2.AppendLine(")G ON A.DocNo = G.DroppingReqDocNo AND A.ItCode = G.ItCode ");
            }
            SQLDtl2.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm2, SQLDtl2.ToString(),
                new string[] 
                { 
                    "BCCode", 
                    "BCName", "Amt", "Remark", "MRDocNo", "ItCode", 
                    "ItName", "Qty", "VdCode", "VdName", "VdBankAcDNo",
                    "BankAcNo", "LocalCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 9 });
        }

        private void ShowDroppingRequestFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                   "Select DNo, FileName, CreateBy, CreateDt From TblDroppingRequestFile Where DocNo=@DocNo Order By DNo",

                    new string[]
                    {
                        // 0
                        "DNo",

                        //1-3
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd4, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd4, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 7, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ComputePRJIAmt()
        {
            if (Sm.GetLue(LueDeptCode).Length > 0) return;

            if (TxtPRJIDocNo.Text.Length > 0)
            {
                string mPRBPDocNo = string.Empty;
                if (Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        Grd1.Cells[i, 19].Value = 0m;
                        if (Sm.GetGrdStr(Grd1, i, 15).Length > 0)
                        {
                            if (mPRBPDocNo.Length > 0) mPRBPDocNo += ",";
                            mPRBPDocNo += string.Concat(Sm.GetGrdStr(Grd1, i, 15), Sm.GetGrdStr(Grd1, i, 1)); // PRBPDocNo + PRBPDNo
                        }
                    }
                }

                if (mPRBPDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();
                    var cm = new MySqlCommand();
                    var l = new List<PRJIAmount>();

                    SQL.AppendLine("Select C.DocNo, C.DNo, Sum(A.Amt) Amt ");
                    SQL.AppendLine("From TblProjectImplementationDtl2 A ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.DocNo = B.PRJIDocNo And A.ResourceItCode = B.ResourceItCode ");
                    SQL.AppendLine("    And A.DocNo = @PRJIDocNo And B.CancelInd = 'N' ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPDtl C On B.DocNo = C.DocNo ");
                    SQL.AppendLine("    And Find_In_Set(Concat(C.DocNo, C.DNo), @PRBPDocNo) ");
                    SQL.AppendLine("Group By C.DocNo, C.DNo; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@PRBPDocNo", mPRBPDocNo);

                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Amt" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new PRJIAmount()
                                {
                                    DocNo = Sm.DrStr(dr, c[0]),
                                    DNo = Sm.DrStr(dr, c[1]),
                                    Amt = Sm.DrDec(dr, c[2])
                                });
                            }
                        }

                        dr.Close();
                    }

                    if (l.Count > 0)
                    {
                        for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                        {
                            foreach(var x in l.Where(w => 
                                w.DocNo == Sm.GetGrdStr(Grd1, i, 15) && 
                                w.DNo == Sm.GetGrdStr(Grd1, i, 1)
                                ))
                            {
                                Grd1.Cells[i, 19].Value = x.Amt;
                                break;
                            }
                        }
                    }

                    l.Clear();
                }
            }
        }

        private void ParPrint()
        {
            string[] TableName = { "DroppingRequestHdr", "DroppingRequestHdrMNET", "DroppingRequestDtl", "DroppingRequestDtl1", "DroppingRequestDtlMNET", "DroppingRequestSubTotal", "DroppingRequestGrandTotal", "DroppingRequestSignMNET" };

            string BCCode = string.Empty;

            var l = new List<DroppingRequestHdr>();
            var lMNET = new List<DroppingRequestHdrMNET>();
            var lDtl = new List<DroppingRequestDtl>();
            var lDtl1 = new List<DroppingRequestDtl1>();
            var lDtlMNET = new List<DroppingRequestDtlMNET>();
            var lST = new List<DroppingRequestSubtotal>();
            var lGT = new List<DroppingRequestGrandTotal>();
            var lSignMNET = new List<DroppingRequestSignMNET>();
            var myLists = new List<IList>();

            decimal mRAP = 0m, mCurrentAmt = 0m, varRAP = 0m;
            decimal mTotalRAP = 0m, mTotalOpeningBalanceAmt = 0m,
                mTotalAmt = 0m, mTotalCurrentAmt = 0m, mTotalDroppingPercentage = 0m,
                mTotalOutstandingRAPAmt = 0m, mTotalOutstandingRAPPercentage = 0m,
                mTotalRAP1 = 0m, mTotalOpeningBalanceAmt1 = 0m,
                mTotalAmt1 = 0m, mTotalCurrentAmt1 = 0m, mTotalDroppingPercentage1 = 0m,
                mTotalOutstandingRAPAmt1 = 0m, mTotalOutstandingRAPPercentage1 = 0m;
                       

            string sSQL = string.Empty;

            sSQL += "SELECT Convert(RIGHT(CONCAT('0', IF(COUNT(*) = 0, '1', COUNT(*))), 2), char(100)) AS ReloadNo ";
            sSQL += "FROM TblDroppingRequestHdr X1 ";
            sSQL += "INNER JOIN TblDepartmentBudgetSite X2 ON X1.DeptCode = X2.DeptCode ";
            sSQL += "    AND X2.SiteCode = @Param1 ";
            sSQL += "    AND X1.CancelInd = 'N' ";
            sSQL += "    AND X1.DeptCode IS NOT NULL ";
            sSQL += "    AND X1.DocNo != @Param2 ";
            sSQL += "    AND X1.DocDt <= @Param3 ";

            string mDeptCode = Sm.GetLue(LueDeptCode);
            string mSiteCode = Sm.GetValue("SELECT SiteCode FROM TblDepartmentBudgetSite WHERE DeptCode = @Param ORDER BY SiteCode ASC LIMIT 1", Sm.GetLue(LueDeptCode));
            string mYr = Sm.GetLue(LueYr);
            string mMth = Sm.GetLue(LueMth);
            string mDocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string mReloadNo = Sm.GetValue(sSQL, mSiteCode, TxtDocNo.Text, mDocDt);
            string mDocTitle = Sm.GetValue("Select ParValue From TblParameter Where Parcode='DocTitle';");

            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.Yr YearBTL, DATE_FORMAT(CONCAT(A.Yr, A.Mth, '01'), '%M %Y') Mth, LEFT(A.DocNo, 4) DocNo, B.ParValue AS CompanyName, ");
            SQL.AppendLine("E.SiteName, CONCAT(IfNull(@SiteCode, ''), '.0.', RIGHT(A.Yr, 2), '.', @ReloadNo) AS `Code`, IFNULL(K.Amt, 0.00) Amt, F.EmpName PICName, ");
            SQL.AppendLine("TRIM(CONCAT( ");
            SQL.AppendLine("    Case When C.BankAcNo Is Not Null ");
            SQL.AppendLine("        Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(C.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(' ', D.BankName) Else '' End ");
            SQL.AppendLine(")) BankAccount, Case A.ProcessInd When 'D' Then 'DRAFT RAB' When 'F' Then 'FINAL RAB' END AS ProcessInd, ");
            SQL.AppendLine("CONCAT(G.ParValue, ', ', DATE_FORMAT(A.DocDt, '%d %M %Y')) SignDt, H.ParValue SignName1, I.ParValue SignName2, J.ParValue SignName3, ");
            SQL.AppendLine("-- print 2 ");
            SQL.AppendLine("'Biaya Tidak Langsung' AS ProjectName, A.Mth Mth2, CONCAT(IFNULL(E.Address, ''), ', ', DATE_FORMAT(A.DocDt, '%d %M %Y')) SignDt2, CONCAT('KEPALA ', E.SiteName) HOSiteName ");
            SQL.AppendLine("FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'ReportTitle1' ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.DeptCode IS NOT NULL ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblBankAccount C ON A.BankAcCode2 = C.BankAcCode ");
            SQL.AppendLine("LEFT JOIN TblBank D ON C.BankCode = D.BankCode ");
            SQL.AppendLine("LEFT JOIN TblSite E ON E.SiteCode = @SiteCode ");
            SQL.AppendLine("LEFT JOIN TblEmployee F ON A.PICCode = F.EmpCode ");
            SQL.AppendLine("LEFT JOIN TblParameter G ON G.ParCode = 'DroppingRequestPrintOutCityName' ");
            SQL.AppendLine("LEFT JOIN TblParameter H ON H.ParCode = 'DroppingRequestPrintOutSignName1' ");
            SQL.AppendLine("LEFT JOIN TblParameter I ON I.ParCode = 'DroppingRequestPrintOutSignName2' ");
            SQL.AppendLine("LEFT JOIN TblParameter J ON J.ParCode = 'DroppingRequestPrintOutSignName3' ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            //SQL.AppendLine("    SELECT T2.DeptCode, T1.Yr, SUM(T3.Amt) Amt ");
            //SQL.AppendLine("    FROM TblBudgetHdr T1 ");
            //SQL.AppendLine("    INNER JOIN TblBudgetRequestHdr T2 ON T1.BudgetRequestDocNo = T2.DocNo ");
            //SQL.AppendLine("        AND T2.DeptCode = @DeptCode ");
            //SQL.AppendLine("        AND T1.Status = 'A' ");
            //SQL.AppendLine("        AND T2.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblBudgetDtl T3 ON T1.DocNo = T3.DocNo ");
            //SQL.AppendLine("    GROUP BY T2.DeptCode, T1.Yr ");
            SQL.AppendLine("    Select A.Yr, A.deptCode, IFNULL(SUM(C.Amt2), 0.00) As Amt ");
            SQL.AppendLine("    From tblBudgetRequestHdr A ");
            SQL.AppendLine("    Inner Join TblbudgetRequestDtl B On A.DocNo = B.DocNO ");
            SQL.AppendLine("        And A.Status = 'A' ");
            SQL.AppendLine("        And Cancelind = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblBudgetSummary C ON A.Yr = C.Yr ");
            if(!mIsBudget2YearlyFormat)
                 SQL.AppendLine("        AND A.Mth = C.Mth ");
            SQL.AppendLine("        AND A.DeptCode = C.DeptCode ");
            SQL.AppendLine("        AND B.BCCode = C.BCCode ");
            SQL.AppendLine("    Group BY A.Yr, A.deptCode ");
            SQL.AppendLine(") K ON A.DeptCode = K.DeptCode And A.Yr = K.Yr ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("SELECT A.Yr, DATE_FORMAT(CONCAT(A.Yr, A.Mth, '01'), '%M %Y') Mth, LEFT(A.DocNo, 4) DocNo, B.ParValue AS CompanyName, ");
            SQL.AppendLine("H.SiteName, If(P.ParValue = '2', E.ProjectCode2, E.ProjectCode) `Code`, D.Amt, I.EmpName PICName, ");
            SQL.AppendLine("TRIM(CONCAT( ");
            SQL.AppendLine("    Case When J.BankAcNo Is Not Null ");
            SQL.AppendLine("        Then Concat(J.BankAcNo, ' [', IfNull(J.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(J.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When K.BankName Is Not Null Then Concat(' ', K.BankName) Else '' End ");
            SQL.AppendLine(")) BankAccount, Case A.ProcessInd When 'D' Then 'DRAFT RAB' When 'F' Then 'FINAL RAB' END AS ProcessInd, ");
            SQL.AppendLine("CONCAT(L.ParValue, ', ', DATE_FORMAT(A.DocDt, '%d %M %Y')) SignDt, M.ParValue SignName1, N.ParValue SignName2, O.ParValue SignName3, ");
            SQL.AppendLine("-- print 2 ");
            SQL.AppendLine("G.ProjectName, A.Mth Mth2, CONCAT(IFNULL(H.Address, ''), ', ', DATE_FORMAT(A.DocDt, '%d %M %Y')) SignDt2, CONCAT('KEPALA ', H.SiteName) HOSiteName ");
            SQL.AppendLine("FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'ReportTitle1' ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("    AND A.PRJIDocNo IS NOT NULL ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN TblProjectImplementationHdr C ON A.PRJIDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr D ON C.SOContractDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr E ON D.SOCDocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblBOQHdr F ON E.BOQDocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr G ON F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblSite H ON G.SiteCode = H.SiteCode ");
            SQL.AppendLine("LEFT JOIN TblEmployee I ON G.PICCode = I.EmpCode ");
            SQL.AppendLine("INNER JOIN TblBankAccount J ON A.BankAcCode2 = J.BankAcCode ");
            SQL.AppendLine("LEFT JOIN TblBank K ON J.BankCode = K.BankCode ");
            SQL.AppendLine("LEFT JOIN TblParameter L ON L.ParCode = 'DroppingRequestPrintOutCityName' ");
            SQL.AppendLine("LEFT JOIN TblParameter M ON M.ParCode = 'DroppingRequestPrintOutSignName1' ");
            SQL.AppendLine("LEFT JOIN TblParameter N ON N.ParCode = 'DroppingRequestPrintOutSignName2' ");
            SQL.AppendLine("LEFT JOIN TblParameter O ON O.ParCode = 'DroppingRequestPrintOutSignName3' ");
            SQL.AppendLine("LEFT JOIN TblParameter P ON P.ParCode = 'ProjectAcNoFormula'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@Mth", mMth);
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@ReloadNo", mReloadNo);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "Mth",
                    //1-5
                    "DocNo", "CompanyName", "SiteName", "Code", "Amt", 
                    //6-10
                    "PICName", "BankAccount", "ProcessInd", "SignDt", "SignName1", 
                    //11-15
                    "SignName2", "SignName3", "ProjectName", "SignDt2", "HOSiteName",
                    //16
                    "Mth2", "YearBTL"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DroppingRequestHdr()
                        {
                            Mth = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            CompanyName = Sm.DrStr(dr, c[2]),
                            SiteName = Sm.DrStr(dr, c[3]),
                            Code = Sm.DrStr(dr, c[4]),
                            Amt = Sm.DrDec(dr, c[5]),

                            PICName = Sm.DrStr(dr, c[6]),
                            BankAccount = Sm.DrStr(dr, c[7]),
                            ProcessInd = Sm.DrStr(dr, c[8]),
                            SignDt = Sm.DrStr(dr, c[9]),
                            SignName1 = Sm.DrStr(dr, c[10]),

                            SignName2 = Sm.DrStr(dr, c[11]),
                            SignName3 = Sm.DrStr(dr, c[12]),
                            ProjectName = Sm.DrStr(dr, c[13]),
                            SignDt2 = Sm.DrStr(dr, c[14]),
                            HOSiteName = Sm.DrStr(dr, c[15]),

                            Mth2 = Sm.DrStr(dr, c[16]),
                            Yr = mYr,

                            TotalAmt = 0m,
                            TotalCurrentAmt = 0m,
                            TotalDroppingPercentage = 0m,
                            TotalOpeningBalanceAmt = 0m,
                            TotalOutstandingRAPAmt = 0m,
                            TotalOutstandingRAPPercentage = 0m,
                            TotalRAP = 0m,
                            YearBTL = Sm.DrStr(dr, c[17]),
                            TotalAmt1 = 0m,
                            TotalCurrentAmt1 = 0m,
                            TotalDroppingPercentage1 = 0m,
                            TotalOpeningBalanceAmt1 = 0m,
                            TotalOutstandingRAPAmt1 = 0m,
                            TotalOutstandingRAPPercentage1 = 0m,
                            TotalRAP1 = 0m,
                        });
                    }
                }

                dr.Close();
            }

            #endregion

            #region Header MNET

            if (mDocTitle == "MNET")
            {
                var SQLHdr = new StringBuilder();
                var cmHdr = new MySqlCommand();

                SQLHdr.AppendLine("SELECT A.DocNo, A.CreateBy, E.ProjectType, F.OptDesc AS ProjectName, A.UsageDt,  ");
                SQLHdr.AppendLine("A.Purpose, DATE_FORMAT(LEFT(A.DocDt, 8), '%Y-%m-%d') AS DocDt, E.CCCode, G.CCName,  ");
                SQLHdr.AppendLine("A.PRJIDocNo AS PRJIDocNo, C.DocNo AS SOCDocNo, A.Remark, @CompanyLogo As CompanyLogo, Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' END AS Status  ");
                SQLHdr.AppendLine("FROM tbldroppingrequesthdr A ");
                SQLHdr.AppendLine("Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo  ");
                SQLHdr.AppendLine("Inner Join TblSOContractRevisionHdr B1 On B.SOContractDocno = B1.DocNo  ");
                SQLHdr.AppendLine("Inner Join TblSOContractHdr C On B1.SOCDocNo = C.DocNo  ");
                SQLHdr.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo  ");
                SQLHdr.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo  ");
                SQLHdr.AppendLine("LEFT JOIN tbloption F ON F.OptCat='ProjectType' AND E.ProjectType=F.OptCode ");
                SQLHdr.AppendLine("LEFT JOIN tblcostcenter G ON E.CCCode=G.CCCode ");
                SQLHdr.AppendLine("WHERE A.DocNo=@DocNo Limit 1 ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cmHdr.Connection = cn;
                    cmHdr.CommandText = SQLHdr.ToString();
                    Sm.CmParam<String>(ref cmHdr, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmHdr, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr = cmHdr.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "CreateBy", "ProjectType", "ProjectName", "UsageDt", "Purpose", 
                        //6-10
                        "DocDt", "CCCode", "CCName", "PRJIDocNo", "SOCDocNo", 
                        //11-13
                        "Remark", "CompanyLogo", "Status"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lMNET.Add(new DroppingRequestHdrMNET()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                CreateBy = Sm.DrStr(dr, c[1]),
                                ProjectType = Sm.DrStr(dr, c[2]),
                                ProjectName = Sm.DrStr(dr, c[3]),
                                UsageDt = Sm.DrStr(dr, c[4]),
                                Purpose = Sm.DrStr(dr, c[5]),
                                DocDt = Sm.DrStr(dr, c[6]),
                                CCCode = Sm.DrStr(dr, c[7]),
                                CCName = Sm.DrStr(dr, c[8]),
                                PRJIDocNo = Sm.DrStr(dr, c[9]),
                                SOCDocNo = Sm.DrStr(dr, c[10]),
                                Remark = Sm.DrStr(dr, c[11]),
                                CompanyLogo = Sm.DrStr(dr, c[12]),
                                Status = Sm.DrStr(dr, c[13])
                            });
                        }
                    }

                    dr.Close();
                }
            }

            #endregion

            #region Detail

            var SQLD = new StringBuilder();
            var cmd = new MySqlCommand();

            SQLD.AppendLine("SELECT C.LocalCode AS `No`, C.BCName AS Detail, IFNULL(D.Amt, 0.00) RAP,  E.Amount, ");
            if (mIsDroppingRequestDeptUseQtyCalculation)
                SQLD.AppendLine("(B.Amt*B.Qty) Amt, (IFNULL(E.Amt, 0.00) + (B.Amt*B.Qty)) CurrentAmt, ");
            else
                SQLD.AppendLine("B.Amt, (IFNULL(E.Amt, 0.00) + B.Amt) CurrentAmt, ");
            SQLD.AppendLine("B.Remark, ");
            SQLD.AppendLine("-- print 2 ");
            SQLD.AppendLine("CONCAT('(', IfNull(C.LocalCode, ''), ') ', C.BCName) RekCode, F.ItName ");
            SQLD.AppendLine("FROM TblDroppingRequestHdr A ");
            SQLD.AppendLine("INNER JOIN TblDroppingRequestDtl2 B ON A.DocNo = B.DocNo ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("    AND A.CancelInd = 'N' ");
            SQLD.AppendLine("    AND A.DeptCode IS NOT NULL ");
            SQLD.AppendLine("INNER JOIN TblBudgetCategory C ON B.BCCode = C.BCCode ");

            #region Old Code
            //SQLD.AppendLine("LEFT JOIN ");
            //SQLD.AppendLine("( ");
            //SQLD.AppendLine("    SELECT T2.BCCode,  SUM(T2.Amt) Amt ");
            //SQLD.AppendLine("    FROM TblBudgetRequestHdr T1 ");
            //SQLD.AppendLine("    INNER JOIN TblBudgetRequestDtl T2 ON T1.DocNo = T2.DocNo ");
            //SQLD.AppendLine("        AND T1.CancelInd = 'N' ");
            //SQLD.AppendLine("        AND T1.DeptCode = @DeptCode ");
            //SQLD.AppendLine("        AND T1.Yr = @Yr ");
            //SQLD.AppendLine("    Group By T2.BCCode ");
            ////SQLD.AppendLine("    INNER JOIN TblBudgetHdr T3 ON T1.DocNo = T3.BudgetRequestDocNo ");
            ////SQLD.AppendLine("        AND T3.Status = 'A' ");
            ////SQLD.AppendLine("    INNER JOIN TblBudgetDtl T4 ON T3.DocNo = T4.DocNo ");
            ////SQLD.AppendLine("        AND T2.DocNo = T4.BudgetRequestDocNo ");
            ////SQLD.AppendLine("        AND T2.DNo = T4.BudgetRequestDNo ");
            //SQLD.AppendLine(") D ON B.BCCode = D.BCCode ");
            #endregion

            #region wed
            SQLD.AppendLine("Left Join ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("    Select T2.BCCode, IfNull(Sum(T3.Amt2), 0.00) Amt ");
            SQLD.AppendLine("    From TblBudgetRequestHdr T1 ");
            SQLD.AppendLine("    Inner Join TblBudgetRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQLD.AppendLine("        And T1.CancelInd = 'N' ");
            SQLD.AppendLine("        And T1.DeptCode = @DeptCode ");
            SQLD.AppendLine("        And T1.Yr = @Yr ");
            SQLD.AppendLine("    Left Join TblBudgetSummary T3 On T1.Yr = T3.Yr ");
            if(!mIsBudget2YearlyFormat)
                 SQLD.AppendLine("        And T1.Mth = T3.Mth ");
            SQLD.AppendLine("        And T1.DeptCode = T3.DeptCode ");
            SQLD.AppendLine("        And T2.BCCode = T3.BCCode ");
            SQLD.AppendLine("    Group By T2.BCCode ");
            SQLD.AppendLine(") D On B.BCCode = D.BCCode ");
            #endregion

            SQLD.AppendLine("Left JOIN ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("   SELECT B.BcCode, ");
            if (mIsDroppingRequestDeptUseQtyCalculation)
                SQLD.AppendLine("   SUM(B.Amt*B.Qty) Amt, SUM((B.Amt*B.Qty) - B.outstandingamt) Amount ");
            else
                SQLD.AppendLine("   SUM(B.Amt) Amt, SUM(B.Amt-B.outstandingamt) Amount ");
            SQLD.AppendLine("   FROM tbldroppingrequesthdr A  ");
            SQLD.AppendLine("   INNER JOIN tbldroppingrequestdtl2 B ON A.DocNo=B.DocNo ");
            SQLD.AppendLine("     AND A.DeptCode = @DeptCode  ");
            SQLD.AppendLine("     AND LEFT(A.DocDt, 6) < CONCAT(@Yr, @Mth)  ");
            SQLD.AppendLine("     AND B.OutstandingAmt =0  ");
            SQLD.AppendLine("     Where A.CancelInd = 'N' ");
            SQLD.AppendLine("     Group BY B.BcCode ");
            SQLD.AppendLine(" )E ON B.BCCode = E.BCCode ");

            SQLD.AppendLine("INNER JOIN TblItem F ON B.ItCode = F.ItCode ");

            SQLD.AppendLine("UNION ALL ");

            SQLD.AppendLine("SELECT NULL AS `No`, D.ItName Detail, If(A.Status = 'A', A.Amt, 0.00) RAP, 0.00 OpeningBalance, ");
            SQLD.AppendLine("(B.RemunerationAmt + B.DirectCostAmt) Amt, (B.RemunerationAmt + B.DirectCostAmt) CurrentAmt, B.Remark, ");
            SQLD.AppendLine("-- print 2 ");
            SQLD.AppendLine("C.ResourceItCode AS RekCode, D.ItName ");
            SQLD.AppendLine("FROM TblDroppingRequestHdr A ");
            SQLD.AppendLine("INNER JOIN TblDroppingRequestDtl B ON A.DocNo = B.DocNo ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("    AND A.CancelInd = 'N' ");
            SQLD.AppendLine("    AND A.PRJIDocNo IS NOT NULL ");
            SQLD.AppendLine("INNER JOIN TblProjectImplementationRBPHdr C ON B.PRBPDocNo = C.DocNo ");
            SQLD.AppendLine("INNER JOIN TblItem D ON C.ResourceItCode = D.ItCode ");
            SQLD.AppendLine("Order By No ; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmd.Connection = cnd;
                cmd.CommandText = SQLD.ToString();
                Sm.CmParam<String>(ref cmd, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmd, "@Yr", mYr);
                Sm.CmParam<String>(ref cmd, "@Mth", mMth);
                Sm.CmParam<String>(ref cmd, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cmd, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cmd, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cmd, "@ReloadNo", mReloadNo);

                var drd = cmd.ExecuteReader();
                var cd = Sm.GetOrdinal(drd, new string[] 
                {
                    //0
                    "No", 
                    //1-5
                    "Detail", "RAP", "Amount", "Amt", "CurrentAmt", 
                    //6-8
                    "RekCode", "ItName", "Remark"
                });
                if (drd.HasRows)
                {
                    while (drd.Read())
                    {
                        mRAP = Sm.DrDec(drd, cd[2]);
                        mCurrentAmt = Sm.DrDec(drd, cd[5]);

                        lDtl.Add(new DroppingRequestDtl()
                        {
                            No = Sm.DrStr(drd, cd[0]),
                            Detail = Sm.DrStr(drd, cd[1]),
                            RAP = Sm.DrDec(drd, cd[2]),
                            OpeningBalanceAmt = Sm.DrDec(drd, cd[3]),
                            Amt = Sm.DrDec(drd, cd[4]),
                            CurrentAmt = Sm.DrDec(drd, cd[4]) + Sm.DrDec(drd, cd[3]),
                            RekCode = Sm.DrStr(drd, cd[6]),
                            ItName = Sm.DrStr(drd, cd[7]),
                            Remark = Sm.DrStr(drd, cd[8]),

                            SubRAP = 0m,
                            SubOpeningBalanceAmt = 0m,
                            SubAmt = 0m,
                            SubCurrentAmt = 0m,
                            SubOutstandingRAPAmt = 0m,
                            SubOutstandingRAPPercentage = 0m,

                            DroppingPercentage = (mRAP == 0m) ? 0m : (mCurrentAmt / mRAP) * 100m,
                            OutstandingRAPAmt = mRAP - mCurrentAmt,
                            OutstandingRAPPercentage = (mRAP == 0m) ? 0m : ((mRAP - mCurrentAmt) / mRAP) * 100m,
                            RAP2 = mRAP - (mRAP - mCurrentAmt)
                        });
                    }
                }

                drd.Close();
            }

            #endregion

            #region Detail 1

            var SQLD1 = new StringBuilder();
            var cmd1 = new MySqlCommand();

            SQLD1.AppendLine("SELECT C.LocalCode AS `No`, C.BCName AS Detail, IFNULL(D.Amt, 0.00) RAP, IfNull(E.Amount, 0.00) Amount, ");
            SQLD1.AppendLine("B.Amt, (IFNULL(E.Amt, 0.00) + B.Amt) CurrentAmt, ");
            SQLD1.AppendLine("B.Remark ");
            SQLD1.AppendLine("FROM TblDroppingRequestHdr A ");
            SQLD1.AppendLine("INNER JOIN ");
            SQLD1.AppendLine("( ");
            SQLD1.AppendLine("    SELECT DocNo, BCCode, Group_Concat(Distinct Remark) Remark, ");
            if (mIsDroppingRequestDeptUseQtyCalculation)
                SQLD1.AppendLine("    SUM(Amt*Qty) Amt ");
            else
                SQLD1.AppendLine("    SUM(Amt) Amt ");
            SQLD1.AppendLine("    FROM TblDroppingRequestDtl2 ");
            SQLD1.AppendLine("    WHERE DocNo = @DocNo ");
            SQLD1.AppendLine("    GROUP BY DocNo, BCCode ");
            SQLD1.AppendLine(") B ON A.DocNo = B.DocNo ");
            SQLD1.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD1.AppendLine("    AND A.CancelInd = 'N' ");
            SQLD1.AppendLine("    AND A.DeptCode IS NOT NULL ");
            SQLD1.AppendLine("INNER JOIN TblBudgetCategory C ON B.BCCode = C.BCCode ");
            SQLD1.AppendLine("Left Join ");
            SQLD1.AppendLine("( ");
            SQLD1.AppendLine("    Select T2.BCCode, IfNull(Sum(T3.Amt2), 0.00) Amt ");
            SQLD1.AppendLine("    From TblBudgetRequestHdr T1 ");
            SQLD1.AppendLine("    Inner Join TblBudgetRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQLD1.AppendLine("        And T1.CancelInd = 'N' ");
            SQLD1.AppendLine("        And T1.DeptCode = @DeptCode ");
            SQLD1.AppendLine("        And T1.Yr = @Yr ");
            SQLD1.AppendLine("    Left Join TblBudgetSummary T3 On T1.Yr = T3.Yr ");
            if(!mIsBudget2YearlyFormat)
                 SQLD1.AppendLine("        And T1.Mth = T3.Mth ");
            SQLD1.AppendLine("        And T1.DeptCode = T3.DeptCode "); 
            SQLD1.AppendLine("        And T2.BCCode = T3.BCCode ");
            SQLD1.AppendLine("    Group By T2.BCCode ");
            SQLD1.AppendLine(") D On B.BCCode = D.BCCode ");
            SQLD1.AppendLine("Left JOIN ");
            SQLD1.AppendLine("( ");
            SQLD1.AppendLine("   SELECT B.BcCode, ");
            if (mIsDroppingRequestDeptUseQtyCalculation)
                SQLD1.AppendLine("   SUM(B.Amt*B.Qty) Amt, SUM((B.Amt*B.Qty) - B.outstandingamt) Amount ");
            else
                SQLD1.AppendLine("   SUM(B.Amt) Amt, SUM(B.Amt-B.outstandingamt) Amount ");
            SQLD1.AppendLine("   FROM tbldroppingrequesthdr A  ");
            SQLD1.AppendLine("   INNER JOIN tbldroppingrequestdtl2 B ON A.DocNo=B.DocNo ");
            SQLD1.AppendLine("     AND A.DeptCode = @DeptCode  ");
            SQLD1.AppendLine("     AND LEFT(A.DocDt, 6) < CONCAT(@Yr, @Mth)  ");
            SQLD1.AppendLine("     AND B.OutstandingAmt =0  ");
            SQLD1.AppendLine("     Where A.CancelInd = 'N' ");
            SQLD1.AppendLine("     Group BY B.BcCode  ");
            SQLD1.AppendLine(")E ON B.BCCode = E.BCCode ");

            using (var cnd1 = new MySqlConnection(Gv.ConnectionString))
            {
                cnd1.Open();
                cmd1.Connection = cnd1;
                cmd1.CommandText = SQLD1.ToString();
                Sm.CmParam<String>(ref cmd1, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmd1, "@Yr", mYr);
                Sm.CmParam<String>(ref cmd1, "@Mth", mMth);
                Sm.CmParam<String>(ref cmd1, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cmd1, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cmd1, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cmd1, "@ReloadNo", mReloadNo);

                var drd1 = cmd1.ExecuteReader();
                var cd1 = Sm.GetOrdinal(drd1, new string[] 
                {
                    //0
                    "No", 
                    //1-5
                    "Detail", "RAP", "Amount", "Amt", "CurrentAmt", 
                    //6
                    "Remark"
                });
                if (drd1.HasRows)
                {
                    while (drd1.Read())
                    {
                        mRAP = Sm.DrDec(drd1, cd1[2]);
                        mCurrentAmt = Sm.DrDec(drd1, cd1[5]);

                        lDtl1.Add(new DroppingRequestDtl1()
                        {
                            No = Sm.DrStr(drd1, cd1[0]),
                            Detail = Sm.DrStr(drd1, cd1[1]),
                            RAP = Sm.DrDec(drd1, cd1[2]),
                            OpeningBalanceAmt = Sm.DrDec(drd1, cd1[3]),
                            Amt = Sm.DrDec(drd1, cd1[4]),
                            CurrentAmt = Sm.DrDec(drd1, cd1[4]) + Sm.DrDec(drd1, cd1[3]),
                            Remark = Sm.DrStr(drd1, cd1[6]),

                            DroppingPercentage = (mRAP == 0m) ? 0m : (mCurrentAmt / mRAP) * 100m,
                            OutstandingRAPAmt = mRAP - mCurrentAmt,
                            OutstandingRAPPercentage = (mRAP == 0m) ? 0m : ((mRAP - mCurrentAmt) / mRAP) * 100m,
                        });
                    }
                }

                drd1.Close();
            }

            #endregion

            #region apa ini
            //lDtl.OrderBy(x => x.No);
            //for (int i = 0; i < lDtl.Count; i++)
            //{
            //    //BCCode = i == 0 ? lDtl[i].No : BCCode;
            //    BCCode = BCCode.Length==0 ? lDtl[i].No : BCCode;

            //    if (lDtl[i].No == BCCode)
            //    {
            //        BCCode = lDtl[i].No;
            //        mIndex = mIndex + 1;
            //        if (mIndex > 1)
            //        {
            //            lDtl[i].RAP = lDtl[i].RAP - varRAP;
            //        }
            //        varRAP = varRAP + lDtl[i].Amt;
            //    }
            //    else
            //    {
            //        BCCode = string.Empty;
            //        varRAP = 0m;
            //        mIndex = 1;
            //        if (mIndex > 1)
            //        {
            //            lDtl[i].RAP = lDtl[i].RAP - varRAP;
            //        }
            //        varRAP = varRAP + lDtl[i].Amt;
            //    }


            //    lDtl[i].DroppingPercentage = (lDtl[i].RAP == 0m) ? 0m : (lDtl[i].mCurrentAmt / lDtl[i].RAP) * 100m;
            //    lDtl[i].OutstandingRAPAmt = lDtl[i].RAP - lDtl[i].mCurrentAmt;
            //    lDtl[i].OutstandingRAPPercentage = (lDtl[i].RAP == 0m) ? 0m : ((lDtl[i].RAP - lDtl[i].mCurrentAmt) / lDtl[i].RAP) * 100m;
            //    lDtl[i].RAP2 = lDtl[i].RAP - (lDtl[i].RAP - lDtl[i].mCurrentAmt);
            //}

            #endregion

            if (lDtl.Count > 0)
            {
                foreach (var x in lDtl)
                {
                    mTotalAmt += x.Amt;
                    mTotalCurrentAmt += x.CurrentAmt;
                    //mTotalDroppingPercentage += x.DroppingPercentage;
                    mTotalOpeningBalanceAmt += x.OpeningBalanceAmt;
                    mTotalOutstandingRAPAmt += x.OutstandingRAPAmt;
                    mTotalOutstandingRAPPercentage += x.OutstandingRAPPercentage;
                    mTotalRAP += x.RAP;
                }

                string mRekCode = string.Empty;
                decimal mSubRAP = 0m, mSubOpeningBalanceAmt = 0m,
                    mSubAmt = 0m, mSubCurrentAmt = 0m, 
                    mOutstandingRAPAmt = 0m, 
                    mSubOutstandingRAPPercentage = 0m;

                foreach(var x in lDtl.OrderBy(o => o.RekCode))
                {
                    if (mRekCode.Length == 0)
                    {
                        mRekCode = x.RekCode;
                        mOutstandingRAPAmt = x.RAP;
                    }

                    if (mRekCode == x.RekCode)
                    {
                        mOutstandingRAPAmt -= x.CurrentAmt; // subtract
                        mSubRAP = x.RAP; // equal
                        mSubOpeningBalanceAmt += x.OpeningBalanceAmt; // sum
                        mSubAmt += x.Amt; // sum
                        mSubCurrentAmt += x.CurrentAmt; // sum

                        x.OutstandingRAPAmt = mOutstandingRAPAmt;
                        x.OutstandingRAPPercentage = (x.RAP == 0m) ? 0m : (mOutstandingRAPAmt / x.RAP) * 100m;

                        mSubOutstandingRAPPercentage = x.OutstandingRAPPercentage; // equal
                    }
                    else
                    {
                        lST.Add(new DroppingRequestSubtotal()
                        {
                            RekCode = mRekCode,
                            RAP = mSubRAP,
                            OpeningBalanceAmt = mSubOpeningBalanceAmt,
                            Amt = mSubAmt,
                            CurrentAmt = mSubCurrentAmt,
                            OutstandingRAPAmt = mOutstandingRAPAmt,
                            OutstandingRAPPercentage = mSubOutstandingRAPPercentage
                        });

                        mOutstandingRAPAmt = x.RAP - x.CurrentAmt;
                        mRekCode = x.RekCode;
                        mSubRAP = x.RAP;
                        mSubOpeningBalanceAmt = x.OpeningBalanceAmt;
                        mSubAmt = x.Amt;
                        mSubCurrentAmt = x.CurrentAmt;

                        x.OutstandingRAPAmt = mOutstandingRAPAmt;
                        x.OutstandingRAPPercentage = (x.RAP == 0m) ? 0m : (mOutstandingRAPAmt / x.RAP) * 100m;

                        mSubOutstandingRAPPercentage = x.OutstandingRAPPercentage;
                    }
                }

                lST.Add(new DroppingRequestSubtotal()
                {
                    RekCode = mRekCode,
                    RAP = mSubRAP,
                    OpeningBalanceAmt = mSubOpeningBalanceAmt,
                    Amt = mSubAmt,
                    CurrentAmt = mSubCurrentAmt,
                    OutstandingRAPAmt = mOutstandingRAPAmt,
                    OutstandingRAPPercentage = mSubOutstandingRAPPercentage
                });

                if (l.Count > 0)
                {
                    foreach (var y in l)
                    {
                        y.TotalAmt = mTotalAmt;
                        y.TotalCurrentAmt = mTotalCurrentAmt;
                        y.TotalDroppingPercentage = mTotalDroppingPercentage;
                        y.TotalOpeningBalanceAmt = mTotalOpeningBalanceAmt;
                        y.TotalOutstandingRAPAmt = mTotalOutstandingRAPAmt;
                        y.TotalOutstandingRAPPercentage = mTotalOutstandingRAPPercentage;
                        y.TotalRAP = mTotalRAP;
                    }
                }
            }

            if(lDtl1.Count > 0)
            {
                foreach(var x in lDtl1)
                {
                    mTotalAmt1 += x.Amt;
                    mTotalCurrentAmt1 += x.CurrentAmt;
                    //mTotalDroppingPercentage1 += x.DroppingPercentage;
                    mTotalOpeningBalanceAmt1 += x.OpeningBalanceAmt;
                    mTotalOutstandingRAPAmt1 += x.OutstandingRAPAmt;
                    mTotalOutstandingRAPPercentage1 += x.OutstandingRAPPercentage;
                    mTotalRAP1 += x.RAP;
                }

                if (l.Count > 0)
                {
                    foreach (var y in l)
                    {
                        y.TotalAmt1 = mTotalAmt1;
                        y.TotalCurrentAmt1 = mTotalCurrentAmt1;
                        //y.TotalDroppingPercentage1 = mTotalDroppingPercentage1;
                        y.TotalDroppingPercentage1 = (mTotalRAP1 == 0m) ? 0m : (mTotalCurrentAmt1 / mTotalRAP1) * 100m;
                        y.TotalOpeningBalanceAmt1 = mTotalOpeningBalanceAmt1;
                        y.TotalOutstandingRAPAmt1 = mTotalOutstandingRAPAmt1;
                        //y.TotalOutstandingRAPPercentage1 = mTotalOutstandingRAPPercentage1;
                        y.TotalOutstandingRAPPercentage1 = (mTotalRAP1 == 0m) ? 0m : (mTotalOutstandingRAPAmt1 / mTotalRAP1) * 100m;
                        y.TotalRAP1 = mTotalRAP1;
                    }
                }
            }

            if (lST.Count > 0)
            {
                decimal mGTRAP = 0m, mGTOpeningBalanceAmt = 0m, mGTAmt = 0m, mGTCurrentAmt = 0m,
                    mGTOutstandingRAPAmt = 0m, mGTOutstandingRAPPercentage = 0m;

                foreach (var x in lST)
                {
                    foreach(var y in lDtl.Where(o => o.RekCode == x.RekCode))
                    {
                        y.SubRAP = x.RAP;
                        y.SubOpeningBalanceAmt = x.OpeningBalanceAmt;
                        y.SubAmt = x.Amt;
                        y.SubCurrentAmt = x.CurrentAmt;
                        y.SubOutstandingRAPAmt = x.OutstandingRAPAmt;
                        y.SubOutstandingRAPPercentage = x.OutstandingRAPPercentage;
                    }
                }

                foreach (var x in lST)
                {
                    mGTRAP += x.RAP;
                    mGTOpeningBalanceAmt += x.OpeningBalanceAmt;
                    mGTAmt += x.Amt;
                    mGTCurrentAmt += x.CurrentAmt;
                    mGTOutstandingRAPAmt += x.OutstandingRAPAmt;
                    mGTOutstandingRAPPercentage += x.OutstandingRAPPercentage;
                }

                lGT.Add(new DroppingRequestGrandTotal()
                {
                    RAP = mGTRAP,
                    OpeningBalanceAmt = mGTOpeningBalanceAmt,
                    Amt = mGTAmt,
                    CurrentAmt = mGTCurrentAmt,
                    OutstandingRAPAmt = mGTOutstandingRAPAmt,
                    //OutstandingRAPPercentage = mGTOutstandingRAPPercentage
                    OutstandingRAPPercentage = mGTRAP == 0m ? 0m : (mGTOutstandingRAPAmt / mGTRAP) * 100m
                });
            }

            #region Detail MNET

            // todo : detail printout

            if (mDocTitle == "MNET")
            {
                var SQLDtlM = new StringBuilder();
                var cmdDtlM = new MySqlCommand();

                SQLDtlM.AppendLine("SELECT A.DocNo, B.DNo, E.ItName, B.Qty, E.InventoryUOMCode AS UoMCode, F.UomName AS UoMName,  ");
                SQLDtlM.AppendLine("I.CurCode, J.CurName, if(B.RemunerationAmt > 0, B.RemunerationAmt, B.DirectCostAmt) AS TotalPrice, B.Remark  ");
                SQLDtlM.AppendLine("FROM tbldroppingrequesthdr A ");
                SQLDtlM.AppendLine("INNER JOIN tbldroppingrequestdtl B ON A.DocNo=B.DocNo ");
                SQLDtlM.AppendLine("INNER JOIN tblprojectimplementationrbphdr C ON B.PRBPDocNo = C.DocNo ");
                SQLDtlM.AppendLine("-- INNER JOIN tblprojectimplementationrbpdtl D ON B.PRBPDNo = D.DNo ");
                SQLDtlM.AppendLine("Inner Join TblItem E On C.ResourceItCode = E.ItCode ");
                SQLDtlM.AppendLine("INNER JOIN tbluom F ON E.InventoryUOMCode=F.UomCode ");
                SQLDtlM.AppendLine("INNER JOIN tblprojectimplementationhdr G ON A.PRJIDocNo=G.DocNo ");
                SQLDtlM.AppendLine("INNER JOIN tblsocontractrevisionhdr H ON G.SOContractDocNo=H.DocNo ");
                SQLDtlM.AppendLine("INNER JOIN tblsocontracthdr I ON H.SOCDocNo=I.DocNo ");
                SQLDtlM.AppendLine("INNER JOIN tblcurrency J ON I.CurCode=J.CurCode ");
                SQLDtlM.AppendLine("WHERE A.DocNo=@DocNo ");

                using (var cnd = new MySqlConnection(Gv.ConnectionString))
                {
                    cnd.Open();
                    cmdDtlM.Connection = cnd;
                    cmdDtlM.CommandText = SQLDtlM.ToString();
                    Sm.CmParam<String>(ref cmdDtlM, "@DocNo", TxtDocNo.Text);

                    var drd = cmdDtlM.ExecuteReader();
                    var cd = Sm.GetOrdinal(drd, new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DNo", "ItName", "Qty", "UoMCode", "UoMName", 
                        //6-9
                        "CurCode", "CurName", "TotalPrice", "Remark"
                    });
                    if (drd.HasRows)
                    {
                        int Row = 0;
                        while (drd.Read())
                        {
                            Row = Row + 1;
                            lDtlMNET.Add(new DroppingRequestDtlMNET()
                            {
                                Row = Row,
                                DocNo = Sm.DrStr(drd, cd[0]),
                                DNo = Sm.DrStr(drd, cd[1]),
                                ItName = Sm.DrStr(drd, cd[2]),
                                Qty = Sm.DrDec(drd, cd[3]),
                                UoMCode = Sm.DrStr(drd, cd[4]),
                                UoMName = Sm.DrStr(drd, cd[5]),
                                CurCode = Sm.DrStr(drd, cd[6]),
                                CurName = Sm.DrStr(drd, cd[7]),
                                TotalPrice = Sm.DrDec(drd, cd[8]),
                                Remark = Sm.DrStr(drd, cd[9])
                            });
                        }
                    }

                    drd.Close();
                }
            }

            #endregion

            #region Detail Sign MNET

            // todo : ttd printout
            if (mDocTitle == "MNET")
            {
                var cmSignMnet = new MySqlCommand();

                var SQLSignMNET = new StringBuilder();
                using (var cnSignMNET = new MySqlConnection(Gv.ConnectionString))
                {
                    cnSignMNET.Open();
                    cmSignMnet.Connection = cnSignMNET;
                    SQLSignMNET.AppendLine("Select A.UserName, A.PosName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d %M %Y') as LastUpDt, A.EmpPict   ");
                    SQLSignMNET.AppendLine("From (   ");

                    SQLSignMNET.AppendLine("	SELECT B.UserName As UserName, 0 As Seq, 'Created By,' As Title, B.Remark AS PosName, Left(A.DocDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict   ");
                    SQLSignMNET.AppendLine("	From TblDroppingRequestHdr A   ");
                    SQLSignMNET.AppendLine("	Inner Join TblUser B On A.CreateBy=B.UserCode   ");
                    SQLSignMNET.AppendLine("	LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'   ");
                    SQLSignMNET.AppendLine("	Where A.DocNo = @DocNo ");

                    SQLSignMNET.AppendLine("	UNION ALL ");
                    SQLSignMNET.AppendLine("	SELECT A.Property1 AS UserName, A.OptCode AS Seq, 'Approved By,' As Title, A.Property2 AS PosName, NULL AS LastUpDt, NULL AS EmpPict   ");
                    SQLSignMNET.AppendLine("	FROM tbloption A ");
                    SQLSignMNET.AppendLine("	WHERE OptCat = 'ReportTitleDroppingRequest' ");

                    SQLSignMNET.AppendLine(") A    ");
                    SQLSignMNET.AppendLine("Order By A.Seq Desc  ");

                    cmSignMnet.CommandText = SQLSignMNET.ToString();
                    Sm.CmParam<String>(ref cmSignMnet, "@DocNo", TxtDocNo.Text);
                    var drSignMNET = cmSignMnet.ExecuteReader();

                    var cSignMNET = Sm.GetOrdinal(drSignMNET, new string[]
                    {
                            //0
                            "UserName",
                            
                            //1-5
                            "Posname","Seq","Title","LastUpDt","EmpPict"
                    });
                    if (drSignMNET.HasRows)
                    {
                        while (drSignMNET.Read())
                        {
                            lSignMNET.Add(new DroppingRequestSignMNET()
                            {
                                UserName = Sm.DrStr(drSignMNET, cSignMNET[0]),
                                Posname = Sm.DrStr(drSignMNET, cSignMNET[1]),
                                Sequence = Sm.DrStr(drSignMNET, cSignMNET[2]),
                                Title = Sm.DrStr(drSignMNET, cSignMNET[3]),
                                LastUpDt = Sm.DrStr(drSignMNET, cSignMNET[4]),
                                EmpPict = Sm.DrStr(drSignMNET, cSignMNET[5]),
                                Space = ""
                            });
                        }
                    }

                    drSignMNET.Close();
                }
            }

            #endregion

            myLists.Add(l);
            myLists.Add(lMNET);
            myLists.Add(lDtl);
            myLists.Add(lDtl1);
            myLists.Add(lDtlMNET);
            myLists.Add(lST);
            myLists.Add(lGT);
            myLists.Add(lSignMNET);

            if (mDocTitle == "MNET")
            {
                Sm.PrintReport("DroppingRequestMNET", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("DroppingRequest", myLists, TableName, false);
                Sm.PrintReport("DroppingRequest2", myLists, TableName, false);
            }
           

            l.Clear(); lDtl.Clear(); lDtl1.Clear(); lST.Clear(); lGT.Clear();
        }

        private void SetLueBCCode(ref DXE.LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select Distinct C.BCCode Col1, C.BCName Col2 ");
            //SQL.AppendLine("From TblBudgetRequestHdr A ");
            //SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.Status = 'A' And A.Yr = @Yr And A.Mth = @Mth ");
            //SQL.AppendLine("    And B.Amt > 0 ");
            SQL.AppendLine("Select Distinct C.BCCode Col1, C.BCName Col2 From TblBudgetSummary A ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode = C.BCCode And A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Where C.ActInd = 'Y' ");
            if (DeptCode.Length > 0)
                SQL.AppendLine("    And A.DeptCode = @DeptCode ");
            if (BCCode.Length > 0)
                SQL.AppendLine("    And C.BCCode = @BCCode ");
            if (Sm.GetLue(LueYr).Length > 0)
                SQL.AppendLine("And A.Yr = @Yr ");
            if (!mIsBudget2YearlyFormat)
            {
                if (Sm.GetLue(LueMth).Length > 0)
                    SQL.AppendLine("And A.Mth = @Mth ");
            }
            
            SQL.AppendLine("Order By C.BCName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        private decimal GetBudgetRequestAmt(string BCCode)
        {
            decimal BCAmt = 0m;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //SQL.AppendLine("Select C.BCCode, Sum(B.Amt) Amt ");
            //SQL.AppendLine("From TblBudgetHdr A  ");
            //SQL.AppendLine("Inner Join TblBudgetDtl B On A.DocNo = B.DocNo  ");
            //SQL.AppendLine("Inner Join TblBudgetRequestDtl C On C.DocNo = B.BudgetRequestDocNo And C.DNo = B.BudgetRequestDNo ");
            //SQL.AppendLine("Inner Join TblBudgetRequesthdr D On C.DocNo = D.DocNo  And D.BudgetDocNo = A.DocNo  ");
            SQL.AppendLine("Select D.BcCode, D.Amt2 As Amt ");
            SQL.AppendLine("From TblBudgetSummary D ");
            SQL.AppendLine("Where D.Yr = @Yr  ");
            if(!mIsBudget2YearlyFormat)
                SQL.AppendLine("And D.Mth = @Mth  ");
            //SQL.AppendLine("And D.CancelInd = 'N'  ");
            SQL.AppendLine("And D.DeptCode = @DeptCode  ");
            SQL.AppendLine("And Find_In_Set(D.BCCode, @BCCode) "); 
            SQL.AppendLine("Group By D.BCCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        BCAmt = Sm.DrDec(dr, c[1]);
                    }
                }
                dr.Close();
            }

            return BCAmt;
        }

        private string GetBudgetRequestRemark(string BCCode)
        {
            string mRemark = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.BCCode, IfNull(B.Remark, A.Remark) Remark ");
            SQL.AppendLine("From TblBudgetRequestHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo ");
            if(!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And A.Mth = @Mth ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DeptCode = @DeptCode ");
            SQL.AppendLine("    And Find_In_Set(B.BCCode, @BCCode) ");
            SQL.AppendLine("Where A.BudgetDocNo is not null ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Remark" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mRemark = Sm.DrStr(dr, c[1]);
                    }
                }
                dr.Close();
            }

            return mRemark;
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotalAmt()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 2).Length != 0)
                {
                    Total += (Sm.GetGrdDec(Grd1, row, 8) + Sm.GetGrdDec(Grd1, row, 9));
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeTotalDept()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0)
                {
                    if (mIsDroppingRequestDeptUseQtyCalculation)
                        Total += Sm.GetGrdDec(Grd2, row, 3) * Sm.GetGrdDec(Grd2, row, 9);
                    else
                        Total += Sm.GetGrdDec(Grd2, row, 3);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Total, 0);
        }


        internal void ValidateOutstandingBudgetCategory(string BCCode, int eRow)
        {
            decimal Total = 0m;
            string BS = string.Empty;
            if (!mIsBudget2YearlyFormat)
            {
                BS = Sm.GetValue("Select Amt2 From TblBudgetSummary Where DeptCode =@Param1 And BcCode =@Param2 And Concat(Yr,Mth) = @Param3; ",
                    Sm.GetLue(LueDeptCode), BCCode, String.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            }
            else
            {
                BS = Sm.GetValue("Select SUM(Amt2) From TblBudgetSummary Where DeptCode =@Param1 And BcCode =@Param2 And Yr = @Param3; ",
                        Sm.GetLue(LueDeptCode), BCCode, Sm.GetLue(LueYr));
  
            }
            decimal BudgetSummary = BS.Length > 0 ? Decimal.Parse(BS) : 0m;

            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                string bcCode = Sm.GetGrdStr(Grd2, row, 1);
                if (bcCode.Length > 0 && (bcCode == BCCode)) //&& (row != eRow)
                {
                    Total = Total + Sm.GetGrdDec(Grd2, row, 3);
                }
            }

            if (Total > BudgetSummary)
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount budget category " + Sm.GetGrdStr(Grd2, eRow, 2) + " (" + Sm.FormatNum(Total, 0) + ") "+Environment.NewLine+" is bigger than budget amount " + Sm.FormatNum(BudgetSummary, 0) + " ");
                Grd2.Cells[eRow, 3].Value = 0m;
            }
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocapprovalSetting ");
            SQL.AppendLine("Where DocType = 'DroppingRequest' ");
            if (TxtPRJIDocNo.Text.Length > 0)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select E.SiteCode ");
                SQL.AppendLine("    From TblProjectImplementationHdr A ");
                SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.Docno And A.DocNo = @Param1 ");
                SQL.AppendLine("    Inner join TblSOContractHdr C On B.SOCDocNo = C.Docno ");
                SQL.AppendLine("    Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr E On D.LOPDocNo = E.Docno ");
                SQL.AppendLine(") ");
            }
            if (Sm.GetLue(LueDeptCode).Length > 0)
            {
                SQL.AppendLine("And DeptCode Is Not Null ");
                SQL.AppendLine("And DeptCode = @Param2 ");
            }
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueDeptCode), string.Empty);
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        internal void SetLueVdBankAccount(ref LookUpEdit Lue, string VdCode, string DNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DNo As Col1, Concat('[', A.BankAcNo, '] ', A.BankAcName, ' ', B.BankName) Col2 ");
            SQL.AppendLine("From TblVendorBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode = B.BankCode ");
            SQL.AppendLine("Where A.VdCode = @VdCode ");
            if(DNo.Length > 0)
                SQL.AppendLine("And A.DNo = @DNo ");
            SQL.AppendLine("Order By A.BankAcNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            //mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            //mIsDroppingRequestForProjectCheckMRExists = Sm.GetParameterBoo("IsDroppingRequestForProjectCheckMRExists");
            //mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            //mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            //mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            //mIsDroppingRequestRemunerationProceedToPayment = Sm.GetParameterBoo("IsDroppingRequestRemunerationProceedToPayment");
            //mIsDroppingRequestDeptUseQtyCalculation = Sm.GetParameterBoo("IsDroppingRequestDeptUseQtyCalculation");
            //mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            //mIsDroppingRequestProcessedByMonthAndYear = Sm.GetParameterBoo("IsDroppingRequestProcessedByMonthAndYear");
            //mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            //mIsDroppingRequestUseMRReceivingPartial = Sm.GetParameterBoo("IsDroppingRequestUseMRReceivingPartial");
            //mIsDroppingRequestWithdraw = Sm.GetParameterBoo("IsDroppingRequestWithdraw");
            //mIsDroppingRequestPRJIMultipleDraftEnabled = Sm.GetParameterBoo("IsDroppingRequestPRJIMultipleDraftEnabled");
            //mIsDroppingRequestUseType = Sm.GetParameterBoo("IsDroppingRequestUseType");
            //mIsDroppingReqAllowToUploadFile = Sm.GetParameterBoo("IsDroppingReqAllowToUploadFile");
            //mIsDroppingReqUploadFileMandatory = Sm.GetParameterBoo("IsDroppingReqUploadFileMandatory");
            //mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            //mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            //mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            //mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            //mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            //mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            //mFormatFTPClient = Sm.GetParameter("FormatFTPClient");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ListItCtCodeForResource', 'IsDroppingRequestForProjectCheckMRExists', 'BankAccountFormat', 'IsFilterBySite', 'IsFilterByDept', ");
            SQL.AppendLine("'IsDroppingRequestRemunerationProceedToPayment', 'IsDroppingRequestDeptUseQtyCalculation', 'IsBudget2YearlyFormat', 'IsDroppingRequestProcessedByMonthAndYear', 'IsPSModuleShowApproverRemarkInfo', ");
            SQL.AppendLine("'IsDroppingRequestUseMRReceivingPartial', 'IsDroppingRequestWithdraw', 'IsDroppingRequestPRJIMultipleDraftEnabled', 'IsDroppingRequestUseType', 'IsDroppingReqAllowToUploadFile', ");
            SQL.AppendLine("'IsDroppingReqUploadFileMandatory', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', 'IsFindMRinformationDroppingRequestActive', 'IsDroppingRequestUseUsageDate', 'IsDroppingRequestUsePurpose' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open(); cm.Connection = cn; cm.CommandText = SQL.ToString(); var dr = cm.ExecuteReader(); var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]); ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDroppingRequestForProjectCheckMRExists": mIsDroppingRequestForProjectCheckMRExists = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsDroppingRequestRemunerationProceedToPayment": mIsDroppingRequestRemunerationProceedToPayment = ParValue == "Y"; break;
                            case "IsDroppingRequestDeptUseQtyCalculation": mIsDroppingRequestDeptUseQtyCalculation = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsDroppingRequestProcessedByMonthAndYear": mIsDroppingRequestProcessedByMonthAndYear = ParValue == "Y"; break;
                            case "IsPSModuleShowApproverRemarkInfo": mIsPSModuleShowApproverRemarkInfo = ParValue == "Y"; break;
                            case "IsDroppingRequestUseMRReceivingPartial": mIsDroppingRequestUseMRReceivingPartial = ParValue == "Y"; break;
                            case "IsDroppingRequestWithdraw": mIsDroppingRequestWithdraw = ParValue == "Y"; break;
                            case "IsDroppingRequestPRJIMultipleDraftEnabled": mIsDroppingRequestPRJIMultipleDraftEnabled = ParValue == "Y"; break;
                            case "IsDroppingRequestUseType": mIsDroppingRequestUseType = ParValue == "Y"; break;
                            case "IsDroppingReqAllowToUploadFile": mIsDroppingReqAllowToUploadFile = ParValue == "Y"; break;
                            case "IsDroppingReqUploadFileMandatory": mIsDroppingReqUploadFileMandatory = ParValue == "Y"; break;
                            case "IsFindMRinformationDroppingRequestActive": mIsFindMRinformationDroppingRequestActive = ParValue == "Y"; break;
                            case "IsDroppingRequestUseUsageDate": mIsDroppingRequestUseUsageDate = ParValue == "Y"; break;
                            case "IsDroppingRequestUsePurpose": mIsDroppingRequestUsePurpose = ParValue == "Y"; break;

                            //string
                            case "ListItCtCodeForResource": mListItCtCodeForResource = ParValue; break;
                            case "BankAccountFormat": mBankAccountFormat = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void LueRequestEdit2(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
            {
                Lue.EditValue = null;
                SetLueVdBankAccount(ref Lue, Sm.GetGrdStr(Grd, e.RowIndex, 4), string.Empty);
            }
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDroppingRequestFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsDroppingReqAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblDroppingRequestFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        private MySqlCommand UpdateDroppingRequestFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDroppingRequestFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private bool IsGrd4ValueNotValid()
        {
            if (Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to upload at least 1 file.");
                return true;
            }
            return false;
        }

        #endregion

        #region Events

        #region Button Clicks

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month"))
                Sm.FormShowDialog(new FrmDroppingRequestDlg(this, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
        }

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project Implementation#", false))
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPICCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDroppingRequestDlg4(this));
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueOption), "IsDroppingRequestUseType");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }

        private void LueBankAcCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode2, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode2), string.Empty);
                if (Sm.GetLue(LueVdCode).Length > 0)
                {
                    SetLueVdBankAccount(ref LueVdBankAcDNo2, Sm.GetLue(LueVdCode), string.Empty);
                    //Sm.RefreshLookUpEdit(LueVdBankAcDNo2, new Sm.RefreshLue3(SetLueVdBankAccount), Sm.GetLue(LueVdCode2), string.Empty);
                }
                else
                {
                    SetLueVdBankAccount(ref LueVdBankAcDNo2, string.Empty, string.Empty);
                    //Sm.RefreshLookUpEdit(LueVdBankAcDNo2, new Sm.RefreshLue3(SetLueVdBankAccount), string.Empty, string.Empty);
                }
            }
        }

        private void LueVdCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueVdCode_Leave(object sender, EventArgs e)
        {
            if (LueVdCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueVdCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueVdCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueVdCode.GetColumnValue("Col2");
                }
                LueVdCode.Visible = false;
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
        }

        private void LueBCCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueBCCode_Leave(object sender, EventArgs e)
        {
            if (LueBCCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueBCCode).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value =
                    Grd2.Cells[fCell.RowIndex, 2].Value = null;

                    Grd2.Cells[fCell.RowIndex, 3].Value = 0m;
                    Grd2.Cells[fCell.RowIndex, 4].Value = null;
                    ComputeTotalDept();
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueBCCode);
                    Grd2.Cells[fCell.RowIndex, 2].Value = LueBCCode.GetColumnValue("Col2");

                    Grd2.Cells[fCell.RowIndex, 3].Value = GetBudgetRequestAmt(Sm.GetLue(LueBCCode));
                    Grd2.Cells[fCell.RowIndex, 4].Value = GetBudgetRequestRemark(Sm.GetLue(LueBCCode));
                    ComputeTotalDept(); 
                    ValidateOutstandingBudgetCategory(Sm.GetGrdStr(Grd2, fCell.RowIndex, 1), fCell.RowIndex);
                }
                LueBCCode.Visible = false;
            }
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.GetLue(LueDeptCode).Length > 0)
                {
                    Sm.ClearGrd(Grd2, true);
                    SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
                    ComputeTotalDept();
                }

                if (TxtPRJIDocNo.Text.Length > 0)
                {
                    TxtPRJIDocNo.EditValue = null;
                    Sm.ClearGrd(Grd1, true);
                    ComputeTotalAmt();
                }
            }
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.GetLue(LueDeptCode).Length > 0)
                {
                    Sm.ClearGrd(Grd2, true);
                    SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
                    ComputeTotalDept();
                }

                if (TxtPRJIDocNo.Text.Length > 0)
                {
                    TxtPRJIDocNo.EditValue = null;
                    Sm.ClearGrd(Grd1, true);
                    ComputeTotalAmt();
                }
            }
        }

        private void LueVdCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode2, new Sm.RefreshLue2(Sl.SetLueVdCode2), string.Empty);
                if (Sm.GetLue(LueVdCode2).Length > 0)
                {
                    SetLueVdBankAccount(ref LueVdBankAcDNo, Sm.GetLue(LueVdCode2), string.Empty);
                    //Sm.RefreshLookUpEdit(LueVdBankAcDNo, new Sm.RefreshLue3(SetLueVdBankAccount), Sm.GetLue(LueVdCode2), string.Empty);
                }
                else
                {
                    SetLueVdBankAccount(ref LueVdBankAcDNo, string.Empty, string.Empty);
                    //Sm.RefreshLookUpEdit(LueVdBankAcDNo, new Sm.RefreshLue3(SetLueVdBankAccount), string.Empty, string.Empty);
                }
            }
        }

        private void LueVdCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueVdCode2_Leave(object sender, EventArgs e)
        {
            if (LueVdCode2.Visible && fAccept && fCell.ColIndex == 11)
            {
                if (Sm.GetLue(LueVdCode2).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 10].Value =
                    Grd2.Cells[fCell.RowIndex, 11].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 10].Value = Sm.GetLue(LueVdCode2);
                    Grd2.Cells[fCell.RowIndex, 11].Value = LueVdCode2.GetColumnValue("Col2");
                }
                LueVdCode2.Visible = false;
            }
        }

        private void LueVdBankAcDNo_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdBankAcDNo, new Sm.RefreshLue3(SetLueVdBankAccount), Sm.GetLue(LueVdCode2), string.Empty);
            }
        }

        private void LueVdBankAcDNo_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueVdBankAcDNo_Leave(object sender, EventArgs e)
        {
            if (LueVdBankAcDNo.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueVdBankAcDNo).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 12].Value =
                    Grd2.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LueVdBankAcDNo);
                    Grd2.Cells[fCell.RowIndex, 13].Value = LueVdBankAcDNo.GetColumnValue("Col2");
                }
                LueVdBankAcDNo.Visible = false;
            }
        }

        private void LueVdBankAcDNo2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdBankAcDNo2, new Sm.RefreshLue3(SetLueVdBankAccount), Sm.GetLue(LueVdCode), string.Empty);
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1,7,14 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueFontSize_EditValueChanged_1(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void LueVdBankAcDNo2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueVdBankAcDNo2_Leave(object sender, EventArgs e)
        {
            if (LueVdBankAcDNo2.Visible && fAccept && fCell.ColIndex == 17)
            {
                if (Sm.GetLue(LueVdBankAcDNo2).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 16].Value =
                    Grd1.Cells[fCell.RowIndex, 17].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 16].Value = Sm.GetLue(LueVdBankAcDNo2);
                    Grd1.Cells[fCell.RowIndex, 17].Value = LueVdBankAcDNo2.GetColumnValue("Col2");
                }
                LueVdBankAcDNo2.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class BudgetDepartment
        {
            public string BCCode { get; set; }
            public decimal DroppingAmt { get; set; }
            public decimal Qty { get; set; }
        }

        private class BudgetDepartment2
        {
            public string BCCode { get; set; }
            public decimal RequestAmt { get; set; }
            public decimal DroppingAmt { get; set; }
        }

        private class PRJIAmount
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class DroppingRequestHdr
        {
            public string Mth { get; set; }
            public string DocNo { get; set; }
            public string CompanyName { get; set; }
            public string SiteName { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public string PICName { get; set; }
            public string BankAccount { get; set; }
            public string ProcessInd { get; set; }
            public string SignDt { get; set; }
            public string SignName1 { get; set; }
            public string SignName2 { get; set; }
            public string SignName3 { get; set; }
            public string Yr { get; set; }
            public string Mth2 { get; set; }
            public string ProjectName { get; set; }
            public string SignDt2 { get; set; }
            public string HOSiteName { get; set; }

            public decimal TotalRAP { get; set; }
            public decimal TotalOpeningBalanceAmt { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalCurrentAmt { get; set; }
            public decimal TotalDroppingPercentage { get; set; }
            public decimal TotalOutstandingRAPAmt { get; set; }
            public decimal TotalOutstandingRAPPercentage { get; set; }
            public string YearBTL { get; set; }

            public decimal TotalRAP1 { get; set; }
            public decimal TotalOpeningBalanceAmt1 { get; set; }
            public decimal TotalAmt1 { get; set; }
            public decimal TotalCurrentAmt1 { get; set; }
            public decimal TotalDroppingPercentage1 { get; set; }
            public decimal TotalOutstandingRAPAmt1 { get; set; }
            public decimal TotalOutstandingRAPPercentage1 { get; set; }
        }

        private class DroppingRequestHdrMNET 
        {
            public string DocNo { get; set; }
            public string CreateBy { get; set; }
            public string ProjectType { get; set; }
            public string ProjectName { get; set; }
            public string UsageDt { get; set; }
            public string Purpose { get; set; }
            public string DocDt { get; set; }
            public string CCCode { get; set; }
            public string CCName { get; set; }
            public string PRJIDocNo { get; set; }
            public string SOCDocNo { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string Status { get; set; }
        }

        private class DroppingRequestDtl
        {
            public string No { get; set; }
            public string Detail { get; set; }
            public decimal RAP { get; set; }
            public decimal RAP2 { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal CurrentAmt { get; set; }
            public decimal mCurrentAmt { get; set; }
            public string RekCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal DroppingPercentage { get; set; }
            public decimal OutstandingRAPAmt { get; set; }
            public decimal OutstandingRAPPercentage { get; set; }

            //subtotal
            public decimal SubRAP { get; set; }
            public decimal SubOpeningBalanceAmt { get; set; }
            public decimal SubAmt { get; set; }
            public decimal SubCurrentAmt { get; set; }
            public decimal SubOutstandingRAPAmt { get; set; }
            public decimal SubOutstandingRAPPercentage { get; set; }
        }

        private class DroppingRequestDtl1
        {
            public string No { get; set; }
            public string Detail { get; set; }
            public decimal RAP { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal CurrentAmt { get; set; }
            public string Remark { get; set; }
            public decimal DroppingPercentage { get; set; }
            public decimal OutstandingRAPAmt { get; set; }
            public decimal OutstandingRAPPercentage { get; set; }
        }

        private class DroppingRequestDtlMNET
        {
            public int Row { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string UoMCode { get; set; }
            public string UoMName { get; set; }
            public string CurCode { get; set; }
            public string CurName { get; set; }
            public decimal TotalPrice { get; set; }
            public string Remark { get; set; }
        }

        private class DroppingRequestSubtotal
        {
            public string RekCode { get; set; }
            public decimal RAP { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal CurrentAmt { get; set; }
            public decimal OutstandingRAPAmt { get; set; }
            public decimal OutstandingRAPPercentage { get; set; }
        }

        private class DroppingRequestGrandTotal
        {
            public decimal RAP { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal CurrentAmt { get; set; }
            public decimal OutstandingRAPAmt { get; set; }
            public decimal OutstandingRAPPercentage { get; set; }
        }

        private class DroppingRequestSignMNET 
        {
            public string UserName { get; set; }
            public string Posname { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string EmpPict { get; set; }
            public string Space { get; set; }
        }

        #endregion

        #region Misc
        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }
        #endregion

    }
}
