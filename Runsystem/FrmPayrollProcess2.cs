﻿#region Update 
/*
    KMI
    06/05/2017 [TKG] Mem-proses tunjangan cuti besar untuk KMI
    18/05/2017 [TKG] ot libur dikurangi 1 jam apabila jumlah ot >= 4 jam
    23/05/2017 [TKG] untuk kmi, take home pay tidak dikurangi fixed deduction
    12/07/2017 [TKG] untuk kmi, uang lapangan diberikan apabila >= minimum jam kerja
    21/07/2017 [TKG] untuk kmi, ot rutin berdasarkan jam aktual dan jumlah OT bisa menggantikan leave.
    22/07/2017 [TKG] ditambah validasi untuk working schedule tanpa istirahat 
    21/08/2017 [TKG] perubahan perhitungan meal/transport/fieldassignment berdasarkan site.
    25/08/2017 [TKG] perubahan perhitungan meal/transport/fieldassignment berdasarkan multiple allowance.
    15/10/2017 [TKG] Meal allowance bisa diambil lbh dari 1 kode allowance
    30/10/2017 [TKG] 
        uang makan diberikan apabila >= minimum jam kerja,
        uang makan tidak akan diberikan apabila masuk kerja setelah jam istirahat
    19/08/2019 [TKG] Hitungan Proporsional Salary, Tunjangan Jabatan, dan Tunjangan Daerah
    23/08/2019 [TKG] menggunakan department di tanggal akhir periode sebagai patokan proses.
    30/08/2019 [TKG] berdasarkan payroll tax formula di payroll group, perhitungan pajak menggunakan formula gross atau gross up.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcess2 : Form
    {
        #region Field, Property

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,

            mNeglectLeaveCode = "002",
            mOTRequestType = "1",
            mCoordinatorLeaveCode = "007",
            mDisasterLeaveCode = "014",
            mGoHomeEarlyLeaveCode = "017",
            mMaternityLeaveCode = "015",
            mMiscarriageLeaveCode = "016",
            mOperatorLeaveCode = "001",
            mEmpSystemTypeHarian = "1",
            mEmpSystemTypeBorongan = "2",
            mEmpSystemTypeAllIn = "3",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mPayrunPeriod2Mingguan = "2",
            mADCodeIncPerformance = "03",
            mADCodeIncPerformance2 = "19",
            mADCodeFieldAssignment = "02",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mADCodePresenceReward = "'02', '04'",
            mADCodePresenceReward2Mingguan = "04",
            mADCodePresenceRewardBulanan = "02",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSPCodeForPension = "BPJS-PNS",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "1",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mWSCodesHolidayWithExtraFooding = string.Empty,
            mADCodeEmploymentPeriod = string.Empty,
            mADCodeSalary = string.Empty,
            mPayrollTaxFormula = string.Empty;

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mGoHomeEarlyLeaveHr1 = 4.5m,
            mGoHomeEarlyLeaveHr2 = 5m,
            mGoHomeEarlyLeavePerc1 = 50m,
            mGoHomeEarlyLeavePerc2 = 75m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 8m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mMealMinWorkDuration = 0m
            ;

        private bool
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsPPProcessAllOutstandingSS = false;
            

        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<EmployeeVarAllowance> mlEmployeeVarAllowance = null;
        private List<EmployeeVarAllowanceMultiplePeriod> mlEmployeeVarAllowanceMultiplePeriod = null;
        private List<GradeLevelVarAllowance> mlGradeLevelVarAllowance = null;
        private List<GradeLevelPresenceReward> mlGradeLevelPresenceReward = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;

        #endregion

        #region Constructor

        public FrmPayrollProcess2(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mOTRequestType = Sm.GetParameter("OTRequestType");
            mWSCodesHolidayWithExtraFooding = Sm.GetParameter("WSCodesHolidayWithExtraFooding");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            mWorkingHr = Sm.GetParameterDec("WorkingHr");
            mNoWorkDay2Mingguan = Sm.GetParameterDec("NoWorkDay2Mingguan");
            mNoWorkDayBulanan = Sm.GetParameterDec("NoWorkDayBulanan");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            mHOWorkingDayPerMth = Sm.GetParameterDec("HOWorkingDayPerMth");
            mSiteWorkingDayPerMth = Sm.GetParameterDec("SiteWorkingDayPerMth");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
            mDisasterLeaveCode = Sm.GetParameter("DisasterLeaveCode");
            mGoHomeEarlyLeaveCode = Sm.GetParameter("GoHomeEarlyLeaveCode");
            mMaternityLeaveCode = Sm.GetParameter("MaternityLeaveCode");
            mMiscarriageLeaveCode = Sm.GetParameter("MiscarriageLeaveCode");
            mOperatorLeaveCode = Sm.GetParameter("OperatorLeaveCode");
            mEmpSystemTypeHarian = Sm.GetParameter("EmpSystemTypeHarian");
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mEmpSystemTypeAllIn = Sm.GetParameter("EmpSystemTypeAllIn");
            mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            mPayrunPeriod2Mingguan = Sm.GetParameter("PayrunPeriod2Mingguan");
            mADCodeIncPerformance = Sm.GetParameter("ADCodeIncPerformance");
            mADCodeIncPerformance2 = Sm.GetParameter("ADCodeIncPerformance2");
            mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            mADCodeFieldAssignment = Sm.GetParameter("ADCodeFieldAssignment");
            mADCodePresenceReward = Sm.GetParameter("ADCodePresenceReward");
            mADCodePresenceReward2Mingguan = Sm.GetParameter("ADCodePresenceReward2Mingguan");
            mADCodePresenceRewardBulanan = Sm.GetParameter("ADCodePresenceRewardBulanan");
            mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            mSSPCodeForPension = Sm.GetParameter("SSPCodeForPension");
            mSSForRetiring = Sm.GetParameter("SSForRetiring");
            mSSOldAgeInsurance = Sm.GetParameter("SSOldAgeInsurance");
            mIsWorkingHrBasedOnSchedule = Sm.GetParameterBoo("IsWorkingHrBasedOnSchedule");
            mIsUseLateValidation = Sm.GetParameterBoo("IsUseLateValidation");
            mIsUseOTReplaceLeave = Sm.GetParameterBoo("IsUseOTReplaceLeave");
            mIsOTRoutineRounding = Sm.GetParameterBoo("IsOTRoutineRounding");
            mIsOTBasedOnOTRequest = Sm.GetParameterBoo("IsOTBasedOnOTRequest");
            mIsPayrollProcessUseSiteWorkingDayPerMth = Sm.GetParameterBoo("IsPayrollProcessUseSiteWorkingDayPerMth");
            mHoliday1stHrForHO = Sm.GetParameterDec("Holiday1stHrForHO");
            mHoliday1stHrForSite = Sm.GetParameterDec("Holiday1stHrForSite");
            mHoliday2ndHrForHO = Sm.GetParameterDec("Holiday2ndHrForHO");
            mHoliday2ndHrForSite = Sm.GetParameterDec("Holiday2ndHrForSite");
            mHoliday1stHrIndex = Sm.GetParameterDec("Holiday1stHrIndex");
            mHoliday2ndHrIndex = Sm.GetParameterDec("Holiday2ndHrIndex");
            mHoliday3rdHrIndex = Sm.GetParameterDec("Holiday3rdHrIndex");
            mNonNPWPTaxPercentage = Sm.GetParameterDec("NonNPWPTaxPercentage");
            mADCodeEmploymentPeriodYr = Sm.GetParameterDec("ADCodeEmploymentPeriodYr");
            mADCodeEmploymentPeriod = Sm.GetParameter("ADCodeEmploymentPeriod");
            mADCodeSalary = Sm.GetParameter("ADCodeSalary");
            mIsOTHolidayDeduct1Hr = Sm.GetParameterBoo("IsOTHolidayDeduct1Hr");
            mFieldAssignmentMinWorkDuration = Sm.GetParameterDec("FieldAssignmentMinWorkDuration");
            mMealMinWorkDuration = Sm.GetParameterDec("MealMinWorkDuration");
            mIsPPProcessAllOutstandingSS = Sm.GetParameterBoo("IsPPProcessAllOutstandingSS");
        }

        private void GetValue()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(EmployerPerc) ");
            SQL.AppendLine("From TblSS ");
            SQL.AppendLine("Where Find_In_Set(");
            SQL.AppendLine("    IfNull(SSCode, ''), ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSCodeEmployment'), '') ");
            SQL.AppendLine("    );");

            var SSEmploymentErPerc = Sm.GetValue(SQL.ToString());
            if (SSEmploymentErPerc.Length > 0) mSSEmploymentErPerc = decimal.Parse(SSEmploymentErPerc);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc ");
            SQL.AppendLine("From TblSS ");
            SQL.AppendLine("Where SSCode In (");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance'), '') ");
            SQL.AppendLine("    );");

            var SSOldAgeEePerc = Sm.GetValue(SQL.ToString());
            if (SSOldAgeEePerc.Length > 0) mSSOldAgeEePerc = decimal.Parse(SSOldAgeEePerc);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc ");
            SQL.AppendLine("From TblSS ");
            SQL.AppendLine("Where SSCode In (");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSForRetiring'), '') ");
            SQL.AppendLine("    );");

            var SSPensionEePerc = Sm.GetValue(SQL.ToString());
            if (SSPensionEePerc.Length > 0) mSSPensionEePerc = decimal.Parse(SSPensionEePerc);

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-10
                        "Join Date",
                        "Resign Date",
                        "Employment" + Environment.NewLine + "Status",
                        "Type",
                        "Grade Level",
                        
                        //11-14
                        "Period",
                        "Group",
                        "Site",
                        "Invalid Department"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 180, 200, 
                        
                        //6-10
                        100, 100, 130, 130, 150,

                        //11-14
                        130, 150, 150, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0, 14 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mSystemType = string.Empty; 
            mPayrunPeriod = string.Empty;
            mPGCode = string.Empty;
            mSiteCode = string.Empty;
            mHOInd = true;

            if (mlEmployeeVarAllowance.Count > 0)
                mlEmployeeVarAllowance.Clear();

            if (mSalaryInd=="2" && mlEmployeeVarAllowanceMultiplePeriod.Count > 0)
                mlEmployeeVarAllowanceMultiplePeriod.Clear();

            if (mlGradeLevelVarAllowance.Count > 0)
                mlGradeLevelVarAllowance.Clear();

            if (mlGradeLevelPresenceReward.Count > 0)
                mlGradeLevelPresenceReward.Clear();

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.Clear();

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDeptCode, TxtSystemType, TxtPayrunPeriod, TxtPGCode, TxtSiteCode,
                DteStartDt, DteEndDt 
            });
            ChkDelData.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
            SQL.AppendLine("From TblPayrun ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("Order By Concat(PayrunCode, ' : ', PayrunName);");
            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            if (Sm.IsLueEmpty(LuePayrunCode, "Payrun")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo1();
                ShowPayrunInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPayrunInfo1()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, C.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, D.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, E.PGName, A.SiteCode, F.SiteName, F.HOInd, ");
            SQL.AppendLine("A.StartDt, A.EndDt, E.PayrollTaxFormula ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.SystemType=C.OptCode And C.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption D On A.PayrunPeriod=D.OptCode And D.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr E On A.PGCode=E.PGCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode;");

            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DeptCode", 
                        
                        //1-5
                        "DeptName", 
                        "SystemType", 
                        "SystemTypeDesc", 
                        "PayrunPeriod", 
                        "PayrunPeriodDesc", 
                        
                        //6-10
                        "PGCode", 
                        "PGName",
                        "SiteCode",
                        "SiteName",
                        "HOInd",
                        
                        //11-13
                        "StartDt", 
                        "EndDt",
                        "PayrollTaxFormula"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mDeptCode = Sm.DrStr(dr, c[0]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[1]);
                        mSystemType = Sm.DrStr(dr, c[2]);
                        TxtSystemType.EditValue = Sm.DrStr(dr, c[3]);
                        mPayrunPeriod = Sm.DrStr(dr, c[4]);
                        TxtPayrunPeriod.EditValue = Sm.DrStr(dr, c[5]);
                        mPGCode = Sm.DrStr(dr, c[6]);
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[7]);
                        mSiteCode = Sm.DrStr(dr, c[8]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[9]);
                        mHOInd = Sm.DrStr(dr, c[10]) == "Y";
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[11]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[12]));
                        mPayrollTaxFormula = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowPayrunInfo2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblEmployee ");
            SQL.AppendLine("            Where EmpCode=X.EmpCode ");
            SQL.AppendLine("            And JoinDt<=@EndDt ");
            SQL.AppendLine("            And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@StartDt)) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 ");
            SQL.AppendLine("        On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt",  
                    
                    //6-10
                    "ResignDt", "EmploymentStatusDesc", "SystemTypeDesc", "GrdLvlName", "PayrunPeriodDesc", 
                    
                    //11-12
                    "PGName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Grd.Cells[Row, 14].Value = false;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 14 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePayrunCode, "Payrun") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPayrunNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of employees is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPayrunNotValid()
        {
            return
                Sm.IsDataExist(
                    "Select PayrunCode From TblPayrun Where PayrunCode=@Param And IfNull(Status, 'O')='C';",
                    Sm.GetLue(LuePayrunCode),
                    "Payrun Code : " + Sm.GetLue(LuePayrunCode) + Environment.NewLine +
                    "Payrun Name : " + LuePayrunCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This payrun already closed." + Environment.NewLine +
                    "You need to cancel it's voucher request."
                );
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var cm = new MySqlCommand();

            if (!ChkDelData.Checked)
            {
                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r< Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                        if ((Sm.GetGrdBool(Grd1, r, 0) || Sm.GetGrdBool(Grd1, r, 14)) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(EmpCode=@EmpCode" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode" + r.ToString(), EmpCode);
                        }
                    }
                    if (Filter.Length>0) 
                        Filter = " And (" + Filter + ") ";
                    else
                        Filter = " And 1=0 ";
                }
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode " + Filter + ";") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode " + Filter + ";");

            if (ChkDelData.Checked)
            {
                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select DocNo From TblProductionIncentiveHdr ");
                SQL.AppendLine("    Where CancelInd='Y' And DocNo=T.DocNo ");
                SQL.AppendLine(");");

                SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select DocNo From TblProductionIncentiveHdr ");
                SQL.AppendLine("    Where CancelInd='N' And DocNo=T.DocNo ");
                SQL.AppendLine(");");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");
            }
            else
            {
                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblEmpSSListHdr ");
                SQL.AppendLine("    Where CancelInd='Y' ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblEmpSSListHdr ");
                SQL.AppendLine("    Where CancelInd='N' ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblProductionIncentiveHdr ");
                SQL.AppendLine("    Where CancelInd='Y' ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblProductionIncentiveHdr ");
                SQL.AppendLine("    Where CancelInd='N' ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
                SQL.AppendLine("And T.CancelInd='N' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And T.Docno In (");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
                SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref Result2 r)
        { 
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Insert Into TblPayrollProcess1 ");
            SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, NPWP, ");
            SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
            SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
            SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
            SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Regional, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
            SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, ");
            SQL.AppendLine("SalaryAdjustment, Tax, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @NPWP, ");
            SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
	        SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
	        SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, @EmploymentPeriodAllowance, ");
	        SQL.AppendLine("@IncEmployee, @IncMinWages, @IncProduction, @IncPerformance, @PresenceReward, ");
            SQL.AppendLine("@HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @Functional, @Regional, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
            SQL.AppendLine("@DedEmployee, @DedProduction, @DedProdLeave, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, ");
	        SQL.AppendLine("@SalaryAdjustment, @Tax, @Amt, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblProductionIncentiveHdr ");
            SQL.AppendLine("    Where CancelInd='Y' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblProductionIncentiveDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblProductionIncentiveHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "NPWP", r.NPWP);
            Sm.CmParam<String>(ref cm, "PTKP", r.PTKP);
	        Sm.CmParam<Decimal>(ref cm, "Salary", r.Salary);
	        Sm.CmParam<Decimal>(ref cm, "WorkingDay", r.WorkingDay);
	        Sm.CmParam<Decimal>(ref cm, "PLDay", r.PLDay);
	        Sm.CmParam<Decimal>(ref cm, "PLHr", r.PLHr);
	        Sm.CmParam<Decimal>(ref cm, "PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "UPLDay", r.UPLDay);
	        Sm.CmParam<Decimal>(ref cm, "UPLHr", r.UPLHr);
	        Sm.CmParam<Decimal>(ref cm, "UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "OT1Hr", r.OT1Hr);
	        Sm.CmParam<Decimal>(ref cm, "OT2Hr", r.OT2Hr);
	        Sm.CmParam<Decimal>(ref cm, "OTHolidayHr", r.OTHolidayHr);
	        Sm.CmParam<Decimal>(ref cm, "OT1Amt", r.OT1Amt);
	        Sm.CmParam<Decimal>(ref cm, "OT2Amt", r.OT2Amt);
	        Sm.CmParam<Decimal>(ref cm, "OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<Decimal>(ref cm, "NonTaxableFixAllowance", r.NonTaxableFixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "TaxableFixAllowance", r.TaxableFixAllowance);
            Sm.CmParam<Decimal>(ref cm, "FixAllowance", r.FixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "EmploymentPeriodAllowance", r.EmploymentPeriodAllowance);
	        Sm.CmParam<Decimal>(ref cm, "IncEmployee", r.IncEmployee);
	        Sm.CmParam<Decimal>(ref cm, "IncMinWages", r.IncMinWages);
	        Sm.CmParam<Decimal>(ref cm, "IncProduction", r.IncProduction);
	        Sm.CmParam<Decimal>(ref cm, "IncPerformance", r.IncPerformance);
	        Sm.CmParam<Decimal>(ref cm, "PresenceReward", r.PresenceReward);
	        Sm.CmParam<Decimal>(ref cm, "HolidayEarning", r.HolidayEarning);
	        Sm.CmParam<Decimal>(ref cm, "ExtraFooding", r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "FieldAssignment", r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "Transport", r.Transport);
            Sm.CmParam<Decimal>(ref cm, "Meal", r.Meal);
            Sm.CmParam<Decimal>(ref cm, "Functional", r.Functional);
            Sm.CmParam<Decimal>(ref cm, "Regional", r.Regional);
	        Sm.CmParam<Decimal>(ref cm, "SSEmployerHealth", r.SSEmployerHealth);
	        Sm.CmParam<Decimal>(ref cm, "SSEmployerEmployment", r.SSEmployerEmployment);
            Sm.CmParam<Decimal>(ref cm, "SSEmployerPension", r.SSEmployerPension);
            Sm.CmParam<Decimal>(ref cm, "NonTaxableFixDeduction", r.NonTaxableFixDeduction);
            Sm.CmParam<Decimal>(ref cm, "TaxableFixDeduction", r.TaxableFixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "FixDeduction", r.FixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "DedEmployee", r.DedEmployee);
	        Sm.CmParam<Decimal>(ref cm, "DedProduction", r.DedProduction);
	        Sm.CmParam<Decimal>(ref cm, "DedProdLeave", r.DedProdLeave);
            Sm.CmParam<Decimal>(ref cm, "EmpAdvancePayment", r.EmpAdvancePayment);
            Sm.CmParam<Decimal>(ref cm, "SSEmployeeHealth", r.SSEmployeeHealth);
	        Sm.CmParam<Decimal>(ref cm, "SSEmployeeEmployment", r.SSEmployeeEmployment);
            Sm.CmParam<Decimal>(ref cm, "SSEmployeePension", r.SSEmployeePension);
	        Sm.CmParam<Decimal>(ref cm, "SalaryAdjustment", r.SalaryAdjustment);
	        Sm.CmParam<Decimal>(ref cm, "Tax", r.Tax);
            Sm.CmParam<Decimal>(ref cm, "Amt", r.Amt);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess2(ref Result1 r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPayrollProcess2 ");
            SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
            SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, ");
            SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
            SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
            SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
            SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
            SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, ProductionWages, ");
            SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
            SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
            SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
            SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, DedProduction, ");
            SQL.AppendLine("DedProdLeave, DeptCodeActual, CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, ");
            SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
            SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
            SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
            SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
            SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @ProductionWages, ");
            SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
            SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
            SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
            SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
            SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @DedProduction, ");
            SQL.AppendLine("@DedProdLeave, @DeptCodeActual, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
            Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
            Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
            Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
            Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
            Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
            Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
            Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
            Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
            Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
            Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
            Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
            Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
            Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
            Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
            Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
            Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
            Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
            Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
            Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
            Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
            Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
            Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
            Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
            Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
            Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
            Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
            Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
            Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
            Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
            Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
            Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
            Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
            Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "Transport", r.Transport);
            Sm.CmParam<Decimal>(ref cm, "Meal", r.Meal);
            Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
            Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
            Sm.CmParam<String>(ref cm, "@DeptCodeActual", r.DeptCodeActual);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "Yr", x.Yr);
            Sm.CmParam<String>(ref cm, "Mth", x.Mth);
            Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeletePayrollProcess2()
        {
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 14) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + r.ToString(), EmpCode);
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " And 1=0 ";
            }


            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode " + Filter + ";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            return cm;
        }

        #endregion

        #region Process Data

        private void ProcessData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI = new List<TI>();
            var lTI2 = new List<TI2>();

            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI(ref lTI);
            ProcessTI2(ref lTI2);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);

            #endregion

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r => 
                    {
                        if (mSalaryInd == "1" && mSystemType == mEmpSystemTypeAllIn)
                            Process4ForAllIn(ref r);
                        else
                            Process4(ref r);
                    });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI, ref lTI2); });

            #region Save Data

            var cml = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());
            lResult2.ForEach(r => { cml.Add(SavePayrollProcess1(ref r)); });
            lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });
            
            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            cml.Add(DeletePayrollProcess2());

            Sm.ExecCommands(cml);

            #endregion

            LuePayrunCode.EditValue = null;
            ClearData();
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (Grd1.Rows.Count >= 1 && lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = Sm.GetLue(LuePayrunCode);
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty; 
                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;

                            foreach (var i in 
                                lEmployeePPS
                                    .Where(Index => Index.EmpCode == EmpCodeTemp)
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, i.StartDt)>=0)
                                {
                                    DeptCodeTemp = i.DeptCode;
                                    GrdLvlCodeTemp = i.GrdLvlCode;
                                    EmploymentStatusTemp = i.EmploymentStatus;
                                    SystemTypeTemp = i.SystemType;
                                    PayrunPeriodTemp = i.PayrunPeriod;
                                    PGCodeTemp = i.PGCode;
                                    SiteCodeTemp = i.SiteCode;
                                    PosCodeTemp = i.PosCode;
                                    HOIndTemp = i.HOInd;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    DeptCodeActual = DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    IncEmployee = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });

                //lResult1.ForEach(A =>
                //{
                    
                //    A.LatestPaidDt = lEmpPaidDt.FirstOrDefault(
                //        x => A.EmpCode.Equals(x.EmpCode??"") && A.Dt.Equals(x.Dt??"")
                //        ).Dt??"";
                //});
                
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region ExtraFooding

            var lExtraFooding = new List<ExtraFooding>();
            ProcessExtraFooding(ref lExtraFooding);
            if (lExtraFooding.Count > 0)
            {
                foreach (var ef in lExtraFooding)
                {
                    foreach (var r in lResult1.Where(Index => 
                        Index.WSCode == ef.WSCode && 
                        Index.GrdLvlCode == ef.GrdLvlCode))
                        r._ExtraFooding = ef.Amt;
                }
                lLeave.Clear();
            }

            #endregion

            #region FieldAssignment

            if (mSalaryInd == "2")
            {
                var lFieldAssignment = new List<FieldAssignment>();
                ProcessFieldAssignment(ref lFieldAssignment);
                if (lFieldAssignment.Count > 0)
                {
                    foreach (var fa in lFieldAssignment)
                    {
                        foreach (var r in lResult1.Where(Index =>
                            Index.WSCode == fa.WSCode &&
                            Index.EmpCode == fa.EmpCode))
                            r._FieldAssignment = fa.Amt;
                            
                    }
                    lFieldAssignment.Clear();
                }
            }

            #endregion

            #region Grade Level

            var lGradeLevel = new List<GradeLevel>();
            ProcessGradeLevel(ref lGradeLevel);
            if (lGradeLevel.Count > 0)
            {
                foreach (var gl in lGradeLevel)
                {
                    foreach (var r in lResult1.Where(Index => Index.GrdLvlCode == gl.GrdLvlCode))
                    {
                        r.BasicSalary = gl.BasicSalary;
                        r.BasicSalary2 = gl.BasicSalary2;
                    }
                }
                lGradeLevel.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;
                        //r.LatestPaidDt = e.LatestPaidDt;
                        r._IncPerformance = e.IncPerformance;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0)
                            r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0)
                                r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0)
                        {
                            //if (Sm.CompareDtTm(r.Dt, r.LatestPaidDt) <=0)
                                r.ProcessInd = false;
                        }
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            r.SalaryAD = esl.SalaryAD;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region Employee Variable Allowance

            ProcessEmployeeVarAllowance(ref mlEmployeeVarAllowance);
           
            #endregion

            #region Employee Variable Allowance Multiple Period

            if (mSalaryInd == "2")
                ProcessEmployeeVarAllowanceMultiplePeriod(ref mlEmployeeVarAllowanceMultiplePeriod);

            #endregion

            #region Employee Grade Level Variable Allowance

            ProcessGradeLevelVarAllowance(ref mlGradeLevelVarAllowance);

            #endregion

            #region Premi Hadir

            if (mSalaryInd=="1") ProcessGradeLevelPresenceReward(ref mlGradeLevelPresenceReward);

            #endregion

            #region Production Wages

            var lProductionWages = new List<ProductionWages>();
            ProcessProductionWages(ref lProductionWages);
            if (lProductionWages.Count > 0)
            {
                foreach (var pw in lProductionWages)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == pw.EmpCode && Index.Dt == pw.Dt))
                    {
                        r.ProductionWages = pw.Amt;
                        break;
                    }
                }
                lProductionWages.Clear();
            }

            #endregion

            #region Production Penalty

            //var lProductionPenalty = new List<ProductionPenalty>();
            //ProcessProductionPenalty(ref lProductionPenalty);
            //if (lProductionPenalty.Count > 0)
            //{
            //    foreach (var p in lProductionPenalty)
            //    {
            //        foreach (var r in lResult1.Where(Index => Index.EmpCode == p.EmpCode && Index.Dt == p.Dt))
            //        {
            //            r.DedProduction = p.Amt;
            //            break;
            //        }
            //    }
            //    lProductionPenalty.Clear();
            //}

            #endregion

            #region Production Penalty Leave

            //var lProductionPenaltyLeave = new List<ProductionPenaltyLeave>();
            //ProcessProductionPenaltyLeave(ref lProductionPenaltyLeave);
            //if (lProductionPenaltyLeave.Count > 0)
            //{
            //    foreach (var p in lProductionPenaltyLeave)
            //    {
            //        foreach (var r in lResult1.Where(Index => Index.EmpCode == p.EmpCode && Index.Dt == p.Dt))
            //        {
            //            r.DedProdLeave = p.Amt;
            //            break;
            //        }
            //    }
            //    lProductionPenaltyLeave.Clear();
            //}

            #endregion

            #region Production Minimum Wages

            var lProductionMinWages = new List<ProductionMinWages>();
            ProcessProductionMinWages(ref lProductionMinWages);
            if (lProductionMinWages.Count > 0)
            {
                foreach (var p in lProductionMinWages)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == p.EmpCode && Index.Dt == p.Dt))
                    {
                        r.IncMinWages = p.Amt;
                        break;
                    }
                }
                lProductionMinWages.Clear();
            }

            #endregion

            #region Production Insentif

            var lProductionInsentif = new List<ProductionInsentif>();
            ProcessProductionInsentif(ref lProductionInsentif);
            if (lProductionInsentif.Count > 0)
            {
                foreach (var p in lProductionInsentif)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == p.EmpCode && Index.Dt == p.Dt))
                    {
                        r.IncProduction = p.Amt;
                        break;
                    }
                }
                lProductionInsentif.Clear();
            }

            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion

            #region Employee Insentif

            var lIncEmployee = new List<IncEmployee>();
            ProcessIncEmployee(ref lIncEmployee);
            if (lIncEmployee.Count > 0)
            {
                foreach (var ie in lIncEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ie.EmpCode && Index.Dt == ie.DocDt))
                    {
                        r.IncEmployee = ie.Amt;
                        break;
                    }
                }
                lProductionInsentif.Clear();
            }

            #endregion

            #region EmpAD

            if (mSalaryInd == "2" && mADCodeEmploymentPeriod.Length > 0)
                ProcessEmpAD();

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            string EmpCodeTemp = string.Empty, DeptCode = string.Empty;
            bool IsFirst = true, IsProcess = false;
            
            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        IsFirst = true;
                        foreach (var x in lResult1.Where(x =>string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderByDescending(x => x.Dt))
                        {
                            if (IsFirst)
                            {
                                DeptCode = x.DeptCode;
                                IsFirst = false;
                            }
                            else
                                x.DeptCode = DeptCode;
                        }

                        foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                        {
                            x.Salary = x.EmpSalary2;
                            if (x.ProcessInd)
                            {
                                if (!(
                                    Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                    Sm.CompareStr(x.SystemType, mSystemType) &&
                                    Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                    Sm.CompareStr(x.PGCode, mPGCode) &&
                                    Sm.CompareStr(x.SiteCode, mSiteCode)
                                    ))
                                {
                                    x.ProcessInd = false;
                                }
                            }
                        }

                        IsProcess = false;
                        foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0))
                        {
                            if (x.ProcessInd)
                            {
                                IsProcess = true;
                                break;
                            }
                        }
                        if (!IsProcess)
                        {
                            Sm.StdMsg(mMsgType.Warning, 
                                "Employee's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine + 
                                "Employee's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine + 
                                "Department : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine + 
                                "Invalid department." + Environment.NewLine + 
                                "System will not process this employee and will delete existing employee's data for this payrun."
                                );
                            Grd1.Cells[Row, 0].Value = false;
                            Grd1.Cells[Row, 14].Value = true;
                        }
                    }
                }
            }
        }

        private void Process4(ref Result1 r)
        {
            string
                ActualDtIn = string.Empty, 
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty, 
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode, 
                Dt = r.Dt;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                //int.Parse(r.WSIn1) > int.Parse(r.WSOut1))
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                {    
                    r.LateInd = true;
                    r.PresenceRewardInd = false;
                }
            }

            if (!mIsUseLateValidation && r.LateInd) r.LateInd = false;

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            string ROT1 = string.Empty, ROT2 = string.Empty;

            #region Set OT Rutin (In)

            if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT1 = r.Dt + r.WSIn3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //1500 < 1600
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //1500 > 1400
                        ROT1 = r.Dt + r.WSIn3;
                    }
                    //}
                }
                else
                {
                    //Shift 3
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT1 = Dt2 + r.WSIn3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //0400<0500
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //0400>0300
                        ROT1 = Dt2 + r.WSIn3;
                    }
                    //}
                }
            }

            #endregion

            #region Set OT Rutin (Out)

            if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    ////Shift 1, 2
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT2 = r.Dt + r.WSOut3;
                    //}
                    //else
                    //{
                        if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                        {
                            //1900<2000
                            ROT2 = r.Dt + r.WSOut3;
                        }
                        else
                        {
                            //1900>1800
                            ROT2 = ActualDtOut + ActualTmOut;
                        }
                    //}
                }
                else
                {
                    //Shift 3
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT2 = Dt2 + r.WSOut3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //0300<0400
                        ROT2 = Dt2 + r.WSOut3;
                    }
                    else
                    {
                        //0300>0200
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                    //}
                }
            }

            #endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            if (ROT1.Length > 0 && ROT2.Length > 0)
            {
                ROTDuration = ComputeDuration(ROT2, ROT1);
                if (ROTDuration < 0) ROTDuration = 0m;
                if (mIsOTRoutineRounding && ROTDuration > 0)
                {
                    ROTDuration = RoundTo1Hour((double)ROTDuration);
                }
            }

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;
            
            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    //Full Day Leave
                    WorkingDuration = 0m;
                    ROTDuration = 0m;
                }
                else
                {
                    if (r.LeaveType.Length == 0)
                    {
                        //No Leave
                        WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                        {
                            // Menghitung lama waktu istirahat
                            if (r.OneDayInd)
                            {
                                // Shift 1, 2
                                if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            }
                            else
                            {
                                //Shift 3
                                if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                                //if (decimal.Parse(r.WSIn2) <= decimal.Parse(r.WSOut2))
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                                else
                                    BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                            }
                        }
                        WorkingDuration -= BreakDuration;
                    }
                    else
                    {
                        //Hourly Leave
                        string
                            LeaveStartDtTm = string.Empty,
                            LeaveEndDtTm = string.Empty,
                            BreakStartDtTm = string.Empty,
                            BreakEndDtTm = string.Empty;

                        if (r.OneDayInd)
                        {
                            //shift 1, 2
                            LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                            LeaveEndDtTm = r.Dt + r.LeaveEndTm;
                            BreakStartDtTm = r.Dt + r.WSIn2;
                            BreakEndDtTm = r.Dt + r.WSOut2;
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.LeaveStartTm, r.WSIn1) < 0)
                            {
                                LeaveStartDtTm = Dt2 + r.LeaveStartTm;
                                LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                            }
                            else
                            {
                                LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                                if (Sm.CompareDtTm(r.LeaveEndTm, r.WSOut1) <= 0)
                                    LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                                else
                                    LeaveEndDtTm = Dt + r.LeaveEndTm;
                            }

                            if (Sm.CompareDtTm(r.WSIn2, r.WSIn1) < 0)
                                BreakStartDtTm = Dt2 + r.WSIn2;
                            else
                                BreakStartDtTm = Dt + r.WSIn2;
                            
                            if (Sm.CompareDtTm(r.WSOut2, r.WSOut1) < 0)
                                BreakEndDtTm = Dt2 + r.WSOut2;                            
                            else
                                BreakEndDtTm = Dt + r.WSOut2;
                        }
                        if (r.OneDayInd)
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                        }
                        else
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt2 + r.WSOut1, Dt + r.WSIn1);
                        }
                        WorkingDuration -= r.LeaveDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                            WorkingDuration -= ComputeDuration(BreakEndDtTm, BreakStartDtTm);
                    }
                }
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion

            #region Menghitung OT

            decimal TotalOTHr = 0m;

            if (r.SystemType == mEmpSystemTypeHarian && !r.LateInd)
            {
                decimal OTHr = 0m;
                string 
                    StartTm = string.Empty, 
                    EndTm = string.Empty;

                if (mOTRequestType=="2")
                {
                    #region Use OT Request

                    TotalOTHr += ROTDuration;
                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            if (EndTm == "0000")
                                OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            else
                            {
                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                    OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
                                else
                                    OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }

                    #endregion
                }
                else
                {
                    bool
                        IsOTAdjusted = false,
                        IsOTSpecial = false; // beda tanggal

                    string
                        StartTmTemp = string.Empty,
                        EndTmTemp = string.Empty,
                        OTDtTemp = r.OneDayInd ? Dt : Dt2,
                        SpecialStartDt = string.Empty,
                        SpecialStartTm = string.Empty,
                        SpecialEndDt = string.Empty,
                        SpecialEndTm = string.Empty
                        ;

                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        IsOTAdjusted = false;
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        #region Use OT Adjustment

                        foreach (var ota in mlOTAdjustment.Where(
                            ota =>
                                ota.OTDocNo == x.OTDocNo &&
                                ota.EmpCode == EmpCode &&
                                ota.Dt == Dt
                                ))
                        {
                            IsOTAdjusted = true;
                            StartTm = ota.Tm1;
                            EndTm = ota.Tm2;
                        }

                        #endregion

                        #region Use Attendance Barcode

                        if (!IsOTAdjusted)
                        {
                            StartTmTemp = string.Empty;
                            EndTmTemp = string.Empty;

                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            var IsOTOneDayInd = true;
                            var Tm1Temp = string.Empty;

                            if (r.OneDayInd)
                            {
                                #region Shift 1 & 2

                                if (Sm.CompareDtTm(StartTm, EndTm) > 0)
                                {
                                    #region Case 1

                                    IsOTSpecial = true;

                                    foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(Dt + "0000", (i.Dt + i.Tm)) <= 0
                                                        )
                                                    .OrderBy(o1 => o1.Dt)
                                                    .ThenBy(o2 => o2.Tm)
                                                    .Take(1)
                                                    )
                                    {
                                        SpecialStartDt = index.Dt;
                                        SpecialStartTm = index.Tm;
                                    }

                                    if (
                                        SpecialStartDt.Length > 0 &&
                                        SpecialStartTm.Length > 0 &&
                                        Sm.CompareDtTm(SpecialStartDt + SpecialStartTm, Dt + StartTm) <= 0
                                        )
                                    {
                                        SpecialStartDt = Dt;
                                        SpecialStartTm = StartTm;
                                    }

                                    if (SpecialStartDt.Length > 0 && SpecialStartTm.Length > 0)
                                    {
                                        foreach (var index in mlAttendanceLog
                                               .Where(
                                                   i =>
                                                       i.EmpCode == EmpCode &&
                                                       (i.Dt == Dt || i.Dt == Dt2) &&
                                                       Sm.CompareDtTm(Dt + StartTm, (i.Dt + i.Tm)) < 0 &&
                                                       Sm.CompareDtTm((i.Dt + i.Tm), Dt2 + EndTm) <= 0
                                                   )
                                               .OrderByDescending(o1 => o1.Dt)
                                               .ThenByDescending(o2 => o2.Tm)
                                               .Take(1)
                                               )
                                        {
                                            SpecialEndDt = index.Dt;
                                            SpecialEndTm = index.Tm;
                                        }


                                        if (SpecialEndDt.Length <= 0 || SpecialEndTm.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                   .Where(
                                                       i =>
                                                           i.EmpCode == EmpCode &&
                                                           i.Dt == Dt2 &&
                                                           Sm.CompareDtTm(Dt2 + EndTm, i.Dt + i.Tm) <= 0
                                                       )
                                                   .OrderBy(o1 => o1.Dt)
                                                   .ThenBy(o2 => o2.Tm)
                                                   .Take(1)
                                                   )
                                            {
                                                SpecialEndDt = index.Dt;
                                                SpecialEndTm = index.Tm;
                                            }

                                            if (
                                                SpecialEndDt.Length > 0 &&
                                                SpecialEndTm.Length > 0 &&
                                                Sm.CompareDtTm(Dt2 + EndTm, SpecialEndDt + SpecialEndTm) <= 0
                                            )
                                            {
                                                SpecialEndDt = Dt2;
                                                SpecialEndTm = EndTm;
                                            }
                                        }
                                    }
                                    StartTmTemp = SpecialStartTm;
                                    EndTmTemp = SpecialEndTm;

                                    #endregion
                                }

                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                {
                                    #region case 2
                                    // Standard

                                    foreach (var index in mlAttendanceLog
                                    .Where(
                                        i =>
                                            i.EmpCode == EmpCode &&
                                            i.Dt == Dt &&
                                            Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                        )
                                    .OrderByDescending(o => o.Tm)
                                    .Take(1)
                                    )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }

                                    if (StartTmTemp.Length > 0)
                                    {
                                        if (x.Tm2 == "0000")
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, "2359") <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            if (EndTmTemp.Length <= 0)
                                            {
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt2 &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm2) <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            //if (EndTmTemp.Length <= 0)
                                            //{
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = Sm.Left(index.Tm, 4);
                                                }
                                            //}
                                            if (EndTmTemp.Length <= 0)
                                            {
                                                EndTmTemp = Sm.Left(x.Tm2, 4);
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Shift 3
                                if (Sm.CompareDtTm(r.WSOut1, x.Tm1) <= 0)
                                {
                                    if (Sm.CompareDtTm(r.WSOut1, ActualTmOut) <= 0)
                                        StartTmTemp = r.WSOut1;
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            StartTmTemp = Sm.Left(index.Tm, 4);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }
                                }

                                if (StartTmTemp.Length > 0)
                                {
                                    if (x.Tm2 == "0000")
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + "2359") <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            var OTDtTemp2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp2 &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm2) <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (StartTmTemp.Length <= 0 || EndTmTemp.Length <= 0)
                            {
                                StartTm = string.Empty;
                                EndTm = string.Empty;
                            }
                            else
                            {
                                if (!IsOTSpecial)
                                {
                                    StartTm = x.Tm1;
                                    if (x.Tm2 == "0000")
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) != 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) < 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                }
                            }
                        }

                        #endregion

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            if (IsOTSpecial)
                            {
                                OTHr = ComputeDuration(SpecialEndDt + SpecialEndTm, SpecialStartDt + SpecialStartTm);
                            }
                            else
                            {
                                if (EndTm == "0000")
                                {
                                    if (r.OneDayInd)
                                        OTHr = ComputeDuration(Dt2 + EndTm, OTDtTemp + StartTm);
                                    else
                                    {
                                        var Dt3 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                        OTHr = ComputeDuration(Dt3 + EndTm, OTDtTemp + StartTm);
                                    }
                                }
                                else
                                    OTHr = ComputeDuration(OTDtTemp + EndTm, OTDtTemp + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }
                }
            }

            if (mSalaryInd == "2")
            {
                if (mIsOTHolidayDeduct1Hr && (r.HolInd || r.WSHolidayInd))
                    if (TotalOTHr>=4) TotalOTHr = TotalOTHr-1;
            }

            #endregion

            #region OT menggantikan leave

            //if (mSalaryInd=="1" && mIsUseOTReplaceLeave)
            if (mIsUseOTReplaceLeave)
            {
                var NonROTDuration = RoundTo30Minutes((double)(TotalOTHr));
                TotalOTHr = RoundTo30Minutes((double)(TotalOTHr + ROTDuration));
                if (TotalOTHr > 0m && r.LeaveDuration > 0m)
                {
                    r.OTToLeaveInd = true;
                    if (r.LeaveType != "F")
                    {
                        r.WorkingDuration = r.WorkingDuration + r.LeaveDuration;
                        if (r.LeaveDuration >= NonROTDuration)
                        {
                            r.LeaveDuration -= NonROTDuration;
                            NonROTDuration = 0m;
                        }
                        else
                        {
                            NonROTDuration -= r.LeaveDuration;
                            r.LeaveDuration = 0;
                        }
                        r.WorkingDuration = r.WorkingDuration - r.LeaveDuration;

                        if (r.WSOut3.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.WSIn3, r.WSOut3) <= 0)
                                ROTDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn3);
                            else
                                ROTDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn3);
                        }
                        else
                            ROTDuration = 0m;
                        
                        if (r.LeaveDuration >= ROTDuration)
                        {
                            r.LeaveDuration -= ROTDuration;
                            ROTDuration = 0m;
                        }
                        else
                        {
                            ROTDuration -= r.LeaveDuration;
                            r.LeaveDuration = 0;
                        }
                        TotalOTHr = RoundTo30Minutes((double)(NonROTDuration + ROTDuration));
                    }
                }
            }

            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length>0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (
                        r.LeaveDuration != 0m &&
                        mWorkingHr != 0m &&
                        !(r.SystemType == mEmpSystemTypeBorongan && r.LeaveCode == mOperatorLeaveCode)
                    )
                {
                    //if (r.SystemType == mEmpSystemTypeBorongan)
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.PLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.PLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.UPLDay = 1;
                }
                else
                    r.UPLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m) 
                {
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            if (r.LeaveCode == mCoordinatorLeaveCode && r.LeaveType != "F")
                                r.UPLAmt = r.ProductionWages * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.UPLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.UPLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r._DeductTHPInd)
                    r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            #region Menghitung OT Amount

            if (r.SystemType == mEmpSystemTypeHarian && r.Salary != 0m && TotalOTHr >= 0.5m)
            {
                decimal TotalOTHrTemp = TotalOTHr;
                if (r.HolInd || r.WSHolidayInd)
                {
                    r.OTHolidayHr = TotalOTHrTemp;

                    #region SalaryInd = 1

                    if (mSalaryInd == "1")
                    {
                        if (r._IsUseLatestGrdLvlSalary)
                        {
                            if (TotalOTHrTemp <= mWorkingHr)
                                r.OTHolidayAmt = (TotalOTHrTemp / mWorkingHr) * r.HolidayIndex * r._LatestDailyGrdLvlSalary;
                            else
                                r.OTHolidayAmt =
                                    (r._LatestDailyGrdLvlSalary * r.HolidayIndex) +
                                    ((r._LatestMonthlyGrdLvlSalary / 173) * mOTFormulaIndex2 * (TotalOTHrTemp - mWorkingHr));
                        }
                        else
                        {
                            if (TotalOTHrTemp <= mWorkingHr)
                                r.OTHolidayAmt = (TotalOTHrTemp / mWorkingHr) * r.HolidayIndex * r.BasicSalary2;
                            else
                                r.OTHolidayAmt =
                                    (r.BasicSalary2 * r.HolidayIndex) +
                                    ((r.BasicSalary / 173) * mOTFormulaIndex2 * (TotalOTHrTemp - mWorkingHr));
                        }
                    }

                    #endregion

                    #region SalaryInd = 2

                    if (mSalaryInd == "2")
                    {
                        int i = 1;
                        if (r._IsHOInd)
                        {
                            while (TotalOTHrTemp > 0)
                            {
                                if (i == 1)
                                {
                                    if (TotalOTHrTemp >= mHoliday1stHrForHO)
                                    {
                                        r.OTHolidayAmt = mHoliday1stHrForHO * mHoliday1stHrIndex * ((r.EmpSalary + r.SalaryAD) / 173);
                                        TotalOTHrTemp -= mHoliday1stHrForHO;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * ((r.EmpSalary + r.SalaryAD) / 173);
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 2)
                                {
                                    if (TotalOTHrTemp >= mHoliday2ndHrForHO)
                                    {
                                        r.OTHolidayAmt += (mHoliday2ndHrForHO * mHoliday2ndHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                        TotalOTHrTemp -= mHoliday2ndHrForHO;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 3)
                                {
                                    r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                    TotalOTHrTemp = 0m;
                                }
                                i++;
                            }
                        }

                        if (!r._IsHOInd)
                        {
                            while (TotalOTHrTemp > 0)
                            {
                                if (i == 1)
                                {
                                    if (TotalOTHrTemp >= mHoliday1stHrForSite)
                                    {
                                        r.OTHolidayAmt = mHoliday1stHrForSite * mHoliday1stHrIndex * ((r.EmpSalary + r.SalaryAD) / 173);
                                        TotalOTHrTemp -= mHoliday1stHrForSite;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * ((r.EmpSalary + r.SalaryAD) / 173);
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 2)
                                {
                                    if (TotalOTHrTemp >= mHoliday2ndHrForSite)
                                    {
                                        r.OTHolidayAmt += (mHoliday2ndHrForSite * mHoliday2ndHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                        TotalOTHrTemp -= mHoliday2ndHrForSite;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 3)
                                {
                                    r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * ((r.EmpSalary + r.SalaryAD) / 173));
                                    TotalOTHrTemp = 0m;
                                }
                                i++;
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    if (TotalOTHrTemp <= mOTFormulaHr1)
                    {
                        r.OT1Hr = TotalOTHrTemp;
                        if (mSalaryInd == "1")
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * (r._LatestMonthlyGrdLvlSalary / 173);
                            else
                                r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * (r.BasicSalary / 173);
                        }
                        if (mSalaryInd == "2")
                            r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * ((r.EmpSalary+r.SalaryAD) / 173);
                        TotalOTHrTemp = 0m;
                    }
                    else
                    {
                        r.OT1Hr = mOTFormulaHr1;
                        if (mSalaryInd == "1")
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * (r._LatestMonthlyGrdLvlSalary / 173);
                            else
                                r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * (r.BasicSalary / 173);
                        }
                        if (mSalaryInd == "2")
                            r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * ((r.EmpSalary + r.SalaryAD) / 173);
                        TotalOTHrTemp -= mOTFormulaHr1;
                    }

                    if (TotalOTHrTemp>0m && TotalOTHrTemp <= mOTFormulaHr2)
                    {
                        r.OT2Hr = TotalOTHrTemp;
                        if (mSalaryInd == "1")
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * (r._LatestMonthlyGrdLvlSalary / 173);
                            else
                                r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * (r.BasicSalary / 173);
                        }
                        if (mSalaryInd == "2")
                            r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * ((r.EmpSalary + r.SalaryAD) / 173);
                        TotalOTHrTemp = 0m;
                    }
                }
            }

            #endregion

            #region Compute Extra Fooding

            if (mSalaryInd == "1")
            {
                if (r.WorkingDuration >= 2m)
                    r.ExtraFooding = r._ExtraFooding;
                else
                {
                    foreach (string h in mWSCodesHolidayWithExtraFooding.Split('#'))
                    {
                        if (Sm.CompareStr(r.WSCode, h))
                        {
                            var ExtraFoodingWorkingIn = ActualDtIn + ActualTmIn;
                            var ExtraFoodingWorkingOut = ActualDtOut + ActualTmOut;
                            if (ExtraFoodingWorkingIn.Length>0 && ExtraFoodingWorkingOut.Length>0)
                            {
                                if (ComputeDuration(ExtraFoodingWorkingOut, ExtraFoodingWorkingIn) >= 2m)
                                    r.ExtraFooding = r._ExtraFooding;
                            }
                            break;
                        }
                    }
                }
            }

            #endregion

            #region Compute Transport/Meal/FieldAssignment

            
            if (r.WorkingDuration > 0 && mlEmployeeVarAllowanceMultiplePeriod.Count > 0)
            {
                if (mADCodeTransport.Length > 0)
                {
                    foreach (
                        var evl in mlEmployeeVarAllowanceMultiplePeriod.Where(i=>
                            Sm.CompareStr(i.EmpCode, EmpCode) &&
                            Sm.CompareStr(i.ADCode, mADCodeTransport) &&
                            Sm.CompareDtTm(i.StartDt, Dt)<=0 &&
                            Sm.CompareDtTm(Dt, i.EndDt)<= 0)
                            )
                            r.Transport = evl.Amt;
                }

                if (mADCodeMeal.Length > 0)
                {
                    if (mMealMinWorkDuration > 0m && 
                        ActualDtOut.Length>0 && 
                        ActualTmOut.Length>0 && 
                        ActualDtIn.Length>0 && 
                        ActualTmIn.Length>0)
                    {
                        if (ComputeDuration(ActualDtOut + ActualTmOut, ActualDtIn + ActualTmIn) >= mMealMinWorkDuration)
                        {
                            if (r.WSIn2.Length > 0)
                            {
                                if (Sm.CompareDtTm(string.Concat(ActualDtIn, ActualTmIn), string.Concat(r.Dt, r.WSIn2)) <= 0)
                                {
                                    var Value = string.Concat("#", mADCodeMeal.Replace(",", "##"), "#");
                                    foreach (
                                        var evl in mlEmployeeVarAllowanceMultiplePeriod
                                        .Where(i =>
                                            Sm.CompareStr(i.EmpCode, EmpCode) &&
                                            Value.Contains(string.Concat("#", i.ADCode, "#")) &&
                                            Sm.CompareDtTm(i.StartDt, Dt) <= 0 &&
                                            Sm.CompareDtTm(Dt, i.EndDt) <= 0)
                                            )
                                        r.Meal += evl.Amt;
                                }
                            }
                            else
                            {
                                var Value = string.Concat("#", mADCodeMeal.Replace(",", "##"), "#");
                                foreach (
                                    var evl in mlEmployeeVarAllowanceMultiplePeriod
                                    .Where(i =>
                                        Sm.CompareStr(i.EmpCode, EmpCode) &&
                                        Value.Contains(string.Concat("#", i.ADCode, "#")) &&
                                        Sm.CompareDtTm(i.StartDt, Dt) <= 0 &&
                                        Sm.CompareDtTm(Dt, i.EndDt) <= 0)
                                        )
                                    r.Meal += evl.Amt;
                            }
                        }
                    }
                }

                if (mADCodeFieldAssignment.Length > 0)
                {
                    if (mFieldAssignmentMinWorkDuration > 0m &&
                        ActualDtOut.Length > 0 &&
                        ActualTmOut.Length > 0 &&
                        ActualDtIn.Length > 0 &&
                        ActualTmIn.Length > 0)
                    {
                        if (ComputeDuration(ActualDtOut + ActualTmOut, ActualDtIn + ActualTmIn) >= mFieldAssignmentMinWorkDuration)
                        {    
                            foreach (
                                var evl in mlEmployeeVarAllowanceMultiplePeriod.Where(i =>
                                    Sm.CompareStr(i.EmpCode, EmpCode) &&
                                    Sm.CompareStr(i.ADCode, mADCodeFieldAssignment) &&
                                    Sm.CompareDtTm(i.StartDt, Dt) <= 0 &&
                                    Sm.CompareDtTm(Dt, i.EndDt) <= 0)
                                    )
                                r.FieldAssignment = evl.Amt;
                        }
                    }
                }
                
            }

            #endregion

            #region Compute Performance Incentive

            if (mSalaryInd == "1")
            {
                if (r.SystemType == mEmpSystemTypeHarian)
                {
                    var IncPerformanceDuration = RoundTo30Minutes((double)(r.WorkingDuration));

                    if (r.HolInd || r.WSHolidayInd)
                        IncPerformanceDuration = r.OTHolidayHr; 

                    //based on employee's salary

                    if (IncPerformanceDuration >= mWorkingHr)
                        r.IncPerformance = r._IncPerformance;
                    else
                        r.IncPerformance = (IncPerformanceDuration / mWorkingHr) * r._IncPerformance;

                    //based on grade level

                    if (mADCodeIncPerformance2.Length > 0)
                    {
                        if (mlGradeLevelVarAllowance.Count > 0)
                        {
                            string GrdLvlCode = r.GrdLvlCode;
                            decimal IncPerformance2 = 0m;

                            foreach (var glva in mlGradeLevelVarAllowance
                                .Where(Index =>
                                        Index.GrdLvlCode == GrdLvlCode &&
                                        Index.ADCode == mADCodeIncPerformance2
                                    )
                                )
                            {
                                IncPerformance2 = glva.Amt;
                                break;
                            }

                            if (IncPerformanceDuration >= mWorkingHr)
                                r.IncPerformance += IncPerformance2;
                            else
                                r.IncPerformance += (IncPerformanceDuration / mWorkingHr) * IncPerformance2;
                        }
                    }
                }
            }

            #endregion

            #region Checking Presence Reward

            if (mSalaryInd == "1")
            {
                r.PresenceRewardInd = true;

                if (r.ProcessInd && !r.HolInd && !r.WSHolidayInd && (r.LateInd || (r.LeaveCode.Length == 0 && r.WorkingDuration <= 0)))
                    r.PresenceRewardInd = false;

                if (r.ProcessInd && r.LeaveCode.Length != 0 && r.LeaveCode != mGoHomeEarlyLeaveCode)
                    r.PresenceRewardInd = false;

                if (r.ResignDt.Length >= 8)
                {
                    if (Sm.CompareDtTm(Sm.Left(r.ResignDt, 8), r.Dt) <= 0)
                        r.PresenceRewardInd = false;
                }
            }

            #endregion

            #region Update Salary based on GoHomeEarlyLeave

            if (mSalaryInd=="1" && r.SystemType == mEmpSystemTypeHarian && r.LeaveCode.Length>0 && r.LeaveCode == mGoHomeEarlyLeaveCode)
            {
                if (r.WorkingDuration <= mGoHomeEarlyLeaveHr1)
                {
                    r.Salary = (mGoHomeEarlyLeavePerc1 / 100) * r.Salary;
                }
                else
                {
                    if (r.WorkingDuration <= mGoHomeEarlyLeaveHr2)
                    {
                        r.Salary = (mGoHomeEarlyLeavePerc2 / 100) * r.Salary;
                    }
                }
            }

            #endregion

            #region Compute Holiday Earning

            if (mSalaryInd == "1")
            {
                if (!r.LateInd && (r.LeaveCode.Length == 0 || r.LeaveCode == mGoHomeEarlyLeaveCode))
                {
                    r._IsFullDayInd = true;
                }

                if (
                    mSalaryInd == "1" &&
                    Sm.CompareStr(r.EmploymentStatus, mEmploymentStatusTetap) &&
                    Sm.CompareStr(r.PayrunPeriod, mPayrunPeriod2Mingguan) &&
                    r.HolInd &&
                    !r.WSHolidayInd
                    )
                {
                    r.HolidayEarning = r.BasicSalary2;
                }
            }
            #endregion

            #region Recompute Salary

            if (mSalaryInd == "1")
            {
                if (r.SystemType != mEmpSystemTypeBorongan)
                {
                    if (r.HolInd || r.WSHolidayInd)
                        r.Salary = 0m;
                }

                if (!((r.HolInd || r.WSHolidayInd) && r.SystemType == mEmpSystemTypeBorongan))
                {
                    if (r.WSHolidayInd && r.WorkingDuration <= 0)
                        r.Salary = 0m;
                }

                if (!(r.HolInd || r.WSHolidayInd) && r.SystemType != mEmpSystemTypeBorongan)
                {
                    if (r.LeaveCode != mGoHomeEarlyLeaveCode)
                    {
                        var Duration = r.WorkingDuration;
                        if (Duration > mWorkingHr) Duration = mWorkingHr;
                        r.Salary = (Duration / mWorkingHr) * r.Salary;
                    }
                }
            }

            #endregion

            #region Validate Borongan (Production Leave)

            if (mSalaryInd == "1")
            {
                if (r.SystemType == mEmpSystemTypeBorongan)
                {
                    if (r.LeaveType == "F")
                    {
                        r.ProductionWages = 0m;
                        r.IncMinWages = 0m;
                        r._IsFullDayInd = false;
                        r.LateInd = false;
                        r.WorkingDuration = 0m;
                        r.Salary = 0m;
                        r.PLHr = 0m;
                        r.UPLHr = 0m;
                        r.OT1Hr = 0m;
                        r.OT2Hr = 0m;
                        r.OTHolidayHr = 0m;
                        r.OT1Amt = 0m;
                        r.OT2Amt = 0m;
                        r.OTHolidayAmt = 0m;
                        r.IncPerformance = 0m;
                        r.PresenceRewardInd = false;
                        r.HolidayEarning = 0m;
                        r.ExtraFooding = 0m;
                        r.FieldAssignment = 0m;
                        r.Meal = 0m;
                        r.Transport = 0m;
                    }
                    if (r.ActualIn.Length==0)
                    {
                        r.Salary = 0m;
                        r.ProductionWages = 0m;
                    }
                }
            }

            #endregion

            #region Unprocessed data

            if (mIsUseLateValidation && r.LateInd) r.ProcessInd = false;

            if (!r.ProcessInd)
            {
                r._LatestMonthlyGrdLvlSalary = 0m;
                r._LatestDailyGrdLvlSalary = 0m;
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                r.EmpSalary = 0m;
                r.EmpSalary2 = 0m;
                r.BasicSalary = 0m;
                if (mSalaryInd == "2") r.BasicSalary2 = 0m;
                r.ProductionWages = 0m;
                r.Salary = 0m;
                if (mSalaryInd == "2") r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt  = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr  = 0m;
                r.OT2Hr  = 0m;
                r.OTHolidayHr  = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt  = 0m;
                r.OTHolidayAmt  = 0m;
                r.OTToLeaveInd  = false;
                r.IncMinWages  = 0m;
                r.IncProduction  = 0m;
                r.IncPerformance  = 0m;
                //r.PresenceRewardInd  = false;
                r.HolidayEarning  = 0m;
                r.ExtraFooding  = 0m;
                r.FieldAssignment = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                r.DedProduction  = 0m;
                r.DedProdLeave  = 0m;
                r.IncEmployee = 0m;
            }

            #endregion
        }

        private void Process4ForAllIn(ref Result1 r)
        {
            if (!r.ProcessInd) r.Salary = 0m;
            r._IsFullDayInd = false;
            r.ActualIn = string.Empty;
            r.ActualOut = string.Empty;
            r.WorkingIn = string.Empty;
            r.WorkingOut = string.Empty;
            r.WorkingDuration = 0m;
            r.ProductionWages = 0m;
            r.LeaveCode = string.Empty;
            r.LeaveType = string.Empty;
            r.PaidLeaveInd = false;
            r.CompulsoryLeaveInd = false;
            r.LeaveStartTm = string.Empty;
            r.LeaveEndTm = string.Empty;
            r.LeaveDuration = 0m;
            r.PLDay = 0m;
            r.PLHr = 0m;
            r.PLAmt = 0m;
            r.UPLDay = 0m;
            r.UPLHr = 0m;
            r.UPLAmt = 0m;
            r.ProcessPLAmt = 0m;
            r.OT1Hr = 0m;
            r.OT2Hr = 0m;
            r.OTHolidayHr = 0m;
            r.OT1Amt = 0m;
            r.OT2Amt = 0m;
            r.OTHolidayAmt = 0m;
            r.OTToLeaveInd = false;
            r.IncMinWages = 0m;
            r.IncProduction = 0m;
            r.IncPerformance = 0m;
            r.PresenceRewardInd  = false;
            r.HolidayEarning = 0m;
            r.ExtraFooding = 0m;
            r.FieldAssignment = 0m;
            r.Meal = 0m;
            r.Transport = 0m;
            r.DedProduction = 0m;
            r.DedProdLeave = 0m;
            r.IncEmployee = 0m;
        }

        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = Sm.GetLue(LuePayrunCode),
                Filter = string.Empty;
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length>0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " (" + Filter + ")";
            else
                Filter = " 1=0 ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, ");
            SQL.AppendLine("IfNull(B.DedProduction, 0) As DedProduction, ");
            SQL.AppendLine("IfNull(C.DedEmployee, 0) As DedEmployee, ");
            SQL.AppendLine("IfNull(D.SSEmployerHealth, 0) As SSEmployerHealth, ");
            SQL.AppendLine("IfNull(E.SSEmployerEmployment, 0) As SSEmployerEmployment, ");
            SQL.AppendLine("IfNull(F.SSEmployeeHealth, 0) As SSEmployeeHealth, ");
            SQL.AppendLine("IfNull(G.SSEmployeeEmployment, 0) As SSEmployeeEmployment, ");
            SQL.AppendLine("IfNull(H.SalaryAdjustment, 0) As SalaryAdjustment, ");
            SQL.AppendLine("IfNull(I.NonTaxableFixedAllowance, 0) As NonTaxableFixedAllowance, ");
            SQL.AppendLine("IfNull(J.TaxableFixedAllowance, 0) As TaxableFixedAllowance, ");
            SQL.AppendLine("IfNull(K.NonTaxableFixedDeduction, 0) As NonTaxableFixedDeduction, ");
            SQL.AppendLine("IfNull(L.TaxableFixedDeduction, 0) As TaxableFixedDeduction, ");
            SQL.AppendLine("IfNull(M.ProductionIncentiveAmt, 0) As ProductionIncentiveAmt, ");
            SQL.AppendLine("IfNull(N.SSEmployerNonRetiring, 0) As SSEmployerNonRetiring, ");
            SQL.AppendLine("IfNull(O.SSEmployeeOldAgeInsurance, 0) As SSEmployeeOldAgeInsurance, ");
            SQL.AppendLine("IfNull(P.SSEmployerPension, 0) As SSEmployerPension, ");
            SQL.AppendLine("IfNull(P.SSEmployeePension, 0) As SSEmployeePension, ");
            SQL.AppendLine("IfNull(Q.EmploymentPeriodAllowance, 0) As EmploymentPeriodAllowance, ");
            SQL.AppendLine("IfNull(R.Functional, 0.00) As Functional, ");
            SQL.AppendLine("IfNull(S.Regional, 0.00) As Regional ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As DedProduction From ( ");
            SQL.AppendLine("        Select T2.EmpCode, T2.Penalty As Amt ");
            SQL.AppendLine("        From TblPNTHdr T1 ");
            SQL.AppendLine("        Inner Join TblPNTDtl4 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T2.Penalty As Amt ");
            SQL.AppendLine("        From TblPNT2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblPNT2Dtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T1.AmtInspnt) As DedEmployee ");
            SQL.AppendLine("    From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpInsPntDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("    Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='02' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode=@SSPCodeForHealth ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And (T1.SSPCode=@SSPCodeForEmployment Or T1.SSPCode=@SSPCodeForPension) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode=@SSPCodeForHealth ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And (T1.SSPCode=@SSPCodeForEmployment Or T1.SSPCode=@SSPCodeForPension) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr A ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr A ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "B.") + ") ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As NonTaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("        On A.ADCode=B.ADCode ");
            SQL.AppendLine("        And B.ADType='A' ");
            SQL.AppendLine("        And B.TaxInd='N' ");
            SQL.AppendLine("        And B.AmtType='1' ");
            SQL.AppendLine("        And B.ADCode Not In ('01', '06') ");
            SQL.AppendLine("    Where ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("        On A.ADCode=B.ADCode ");
            SQL.AppendLine("        And B.ADType='A' ");
            SQL.AppendLine("        And B.TaxInd='Y' ");
            SQL.AppendLine("        And B.AmtType='1' ");
            SQL.AppendLine("        And B.ADCode Not In ('01', '06') ");
            SQL.AppendLine("    Where ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As NonTaxableFixedDeduction ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.TaxInd='N' And B.AmtType='1' ");
            SQL.AppendLine("    Where ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") K On A.EmpCode=K.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedDeduction ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode, Sum(B.Amt) As ProductionIncentiveAmt ");
            SQL.AppendLine("    From TblProductionIncentiveHdr A ");
            SQL.AppendLine("    Inner Join TblProductionIncentiveDtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "B.") + ") ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.StartDt=@StartDt ");
            SQL.AppendLine("    And A.EndDt=@EndDt ");
            SQL.AppendLine("    And IfNull(A.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    Group BY B.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerNonRetiring ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And T2.SSCode<>@SSForRetiring ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode=@SSPCodeForEmployment ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") N On A.EmpCode=N.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployeeAmt) As SSEmployeeOldAgeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And T2.SSCode=@SSOldAgeInsurance ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode=@SSPCodeForEmployment ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerPension, Sum(T2.EmployeeAmt) As SSEmployeePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And T2.SSCode=@SSForRetiring ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") P On A.EmpCode=P.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As EmploymentPeriodAllowance ");
            SQL.AppendLine("    From TblEmpAD T ");
            SQL.AppendLine("    Where (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    And (T.Dt Is Not Null And T.Dt Between @StartDt And @EndDt) ");
            SQL.AppendLine("    And T.CancelInd='N' ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T.") + ") ");
            SQL.AppendLine("    Group By T.EmpCode ");
            SQL.AppendLine(") Q On A.EmpCode=Q.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Functional ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Where A.ADCode='01' ");
            SQL.AppendLine("    And ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") R On A.EmpCode=R.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Regional ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Where A.ADCode='06' ");
            SQL.AppendLine("    And ((A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt)) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") S On A.EmpCode=S.EmpCode ");


            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.CmParam<String>(ref cm, "@SSPCodeForHealth", mSSPCodeForHealth);
                Sm.CmParam<String>(ref cm, "@SSPCodeForEmployment", mSSPCodeForEmployment);
                Sm.CmParam<String>(ref cm, "@SSPCodeForPension", mSSPCodeForPension);
                Sm.CmParam<String>(ref cm, "@SSForRetiring", mSSForRetiring);
                Sm.CmParam<String>(ref cm, "@SSOldAgeInsurance", mSSOldAgeInsurance);
            
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", "ResignDt", "NPWP", "PTKP", "DedProduction", 
                
                    //6-10
                    "DedEmployee", "SSEmployerHealth", "SSEmployerEmployment", "SSEmployeeHealth", "SSEmployeeEmployment",

                    //11-15
                    "SalaryAdjustment", "NonTaxableFixedAllowance", "TaxableFixedAllowance", "NonTaxableFixedDeduction", "TaxableFixedDeduction",

                    //16-20
                    "ProductionIncentiveAmt", "SSEmployerNonRetiring", "SSEmployeeOldAgeInsurance", "SSEmployerPension", "SSEmployeePension",

                    //21-23
                    "EmploymentPeriodAllowance", "Functional", "Regional"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            DedProduction = Sm.DrDec(dr, c[5]),
                            DedEmployee = Sm.DrDec(dr, c[6]),
                            SSEmployerHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[9]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[10]),
                            SalaryAdjustment = Sm.DrDec(dr, c[11]),
                            NonTaxableFixAllowance = Sm.DrDec(dr, c[12]),
                            TaxableFixAllowance = Sm.DrDec(dr, c[13]),
                            NonTaxableFixDeduction = Sm.DrDec(dr, c[14]),
                            TaxableFixDeduction = Sm.DrDec(dr, c[15]),
                            FixAllowance = Sm.DrDec(dr, c[12]) + Sm.DrDec(dr, c[13]),
                            FixDeduction = Sm.DrDec(dr, c[14]) + Sm.DrDec(dr, c[15]),
                            IncProduction = Sm.DrDec(dr, c[16]),
                            SSEmployerNonRetiring = Sm.DrDec(dr, c[17]),
                            SSEmployeeOldAgeInsurance = Sm.DrDec(dr, c[18]),
                            SSEmployerPension = Sm.DrDec(dr, c[19]),
                            SSEmployeePension = Sm.DrDec(dr, c[20]),
                            EmploymentPeriodAllowance = Sm.DrDec(dr, c[21]),
                            Functional = Sm.DrDec(dr, c[22]),
                            Regional = Sm.DrDec(dr, c[23]),
                            Salary = 0m,
                            WorkingDay = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            IncMinWages = 0m,
                            IncPerformance = 0m,
                            PresenceReward = 0m,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            FieldAssignment = 0m,
                            DedProdLeave = 0m,
                            IncEmployee = 0m,
                            Tax = 0m,
                            Amt = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            var EmpCode = r.EmpCode;
            decimal LatestSalary = 0m, LatestSalary2 = 0m, HariMasukKerja = 0m;
            
            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode && x.ProcessInd).OrderBy(Index=>Index.Dt))
            {
                if (x.ProcessInd)
                {
                    LatestSalary = x.EmpSalary;
                    LatestSalary2 = x.EmpSalary2;
                    r.Salary += x.Salary;
                    r.PLDay += x.PLDay;
                    r.PLHr += x.PLHr;
                    r.PLAmt += x.PLAmt;
                    r.UPLDay += x.UPLDay;
                    r.UPLHr += x.UPLHr;
                    r.UPLAmt += x.UPLAmt;
                    r.ProcessPLAmt += x.ProcessPLAmt;
                    r.ProcessUPLAmt += x.ProcessUPLAmt;
                    r.OT1Hr += x.OT1Hr;
                    r.OT2Hr += x.OT2Hr;
                    r.OTHolidayHr += x.OTHolidayHr;
                    r.OT1Amt += x.OT1Amt;
                    r.OT2Amt += x.OT2Amt;
                    r.OTHolidayAmt += x.OTHolidayAmt;
                    r.IncMinWages += x.IncMinWages;
                    r.IncProduction += x.IncProduction;
                    r.IncPerformance += x.IncPerformance;
                    r.ExtraFooding += x.ExtraFooding;
                    r.FieldAssignment += x.FieldAssignment;
                    r.DedProdLeave += x.DedProdLeave;
                    r.Transport += x.Transport;
                    r.Meal += x.Meal;
                    r.IncEmployee += x.IncEmployee;
                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode).OrderBy(Index => Index.Dt))
            {
                if (x.ProcessInd && !(x.HolInd || x.WSHolidayInd) && x.WorkingDuration > 0m && x.LeaveCode.Length == 0)
                    HariMasukKerja += 1m;
            }


            r.Salary = LatestSalary;
            var JumlahHarikerjaTiapBulan = mHOInd ? mHOWorkingDayPerMth : mSiteWorkingDayPerMth;
            if (HariMasukKerja>JumlahHarikerjaTiapBulan) HariMasukKerja=JumlahHarikerjaTiapBulan;
            if (HariMasukKerja < JumlahHarikerjaTiapBulan)
            { 
                if (JumlahHarikerjaTiapBulan==0m)
                {
                    r.Salary = 0m;
                    r.Functional = 0m;
                    r.Regional = 0m;
                }
                else
                {
                    r.Salary = (HariMasukKerja / JumlahHarikerjaTiapBulan) * r.Salary;
                    r.Functional = (HariMasukKerja / JumlahHarikerjaTiapBulan) * r.Functional;
                    r.Regional = (HariMasukKerja / JumlahHarikerjaTiapBulan) * r.Regional;
                }
            }
            if (r.Salary < 0m) r.Salary = 0m;
            if (r.Functional < 0m) r.Functional = 0m;
            if (r.Regional < 0m) r.Regional = 0m;

            decimal EmpAdvancePayment = 0m;
            foreach (var x in mlAdvancePaymentProcess.Where(x => x.EmpCode == EmpCode))
                EmpAdvancePayment += x.Amt;

            r.EmpAdvancePayment = EmpAdvancePayment;


            r.Amt =
                r.Salary -
                r.ProcessUPLAmt +
                r.OT1Amt +
                r.OT2Amt +
                r.OTHolidayAmt +
                r.Functional +
                r.Regional +
                r.FixAllowance +
                r.EmploymentPeriodAllowance +
                r.IncEmployee +
                r.IncMinWages +
                r.IncProduction +
                r.IncPerformance +
                r.PresenceReward +
                r.HolidayEarning +
                r.Transport +
                r.Meal +
                r.FieldAssignment +
                r.ExtraFooding -
                r.DedEmployee -
                r.DedProduction -
                r.DedProdLeave -
                r.EmpAdvancePayment -
                r.SSEmployeeHealth -
                r.SSEmployeeEmployment -
                r.SSEmployeePension +
                r.SalaryAdjustment;

            if (r.Amt < 0m) r.Amt = 0m;

            #region Compute Tax

            if (r.PTKP.Length > 0 && r.Amt > 0)
            {
                if (mPayrollTaxFormula=="2")
                    ComputeTax2(ref r, ref lResult1, ref lNTI, ref lTI2);
                else
                    ComputeTax1(ref r, ref lResult1, ref lNTI, ref lTI);
            }
            #endregion

            r.Amt -= r.Tax;
            if (r.Amt < 0m) r.Amt = 0m;
        }

        private void ComputeTax1(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            decimal
                Tax = 0m,
                NTIAmt = 0m,
                FunctionalExpenses = 0m,
                Amt = 0m,
                JoinYr = 0m;

            string NTI = r.PTKP;

            bool IsFullYr = true;

            Amt =
                r.Salary +
                r.Functional +
                r.Regional +
                r.TaxableFixAllowance +
                r.FieldAssignment +
                r.Meal +
                r.Transport +
                r.IncProduction +
                r.OT1Amt +
                r.OT2Amt +
                r.OTHolidayAmt +
                r.SalaryAdjustment;

            Amt = Amt + r.SSEmployerEmployment + r.SSEmployerPension;

            FunctionalExpenses = (Amt * (mFunctionalExpenses / 100m));
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Tax =
                Amt -
                FunctionalExpenses -
                r.SSEmployeeOldAgeInsurance -
                r.SSEmployeePension;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Tax = Tax * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Tax = Tax * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Tax > NTIAmt)
                Tax -= NTIAmt;
            else
                Tax = 0m;

            Tax = Tax % 1000 >= 500 ? Tax + 1000 - Tax % 1000 : Tax - Tax % 1000;

            if (Tax > 0 && lTI.Count > 0)
            {
                decimal TaxTemp = Tax, Amt2Temp = 0m;
                Tax = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            Tax += (TaxTemp * i.TaxRate / 100);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            Tax += ((i.Amt2 - Amt2Temp) * i.TaxRate / 100);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
                Tax = Tax / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
                if (r.NPWP.Length == 0)
                    Tax = Tax * (mNonNPWPTaxPercentage / 100);
            }
            if (Tax <= 0) Tax = 0m;
            r.Tax = Tax;
        }

        private void ComputeTax2(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI2> lTI2)
        {
            decimal
                 Tax = 0m,
                 NTIAmt = 0m,
                 FunctionalExpenses = 0m,
                 Amt = 0m,
                 JoinYr = 0m;

            string NTI = r.PTKP;

            bool IsFullYr = true;

            Amt =
                r.Salary +
                r.Functional +
                r.Regional +
                r.TaxableFixAllowance +
                r.FieldAssignment +
                r.Meal +
                r.Transport +
                r.IncProduction +
                r.OT1Amt +
                r.OT2Amt +
                r.OTHolidayAmt +
                r.SalaryAdjustment;

            Amt = Amt + r.SSEmployerEmployment + r.SSEmployerPension;

            FunctionalExpenses = (Amt * (mFunctionalExpenses / 100m));
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Tax =
                Amt -
                FunctionalExpenses -
                r.SSEmployeeOldAgeInsurance -
                r.SSEmployeePension;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Tax = Tax * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Tax = Tax * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Tax > NTIAmt)
                Tax -= NTIAmt;
            else
                Tax = 0m;

            Tax = Tax % 1000 >= 500 ? Tax + 1000 - Tax % 1000 : Tax - Tax % 1000;

            if (Tax > 0 && lTI2.Count > 0)
            {
                foreach (TI2 i in lTI2.OrderBy(x => x.SeqNo))
                {
                    if (Tax >= i.Amt1 && Tax <= i.Amt2)
                    {
                        Tax = ((Tax - (i.Amt1 - 1m)) * (i.Value1 / i.Value2)) + i.Value3;
                        break;
                    }
                }
            }
            Tax = Tax / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            if (Tax <= 0) Tax = 0m;
            r.Tax = Tax;
        }

        #endregion

        #region Get Tables Data

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            SQL.AppendLine("    And Not Exists( ");
            SQL.AppendLine("        Select DocNo From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode<>@PayrunCode ");
            SQL.AppendLine("        And Yr=T2.Yr ");
            SQL.AppendLine("        And Mth=T2.Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' " + Filter);;
            SQL.AppendLine(";");

            string Year = Sm.Left(Sm.GetDte(DteEndDt), 4), Month = Sm.GetDte(DteEndDt).Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI2(ref List<TI2> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.Value1, B.Value2, B.Value3 ");
            SQL.AppendLine("From TblTI2Hdr A ");
            SQL.AppendLine("Inner Join TblTI2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI2()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Value1 = Sm.DrDec(dr, c[4]),
                            Value2 = Sm.DrDec(dr, c[5]),
                            Value3 = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd ");
            SQL.AppendLine("From TblLeaveDtl;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LeaveCode", "SystemType", "PayrunPeriod", "DeductTHPInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Where ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter + ";");
            else
                SQL.AppendLine(" 0=1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", "SiteCode",

                    //11
                    "HOInd"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            EmploymentStatus = Sm.DrStr(dr, c[6]),
                            SystemType = Sm.DrStr(dr, c[7]),
                            PayrunPeriod = Sm.DrStr(dr, c[8]),
                            PGCode = Sm.DrStr(dr, c[9]),
                            SiteCode = Sm.DrStr(dr, c[10]),
                            HOInd = Sm.DrStr(dr, c[11])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "In1", "Out1", 
                    
                    //6-9
                    "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            In1 = Sm.DrStr(dr, c[4]),
                            Out1 = Sm.DrStr(dr, c[5]),
                            In2 = Sm.DrStr(dr, c[6]),
                            Out2 = Sm.DrStr(dr, c[7]),
                            In3 = Sm.DrStr(dr, c[8]),
                            Out3 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("Where A.CancelInd='N'; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T1.") + ") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessExtraFooding(ref List<ExtraFooding> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.WSCode, T2.GrdLvlCode, Sum(T2.Amt) As Amt ");
            SQL.AppendLine("From TblWorkScheduleAD T1 ");
            SQL.AppendLine("Inner Join TblGradeLevelDtl T2 ");
            SQL.AppendLine("    On T1.ADCode=T2.ADCode ");
            SQL.AppendLine("    And T2.ADCode In ('01', '05', '06') ");
            //SQL.AppendLine("    And Position(Concat('#', T2.ADCode, '#') In @VarAllowance3)>0 ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select Distinct WSCode From TblEmpWorkSchedule ");
            SQL.AppendLine("    Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0) SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    And WSCode=T1.WSCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Group By T1.WSCode, T2.GrdLvlCode;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "WSCode", "GrdLvlCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ExtraFooding()
                        {
                            WSCode = Sm.DrStr(dr, c[0]),
                            GrdLvlCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessFieldAssignment(ref List<FieldAssignment> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(X.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select Distinct T1.WSCode, T2.EmpCode, Amt ");
            SQL.AppendLine("From TblWorkScheduleAD T1 ");
            SQL.AppendLine("Inner Join TblEmployeeAllowanceDeduction T2 ");
            SQL.AppendLine("    On T1.ADCode=T2.ADCode ");
            SQL.AppendLine("    And T2.ADCode=@ADCode ");
            if (Filter.Length != 0) SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.") + ") ");
            SQL.AppendLine("Where T1.ADCode=@ADCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select Distinct WSCode From TblEmpWorkSchedule ");
            SQL.AppendLine("    Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0) SQL.AppendLine("    And (" + Filter.Replace("X.", string.Empty) + ") ");
            SQL.AppendLine("    And WSCode=T1.WSCode ");
            SQL.AppendLine(");");


            Sm.CmParam<String>(ref cm, "@ADCode", mADCodeFieldAssignment);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WSCode", "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new FieldAssignment()
                        {
                            WSCode = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessGradeLevel(ref List<GradeLevel> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select GrdLvlCode, BasicSalary, BasicSalary2 From TblGradeLevelHdr;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "BasicSalary", "BasicSalary2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new GradeLevel() 
                        { 
                            GrdLvlCode = Sm.DrStr(dr, c[0]),
                            BasicSalary = Sm.DrDec(dr, c[1]),
                            BasicSalary2 = Sm.DrDec(dr, c[2])
                        });
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblEmployeeAllowanceDeduction B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And B.ADCode=@ADCodeIncPerformance ");
            SQL.AppendLine("    And (B.StartDt is Null Or (B.StartDt is Not Null And B.StartDt<=@StartDt)) ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter.Replace("Tbl.", "A.") + ") ;");

            Sm.CmParam<string>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<string>(ref cm, "@ADCodeIncPerformance", mADCodeIncPerformance);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "JoinDt", "ResignDt", "Amt" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            IncPerformance = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            if (mSalaryInd == "1")
            {
                SQL.AppendLine("Select A.EmpCode, A.StartDt, A.Amt, A.Amt2, 0 As SalaryAD ");
                SQL.AppendLine("From TblEmployeeSalary A ");
                SQL.AppendLine("Where A.StartDt<=@EndDt ");
                //SQL.AppendLine("Where A.ActInd='Y' ");
                if (Filter.Length != 0)
                    SQL.AppendLine("And (" + Filter + ") ;");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode, B.StartDt, IfNull(B.Amt, 0) As Amt, IfNull(B.Amt2, 0) As Amt2, IfNull(C.Amt, 0) As SalaryAD ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblEmployeeSalary B On A.EmpCode=B.EmpCode And B.StartDt<=@EndDt ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select EmpCode, Sum(Amt) As Amt ");
                SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("    Where Find_In_Set(IfNull(ADCode, ''), @ADCodeSalary) ");
                if (Filter.Length != 0)
                SQL.AppendLine("    And (" + Filter.Replace("A.", string.Empty) + ") ");
                SQL.AppendLine("    Group By EmpCode ");
                SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");
                if (Filter.Length != 0)
                    SQL.AppendLine("Where (" + Filter + ") ;");
            }

            Sm.CmParam<String>(ref cm, "@ADCodeSalary", mADCodeSalary);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "StartDt", "Amt", "Amt2", "SalaryAD" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            SalaryAD = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessProductionWages(ref List<ProductionWages> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T2.EmpCode, T1.DocDt As Dt, Sum(T2.Wages2) As Amt ");
            SQL.AppendLine("From TblPWGHdr T1 ");
            SQL.AppendLine("Inner Join TblPWGDtl5 T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group By T2.EmpCode, T1.DocDt;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "Dt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionWages()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessProductionPenalty(ref List<ProductionPenalty> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T.EmpCode, T.Dt, Sum(T.Amt) As Amt From ( ");
            SQL.AppendLine("        Select T2.EmpCode, T1.DocDt As Dt, T2.Penalty As Amt ");
            SQL.AppendLine("        From TblPNTHdr T1 ");
            SQL.AppendLine("        Inner Join TblPNTDtl4 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T1.DocDt As Dt, T2.Penalty As Amt ");
            SQL.AppendLine("        From TblPNT2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblPNT2Dtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    ) T Group By T.EmpCode, T.Dt;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionPenalty()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessProductionPenaltyLeave(ref List<ProductionPenaltyLeave> l)
        {
            //l.Clear();

            //var cm = new MySqlCommand();
            //var SQL = new StringBuilder();
            //string Filter = string.Empty;
            //string EmpCode = string.Empty;

            //if (Grd1.Rows.Count >= 1)
            //{
            //    int No = 1;
            //    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    {
            //        EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
            //        if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
            //        {
            //            if (Filter.Length > 0) Filter += " Or ";
            //            Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
            //            Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
            //            No += 1;
            //        }
            //    }
            //}

            //SQL.AppendLine("Select T.EmpCode, T.Dt, Sum(T.Amt) As Amt From ( ");

            //SQL.AppendLine("    Select T2.EmpCode, T1.DocDt As Dt, ");
            //SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As Amt ");
            //SQL.AppendLine("    From TblPWGHdr T1 ");
            //SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='N' ");
            //SQL.AppendLine("    And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            //SQL.AppendLine("    Inner Join (");
            //SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            //SQL.AppendLine("        From ( ");
            //SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            //SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            //SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            //SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            //SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 On X1.DocNo=X2.DocNo And X2.LeaveDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            //SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            //SQL.AppendLine("            And (" + Filter.Replace("Tbl.", "X1.") + ") ");
            //SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            //SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            //SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    And T1.DocDt Between @StartDt And @EndDt ");

            //SQL.AppendLine("    Union All ");

            //SQL.AppendLine("    Select T2.EmpCode, T1.DocDt As Dt, ");
            //SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As Amt ");
            //SQL.AppendLine("    From TblPWGHdr T1 ");
            //SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='Y' ");
            //SQL.AppendLine("    And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            //SQL.AppendLine("    Inner Join (");
            //SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            //SQL.AppendLine("        From ( ");
            //SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            //SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            //SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            //SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            //SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 On X1.DocNo=X2.DocNo And X2.LeaveDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            //SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            //SQL.AppendLine("            And (" + Filter.Replace("Tbl.", "X1.") + ") ");
            //SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            //SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            //SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    And T1.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine(") T Group By T.EmpCode, T.Dt;");

            //Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            //Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            //Sm.CmParam<Decimal>(ref cm, "@DirectLaborDailyWorkHour", mDirectLaborDailyWorkHour);
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Amt" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            l.Add(new ProductionPenaltyLeave()
            //            {
            //                EmpCode = Sm.DrStr(dr, c[0]),
            //                Dt = Sm.DrStr(dr, c[1]),
            //                Amt = Sm.DrDec(dr, c[2])
            //            });
            //        }
            //    }
            //    dr.Close();
            //}
        }

        private void ProcessProductionMinWages(ref List<ProductionMinWages> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Sum(Amt) As Amt From ( ");
            SQL.AppendLine("    Select T3.EmpCode, T1.DocDt As Dt, T3.InsentifAmt As Amt ");
            SQL.AppendLine("    From TblPWGHdr T1");
            SQL.AppendLine("    Inner join TblInsentifHdr T2 On T1.DocNo = T2.PWGDocNo And T2.CancelInd ='N' ");
            SQL.AppendLine("    Inner Join TblInsentifDtl T3 ");
            SQL.AppendLine("        On T2.DocNo = T3.DocNo ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.EmpCode, T1.DocDt As Dt, T2.Incentive As Amt ");
            SQL.AppendLine("    From TblIncentiveHdr T1 ");
            SQL.AppendLine("    Inner Join TblIncentiveDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (" + Filter.Replace("T3.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") T Group BY EmpCode, Dt ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionMinWages()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessProductionInsentif(ref List<ProductionInsentif> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T2.EmpCode, T1.EndDt As Dt, Sum(T2.InsentifAmt) As Amt ");
            SQL.AppendLine("From TblInsentif2Hdr T1 ");
            SQL.AppendLine("Inner Join TblInsentif2Dtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.EndDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group BY T2.EmpCode, T1.EndDt;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionInsentif()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            var EndDtTemp = Sm.GetDte(DteEndDt);

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeVarAllowance(ref List<EmployeeVarAllowance> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.ADCode, A.StartDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("    On A.ADCode=B.ADCode ");
            SQL.AppendLine("    And B.ADType='A' ");
            SQL.AppendLine("    And B.AmtType='2' ");
            SQL.AppendLine("Where A.StartDt<=@EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "StartDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeVarAllowance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeVarAllowanceSite(ref List<EmployeeVarAllowanceSite> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string 
                Filter = string.Empty,
                Filter2 = string.Empty, 
                EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0;r<Grd1.Rows.Count;r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.ADCode, A.StartDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("    On A.ADCode=B.ADCode ");
            SQL.AppendLine("    And B.ADType='A' ");
            SQL.AppendLine("    And B.AmtType='2' ");
            SQL.AppendLine("Where A.SiteCode Is Not Null ");
            SQL.AppendLine("And A.SiteCode=@SiteCode ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");

            if (mADCodeMeal.Length > 0)
            {
                Filter2 = " A.ADCode=@ADCodeMeal ";
                Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);
            }

            if (mADCodeTransport.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " A.ADCode=@ADCodeTransport ";
                Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            }

            if (mADCodeFieldAssignment.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " A.ADCode=@ADCodeFieldAssignment ";
                Sm.CmParam<String>(ref cm, "@ADCodeFieldAssignment", mADCodeFieldAssignment);
            }

            if (Filter2.Length > 0)
                Filter2 = " And (" + Filter2 + ") ";
            else
                Filter2 = " And 0=1 ";

            SQL.AppendLine(Filter2);
            SQL.AppendLine("; ");
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeVarAllowanceSite()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeVarAllowanceMultiplePeriod(ref List<EmployeeVarAllowanceMultiplePeriod> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string
                Filter = string.Empty,
                Filter2 = string.Empty,
                EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.ADCode, A.StartDt, IfNull(A.EndDt, '20991231') As EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("    On A.ADCode=B.ADCode ");
            if (Filter.Length != 0)
                SQL.AppendLine(" Where (" + Filter + ") ");
            else
                SQL.AppendLine(" Where 0=1 ");

            if (mADCodeMeal.Length > 0)
            {
                //Filter2 = " A.ADCode=@ADCodeMeal ";
                //Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);

                Filter2 = 
                    " Find_In_Set( " +
                    "    IfNull(A.ADCode, ''), " +
                    "    IfNull((Select ParValue From TblParameter Where ParCode='ADCodeMeal'), '') " +
                    "    ) ";
            }

            if (mADCodeTransport.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " A.ADCode=@ADCodeTransport ";
                Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            }

            if (mADCodeFieldAssignment.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " A.ADCode=@ADCodeFieldAssignment ";
                Sm.CmParam<String>(ref cm, "@ADCodeFieldAssignment", mADCodeFieldAssignment);
            }

            if (Filter2.Length > 0)
                Filter2 = " And (" + Filter2 + ") ";
            else
                Filter2 = " And 0=1 ";

            SQL.AppendLine(Filter2);
            SQL.AppendLine("; ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "ADCode", "StartDt", "EndDt", "Amt" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeVarAllowanceMultiplePeriod()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            EndDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessGradeLevelVarAllowance(ref List<GradeLevelVarAllowance> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.GrdLvlCode, A.ADCode, A.Amt ");
            SQL.AppendLine("From TblGradeLevelDtl A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B ");
            SQL.AppendLine("    On A.ADCode=B.ADCode ");
            SQL.AppendLine("    And B.ADType='A' ");
            SQL.AppendLine("    And B.AmtType='2' ");
            SQL.AppendLine("; ");
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "ADCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new GradeLevelVarAllowance()
                        {
                            GrdLvlCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessGradeLevelPresenceReward(ref List<GradeLevelPresenceReward> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select GrdLvlCode, ADCode, Amt ");
            SQL.AppendLine("From TblGradeLevelDtl ");
            SQL.AppendLine("Where ADCode In ("+mADCodePresenceReward+"); ");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "ADCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new GradeLevelPresenceReward()
                        {
                            GrdLvlCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<string>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessIncEmployee(ref List<IncEmployee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.DocDt, T2.EmpCode, Sum(T1.AmtInspnt) As Amt ");
            SQL.AppendLine("From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpInsPntDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='01' ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Group By T1.DocDt, T2.EmpCode;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DocDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new IncEmployee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpAD()
        {
            if (ChkDelData.Checked)
                ProcessEmpAD1a();
            else
                ProcessEmpAD1b();
            ProcessEmpAD2();
        }

        private void ProcessEmpAD1a()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblEmpAD ");
            SQL.AppendLine("Where PayrunCode Is Not Null ");
            SQL.AppendLine("And PayrunCode=@PayrunCode ");
            SQL.AppendLine("And ADCode=@ADCode;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ADCode", mADCodeEmploymentPeriod);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            Sm.ExecCommand(cm);
        }

        private void ProcessEmpAD1b()
        {
            string EmpCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length != 0)
                Filter = " And ( " + Filter + ");";
            else
                Filter = " And 1=0;";

            SQL.AppendLine("Delete From TblEmpAD ");
            SQL.AppendLine("Where PayrunCode Is Not Null ");
            SQL.AppendLine("And PayrunCode=@PayrunCode ");
            SQL.AppendLine("And ADCode=@ADCode ");
            SQL.AppendLine(Filter);

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ADCode", mADCodeEmploymentPeriod);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            Sm.ExecCommand(cm);
        }

        private void ProcessEmpAD2()
        {
            string EmpCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<EmpAD>();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + r.ToString(), EmpCode);        
                    }
                }
            }

            if (Filter.Length != 0)
                Filter = " And ( " + Filter +") ";
            else
                Filter = " And 1=0 ";

            Sm.CmParam<String>(ref cm, "@ADCodeSalary", mADCodeSalary);

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, B.Amt+IfNull(C.Amt, 0) As Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblEmployeeSalary B On A.EmpCode=B.EmpCode And B.ActInd='Y' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select EmpCode, Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("    Where Find_In_Set(IfNull(ADCode, ''), @ADCodeSalary) ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Where 0=0 " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpAD()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                int 
                    y1 = int.Parse(Sm.Left(Sm.GetLue(LuePayrunCode), 4)),
                    y2 = 0;
                string 
                    Dt = string.Empty,
                    StartDt = Sm.Left(Sm.GetDte(DteStartDt), 8),
                    EndDt = Sm.Left(Sm.GetDte(DteEndDt), 8);

                for (int i = 0; i < l.Count; i++)
                {
                    Dt = string.Empty;
                    y2 = (int.Parse(Sm.Left(l[i].JoinDt, 4)) + (int)(mADCodeEmploymentPeriodYr));
                    while (y2 <= y1)
                    {
                        if (y2 == y1)
                        {
                            Dt = y2.ToString();
                            Dt = Sm.Right(l[i].JoinDt, 4);
                            Dt = string.Concat(y2.ToString(), Sm.Right(l[i].JoinDt, 4));
                            if (StartDt.CompareTo(Dt) != 1 && Dt.CompareTo(EndDt) != 1)
                            {
                                l[i].Dt = Dt;
                                l[i].Yr = y2.ToString();
                                ProcessEmpAD2(l[i]);
                            }
                            break;
                        }
                        y2 += (int)mADCodeEmploymentPeriodYr;
                    }
                }
                l.Clear();
            }
        }

        private void ProcessEmpAD2(EmpAD x)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpAD(Dt, EmpCode, ADCode, CancelInd, Yr, Amt, PayrunCode, CreateBy, CreateDt) " +
                    "Values(@Dt, @EmpCode, @ADCode, 'N', @Yr, @Amt, @PayrunCode, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@Dt", x.Dt);
            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@ADCode", mADCodeEmploymentPeriod);
            Sm.CmParam<String>(ref cm, "@Yr", x.Yr);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                GetParameter();
                GetValue();
                SetLuePayrunCode(ref LuePayrunCode);
                SetGrd();

                mlAttendanceLog = new List<AttendanceLog>();
                //mlOTFormula = new List<OTFormula>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlEmployeeVarAllowance = new List<EmployeeVarAllowance>();
                mlEmployeeVarAllowanceMultiplePeriod = new List<EmployeeVarAllowanceMultiplePeriod>();
                mlGradeLevelVarAllowance = new List<GradeLevelVarAllowance>();
                mlGradeLevelPresenceReward = new List<GradeLevelPresenceReward>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LuePayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData();
            Sm.RefreshLookUpEdit(LuePayrunCode, new Sm.RefreshLue1(SetLuePayrunCode));
        }

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            ClearData();
            ShowPayrunInfo();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        #endregion

        #region Class

        private class Result1
        {
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public string DeptCodeActual { get; set; }
        }

        private class Result2
        {
            public string _LatestGrdLvlCode { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
	        public decimal FixAllowance { get; set; }
	        public decimal EmploymentPeriodAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal IncMinWages { get; set; }
	        public decimal IncProduction { get; set; }
	        public decimal IncPerformance { get; set; }
	        public decimal PresenceReward { get; set; }
	        public decimal HolidayEarning { get; set; }
	        public decimal ExtraFooding { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal DedProduction { get; set; }
	        public decimal DedProdLeave { get; set; }
            public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal Functional { get; set; }
            public decimal Regional { get; set; }
        }

        #region Additional Class

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal SalaryAD { get; set; }
        }

        private class IncEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class DedEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class TI2
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
        }

        private class EmpWorkSchedule
         {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class ExtraFooding
        {
            public string WSCode { get; set; }
            public string GrdLvlCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class FieldAssignment
        {
            public string EmpCode { get; set; }
            public string WSCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
            public decimal IncPerformance { get; set; }
        }

        private class GradeLevel
        {
            public string GrdLvlCode { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
        }

        private class ProductionWages
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProductionPenalty
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProductionPenaltyLeave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProductionMinWages
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProductionInsentif
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmployeeVarAllowance
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class GradeLevelVarAllowance
        {
            public string GrdLvlCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class GradeLevelPresenceReward
        {
            public string GrdLvlCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpAD
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public decimal Amt { get; set; }
            public string Dt { get; set; }
            public string Yr { get; set; }
        }

        private class EmployeeVarAllowanceSite
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
        }


        private class EmployeeVarAllowanceMultiplePeriod
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

        #endregion
    }
}
