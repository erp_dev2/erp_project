﻿#region Update
/*
    04/07/2017 [HAR] tambah grid input route ETA ETD  
    07/07/2018 [TKG] tambah default KPBC berdasarkan parameter SPKPBCDefault saat insert new data
    07/07/2018 [TKG] default consignee dari nama customer.
    10/07/2018 [HAR] tambah inputan place delivery place receipt berdasarkan parameter
    10/07/2018 [ARI] HS menggunakan combo
    13/07/2018 [HAR] Ubah sususnan inputan
    01/08/2018 [ARI] label local document diganti No. invoice (pake parameter localdocument)
    14/08/2018 [TKG] tambah fasilitas untuk meng-copy port of discharge ke place of delivery
    04/09/2018 [HAR] tambah  button download
    10/09/2018 [TKG] copy PEB dan BL No otomatis ke SI.
    12/09/2018 [HAR] status released edit bagian kanan 
    22/09/2018 [HAR] status released edit bagian kanan ini gak jadi pingin bisa edit semua
    24/09/2018 [HAR] status final edit bagian kanan saja berdasarkan parameter
    12/11/2018 [HAR] ftp tambah parameter buat nentuin format koneksi FTP
    02/03/2019 [TKG] tambah validasi IsDKOExisted() saat dokumen mau dicancel tetapi sudah dibuat DKO.
    19/03/2022 [TKG/PHT] merubah GetParameter() dan proses save
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmSP : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mIsSPUpdateDataToSInv = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmSPFind FrmFind;
        private bool 
            mIsNotCopySalesLocalDocNo = false,
            mIsSPUseCustomerShippingAddress = false,
            mIsSPFinalAllowEdit = false;
        private string 
            mSPKPBCDefault = string.Empty,
            mLocalDocument = "0";
        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmSP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Shipment Planning";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueType, "EstimatedType");
                SetLueCtCode(ref LueCtCode);
                SetLueLoading(ref LueLoading);
                SetLueStatus(ref LueStatus);
                SetLueHS(ref LueHSCode);
                LueType.Visible = false;
                if (mLocalDocument == "1")
                    LblLocalDoc.Text = "      No. Invoice";
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Route",
                        
                        "Date", "Typecode", "Type"
                    },
                    new int[] 
                    {
                         200, 
                         130, 80, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
        }

     
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt,DteStfDt, LueStatus, LueCtCode, 
                        LueNotify, TxtBL, LueHSCode, TxtPEB, TxtKpbc, 
                        MeeReceipt, LueLoading, LueDischarge, MeeDelivery, MeeNotify,
                        TxtMark, TxtSize, MeeConsignee, TxtSource, TxtSPName, 
                        TxtLocalDocNo, TxtVLegal, DteVLegalDt, DtePEBDt, TxtNameDelivery, TxtNameReceipt
                    }, true);
                    TxtDocNo.Focus();
                    Grd1.ReadOnly = true;
                    BtnCtNotify.Enabled = false;
                    BtnSource.Enabled = false;
                    BtnReceipt.Enabled = false;
                    BtnPlaceDelivery.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteStfDt, LueCtCode, LueNotify, TxtBL, 
                        LueHSCode, MeeConsignee, MeeNotify, TxtLocalDocNo, TxtPEB, 
                        TxtKpbc, MeeReceipt, LueLoading, LueDischarge, MeeDelivery, 
                        TxtMark, TxtSize, TxtSPName, TxtVLegal, DteVLegalDt, TxtNameDelivery, 
                        TxtNameReceipt, DtePEBDt
                    }, false);
                    DteDocDt.Focus();
                    Grd1.ReadOnly = false;
                    BtnCtNotify.Enabled = true;
                    BtnSource.Enabled = true;
                    if (!mIsSPUseCustomerShippingAddress) BtnReceipt.Enabled = true;
                    BtnPlaceDelivery.Enabled = true;
                    break;
                case mState.Edit:
                    if (Sm.GetLue(LueStatus) == "F" && mIsSPFinalAllowEdit == true)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtKpbc, LueLoading, TxtNameReceipt, TxtNameDelivery, 
                            LueDischarge, MeeReceipt, MeeDelivery, TxtMark, TxtSize,
                            TxtSize, TxtSPName, TxtVLegal, DteVLegalDt, DtePEBDt
                        }, false);
                        Grd1.ReadOnly = true;
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueNotify, DteStfDt, TxtBL, LueHSCode, LueStatus, 
                            TxtPEB, TxtKpbc, MeeReceipt, TxtLocalDocNo, LueLoading, 
                            LueDischarge, MeeDelivery, TxtMark, TxtSize, MeeConsignee, 
                            MeeNotify, TxtSPName, TxtVLegal, DteVLegalDt, DtePEBDt, TxtNameDelivery, 
                            TxtNameReceipt, 
                        }, false);
                        Grd1.ReadOnly = false;
                        BtnSource.Enabled = true;
                        if (!mIsSPUseCustomerShippingAddress) BtnReceipt.Enabled = true;
                        BtnPlaceDelivery.Enabled = true;
                    }
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteStfDt, LueCtCode, LueStatus, 
                LueNotify, TxtBL, LueHSCode, TxtLocalDocNo, TxtPEB, 
                TxtKpbc, MeeReceipt, LueLoading, LueDischarge, MeeDelivery,
                MeeConsignee, MeeNotify, TxtMark, TxtSize, TxtSource, 
                TxtSPName, TxtVLegal, DteVLegalDt, DtePEBDt, TxtNameDelivery, TxtNameReceipt
            });
            ClearGrd();
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueNotify, TxtBL, LueHSCode, MeeConsignee, TxtPEB, 
                TxtKpbc, MeeReceipt, LueLoading, LueDischarge, MeeDelivery, 
                MeeNotify, TxtMark, TxtSize, TxtSource, TxtSPName, 
                TxtVLegal, DteVLegalDt, DtePEBDt, TxtNameReceipt, TxtNameDelivery
            });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetLue(LueStatus, "P");
                Sm.SetDteCurrentDate(DteDocDt);
                DteStfDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                TxtKpbc.EditValue = mSPKPBCDefault;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No  || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SP", "TblSP");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSP(DocNo));
            cml.Add(SaveSPDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            // cml.Add(SaveSPDtl(DocNo, Row));
            //
            if (Sm.GetLue(LueStatus) == "C") cml.Add(UpdateSO(DocNo));
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                Sm.IsDteEmpty(DteDocDt, "Stuffing Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueNotify, "Notify Party") ||
                Sm.IsMeeEmpty(MeeConsignee, "Consignee") ||
                Sm.IsMeeEmpty(MeeDelivery, "Place of Delivery") ||
                Sm.IsLueEmpty(LueLoading, "Port of Loading") ||
                Sm.IsMeeEmpty(MeeReceipt, "Place of Receipt") ||
                Sm.IsLueEmpty(LueDischarge, "Port of Discharge") ||
                IsSPCancelled() ||
                IsSPReleased() ||
                IsSPFinal() ||
                IsDKOExisted()
                ;
        }

        private bool IsDKOExisted()
        {
            if (!(TxtDocNo.Text.Length>0 && Sm.GetLue(LueStatus)=="C")) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDKO A ");
            SQL.AppendLine("Inner Join TblPLHdr B On A.PLDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSIHdr C On B.SIDocNo=C.DocNo And C.SPDocNo=@Param ");
            SQL.AppendLine("Where A.CancelInd='N' And A.PLDocNo Is Not Null ");
            SQL.AppendLine("Limit 1;");
            
            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text,
                "You can't cancel this document." +Environment.NewLine +
                "This document has already processed to DKO."
                );
        }

        private bool IsSPReleased()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Where A.Status = 'R' And A.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm) && Sm.GetLue(LueStatus) == "P")
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Released.");
                return true;
            }
            return false;
        }

        private bool IsSPFinal()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Where A.Status = 'F' And A.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm) && !mIsSPFinalAllowEdit)
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Final.");
                return true;
            }
            return false;
        }

        private bool IsSPCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Where A.Status = 'C' And A.DocNo=@DocNo "
            };

            var cm2 = new MySqlCommand()
            {
                CommandText =
                    "Select Distinct PLDOcNo "+
                    "From TblDoCt2hdr A "+
                    "Inner Join TblDoCt2Dtl A2 On A.DocNo = A2.DocNo And A2.CancelInd = 'N' "+
                    "Inner Join TblPLHdr B On A.PlDocNo = B.DocNo "+
                    "Inner Join TblSIHdr C On B.SIDocNo = C.DocNo "+
                    "Inner Join TblSp D On C.SPDocNo = D.DocNo "+
                    "Where D.DocNo=@DocNo "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);


            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled .");
                return true;
            }
            if (Sm.IsDataExist(cm2) && Sm.GetLue(LueStatus) == "C")
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed to Delivery Order .");
                return true;
            }

            return false;
        }
      
        private MySqlCommand SaveSP(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblSpDtl Where DocNo = @DocNo ; ");

            SQL.AppendLine("Insert Into TblSP(DocNo, LocalDocNo, DocDt, StfDt, Status, CtCode, CtNotifyParty, CtNotifyParty2, BLNo, HSCode, Consignee, PEB, KPBC, PlaceReceipt, ");
            SQL.AppendLine("PortCode1, PortCode2, PlaceDelivery, Mark, Size, SpName, VLegal, VLegalDt, PEBDt, SANameDelivery, SANameReceipt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, @StfDt, @Status, @CtCode, @CtNotifyParty, @CtNotifyParty2, @BLNo, @HSCode, @Consignee, @PEB, @KPBC, @PlaceReceipt, ");
            SQL.AppendLine("@PortCode1, @PortCode2, @PlaceDelivery, @Mark, @Size, @SpName, @VLegal, @VLegalDt, @PEBDt, @SANameDelivery, @SANameReceipt,  @UserCode,  CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    LocalDocNo=@LocalDocNo, StfDt=@StfDt, Status=@Status, CtNotifyParty=@CtNotifyParty, CtNotifyParty2=@CtNotifyParty2, BLNo=@BLNo, HSCode=@HSCode, Consignee=@Consignee, PEB=@PEB, KPBC=@KPBC, PlaceReceipt=@PlaceReceipt, ");
            SQL.AppendLine("    PortCode1=@PortCode1, PortCode2=@PortCode2, PlaceDelivery=@PlaceDelivery, Mark=@Mark, Size=@Size, VLegal=@VLegal, VLegalDt=@VLegalDt, PEBDt=@PEBDt, LastUpBy=@UserCode, ");
            SQL.AppendLine("    SpName=@SpName, SANameDelivery=@SANameDelivery, SANameReceipt=@SANameReceipt,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (mIsSPUpdateDataToSInv == "N") 
            {
                if (!mIsNotCopySalesLocalDocNo)
                {
                    SQL.AppendLine("Update TblSIHdr SET LocalDocNo=@LocalDocNo, SPPortCode2=@PortCode2, SPBLNo=@BLNo, SPPEB=@PEB Where SPDocNo=@DocNo; ");
                    SQL.AppendLine("Update TblPLHdr SET LocalDocNo=@LocalDocNo Where DocNo=@PLDocNo; ");
                    SQL.AppendLine("Update TblSinv SET LocalDocNo=@LocalDocNo Where DocNo=@SInvDocNo; ");
                }
            }
            else
            {
                if (mIsNotCopySalesLocalDocNo)
                {
                    SQL.AppendLine("Update TblSIHdr Set ");
                    SQL.AppendLine("    SPPortCode2=@PortCode2, ");
                    SQL.AppendLine("    SPStfDt=@StfDt, ");
                    SQL.AppendLine("    SPCtNotifyParty=@CtNotifyParty, ");
                    SQL.AppendLine("    SPBLNo=@BLNo, ");
                    SQL.AppendLine("    SPHSCode=@HSCode,  ");
                    SQL.AppendLine("    SPConsignee=@Consignee, ");
                    SQL.AppendLine("    SPPEB=@PEB, ");
                    SQL.AppendLine("    SPKPBC=@KPBC, ");
                    SQL.AppendLine("    SPPlaceReceipt=@PlaceReceipt, ");
                    SQL.AppendLine("    SpPortCode1=@PortCode1, ");
                    SQL.AppendLine("    SpPortCode2=@PortCode2, ");
                    SQL.AppendLine("    SpPlaceDelivery=@PlaceDelivery, ");
                    SQL.AppendLine("    SpMark=@Mark, ");
                    SQL.AppendLine("    SpSize=@Size ");
                    SQL.AppendLine("Where SPDocNo=@DocNo; ");
                }
                else
                {
                    SQL.AppendLine("Update TblSIHdr Set ");
                    SQL.AppendLine("    LocalDocNo=@LocalDocNo, ");
                    SQL.AppendLine("    SPPortCode2=@PortCode2, ");
                    SQL.AppendLine("    SPStfDt=@StfDt, ");
                    SQL.AppendLine("    SPCtNotifyParty=@CtNotifyParty, ");
                    SQL.AppendLine("    SPBLNo=@BLNo, ");
                    SQL.AppendLine("    SPHSCode=@HSCode,  ");
                    SQL.AppendLine("    SPConsignee=@Consignee, ");
                    SQL.AppendLine("    SPPEB=@PEB, ");
                    SQL.AppendLine("    SPKPBC=@KPBC, ");
                    SQL.AppendLine("    SPPlaceReceipt=@PlaceReceipt, ");
                    SQL.AppendLine("    SpPortCode1=@PortCode1, ");
                    SQL.AppendLine("    SpPortCode2=@PortCode2, ");
                    SQL.AppendLine("    SpPlaceDelivery=@PlaceDelivery, ");
                    SQL.AppendLine("    SpMark=@Mark, ");
                    SQL.AppendLine("    SpSize=@Size ");
                    SQL.AppendLine("Where SPDocNo=@DocNo; ");

                    SQL.AppendLine("Update TblPLHdr Set LocalDocNo=@LocalDocNo Where DocNo=@PLDocNo; ");
                    SQL.AppendLine("Update TblSinv Set LocalDocNo=@LocalDocNo Where DocNo=@SInvDocNo; ");
                }
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PLDocNo", Sm.GetValue("Select A.DocNo " +
                           "From TblPlhdr A " +
                           "Inner Join TblSIHdr B On A.SIDocNo = B.DocNo " +
                           "Inner Join TblSP C On B.SpDocNo = C.DocNo " +
                           "Where C.DocNo='" + DocNo + "' limit 1 "));
            Sm.CmParam<String>(ref cm, "@SinvDocNo", Sm.GetValue("Select A1.DocNo " +
                           "From TblSinv A1 " +
                           "Inner Join TblPlhdr A  On A1.PLDocNo = A.DocNo " +
                           "Inner Join TblSIHdr B On A.SIDocNo = B.DocNo " +
                           "Inner Join TblSP C On B.SpDocNo = C.DocNo " +
                           "Where C.DocNo='" + DocNo + "' limit 1 "));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StfDt", Sm.GetDte(DteStfDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtNotifyParty", Sm.GetLue(LueNotify));
            Sm.CmParam<String>(ref cm, "@CtNotifyParty2", MeeNotify.Text);
            Sm.CmParam<String>(ref cm, "@BLNo", TxtBL.Text);
            Sm.CmParam<String>(ref cm, "@Consignee", MeeConsignee.Text);
            Sm.CmParam<String>(ref cm, "@HSCode", Sm.GetLue(LueHSCode));
            Sm.CmParam<String>(ref cm, "@PEB", TxtPEB.Text);
            Sm.CmParam<String>(ref cm, "@KPBC", TxtKpbc.Text);
            Sm.CmParam<String>(ref cm, "@PlaceReceipt", MeeReceipt.Text);
            Sm.CmParam<String>(ref cm, "@PortCode1", Sm.GetLue(LueLoading));
            Sm.CmParam<String>(ref cm, "@PortCode2", Sm.GetLue(LueDischarge));
            Sm.CmParam<String>(ref cm, "@PlaceDelivery", MeeDelivery.Text);
            Sm.CmParam<String>(ref cm, "@Mark", TxtMark.Text);
            Sm.CmParam<String>(ref cm, "@Size", TxtSize.Text);
            Sm.CmParam<String>(ref cm, "@SPName", TxtSPName.Text);
            Sm.CmParam<String>(ref cm, "@VLegal", TxtVLegal.Text);
            Sm.CmParamDt(ref cm, "@VLegalDt", Sm.GetDte(DteVLegalDt));
            Sm.CmParamDt(ref cm, "@PEBDt", Sm.GetDte(DtePEBDt));
            Sm.CmParam<String>(ref cm, "@SANameDelivery", TxtNameDelivery.Text); 
            Sm.CmParam<String>(ref cm, "@SANameReceipt", TxtNameReceipt.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateSO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("    From TblSIDtl  A ");
            SQL.AppendLine("    Inner Join TblSIHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where B.SPDocNo=@SPDocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.SODocNo, A.SODNo, Sum(IfNull(B.QtyPackagingUnit, 0)) As QtyPackagingUnit ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("        From TblSIDtl  A ");
            SQL.AppendLine("        Inner Join TblSIHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where B.SPDocNo=@SPDocNo ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    Inner Join TblSIDtl B On A.SODocNo=B.SODocNo And A.SODNo=B.SODNo ");
            SQL.AppendLine("    Inner Join TblSIHdr C On B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblSP D On C.SPDocNo = D.DocNo And D.Status <> 'C' ");
            SQL.AppendLine("    Group By A.SODocNo, A.SODNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("Set T1.ProcessInd4 =  ");
            SQL.AppendLine("    Case When IfNull(T3.QtyPackagingUnit, 0)=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(T3.QtyPackagingUnit, 0)>=T1.QtyPackagingUnit Then 'F' Else 'P' End ");
            SQL.AppendLine("End; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SPDocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveSPDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Shipment Planning (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSPDtl(DocNo, DNo, Route, EstimatedDt, EstimatedType, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @Route_" + r.ToString() +
                        ", @EstimatedDt_" + r.ToString() +
                        ", @EstimatedType_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Route_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParamDt(ref cm, "@EstimatedDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@EstimatedType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSPDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSPDtl(DocNo, DNo, Route, EstimatedDt, EstimatedType, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @Route, @EstimatedDt, @EstimatedType,  @CreateBy, CurrentDateTime()); ");

            
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Route", Sm.GetGrdStr(Grd1, Row, 0));
        //    Sm.CmParamDt(ref cm, "@EstimatedDt", Sm.GetGrdDate(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@EstimatedType", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

       

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSP(DocNo, 0);
                ShowSPDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowSP(string DocNo, int con)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, LocalDocNo, DocDt, StfDt, Status, ");
            SQL.AppendLine("CtCode, CtNotifyParty, CtNotifyParty2, BLNo, HSCode, Consignee, ");
            SQL.AppendLine("PEB, KPBC, PlaceReceipt, PortCode1, PortCode2, PlaceDelivery, Mark, ");
            SQL.AppendLine("Size, SPName, VLegal, VLegalDt, PEBDt, SANameDelivery, SANameReceipt ");
            SQL.AppendLine("From TblSP Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "LocalDocNo", "DocDt", "StfDt", "Status", "CtCode",   
                        //6-10
                        "CtNotifyParty","CtNotifyParty2", "BLNo", "HSCode", "Consignee",    
                        //11-15
                        "PEB","KPBC", "PlaceReceipt", "PortCode1", "PortCode2",   
                        //16-20
                        "PlaceDelivery", "Mark","Size", "SPName", "VLegal",
                        //21-24
                        "VLegalDt", "PEBDt", "SANameDelivery", "SANameReceipt", 
                     
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (con == 0)
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                            Sm.SetDte(DteStfDt, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueStatus, Sm.DrStr(dr, c[4]));
                        }
                        else
                        {
                            TxtSource.EditValue = Sm.DrStr(dr, c[0]);
                        }
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode));
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[6]));
                        MeeNotify.EditValue = Sm.DrStr(dr, c[7]);
                        TxtBL.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueHSCode, Sm.DrStr(dr, c[9]));
                        MeeConsignee.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPEB.EditValue = Sm.DrStr(dr, c[11]);
                        TxtKpbc.EditValue = Sm.DrStr(dr, c[12]);
                        MeeReceipt.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetLue(LueLoading, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueDischarge, Sm.DrStr(dr, c[15]));
                        MeeDelivery.EditValue = Sm.DrStr(dr, c[16]);
                        TxtMark.EditValue = Sm.DrStr(dr, c[17]);
                        TxtSize.EditValue = Sm.DrStr(dr, c[18]);
                        TxtSPName.EditValue = Sm.DrStr(dr, c[19]);
                        TxtVLegal.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteVLegalDt, Sm.DrStr(dr, c[21]));
                        Sm.SetDte(DtePEBDt, Sm.DrStr(dr, c[22]));
                        TxtNameDelivery.EditValue = Sm.DrStr(dr, c[23]);
                        TxtNameReceipt.EditValue = Sm.DrStr(dr, c[24]);
                    }, true
                );
        }

        private void ShowSPDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Route, A.EstimatedDt, A.EstimatedType, B.OptDesc from TblSPDtl A ");
            SQL.AppendLine("Inner Join TblOption B On A.EstimatedType = B.OptCode And B.OptCat = 'EstimatedType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.Dno");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "Route", 
                    "EstimatedDt", "EstimatedType", "OptDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3); ;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }



        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSPFinalAllowEdit', 'IsSPUseCustomerShippingAddress', 'IsNotCopySalesLocalDocNo', 'IsSPUpdateDataToSInv', 'SPKPBCDefault', ");
            SQL.AppendLine("'LocalDocument', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSPFinalAllowEdit": mIsSPFinalAllowEdit = ParValue == "Y"; break;
                            case "IsSPUseCustomerShippingAddress": mIsSPUseCustomerShippingAddress = ParValue == "Y"; break;
                            case "IsNotCopySalesLocalDocNo": mIsNotCopySalesLocalDocNo = ParValue == "Y"; break;
                            
                            //string
                            case "IsSPUpdateDataToSInv": mIsSPUpdateDataToSInv = ParValue; break;
                            case "SPKPBCDefault": mSPKPBCDefault = ParValue; break;
                            case "LocalDocument": mLocalDocument = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        public static void SetLueLoading(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PortCode As Col1, PortName As Col2 From TblPort " +
                "Where LoadingInd = 'Y' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Col1, Col2 From ( "+
                "Select 'P' As Col1, 'Planning' As Col2  " +
                "Union All "+
                "Select 'R' As Col1, 'Released' As Col2  "+
                "Union All " +
                "Select 'C' As Col1, 'Cancelled' As Col2  " +
                "Union All " +
                "Select 'F' As Col1, 'Final' As Col2  " +
                ")T ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        public static void SetLueDischarge(ref LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            string CtQtInd = Sm.GetValue("select B.CtQtInd "+
            "From tblCtQTHdr A "+
            "Inner Join TblDeliveryType B On A.ShpMCode=B.DtCode "+
            "Where A.CtCode = '" + CtCode + "' And A.ActInd ='Y' Order by A.DocDt Desc limit 1 ");

            if (CtQtInd == "Y")
            {
                SQL.AppendLine("Select B.PortCode As Col1, C.PortName As Col2 ");
                SQL.AppendLine("From TblCtQtHdr A ");
                SQL.AppendLine("Inner Join TblCtQtDtl4 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblPort C On B.PortCode = C.PortCode ");
                SQL.AppendLine("Where A.CtCOde='" + CtCode + "' And A.ActInd = 'Y' ");
                SQL.AppendLine("Group By A.CtCode, Dno Order by C.PortName ");
            }
            else
            {
                SQL.AppendLine("Select PortCode As Col1, PortName As Col2 From TblPort ");
                SQL.AppendLine("Where DischargeInd = 'Y' ");
            }

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueNotify(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
               ref Lue,
               "Select  NotifyParty As Col1 From TblCustomerNotifyParty  " +
               "Where CtCode= '" + CtCode + "' Order By NotifyParty ",
               "Notify Party");
        }

        public static void SetLueHS(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select HSCode As Col1, HSName As Col2 From TblHS ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //if (TxtDocNo.Text.Length == 0)
            //{
            //    SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
            //    SQL.AppendLine("From ( ");
            //    SQL.AppendLine("Select Distinct CtCode ");
            //    SQL.AppendLine("From TblSOHdr ");
            //    SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
            //}
            //else
            //{
                SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Distinct CtCode ");
                SQL.AppendLine("From TblSOHdr ");
                SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct CtCode  ");
                SQL.AppendLine("From TblSP ");
            //}
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode= B.CtCode And B.ActInd = 'Y' ");
            SQL.AppendLine("Order By B.CtName");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void DownloadFileKu(string TxtFile)
        {
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                Application.DoEvents();
                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = string.Concat(this.Text, "         ", "Connecting...");
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception exc)
            {
                Sm.StdMsg(mMsgType.Warning, exc.ToString());
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueNotify_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueNotify, new Sm.RefreshLue2(SetLueNotify), Sm.GetLue(LueCtCode));
        }

        private void DteRouteDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteRouteDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteRouteDt, ref fCell, ref fAccept);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption),"EstimatedType");
        }

        private void LueType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }       
       
        private void LueType_Leave(object sender, EventArgs e)
        {
                if (LueType.Visible && fAccept && fCell.ColIndex == 3)
                {
                    if (LueType.Text.Length == 0)
                    {
                        Grd1.Cells[fCell.RowIndex, 2].Value = null;
                        Grd1.Cells[fCell.RowIndex, 3].Value = null;
                    }
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueType);
                        Grd1.Cells[fCell.RowIndex, 3].Value = LueType.GetColumnValue("Col2");
                    }
                    LueType.Visible = false;
               }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 3) 
                    Sm.LueRequestEdit(ref Grd1, ref LueType, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                if (e.ColIndex == 1) Sm.DteRequestEdit(Grd1, DteRouteDt, ref fCell, ref fAccept, e);
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 0 }, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData2();
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
                SetLueDischarge(ref LueDischarge, Sm.GetLue(LueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueNotify.EditValue = null;
                    Sm.SetControlReadOnly(LueNotify, true);
                }
                else
                {
                    SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode));
                    Sm.SetControlReadOnly(LueNotify, false);
                    if (MeeConsignee.Text.Length == 0)
                        MeeConsignee.EditValue = LueCtCode.GetColumnValue("Col2");
                }
            }
        }

        private void BtnCtNotify_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                var f = new FrmCustomer(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mMenuCode = mMenuCode;
                f.mCtCode = Sm.GetLue(LueCtCode);
                f.ShowDialog();
                SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode));
            }
        }

        private void BtnSource_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSPDlg(this, Sm.GetLue(LueCtCode)));
        }

        private void TxtVLegal_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVLegal);

        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));

        }

        private void LueLoading_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLoading, new Sm.RefreshLue1(SetLueLoading));

        }

        private void LueDischarge_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDischarge, new Sm.RefreshLue2(SetLueDischarge), Sm.GetLue(LueCtCode));
        }

        private void BtnReceipt_Click(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueLoading).Length > 0)
                MeeReceipt.EditValue = Sm.GetValue("Select portName From tblPort Where portCode = '" + Sm.GetLue(LueLoading) + "'");
        }

        private void BtnDischarge_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSPDlg2(this, Sm.GetLue(LueCtCode), "2"));
        }

        private void LueHSCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueHSCode, new Sm.RefreshLue1(SetLueHS));
        }

        private void BtnPlaceDelivery_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueDischarge, "Port of discharge")) 
            {
                if (MeeDelivery.Text.Length>0)
                    if (Sm.StdMsgYN("Question", "Do you want to copy port of discharge to place of delivery ?") == DialogResult.No) return;
                MeeDelivery.EditValue = LueDischarge.GetColumnValue("Col2");
            }
        }

        #endregion

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        #endregion
    }
}
