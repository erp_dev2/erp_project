﻿#region Update
/*
    12/11/2020 [WED/VIR] new apps, Laba Rugi format 1
    13/11/2020 [WED/VIR] 8.100 diganti ke 8.110 && UPDATE `tbloption` SET `OptCode`='8.110' WHERE  `OptCat`='ProfitLoss6SpecialCategory' AND `OptCode`='8.100';
    04/01/2021 [WED/VIR] rombak (kategori dan deskripsi disesuaikan dengan draft, nilai 0 tetap muncul, COA yg journalnya connect dengan SOContract dimunculkan)
    08/03/2021 [WED/VIR] beberapa nilai dari list UpdatedRowData belum ikut terkalkulasi di proses special COA nya
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss6 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mProfitLoss6SelectedCOA = string.Empty;

        private bool
            mIsAccountingRptUseJournalPeriod = false,
            mIsRptAccountingShowJournalMemorial = false;
        
        #endregion

        #region Constructor

        public FrmRptProfitLoss6(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Category",
                    "Description",
                    "Selected COA#",
                    "Amount",
                    "Sequence",

                    //6-7
                    "Special Ind",
                    "COAHead"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 250, 0, 150, 0,

                    //6-7
                    0, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsTxtEmpty(TxtDocNo, "SO Contract#", false) || Sm.IsDteEmpty(DtePeriodTo, "Period To")) return;

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<RowData>();
            var l2 = new List<COA>();
            var l3 = new List<COASOContract>();
            var l4 = new List<UpdatedRowData>();
            var l5 = new List<UpdatedRowData2>();

            try
            {
                string mPeriodTo = Sm.Left(Sm.GetDte(DtePeriodTo), 8);

                PrepDataCOA(ref l2);
                if (l2.Count > 0)
                {
                    PrepDataCOASOContract(ref l2, ref l3);
                    if (l3.Count > 0)
                    {
                        string SelectedCOA = GetSelectedCOA(ref l3);
                        Process1(ref l3, SelectedCOA, mPeriodTo);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process2_JournalMemorial(ref l3, SelectedCOA, mPeriodTo);
                        else
                            Process2(ref l3, SelectedCOA, mPeriodTo);
                        Process3(ref l, ref l2, ref l3);
                        if (l.Count > 0)
                        {
                            Process4(ref l, ref l4);
                            Process5(ref l4, ref l5);
                            Process7(ref l5);
                            GrdSummary();
                        }
                        else
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, "No Data");
                return;
            }

            var l = new List<PrintOutHdr>();
            var ldtl = new List<PrintOutDtl>();
            string[] TableName = { "PrintOutHdr", "PrintOutDtl" };
            List<IList> myLists = new List<IList>();

            #region Header

            l.Add(new PrintOutHdr() 
            {
                ProjectName = TxtProjectName.Text.ToUpper(),
                PeriodTo = DtePeriodTo.Text.Replace("-", " ")
            });

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    ldtl.Add(new PrintOutDtl()
                    {
                        Category = Sm.GetGrdStr(Grd1, i, 1),
                        Description = Sm.GetGrdStr(Grd1, i, 2),
                        SelectedAcNo = Sm.GetGrdStr(Grd1, i, 3),
                        Amt = Sm.GetGrdDec(Grd1, i, 4),
                        Seq = Sm.GetGrdStr(Grd1, i, 5),
                        SpecialInd = Sm.GetGrdStr(Grd1, i, 6)
                    });
                }
            }

            #endregion

            myLists.Add(l);
            myLists.Add(ldtl);

            Sm.PrintReport("RptProfitLoss6", myLists, TableName, false);

            l.Clear(); ldtl.Clear(); myLists.Clear();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mProfitLoss6SelectedCOA = Sm.GetParameter("ProfitLoss6SelectedCOA");
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

            if (mProfitLoss6SelectedCOA.Length == 0) mProfitLoss6SelectedCOA = "5.001,5.002,5.003,5.004,5.005,5.006,5.007,5.009,5.010,5.100,6.001,6.002,6.004,6.005,6.007,6.008,7.000,8.000,8.110,8.200";

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void PrepDataCOA(ref List<COA> l2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(IfNull(IfNull(C.OptDesc, B.AcDescAlias), B.AcDesc), 'Penjualan') As ParentDesc, ");
            SQL.AppendLine("A.AcNo, IfNull(A.AcDescAlias, A.AcDesc) AcDesc, D.OptDesc As AcType ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblCOA B On A.Parent = B.AcNo ");
            SQL.AppendLine("Left Join TblOption C On A.AcNo = C.OptCode And C.OptCat = 'ProfitLoss6SpecialCategory' ");
            SQL.AppendLine("Left Join TblOption D On Left(A.AcNo, 1) = D.OptCode And D.OptCat = 'ProfitLoss6AcType' ");
            SQL.AppendLine("Where Find_In_Set(A.AcNo, @AcNo); ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AcNo", mProfitLoss6SelectedCOA);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParentDesc", "AcNo", "AcDesc", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COA()
                        {
                            ParentDesc = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            AcType = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepDataCOASOContract(ref List<COA> l2, ref List<COASOContract> l3)
        {
            var SQL = new StringBuilder();
            var lastObj = l2.Last();

            SQL.AppendLine("Select Distinct T.AcNo, T.Parent From ( ");
            SQL.AppendLine("    Select AcNo, Parent ");
            SQL.AppendLine("    From TblCOA ");
            SQL.AppendLine("    Where SOCDocNo Is Not Null ");
            SQL.AppendLine("    And SOCDocNo = @DocNo ");
            SQL.AppendLine("    And ");
            SQL.AppendLine("    ( ");

            foreach (var x in l2)
            {
                SQL.AppendLine("        AcNo Like '" + x.AcNo + "%' ");

                if (x != lastObj)
                    SQL.AppendLine("        Or  ");
            }

            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.AcNo, B.Parent ");
            SQL.AppendLine("    From TblJournalDtl A ");
            SQL.AppendLine("    Inner Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo = @DocNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Order By T.AcNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COASOContract()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            DAmt = 0m,
                            CAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<COASOContract> l3)
        {
            string AcNo = string.Empty;

            foreach (var x in l3)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        // Opening Balance
        private void Process1(ref List<COASOContract> l3, string SelectedCOA, string PeriodTo)
        {
            string Yr = Sm.Left(PeriodTo, 4);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var lJournal = new List<Journal>();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Order By B.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l3.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l3[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l3[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l3[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l3[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l3[j].DAmt += lJournal[i].DAmt;
                            l3[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l3[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        // Journal sampai Period To
        private void Process2(ref List<COASOContract> l3, string SelectedCOA, string PeriodTo)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
                SQL.AppendLine("    And A.Period Is Not Null And Left(A.Period, 6) <= @YrMth ");
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(PeriodTo, 6));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l3.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l3[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l3[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l3[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l3[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l3[j].DAmt += lJournal[i].DAmt;
                            l3[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l3[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process2_JournalMemorial(ref List<COASOContract> l3, string SelectedCOA, string PeriodTo)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
            }
            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(PeriodTo, 6));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l3.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l3[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l3[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l3[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l3[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l3[j].DAmt += lJournal[i].DAmt;
                            l3[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l3[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        // ceplokin data ke list row
        private void Process3(ref List<RowData> l, ref List<COA> l2, ref List<COASOContract> l3)
        {
            #region Old Code
            //foreach (var x in l3)
            //{
            //    foreach (var y in l2.Where(w => x.AcNo.Contains(string.Concat(w.AcNo, "."))))
            //    {
            //        if (l.Count == 0)
            //        {
            //            l.Add(new RowData()
            //            {
            //                Category = y.ParentDesc.ToUpper(),
            //                Description = y.AcDesc,
            //                SelectedAcNo = y.AcNo,
            //                Amt = (y.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt),
            //                SpecialInd = false,
            //                COAHead = GetCOAHead(y.AcNo)
            //            });
            //        }
            //        else
            //        {
            //            bool IsExists = false;
            //            foreach (var z in l.Where(w => w.SelectedAcNo == y.AcNo))
            //            {
            //                z.Amt += (y.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
            //                IsExists = true;
            //            }

            //            if (!IsExists)
            //            {                            
            //                l.Add(new RowData()
            //                {
            //                    Category = y.ParentDesc.ToUpper(),
            //                    Description = y.AcDesc,
            //                    SelectedAcNo = y.AcNo,
            //                    Amt = (y.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt),
            //                    SpecialInd = false,
            //                    COAHead = GetCOAHead(y.AcNo)
            //                });
            //            }
            //        }
            //    }
            //}
            #endregion

            foreach(var x in l2)
            {
                l.Add(new RowData()
                {
                    Category = x.ParentDesc.ToUpper(),
                    Description = x.AcDesc,
                    SelectedAcNo = x.AcNo,
                    AcType = x.AcType,
                    Amt = 0m,
                    SpecialInd = false,
                    COAHead = GetCOAHead(x.AcNo)
                });
            }

            foreach (var x in l)
            {
                foreach (var y in l3.Where(w => w.AcNo.Contains(string.Concat(x.SelectedAcNo, "."))))
                {
                    foreach (var z in l.Where(w => w.SelectedAcNo == x.SelectedAcNo))
                    {
                        z.Amt += (x.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }
                }                
            }
        }

        private string GetCOAHead(string AcNo)
        {
            if (AcNo == "8.000") return "7";
            if (AcNo.Contains("8.1")) return "81";
            if (AcNo == "8.200") return "82";

            return Sm.Left(AcNo, 1);
        }

        // Get Special Row
        private void Process4(ref List<RowData> l, ref List<UpdatedRowData> l4)
        {
            string mCOAHead = string.Empty;
            string mSelectedAcNo = string.Empty;
            bool Is82Exists = false;
            int mSeqNo = 1;

            foreach (var x in l)
            {
                if (mCOAHead.Length == 0) mCOAHead = x.COAHead;

                if (mCOAHead != x.COAHead) mSeqNo += 1;

                if (mCOAHead == "5" && x.COAHead == "6")
                {
                    l4.Add(new UpdatedRowData() 
                    {
                        Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", "LABA BRUTO"),
                        Description = "Laba Bruto",
                        SelectedAcNo = "56",
                        Amt = GetSumAmt("4", "5", ref l, ref l4, false, true, false, false),
                        SpecialInd = true,
                        COAHead = "56"
                    });

                    mSeqNo += 1;
                }

                if (mCOAHead == "6" && x.COAHead == "7")
                {
                    l4.Add(new UpdatedRowData()
                    {
                        Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", "LABA BERSIH USAHA"),
                        Description = "Laba Bersih Usaha",
                        SelectedAcNo = "67",
                        Amt = GetSumAmt("56", "6", ref l, ref l4, false, true, true, false),
                        SpecialInd = true,
                        COAHead = "67"
                    });

                    mSeqNo += 1;
                }

                if (mSelectedAcNo == "8.000" && x.SelectedAcNo.Contains("8.1"))
                {
                    l4.Add(new UpdatedRowData()
                    {
                        Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", "LABA BERSIH SEBELUM PPh BADAN & BUNGA BANK"),
                        Description = "Laba Bersih Sebelum PPh Badan & Bunga Bank",
                        SelectedAcNo = "8081",
                        Amt = GetSumAmt("67", "7", ref l, ref l4, true, true, true, false),
                        SpecialInd = true,
                        COAHead = "8081"
                    });

                    mSeqNo += 1;
                }

                if (mSelectedAcNo.Contains("8.1") && x.SelectedAcNo == "8.200")
                {
                    l4.Add(new UpdatedRowData()
                    {
                        Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", "LABA BERSIH SEBELUM PPh BADAN"),
                        Description = "Laba Bersih Sebelum PPh Badan",
                        SelectedAcNo = "8182",
                        Amt = GetSumAmt("8081", "81", ref l, ref l4, false, true, true, true),
                        SpecialInd = true,
                        COAHead = "8182"
                    });

                    mSeqNo += 1;

                    Is82Exists = true;
                }

                l4.Add(new UpdatedRowData() 
                {
                    Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", x.Category.ToUpper()),
                    Description = x.Description,
                    SelectedAcNo = x.SelectedAcNo,
                    Amt = x.Amt,
                    SpecialInd = false,
                    COAHead = x.COAHead
                });

                mCOAHead = x.COAHead;
                mSelectedAcNo = x.SelectedAcNo;
            }

            if (Is82Exists)
            {
                l4.Add(new UpdatedRowData()
                {
                    Category = string.Concat(Sm.Right(string.Concat("00", mSeqNo.ToString()), 2), ". ", "LABA BERSIH SETELAH PPh BADAN"),
                    Description = "Laba Bersih Setelah PPh Badan",
                    SelectedAcNo = "8283",
                    Amt = GetSumAmt("8182", "82", ref l, ref l4, false, true, true, true),
                    SpecialInd = true,
                    COAHead = "8283"
                });
            }
        }

        private void Process5(ref List<UpdatedRowData> l4, ref List<UpdatedRowData2> l5)
        {
            l5 = l4.GroupBy(g => new { g.Category, g.Description })
                .Select(s => new UpdatedRowData2
                {
                    Category = s.First().Category,
                    Description = s.First().Description,
                    SelectedAcNo = s.First().SelectedAcNo,
                    Amt = s.Sum(a => a.Amt),
                    SpecialInd = s.First().SpecialInd,
                    COAHead = s.First().COAHead
                }).ToList();
        }

        private decimal GetSumAmt(string COAHead1, string COAHead2, ref List<RowData> l, ref List<UpdatedRowData> l4, bool Plus, bool FromTop, bool IsNeedSpecialCOA, bool IsOnlyUseSpecialCOA)
        {
            decimal Amt = 0m, Amt1 = 0m, Amt2 = 0m;

            if (!IsOnlyUseSpecialCOA)
            {
                foreach (var x in l)
                {
                    if (x.COAHead == COAHead1)
                    {
                        Amt1 += x.Amt;
                    }

                    if (x.COAHead == COAHead2)
                    {
                        Amt2 += x.Amt;
                    }
                }
            }

            if (IsNeedSpecialCOA)
            {
                if (l4.Count > 0)
                {
                    foreach (var x in l4)
                    {
                        if (x.COAHead == COAHead1) Amt1 += x.Amt;
                        if (x.COAHead == COAHead2) Amt2 += x.Amt;
                    }
                }
            }

            if (Amt1 != 0m || Amt2 != 0m)
            {
                if (FromTop)
                {
                    if (Plus) Amt = Amt1 + Amt2;
                    else Amt = Amt1 - Amt2;
                }
                else
                {
                    if (Plus) Amt = Amt2 + Amt1;
                    else Amt = Amt2 - Amt1;
                }
            }

            return Amt;
        }

        // ceplokin data ke grid, yg nilai nya gak 0
        private void Process7(ref List<UpdatedRowData2> l5)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            int mSeq = 1;
            string mCategoryName = string.Empty;

            Grd1.BeginUpdate();

            foreach (var x in l5)
            {
                if (mCategoryName.Length == 0) mCategoryName = x.Category;
                if (mCategoryName != x.Category)
                {
                    mSeq += 1;
                    mCategoryName = x.Category;
                }

                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.Category;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.Description;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.SelectedAcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.Amt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.Right(string.Concat("000", mSeq.ToString()), 3);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = x.SpecialInd ? "Y" : "N";
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = x.COAHead;

                Row += 1;
            }

            Grd1.EndUpdate();
        }

        // summary grid
        private void GrdSummary()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4 });
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void BtnDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptProfitLoss6Dlg(this));
        }

        #endregion

        #endregion

        #region Class

        private class PrintOutHdr
        {
            public string ProjectName { get; set; }
            public string PeriodTo { get; set; }
        }

        private class PrintOutDtl
        {
            public string Seq { get; set; }
            public string Category { get; set; }
            public string Description { get; set; }
            public string SelectedAcNo { get; set; }
            public decimal Amt { get; set; }
            public string SpecialInd { get; set; }
        }

        private class RowData
        {
            public string Category { get; set; }
            public string Description { get; set; }
            public string SelectedAcNo { get; set; }
            public string AcType { get; set; }
            public decimal Amt { get; set; }
            public bool SpecialInd { get; set; }
            public string COAHead { get; set; }
        }

        private class UpdatedRowData
        {
            public string Category { get; set; }
            public string Description { get; set; }
            public string SelectedAcNo { get; set; }
            public decimal Amt { get; set; }
            public bool SpecialInd { get; set; }
            public string COAHead { get; set; }
        }

        private class UpdatedRowData2
        {
            public string Category { get; set; }
            public string Description { get; set; }
            public string SelectedAcNo { get; set; }
            public decimal Amt { get; set; }
            public bool SpecialInd { get; set; }
            public string COAHead { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string ParentDesc { get; set; }
        }

        private class COASOContract
        {
            public string AcNo { get; set; }
            public string Parent { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion
    }
}
