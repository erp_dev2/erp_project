﻿#region Update
/* 
    22/09/2019 [TKG] New reporting
 *  25/11/2020 [ICA/PHT] Menambah Kolom Grade Level
 *  10/05/2021 [TRI/PHT] Reporting employee salary for social security (0210711), hanya kolom wanaarta saja yang muncul
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSalSS : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false;
        private List<SSProgram> l;

        #endregion

        #region Constructor

        public FrmRptEmpSalSS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");

                l = new List<SSProgram>();
                SetSSProgram(ref l);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10 + l.Count;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Employee's Code",
                    "Employee's Name",
                    "Old Code",
                    "Position",
                    "Department",
                    
                    //6-9
                    "Division",
                    "Site",
                    "Status",
                    "Grade Level"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    120, 200, 100, 180, 180, 

                    //6-8
                    150, 180, 100, 100
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 8, 9 }, false);
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Cols[10 + i].Text = l[i].SSPName.Trim().Replace(" ", Environment.NewLine);
                Grd1.Cols[10 + i].Width = 180;
                Grd1.Header.Cells[0, 10 + i].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[10 + i].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[10 + i].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[10 + i].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            }
            Grd1.Cols[8].Move(Grd1.Cols.Count-1);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                List<int> colList = new List<int>();

                Process1();
                if (Grd1.Rows.Count > 0)
                {
                    Process2();
                    Process3();
                    for (int c = 10; c < Grd1.Cols.Count; c++)
                    {
                        colList.Add(c);
                    }
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;

                    int[] cols = colList.ToArray();
                    iGSubtotalManager.ShowSubtotals(Grd1, cols);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Grd1.Cols.AutoWidth();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.DivisionName, E.SiteName, F.OptDesc As EmploymentStatusDesc, G.GrdLvlName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblDivision D On A.DivisionCode=D.DivisionCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblOption F On A.EmploymentStatus=F.OptCode And F.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G ON A.GrdLvlCode = G.GrdLvlCode ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (A.SiteCode Is Null Or ");
                SQL.AppendLine("    (A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And (A.DeptCode Is Null Or ");
                SQL.AppendLine("    (A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }

            switch (Sm.GetLue(LueStatus))
            {
                case "1":
                    Filter = " And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ";
                    break;
                case "2":
                    Filter = " And A.ResignDt Is Not Null And A.ResignDt<=@CurrentDate ";
                    break;
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
            
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + Filter + " Order By A.EmpName;",
                new string[] { 
                    "EmpCode", 
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "DivisionName", 
                    "SiteName", "EmploymentStatusDesc", "GrdLvlName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, true, false, false, false
            );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
        }

        private void Process2()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                for (int c = 10; c < Grd1.Cols.Count; c++)
                    Grd1.Cells[r, c].Value = 0m;
            }
        }

        private void Process3()
        {
            string
                Filter = string.Empty,
                EmpCode = string.Empty,
                SSPCode = string.Empty,
                EmpCodeTemp = string.Empty,
                SSPCodeTemp = string.Empty;
            decimal Amt = 0m;
            int r = 0;
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + Row.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText =
                        "Select EmpCode, SSPCode, Amt " +
                        "From TblEmployeeSalarySS " +
                        "Where StartDt<=@CurrentDate And @CurrentDate<=EndDt " +
                        Filter + " Order By EmpCode;";
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "SSPCode", "Amt" });
                    if (dr.HasRows)
                    {
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            EmpCode = dr.GetString(c[0]);
                            SSPCode = dr.GetString(c[1]);
                            Amt = dr.GetDecimal(c[2]);

                            if (Sm.CompareStr(EmpCode, EmpCodeTemp))
                            {
                                for (int x = 0; x < l.Count; x++)
                                {
                                    if (Sm.CompareStr(SSPCode, l[x].SSPCode))
                                    {
                                        Grd1.Cells[r, l[x].Col].Value = Sm.GetGrdDec(Grd1, r, l[x].Col) + Amt;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < Grd1.Rows.Count; i++)
                                {
                                    if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, i, 1)))
                                    {
                                        for (int x = 0; x < l.Count; x++)
                                        {
                                            if (Sm.CompareStr(SSPCode, l[x].SSPCode))
                                            {
                                                Grd1.Cells[i, l[x].Col].Value = Sm.GetGrdDec(Grd1, i, l[x].Col) + Amt;
                                                break;
                                            }
                                        }
                                        r = i;
                                        break;
                                    }
                                }
                                EmpCodeTemp = EmpCode;
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                }
        }

        private void SetSSProgram(ref List<SSProgram> l)
        {
            var GrdCol = 9;
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select SSPCode, SSPName From TblSSProgram Order By SSPName;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SSPCode", "SSPName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new SSProgram()
                        {
                            SSPCode = Sm.DrStr(dr, c[0]),
                            SSPName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class SSProgram
        {
            public string SSPCode { get; set; }
            public string SSPName { get; set; }
            public int Col { get; set; }
        }

        #endregion

    }
}
