﻿namespace RunSystem
{
    partial class FrmReorderPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtROPValue = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtMinStockValue = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblItCtCode = new System.Windows.Forms.Label();
            this.ChkItCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueOptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtKoefisien = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueFormulaCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.LueItScCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.ChkItScCode = new DevExpress.XtraEditors.CheckEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LueItGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkItGrpCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtROPValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStockValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKoefisien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFormulaCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItScCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItGrpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnRefresh);
            this.panel1.Location = new System.Drawing.Point(733, 0);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.BtnRefresh, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkItGrpCode);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.LueItGrpCode);
            this.panel2.Controls.Add(this.LueItScCode);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.ChkItScCode);
            this.panel2.Controls.Add(this.LueFormulaCode);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueItCtCode);
            this.panel2.Controls.Add(this.LblItCtCode);
            this.panel2.Controls.Add(this.ChkItCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.LueOptCode);
            this.panel2.Controls.Add(this.TxtKoefisien);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.ChkItCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Size = new System.Drawing.Size(733, 266);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 266);
            this.panel3.Size = new System.Drawing.Size(733, 207);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 431);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(733, 207);
            this.Grd1.TabIndex = 40;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(126, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(259, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(50, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 10;
            this.label7.Text = "Document#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtROPValue);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.TxtMinStockValue);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(416, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(317, 266);
            this.panel4.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(4, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 14);
            this.label5.TabIndex = 38;
            this.label5.Text = "Re-Order Point Value";
            // 
            // TxtROPValue
            // 
            this.TxtROPValue.EnterMoveNextControl = true;
            this.TxtROPValue.Location = new System.Drawing.Point(128, 220);
            this.TxtROPValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtROPValue.Name = "TxtROPValue";
            this.TxtROPValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtROPValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtROPValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtROPValue.Properties.Appearance.Options.UseFont = true;
            this.TxtROPValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtROPValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtROPValue.Size = new System.Drawing.Size(181, 20);
            this.TxtROPValue.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 14);
            this.label4.TabIndex = 36;
            this.label4.Text = "Minimum Stock value";
            // 
            // TxtMinStockValue
            // 
            this.TxtMinStockValue.EnterMoveNextControl = true;
            this.TxtMinStockValue.Location = new System.Drawing.Point(128, 199);
            this.TxtMinStockValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinStockValue.Name = "TxtMinStockValue";
            this.TxtMinStockValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinStockValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinStockValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinStockValue.Properties.Appearance.Options.UseFont = true;
            this.TxtMinStockValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinStockValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinStockValue.Size = new System.Drawing.Size(181, 20);
            this.TxtMinStockValue.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(21, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Document\'s Date";
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(126, 175);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 12;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 500;
            this.LueItCtCode.Size = new System.Drawing.Size(259, 20);
            this.LueItCtCode.TabIndex = 26;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // LblItCtCode
            // 
            this.LblItCtCode.AutoSize = true;
            this.LblItCtCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItCtCode.ForeColor = System.Drawing.Color.Black;
            this.LblItCtCode.Location = new System.Drawing.Point(25, 178);
            this.LblItCtCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItCtCode.Name = "LblItCtCode";
            this.LblItCtCode.Size = new System.Drawing.Size(94, 14);
            this.LblItCtCode.TabIndex = 25;
            this.LblItCtCode.Text = "Item\'s Category";
            this.LblItCtCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkItCtCode
            // 
            this.ChkItCtCode.Location = new System.Drawing.Point(393, 175);
            this.ChkItCtCode.Name = "ChkItCtCode";
            this.ChkItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCtCode.Properties.Caption = " ";
            this.ChkItCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCtCode.TabIndex = 27;
            this.ChkItCtCode.ToolTip = "Remove filter";
            this.ChkItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCtCode.ToolTipTitle = "Run System";
            this.ChkItCtCode.CheckedChanged += new System.EventHandler(this.ChkItCtCode_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(58, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "Coefficient";
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(126, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(125, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // LueOptCode
            // 
            this.LueOptCode.EnterMoveNextControl = true;
            this.LueOptCode.Location = new System.Drawing.Point(126, 47);
            this.LueOptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueOptCode.Name = "LueOptCode";
            this.LueOptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.Appearance.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOptCode.Properties.DropDownRows = 12;
            this.LueOptCode.Properties.NullText = "[Empty]";
            this.LueOptCode.Properties.PopupWidth = 500;
            this.LueOptCode.Size = new System.Drawing.Size(125, 20);
            this.LueOptCode.TabIndex = 15;
            this.LueOptCode.ToolTip = "F4 : Show/hide list";
            this.LueOptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOptCode.EditValueChanged += new System.EventHandler(this.LueOptCode_EditValueChanged);
            // 
            // TxtKoefisien
            // 
            this.TxtKoefisien.EnterMoveNextControl = true;
            this.TxtKoefisien.Location = new System.Drawing.Point(126, 68);
            this.TxtKoefisien.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKoefisien.Name = "TxtKoefisien";
            this.TxtKoefisien.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKoefisien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKoefisien.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKoefisien.Properties.Appearance.Options.UseFont = true;
            this.TxtKoefisien.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKoefisien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKoefisien.Size = new System.Drawing.Size(125, 20);
            this.TxtKoefisien.TabIndex = 17;
            this.TxtKoefisien.Validated += new System.EventHandler(this.TxtKoefisien_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(86, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Item";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(126, 154);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(259, 20);
            this.TxtItCode.TabIndex = 23;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(393, 154);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCode.TabIndex = 24;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(81, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "Month";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(72, 243);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 34;
            this.label6.Text = "Remark";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(126, 240);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(259, 20);
            this.MeeRemark.TabIndex = 35;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(127, 110);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 12;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(259, 20);
            this.LueWhsCode.TabIndex = 19;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(55, 113);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 14);
            this.label9.TabIndex = 18;
            this.label9.Text = "Warehouse";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFormulaCode
            // 
            this.LueFormulaCode.EnterMoveNextControl = true;
            this.LueFormulaCode.Location = new System.Drawing.Point(126, 89);
            this.LueFormulaCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFormulaCode.Name = "LueFormulaCode";
            this.LueFormulaCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFormulaCode.Properties.Appearance.Options.UseFont = true;
            this.LueFormulaCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFormulaCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFormulaCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFormulaCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFormulaCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFormulaCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFormulaCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFormulaCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFormulaCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFormulaCode.Properties.DropDownRows = 12;
            this.LueFormulaCode.Properties.NullText = "[Empty]";
            this.LueFormulaCode.Properties.PopupWidth = 500;
            this.LueFormulaCode.Size = new System.Drawing.Size(259, 20);
            this.LueFormulaCode.TabIndex = 21;
            this.LueFormulaCode.ToolTip = "F4 : Show/hide list";
            this.LueFormulaCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFormulaCode.EditValueChanged += new System.EventHandler(this.LueFormulaCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(74, 92);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 20;
            this.label10.Text = "Formula";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefresh.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefresh.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRefresh.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnRefresh.Location = new System.Drawing.Point(0, 217);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(70, 31);
            this.BtnRefresh.TabIndex = 11;
            this.BtnRefresh.Text = "  &Refresh";
            this.BtnRefresh.ToolTip = "Refresh Data (Alt-R)";
            this.BtnRefresh.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefresh.ToolTipTitle = "Run System";
            this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // LueItScCode
            // 
            this.LueItScCode.EnterMoveNextControl = true;
            this.LueItScCode.Location = new System.Drawing.Point(126, 196);
            this.LueItScCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItScCode.Name = "LueItScCode";
            this.LueItScCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.Appearance.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItScCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItScCode.Properties.DropDownRows = 12;
            this.LueItScCode.Properties.NullText = "[Empty]";
            this.LueItScCode.Properties.PopupWidth = 500;
            this.LueItScCode.Size = new System.Drawing.Size(259, 20);
            this.LueItScCode.TabIndex = 29;
            this.LueItScCode.ToolTip = "F4 : Show/hide list";
            this.LueItScCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItScCode.EditValueChanged += new System.EventHandler(this.LueItScCode_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(4, 199);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 14);
            this.label11.TabIndex = 28;
            this.label11.Text = "Item\'s SubCategory";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkItScCode
            // 
            this.ChkItScCode.Location = new System.Drawing.Point(393, 196);
            this.ChkItScCode.Name = "ChkItScCode";
            this.ChkItScCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItScCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItScCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItScCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItScCode.Properties.Caption = " ";
            this.ChkItScCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItScCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItScCode.TabIndex = 30;
            this.ChkItScCode.ToolTip = "Remove filter";
            this.ChkItScCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItScCode.ToolTipTitle = "Run System";
            this.ChkItScCode.CheckedChanged += new System.EventHandler(this.ChkItScCode_CheckedChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(41, 221);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(78, 14);
            this.label49.TabIndex = 31;
            this.label49.Text = "Item\'s Group";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItGrpCode
            // 
            this.LueItGrpCode.EnterMoveNextControl = true;
            this.LueItGrpCode.Location = new System.Drawing.Point(126, 218);
            this.LueItGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItGrpCode.Name = "LueItGrpCode";
            this.LueItGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItGrpCode.Properties.DropDownRows = 20;
            this.LueItGrpCode.Properties.NullText = "[Empty]";
            this.LueItGrpCode.Properties.PopupWidth = 500;
            this.LueItGrpCode.Size = new System.Drawing.Size(259, 20);
            this.LueItGrpCode.TabIndex = 32;
            this.LueItGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueItGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItGrpCode.EditValueChanged += new System.EventHandler(this.LueItGrpCode_EditValueChanged);
            // 
            // ChkItGrpCode
            // 
            this.ChkItGrpCode.Location = new System.Drawing.Point(393, 219);
            this.ChkItGrpCode.Name = "ChkItGrpCode";
            this.ChkItGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItGrpCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItGrpCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItGrpCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItGrpCode.Properties.Caption = " ";
            this.ChkItGrpCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItGrpCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItGrpCode.TabIndex = 33;
            this.ChkItGrpCode.ToolTip = "Remove filter";
            this.ChkItGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItGrpCode.ToolTipTitle = "Run System";
            this.ChkItGrpCode.CheckedChanged += new System.EventHandler(this.ChkItGrpCode_CheckedChanged);
            // 
            // FrmReorderPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 473);
            this.Name = "FrmReorderPoint";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtROPValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStockValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKoefisien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFormulaCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItScCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItGrpCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label7;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtROPValue;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtMinStockValue;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        private System.Windows.Forms.Label LblItCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCtCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.LookUpEdit LueOptCode;
        internal DevExpress.XtraEditors.TextEdit TxtKoefisien;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueFormulaCode;
        private System.Windows.Forms.Label label10;
        protected DevExpress.XtraEditors.SimpleButton BtnRefresh;
        private DevExpress.XtraEditors.LookUpEdit LueItScCode;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.CheckEdit ChkItScCode;
        private DevExpress.XtraEditors.CheckEdit ChkItGrpCode;
        private System.Windows.Forms.Label label49;
        private DevExpress.XtraEditors.LookUpEdit LueItGrpCode;
    }
}