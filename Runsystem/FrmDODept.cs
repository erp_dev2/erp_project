﻿#region Update
/*
    04/04/2017 [WED] Setelah InsertData(), diarahkan ke ShowData(), bukan BtnInsertClick()
    04/04/2017 [WED] Tambah textbox DO Journal# di Header
    20/06/2017 [TKG] Update remark di journal
    15/07/2017 [TKG] tambah journal antar entity
    31/07/2017 [HAR] Cancel document
    02/08/2017 [TKG] ubah journal
    09/08/2017 [WED] validasi tanggal save DODept > tanggal DORequest, berdasarkan parameter IsDODateCompared
    10/08/2017 [WED] item yang muncul dari DO Request yang status approval nya = A
    19/09/2017 [TKG] tambah cancel reason
    22/12/2017 [TKG] Filter department berdasarkan group
    08/01/2018 [HAR] bug fixing saat save costcenter
    23/05/2018 [TKG] tambah local document#
    17/07/2018 [TKG] tambah cost center saat journal
    01/08/2018 [TKG] menggunakan parameter IsAutoJournalActived untuk validasi apakah cost category boleh kosong atau tidak.
    02/08/2018 [TKG] Berdasarkan parameter IsDODeptJournalCancelUseTheSameCCtAcNo, apakah saat journal cancel do menggunakan nomor rekening coa yg lama atau yg terakhir ada di tem cost cettagory. 
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    20/05/2019 [TKG] parameter IsSystemUseCostCenter diganti dengan IsDODeptUseCostCenter
    16/12/2019 [TKG/IMS] journal untuk moving average
    10/08/2021 [RDA/ALL] validasi coa saat auto journal aktif
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDODept : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mCCCode = string.Empty;
        private string mDeptCode = string.Empty;
        internal FrmDODeptFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool 
            mIsShowForeignName = false, 
            mIsItGrpCodeShow = false, 
            mIsDODeptFilterByAuthorization = false,
            mIsDODeptUseCostCenter = false;
        private bool 
            mIsAutoJournalActived = false, 
            mIsEntityMandatory = false,
            mIsDODateCompared = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDODeptJournalCancelUseTheSameCCtAcNo = false, 
            mIsMovingAvgEnabled = false,
            mIsCheckCOAJournalNotExists = false;
        
        #endregion

        #region Constructor

        public FrmDODept(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Department (with DO Request)";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsDODeptUseCostCenter = Sm.GetParameterBoo("IsDODeptUseCostCenter");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsDODeptFilterByAuthorization = Sm.GetParameterBoo("IsDODeptFilterByAuthorization");
            mIsDODateCompared = Sm.GetParameterBoo("IsDODateCompared");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDODeptJournalCancelUseTheSameCCtAcNo = Sm.GetParameterBoo("IsDODeptJournalCancelUseTheSameCCtAcNo");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo", 
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code", 
                        "", 
                        
                        //6-10
                        "Item's Name", 
                        "Replacement",
                        "Batch#", 
                        "Source",
                        "Lot", 
                        
                        //11-15
                        "Bin", 
                        "Stock",
                        "Quantity",
                        "UoM", 
                        "Stock",
                        
                        //16-20
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",

                        //21-25
                        "Asset Code",
                        "Asset",
                        "",
                        "Cost Category",
                        "Inventory's"+Environment.NewLine+"COA Account#",
                        
                        //26-30
                        "Remark",
                        "Group",
                        "Display Name",
                        "Foreign Name",
                        "Cancel Reason",

                        //31
                        "Cost Category's"+Environment.NewLine+"COA Account#",
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        50, 0, 20, 120, 20, 
                        
                        //6-10
                        170, 90, 200, 170, 80,  
                        
                        //11-15
                        80, 100, 100, 100, 100,  
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-25
                        100, 150, 20, 150, 150, 

                        //26-30
                        200, 100, 300, 150, 300,

                        //31
                        0
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 15, 16, 18, 19 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 7 });
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 23 });
            Grd1.Cols[28].Move(23);
            Grd1.Cols[29].Move(7);
            Grd1.Cols[30].Move(3);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 9, 10, 11, 15, 16, 17, 18, 19, 20, 23, 27, 30, 31 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[27].Move(6);
            }

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 19;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {                        
                        //0
                        "DNo",

                        //1-5
                        "",
                        "DO Request From Dept DNo",
                        "Item Code", 
                        "Item Name",
                        "Requested"+Environment.NewLine+"Quantity", 
                        
                        //6-10
                        "DO"+Environment.NewLine+"Quantity",
                        "Balance",
                        "Requested"+Environment.NewLine+"Quantity (2)", 
                        "DO"+Environment.NewLine+"Quantity (2)",
                        "Balance (2)",

                        //11-15
                        "Requested"+Environment.NewLine+"Quantity (3)",
                        "DO"+Environment.NewLine+"Quantity (3)",
                        "Balance (3)",
                        "Asset Code",
                        "Asset Name",

                        //16-18
                        "Group",
                        "Display Name",
                        "Foreign Name"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 0, 100, 150, 100, 
                        
                        //6-10
                        100, 100, 100, 100, 100,

                        //11-15
                        100, 100, 100, 100, 200,

                        //16-18
                        100, 300, 150
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Grd2.Cols[17].Move(16);
            Grd2.Cols[18].Move(5);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 8, 9, 10, 11, 12, 13, 16 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd2, new int[] { 18 }, false);
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);

            if (mIsItGrpCodeShow)
            {
                Grd2.Cols[16].Visible = true;
                Grd2.Cols[16].Move(4);
            }

            #endregion

            Sm.SetGrdProperty(Grd1, false);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            if (BtnSave.Enabled)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 9, 10, 11, 30 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10, 11, 12, 13 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, TxtLocalDocNo, LueWhsCode, MeeRemark, TxtJournalDocNo }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
                    Sm.GrdColInvisible(Grd1, new int[] { 30 }, !ChkHideInfoInGrd.Checked);
                    BtnDORequestDeptDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, TxtLocalDocNo, LueWhsCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 7, 13, 16, 19, 26 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColInvisible(Grd1, new int[] { 30 }, false);
                    BtnDORequestDeptDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 30 });
                    Sm.GrdColInvisible(Grd1, new int[] { 30 }, true);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCCCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, TxtDORequestDeptDocNo, 
                TxtDORequestDeptDocNo, TxtWODocNo, TxtDeptCode, TxtEntName, TxtCCName, 
                MeeRemark2, MeeRemark
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 13, 15, 16, 18, 19 });
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd2, 0, 2);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtDORequestDeptDocNo.Text.Length != 0)
            {
                if (e.ColIndex == 3)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDODeptDlg2(this, Sm.GetLue(LueWhsCode), Sm.GetValue("Select CCCode From TblDORequestDeptHdr Where DocNo = '" + TxtDORequestDeptDocNo.Text + "'")));
                }

                if (Sm.IsGrdColSelected(new int[] { 3, 13, 16, 19, 21 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                }
            }

            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeBalance();
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtDORequestDeptDocNo.Text.Length != 0)
                Sm.FormShowDialog(new FrmDODeptDlg2(this, Sm.GetLue(LueWhsCode), Sm.GetValue("Select CCCode From TblDORequestDeptHdr Where DocNo = '" + TxtDORequestDeptDocNo.Text + "'")));

            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                //ini ke asset atau cost category atau item cost category ?
                //code nya dari asset code
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 13, 16, 19 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 21, 30 }, e);

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 13, 16, 19, 14, 17, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 13, 19, 16, 14, 20, 17);
            }

            if (e.ColIndex == 16)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 16, 13, 19, 17, 14, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 16, 19, 13, 17, 20, 14);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 19, 13, 16, 20, 14, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 19, 16, 13, 20, 17, 14);
            }

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 16);

            if (Sm.IsGrdColSelected(new int[] { 1, 13, 16, 19 }, e.ColIndex)) ComputeBalance();

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDODeptFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetParameter("NumberOfInventoryUomCode");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "DoDeptHdr", "DoDeptHdr1", "DoDeptHdr2", "DoDeptDtl" };

            var l = new List<DoDeptHdr>();
            var l1 = new List<DoDeptHdr1>();
            var l2 = new List<DoDeptHdr2>();
            var ldtl = new List<DoDeptDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Hdr
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, C.DeptName, A.DORequestDeptDocNo, Date_Format(D.DocDt,'%d %M %Y') As DORequestDeptDocDt,D.Remark, E.CCName, ");
            SQL.AppendLine("F.UserName As DoReqCreateBy, F1.UserName As DoToDeptCreateby, ");
            SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict, ");
            SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F1.UserCode, '.JPG') As EmpPict2 ");
            SQL.AppendLine("From TblDoDeptHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblDORequestDeptHdr D On A.DORequestDeptDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblCostCenter E On D.CCCode=E.CCCode ");
            SQL.AppendLine("Inner Join tbluser F On D.CreateBy=F.UserCode ");
            SQL.AppendLine("Inner Join tbluser F1 On A.CreateBy=F1.UserCode ");
            SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         //6-10
                         "DeptName",
                         "DORequestDeptDocNo",
                         "DORequestDeptDocDt",
                         "Remark",
                         "CompanyLogo",
                         //11-13
                         "CCName",
                         "DoReqCreateBy",
                         "DoToDeptCreateby",
                         "EmpPict",
                         "EmpPict2"
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DoDeptHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),
                            DeptName = Sm.DrStr(dr, c[6]),
                            DORequestDeptDocNo = Sm.DrStr(dr, c[7]),
                            DORequestDeptdDocDt = Sm.DrStr(dr, c[8]),
                            Remark = Sm.DrStr(dr, c[9]),
                            CompanyLogo = Sm.DrStr(dr, c[10]),
                            CCName = Sm.DrStr(dr, c[11]),
                            DoReqCreateBy = Sm.DrStr(dr, c[12]),
                            DoToDeptCreateby = Sm.DrStr(dr, c[13]),
                            EmpPict = Sm.DrStr(dr, c[14]),
                            EmpPict2 = Sm.DrStr(dr, c[15]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Hdr1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Where DocType = 'DORequestDept' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDORequestDeptDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new DoDeptHdr1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Hdr2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Where DocType = 'DORequestDept' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDORequestDeptDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"

                        
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new DoDeptHdr2()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Dtl
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQLDtl.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, ");
                SQLDtl.AppendLine("A.Remark, B.ItGrpCode ");
                SQLDtl.AppendLine("From TblDoDeptDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                             //0
                             "ItCode",
                             
                             //1-5
                             "ItName",
                             "BatchNo",
                             "Source",
                             "Lot",
                             "Bin",
                             
                             //6-10
                             "Qty",
                             "Qty2",
                             "Qty3",
                             "InventoryUOMCode",
                             "InventoryUOMCode2",

                             //11-13
                             "InventoryUOMCode3",
                             "Remark",
                             "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new DoDeptDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty1 = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            Remark = Sm.DrStr(drDtl, cDtl[12]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[13]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            int a = int.Parse(ParValue);

            if (a == 1)
            {
                Sm.PrintReport("DODept1", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("DODept2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("DODept3", myLists, TableName, false);
            }

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept", "TblDODeptHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDODeptHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 1) cml.Add(SaveDODeptDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 1) cml.Add(SaveDODeptDtl2(DocNo, Row));

            cml.Add(SaveStock(DocNo));
            cml.Add(UpdateDORequest(DocNo));

            if (mIsAutoJournalActived)
            {
                if (mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal(DocNo));
                else
                    cml.Add(SaveJournal2(DocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));  
            var EntCode2 = string.Empty;

            if (mCCCode.Length>0) EntCode2 = Sm.GetEntityCostCenter(mCCCode);

            return (EntCode1 != EntCode2);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsDODateCompared && IsDODateCompared()) ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtDORequestDeptDocNo, "DO Request from department document#", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsWOAlreadyCancelled() ||
                IsWOAlreadySettled() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                //IsDORequestProcessedToDODeptAlready() ||
                IsDORequestDeptNotValid() ||
                IsBalanceNotValid() ||
                IsItemCostCategoryInvalid() ||
                IsJournalSettingInvalid()
                ;
        }

        private bool IsJournalSettingInvalid() 
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;
            string mAcNoForCOGS = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForCOGS'");
            string mAcNoForSaleOfFinishedGoods = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForSaleOfFinishedGoods'");

            if (mIsEntityMandatory && IsEntityDifferent())
            {
                //Parameter
                if (mAcNoForCOGS.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }

                if (mAcNoForSaleOfFinishedGoods.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                    return true;
                }
                
                //Table
                if (Sm.GetValue("Select AcNo1 From TblEntity Where EntCode = @Param", Sm.GetEntityCostCenter(TxtCCName.Text)).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Cost Center's COA account# (" + TxtCCName.Text + ") is empty.");
                    return true;
                }

                if (Sm.GetValue("Select AcNo2 From TblEntity Where EntCode = @Param", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode))).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Warehouse's COA account# (" + LueWhsCode.Text + ") is empty.");
                    return true;
                }
            }

            //Table
            if (mIsMovingAvgEnabled)
            {
                if (IsJournalSettingInvalid_ItemCategoryWithAvgInd(Msg, "Y")) return true;
                if (IsJournalSettingInvalid_ItemCategoryWithAvgInd(Msg, "N")) return true;
            }
            else
            {
                if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;
            }

            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
            

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategoryWithAvgInd(string Msg, string MovingAvgInd)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And B.MovingAvgInd=’" + MovingAvgInd + "’ ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, CCtName = string.Empty;

            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode And B.AcNo is Null ");
            SQL.AppendLine("Where A.CCCode=@CCCode "); 
            SQL.AppendLine("    And A.ItCode In ( ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            
            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsItemCostCategoryInvalid()
        {
            if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
            {
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                string ItCode = string.Empty, Filter = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        ItCode = Sm.GetGrdStr(Grd1, r, 4);
                        if (ItCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " Where (" + Filter + ") ";
                else
                    Filter = " Where 1=0 ";

                SQL.AppendLine("Select ItName From TblItem ");
                SQL.AppendLine(Filter.Replace("A.",""));
                SQL.AppendLine("And ItCode Not In (");
                SQL.AppendLine("    Select A.ItCode ");
                SQL.AppendLine("    From TblItemCostCategory A, TblCostCategory B ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    And A.CCtCode=B.CCtCode ");
                SQL.AppendLine("    And A.CCCode=B.CCCode ");
                SQL.AppendLine("    And A.CCCode=@CCCode ");
                SQL.AppendLine("    And B.AcNo Is Not Null ");
                SQL.AppendLine("    ) Limit 1; ");

                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);

                cm.CommandText = SQL.ToString();

                ItCode = Sm.GetValue(cm);
                if (ItCode.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item : " + ItCode + Environment.NewLine +
                        "Item's cost category coa account# is empty.");
                    return true;
                }    
            }
            return false;
        }

        private bool IsDODateCompared()
        {
            decimal mDORequestDocDt = Decimal.Parse(Sm.GetValue("Select DocDt From TblDORequestDeptHdr Where DocNo = '" + TxtDORequestDeptDocNo.Text + "';"));
            decimal mDocDt = Decimal.Parse(Sm.Left(Sm.GetDte(DteDocDt), 8));
            string ReqType = TxtWODocNo.Text.Length > 0 ? "Request Part List" : "DO Request";

            if(mDocDt < mDORequestDocDt)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    ReqType + " Date : " + Sm.Right(mDORequestDocDt.ToString(), 2) + "/" + 
                    mDORequestDocDt.ToString().Substring(4, 2) + "/" +
                    Sm.Left(mDORequestDocDt.ToString(), 4) + 
                    Environment.NewLine +
                    "DO To Department Date : " + Sm.Right(mDocDt.ToString(), 2) + "/" +
                    mDocDt.ToString().Substring(4, 2) + "/" +
                    Sm.Left(mDocDt.ToString(), 4) + 
                    Environment.NewLine + Environment.NewLine +
                    "DO To Department's date should be the same or bigger than " + ReqType + "'s date.");
                DteDocDt.Focus();
                return true;
            }

            return false;
        }

        private bool IsWOAlreadyCancelled()
        {
            if (TxtWODocNo.Text.Length == 0) return false;
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already cancelled."
                );
        }

        private bool IsWOAlreadySettled()
        {
            if (TxtWODocNo.Text.Length == 0) return false;
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where SettleInd='Y' And DocNo=@Param;",
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already settled."
                );
        }

        private void ReComputeStock()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3 From TblStockSummary " +
                        "Where WhsCode=@WhsCode " +
                        "And Locate(Concat('##', ItCode, BatchNo, Source, Lot, Bin, '##'), @SelectedItem)>1 " +
                        "Order By ItCode, BatchNo, Source, Lot, Bin;"
                };
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@SelectedItem", GetSelectedItem());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "BatchNo", "Source", "Lot", "bin", "Qty", 
                        "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 8), Sm.DrStr(dr, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Sm.DrStr(dr, 2)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.DrStr(dr, 3)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 11), Sm.DrStr(dr, 4)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 6);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 7);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (mIsDODeptUseCostCenter)
            {
                SetCCtName();
                SetAcNo();
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, "Quantity (1) is empty.") ||
                    (Sm.GetGrdStr(Grd1, Row, 17).Length > 0 && mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 16, true, "Quantity (2) is empty.")) ||
                    (Sm.GetGrdStr(Grd1, Row, 20).Length > 0 && mNumberOfInventoryUomCode == 3 && Sm.IsGrdValueEmpty(Grd1, Row, 19, true, "Quantity (3) is empty."))
                    ) return true;

                if (
                   ((mIsAutoJournalActived && Sm.IsGrdValueEmpty(Grd1, Row, 24, false,
                    "Cost category is empty." + Environment.NewLine +
                    "Please contact Finance/Accounting Department !"
                    ))) ||
                    ((mIsAutoJournalActived && Sm.IsGrdValueEmpty(Grd1, Row, 25, false,
                        "COA's account# is empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !"
                        )))
                   ) return true;


                if (((Sm.GetGrdStr(Grd1, Row, 13).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 13).Length != 0 && Sm.GetGrdDec(Grd1, Row, 13) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 16).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 16).Length != 0 && Sm.GetGrdDec(Grd1, Row, 16) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 19).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 19).Length != 0 && Sm.GetGrdDec(Grd1, Row, 19) == 0m)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.");
                    return true;
                }

                if (IsDOQtyBiggerThanStock(Row)) return true;
            }
            return false;
        }

        private void SetCCtName()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string 
                Filter = string.Empty, 
                ItCode = string.Empty, 
                CCtName = string.Empty,
                AcNo = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 4);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.ItCode, B.CCtName, B.AcNo ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCCode=B.CCCode And A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCtName", "AcNo" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        CCtName = Sm.DrStr(dr, 1);
                        AcNo = Sm.DrStr(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 4), ItCode))
                            {
                                Grd1.Cells[r, 24].Value = CCtName;
                                Grd1.Cells[r, 31].Value = AcNo;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetAcNo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ItCode = string.Empty, AcNo = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 4);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.ItCode, B.AcNo ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "AcNo" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        AcNo = Sm.DrStr(dr, 1);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 4), Sm.DrStr(dr, 0)))
                                Grd1.Cells[r, 25].Value = AcNo;
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsDOQtyBiggerThanStock(int Row)
        {
            decimal
                Stock1 = Sm.GetGrdDec(Grd1, Row, 12),
                DO1 = Sm.GetGrdDec(Grd1, Row, 13),
                Stock2 = Sm.GetGrdDec(Grd1, Row, 15),
                DO2 = Sm.GetGrdDec(Grd1, Row, 16),
                Stock3 = Sm.GetGrdDec(Grd1, Row, 18),
                DO3 = Sm.GetGrdDec(Grd1, Row, 19);

            if (DO1 > Stock1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 1 (" + Sm.FormatNum(DO1, 0) + ") is greater than stock (" + Sm.FormatNum(Stock1, 0) + ")."
                    );
                return true;
            }

            if (DO2 > Stock2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 2 (" + Sm.FormatNum(DO2, 0) + ") is greater than stock (" + Sm.FormatNum(Stock2, 0) + ")."
                    );
                return true;
            }

            if (DO3 > Stock3)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 3 (" + Sm.FormatNum(DO3, 0) + ") is greater than stock (" + Sm.FormatNum(Stock3, 0) + ")."
                    );
                return true;
            }

            return false;
        }

        private bool IsDORequestProcessedToDODeptAlready()
        {
            var SQL = new StringBuilder();

            string
                DORequestDeptDNo = string.Empty,
                SelectedDORequestDept = string.Empty,
                DocNo = TxtDORequestDeptDocNo.Text,
                DNo = string.Empty;

            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    DNo = Sm.GetGrdStr(Grd2, Row, 2);
                    if (DNo.Length > 0) SelectedDORequestDept += "##" + DocNo + DNo + "##";
                }
            }

            SQL.AppendLine("Select B.DORequestDeptDNo ");
            SQL.AppendLine("From TblDODeptHdr A, TblDODeptDtl2 B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And Position(Concat('##', A.DORequestDeptDocNo, B.DORequestDeptDNo, '##') In @SelectedDORequestDept)>0 ");
            SQL.AppendLine("Order By B.DORequestDeptDNo ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SelectedDORequestDept",
                (SelectedDORequestDept.Length == 0 ?
                    "##XXX##" :
                    SelectedDORequestDept
                ));

            DORequestDeptDNo = Sm.GetValue(cm);
            if (DORequestDeptDNo.Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(DORequestDeptDNo, Sm.GetGrdStr(Grd2, Row, 2)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                                    "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                                    "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                                    "This requested item already processed."
                                    );
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDORequestDeptNotValid()
        {
            string ItCode1 = string.Empty, ItCode2 = string.Empty;
            bool IsValid = false;

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                ItCode1 = Sm.GetGrdStr(Grd2, Row, 3);
                if (ItCode1.Length > 1)
                {
                    IsValid = false;
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        ItCode2 = Sm.GetGrdStr(Grd1, Row2, 4);
                        if (ItCode2.Length > 1 && Sm.CompareStr(ItCode1, ItCode2))
                        {
                            IsValid = true;
                            break;
                        }
                    }
                    if (!IsValid)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                                "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "You haven't processed this requested item in the DO item list."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsBalanceNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 1)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 7) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                            "Requested Quantity : " + Sm.GetGrdDec(Grd2, Row, 5) + Environment.NewLine +
                            "DO Quantity : " + Sm.GetGrdDec(Grd2, Row, 6) + Environment.NewLine +
                            "Balance : " + Sm.GetGrdDec(Grd2, Row, 7) + Environment.NewLine + Environment.NewLine +
                            "Balance is less than 0.00."
                            );
                        return true;
                    }

                    if (Grd2.Cols[10].Visible && Sm.GetGrdDec(Grd2, Row, 10) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                            "Requested Quantity : " + Sm.GetGrdDec(Grd2, Row, 8) + Environment.NewLine +
                            "DO Quantity : " + Sm.GetGrdDec(Grd2, Row, 9) + Environment.NewLine +
                            "Balance : " + Sm.GetGrdDec(Grd2, Row, 10) + Environment.NewLine + Environment.NewLine +
                            "Balance is less than 0.00."
                            );
                        return true;
                    }

                    if (Grd2.Cols[13].Visible && Sm.GetGrdDec(Grd2, Row, 13) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                            "Requested Quantity : " + Sm.GetGrdDec(Grd2, Row, 11) + Environment.NewLine +
                            "DO Quantity : " + Sm.GetGrdDec(Grd2, Row, 12) + Environment.NewLine +
                            "Balance : " + Sm.GetGrdDec(Grd2, Row, 13) + Environment.NewLine + Environment.NewLine +
                            "Balance is less than 0.00."
                            );
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveDODeptHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, CCCode, DORequestDeptDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, (Select DeptCode From TblDORequestDeptHdr Where DocNo=@DORequestDeptDocNo), @CCCode, @DORequestDeptDocNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDeptDocNo", TxtDORequestDeptDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue("Select CCCode From TblDORequestDeptHdr Where DocNo='" + TxtDORequestDeptDocNo.Text + "'"));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDODeptDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, AssetCode, AcNo, Remark, CreateBy, CreateDt) Values " +
                    "(@DocNo, @DNo, 'N', @ItCode, @ReplacementInd, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @AssetCode, @AcNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@ReplacementInd", Sm.GetGrdBool(Grd1, Row, 7) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 31));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDODeptDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =

                    "Insert Into TblDODeptDtl2(DocNo, DNo, DORequestDeptDNo, Qty, Qty2, Qty3, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DORequestDeptDNo, @Qty, @Qty2, @Qty3, @CreateBy, CurrentDateTime())" +
                    "On Duplicate Key " +
                    "Update Qty=@Qty, Qty2=@Qty2, Qty3=@Qty3, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();",

            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@DORequestDeptDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDORequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDORequestDeptDtl T0 ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select A.DocNo, B.DNo, B.ItCode, C.ItName, B.Qty As QtyDOR, E.Qty As QTYDO  ");
            SQL.AppendLine("    From  TblDORequestDeptHdr A  ");
            SQL.AppendLine("    Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode  ");
            SQL.AppendLine("    Left Join TblAsset D On B.AssetCode= D.AssetCode  ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.DORequestDeptDocNo, B.DorequestDeptDno, SUM(B.Qty) As Qty, ");
            SQL.AppendLine("        SUM(B.Qty2) As Qty2, SUM(B.Qty3) As Qty3  ");
            SQL.AppendLine("        from TblDOdepthdr A");
            SQL.AppendLine("        Inner Join TblDODeptDtl2 B On A.DocNo = B.DocNo");
            SQL.AppendLine("        Where A.DORequestDeptDocNo=@DocNo ");
            SQL.AppendLine("        Group BY A.DORequestDeptDocNo, B.DorequestDeptDno ");
            SQL.AppendLine("    )E On E.DORequestDeptDocNo = A.DocNo And E.DorequestDeptDno = B.Dno  ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(" )T1 On T0.DocNo = T1.DocNo And T0.Dno = T1.Dno ");


            SQL.AppendLine("Set T0.ProcessInd =  ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("    When T0.Qty=0.00 Then 'F' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(T1.QtyDO, 0)>=T0.Qty Then 'F' ");
            SQL.AppendLine("        Else ");
            SQL.AppendLine("            Case When IfNull(T1.QtyDO, 0)=0.00 Then 'O' Else 'P' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("End; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDORequestDeptDocNo.Text);
            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join TblStockMovement T2 On T1.WhsCode=T2.WhsCode And T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T2.DocType=@DocType And T2.DocNo=@DocNo ");
            SQL.AppendLine("Set T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "05");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('DO To Department With DO Request : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(mCCCode));
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('DO To Department : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid(DNo)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            //update cancel indicator detail, save ke stockmovement, save ke stock summary
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) &&
                    !Sm.GetGrdBool(Grd1, r, 2) &&
                    Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    cml.Add(EditDODeptDtl(r));     
                }
            }

            cml.Add(DelDODeptDtl2()); 

            //insert yang baru
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 1) 
                    cml.Add(SaveDODeptDtl2(TxtDocNo.Text, Row));

            //update do request
           cml.Add(UpdateDORequest(TxtDocNo.Text));

            if (mIsAutoJournalActived)
            {
                if (mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal());
                else
                    cml.Add(SaveJournal2());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            string DNo = string.Empty;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDODeptDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DNo = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(DNo, Sm.GetGrdStr(Grd1, Row, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsCancelReasonEmpty() ||
                IsCancelledItemUsedForAnotherTransactions(DNo);
        }

        private bool IsCancelReasonEmpty()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) &&
                    !Sm.GetGrdBool(Grd1, r, 2) &&
                    Sm.GetGrdStr(Grd1, r, 30).Length == 0 &&
                    Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine + Environment.NewLine +
                        "Please input the reason why you cancel this item.");
                    return true;
                }
            }
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemUsedForAnotherTransactions(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin ");
            SQL.AppendLine("From TblRecvDeptDtl A, TblDODeptDtl B, TblItem C ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DODeptDocNo=B.DocNo ");
            SQL.AppendLine("And A.DODeptDNo=B.DNo ");
            SQL.AppendLine("And B.ItCode=C.ItCode ");
            SQL.AppendLine("And A.DODeptDocNo=@DocNo ");
            SQL.AppendLine("And Position(Concat('##', A.DODeptDNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "BatchNo", "Source", "Lot", "Bin" 
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                             "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                             "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                             "Batch# : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                             "Source : " + Sm.DrStr(dr, 3) + Environment.NewLine +
                             "Lot : " + Sm.DrStr(dr, 4) + Environment.NewLine +
                             "Bin : " + Sm.DrStr(dr, 5) + Environment.NewLine + Environment.NewLine +
                             "Department already returned this item.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }


        private MySqlCommand EditDODeptDtl(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptDtl Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");    
            SQL.AppendLine("And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, @CancelReason, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source;");

            SQL.AppendLine("Delete From TblDODeptDtl2 Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocType", "05");
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, r, 30));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 19));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DelDODeptDtl2()
        {
            var cm = new MySqlCommand() 
            { CommandText = "Delete From TblDODeptDtl2 Where DocNo=@DocNo;" };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text); 
            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Update TblDODeptDtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(mCCCode));
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Update TblDODeptDtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo" + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");

            if (mIsMovingAvgEnabled)
            {
                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));

                SQL.AppendLine("    Union All ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, 0.00 As DAmt, B.Qty*IfNull(F.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
            }
            else
            {
                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    SQL.AppendLine("        Select B.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDODeptHdr(DocNo);
                ShowDODeptDtl2(DocNo);
                ShowDODeptDtl(DocNo);
                ComputeBalance();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDODeptHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.WhsCode, A.DORequestDeptDocNo, C.LocalDocNo As DORequestDeptLocalDocNo, C.WODocNo, ");
            SQL.AppendLine("B.DeptName, C.Remark As RemarkDOR, D.EntName, C.CCCode, E.CCName, A.Remark, A.JournalDocNo ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblDORequestDeptHdr C On A.DORequestDeptDocNo = C.DocNo ");
            SQL.AppendLine("Left Join TblEntity D On C.EntCode=D.EntCode ");
            SQL.AppendLine("Left Join TblCostCenter E On C.CCCode=E.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "LocalDocNo", "WhsCode", "DORequestDeptDocNo", "DORequestDeptLocalDocNo", 
                        
                        //6-10
                        "WODocNo", "DeptName", "RemarkDOR", "EntName", "CCCode", 
                        
                        //11-13
                        "CCName", "Remark", "JournalDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        TxtDORequestDeptDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtDORequestDeptLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtWODocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[8]);
                        TxtEntName.EditValue = Sm.DrStr(dr, c[9]);
                        mCCCode = Sm.DrStr(dr, c[10]);
                        TxtCCName.EditValue = Sm.DrStr(dr, c[11]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowDODeptDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.CancelReason, B.ItCode, C.ItName, C.ForeignName, ");
            SQL.AppendLine("B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, ");

            SQL.AppendLine("Case When B.CancelInd='N' Then B.Qty Else 0.00 End+IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty) From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
            SQL.AppendLine("), 0.00) As AvailableStock, ");
            SQL.AppendLine("Case When B.CancelInd='N' Then B.Qty2 Else 0.00 End+IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty2) From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
            SQL.AppendLine("), 0.00) As AvailableStock2, ");
            SQL.AppendLine("Case When B.CancelInd='N' Then B.Qty3 Else 0.00 End+IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty3) From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
            SQL.AppendLine("), 0.00) As AvailableStock3, ");

            SQL.AppendLine("B.ReplacementInd, B.AssetCode, D.Assetname, D.DisplayName, ");
            SQL.AppendLine("G.CCtName, E.AcNo, C.ItGrpCode, B.AcNo As AcNo2 ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblAsset D On B.AssetCode = D.AssetCode ");
            SQL.AppendLine("Inner Join TblItemCategory E On C.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("Left Join TblItemCostCategory F On C.ItCode=F.ItCode And F.CCCode=@CCCode ");
            SQL.AppendLine("Left Join TblCostCategory G On F.CCCode=G.CCCode And F.CCtCode=G.CCtCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",  

                    //1-5
                    "CancelInd", "ItCode", "ItName", "BatchNo", "Source",   
                        
                    //6-10
                    "Lot", "Bin", "AvailableStock", "Qty", "InventoryUomCode", 

                    //11-15
                    "AvailableStock2", "Qty2", "InventoryUomCode2", "AvailableStock3", "Qty3", 
            
                    //16-20
                    "InventoryUomCode3", "ReplacementInd",  "AssetCode", "AssetName", "CCtname", 
                    
                    //21-25
                    "AcNo", "Remark", "ItGrpCode", "DisplayName", "ForeignName",

                    //26-27
                    "CancelReason", "AcNo2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2, 7 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowDODeptDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItName, C.ForeignName, B.Qty, B.Qty2, B.Qty3, B.AssetCode, D.AssetName, D.DisplayName, C.ItGrpCode ");
            SQL.AppendLine("From  TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblAsset D On B.AssetCode = D.AssetCode ");
            SQL.AppendLine("Where Concat(A.DocNo, B.DNo) In ( ");
            SQL.AppendLine("    Select Concat(T1.DORequestDeptDocNo, T2.DORequestDeptDNo) ");
            SQL.AppendLine("    From TblDODeptHdr T1, TblDODeptDtl2 T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.DocNo=@DocNo);");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "ItCode", "ItName", "Qty", "Qty2", "Qty3",
                    
                    //6-10
                    "AssetCode", "AssetName", "ItGrpCode", "DisplayName", "ForeignName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Grd.Cells[Row, 6].Value = 0m;
                    Grd.Cells[Row, 7].Value = Grd.Cells[Row, 5].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = Grd.Cells[Row, 8].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 5);
                    Grd.Cells[Row, 12].Value = 0m;
                    Grd.Cells[Row, 13].Value = Grd.Cells[Row, 11].Value;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 10);
                }, false, false, false, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.WhsCode As Col1, T.WhsName As Col2 ");
                SQL.AppendLine("From TblWarehouse T ");
                SQL.AppendLine("Where 0=0 ");
                if (WhsCode.Length != 0) SQL.AppendLine("And T.WhsCode=@WhsCode ");
                if (mIsDODeptFilterByAuthorization)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select WhsCode From TblGroupWarehouse ");
                    SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By T.WhsName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (WhsCode.Length != 0) Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (WhsCode.Length != 0) Sm.SetLue(LueWhsCode, WhsCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedDORequestItem()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 3).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 3) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeBalance()
        {
            decimal
                DORequestQty = 0m, DOQty = 0m, Balance = 0m,
                DORequestQty2 = 0m, DOQty2 = 0m, Balance2 = 0m,
                DORequestQty3 = 0m, DOQty3 = 0m, Balance3 = 0m;


            #region text DocNo null
            if (Grd2.Rows.Count != 0)
            {
                for (int Row1 = 0; Row1 < Grd2.Rows.Count; Row1++)
                {
                    if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                    {
                        DORequestQty = Sm.GetGrdDec(Grd2, Row1, 5);
                        DOQty = 0m;
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 4).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 4)) &&
                                Sm.GetGrdBool(Grd1, Row2, 1) != true
                                )
                                DOQty += Sm.GetGrdDec(Grd1, Row2, 13);
                        }
                        Balance = DORequestQty - DOQty;
                    }

                    if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                    {
                        DORequestQty2 = Sm.GetGrdDec(Grd2, Row1, 8);
                        DOQty2 = 0m;
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 4).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 4)) &&
                                Sm.GetGrdBool(Grd1, Row2, 1) != true
                                )
                                DOQty2 += Sm.GetGrdDec(Grd1, Row2, 16);
                        }
                        Balance2 = DORequestQty2 - DOQty2;
                    }

                    if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                    {
                        DORequestQty3 = Sm.GetGrdDec(Grd2, Row1, 11);
                        DOQty3 = 0m;
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 4).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 4)) &&
                                Sm.GetGrdBool(Grd1, Row2, 1) != true
                                )
                                DOQty3 += Sm.GetGrdDec(Grd1, Row2, 19);
                        }
                        Balance3 = DORequestQty3 - DOQty3;
                    }

                    Grd2.Cells[Row1, 6].Value = DOQty;
                    Grd2.Cells[Row1, 7].Value = Balance;
                    Grd2.Cells[Row1, 9].Value = DOQty2;
                    Grd2.Cells[Row1, 10].Value = Balance2;
                    Grd2.Cells[Row1, 12].Value = DOQty3;
                    Grd2.Cells[Row1, 13].Value = Balance3;
                }
            }
            #endregion


        }

        public void ShowDORequestDeptInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItName, C.ForeignName, (B.Qty-ifnull(E.Qty, 0)) As Qty, (B.Qty2-ifnull(E.Qty2, 0)) As Qty2, (B.Qty3-ifnull(E.Qty3, 0)) As Qty3, B.AssetCode, D.AssetName, D.DisplayName ");
            SQL.AppendLine("From  TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblAsset D On B.AssetCode= D.AssetCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("         Select A.DORequestDeptDocNo, B.DorequestDeptDno, SUM(B.Qty) As Qty, ");
            SQL.AppendLine("            SUM(B.Qty2) As Qty2, SUM(B.Qty3) As Qty3  ");
            SQL.AppendLine("            from TblDOdepthdr A");
            SQL.AppendLine("            Inner Join TblDODeptDtl2 B On A.DocNo = B.DocNo");
            SQL.AppendLine("            Where A.DORequestDeptDocNo=@DocNo");
            SQL.AppendLine("            Group BY A.DORequestDeptDocNo, B.DorequestDeptDno  ");
            SQL.AppendLine(")E On E.DORequestDeptDocNo=A.DocNo And E.DorequestDeptDno = B.Dno");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And B.Status = 'A' ");
            //SQL.AppendLine("And Concat(A.DocNo, B.DNo) Not In (");
            //SQL.AppendLine("    Select Concat(T1.DORequestDeptDocNo, T2.DORequestDeptDNo) ");
            //SQL.AppendLine("    From TblDODeptHdr T1, TblDODeptDtl2 T2 ");
            //SQL.AppendLine("    Where T1.DocNo=T2.DocNo);");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "ItCode", "ItName", "Qty", "Qty2", "Qty3",
  
                    //6-9
                    "AssetCode", "Assetname", "DisplayName", "ForeignName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Grd.Cells[Row, 6].Value = 0m;
                    Grd.Cells[Row, 7].Value = Grd.Cells[Row, 5].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = Grd.Cells[Row, 8].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 5);
                    Grd.Cells[Row, 12].Value = 0m;
                    Grd.Cells[Row, 13].Value = Grd.Cells[Row, 11].Value;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 9);
                }, false, false, false, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        public void SetAsset()
        {
            for (int RowY = 0; RowY < Grd1.Rows.Count; RowY++)
            {
                string ItCode = Sm.GetGrdStr(Grd1, RowY, 4);
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (ItCode == Sm.GetGrdStr(Grd2, Row, 3))
                    {
                        Grd1.Cells[RowY, 21].Value = Sm.GetGrdStr(Grd2, Row, 14);
                        Grd1.Cells[RowY, 22].Value = Sm.GetGrdStr(Grd2, Row, 15);
                        Grd1.Cells[RowY, 28].Value = Sm.GetGrdStr(Grd2, Row, 17);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void BtnDORequestDeptDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    { TxtDORequestDeptDocNo, TxtDeptCode, TxtCCName, MeeRemark2 });
                    ClearGrd();
                    Sm.FormShowDialog(new FrmDODeptDlg(this, Sm.GetLue(LueWhsCode)));
                }
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueWhsCode), string.Empty);
                ClearGrd();
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                { TxtDORequestDeptDocNo, TxtDeptCode });
            }
        }

        #endregion

        #region Button Event

        private void BtnWODocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWODocNo, "WO#", false))
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWODocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORequestDeptDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDeptDocNo, "DO request#", false))
            {
                if (TxtWODocNo.Text.Length == 0)
                {
                    var f = new FrmDORequestDept(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDORequestDeptDocNo.Text;
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDORequestDeptWO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDORequestDeptDocNo.Text;
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtDORequestDeptDocNo.Text.Length != 0)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDODeptDlg3(this, TxtDORequestDeptDocNo.Text));
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string ItCode = string.Empty;

                for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    ItCode = Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 3);
                    break;
                }

                if (Grd2.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                            Grd2.Rows.RemoveAt(Grd2.SelectedRows[Index].Index);
                        if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();
                    }
                }

                for (int Index = Grd1.Rows.Count - 1; Index >= 0; Index--)
                {
                    if (
                        Sm.GetGrdStr(Grd1, Index, 4).Length != 0 &&
                        Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Index, 4))
                        )
                    {
                        Grd1.Rows.RemoveAt(Index);
                    }
                }
                if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 &&
                BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                TxtDORequestDeptDocNo.Text.Length != 0)

                Sm.FormShowDialog(new FrmDODeptDlg3(this, TxtDORequestDeptDocNo.Text));
        }

        #endregion

        #endregion

        #region Report Class

        class DoDeptHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string DeptName { get; set; }
            public string DORequestDeptDocNo { get; set; }
            public string DORequestDeptdDocDt { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CCName { get; set; }
            public string DoReqCreateBy { get; set; }
            public string DoToDeptCreateby { get; set; }
            public string EmpPict { get; set; }
            public string EmpPict2 { get; set; }
        }

        class DoDeptHdr1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
        }

        class DoDeptHdr2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
        }

        class DoDeptDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string Remark { get; set; }
            public string ItGrpCode { get; set; }
        }

        #endregion
    }
}
