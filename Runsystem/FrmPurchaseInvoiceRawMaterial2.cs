﻿#region Update
/*
    19/04/2017 [TKG] tambah informasi retur.
    20/04/2017 [WED] tambah informasi retur di Print Out.
    20/04/2017 [WED] tambah ordering informasi retur berdasarkan DNo (di transaksi dan di print out)
    10/05/2017 [WED] perubahan grid, ShowReturInfo() dan BtnPrintClick untuk tampilan nilai retur dari Recv Raw Material
    10/05/2017 [WED] validasi nilai maksimum grand total selama sebulan, berdasarkan parameter PIRawMaterialMonthlyMaxAmount
    05/04/2018 [TKG] Purchase Invoice Raw Material (apabila harga raw material lbh kecil dari 1 untuk sementara dianggap 0).
    16/05/2018 [TKG] Tambah tax slip
    17/05/2018 [ARI] Tambah printout PPh 22
    28/06/2018 [TKG] ubah informasi saat menyimpan VR
    15/10/2018 [TKG] tambah proses untuk PI raw material untuk double payment.
    31/10/2018 [DITA] print out baru dengan 2 type payment
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    13/05/2019 [TKG] Tambah validasi TaxInd untuk kebutuhan VR VAT
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceRawMaterial2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mMonthlyMaxAmt = string.Empty;
        private string 
            VdCode = string.Empty, 
            mDocType = "51",
            mGlobalVendorRMP = string.Empty,
            mSeparator = ";";
        internal FrmPurchaseInvoiceRawMaterial2Find FrmFind;
        internal decimal PIRawMaterialWithNPWP = 0m, PIRawMaterialWithoutNPWP = 0m, PIRounding=0m;
        private bool mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmPurchaseInvoiceRawMaterial2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Pembayaran Bahan Baku";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetPIRounding(); 
                Sl.SetLueAcType(ref LueAcType);
                SetLueDocType(ref LueDocType);

                tabControl1.SelectTab("tabPage6");
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType2);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode2);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueBankCode(ref LueBankCode2);

                tabControl1.SelectTab("tabPage1");

                SetGrd(ref Grd1);
                SetGrd(ref Grd2);
                SetGrd();

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd(ref iGrid Grd)
        {
            Grd.Cols.Count = 9;
            Grd.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Quantity" + Environment.NewLine + "Batang",
                        "Quantity"+ Environment.NewLine + "Kubik",
                        "Quotation" + Environment.NewLine + "Document#",
                        
                        //6-8
                        "Quotation" + Environment.NewLine + "D#",
                        "Price",
                        "Amount"
                        
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        130, 300, 100, 100, 150,
                        
                        //6-8
                        100, 100, 100 
                    }
                );
            Sm.GrdFormatDec(Grd, new int[] { 3, 4, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd, new int[] { 0, 1, 5, 6 }, false);
        }

        private void SetGrd()
        {
            #region Grid 3

            Grd3.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] { "Document#", "Warehouse" },
                    new int[]{ 150, 400 }
                );

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 3;
            Grd4.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(Grd4, new string[]{ "Panjang", "Jumlah", "Keterangan" }, new int[] { 130, 130, 250 });
            Sm.GrdFormatDec(Grd4, new int[] { 1 }, 0);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueAcType, LueDocType,
                        TxtCurCode, TxtGrandTotal, TxtRawMaterialVerifyDocNo, TxtQueueNo, DteQueueDt, 
                        TxtVdCode, TxtTin, TxtTotalAmt, TxtOutstandingAmt, TxtAmt, 
                        TxtTaxPercentage, TxtTax, TxtTTCode, TxtTC, TxtOutstandingTC, 
                        TxtTransportCost, TxtPaymentUser, LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, 
                        TxtPaidToBankAcNo, LuePaymentType, LueBankAcCode, LueBankCode, TxtGiroNo, 
                        DteDueDt, TxtNail, TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark,
                        TxtTotalLog, TxtTotalLog1, TxtTotalLog2, TxtTotalBalok, TxtTotalBalok1,
                        TxtTotalBalok2, ChkTaxInd, TxtRoundingValue, TxtAmt1, LuePaymentType2, 
                        LueBankAcCode2, TxtAmt2, TxtPaymentUser2, LuePaidToBankCode2, TxtPaidToBankBranch2, 
                        TxtPaidToBankAcName2, TxtPaidToBankAcNo2, LueBankCode2, TxtGiroNo2, DteDueDt2
                    }, true);
                    BtnRawMaterialVerifyDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueAcType, LueDocType, TxtTransportCost, TxtPaymentUser, 
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LuePaymentType, 
                        LueBankAcCode, TxtNail, MeeRemark, TxtAmt1, LuePaymentType2, 
                        LueBankAcCode2, TxtAmt2, TxtPaymentUser2, LuePaidToBankCode2, TxtPaidToBankBranch2, 
                        TxtPaidToBankAcName2, TxtPaidToBankAcNo2, LueBankCode2, TxtGiroNo2, DteDueDt2
                    }, false);
                    BtnRawMaterialVerifyDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, ChkCancelInd, DteDocDt, LueAcType, LueDocType,
                TxtCurCode, TxtRawMaterialVerifyDocNo, TxtQueueNo, DteQueueDt, 
                TxtVdCode, TxtTin, TxtTTCode, TxtPaymentUser, LuePaidToBankCode, 
                TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LuePaymentType, LueBankAcCode, 
                LueBankCode, TxtGiroNo, DteDueDt, TxtVoucherRequestDocNo, TxtVoucherDocNo, 
                TxtTaxSlip, MeeRemark, LuePaymentType2, LueBankAcCode2, TxtPaymentUser2, 
                LuePaidToBankCode2, TxtPaidToBankBranch2, TxtPaidToBankAcName2, TxtPaidToBankAcNo2, LueBankCode2, 
                TxtGiroNo2, DteDueDt2, TxtVoucherRequestDocNo2, TxtVoucherDocNo2, TxtVoucherRequestDocNo3, 
                TxtVoucherDocNo3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtGrandTotal, TxtTotalAmt, TxtOutstandingAmt, TxtAmt, TxtTaxPercentage, 
                TxtTax, TxtTC, TxtOutstandingTC, TxtTransportCost, TxtNail, 
                TxtTotalLog, TxtTotalLog1, TxtTotalLog2, TxtTotalBalok, TxtTotalBalok1,
                TxtTotalBalok2, TxtRoundingValue, TxtTotalOutstandingAmtLog, TxtTotalOutstandingAmtBalok, TxtAmt1, 
                TxtAmt2
            }, 0);
            ChkCancelInd.Checked = false;
            ChkTaxInd.Checked = false;
            VdCode = string.Empty;
            PIRawMaterialWithNPWP = 0m;
            PIRawMaterialWithoutNPWP = 0m;
            ClearGrd(ref Grd1);
            ClearGrd(ref Grd2);
            Sm.ClearGrd(Grd3, true);
            ClearGrd4();
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ClearGrd(ref iGrid Grd)
        {
            Grd.Rows.Clear();
            Grd.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 3, 4, 7, 8 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 1 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPurchaseInvoiceRawMaterial2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueAcType, "C");
                Sm.SetLue(LueDocType, "1");
                TxtCurCode.Text = Sm.GetParameter("MainCurCode");
                BtnRawMaterialVerifyDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RawMaterial>();
            var ldtl = new List<RawMaterialDtl>();
            var ldtl1 = new List<RawMaterialDtl1>();
            var ldtl2 = new List<RawMaterialDtl2>();
            var ldtl3 = new List<RawMaterialDtl3>();
            var l2 = new List<RawMaterialPPH22>();

            string[] TableName = { "RawMaterial", "RawMaterialDtl", "RawMaterialDtl1", "RawMaterialDtl2", "RawMaterialDtl3", "RawMaterialPPH22" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd,  A.AcType, ");
            SQL.AppendLine("Concat(IfNull(C.BankAcNo, ''), ' ', IfNull(Concat('[ ', C.BankAcNm, ' ]'), '')) As BankAcc, ");
            SQL.AppendLine("(Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine("A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, A.Remark As RemarkHdr, A.CurCode, ");
            SQL.AppendLine("	 ifnull(Concat(A.PaymentUser, '     ', J.VdName), ifnull(J.VdName, A.PaymentUser)) As PaymentUser, A.RawMaterialVerifyDocNo, A.VoucherRequestDocNo, A.Nail, J.VdName As VdLog, ");
            SQL.AppendLine("N.VdName As VdBalok, A.Tax, A.TransportCost, A.DocType, A.RoundingValue, ");
            SQL.AppendLine("A.PaidToBankAcName, A.PaidToBankAcNo, O.BankName As PaidToBankName, A.PaidToBankBranch, ");
            SQL.AppendLine("A.PaidToBankAcName2, A.PaidToBankAcNo2, P.BankName As PaidToBankName2, A.PaidToBankBranch2, ");
            SQL.AppendLine("(Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType2 Limit 1) As PaymentType2, ");
            SQL.AppendLine("A.Amt1, A.Amt2 ");
            SQL.AppendLine(" From TblPurchaseInvoiceRawMaterialHdr A  ");
            SQL.AppendLine(" Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" left Join TblVoucherHdr K On A.DocNo = K.VoucherRequestDocNo ");
            SQL.AppendLine(" Left Join TblUser F On K.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblPurchaseInvoiceRawMaterialDtl G On A.DocNo=G.DocNo ");
            SQL.AppendLine(" left Join TblRawMaterialVerify L On A.RawMaterialVerifyDocNo = L.DocNo ");
            SQL.AppendLine(" Left Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo = M.DocNo ");
            SQL.AppendLine(" left Join TblVendor J On J.VdCode = M.VdCode1 ");
            SQL.AppendLine(" left Join TblVendor N On N.VdCode = M.VdCode2 ");
            SQL.AppendLine(" Left Join TblBank O On A.PaidToBankCode=O.BankCode ");
            SQL.AppendLine(" Left Join TblBank P On A.PaidToBankCode2=P.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         "CancelInd",
 
                         //6-10 
                         "AcType", 
                         "BankAcc",
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName",
 
                         //11-15, 
                         "GiroDueDt", 
                         "PICName",
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",

                         //16-20
                         "CurCode",
                         "PaymentUser",
                         "RawMaterialVerifyDocNo",
                         "VoucherRequestDocNo",
                         "Nail",

                         //21-25
                         "VdLog",
                         "VdBalok",
                         "Tax",
                         "TransportCost",
                         "DocType",

                         //26-30
                         "RoundingValue",
                         "PaidToBankAcName", 
                         "PaidToBankAcNo", 
                         "PaidToBankName", 
                         "PaidToBankBranch",

                         //31-35
                         "PaymentType2",
                         "PaidToBankAcName2", 
                         "PaidToBankAcNo2", 
                         "PaidToBankName2", 
                         "PaidToBankBranch2",

                         //36-37
                         "Amt1",
                         "Amt2"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RawMaterial()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),

                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            CancelInd = Sm.DrStr(dr, c[5]),
                            AcType = Sm.DrStr(dr, c[6]),
                            BankAcc = Sm.DrStr(dr, c[7]),
                            PaymentType = Sm.DrStr(dr, c[8]),
                            GiroNo = Sm.DrStr(dr, c[9]),
                            GiroBankName = Sm.DrStr(dr, c[10]),
                            GiroDueDt = Sm.DrStr(dr, c[11]),
                            PICName = Sm.DrStr(dr, c[12]),
                            AmtHdr = Sm.DrDec(dr, c[13]),
                            RemarkHdr = Sm.DrStr(dr, c[14]),

                            CompanyLogo = Sm.DrStr(dr, c[15]),
                            CurCode = Sm.DrStr(dr, c[16]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PaymentUser = Sm.DrStr(dr, c[17]),
                            RawMaterialVerifyDocNo = Sm.DrStr(dr, c[18]),
                            VoucherRequestDocNo = Sm.DrStr(dr, c[19]),
                            Nail = Sm.DrDec(dr, c[20]),
                            VdLog = Sm.DrStr(dr, c[21]),
                            VdBalok = Sm.DrStr(dr, c[22]),
                            Tax = Sm.DrDec(dr, c[23]),
                            TransportCost = Sm.DrDec(dr, c[24]),
                            DocType = Sm.DrStr(dr, c[25]),
                            RoundingValue = Sm.DrDec(dr, c[26]),
                            PaidToBankAcName = Sm.DrStr(dr, c[27]),
                            PaidToBankAcNo = Sm.DrStr(dr, c[28]),
                            PaidToBankName = Sm.DrStr(dr, c[29]),
                            PaidToBankBranch = Sm.DrStr(dr, c[30]),
                            PaymentType2 = Sm.DrStr(dr, c[31]),
                            PaidToBankAcName2 = Sm.DrStr(dr, c[32]),
                            PaidToBankAcNo2 = Sm.DrStr(dr, c[33]),
                            PaidToBankName2 = Sm.DrStr(dr, c[34]),
                            PaidToBankBranch2 = Sm.DrStr(dr, c[35]),
                            
                            Amt1 = Sm.DrDec(dr, c[36]),
                            Amt2 = Sm.DrDec(dr, c[37]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                string ItCtCode = string.Empty;
                cnDtl.Open();
                cmDtl.Connection = cnDtl;


                if (Sm.GetLue(LueDocType) == "1")
                {
                    ItCtCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual'");

                    SQLDtl.AppendLine("Select A.DNo, A.ItCode, SUM(O.Qty) As QtyItem, A.Qty, B.PurchaseUomCode, ");
                    SQLDtl.AppendLine("Concat(B.ItName, ' ( ',  ");
                    SQLDtl.AppendLine("            Concat( ");
                    SQLDtl.AppendLine("            'L: ', B.Length, ");
                    SQLDtl.AppendLine("            ', D: ', B.Diameter ");
                    SQLDtl.AppendLine("            ), ");
                    SQLDtl.AppendLine("' )') As ItName, ");
                    SQLDtl.AppendLine("A.QtDocNo, A.QtDNo, C.UPrice, A.Qty*C.UPrice As Amt ");
                    SQLDtl.AppendLine("From TblPurchaseInvoiceRawMaterialDtl A ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblQt2Dtl C On A.QtDocNo=C.DocNo And A.QtDNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr D On A.DocNo = D.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblRawMaterialVerify L On D.RawMaterialVerifyDocNo = L.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo = M.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialHdr N On N.LegalDocVerifyDocNo = M.DocNo And N.cancelInd = 'N' And N.ProcessInd = 'F' ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 O On O.DocNo = N.DOcNo And A.ItCode = O.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.ItCtCode = '" + ItCtCode + "' Group By A.DNo, A.ItCode ");
                }
                else
                {
                    ItCtCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual2'");
                    SQLDtl.AppendLine("Select A.DNo, A.ItCode, SUM(O.Qty) As QtyItem, A.Qty, B.PurchaseUomCode, ");
                    SQLDtl.AppendLine("Concat(B.ItName, ' ( ',  ");
                    SQLDtl.AppendLine("Concat(  ");
                    SQLDtl.AppendLine("        'L: ', B.Length, ");
                    SQLDtl.AppendLine("        ', W: ', B.Width,  ");
                    SQLDtl.AppendLine("        ', H: ', B.Height ");
                    SQLDtl.AppendLine("        ),  ");
                    SQLDtl.AppendLine("' )') As ItName, ");
                    SQLDtl.AppendLine("A.QtDocNo, A.QtDNo, C.UPrice, A.Qty*C.UPrice As Amt ");
                    SQLDtl.AppendLine("From TblPurchaseInvoiceRawMaterialDtl A ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblQt2Dtl C On A.QtDocNo=C.DocNo And A.QtDNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr D On A.DocNo = D.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblRawMaterialVerify L On D.RawMaterialVerifyDocNo = L.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo = M.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialHdr N On N.LegalDocVerifyDocNo = M.DocNo  And N.cancelInd = 'N' And N.ProcessInd = 'F' ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 O On O.DocNo = N.DOcNo And A.ItCode = O.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.ItCtCode = '" + ItCtCode + "' Group By A.DNo, A.ItCode ");

                }

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                cmDtl.CommandText = SQLDtl.ToString();

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItName",

                         //1-5
                         "QtyItem",
                         "Qty" ,
                         "PurchaseUomCode",
                         "UPrice" ,
                         "Amt",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RawMaterialDtl()
                        {
                            Item = Sm.DrStr(drDtl, cDtl[0]),
                            QtyItem = Sm.DrDec(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            Uom = Sm.DrStr(drDtl, cDtl[3]),
                            UPrice = Sm.DrDec(drDtl, cDtl[4]),
                            Amount = Sm.DrDec(drDtl, cDtl[5]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Data 2
            var cmDtl1 = new MySqlCommand();
            var SQLDtl1 = new StringBuilder();

            SQLDtl1.AppendLine("Select A.DocNo, ");
            SQLDtl1.AppendLine("Group_Concat(Distinct(E.EmpName)) As Grid, ");
            SQLDtl1.AppendLine("Group_Concat(Distinct(G.EmpName)) As AsGrid, ");
            SQLDtl1.AppendLine("Group_Concat(Distinct(I.EmpName)) As BongDal, ");
            SQLDtl1.AppendLine("Group_Concat(Distinct(K.EmpName)) As BongLu, ");
            SQLDtl1.AppendLine("Group_concat(Distinct(M1.DlCode)) As Leg,  O.TTName, N.LicenceNo, P.DocNo As Queue  ");
            SQLDtl1.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQLDtl1.AppendLine("Left Join TblRawMaterialVerify B On A.RawMaterialVerifyDocNo = B.DocNo ");
            SQLDtl1.AppendLine("Left Join TblRecvrawMaterialHdr C On B.LegalDocVerifyDocNo = C.LegalDocVerifyDocNo And C.CancelInd = 'N' ");
            SQLDtl1.AppendLine("Left Join TblRecvRawMaterialDtl D On C.DocNo = D.DocNo And D.EmpType = '1' ");
            SQLDtl1.AppendLine("Left join TblEmployee E On E.EmpCode = D.EmpCode	            ");
            SQLDtl1.AppendLine("Left Join TblRecvRawMaterialDtl F On C.DocNo = F.DocNo And F.EmpType = '2' ");
            SQLDtl1.AppendLine("Left join TblEmployee G On G.EmpCode = F.EmpCode	            ");
            SQLDtl1.AppendLine("Left Join TblRecvRawMaterialDtl H On C.DocNo = H.DocNo And H.EmpType = '3' ");
            SQLDtl1.AppendLine("Left join TblEmployee I On H.EmpCode = I.EmpCode	            ");
            SQLDtl1.AppendLine("Left Join TblRecvRawMaterialDtl J On C.DocNo = J.DocNo And J.EmpType = '4' ");
            SQLDtl1.AppendLine("left Join TblRawMaterialVerify L On A.RawMaterialVerifyDocNo = L.DocNo ");
            SQLDtl1.AppendLine("Left Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo = M.DocNo ");
            SQLDtl1.AppendLine("Left Join TblLoadingQueue P On M.QueueNo=P.DocNo ");
            SQLDtl1.AppendLine("Left join TblEmployee K On J.EmpCode = K.EmpCode ");
            SQLDtl1.AppendLine("Left Join TblLoadingQueue N On M.QueueNo=N.DocNo ");
            SQLDtl1.AppendLine("Left Join TblTransportType O On O.TTCode=N.TTCode ");
            SQLDtl1.AppendLine("Left Join TblLegalDocVerifyDtl M1 On M1.DocNo=M.DocNo ");
            SQLDtl1.AppendLine("Where A.DocNo = @DocNo ");


            using (var cnDtl1 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl1.Open();
                cmDtl1.Connection = cnDtl1;
                cmDtl1.CommandText = SQLDtl1.ToString();
                Sm.CmParam<String>(ref cmDtl1, "@DocNo", TxtDocNo.Text);
                var drDtl1 = cmDtl1.ExecuteReader();
                var cDtl1 = Sm.GetOrdinal(drDtl1, new string[] 
                        {
                         "Grid",

                         //1-5
                         "AsGrid",
                         "BongDal",
                         "BongLu", 
                         "Leg", 
                         "TTName",
 
                         //6-10 
                         "LicenceNo",
                         "Queue"
                        });
                if (drDtl1.HasRows)
                {
                    while (drDtl1.Read())
                    {
                        ldtl1.Add(new RawMaterialDtl1()
                        {
                            Grid = Sm.DrStr(drDtl1, cDtl1[0]),
                            AsGrid = Sm.DrStr(drDtl1, cDtl1[1]),
                            BongDal = Sm.DrStr(drDtl1, cDtl1[2]),

                            BongLu = Sm.DrStr(drDtl1, cDtl1[3]),
                            Leg = Sm.DrStr(drDtl1, cDtl1[4]),
                            TTName = Sm.DrStr(drDtl1, cDtl1[5]),
                            LicenceNo = Sm.DrStr(drDtl1, cDtl1[6]),
                            Queue = Sm.DrStr(drDtl1, cDtl1[7]),
                        });
                    }
                }
                drDtl1.Close();
            }
            myLists.Add(ldtl1);
            #endregion

            #region Detail Data 3
            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select SUM(A.Amt-A.Tax-A.TransportCost+A.RoundingValue) As AmtAll ");
            SQLDtl2.AppendLine("From TblPurchaseInvoicerawmaterialHdr A ");
            SQLDtl2.AppendLine("Where A.RawMaterialVerifyDocNo = @Ver And A.CancelInd = 'N' ");
            SQLDtl2.AppendLine("Group By A.RawMaterialVerifyDocNo ");


            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                cmDtl2.CommandText = SQLDtl2.ToString();
                //Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtl2, "@Ver", TxtRawMaterialVerifyDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         "AmtAll",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new RawMaterialDtl2()
                        {
                            AmtAll = Sm.DrDec(drDtl2, cDtl2[0]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Data 4 - Retur

            var cmDtl3 = new MySqlCommand();
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Select IfNull(C.OptDesc, Round(B.Length, 2)) As Length, B.Qty, B.Remark  ");
            SQLDtl3.AppendLine("From TblRecvRawMaterialHdr A ");
            SQLDtl3.AppendLine("Inner Join TblRecvRawMaterialDtl4 B On A.DocNo=B.DocNo ");
            SQLDtl3.AppendLine("Left Join TblOption C On Round(B.Length, 0) = C.OptCode And C.OptCat = 'ReturnRawMaterialLength' ");
            SQLDtl3.AppendLine("Where A.LegalDocVerifyDocNo In ( ");
            SQLDtl3.AppendLine("    Select LegalDocVerifyDocNo ");
            SQLDtl3.AppendLine("    From TblRawMaterialVerify ");
            SQLDtl3.AppendLine("    Where DocNo=@RawMaterialDocNo ");
            SQLDtl3.AppendLine(") ");
            SQLDtl3.AppendLine("And A.CancelInd='N' ");
            SQLDtl3.AppendLine("Order By B.Length, B.DocNo, B.DNo;");

            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;
                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@RawMaterialDocNo", TxtRawMaterialVerifyDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         "Length",
                         "Qty", "Remark"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new RawMaterialDtl3()
                        {
                            Length = Sm.DrStr(drDtl3, cDtl3[0]),
                            Qty = Sm.DrDec(drDtl3, cDtl3[1]),
                            Remark = Sm.DrStr(drDtl3, cDtl3[2]),
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            #region PPH23
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.DocNo, Date_Format(A.Docdt, '%d %M %Y')As DocDt, A.Amt, A.Tax, ((A.Tax/A.Amt)*100) As TaxPercentage, D.VdCode, F.VdName, F.Tin, F.Address, G.CityName, ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'NPWP', A.TaxSlip ");
            SQL2.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A   ");
            SQL2.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL2.AppendLine("Inner Join TblRawMaterialVerify C On A.RawMaterialVerifyDocNo=C.DocNo ");
            SQL2.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo = D.DocNo ");
            SQL2.AppendLine("Inner Join TblLoadingQueue E On D.QueueNo = E.DocNo ");
            SQL2.AppendLine("Inner Join TblVendor F On D.VdCode = F.VdCode ");
            SQL2.AppendLine("left Join TblCity G On F.CityCode=G.CityCode ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         "DocNo",

                         //1-5
                         "DocDt",
                         "Amt",
                         "Tax", 
                         "TaxPercentage", 
                         "VdCode",
 
                         //6-10 
                         "VdName", 
                         "Tin",
                         "Address",
                         "CityName",
                         "CompanyName",

                         //11-12
                         "NPWP",
                         "TaxSlip"
                });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new RawMaterialPPH22()
                        {
                            DocNo = Sm.DrStr(dr2, c2[0]),
                            DocDt = Sm.DrStr(dr2, c2[1]),
                            Amt = Sm.DrDec(dr2, c2[2]),
                            Tax = Sm.DrDec(dr2, c2[3]),
                            TaxPercentage = Sm.DrDec(dr2, c2[4]),
                            VdCode = Sm.DrStr(dr2, c2[5]),

                            VdName = Sm.DrStr(dr2, c2[6]),
                            Tin = Sm.DrStr(dr2, c2[7]),
                            Address = Sm.DrStr(dr2, c2[8]),
                            CityName = Sm.DrStr(dr2, c2[9]),
                            CompanyName = Sm.DrStr(dr2, c2[10]),
                            NPWP = Sm.DrStr(dr2, c2[11]),
                            TaxSlip = Sm.DrStr(dr2, c2[12]),
                            Terbilang = ConvertFromDecToWord(Decimal.Parse(RemoveChar(Sm.DrDec(dr2, c2[3])))) + dec(Sm.DrDec(dr2, c2[3])),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            Sm.PrintReport("PurchaseInvoiceRawMaterial2", myLists, TableName, false);
            if (Doctitle == "IOK")
                Sm.PrintReport("PPH22", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private bool IsPriceEmpty()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd1, r, 7) <= 0m) return true;
                    }
                }
            }

            if (Grd2.Rows.Count > 0)
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd2, r, 7) <= 0m) return true;
                    }
                }
            }
            return false;
        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            if (IsPriceEmpty())
            {
                if (Sm.StdMsgYN("Question", 
                    "One or more items has no price." + Environment.NewLine + 
                    "Do you still want to save this document ?"
                    ) == DialogResult.No) return;
            }

            Cursor.Current = Cursors.WaitCursor;


            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseInvoiceRawMaterial", "TblPurchaseInvoiceRawMaterialHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SavePurchaseInvoiceRawMaterialHdr(DocNo, VoucherRequestDocNo));

            int DNo = 0;

            if (Sm.GetLue(LueDocType) == "1")
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SavePurchaseInvoiceRawMaterialDtl(DocNo, ref DNo, ref Grd1, Row));
            }

            if (Sm.GetLue(LueDocType) == "2")
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SavePurchaseInvoiceRawMaterialDtl(DocNo, ref DNo, ref Grd2, Row));
            }

            cml.Add(SaveVoucherRequest(VoucherRequestDocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LueDocType, "Document Type (For)") ||
                Sm.IsTxtEmpty(TxtRawMaterialVerifyDocNo, "Verification document#", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Paid amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsTxtEmpty(TxtCurCode, "Currency", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPaymentTypeNotValid() ||
                //IsRawMaterialVerifyAlreadyProcessed() ||
                IsLogAmtInvalid() ||
                IsBalokAmtInvalid() ||
                IsAmtNotValid() ||
                IsTransportCostNotValid() ||
                IsGrdValueNotValid() ||
                IsVendorInvalid() ||
                IsGrandTotalInvalid() ||
                IsAmt2InfoInvalid() ||
                ((mMonthlyMaxAmt.Length > 0 && mMonthlyMaxAmt != "0") && IsMonthlyMaxAmtExceeds());
        }

        private bool IsGrandTotalInvalid()
        {
            decimal 
                GrandTotal = decimal.Parse(TxtGrandTotal.Text),
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);

            if (GrandTotal != (Amt1 + Amt2))
            {
                Sm.StdMsg(mMsgType.Warning, "First and second payment amount is not the same as grand total.");
                return true;
            }
            return false;
        }

        private bool IsAmt2InfoInvalid()
        {
            decimal Amt2 = decimal.Parse(TxtAmt2.Text);

            if (Amt2!=0)
            {
                if (Sm.IsLueEmpty(LuePaymentType2, "Second payment's payment type")) return true;
                if (Sm.IsLueEmpty(LueBankAcCode2, "Second payment's cash/bank account")) return true;
            }
            return false;
        }

        private bool IsMonthlyMaxAmtExceeds()
        {
            decimal Amt = 0m, Accumulation = 0m;
            decimal MonthlyAmt = 0m;

            #region Inserted Amount

            if (TxtAmt.EditValue.ToString().Length != 0) Amt = decimal.Parse(TxtAmt.EditValue.ToString());

            #endregion

            #region Compute Monthly Amount

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //SQL.AppendLine("Select (IfNull(Sum(A.Amt), 0) - IfNull(Sum(A.Tax), 0) - IfNull(Sum(A.TransportCost), 0)) As GrandTotal ");
            SQL.AppendLine("Select IfNull(Sum(A.Amt), 0) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On A.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo = D.DocNo ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @ThisMonth ");
            SQL.AppendLine("And D.VdCode = @VdCode ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ThisMonth", Sm.GetDte(DteDocDt).Substring(0, 6));
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        MonthlyAmt = decimal.Parse(Sm.DrStr(dr, 0));
                    }
                }
                dr.Close();
            }

            #endregion

            Accumulation = Amt + MonthlyAmt;

            if (Accumulation >= decimal.Parse(mMonthlyMaxAmt))
            {
                return Sm.StdMsgYN("Question",
                    "Month  : " + Sm.GetDte(DteDocDt).Substring(4, 2) + Environment.NewLine +
                    "Vendor : " + TxtVdCode.Text + Environment.NewLine + Environment.NewLine +
                    "Monthly Amount accumulation exceeds " +
                    Sm.GetValue("Select Convert(Format(IfNull(ParValue, 0), 2) Using utf8) From TblParameter Where ParCode = 'PIRawMaterialMonthlyMaxAmount' ")
                    + ". " + Environment.NewLine + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
            else
            {
                return false;
            }
        }

        private bool IsVendorInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblRawMaterialVerify A ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr B ");
            SQL.AppendLine("    On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("    And (B.VdCode=@VdCode Or IfNull(B.VdCode1, '')=@VdCode Or IfNull(B.VdCode2, '')=@VdCode) ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtRawMaterialVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", mGlobalVendorRMP);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid vendor.");
                return true;
            }
            return false;
        }

        private bool IsLogAmtInvalid()
        {
            if (Sm.GetLue(LueDocType) != "1") return false;

            ComputeTotalOutstandingAmtLog();

            decimal 
                Amt = decimal.Parse(TxtAmt.Text),
                Outstanding = decimal.Parse(TxtTotalOutstandingAmtLog.Text)
                ;

            if (Amt > Outstanding)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Paid amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine +
                    "Outstanding amount (log) : " + Sm.FormatNum(Outstanding, 0) + Environment.NewLine + Environment.NewLine +
                    "Paid amount is bigger than Outstanding amount (log).");
                return true;
            }
            return false;
        }

        private bool IsBalokAmtInvalid()
        {
            if (Sm.GetLue(LueDocType) != "2") return false;

            ComputeTotalOutstandingAmtBalok();

            decimal
                Amt = decimal.Parse(TxtAmt.Text),
                Outstanding = decimal.Parse(TxtTotalOutstandingAmtBalok.Text)
                ;

            if (Amt > Outstanding)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Paid amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine +
                    "Outstanding amount (balok) : " + Sm.FormatNum(Outstanding, 0) + Environment.NewLine + Environment.NewLine +
                    "Paid amount is bigger than Outstanding amount (balok).");
                return true;
            }
            return false;
        }

        private bool IsRawMaterialVerifyAlreadyProcessed()
        {
            return false;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo ");
            SQL.AppendLine("And DocType=@DocType;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Verification# : " + TxtRawMaterialVerifyDocNo.Text + Environment.NewLine +
                    "For : " + LueDocType.GetColumnValue("Col2") + Environment.NewLine +
                    "This data already processed.");
                return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsAmtNotValid()
        {
            decimal OutstandingAmt = 0m, Amt = 0m;

            ComputeOutstandingAmt();
            ComputeTax();
            ComputeGrandTotal(true);

            OutstandingAmt = decimal.Parse(TxtOutstandingAmt.Text);
            Amt = decimal.Parse(TxtAmt.Text);

            if (Amt > OutstandingAmt)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Outstanding Amount : " + TxtOutstandingAmt.Text + Environment.NewLine +
                    "Paid Amount : " + TxtAmt.Text + Environment.NewLine + Environment.NewLine +
                    "Paid amount is bigger than outstanding amount."
                    );
                TxtAmt.Focus();
                return true;
            }
            return false;
        }

        private bool IsTransportCostNotValid()
        {
            decimal OutstandingTC = 0m, TransportCost = 0m;

            ComputeOutstandingTC();
            ComputeGrandTotal(true);

            OutstandingTC = decimal.Parse(TxtOutstandingTC.Text);
            TransportCost = decimal.Parse(TxtTransportCost.Text);

            if (TransportCost > OutstandingTC)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Outstanding Transport Cost : " + TxtOutstandingTC.Text + Environment.NewLine +
                    "Paid Transport Cost Amount : " + TxtTransportCost.Text + Environment.NewLine + Environment.NewLine +
                    "Paid transport cost is bigger than outstanding transport cost."
                    );
                TxtTransportCost.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Unit price should not be 0.")) return true;
                    }
                }
            }
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 5, false, "Unit price should not be 0.")) return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SavePurchaseInvoiceRawMaterialHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceRawMaterialHdr");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, DocType, RawMaterialVerifyDocNo, VoucherRequestDocNo, ");
            SQL.AppendLine("AcType, PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo, ");
            SQL.AppendLine("PaymentType, BankAcCode, Amt1, PaymentType2, BankAcCode2, Amt2, BankCode, GiroNo, DueDt, CurCode, Qty1, Qty2, Amt, Tax, TotalTransportCost, TransportCost, RoundingValue, Nail, Remark, ");
            SQL.AppendLine("PaymentUser2, PaidToBankCode2, PaidToBankBranch2, PaidToBankAcName2, PaidToBankAcNo2, BankCode2, GiroNo2, DueDt2, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @DocType, @RawMaterialVerifyDocNo, @VoucherRequestDocNo, ");
            SQL.AppendLine("@AcType, @PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @Amt1, @PaymentType2, @BankAcCode2, @Amt2, @BankCode, @GiroNo, @DueDt, @CurCode, @Qty1, @Qty2, @Amt, @Tax, @TotalTransportCost, @TransportCost, @RoundingValue, @Nail, @Remark, ");
            SQL.AppendLine("@PaymentUser2, @PaidToBankCode2, @PaidToBankBranch2, @PaidToBankAcName2, @PaidToBankAcNo2, @BankCode2, @GiroNo2, @DueDt2, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblRawMaterialVerify Set ");
            SQL.AppendLine("    ProcessInd= ");
            SQL.AppendLine("    Case When Not Exists( ");
            SQL.AppendLine("            Select 1 From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("            Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo And CancelInd='N' Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When @Total<=IfNull(( ");
            SQL.AppendLine("                Select Sum(Amt) From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("                Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo And CancelInd='N' ");
            SQL.AppendLine("            ), 0) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@RawMaterialVerifyDocNo And CancelInd='N' And IfNull(ProcessInd, 'O')<>'F'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            var Value = Sm.GetLue(LuePaidToBankCode);
            if (Value.Length > 3) Value = Sm.Left(Value, Value.Length - 3);
            var Value2 = Sm.GetLue(LuePaidToBankCode2);
            if (Value2.Length > 3) Value2 = Sm.Left(Value2, Value2.Length - 3);

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", Value);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PaymentUser2", TxtPaymentUser2.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode2", Value2);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch2", TxtPaidToBankBranch2.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName2", TxtPaidToBankAcName2.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo2", TxtPaidToBankAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@BankCode2", Sm.GetLue(LueBankCode2));
            Sm.CmParam<String>(ref cm, "@GiroNo2", TxtGiroNo2.Text);
            Sm.CmParamDt(ref cm, "@DueDt2", Sm.GetDte(DteDueDt2));
            Sm.CmParam<String>(ref cm, "@PaymentType2", Sm.GetLue(LuePaymentType2));
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Decimal.Parse(TxtTotalLog1.Text) + Decimal.Parse(TxtTotalBalok1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Decimal.Parse(TxtTotalLog2.Text) + Decimal.Parse(TxtTotalBalok2.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTransportCost", Decimal.Parse(TxtTC.Text));
            Sm.CmParam<Decimal>(ref cm, "@TransportCost", Decimal.Parse(TxtTransportCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Decimal.Parse(TxtRoundingValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@Nail", Decimal.Parse(TxtNail.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Total", Decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceRawMaterialDtl(string DocNo, ref int DNo, ref iGrid Grd, int Row)
        {
            DNo += 1;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseInvoiceRawMaterialDtl(DocNo, DNo, ItCode, Qty, QtDocNo, QtDNo, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ItCode, @Qty, @QtDocNo, @QtDNo, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 4));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd, Row, 5));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");
            string type;

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }


        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo) //, string OutgoingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, ");
            SQL.AppendLine("CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'A', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='PurchaseInvoiceRawMaterialDeptCode'), ");
            SQL.AppendLine("'11', Null, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@UserCode Limit 1), ");
            SQL.AppendLine("0, @CurCode, @Amt-@Tax-@TransportCost+@RoundingValue, @PaymentUser, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Description, @Amt-@Tax-@TransportCost+@RoundingValue, Null, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TransportCost", Decimal.Parse(TxtTransportCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Decimal.Parse(TxtRoundingValue.Text));
            if (decimal.Parse(TxtAmt2.Text) != 0m)
            {
                if (MeeRemark.Text.Length == 0)
                    Sm.CmParam<String>(ref cm, "@Remark", "Pemisahan metode pembayaran");
                else
                    Sm.CmParam<String>(ref cm, "@Remark", string.Concat("Pemisahan metode pembayaran. ", MeeRemark.Text));
            }
            else
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", string.Concat(LueDocType.GetColumnValue("Col2"), " (", TxtVdCode.Text, ")"));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string OutgoingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, ");
            SQL.AppendLine("CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'A', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='PurchaseInvoiceRawMaterialDeptCode'), ");
            SQL.AppendLine("'11', Null, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@UserCode Limit 1), ");
            SQL.AppendLine("0, @CurCode, @Amt-@Tax-@TransportCost+@RoundingValue, @PaymentUser, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TransportCost", Decimal.Parse(TxtTransportCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Decimal.Parse(TxtRoundingValue.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo, ref int DNo, ref iGrid Grd, int Row)
        {
            DNo += 1;
            string Description =
                Sm.GetGrdStr(Grd, Row, 2) + " ( " +
                Sm.FormatNum(Sm.GetGrdDec(Grd, Row, 4), 0) + " x" +
                Sm.FormatNum(Sm.GetGrdDec(Grd, Row, 7), 0) + " )";

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Description);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl2(string DocNo, ref int DNo, string Description, decimal Amt, decimal Value)
        {
            DNo += 1;
          
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Description);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Value*Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Purchase Invoice (Raw Material) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select Concat(B.ParValue, @VdCode) As AcNo, ");
            SQL.AppendLine("        IfNull(A.Amt, 0) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoUnInvoiceAP' And IfNull(B.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(A.Tax, 0) As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForTaxPayable22' And IfNull(B.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(A.TransportCost, 0) As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForRawmaterial_Salary' And IfNull(B.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.ParValue As AcNo, ");
            SQL.AppendLine("        IfNull(A.RoundingValue, 0) As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForPurchasingCost_Rounding' And IfNull(B.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(B.ParValue, @VdCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo In ( ");
            SQL.AppendLine("            Select VoucherRequestDocNo ");
            SQL.AppendLine("            From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("            Where DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ) ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelPurchaseInvoiceRawMaterialHdr());

            if (mIsAutoJournalActived && IsJournalDataExisted()) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            ComputeOutstandingAmt();
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                IsDataProcessedAlready2() ||
                (ChkCancelInd.Checked && IsTaxIndInvalid());
        }

        private bool IsTaxIndInvalid()
        {
            if (Sm.IsDataExist("Select 1 From TblPurchaseInvoiceRawMaterialHdr Where DocNo=@Param And TaxInd='Y';", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already prepared for Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPurchaseInvoiceRawMaterialHdr " +
                    "Where JournalDocNo Is Not Null And DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPurchaseInvoiceRawMaterialHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);
            
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedAlready2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo2 ");
            SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("    Where DocNo=@Param ");
            SQL.AppendLine("    And VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo3 ");
            SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("    Where DocNo=@Param ");
            SQL.AppendLine("    And VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("    ); ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text, "Data already processed into voucher.");
        }

        private bool IsInventoryDataNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType ");
            SQL.AppendLine("From TblStockMovement A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select DocType, DocNo, DNo, Source, CancelInd ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocType<>@DocType ");
            SQL.AppendLine("    And Substring(Source, 4, Length(Source)-7) In ( ");
            SQL.AppendLine("        Select RecvVdRawMaterialDocNo ");
            SQL.AppendLine("        From TblPurchaseInvoicerawMaterialDtl2 ");
            SQL.AppendLine("        Where DocNo=@DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.DNo=B.DNo ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Where A.DocType<>@DocType ");
            SQL.AppendLine("And Substring(A.Source, 4, Length(A.Source)-7) In ( ");
            SQL.AppendLine("    Select RecvVdRawMaterialDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoicerawMaterialDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And B.CancelInd Is Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Stock data already used in another transaction.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelPurchaseInvoiceRawMaterialHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo2 From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo And VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo3 From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo And VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblRawMaterialVerify Set ");
            SQL.AppendLine("    ProcessInd= ");
            SQL.AppendLine("    Case When Not Exists( ");
            SQL.AppendLine("            Select DocNo From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("            Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo And CancelInd='N' Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When @Total<=IfNull(( ");
            SQL.AppendLine("                Select Sum(Amt) From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("                Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo And CancelInd='N' ");
            SQL.AppendLine("            ), 0) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@RawMaterialVerifyDocNo And CancelInd='N'; ");

            //SQL.AppendLine("Insert Into TblStockMovement ");
            //SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, Qty, Qty2, Qty3, ");
            //SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, Qty, Qty2, Qty3, ");
            //SQL.AppendLine("Remark, CreateBy, CreateDt ");
            //SQL.AppendLine("From TblStockMovement ");
            //SQL.AppendLine("Where DocType=@DocType ");
            //SQL.AppendLine("And Substring(Source, 4, Length(Source)-7) In ( ");
            //SQL.AppendLine("    Select RecvVdRawMaterialDocNo ");
            //SQL.AppendLine("    From TblPurchaseInvoicerawMaterialDtl2 ");
            //SQL.AppendLine("    Where DocNo=@DocNo ");
            //SQL.AppendLine("); ");

            //SQL.AppendLine("Update TblStockSummary Set ");
            //SQL.AppendLine("    Qty=0, Qty2=0, Qty3=0, ");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where Substring(Source, 1, 2)=@DocType ");
            //SQL.AppendLine("And Substring(Source, 4, Length(Source)-7) In ( ");
            //SQL.AppendLine("    Select RecvVdRawMaterialDocNo ");
            //SQL.AppendLine("    From TblPurchaseInvoicerawMaterialDtl2 ");
            //SQL.AppendLine("    Where DocNo=@DocNo ");
            //SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Total", Decimal.Parse(TxtTotalAmt.Text));
            //Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Purchase Invoice (Raw Material) : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblPurchaseInvoiceRawMaterialHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblPurchaseInvoiceRawMaterialHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPurchaseInvoiceRawMaterialHdr(DocNo);
                ShowItemInfo(TxtRawMaterialVerifyDocNo.Text, "1", ref Grd1);
                ShowItemInfo(TxtRawMaterialVerifyDocNo.Text, "2", ref Grd2);
                //ShowPurchaseInvoiceRawMaterialDtl(DocNo, "1", ref Grd1);
                //ShowPurchaseInvoiceRawMaterialDtl(DocNo, "2", ref Grd2);
                ShowPurchaseInvoiceRawMaterialDtl2(DocNo);
                TxtTotalLog1.EditValue = Sm.FormatNum(ComputeTotalQty(1, ref Grd1), 0);
                TxtTotalLog2.EditValue = Sm.FormatNum(ComputeTotalQty(2, ref Grd1), 0);
                TxtTotalBalok1.EditValue = Sm.FormatNum(ComputeTotalQty(1, ref Grd2), 0);
                TxtTotalBalok2.EditValue = Sm.FormatNum(ComputeTotalQty(2, ref Grd2), 0);
                ComputeTotalAmt();
                ComputeOutstandingAmt();
                ComputeTotalOutstandingAmtLog();
                ComputeTotalOutstandingAmtBalok();
                ComputeTax();
                ComputeOutstandingTC();
                ComputeGrandTotal(true);
                ShowReturInfo(TxtRawMaterialVerifyDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPurchaseInvoiceRawMaterialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.VoucherDocNo, D.VdCode, F.VdName, F.Tin, F.TaxInd, ");
            SQL.AppendLine("D.QueueNo, E.CreateDt As QueueDt, G.TTName, G.UnLoadingCost, ((A.Tax/A.Amt)*100) As TaxPercentage, ");
            SQL.AppendLine("H.VoucherDocNo As VoucherDocNo2, I.VoucherDocNo As VoucherDocNo3 ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On A.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLoadingQueue E On D.QueueNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblVendor F On D.VdCode = F.VdCode ");
            SQL.AppendLine("Left Join TblTransportType G On E.TTCode = G.TTCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr H On A.VoucherRequestDocNo2=H.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr I On A.VoucherRequestDocNo3=I.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "AcType", "DocType", "CurCode",    

                        //6-10
                        "RawMaterialVerifyDocNo", "QueueNo", "QueueDt", "VdCode", "VdName", 
                        
                        //11-15
                        "Tin", "Amt", "TaxPercentage", "Tax", "TTName",    
                        
                        //16-20
                        "TotalTransportCost", "TransportCost", "PaymentUser", "PaidToBankCode", "PaidToBankBranch", 
                        
                        //21-25
                        "PaidToBankAcName", "PaidToBankAcNo", "PaymentType", "BankAcCode", "BankCode", 
                        
                        //26-30
                        "GiroNo", "DueDt", "Nail", "VoucherRequestDocNo", "VoucherDocNo",
                        
                        //31-35
                        "TaxInd", "Deduction", "TaxSlip", "Remark", "Amt1",
 
                        //36-40
                        "PaymentType2", "BankAcCode2", "Amt2", "VoucherRequestDocNo2", "VoucherDocNo2",

                        //41-45
                        "VoucherRequestDocNo3", "VoucherDocNo3", "PaymentUser2", "PaidToBankCode2", "PaidToBankBranch2", 
                        
                        //46-50
                        "PaidToBankAcName2", "PaidToBankAcNo2", "BankCode2", "GiroNo2", "DueDt2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueDocType, Sm.DrStr(dr, c[4]));
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtRawMaterialVerifyDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteQueueDt, Sm.DrStr(dr, c[8]));
                        VdCode = Sm.DrStr(dr, c[9]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtTin.EditValue = Sm.DrStr(dr, c[11]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        TxtTaxPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[15]);
                        TxtTC.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        TxtTransportCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[18]);
                        SetLuePaidToBankCode(ref LuePaidToBankCode, string.Empty, "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[19]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[20]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[21]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[24]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[25]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[27]));
                        TxtNail.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[29]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[30]);
                        ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[31]), "Y");
                        TxtRoundingValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[32]), 0);
                        TxtTaxSlip.EditValue = Sm.DrStr(dr, c[33]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[34]);
                        TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                        Sm.SetLue(LuePaymentType2, Sm.DrStr(dr, c[36]));
                        Sm.SetLue(LueBankAcCode2, Sm.DrStr(dr, c[37]));
                        TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                        TxtVoucherRequestDocNo2.EditValue = Sm.DrStr(dr, c[39]);
                        TxtVoucherDocNo2.EditValue = Sm.DrStr(dr, c[40]);
                        TxtVoucherRequestDocNo3.EditValue = Sm.DrStr(dr, c[41]);
                        TxtVoucherDocNo3.EditValue = Sm.DrStr(dr, c[42]);
                        TxtPaymentUser2.EditValue = Sm.DrStr(dr, c[43]);
                        SetLuePaidToBankCode(ref LuePaidToBankCode2, string.Empty, "1");
                        Sm.SetLue(LuePaidToBankCode2, Sm.DrStr(dr, c[44]));
                        TxtPaidToBankBranch2.EditValue = Sm.DrStr(dr, c[45]);
                        TxtPaidToBankAcName2.EditValue = Sm.DrStr(dr, c[46]);
                        TxtPaidToBankAcNo2.EditValue = Sm.DrStr(dr, c[47]);
                        Sm.SetLue(LueBankCode2, Sm.DrStr(dr, c[48]));
                        TxtGiroNo2.EditValue = Sm.DrStr(dr, c[49]);
                        Sm.SetDte(DteDueDt2, Sm.DrStr(dr, c[50]));
                    }, true
                );
        }

        private void ShowPurchaseInvoiceRawMaterialDtl(string DocNo, string DocType, ref iGrid grd)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual", Sm.GetParameter("ItCtRMPActual"));
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", Sm.GetParameter("ItCtRMPActual2"));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, D.Qty1, A.Qty As Qty2, B.PurchaseUomCode, ");
            SQL.AppendLine("Concat(B.ItName, ' ( ', ");
            SQL.AppendLine("    Case B.ItCtCode ");
            SQL.AppendLine("        When @ItCtRMPActual Then ");
            SQL.AppendLine("            Concat( ");
            SQL.AppendLine("            'D: ', Convert(Format(B.Diameter, 2) Using utf8) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        When @ItCtRMPActual2 Then ");
            SQL.AppendLine("            Concat( ");
            SQL.AppendLine("            'W: ', Convert(Format(B.Width, 2) Using utf8), ");
            SQL.AppendLine("            ', H: ', Convert(Format(B.Height, 2) Using utf8) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    End, ");
            SQL.AppendLine("' )') As ItName, ");
            SQL.AppendLine("A.QtDocNo, A.QtDNo, C.UPrice, A.Qty*C.UPrice As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialDtl A ");
            SQL.AppendLine("Inner Join TblItem B ");
            SQL.AppendLine("    On A.ItCode=B.ItCode ");
            SQL.AppendLine("    And B.ItCtCode=Case When @DocType='1' Then @ItCtRMPActual Else @ItCtRMPActual2 End ");
            SQL.AppendLine("Inner Join TblQt2Dtl C On A.QtDocNo=C.DocNo And A.QtDNo=C.DNo ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("        Select T3.ItCode, Sum(T3.Qty) As Qty1 ");
            SQL.AppendLine("        From  TblRawMaterialVerify T1 ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterialHdr T2 ");
            SQL.AppendLine("            On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo ");
            SQL.AppendLine("            And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterialDtl2 T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("        Where T1.DocNo=@RawMaterialVerifyDocNo ");
            SQL.AppendLine("        Group By T3.ItCode ");
            SQL.AppendLine(") D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref grd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "Qty1", "Qty2", "QtDocNo", 

                    //6-8
                    "QtDNo", "UPrice", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    if (dr.GetDecimal(c[7]) < 1m)
                    {
                        Grd.Cells[Row, 7].Value = 0m;
                        Grd.Cells[Row, 8].Value = 0m;
                    }
                    else
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref grd, grd.Rows.Count - 1, new int[] { 3, 4, 7, 8 });
            Sm.FocusGrd(grd, 0, 1);
        }

        private void ShowPurchaseInvoiceRawMaterialDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RecvVdRawMaterialDocNo, B.WhsName ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialDtl2 A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.RecvVdRawMaterialDocNo");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { "RecvVdRawMaterialDocNo", "WhsName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Additional Method

        #region Terbilang

        private string ConvertFromDecToWord(Decimal d)
        {
            string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
            string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
            string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
            string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };

            string strHasil = "";
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = ConvertFromDecToWord(Decimal.Round(frac * 100)) + "";
            else
                strHasil = "";
            int xDigit = 0;
            int xPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpx = "";
                xDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                xPosisi = (strTemp.Length - i) + 1;
                switch (xPosisi % 3)
                {
                    case 1:
                        bool allNull = false;
                        if (i == 1)
                            tmpx = satuan[xDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpx = belasan[xDigit] + " ";
                        else if (xDigit > 0)
                            tmpx = satuan[xDigit] + " ";
                        else
                        {
                            allNull = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0")
                                    allNull = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0")
                                    allNull = false;
                            tmpx = "";
                        }

                        if ((!allNull) && (xPosisi > 1))
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpx = "se" + ribuan[(int)Decimal.Round(xPosisi / 3m)] + " ";
                            else
                                tmpx = tmpx + ribuan[(int)Decimal.Round(xPosisi / 3)] + " ";
                        strHasil = tmpx + strHasil;
                        break;
                    case 2:
                        if (xDigit > 0)
                            strHasil = puluhan[xDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (xDigit > 0)
                            if (xDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[xDigit] + " ratus " + strHasil;
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }
            return strHasil;
        }

        private string RemoveChar(Decimal x)
        {
            string numb = x.ToString();
            int indexChar = numb.IndexOf(".");
            numb = numb.Substring(0, indexChar);
            return numb;
        }

        private string dec(Decimal a)
        {
            string numbEnd = " koma ";
            string numbInd = string.Empty;

            string numb = a.ToString();
            int indexChar = numb.IndexOf(".") + 1;

            numb = numb.Substring(indexChar, (numb.Length - indexChar));

            string numbDec = numb;
            for (int i = 0; i < numbDec.Length; i++)
            {
                if (numbDec[i].ToString() == "0")
                {
                    numbInd = "nol ";
                }
                else if (numbDec[i].ToString() == "1")
                {
                    numbInd = "satu ";
                }
                else if (numbDec[i].ToString() == "2")
                {
                    numbInd = "dua ";
                }
                else if (numbDec[i].ToString() == "3")
                {
                    numbInd = "tiga ";
                }
                else if (numbDec[i].ToString() == "4")
                {
                    numbInd = "empat ";
                }
                else if (numbDec[i].ToString() == "5")
                {
                    numbInd = "lima ";
                }
                else if (numbDec[i].ToString() == "6")
                {
                    numbInd = "enam ";
                }
                else if (numbDec[i].ToString() == "7")
                {
                    numbInd = "tujuh ";
                }
                else if (numbDec[i].ToString() == "8")
                {
                    numbInd = "delapan ";
                }
                else
                {
                    numbInd = "sembilan ";
                }

                numbEnd = numbEnd + numbInd;
            }

            numb = numbEnd;
            return numb;
        }

        #endregion

        private void GetParameter()
        {
            mGlobalVendorRMP = Sm.GetParameter("GlobalVendorRMP");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mMonthlyMaxAmt = Sm.GetParameter("PIRawMaterialMonthlyMaxAmount");
        }

        private void SetPIRounding()
        {
            PIRounding = 0m;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                PIRounding = (decimal)new MySqlCommand(
                    "Select Cast(( " +
                    "   Select ParValue From TblParameter Where ParCode='PIRounding' " +
                    ") As Decimal(18, 4)) As PIRounding;",
                    cn).ExecuteScalar();
            }
        }

        private void HideInfoInGrd(ref iGrid Grd)
        {
            Sm.GrdColInvisible(Grd, new int[] { 1, 5 }, !ChkHideInfoInGrd.Checked);
        }

        internal void ShowInfo(string DocNo)
        {
            try
            {
                VdCode = string.Empty;
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo,
                    LuePaidToBankCode2, TxtPaidToBankBranch2, TxtPaidToBankAcName2, TxtPaidToBankAcNo2
                });

                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                    TxtTotalAmt, TxtOutstandingAmt, TxtAmt,
                    TxtOutstandingTC, TxtTransportCost   
                }, 0);

                ShowItemInfo(DocNo, "1", ref Grd1);
                ShowItemInfo(DocNo, "2", ref Grd2);

                TxtTotalLog1.EditValue = Sm.FormatNum(ComputeTotalQty(1, ref Grd1), 0);
                TxtTotalLog2.EditValue = Sm.FormatNum(ComputeTotalQty(2, ref Grd1), 0);
                TxtTotalBalok1.EditValue = Sm.FormatNum(ComputeTotalQty(1, ref Grd2), 0);
                TxtTotalBalok2.EditValue = Sm.FormatNum(ComputeTotalQty(2, ref Grd2), 0);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                SetLuePaidToBankCode(ref LuePaidToBankCode, VdCode, "2");

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode2 }, false);
                SetLuePaidToBankCode(ref LuePaidToBankCode2, VdCode, "2");

                ComputeTotalAmt();

                ComputeTotalOutstandingAmtLog();
                ComputeTotalOutstandingAmtBalok();

                ComputeOutstandingAmt();

                if (Sm.GetLue(LueDocType).Length == 0)
                    TxtAmt.Text = TxtOutstandingAmt.Text;
                else
                {
                    if (Sm.GetLue(LueDocType) == "1")
                        TxtAmt.Text = TxtTotalOutstandingAmtLog.Text; //TxtTotalLog.Text;
                    else
                        TxtAmt.Text = TxtTotalOutstandingAmtBalok.Text; //TxtTotalBalok.Text;
                }

                ComputeTax();
                ComputeOutstandingTC();
                TxtTransportCost.Text = TxtOutstandingTC.Text;
                ComputeGrandTotal(false);
                ShowReturInfo(TxtRawMaterialVerifyDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowItemInfo(string DocNo, string DocType, ref iGrid grd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.ItCode, T2.PurchaseUomCode, T1.VdCode, ");
            SQL.AppendLine("Concat(T2.ItName, ' ( ', ");
            SQL.AppendLine("    Case T2.ItCtCode ");
            SQL.AppendLine("        When @ItCtRMPActual Then ");
            SQL.AppendLine("            Concat( ");
            SQL.AppendLine("            'D: ', Convert(Format(T2.Diameter, 2) Using utf8) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        When @ItCtRMPActual2 Then ");
            SQL.AppendLine("            Concat( ");
            SQL.AppendLine("            'W: ', Convert(Format(T2.Width, 2) Using utf8), ");
            SQL.AppendLine("            ', H: ', Convert(Format(T2.Height, 2) Using utf8) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    End, ");
            SQL.AppendLine("' )') As ItName, T1.Qty As Qty1, ");
            SQL.AppendLine("(T1.Qty * (");
            SQL.AppendLine("Case T2.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then Truncate(((0.25*3.1415926*T2.Diameter*T2.Diameter*T2.Length)/1000000), 4) ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then Truncate(((T2.Length*T2.Width*T2.Height)/1000000), 4) ");
            SQL.AppendLine("    Else 0 End ");
            SQL.AppendLine(")) As Qty2, ");
            SQL.AppendLine("Left(T1.UPriceKey, Length(T1.UPriceKey)-3) As QtDocNo, ");
            SQL.AppendLine("Right(T1.UPriceKey, 3) As QtDNo, ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select UPrice From TblQt2Dtl ");
            SQL.AppendLine("    Where DocNo=Left(T1.UPriceKey, Length(T1.UPriceKey)-3) ");
            SQL.AppendLine("    And DNo=Right(T1.UPriceKey, 3) ");
            SQL.AppendLine("), 0) As UPrice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, VdCode, Qty,  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(A.DocNo, B.DNo) As UPriceKey ");
            SQL.AppendLine("        From TblQt2Hdr A, TblQt2Dtl B ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.DocDt<=T.LoadingQueueDocDt ");
            SQL.AppendLine("        And B.PGCode=T.PGCode ");
            SQL.AppendLine("        Order By A.DocDt Desc, A.DocNo Desc ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) As UPriceKey ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.ItCode, F.ItCtCode, F.ItName, F.PGCode, F.Length, F.Height, D.VdCode, Left(E.CreateDt, 8) As LoadingQueueDocDt, Sum(C.Qty) As Qty ");
            SQL.AppendLine("        From  TblRawMaterialVerify A ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterialHdr B On A.LegalDocVerifyDocNo=B.LegalDocVerifyDocNo And B.CancelInd='N' And B.ProcessInd='F' ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr D On A.LegalDocVerifyDocNo=D.DocNo And D.CancelInd='N' And D.ProcessInd1='F' And D.ProcessInd2='F' ");
            SQL.AppendLine("        Inner Join TblLoadingQueue E On D.QueueNo=E.DocNo ");
            SQL.AppendLine("        Inner Join TblItem F ");
            SQL.AppendLine("            On C.ItCode=F.ItCode ");
            SQL.AppendLine("            And F.ItCtCode=Case When @DocType='1' Then @ItCtRMPActual Else @ItCtRMPActual2 End ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            //SQL.AppendLine("        And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Group By C.ItCode, F.ItCtCode, F.ItName, F.Length, F.Height, D.VdCode, Left(E.CreateDt, 8) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") T1, TblItem T2 ");
            SQL.AppendLine("Where T1.ItCode=T2.ItCode ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@ItCtRMP", Sm.GetParameter("ItCtRMP"));
            Sm.CmParam<String>(ref cm, "@ItCtRMP2", Sm.GetParameter("ItCtRMP2"));
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual", Sm.GetParameter("ItCtRMPActual"));
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", Sm.GetParameter("ItCtRMPActual2"));

            Sm.ShowDataInGrid(
                ref grd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItName", "Qty1", "Qty2", "QtDocNo", "QtDNo", 
                    
                    //6-7
                    "VdCode", "UPrice"                            
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    VdCode = dr.GetString(c[6]);
                    if (dr.GetDecimal(c[7]) < 1m)
                    {
                        Grd.Cells[Row, 7].Value = 0m;
                        Grd.Cells[Row, 8].Value = 0m;
                    }
                    else
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                        Grd.Cells[Row, 8].Value = dr.GetDecimal(c[3]) * dr.GetDecimal(c[7]);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref grd, grd.Rows.Count - 1, new int[] { 3, 4, 7, 8 });
            Sm.FocusGrd(grd, 0, 1);
        }

        private void ComputeTotalAmt()
        {
            decimal TotalLog = 0m, TotalBalok = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    TotalLog += Sm.GetGrdDec(Grd1, Row, 8);

            TxtTotalLog.Text = Sm.FormatNum(TotalLog, 0);

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 8).Length > 0)
                    TotalBalok += Sm.GetGrdDec(Grd2, Row, 8);

            TxtTotalBalok.Text = Sm.FormatNum(TotalBalok, 0);

            TxtTotalAmt.Text = Sm.FormatNum(TotalLog+TotalBalok, 0);
        }

        private decimal ComputeTotalQty(byte Type, ref iGrid Grd)
        {
            //Type = 1 -> Batang
            //Type = 2 -> Kubik

            decimal Total = 0m;
            int Col = (Type == 1) ? 3 : 4;

            for (int Row = 0; Row < Grd.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd, Row, Col).Length > 0)
                    Total += Sm.GetGrdDec(Grd, Row, Col);

            return Total;
        }

        private void ComputeOutstandingAmt()
        {
            string PaidAmt = "0";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(Amt) Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And DocNo<>@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length == 0) ? "XXX" : TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            PaidAmt = Sm.GetValue(cm);

            if (PaidAmt.Length > 0)
                TxtOutstandingAmt.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalAmt.Text) - decimal.Parse(PaidAmt), 0);
            else
                TxtOutstandingAmt.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalAmt.Text), 0);
        }

        private void ComputeTotalOutstandingAmtLog()
        {
            var Amt = "0";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(Amt) Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("Where RawMaterialVerifyDocNo=@Param ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And DocType='1';");

            Amt = Sm.GetValue(SQL.ToString(), TxtRawMaterialVerifyDocNo.Text);

            if (Amt.Length > 0)
                TxtTotalOutstandingAmtLog.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalLog.Text) - decimal.Parse(Amt), 0);
            else
                TxtTotalOutstandingAmtLog.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalLog.Text), 0);

            if (!BtnSave.Enabled && Sm.GetLue(LueDocType)=="1")
                TxtTotalOutstandingAmtLog.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalOutstandingAmtLog.Text) + decimal.Parse(TxtAmt.Text), 0);
        }

        private void ComputeTotalOutstandingAmtBalok()
        {
            var Amt = "0";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(Amt) Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("Where RawMaterialVerifyDocNo=@Param ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And DocType='2';");

            Amt = Sm.GetValue(SQL.ToString(), TxtRawMaterialVerifyDocNo.Text);

            if (Amt.Length > 0)
                TxtTotalOutstandingAmtBalok.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalBalok.Text) - decimal.Parse(Amt), 0);
            else
                TxtTotalOutstandingAmtBalok.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalBalok.Text), 0);

            if (!BtnSave.Enabled && Sm.GetLue(LueDocType) == "2")
                TxtTotalOutstandingAmtBalok.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalOutstandingAmtBalok.Text) + decimal.Parse(TxtAmt.Text), 0);
        }

        private void ComputeTax()
        {
            decimal Amt = 0m, TaxPercentage = 0m;

            if (TxtAmt.EditValue.ToString().Length!=0) Amt=decimal.Parse(TxtAmt.EditValue.ToString());
            if (TxtTaxPercentage.EditValue.ToString().Length != 0) TaxPercentage = decimal.Parse(TxtTaxPercentage.EditValue.ToString());
            
            TxtTax.Text = Sm.FormatNum(Amt*TaxPercentage*0.01m, 0);
        }

        private void ComputeOutstandingTC()
        {
            string PaidAmt = "0";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(TransportCost) TransportCost ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
            SQL.AppendLine("Where RawMaterialVerifyDocNo=@RawMaterialVerifyDocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And DocNo<>@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length == 0) ? "XXX" : TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RawMaterialVerifyDocNo", TxtRawMaterialVerifyDocNo.Text);
            PaidAmt = Sm.GetValue(cm);

            if (PaidAmt.Length > 0)
                TxtOutstandingTC.EditValue = Sm.FormatNum(decimal.Parse(TxtTC.Text) - decimal.Parse(PaidAmt), 0);
            else
                TxtOutstandingTC.EditValue = Sm.FormatNum(decimal.Parse(TxtTC.Text), 0);
        }

        private void ComputeGrandTotal(bool IsValidate)
        {
            decimal Amt = 0m, Tax = 0m, TransportCost = 0m, GrandTotal = 0m, RoundingValue = 0m;

            if (TxtAmt.EditValue.ToString().Length != 0) Amt = decimal.Parse(TxtAmt.EditValue.ToString());
            if (TxtTax.EditValue.ToString().Length != 0) Tax = decimal.Parse(TxtTax.EditValue.ToString());
            if (TxtTransportCost.EditValue.ToString().Length != 0) TransportCost = decimal.Parse(TxtTransportCost.EditValue.ToString());

            //1780
            GrandTotal = Amt - Tax - TransportCost;

            if (PIRounding != 0)
            {
                //280
                RoundingValue = GrandTotal % (PIRounding * 2);

                if (RoundingValue > PIRounding && RoundingValue < (PIRounding * 2))
                {
                    //220
                    RoundingValue = (PIRounding * 2) - RoundingValue;
                    //2000
                    GrandTotal += RoundingValue;
                }
                else
                {
                    //1500
                    GrandTotal -= RoundingValue;
                    //-280
                    RoundingValue *= -1;
                }
            }
            TxtRoundingValue.EditValue = Sm.FormatNum(RoundingValue, 0);
            TxtGrandTotal.EditValue = Sm.FormatNum(GrandTotal, 0);
            if (!IsValidate)
            {
                TxtAmt1.EditValue = Sm.FormatNum(GrandTotal, 0);
                TxtAmt2.EditValue = Sm.FormatNum(0m, 0);
            }
        }

        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string VdCode, string Type)
        {
            //Type = 1 -> For View
            //Type = 2 -> For Insert

            var SQL = new StringBuilder();

            if (Type == "1")
            {
                SQL.AppendLine("Select BankCode As Col1, BankName As Col2, '-' As Col3 ");
                SQL.AppendLine("From TblBank Order By BankName;");
            }
            else
            {
                SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                SQL.AppendLine("Trim(Concat(");
                SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                SQL.AppendLine(")) As Col3 ");
                SQL.AppendLine("From TblVendorBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                SQL.AppendLine("Where VdCode='" + VdCode + "' ");
                SQL.AppendLine("Order By B.BankName, A.DNo; ");
            }
            Sm.SetLue3(
                ref Lue,
                SQL.ToString(),
                0, 170, 300, false, true, true, "Code", "Bank", "Bank Branch/Account Name/Account No.", "Col2", "Col1");
        }

        private void ShowPaidToBankCodeInfo(string VdCode, string DNo, bool IsFirst)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblVendorBankAccount Where VdCode=@VdCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            if (IsFirst)
                            {
                                TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                                TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                                TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                            }
                            else
                            {
                                TxtPaidToBankBranch2.EditValue = Sm.DrStr(dr, c[0]);
                                TxtPaidToBankAcName2.EditValue = Sm.DrStr(dr, c[1]);
                                TxtPaidToBankAcNo2.EditValue = Sm.DrStr(dr, c[2]);
                            }
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2;");
            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowReturInfo(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(C.OptDesc, Round(B.Length, 2)) As Length, B.Qty, B.Remark  ");
            SQL.AppendLine("From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("Inner Join TblRecvRawMaterialDtl4 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblOption C On Round(B.Length, 0) = C.OptCode And C.OptCat = 'ReturnRawMaterialLength' ");
            SQL.AppendLine("Where A.LegalDocVerifyDocNo In ( ");
            SQL.AppendLine("    Select LegalDocVerifyDocNo ");
            SQL.AppendLine("    From TblRawMaterialVerify ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By B.Length, B.DocNo, B.DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { "Length", "Qty", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, false, false
            );
            Grd4.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnRawMaterialVerifyDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPurchaseInvoiceRawMaterial2Dlg(this));
        }

        private void BtnRawMaterialVerifyDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRawMaterialVerifyDocNo, "Verification#", false))
            {
                try
                {
                    var f = new FrmRawMaterialVerify(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtRawMaterialVerifyDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo2, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo2.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo2, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo2.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo3_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo3, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo3.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo3_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo3, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo3.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }


        #endregion

        #region Misc Control Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd(ref Grd1);
            HideInfoInGrd(ref Grd2);
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
                if (Sm.GetLue(LueDocType).Length == 0)
                    TxtAmt.Text = Sm.FormatNum(0m, 0);
                else
                {
                    ComputeTotalOutstandingAmtLog();
                    ComputeTotalOutstandingAmtBalok();

                    if (Sm.GetLue(LueDocType) == "1")
                        TxtAmt.Text = TxtTotalOutstandingAmtLog.Text; // TxtTotalLog.Text;
                    else
                        TxtAmt.Text = TxtTotalOutstandingAmtBalok.Text;  //TxtTotalBalok.Text;
                }

                TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0);

                if (Sm.GetLue(LueDocType).Length != 0)
                {
                    if (Sm.GetLue(LueDocType) == "1" && ChkTaxInd.Checked)
                    {
                        if (TxtTin.Text.Length != 0)
                            TxtTaxPercentage.EditValue = Sm.FormatNum(PIRawMaterialWithNPWP, 0);
                        else
                            TxtTaxPercentage.EditValue = Sm.FormatNum(PIRawMaterialWithoutNPWP, 0);
                    }

                    if (Sm.GetLue(LueDocType) == "2")
                        TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0);

                    //if (Sm.GetLue(LueDocType) == "2" && TxtTin.Text.Length != 0)
                    //    TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0); //Sm.FormatNum(PIRawMaterialWithNPWP, 0);
                    //else
                    //    TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0); //Sm.FormatNum(PIRawMaterialWithoutNPWP, 0);
                }

                ComputeTax();
                ComputeGrandTotal(false);
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), VdCode, "2");
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo });
                var Value = Sm.GetLue(LuePaidToBankCode);
                if (Value.Length != 0)
                {
                    var DNo = string.Empty;
                    if (Value.Length > 3) DNo = Sm.Right(Value, 3);
                    ShowPaidToBankCodeInfo(VdCode, DNo, true);
                }
            }
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmt, 0);
                ComputeTax();
                ComputeGrandTotal(false);
            }
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(MeeRemark);
        }

        private void TxtNail_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtNail, 0);
        }

        private void TxtTransportCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtTransportCost, 0);
                ComputeGrandTotal(false);
            }
        }

        private void LuePaymentType2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType2, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode2, TxtGiroNo2, DteDueDt2 });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode2, false);
                    Sm.SetControlReadOnly(TxtGiroNo2, true);
                    Sm.SetControlReadOnly(DteDueDt2, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType2), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode2, false);
                    Sm.SetControlReadOnly(TxtGiroNo2, false);
                    Sm.SetControlReadOnly(DteDueDt2, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode2, true);
                Sm.SetControlReadOnly(TxtGiroNo2, true);
                Sm.SetControlReadOnly(DteDueDt2, true);
            }
        }

        private void LueBankAcCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode2, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtAmt2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt2, 0);
        }

        private void TxtAmt1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt1, 0);

            decimal GrandTotal = 0m, Amt1 = 0m, Amt2 = 0m;
            if (TxtGrandTotal.Text.Length > 0) GrandTotal = decimal.Parse(TxtGrandTotal.Text);
            if (TxtAmt1.Text.Length > 0) Amt1 = decimal.Parse(TxtAmt1.Text);
            Amt2 = GrandTotal - Amt1;
            if (Amt2 < 0) Amt2 = 0m;
            TxtAmt2.EditValue = Sm.FormatNum(Amt2, 0);
        }

        private void LueBankCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode2, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo2);
        }

        private void TxtPaymentUser2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser2);
        }

        private void LuePaidToBankCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaidToBankCode2, new Sm.RefreshLue3(SetLuePaidToBankCode), VdCode, "2");
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPaidToBankBranch2, TxtPaidToBankAcName2, TxtPaidToBankAcNo2 });
                var Value = Sm.GetLue(LuePaidToBankCode2);
                if (Value.Length != 0)
                {
                    var DNo = string.Empty;
                    if (Value.Length > 3) DNo = Sm.Right(Value, 3);
                    ShowPaidToBankCodeInfo(VdCode, DNo, false);
                }
            }
        }

        private void TxtPaidToBankBranch2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch2);
        }

        private void TxtPaidToBankAcNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo2);
        }

        private void TxtPaidToBankAcName2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName2);
        }

        #endregion

        #endregion

        #region Report Class

        #region Report Class

        private class RawMaterial
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string AcType { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public string PaymentUser { get; set; }
            public string RawMaterialVerifyDocNo { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public decimal Nail { get; set; }
            public string VdLog { get; set; }
            public string VdBalok { get; set; }
            public decimal Tax { get; set; }
            public decimal TransportCost { get; set; }
            public string DocType { get; set; }
            public decimal RoundingValue { get; set; }
            public string PaidToBankAcName { get; set; }
            public string PaidToBankAcNo { get; set; }
            public string PaidToBankName { get; set; }
            public string PaidToBankBranch { get; set; }
            public string PaidToBankAcName2 { get; set; }
            public string PaidToBankAcNo2 { get; set; }
            public string PaidToBankName2 { get; set; }
            public string PaidToBankBranch2 { get; set; }
            public string PaymentType2 { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
        }

        class RawMaterialDtl
        {
            public string Item { get; set; }
            public decimal QtyItem { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amount { get; set; }
        }

        class RawMaterialDtl1
        {
            public string Grid { get; set; }
            public string AsGrid { get; set; }
            public string BongDal { get; set; }
            public string BongLu { get; set; }
            public string Leg { get; set; }
            public string TTName { get; set; }
            public string LicenceNo { get; set; }
            public string Queue { get; set; }
        }

        class RawMaterialDtl2
        {
            public decimal AmtAll { get; set; }
        }

        class RawMaterialDtl3
        {
            public string Length { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
        }

        class RawMaterialPPH22
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Tax { get; set; }
            public decimal TaxPercentage { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string Tin { get; set; }
            public string Address { get; set; }
            public string CityName { get; set; }
            public string CompanyName { get; set; }
            public string NPWP { get; set; }
            public string Terbilang { get; set; }
            public string TaxSlip { get; set; }
        }

        #endregion

        #endregion
    }
}
