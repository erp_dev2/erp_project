﻿#region Update
/*
    22/11/2019 [TKG/SIER] New application
    04/02/2020 [TKG/SIER] hanya boleh 1 warehouse
    04/03/2020 [HAR/SIER] tambah filter customer category
    06/03/2020 [IBL/SIER] merubah field date di copying DO to customer menjadi periode, yang berisikan bulan dan tahun (feedback)
    17/06/2020 [DITA/SIER] Data doct yg bisa dipilih hanya yang sesuai backdate
 *  28/04/2021 [HAR/SIER] BUG : Copying Do to CustomerDocument DO yang sdh dipakai, masih muncul
 *  28/05/2021 [RDH/SIER] Tambah validasi hanya document DO to Customer yang ACTIVE saja yang bisa dipilih. Semua Document DO to Customer selama itu aktif.
 *  29/03/2022 [SET/SIER] Field Customer Category dan Customer terfilter berdasarkan group setting berdasar parameter IsFilterByCtCt
 *  27/07/2022 [MAU/SIER] Tambah filter DO to Customer yang masih Draft tidak dimunculkan di menu Copy DO to Customer
 *  11/08/2022 [TYO/SIER] tambah kolom total amount
 *  15/09/2022 [MAU/SIER] Feedback : validasi customer category empty ketika klik refresh berdasarkan group
 *  26/09/2022 [VIN/SIER] BUG : customer belum ter otorisasi by group 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt6Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt6 mFrmParent;

        #endregion

        #region Constructor

        public FrmDOCt6Dlg(FrmDOCt6 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetGrd();
                SetSQL();
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueMth(LueMth);
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#", 
                        "",
                        "Date", 
                        "Customer",
                        
                        //6-10
                        "Customer"+Environment.NewLine+"Category",
                        "Warehouse",
                        "Shipping",
                        "Remark",
                        "PassedInd",

                        //11
                        "Total Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 20, 100, 200,
                        
                        //6-10
                        200, 120,  300, 250, 0,

                        //11
                        150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] {10});
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[11].Move(9);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtName, D.CtCtname, C.WhsName, A.SAName, A.Remark, ");
            SQL.AppendLine("Case When DATEDIFF(A.DocDt, @DocDt ) BETWEEN @NoOfDayDOCt6ToChoose * -1 And 0 then 'Y'  ");
            SQL.AppendLine("Else 'N' END PassedInd, E.Amt As TotalAmount ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCustomerCategory D On B.CtCtCode = D.CtCtCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T2.docno, SUM(T1.Qty*T1.UPrice) as Amt ");
            SQL.AppendLine("    FROM tbldoctdtl T1 ");
            SQL.AppendLine("    INNER JOIN tbldocthdr T2 ON T2.Docno = T1.DocNo ");
            SQL.AppendLine("    GROUP BY  T1.docno ");
            SQL.AppendLine(") E On E.DocNo = A.Docno ");
            SQL.AppendLine("Where A.Status='A' And A.ProcessInd='F' ");
            if(Sm.GetLue(LueYr).Length > 0)
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            if (Sm.GetLue(LueMth).Length > 0)
                SQL.AppendLine("And Substring(A.DocDt, 5, 2)=@Mth ");
            SQL.AppendLine("And A.WhsCode=@WhsCode ");
            if (mFrmParent.mIsCopyingDOUseActiveDoc)
            {
                SQL.AppendLine("And A.DocNo In ( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T1.Status='A' ");
                SQL.AppendLine("    And T2.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("And A.DocNo not In ( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T1.Status='A' ");
                SQL.AppendLine("    And T1.DOCtDocNoSource Is Not Null ");
                SQL.AppendLine("    And T2.CancelInd='Y' ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.DocDt Desc, A.DocNo;");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 9, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month")
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@NoOfDayDOCt6ToChoose", mFrmParent.mNoOfDayDOCt6ToChoose);
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(mFrmParent.DteDocDt));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), "B.CtCtCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[] 
                        { 
                            //0
                            "DocNo",
 
                            //1-5
                            "DocDt", "CtName",  "CtCtname", "WhsName", "SAName", 
                            //6-8
                            "Remark", "PassedInd", "TotalAmount"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Grd.Cells[Row, 2].Value = string.Empty;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;
            bool mIsDataNotPassed = false;

            if (Grd1.Rows.Count != 0)
            {
                mIsDataNotPassed = IsDataNotPassed();

                if (!mIsDataNotPassed)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                            mFrmParent.Grd1.Rows.Add();
                            Grd1.Rows.AutoHeight();
                        }
                    }
                }
            }

            if(!mFrmParent.mIsDOCt6UseBackDateOnly)
                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Document.");
            else
                if (!IsChoose && !mIsDataNotPassed) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Document.");
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            for (int r=0;r< mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 3), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        private bool IsDataNotPassed()
        {
            string mDocNoTemp = string.Empty;
            if (!mFrmParent.mIsDOCt6UseBackDateOnly) return false;
            
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10) == "N")
                        mDocNoTemp += Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;
                }
            }

            if (mDocNoTemp.Length > 0)
            {
                var s = new StringBuilder();
                s.AppendLine("You can't choose this document ");
                s.AppendLine(mDocNoTemp);
                Sm.StdMsg(mMsgType.Warning, s.ToString());
                return true;
            }
            

            return false;
            
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCt(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCt(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r< Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.ClearGrd(Grd1, false);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode),string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer Category");
        }
        #endregion

        
       

       

        #endregion

        
    }
}
