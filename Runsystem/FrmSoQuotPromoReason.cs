﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSoQuotPromoReason : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSoQuotPromoReasonFind FrmFind;

        #endregion

        #region Constructor

        public FrmSoQuotPromoReason(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRsnNo, TxtRsnDesc
                    }, true);
                    TxtRsnNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtRsnNo, TxtRsnDesc
                    }, false);
                    TxtRsnNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtRsnNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRsnDesc
                    }, false);
                    TxtRsnDesc.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtRsnNo, TxtRsnDesc
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSoQuotPromoReasonFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRsnNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRsnNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSoQuotPromoReason Where RsnNo=@RsnNo" };
                Sm.CmParam<String>(ref cm, "@RsnNo", TxtRsnNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSoQuotPromoReason(RsnNo, RsnDesc, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@RsnNo, @RsnDesc, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update RsnDesc=@RsnDesc, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@RsnNo", TxtRsnNo.Text);
                Sm.CmParam<String>(ref cm, "@RsnDesc", TxtRsnDesc.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtRsnNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string RsnNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RsnNo", RsnNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblSoQuotPromoReason Where RsnNo = @RsnNo",
                        new string[] 
                        {
                            "RsnNo", "RsnDesc"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtRsnNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtRsnDesc.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtRsnNo, "Reason Number", false) ||
                Sm.IsTxtEmpty(TxtRsnDesc, "Reason Description", false) ||
                IsEntCodeExisted();
        }

        private bool IsEntCodeExisted()
        {
            if (!TxtRsnNo.Properties.ReadOnly && Sm.IsDataExist("Select RsnNo From TblSoQuotPromoReason Where RsnNo ='" + TxtRsnNo.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Reason Number ( " + TxtRsnNo.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion
        #endregion

        #region Event
        private void TxtRsnNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRsnNo);
        }

        private void TxtRsnDesc_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRsnDesc);
        }
        #endregion

    }
}
