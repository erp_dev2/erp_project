﻿#region Update
/*
    02/01/2018 [HAR] info competence dihapus
    29/03/2018 [HAR] tambah infromasi type internal atau external dari option 
    05/09/2018 [HAR] hanya bisa lihat berdasarkan site dan yang site headernya kosong
    14/12/2018 [HAR] BUg : Filter training
    27/07/2020 [IBL/SIER] tambah data dengan site = all
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmTraining mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingFind(FrmTraining FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sl.SetLueTrainerType(ref LueTrainingType);
            SetSQL();
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Training"+Environment.NewLine+"Code", 
                        "Training"+Environment.NewLine+"Name",
                        "Active",
                        "Training Type",
                        "Site", 
                        
                        //6-10                        
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //11
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 }); 
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] {  6, 7, 8, 9, 10, 11}, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrainingCode, A.TrainingName, A.ActiveInd, B.OptDesc As trainingtype, ");
            SQL.AppendLine("Case ");
	        SQL.AppendLine("    When A.SiteCode = 'all' Then 'ALL' ");
	        SQL.AppendLine("    Else C.SiteName ");
            SQL.AppendLine("End ");
            SQL.AppendLine("As SiteName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblTraining A ");
            SQL.AppendLine("Inner Join Tbloption B On A.TrainingType = B.OptCode And B.OptCat='TrainerType' ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode = C.SiteCode ");
            SQL.AppendLine("Where 0=0 ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And ( ");
		        SQL.AppendLine(" (A.sitecode is null) ");
		        if(mFrmParent.mIsTrainingUseAllSite)
                    SQL.AppendLine(" Or (A.sitecode Is Not Null And A.SiteCode = 'all') ");
		        SQL.AppendLine(" Or (A.SiteCode Is Not Null ");
		        SQL.AppendLine("    And Exists( ");
		        SQL.AppendLine("        Select 1 From TblGroupSite ");
		        SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
		        SQL.AppendLine("        And GrpCode In ( ");
		        SQL.AppendLine("            Select GrpCode From TblUser ");
		        SQL.AppendLine("            Where UserCode=@UserCode ");
		        SQL.AppendLine("        ) ");
		        SQL.AppendLine("    ) ");
		        SQL.AppendLine("  ) ");
                SQL.AppendLine(") ");
            }

           
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                var SQL2 = new StringBuilder();

                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtTrainingName.Text, new string[] { "A.TrainingCode", "A.TrainingName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTrainingType), "A.TrainingType", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.TrainingName",
                        new string[]
                        {
                            //0
                            "TrainingCode", 
                                
                            //1-5
                            "TrainingName", "ActiveInd", "trainingtype", "SiteName", "CreateBy",  
                            
                            //6-8
                           "CreateDt", "LastUpBy",  "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 11, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtTrainingName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTrainingName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Training");
        }

        private void ChkTrainingType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueTrainingType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingType, new Sm.RefreshLue1(Sl.SetLueTrainerType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        
        #endregion

       
    }
}
