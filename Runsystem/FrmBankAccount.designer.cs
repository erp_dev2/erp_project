﻿namespace RunSystem
{
    partial class FrmBankAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBankAccount));
            this.TxtBankAcCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtBankAcTp = new DevExpress.XtraEditors.TextEdit();
            this.lblBankAccountType = new System.Windows.Forms.Label();
            this.TxtBankAcNm = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueCoaAcNo = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAutoNoDebit = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.AutoNoCredit = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSequence = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkHiddenInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtBranchAcNm = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeBranchAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtBranchPhone = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtBranchFax = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtBranchSwiftCode = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.lblSite = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCOACostCenter = new System.Windows.Forms.Label();
            this.TxtCCCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtCCName = new DevExpress.XtraEditors.TextEdit();
            this.BtnCCCode = new DevExpress.XtraEditors.SimpleButton();
            this.LblInterOffice = new System.Windows.Forms.Label();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnBankAcTp = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBankAcTpCode = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCoaAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAutoNoDebit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoNoCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSequence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHiddenInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchAcNm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBranchAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchSwiftCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 445);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 100);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 80);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnSave.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 60);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 40);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 20);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnFind.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 120);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 20);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnBankAcTp);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblInterOffice);
            this.panel2.Controls.Add(this.BtnCCCode);
            this.panel2.Controls.Add(this.TxtCCName);
            this.panel2.Controls.Add(this.TxtCCCode);
            this.panel2.Controls.Add(this.LblCOACostCenter);
            this.panel2.Controls.Add(this.lblSite);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.LueEntCode);
            this.panel2.Controls.Add(this.TxtBranchSwiftCode);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtBranchFax);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtBranchPhone);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.MeeBranchAddress);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtBranchAcNm);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.ChkHiddenInd);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.TxtSequence);
            this.panel2.Controls.Add(this.LueBankCode);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueCurCode);
            this.panel2.Controls.Add(this.AutoNoCredit);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtAutoNoDebit);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueCoaAcNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtBankAcNm);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtBankAcTp);
            this.panel2.Controls.Add(this.lblBankAccountType);
            this.panel2.Controls.Add(this.TxtBankAcNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtBankAcCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtBankAcTpCode);
            this.panel2.Size = new System.Drawing.Size(772, 445);
            // 
            // TxtBankAcCode
            // 
            this.TxtBankAcCode.EnterMoveNextControl = true;
            this.TxtBankAcCode.Location = new System.Drawing.Point(188, 7);
            this.TxtBankAcCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBankAcCode.Name = "TxtBankAcCode";
            this.TxtBankAcCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcCode.Properties.MaxLength = 16;
            this.TxtBankAcCode.Size = new System.Drawing.Size(215, 20);
            this.TxtBankAcCode.TabIndex = 10;
            this.TxtBankAcCode.Validated += new System.EventHandler(this.TxtBankAcCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(96, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Account Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(148, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Bank";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(188, 51);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 80;
            this.TxtBankAcNo.Size = new System.Drawing.Size(381, 20);
            this.TxtBankAcNo.TabIndex = 16;
            this.TxtBankAcNo.Validated += new System.EventHandler(this.TxtBankAcNo_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(81, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Account Number";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcTp
            // 
            this.TxtBankAcTp.EnterMoveNextControl = true;
            this.TxtBankAcTp.Location = new System.Drawing.Point(188, 73);
            this.TxtBankAcTp.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBankAcTp.Name = "TxtBankAcTp";
            this.TxtBankAcTp.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcTp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcTp.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcTp.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcTp.Properties.MaxLength = 80;
            this.TxtBankAcTp.Size = new System.Drawing.Size(215, 20);
            this.TxtBankAcTp.TabIndex = 18;
            this.TxtBankAcTp.Validated += new System.EventHandler(this.TxtBankAcTp_Validated);
            // 
            // lblBankAccountType
            // 
            this.lblBankAccountType.AutoSize = true;
            this.lblBankAccountType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBankAccountType.ForeColor = System.Drawing.Color.Black;
            this.lblBankAccountType.Location = new System.Drawing.Point(96, 76);
            this.lblBankAccountType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblBankAccountType.Name = "lblBankAccountType";
            this.lblBankAccountType.Size = new System.Drawing.Size(85, 14);
            this.lblBankAccountType.TabIndex = 17;
            this.lblBankAccountType.Text = "Account Type";
            this.lblBankAccountType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNm
            // 
            this.TxtBankAcNm.EnterMoveNextControl = true;
            this.TxtBankAcNm.Location = new System.Drawing.Point(188, 95);
            this.TxtBankAcNm.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBankAcNm.Name = "TxtBankAcNm";
            this.TxtBankAcNm.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNm.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNm.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNm.Properties.MaxLength = 255;
            this.TxtBankAcNm.Size = new System.Drawing.Size(381, 20);
            this.TxtBankAcNm.TabIndex = 20;
            this.TxtBankAcNm.Validated += new System.EventHandler(this.TxtBankAcNm_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(93, 98);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Account Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(134, 381);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 45;
            this.label6.Text = "Remark";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(78, 120);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 14);
            this.label7.TabIndex = 21;
            this.label7.Text = "Chart Of Account";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCoaAcNo
            // 
            this.LueCoaAcNo.EnterMoveNextControl = true;
            this.LueCoaAcNo.Location = new System.Drawing.Point(188, 117);
            this.LueCoaAcNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueCoaAcNo.Name = "LueCoaAcNo";
            this.LueCoaAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCoaAcNo.Properties.Appearance.Options.UseFont = true;
            this.LueCoaAcNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCoaAcNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCoaAcNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCoaAcNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCoaAcNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCoaAcNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCoaAcNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCoaAcNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCoaAcNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCoaAcNo.Properties.DropDownRows = 25;
            this.LueCoaAcNo.Properties.MaxLength = 40;
            this.LueCoaAcNo.Properties.NullText = "[Empty]";
            this.LueCoaAcNo.Properties.PopupWidth = 500;
            this.LueCoaAcNo.Size = new System.Drawing.Size(456, 20);
            this.LueCoaAcNo.TabIndex = 22;
            this.LueCoaAcNo.ToolTip = "F4 : Show/hide list";
            this.LueCoaAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCoaAcNo.EditValueChanged += new System.EventHandler(this.LueCoaAcNo_EditValueChanged);
            // 
            // TxtAutoNoDebit
            // 
            this.TxtAutoNoDebit.EnterMoveNextControl = true;
            this.TxtAutoNoDebit.Location = new System.Drawing.Point(188, 161);
            this.TxtAutoNoDebit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAutoNoDebit.Name = "TxtAutoNoDebit";
            this.TxtAutoNoDebit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAutoNoDebit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAutoNoDebit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAutoNoDebit.Properties.Appearance.Options.UseFont = true;
            this.TxtAutoNoDebit.Properties.MaxLength = 3;
            this.TxtAutoNoDebit.Size = new System.Drawing.Size(59, 20);
            this.TxtAutoNoDebit.TabIndex = 26;
            this.TxtAutoNoDebit.Validated += new System.EventHandler(this.TxtAutoNoIn_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 164);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(164, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "Auto Number (Voucher - In)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AutoNoCredit
            // 
            this.AutoNoCredit.EnterMoveNextControl = true;
            this.AutoNoCredit.Location = new System.Drawing.Point(188, 183);
            this.AutoNoCredit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.AutoNoCredit.Name = "AutoNoCredit";
            this.AutoNoCredit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.AutoNoCredit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoNoCredit.Properties.Appearance.Options.UseBackColor = true;
            this.AutoNoCredit.Properties.Appearance.Options.UseFont = true;
            this.AutoNoCredit.Properties.MaxLength = 3;
            this.AutoNoCredit.Size = new System.Drawing.Size(59, 20);
            this.AutoNoCredit.TabIndex = 28;
            this.AutoNoCredit.Validated += new System.EventHandler(this.TxtAutoNoOut_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(7, 186);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(174, 14);
            this.label9.TabIndex = 27;
            this.label9.Text = "Auto Number (Voucher - Out)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(126, 142);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 14);
            this.label10.TabIndex = 23;
            this.label10.Text = "Currency";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(188, 139);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 25;
            this.LueCurCode.Properties.MaxLength = 3;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(215, 20);
            this.LueCurCode.TabIndex = 24;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(78, 208);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 14);
            this.label11.TabIndex = 29;
            this.label11.Text = "Report Sequence";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(188, 29);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 25;
            this.LueBankCode.Properties.MaxLength = 16;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 500;
            this.LueBankCode.Size = new System.Drawing.Size(381, 20);
            this.LueBankCode.TabIndex = 14;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // TxtSequence
            // 
            this.TxtSequence.EnterMoveNextControl = true;
            this.TxtSequence.Location = new System.Drawing.Point(188, 205);
            this.TxtSequence.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtSequence.Name = "TxtSequence";
            this.TxtSequence.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSequence.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSequence.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSequence.Properties.Appearance.Options.UseFont = true;
            this.TxtSequence.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSequence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSequence.Size = new System.Drawing.Size(59, 20);
            this.TxtSequence.TabIndex = 30;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(188, 379);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(381, 20);
            this.MeeRemark.TabIndex = 46;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // ChkHiddenInd
            // 
            this.ChkHiddenInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkHiddenInd.Location = new System.Drawing.Point(406, 6);
            this.ChkHiddenInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkHiddenInd.Name = "ChkHiddenInd";
            this.ChkHiddenInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkHiddenInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHiddenInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkHiddenInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkHiddenInd.Properties.Appearance.Options.UseFont = true;
            this.ChkHiddenInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHiddenInd.Properties.Caption = "Hidden";
            this.ChkHiddenInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHiddenInd.Size = new System.Drawing.Size(74, 22);
            this.ChkHiddenInd.TabIndex = 11;
            // 
            // TxtBranchAcNm
            // 
            this.TxtBranchAcNm.EnterMoveNextControl = true;
            this.TxtBranchAcNm.Location = new System.Drawing.Point(188, 227);
            this.TxtBranchAcNm.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBranchAcNm.Name = "TxtBranchAcNm";
            this.TxtBranchAcNm.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBranchAcNm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBranchAcNm.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBranchAcNm.Properties.Appearance.Options.UseFont = true;
            this.TxtBranchAcNm.Properties.MaxLength = 255;
            this.TxtBranchAcNm.Size = new System.Drawing.Size(381, 20);
            this.TxtBranchAcNm.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(52, 229);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 14);
            this.label12.TabIndex = 31;
            this.label12.Text = "Branch Account Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeBranchAddress
            // 
            this.MeeBranchAddress.EnterMoveNextControl = true;
            this.MeeBranchAddress.Location = new System.Drawing.Point(188, 249);
            this.MeeBranchAddress.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MeeBranchAddress.Name = "MeeBranchAddress";
            this.MeeBranchAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBranchAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeBranchAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBranchAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBranchAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBranchAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBranchAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBranchAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBranchAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBranchAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBranchAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBranchAddress.Properties.MaxLength = 400;
            this.MeeBranchAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeBranchAddress.Properties.ShowIcon = false;
            this.MeeBranchAddress.Size = new System.Drawing.Size(381, 20);
            this.MeeBranchAddress.TabIndex = 34;
            this.MeeBranchAddress.ToolTip = "F4 : Show/hide text";
            this.MeeBranchAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBranchAddress.ToolTipTitle = "Run System";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(90, 251);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 14);
            this.label13.TabIndex = 33;
            this.label13.Text = "Branch Address";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBranchPhone
            // 
            this.TxtBranchPhone.EnterMoveNextControl = true;
            this.TxtBranchPhone.Location = new System.Drawing.Point(188, 271);
            this.TxtBranchPhone.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBranchPhone.Name = "TxtBranchPhone";
            this.TxtBranchPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBranchPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBranchPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBranchPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtBranchPhone.Properties.MaxLength = 80;
            this.TxtBranchPhone.Size = new System.Drawing.Size(215, 20);
            this.TxtBranchPhone.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(98, 273);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 14);
            this.label14.TabIndex = 35;
            this.label14.Text = "Branch Phone";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBranchFax
            // 
            this.TxtBranchFax.EnterMoveNextControl = true;
            this.TxtBranchFax.Location = new System.Drawing.Point(188, 293);
            this.TxtBranchFax.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBranchFax.Name = "TxtBranchFax";
            this.TxtBranchFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBranchFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBranchFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBranchFax.Properties.Appearance.Options.UseFont = true;
            this.TxtBranchFax.Properties.MaxLength = 80;
            this.TxtBranchFax.Size = new System.Drawing.Size(215, 20);
            this.TxtBranchFax.TabIndex = 38;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(115, 295);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 14);
            this.label15.TabIndex = 37;
            this.label15.Text = "Branch Fax";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBranchSwiftCode
            // 
            this.TxtBranchSwiftCode.EnterMoveNextControl = true;
            this.TxtBranchSwiftCode.Location = new System.Drawing.Point(188, 315);
            this.TxtBranchSwiftCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtBranchSwiftCode.Name = "TxtBranchSwiftCode";
            this.TxtBranchSwiftCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBranchSwiftCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBranchSwiftCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBranchSwiftCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBranchSwiftCode.Properties.MaxLength = 11;
            this.TxtBranchSwiftCode.Size = new System.Drawing.Size(215, 20);
            this.TxtBranchSwiftCode.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(73, 317);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 14);
            this.label16.TabIndex = 39;
            this.label16.Text = "Branch Swift Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(140, 340);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 14);
            this.label17.TabIndex = 41;
            this.label17.Text = "Entity";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(188, 337);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.MaxLength = 40;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 500;
            this.LueEntCode.Size = new System.Drawing.Size(381, 20);
            this.LueEntCode.TabIndex = 42;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // lblSite
            // 
            this.lblSite.AutoSize = true;
            this.lblSite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite.ForeColor = System.Drawing.Color.Red;
            this.lblSite.Location = new System.Drawing.Point(148, 361);
            this.lblSite.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblSite.Name = "lblSite";
            this.lblSite.Size = new System.Drawing.Size(28, 14);
            this.lblSite.TabIndex = 43;
            this.lblSite.Text = "Site";
            this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(188, 358);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(381, 20);
            this.LueSiteCode.TabIndex = 44;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // LblCOACostCenter
            // 
            this.LblCOACostCenter.AutoSize = true;
            this.LblCOACostCenter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOACostCenter.ForeColor = System.Drawing.Color.Red;
            this.LblCOACostCenter.Location = new System.Drawing.Point(107, 403);
            this.LblCOACostCenter.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOACostCenter.Name = "LblCOACostCenter";
            this.LblCOACostCenter.Size = new System.Drawing.Size(72, 14);
            this.LblCOACostCenter.TabIndex = 47;
            this.LblCOACostCenter.Text = "Cost Center";
            this.LblCOACostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCCCode
            // 
            this.TxtCCCode.EnterMoveNextControl = true;
            this.TxtCCCode.Location = new System.Drawing.Point(188, 400);
            this.TxtCCCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCCCode.Name = "TxtCCCode";
            this.TxtCCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCode.Properties.MaxLength = 40;
            this.TxtCCCode.Properties.ReadOnly = true;
            this.TxtCCCode.Size = new System.Drawing.Size(152, 20);
            this.TxtCCCode.TabIndex = 48;
            // 
            // TxtCCName
            // 
            this.TxtCCName.EnterMoveNextControl = true;
            this.TxtCCName.Location = new System.Drawing.Point(341, 400);
            this.TxtCCName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCCName.Name = "TxtCCName";
            this.TxtCCName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCName.Properties.Appearance.Options.UseFont = true;
            this.TxtCCName.Properties.MaxLength = 80;
            this.TxtCCName.Properties.ReadOnly = true;
            this.TxtCCName.Size = new System.Drawing.Size(368, 20);
            this.TxtCCName.TabIndex = 49;
            // 
            // BtnCCCode
            // 
            this.BtnCCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCCCode.Appearance.Options.UseBackColor = true;
            this.BtnCCCode.Appearance.Options.UseFont = true;
            this.BtnCCCode.Appearance.Options.UseForeColor = true;
            this.BtnCCCode.Appearance.Options.UseTextOptions = true;
            this.BtnCCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCCCode.Image")));
            this.BtnCCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCCCode.Location = new System.Drawing.Point(708, 399);
            this.BtnCCCode.Name = "BtnCCCode";
            this.BtnCCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCCCode.TabIndex = 50;
            this.BtnCCCode.ToolTip = "Find COA\'s Account";
            this.BtnCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCCCode.ToolTipTitle = "Run System";
            this.BtnCCCode.Click += new System.EventHandler(this.BtnCCCode_Click);
            // 
            // LblInterOffice
            // 
            this.LblInterOffice.AutoSize = true;
            this.LblInterOffice.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInterOffice.ForeColor = System.Drawing.Color.Red;
            this.LblInterOffice.Location = new System.Drawing.Point(13, 423);
            this.LblInterOffice.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInterOffice.Name = "LblInterOffice";
            this.LblInterOffice.Size = new System.Drawing.Size(166, 14);
            this.LblInterOffice.TabIndex = 51;
            this.LblInterOffice.Text = "COA\'s Account (Inter Office)";
            this.LblInterOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(188, 420);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 40;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo.TabIndex = 52;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(341, 420);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc.TabIndex = 53;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(708, 419);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 54;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(475, 6);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActInd.TabIndex = 3;
            // 
            // BtnBankAcTp
            // 
            this.BtnBankAcTp.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBankAcTp.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBankAcTp.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBankAcTp.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBankAcTp.Appearance.Options.UseBackColor = true;
            this.BtnBankAcTp.Appearance.Options.UseFont = true;
            this.BtnBankAcTp.Appearance.Options.UseForeColor = true;
            this.BtnBankAcTp.Appearance.Options.UseTextOptions = true;
            this.BtnBankAcTp.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBankAcTp.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBankAcTp.Image = ((System.Drawing.Image)(resources.GetObject("BtnBankAcTp.Image")));
            this.BtnBankAcTp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBankAcTp.Location = new System.Drawing.Point(407, 71);
            this.BtnBankAcTp.Name = "BtnBankAcTp";
            this.BtnBankAcTp.Size = new System.Drawing.Size(24, 21);
            this.BtnBankAcTp.TabIndex = 55;
            this.BtnBankAcTp.ToolTip = "Find Bank Account Type";
            this.BtnBankAcTp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBankAcTp.ToolTipTitle = "Run System";
            this.BtnBankAcTp.Click += new System.EventHandler(this.BtnBankAcTp_Click);
            // 
            // TxtBankAcTpCode
            // 
            this.TxtBankAcTpCode.EnterMoveNextControl = true;
            this.TxtBankAcTpCode.Location = new System.Drawing.Point(439, 73);
            this.TxtBankAcTpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcTpCode.Name = "TxtBankAcTpCode";
            this.TxtBankAcTpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcTpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcTpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcTpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcTpCode.Properties.MaxLength = 80;
            this.TxtBankAcTpCode.Size = new System.Drawing.Size(111, 20);
            this.TxtBankAcTpCode.TabIndex = 56;
            this.TxtBankAcTpCode.Visible = false;
            // 
            // FrmBankAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 445);
            this.Name = "FrmBankAccount";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCoaAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAutoNoDebit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoNoCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSequence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHiddenInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchAcNm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBranchAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBranchSwiftCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTpCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNm;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcTp;
        private System.Windows.Forms.Label lblBankAccountType;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.TextEdit AutoNoCredit;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtAutoNoDebit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.LookUpEdit LueCoaAcNo;
        internal DevExpress.XtraEditors.LookUpEdit LueBankCode;
        internal DevExpress.XtraEditors.TextEdit TxtSequence;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.CheckEdit ChkHiddenInd;
        internal DevExpress.XtraEditors.TextEdit TxtBranchSwiftCode;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtBranchFax;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtBranchPhone;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeBranchAddress;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtBranchAcNm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private System.Windows.Forms.Label lblSite;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label LblCOACostCenter;
        internal DevExpress.XtraEditors.TextEdit TxtCCCode;
        internal DevExpress.XtraEditors.TextEdit TxtCCName;
        public DevExpress.XtraEditors.SimpleButton BtnCCCode;
        private System.Windows.Forms.Label LblInterOffice;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        public DevExpress.XtraEditors.SimpleButton BtnBankAcTp;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcTpCode;
    }
}