﻿#region Update
    // 01/08/2017 [WED] Tambah warning saat save, jika HM baru lebih dari 24 jam di hari yg sama dengan dokumen sebelumnya
    // 09/08/2017 [ari] Location berdasarkan Setingan Grup SITE
    // 26/01/2018 [HAR] Tambah inputan shift dialog ama historical
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.Globalization;

#endregion

namespace RunSystem
{
    public partial class FrmHoursMeter : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmHoursMeterFind FrmFind;
        private string
            mInitDt = string.Empty,
            mInsertedDt = string.Empty,
            mDecimalPointHoursMeter = string.Empty;

        internal bool mIsFilterBySite = false;

        private decimal
            mInitHoursMeter = 0m,
            mInsertedHoursMeter = 0m;

        #endregion

        #region Constructor

        public FrmHoursMeter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Hours Meter";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueLocCode(ref LueLocCode);
                SetLueShiftCode(ref LueShiftCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mDecimalPointHoursMeter = Sm.GetParameter("DecimalPointHoursMeter");
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Technical"+Environment.NewLine+"Object",
                        
                        //1-5
                        "Description",
                        "Location",
                        "Hours"+Environment.NewLine+"Meter",
                        "HM",
                        "HMDocDt",
                        //6-7
                        "HMdocno",
                        "Last HM",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        120, 
                        
                        //1-5
                        150, 120, 200, 50, 0,

                        //6-7
                        150, 100, 20
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4, 5, 6, 8 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      TxtDocNo, LueLocCode, DteDocDt, LueShiftCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    BtnRefresh.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueLocCode, DteDocDt, LueShiftCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 8 });
                    BtnRefresh.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3 });
                    break;
            }
        }

        private void ClearData()
        {
            mInitDt = string.Empty;
            mInsertedDt = string.Empty;
            mInitHoursMeter = 0m;
            mInsertedHoursMeter = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueLocCode, LueShiftCode
            });
            Sm.ClearGrd(Grd1, true);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmHoursMeterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueLocCode(ref LueLocCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Save Data

        #region Insert

        private void InsertData()
        {
            if ((Sm.StdMsgYN("Save", "") == DialogResult.No) || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "HoursMeter", "TblHoursMeter");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveHoursMeter(DocNo,Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsHoursMeterExceed24()
                ;
        }

        private bool IsHoursMeterExceed24()
        {
            bool flag = false;
            
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                mInsertedHoursMeter = Sm.GetGrdDec(Grd1, Row, 3);// Decimal.Parse(string.Concat(Sm.GetGrdDec(Grd1, Row, 3).ToString(), ".00"));
                mInitHoursMeter = Sm.GetGrdDec(Grd1, Row, 4);
                mInitDt = Sm.GetGrdStr(Grd1, Row, 5);
                mInsertedDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

                if (mInitDt == mInsertedDt)
                {
                    if (mInsertedHoursMeter - mInitHoursMeter <= 24)
                    {
                        flag = false;
                    }
                    else
                    {
                        // kalau HoursMeter awal selisihnya lebih dari 24 jam dari Hours Meter yg di save, muncul warning

                        if (Sm.StdMsgYN("Question",
                            "Description                    : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Initial Hours Meter       : " + mInitHoursMeter + Environment.NewLine +
                            "Inserted Hours Meter   : " + mInsertedHoursMeter + Environment.NewLine + Environment.NewLine +
                            "Inserted Hours Meter exceeds more than 24 hours from Initial Hours Meter." + Environment.NewLine +
                            "Do you want to continue to save this data ?"
                            ) == DialogResult.No)
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            //break;
                        }
                    }
                }
                else
                    flag = false;
            }

            return flag;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                decimal a = Sm.GetGrdDec(Grd1, Row, 3);
                decimal b = Sm.GetGrdDec(Grd1, Row, 4);
                if (a > 0)
                {
                    if ( a < b )
                    {
                        Sm.StdMsg(mMsgType.Warning, "New hours meter on '"+Sm.GetGrdStr(Grd1, Row, 1)+"' should be bigger than initial hour meter.");
                        Sm.FocusGrd(Grd1, Row, 3);
                        return true;
                    }
                }
                    
             }

            return false;
        }

        private bool IsGrdHMNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    string HM = Sm.GetGrdStr(Grd1, Row, 3);
                    int index = HM.IndexOf(".");
                    
                    string HMAfter = HM.Substring(index, HM.Length);
                    char[] result = new char[HMAfter.Length];
                    for (int i = 0; i < HMAfter.Length; i++)
                    {
                        if (HMAfter[i] != ',')
                        {
                            result[index++] = HMAfter[i];
                        }
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail Hours Meter.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveHoursMeter(string DocNo, int Row)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblHoursMeter(DocNo, DocDt, TOCode, HoursMeter, ShiftCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @TOCode, @HoursMeter, @ShiftCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("    On Duplicate Key Update ShiftCode=@ShiftCode, HoursMeter = @HoursMeter, LastUpBy =@UserCode, LastUpDt = CurrentDateTime() ; ");

            SQL.AppendLine("Update TblTOHdr set ");
            SQL.AppendLine("    HMDocNo=@HMDocNo ");
            SQL.AppendLine("Where AssetCode=@AssetCode");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@HMDocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TOCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@HoursMeter", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ShiftCode", Sm.GetLue(LueShiftCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }
      
        #endregion

        #endregion
        
        #region ShowData

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowHoursMeterHdr(DocNo);
                ShowHoursMeterDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowHoursMeterHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, '' As LocCode, A.ShiftCode  ");
            SQL.AppendLine("From TblHoursMeter A ");
            SQL.AppendLine("Inner Join TblToHdr B On A.TOCode=B.AssetCode ");
            SQL.AppendLine("Inner Join TblLocation C On B.LocCode=C.LocCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-2
                        "DocDt", "LocCode", "ShiftCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueLocCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueShiftCode, Sm.DrStr(dr, c[3]));
                    }, true
                );
        }

        private void ShowHoursMeterDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ToCode, D.AssetName, C.LocName, A.HoursMeter ");
                SQL.AppendLine("From TblHoursMeter A ");
                SQL.AppendLine("Inner Join TblToHdr B On A.ToCode =B.AssetCode ");
                SQL.AppendLine("Inner Join TblLocation C On B.LocCode=C.LocCode ");
                SQL.AppendLine("Inner Join TblAsset D On A.TOCode=D.AssetCode");
                SQL.AppendLine("Where A.DocNo=@DocNo");
                SQL.AppendLine("Order By C.LocName, D.AssetName ;");
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "ToCode", 
                        
                        "AssetName", "LocName", "HoursMeter"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                    }, false, false, true, false
                     );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Additional Methods

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueShiftCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
            SQL.AppendLine("From TblOption Where OptCat = 'Shift' ");
            SQL.AppendLine("Order By OptCode ;");

            cm.CommandText = SQL.ToString();
           
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Shift", "Col2", "Col1");
        }

        private void ReListDataHM()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string Filter = string.Empty;
            string HMDate = string.Empty;
           

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    string TOCode = Sm.GetGrdStr(Grd1, Row, 0);
                    HMDate = Sm.GetDte(DteDocDt).Substring(0, 8);
                    if (TOCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.TOCode=@TOCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@TOCode" + No.ToString(), TOCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.DocNo, A.ToCOde, A.HoursMeter ");
            SQL.AppendLine("From TblHoursmeter A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.TOCode, ");
            SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo)) As Key1  ");
            SQL.AppendLine("    From TblHoursmeter T1 ");
            if(HMDate.Length>0)
                SQL.AppendLine("    Where Concat(T1.DocDt, ShiftCode) < "+HMDate+Sm.GetLue(LueShiftCode)+" ");
            SQL.AppendLine("    Group By T1.TOCode ");
            SQL.AppendLine("    ) B On Concat(A.DocDt, A.DocNo)=B.Key1 And A.ToCode=B.TOCode ");
            SQL.AppendLine("Where 0=0   ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-2
                    "toCode", "HoursMeter"
                });
                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0) == Sm.DrStr(dr, 1))
                            {
                                Grd1.Cells[Row, 6].Value = Sm.DrStr(dr, 0);
                                Grd1.Cells[Row, 7].Value = Sm.DrStr(dr, 2);
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }
  


        #endregion

        #endregion
       
        #region Event

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {

            Grd1.ReadOnly = false;

            Cursor.Current = Cursors.WaitCursor;
            string Filter = string.Empty;

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetLue(LueLocCode));

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "B.LocCode", true);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select B.AssetCode, D.AssetName, C.LocName, IfNull(E.HoursMeter,'0') As HoursMeter, IfNull(E.DocDt, '') As HMDocDt ");
            SQL.AppendLine("From TblToHdr B  ");
            SQL.AppendLine("Inner Join TblLocation C On B.LocCode=C.LocCode ");
            SQL.AppendLine("Inner Join TblAsset D On B.AssetCode=D.AssetCode");
            SQL.AppendLine("Left Join TblHoursmeter E On B.HMDocNo=E.DocNo And B.AssetCode=E.TOCode ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + Filter + "Order by C.LocName, D.AssetName",
                new string[] 
                    { 
                        "AssetCode", 
                        
                        "AssetName", "LocName", "HoursMeter" ,"HMDocDt"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                }, false, false, true, false
                 );
            Sm.FocusGrd(Grd1, 0, 1);
            ReListDataHM();
        }



        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            //if (e.ColIndex == 3)
            //{
            //    if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
            //    {
            //        //if (mDecimalPointHoursMeter.Length>0 && mDecimalPointHoursMeter == "Comma")
            //        //{
            //        //    //System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("is-IS");
            //        //    //decimal a = Decimal.Parse(Sm.GetGrdStr(Grd1, e.RowIndex, 3));
            //        //    //Grd1.Cells[e.RowIndex, 3].Value = (a).ToString("N", new CultureInfo("is-IS"));
            //        //}
            //    }
            //}
        }

        private void LueShiftCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShiftCode, new Sm.RefreshLue1(SetLueShiftCode));
        }


        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmHoursMeterDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 0), Sm.GetDte(DteDocDt).Substring(0, 8));
                f1.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmHoursMeterDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 0), Sm.GetDte(DteDocDt).Substring(0, 8));
                f1.ShowDialog();
            }
        }
        #endregion

    }

 }
   
