﻿namespace RunSystem
{
    partial class FrmMaintenanceSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.LueLocCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnCopyPattern = new DevExpress.XtraEditors.SimpleButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnCopyData = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnClearData = new DevExpress.XtraEditors.SimpleButton();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.LueDocNo = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnExcel = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTOCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkTOCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTOCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTOCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnExcel);
            this.panel1.Location = new System.Drawing.Point(912, 0);
            this.panel1.Size = new System.Drawing.Size(70, 509);
            this.panel1.TabIndex = 0;
            this.panel1.Controls.SetChildIndex(this.BtnRefresh, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.BtnExcel, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 467);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtTOCode);
            this.panel2.Controls.Add(this.ChkTOCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueLocCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(912, 75);
            this.panel2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(50, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 14);
            this.label5.TabIndex = 13;
            this.label5.Text = "Location";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLocCode
            // 
            this.LueLocCode.EnterMoveNextControl = true;
            this.LueLocCode.Location = new System.Drawing.Point(106, 26);
            this.LueLocCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLocCode.Name = "LueLocCode";
            this.LueLocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.Appearance.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLocCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLocCode.Properties.DropDownRows = 30;
            this.LueLocCode.Properties.NullText = "[Empty]";
            this.LueLocCode.Properties.PopupWidth = 300;
            this.LueLocCode.Size = new System.Drawing.Size(287, 20);
            this.LueLocCode.TabIndex = 14;
            this.LueLocCode.ToolTip = "F4 : Show/hide list";
            this.LueLocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLocCode.EditValueChanged += new System.EventHandler(this.LueLocCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(215, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 11;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(62, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Period";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(228, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt2.TabIndex = 12;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(106, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt1.TabIndex = 10;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(818, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(94, 75);
            this.panel4.TabIndex = 14;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.BtnCopyPattern);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, -6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(94, 27);
            this.panel6.TabIndex = 33;
            // 
            // BtnCopyPattern
            // 
            this.BtnCopyPattern.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyPattern.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyPattern.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyPattern.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.BtnCopyPattern.Appearance.Options.UseBackColor = true;
            this.BtnCopyPattern.Appearance.Options.UseFont = true;
            this.BtnCopyPattern.Appearance.Options.UseForeColor = true;
            this.BtnCopyPattern.Appearance.Options.UseTextOptions = true;
            this.BtnCopyPattern.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyPattern.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnCopyPattern.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyPattern.Location = new System.Drawing.Point(0, 0);
            this.BtnCopyPattern.Name = "BtnCopyPattern";
            this.BtnCopyPattern.Size = new System.Drawing.Size(94, 27);
            this.BtnCopyPattern.TabIndex = 15;
            this.BtnCopyPattern.Text = "Copy Pattern";
            this.BtnCopyPattern.ToolTip = "Copy Pattern (First Record Only)";
            this.BtnCopyPattern.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyPattern.ToolTipTitle = "Run System";
            this.BtnCopyPattern.Click += new System.EventHandler(this.BtnCopyPattern_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BtnCopyData);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 21);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(94, 27);
            this.panel5.TabIndex = 33;
            // 
            // BtnCopyData
            // 
            this.BtnCopyData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyData.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnCopyData.Appearance.Options.UseBackColor = true;
            this.BtnCopyData.Appearance.Options.UseFont = true;
            this.BtnCopyData.Appearance.Options.UseForeColor = true;
            this.BtnCopyData.Appearance.Options.UseTextOptions = true;
            this.BtnCopyData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnCopyData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyData.Location = new System.Drawing.Point(0, 0);
            this.BtnCopyData.Name = "BtnCopyData";
            this.BtnCopyData.Size = new System.Drawing.Size(94, 27);
            this.BtnCopyData.TabIndex = 16;
            this.BtnCopyData.Text = "Copy Data";
            this.BtnCopyData.ToolTip = "Copy Data Based On First Record";
            this.BtnCopyData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyData.ToolTipTitle = "Run System";
            this.BtnCopyData.Click += new System.EventHandler(this.BtnCopyData_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BtnClearData);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(94, 27);
            this.panel3.TabIndex = 32;
            // 
            // BtnClearData
            // 
            this.BtnClearData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnClearData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnClearData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClearData.Appearance.ForeColor = System.Drawing.Color.Red;
            this.BtnClearData.Appearance.Options.UseBackColor = true;
            this.BtnClearData.Appearance.Options.UseFont = true;
            this.BtnClearData.Appearance.Options.UseForeColor = true;
            this.BtnClearData.Appearance.Options.UseTextOptions = true;
            this.BtnClearData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnClearData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnClearData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnClearData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnClearData.Location = new System.Drawing.Point(0, 0);
            this.BtnClearData.Name = "BtnClearData";
            this.BtnClearData.Size = new System.Drawing.Size(94, 27);
            this.BtnClearData.TabIndex = 17;
            this.BtnClearData.Text = "Clear Schedule";
            this.BtnClearData.ToolTip = "Clear Schedule";
            this.BtnClearData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnClearData.ToolTipTitle = "Run System";
            this.BtnClearData.Click += new System.EventHandler(this.BtnClearData_Click);
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 75);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(912, 434);
            this.Grd1.TabIndex = 18;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // LueDocNo
            // 
            this.LueDocNo.EnterMoveNextControl = true;
            this.LueDocNo.Location = new System.Drawing.Point(92, 96);
            this.LueDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueDocNo.Name = "LueDocNo";
            this.LueDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocNo.Properties.Appearance.Options.UseFont = true;
            this.LueDocNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDocNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDocNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDocNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDocNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDocNo.Properties.DropDownRows = 30;
            this.LueDocNo.Properties.NullText = "[Empty]";
            this.LueDocNo.Properties.PopupWidth = 220;
            this.LueDocNo.Size = new System.Drawing.Size(220, 20);
            this.LueDocNo.TabIndex = 19;
            this.LueDocNo.ToolTip = "F4 : Show/hide list";
            this.LueDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDocNo.EditValueChanged += new System.EventHandler(this.LueDocNo_EditValueChanged);
            this.LueDocNo.Leave += new System.EventHandler(this.LueDocNo_Leave);
            this.LueDocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDocNo_KeyDown);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnExcel.Location = new System.Drawing.Point(0, 62);
            this.BtnExcel.Name = "BtnExcel";
            this.BtnExcel.Size = new System.Drawing.Size(70, 31);
            this.BtnExcel.TabIndex = 8;
            this.BtnExcel.Text = "  &Excel";
            this.BtnExcel.ToolTip = "Export To Excel (Alt-E)";
            this.BtnExcel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExcel.ToolTipTitle = "Run System";
            this.BtnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Technical Object";
            // 
            // TxtTOCode
            // 
            this.TxtTOCode.EnterMoveNextControl = true;
            this.TxtTOCode.Location = new System.Drawing.Point(106, 48);
            this.TxtTOCode.Name = "TxtTOCode";
            this.TxtTOCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTOCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTOCode.Properties.MaxLength = 40;
            this.TxtTOCode.Size = new System.Drawing.Size(287, 20);
            this.TxtTOCode.TabIndex = 16;
            this.TxtTOCode.Validated += new System.EventHandler(this.TxtTOCode_Validated);
            // 
            // ChkTOCode
            // 
            this.ChkTOCode.Location = new System.Drawing.Point(396, 47);
            this.ChkTOCode.Name = "ChkTOCode";
            this.ChkTOCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTOCode.Properties.Appearance.Options.UseFont = true;
            this.ChkTOCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTOCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTOCode.Properties.Caption = " ";
            this.ChkTOCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTOCode.Size = new System.Drawing.Size(19, 22);
            this.ChkTOCode.TabIndex = 17;
            this.ChkTOCode.ToolTip = "Remove filter";
            this.ChkTOCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTOCode.ToolTipTitle = "Run System";
            this.ChkTOCode.CheckedChanged += new System.EventHandler(this.ChkTOCode_CheckedChanged);
            // 
            // FrmMaintenanceSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 509);
            this.Controls.Add(this.LueDocNo);
            this.Controls.Add(this.Grd1);
            this.Controls.Add(this.panel2);
            this.Name = "FrmMaintenanceSchedule";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.Grd1, 0);
            this.Controls.SetChildIndex(this.LueDocNo, 0);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTOCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTOCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueLocCode;
        private DevExpress.XtraEditors.LookUpEdit LueDocNo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnCopyPattern;
        private System.Windows.Forms.Panel panel5;
        public DevExpress.XtraEditors.SimpleButton BtnCopyData;
        private System.Windows.Forms.Panel panel3;
        public DevExpress.XtraEditors.SimpleButton BtnClearData;
        protected DevExpress.XtraEditors.SimpleButton BtnExcel;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtTOCode;
        private DevExpress.XtraEditors.CheckEdit ChkTOCode;

    }
}