﻿#region Update
/*
    18/04/2019 [TKG] bug ambil data dari Recv
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTransferRequestWhs2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private int mNumberOfInventoryUomCode = 1;
        private bool mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptTransferRequestWhs2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        # region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                GetParameter();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.DNo, F.ItCode, I.ItGrpName, F.ItName, F.ForeignName, G.WhsName As WhsFrom, H.WhsName As WhsTo, B.BatchNo, ");
            SQL.AppendLine("B.Qty, F.InventoryUomCode, B.Qty2, F.InventoryUomCode2, B.Qty3, F.InventoryUomCode3, D.DocNo As DODocNo, D.Qty As QtyDo, ");
            SQL.AppendLine("D.Qty2 As Qty2Do, D.Qty3 As Qty3Do, ");
            SQL.AppendLine("IfNull(E.DocNo, J.DocNo) As RecvDocNo, ");
            SQL.AppendLine("IfNull(E.Qty, IfNull(J.Qty, 0.00)) As QtyRecv, ");
            SQL.AppendLine("IfNull(E.Qty2, IfNull(J.Qty2, 0.00)) As Qty2Recv, ");
            SQL.AppendLine("IfNull(E.Qty3, IfNull(J.Qty3, 0.00)) As Qty3Recv, ");
            SQL.AppendLine("B.Remark ");
            SQL.AppendLine("From TblTransferRequestWhs2Hdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestWhs2dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDOwhshdr C On A.DocNo=C.TransferRequestWhs2DocNo ");
            SQL.AppendLine("Left Join TblDOwhsdtl D On C.DocNo=D.DocNo And B.DNo=D.TransferrequestWhs2DNo ");
            SQL.AppendLine("Left Join TblRecvWhs4dtl E On D.DocNo=E.DoWhsDocNo And D.DNo=E.DoWhsDNo ");
            SQL.AppendLine("Inner Join TblItem F On B.ItCode=F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblWarehouse G On A.WhsCode = G.WhsCode ");
            SQL.AppendLine("Left Join TblWarehouse H On A.WhsCode2 = H.WhsCode ");
            SQL.AppendLine("Left Join tblitemgroup I On F.ItGrpCode=I.ItGrpCode ");
            SQL.AppendLine("Left Join TblRecvWhsDtl J On D.DocNo=J.DOWhsDocNo And D.DNo=J.DOWhsDNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Requested#",
                    "Date",
                    "DNo",
                    "From",
                    "To",
                   
                    //6-10
                    "Item's Code",
                    "Item's Name",
                    "Group",
                    "Foreign Name",
                    "Batch#",
                    
                    //11-15
                    "Requested"+Environment.NewLine+"Quantity",
                    "UoM",
                    "Requested"+Environment.NewLine+"Quantity(2)",
                    "UoM",
                    "Requested"+Environment.NewLine+"Quantity(3)",

                    //16-20
                    "UoM",
                    "DO#",
                    "Do"+Environment.NewLine+"Quantity",
                    "UoM",
                    "Do"+Environment.NewLine+"Quantity(2)",
                   
                    //21-25
                    "UoM",
                    "Do"+Environment.NewLine+"Quantity(3)",
                    "UoM",
                    "Received#",
                    "Received"+Environment.NewLine+"Quantity",
                    
                    //26-30
                    "UoM",
                    "Received"+Environment.NewLine+"Quantity(2)",
                    "UoM",
                    "Received"+Environment.NewLine+"Quantity(3)",
                    "UoM",

                    //31
                    "Remark",
                   
                },
                new int[] 
                {
                    //0
                    30,

                    //1-5
                    140, 100, 60, 250, 250,  
                    
                    //6-10
                    100, 250, 150, 200, 270, 

                    //11-15
                    100, 60, 100, 60, 100, 
                    
                    //16-20
                    60, 150, 100, 60, 100, 
                    
                    //21-25
                    60, 100, 60, 150, 100,  
                    
                    //26-30
                    60, 100, 60, 100, 60, 
                    
                    //31
                    170

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 18, 20, 22, 25, 27, 29 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 8, 9, 13, 14, 15, 16, 20, 21, 22, 23, 27, 28, 29, 30 }, false);
             
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "F.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DNo", "WhsFrom", "WhsTo", "ItCode", 
                            
                            //6-10
                            "Itname", "ItGrpName", "ForeignName", "BatchNo", "Qty",  
                            
                            //11-15
                            "Qty2", "Qty3", "DODocNo", "QtyDO", "Qty2DO", 
                            
                            //16-20
                            "Qty3DO", "RecvDocNo", "QtyRecv", "Qty2Recv", "Qty3Recv",
 
                            //21-24
                            "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13); 
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);

                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(7);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 20, 22, 25, 27, 29 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] 
                { 
                    13, 14, 20, 21, 27, 28
                }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] 
                    { 
                        13, 14, 15, 16, 20, 21, 
                        22, 23, 27, 28, 29, 30
                    }, true);
            }
        }

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
