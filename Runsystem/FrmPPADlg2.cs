﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPPADlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPPA mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmPPADlg2(FrmPPA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                SetLueGrpLevelCode(ref LueGrpLevelCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select GrdLvlCode, grdLvlName, GrdLvlGrpName ");
            SQL.AppendLine("From TblGradeLevelhdr A ");
            SQL.AppendLine("Inner Join TblGradeLevelGroup B On A.GrdLvlGrpCode = B.GrdLvlGrpCode ");
               
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Level Code",
                        "Level Name",
                        "Group Grade Level"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty; ;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedDocNo", mFrmParent.GetSelectedGrdLevel());

                Sm.FilterStr(ref Filter, ref cm, TxtGrdLevel.Text, new string[]{"A.GrdLvlCode", "A.GrdLvlName"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGrpLevelCode), "A.GrdLvlGrpCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By GrdLvlCode ;",
                        new string[] 
                        { 
                            //0
                            "GrdLvlCode", 
                            
                            //1-5
                           "GrdLvlName", "GrdLvlGrpName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 grade level.");
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string GrdCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(GrdCode, Sm.GetGrdStr(mFrmParent.Grd1, Index, 2)))
                    return true;
            return false;
        }

        #endregion

        #region Additional Method
        public static void SetLueGrpLevelCode(ref LookUpEdit Lue)
        { 
            Sm.SetLue2(
                ref Lue,
                "Select GrdLvlGrpCode As Col1, GrdLvlGrpName As Col2 from TblGradeLevelGroup",
                0, 35, false, true, "Code", "Publisher Name", "Col2", "Col1");
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd1, 0, 2).Length != 0)
                {
                    var Act = Sm.GetGrdStr(Grd1, 0, 2);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0) Grd1.Cells[Row,2].Value = Act;
                }
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkGrdLevel_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Grade");
        }

        private void TxtGrdLevel_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGrpLevelCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        private void LueGrpLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrpLevelCode, new Sm.RefreshLue1(SetLueGrpLevelCode));
        }
        #endregion

       

       
    }
}
