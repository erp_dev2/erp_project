﻿#region Update
/*
    18/12/2019 [DITA+HAR/IMS] new apps
    18/12/2019 [HAR+VIN+DITA/IMS] printout otslip
    */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTSlip : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mDeptCode = string.Empty;
        private string 
            mIsFormPrintOutPayslip = string.Empty,
            mCompany = string.Empty,
            mAddress = string.Empty,
            mPhone = string.Empty,
            mUserName = string.Empty,
            mEmpSystemTypeBorongan = string.Empty,
            mEmpCodeSincerely = string.Empty;
        private bool mIsNotFilterByAuthorization = false;
      
        #endregion

        #region Constructor

        public FrmOTSlip(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Pay Slip";
                GetParameter();
                mUserName = Sm.GetValue("Select UserName From TblUser Where UserCode=@Param;", Gv.CurrentUserCode);
                SetGrd();
                BtnPayrunCode_Click(sender, e);
                BtnSave.Enabled = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mIsFormPrintOutPayslip = Sm.GetParameter("FormPrintOutPayslip");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mCompany = Sm.GetParameter("ReportTitle1");
            mAddress = Sm.GetParameter("ReportTitle2");
            mPhone = Sm.GetParameter("ReportTitle4");
            mEmpCodeSincerely = Sm.GetParameter("EmpCodeSincerely");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",
                        
                        
                        //6-7
                        "Join Date",
                        "Resign Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        100, 250, 100, 150, 150, 
                        
                        //6-7
                        100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtPayrunCode, TxtPayrunPeriod, TxtPayrunName, TxtDeptName, TxtSiteName, 
                  DteStartDt, DteEndDt, LueAGCode 
            });
            Sm.ClearGrd(Grd1, true);
        }

        private void ShowPayrunInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            //if (Sm.GetLue(LueAGCode).Length > 0 &&
            //    !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
            //    SQL.AppendLine("Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And F.AGCode=@AGCode ");
            SQL.AppendLine("Where A.EmpCode In ( ");
            SQL.AppendLine("    Select EmpCode From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=@PayrunCode ");
            //SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Not Null ");
            SQL.AppendLine(") ");
            //if (
            //    Sm.GetLue(LueAGCode).Length > 0 &&
            //    Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup")
            //    )
            //{
            //    SQL.AppendLine("And ( ");
            //    SQL.AppendLine("    A.EmpCode Not In ( ");
            //    SQL.AppendLine("        Select B.EmpCode ");
            //    SQL.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
            //    SQL.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
            //    SQL.AppendLine("        ) ");
            //    if (mDeptCode.Length != 0)
            //    {
            //        SQL.AppendLine("Or ");
            //        SQL.AppendLine("    A.EmpCode In ( ");
            //        SQL.AppendLine("        Select B.EmpCode ");
            //        SQL.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
            //        SQL.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
            //        SQL.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
            //        SQL.AppendLine("        ) ");
            //    }
            //    SQL.AppendLine(") ");
                
            //}
            //if (!mIsNotFilterByAuthorization)
            //{
            //    SQL.AppendLine("And Exists( ");
            //    SQL.AppendLine("    Select 1 From TblEmployee ");
            //    SQL.AppendLine("    Where EmpCode=A.EmpCode ");
            //    SQL.AppendLine("    And GrdLvlCode In ( ");
            //    SQL.AppendLine("        Select T2.GrdLvlCode ");
            //    SQL.AppendLine("        From TblPPAHdr T1 ");
            //    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
            //    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine(") ");
            //}

            SQL.AppendLine("Order By A.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            if (Sm.GetLue(LueAGCode).Length > 0)
                Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            if (mDeptCode.Length != 0)
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);

            int i = 0;
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt", 
                    
                    //6
                    "ResignDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = ++i;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                }, true, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtPayrunCode.Text.Length > 0) ParPrint(TxtPayrunCode.Text);
        }

        #endregion

        #region Additional Method

        private void ParPrint(string Payrun)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string PayslipPrintBy = 
                "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", 
                Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));

                var l = new List<OTSlip>();
                string[] TableName = { "OTSlip" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();


                var SQL = new StringBuilder();

               
               SQL.AppendLine("Select @CompanyLogo As CompanyLogo, ");
               SQL.AppendLine("A.payrunCode, A.EmpCode, B.EmpCodeold, B.Empname, C.Deptname, E.bankName, ");
               SQL.AppendLine("B.BankAcNo, Left(D.StartDt, 4) As Yr,  DATE_FORMAT(D.StartDt, '%M') AS Mth, ");
               SQL.AppendLine("H.OThr, G.ParValue As OtAmtHr, H.Otamt, I.OTHolidayhr, F.ParValue As OtAmtHolidayhr, I.OtHolidayamt ");
               SQL.AppendLine("from Tblpayrollprocess1 A ");
               SQL.AppendLine("Inner Join TblEmpLoyee B On A.EmpCode = B.EmpCode ");
               SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode  = C.DeptCode ");
               SQL.AppendLine("Inner Join Tblpayrun D On A.payrunCode = D.payRunCode ");
               SQL.AppendLine("Left jOIN Tblbank E on B.bankCode = E.bankCode ");
               SQL.AppendLine("Inner Join Tblparameter F on 0=0 And F.parCode ='OTSetHolidayAmount' ");
               SQL.AppendLine("Inner Join Tblparameter G On 0=0 And G.parCode ='OTSetNonHolidayAmount' ");
               SQL.AppendLine("Inner Join ");
               SQL.AppendLine("( ");
               SQL.AppendLine("Select PayrunCode, EmpCode, SUM(OT1Hr) As OtHr, SUM(OT1Amt) As OtAmt ");
               SQL.AppendLine("From Tblpayrollprocess2 ");
               SQL.AppendLine("Group By PayrunCode, EmpCode ");
               SQL.AppendLine(")H On A.payrunCode = H.payrunCode And A.EmpCode = H.EmpCode ");
               SQL.AppendLine("Inner Join ");
               SQL.AppendLine("( ");
               SQL.AppendLine("Select PayrunCode, EmpCode, SUM(OTHolidayHr) As OtHolidayhr, SUM(OTholidayAmt) As OtHolidayAmt ");
               SQL.AppendLine("From Tblpayrollprocess2 ");
               SQL.AppendLine("Group By PayrunCode, EmpCode ");
               SQL.AppendLine(")I On A.payrunCode = I.payrunCode And A.EmpCode = I.EmpCode ");
               SQL.AppendLine("WHERE A.payrunCode = @payrunCode ");
               SQL.AppendLine("ORDER BY B.EmpName ");
               
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                   
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                  {
                    //0
                    "CompanyLogo",

                    //1-5
                    "EmpCode",
                    "EmpCodeold",
                    "Empname",
                    "Deptname",
                    "bankName",
                    //6-10
                    "BankAcNo",
                    "Yr",
                    "mth",
                    "OThr",
                    "OtAmtHr",
                    //11-14
                    "Otamt",
                    "OTHolidayhr",
                    "OtAmtHolidayhr",
                    "OtHolidayamt",

                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new OTSlip()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                EmpCode = Sm.DrStr(dr, c[1]),
                                EmpCodeold = Sm.DrStr(dr, c[2]),
                                Empname = Sm.DrStr(dr, c[3]),
                                Deptname = Sm.DrStr(dr, c[4]),
                                BankName = Sm.DrStr(dr, c[5]),

                                BankAcNo = Sm.DrStr(dr, c[6]),
                                Yr = Sm.DrStr(dr, c[7]),
                                Mth = Sm.DrStr(dr, c[8]),
                                OThr = Sm.DrDec(dr, c[9]),
                                OtAmtHr = Sm.DrDec(dr, c[10]),

                                Otamt = Sm.DrDec(dr, c[11]),
                                OTHolidayhr = Sm.DrDec(dr, c[12]),
                                OtAmtHolidayhr = Sm.DrDec(dr, c[13]),
                                OtHolidayamt = Sm.DrDec(dr, c[14]),
                               

                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                Sm.PrintReport("OTSlip", myLists, TableName, false);
            
           
        }

        internal void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')=@DeptCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AllEmployeeWithNoAttendanceGroup' As Col1, 'All Employee With No Attendance Group' As Col2 ");
            SQL.AppendLine("Order By Col2;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsEmpSystemTypeBorongan()
        {
            var cm = new MySqlCommand() 
            { CommandText = "Select PayrunCode From TblPayrun Where PayrunCode=@PayrunCode And SystemType=@EmpSystemTypeBorongan;" };
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpSystemTypeBorongan", mEmpSystemTypeBorongan);
            return Sm.IsDataExist(cm);
        }


        #region list printout

        private void Process1(ref List<PayrollAD> lPayrollAD)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, A.ADCode, A.Amt, If(A.Amt>0,'Rp','')As Curcode ");
                SQL.AppendLine("    From TblPayrollProcessAD A ");
                SQL.AppendLine("    Where A.payrunCode=@PayrunCode  ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.parvalue, A.Meal, If(A.Meal>0,'Rp','')As Curcode ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeMeal' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue, A.Transport, If(A.Transport>0,'Rp','')As Curcode  ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeTransport' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue, A.VariableAllowance, If(A.VariableAllowance>0,'Rp','')As Curcode   ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeVariable' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine(")Z Order By Z.payrunCode, Z.EmpCode, Z.AdCode ");

                Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
                
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PayrunCode", "EmpCode", "ADCode", "Amt", "Curcode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lPayrollAD.Add(new PayrollAD()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            ADCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            CurCode = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<AD> lAD)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select A.ADCode, A.ADName, A.ADType ");
                SQL.AppendLine("From TblAllowanceDeduction A ");
                //SQL.AppendLine("Where ADCode not in (Select parvalue From tblparameter Where parcode in('ADCodeTransport', 'ADCodeVariable', 'ADCodeMeal')) ");
                SQL.AppendLine("Order By A.ADCode, A.ADType; ");
            
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ADCode", "ADName", "ADType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lAD.Add(new AD()
                        {
                            ADCode = Sm.DrStr(dr, c[0]),
                            ADName = Sm.DrStr(dr, c[1]),
                            ADType = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }



        #endregion

        #endregion

        #endregion

        #region Event

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmOTSlipDlg(this));
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), mDeptCode);
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPayrunCode, "Payrun Code", false)) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowPayrunInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
    }

    #region Report Class

    class OTSlip 
    {
        public string CompanyLogo { get; set; }

        public string EmpCode { get; set; }
        public string EmpCodeold { get; set; }
        public string Empname { get; set; }
        public string Deptname { set; get; }
        public string BankAcNo { set; get; }
        public string BankName { get; set; }
        public string DisplayName { get; set; }
        public string Yr { get; set; }
        public string Mth { set; get; }
        public decimal OThr { get; set; }
        public decimal OtAmtHr { get; set; }
        public decimal Otamt { get; set; }
        public decimal OTHolidayhr { get; set; }
        public decimal OtAmtHolidayhr { get; set; }
        public decimal OtHolidayamt { get; set; }
       
    }


    #endregion
}
