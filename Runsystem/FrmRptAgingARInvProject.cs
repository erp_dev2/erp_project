﻿#region Update
/*
    09/12/2020 [DITA/IMS] New Apps
    10/12/2020 [DITA/IMS] perubahan rumus invoice amt dapet dari amt after tax nya
    17/12/2020 [DITA/IMS] Bug saat refresh setelah penambahan kolom status
    21/01/2021 [IBL/IMS] Menambah kolom deduction
    01/02/2021 [WED/IMS] ubah rumus Paid Amount
    01/02/2021 [WED/IMS] ubah rumus Balance
    02/02/2021 [DITA/IMS] Bug Sales invoice for project yang belum di voucherkan muncul nilai paid/settled nya
    11/02/2021 [DITA/IMS] ubah rumus paid amount & balance
    16/02/2021 [DITA/IMS] ubah rumus paid amount (yg tadinya ambil dari incoming, minta diambil dari amount voucher) & balance <--- perubahan dari aziz
    18/06/2021 [DITA/IMS] kolom paid/settled minta ditambah dari amount sli for project settlement juga
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInvProject : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARInvProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.DueDt, ");
            SQL.AppendLine("A.SOContractDocNo, B.PONo, C.CtCode, C.CtName, D.PtName, J.CurName, B.Amt+B.AmtBOM SOContractAmt, ");
            SQL.AppendLine("F.DOCtVerifyDocNo, F.TaxNo, F.TaxNo2, F.Amt, F.TaxAmt, F.TaxAmt2, F.DownPayment, ifnull(F.InvoiceAmt,0) InvoiceAmt, ");
            SQL.AppendLine("E.VoucherDocNo, ");
            SQL.AppendLine("If(Length(E.VoucherDocNo) > 0, ifnull(E.PaidAmt, 0.00)+ ifnull(L.SettleAmt, 0.00), ifnull(L.SettleAmt, 0.00)) PaidAmt, ");
            SQL.AppendLine("I.ProjectCode, I.Projectname, IfNull(K.Deduction, 0) Deduction ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo And A.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("(  ");
            //SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(T3.VoucherRequestDocNo) VoucherRequestDocNo, GROUP_CONCAT(T4.DocNo) VoucherDocNo, SUM(T4.Amt) PaidAmt  ");
            //SQL.AppendLine("    FROM TblSOContractDownpayment2Hdr T1  ");
            //SQL.AppendLine("    INNER JOIN TblIncomingPaymentDtl T2 ON T1.DocNo = T2.InvoiceDocNo AND InvoiceType = '7'  ");
            //SQL.AppendLine("    INNER JOIN TblIncomingPaymentHdr T3 ON T2.DocNo = T3.DocNo AND T3.CancelInd = 'N' AND T3.STATUS IN('O', 'A') "); 
            //SQL.AppendLine("    LEFT JOIN TblVoucherHdr T4 ON T3.VoucherRequestDocNo = T4.VoucherRequestDocNo AND T4.CancelInd = 'N'  ");
            //SQL.AppendLine("    GROUP BY T1.DocNo  ");
            SQL.AppendLine("    Select T1.DocNo, Group_Concat(IfNull(T3.VOucherRequestDocNo, '')) VoucherRequestDocNo, Group_Concat(IfNull(T4.DocNo, '')) VoucherDocNo, Sum(IfNull(T4.Amt, 0.00)) PaidAmt ");
            SQL.AppendLine("    From TblSOContractDownpayment2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 On T1.DocNo = T2.InvoiceDocNo And T2.InvoiceType = '7' ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.DocNo = T3.DocNo And T3.CancelInd = 'N' And T3.Status = 'A' ");
            SQL.AppendLine("    Left Join TblVoucherHdr T4 On T3.VoucherRequestDocNo = T4.VoucherRequestDocNo ");
            SQL.AppendLine("        And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") E ON A.DocNo = E.DocNo ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT A.DocNo , GROUP_CONCAT(distinct B.DOCtVerifyDocNo)DOCtVerifyDocNo, A.TaxInvoiceNo2 TaxNo, A.TaxInvoiceNo3 TaxNo2, ");
            SQL.AppendLine("    SUM(B.Amt) Amt, SUM(B.TaxAmt) TaxAmt, SUM(B.TaxAmt2) TaxAmt2, SUM(B.TerminDP) Downpayment, SUM(B.AfterTax) InvoiceAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A  ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl B ON A.DocNo = B.DocNo  And A.CancelInd='N' ");
            //SQL.AppendLine("    Where Exists ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select 1 ");
            //SQL.AppendLine("        From TblIncomingPaymentDtl ");
            //SQL.AppendLine("        Where InvoiceType = '7' ");
            //SQL.AppendLine("        And InvoiceDocNo = A.DocNo ");
            //SQL.AppendLine("        And DocNo In ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct X2.DocNo ");
            //SQL.AppendLine("            From TblVoucherHdr X1 ");
            //SQL.AppendLine("            Inner Join TblIncomingPaymentHdr X2 On X1.VoucherRequestDocNo = X2.VoucherREquestDocNo ");
            //SQL.AppendLine("                And X1.CancelInd = 'N' ");
            //SQL.AppendLine("                And X2.CancelInd = 'N' ");
            //SQL.AppendLine("                And X2.Status = 'A' ");
            //SQL.AppendLine("        ) ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("    GROUP BY A.DocNo ");
               
            SQL.AppendLine("    UNION ALL  ");
            	
            SQL.AppendLine("    SELECT A.DocNo ,GROUP_CONCAT( distinct B.DOCtVerifyDocNo) DOCtVerifyDocNo, A.TaxInvoiceNo TaxNo, A.TaxInvoiceNo4 TaxNo2, ");
            SQL.AppendLine("    SUM(B.Amt) Amt,SUM(B.TaxAmt) TaxAmt, SUM(B.TaxAmt2) TaxAmt2, SUM(B.TerminDP) Downpayment, SUM(B.AfterTax) InvoiceAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A  ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl2 B ON A.DocNo = B.DocNo And A.CancelInd='N'  ");
            //SQL.AppendLine("    Where Exists ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select 1 ");
            //SQL.AppendLine("        From TblIncomingPaymentDtl ");
            //SQL.AppendLine("        Where InvoiceType = '7' ");
            //SQL.AppendLine("        And InvoiceDocNo = A.DocNo ");
            //SQL.AppendLine("        And DocNo In ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct X2.DocNo ");
            //SQL.AppendLine("            From TblVoucherHdr X1 ");
            //SQL.AppendLine("            Inner Join TblIncomingPaymentHdr X2 On X1.VoucherRequestDocNo = X2.VoucherREquestDocNo ");
            //SQL.AppendLine("                And X1.CancelInd = 'N' ");
            //SQL.AppendLine("                And X2.CancelInd = 'N' ");
            //SQL.AppendLine("                And X2.Status = 'A' ");
            //SQL.AppendLine("        ) ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("    GROUP BY A.DocNo ");
               
            SQL.AppendLine(") F ON A.DocNo = F.DocNo  ");
            SQL.AppendLine("Inner Join TblBOQHdr G On B.BOQDocNo=G.DocNo  "); 
            SQL.AppendLine("Inner Join TblLOphdr H On G.LOPDocNo=H.DocNO  ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup I ON H.PGCode = I.PGCode  ");
            SQL.AppendLine("LEFT JOIN TblCurrency J ON A.CurCode = J.CurCode   ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.DocNo, Sum(IfNull(T5.DAmt, 0.00) - IfNull(T5.CAmt, 0.00)) As Deduction ");
	        SQL.AppendLine("    FROM TblSOContractDownpayment2Hdr T1 ");
            SQL.AppendLine("    INNER JOIN TblSOContractHdr T2 On T1.SOContractDocNo=T2.DocNo And T1.CancelInd='N' ");
            SQL.AppendLine("    INNER JOIN TblCustomer T3 On T2.CtCode=T3.CtCode ");
            SQL.AppendLine("    INNER JOIN TblIncomingPaymentDtl T4 On T1.DocNo = T4.InvoiceDocNo ");
            SQL.AppendLine("    LEFT JOIN TblIncomingPaymentDtl2 T5 On T4.DocNo = T5.DocNo ");
            SQL.AppendLine("    INNER JOIN TblIncomingPaymentHdr T6 On T4.DocNo = T6.DocNo And T6.CancelInd = 'N' And T6.Status In ('O', 'A') ");
            SQL.AppendLine("    INNER JOIN TblParameter T7 On T7.ParCode = 'CustomerAcNoAR' ");
            SQL.AppendLine("    AND T5.AcNo = Concat(T7.ParValue,T3.CtCode) ");
            SQL.AppendLine("    GROUP BY T1.DocNo ");
            SQL.AppendLine(") K On A.DocNo = K.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select SOContractDownpayment2DocNo, Sum(Amt) SettleAmt ");
            SQL.AppendLine("    From TblSOContractDPSettlement2Hdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    Group By SOContractDownpayment2DocNo ");
            SQL.AppendLine(") L On A.DocNo = L.SOContractDownpayment2DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2  ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Currency",
                        "Customer",
                        "Customer PO#",
                        "Project Code",

                        //6-10
                        "Project Name",
                        "SO Contract#",
                        "SO Contract"+Environment.NewLine+"Amount",
                        "DO To Cutomer"+Environment.NewLine+"Verification#",
                        "Sales Invoice"+Environment.NewLine+"for Project#",

                        //11-15
                        "Local Document#",
                        "Tax Invoice# 1",
                        "Tax Invoice# 2",
                        "Amount Before"+Environment.NewLine+"Tax",
                        "Tax 1",

                        //16-20
                        "Tax 2",
                        "Downpayment",
                        "Invoice Amount",
                        "Deduction",
                        "Paid/Settled",

                        //21-25
                        "Voucher#",
                        "",
                        "Balance",
                        "Due Date",
                        "Aging (Days)",

                        //26-30
                        "Aging Current AR",
                        "Over Due 1-30 Days",
                        "Over Due 31-60 Days",
                        "Over Due 61-90 Days",
                        "Over Due 91-120 Days",

                        //31
                        "Over 120 Days"


                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        120, 100, 200, 100, 100,

                        //6-10
                        150, 200, 120, 200, 200, 

                        //11-15
                        120, 120, 120, 120, 120,

                        //16-20
                        120, 120, 120, 120, 120,

                        //21-25
                        120, 20, 120, 100, 100,

                        //26-30
                        100, 100, 100, 100, 100,

                        //31
                        100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 14, 15, 16, 17, 18, 19, 20, 23, 25, 26, 27, 28, 29, 30, 31  }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1, 24 });
            Sm.GrdColButton(Grd1, new int[] {22 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 25, 26, 27, 28, 29, 30, 31});
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, new string[] { "SOContractDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "I.ProjectName", "I.ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, new string[] { "B.PONo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By DocNo;",
                        new string[]
                        { 
                            //0
                            "DocDt", 

                            //1-5
                            "CurName", "CtName", "PONo", "ProjectCode", "ProjectName",
                            
                            //6-10
                            "SOContractDocNo", "SOContractAmt", "DOCtVerifyDocNo", "DocNo", "TaxNo", 

                            //11-15
                            "TaxNo2", "Amt", "TaxAmt", "TaxAmt2", "Downpayment",

                            //16-20
                            "InvoiceAmt", "Deduction", "PaidAmt", "VoucherDocNo", "LocalDocNo",

                            //21
                            "DueDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            //Grd.Cells[Row, 23].Value = Sm.DrDec(dr, c[16]) + Sm.DrDec(dr, c[17]) - Sm.DrDec(dr, c[18]) - Sm.DrDec(dr, c[15]);
                            Grd.Cells[Row, 23].Value = Sm.DrDec(dr, c[16]) - (Sm.DrDec(dr, c[18]) - Sm.DrDec(dr, c[17]));
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 21);

                            Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 25, 26, 27, 28, 29, 30, 31 });
         
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 14, 15, 16, 17, 18, 19, 20, 23, 25, 26, 27, 28, 29, 30, 31 });
                  }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            
            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                var f = new FrmRptAgingARInvProjectDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                var f = new FrmRptAgingARInvProjectDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }



        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SLI For Project#");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion


    }
}
