﻿#region Update
/*
    02/10/2017 [TKG] New Transaction : Lot Bin Transfer     
    13/10/2017 [TKG] Bin bisa kosong  
    10/10/2022 [MAU/IOK] kolom transfer otomatis terisi (autofill)
    14/10/2022 [MAU/IOK] (Feedback) : penyesuaian kolom transfer
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmLotBinTransfer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType1 = "33", mDocType2 = "34";
        iGCell fCell;
        bool fAccept;
        internal FrmLotBinTransferFind FrmFind;

        #endregion

        #region Constructor

        public FrmLotBinTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Lot/Bin Transfer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin);
                LueLot.Visible = false;
                LueBin.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Local"+Environment.NewLine+"Code",
                        "Foreign"+Environment.NewLine+"Name",
                        
                        //6-10
                        "Property",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot"+Environment.NewLine+"(From)",

                        //11-15
                        "Bin"+Environment.NewLine+"(From)",
                        "Lot"+Environment.NewLine+"(To)",
                        "Bin"+Environment.NewLine+"(To)",
                        "Stock",
                        "Transfer",
                        
                        //16-20
                        "Balance",
                        "UoM",
                        "Stock",
                        "Transfer",
                        "Balance",

                        //21-25
                        "UoM",
                        "Stock",
                        "Transfer",
                        "Balance",
                        "UoM",

                        //26
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 100, 180, 100, 180, 
                        
                        //6-10
                        0, 0, 180, 180, 80, 
                        
                        //11-15
                        80, 80, 80, 100, 100,

                        //16-20
                        100, 80, 100, 100, 100,

                        //21-25
                        80, 100, 100, 100, 80,

                        //26
                        300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 16, 18, 19, 20, 22, 23, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 7, 9, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14, 16, 17, 18, 20, 21, 22, 24, 25 });
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 9, }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23, 24, 25 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 12, 13, 15, 19, 23, 26 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 12, 13, 15, 19, 23, 26 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtDocNo, DteDocDt, LueWhsCode, MeeRemark });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 16, 18, 19, 20, 22, 23, 24 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLotBinTransferFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }
        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", String.Empty, mMenuCode) == DialogResult.No || 
                    IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LotBinTransfer", "TblLotBinTransferHdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveLotBinTransferHdr(DocNo));
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        cml.Add(SaveLotBinTransferDtl(DocNo, r));
                }

                cml.Add(SaveStock1(DocNo));

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        cml.Add(SaveStock2(r));
                }
                
                Sm.ExecCommands(cml);

                BtnInsertClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmLotBinTransferDlg(this, Sm.GetLue(LueWhsCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 12, 13, 15, 19, 23, 26 }, e.ColIndex))
                {
                    if (e.ColIndex == 12) LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 13) LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 18, 19, 20, 22, 23, 24 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmLotBinTransferDlg(this, Sm.GetLue(LueWhsCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 19, 23 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 26 }, e);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 25)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 19 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 21), Sm.GetGrdStr(Grd1, e.RowIndex, 25)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 19);




            // transfer1 = 15
            // transfer2 = 19
            // transfer3 = 23

            // stock1 = 14
            // stock2 = 18
            // stock3 = 22

            // balance1 = 16
            // balance2 = 20
            // balance3 = 24
            // satuan = stock1 / stock2


            if (e.ColIndex == 15)
            {

                // transfer2 = transfer 1 / (stock1/stock2)
                // 19 = 15 / (14/18)



                Grd1.Cells[e.RowIndex, 19].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 15) / (Sm.GetGrdDec(Grd1, e.RowIndex, 14) / Sm.GetGrdDec(Grd1, e.RowIndex, 18));
                Grd1.Cells[e.RowIndex, 23].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 15) / (Sm.GetGrdDec(Grd1, e.RowIndex, 14) / Sm.GetGrdDec(Grd1, e.RowIndex, 18));
                Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 14) - Sm.GetGrdDec(Grd1, e.RowIndex, 15);
                Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 18) - Sm.GetGrdDec(Grd1, e.RowIndex, 19);
                Grd1.Cells[e.RowIndex, 24].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 22) - Sm.GetGrdDec(Grd1, e.RowIndex, 23);
            }

            if (e.ColIndex == 19)
            {
                // transfer1 = (stock1 / stock2) * transfer2
                // 15 =     14    /   18      *      19      


                // 

                Grd1.Cells[e.RowIndex, 15].Value = (Sm.GetGrdDec(Grd1, e.RowIndex, 14) / Sm.GetGrdDec(Grd1, e.RowIndex, 18)) * Sm.GetGrdDec(Grd1, e.RowIndex, 19);
                Grd1.Cells[e.RowIndex, 23].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 19);
                Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 14) - Sm.GetGrdDec(Grd1, e.RowIndex, 15);
                Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 18) - Sm.GetGrdDec(Grd1, e.RowIndex, 19);
                Grd1.Cells[e.RowIndex, 24].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 22) - Sm.GetGrdDec(Grd1, e.RowIndex, 23);
            }

            if (e.ColIndex == 23)
            {
                Grd1.Cells[e.RowIndex, 15].Value = (Sm.GetGrdDec(Grd1, e.RowIndex, 14) / Sm.GetGrdDec(Grd1, e.RowIndex, 18)) * Sm.GetGrdDec(Grd1, e.RowIndex, 23);
                Grd1.Cells[e.RowIndex, 19].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 23);
                Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 14) - Sm.GetGrdDec(Grd1, e.RowIndex, 15);
                Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 18) - Sm.GetGrdDec(Grd1, e.RowIndex, 19);
                Grd1.Cells[e.RowIndex, 24].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 22) - Sm.GetGrdDec(Grd1, e.RowIndex, 23);
            }

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 14, 15, 16, 18, 19, 20, 22, 23, 24 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    if (Sm.GetGrdStr(Grd1, r, e.ColIndex).Length != 0) 
                        Total += Sm.GetGrdDec(Grd1, r, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            ReComputeStock();
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 12, false, "Lot (Transfer To) is empty.")) return true;
                //if (Sm.IsGrdValueEmpty(Grd1, Row, 13, false, "Bin (Transfer To) is empty.")) return true;

                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Foreign Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15)<=0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 16)<0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                    return true;
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 20) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                        return true;
                    }
                }

                if (Grd1.Cols[23].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 23) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 24) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                        return true;
                    }
                }
            }

            return false;
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 9).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, r, 9);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 11), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 5);

                                Grd1.Cells[Row, 16].Value = Sm.GetGrdDec(Grd1, Row, 14) - Sm.GetGrdDec(Grd1, Row, 15);
                                Grd1.Cells[Row, 20].Value = Sm.GetGrdDec(Grd1, Row, 18) - Sm.GetGrdDec(Grd1, Row, 19);
                                Grd1.Cells[Row, 24].Value = Sm.GetGrdDec(Grd1, Row, 22) - Sm.GetGrdDec(Grd1, Row, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private MySqlCommand SaveLotBinTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLotBinTransferHdr(DocNo, DocDt, WhsCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveLotBinTransferDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLotBinTransferDtl ");
            SQL.AppendLine("(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot1, Bin1, Lot2, Bin2, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Balance, Balance2, Balance3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @PropCode, @BatchNo, @Source, @Lot1, IfNull(@Bin1, '-'), @Lot2, IfNull(@Bin2, '-'), ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @Balance, @Balance2, @Balance3, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot1", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin1", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot2", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin2", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Balance2", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Balance3", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock1(string DocNo)
        {
            var SQL = new StringBuilder();

            //From
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot1, IfNull(B.Bin1, '-'), B.ItCode, B.PropCode, B.BatchNo, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, B.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblLotBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblLotBinTransferDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            //To
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot2, IfNull(B.Bin2, '-'), B.ItCode, B.PropCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, B.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblLotBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblLotBinTransferDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot2, IfNull(B.Bin2, '-'), B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("0.00, 0.00, 0.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblLotBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblLotBinTransferDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblStockSummary T ");
            SQL.AppendLine("    Where T.WhsCode=A.WhsCode ");
            SQL.AppendLine("    And T.Lot=B.Lot2 ");
            SQL.AppendLine("    And IfNull(T.Bin, '-')=IfNull(B.Bin2, '-') ");
            SQL.AppendLine("    And T.Source=B.Source ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType1", mDocType1);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock2(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot1 ");
            SQL.AppendLine("And IfNull(Bin, '-')=IfNull(@Bin1, '-') ");
            SQL.AppendLine("And Source=@Source; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot2 ");
            SQL.AppendLine("And IfNull(Bin, '-')=IfNull(@Bin2, '-') ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot1", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Bin1", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@Lot2", Sm.GetGrdStr(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@Bin2", Sm.GetGrdStr(Grd1, r, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 19));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 23));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLotBinTransferHdr(DocNo);
                ShowLotBinTransferDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLotBinTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, Remark From TblLotBinTransferHdr " +
                    "Where DocNo=@DocNo;",
                    new string[] 
                    { "DocNo", "DocDt", "WhsCode", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowLotBinTransferDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, ");
            SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot1, A.Bin1, A.Lot2, A.Bin2, ");
            SQL.AppendLine("(A.Balance+A.Qty) As Stock, A.Qty, A.Balance, B.InventoryUOMCode, ");
            SQL.AppendLine("(A.Balance2+A.Qty2) As Stock2, A.Qty2, A.Balance2, B.InventoryUOMCode2, ");
            SQL.AppendLine("(A.Balance3+A.Qty3) As Stock3, A.Qty3, A.Balance3, B.InventoryUOMCode3, ");
            SQL.AppendLine("A.Remark ");
            SQL.AppendLine("From TblLotBinTransferDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "ForeignName", "PropCode", 

                    //6-10
                    "PropName", "BatchNo", "Source", "Lot1", "Bin1", 

                    //11-15
                    "Lot2", "Bin2", "Stock", "Qty", "Balance", 

                    //16-20
                    "InventoryUomCode", "Stock2", "Qty2", "Balance2", "InventoryUomCode2", 

                    //21-25
                    "Stock3", "Qty3", "Balance3", "InventoryUomCode3", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 18, 19, 20, 22, 23, 24 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

       
        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 13].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        #endregion

        #endregion
    }
}
