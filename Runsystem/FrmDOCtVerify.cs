﻿#region Update
/*
    08/03/2020 [TKG/IMS] New Application
    26/03/2020 [TKG/IMS] DO To Customer  verification bisa memproses data DO To Customer secara partial.
    23/04/2020 [TKG/IMS] tambah journal
    16/06/2020 [DITA/IMS] menampilkan SOC Remark dan tambah kolom notes param --> IsSalesTransactionShowSOContractRemark, IsSalesTransactionUseItemNotes
    17/06/2020 [WED/IMS] journal remark hdr ditambah informasi SOContract#, ambil dari DR
    09/07/2020 [HAR/IMS] SO Contract Remark ambil dari detail
    24/09/2020 [WED/IMS] tambah informasi No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber. Remark header bisa di edit berkali kali berdasarkan parameter IsRemarkHdrEditableOvertime
    30/09/2020 [DITA/IMS] Tambah inputan baru local document, no lppb, delivery date, received date
    30/09/2020 [DITA/IMS] Printout baru
    12/11/2020 [VIN/IMS] Tambah informasi Project, dan Customer PO di Detail 
    04/12/2020 [WED/IMS] docabbreviation dibedakan antara inventory dan service
    08/12/2020 [WED/IMS] link ke SO Contract Dtl diperbaiki
    29/12/2020 [ICA/IMS] Menambah Posname di printout
    21/01/2020 [DITA/IMS] menonaktifkan journal acno cogs berdasarkan parameter : IsAcNoForCOGSNotActive
    29/01/2021 [WED/IMS] DO To Customer bisa ditarik disini
    06/03/2021 [IBL/IMS] Tambah printout BAPP
    06/03/2021 [IBL/IMS] penyesuaian printout LPPB : format landsacape
    12/03/2021 [DITA/IMS] perbaikan Bug saat menampilkan item dismantle
    14/06/2021 [VIN/IMS] penyesuaian total progress  printout
    21/07/2021 [VIN/IMS] penyesuaian printout so contract no decimal
    28/07/2021 [VIN/IMS] Perubahan source item dismantle
    19/08/2021 [MYA/ALL] Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong (baik dari Menu Master Data maupun System's Parameter) sehingga terhindar dari jurnal yang tidak seimbang.
    25/11/2021 [VIN/ALL] Tambah max length 30 -> 100 -> lppb dan localdoc
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.Reflection;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmDOCtVerify : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal byte mDocType = 0;
        internal int mNumberOfInventoryUomCode = 1;
        private string
            mMainCurCode = string.Empty,
            mCustomerAcNoNonInvoice = string.Empty,
            mAcNoForCOGS = string.Empty,
            mAcNoForSaleOfFinishedGoods = string.Empty;
        private bool mIsAutoJournalActived = false,
            mIsDetailShowColumnNumber = false,
            mIsCheckCOAJournalNotExists = false,
            mIsRemarkHdrEditableOvertime = false,
            mIsAcNoForCOGSNotActive = false;
        internal bool
            mIsSalesTransactionShowSOContractRemark = false,
            mIsSalesTransactionUseItemNotes = false;
        internal FrmDOCtVerifyFind FrmFind;

        #endregion

        #region Constructor

        public FrmDOCtVerify(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Customer Based On Delivery Request Verification";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mCustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice");
            mAcNoForCOGS = Sm.GetParameter("AcNoForCOGS");
            mAcNoForSaleOfFinishedGoods = Sm.GetParameter("AcNoForSaleOfFinishedGoods");
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

            mIsSalesTransactionShowSOContractRemark = Sm.GetParameterBoo("IsSalesTransactionShowSOContractRemark");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
            mIsRemarkHdrEditableOvertime = Sm.GetParameterBoo("IsRemarkHdrEditableOvertime");
            mIsAcNoForCOGSNotActive = Sm.GetParameterBoo("IsAcNoForCOGSNotActive");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "DO To Customer DNo",
                        
                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Specification",
                        "Property Code",

                        //6-10
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",

                        //11-15
                        "Outstanding DO",
                        "Verified",
                        "UoM",
                        "Outstanding DO",
                        "Verified",

                        //16-20
                        "UoM",
                        "Outstanding DO",
                        "Verified",
                        "UoM",
                        "Remark",

                        //21-25
                        "SO Contract" + Environment.NewLine + "Remark",
                        "Notes",
                        "SO Contract's"+ Environment.NewLine + "No",
                        "Project Code",
                        "Project Name",

                        //26-30
                        "Customer PO#",
                        "Item Code"+Environment.NewLine+"Dismantle",
                        "Item Name"+Environment.NewLine+"Dismantle",
                        "SOContractDocNo",
                        "SOContractDNo",

                        //31-33
                        "ContractQty",
                        "ContracatUom",
                        "DeliveryDt"
                    },
                   new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        100, 200, 100, 200, 0, 

                        //6-10
                        0, 200, 200, 80, 80,  
                        
                        //11-15
                        100, 100, 80, 100, 100,  
                        
                        //16-20
                        80, 100, 100, 80, 200,

                        //21-25
                        200, 200, 100, 100, 200, 

                        //26-30
                        150, 150, 250, 0, 0,

                        //31-33
                        0, 0, 0
                    }
              );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14, 15, 17, 18, 31 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 5, 6, 8, 9, 10, 14, 15, 16, 17, 18, 19, 29, 30, 31, 32, 33 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 16, 17, 19, 21, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 });
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16 }, true);
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, true);
            if (!mIsSalesTransactionUseItemNotes) Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            if (!mIsSalesTransactionShowSOContractRemark) Sm.GrdColInvisible(Grd1, new int[] { 21 }, false);
            Grd1.Cols[28].Move(5);
            Grd1.Cols[27].Move(5);
            Grd1.Cols[23].Move(2);
            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd1, new int[] { 23 });
            Grd1.Cols[24].Move(20);
            Grd1.Cols[25].Move(21);
            Grd1.Cols[26].Move(22);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeCancelReason, ChkCancelInd, 
                        MeeRemark, DteDeliveryDt, DteReceivedDt, TxtLocalDocNo, TxtLPPBNo }, true);
                    BtnDOCt2DocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 12, 15, 18, 20, 22 });
                    Sm.GrdColInvisible(Grd1, new int[] { 11, 14, 17 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark, TxtLPPBNo, 
                        TxtLocalDocNo, DteReceivedDt, DteDeliveryDt  }, false);
                    BtnDOCt2DocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12, 15, 18, 20, 22, 23 });
                    Sm.GrdColInvisible(Grd1, new int[] { 11 }, true);
                    if (mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 14 }, true);
                    if (mNumberOfInventoryUomCode == 3) Sm.GrdColInvisible(Grd1, new int[] { 14, 17 }, true);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    if (mIsRemarkHdrEditableOvertime) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark, TxtLocalDocNo, TxtLPPBNo, DteDeliveryDt, DteReceivedDt }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDocType = 0;
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtDOCt2DocNo, TxtLocalDocNo, 
                TxtCtCode, TxtWhsCode, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2,
                TxtLPPBNo, TxtLocalDocNo, DteReceivedDt, DteDeliveryDt
            });
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 14, 15, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCtVerifyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 12, 15, 18 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 20 }, e);

            try
            {
                if (e.ColIndex == 12)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 1, 12, 15, 18, 13, 16, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 1, 12, 18, 21, 13, 19, 16);
                }

                if (e.ColIndex == 15)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 1, 15, 12, 18, 16, 13, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 1, 15, 18, 12, 16, 19, 13);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 1, 18, 12, 15, 19, 13, 16);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 1, 18, 15, 12, 19, 16, 13);
                }

                if (e.ColIndex == 12 && Sm.CompareGrdStr(Grd1, e.RowIndex, 13, Grd1, e.RowIndex, 16))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 12);

                if (e.ColIndex == 12 && Sm.CompareGrdStr(Grd1, e.RowIndex, 13, Grd1, e.RowIndex, 19))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 12);

                if (e.ColIndex == 15 && Sm.CompareGrdStr(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 19))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty; //Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCtVerify", "TblDOCtVerifyHdr");
            bool IsService = IsItemService();

            if (IsService) DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCtVerifyService", "TblDOCtVerifyHdr");
            else DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCtVerifyInventory", "TblDOCtVerifyHdr");
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCtVerify(DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsJournalSettingInvalid() ||
                IsGrdValueNotValid();
        }

        //private bool IsDataProcessedAlready()
        //{
        //    return false;
        //    Sm.IsDataExist(
        //    "Select 1 From TblDOCtVerifyHdr Where CancelInd='N' And DOCt2DocNo=@Param;",
        //    TxtDOCt2DocNo.Text,
        //    "DO to customer already verified.");
        //}

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Item list is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            string mVendorAcNoUnInvoiceAP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='VendorAcNoUnInvoiceAP'");
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;
            if (!mIsAcNoForCOGSNotActive)
            {
                if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;

                if (mAcNoForCOGS.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }
            }

            if (mCustomerAcNoNonInvoice.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }

            if (mAcNoForSaleOfFinishedGoods.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            //int CountGrd = Grd1.Rows.Count - 1; 
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 1);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + (COA == "AcNo" ? "(Stock)" : "") + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private void RecomputeOutstandingDOQty()
        {
            var SQL = new StringBuilder();
            string DNo = string.Empty;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;

            if (mDocType == 1)
            {
                SQL.AppendLine("Select A.DNo, ");
                SQL.AppendLine("A.Qty-A.DOCtVerifyQty As Qty, ");
                SQL.AppendLine("A.Qty2-A.DOCtVerifyQty2 As Qty2, ");
                SQL.AppendLine("A.Qty3-A.DOCtVerifyQty3 As Qty3 ");
                SQL.AppendLine("From TblDOCtDtl A ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");
            }
            else
            {
                SQL.AppendLine("Select A.DNo, ");
                SQL.AppendLine("A.Qty-A.DOCtVerifyQty As Qty, ");
                SQL.AppendLine("A.Qty2-A.DOCtVerifyQty2 As Qty2, ");
                SQL.AppendLine("A.Qty3-A.DOCtVerifyQty3 As Qty3 ");
                SQL.AppendLine("From TblDOCt2Dtl A ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDOCt2DocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "Qty", "Qty2", "Qty3" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DNo = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Qty2 = Sm.DrDec(dr, 2);
                        Qty3 = Sm.DrDec(dr, 3);
                        for (int r = 0; r <= Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 0).Length > 0 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 0), DNo))
                            {
                                Grd1.Cells[r, 11].Value = Qty;
                                Grd1.Cells[r, 14].Value = Qty2;
                                Grd1.Cells[r, 17].Value = Qty3;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsGrdValueNotValid()
        {
            RecomputeOutstandingDOQty();
            var Msg = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, r, 12) > Sm.GetGrdDec(Grd1, r, 11))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg +
                        "Verified quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 12), 0) + ") should not be bigger than DO's quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 11), 0) + ").");
                    return true;
                }
                if (Sm.GetGrdDec(Grd1, r, 15) > Sm.GetGrdDec(Grd1, r, 14))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg +
                        "Verified quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 15), 0) + ") should not be bigger than DO's quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 14), 0) + ").");
                    return true;
                }
                if (Sm.GetGrdDec(Grd1, r, 18) > Sm.GetGrdDec(Grd1, r, 17))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg +
                        "Verified quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 18), 0) + ") should not be bigger than DO's quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 17), 0) + ").");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveDOCtVerify(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string r = string.Empty;

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblDOCtVerifyHdr(DocNo, DocDt, CancelReason, CancelInd, DOCt2DocNo, DOCtDocNo, ");
            SQL.AppendLine("DeliveryDt, ReceivedDt, LocalDocNo, LPPBNo,  Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @DOCt2DocNo, @DOCtDocNo, ");
            SQL.AppendLine("@DeliveryDt, @ReceivedDt, @LocalDocNo, @LPPBNo, @Remark, @UserCode, @Dt);");

            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 1).Length > 0)
                {
                    r = row.ToString();

                    SQL.AppendLine("Insert Into TblDOCtVerifyDtl(DocNo, DNo, DOCt2DocNo, DOCt2DNo, DOCtDocNo, DOCtDNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, Notes, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @DNo_" + r + ", @DOCt2DocNo, @DOCt2DNo_" + r + ", @DOCtDocNo, @DOCtDNo_" + r + ", @ItCode_" + r + ", @PropCode_" + r + ", @BatchNo_" + r + ", @Source_" + r + ", @Lot_" + r + ", @Bin_" + r + ", @Qty_" + r + ", @Qty2_" + r + ", @Qty3_" + r + ", @Remark_" + r + ", @Notes_" + r + ", @UserCode, @Dt);");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r, Sm.Right(string.Concat("00000", (row+1).ToString()), 6));
                    if (mDocType == 1)
                    {
                        Sm.CmParam<String>(ref cm, "@DOCt2DNo_" + r, string.Empty);
                        Sm.CmParam<String>(ref cm, "@DOCtDNo_" + r, Sm.GetGrdStr(Grd1, row, 0));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@DOCt2DNo_" + r, Sm.GetGrdStr(Grd1, row, 0));
                        Sm.CmParam<String>(ref cm, "@DOCtDNo_" + r, string.Empty);
                    }
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r, Sm.GetGrdStr(Grd1, row, 1));
                    Sm.CmParam<String>(ref cm, "@PropCode_" + r, Sm.GetGrdStr(Grd1, row, 5));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r, Sm.GetGrdStr(Grd1, row, 7));
                    Sm.CmParam<String>(ref cm, "@Source_" + r, Sm.GetGrdStr(Grd1, row, 8));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r, Sm.GetGrdStr(Grd1, row, 9));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r, Sm.GetGrdStr(Grd1, row, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r, Sm.GetGrdDec(Grd1, row, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r, Sm.GetGrdDec(Grd1, row, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r, Sm.GetGrdDec(Grd1, row, 18));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r, Sm.GetGrdStr(Grd1, row, 20));
                    Sm.CmParam<String>(ref cm, "@Notes_" + r, Sm.GetGrdStr(Grd1, row, 22));
                }
            }

            if (mDocType == 1)
            {
                SQL.AppendLine("Update TblDOCtDtl A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DOCtDocNo As DocNo, T2.DOCtDNo As DNo, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.DOCtDocNo=@DOCtDocNo ");
                SQL.AppendLine("    Group By T2.DOCtDocNo, T2.DOCtDNo ");
                SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DOCtVerifyQty=IfNull(B.Qty, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty2=IfNull(B.Qty2, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty3=IfNull(B.Qty3, 0.00) ");
                SQL.AppendLine("Where A.DocNo=@DOCtDocNo; ");
            }
            else
            {
                SQL.AppendLine("Update TblDOCt2Dtl A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DOCt2DocNo As DocNo, T2.DOCt2DNo As DNo, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.DOCt2DocNo=@DOCt2DocNo ");
                SQL.AppendLine("    Group By T2.DOCt2DocNo, T2.DOCt2DNo ");
                SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DOCtVerifyQty=IfNull(B.Qty, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty2=IfNull(B.Qty2, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty3=IfNull(B.Qty3, 0.00) ");
                SQL.AppendLine("Where A.DocNo=@DOCt2DocNo; ");
            }
            
            cm.CommandText = SQL.ToString();
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (mDocType == 1)
            {
                Sm.CmParam<String>(ref cm, "@DOCt2DocNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", TxtDOCt2DocNo.Text);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DOCt2DocNo", TxtDOCt2DocNo.Text);
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", string.Empty);
            }
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LPPBNo", TxtLPPBNo.Text);
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetDte(DteDeliveryDt));
            Sm.CmParamDt(ref cm, "@ReceivedDt", Sm.GetDte(DteReceivedDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtVerifyHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer Verification : ', A.DocNo) As JnDesc, @MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("C.CCCode, CONCAT(A.Remark, ' | SO Contract# : ', D.SOContractDocNo) Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtVerifyHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.DocNo, T1.DocNo As DODocNo, T1.WhsCode ");
            SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyHdr T2 On T2.DOCt2DocNo = T1.DocNo And T2.DOCt2DocNo Is Not Null And T2.DOcNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.DocNo, T1.DocNo DODocNo, T1.WhsCode ");
            SQL.AppendLine("    From TblDOCtHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyHdr T2 On T2.DOCtDocNo = T1.DocNo And T2.DOCtDocNo Is Not Null And T2.DOcNo = @DocNo ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            SQL.AppendLine("INNER JOIN  ");
            SQL.AppendLine("( ");
            #region Old Code
            //SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(DISTINCT T3.SODocNo) SOContractDocNo ");
            //SQL.AppendLine("    FROM TblDOCtVerifyHdr T1 ");
            //SQL.AppendLine("    INNER JOIN TblDOCt2Hdr T2 ON T1.DOCt2DocNo = T2.DocNo ");
            //SQL.AppendLine("        AND T1.DocNo = @DocNo ");
            //SQL.AppendLine("        AND T2.DRDocNo IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblDRDtl T3 ON T2.DRDocNo = T3.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblDRHdr T4 ON T3.DocNo = T4.DocNo AND T4.DocType = '2' ");
            //SQL.AppendLine("    GROUP BY T1.DocNo ");
            #endregion

            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct IfNull(T3.SOContractDocNo, '')) SOContractDocNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DOcNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T2.SOContractDocNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner JOin TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine(") D ON A.DocNo = D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, D.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (!mIsAcNoForCOGSNotActive)
            {
                //1
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0.00)*IfNull(C.Excrate, 0.00)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCtVerifyHdr A ");
                SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");

                //2
                SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0.00)*IfNull(C.ExcRate, 0.00)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtVerifyHdr A ");
                SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
            }

            //3
            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, C.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*H.UPrice*B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr A ");
            SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr C On B.DOCt2DocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl D On B.DOCt2DocNo=D.DocNo And B.DOCt2DNo = D.DNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr G On D.SOContractDocNo=G.DocNo  ");
            SQL.AppendLine("        Inner Join TblSOContractDtl H On D.SOContractDocNo=H.DocNo And D.SOContractDNo=H.DNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, C.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*H.UPrice*B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr A ");
            SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo = D.DNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr G On C.SOContractDocNo=G.DocNo  ");
            SQL.AppendLine("        Inner Join TblSOContractDtl H On C.SOContractDocNo=H.DocNo And D.SOContractDNo=H.DNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");

            //4
            SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) End*H.UPrice*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr A ");
            SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr C On B.DOCt2DocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl D On B.DOCt2DocNo=D.DocNo And B.DOCt2DNo = D.DNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr G On D.SOContractDocNo=G.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl H On D.SOContractDocNo=H.DocNo And D.SOContractDNo=H.DNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) End*H.UPrice*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr A ");
            SQL.AppendLine("        Inner join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo = D.DNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr G On C.SOContractDocNo=G.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl H On C.SOContractDocNo=H.DocNo And D.SOContractDNo=H.DNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblCostCenter C on A.CCCode=C.CCCode  ");
            SQL.AppendLine("Left Join TblProfitCenter D on C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo In (Select JournalDocNo From TblDOCtVerifyHdr Where DocNo=@DocNo And JournalDocNo is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoNonInvoice", mCustomerAcNoNonInvoice);
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            Sm.CmParam<String>(ref cm, "@AcNoForSaleOfFinishedGoods", mAcNoForSaleOfFinishedGoods);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            bool IsJournalDocNo2Exists = Sm.IsDataExist("Select 1 From TblDOCtVerifyHdr Where DocNo = @Param And JournalDocNo2 Is Not Null; ", TxtDocNo.Text);
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid(IsJournalDocNo2Exists)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditDOCtVerifyHdr());

            if (mIsAutoJournalActived && ChkCancelInd.Checked && !IsJournalDocNo2Exists) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid(bool IsJournalDocNo2Exists)
        {
            bool IsDataAlreadyCancelled = Sm.IsDataExist("Select 1 From TblDOCtVerifyHdr Where CancelInd = 'Y' And DocNo = @Param; ", TxtDocNo.Text);
            bool ChangeToCancel = ChkCancelInd.Checked && !IsDataAlreadyCancelled;

            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedToSOContractDownpayment2(ChangeToCancel) ||
                (!IsJournalDocNo2Exists && ChkCancelInd.Checked && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)))
                ;
        }

        private bool IsDataProcessedToSOContractDownpayment2(bool ChangeToCancel)
        {
            if (!ChangeToCancel) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo From ( ");
            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.DOCtVerifyDocNo = @Param ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.DOCtVerifyDocNo = @Param ");
            SQL.AppendLine(") T Limit 1; ");

            string mSOContractDownpayment2DocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);
            if (mSOContractDownpayment2DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Sales Invoice Project #" + mSOContractDownpayment2DocNo);
                return true;
            }

            return false;
        }

        private bool IsDocNotCancelled()
        {
            if (mIsRemarkHdrEditableOvertime) return false;

            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this verification document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDOCtVerifyHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand EditDOCtVerifyHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtVerifyHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (mIsRemarkHdrEditableOvertime)
            {
                SQL.AppendLine("Update TblDOCtVerifyHdr Set ");
                SQL.AppendLine("    Remark = @Remark, LPPBNo = @LPPBNo, LocalDocNo = @LocalDocNo, DeliveryDt = @DeliveryDt, ReceivedDt = @ReceivedDt ");
                SQL.AppendLine("Where Docno = @DocNo; ");
            }

            if (mDocType == 1)
            {
                SQL.AppendLine("Update TblDOCtDtl A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DOCtDocNo As DocNo, T2.DOCtDNo As DNo, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
                SQL.AppendLine("    Inner JOin TblDOCtVerifyDtl T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.DOCt2DocNo=@DOCt2DocNo ");
                SQL.AppendLine("    Group By T2.DOCtDocNo, T2.DOCtDNo ");
                SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DOCtVerifyQty=IfNull(B.Qty, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty2=IfNull(B.Qty2, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty3=IfNull(B.Qty3, 0.00) ");
                SQL.AppendLine("Where A.DocNo=@DOCtDocNo; ");
            }
            else
            {
                SQL.AppendLine("Update TblDOCt2Dtl A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DOCt2DocNo As DocNo, T2.DOCt2DNo As DNo, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
                SQL.AppendLine("    Inner JOin TblDOCtVerifyDtl T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.DOCt2DocNo=@DOCt2DocNo ");
                SQL.AppendLine("    Group By T2.DOCt2DocNo, T2.DOCt2DNo ");
                SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DOCtVerifyQty=IfNull(B.Qty, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty2=IfNull(B.Qty2, 0.00), ");
                SQL.AppendLine("    A.DOCtVerifyQty3=IfNull(B.Qty3, 0.00) ");
                SQL.AppendLine("Where A.DocNo=@DOCt2DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LPPBNo", TxtLPPBNo.Text);
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetDte(DteDeliveryDt));
            Sm.CmParamDt(ref cm, "@ReceivedDt", Sm.GetDte(DteReceivedDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mDocType == 1)
            {
                Sm.CmParam<String>(ref cm, "@DOCt2DocNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", TxtDOCt2DocNo.Text);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DOCt2DocNo", TxtDOCt2DocNo.Text);
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", string.Empty);
            }
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblDOCtVerifyHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCtVerifyHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCtVerifyHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCtVerifyHdr(DocNo);
                mDocType = GetDocType(TxtDOCt2DocNo.Text);
                ShowDOCtVerifyDtl(DocNo);
                GetDismantleItem(Sm.GetGrdStr(Grd1, 0, 29));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCtVerifyHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("IfNull(A.DOCt2DocNo, A.DOCtDocNo) DOCt2DocNo, C.CtName, D.WhsName, A.Remark, A.JournalDocNo, A.JournalDocNo2, ");
            SQL.AppendLine("A.LPPBNo, A.LocalDocNo, A.DeliveryDt, A.ReceivedDt ");
            SQL.AppendLine("From TblDOCtVerifyHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.CtCode, T2.WhsCode ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DocNo = T2.DOcNo ");
            SQL.AppendLine("        ANd T1.DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T2.CtCode, T2.WhsCode ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DOcNo ");
            SQL.AppendLine("        ANd T1.DocNo = @DocNo ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On B.WhsCode=D.WhsCode ");
            //SQL.AppendLine("Inner Join TblDRHdr E ON B.DRDocno = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "DOCt2DocNo", "LocalDocNo", 
                    //6-10
                    "CtName", "WhsName", "Remark", "JournalDocNo", "JournalDocNo2",
                    //11-14
                     "LPPBNo", "DeliveryDt", "ReceivedDt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                    TxtDOCt2DocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtCtCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtWhsCode.EditValue = Sm.DrStr(dr, c[7]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[9]);
                    TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[10]);
                    TxtLPPBNo.EditValue = Sm.DrStr(dr, c[11]);
                    Sm.SetDte(DteDeliveryDt, Sm.DrStr(dr, c[12]));
                    Sm.SetDte(DteReceivedDt, Sm.DrStr(dr, c[13]));
                }, true
            );
        }

        private void ShowDOCtVerifyDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(A.DOCt2DNo, A.DOCtDNo) DOCt2DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
            SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            //SQL.AppendLine("IfNull(D.Qty, 0.00)-IfNull(D.DOCtVerifyQty, 0.00)+Case When E.CancelInd='Y' Then 0.00 Else A.Qty End As DOCt2Qty, A.Qty, B.InventoryUomCode, ");
            //SQL.AppendLine("IfNull(D.Qty2, 0.00)-IfNull(D.DOCtVerifyQty2, 0.00)+Case When E.CancelInd='Y' Then 0.00 Else A.Qty2 End  As DOCt2Qty2, A.Qty2, B.InventoryUomCode2, ");
            //SQL.AppendLine("IfNull(D.Qty3, 0.00)-IfNull(D.DOCtVerifyQty3, 0.00)+Case When E.CancelInd='Y' Then 0.00 Else A.Qty3 End As DOCt2Qty3, A.Qty3, B.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("0.00 As DOCt2Qty, A.Qty, B.InventoryUomCode, ");
            SQL.AppendLine("0.00  As DOCt2Qty2, A.Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("0.00 As DOCt2Qty3, A.Qty3, B.InventoryUomCode3, A.Remark, A.Notes, D.No, D.SOCRemark, ");
            SQL.AppendLine("E.PONo, H.PGCode, H.ProjectName, D.SOContractDocNo, D.SOContractDNo ");
            SQL.AppendLine("From TblDOCtVerifyDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, T3.SOContractDocNo, T3.SOContractDNo, T4.No, T4.Remark SOCRemark, ");
            SQL.AppendLine("    T3.Qty, T3.Qty2, T3.Qty3, ");
            SQL.AppendLine("    T3.DOCtVerifyQty, T3.DOCtVerifyQty2, T3.DOCtVerifyQty3 ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("        And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T3 On T2.DOCt2DocNo = T3.DocNo And T2.DOCt2DNo = T3.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl T4 On T3.SOContractDocno =T4.DocNo And T3.SOContractDNo = T4.DNo ");
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select T2.DocNo, T2.DNo, T4.SOContractDocNo, T3.SOContractDNo, T5.No, T5.Remark SOCRemark, ");
            SQL.AppendLine("    T3.Qty, T3.Qty2, T3.Qty3, ");
            SQL.AppendLine("    T3.DOCtVerifyQty, T3.DOCtVerifyQty2, T3.DOCtVerifyQty3 ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T3 On T2.DOCtDocNo = T3.DocNo And T2.DOCtDNo = T3.DNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T4 On T3.DocNo = T4.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl T5 On T4.SOContractDocno =T5.DocNo And T3.SOContractDNo = T5.DNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo And A.DNo = D.DNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr E On D.SOContractDocNO = E.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr F On E.BOQDocNO = F.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr G ON F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("Left JOin TblProjectGroup H On G.PGCode = H.PGCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.No, T4.Remark SOCRemark, ");
            //SQL.AppendLine("    T6.PONo, T8.PGCode, T8.ProjectName, T6.DocNo SOCDocNo ");
            //SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblSOContractDtl T4 On T2.SOContractDocNo = T4.DocNo And T2.SOContractDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblDOCtVerifyHdr T5 On T5.DOCt2DocNo = T1.DocNo And T5.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN tblsocontracthdr T6 ON T6.DocNo=T4.DocNo ");
            //SQL.AppendLine("    INNER JOIN tblboqhdr T7 ON T6.BOQDocNo=T7.DocNo ");
            //SQL.AppendLine("    LEFT JOIN tbllophdr T8 ON T7.LOPDocNo=T8.DocNo ");
            //SQL.AppendLine(") E On D.DocNo = E.DocNo And D.DNo = E.DNo ");
            //SQL.AppendLine("Inner Join TblDOCtVerifyHdr E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DOCt2DNo",
 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Specification", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "DOCt2Qty", "Qty", "InventoryUomCode", "DOCt2Qty2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "DOCt2Qty3", "Qty3", "InventoryUomCode3", "Remark",

                    //21-25
                    "Notes", "No", "SOCRemark", "PGCode", "ProjectName", 
                    
                    //26-28
                    "PONo", "SOContractDocNo", "SOContractDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetDismantleItem(string SOContractDocNo)
        {
            if (Grd1.Rows.Count > 1)
            {
                var SQL = new StringBuilder();
                var l = new List<ItemDismantle>();
                bool IsService = false;

                IsService = Sm.IsDataExist("Select 1 From TblSOContractDtl4 Where DocNo = @Param Limit 1; ", SOContractDocNo);
                GenerateDismantleSQL(ref SQL, IsService);
                GetItemDismantleData(ref l, ref SQL, SOContractDocNo);
                CompareItemDismantleToGrid(ref l);
                l.Clear();
            }
        }

        private void CompareItemDismantleToGrid(ref List<ItemDismantle> l)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                foreach (var x in l.Where(w => 
                    Sm.GetGrdStr(Grd1, i, 29) == w.SOContractDocNo &&
                    Sm.GetGrdStr(Grd1, i, 30) == w.SOContractDNo
                    ))
                {
                    Grd1.Cells[i, 27].Value = x.ItCodeDismantle;
                    Grd1.Cells[i, 28].Value = x.ItNameDismantle;
                    Grd1.Cells[i, 31].Value = x.ContractQty;
                    Grd1.Cells[i, 32].Value = x.ContractUom;
                    Grd1.Cells[i, 33].Value = x.DeliveryDt;
                }
            }
        }

        private void GetItemDismantleData(ref List<ItemDismantle> l, ref StringBuilder SQL, string SOContractDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", SOContractDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "ItCodeDismantle", "ItNameDismantle", "ContractQty", "ContractUom", "DeliveryDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ItemDismantle()
                        {
                            SOContractDocNo = SOContractDocNo,
                            SOContractDNo = Sm.DrStr(dr, c[1]),
                            ItCodeDismantle = Sm.DrStr(dr, c[2]),
                            ItNameDismantle = Sm.DrStr(dr, c[3]),
                            ContractQty = Sm.DrDec(dr, c[4]),
                            ContractUom = Sm.DrStr(dr, c[5]),
                            DeliveryDt = Sm.DrStr(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GenerateDismantleSQL(ref StringBuilder SQL, bool IsService)
        {
            if (IsService)
            {
                SQL.AppendLine("Select Distinct A.DocNo, B.DNo, C.ItCodeDismantle, D.ItName ItNameDismantle, A.QtyPackagingUnit ContractQty, A.PackagingUnitUomCode ContractUom, ");
                SQL.AppendLine("Concat(DATE_FORMAT(A.DeliveryDt, '%d'),' ',  ");
                SQL.AppendLine("Case MonthName(A.DeliveryDt)   ");
                SQL.AppendLine("When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'   ");
                SQL.AppendLine("When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'   ");
                SQL.AppendLine("When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember'  ");
                SQL.AppendLine("END,' ', DATE_FORMAT(A.DeliveryDt, '%Y')) As DeliveryDt ");
                SQL.AppendLine("From TblSOContractDtl A ");
                SQL.AppendLine("Inner Join TblSOContractDtl4 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And A.ItCode = B.ItCode And A.No = B.No ");
                SQL.AppendLine("Inner Join TblBOQDtl2 C On B.BOQDocNo = C.DocNo ");
                SQL.AppendLine("    And A.ItCode = C.ItCode And B.SeqNo = C.SeqNo ");
                SQL.AppendLine("Left Join TblItem D On C.ItCodeDismantle = D.ItCode ");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.DocNo, B.DNo, C.ItCodeDismantle, D.ItName ItNameDismantle, A.QtyPackagingUnit ContractQty, A.PackagingUnitUomCode ContractUom, ");
                SQL.AppendLine("Concat(DATE_FORMAT(A.DeliveryDt, '%d'),' ',  ");
                SQL.AppendLine("Case MonthName(A.DeliveryDt)   ");
                SQL.AppendLine("When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'   ");
                SQL.AppendLine("When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'   ");
                SQL.AppendLine("When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember'  ");
                SQL.AppendLine("END,' ', DATE_FORMAT(A.DeliveryDt, '%Y')) As DeliveryDt ");
                SQL.AppendLine("From TblSOContractDtl A ");
                SQL.AppendLine("Inner Join TblSOContractDtl5 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And A.ItCode = B.ItCode ");
                SQL.AppendLine("Inner Join TblBOQDtl3 C On B.BOQDocNo = C.DocNo ");
                SQL.AppendLine("    And A.ItCode = C.ItCode And B.SeqNo = C.SeqNo ");
                SQL.AppendLine("Left Join TblItem D On C.ItCodeDismantle = D.ItCode ");
            }
        }

        private byte GetDocType(string DocNo)
        {
            byte DocType = 0;

            if (Sm.IsDataExist("Select 1 From TblDOCtHdr Where DocNo = @Param; ", DocNo)) DocType = 1;

            return DocType;
        }

        private bool IsItemService()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo And A.DocNo = @Param ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    And C.ServiceItemInd = 'Y' ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtDOCt2DocNo.Text);
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle"), mItNameDismantle = string.Empty;
            var l = new List<DOCtVerifyHeader>();
            var ldtl = new List<DOCtVerifyDetail>();
            var lSign = new List<DOCtVerifySign>();
            var lTotalProgress = new List<TotalProgress>();

            string[] TableName = { "DOCtVerifyHeader", "DOCtVerifyDetail", "DOCtVerifySign", "TotalProgress" };
            List<IList> myLists = new List<IList>();

            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            var SQL1 = new StringBuilder();
            var cm1 = new MySqlCommand();

            SQL1.AppendLine("Select @CompanyFooterImage As CompanyFooterImage, ");
            SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',  ");
            SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhoneNumber' ");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@CompanyFooterImage", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png");
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(
                    dr1, new string[]
                    {
                        //0
                        "CompanyFooterImage",
                        //1-4
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyAddressCity",
                        "CompanyPhoneNumber",
                    });

                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l.Add(new DOCtVerifyHeader()
                        {
                            CompanyLogo = @Sm.CompanyLogo(),
                            CompanyFooterImage = Sm.DrStr(dr1, c1[0]),
                            CompanyName = Sm.DrStr(dr1, c1[1]),
                            CompanyAddress = Sm.DrStr(dr1, c1[2]),
                            CompanyAddressCity = Sm.DrStr(dr1, c1[3]),
                            CompanyPhoneNumber = Sm.DrStr(dr1, c1[4]),

                            CtName = TxtCtCode.Text,
                            LPPBNo = TxtLPPBNo.Text,
                            DocDt = String.Format("{0:dd MMM yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt))),
                            DeliveryDt = (Sm.GetDte(DteDeliveryDt).Length == 0) ? string.Empty : String.Format("{0:dd MMM yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDeliveryDt))),
                            ReceivedDt = (Sm.GetDte(DteReceivedDt).Length == 0) ? string.Empty : String.Format("{0:dd MMM yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteReceivedDt))),
                            PONo = string.Empty,

                            ProjectCode = string.Empty,
                            ProjectName = string.Empty,
                            LocalDocNo = TxtLocalDocNo.Text,
                            Remark = MeeRemark.Text,
                        });
                    }
                }
                dr1.Close();
            }

            if (l.Count > 0)
            {

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                #region Old Code
                //SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                //SQL.AppendLine("A.DocNo, C.CtName, A.LPPBNo, DATE_FORMAT(A.DocDt,'%d %M %Y') DocDt, DATE_FORMAT(A.ReceivedDt,'%d %M %Y') ReceivedDt,");
                //SQL.AppendLine("F.PONo, F.ProjectCode, F.ProjectName, A.LocalDocNo ");
                //SQL.AppendLine("From TblDOCtVerifyHdr A  ");
                //SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCt2DocNo=B.DocNo  ");
                //SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode  ");
                //SQL.AppendLine("Inner Join TblDOCtVerifyDtl D On A.DocNo=D.DocNo  ");
                //SQL.AppendLine("Inner Join TblDOCt2Dtl E On D.DOCt2DocNo=E.DocNo And D.DOCt2DNo=E.DNo ");
                //SQL.AppendLine("Left Join  ");
                //SQL.AppendLine("(  ");
                //SQL.AppendLine("    Select T1.DocNo, T2.DNo, Group_Concat(Distinct T4.PONo SEPARATOR '\n') PONo, ");
                //SQL.AppendLine("    Group_Concat(DISTINCT T7.ProjectCode SEPARATOR '\n') ProjectCode , Group_Concat(DISTINCT T7.ProjectName SEPARATOR '\n') ProjectName ");
                //SQL.AppendLine("    From TblDOCt2Hdr T1  ");
                //SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo  ");
                //SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T2.SOContractDocNo = T4.DocNo  ");
                //SQL.AppendLine("    Inner Join TblSOContractDtl T42 On T2.SOCOntractDocNo = T42.DocNo And T2.SOContractDNo = T42.DNo ");
                //SQL.AppendLine("    Inner Join TblBOQHdr T5 ON T4.BOQDocNo = T5.DocNo  ");
                //SQL.AppendLine("    Inner Join TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo  ");
                //SQL.AppendLine("    Inner Join TblProjectGroup T7 ON T6.PGCode = T7.PGCode  ");
                //SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T8 ON T8.DOCt2DocNo = T1.DocNo And T8.DocNo = @DocNo  ");
                //SQL.AppendLine("    Group By T1.DocNo, T2.DNo ");
                //SQL.AppendLine(") F On B.DocNo = F.DocNo  And E.DNo = F.DNo ");
                //SQL.AppendLine("WHERE A.DocNo = @DocNo ");
                #endregion

                SQL.AppendLine("Select T1.DocNo, Group_Concat(Distinct IfNull(T2.PONo, '') SEPARATOR '\n') PONo, ");
                SQL.AppendLine("Group_Concat(Distinct IfNull(T5.ProjectCode, '') SEPARATOR '\n') ProjectCode, ");
                SQL.AppendLine("Group_Concat(Distinct IfNull(T5.ProjectName, '') SEPARATOR '\n') ProjectName ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.DocNo, D.SOContractDocNo ");
                SQL.AppendLine("    From TblDOCtVerifyHdr A ");
                SQL.AppendLine("    Inner Join TblDOCtVerifyDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo And A.DOCt2DocNo Is Not Null ");
                SQL.AppendLine("    Inner Join TblDOCt2Hdr C On A.DOCt2DOcNo = C.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl D On C.DocNo = D.DocNo And B.DOCt2DNo = D.DNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.DocNo, C.SOContractDocNo ");
                SQL.AppendLine("    From TblDOCtVerifyHdr A ");
                SQL.AppendLine("    Inner Join TblDOCtVerifyDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo And A.DOCtDocNo Is Not Null ");
                SQL.AppendLine("    Inner Join TblDOCtHdr C On A.DOCtDOcNo = C.DocNo ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Inner Join TblSOContractHdr T2 On T1.SOContractDocNo = T2.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr T3 On T2.BOQDocNO = T3.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr T4 On T3.LOPDocNo = T4.DocNO ");
                SQL.AppendLine("Left Join TblProjectGroup T5 On T4.PGCode = T5.PGCode ");
                SQL.AppendLine("Group By T1.DocNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "PONo", "ProjectCode", "ProjectName" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            foreach(var x in l)
                            {
                                x.PONo = Sm.DrStr(dr, c[0]);
                                x.ProjectCode = Sm.DrStr(dr, c[1]);
                                x.ProjectName = Sm.DrStr(dr, c[2]);
                            }
                        }
                    }
                    dr.Close();
                }
            }
            myLists.Add(l);

            #endregion

            #region Detail
            int no = 1;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (mItNameDismantle == Sm.GetGrdStr(Grd1, i, 28))
                {
                    ldtl.Add(new DOCtVerifyDetail()
                    {
                        nomor = no,
                        SOContractNo = decimal.Parse(Sm.GetGrdStr(Grd1, i, 23)),
                        ItInternalCode = Sm.GetGrdStr(Grd1, i, 3),
                        ItName = Sm.GetGrdStr(Grd1, i, 2),
                        SOContractRemark = Sm.GetGrdStr(Grd1, i, 21),
                        UOM = Sm.GetGrdStr(Grd1, i, 13),
                        Verified = Sm.GetGrdDec(Grd1, i, 12),
                        Remark = Sm.GetGrdStr(Grd1, i, 20),
                        ItNameDismantle = Sm.GetGrdStr(Grd1, i, 28),
                        ContractQty = Sm.GetGrdDec(Grd1, i, 31),
                        ContractUomCode = Sm.GetGrdStr(Grd1, i, 32),
                        DeliveryDt = Sm.GetGrdStr(Grd1, i, 33),
                        SOContractDNo = Sm.GetGrdStr(Grd1, i, 30),
                        TotalProgress = 0m,
                    });
                    no++;
                }
                else
                {
                    mItNameDismantle = Sm.GetGrdStr(Grd1, i, 28);
                    no = 1;
                    ldtl.Add(new DOCtVerifyDetail()
                    {
                        nomor = no,
                        SOContractNo = decimal.Parse(Sm.GetGrdStr(Grd1, i, 23)),
                        ItInternalCode = Sm.GetGrdStr(Grd1, i, 3),
                        ItName = Sm.GetGrdStr(Grd1, i, 2),
                        SOContractRemark = Sm.GetGrdStr(Grd1, i, 21),
                        UOM = Sm.GetGrdStr(Grd1, i, 13),
                        Verified = Sm.GetGrdDec(Grd1, i, 12),
                        Remark = Sm.GetGrdStr(Grd1, i, 20),
                        ItNameDismantle = Sm.GetGrdStr(Grd1, i, 28),
                        ContractQty = Sm.GetGrdDec(Grd1, i, 31),
                        ContractUomCode = Sm.GetGrdStr(Grd1, i, 32),
                        DeliveryDt = Sm.GetGrdStr(Grd1, i, 33),
                        SOContractDNo = Sm.GetGrdStr(Grd1, i, 30),
                        TotalProgress = 0m,


                    });
                    no++;
                }
            }

            myLists.Add(ldtl);

            #endregion

            #region Sign

            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            SQL2.AppendLine("Select ifnull(C.EmpName, B.UserName) EmpName, ifnull(D.PosName, '') PosName ");
            SQL2.AppendLine("From TblDOCtVerifyHdr A ");
            SQL2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
            SQL2.AppendLine("Left Join TblEmployee C On A.CreateBy=C.UserCode ");
            SQL2.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL2.AppendLine("Where DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                {
                    "EmpName",
                    "PosName"
                });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        lSign.Add(new DOCtVerifySign()
                        {
                            EmpName = Sm.DrStr(dr2, c2[0]),
                            PosName = Sm.DrStr(dr2, c2[1])
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(lSign);

            #endregion

            #region Total Progress

            var SQLP = new StringBuilder();

            SQLP.AppendLine("Select T.SOContractDNo, SUM(T.Qty) TotalProgress ");
            SQLP.AppendLine("From ");
            SQLP.AppendLine("( ");
            SQLP.AppendLine("	Select B.SOContractDNo, B.Qty ");
            SQLP.AppendLine("	From TblDOCtHdr A  ");
            SQLP.AppendLine("	Inner Join TblDOCtDtl B oN A.DocNo = B.DocNo And A.`Status` In ('O', 'A') And B.CancelInd = 'N' ");
            SQLP.AppendLine("	Where Concat(A.DocDt, Left(A.DocNo,4)) <= Concat(@DocDt, Left(@DocNo,4)) ");
            SQLP.AppendLine("	And A.SOContractDocNo = @SOContractDocNo ");
            SQLP.AppendLine(") T ");
            SQLP.AppendLine("Group By T.SOContractDNo; ");


            var cmP = new MySqlCommand();
            string SOCOntractDocNo = Sm.GetValue("Select SOContractDocNo from TblDOCtHdr where DocNo  = '" + TxtDOCt2DocNo.Text + "'");
            string DocDt = Sm.GetValue("Select DocDt from TblDOCtHdr where DocNo  = '" + TxtDOCt2DocNo.Text + "'");
            Sm.CmParam<String>(ref cmP, "@SOContractDocNo", SOCOntractDocNo);
            Sm.CmParam<String>(ref cmP, "@DocNo", TxtDOCt2DocNo.Text);
            Sm.CmParam<String>(ref cmP, "@DocDt", DocDt);
            using (var cnP = new MySqlConnection(Gv.ConnectionString))
            {
                cnP.Open();
                cmP.Connection = cnP;
                cmP.CommandTimeout = 600;
                cmP.CommandText = SQLP.ToString();
                var drP = cmP.ExecuteReader();
                var cP = Sm.GetOrdinal(drP, new string[] { "SOContractDNo", "TotalProgress" });
                if (drP.HasRows)
                {
                    while (drP.Read())
                    {
                        lTotalProgress.Add(new TotalProgress()
                        {
                            SOContractDNo = Sm.DrStr(drP, cP[0]),
                            TotalProgressDO = Sm.DrDec(drP, cP[1]),

                        });
                    }
                }
                drP.Close();
            }

            foreach (var x in ldtl)
            {
                foreach (var y in lTotalProgress.Where(w => w.SOContractDNo == x.SOContractDNo))
                {
                    x.TotalProgress = y.TotalProgressDO;
                    
                }
            }


            myLists.Add(ldtl);

            #endregion


            if (Sm.GetParameter("DocTitle") == "IMS")
            {
                if (TxtDOCt2DocNo.Text.Contains("SJN"))
                    Sm.PrintReport("DOCtVerifyIMS", myLists, TableName, false); //LPPB
                else
                    Sm.PrintReport("DOCtVerifyIMS_2", myLists, TableName, false); //BAPP
            }
        }

        internal void ShowDOCt2Info(string DocNo)
        {
            ShowDOCt2Dtl(DocNo);
            GetDismantleItem(Sm.GetGrdStr(Grd1, 0, 29));
        }

        private void ShowDOCt2Dtl(string DocNo)
        {
            ClearGrd();
            mDocType = GetDocType(DocNo);

            var SQL = new StringBuilder();

            if (mDocType == 1)
            {
                SQL.AppendLine("Select B.DNo, B.ItCode, G.ItName, G.ItCodeInternal, G.Specification, ");
                SQL.AppendLine("B.PropCode, I.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQL.AppendLine("B.Qty - B.DOCtVerifyQty As Qty, G.InventoryUOMCode, ");
                SQL.AppendLine("B.Qty2 - B.DOCtVerifyQty2 As Qty2, G.InventoryUOMCode2, ");
                SQL.AppendLine("B.Qty3 - B.DOCtVerifyQty3 As Qty3, G.InventoryUOMCode3, ");
                SQL.AppendLine("D.No, D.Remark SOCRemark, F.PGCode, H.ProjectName, C.PONo, A.SOContractDocNo, B.SOContractDNo ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr C On A.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractDtl D On A.SOContractDocNo = D.DocNo And B.SOContractDNo = D.DNo ");
                SQL.AppendLine("Inner Join TblBOQHdr E On C.BOQDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNO = F.DocNo ");
                SQL.AppendLine("Inner Join TblItem G On B.ItCode = G.ItCode ");
                SQL.AppendLine("Left Join TblProjectGroup H On F.PGCode = H.PGCode ");
                SQL.AppendLine("Left Join TblProperty I On B.PropCode = I.PropCode ");
                SQL.AppendLine("Order By B.DNo; ");
            }
            else
            {
                SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
                SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("A.Qty-A.DOCtVerifyQty As Qty, B.InventoryUomCode, ");
                SQL.AppendLine("A.Qty2-A.DOCtVerifyQty2 As Qty2, B.InventoryUomCode2, ");
                SQL.AppendLine("A.Qty3-A.DOCtVerifyQty3 As Qty3, B.InventoryUomCode3, D.No, ");
                SQL.AppendLine("D.SOCRemark, D.PGCode, D.ProjectName, D.PONo, A.SOContractDocNo, A.SOContractDNo ");
                SQL.AppendLine("From TblDOCt2Dtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode, T4.No, T4.Remark SOCRemark, ");
                SQL.AppendLine("    T5.PONo, T7.PGCode, T7.ProjectName ");
                SQL.AppendLine("    From TblDOCt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblSOContractDtl T4 On T2.SOContractDocNo = T4.DocNo And T2.SOContractDNo = T4.DNo ");
                SQL.AppendLine("    INNER JOIN tblsocontracthdr T5 ON T4.DocNo=T5.DocNo ");
                SQL.AppendLine("    INNER JOIN tblboqhdr T6 ON T5.BOQDocNo=T6.DocNo ");
                SQL.AppendLine("    INNER JOIN tbllophdr T7 ON T6.LOPDocNo=T7.DocNo ");
                SQL.AppendLine(") D On A.DocNo = D.DocNo And A.DNo = D.DNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Specification", "PropCode", 

                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 

                    //11-15
                    "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 

                    //16-20
                    "InventoryUomCode3", "No", "SOCRemark", "PGCode", "ProjectName",

                    //21-23
                    "PONo", "SOContractDocNo", "SOContractDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Grd1.Cells[Row, 20].Value = string.Empty;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnDOCt2DocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmDOCtVerifyDlg(this));
        }

        private void BtnDOCt2DocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDOCt2DocNo, "DO to customer#", false))
            {
                if (mDocType == 1)
                {
                    var f = new FrmDOCt8("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDOCt2DocNo.Text;
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt9("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDOCt2DocNo.Text;
                    f.ShowDialog();
                }
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

        #region Class 

        private class DOCtVerifyHeader
        {
            public string CompanyLogo { get; set; }
            public string CompanyFooterImage { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhoneNumber { get; set; }

            public string LPPBNo { get; set; }
            public string DocDt { get; set; }
            public string DeliveryDt { get; set; }
            public string ReceivedDt { get; set; }
            public string PONo { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string LocalDocNo { get; set; }
            public string CtName { get; set; }
            public string Remark { get; set; }
        }

        private class DOCtVerifyDetail
        {
            public decimal nomor { get; set; }
            public decimal SOContractNo { get; set; }
            public string ItInternalCode { get; set; }
            public string ItName { get; set; }
            public string SOContractRemark { get; set; }
            public string UOM { get; set; }
            public decimal Verified { get; set; }
            public string Remark { get; set; }
            public string ItCodeDismantle { get; set; }
            public string ItNameDismantle { get; set; }
            public decimal ContractQty { get; set; }
            public decimal TotalProgress { get; set; }
            public string ContractUomCode { get; set; }
            public string DeliveryDt { get; set; }
            public string SOContractDNo { get; set; }
        }

        private class DOCtVerifySign
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        private class ItemDismantle
        {
            public string SOContractDocNo { get; set; }
            public string SOContractDNo { get; set; }
            public string ItCodeDismantle { get; set; }
            public string ItNameDismantle { get; set; }
            public decimal ContractQty { get; set; }
            public string ContractUom { get; set; }
            public string DeliveryDt { get; set; }
        }
        class TotalProgress
        {
            public string SOContractDNo { get; set; }
            public decimal TotalProgressDO { get; set; }
        }
        #endregion
        
    }
}
