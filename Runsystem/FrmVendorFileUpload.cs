﻿#region Update
/*
    28/07/2020 [VIN/SRN] new app
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;




#endregion

namespace RunSystem
{
    public partial class FrmVendorFileUpload : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mFileLocationName = string.Empty;
        internal FrmVendorFileUploadFind FrmFind;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;


        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmVendorFileUpload(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
            GetParameter();
            Sl.SetLueOption(ref LueStatus, "VendorUploadFileStatus");
            Sl.SetLueVdCode(ref LueVdCode);
            SetLueFileCategory(ref LueFileCategory, string.Empty, string.Empty);

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueVdCode, LueFileCategory, LueStatus, TxtFile, TxtLicensed, 
                        TxtLocalDocNo, DteDocDt1, DteDocDt2 
                    }, true);
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    if (Sm.GetLue(LueVdCode).Length > 0)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    LueVdCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueVdCode, LueFileCategory, TxtFile, TxtLicensed, 
                        TxtLocalDocNo, DteDocDt1, DteDocDt2 
                    }, false);
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = false;
                    ChkFile.Enabled = true;
                    LueVdCode.Focus();
                    break;
                case mState.Edit:
                    if (LueStatus.Text == "Approved")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            { 
                                LueStatus
                            }, false);
                    }
                    
                    LueStatus.Focus();
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Method

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueVdCode, LueFileCategory, LueStatus, TxtFile, TxtLicensed, 
                TxtLocalDocNo, DteDocDt1, DteDocDt2 
            });
            mFileLocationName = null;
            ChkFile.Checked = false;
            PbUpload.Value = 0;
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorFileUploadFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueVdCode, "Vendor")) return;
            SetFormControl(mState.Edit);

        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {

        }

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditVendorUploadFile(Sm.GetLue(LueVdCode), Sm.GetLue(LueFileCategory), TxtFile.Text, Sm.GetLue(LueStatus));
            //ShowData(Sm.GetLue(LueVdCode), Sm.GetLue(LueFileCategory), TxtFile.Text, Sm.GetLue(LueStatus));

        }

        private void EditVendorUploadFile(string VdCode, string CategoryCode, string Status, string FileLocation)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update tblvendorfileupload Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CategoryCode=@CategoryCode And VdCode= @VdCode And Status=@Status And FileLocation=@FileLocation; ");



            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@CategoryCode", CategoryCode);
            Sm.CmParam<String>(ref cm, "@Status", Status);
            Sm.CmParam<String>(ref cm, "@FileLocation", FileLocation);

            Sm.ExecCommand(cm);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                string VdCode = Sm.GetLue(LueVdCode);
                string Category = Sm.GetLue(LueFileCategory);
                string Status = Sm.GetLue(LueStatus);
                string FileLocation = TxtFile.Text;
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("INSERT INTO tblvendorfileupload (VdCode, CategoryCode, Status, FileLocation, LocalDocNo, LicensedBy, StartDt, EndDt, CreateBy, CreateDt) ");
                SQL.AppendLine("VALUES(@VdCode, @CategoryCode, 'A', @FileLocation, @LocalDocNo, @LicensedBy, @StartDt, @EndDt, @UserCode, CurrentDateTime()) ");

                SQL.AppendLine("   On Duplicate Key  ");
                SQL.AppendLine("        Update Status=@Status, VdCode=@VdCode, CategoryCode=@CategoryCode, FileLocation=@FileLocation, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
                if (LueStatus.Text == "Cancelled")
                    SQL.AppendLine(" delete from tblvendorfileupload where Status='A' and VdCode=@VdCode and CategoryCode=@CategoryCode and FileLocation=@FileLocation  ");


                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                Sm.CmParam<String>(ref cm, "@CategoryCode", Sm.GetLue(LueFileCategory));
                Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
                Sm.CmParam<String>(ref cm, "@FileLocation", TxtFile.Text);
                Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
                Sm.CmParam<String>(ref cm, "@LicensedBy", TxtLicensed.Text);
                Sm.CmParam<String>(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
                Sm.CmParam<String>(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


                Sm.ExecCommand(cm);

                if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1" && Sm.GetLue(LueStatus).Length == 0)
                    UploadFile(VdCode, Category, FileLocation);

                if (Sm.GetLue(LueStatus).Length == 0)
                    ShowData(VdCode, Category, mFileLocationName, "A");
                else
                    ShowData(VdCode, Category, FileLocation, Status);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }


        #endregion

        #region Show Data

        public void ShowData(string VdCode, string CategoryCode, string FileLocation, string Status)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@CategoryCode", CategoryCode);
                Sm.CmParam<String>(ref cm, "@FileLocation", FileLocation);
                Sm.CmParam<String>(ref cm, "@Status", Status);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "SELECT VdCode, CategoryCode, FileLocation, Status, CategoryCode, LocalDocNo, LicensedBy, StartDt, EndDt " +
                        "FROM tblvendorfileupload " +
                        "Where CategoryCode=@CategoryCode And VdCode= @VdCode And Status=@Status And FileLocation=@FileLocation ",

                        new string[] 
                        {
                            "VdCode",
                            "CategoryCode", "Status", "FileLocation", "LocalDocNo", "LicensedBy", 
                            "StartDt", "EndDt"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[0]));
                            if (Sm.GetLue(LueStatus).Length == 0)
                            {
                                SetLueFileCategory(ref LueFileCategory, string.Empty, Sm.DrStr(dr, c[1]));
                                Sm.SetLue(LueFileCategory, Sm.DrStr(dr, c[1]));
                            }
                            else
                                SetLueFileCategory(ref LueFileCategory, string.Empty, Sm.DrStr(dr, c[1]));

                            Sm.SetLue(LueStatus, Sm.DrStr(dr, c[2]));
                            TxtFile.EditValue = Sm.DrStr(dr, c[3]);
                            TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtLicensed.EditValue = Sm.DrStr(dr, c[5]);
                            Sm.SetDte(DteDocDt1, Sm.DrStr(dr, c[6]));
                            Sm.SetDte(DteDocDt2, Sm.DrStr(dr, c[7]));

                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsLueEmpty(LueFileCategory, "Category") ||
                Sm.IsTxtEmpty(TxtFile, "Attachment File", false);
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                //FormatFTPClient = 1
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                
                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string VdCode, string CategoryCode, string FileLocation)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            //FormatFTPClient = 1
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            
            //FormatFTPClient = 2
            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateVdFile(VdCode, CategoryCode, toUpload.Name));
            mFileLocationName = toUpload.Name;
            Sm.ExecCommands(cml);
        }
        

        private MySqlCommand UpdateVdFile(string VdCode, string CategoryCode, string FileLocation)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update tblvendorfileupload  ");
            SQL.AppendLine("    Set FileLocation=@FileLocation ");
            SQL.AppendLine("Where VdCode=@VdCode  and CategoryCode=@CategoryCode  and Status='A'  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@FileLocation", FileLocation);
            Sm.CmParam<String>(ref cm, "@CategoryCode", CategoryCode);
            return cm;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select VdCode From tblvendorfileupload ");
                SQL.AppendLine("Where vdcode=@vdcode and FileLocation=@FileLocation and Status=@Status and CategoryCode=@CategoryCode");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileLocation", toUpload.Name);
                Sm.CmParam<String>(ref cm, "@vdcode", Sm.GetLue(LueVdCode));
                Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
                Sm.CmParam<String>(ref cm, "@CategoryCode", Sm.GetLue(LueFileCategory));



                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        public static void SetLueFileCategory(ref LookUpEdit Lue, string VdCode, string CategoryCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();


            SQL.AppendLine("Select CategoryCode As Col1, CategoryName As Col2 From TblVendorFileUploadCategory ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (CategoryCode.Length != 0)
                SQL.AppendLine("And CategoryCode=@CategoryCode");
            else
            {
                SQL.AppendLine("And CategoryCode Not In ( ");
                SQL.AppendLine("Select CategoryCode From TblVendorFileUpload Where VdCode=@VdCode And Status = 'A') Order By CategoryName; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@CategoryCode", CategoryCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");


        }

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            SetLueFileCategory(ref LueFileCategory ,Sm.GetLue(LueVdCode), string.Empty);
        }

        private void LueFileCategory_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFileCategory, new Sm.RefreshLue3(SetLueFileCategory), Sm.GetLue(LueVdCode), string.Empty);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue2(Sl.SetLueOption), "VendorUploadFileStatus");

        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        #endregion

        
    }
}
