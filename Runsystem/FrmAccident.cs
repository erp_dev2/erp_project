﻿#region Update
/*
    09/07/2018 [TKG] Injured description bisa diedit
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAccident : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        public string DeptCodeOld, PosCodeOld, GrdLvlCodeOld;
        internal FrmAccidentFind FrmFind;

        #endregion

        #region Constructor

        public FrmAccident(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Accident";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        # region Standart Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeImmediate, ChkCancelInd, DteAcc, TmeAcc, 
                        MeeInjured, LueDeptCode, MeeLocation, MeeRemark 
                    }, true);
                    BtnEmpCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, MeeImmediate, DteAcc, TmeAcc, 
                        MeeInjured, MeeLocation, MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeInjured }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                    
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeImmediate, DteAcc, TmeAcc, 
                TxtEmpCode, TxtEmpName, TxtDeptName, TxtPosition, MeeInjured, 
                LueDeptCode, MeeLocation, MeeRemark 
            });
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAccidentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Accident", "TblAccident");
            string EmpCode = TxtEmpCode.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAccident(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document's date") ||
                Sm.IsDteEmpty(DteAcc, "Date") ||
                Sm.IsTmeEmpty(TmeAcc, "Time") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false);
        }

        private MySqlCommand SaveAccident(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into tblaccident(DocNo, DocDt, CancelInd, ImmediateDesc, Dt, Tm, EmpCode, InjuredDesc, DeptCode, LocationDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @ImmediateDesc, @Dt, @Tm, @EmpCode, @InjuredDesc, @DeptCode, @LocationDesc, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ImmediateDesc", MeeImmediate.Text);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteAcc));
            Sm.CmParam<String>(ref cm, "@Tm", Sm.GetTme(TmeAcc));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@InjuredDesc", MeeInjured.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@LocationDesc", MeeLocation.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAccident());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAccident Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand EditAccident()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAccident Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ");
            SQL.AppendLine("    InjuredDesc=@InjuredDesc, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@InjuredDesc", MeeInjured.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAccident(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAccident(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ImmediateDesc, A.Dt As Date, A.Tm As Time, A.EmpCode, B.EmpName, ");
            SQL.AppendLine("C.DeptName, D.PosName, A.InjuredDesc, A.DeptCode, A.LocationDesc, A.Remark ");
            SQL.AppendLine("From TblAccident A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ImmediateDesc", "Date", "Time",

                        //6-10
                        "EmpCode", "EmpName", "DeptName", "PosName", "InjuredDesc", 

                        //11-13
                         "DeptCode", "LocationDesc", "Remark"

                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    MeeImmediate.EditValue = Sm.DrStr(dr, c[3]);
                    Sm.SetDte(DteAcc, Sm.DrStr(dr, c[4]));
                    Sm.SetTme(TmeAcc, Sm.DrStr(dr, c[5]));
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtDeptName.EditValue = Sm.DrStr(dr, c[8]);
                    TxtPosition.EditValue = Sm.DrStr(dr, c[9]);
                    MeeInjured.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[11]));
                    MeeLocation.EditValue = Sm.DrStr(dr, c[12]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                }, true
        );

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAccidentDlg(this));
        }

        #endregion

        #endregion
    }
}
