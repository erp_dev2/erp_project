﻿#region
 /*
  25/10/2017 [HAR] BUg fixing saat filter 
  03/01/2022 [ICA/MNET] Oustanding di kalkulasikan juga dengan PI yg ditarik di Net Off Payment
  01/02/2023 [RDA/MNET] List of Purchase Invoice dengan nilai outstanding = 0 tidak dimunculkan
  
  */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAPSDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAPS mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAPSDlg2(FrmAPS FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.VdInvNo, A.VdCode, C.Vdname, A.CurCode, (A.Amt - A.Downpayment + A.TaxAmt) - ifnull(B.AmtInvOp, 0) - ifnull(D.AmtAps, 0)  As OutstandingInv ");
            SQL.AppendLine("From TblPurchaseInvoicehdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.InvoiceDocNo, SUM(T.AMT) AS AmtInvOP ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.InvoiceDocNo, A.Amt ");
            SQL.AppendLine("        From tblOutgoingPaymentDtl A ");
            SQL.AppendLine("        Inner Join Tbloutgoingpaymenthdr B On A.Docno = B.Docno And B.CancelInd = 'N' ");
            SQL.AppendLine("        Where B.Status <> 'C' ");
            SQL.AppendLine("        Union ALL ");
            SQL.AppendLine("        Select B.InvoiceDocNo, B.Amt ");
            SQL.AppendLine("        From TblNetOffPaymentHdr A "); 
            SQL.AppendLine("        Inner Join TblNetOffPaymentDtl2 B On A.DocNo = B.DocNo");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr C On B.InvoiceDocNo = C.DocNo And IfNull(C.ProcessInd, 'O') <> 'F' ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("            And IfNull(A.Status, 'O') <> 'C' ");
            SQL.AppendLine("    )T Group By T.InvoiceDocNo ");
            SQL.AppendLine(")B On B.InvoiceDocNo = A.DocNo");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select  A.PurchaseInvoiceDocNo, SUM(A.Amt) As AmtAps ");
	        SQL.AppendLine("    From TblApsHdr A  ");
	        SQL.AppendLine("    Where A.CancelInd = 'N' ");
	        SQL.AppendLine("    Group by A.PurchaseInvoiceDocNo ");
            SQL.AppendLine(")D On D.PurchaseInvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.ProcessInd <> 'F' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Group By A.DocNo, A.DocDt, A.VdInvNo, C.Vdname, A.CurCode  ");
            SQL.AppendLine(")Z ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "",
                        "Date",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "Vendor",
                        
                        //6-7
                        "Currency",
                        "Outstanding",
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where Z.OutstandingInv > 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Z.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "Z.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Z.DocDt, Z.DocNo;",
                        new string[]
                        {

                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt",  "VdInvNo", "VdName", "CurCode",  "OutstandingInv", 
                                                       
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, ((Grd1.Rows.Count > 1) ? 1 : 0), 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.ShowDataPurchaseInvoice(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #region Grid Method
        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }           
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
       
    }
}
