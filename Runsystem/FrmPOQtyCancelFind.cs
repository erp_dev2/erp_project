﻿#region Update
// 27/20/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
// 05/01/2022 [RIS/PHT] Membuat terfilter item category dengan param IsFilterByItCt
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPOQtyCancelFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPOQtyCancel mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPOQtyCancelFind(FrmPOQtyCancel FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.NewMRInd, A.PODocNo, F.VdName, E.ItCode, G.ItName, A.Qty, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo=E.DocNo And D.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Left join TblVendor F On B.VdCode = F.VdCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("INNER Join TblItem G On E.ItCode = G.ItCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=G.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblItem G On E.ItCode = G.ItCode ");
            }
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("Inner Join TblQtHdr H On D.QtDocNo=H.DocNo ");
                SQL.AppendLine("    And (H.PtCode Is Null Or ( ");
                SQL.AppendLine("    And H.PtCode Is Not Null ");
                SQL.AppendLine("    And H.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestHdr I On E.DocNo=I.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (I.SiteCode Is Null Or ( ");
                SQL.AppendLine("    I.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(I.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        (Doctitle=="IMS") ? "New PR" : "New MR", 
                        "PO#",
                        
                        //6-10
                        "Vendor",
                        "Item's"+Environment.NewLine+"Code",
                        "Item'sName",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "Created"+Environment.NewLine+"By",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkNewMRInd.Checked)
                    Filter += " And A.NewMRInd='Y' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "A.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "G.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "NewMRInd", "PODocNo", "VdName", 
                            
                            //6-10
                            "ItCode", "ItName", "Qty", "CreateBy", "CreateDt",  
                            
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
