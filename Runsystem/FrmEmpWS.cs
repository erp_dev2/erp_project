﻿#region  Update
/*
    16/08/2017 [TKG] tetap menampilkan karyawan yg resign sebelum masanya.
    21/09/2017 [TKG] mengganti proses penyimpanan spy jauh lbh cepat
    06/11/2017 [TKG] work schedule berdasarkan site
    20/11/2017 [TKG] 
        menggunakan parameter IsFilterBySiteHR untuk filter site berdasarkan group
        menggunakan parameter IsFilterByDeptHR untuk filter department berdasarkan group
    15/01/2018 [HAR] tambah warna biru untuk tanggal yang sdh dipake leave request (leave sdh di approve)
    13/03/2018 [TKG] bug employee tanpa AG tidak muncul
    28/06/2018 [TKG] tambah informasi jumlah employee
    27/08/2018 [TKG] Working Schedule Amandment ada warna hijau saat karyawan mendapat training assignment 
    16/08/2019 [TKG] Routine holiday tetap ditampilkan.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmEmpWS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty;
        private string
            mWSCodeForNationalHoliday = string.Empty,
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty;
        private bool
            mIsNationalHolidayAutoUpdateWorkSchedule = false,
            mIsWorkScheduleBasedOnSite = false,
            mIsEmpExchangeHolidaysActived = false,
            mIsEmpWSDtlSaved = false;
        internal bool
            mIsFilterBySite = false,
            mIsSiteMandatory = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false;
        private int mCol = -1;
        iGCell fCell;
        bool fAccept;


        internal FrmEmpWSFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpWS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Employee's Working Schedule Amendment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                //Sl.SetLueDeptCode(ref LueDeptCode);
                //Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueAGCode(ref LueAGCode);
                if (!mIsWorkScheduleBasedOnSite) Sl.SetLueWSCode(ref LueWSCode);
                if (mIsSiteMandatory || mIsWorkScheduleBasedOnSite) LblSiteCode.ForeColor = Color.Red;
                LueWSCode.Visible = false;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mWSCodeForNationalHoliday = Sm.GetParameter("WSCodeForNationalHoliday");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mIsNationalHolidayAutoUpdateWorkSchedule = Sm.GetParameterBoo("IsNationalHolidayAutoUpdateWorkSchedule");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsWorkScheduleBasedOnSite = Sm.GetParameterBoo("IsWorkScheduleBasedOnSite");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsEmpExchangeHolidaysActived = Sm.GetParameterBoo("IsEmpExchangeHolidaysActived");
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Employee"+Environment.NewLine+"Code",
                        
                        //1-5
                        "Employee Name",
                        "Old Code",
                        "Position",
                        "Department",
                        "Attendance"+Environment.NewLine+"Group",

                        //6
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        200, 80, 150, 150, 150,

                        //6
                        150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 6 }, false);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, 
                        DteStartDt, DteEndDt, LueDeptCode, LueAGCode, LueSiteCode, 
                        TxtEmpCode, ChkEmpCode, MeeRemark 
                    }, true);
                    BtnRefreshData.Enabled = false;
                    BtnCopyPattern.Enabled = false;
                    BtnCopyData.Enabled = false;
                    BtnClearData.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, 
                        DteStartDt, DteEndDt, LueDeptCode, LueAGCode, LueSiteCode, 
                        TxtEmpCode, ChkEmpCode, MeeRemark 
                    }, false);
                    BtnRefreshData.Enabled = true;
                    BtnCopyPattern.Enabled = true;
                    BtnCopyData.Enabled = true;
                    BtnClearData.Enabled = true;
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo,
                TxtStatus, DteDocDt, DteStartDt, DteEndDt, LueDeptCode, 
                LueAGCode, LueSiteCode, TxtEmpCode, MeeRemark, LueWSCode 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtNoOfEmployees }, 0);
            ChkEmpCode.Checked = false;
            ClearGrd1();
            Sm.ClearGrd(Grd2, true);
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Cols.Count = 7;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpWSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteStartDt.DateTime = DteDocDt.DateTime;
                DteEndDt.DateTime = DteDocDt.DateTime;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            NationalHolidayAutoUpdateWorkSchedule();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpWS", "TblEmpWSHdr");
            var l = new List<EmpWorkSchedule>();

            PrepareData(ref l);

            var cml = new List<MySqlCommand>();

            //l.ForEach(i => { cml.Add(SaveEmpWSDtl(DocNo, ref i)); });

            cml.Add(SaveEmpWSDtl(DocNo, ref l));

            cml.Add(SaveEmpWSHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsGrdEmpty())
                return true;

            if (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site"))
                return true;

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpWSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpWSHdr(DocNo, DocDt, Status, StartDt, EndDt, DeptCode, AGCode, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @StartDt, @EndDt, @DeptCode, @AGCode, @SiteCode, @Remark, @CreateBy, CurrentDateTime()); ");

            if (mIsEmpExchangeHolidaysActived)
            {
                SQL.AppendLine("Insert Into TblEmpExchangeHolidays ");
                SQL.AppendLine("(DocNo, EmpCode, Dt, WSCodeFrom, WSCodeTo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select Distinct C.DocNo, A.EmpCode, A.Dt, B.WSCode, E.WSCode, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblEmpWorkSchedule A ");
                SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode And B.HolidayInd='Y' ");
                SQL.AppendLine("Inner Join TblEmpWSDtl C On A.EmpCode=C.EmpCode And A.Dt=C.Dt And C.DocNo=@DocNo ");
                SQL.AppendLine("Inner Join TblEmpWSHdr D On C.DocNo=D.DocNo And D.Status In ('O', 'A') ");
                SQL.AppendLine("Inner Join TblWorkSchedule E On C.WSCode=E.WSCode And E.HolidayInd='N'; ");
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DeptCode In (Select DeptCode From TblEmpWSHdr Where DocNo=@DocNo) ");
            if (mIsSiteMandatory)
                SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
            SQL.AppendLine("And T.DocType='EmpWS'; ");

            SQL.AppendLine("Update TblEmpWSHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblDocApproval Where DocType='EmpWS' And DocNo=@DocNo); ");

            //SQL.AppendLine("Delete A.* From TblEmpWorkSchedule A ");
            //SQL.AppendLine("Inner join TblEmpWSHdr B On B.DocNo=@DocNo And B.Status='A' ");
            //SQL.AppendLine("Inner join TblEmpWSDtl C On C.DocNo=@DocNo And A.EmpCode=C.EmpCode And A.Dt=C.Dt; ");

            //SQL.AppendLine("Insert Into TblEmpWorkSchedule(EmpCode, Dt, WSCode, ExchangeHolidayInd, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select T2.EmpCode, T2.Dt, T2.WSCode, 'N', T2.CreateBy, T2.CreateDt ");
            //SQL.AppendLine("From TblEmpWSHdr T1, TblEmpWSDtl T2 ");
            //SQL.AppendLine("Where T1.DocNo=@DocNo And T1.DocNo=T2.DocNo And T1.Status='A'; ");

            SQL.AppendLine("Update TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblEmpWSDtl B On A.EmpCode=B.EmpCode And A.Dt=B.Dt And B.DocNo=@DocNo ");
            SQL.AppendLine("Inner Join TblEmpWSHdr C On B.DocNo=C.DocNo And C.Status='A' ");
            SQL.AppendLine("Set A.WSCode=B.WSCode, A.LastUpBy=@CreateBy, A.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblEmpWorkSchedule(EmpCode, Dt, WSCode, ExchangeHolidayInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T2.EmpCode, T2.Dt, T2.WSCode, 'N', T2.CreateBy, T2.CreateDt ");
            SQL.AppendLine("From TblEmpWSHdr T1, TblEmpWSDtl T2 ");
            SQL.AppendLine("Where T1.DocNo=@DocNo ");
            SQL.AppendLine("And T1.DocNo=T2.DocNo ");
            SQL.AppendLine("And T1.Status='A' ");
            SQL.AppendLine("And Not Exists(Select 1 From TblEmpWorkSchedule Where EmpCode=T2.EmpCode And Dt=T2.Dt);");

            if (mIsEmpExchangeHolidaysActived)
            {
                SQL.AppendLine("Update TblEmpWorkSchedule A ");
                SQL.AppendLine("Inner Join TblEmpExchangeHolidays B On B.DocNo=@DocNo And A.EmpCode=B.EmpCode And A.Dt=B.Dt ");
                SQL.AppendLine("Inner Join TblEmpWSHdr C On B.DocNo=C.DocNo And C.Status='A' ");
                SQL.AppendLine("Set A.ExchangeHolidayInd='Y'  ");
                SQL.AppendLine("Where A.ExchangeHolidayInd='N'; ");
            }

            //SQL.AppendLine("Update TblEmpWorkSchedule A ");
            //SQL.AppendLine("Inner join TblEmpWSHdr B On B.DocNo=@DocNo And B.Status='A' ");
            //SQL.AppendLine("Inner join TblEmpWSDtl C On C.DocNo=@DocNo And A.EmpCode=C.EmpCode And A.Dt=C.Dt ");
            //SQL.AppendLine("Set A.WSCode=C.WSCode, A.LastUpBy=C.CreateBy, A.LastUpDt=C.CreateDt;");

            //SQL.AppendLine("Insert Into TblEmpWorkSchedule(EmpCode, Dt, WSCode, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select T2.EmpCode, T2.Dt, T2.WSCode, T2.CreateBy, T2.CreateDt ");
            //SQL.AppendLine("From TblEmpWSHdr T1, TblEmpWSDtl T2 ");
            //SQL.AppendLine("Where T1.DocNo=@DocNo And T1.DocNo=T2.DocNo And T1.Status='A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select 1 From TblEmpWorkSchedule ");
            //SQL.AppendLine("    Where EmpCode=T2.EmpCode And Dt=T2.Dt ");
            //SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveEmpWSDtl(string DocNo, ref List<EmpWorkSchedule> l)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpWSDtl(DocNo, EmpCode, Dt, WSCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int x = 0;
            var cm = new MySqlCommand();

            l.ForEach(i =>
            {
                x += 1;
                if (x != 1)
                    SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo" + x + ", @EmpCode" + x + ", @Dt" + x + ", @WSCode" + x + ", @UserCode, @CurrentDateTime)");

                Sm.CmParam<String>(ref cm, "@DocNo" + x, DocNo);
                Sm.CmParam<String>(ref cm, "@EmpCode" + x, i.EmpCode);
                Sm.CmParam<String>(ref cm, "@Dt" + x, i.Dt);
                Sm.CmParam<String>(ref cm, "@WSCode" + x, i.WSCode);

            });
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurrentDateTime", Sm.Left(Sm.ServerCurrentDateTime(), 12));
            cm.CommandText = SQL.ToString();
            return cm;
        }

        //private MySqlCommand SaveEmpWSDtl(string DocNo, ref EmpWorkSchedule i)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmpWSDtl(DocNo, EmpCode, Dt, WSCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @EmpCode, @Dt, @WSCode, @UserCode, CurrentDateTime());");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", i.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@Dt", i.Dt);
        //    Sm.CmParam<String>(ref cm, "@WSCode", i.WSCode);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    return cm;
        //}

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpWSHdr(DocNo);
                ShowEmpWSDtl(DocNo);
                ShowDocApproval(DocNo);
                ComputeNumberOfEmployees();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpWSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("(Case Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, ");
            SQL.AppendLine("StartDt, EndDt, DeptCode, AGCode, SiteCode, Remark ");
            SQL.AppendLine("From TblEmpWSHdr Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "StatusDesc", "StartDt", "EndDt", "DeptCode", 
                        
                        //6-8
                        "AGCode", "SiteCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[4]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), string.Empty);
                        SetLueAGCode(ref LueAGCode, string.Empty, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueAGCode, Sm.DrStr(dr, c[6]));
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[7]), string.Empty);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowEmpWSDtl(string DocNo)
        {
            ShowEmployee(DocNo);
            ShowDate();
            ShowHoliday();
            ShowLeave();
            ShowTrainingAssignment();

            var l = new List<EmpWorkSchedule>();

            GetData(DocNo, ref l);
            ShowData(ref l);
        }

        private void ShowEmployee(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, ");
            SQL.AppendLine("C.DeptName, D.AGName, E.SiteName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On C.DeptCode=@DeptCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpHdr D On D.AGCode=@AGCode ");
            SQL.AppendLine("Left Join TblSite E On E.SiteCode=@SiteCode ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select 1 From TblEmpWSDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And EmpCode=A.EmpCode ");
            SQL.AppendLine(") Order By A.EmpName;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "AGName",

                            //6
                            "SiteName"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 0).Length > 0) Grd1.Rows.Add();
        }

        private void GetData(string DocNo, ref List<EmpWorkSchedule> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, B.WSName, B.HolidayInd ");
            SQL.AppendLine("From TblEmpWSDtl A, TblWorkSchedule B ");
            SQL.AppendLine("Where A.WSCode=B.WSCode And A.DocNo=@DocNo Order By A.EmpCode, A.Dt;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "WSCode", "WSName", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            WSName = Sm.DrStr(dr, c[3]),
                            IsHoliday = Sm.DrStr(dr, c[4]) == "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpWS' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ComputeNumberOfEmployees()
        {
            var NumberOfEmployees = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                    NumberOfEmployees += 1;
            TxtNoOfEmployees.EditValue = Sm.FormatNum(NumberOfEmployees, 11);
        }


        private void CopyPattern(int Row)
        {
            int TempCol = -1;
            for (int Col = 7; Col < Grd1.Cols.Count; Col++)
                if (Sm.GetGrdStr(Grd1, Row, Col).Length != 0) TempCol = Col;

            if (TempCol == -1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern existed.");
                return;
            }

            if (TempCol == Grd1.Cols.Count - 1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern copied.");
                return;
            }

            var l = new List<WorkSchedulePattern>();

            for (int Col = 7; Col <= TempCol; Col++)
                l.Add(new WorkSchedulePattern()
                {
                    Pattern = Sm.GetGrdStr(Grd1, Row, Col),
                    PatternColor = Grd1.Cells[Row, Col].ForeColor
                });

            var PatternCount = l.Count;
            int PatternIndex = 0;
            for (int Col = TempCol + 1; Col < Grd1.Cols.Count; Col++)
            {
                Grd1.Cells[Row, Col].Value = l[PatternIndex].Pattern;
                Grd1.Cells[Row, Col].ForeColor = l[PatternIndex].PatternColor;
                if (PatternIndex == PatternCount - 1)
                    PatternIndex = 0;
                else
                    PatternIndex += 1;
            }
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, mCol - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string DeptCode, string AGCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where (ActInd='Y' ");
            if (AGCode.Length != 0)
                SQL.AppendLine("Or AGCode='" + AGCode + "' ");
            SQL.AppendLine(") ");
            if (DeptCode.Length != 0)
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                if (SiteCode.Length != 0)
                    SQL.AppendLine("Or T.SiteCode='" + SiteCode + "' ");
                SQL.AppendLine(") ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PrepareData(ref List<EmpWorkSchedule> l)
        {
            int Col = 7;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Grd1.Cols.Count > 6)
                {
                    Col = 7;
                    while (Col < Grd1.Cols.Count)
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.GetGrdStr(Grd1, Row, 0),
                            Dt = Grd1.Header.Cells[0, Col].Value.ToString(),
                            WSCode = Sm.GetGrdStr(Grd1, Row, Col)
                        });
                        Col += 2;
                    }
                }
            }
        }

        //private void NationalHolidayAutoUpdateWorkSchedule()
        //{
        //    if (!mIsNationalHolidayAutoUpdateWorkSchedule) return;
        //    if (mWSCodesForNationalHoliday.Length == 0 || mWSCodesForHoliday.Length == 0) return;

        //    var cm = new MySqlCommand();
        //    cm.CommandText = "Select WSName from TblWorkSchedule Where WSCode=@WSCode;";
        //    Sm.CmParam<String>(ref cm, "@WSCode", mWSCodesForNationalHoliday);
        //    var WSNameForNationalHoliday = Sm.GetValue(cm);

        //    for (int i = 7; i < Grd1.Cols.Count - 1; i++)
        //    {
        //        if (Grd1.Header.Cells[0, i + 1].BackColor == Color.Pink)
        //        {
        //            for (int r = 0; r < Grd1.Rows.Count; r++)
        //            {
        //                if (
        //                    Sm.GetGrdStr(Grd1, r, 0).Length > 0 &&
        //                    !Sm.CompareStr(Sm.GetGrdStr(Grd1, r, i), mWSCodeForHoliday) &&
        //                    !Sm.CompareStr(Sm.GetGrdStr(Grd1, r, i), mWSCodeForNationalHoliday)
        //                    )
        //                {
        //                    Grd1.Cells[r, i].Value = mWSCodeForNationalHoliday;
        //                    Grd1.Cells[r, i + 1].Value = WSNameForNationalHoliday;
        //                    Grd1.Cells[r, i + 1].ForeColor = Color.Red;
        //                    Grd1.Cells[r, i + 1].BackColor = Color.Pink;
        //                }
        //            }
        //        }
        //    }
        //}

        private void NationalHolidayAutoUpdateWorkSchedule()
        {
            if (!mIsNationalHolidayAutoUpdateWorkSchedule ||
                mWSCodeForNationalHoliday.Length == 0 ||
                mWSCodesForNationalHoliday.Length == 0 ||
                mWSCodesForHoliday.Length == 0) return;

            NationalHolidayAutoUpdateWorkSchedule(
                mWSCodesForNationalHoliday.Split('#').Union(mWSCodesForHoliday.Split('#')).ToArray());
        }

        private void NationalHolidayAutoUpdateWorkSchedule(string[] AllHolidays)
        {
            bool IsNeedUpdate = true;
            var cm = new MySqlCommand();
            cm.CommandText = "Select WSName from TblWorkSchedule Where WSCode=@WSCode;";
            Sm.CmParam<String>(ref cm, "@WSCode", mWSCodeForNationalHoliday);
            var WSNameForNationalHoliday = Sm.GetValue(cm);

            for (int i = 7; i < Grd1.Cols.Count - 1; i++)
            {
                if (Grd1.Header.Cells[0, i + 1].BackColor == Color.Pink)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                        {
                            IsNeedUpdate = true;
                            foreach (string h in AllHolidays)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, i), h))
                                {
                                    IsNeedUpdate = false;
                                    break;
                                }
                            }
                            if (IsNeedUpdate)
                            {
                                Grd1.Cells[r, i].Value = mWSCodeForNationalHoliday;
                                Grd1.Cells[r, i + 1].Value = WSNameForNationalHoliday;
                                Grd1.Cells[r, i + 1].ForeColor = Color.Red;
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                            }
                        }
                    }
                }
            }
        }

        private void ShowEmployee()
        {
            string Filter = " ", Filter2 = " ";

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAGCode), "T1.AGCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
            Sm.FilterStr(ref Filter2, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, ");
            SQL.AppendLine("C.DeptName, D.AGName, E.SiteName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
            if (Filter != " ")
                SQL.AppendLine("Inner ");
            else
                SQL.AppendLine("Left ");
            SQL.AppendLine("    Join ( ");
            SQL.AppendLine("    Select T1.AGCode, T1.AGName, T2.EmpCode ");
            SQL.AppendLine("    From TblAttendanceGrpHdr T1 ");
            SQL.AppendLine("    Inner Join TblAttendanceGrpDtl T2 On T1.AGCode=T2.AGCode ");
            SQL.AppendLine("    Inner Join TblEmployee T3 ");
            SQL.AppendLine("        On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And T3.DeptCode=@DeptCode ");
            SQL.AppendLine("        And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>@CurrentDate)) ");
            SQL.AppendLine(Filter2.Replace("A.", "T3."));
            SQL.AppendLine("    Where T1.ActInd='Y' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
            SQL.AppendLine(Filter2);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString() + " Order By A.EmpName;",
                    new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "AGName",

                            //6
                            "SiteName"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 0).Length > 0) Grd1.Rows.Add();
        }

        private void ShowDate()
        {
            var DtMin = Sm.GetDte(DteStartDt);
            var DtMax = Sm.GetDte(DteEndDt);

            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 0; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                Grd1.Cols.Count = 7 + ((TotalDays + 1) * 2);
                int Col = 7;

                Grd1.BeginUpdate();
                s.ForEach(i =>
                {
                    Grd1.Header.Cells[0, Col].Value = i;
                    Grd1.Cols[Col].Width = 100;
                    Grd1.Cols[Col].Visible = false;
                    Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.MiddleCenter;

                    Grd1.Header.Cells[0, Col + 1].Value =
                        Sm.Right(i, 2) + "-" +
                        CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(
                            int.Parse(i.Substring(4, 2))
                            ) + "-" +
                        Sm.Left(i, 4) + Environment.NewLine +
                        TempDt.DayOfWeek.ToString();
                    Grd1.Cols[Col + 1].Width = 100;
                    Grd1.Header.Cells[0, Col + 1].TextAlign = iGContentAlignment.MiddleCenter;

                    if ((int)TempDt.DayOfWeek == 0)
                    {
                        for (int r = 0; r < Grd1.Rows.Count; r++)
                            Grd1.Cells[r, Col + 1].BackColor = Color.LightSalmon;
                    }

                    TempDt = TempDt.AddDays(1);
                    Col += 2;
                });
                Grd1.EndUpdate();
            }
        }

        private void ShowHoliday()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<Holiday>();
            string 
                StartDt = Sm.GetDte(DteStartDt),
                EndDt = Sm.GetDte(DteEndDt);
            int Yr1 = int.Parse(Sm.Left(StartDt, 4));
            int Yr2 = int.Parse(Sm.Left(EndDt, 4));

            SQL.AppendLine("Select Distinct HolDt From ( ");
            SQL.AppendLine("Select HolDt From TblHoliday ");
            SQL.AppendLine("Where HolDt Between @StartDt And @EndDt ");
            for(int i = Yr1;i<=Yr2;i++)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) As HolDt From TblHoliday ");
                SQL.AppendLine("Where RoutineInd='Y' ");
                SQL.AppendLine("And HolDt<=@EndDt ");
                SQL.AppendLine("And Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) Between @StartDt And @EndDt  ");

                Sm.CmParam<string>(ref cm, "@Yr" + i.ToString(), i.ToString());
            }
            SQL.AppendLine(") T Order By HolDt;");
            Sm.CmParamDt(ref cm, "@StartDt", StartDt);
            Sm.CmParamDt(ref cm, "@EndDt", EndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Holiday()
                        {
                            HolDt = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0 && Grd1.Cols.Count > 7)
            {
                l.ForEach(h =>
                {
                    for (int i = 7; i < Grd1.Cols.Count; i++)
                    {
                        if (Sm.CompareStr(h.HolDt, Grd1.Header.Cells[0, i].Value.ToString()))
                        {
                            Grd1.Header.Cells[0, i + 1].Value += (Environment.NewLine + "HOLIDAY");
                            Grd1.Header.Cells[0, i + 1].BackColor = Color.Pink;
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                        }
                    }
                });
            }
            Grd1.Rows.AutoHeight();
        }

        private void ShowLeave()
        {
            ProcessDataEmpLeave();
        }

        //list employee leave
        private void ProcessDataEmpLeave()
        {
            var lEL = new List<EmpLeave>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.Left(Sm.GetDte(DteStartDt), 8));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteEndDt), 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select A.EmpCode, B.LeaveDt, C.leavename  ");
            SQL.AppendLine("From TblEmpLeavehdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join Tblleave C On A.LeaveCode = C.leaveCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.Empcode=D.EmpCode ");
            SQL.AppendLine("Where A.cancelind = 'N' And A.status = 'A' And D.DeptCode=@DeptCode ");
            SQL.AppendLine("And B.LeaveDt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode,  B.LeaveDt, C.leaveName ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "LeaveDt", "LeaveName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEL.Add(new EmpLeave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            LeaveDt = Sm.DrStr(dr, c[1]),
                            LeaveName = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            if (lEL.Count > 0 && Grd1.Cols.Count > 7)
            {
                lEL.ForEach(h =>
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(h.EmpCode, Sm.GetGrdStr(Grd1, Row, 0)))
                        {
                            for (int i = 7; i < Grd1.Cols.Count; i++)
                            {
                                if (Sm.CompareStr(h.LeaveDt, Grd1.Header.Cells[0, i].Value.ToString()))
                                {
                                    Grd1.Cells[Row, i + 1].BackColor = Color.DeepSkyBlue;
                                }
                            }
                        }
                    }
                });
            }
        }

        private void ShowTrainingAssignment()
        {
            var l = new List<EmpTrainingAssignment>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length <= 0)
                Filter = " And 1=0 ";
            else
                Filter = " And ( " + Filter + ") ";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.Left(Sm.GetDte(DteStartDt), 8));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteEndDt), 8));

            SQL.AppendLine("Select B.EmpCode, B.TrainingSchDt ");
            SQL.AppendLine("From TblTrainingAssignmentHdr A ");
            SQL.AppendLine("Inner Join TblTrainingAssignmentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.TrainingSchDt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.Cancelind = 'N' ");
            SQL.AppendLine("Order By B.EmpCode, B.TrainingSchDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "TrainingSchDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpTrainingAssignment()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            TrainingSchDt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0 && Grd1.Cols.Count > 7)
            {
                l.ForEach(x =>
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(x.EmpCode, Sm.GetGrdStr(Grd1, Row, 0)))
                        {
                            for (int i = 7; i < Grd1.Cols.Count - 1; i++)
                            {
                                if (Grd1.Header.Cells[0, i].Value.ToString().Length > 0)
                                {
                                    if (Sm.CompareStr(x.TrainingSchDt, Grd1.Header.Cells[0, i].Value.ToString()))
                                        Grd1.Cells[Row, i + 1].BackColor = Color.Green;
                                }
                            }
                        }
                    }
                });
            }
        }

        private void GetData(ref List<EmpWorkSchedule> l)
        {
            string Param = string.Empty;
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                        Param += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";
            }
            if (Param.Length == 0) Param = "##XXX##";

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, B.WSName, B.HolidayInd ");
            SQL.AppendLine("From TblEmpWorkSchedule A, TblWorkSchedule B ");
            SQL.AppendLine("Where Locate(Concat('##', A.EmpCode, '##'), @SelectedEmpCode)>0 ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.WSCode=B.WSCode Order By A.EmpCode, A.Dt;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SelectedEmpCode", Param);
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "WSCode", "WSName", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            WSName = Sm.DrStr(dr, c[3]),
                            IsHoliday = Sm.DrStr(dr, c[4]) == "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowData(ref List<EmpWorkSchedule> l)
        {
            if (Grd1.Cols.Count <= 7) return;

            string EmpCode = string.Empty;
            int r = 0;
            int c = 7;
            Grd1.BeginUpdate();
            l.ForEach(i =>
            {
                if (!Sm.CompareStr(EmpCode, i.EmpCode))
                {
                    EmpCode = i.EmpCode;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), i.EmpCode))
                        {
                            r = Row;
                            break;
                        }
                    }
                }
                c = 7;
                while (c < Grd1.Cols.Count)
                {
                    if (Sm.CompareStr(i.Dt, Grd1.Header.Cells[0, c].Value.ToString()))
                    {
                        Grd1.Cells[r, c].Value = i.WSCode;
                        Grd1.Cells[r, c + 1].Value = i.WSName;
                        Grd1.Cells[r, c + 1].ForeColor = i.IsHoliday ? Color.Red : Color.Black;
                        break;
                    }
                    c += 2;
                }
            });
            Grd1.EndUpdate();
        }

        private void SetLueWSCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var SiteCode = Sm.GetLue(LueSiteCode);
            var cm = new MySqlCommand();

            if (SiteCode.Length == 0) SiteCode = "XXXXX";

            SQL.AppendLine("Select WSCode As Col1, WSName As Col2, HolidayInd As Col3 ");
            SQL.AppendLine("From TblWorkSchedule ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SiteCode=@SiteCode ");
            SQL.AppendLine("Order By WSName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.SetLue3(ref Lue, ref cm, 0, 35, 0, false, true, false, "Code", "Name", "Holiday", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            DteEndDt.EditValue = DteStartDt.EditValue;
            ClearGrd1();
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd1();
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            string DeptCode = Sm.GetLue(LueDeptCode);
            LueAGCode.EditValue = null;
            SetLueAGCode(ref LueAGCode, DeptCode, string.Empty);
            ClearGrd1();
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue3(SetLueAGCode), DeptCode, string.Empty);
            ClearGrd1();
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsWorkScheduleBasedOnSite)
                {
                    if (Sm.GetLue(LueSiteCode).Length > 0)
                    {
                        Sm.SetLue(LueWSCode, "<Refresh>");
                        Sm.RefreshLookUpEdit(LueWSCode, new Sm.RefreshLue1(SetLueWSCode));
                    }
                }
                ClearGrd1();
            }
        }

        private void LueWSCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsWorkScheduleBasedOnSite)
                    Sm.RefreshLookUpEdit(LueWSCode, new Sm.RefreshLue1(SetLueWSCode));
                else
                    Sm.RefreshLookUpEdit(LueWSCode, new Sm.RefreshLue1(Sl.SetLueWSCode));
            }
        }

        private void LueWSCode_Leave(object sender, EventArgs e)
        {
            if (LueWSCode.Visible && fAccept)
            {
                Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Black;
                if (Sm.GetLue(LueWSCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, mCol - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, mCol].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, mCol - 1].Value = Sm.GetLue(LueWSCode);
                    Grd1.Cells[fCell.RowIndex, mCol].Value = LueWSCode.GetColumnValue("Col2");
                    if (LueWSCode.GetColumnValue("Col3").ToString() == "Y")
                        Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Red;
                }
                LueWSCode.Visible = false;
            }
        }

        private void LueWSCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0 && e.ColIndex > 6)
            {
                mCol = e.ColIndex;
                LueRequestEdit(Grd1, LueWSCode, ref fCell, ref fAccept, e);
            }
            else
                e.DoDefault = false;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Grd1.CurCell != null && Grd1.CurCell.ColIndex > 7 && e.KeyCode == Keys.Enter && Grd1.CurCell.RowIndex != Grd1.Rows.Count - 1)
            {
                if (Grd1.CurCell.ColIndex == Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex + 1, 8);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex, Grd1.CurCell.ColIndex + 2);
            }
        }

        #endregion

        #region Button Event

        private void BtnClearData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to clear all schedule ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    for (int Col = 7; Col < Grd1.Cols.Count; Col++)
                    {
                        Grd1.Cells[Row, Col].Value = null;
                        Grd1.Cells[Row, Col].ForeColor = Color.Black;
                    }
                }

                Grd1.EndUpdate();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy data ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1 || Sm.GetGrdStr(Grd1, 0, 0).Length == 0) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 1; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        for (int Col = 7; Col < Grd1.Cols.Count; Col++)
                        {
                            Grd1.Cells[Row, Col].Value = Grd1.Cells[0, Col].Value;
                            Grd1.Cells[Row, Col].ForeColor = Grd1.Cells[0, Col].ForeColor;
                        }
                    }
                }

                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void BtnCopyPattern_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy pattern ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) CopyPattern(Row);

                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            try
            {
                ClearGrd1();

                if (
                    Sm.IsDteEmpty(DteStartDt, "Start date") ||
                    Sm.IsDteEmpty(DteEndDt, "End date") ||
                    Sm.IsLueEmpty(LueDeptCode, "Department") ||
                    ((mIsSiteMandatory || mIsWorkScheduleBasedOnSite) && Sm.IsLueEmpty(LueSiteCode, "Site"))
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                ShowEmployee();
                ShowDate();
                ShowHoliday();
                ShowLeave();
                ShowTrainingAssignment();

                var l = new List<EmpWorkSchedule>();

                GetData(ref l);
                ShowData(ref l);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                ComputeNumberOfEmployees();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #endregion

        #region Class

        private class EmpWorkSchedule
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string WSName { get; set; }
            public bool IsHoliday { get; set; }
        }

        private class WorkSchedulePattern
        {
            public string Pattern { get; set; }
            public Color PatternColor { get; set; }
        }

        private class Holiday
        {
            public string HolDt { get; set; }
        }

        private class EmpLeave
        {
            public string EmpCode { get; set; }
            public string LeaveDt { get; set; }
            public string LeaveName { get; set; }
        }

        private class EmpTrainingAssignment
        {
            public string EmpCode { get; set; }
            public string TrainingSchDt { get; set; }
        }

        #endregion
    }
}
