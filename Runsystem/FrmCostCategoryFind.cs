﻿#region Update
/*
    12/02/2018 [TKG] account# berdasarkan otorisasi group thd site/entity
    19/01/2020 [TKG/IMS] tambah alias nomor rekening coa
    14/04/2020 [TKG/IMS] tambah Cost Of Group
    08/10/2020 [ICA/PHT] Menambah kolom Join Cost berdasarkan parameter IsCostCategoryUseJoinCost
    06/11/2020 [TKG/PHT] tambah cost category product
    30/03/2021 [IBL/SRN] tambah filter cost center
    17/06/2021 [TKG/PHT] Berdasarkan parameter IsCostCategoryUseProfitCenter, cost center difilter berdasarkan profit center dan cost center yg muncul adalah cost center yg tidak menjadi parent di cost center yg lain
    29/06/2021 [TRI/IMS] Filter belum jalan di IMS
    14/07/2021 [VIN/PHT] Bug Filter 
    02/11/2021 [HAR/IMS] tambah function export to excell, krn akngka 0 di depan hilang waktu di export ke excell 
    05/07/2021 [VIN/PHT] bug Filter CC
    08/07/2021 [VIN/PHT] bug Filter CC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCostCategory mFrmParent;

        #endregion

        #region Constructor

        public FrmCostCategoryFind(FrmCostCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                if (mFrmParent.mIsCostCategoryUseProfitCenter)
                    Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, string.Empty, "Y");
                else
                    SetLueCCCode(ref LueCCCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Code",
                        "Name",
                        "Join Cost",
                        "Cost Center",
                        "COA Account#",

                        //6-10
                        "COA Description",
                        "Group",
                        "Entity",
                        "Cost of Group",
                        "Product",

                        //11-15
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date", 

                        //16
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 80, 200, 150,  
                        
                        //6-10
                        250,150, 180, 200, 200, 
                        
                        //11-15
                        130, 130, 130, 130, 130,

                        //16
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!mFrmParent.mIsCostCategoryUseJoinCost)
                Grd1.Cols[3].Visible = false;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var Filter = " ";
                var FilterCC = " ";
                var SQL = new StringBuilder();
                if (mFrmParent.mIsCostCategoryUseProfitCenter)
                    Filter = " And 0=0 ";
                else
                    Filter = " Where 0=0 ";


                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCCtCode.Text, new string[] { "A.CCtCode", "A.CCtName" });
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), new string[] { "C.CCCode" });
                FilterCC = " And A.CCCode = '" + Sm.GetLue(LueCCCode) + "' ";


                SQL.AppendLine("Select A.CCtCode, A.CCtName, C.CCName, A.AcNo, A.JoinCostInd, ");
                if (mFrmParent.mIsCOAUseAlias)
                    SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc, ");
                else
                    SQL.AppendLine("B.AcDesc, ");
                SQL.AppendLine("D.OptDesc, E.EntName, F.OptDesc As CostOfGroupDesc, G.OptDesc As CCtProductDesc, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblCostCategory A  ");
                SQL.AppendLine("Left Join TblCOA B on A.AcNo=B.AcNo ");
                if (mFrmParent.mIsCostCategoryFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode is Not Null ");
                    SQL.AppendLine("And B.EntCode In ( ");
                    SQL.AppendLine("    Select T1.EntCode From TblProfitCenter T1, TblSite T2 ");
                    SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode ");
                    SQL.AppendLine("    And T1.EntCode Is Not Null ");
                    SQL.AppendLine("    And T2.ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And T2.SiteCode In (");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join TblCostCenter C On A.CCCode = C.CCCode ");
                SQL.AppendLine("Left Join TblOption D On A.CCGrpCode = D.OptCode And D.OptCat='CostCenterGroup' ");
                SQL.AppendLine("Left Join TblEntity E On B.EntCode=E.EntCode ");
                SQL.AppendLine("Left Join TblOption F On A.CostOfGroupCode=F.OptCode And F.OptCat='CostofGroup' ");
                SQL.AppendLine("Left Join TblOption G On A.CCtProductCode=G.OptCode And G.OptCat='CCtProduct' ");
                if (mFrmParent.mIsCostCategoryUseProfitCenter)
                {
                    SQL.AppendLine("Where (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("    Select CCCode ");
                    SQL.AppendLine("    From TblCostCenter ");
                    SQL.AppendLine("    Where ActInd='Y' ");
                    SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And ProfitCenterCode In (");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    )))) ");
                }
                SQL.AppendLine(Filter);
                if(Sm.GetLue(LueCCCode).Length !=0) SQL.AppendLine(FilterCC);
                SQL.AppendLine(" Order By C.CCName, A.CCtName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "CCtCode",

                            //1-5
                            "CCtName",
                            "JoinCostInd",
                            "CCName",
                            "AcNo",
                            "AcDesc",
                            
                            //6-10
                            "OptDesc",
                            "EntName",
                            "CostOfGroupDesc",
                            "CCtProductDesc",
                            "CreateBy", 
                            
                            //11-13
                            "CreateDt",
                            "LastUpBy",
                            "LastUpDt",

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Order By Col2;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method
        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }
        #endregion

        #region Event

        private void ChkCCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Category");
        }

        private void TxtCCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsCostCategoryUseProfitCenter)
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeFilterByProfitCenter), string.Empty, "Y");
            else
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion
    }

}


