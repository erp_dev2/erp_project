﻿#region Update

/*
    16/06/2022 [IBL/PRODUCT] new apps
    28/06/2022 [IBL/PRODUCT] Memunculkan item yg bersumber dari Investment Stock Initial
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDebtSecuritiesScheduleDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptDebtSecuritiesSchedule mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDebtSecuritiesScheduleDlg(FrmRptDebtSecuritiesSchedule FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Investment's Code",
                    "Investment's Name",
                    "Investment's Bank" + Environment.NewLine + "Account (RDN)#",
                    "Investment's Bank" + Environment.NewLine + "Account Name",
                    "Issuer",
                    
                    //6-10
                    "Category",
                    "Investment Type Code",
                    "Type",
                    "Nominal Amount",
                    "Moving Average" + Environment.NewLine + "Price (%)",

                    //11-15
                    "Currency",
                    "Coupon" + Environment.NewLine + "Interest Rate",
                    "Annual Days" + Environment.NewLine + "Assumption",
                    "Interest Frequency",
                    "Listing Date",

                    //16-20
                    "Last Trade Date",
                    "Last Settlement Date",
                    "Maturity Date",
                    "Last Coupon Date",
                    "Next Coupon Date"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    120, 180, 120, 200, 200, 

                    //6-10
                    150, 100, 180, 180, 180, 

                    //11-15
                    120, 180, 180, 180, 80,

                    //16-20
                    80, 80, 80, 80, 80
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 12, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 }, false);
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("	Select T1.PortofolioId As InvestmentCode, T1.InvestmentName, T1.BankAcNo, T1.BankAcNm, T1.Issuer, T1.InvestmentCtName, ");
            SQL.AppendLine("	T1.OptCode, T1.InvestmentType, T1.NominalAmt, T2.MovingAvgPrice, T1.CurCode, T1.InterestRateAmt, T1.AnnualDays, ");
            SQL.AppendLine("	T1.InterestFreq, T1.ListingDt, T4.LastTradeDt, T4.LastSettlementDt, T1.MaturityDt, T1.LastCouponDt, T1.NextCouponDt ");
            SQL.AppendLine("	From ( ");
            SQL.AppendLine("		Select A.InvestmentCode, D.PortofolioId, D.PortofolioName As InvestmentName, F.BankAcCode, F.BankAcNo, F.BankAcNm, ");
            SQL.AppendLine("		D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, G.OptCode, G.OptDesc As InvestmentType, Sum(IfNull(A.NominalAmt, 0.00)) As NominalAmt, ");
            SQL.AppendLine("		B.DocNo, B.DocType, H.CurCode, C.InterestRateAmt, CAST(I.OptDesc As decimal(16,6)) As AnnualDays, CAST(J.ParValue As decimal(16,6)) As InterestFreq, ");
            SQL.AppendLine("        C.ListingDt, C.MaturityDt, C.LastCouponDt, C.NextCouponDt ");
            SQL.AppendLine("		From TblInvestmentStockSummary A ");
            SQL.AppendLine("		Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("		Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("			And Find_In_Set(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("		Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("		Inner Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("		Inner Join TblOption G On G.OptCat = 'InvestmentType' And B.InvestmentType = G.OptCode ");
            SQL.AppendLine("		Inner Join TblInvestmentStockPrice H On A.Source = H.Source ");
            SQL.AppendLine("		Left Join TblOption I On C.AnnualDays = I.OptCode And I.OptCat = 'AnnualDaysAssumption' ");
            SQL.AppendLine("        Left Join TblParameter J On J.ParCode = 'InterestFrequencyforDebtInvestment' ");
            SQL.AppendLine("		Where B.DocDt <= @ValuationDt ");
            SQL.AppendLine("		Group By A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, F.BankAcCode ");
            SQL.AppendLine("		Having Sum(A.NominalAmt)<>0.00 ");
            SQL.AppendLine("	)T1 ");
            SQL.AppendLine("	Inner Join ( ");
            SQL.AppendLine("		Select A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, ");
            SQL.AppendLine("		(Sum((IfNull(A.NominalAmt, 0.00) * IfNull(F.UPrice, 0.00) * 0.01))) / (Sum(IfNull(A.NominalAmt, 0.00))) As MovingAvgPrice ");
            SQL.AppendLine("		From TblInvestmentStockSummary A ");
            SQL.AppendLine("		Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("		Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("            And Find_In_Set(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("		Inner Join TblOption E On E.OptCat = 'InvestmentType' And B.InvestmentType = E.OptCode ");
            SQL.AppendLine("		Inner Join TblInvestmentStockPrice F On A.Source = F.Source ");
            SQL.AppendLine("		Where B.DocDt <= @ValuationDt ");
            SQL.AppendLine("		Group By A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType ");
            SQL.AppendLine("	)T2 On T1.InvestmentCode = T2.InvestmentCode ");
            SQL.AppendLine("		And T1.InvestmentCtCode = T2.InvestmentCtCode ");
            SQL.AppendLine("		And T1.OptCode = T2.InvestmentType ");
            SQL.AppendLine("	Left Join TblAcquisitionDebtSecuritiesHdr T3 On T1.DocNo = T3.DocNo And T3.CancelInd = 'N' ");
            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select A.TradeDt As LastTradeDt, A.SettlementDt As LastSettlementDt, A.InvestmentDebtCode As InvestmentCode, C.InvestmentCtCode, A.InvestmentType, A.BankAcCode ");
            SQL.AppendLine("		From TblAcquisitionDebtSecuritiesHdr A ");
            SQL.AppendLine("		Inner Join TblInvestmentItemDebt B On A.InvestmentDebtCode = B.InvestmentDebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("		Inner Join ( ");
            SQL.AppendLine("			Select Max(X1.CreateDt) As CreateDt, X1.InvestmentDebtCode, X3.InvestmentCtCode, X1.InvestmentType, X1.BankAcCode ");
            SQL.AppendLine("			From TblAcquisitionDebtSecuritiesHdr X1 ");
            SQL.AppendLine("			Inner Join TblInvestmentItemDebt X2 On X1.InvestmentDebtCode = X2.InvestmentDebtCode ");
            SQL.AppendLine("			Inner Join TblInvestmentPortofolio X3 On X2.PortofolioId = X3.PortofolioId ");
            SQL.AppendLine("			Where X1.CancelInd = 'N' ");
            SQL.AppendLine("			Group By X1.InvestmentDebtCode, X3.InvestmentCtCode, X1.InvestmentType, X1.BankAcCode ");
            SQL.AppendLine("		)D On A.CreateDt = D.CreateDt And A.InvestmentDebtCode = D.InvestmentDebtCode ");
            SQL.AppendLine("			And C.InvestmentCtCode = D.InvestmentCtCode And A.InvestmentType = D.InvestmentType ");
            SQL.AppendLine("			And A.BankAcCode = D.BankAcCode ");
            SQL.AppendLine("	)T4 On T1.InvestmentCode = T4.InvestmentCode ");
            SQL.AppendLine("		And T1.InvestmentCtCode = T4.InvestmentCtCode ");
            SQL.AppendLine("		And T1.OptCode = T4.InvestmentType ");
            SQL.AppendLine("	Where T1.DocType in ('04','01') ");
            if(Sm.GetLue(mFrmParent.LueFinancialInstitution).Length > 0)
                SQL.AppendLine("	And T3.SekuritasCode = @SekuritasCode ");
            if (Sm.GetLue(mFrmParent.LueBankAcCode).Length > 0)
                SQL.AppendLine("	And T1.BankAcCode = @BankAcCode ");
            SQL.AppendLine(")Tbl ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = string.Empty;

                Sm.CmParamDt(ref cm, "@ValuationDt", Sm.GetDte(mFrmParent.DteValuationDt));
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(mFrmParent.LueBankAcCode));
                Sm.CmParam<String>(ref cm, "@SekuritasCode", Sm.GetLue(mFrmParent.LueFinancialInstitution));
                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mFrmParent.mDebtInvestmentCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtInvestment.Text, new string[] { "Tbl.InvestmentCode", "Tbl.InvestmentName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueInvestmentType), "Tbl.OptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter,
                    new string[]
                    {
                    //0
                    "InvestmentCode",

                    //1-5
                    "InvestmentName", "BankAcNo", "BankAcNm", "Issuer", "InvestmentCtName", 

                    //6-10
                    "OptCode", "InvestmentType", "NominalAmt", "MovingAvgPrice", "CurCode", 
                        
                    //11-15
                    "InterestRateAmt", "AnnualDays", "InterestFreq", "ListingDt", "LastTradeDt",

                    //16-19
                    "LastSettlementDt", "MaturityDt", "LastCouponDt", "NextCouponDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ChooseData(int Row)
        {
            Sm.ClearGrd(mFrmParent.Grd2, false);
            mFrmParent.TxtInvestmentCode.Text = Sm.GetGrdStr(Grd1, Row, 1);
            mFrmParent.TxtInvestmentName.Text = Sm.GetGrdStr(Grd1, Row, 2);
            mFrmParent.TxtInvestmentCategory.Text = Sm.GetGrdStr(Grd1, Row, 6);
            Sm.SetLue(mFrmParent.LueInvestmentType, Sm.GetGrdStr(Grd1, Row, 7));
            mFrmParent.TxtCurCode.Text = Sm.GetGrdStr(Grd1, Row, 11);
            mFrmParent.TxtNominalAmt.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0);
            mFrmParent.TxtMovingAvgPrice.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0);
            mFrmParent.TxtInvestmentCost.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9) * Sm.GetGrdDec(Grd1, Row, 10) * 0.01m, 0);
            mFrmParent.TxtCouponInterestRate.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0);
            mFrmParent.TxtAnnualDaysAssumption.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0);
            mFrmParent.TxtInterestFrequency.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0);
            Sm.SetDte(mFrmParent.DteListingDt, Sm.GetGrdDate(Grd1, Row, 15));
            Sm.SetDte(mFrmParent.DteLastTradeDt, Sm.GetGrdDate(Grd1, Row, 16));
            Sm.SetDte(mFrmParent.DteLastSettlementDt, Sm.GetGrdDate(Grd1, Row, 17));
            Sm.SetDte(mFrmParent.DteMaturityDt, Sm.GetGrdDate(Grd1, Row, 18));
            Sm.SetDte(mFrmParent.DteLastCouponDt, Sm.GetGrdDate(Grd1, Row, 19));
            Sm.SetDte(mFrmParent.DteNextCouponDt, Sm.GetGrdDate(Grd1, Row, 20));
            this.Close();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData(e.RowIndex);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtInvestment_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void LueInvestmentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInvestmentType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment Type");
        }

        #endregion

        #endregion

    }
}
