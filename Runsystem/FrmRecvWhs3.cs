﻿#region Update
/*
    17/07/2017 [TKG] tambah journal antar entity
    15/04/2018 [TKG] ubah proses journal untuk beda entity
    17/04/2018 [TKG] ubah proses journal untuk beda entity menjadi 2 dokumen
    17/07/2018 [TKG] tambah cost center saat journal
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    16/12/2019 [TKG/IMS] journal untuk moving average
*/
#endregion

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

namespace RunSystem
{
    public partial class FrmRecvWhs3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvWhs3Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "16";
        private bool 
            mIsRecvWhs3Customized1 = false,
            mIsAutoJournalActived = false,
            mIsEntityMandatory = false, 
            mIsMovingAvgEnabled = false;
        internal bool mIsItGrpCodeShow = false;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvWhs3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Other Warehouse";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                LuePropCode.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2));
            return (EntCode1 != EntCode2);
        }

        private void GetParameter()
        {
            mIsRecvWhs3Customized1 = Sm.GetParameter("IsRecvWhs3Customized1") == "Y";
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            SetNumberOfInventoryUomCode();
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Stock 2",
                        "Quantity 2",
                        "Uom 2",
                        "Stock 3",

                        //21-24
                        "Quantity 3",
                        "Uom 3",
                        "Remark",
                        "Group"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 100, 20, 

                        //6-10
                        80, 250, 0, 100, 200, 
                        
                        //11-15
                        180, 60, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-24
                        80, 80, 400, 100
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 17, 18, 19, 20, 21, 22, 24 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 11, 12, 14, 16, 17, 19, 20, 22, 24 });
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[24].Visible = true;
                Grd1.Cols[24].Move(7);
            }
            ShowInventoryUomCode();

            if (mIsRecvWhs3Customized1)
            {
                Grd1.Cols.Count += 3;
                Grd1.Cols[25].Text = Sm.GetParameter("RecvWhs3BatchNoCol1");
                Grd1.Cols[26].Text = Sm.GetParameter("RecvWhs3BatchNoCol2");
                Grd1.Cols[27].Text = Sm.GetParameter("RecvWhs3BatchNoCol3");
                for (int Col = 25; Col <= 27; Col++)
                {
                    Grd1.Cols[Col].Width = 150;
                    Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;
                    Grd1.Cols[Col].CellStyle.ValueType = typeof(string);
                }
                Grd1.Cols[25].Move(10);
                Grd1.Cols[26].Move(11);
                Grd1.Cols[27].Move(12);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 8, 9, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 9, 10, 12, 13, 15, 18, 21, 23 });
                    if (mIsRecvWhs3Customized1) Sm.GrdColReadOnly(true, true, Grd1, new int[] { 25, 26, 27 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 9, 10, 12, 13, 15, 18, 21, 23 });
                    if (mIsRecvWhs3Customized1) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 25, 26, 27 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, LuePropCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvWhs3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint((int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvWhs3Dlg(this));
                    }

                    if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length!=0)
                    {
                        LueRequestEdit(Grd1, LuePropCode, ref fCell, ref fAccept, e);
                        //Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        SetLuePropertyCode(ref LuePropCode, Sm.GetGrdStr(Grd1, e.RowIndex, 4));
                    }

                    if (
                        Sm.IsGrdColSelected(new int[] { 3, 9, 10, 12, 13, 15, 18, 21, 23 }, e.ColIndex) ||
                        (mIsRecvWhs3Customized1 && Sm.IsGrdColSelected(new int[] { 25, 26, 27 }, e.ColIndex))
                        )
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvWhs3Dlg(this));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 10, 12, 13, 23 }, e);

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);

            if (mIsRecvWhs3Customized1)
            {
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25, 26, 27 }, e);
                if (Sm.IsGrdColSelected(new int[] { 25, 26, 27 }, e.ColIndex))
                    GenerateBatchNo(e.RowIndex);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 18, 21 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }

            if (mIsRecvWhs3Customized1)
            {
                if (e.ColIndex == 13 || e.ColIndex == 26)
                {
                    string Value = string.Empty;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0)
                            Value = Sm.GetGrdStr(Grd1, Row, e.ColIndex);
                        else
                        {
                            if (Value.Length != 0)
                            {
                                Grd1.Cells[Row, e.ColIndex].Value = Value;
                                GenerateBatchNo(Row);
                            }
                        }
                    }
                }
                if (e.ColIndex == 27)
                {
                    var Dt = Sm.GetDte(DteDocDt);
                    if (Dt.Length != 0)
                    {
                        Dt = Dt.Substring(0, 8);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            Grd1.Cells[Row, e.ColIndex].Value = Dt;
                            GenerateBatchNo(Row);
                        }
                    }
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvWhs3", "TblRecvWhs3Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvWhs3Hdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvWhs3Dtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (Transfer From)") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (Transfer To)") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;
                //if (Sm.IsGrdValueEmpty(Grd1, Row, 10, false, "Batch number is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveRecvWhs3Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvWhs3Hdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvWhs3Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvWhs3Dtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, IfNull(@PropCode, '-'), IfNull(@BatchNo, '-'), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), " +
                    "@Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.PropCode, B.BatchNo, B.Source, (Select ParValue from TblParameter Where ParCode='MainCurCode'), 0, 1, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs3Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo, ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            #region Journal 1

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse (Based On Item) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs3Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo2, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse (Based On Item) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs3Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo2 Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On B.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRecvWhsDtl(DNo));

            if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblRecvWhs3Dtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            ReComputeStock();
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsGrdValueNotValid2();
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                {
                    Msg =
                       "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                       "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                       "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                       "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                       "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                       "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                       "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                       "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;


                    if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than cancelled quantity.");
                        return true;
                    }

                    if (Grd1.Cols[18].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than cancelled quantity (2).");
                            return true;
                        }
                    }

                    if (Grd1.Cols[21].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than cancelled quantity (3).");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand CancelRecvWhsDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs3Dtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DocType=@DocType  ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='Y' ");
            SQL.AppendLine("        And Position(Concat('##', DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.ItCode=B.ItCode ");
            SQL.AppendLine("        And A.PropCode=B.PropCode ");
            SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            SQL.AppendLine("    And T1.PropCode=T2.PropCode ");
            SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            #region Journal 1

            SQL.AppendLine("Update TblRecvWhs3Dtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs3Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblRecvWhs3Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On B.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On B.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On B.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On B.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Update TblRecvWhs3Dtl Set JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs3Hdr Where DocNo=@DocNo And JournalDocNo2 Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo2 From TblRecvWhs3Hdr Where DocNo=@DocNo And JournalDocNo2 Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On B.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On B.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs3Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs3Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            #endregion

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(CurrentDt, 2));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(DocDt, 2));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowRecvWhs3Hdr(DocNo);
                ShowRecvWhs3Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvWhs3Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, WhsCode2, Remark From TblRecvWhs3Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "WhsCode2", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowRecvWhs3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0) As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) As Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, C.ItGrpCode ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.ItCode=E.ItCode ");
            SQL.AppendLine("    And B.PropCode=E.PropCode ");
            SQL.AppendLine("    And B.BatchNo=E.BatchNo ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "Remark",
 
                    //21
                    "ItGrpCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private string GetCostCenterWarehouse(string WhsCode)
        {
            return Sm.GetValue("Select CCCode From TblWarehouse Where WhsCode=@Param;", WhsCode);
        }

        private void GenerateBatchNo(int row)
        {
            string CustomBatchNo = string.Empty;

            CustomBatchNo = Sm.GetGrdStr(Grd1, row, 25);
            if (Sm.GetGrdStr(Grd1, row, 26).Length > 0)
            {
                if (CustomBatchNo.Length > 0) CustomBatchNo += "-";
                CustomBatchNo += Sm.GetGrdStr(Grd1, row, 26);
            }
            if (Sm.GetGrdStr(Grd1, row, 27).Length > 0)
            {
                if (CustomBatchNo.Length > 0) CustomBatchNo += "-";
                CustomBatchNo += Sm.GetGrdStr(Grd1, row, 27);
            }
            Grd1.Cells[row, 10].Value = CustomBatchNo.Trim();
        }

        internal string GetSelectedInventory()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }


        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" + Sm.GetGrdStr(Grd1, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvWhs3Hdr>();
            var ldtl = new List<RecvWhs3Dtl>();

            string[] TableName = { "RecvWhs3Hdr", "RecvWhs3Dtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName As WhsTo, C.WhsName As WhsFrom, A.Remark From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2 = C.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         //6-10
                         "WhsTo",
                         "WhsFrom",
                         "Remark",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvWhs3Hdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsTo = Sm.DrStr(dr, c[6]),
                            WhsFrom = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQLDtl.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
                SQLDtl.AppendLine("B.Remark, C.ItGrpCode ");
                SQLDtl.AppendLine("From TblRecvWhs3Hdr A ");
                SQLDtl.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo  And B.CancelInd = 'N' Order By B.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName",
                         "BatchNo",
                         "Source",
                         "Lot",
                         "Bin",
                        

                         //6-10
                         "Qty" ,
                         "Qty2",
                         "Qty3",
                         "InventoryUomCode" ,                        
                         "InventoryUomCode2" ,
                         
                         //11-13
                         "InventoryUomCode3" ,
                         "Remark",
                         "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RecvWhs3Dtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            Remark = Sm.DrStr(drDtl, cDtl[12]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[13])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion


            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("RecvWhs31", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("RecvWhs32", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("RecvWhs33", myLists, TableName, false);
                    break;
            }
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColQty3, int ColUom1, int ColUom2, int ColUom3)
        //{
        //    if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //    {
        //        decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //        if (Convert != 0)
        //        {
        //            Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            if (Sm.CompareGrdStr(Grd, Row, ColUom2, Grd, Row, ColUom3))
        //                Sm.CopyGrdValue(Grd, Row, ColQty3, Grd, Row, ColQty2);
        //        }
        //    }
        //}

        private void SetLuePropertyCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropCode As Col1, B.PropName As Col2 ");
            SQL.AppendLine("From TblItemProperty A ");
            SQL.AppendLine("Inner Join TblProperty B On A.PropCode = B.PropCode ");
            SQL.AppendLine("Where A.ItCode=@ItCode Order By B.PropName;");
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LuePropCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropCode, new Sm.RefreshLue2(SetLuePropertyCode), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4));
        }

        private void LuePropCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePropCode_Leave(object sender, EventArgs e)
        {
            if (LuePropCode.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LuePropCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value =
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LuePropCode);
                    Grd1.Cells[fCell.RowIndex, 9].Value = LuePropCode.GetColumnValue("Col2");
                }
                LuePropCode.Visible = false;
            }
        }
        #endregion

        #endregion

    }

    #region Report Class

    class RecvWhs3Hdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string WhsFrom { get; set; }
        public string WhsTo { get; set; }
        public string Remark { get; set; }
        public string PrintBy { get; set; }
    }

    class RecvWhs3Dtl
    {
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string BatchNo { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }
        public string Remark { get; set; }
        public string ItGrpCode { get; set; }
    }

    #endregion
}
