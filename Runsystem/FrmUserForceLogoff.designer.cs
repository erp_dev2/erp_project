﻿namespace RunSystem
{
    partial class FrmUserForceLogoff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUserForceLogoff));
            this.TxtUserName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnUserCode = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(422, 0);
            this.panel1.Size = new System.Drawing.Size(70, 62);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnUserCode);
            this.panel2.Controls.Add(this.TxtUserName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtUserCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(422, 62);
            // 
            // TxtUserName
            // 
            this.TxtUserName.EnterMoveNextControl = true;
            this.TxtUserName.Location = new System.Drawing.Point(78, 32);
            this.TxtUserName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserName.Name = "TxtUserName";
            this.TxtUserName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUserName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserName.Properties.Appearance.Options.UseFont = true;
            this.TxtUserName.Properties.MaxLength = 80;
            this.TxtUserName.Properties.ReadOnly = true;
            this.TxtUserName.Size = new System.Drawing.Size(319, 20);
            this.TxtUserName.TabIndex = 7;
            this.TxtUserName.Validated += new System.EventHandler(this.TxtUserName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(7, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "User Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(78, 11);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.MaxLength = 16;
            this.TxtUserCode.Properties.ReadOnly = true;
            this.TxtUserCode.Size = new System.Drawing.Size(154, 20);
            this.TxtUserCode.TabIndex = 4;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(10, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "User Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUserCode
            // 
            this.BtnUserCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUserCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUserCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUserCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUserCode.Appearance.Options.UseBackColor = true;
            this.BtnUserCode.Appearance.Options.UseFont = true;
            this.BtnUserCode.Appearance.Options.UseForeColor = true;
            this.BtnUserCode.Appearance.Options.UseTextOptions = true;
            this.BtnUserCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUserCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUserCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnUserCode.Image")));
            this.BtnUserCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUserCode.Location = new System.Drawing.Point(232, 10);
            this.BtnUserCode.Name = "BtnUserCode";
            this.BtnUserCode.Size = new System.Drawing.Size(24, 21);
            this.BtnUserCode.TabIndex = 5;
            this.BtnUserCode.ToolTip = "Show User";
            this.BtnUserCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUserCode.ToolTipTitle = "Run System";
            this.BtnUserCode.Click += new System.EventHandler(this.BtnUserCode_Click);
            // 
            // FrmUserForceLogoff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 62);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmUserForceLogoff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnUserCode;
        protected internal DevExpress.XtraEditors.TextEdit TxtUserName;
        protected internal DevExpress.XtraEditors.TextEdit TxtUserCode;
    }
}