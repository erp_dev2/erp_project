﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpLeave2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpLeave2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                DteDocDt1.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

             SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.Posname, D.Deptname, NumberOfDay, NumberOfHour ");
             SQL.AppendLine("From ( ");
             SQL.AppendLine("   Select EmpCode, Sum(NumberOfDay) NumberOfDay, Sum(NumberOfHour) NumberOfHour ");
             SQL.AppendLine("   From ( ");
             SQL.AppendLine("       Select T1.EmpCode, ");
             SQL.AppendLine("       Case When T1.LeaveType='F' Then 1 Else 0 End As NumberOfDay, ");
             SQL.AppendLine("       Case When T1.LeaveType<>'F' Then T1.DurationHour Else 0 End As NumberOfHour ");
             SQL.AppendLine("       From TblEmpLeaveHdr T1 ");
             SQL.AppendLine("       Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @DocDt1 And @DocDt2 ");
             SQL.AppendLine("       Where T1.CancelInd='N' ");
             SQL.AppendLine("       And IfNull(T1.Status, 'O')<>'C' "); 
             SQL.AppendLine("   Union All ");
             SQL.AppendLine("       Select T2.EmpCode, ");
             SQL.AppendLine("       Case When T1.LeaveType='F' Then 1 Else 0 End As NumberOfDay, ");
             SQL.AppendLine("       Case When T1.LeaveType<>'F' Then T1.DurationHour Else 0 End As NumberOfHour ");
             SQL.AppendLine("       From TblEmpLeave2Hdr T1 ");
             SQL.AppendLine("       Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
             SQL.AppendLine("       Inner Join TblEmpLeave2Dtl2 T3 On T1.DocNo=T3.DocNo And T3.LeaveDt Between @DocDt1 And @DocDt2 ");
             SQL.AppendLine("       Where T1.CancelInd='N' ");
             SQL.AppendLine("       And IfNull(T1.Status, 'O')<>'C' ");
             SQL.AppendLine("   ) Tbl Group By EmpCode ");
             SQL.AppendLine(") A ");
             SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
             SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
             SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name",
                        "Old Code", 
                        "Position",
                        "Department",
                        
                        //6-7
                        "Number of"+Environment.NewLine+"Days",
                        "Number of"+Environment.NewLine+"Hours",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 200, 100, 200, 200, 
                        
                        //6-7
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 },0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Start date") || Sm.IsDteEmpty(DteDocDt2, "End date"))
                {
                    return;
                }
               
               
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "";

                var cm = new MySqlCommand();


                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "D.DeptCode", true);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName","EmpCodeOld","PosName","DeptName","NumberOfDay",
                            //6
                            "NumberOfHour"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                           
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
        }
        #endregion
    }
}
