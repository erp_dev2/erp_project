﻿#region Update
/*
    26/09/2017 [WED] tambah field Indicator 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmQCParameter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mQCPCode = string.Empty;
        internal FrmQCParameterFind FrmFind;

        #endregion

        #region Constructor

        public FrmQCParameter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueQCParameterUomCode(ref LueUomCode);
                
                //if this application is called from other application
                if (mQCPCode.Length != 0)
                {
                    ShowData(mQCPCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtQCPCode, TxtQCPDesc, LueUomCode, TxtRefValue, TxtQCPIndicator
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    TxtQCPCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtQCPCode, TxtQCPDesc, LueUomCode, TxtRefValue, TxtQCPIndicator
                    }, false);
                    TxtQCPCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtQCPDesc, LueUomCode, TxtRefValue, TxtQCPIndicator
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    TxtQCPDesc.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtQCPCode, TxtQCPDesc, LueUomCode, TxtQCPIndicator
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtRefValue
            }, 2);
            ChkActInd.Checked = false;
        }

        internal void ShowData(string QCPCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@QCPCode", QCPCode);
                Sm.ShowDataInCtrl(
                        ref cm, 
                        "Select QCPCode, QCPDesc, QCPIndicator, ActInd, UomCode, RefValue " +
                        "From TblQCParameter " +
                        "Where QCPCode=@QCPCode;",
                        new string[] 
                        {
                            //0
                            "QCPCode",
                            
                            //1-5
                            "QCPDesc", "QCPIndicator", "ActInd", "UomCode", "RefValue"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtQCPCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtQCPDesc.EditValue = Sm.DrStr(dr, c[1]);
                            TxtQCPIndicator.EditValue = Sm.DrStr(dr, c[2]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                            Sm.SetLue(LueUomCode, Sm.DrStr(dr, c[4]));
                            TxtRefValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 2);                            
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmQCParameterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtQCPCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtQCPCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblQCParameter Where QCPCode=@QCPCode" };
                Sm.CmParam<String>(ref cm, "@QCPCode", TxtQCPCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;


                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblQCParameter(QCPCode, QCPDesc, QCPIndicator, ActInd, UomCode, RefValue, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@QCPCode, @QCPDesc, @QCPIndicator, @ActInd, @UomCode, @RefValue, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update ");
                SQL.AppendLine("        QCPDesc=@QCPDesc, QCPIndicator=@QCPIndicator, ActInd=@ActInd, UomCode=@UomCode, RefValue=@RefValue, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@QCPCode", TxtQCPCode.Text);
                Sm.CmParam<String>(ref cm, "@QCPDesc", TxtQCPDesc.Text);
                Sm.CmParam<String>(ref cm, "@QCPIndicator", TxtQCPIndicator.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
                Sm.CmParam<Decimal>(ref cm, "@RefValue", Decimal.Parse(TxtRefValue.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtQCPCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Additional Method

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtQCPCode, "Parameter code", false) ||
                Sm.IsTxtEmpty(TxtQCPDesc, "Description", false) ||
                Sm.IsTxtEmpty(TxtQCPIndicator, "Indicator", false) ||
                Sm.IsTxtEmpty(TxtRefValue, "Reference", true) ||
                Sm.IsLueEmpty(LueUomCode, "UoM") ||
                IsQCPCodeExisted();
        }

        private bool IsQCPCodeExisted()
        {
            if (!TxtQCPCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select QCPCode From TblQCParameter " +
                        "Where QCPCode =@QCPCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@QCPCode", TxtQCPCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Parameter code ( " + TxtQCPCode.Text + " ) already existed.");
                    TxtQCPCode.Focus();
                    return true;
                }
            }
            return false;
        }
        

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueQCParameterUomCode));
        }

        private void TxtRefValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRefValue, 0);
        }

        private void TxtQCPDesc_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtQCPDesc);
        }

        private void TxtQCPCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtQCPCode);
        }

        private void TxtQCPIndicator_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtQCPIndicator);
        }

        #endregion

        #endregion
    }
}
