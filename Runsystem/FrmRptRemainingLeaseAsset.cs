﻿#region Update
/*
    25/10/2019 [TKG/KBN] Reporting baru
    28/10/2019 [TKG/KBN] tambah informasi dan filter cost center
 *  31/01/2019 [HAR/KBN] kolom Rented ambil dari available kolom Available  itu Wide/Area-Rented  
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptRemainingLeaseAsset : RunSystem.FrmBase4
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
       
        #endregion

        #region Constructor

        public FrmRptRemainingLeaseAsset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueAssetCategoryCode(ref LueAssetCategoryCode);
                Sl.SetLueCCCode(ref LueCCCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset's Code", 
                        "Asset's Name",
                        "Category",
                        "Cost Center",
                        "UoM",
                        
                        //6-8
                        "Wide/Area",
                        "Rented",
                        "Available"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 200, 200, 100, 
                        
                        //6-8
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, A.AssetName, B.AssetCategoryName, C.CCName, A.WideUomCode, A.Wide, ");
            SQL.AppendLine("IfNull(E.Available, 0.00) As Rented, ");
            SQL.AppendLine("(IfNull(A.Wide, 0.00)-IfNull(E.Available, 0.00)) As Available ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Left Join TblAssetCategory B On A.AssetCategoryCode=B.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblCostCenter C On A.CCCode=C.CCCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select C2.ItCode, Sum(Qty) As Rented ");
            SQL.AppendLine("    From TblDOCtHdr C1, TblDOCtDtl C2 ");
            SQL.AppendLine("    Where C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    And C1.Status In ('O', 'A') ");
            SQL.AppendLine("    And C2.CancelInd='N' ");
            SQL.AppendLine("    And C2.ItCode In (Select ItCode from TblAsset Where ActiveInd='Y' And RentedInd='Y' And SoldInd='Y' And ItCode Is Not Null) ");
            SQL.AppendLine("    Group By C2.ItCode ");
            SQL.AppendLine(") D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select AssetCode, Sum(Wide) As Available ");
            SQL.AppendLine("    From TblCustomerAsset ");
            SQL.AppendLine("    Group By AssetCode ");
            SQL.AppendLine(") E On A.AssetCode=E.AssetCode ");
            SQL.AppendLine("Where A.ActiveInd='Y' And A.RentedInd='Y' And A.SoldInd='Y' And A.ItCode Is Not Null ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAssetName.Text, new string[] { "A.AssetCode", "A.AssetName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCategoryCode), "A.AssetCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By AssetName;",
                        new string[]
                        {
                            //0
                            "AssetCode", 
                                
                            //1-5
                            "AssetName", "AssetCategoryName", "CCName", "WideUomCode", "Wide", 
                            
                            //6-7
                            "Rented", "Available"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Event

        private void TxtAssetName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueAssetCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCategoryCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCategoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's category");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion

        
    }
}
