﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptSO2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptSO2 mFrmParent;
        private string mSQL = string.Empty, mSODocNo = string.Empty, mSODNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSO2Dlg(FrmRptSO2 FrmParent, string SODocNo, string SODNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSODocNo = SODocNo;
            mSODNo = SODNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                BtnChoose.Enabled = BtnPrint.Enabled = BtnExcel.Enabled = false;
                BtnChoose.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct B.DocNo, B.DocDt ");
            SQL.AppendLine("From TblPLDtl A ");
            SQL.AppendLine("Inner Join TblPLHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.SODocNo = @DocNo And A.SODNo = @DNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Document#",
                    "",
                    "Date",
                    "No. of Container"
                },
                new int[]
                {
                    40, 
                    130, 20, 80, 120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mSODocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mSODNo);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By B.DocDt, A.DocNo; ",
                    new string[] 
                    { 
                        "DocNo", "DocDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Grd1.Cells[Row, 4].Value = 0m;
                    }, true, false, false, false
                );
                CountContainer();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Methods

        private void CountContainer()
        {
            var l = new List<PL>();
            string mDocNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        //l.Add(new PL()
                        //{
                        //    DocNo = Sm.GetGrdStr(Grd1, Row, 1)
                        //});
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, Row, 1);
                    }
                }
            }

            if (mDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select DocNo, Cnt1, Cnt2, Cnt3, Cnt4, Cnt5, ");
                SQL.AppendLine("Cnt6, Cnt7, Cnt8, Cnt9, Cnt10, ");
                SQL.AppendLine("Cnt11, Cnt12, Cnt13, Cnt14, Cnt15, ");
                SQL.AppendLine("Cnt16, Cnt17, Cnt18, Cnt19, Cnt20, ");
                SQL.AppendLine("Cnt21, Cnt22, Cnt23, Cnt24, Cnt25 ");
                SQL.AppendLine("From TblPLHdr ");
                SQL.AppendLine("Where Find_In_Set(DocNo, @PLDocNo); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PLDocNo", mDocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                            //0
                            "DocNo",
                            //1-5
                            "Cnt1",
                            "Cnt2",
                            "Cnt3",
                            "Cnt4",
                            "Cnt5",
                            //6-10
                            "Cnt6",
                            "Cnt7",
                            "Cnt8",
                            "Cnt9",
                            "Cnt10",
                            //11-15
                            "Cnt11",
                            "Cnt12",
                            "Cnt13",
                            "Cnt14",
                            "Cnt15",
                            //16-20
                            "Cnt16",
                            "Cnt17",
                            "Cnt18",
                            "Cnt19",
                            "Cnt20",
                            //21-25
                            "Cnt21",
                            "Cnt22",
                            "Cnt23",
                            "Cnt24",
                            "Cnt25"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PL()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),

                                Cnt1 = Sm.DrStr(dr, c[1]),
                                Cnt2 = Sm.DrStr(dr, c[2]),
                                Cnt3 = Sm.DrStr(dr, c[3]),
                                Cnt4 = Sm.DrStr(dr, c[4]),
                                Cnt5 = Sm.DrStr(dr, c[5]),
                                
                                Cnt6 = Sm.DrStr(dr, c[6]),
                                Cnt7 = Sm.DrStr(dr, c[7]),
                                Cnt8 = Sm.DrStr(dr, c[8]),
                                Cnt9 = Sm.DrStr(dr, c[9]),
                                Cnt10 = Sm.DrStr(dr, c[10]),

                                Cnt11 = Sm.DrStr(dr, c[11]),
                                Cnt12 = Sm.DrStr(dr, c[12]),
                                Cnt13 = Sm.DrStr(dr, c[13]),
                                Cnt14 = Sm.DrStr(dr, c[14]),
                                Cnt15 = Sm.DrStr(dr, c[15]),

                                Cnt16 = Sm.DrStr(dr, c[16]),
                                Cnt17 = Sm.DrStr(dr, c[17]),
                                Cnt18 = Sm.DrStr(dr, c[18]),
                                Cnt19 = Sm.DrStr(dr, c[19]),
                                Cnt20 = Sm.DrStr(dr, c[20]),

                                Cnt21 = Sm.DrStr(dr, c[21]),
                                Cnt22 = Sm.DrStr(dr, c[22]),
                                Cnt23 = Sm.DrStr(dr, c[23]),
                                Cnt24 = Sm.DrStr(dr, c[24]),
                                Cnt25 = Sm.DrStr(dr, c[25])
                            });
                        }
                    }
                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                        {
                            for (int j = 0; j < l.Count; j++)
                            {
                                decimal mCountContainer = 0m;
                                if (Sm.GetGrdStr(Grd1, i, 1) == l[j].DocNo)
                                {
                                    if (l[j].Cnt1.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt2.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt3.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt4.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt5.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt6.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt7.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt8.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt9.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt10.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt11.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt12.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt13.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt14.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt15.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt16.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt17.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt18.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt19.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt20.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt21.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt22.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt23.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt24.Length > 0) mCountContainer += 1;
                                    if (l[j].Cnt25.Length > 0) mCountContainer += 1;

                                    Grd1.Cells[i, 4].Value = mCountContainer;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            l.Clear();
            mDocNo = string.Empty;
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

        #region Class

        private class PL
        {
            public string DocNo { get; set; }
            public string Cnt1 { get; set; }
            public string Cnt2 { get; set; }
            public string Cnt3 { get; set; }
            public string Cnt4 { get; set; }
            public string Cnt5 { get; set; }
            public string Cnt6 { get; set; }
            public string Cnt7 { get; set; }
            public string Cnt8 { get; set; }
            public string Cnt9 { get; set; }
            public string Cnt10 { get; set; }
            public string Cnt11 { get; set; }
            public string Cnt12 { get; set; }
            public string Cnt13 { get; set; }
            public string Cnt14 { get; set; }
            public string Cnt15 { get; set; }
            public string Cnt16 { get; set; }
            public string Cnt17 { get; set; }
            public string Cnt18 { get; set; }
            public string Cnt19 { get; set; }
            public string Cnt20 { get; set; }
            public string Cnt21 { get; set; }
            public string Cnt22 { get; set; }
            public string Cnt23 { get; set; }
            public string Cnt24 { get; set; }
            public string Cnt25 { get; set; }
            public decimal CountContainer { get; set; }
        }

        #endregion

    }
}
