﻿#region Update
/*
    28/05/2018 [WED] tambah informasi LocalDocNo
    18/03/2019 [TKG] proses DO scr partial
 *  12/07/2021 [ICA/IMS] Menambah kolom DO To Other Warehouse for Project DocNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs4Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvWhs4 mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty, mWhsCode2 = string.Empty;
        private bool mIsInventoryShowTotalQty = false, mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmRecvWhs4Dlg(FrmRecvWhs4 FrmParent, string WhsCode, string WhsCode2)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mWhsCode2 = WhsCode2;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                GetParameter();
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.LocalDocNo, B.ItCode, C.ItCodeInternal, C.ItName, E.DocNo as DOWHs4DocNo, ");
            SQL.AppendLine("B.BatchNo, B.Source, B.Lot, B.Bin, ");
            if (mFrmParent.mIsRecvWhs4ProcessDOWhsPartialEnabled)
            {
                SQL.AppendLine("B.Qty-IfNull(D.Qty, 0) As Qty, ");
                SQL.AppendLine("B.Qty2-IfNull(D.Qty2, 0) As Qty2, ");
                SQL.AppendLine("B.Qty3-IfNull(D.Qty3, 0) As Qty3, ");
            }
            else
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, C.ItGrpCode, B.Remark ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.ProcessInd In ('O', 'P') ");
            SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @SelectedDO)<1 ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            if (mFrmParent.mIsRecvWhs4ProcessDOWhsPartialEnabled)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DOWhsDocNo, T2.DOWhsDNo, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
                SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblDOWhsHdr T3 ");
                SQL.AppendLine("        On T2.DOWhsDocNo=T3.DocNo ");
                SQL.AppendLine("        And T3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    Group By T2.DOWhsDocNo, T2.DOWhsDNo ");
                SQL.AppendLine(") D On B.DocNo=D.DOWhsDocNo And B.DNo=D.DOWhsDNo ");
            }
            SQL.AppendLine("Left Join TblDOWhs4Dtl E On B.DocNo = E.DOWhsDocNo And B.DNo = E.DOWhsDNo ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode2 ");
            SQL.AppendLine("And A.WhsCode2=@WhsCode ");
            SQL.AppendLine("And (A.TransferRequestWhsDocNo Is Not Null Or A.TransferRequestWhs2DocNo Is Not Null) ");
            SQL.AppendLine("And A.Status= 'A' ");
            SQL.AppendLine("And A.CancelInd = 'N'");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "DO#",
                        "DO# DNo",
                        "Date",
                        "Item's Code", 

                        //6-10
                        "", 
                        "Item's"+Environment.NewLine+"Local Code", 
                        "Item's Name", 
                        "Batch#",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin", 
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Group",
                        "Remark",

                        //21-22
                        "Local#", 
                        "DO To Other Warehouse"+Environment.NewLine+"For Project#"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 130, 0, 80, 100, 
                        
                        //6-10
                        20, 100, 200, 200, 150,
                        
                        //11-15
                        100, 100, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 100, 250,

                        //21-22
                        130, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7, 10, 15, 16, 17, 18, 19, 22 }, false);
            Grd1.Cols[21].Move(4);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[19].Visible = true;
                Grd1.Cols[19].Move(8);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " "; ;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@WhsCode2", mWhsCode2);
                Sm.CmParam<String>(ref cm, "@SelectedDO", mFrmParent.GetSelectedDO());
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItCodeInternal", "C.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, new string[] { "B.BatchNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, new string[] { "B.Lot" });
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, new string[] { "B.Bin" });

                mIsReCompute = false;
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DNo", "DocDt", "ItCode", "ItCodeInternal", "ItName",  
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty", 
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",
 
                            //16-19
                            "ItGrpCode", "Remark", "LocalDocNo", "DOWHs4DocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        }, true, false, false, false
                    );
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 15, 17 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 22);
                        if (mFrmParent.mIsRecvWhsWithTRCopyRemarkFromDO)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 20);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(Grd1, Row, 3)))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty && mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
