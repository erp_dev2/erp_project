﻿namespace RunSystem
{
    partial class FrmProjectDelivery3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.Project = new System.Windows.Forms.Label();
            this.ChkProjectName = new DevExpress.XtraEditors.CheckEdit();
            this.LueProjectName = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueProjectName);
            this.panel2.Controls.Add(this.Project);
            this.panel2.Controls.Add(this.ChkProjectName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Controls.Add(this.ChkCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.ChkDocNo);
            this.panel2.Size = new System.Drawing.Size(796, 72);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 72);
            this.panel3.Size = new System.Drawing.Size(796, 437);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(796, 437);
            this.Grd1.TabIndex = 19;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Location = new System.Drawing.Point(0, 467);
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(796, 0);
            this.panel1.Size = new System.Drawing.Size(70, 509);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Customer";
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(82, 24);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(287, 20);
            this.LueCtCode.TabIndex = 14;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // ChkCtCode
            // 
            this.ChkCtCode.Location = new System.Drawing.Point(372, 23);
            this.ChkCtCode.Name = "ChkCtCode";
            this.ChkCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCtCode.Properties.Caption = " ";
            this.ChkCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkCtCode.TabIndex = 15;
            this.ChkCtCode.ToolTip = "Remove filter";
            this.ChkCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCtCode.ToolTipTitle = "Run System";
            this.ChkCtCode.CheckedChanged += new System.EventHandler(this.ChkCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(82, 3);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 40;
            this.TxtDocNo.Size = new System.Drawing.Size(287, 20);
            this.TxtDocNo.TabIndex = 11;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // ChkDocNo
            // 
            this.ChkDocNo.Location = new System.Drawing.Point(372, 2);
            this.ChkDocNo.Name = "ChkDocNo";
            this.ChkDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNo.Properties.Caption = " ";
            this.ChkDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNo.TabIndex = 12;
            this.ChkDocNo.ToolTip = "Remove filter";
            this.ChkDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNo.ToolTipTitle = "Run System";
            this.ChkDocNo.CheckedChanged += new System.EventHandler(this.ChkDocNo_CheckedChanged);
            // 
            // Project
            // 
            this.Project.AutoSize = true;
            this.Project.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Project.Location = new System.Drawing.Point(32, 48);
            this.Project.Name = "Project";
            this.Project.Size = new System.Drawing.Size(46, 14);
            this.Project.TabIndex = 16;
            this.Project.Text = "Project";
            // 
            // ChkProjectName
            // 
            this.ChkProjectName.Location = new System.Drawing.Point(372, 44);
            this.ChkProjectName.Name = "ChkProjectName";
            this.ChkProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProjectName.Properties.Appearance.Options.UseFont = true;
            this.ChkProjectName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProjectName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProjectName.Properties.Caption = " ";
            this.ChkProjectName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProjectName.Size = new System.Drawing.Size(19, 22);
            this.ChkProjectName.TabIndex = 18;
            this.ChkProjectName.ToolTip = "Remove filter";
            this.ChkProjectName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProjectName.ToolTipTitle = "Run System";
            this.ChkProjectName.CheckedChanged += new System.EventHandler(this.ChkProjectName_CheckedChanged);
            // 
            // LueProjectName
            // 
            this.LueProjectName.EnterMoveNextControl = true;
            this.LueProjectName.Location = new System.Drawing.Point(82, 45);
            this.LueProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectName.Name = "LueProjectName";
            this.LueProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.Appearance.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectName.Properties.DropDownRows = 30;
            this.LueProjectName.Properties.NullText = "[Empty]";
            this.LueProjectName.Properties.PopupWidth = 300;
            this.LueProjectName.Size = new System.Drawing.Size(287, 20);
            this.LueProjectName.TabIndex = 19;
            this.LueProjectName.ToolTip = "F4 : Show/hide list";
            this.LueProjectName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectName.EditValueChanged += new System.EventHandler(this.LueProjectName_EditValueChanged);
            // 
            // FrmProjectDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 509);
            this.KeyPreview = true;
            this.Name = "FrmProjectDelivery";
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDocNo;
        private System.Windows.Forms.Label Project;
        private DevExpress.XtraEditors.CheckEdit ChkProjectName;
        private DevExpress.XtraEditors.LookUpEdit LueProjectName;
    }
}