﻿#region Update
/* 
    04/01/2018 [TKG] Employee's Salary, Allowance and Deduction
    28/02/2018 [TKG] bug perhitungan brutto untuk potongan, tambah PHDP+Non PHDP, letak brutto paling belakang
    02/03/2018 [TKG] tambh filter religion
    03/03/2018 [HAR] tambah informasi bank
    20/04/2018 [HAR] bug double data karena ada 2 salary atau lebih
    22/04/2019 [TKG] data salary diambil yg terakhir yg bukan 0. 
    19/05/2020 [TKG/SRN] kode employee saat export ke excel tidak sesuai dengan yg di aplikasi
    25/11/2020 [ICA/PHT] Menambah Kolom Division
    29/11/2020 [TKG/PHT] Berdasarkan parameter IsRptEmpSalADShowBasicSalaryComponent,
                         reporting ini ditambah lup untuk menampilkan komponen2 gaji dasar.
 *  13/01/2021 [HAR/PHT] BUG : nilai tunangan tidak seesuai kolomnya
 *  10/05/2021 [TRI/PHT] BUG : pada reporting employee salary allowance and deduction (0210610) ketika digrup hanya kolom zakat saja yang muncul
    01/12/2021 [DITA/PHT] untuk parameter SSPCodeForPension value nya bisa lebih dari 1, diubah menggunakan find_in_set bukan comparestr lagi
    22/12/2021 [VIN/PHT] PHT level ditampilkan dr tblemployee tidak berdasarkan grade nya 
    22/03/2022 [VIN/ALL] BUG: Export to excel pakai Sm. 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSalAD : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsRptEmpSalADShowPHDP = false,
            mIsEmployeeTabAdditionalEnabled = false,
            mIsRptEmpSalADShowBasicSalaryComponent = false;
        private List<AD> l;

        #endregion

        #region Constructor

        public FrmRptEmpSalAD(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                GetParameter();
                Sl.SetLueOption(ref LueReligion, "Religion");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetLue(LueStatus, "1");

                l = new List<AD>();
                SetAD(ref l);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsRptEmpSalADShowPHDP = Sm.GetParameterBoo("IsRptEmpSalADShowPHDP");
            mIsRptEmpSalADShowBasicSalaryComponent = Sm.GetParameterBoo("IsRptEmpSalADShowBasicSalaryComponent");
            mIsEmployeeTabAdditionalEnabled = Sm.GetParameterBoo("IsEmployeeTabAdditionalEnabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20 + l.Count;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Employee's Code",
                    "Employee's Name",
                    "Old Code",
                    "Position",
                    "Grade",
                    
                    //6-10
                    "Level",
                    "Department",
                    "Site",
                    "Status",
                    "Religion",
                    
                    //11-15
                    "Join",
                    "Resign",
                    "Bank",
                    "Non PHDP",
                    "PHDP",

                    //16-19
                    "Salary",
                    "Bruto",
                    "Division",
                    "",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    130, 200, 100, 200, 150, 

                    //6-10
                    150, 200, 200, 130, 150, 
                    
                    //11-15
                    100, 100, 150, 130, 130,  
                    
                    //16-19
                    130, 130, 200, 20
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 19 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 9, 11, 12, 18 }, false);
            if (!mIsRptEmpSalADShowPHDP) Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, false);
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Cols[20 + i].Text = l[i].ADName.Trim().Replace(" ", Environment.NewLine);
                Grd1.Cols[20 + i].Width = 150;
                Grd1.Header.Cells[0, 20 + i].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[20 + i].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[20 + i].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[20 + i].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            }
            Grd1.Cols[17].Move(Grd1.Cols.Count-1);
            Grd1.Cols[18].Move(9);

            if (!mIsRptEmpSalADShowBasicSalaryComponent)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, false);
            else
            {
                Grd1.Cols[19].Move(15);
                Grd1.ReadOnly = false;
                for (int i = 0; i < Grd1.Cols.Count; i++)
                {
                    if (i!=19) Sm.GrdColReadOnly(Grd1, new int[] { i });
                }   
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 9, 11, 12, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                List<int> colList = new List<int>();
                colList.Add(14);
                colList.Add(15);
                colList.Add(16);
                colList.Add(17);

                Process1();
                if (Grd1.Rows.Count > 0)
                {
                    Process2();
                    Process3();

                    for (int c = 20; c < Grd1.Cols.Count; c++)
                    {
                        colList.Add(c);
                    }

                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;

                    int[] cols = colList.ToArray();
                    iGSubtotalManager.ShowSubtotals(Grd1, cols);


                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Grd1.Cols.AutoWidth();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, F.GrdLvlName,D.SiteName,C.DeptName, M.DivisionName, ");
            if(mIsEmployeeTabAdditionalEnabled) 
                SQL.AppendLine("G2.LevelName,  ");
            else
                SQL.AppendLine("G.LevelName,  ");
            SQL.AppendLine("H.OptDesc As EmploymentStatusDesc, K.OptDesc As ReligionDesc, A.JoinDt, A.ResignDt, IfNull(E.Amt, 0.00)-IfNull(J.Amt, 0.00) As NonPHDP, J.Amt As PHDP, E.Amt, IfNull(E.Amt, 0.00)+IfNull(I.Amt, 0.00) As Brutto, L.BankName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");

            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select A.EmpCode, A.Amt  ");
            //SQL.AppendLine("    From TblEmployeeSalary A ");
            //SQL.AppendLine("    Inner Join  ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select A.EmpCode, MAX(A.Con) StartDt ");
            //SQL.AppendLine("        From  ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select EmpCode, Concat(StartDt, DNo) As Con  ");
            //SQL.AppendLine("            From TblEmployeeSalary ");
            //SQL.AppendLine("            Where ActInd = 'Y' ");
            //SQL.AppendLine("        )A ");
            //SQL.AppendLine("        Group By A.EmpCode ");
            //SQL.AppendLine("    )B On A.EmpCode = B.EmpCode And Concat(A.StartDt, A.Dno) = B.StartDt ");
            //SQL.AppendLine(")E On A.EmpCode = E.EmpCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Distinct T1.EmpCode, T1.Amt  ");
            SQL.AppendLine("    From TblEmployeeSalary T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select EmpCode, Max(StartDt) As Dt ");
            SQL.AppendLine("        From TblEmployeeSalary ");
            SQL.AppendLine("        Where Amt<>0.00 ");
            SQL.AppendLine("        Group By EmpCode ");
            SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.Dt ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr G On F.LevelCode=G.LevelCode ");
            SQL.AppendLine("Left Join TblLevelHdr G2 On A.LevelCode=G2.LevelCode ");
            SQL.AppendLine("Left Join TblOption H On A.EmploymentStatus=H.OptCode And H.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction T2 On T1.ADCode=T2.ADCode And T2.AmtType='1' And T2.ADType='A' ");
            SQL.AppendLine("    Where T1.StartDt<=@CurrentDate And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("    Group By T1.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeSalarySS T1 ");
            SQL.AppendLine("    Where T1.StartDt<=@CurrentDate And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("    And Find_In_Set(SSPCode,(Select ParValue From TblParameter Where ParCode='SSPCodeForPension' And ParValue Is Not Null)) ");
            SQL.AppendLine("    Group By T1.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");
            SQL.AppendLine("Left Join TblOption K On A.Religion=K.OptCode And K.OptCat='Religion' ");
            SQL.AppendLine("Left Join TblBank L On A.BankCode = L.BankCode ");
            SQL.AppendLine("Left Join TblDivision M On A.DivisionCode = M.DivisionCode ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            switch (Sm.GetLue(LueStatus))
            {
                case "1":
                    Filter = " And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ";
                    break;
                case "2":
                    Filter = " And A.ResignDt Is Not Null And A.ResignDt<=@CurrentDate ";
                    break;
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
            
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueReligion), "A.Religion", true);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + Filter + " Order By A.EmpName;",
                new string[] { 
                    "EmpCode", 
                    "EmpName", "EmpCodeOld", "PosName", "GrdLvlName", "LevelName", 
                    "DeptName", "SiteName", "EmploymentSTatusDesc", "ReligionDesc", "JoinDt", 
                    "ResignDt", "BankName", "NonPHDP", "PHDP", "Amt", 
                    "Brutto", "DivisionName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                }, true, false, false, false
            );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            //iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 15, 16, 17 });
        }

        private void Process2()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                for (int c = 20; c < Grd1.Cols.Count; c++)
                    Grd1.Cells[r, c].Value = 0m;
            }
        }

        private void Process3()
        {
            string
                Filter = string.Empty,
                EmpCode = string.Empty,
                ADCode = string.Empty,
                EmpCodeTemp = string.Empty,
                ADCodeTemp = string.Empty;
            decimal Amt = 0m;
            int r = 0;
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + Row.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText =
                        "Select EmpCode, ADCode, Amt " +
                        "From TblEmployeeAllowanceDeduction " +
                        "Where StartDt<=@CurrentDate And @CurrentDate<=EndDt " +
                        Filter + " Order By EmpCode;";
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "Amt" });
                    if (dr.HasRows)
                    {
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            EmpCode = dr.GetString(c[0]);
                            ADCode = dr.GetString(c[1]);
                            Amt = dr.GetDecimal(c[2]);

                            if (Sm.CompareStr(EmpCode, EmpCodeTemp))
                            {
                                for (int x = 0; x < l.Count; x++)
                                {
                                    if (Sm.CompareStr(ADCode, l[x].ADCode))
                                    {
                                        Grd1.Cells[r, l[x].Col].Value = Sm.GetGrdDec(Grd1, r, l[x].Col) + Amt;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < Grd1.Rows.Count; i++)
                                {
                                    if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, i, 1)))
                                    {
                                        for (int x = 0; x < l.Count; x++)
                                        {
                                            if (Sm.CompareStr(ADCode, l[x].ADCode))
                                            {
                                                Grd1.Cells[i, l[x].Col].Value = Sm.GetGrdDec(Grd1, i, l[x].Col) + Amt;
                                                break;
                                            }
                                        }
                                        r = i;
                                        break;
                                    }
                                }
                                EmpCodeTemp = EmpCode;
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                }
        }

        private void SetAD(ref List<AD> l)
        {
            var GrdCol = 19;
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select ADCode, ADName From TblAllowanceDeduction Order By ADType, ADName;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ADCode", "ADName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new AD()
                        {
                            ADCode = Sm.DrStr(dr, c[0]),
                            ADName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                //Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                //Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
            }
            Grd1.EndUpdate();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, r, 1).Length != 0)
            {
                e.DoDefault = false;
                ShowBasicSalaryComponent(Sm.GetGrdStr(Grd1, r, 1));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, r, 1).Length != 0)
                ShowBasicSalaryComponent(Sm.GetGrdStr(Grd1, r, 1));
        }

        private void ShowBasicSalaryComponent(string EmpCode)
        {
            var SQL = new StringBuilder();
            var Msg = new StringBuilder();

            SQL.AppendLine("Select Concat('Salary : ', Convert(Format(A.Amt, 2) Using utf8)) As Components ");
            SQL.AppendLine("From TblEmployeeSalary A  ");
            SQL.AppendLine("Inner Join (  ");
	        SQL.AppendLine("    Select Max(StartDt) StartDt  ");
	        SQL.AppendLine("    From TblEmployeeSalary  ");
	        SQL.AppendLine("    Where EmpCode=@EmpCode  ");
	        SQL.AppendLine("    And StartDt<=@Dt ");
            SQL.AppendLine(") B On A.StartDt=B.StartDt ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(B.ADName, ' : ', Convert(Format(A.Amt, 2) Using utf8)) As Components ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And ((A.StartDt Is Null And A.EndDt Is Null) Or  ");
	        SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@Dt) Or  ");
	        SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @Dt<=A.EndDt) Or  ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@Dt And @Dt<=A.EndDt))  ");
            SQL.AppendLine("And Find_In_Set(A.ADCode, IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeBasicSalaryComponent'), '')) ;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParamDt(ref cm, "@Dt", Sm.ServerCurrentDateTime());
                    
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "Components" });
                    if (dr.HasRows)
                    {
                        while (dr.Read()) Msg.AppendLine(Sm.DrStr(dr, c[0]));
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue2(Sl.SetLueOption), "Religion");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkReligion_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Religion");
        }

        #endregion

        #endregion

        #region Class

        private class AD
        {
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public int Col { get; set; }
        }

        #endregion

    }
}
