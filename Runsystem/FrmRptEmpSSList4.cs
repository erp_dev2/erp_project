﻿#region Update
/*
    16/05/2018 [TKG] tambah entity
    03/09/2020 [TKG/SRN] Parameter SSProgramForEmployment bisa diisi lbh dari 1 kode ss
    24/06/2021 [TKG/IMS] department mengambil dari emp ss list
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSSList4 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSSSalaryInd = string.Empty 
            //mSSProgramForEmployment = string.Empty,
            //mSSProgramForPension = string.Empty
            ;
        private List<SS> mListofSS = new List<SS>();
        private bool mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptEmpSSList4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                GenerateSS();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);

                var Dt = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mSSSalaryInd = Sm.GetParameter("SSSalaryInd");
            //mSSProgramForEmployment = Sm.GetParameter("SSProgramForEmployment");
            //mSSProgramForPension = Sm.GetParameter("SSProgramForPension");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySite");
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 12].Value = "'" + Sm.GetGrdStr(Grd1, Row, 12);
                Grd1.Cells[Row, 15].Value = "'" + Sm.GetGrdStr(Grd1, Row, 15);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 12].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 12), Sm.GetGrdStr(Grd1, Row, 12).Length - 1);
                Grd1.Cells[Row, 15].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 15).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, G.SiteName, I.EntName, H.OptDesc As PayrunPeriod, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, B.IDNo, B.BirthDt, B.Age, B.CardNo, B.StartDt, ");
            SQL.AppendLine("B.SSCode, B.Salary, B.EmployerAmt, B.EmployeeAmt, B.TotalAmt ");
            SQL.AppendLine("From TblEmpSSListHdr A ");
            SQL.AppendLine("Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode " + Filter);
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblSS F On B.SSCode=F.SSCode ");
            if (mSSSalaryInd == "1")
            {
                //SQL.AppendLine("And F.SSPCode=@SSProgramForEmployment ");
                SQL.AppendLine("        And Find_In_Set( ");
                SQL.AppendLine("            IfNull(F.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("        ) ");
            }
            else
            {
                //SQL.AppendLine("And F.SSPCode In (@SSProgramForEmployment, @SSProgramForPension) ");
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        Find_In_Set( ");
                SQL.AppendLine("            IfNull(F.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        Or ");
                SQL.AppendLine("        Find_In_Set( ");
                SQL.AppendLine("            IfNull(F.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForPension' And ParValue Is Not Null), '') ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        )");
            }
            SQL.AppendLine("Left Join TblSite G On C.SiteCode=G.SiteCode ");
            SQL.AppendLine("Left Join TblOption H On H.OptCat='PayrunPeriod' And C.PayrunPeriod=H.OptCode ");
            SQL.AppendLine("Left Join TblEntity I On A.EntCode=I.EntCode ");
            SQL.AppendLine("Where A.Yr=@Yr And A.Mth=@Mth And CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (A.SiteCode Is Null Or (");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("Order By E.DeptName, B.EmpCode;");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.Header.Rows.Count = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Participants",
                        "Old"+Environment.NewLine+"Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Site",
                        "Entity",
                        "Payrun Period",
                        "Join",
                        
                        //11-15
                        "Resign",
                        "Identity#",
                        "Birth Date",
                        "Age",
                        "Card#",

                        //16
                        "Start"+Environment.NewLine+"Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 200, 80, 150, 
                        
                        //6-10
                        150, 150, 200, 150, 100, 
                        
                        //11-15
                        100, 150, 100, 80, 130, 
                        
                        //16
                        100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 14 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11, 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 10, 13 }, false);

            for (int Col = 0; Col <= Grd1.Cols.Count-1; Col++)
                Grd1.Header.Cells[0, Col].SpanRows = 2;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 10, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                int Index = -1;
                string Filter = " And 1=1 ";
                string EmpCode = string.Empty, SSCode = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "C.PayrunPeriod", true);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<String>(ref cm, "@SSProgramForEmployment", mSSProgramForEmployment);
                //Sm.CmParam<String>(ref cm, "@SSProgramForPension", mSSProgramForPension);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.ClearGrd(Grd1, false);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = GetSQL(Filter);
                    cm.CommandTimeout = 600;
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] 
                            { 
                                //0
                                "EmpCode",  

                                //1-5
                                "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName", 
                                
                                //6-10
                                "EntName", "PayrunPeriod", "JoinDt", "ResignDt", "IDNo", 
                                
                                //11-15
                                "BirthDt", "Age", "CardNo", "StartDt", "SSCode", 
                                
                                //16-19
                                "Salary", "EmployerAmt", "EmployeeAmt", "TotalAmt"
                            });
                        if (!dr.HasRows)
                            Sm.StdMsg(mMsgType.NoData, "");
                        else
                        {
                            int Row = 0, r = 0;
                            Grd1.Rows.Count = 0;
                            Grd1.BeginUpdate();
                            while (dr.Read())
                            {
                                if (!Sm.CompareStr(EmpCode, Sm.DrStr(dr, 0)))
                                {
                                    Grd1.Rows.Add();
                                    r++;
                                    Row = r - 1;
                                    Grd1.Cells[Row, 0].Value = r;
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 8);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 9);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 11);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 12);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);

                                    SSCode = Sm.DrStr(dr, 15);
                                    if (Grd1.Cols.Count > 16)
                                    {
                                        for (int Col = 17; Col <= Grd1.Cols.Count - 1; Col++)
                                            Grd1.Cells[Row, Col].Value = 0m;
                                    }
                                }
                                else
                                    SSCode = Sm.DrStr(dr, 15);

                                Index = GetCol(SSCode);
                                if (Index >= 0)
                                {
                                    Index *= 4;
                                    Grd1.Cells[Row, 13 + Index].Value = Sm.DrDec(dr, 16);
                                    Grd1.Cells[Row, 14 + Index].Value = Sm.DrDec(dr, 17);
                                    Grd1.Cells[Row, 15 + Index].Value = Sm.DrDec(dr, 18);
                                    Grd1.Cells[Row, 16 + Index].Value = Sm.DrDec(dr, 19);
                                }
                                EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                            }
                        }

                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        for (int Col = 17; Col <= Grd1.Cols.Count - 1; Col++)
                            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { Col });

                        Grd1.EndUpdate();
                        dr.Close();
                        dr.Dispose();
                        cm.Dispose();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private int GetCol(string SSCode)
        {
            foreach (SS s in mListofSS)
            {
                if (Sm.CompareStr(s.SSCode, SSCode))
                    return s.Index;
            }
            return -1;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GenerateSS()
        {
            int Index = 0, Col1 = 0, Col2 = 0, Col3 = 0, Col4 = 0;
            var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@SSProgramForEmployment", mSSProgramForEmployment);
            //if (mSSSalaryInd != "1") Sm.CmParam<String>(ref cm, "@SSProgramForPension", mSSProgramForPension);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select SSCode, SSName From TblSS ");
            //SQL.AppendLine("Where (SSPCode=@SSProgramForEmployment ");
            //if (mSSSalaryInd != "1") SQL.AppendLine("Or SSPCode=@SSProgramForPension ");
            //SQL.AppendLine(");");

            SQL.AppendLine("Where SSPCode Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Find_In_Set( ");
            SQL.AppendLine("        IfNull(SSPCode, ''), ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
            SQL.AppendLine("        ) ");
            if (mSSSalaryInd != "1")
            {
                SQL.AppendLine("    Or ");
                SQL.AppendLine("    Find_In_Set( ");
                SQL.AppendLine("        IfNull(SSPCode, ''), ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForPension' And ParValue Is Not Null), '') ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("); ");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(), new string[] { "SSCode", "SSName" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Index += 1;
                        mListofSS.Add(new SS()
                        {
                            Index = Index,
                            SSCode = Sm.DrStr(dr, c[0])
                        });
                        Grd1.Cols.Count += 4;
                        Col1 = 13 + (Index * 4);
                        Col2 = 14 + (Index * 4);
                        Col3 = 15 + (Index * 4);
                        Col4 = 16 + (Index * 4);

                        Grd1.Header.Cells[1, Col1].Value = Sm.DrStr(dr, c[1]);
                        Grd1.Header.Cells[1, Col1].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Header.Cells[1, Col1].SpanCols = 4;

                        Grd1.Cols[Col1].Text = "Salary";
                        Grd1.Cols[Col1].Width = 130;
                        Grd1.Cols[Col1].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col1].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col2].Text = "Employer";
                        Grd1.Cols[Col2].Width = 130;
                        Grd1.Cols[Col2].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col2].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col3].Text = "Employee";
                        Grd1.Cols[Col3].Width = 130;
                        Grd1.Cols[Col3].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col3].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col4].Text = "Total";
                        Grd1.Cols[Col4].Width = 130;
                        Grd1.Cols[Col4].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col4].TextAlign = iGContentAlignment.MiddleCenter;

                        Sm.GrdFormatDec(Grd1, new int[] { Col1, Col2, Col3, Col4 }, 0);
                        Sm.GrdColReadOnly(Grd1, new int[] { Col1, Col2, Col3, Col4 });
                    }, false
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun Period");
        }

        #endregion

        #endregion

        #region Class

        internal class SS
        {
            public int Index { get; set; }
            public string SSCode { get; set; }
        }

        #endregion
    }
}
