﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace RunSystem
{
    public sealed class WinAPI
    {
        internal struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }

        [DllImport("user32.dll")]
        static extern void SystemParametersInfo(uint uiAction, uint uiParam, ref int pvParam,
                                                uint fWinIni);

        const uint SPI_SETKEYBOARDCUES = 0x100B; /* Constants used for User32 calls. */

        /// Change the setting programmatically for keyboard shortcut Issue 
        public static void GetAltKeyFixed()
        {
            int pv = 1;
            /* Call to systemparametersinfo to set true of pv variable.*/
            SystemParametersInfo(SPI_SETKEYBOARDCUES, 0, ref pv, 0);
            //Set pvParam to TRUE to always underline menu access keys, 
        }

        //Auto Log Off
        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        public static uint GetIdleTime()
        {
            LASTINPUTINFO lastInPut = new LASTINPUTINFO();
            lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            return ((uint)Environment.TickCount - lastInPut.dwTime);
        }
    }
}
