﻿#region Update
/*
  27/04/2020 [DITA/SIER] Menambahkan filter item category
  10/11/2021 [RIS/AMKA] Menampilkan asset list berdasarkan cost center group user
  11/02/2022 [MYA/PHT] Ketika loop master asset DBA ada tambahan multi profit center agar pencarian master asset di DBA bisa terfilter berdasarkan profit centernya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAssetDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDepreciationAsset mFrmParent;
        private string mSQL = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmDepreciationAssetDlg(FrmDepreciationAsset FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                LblMultiProfitCenterCode.Visible = CcbProfitCenterCode.Visible = ChkProfitCenterCode.Visible = mFrmParent.mIsFicoUseMultiProfitCenterFilter;
                
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }

                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AssetCode, Assetname, DisplayName, AssetValue, EcoLife, B.OptDesc As Depreciation, ");
            SQL.AppendLine("PercentageAnnualDepreciation, C.ItCtCode ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblOption B On A.DepreciationCode = B.OptCode And Optcat = 'DepreciationMethod'");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("Left Join TblCostCenter E ON A.CCCode=E.CCCode ");
            SQL.AppendLine("Where ActiveInd = 'Y' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("(Select T.AssetCode From TblDepreciationAssetHdr T Where A.AssetCode = T.AssetCode And T.CancelInd = 'N') ");
            if (mFrmParent.mIsDBACCFilteredByGroup)
            {
                SQL.AppendLine("AND EXISTS ( ");
                SQL.AppendLine("    SELECT 1 FROM TblGroupCostCenter ");
                SQL.AppendLine("    WHERE CCCode=A.CCCode ");
                SQL.AppendLine("    AND GrpCode IN (SELECT GrpCode FROM TblUser WHERE ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "Asset Code", 
                        "", 
                        "Asset Name",
                        "Display Name",
                        "Asset Value",
                        //6-8
                        "Economic Life",
                        "Depreciation",
                        "Percentage"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        100, 20, 150, 150, 150,
                        //6-10
                        100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] {5, 6, 8}, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                string mQueryProfitCenter = string.Empty;

                var cm = new MySqlCommand();

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SetProfitCenter();
                    mQueryProfitCenter += "And E.ProfitCenterCode Is Not Null ";

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_2.Length > 0) Filter_2 += " Or ";
                            Filter_2 += " (E.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_2.Length == 0)
                            mQueryProfitCenter += "    And 1=0 ";
                        else
                            mQueryProfitCenter += "    And (" + Filter_2 + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            mQueryProfitCenter += "    And Find_In_Set(E.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter += "    And E.ProfitCenterCode In ( ";
                            mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter += "    ) ";
                        }
                    }
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "AssetCode", "AssetName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "C.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + mQueryProfitCenter + Filter + " Order By AssetName;",
                        new string[] 
                        { 
                            //0
                            "AssetCode",
 
                            //1-5
                            "AssetName", "DisplayName", "AssetValue", "EcoLife", "Depreciation",
                            //6
                            "PercentageAnnualDepreciation"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ClearGrd();
                    mFrmParent.ShowAssetInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                    mFrmParent.SetMthGrd();
                    mFrmParent.SetCostValue();
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Additional Method

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            //if (mIsFilterByProfitCenter)
            //{
            SQL.AppendLine("WHERE Exists(  ");
            SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
            SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine(") ");
            //}
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }
        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        #endregion       

    }
}
