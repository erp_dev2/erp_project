﻿#region Update
/* 
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;

using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItem : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mItCode = string.Empty,
            mInvestmentCode = string.Empty; //if this application is called from other application
        internal bool
            mIsFilterByItCt = false,
            mIsAccessUsed = false; //Used In Document Approval (PO Request)
        internal FrmInvestmentItemFind FrmFind;
        private string
            mInvestmentCodeInternalNotEmptyInd = string.Empty,
            mSIWeightUom = string.Empty,
            mProcFormatDocNo = string.Empty,
            mItemVolumeUom = string.Empty,
            mItemLWHUom = string.Empty,
            mItemPicture = string.Empty,
            mItCodeSeqNo = string.Empty,
            mItCtCodeForPOService = string.Empty
            ;
        private int NumberOfInventoryUomCode = 1, NumberOfSalesUomCode = 1;
        private bool 
            mIsItemInventoryNotShowCalculationData = false,
            mIsItGrpCodeMandatoryInMasterItem = false,
            mIsAbleToEditItemUom = false,
            mIsInsert = true,
            mIsAbleToEditItemCategory = false,
            mIsItemItCodeInternalMandatory = false,
            mIsItemAllowToUploadFile = false,
            mIsItCodeUseItSeqNo = false,
            mIsItemConversionRate1Editable = false; //Tab Sales Kolom Item Conversi Rate Editable 


        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        private byte[] downloadedData;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmInvestmentItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Investment Item";

            try
            {
                GetParameter();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);


                //if this application is called from other application
                if (mInvestmentCode.Length != 0)
                {
                    ShowData(mInvestmentCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = mIsAccessUsed;
                } 
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInvestmentCode, TxtInvestmentName, TxtForeignName, TxtInvestmentCodeInternal, TxtInvestmentCodeOld, LueInvestmentCtCode, LueInventoryUomCode,
                        MeeSpecification, MeeRemark, ChkActInd,  TxtSource, 
                        TxtPublisherCode
                    }, true);
                    BtnItem.Enabled = false;
                    TxtInvestmentCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInvestmentName, TxtForeignName, TxtInvestmentCodeInternal, TxtInvestmentCodeOld, LueInvestmentCtCode,  LueInventoryUomCode,
                        MeeSpecification, MeeRemark, LueInventoryUomCode, 
                        ChkActInd, TxtPublisherCode, 
                    }, false);

                    TxtInvestmentCode.Focus();
                    BtnItem.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInvestmentName, TxtForeignName, TxtInvestmentCodeInternal, TxtInvestmentCodeOld, MeeSpecification, MeeRemark,
                        ChkActInd, TxtPublisherCode,
                    }, false);
                    if (mIsAbleToEditItemUom)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueInventoryUomCode,
                        }, false);
                    }
                    if (mIsAbleToEditItemCategory) 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueInvestmentCtCode }, false);
                    TxtInvestmentName.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtInvestmentCode, TxtInvestmentName, TxtForeignName, TxtInvestmentCodeInternal, TxtInvestmentCodeOld, 
                LueInvestmentCtCode, MeeSpecification, 
                MeeRemark, LueInventoryUomCode, TxtSource, 
                TxtPublisherCode, 
            });

            ChkActInd.Checked = false;
            
        }

        private void LueRequestEdit(
                    iGrid Grd,
                    DevExpress.XtraEditors.LookUpEdit Lue,
                    ref iGCell fCell,
                    ref bool fAccept,
                    TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

       
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentItemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

     

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                InsertDataClick();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInvestmentCode, "", false)) return;
            mIsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string InvestmentCode = string.Empty;

                if (mIsInsert)
                {
                    var InvestmentCtCode = Sm.GetLue(LueInvestmentCtCode);
                    var InvestmentCtCodeLength = InvestmentCtCode.Length + 1;
                    if (TxtInvestmentCode.Text.Length == 0)
                    {
                        if (mIsItCodeUseItSeqNo)
                        {
                            InvestmentCode = Sm.GetValue(
                                 "Select Concat(@Param1,'-', " +
                                 "   IfNull((Select Right(Concat(Repeat('0', " + mItCodeSeqNo + "), Convert(V+1, Char)), " + mItCodeSeqNo + ") From ( " +
                                 "      Select ItSeqNo As V " +
                                 "      From TblInvestmentItem " +
                                 "      Where Left(InvestmentCode, @Param2)=Concat(@Param1, '-') " +
                                 "      Order By ItSeqNo Desc Limit 1 " +
                                 "      ) As Tbl), Right(Concat(Repeat('0', " + mItCodeSeqNo + "), '1'), "+mItCodeSeqNo+") " +
                                 "      )) As InvestmentCode; ",
                                 InvestmentCtCode, (InvestmentCtCode.Length + 1).ToString(), string.Empty);
                        }
                        else
                        {
                            InvestmentCode = Sm.GetValue(
                                 "Select Concat(@Param1,'-', " +
                                 "   IfNull((Select Right(Concat('00000', Convert(InvestmentCode+1, Char)), 5) From ( " +
                                 "       Select Convert(Right(InvestmentCode, 5), Decimal) As InvestmentCode " +
                                 "      From TblInvestmentItem " +
                                 "      Where Left(InvestmentCode, @Param2)=Concat(@Param1, '-') " +
                                 "      Order By Right(InvestmentCode, 5) Desc Limit 1 " +
                                 "       ) As TblItemTemp), '00001')) As InvestmentCode; ",
                                 InvestmentCtCode, (InvestmentCtCode.Length + 1).ToString(), string.Empty);
                        }
                    }
                    else
                        InvestmentCode = TxtInvestmentCode.Text;
                }
                else
                {
                    InvestmentCode = TxtInvestmentCode.Text;
                }
                var cml = new List<MySqlCommand>();

                cml.Add(SaveItem(InvestmentCode));

               
                Sm.ExecCommands(cml);

                ShowData(InvestmentCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string InvestmentCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItem(InvestmentCode);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowData2(string InvestmentCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowItem2(InvestmentCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItem(string InvestmentCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From TblInvestmentItem T Where T.InvestmentCode=@InvestmentCode ");
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@InvestmentCode", InvestmentCode);
            if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "InvestmentCode",
                        
                        //1-5
                        "InvestmentName", "ForeignName", "InvestmentCtCode", "ItScCode", "ItBrCode", 
                        
                        //6-10
                        "Specification", "Remark", "VdCode", "PurchaseUomCode", "TaxCode1", 
                        
                        //11-15
                        "TaxCode2", "TaxCode3", "LengthUomCode", "HeightUomCode", "WidthUomCode", 
                        
                        //16-20
                        "VolumeUomCode", "SalesUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3",  
                        
                        //21-25
                        "PlanningUomCode", "ItPropCode1", "ItPropCode2", "ItPropCode3", "ItPropCode4",  
                        
                        //26-30
                        "ItPropCode5", "ItPropCode6", "ItPropCode7", "ItPropCode8", "ItPropCode9", 
                        
                        //31-35
                        "ItPropCode10", "ItPropCode11", "ItPropCode12", "ItPropCode13", "ItPropCode14",  
                        
                        //36-40
                        "ItPropCode15", "MinOrder", "Length", "Height", "Width",
                        
                        //41-45
                        "Volume", "MinStock", "MaxStock","ReOrderStock", "ActInd", 
                        
                        //46-50
                        "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd",
                        
                        //51-55
                        "TaxLiableInd", "ControlByDeptCode", "DiameterUomCode", "Diameter", "InvestmentCodeInternal",
 
                        //56-60
                        "PGCode", "SalesUomCode2", "SalesUomCodeConvert12", "SalesUomCodeConvert21", "InventoryUomCodeConvert12",

                        //61-65
                        "InventoryUomCodeConvert13", "InventoryUomCodeConvert21", "InventoryUomCodeConvert23", "InventoryUomCodeConvert31", "InventoryUomCodeConvert32",

                        //66-70
                        "PlanningUomCode2", "ItGrpCode", "Information1", "Information2", "Information3", 
                        
                        //71-75
                        "Information4", "Information5", "PlanningUomCodeConvert12", "PlanningUomCodeConvert21", "ItemRequestDocNo",

                        //76-80
                        "HSCode", "ServiceItemInd", "Weight", "WeightUomCode", "InvestmentCodeOld"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (TxtSource.Text.Length == 0)
                        {
                            TxtInvestmentCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtInvestmentName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtForeignName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtInvestmentCodeInternal.EditValue = Sm.DrStr(dr, c[55]);
                        }
                        SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[3]));
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[6]); 
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        Sl.SetLueUomCode(ref LueInventoryUomCode);
                        Sm.SetLue(LueInventoryUomCode, Sm.DrStr(dr, c[18]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[45]), "Y");
                        TxtPublisherCode.EditValue = Sm.DrStr(dr, c[76]);
                        TxtInvestmentCodeOld.EditValue = Sm.DrStr(dr, c[80]);
                    }, true
                );
        }

        public void ShowItem2(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select InvestmentCode, InvestmentName, ");
            SQL.AppendLine("'Y' As ActInd, InvestmentCodeInternal, ForeignName, InventoryItemInd, SalesItemInd, PurchaseItemInd, ");
            SQL.AppendLine("FixedItemInd, PlanningItemInd, InvestmentCtCode, ItScCode, ItBrCode, ItGrpCode, ItemRequestDocNo, ");
            SQL.AppendLine("Specification, Remark, TaxLiableInd, VdCode, PurchaseUomCode, PGCode, MinOrder, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, Length, Height, Width, Volume, Diameter, ");
            SQL.AppendLine("LengthUomCode, HeightUomCode, WidthUomCode, VolumeUomCode, DiameterUomCode, ");
            SQL.AppendLine("SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, SalesUomCodeConvert21, InventoryUomCode, ");
            SQL.AppendLine("InventoryUomCode2, InventoryUomCode3, InventoryUomCodeConvert12, InventoryUomCodeConvert13, ");
            SQL.AppendLine("InventoryUomCodeConvert21, InventoryUomCodeConvert23, InventoryUomCodeConvert31, InventoryUomCodeConvert32, ");
            SQL.AppendLine("MinStock, MaxStock, ReOrderStock, ControlByDeptCode, PlanningUomCode, PlanningUomCode2, ");
            SQL.AppendLine("PlanningUomCodeConvert12, PlanningUomCodeConvert21, Information1, Information2, Information3, ");
            SQL.AppendLine("Information4, Information5, ItPropCode1, ItPropCode2, ItPropCode3, ItPropCode4, ");
            SQL.AppendLine("ItPropCode5, ItPropCode6, ItPropCode7, ItPropCode8, ItPropCode9, ItPropCode10, ");
            SQL.AppendLine("ItPropCode11, ItPropCode12, ItPropCode13, ItPropCode14, ItPropCode15, ");
            SQL.AppendLine("Weight, WeightUomCode, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblInvestmentItem T Where T.InvestmentCode=@InvestmentCode ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "InvestmentCode",
                        
                        //1-5
                        "InvestmentName", "ForeignName", "InvestmentCtCode", "ItScCode", "ItBrCode", 
                        
                        //6-10
                        "Specification", "Remark", "VdCode", "PurchaseUomCode", "TaxCode1", 
                        
                        //11-15
                        "TaxCode2", "TaxCode3", "LengthUomCode", "HeightUomCode", "WidthUomCode", 
                        
                        //16-20
                        "VolumeUomCode", "SalesUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3",  
                        
                        //21-25
                        "PlanningUomCode", "ItPropCode1", "ItPropCode2", "ItPropCode3", "ItPropCode4",  
                        
                        //26-30
                        "ItPropCode5", "ItPropCode6", "ItPropCode7", "ItPropCode8", "ItPropCode9", 
                        
                        //31-35
                        "ItPropCode10", "ItPropCode11", "ItPropCode12", "ItPropCode13", "ItPropCode14",  
                        
                        //36-40
                        "ItPropCode15", "MinOrder", "Length", "Height", "Width",
                        
                        //41-45
                        "Volume", "MinStock", "MaxStock","ReOrderStock", "ActInd", 
                        
                        //46-50
                        "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd",
                        
                        //51-55
                        "TaxLiableInd", "ControlByDeptCode", "DiameterUomCode", "Diameter", "ItCodeInternal",
 
                        //56-60
                        "PGCode", "SalesUomCode2", "SalesUomCodeConvert12", "SalesUomCodeConvert21", "InventoryUomCodeConvert12",

                        //61-65
                        "InventoryUomCodeConvert13", "InventoryUomCodeConvert21", "InventoryUomCodeConvert23", "InventoryUomCodeConvert31", "InventoryUomCodeConvert32",

                        //66-70
                        "PlanningUomCode2", "ItGrpCode", "Information1", "Information2", "Information3", 
                        
                        //71-75
                        "Information4", "Information5", "PlanningUomCodeConvert12", "PlanningUomCodeConvert21", "ItemRequestDocNo",

                        //76-77
                        "Weight", "WeightUomCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (TxtSource.Text.Length == 0)
                        {
                            TxtInvestmentCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtInvestmentName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtForeignName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtInvestmentCodeInternal.EditValue = Sm.DrStr(dr, c[55]);
                        }
                        SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[3]));
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        Sl.SetLueUomCode(ref LueInventoryUomCode);
                        Sm.SetLue(LueInventoryUomCode, Sm.DrStr(dr, c[18]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[45]), "Y");
                    }, true
                );
        }

     
        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                //Sm.IsTxtEmpty(TxtItCode, "Item code", false) ||
                Sm.IsTxtEmpty(TxtInvestmentName, "Item name", false) ||
                (mIsItemItCodeInternalMandatory && Sm.IsTxtEmpty(TxtInvestmentCodeInternal, "Item's local code", false)) ||
                IsInvestmentNameExisted() ||
                IsInvestmentCodeInternalExisted() ||
                IsInvestmentCodeInternalEmpty() ||
                Sm.IsLueEmpty(LueInvestmentCtCode, "Investment's category") ||
                Sm.IsLueEmpty(LueInventoryUomCode, "Item's inventory UoM") ||
                IsItCodeExisted();
        }

        

        

        private bool IsInvestmentNameExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select InvestmentCode From TblInvestmentItem Where InvestmentName=@InvestmentName And InvestmentCode<>@InvestmentCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@InvestmentCode", TxtInvestmentCode.Text.Length == 0 ? "XXX" : TxtInvestmentCode.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentName", TxtInvestmentName.Text);

            string InvestmentCode = Sm.GetValue(cm);
            if (InvestmentCode.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Investment Code : " + InvestmentCode + Environment.NewLine +
                    "Investment Name : " + TxtInvestmentName.Text + Environment.NewLine + Environment.NewLine +
                    "Existing Investment code With the same name." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsInvestmentCodeInternalExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select InvestmentCode From TblInvestmentItem Where InvestmentCodeInternal=@InvestmentCodeInternal And InvestmentCode<>@InvestmentCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@InvestmentCode", TxtInvestmentCode.Text.Length == 0 ? "XXX" : TxtInvestmentCode.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCodeInternal", TxtInvestmentCodeInternal.Text);

            string InvestmentCode = Sm.GetValue(cm);
            if (InvestmentCode.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Investment Code : " + InvestmentCode + Environment.NewLine +
                    "Investment local code : " + TxtInvestmentCodeInternal.Text + Environment.NewLine + Environment.NewLine +
                    "Existing Investment code With the same local code." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsInvestmentCodeInternalEmpty()
        {
            if (mInvestmentCodeInternalNotEmptyInd != "Y") return false;

            if (TxtInvestmentCodeInternal.Text.Length == 0)
            {
                return Sm.StdMsgYN("Question",
                    "Investment Name : " + TxtInvestmentName.Text + Environment.NewLine + Environment.NewLine +
                    "Investment local code is empty." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
            return false;
        }

        private bool IsItCodeExisted()
        {
            if (!TxtInvestmentCode.Properties.ReadOnly && Sm.IsDataExist("Select 1 From TblItem Where ItCode='" + TxtInvestmentCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Item code ( " + TxtInvestmentCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItem(string InvestmentCode)
        {
            var SQL = new StringBuilder();

            if (mIsInsert)
            {
                SQL.AppendLine("Insert Into TblInvestmentItem(InvestmentCode, InvestmentName, ItSeqNo, ForeignName, InvestmentCodeInternal, InvestmentCodeOld, InvestmentCtCode, ItScCode, ItemRequestDocNo, ");
                SQL.AppendLine("    ItBrCode, ItGrpCode, Specification, Remark, VdCode, PurchaseUomCode, PGCode, ");
                SQL.AppendLine("    TaxCode1, TaxCode2, TaxCode3, LengthUomCode, HeightUomCode, DiameterUomCode, ");
                SQL.AppendLine("    WidthUomCode, VolumeUomCode, WeightUomCode, SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, ");
                SQL.AppendLine("    SalesUomCodeConvert21, InventoryUomCode, InventoryUomCode2, InventoryUomCode3, PlanningUomCode, PlanningUomCode2, ");
                SQL.AppendLine("    MinOrder, Length, Height, Width, Volume, Weight, Diameter, ");
                SQL.AppendLine("    MinStock, MaxStock, ReOrderStock, ControlByDeptCode, ActInd, InventoryItemInd, ");
                SQL.AppendLine("    SalesItemInd, PurchaseItemInd, FixedItemInd, PlanningItemInd, ServiceItemInd,TaxLiableInd, ");
                SQL.AppendLine("    InventoryUomCodeConvert12, InventoryUomCodeConvert13, InventoryUomCodeConvert21, InventoryUomCodeConvert23, ");
                SQL.AppendLine("    InventoryUomCodeConvert31, InventoryUomCodeConvert32, ");
                SQL.AppendLine("    PlanningUomCodeConvert12, PlanningUomCodeConvert21, ");
                SQL.AppendLine("    Information1, Information2, Information3, Information4, Information5, HSCode, ");
                SQL.AppendLine("    CreateBy, CreateDt)");
                SQL.AppendLine("Values(@InvestmentCode, @InvestmentName, @ItSeqNo, @ForeignName, @InvestmentCodeInternal, @InvestmentCodeOld, @InvestmentCtCode, @ItScCode, @ItemRequestDocNo, ");
                SQL.AppendLine("    @ItBrCode, @ItGrpCode, @Specification, @Remark, @VdCode, @PurchaseUomCode, @PGCode, ");
                SQL.AppendLine("    @TaxCode1, @TaxCode2, @TaxCode3, @LengthUomCode, @HeightUomCode, @DiameterUomCode, ");
                SQL.AppendLine("    @WidthUomCode, @VolumeUomCode, @WeightUomCode, @SalesUomCode, @SalesUomCode2, ");
                SQL.AppendLine("    Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert21 End, ");
                SQL.AppendLine("    @InventoryUomCode, @InventoryUomCode2, @InventoryUomCode3, @PlanningUomCode, @PlanningUomCode2, ");
                SQL.AppendLine("    @MinOrder, @Length, @Height, @Width, @Volume, @Weight, @Diameter, ");
                SQL.AppendLine("    @MinStock, @MaxStock, @ReOrderStock, @ControlByDeptCode, @ActInd, @InventoryItemInd, ");
                SQL.AppendLine("    @SalesItemInd, @PurchaseItemInd, @FixedItemInd, @PlanningItemInd, @ServiceItemInd, @TaxLiableInd, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert13 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert21 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert23 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert31 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert32 End, ");
                SQL.AppendLine("    Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert21 End, ");
                SQL.AppendLine("    @Information1, @Information2, @Information3, @Information4, @Information5, @HSCode, ");
                SQL.AppendLine("    @UserCode, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("Update Tblinvestment Set ");
                if (mProcFormatDocNo == "1")
                    SQL.AppendLine(" InvestmentCtCode = @InvestmentCtCode, ");
                SQL.AppendLine("        InvestmentName=@InvestmentName, ForeignName=@ForeignName, InvestmentCodeInternal=@InvestmentCodeInternal, InvestmentCodeOld=@InvestmentCodeOld, InvestmentCtCode=@InvestmentCtCode, ItScCode=@ItScCode, ItemRequestDocNo = @ItemRequestDocNo, ");
                SQL.AppendLine("        ItBrCode=@ItBrCode, ItGrpCode=@ItGrpCode, Specification=@Specification, Remark=@Remark, VdCode=@VdCode, PurchaseUomCode=@PurchaseUomCode, PGCode=@PGCode, ");
                SQL.AppendLine("        TaxCode1=@TaxCode1, TaxCode2=@TaxCode2, TaxCode3=@TaxCode3, LengthUomCode=@LengthUomCode, HeightUomCode=@HeightUomCode, DiameterUomCode=@DiameterUomCode, ");
                SQL.AppendLine("        WidthUomCode=@WidthUomCode, VolumeUomCode=@VolumeUomCode, WeightUomCode=@WeightUomCode, SalesUomCode=@SalesUomCode, SalesUomCode2=@SalesUomCode2, ");
                SQL.AppendLine("        SalesUomCodeConvert12=Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert12 End, ");
                SQL.AppendLine("        SalesUomCodeConvert21=Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert21 End, ");
                SQL.AppendLine("        InventoryUomCode=@InventoryUomCode, InventoryUomCode2=@InventoryUomCode2, InventoryUomCode3=@InventoryUomCode3, PlanningUomCode=@PlanningUomCode, PlanningUomCode2=@PlanningUomCode2, ");
                SQL.AppendLine("        MinOrder=@MinOrder, Length=@Length, Height=@Height, Width=@Width, Volume=@Volume, Weight=@Weight, Diameter=@Diameter, ");
                SQL.AppendLine("        MinStock=@MinStock, MaxStock=@MaxStock, ReOrderStock=@ReOrderStock, ControlByDeptCode=@ControlByDeptCode, ActInd=@ActInd, InventoryItemInd=@InventoryItemInd, ");
                SQL.AppendLine("        SalesItemInd=@SalesItemInd, PurchaseItemInd=@PurchaseItemInd, FixedItemInd=@FixedItemInd, PlanningItemInd=@PlanningItemInd, ServiceItemInd = @ServiceItemInd, TaxLiableInd=@TaxLiableInd, ");
                SQL.AppendLine("        InventoryUomCodeConvert12=Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert12 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert13=Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert13 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert21=Case When @InventoryUomCode2=@InventoryUomCode Then 1.00 Else @InventoryUomCodeConvert21 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert23=Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert23 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert31=Case When @InventoryUomCode3=@InventoryUomCode Then 1.00 Else @InventoryUomCodeConvert31 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert32=Case When @InventoryUomCode3=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert32 End, ");
                SQL.AppendLine("        PlanningUomCodeConvert12=Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert12 End, ");
                SQL.AppendLine("        PlanningUomCodeConvert21=Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert21 End, ");
                SQL.AppendLine("        Information1=@Information1, Information2=@Information2, Information3=@Information3, Information4=@Information4, Information5=@Information5, HSCode=@HSCode, ");
                SQL.AppendLine("        LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where ItCode=@ItCode; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@InvestmentCode", InvestmentCode);
            Sm.CmParam<String>(ref cm, "@InvestmentName", TxtInvestmentName.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCodeOld", TxtInvestmentCodeOld.Text);
            if (mIsItCodeUseItSeqNo)
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", decimal.Parse(Sm.Right(InvestmentCode, int.Parse(mItCodeSeqNo))));
            else
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", 0m);
            Sm.CmParam<String>(ref cm, "@ForeignName", TxtForeignName.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCodeInternal", TxtInvestmentCodeInternal.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", Sm.GetLue(LueInvestmentCtCode));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode", Sm.GetLue(LueInventoryUomCode));
            Sm.CmParam<String>(ref cm, "@PurchaseUomCode", Sm.GetLue(LueInventoryUomCode));
            Sm.CmParam<String>(ref cm, "@SalesUomCode", Sm.GetLue(LueInventoryUomCode));
            Sm.CmParam<String>(ref cm, "@Specification", MeeSpecification.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@HSCode", TxtPublisherCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
      
        #endregion

        #region Additional Method

        internal protected void InsertDataClick()
        {
            mIsInsert = true;
            ClearData();
            SetFormControl(mState.Insert);

            SetLueInvestmentCtCode(ref LueInvestmentCtCode, string.Empty);
            Sl.SetLueUomCode(ref LueInventoryUomCode);
            ChkActInd.Checked = true;
        }

        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAbleToEditItemCategory', 'IsItemItCodeInternalMandatory', 'ItCodeSeqNo', 'ItCtCodeForPOService', 'IsItemConversionRate1Editable', ");
            SQL.AppendLine("'IsAbleToEditItemUom', 'IsFilterByItCt', 'IsItCodeUseItSeqNo', 'ItemVolumeUom', 'ItemLWHUom', ");
            SQL.AppendLine("'IsItemAllowToUploadFile', 'SIWeightUoM', 'FileSizeMaxUploadFTPClient', 'ProcFormatDocNo', 'IsItGrpCodeMandatoryInMasterItem', ");
            SQL.AppendLine("'ItemInformationTitle1', 'ItemInformationTitle2', 'ItemInformationTitle3', 'ItemInformationTitle4', 'ItemInformationTitle5', ");
            SQL.AppendLine("'SharedFolderForFTPClient', 'HostAddrForFTPClient', 'NumberOfSalesUomCode', 'NumberOfInventoryUomCode', 'IsItemInventoryNotShowCalculationData', ");
            SQL.AppendLine("'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsItemInventoryNotShowCalculationData": mIsItemInventoryNotShowCalculationData = ParValue == "Y"; break;
                            case "IsItGrpCodeMandatoryInMasterItem": mIsItGrpCodeMandatoryInMasterItem = ParValue == "Y"; break;
                            case "IsItemAllowToUploadFile": mIsItemAllowToUploadFile = ParValue == "Y"; break;
                            case "IsItCodeUseItSeqNo": mIsItCodeUseItSeqNo = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsItemConversionRate1Editable": mIsItemConversionRate1Editable = ParValue == "Y"; break;
                            case "IsItemItCodeInternalMandatory": mIsItemItCodeInternalMandatory = ParValue == "Y"; break;
                            case "IsAbleToEditItemCategory": mIsAbleToEditItemCategory = ParValue == "Y"; break;
                            case "IsAbleToEditItemUom": mIsAbleToEditItemUom = ParValue == "Y"; break;

                            //string
                            case "ItemVolumeUom": mItemVolumeUom = ParValue; break;
                            case "ItemLWHUom": mItemLWHUom = ParValue; break;
                            case "SIWeightUoM": mSIWeightUom = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "ProcFormatDocNo": mProcFormatDocNo = ParValue; break;
                            case "ItCtCodeForPOService": mItCtCodeForPOService = ParValue; break;
                            case "ItCodeSeqNo": mItCodeSeqNo = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;

                            //integer
                            case "NumberOfInventoryUomCode": if (ParValue.Length>0) NumberOfInventoryUomCode = int.Parse(ParValue); break;
                            case "NumberOfSalesUomCode": if (ParValue.Length > 0) NumberOfSalesUomCode = int.Parse(ParValue); break;

                        }
                    }
                }
                dr.Close();
            }
            if (mItCodeSeqNo.Length == 0) mItCodeSeqNo = "5";
        }

        private void ShowDataInCtrl(ref MySqlCommand cm, string SQL, string[] ColumnTitle, RefreshData rd, bool ShowNoDataInd)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd)
                    {
                        if (mInvestmentCode.Length==0)
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                        else
                            Sm.StdMsg(mMsgType.Warning, "No authorized to see this information.");
                    }
                }
                else
                {
                    while (dr.Read()) rd(dr, c);
                }
                dr.Close();
                dr.Dispose();
                cm.Dispose();
            }
        }

        #region Setlue




        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueInventoryUomCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInventoryUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
            }
        }
        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtInvestmentCode);
        }

        private void TxtInvestmentName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtInvestmentName);
        }

        private void TxtInvestmentCodeOld_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtInvestmentCodeOld);
        }

        private void TxtForeignName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtForeignName);
        }

        private void TxtItCodeInternal_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtInvestmentCodeInternal);
        }

        private void LueInvestmentCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInvestmentCtCode, new Sm.RefreshLue2(SetLueInvestmentCtCode), string.Empty);
                var ItCtCode = Sm.GetLue(LueInvestmentCtCode);
            }
        }  


      

        #endregion

        
        #region Button Event

        private void BtnItem_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInvestmentItemDlg(this));
        }

      
        

      
        #endregion

        #endregion
    }
}
