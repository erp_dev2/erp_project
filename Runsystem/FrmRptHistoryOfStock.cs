﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHistoryOfStock : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsShowForeignName = false, mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptHistoryOfStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's Name", 
                        "Batch#",
                        
                        //6-7
                        "Source",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 20, 200, 250, 
                        
                        //6-7
                        180, 180
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Grd1.Cols[7].Move(5);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6 }, false);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.ItCode, B.ItName, B.ForeignName, A.BatchNo, A.Source ");
            SQL.AppendLine("From TblStockPrice A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName", "B.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "A.Source", false);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.ItCode, A.BatchNo, A.Source",
                new string[]
                    {
                        "ItCode", "ItName", "BatchNo", "Source", "ForeignName"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (e.ColIndex == 1)
                    Sm.FormShowDialog(new FrmRptHistoryOfStockDlg(
                        this.mAccessInd,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 2),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 5),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 6)
                        ));

                if (e.ColIndex == 3)
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (e.ColIndex == 1)
                    Sm.FormShowDialog(new FrmRptHistoryOfStockDlg(
                        this.mAccessInd,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 2),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 5),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 6)
                        ));

                if (e.ColIndex == 3)
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        #endregion

        #endregion
    }
}
