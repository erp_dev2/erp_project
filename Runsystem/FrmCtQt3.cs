﻿#region Update
/*
    01/03/2018 [HAR] Asset di header pindah ke detail, detail tambahh type fix/variable
    22/05/2018 [WED] BUG saat show data, Ct Contact Person name nggak nampil di combobox nya
    16/02/2022 [TKG] Merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
#endregion

namespace RunSystem
{

    public partial class FrmCtQt3 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal bool
            mIsCtQtUseUPrice2 = false,
            mIsCustomerItemNameMandatory = false;
        internal string mCtQtBusinessProcess = "1";
        private string 
            mMainCurCode = string.Empty, 
            mOnlineCtCode = string.Empty;
        internal FrmCtQt3Find FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmCtQt3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer Quotation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueContractType(ref LueContract);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLuePtCode(ref LuePtCode);
                SetLueSPCode(ref LueSPCode);
                SetLueTypeCode(ref LueType);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                tabControl1.SelectTab("TpgQuotationItem");
                LueUomCode.Visible = false;
                LueType.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 2

            Grd2.Cols.Count = 24;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "",
                        "Item's Code",
                        "Local Code",
                        "Item's Name",
                        "Price List"+Environment.NewLine+"Document#",
                        //6-10
                        "Price List"+Environment.NewLine+"Dno",
                        "UoM",
                        "Currency",
                        "Previous Price",
                        "Price",
                        //11-15
                        "Price After"+Environment.NewLine+"Discount",
                        "Discount"+Environment.NewLine+"(%)",
                        "Remark",
                        "Specification",
                        "Customer"+Environment.NewLine+"Item's Code",
                        //16-20
                        "Customer"+Environment.NewLine+"Item's Name",
                        "Asset"+Environment.NewLine+"Indicator",
                        "",
                        "Asset Code"+Environment.NewLine+"Refference",
                        "Asset"+Environment.NewLine+"Refference",
                        //21-23
                        "Display Name"+Environment.NewLine+"Refference",
                        "TypeCode",
                        "Type"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 100, 150, 250, 150,  
                        
                        //6-10
                        100, 50, 80, 100, 100, 
                        
                        //11-15
                        100, 80, 400, 120, 120,

                        //16-20
                        250, 100, 20, 150, 150,
                        //21-23
                        150, 80, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 19, 20, 21, 22 });
            Sm.GrdColCheck(Grd2, new int[] {17});
            Sm.GrdColButton(Grd2, new int[] {18});
            if (mCtQtBusinessProcess!="1")
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 11 });
            else
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 12 });
            Sm.GrdFormatDec(Grd2, new int[] { 9, 10, 11, 12 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0, 1 });
            Grd2.Cols[14].Move(5);
            Grd2.Cols[16].Move(6);
            Grd2.Cols[15].Move(6);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 5, 6, 14, 17, 22 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd2, new int[] { 16 });

            #endregion

        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 5, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtLocalDocNo, DteDocDt, LueCtCode, LueCtContactPersonName, LueSPCode, DteQtStartDt, DteQtEndDt, LuePtCode,
                        LueCurCode, TxtCreditLimit, TxtCtReplace, LueContract, MeeRemark
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = true;
                    BtnCtContactPersonName.Enabled = false;
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 13, 23 });
                    if (mCtQtBusinessProcess != "1")
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 12 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 11 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueCtCode, LueSPCode, DteQtStartDt, DteQtEndDt, LueContract, LuePtCode, 
                        LueCurCode, TxtCreditLimit, MeeRemark
                    }, false);
                    DteDocDt.Focus();
                    ChkActInd.Checked = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Checked = true;
                    BtnCtContactPersonName.Enabled = true;
                    BtnCtReplace.Enabled = true;
                    BtnCtReplace2.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 13, 23 });
                    if (mCtQtBusinessProcess != "1")
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 12 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 11 });
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, LueCtContactPersonName, LueSPCode, DteQtStartDt, DteQtEndDt, LuePtCode,
                LueCurCode, TxtCtReplace, LueContract, MeeRemark
            });
            ClearGrd();
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                 TxtCreditLimit, 
            }, 0);
            ChkPrintSignatureInd.Checked = false;
            ChkActInd.Checked = false;
        }

        private void ClearGrd()
        {
            ClearGrd2();
        }

        internal void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 9, 10, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCtQt3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteQtStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           // ParPrint();
        }

        #endregion

        #region Grid Method

        #region Grid 2

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmCtQt3Dlg2(
                    this,
                    Sm.GetDte(DteDocDt),
                    Sm.GetLue(LueCurCode),
                    Sm.GetLue(LueCtCode),
                    TxtCtReplace.Text
                    ));

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 18 && BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer") && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                Sm.FormShowDialog(new FrmCtQt3Dlg(this, e.RowIndex));            
        }

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmCtQt3Dlg2(
                            this,
                            Sm.GetDte(DteDocDt),
                            Sm.GetLue(LueCurCode),
                            Sm.GetLue(LueCtCode),
                            TxtCtReplace.Text
                            ));
                }

                if (e.ColIndex == 15)
                {
                    SetLueUomCode(ref LueUomCode, Sm.GetGrdStr(Grd2, e.RowIndex, 2));
                    LueRequestEdit(Grd2, LueUomCode, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 23)
                {
                    SetLueTypeCode(ref LueType);
                    LueRequestEdit(Grd2, LueType, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 18 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                {
                    Sm.FormShowDialog(new FrmCtQt3Dlg(this, e.RowIndex));
                }


                if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 11, 12 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 13 }, e);

            if (mCtQtBusinessProcess!="1")
            {
                if (Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
                    ComputePriceAfterDiscount(e.RowIndex);
            }
            else
            {
                if (Sm.IsGrdColSelected(new int[] { 11 }, e.ColIndex))
                    ComputeDiscount(e.RowIndex);
            }
        }

        private void Grd2_ColHdrClick(object sender, iGColHdrClickEventArgs e)
        {
            if (e.ColIndex == 12 && mCtQtBusinessProcess != "1")
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy discount based on first record ?") == DialogResult.Yes)
                {
                    if (Sm.GetGrdStr(Grd2, 0, 12).Length != 0)
                    {
                        var Discount = Sm.GetGrdDec(Grd2, 0, 12);
                        for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                            {
                                Grd2.Cells[Row, 12].Value = Discount;
                                ComputePriceAfterDiscount(Row);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CtQt2", "TblCtQt2Hdr");

            var cml = new List<MySqlCommand>();
            // var cml2 = new List<MySqlCommand>();

            cml.Add(SaveCtQtHdr(DocNo));
            cml.Add(SaveCtQtDtl(DocNo));

            //for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
            //    if (Sm.GetGrdStr(Grd2, Row2, 2).Length > 0) 
            //        cml.Add(SaveCtQtDtl(DocNo, Row2));
            
            if (TxtCtReplace.Text.Length != 0)
                EditCtQtCancel(TxtCtReplace.Text);

            Sm.ExecCommands(cml);

            //Sm.ExecCommands(cml2);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsDteEmpty(DteQtEndDt, "Quotation End Date") ||
                Sm.IsLueEmpty(LueContract, "Contract") ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyRequestNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Item is empty.")) return true;
                if (Sm.GetGrdDec(Grd2, Row, 12) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                        "Discount should not be less than 0.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }


        private bool IsQtyRequestNotValid()
        {
            for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 11) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                            "Price after discount must be greater than 0.");
                        return true;
                    }
                }
            }

            return false;
        }


        private MySqlCommand SaveCtQtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Customer Quotation (CtQt2Hdr) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblCtQt2Hdr(DocNo, DocDt, LocalDocNo, ActInd, Status, CtCode, CtContactPersonName, ContractType, QtStartDt, QtEndDt, ");
            SQL.AppendLine("SPCode, PtCode, CurCode, CreditLimit, PrintSignatureInd, CtQtDocNoReplace, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @ActInd, 'O', @CtCode, @CtContactPersonName, @ContractType, @QtStartDt, @QtEndDt, ");
            SQL.AppendLine("@SPCode, @PtCode, @CurCode, @CreditLimit, @PrintSignatureInd, @CtQtDocNoReplace, @Remark, @UserCode, @Dt); ");

            if (!Sm.CompareStr(Sm.GetLue(LueCtCode), mOnlineCtCode))
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='CtQt2'; ");
            }

            SQL.AppendLine("Update TblCtQt2Hdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CtQt2' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@ContractType", Sm.GetLue(LueContract));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParamDt(ref cm, "@QtEndDt", Sm.GetDte(DteQtEndDt));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", TxtCtReplace.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        private MySqlCommand SaveCtQtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Customer Quotation (CtQt2Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblCtQt2Dtl(DocNo, DNo, ItCode,  AssetInd, ItemPrice2DocNo, ItemPrice2DNo, PriceUomCode, UPricePrev, UPrice, UPriceAfDisc, Discount, AssetCode, QtType, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @AssetInd_" + r.ToString() +
                        ", @ItemPrice2DocNo_" + r.ToString() +
                        ", @ItemPrice2DNo_" + r.ToString() +
                        ", @PriceUomCode_" + r.ToString() +
                        ", @UPricePrev_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @UPriceAfDisc_" + r.ToString() +
                        ", @Discount_" + r.ToString() +
                        ", @AssetCode_" + r.ToString() +
                        ", @QtType_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 6));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@ItemPrice2DocNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                    Sm.CmParam<String>(ref cm, "@ItemPrice2DNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 6));
                    Sm.CmParam<String>(ref cm, "@PriceUomCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@UPricePrev_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceAfDisc_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Discount_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 12));
                    Sm.CmParam<String>(ref cm, "@AssetCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 19));
                    Sm.CmParam<String>(ref cm, "@QtType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 22));
                    Sm.CmParam<String>(ref cm, "@AssetInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 17)? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 13));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveCtQtDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblCtQt2Dtl(DocNo, DNo, ItCode,  AssetInd, ItemPrice2DocNo, ItemPrice2DNo, PriceUomCode, UPricePrev, UPrice, UPriceAfDisc, Discount, AssetCode, QtType, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @AssetInd, @ItemPrice2DocNo, @ItemPrice2DNo, @PriceUomCode, @UPricePrev, @UPrice, @UPriceAfDisc, @Discount, @AssetCode, @QtType, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
        //    Sm.CmParam<String>(ref cm, "@ItemPrice2DocNo", Sm.GetGrdStr(Grd2, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ItemPrice2DNo", Sm.GetGrdStr(Grd2, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd2, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@UPricePrev", Sm.GetGrdDec(Grd2, Row, 9)); 
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceAfDisc", Sm.GetGrdDec(Grd2, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd2, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd2, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@QtType", Sm.GetGrdStr(Grd2, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 13));
        //    Sm.CmParam<String>(ref cm, "@AssetInd", Sm.GetGrdStr(Grd2, Row, 17)=="True"?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsActIndDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditCtQtCancel(TxtDocNo.Text);
            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Customer Quotation", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblCtQt2Hdr Where DocNo=@DocNo And ActInd='N' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already non-active.");
                return true;
            }
            return false;
        }

        private void EditCtQtCancel(string DocNo)
        {
            if (Sm.GetLue(LueCtCode).Length>0)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Update TblCtQt2Hdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                        "Where DocNo=@DocNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);
            }
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowCtQtHdr(DocNo);
                ShowCtQtDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCtQtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.LocalDocNo, A.ActInd, A.CtCode, A.CtContactPersonName, "+
                    "A.QtStartDt, A.QtEndDt, A.ContractType, A.SPCode, A.PtCode, " +
                    "A.CurCode, A.CreditLimit, A.PrintSignatureInd, A.CtQtDocNoReplace, A.Remark "+
                    "From TblCtQt2Hdr A "+
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "LocalDocNo", "ActInd", "CtCode", "CtContactPersonName",
                        //6-10
                        "QtStartDt", "QtEndDt", "ContractType", "SPCode", "PtCode", 
                        //11-15
                        "CurCode", "CreditLimit", "PrintSignatureInd", "CtQtDocNoReplace", "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[4]));
                        SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[5]));
                        Sm.SetDte(DteQtStartDt, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteQtEndDt, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueContract, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[11]));
                        TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y");
                        TxtCtReplace.EditValue = Sm.DrStr(dr, c[14]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    }, true
                );
        }

        private void ShowCtQtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, if(B.AssetInd = 'Y', E.AssetCode, B.ItCode) As ItCode, ");
            SQL.AppendLine("if(B.AssetInd = 'Y', E.AssetName, C.ItCodeInternal) ItCodeInternal,  ");
            SQL.AppendLine("if(B.AssetInd = 'Y', ifNUll(E.DisplayName, E.AssetName), C.ItName) ItName, B.ItemPrice2DocNo, B.ItemPrice2DNo,  ");
            SQL.AppendLine("B.PriceUomCode, A.CurCode, B.UPricePrev, ");
            SQL.AppendLine("B.UPrice, B.UPriceAfDisc, B.Discount, B.Remark, C.Specification, D.CtItCode, D.CtItName, ");
            SQL.AppendLine("B.AssetCode, F.AssetName, F.DisplayName, B.QtType, if(B.QtType='F', 'Fix', 'Variable') As QtTypeName  ");
            SQL.AppendLine("From TblCtQt2Hdr A "); 
            SQL.AppendLine("Inner Join TblCtQt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("LEFT JOIN TblCustomerItem D ON B.ItCode=D.ItCode And D.CtCode=@CtCode ");
            SQL.AppendLine("Left Join TblAsset E On B.ItCode = E.AssetCode ");
            SQL.AppendLine("Left Join TblAsset F On B.AssetCode = F.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;  ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "ItemPrice2DocNo", "ItemPrice2DNo", "PriceUomCode",  
                    
                    //6-10
                    "CurCode", "UPricePrev", "UPrice", "UPriceAfDisc", "Discount",  
                    
                    //11-15
                    "Remark" , "Specification", "CtItCode", "CtItName", "AssetCode", 
                    //16-19
                    "AssetName", "DisplayName", "QtType", "QtTypeName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method


        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueTypeCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'F' As Col1, 'FIX' As Col2  " +
                "Union ALL Select 'V' As Col1, 'VARIABLE' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");

        }

        public static void SetLueContractType(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select 'One Time Charge' As Col1  "+
                "Union ALL "+
                "Select 'Recurring' As Col1 ;",
                "Name");
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'CtQtBusinessProcess', 'IsCtQtUseUPrice2', 'IsCustomerItemNameMandatory', 'MainCurCode', 'OnlineCtCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCtQtUseUPrice2": mIsCtQtUseUPrice2 = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;

                            //string
                            case "OnlineCtCode": mOnlineCtCode = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "CtQtBusinessProcess": mCtQtBusinessProcess = ParValue; break;
                            
                        }
                    }
                }
                dr.Close();
            }
        }

        internal void BtnInsertClick()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        internal void ComputeDiscount(int Row)
        {
            decimal UPrice = Sm.GetGrdDec(Grd2, Row, 10);
            if (UPrice == 0)
                Grd2.Cells[Row, 12].Value = 0m;
            else
                Grd2.Cells[Row, 12].Value = 100 - ((Sm.GetGrdDec(Grd2, Row, 11) * 100) / UPrice);
        }

        internal void ComputePriceAfterDiscount(int Row)
        {
            decimal UPrice = 0m, Discount = 0m;
            if (Sm.GetGrdStr(Grd2, Row, 10).Length > 0) UPrice = Sm.GetGrdDec(Grd2, Row, 10);
            if (Sm.GetGrdStr(Grd2, Row, 12).Length > 0) Discount = Sm.GetGrdDec(Grd2, Row, 12);
            Grd2.Cells[Row, 11].Value = (((100 - Discount) / 100) * UPrice);
        }

        private void SetLueUomCode(ref LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct UomCode As Col1, UomCode As Col2 From TblUom ");
            SQL.AppendLine("Where UomCode In ( ");
            SQL.AppendLine("   Select SalesUomCode From TblItem Where ItCode=@ItCode And SalesUomCode Is Not Null ");
            SQL.AppendLine("   Union All ");
            SQL.AppendLine("   Select SalesUomCode2 From TblItem Where ItCode=@ItCode And SalesUomCode2 Is Not Null ");
            SQL.AppendLine(") Order By UomName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(
                ref Lue, ref cm,
                35, 0, true, false, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetDefault()
        {
            if (mCtQtBusinessProcess != "1")
                SetDefault2();
            else
                SetDefault1();
        }

        private void SetDefault2()
        {
            ClearGrd2();

            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode, "Currency")
            ) return;

            var SQL = new StringBuilder();

            string CtReplace = TxtCtReplace.Text;

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, C.PriceUomCode, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, IfNull(C.UPrice, 0) As UPrice, C.DocDt, IfNull(D.UPrice, 0) As PrevUPrice,  ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As PriceAfterDisc, E.CtItCode, E.CtItName, C.AssetInd  ");
            SQL.AppendLine("From TblItem A   ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode  ");
            SQL.AppendLine("Inner Join (  ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C2.PriceUomCode, C1.DocDt, C2.ItCode, C2.UPrice, C2.AssetInd  ");
            SQL.AppendLine("    From TblItemPrice2Hdr C1  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl C2 On C1.DocNo=C2.DocNo  ");
            SQL.AppendLine("    Where C1.ActInd='Y'  ");
            SQL.AppendLine("    And C1.CurCode=@CurCode  ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode  ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select Distinct T4.ItCode, T1.CurCode, T2.PriceUomCode, T2.UPrice   ");
            SQL.AppendLine("    From TblCtQt2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo  ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode  ");
            SQL.AppendLine("    And T1.CurCode=@CurCode  ");
            SQL.AppendLine("    And T1.ActInd='Y'  ");
            SQL.AppendLine("    And T1.Status='A'  ");
            SQL.AppendLine(") D  ");
            SQL.AppendLine("    On A.ItCode=D.ItCode  ");
            SQL.AppendLine("    And C.CurCode=D.CurCode  ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode  ");
            SQL.AppendLine("Left Join TblCustomerItem E ON A.ItCode=E.ItCode And E.CtCode=@CtCode  ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y'  ");
            SQL.AppendLine("And A.ActInd = 'Y'  ");
            SQL.AppendLine("And A.ItCode In (  ");
            SQL.AppendLine("    Select X2.ItCode  ");
            SQL.AppendLine("    From TblItemPrice2Hdr X1  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl X2 On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("    Where X1.ActInd='Y'  ");
            SQL.AppendLine("    And X1.CurCode=@CurCode  ");
            SQL.AppendLine(")  ");
            SQL.AppendLine("And A.ItCode In (  ");
            SQL.AppendLine("    Select X3.ItCode  ");
            SQL.AppendLine("    From TblCtQt2Hdr X1  ");
            SQL.AppendLine("    Inner Join TblCtQt2Dtl X2 On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl X3 On X2.ItemPrice2DocNo=X3.DocNo And X2.ItemPrice2DNo=X3.DNo  ");
            SQL.AppendLine("    Where X1.ActInd='Y'  ");
            SQL.AppendLine("    And X1.Status='A'  ");
            SQL.AppendLine("    And X1.CurCode=@CurCode  ");
            SQL.AppendLine("    And X1.CtCode=@CtCode  ");
            SQL.AppendLine(")  ");
           
            SQL.AppendLine("UNION ALL  ");

            SQL.AppendLine("Select A1.AssetCode, A1.AssetName, ifnUll(A1.DisplayName, A1.AssetName),   ");
            SQL.AppendLine("B.ItCtName, C.PriceUomCode, C.DocNo, C.DNo, C.CurCode,  ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As UPrice, C.DocDt, IfNull(D.UPrice, 0) As PrevUPrice,  ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As PriceAfterDisc, '' CtItCode, '' CtItName, C.AssetInd  ");
            SQL.AppendLine("From TblAsset A1  ");
            SQL.AppendLine("Inner Join TblItem A On A1.ItCode = A.ItCode    ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode   ");
            SQL.AppendLine("Inner Join (  ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C2.PriceUomCode, C1.DocDt, C2.ItCode, C2.UPrice, C2.AssetInd  ");
	        SQL.AppendLine("    From TblItemPrice2Hdr C1  ");
	        SQL.AppendLine("    Inner Join TblItemPrice2Dtl C2 On C1.DocNo=C2.DocNo  ");
	        SQL.AppendLine("    Where C1.ActInd='Y' And C2.AssetInd = 'Y'  ");
	        SQL.AppendLine("    And C1.CurCode=@CurCode  ");
            SQL.AppendLine(") C On A1.AssetCode=C.ItCode  ");
            SQL.AppendLine("Left Join (  ");
	        SQL.AppendLine("    Select Distinct T4.ItCode, T1.CurCode, T2.PriceUomCode, T2.UPrice  ");
	        SQL.AppendLine("    From TblCtQt2Hdr T1  ");
	        SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo  ");
	        SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo  ");
	        SQL.AppendLine("    Where T1.CtCode=@CtCode  ");
	        SQL.AppendLine("    And T1.CurCode=@CurCode  ");
	        SQL.AppendLine("    And T1.ActInd='Y'  ");
	        SQL.AppendLine("    And T1.Status='A' And T2.AssetInd = 'Y'  ");
            SQL.AppendLine(") D  ");
            SQL.AppendLine("On A.ItCode=D.ItCode  ");
            SQL.AppendLine("And C.CurCode=D.CurCode  ");
            SQL.AppendLine("And C.PriceUomCode=D.PriceUomCode  ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", CtReplace);

            Sm.ShowDataInGrid(
               ref Grd2, ref cm,
               SQL.ToString(),
               new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "DocNo", "DNo", "CurCode",   
                    
                    //6-10
                    "PrevUPrice", "UPrice", "PriceAfterDisc", "PriceUomCode", "CtItCode",

                    //11
                    "CtItName"
                },
               (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
               {
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 9);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                   ComputeDiscount(Row);
               }, false, false, false, false
           );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12 });
        }

        private void SetDefault1()
        {
            ClearGrd2();

            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode, "Currency")
            ) return;

            var SQL = new StringBuilder();

            string CtReplace = TxtCtReplace.Text;

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, C.PriceUomCode, F.CtItCode, F.CtItName, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, C.UPrice, C.DocDt, IfNull(D.UPrice, 0) As PrevUPrice, ");
            if (CtReplace.Length > 0)
                SQL.AppendLine("IfNull(E.PriceAfterDisc, C.UPrice) As PriceAfterDisc, ");
            else
                SQL.AppendLine("IfNull(D.UPrice, C.UPrice) As PriceAfterDisc, ");
            SQL.AppendLine("C.AssetInd ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C2.PriceUomCode, C1.DocDt, C2.ItCode, C2.UPrice, C2.AssetInd ");
            SQL.AppendLine("    From TblItemPrice2Hdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Where C1.ActInd='Y' ");
            SQL.AppendLine("    And C1.CurCode=@CurCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T1.CurCode, T2.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select X4.ItCode, X3.CurCode, X4.PriceUomCode, Max(Concat(X1.DocDt, X1.DocNo)) As Key1 ");
            SQL.AppendLine("        From TblCtQt2Hdr X1 ");
            SQL.AppendLine("        Inner Join TblCtQt2Dtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPrice2Hdr X3 On X2.ItemPrice2DocNo=X3.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPrice2Dtl X4 On X2.ItemPrice2DocNo=X4.DocNo And X2.ItemPrice2DNo=X4.DNo ");
            SQL.AppendLine("        Where X1.CtCode=@CtCode ");
            SQL.AppendLine("        And X1.CurCode=@CurCode ");
            SQL.AppendLine("        Group By X4.ItCode, X3.CurCode, X4.PriceUomCode ");
            SQL.AppendLine("        ) T5 ");
            SQL.AppendLine("            On T4.ItCode=T5.ItCode ");
            SQL.AppendLine("            And T1.CurCode=T5.CurCode ");
            SQL.AppendLine("            And T4.PriceUomCode=T5.PriceUomCode ");
            SQL.AppendLine("            And Concat(T1.DocDt, T1.DocNo)=T5.Key1 ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.ItCode=D.ItCode ");
            SQL.AppendLine("    And C.CurCode=D.CurCode ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode ");

            if (CtReplace.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T4.ItCode, T2.UPrice As PriceAfterDisc ");
                SQL.AppendLine("    From TblCtQt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo ");
                SQL.AppendLine("    Where T1.DocNo=@CtQtDocNoReplace ");
                SQL.AppendLine(") E On A.ItCode=E.ItCode ");
            }
            SQL.AppendLine("Left Join TblCustomerItem F ON A.ItCode=F.ItCode And F.CtCode=@CtCode ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine("And A.ItCode In ( ");
            SQL.AppendLine("    Select Distinct T4.ItCode ");
            SQL.AppendLine("    From TblCtQt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode ");
            SQL.AppendLine("    And T1.CurCode=@CurCode ");

            if (CtReplace.Length == 0)
            {
                SQL.AppendLine("        And Concat(T1.DocDt, T1.DocNo) In ( ");
                SQL.AppendLine("            Select Max(Concat(DocDt, DocNo)) As Key1 ");
                SQL.AppendLine("            From TblCtQt2Hdr ");
                SQL.AppendLine("            Where CtCode=@CtCode ");
                SQL.AppendLine("            And CurCode=@CurCode ");
                SQL.AppendLine("        ) ");
            }
            else
            {
                SQL.AppendLine("        And T1.DocNo=@CtQtDocNoReplace ");
            }
            SQL.AppendLine("    ) ");
            
            SQL.AppendLine("UNION ALL  ");

            SQL.AppendLine("Select A1.AssetCode, A1.AssetName, ifnUll(A1.DisplayName, A1.AssetName),   ");
            SQL.AppendLine("B.ItCtName, C.PriceUomCode, '' CtItCode, '' CtItName, C.DocNo, C.DNo, C.CurCode,  ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As UPrice, C.DocDt, IfNull(D.UPrice, 0) As PrevUPrice,  ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As PriceAfterDisc, C.AssetInd   ");
            SQL.AppendLine("From TblAsset A1  ");
            SQL.AppendLine("Inner Join TblItem A On A1.ItCode = A.ItCode    ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode   ");
            SQL.AppendLine("Inner Join (  ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C2.PriceUomCode, C1.DocDt, C2.ItCode, C2.UPrice, C2.AssetInd  ");
            SQL.AppendLine("    From TblItemPrice2Hdr C1  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl C2 On C1.DocNo=C2.DocNo  ");
            SQL.AppendLine("    Where C1.ActInd='Y' And C2.AssetInd = 'Y'  ");
            SQL.AppendLine("    And C1.CurCode=@CurCode  ");
            SQL.AppendLine(") C On A1.AssetCode=C.ItCode  ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select Distinct T4.ItCode, T1.CurCode, T2.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQt2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblCtQt2Dtl T2 On T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Hdr T3 On T2.ItemPrice2DocNo=T3.DocNo  ");
            SQL.AppendLine("    Inner Join TblItemPrice2Dtl T4 On T2.ItemPrice2DocNo=T4.DocNo And T2.ItemPrice2DNo=T4.DNo  ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode  ");
            SQL.AppendLine("    And T1.CurCode=@CurCode  ");
            SQL.AppendLine("    And T1.ActInd='Y'  ");
            SQL.AppendLine("    And T1.Status='A' And T2.AssetInd = 'Y'  ");
            SQL.AppendLine(") D  ");
            SQL.AppendLine("On A.ItCode=D.ItCode  ");
            SQL.AppendLine("And C.CurCode=D.CurCode  ");
            SQL.AppendLine("And C.PriceUomCode=D.PriceUomCode;  ");


            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", CtReplace);

            Sm.ShowDataInGrid(
               ref Grd2, ref cm,
               SQL.ToString(),
               new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "DocNo", "DNo", "CurCode",   
                    
                    //6-10
                    "PrevUPrice", "UPrice", "PriceAfterDisc", "PriceUomCode", "CtItCode",

                    //11
                    "CtItName", "AssetInd"
                },
               (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
               {
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 9);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                   Sm.SetGrdValue("B", Grd, dr, c, Row, 17, 12);
                   ComputeDiscount(Row);
               }, false, false, false, false
           );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12 });
        }

        #region Print

        //private void ParPrint()
        //{
        //    if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
        //    string Doctitle = Sm.GetParameter("DocTitle");

        //    var l = new List<CtQtHdr>();
        //    var l2 = new List<CtQtHdr2>();
        //    var ldtl = new List<CtQtDtl>();

        //    string[] TableName = { "CtQtHdr", "CtQtHdr2", "CtQtDtl" };
        //    List<IList> myLists = new List<IList>();

        //    var cm = new MySqlCommand();

        //    #region Header
        //    var SQL = new StringBuilder();
        //    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
        //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
        //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
        //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
        //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail',");
        //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
        //    SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d-%M-%y')As DocDt, A.CtContactPersonName As CPName, B.CtName, B.Address, ");
        //    SQL.AppendLine("ifnull(C.SpName, A.SpCode) As SpName, D.PtName, E.DtName, DATE_FORMAT(A.QtStartDt,'%d-%M-%y')As QtStartDt, ");
        //    SQL.AppendLine("DATE_FORMAT(Date_Add(A.QtStartDt, interval A.QtMth month),'%d-%M-%y')As EndDt, F.UserName, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict ");
        //    SQL.AppendLine("From TblCtQt2Hdr A ");
        //    SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
        //    SQL.AppendLine("Left Join TblSalesPerson C On A.SpCode=C.SpCode ");
        //    SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
        //    SQL.AppendLine("Left Join TblDeliveryType E On A.ShpMCode=E.DtCode ");
        //    SQL.AppendLine("Inner Join Tbluser F On A.CreateBy=F.UserCode  ");
        //    SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //                {
        //                //0
        //                 "CompanyLogo",

        //                 //1-5
        //                 "CompanyName",
        //                 "CompanyAddress",
        //                 "CompanyAddressCity",
        //                 "CompanyPhone",
        //                 "CompanyEmail",

        //                 //6-10
        //                 "DocNo",
        //                 "DocDt",
        //                 "CPName",
        //                 "CtName",
        //                 "Address",
                         
        //                 //11-15
        //                 "SpName",
        //                 "PtName",
        //                 "DtName",
        //                 "QtStartDt",
        //                 "EndDt",
                         
        //                 //16-17
        //                 "UserName",
        //                 "EmpPict"

                         
        //                });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new CtQtHdr()
        //                {
        //                    CompanyLogo = Sm.DrStr(dr, c[0]),

        //                    CompanyName = Sm.DrStr(dr, c[1]),
        //                    CompanyAddress = Sm.DrStr(dr, c[2]),
        //                    CompanyAddressCity = Sm.DrStr(dr, c[3]),
        //                    CompanyPhone = Sm.DrStr(dr, c[4]),
        //                    CompanyEmail = Sm.DrStr(dr, c[5]),
        //                    DocNo = Sm.DrStr(dr, c[6]),
        //                    DocDt = Sm.DrStr(dr, c[7]),
        //                    CPName = Sm.DrStr(dr, c[8]),
        //                    CtName = Sm.DrStr(dr, c[9]),
        //                    Address = Sm.DrStr(dr, c[10]),
        //                    SpName = Sm.DrStr(dr, c[11]),
        //                    PtName = Sm.DrStr(dr, c[12]),
        //                    DtName = Sm.DrStr(dr, c[13]),
        //                    QtStartDt = Sm.DrStr(dr, c[14]),
        //                    EndDt = Sm.DrStr(dr, c[15]),
        //                    UserName = Sm.DrStr(dr, c[16]),
        //                    EmpPict = Sm.DrStr(dr, c[17]),
        //                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //    myLists.Add(l);
        //    #endregion

        //    #region Header2
        //    var cm1 = new MySqlCommand();
        //    var SQL1 = new StringBuilder();

        //    SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
        //    SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd ");
        //    SQL1.AppendLine("from TblDocApproval A ");
        //    SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
        //    SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
        //    SQL1.AppendLine("Where DocType = 'CtQt' ");
        //    SQL1.AppendLine("And DocNo =@DocNo ");
        //    SQL1.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

        //    using (var cn1 = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn1.Open();
        //        cm1.Connection = cn1;
        //        cm1.CommandText = SQL1.ToString();
        //        Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
        //        var dr1 = cm1.ExecuteReader();
        //        var c1 = Sm.GetOrdinal(dr1, new string[] 
        //                {
        //                 //0
        //                 "ApprovalDno",
        //                 //1-3
        //                 "UserCode",
        //                 "UserName",
        //                 "EmpPict",
        //                 "SignInd"

                        
        //                });
        //        if (dr1.HasRows)
        //        {
        //            while (dr1.Read())
        //            {
        //                l2.Add(new CtQtHdr2()
        //                {
        //                    ApprovalDno = Sm.DrStr(dr1, c1[0]),
        //                    UserCode = Sm.DrStr(dr1, c1[1]),
        //                    UserName = Sm.DrStr(dr1, c1[2]),
        //                    EmpPict = Sm.DrStr(dr1, c1[3]),
        //                    SignInd = Sm.DrStr(dr1, c1[4]),
        //                });
        //            }
        //        }

        //        dr1.Close();
        //    }
        //    myLists.Add(l2);
        //    #endregion

        //    #region Detail
        //    var cmDtl = new MySqlCommand();

        //    var SQLDtl = new StringBuilder();
        //    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cnDtl.Open();
        //        cmDtl.Connection = cnDtl;

        //        SQLDtl.AppendLine("Select A.DocNo, E.ItName, D.UPrice, B.Discount, (D.UPrice)-(D.UPrice)*((B.Discount)/100) As SubTotal, F.CtItName, ");
        //        SQLDtl.AppendLine("(Select ParValue From TblParameter Where ParCode = 'IsCustomerItemNameMandatory') As IsCustomerItemNameMandatory, G.CurName ");
        //        SQLDtl.AppendLine("From TblCtQt2Hdr A ");
        //        SQLDtl.AppendLine("Inner Join TblCtQt2Dtl B On A.DocNo=B.DocNo ");
        //        SQLDtl.AppendLine("Left Join TblItemPriceHdr C On B.ItemPriceDocNo=C.DocNo And C.CurCode = @CurCode ");
        //        SQLDtl.AppendLine("Left Join TblItemPriceDtl D On B.ItemPriceDocNo=D.DocNo And B.ItemPriceDNo=D.Dno ");
        //        SQLDtl.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
        //        SQLDtl.AppendLine("Left Join TblCustomerItem F On A.CtCode = F.CtCode And E.ItCode = F.ItCode ");
        //        SQLDtl.AppendLine("Left Join TblCurrency G On A.Curcode=G.Curcode ");
        //        SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By B.DNo ");

        //        cmDtl.CommandText = SQLDtl.ToString();

        //        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

        //        var drDtl = cmDtl.ExecuteReader();
        //        var cDtl = Sm.GetOrdinal(drDtl, new string[] 
        //            {
        //             //0
        //             "DocNo" ,

        //             //1-5
        //             "ItName" ,
        //             "UPrice",
        //             "Discount",
        //             "SubTotal",
        //             "CtItName",

        //             //6-7
        //             "IsCustomerItemNameMandatory",
        //             "CurName"
        //            });
        //        if (drDtl.HasRows)
        //        {
        //            while (drDtl.Read())
        //            {
        //                ldtl.Add(new CtQtDtl()
        //                {
        //                    DocNo = Sm.DrStr(drDtl, cDtl[0]),
        //                    ItName = Sm.DrStr(drDtl, cDtl[1]),
        //                    UPrice = Sm.DrDec(drDtl, cDtl[2]),
        //                    Discount = Sm.DrDec(drDtl, cDtl[3]),
        //                    SubTotal = Sm.DrDec(drDtl, cDtl[4]),
        //                    CtItName = Sm.DrStr(drDtl, cDtl[5]),
        //                    IsCustomerItemNameMandatory = Sm.DrStr(drDtl, cDtl[6]),
        //                    CurName = Sm.DrStr(drDtl, cDtl[7]),
        //                });
        //            }
        //        }
        //        drDtl.Close();
        //    }
        //    myLists.Add(ldtl);
        //    #endregion
        //    if (Doctitle == "MSI")
        //    {
        //        Sm.PrintReport("CtQtMSI", myLists, TableName, false);
        //    }
        //    else
        //    {
        //        Sm.PrintReport("CtQt", myLists, TableName, false);
        //    }

        //}

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LblSetDefault_Click(object sender, EventArgs e)
        {
            try
            {
                SetDefault();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtCtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCtContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueCtContactPersonName, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                    Sm.SetControlReadOnly(LueCtContactPersonName, false);
                }
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtCtReplace  
                });
                Sm.ClearGrd(Grd2, true);
            }
        }

        private void BtnCtContactPersonName_Click_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                }
            }
        }

        private void LueCtContactPersonName_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueCurCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                TxtCtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
            }
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtCreditLimit, 0);
        }

        private void LueUomCode_Leave(object sender, EventArgs e)
        {
            if (LueUomCode.Visible && fAccept && fCell.ColIndex == 15)
            {
                if (Sm.GetLue(LueUomCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueUomCode);
            }
        }

        private void LueUomCode_Validated(object sender, EventArgs e)
        {
            LueUomCode.Visible = false;
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 23)
            {
                if (Sm.GetLue(LueType).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 22].Value =
                    Grd2.Cells[fCell.RowIndex, 23].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 22].Value = Sm.GetLue(LueType);
                    Grd2.Cells[fCell.RowIndex, 23].Value = LueType.GetColumnValue("Col2");
                }
                LueType.Visible = false;
            }
        }

        private void LueType_Validated(object sender, EventArgs e)
        {
            LueType.Visible = false;
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 20)
            {
                if (Sm.GetGrdStr(Grd2, 0, 20).Length != 0)
                {
                    var AssetCode = Sm.GetGrdStr(Grd2, 0, 19);
                    var AssetName = Sm.GetGrdStr(Grd2, 0, 20);
                    var DisplayName = Sm.GetGrdStr(Grd2, 0, 21);

                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 19).Length != 0) Grd2.Cells[Row, 19].Value = AssetCode;
                        if (Sm.GetGrdStr(Grd2, Row, 20).Length != 0) Grd2.Cells[Row, 20].Value = AssetName;
                        if (Sm.GetGrdStr(Grd2, Row, 21).Length != 0) Grd2.Cells[Row, 21].Value = DisplayName;
                    }
                }
            }
        }

        #endregion

        #region Button Event

        private void BtnCtReplace_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer") &&
                !Sm.IsLueEmpty(LueCurCode, "Currency"))
                Sm.FormShowDialog(new FrmCtQt3Dlg3(this, Sm.GetLue(LueCurCode)));
        }

        private void BtnCtReplace2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtCtReplace.EditValue != null && TxtCtReplace.Text.Length != 0)
                        {

                            var f = new FrmCtQt3(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtCtReplace.Text;
                            f.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueContract_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueContract, new Sm.RefreshLue1(SetLueContractType));
        }

        #endregion

        #endregion

        #region Report Class

        class CtQtHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CPName { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SpName { get; set; }
            public string PtName { get; set; }
            public string QtStartDt { get; set; }
            public string DtName { get; set; }
            public string EndDt { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PrintBy { get; set; }
            
        }

        class CtQtHdr2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
        }

        class CtQtDtl
        {
            public string DocNo { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal SubTotal { get; set; }
            public string CtItName { get; set; }
            public string IsCustomerItemNameMandatory { get; set; }
            public string CurName { get; set; }
        }


        #endregion

        
    }
}