﻿#region Update
/*
    21/04/2022 [HAR/GSS] menu baru
    26/04/2022 [TRI/GSS] BUG generate Investment equitycode belum sesuai
    27/04/2022 [RDA/GSS] resize action bar
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemEquity : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmInvestmentItemEquityFind FrmFind;
        internal bool mIsInsert = false;
        internal string mInvestmentItemEquityCode = string.Empty;

        #endregion

        #region Constructor

        public FrmInvestmentItemEquity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Investment Item Equity";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);


                //if this application is called from other application
                //if (mInvestmentCode.Length != 0)
                //{
                //    ShowData(mInvestmentCode);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                //    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = mIsAccessUsed;
                //}
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtInvestmentEquityCode, TxtPortofolioId, TxtInvestmentName, TxtForeignName,
                        LueInvestmentCtCode, LueCurCode, TxtIssuer, TxtStockExchange, LueInvestmentUomCode,
                        TxtSpecification, MeeRemark, DteUpdateDt, DteStatementDt, TxtYearEnd, 
                        TxtIssuedShares, TxtMarketCap, TxtStockIndex, DteListingDt, TxtSector,
                        TxtSales, TxtEquity,TxtAsset, TxtLiability, TxtNetCashFlow, 
                        TxtOperatingProfit,  TxtNetProfit, TxtShareDividen, TxtShareEarning, TxtShareRevenue,
                        TxtShareBookValue, TxtShareCashFlow, TxtShareCashEqvl, TxtShareAssetValue,TxtDividenYield,
                        TxtPriceEarning, TxtPriceSales, TxtPriceBookvalue, TxtPriceCashFlow,
                        TxtMarginGross, TxtMarginOperating, TxtMarginNet, TxtMarginTax, TxtDividenPayout,
                        TxtRoe, TxtRoa, TxtDebtEquity, TxtCash, TxtQuick, TxtCurrent, ChkActInd
                    }, true);
                    BtnPortofio.Enabled = false;
                    TxtInvestmentEquityCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtForeignName,
                        LueInvestmentUomCode,
                        TxtSpecification, MeeRemark, DteStatementDt, TxtYearEnd,
                        TxtIssuedShares, TxtMarketCap, TxtStockIndex,
                        TxtSales, TxtEquity,TxtAsset, TxtLiability, TxtNetCashFlow,
                        TxtOperatingProfit,  TxtNetProfit, TxtShareDividen, TxtShareEarning, TxtShareRevenue,
                        TxtShareBookValue, TxtShareCashFlow, TxtShareCashEqvl, TxtShareAssetValue,TxtDividenYield,
                        TxtPriceEarning, TxtPriceSales, TxtPriceBookvalue, TxtPriceCashFlow,
                        TxtMarginGross, TxtMarginOperating, TxtMarginNet, TxtMarginTax, TxtDividenPayout,
                        TxtRoe, TxtRoa, TxtDebtEquity, TxtCash, TxtQuick, TxtCurrent,
                    }, false);
                    BtnPortofio.Focus();
                    BtnPortofio.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        ChkActInd,
                    }, false);
                    ChkActInd.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtInvestmentEquityCode, TxtPortofolioId, TxtInvestmentName, TxtForeignName,
                LueInvestmentCtCode, LueCurCode, TxtIssuer, TxtStockExchange, LueInvestmentUomCode,
                TxtSpecification, MeeRemark, DteUpdateDt, DteStatementDt, TxtYearEnd,
                TxtIssuedShares, TxtMarketCap, TxtStockIndex, DteListingDt, TxtSector,
                TxtSales, TxtEquity,TxtAsset, TxtLiability, TxtNetCashFlow,
                TxtOperatingProfit,  TxtNetProfit, 
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
               TxtShareDividen, TxtShareEarning, TxtShareRevenue,
                TxtShareBookValue, TxtShareCashFlow, TxtShareCashEqvl, TxtShareAssetValue,TxtDividenYield,
                TxtPriceEarning, TxtPriceSales, TxtPriceBookvalue, TxtPriceCashFlow,
                TxtMarginGross, TxtMarginOperating, TxtMarginNet, TxtMarginTax, TxtDividenPayout,
                TxtRoe, TxtRoa, TxtDebtEquity, TxtCash, TxtQuick, TxtCurrent
            }, 0);
            ChkActInd.Checked = false;

        }

        private void LueRequestEdit(
                    iGrid Grd,
                    DevExpress.XtraEditors.LookUpEdit Lue,
                    ref iGCell fCell,
                    ref bool fAccept,
                    TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentItemEquityFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }



        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                InsertDataClick();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInvestmentEquityCode, "", false)) return;
            mIsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string InvestmentEquityCode = string.Empty;

                if (mIsInsert)
                {
                    var InvestmentCtCode = Sm.GetLue(LueInvestmentCtCode);
                    var InvestmentCtCodeLength = InvestmentCtCode.Length + 1;
                    if (TxtInvestmentEquityCode.Text.Length == 0)
                    {
                        {
                            InvestmentEquityCode = Sm.GetValue(
                                 "Select Concat(@Param1,'-', " +
                                 "   IfNull((Select Right(Concat('00000', Convert(investmentequitycode+1, Char)), 5) From ( " +
                                 "       Select Convert(Right(investmentequitycode, 5), Decimal) As investmentequitycode " +
                                 "      From tblinvestmentitemequity " +
                                 "      Where Left(investmentequitycode, @Param2)=Concat(@Param1, '-') " +
                                 "      Order By Right(investmentequitycode, 5) Desc Limit 1 " +
                                 "       ) As TblItemTemp), '00001')) As investmentequitycode; ",
                                 InvestmentCtCode, (InvestmentCtCode.Length + 1).ToString(), string.Empty);
                        }
                    }
                    else
                        InvestmentEquityCode = TxtInvestmentEquityCode.Text;
                }
                else
                {
                    InvestmentEquityCode = TxtInvestmentEquityCode.Text;
                }
                var cml = new List<MySqlCommand>();

                cml.Add(SaveItem(InvestmentEquityCode));


                Sm.ExecCommands(cml);

                ShowData(InvestmentEquityCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string InvestmentEquityCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItem(InvestmentEquityCode);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowData2(string InvestmentPortofolioId)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowItem2(InvestmentPortofolioId);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItem(string InvestmentEquityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From TblInvestmentItemEquity T ");
            SQL.AppendLine("Inner Join tblinvestmentportofolio T2 On T.PortofolioId = T2.PortofolioId ");
            SQL.AppendLine("WHERE T.InvestmentEquityCode = @InvestmentEquityCode ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@InvestmentEquityCode", InvestmentEquityCode);
            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "InvestmentEquityCode", 
                        //1-5
                        "ActInd", "PortofolioId",  "PortofolioName", "ForeignName", "InvestmentCtCode",
                        //6-10
                        "CurCode", "Issuer", "StockExchange","UomCode", "Specification", 
                        //11-15
                        "StatementDt", "YearEnd","IssuedShares", "MarketCap", "StockIndex",
                        //16-20
                        "ListingDt", "Sector", "Sales", "Equity", "Asset", 
                        //21-25
                        "Liability", "NetCashFlow","OperatingProfit", "NetProfit", "ShareDividen",
                        //26-30
                        "ShareEarning", "ShareRevenue", "ShareBookValue","ShareCashFlow", "ShareCashEqvl",
                        //31-35
                        "ShareNetAsset", "DividenYield", "PriceEarning", "PriceSales","PriceBookValue",
                        //36-40
                        "PriceCashFlow", "MarginGross", "MarginOperating", "MarginNet", "MarginTax",
                        //41-45
                        "DividenPayout", "RoE", "RoA", "DebtEquity", "Cash", 
                        //46-46
                        "Quick", "Current" , "Remark", "UpdateDt"
        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtInvestmentEquityCode.Text = Sm.DrStr(dr, c[0]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                        TxtPortofolioId.Text = Sm.DrStr(dr, c[2]);
                        TxtInvestmentName.Text = Sm.DrStr(dr, c[3]);
                        TxtForeignName.Text = Sm.DrStr(dr, c[4]);
                        SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        TxtIssuer.Text = Sm.DrStr(dr, c[7]);
                        TxtStockExchange.Text = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueInvestmentUomCode, Sm.DrStr(dr, c[9]));
                        TxtSpecification.Text = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteStatementDt, Sm.DrStr(dr, c[11]));
                        TxtYearEnd.Text = Sm.DrStr(dr, c[12]);
                        TxtIssuedShares.Text = Sm.DrStr(dr, c[13]);
                        TxtMarketCap.Text = Sm.DrStr(dr, c[14]);
                        TxtStockIndex.Text = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteListingDt, Sm.DrStr(dr, c[16]));
                        TxtSector.Text = Sm.DrStr(dr, c[17]);
                        TxtSales.Text = Sm.DrStr(dr, c[18]);
                        TxtEquity.Text = Sm.DrStr(dr, c[19]);
                        TxtAsset.Text = Sm.DrStr(dr, c[20]);
                        TxtLiability.Text = Sm.DrStr(dr, c[21]);
                        TxtNetCashFlow.Text = Sm.DrStr(dr, c[22]);
                        TxtOperatingProfit.Text = Sm.DrStr(dr, c[23]);
                        TxtNetProfit.Text = Sm.DrStr(dr, c[24]);
                        TxtShareDividen.Text = Sm.DrStr(dr, c[25]);
                        TxtShareEarning.Text = Sm.DrStr(dr, c[26]);
                        TxtShareRevenue.Text = Sm.DrStr(dr, c[27]);
                        TxtShareBookValue.Text = Sm.DrStr(dr, c[28]);
                        TxtShareCashFlow.Text = Sm.DrStr(dr, c[29]);
                        TxtShareCashEqvl.Text = Sm.DrStr(dr, c[30]);
                        TxtShareAssetValue.Text = Sm.DrStr(dr, c[31]);
                        TxtDividenYield.Text = Sm.DrStr(dr, c[32]);
                        TxtPriceEarning.Text = Sm.DrStr(dr, c[33]);
                        TxtPriceSales.Text = Sm.DrStr(dr, c[34]);
                        TxtPriceBookvalue.Text = Sm.DrStr(dr, c[35]);
                        TxtPriceCashFlow.Text = Sm.DrStr(dr, c[36]);
                        TxtMarginGross.Text = Sm.DrStr(dr, c[37]);
                        TxtMarginOperating.Text = Sm.DrStr(dr, c[38]);
                        TxtMarginNet.Text = Sm.DrStr(dr, c[39]);
                        TxtMarginTax.Text = Sm.DrStr(dr, c[40]);
                        TxtDividenPayout.Text = Sm.DrStr(dr, c[41]);
                        TxtRoe.Text = Sm.DrStr(dr, c[42]);
                        TxtRoa.Text = Sm.DrStr(dr, c[43]);
                        TxtDebtEquity.Text = Sm.DrStr(dr, c[44]);
                        TxtCash.Text = Sm.DrStr(dr, c[45]);
                        TxtQuick.Text = Sm.DrStr(dr, c[46]);
                        TxtCurrent.Text = Sm.DrStr(dr, c[47]);
                        MeeRemark.Text = Sm.DrStr(dr, c[48]);
                        Sm.SetDte(DteUpdateDt, Sm.DrStr(dr, c[49]));
                    }, true
                );
        }
        private void ShowDataInCtrl(ref MySqlCommand cm, string SQL, string[] ColumnTitle, RefreshData rd, bool ShowNoDataInd)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd)
                    {
                        if (mInvestmentItemEquityCode.Length == 0)
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                        else
                            Sm.StdMsg(mMsgType.Warning, "No authorized to see this information.");
                    }
                }
                else
                {
                    while (dr.Read()) rd(dr, c);
                }
                dr.Close();
                dr.Dispose();
                cm.Dispose();
            }
        }

        public void ShowItem2(string portofolioid)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From tblinvestmentportofolio T ");
            SQL.AppendLine("WHERE T.PortofolioId = @portofolioid ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@portofolioid", portofolioid);
            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "portofolioName", 
                        //1-5
                        "Issuer", "StockExchange",  "UpdateDt", "ListingDt", "Sector",
                        //6-10
                        "CurCode", "InvestmentCtCode"
        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtInvestmentName.Text = Sm.DrStr(dr, c[0]);
                        TxtIssuer.Text = Sm.DrStr(dr, c[1]);
                        TxtStockExchange.Text = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteUpdateDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteListingDt, Sm.DrStr(dr, c[4]));
                        TxtSector.Text = Sm.DrStr(dr, c[5]);
                        Sl.SetLueCurCode(ref LueCurCode);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[7]));

                    }, true
                );
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtInvestmentName, "Item name", false) ||
                Sm.IsLueEmpty(LueInvestmentCtCode, "Investment's category") ||
                IsItCodeExisted();
        }
       
        private bool IsItCodeExisted()
        {
            if (!TxtInvestmentEquityCode.Properties.ReadOnly && Sm.IsDataExist("Select 1 From TblInvestmentItemEquity Where InvestmentEquityCode='" + TxtInvestmentEquityCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Item code ( " + TxtInvestmentEquityCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItem(string InvestmentCode)
        {
            var SQL = new StringBuilder();

            if (mIsInsert)
            {
                SQL.AppendLine("Insert Into TblInvestmentItemEquity( " );
                SQL.AppendLine("   InvestmentEquityCode, ActInd, PortofolioId, ForeignName, UomCode, Specification, StatementDt, YearEnd, IssuedShares, ");
                SQL.AppendLine("    MarketCap, StockIndex, Sales, Equity, Asset, Liability, NetCashFlow, ");
                SQL.AppendLine("    OperatingProfit, NetProfit, ShareDividen, ShareEarning, ShareRevenue, ShareBookValue, ");
                SQL.AppendLine("    ShareCashFlow, ShareCashEqvl, ShareNetAsset, DividenYield, PriceEarning, PriceSales, ");
                SQL.AppendLine("    PriceBookValue, PriceCashFlow, MarginGross, MarginOperating, MarginNet, MarginTax, ");
                SQL.AppendLine("    DividenPayout, RoE, RoA, DebtEquity, Cash, Quick, Current, ");
                SQL.AppendLine("    Remark, CreateBy, CreateDt)");
                SQL.AppendLine("Values ");
                SQL.AppendLine("   (@InvestmentEquityCode, @ActInd, @PortofolioId, @ForeignName, @UomCode, @Specification, @StatementDt, @YearEnd, @IssuedShares, ");
                SQL.AppendLine("    @MarketCap, @StockIndex, @Sales, @Equity, @Asset, @Liability, @NetCashFlow, ");
                SQL.AppendLine("    @OperatingProfit, @NetProfit, @ShareDividen, @ShareEarning, @ShareRevenue, @ShareBookValue, ");
                SQL.AppendLine("    @ShareCashFlow, @ShareCashEqvl, @ShareNetAsset, @DividenYield, @PriceEarning, @PriceSales, ");
                SQL.AppendLine("    @PriceBookValue, @PriceCashFlow, @MarginGross, @MarginOperating, @MarginNet, @MarginTax, ");
                SQL.AppendLine("    @DividenPayout, @RoE, @RoA, @DebtEquity, @Cash, @Quick, @Current, ");
                SQL.AppendLine("    @remark, @UserCode, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("Update TblInvestmentItemEquity Set ");
                SQL.AppendLine("    ActInd=@ActInd, PortofolioId=@PortofolioId, ");
                SQL.AppendLine("    ForeignName=@ForeignName, UomCode=@UomCode, Specification=@Specification, StatementDt=@StatementDt, YearEnd=@YearEnd, IssuedShares=@IssuedShares, ");
                SQL.AppendLine("    MarketCap=@MarketCap, StockIndex=@StockIndex, Sales=@Sales, Equity=@Equity, Asset=@Asset, Liability=@Liability, NetCashFlow=@NetCashFlow, ");
                SQL.AppendLine("    OperatingProfit=@OperatingProfit, NetProfit=@NetProfit, ShareDividen=@ShareDividen, ShareEarning=@ShareEarning, ShareRevenue=@ShareRevenue, ShareBookValue=@ShareBookValue, ");
                SQL.AppendLine("    ShareCashFlow=@ShareCashFlow, ShareCashEqvl=@ShareCashEqvl, ShareNetAsset=@ShareNetAsset, DividenYield=@DividenYield, PriceEarning=@PriceEarning, PriceSales=@PriceSales, ");
                SQL.AppendLine("    PriceBookValue=@PriceBookValue, PriceCashFlow=@PriceCashFlow, MarginGross=@MarginGross, MarginOperating=@MarginOperating, MarginNet=@MarginNet, MarginTax=@MarginTax, ");
                SQL.AppendLine("    DividenPayout=@DividenPayout, RoE=@RoE, RoA=@RoA, DebtEquity=@DebtEquity, Cash=@Cash, Quick=@Quick, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where InvestmentEquityCode=@InvestmentEquityCode; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@InvestmentEquityCode", InvestmentCode);
            Sm.CmParam<String>(ref cm, "@InvestmentName", TxtInvestmentName.Text);
            Sm.CmParam<String>(ref cm, "@ForeignName", TxtForeignName.Text);
            Sm.CmParam<String>(ref cm, "@PortofolioId", TxtPortofolioId.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueInvestmentUomCode));
            Sm.CmParam<String>(ref cm, "@Specification", TxtSpecification.Text);

            Sm.CmParamDt(ref cm, "@StatementDt", Sm.GetDte(DteStatementDt));
            Sm.CmParam<String>(ref cm, "@YearEnd", TxtYearEnd.Text);
            Sm.CmParam<String>(ref cm, "@IssuedShares", TxtIssuedShares.Text);
            Sm.CmParam<String>(ref cm, "@MarketCap", TxtMarketCap.Text);
            Sm.CmParam<String>(ref cm, "@StockIndex", TxtStockIndex.Text);
            Sm.CmParam<String>(ref cm, "@Sales", TxtSales.Text);
            Sm.CmParam<String>(ref cm, "@Equity", TxtEquity.Text);
            Sm.CmParam<String>(ref cm, "@Asset", TxtAsset.Text);
            Sm.CmParam<String>(ref cm, "@Liability", TxtLiability.Text);
            Sm.CmParam<String>(ref cm, "@NetCashFlow", TxtNetCashFlow.Text);
            Sm.CmParam<String>(ref cm, "@OperatingProfit", TxtOperatingProfit.Text);
            Sm.CmParam<String>(ref cm, "@NetProfit", TxtNetProfit.Text);
            Sm.CmParam<Decimal>(ref cm, "@ShareDividen", Decimal.Parse(TxtShareDividen.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareEarning", Decimal.Parse(TxtShareEarning.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareRevenue", Decimal.Parse(TxtShareRevenue.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareBookValue", Decimal.Parse(TxtShareBookValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareCashFlow", Decimal.Parse(TxtShareCashFlow.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareCashEqvl", Decimal.Parse(TxtShareCashEqvl.Text));
            Sm.CmParam<Decimal>(ref cm, "@ShareNetAsset", Decimal.Parse(TxtShareAssetValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@DividenYield", Decimal.Parse(TxtDividenYield.Text));
            Sm.CmParam<Decimal>(ref cm, "@PriceEarning", Decimal.Parse(TxtPriceEarning.Text));
            Sm.CmParam<Decimal>(ref cm, "@PriceSales", Decimal.Parse(TxtPriceSales.Text));
            Sm.CmParam<Decimal>(ref cm, "@PriceBookValue", Decimal.Parse(TxtPriceBookvalue.Text));
            Sm.CmParam<Decimal>(ref cm, "@PriceCashFlow", Decimal.Parse(TxtPriceCashFlow.Text));
            Sm.CmParam<Decimal>(ref cm, "@MarginGross", Decimal.Parse(TxtMarginGross.Text));
            Sm.CmParam<Decimal>(ref cm, "@MarginOperating", Decimal.Parse(TxtMarginOperating.Text));
            Sm.CmParam<Decimal>(ref cm, "@MarginNet", Decimal.Parse(TxtMarginNet.Text));
            Sm.CmParam<Decimal>(ref cm, "@MarginTax", Decimal.Parse(TxtMarginTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@DividenPayout", Decimal.Parse(TxtDividenPayout.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoE", Decimal.Parse(TxtRoe.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoA", Decimal.Parse(TxtRoa.Text));
            Sm.CmParam<Decimal>(ref cm, "@DebtEquity", Decimal.Parse(TxtDebtEquity.Text));
            Sm.CmParam<Decimal>(ref cm, "@Cash", Decimal.Parse(TxtCash.Text));
            Sm.CmParam<Decimal>(ref cm, "@Quick", Decimal.Parse(TxtQuick.Text));
            Sm.CmParam<Decimal>(ref cm, "@Current", Decimal.Parse(TxtCurrent.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        internal protected void InsertDataClick()
        {
            mIsInsert = true;
            ClearData();
            SetFormControl(mState.Insert);

            SetLueInvestmentCtCode(ref LueInvestmentCtCode, string.Empty);
            Sl.SetLueUomCode(ref LueInvestmentUomCode);
            ChkActInd.Checked = true;
        }

       
        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }


        #region Setlue




        #endregion

        #endregion

        #endregion

        #region event 
        private void BtnPortofio_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInvestmentItemEquityDlg(this));
        }

        private void TxtShareDividen_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareDividen, 0);
        }

        private void TxtShareEarning_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareEarning, 0);
        }

        private void TxtShareRevenue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareRevenue, 0);
        }

        private void TxtShareBookValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareBookValue, 0);
        }

        private void TxtShareCashFlow_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareCashFlow, 0);
        }

        private void TxtShareCashEqvl_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareCashEqvl, 0);
        }

        private void TxtShareAssetValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtShareAssetValue, 0);
        }

        private void TxtDividenYield_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDividenYield, 0);
        }

        private void TxtPriceEarning_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPriceEarning, 0);
        }

        private void TxtPriceSales_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPriceSales, 0);
        }

        private void TxtPriceBookvalue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPriceBookvalue, 0);
        }

        private void TxtPriceCashFlow_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPriceCashFlow, 0);
        }

        private void TxtMarginGross_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMarginGross, 0);
        }

        private void TxtMarginOperating_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMarginOperating, 0);
        }

        private void TxtMarginNet_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMarginNet, 0);
        }

        private void TxtMarginTax_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMarginTax, 0);
        }

        private void TxtDividenPayout_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDividenPayout, 0);
        }

        private void TxtRoe_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRoe, 0);
        }

        private void TxtRoa_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRoa, 0);
        }

        private void TxtDebtEquity_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDebtEquity, 0);
        }

        private void TxtCash_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCash, 0);
        }

        private void TxtQuick_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQuick, 0);
        }

        private void TxtCurrent_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCurrent, 0);
        }

        #endregion
    }
}
