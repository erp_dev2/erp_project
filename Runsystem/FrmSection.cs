﻿#region Update 

//16/11/2021 [RIS/ALL] Membuat non-active bisa active

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSection : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSectionFind FrmFind;

        #endregion

        #region Constructor

        public FrmSection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSectionCode, TxtSectionName, ChkActInd
                    }, true);
                    TxtSectionCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtSectionCode, TxtSectionName
                    }, false);
                    TxtSectionCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSectionName, ChkActInd,
                    }, false);
                    TxtSectionName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSectionCode, TxtSectionName, ChkActInd
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
          if (FrmFind == null) FrmFind = new FrmSectionFind(this);
          Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSectionCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSectionCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSection Where SectionCode=@SectionCode" };
                Sm.CmParam<String>(ref cm, "@SectionCode", TxtSectionCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();


                SQL.AppendLine("Insert Into TblSection(SectionCode, SectionName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SectionCode, @SectionName, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ActInd=@ActInd, SectionName=@SectionName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");


                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@SectionCode", TxtSectionCode.Text);
                Sm.CmParam<String>(ref cm, "@SectionName", TxtSectionName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtSectionCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SecCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SectionCode", SecCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select SectionCode, SectionName, ActInd From TblSection Where SectionCode=@SectionCode;",
                        new string[] 
                        {
                            "SectionCode", "SectionName", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtSectionCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSectionName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSectionCode, "Section Code", false) ||
                Sm.IsTxtEmpty(TxtSectionName, "Section Name", false) ||
                IsSectionExisted();
            //IsDataInactiveAlready();
           
        }

        private bool IsSectionExisted()
        {
            if (!TxtSectionCode.Properties.ReadOnly && Sm.IsDataExist("Select SectionCode From TblSection Where SectionCode='" + TxtSectionCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Section code ( " + TxtSectionCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select SectionCode From TblSection " +
                    "Where ActInd='N' And SectionCode=@SectionCode;"
            };
            Sm.CmParam<String>(ref cm, "@SectionCode", TxtSectionCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This section already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void TxtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSectionCode);
        }
        #endregion
    }
}
