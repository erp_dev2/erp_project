﻿#region Update
/*
    23/10/2020 [TKG/SIER] tambah employee
    09/04/2021 [TKG/PHT] tambah PwdLastUpDt
    19/04/2022 [WED/GSS] tambah langsung encrypt password ketika membuat user baru berdasarkan parameter IsPasswordForLoginNeedToEncode
    19/01/2023 [TYO/MNET] tambah field position berdasarkan parameter IsUserUseRemark
    27/01/2023 [TYO/PHT] tambah else di show data param mIsUserUseRemark
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUser : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsPasswordForLoginNeedToEncode = false,
            mIsUserUseRemark =false;
        internal FrmUserFind FrmFind;

        #endregion

        #region Constructor

        public FrmUser(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                SetLueGrpCode(ref LueGrpCode);

                if (!mIsUserUseRemark)
                    LblRemark.Visible = MeeRemark.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsPasswordForLoginNeedToEncode = Sm.GetParameterBoo("IsPasswordForLoginNeedToEncode");
            mIsUserUseRemark = Sm.GetParameterBoo("IsUserUseRemark");
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, TxtUserName, LueGrpCode, DteExpDt, ChkNotifyInd, MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    TxtUserCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, TxtUserName, LueGrpCode, DteExpDt, ChkNotifyInd, MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    TxtUserCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtUserCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserName, LueGrpCode, DteExpDt, ChkNotifyInd, MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    TxtUserName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtUserCode, TxtUserName, LueGrpCode, TxtEmpCode, TxtEmpName, 
                DteExpDt, DtePwdLastUpDt, MeeRemark
            });
            ChkNotifyInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmUserFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUserCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUserCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblUser Where UserCode=@UserCode" };
                Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                string PwdEncode = (mIsPasswordForLoginNeedToEncode) ? Sm.GetHashBase64(Sm.GetHashSha256(TxtUserCode.Text)) : TxtUserCode.Text;

                var SQL = new StringBuilder();

                SQL.AppendLine("Set @Dt:=CurrentDateTime();  ");

                SQL.AppendLine("Insert Into TblUser(UserCode, UserName, GrpCode, ExpDt, Pwd, NotifyInd, PwdLastUpDt, CreateBy, CreateDt, Remark) ");
                SQL.AppendLine("Values(@UserCode, @UserName, @GrpCode, @ExpDt, @Pwd, @NotifyInd, Left(@Dt, 8), @UserCode2, @Dt, ");
                if (mIsUserUseRemark)
                    SQL.AppendLine(" @Remark) ");
                else
                    SQL.AppendLine(" NULL) ");
                SQL.AppendLine("On Duplicate Key Update ");
                SQL.AppendLine("    UserName=@UserName, GrpCode=@GrpCode, ExpDt=@ExpDt, LastUpBy=@UserCode2, NotifyInd=@NotifyInd, LastUpDt=@Dt  ");
                if (mIsUserUseRemark)
                    SQL.AppendLine(", Remark = @Remark; ");
                else
                    SQL.AppendLine("; ");

                SQL.AppendLine("Update TblEmployee Set UserCode=Null Where UserCode Is Not Null And UserCode=@UserCode; ");
                if (TxtEmpCode.Text.Length>0)
                    SQL.AppendLine("Update TblEmployee Set UserCode=@UserCode Where EmpCode=@EmpCode; ");

                var cml = new List<MySqlCommand>();
                var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
                Sm.CmParam<String>(ref cm, "@UserName", TxtUserName.Text);
                Sm.CmParam<String>(ref cm, "@GrpCode", Sm.GetLue(LueGrpCode));
                Sm.CmParamDt(ref cm, "@ExpDt", Sm.GetDte(DteExpDt));
                Sm.CmParam<String>(ref cm, "@Pwd", PwdEncode);
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParam<String>(ref cm, "@NotifyInd", ChkNotifyInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);

                cml.Add(cm);
                Sm.ExecCommands(cml);

                ShowData(TxtUserCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string UserCode)
        {
            try
            {
                var SQL = new StringBuilder();
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                SQL.AppendLine("Select A.UserCode, A.UserName, A.GrpCode, A.ExpDt, A.NotifyInd, B.EmpCode, B.EmpName, A.PwdLastUpDt, ");
                if (mIsUserUseRemark)
                    SQL.AppendLine("A.Remark ");
                else
                    SQL.AppendLine("Null As Remark ");
                SQL.AppendLine("From TblUser A ");
                SQL.AppendLine("Left Join TblEmployee B On A.UserCode=B.UserCode ");
                SQL.AppendLine("Where A.UserCode=@UserCode; ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(), new string[] 
                    {
                        "UserCode", 
                        "UserName", "GrpCode", "ExpDt", "NotifyInd", "EmpCode",
                        "EmpName", "PwdLastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtUserCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtUserName.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetLue(LueGrpCode, Sm.DrStr(dr, c[2]));
                        Sm.SetDte(DteExpDt, Sm.DrStr(dr, c[3]));
                        ChkNotifyInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DtePwdLastUpDt, Sm.DrStr(dr, c[7]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUserCode, "User code", false) ||
                Sm.IsTxtEmpty(TxtUserName, "User name", false) ||
                Sm.IsLueEmpty(LueGrpCode, "Group") ||
                IsUserCodeExisted();
        }

        private bool IsUserCodeExisted()
        {
            if (!TxtUserCode.Properties.ReadOnly && Sm.IsDataExist("Select UserCode From TblUser Where UserCode='" + TxtUserCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "User code ( " + TxtUserCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        internal void SetLueGrpCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select GrpCode As Col1, GrpName As Col2 " +
                "From TblGroup " +
                "Where GrpCode<>Case When Exists(Select UserCode From TblUser Where GrpCode='SysAdm' And UserCode='"+Gv.CurrentUserCode+"') Then '' Else 'SysAdm' End " +
                "Order By GrpName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtUserCode);
        }

        private void TxtUserName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtUserName);
        }

        private void LueGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrpCode, new Sm.RefreshLue1(SetLueGrpCode));
        }

        private void TxtEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtEmpCode.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Remove employee's code ?") == DialogResult.Yes)
                {
                    TxtEmpCode.EditValue = null;
                    TxtEmpName.EditValue = null;
                }
            }
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmUserDlg(this));
        }

        #endregion

        #endregion
    }
}
