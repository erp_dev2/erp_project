﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhsFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvWhs mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmRecvWhsFind(FrmRecvWhs FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                SetGrd();
                SetSQL();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, C.WhsName, D.WhsName As WhsName2, ");
            SQL.AppendLine("B.DOWhsDocNo, F.ItCode, G.ItCodeInternal, G.ItName, G.ForeignName, ");
            SQL.AppendLine("IfNull(B.BatchNo, F.BatchNo) As BatchNo, ");
            SQL.AppendLine("IfNull(B.Source, F.Source) As Source, ");
            SQL.AppendLine("IfNull(B.Lot, F.BatchNo) As Lot, ");
            SQL.AppendLine("IfNull(B.Bin, F.BatchNo) As Bin, ");
            SQL.AppendLine("F.Qty As DOQty, B.Qty, F.Qty2 As DOQty2, B.Qty2, F.Qty3 As DOQty3, B.Qty3, ");
            SQL.AppendLine("G.InventoryUomCode, G.InventoryUomCode2, G.InventoryUomCode3,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, G.ItGrpCode ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode2=D.WhsCode ");
            SQL.AppendLine("Inner Join TblDOWhsHdr E On B.DOWhsDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl F On B.DOWhsDocNo=F.DocNo And B.DOWhsDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "From",
                        "To",

                        //6-10
                        "DO#",
                        "",
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        
                        //11-15
                        "Item's Name",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        
                        //16-20
                        "DO"+Environment.NewLine+"Quantity", 
                        "Received"+Environment.NewLine+"Quantity",
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",    
                        "Received"+Environment.NewLine+"Quantity",
                        
                        //21-25
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",    
                        "Received"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Created"+Environment.NewLine+"By",
                        
                        //26-30
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //31-32
                        "Group",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 50, 150, 150, 
                        
                        //6-10
                        130, 20, 100, 20, 100, 
                        
                        //11-15
                        250, 200, 170, 60, 60, 

                        //16-20
                        80, 80, 80, 80, 80,  

                        //21-25
                        80, 80, 80, 80, 100, 

                        //26-30
                        80, 80, 100, 80, 80,

                        //31-32
                        100, 230
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 7, 9 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 17, 19, 20, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 26, 29 });
            Sm.GrdFormatTime(Grd1, new int[] { 27, 30 });
            if (mFrmParent.mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 13, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }, false);
                Grd1.Cols[32].Move(12);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 13, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 }, false);
            
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 });
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[31].Visible = true;
                Grd1.Cols[31].Move(11);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 13, 25, 26, 27, 28, 29, 30 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDOWhsDocNo.Text, "B.DOWhsDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode2), "A.WhsCode2", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "F.ItCode", "G.ItCodeInternal", "G.ItName", "G.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "F.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "F.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "F.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "WhsName2", "WhsName", "DOWhsDocNo", 
                            
                            //6-10
                           "ItCode", "ItCodeInternal", "ItName", "BatchNo", "Source",   
                            
                            //11-15
                            "Lot", "Bin", "DOQty", "Qty", "InventoryUomCode", 
                            
                            //16-20
                            "DOQty2", "Qty2", "InventoryUomCode2", "DOQty3", "Qty3", 
                            
                            //21-25
                            "InventoryUomCode3", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //26-27
                            "ItGrpCode", "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 30, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 27);
                        }, true, false, false, false
                    );
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 16, 17, 19, 20, 22, 23 });
                    Grd1.Rows.ExpandAll();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmDOWhs(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmDOWhs(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse (From)");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse (To)");
        }

        private void TxtDOWhsDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOWhsDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
