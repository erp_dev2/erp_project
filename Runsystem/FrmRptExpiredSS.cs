﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptExpiredSS : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptExpiredSS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetLueResignOrEndDt();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                var Dt = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));
                Sl.SetLueSSPCode(ref LueSSPCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueResignOrEndDt()
        {
            Sl.SetLookUpEdit(LueResignOrEndDt, new string[] { null, "Resign Date", "End Date" });
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 2].Value = "'" + Sm.GetGrdStr(Grd1, Row, 2);
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
                Grd1.Cells[Row, 9].Value = "'" + Sm.GetGrdStr(Grd1, Row, 9);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 2].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 2).Length - 1);
                Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
                Grd1.Cells[Row, 9].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 9), Sm.GetGrdStr(Grd1, Row, 9).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private string GetSQL(string ResignOrEndDt, string Filter)
        {
            bool IsResignDt = Sm.CompareStr(ResignOrEndDt, "Resign Date");

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RecType, T.EmpCode, T.EmpCodeOld, T.CardNo, T.EmpName, T.Status, T.DeptName, ");
            SQL.AppendLine("T.JoinDt, T.ResignDt, T.BirthDt, T.IDNo, T.Age, T.SSName, T.StartDt, T.EndDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As RecType, A.EmpCode, C.EmpCodeOld, A.CardNo, C.EmpName, 'Peserta' As Status, D.DeptName, ");
            SQL.AppendLine("    C.JoinDt, C.ResignDt, C.BirthDt, C.IDNumber As IDNo, ");
            SQL.AppendLine("    Case When C.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(C.BirthDt, '%Y%m%d'), STR_TO_DATE(E.CurrentDt, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    B.SSName, A.StartDt, A.EndDt ");
            SQL.AppendLine("    From TblEmployeeSS A ");
            SQL.AppendLine("    Inner Join TblSS B On A.SSCode=B.SSCode " + Filter);
            SQL.AppendLine("    Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            if (IsResignDt)
            {
                SQL.AppendLine("    And C.ResignDt Is Not Null And Left(C.ResignDt, 6)=Concat(@Yr, @Mth) ");
            }
            SQL.AppendLine("    Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
            SQL.AppendLine("    Inner Join (Select Left(CurrentDateTime(), 8) As CurrentDt) E On 0=0 ");
            if (!IsResignDt)
            {
                SQL.AppendLine("    Where A.EndDt Is Not Null And Left(A.EndDt, 6)=Concat(@Yr, @Mth) ");
            }
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As RecType, A.EmpCode, Null As EmpCodeOld, A.CardNo, A.FamilyName As EmpName, C.OptDesc As Status, E.DeptName As DeptName, ");
            SQL.AppendLine("    D.JoinDt, D.ResignDt, A.BirthDt, A.IDNo, ");
            SQL.AppendLine("    Case When A.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(A.BirthDt, '%Y%m%d'), STR_TO_DATE(F.CurrentDt, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    B.SSName, A.StartDt, A.EndDt ");
            SQL.AppendLine("    From TblEmployeeFamilySS A ");
            SQL.AppendLine("    Inner Join TblSS B On A.SSCode=B.SSCode " + Filter);
            SQL.AppendLine("    Left Join TblOption C On C.OptCat='FamilyStatus' And A.Status=C.OptCode ");
            SQL.AppendLine("    Inner Join TblEmployee D On A.EmpCode=D.EmpCode ");
            if (IsResignDt)
            {
                SQL.AppendLine("    And D.ResignDt Is Not Null And Left(D.ResignDt, 6)=Concat(@Yr, @Mth) ");
            }
            SQL.AppendLine("    Inner Join TblDepartment E On D.DeptCode=E.DeptCode ");
            SQL.AppendLine("    Inner Join (Select Left(CurrentDateTime(), 8) As CurrentDt) F On 0=0 ");
            if (!IsResignDt)
            {
                SQL.AppendLine("    Where A.EndDt Is Not Null And Left(A.EndDt, 6)=Concat(@Yr, @Mth) ");
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Order By T.DeptName, T.EmpName, T.EmpCode, T.RecType;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "Old"+Environment.NewLine+"Code",
                        "Card#",
                        "Participants Name",
                        "Department",

                        //6-10
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        "Birth Date",
                        "Identity#",
                        "Age",

                        //11-13
                        "Social Security Name",
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 80, 150, 200, 150, 
                        
                        //6-10
                        100, 100, 100, 150, 80,  
                        
                        //11-13
                        200, 100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7, 8, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsLueEmpty(LueResignOrEndDt, "Showing data condition") ||
                    Sm.IsLueEmpty(LueYr, "Year") ||
                    Sm.IsLueEmpty(LueMth, "Month")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                string EmpCode = string.Empty, SSCode = string.Empty, Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSSPCode), "B.SSPCode", true);
                
                Sm.ClearGrd(Grd1, false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Sm.GetLue(LueResignOrEndDt), Filter),
                        new string[]
                        {
                            //0
                            "EmpCode",  

                            //1-5
                            "EmpCodeOld", "CardNo", "EmpName", "DeptName", "JoinDt",   
                            
                            //6-10
                            "ResignDt", "BirthDt", "IDNo", "Age", "SSName", 
                            
                            //11-12
                            "StartDt", "EndDt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                        }, true, false, false, false
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSSPCode, new Sm.RefreshLue1(Sl.SetLueSSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSSPCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Social security program");
        }

        #endregion

        #endregion

        #endregion
    }
}
