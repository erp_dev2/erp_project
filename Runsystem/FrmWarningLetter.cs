﻿#region Update
/* 
    21/08/2017 [TKG] Tambah sequence.
    03/10/2017 [TKG] Tambah percentage deduction.
    11/10/2017 [TKG] CLEAR percentage deduction.
    06/11/2020 [TKG] tambah point, affected allowance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWarningLetter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmWarningLetterFind FrmFind;

        #endregion

        #region Constructor

        public FrmWarningLetter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueWLType, "WLType");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWlCode, TxtWlName, TxtNoOfDays, LueWLType, TxtSeqNo, 
                        TxtPDeduction, TxtPoint, LueADCode
                    }, true);
                    TxtWlCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWlCode, TxtWlName, TxtNoOfDays, LueWLType, TxtSeqNo, 
                        TxtPDeduction, TxtPoint, LueADCode
                    }, false);
                    TxtWlCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWlName, TxtNoOfDays, TxtSeqNo, TxtPDeduction, TxtPoint, 
                        LueADCode
                    }, false);
                    TxtWlName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWlCode, TxtWlName, LueWLType, TxtSeqNo, LueADCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtNoOfDays, TxtPDeduction, TxtPoint }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWarningLetterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueADCode(ref LueADCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWlCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWlCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWarningLetter Where WlCode=@WlCode" };
                Sm.CmParam<String>(ref cm, "@WlCode", TxtWlCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWarningLetter(WlCode, WlName, NoOfDays, WLType, SeqNo, PDeduction, Point, ADCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WlCode, @WlName, @NoOfDays, @WLType, @SeqNo, @PDeduction, @Point, @ADCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update WlName=@WlName, NoOfDays=@NoOfDays, WLType=@WLType, SeqNo=@SeqNo, PDeduction=@PDeduction, Point=@Point, ADCode=@ADCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@WlCode", TxtWlCode.Text);
                Sm.CmParam<String>(ref cm, "@WlName", TxtWlName.Text);
                Sm.CmParam<Decimal>(ref cm, "@NoOfDays", decimal.Parse(TxtNoOfDays.Text));
                Sm.CmParam<String>(ref cm, "@WLType", Sm.GetLue(LueWLType));
                Sm.CmParam<String>(ref cm, "@SeqNo", TxtSeqNo.Text);
                Sm.CmParam<Decimal>(ref cm, "@PDeduction", decimal.Parse(TxtPDeduction.Text));
                Sm.CmParam<Decimal>(ref cm, "@Point", decimal.Parse(TxtPoint.Text));
                Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetLue(LueADCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtWlCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string WlCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@WlCode", WlCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select WlCode, WlName, NoOfDays, WLType, SeqNo, PDeduction, Point, ADCode From TblWarningLetter Where WlCode=@WlCode;",
                        new string[] 
                        {
                            "WlCode", 
                            "WlName", "NoOfDays", "WLType", "SeqNo", "PDeduction" ,
                            "Point", "ADCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWlCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWlName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtNoOfDays.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            Sm.SetLue(LueWLType, Sm.DrStr(dr, c[3]));
                            TxtSeqNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtPDeduction.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            TxtPoint.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                            SetLueADCode(ref LueADCode, Sm.DrStr(dr, c[7]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWlCode, "Warning letter code", false) ||
                Sm.IsTxtEmpty(TxtWlName, "Warning letter name", false) ||
                Sm.IsTxtEmpty(TxtNoOfDays, "Number of days", true) ||
                Sm.IsLueEmpty(LueWLType, "Warning letter type") ||
                Sm.IsTxtEmpty(TxtSeqNo, "Sequence", false) ||
                IsWlCodeExisted();
        }

        private bool IsWlCodeExisted()
        {
            if (TxtWlCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select 1 From TblWarningLetter Where WlCode=@Param Limit 1;",
                TxtWlCode.Text,
                "Warning letter code ( " + TxtWlCode.Text + " ) already existed.");
        }

        #endregion

        #region Additonal Method

        private void SetLueADCode(ref LookUpEdit Lue, string Code)
        {
            Sm.SetLue2(
                ref Lue, "Select ADCode As Col1, ADName As Col2 From TblAllowanceDeduction Order By ADName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ADCode As Col1, ADName As Col2 From TblAllowanceDeduction ");
            if (Code.Length > 0)
                SQL.AppendLine("Where ADCode=@Code;");
            else
                SQL.AppendLine("Where ADType='A' Order By ADName;");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWlCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWlCode);
        }

        private void TxtWlName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWlName);
        }

        private void LueWLType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWLType, new Sm.RefreshLue2(Sl.SetLueOption), "WLType");
        }

        private void TxtNoOfDays_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtNoOfDays, 0);
                TxtNoOfDays.EditValue = Math.Truncate(decimal.Parse(TxtNoOfDays.Text));
                Sm.FormatNumTxt(TxtNoOfDays, 0);
            }
        }

        private void TxtSeqNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSeqNo);
        }

        private void TxtPDeduction_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPDeduction, 0);
        }

        private void LueADCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueADCode, new Sm.RefreshLue2(SetLueADCode), string.Empty);
        }

        private void TxtPoint_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPoint, 0);
        }

        #endregion

        #endregion
    }
}
