﻿#region Update
/*
    16/02/2022 [TKG/GSS] Merubah GetParameter() dan proses save
    14/03/2022 [VIN/IOK] Bug Save detail typo @@
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalaryAdjustment2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = string.Empty;
        internal bool mIsNotFilterByAuthorization = false;
        internal bool mIsFilterBySite = false;
        internal FrmSalaryAdjustment2Find FrmFind;

        #endregion

        #region Constructor

        public FrmSalaryAdjustment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Salary Adjustment";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Department", 
                        //6-7
                        "Position",
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        40,
                        //1-5
                        20, 80, 200, 120, 200, 
                        //6-10
                        150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] {2, 4} );

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, ChkCancelInd, TxtAmt, LueDeptCode, DtePaidDt, MeeDesc, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueDeptCode, TxtAmt, DtePaidDt, MeeDesc, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    ChkCancelInd.Checked = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, DtePaidDt,  MeeDesc, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalaryAdjustment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmSalaryAdjustment2Dlg(this, Sm.GetLue(LueDeptCode)));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmSalaryAdjustment2Dlg(this, Sm.GetLue(LueDeptCode)));
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalaryAdjustment2", "TblSalaryAdjustment2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalaryAdjustment2Hdr(DocNo));
            cml.Add(SaveSalaryAdjustment2Dtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row,2).Length > 0) 
            //        cml.Add(SaveSalaryAdjustment2Dtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeDesc, "Description") || 
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsDteEmpty(DtePaidDt, "Payment date") ||
                IsGrdEmpty() ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in the list.");
                return true;
            }
            return false;
        }

 
        private MySqlCommand SaveSalaryAdjustment2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Salary Adjustment 2 (Hdr) */ ");
            SQL.AppendLine("Insert Into TblSalaryAdjustment2Hdr(DocNo, DocDt, CancelInd, DeptCode, Amt, PaidDt, Description,  Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @DeptCode, @Amt, @PaidDt, @Description, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParamDt(ref cm, "@PaidDt", Sm.GetDte(DtePaidDt));
            Sm.CmParam<String>(ref cm, "@Description", MeeDesc.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalaryAdjustment2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Salary Adjustment 2 (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalaryAdjustment2Dtl(DocNo, DNo, EmpCode, PayrunCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @EmpCode_" + r.ToString() +
                        ", Null, @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveSalaryAdjustment2Dtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSalaryAdjustment2Dtl ");
        //    SQL.AppendLine("(DocNo, DNo, EmpCode, PayrunCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, null, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            try
            {
                if (IsEditedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblSalaryAdjustment2Hdr Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var cm = new MySqlCommand();
            cm.CommandText = "Select DocNo From TblSalaryAdjustment2Hdr Where DocNo=@DocNo And CancelInd='Y';";
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalaryAdjustment2Hdr(DocNo);
                ShowSalaryAdjustment2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalaryAdjustment2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.DeptCode, ");
            SQL.AppendLine("A.Amt, A.PaidDt, A.Description, A.Remark ");
            SQL.AppendLine("From TblSalaryAdjustment2Hdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "DeptCode", "Amt", "PaidDt",  
                        
                        //6-7
                        "Description", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sm.SetLue(LueDeptCode,  Sm.DrStr(dr, c[3]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        Sm.SetDte(DtePaidDt, Sm.DrStr(dr, c[5]));
                        MeeDesc.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowSalaryAdjustment2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.Empname, B.EmpCodeOld, D.Deptname, C.PosName, A.PayrunCode ");
            SQL.AppendLine("From TblSalaryAdjustment2Dtl A "); 
            SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode = D.DeptCode ");
            SQL.AppendLine("Where DocNo=@DocNo ; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "DNo", "EmpCode", "Empname", "EmpCodeOld", "Deptname", "PosName", "PayrunCode" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #endregion

        #region Additional Method

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2)+
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            //mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsPayrollDataFilterByAuthorization', 'IsFilterBySite' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsPayrollDataFilterByAuthorization": mIsNotFilterByAuthorization = ParValue == "N"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string FormatNum(string Value)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }            
                return String.Format((Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}", NumValue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        #endregion

        #region Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            TxtAmt.EditValue = FormatNum(TxtAmt.Text);
        }

        #endregion
    }
}
