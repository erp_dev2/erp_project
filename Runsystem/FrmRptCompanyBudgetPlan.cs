﻿#region Update
/*
    03/02/2021 [WED/PHT] new apps
    17/03/2021 [DITA/PHT] tambah kolom budget type
    06/02/2021 [HAR/PHT] mempercepat proses PrepBudgetType  
    07/06/2021 [TKG/PHT] Mengurutkan yang menggunakan multi profit center berdasarkan parentnya
    24/06/2021 [TKG/PHT] cost center yang muncul di multi cost center dibuat hanya child saja
    28/07/2021 [DITA/PHT] Semua transaksi yang membentuk journal apabila COA yang di journal terdaftar di budget category maka akan muncul realisasi nya dari nilai journal terhadap nilai masing masing budget category nya
    21/10/2021 [TKG/PHT] merubah filter. 
    21/10/2021 [RDH/PHT] budget transfer cost center berpengaruh(menambah amt remaining) ke reporting berdasarkan year, month from ,month to di budget transfer cost center, 
    28/10/2021 [DITA/PHT] ubah query untuk PrepData bispro budget transfer nya
    02/12/2021 [TRI/PHT] bug budget type tidak muncul saat diklik lop detail COA
    14/12/2021 [DITA/PHT] ubah query saat prep data -> budget transfer mempengaruhi cbp (nge get semua amount transfer&receive tiap bulan) + perubahan saat process all data -> calculate remain
    14/12/2021 [ICA/PHT] [BUG] ubah CPB -> CBPTo pada ProsessSummary
    14/12/2021 [ICA/PHT] Mengupdate Amount dan Qty Budget Transfer
    14/01/2022 [TRI/PHT] Pilihan Cost center yang ditampilkan dari child profit center yang dipilih juga
    20/01/2022 [TRI/PHT] BUG Pilihan Cost center belum sesuai dengan parent profit center
    03/02/2022 [IBL/PHT] Menambah field cost center parent untuk memfilter field Cost Center. Berdasarkan parameter IsRptCBPPLUseCostCenterParentFilter
    09/02/2022 [WED/PHT] tambah validasi parameter IsCASUseItemRateNotBasedOnCBP untuk hitung rate CAS nya
                         BUG di prep journal untuk Misc item, param di query menu nya belom pakai "Frm"
    14/02/2022 [WED/PHT] BUG perhitungan realisasi. Sumbernya menjadi dari CAS (item+COA+CCCode sama dengan CBP); Journal selain CAS dan Voucher CAS (Misc); CAS (COA+CCCode sama, item beda) --> Misc'
    21/02/2022 [IBL/PHT] Feedback filter Ccb Cost Center belum menarik semua child ketika memilih Cost Center Parent.
    15/03/2022 [DITA/PHT] saat perhitungan realisasi jika ada data recv yg sudah di jurnalkan dan item nya sama kaya cbp maka akan tercatat sebagai item asli, tetapi jia item nya ga ada di cbp maka akan masuk item Misc
    18/03/2022 [DITA/PHT] bug saat load cost center setelah filter profit center lama bgt, kena param IsRptCBPPLUseCostCenterParentFilter
    30/03/2022 [DITA/PHT] untuk perhitungan item misc receiving, qty tidak lagi di hardcode 1 tapi ambil qty nya si receiving
    04/04/2022 [DITA/PHT] penyesuaian rumus untuk quartal kolom Up To 
    06/04/2022 [DITA/PHT] penyesuaian jurnal receiving yg saat ini diubah save ke recvdtl
    18/04/2022 [BRI/PHT] penyesuaian export to excel
    25/04/2022 [WED/PHT] percepat loading show data
    17/05/2022 [DITA/PHT] filter cost center parent diubah jadi filter directorate group
    20/05/2022 [DITA/PHT] kolom account pas sortir malah muncul angka, karena method GrdAfterContentSorted  di base6 ga virtual
    23/05/2022 [ICA/PHT] Menampilkan CBPPL yg belum complate berdasarkan parameter IsRptCBPPLShowNotCompletedCBPPL
    27/05/2022 [DITA/PHT] bug :saat param IsRptCBPPLShowNotCompletedCBPPL aktif,semua data yg sudah compleetd ga muncul/no data
    09/06/2022 [BRI/PHT] mematikan function groupbox
    16/06/2022 [DITA/PHT] bug: directorate group yg ga ada cost center nya malah nampilin semua cost center di filter
    17/06/2022 [DITA/PHT] amount dari journal ga masuk ke itemmisc + up to thiss month bulan januari masih ada bug untuk kasus jurnal GetJournalToCBP
    20/03/2023 [RDA/PHT] menghilangkan opsi dropdown cost center "Consolidate" after klik button lup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyBudgetPlan : RunSystem.FrmBase6
    {
        #region Field

        internal List<CBP> ml = new List<CBP>();
        private List<String> mlCostCenter = null;
        private string
            mMenuCode = string.Empty,
            mSQL = string.Empty,
            mDocType = string.Empty // 2 : PL; 3 : BS
            ;
        internal string
            mAccessInd = string.Empty,
            mDirectorateGroupCode = string.Empty,
            mNotParentInd = string.Empty;

        private bool 
            mIsAllCostCenterSelected = false,
            mIsRptCBPPLShowNotCompletedCBPPL = false;

        internal bool mQuarterMenu = false;
        private bool mIsRptCBPPLUseDirectorateGroupFilter = false, mIsCASUseItemRateNotBasedOnCBP = false;

        private List<int> mDecimalColsList = new List<int>();
        internal int[] mDecimalCols = { };

        private List<int> mPartOfQ1ColsList = new List<int>();
        internal int[] mPartOfQ1Cols = { };

        private List<int> mPartOfQ2ColsList = new List<int>();
        internal int[] mPartOfQ2Cols = { };

        private List<int> mPartOfQ3ColsList = new List<int>();
        internal int[] mPartOfQ3Cols = { };

        private List<int> mPartOfQ4ColsList = new List<int>();
        internal int[] mPartOfQ4Cols = { };

        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                GetColCounts(7, 222, (11 + 3), (49 + 3), (63 + 3), (101 + 3), (115 + 3), (153 + 3), (167 + 3), (205 + 3));
                SetGrd();
                Grd1.GroupBox.Visible = false;
                if (mIsRptCBPPLUseDirectorateGroupFilter)
                {
                    mlCostCenter = new List<String>();
                    panel5.Visible = true;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            bool mIsPL = Sm.GetParameter("MenuCodeForReportingCBPPLMonthly") == mMenuCode || Sm.GetParameter("MenuCodeForReportingCBPPLQuarterly") == mMenuCode;
            if (mIsPL) mDocType = "2";
            else mDocType = "3";

            mQuarterMenu = Sm.GetParameter("MenuCodeForReportingCBPPLQuarterly") == mMenuCode || Sm.GetParameter("MenuCodeForReportingCBPBSQuarterly") == mMenuCode;
            mIsRptCBPPLUseDirectorateGroupFilter = Sm.GetParameterBoo("IsRptCBPPLUseDirectorateGroupFilter");
            mIsCASUseItemRateNotBasedOnCBP = Sm.GetParameterBoo("IsCASUseItemRateNotBasedOnCBP");
            mIsRptCBPPLShowNotCompletedCBPPL = Sm.GetParameterBoo("IsRptCBPPLShowNotCompletedCBPPL");
        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 4;
            Grd1.Cols.Count = mDecimalCols[mDecimalCols.Count() - 1] + 1;
            Grd1.FrozenArea.ColCount = 2;
            for (int i = 0; i < Grd1.Cols.Count; i++)
                if (i != 2)
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { i });

            #region general
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account Name";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 4;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Account#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 4;

            Sm.GrdColButton(Grd1, new int[] { 2 });
            Grd1.Cols[2].Width = 20;
            //Grd1.Header.Cells[0, 2].Value = "Directorate" + Environment.NewLine + "(Department's Group)";
            //Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            //Grd1.Header.Cells[0, 2].SpanRows = 4;

            Grd1.Cols[3].Width = 0;
            Grd1.Header.Cells[0, 3].Value = "Item's Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 4;

            Grd1.Cols[4].Width = 0;
            Grd1.Header.Cells[0, 4].Value = "Item's Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 4;

            Grd1.Cols[5].Width = 0;
            Grd1.Header.Cells[0, 5].Value = "UoM";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 4;

            Grd1.Cols[6].Width = 0;
            Grd1.Header.Cells[0, 6].Value = "Budget Type";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 4;
            Grd1.Cols[6].Move(2);

            #endregion

            #region year

            Grd1.Header.Cells[0, 4 + 3].Value = "Average" + Environment.NewLine + "Rate";
            Grd1.Header.Cells[0, 4 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 5 + 3].Value = "1 Year";
            Grd1.Header.Cells[3, 5 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 5 + 3].SpanCols = 6;
            Grd1.Header.Cells[1, 5 + 3].Value = "RKAP";
            Grd1.Header.Cells[1, 5 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 5 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 5 + 3].SpanRows = 2;
            Grd1.Header.Cells[1, 7 + 3].Value = "Realization";
            Grd1.Header.Cells[1, 7 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 7 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 7 + 3].SpanRows = 2;
            Grd1.Header.Cells[1, 9 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[1, 9 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 9 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 9 + 3].SpanRows = 2;
            Grd1.Header.Cells[0, 5 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 6 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 7 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 8 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 9 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 10 + 3].Value = "Amount";

            Grd1.Cols[4 + 3].Visible = false;
            for (int c = 0; c < 3; c++)
                Grd1.Cols[5 + 3 + (2 * c)].Visible = false;

            #endregion

            #region januari

            Grd1.Header.Cells[0, 11 + 3].Value = "Rate" + Environment.NewLine + "Jan";
            Grd1.Header.Cells[0, 11 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 12 + 3].Value = "Jan";
            Grd1.Header.Cells[3, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 12 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 12 + 3].Value = "RO";
            Grd1.Header.Cells[2, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 12 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 16 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 16 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 16 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 20 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 20 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 20 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 12 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 12 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 12 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 14 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 14 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 14 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 16 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 16 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 16 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 18 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 18 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 18 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 20 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 20 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 20 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 22 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 22 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 22 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 12 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 13 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 14 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 15 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 16 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 17 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 18 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 19 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 20 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 21 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 22 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 23 + 3].Value = "Amount";

            #endregion

            #region februari

            Grd1.Header.Cells[0, 24 + 3].Value = "Rate" + Environment.NewLine + "Feb";
            Grd1.Header.Cells[0, 24 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 24 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 25 + 3].Value = "Feb";
            Grd1.Header.Cells[3, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 25 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 25 + 3].Value = "RO";
            Grd1.Header.Cells[2, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 25 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 29 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 29 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 29 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 33 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 33 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 33 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 25 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 25 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 25 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 27 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 27 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 27 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 29 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 29 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 29 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 31 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 31 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 31 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 33 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 33 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 33 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 35 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 35 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 35 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 25 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 26 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 27 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 28 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 29 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 30 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 31 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 32 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 33 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 34 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 35 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 36 + 3].Value = "Amount";

            #endregion

            #region maret

            Grd1.Header.Cells[0, 37 + 3].Value = "Rate" + Environment.NewLine + "Mar";
            Grd1.Header.Cells[0, 37 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 37 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 38 + 3].Value = "Mar";
            Grd1.Header.Cells[3, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 38 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 38 + 3].Value = "RO";
            Grd1.Header.Cells[2, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 38 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 42 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 42 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 42 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 46 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 46 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 46 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 38 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 38 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 38 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 40 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 40 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 40 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 42 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 42 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 42 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 44 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 44 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 44 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 46 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 46 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 46 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 48 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 48 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 48 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 38 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 39 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 40 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 41 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 42 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 43 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 44 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 45 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 46 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 47 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 48 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 49 + 3].Value = "Amount";

            #endregion

            #region q1

            Grd1.Header.Cells[0, 50 + 3].Value = "Rate" + Environment.NewLine + "Q1";
            Grd1.Header.Cells[0, 50 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 50 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 51 + 3].Value = "Q1";
            Grd1.Header.Cells[3, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 51 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 51 + 3].Value = "RO";
            Grd1.Header.Cells[2, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 51 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 55 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 55 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 55 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 59 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 59 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 59 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 51 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 51 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 51 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 53 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 53 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 53 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 55 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 55 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 55 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 57 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 57 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 57 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 59 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 59 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 59 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 61 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 61 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 61 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 51 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 52 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 53 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 54 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 55 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 56 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 57 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 58 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 59 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 60 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 61 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 62 + 3].Value = "Amount";

            #endregion

            #region april

            Grd1.Header.Cells[0, 63 + 3].Value = "Rate" + Environment.NewLine + "Apr";
            Grd1.Header.Cells[0, 63 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 63 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 64 + 3].Value = "Apr";
            Grd1.Header.Cells[3, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 64 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 64 + 3].Value = "RO";
            Grd1.Header.Cells[2, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 64 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 68 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 68 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 68 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 72 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 72 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 72 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 64 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 64 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 64 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 66 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 66 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 66 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 68 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 68 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 68 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 70 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 70 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 70 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 72 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 72 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 72 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 74 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 74 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 74 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 64 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 65 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 66 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 67 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 68 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 69 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 70 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 71 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 72 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 73 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 74 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 75 + 3].Value = "Amount";

            #endregion

            #region mei

            Grd1.Header.Cells[0, 76 + 3].Value = "Rate" + Environment.NewLine + "May";
            Grd1.Header.Cells[0, 76 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 76 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 77 + 3].Value = "May";
            Grd1.Header.Cells[3, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 77 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 77 + 3].Value = "RO";
            Grd1.Header.Cells[2, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 77 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 81 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 81 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 81 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 85 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 85 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 85 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 77 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 77 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 77 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 79 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 79 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 79 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 81 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 81 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 81 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 83 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 83 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 83 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 85 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 85 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 85 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 87 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 87 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 87 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 77 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 78 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 79 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 80 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 81 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 82 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 83 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 84 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 85 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 86 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 87 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 88 + 3].Value = "Amount";

            #endregion

            #region juni

            Grd1.Header.Cells[0, 89 + 3].Value = "Rate" + Environment.NewLine + "Jun";
            Grd1.Header.Cells[0, 89 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 89 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 90 + 3].Value = "Jun";
            Grd1.Header.Cells[3, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 90 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 90 + 3].Value = "RO";
            Grd1.Header.Cells[2, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 90 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 94 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 94 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 94 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 98 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 98 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 98 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 90 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 90 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 90 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 92 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 92 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 92 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 94 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 94 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 94 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 96 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 96 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 96 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 98 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 98 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 98 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 100 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 100 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 100 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 90 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 91 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 92 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 93 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 94 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 95 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 96 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 97 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 98 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 99 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 100 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 101 + 3].Value = "Amount";

            #endregion

            #region q2

            Grd1.Header.Cells[0, 102 + 3].Value = "Rate" + Environment.NewLine + "Q2";
            Grd1.Header.Cells[0, 102 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 102 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 103 + 3].Value = "Q2";
            Grd1.Header.Cells[3, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 103 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 103 + 3].Value = "RO";
            Grd1.Header.Cells[2, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 103 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 107 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 107 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 107 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 111 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 111 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 111 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 103 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 103 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 103 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 105 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 105 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 105 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 107 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 107 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 107 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 109 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 109 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 109 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 111 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 111 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 111 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 113 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 113 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 113 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 103 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 104 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 105 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 106 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 107 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 108 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 109 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 110 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 111 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 112 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 113 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 114 + 3].Value = "Amount";

            #endregion

            #region juli

            Grd1.Header.Cells[0, 115 + 3].Value = "Rate" + Environment.NewLine + "Jul";
            Grd1.Header.Cells[0, 115 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 115 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 116 + 3].Value = "Jul";
            Grd1.Header.Cells[3, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 116 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 116 + 3].Value = "RO";
            Grd1.Header.Cells[2, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 116 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 120 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 120 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 120 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 124 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 124 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 124 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 116 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 116 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 116 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 118 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 118 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 118 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 120 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 120 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 120 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 122 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 122 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 122 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 124 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 124 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 124 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 126 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 126 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 126 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 116 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 117 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 118 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 119 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 120 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 121 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 122 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 123 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 124 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 125 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 126 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 127 + 3].Value = "Amount";

            #endregion

            #region agustus

            Grd1.Header.Cells[0, 128 + 3].Value = "Rate" + Environment.NewLine + "Ags";
            Grd1.Header.Cells[0, 128 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 128 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 129 + 3].Value = "Ags";
            Grd1.Header.Cells[3, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 129 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 129 + 3].Value = "RO";
            Grd1.Header.Cells[2, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 129 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 133 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 133 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 133 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 137 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 137 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 137 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 129 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 129 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 129 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 131 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 131 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 131 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 133 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 133 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 133 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 135 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 135 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 135 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 137 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 137 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 137 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 139 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 139 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 139 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 129 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 130 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 131 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 132 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 133 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 134 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 135 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 136 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 137 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 138 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 139 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 140 + 3].Value = "Amount";

            #endregion

            #region september

            Grd1.Header.Cells[0, 141 + 3].Value = "Rate" + Environment.NewLine + "Sep";
            Grd1.Header.Cells[0, 141 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 141 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 142 + 3].Value = "Sep";
            Grd1.Header.Cells[3, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 142 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 142 + 3].Value = "RO";
            Grd1.Header.Cells[2, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 142 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 146 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 146 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 146 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 150 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 150 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 150 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 142 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 142 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 142 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 144 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 144 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 144 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 146 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 146 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 146 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 148 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 148 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 148 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 150 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 150 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 150 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 152 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 152 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 152 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 142 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 143 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 144 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 145 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 146 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 147 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 148 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 149 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 150 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 151 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 152 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 153 + 3].Value = "Amount";

            #endregion

            #region q3

            Grd1.Header.Cells[0, 154 + 3].Value = "Rate" + Environment.NewLine + "Q3";
            Grd1.Header.Cells[0, 154 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 154 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 155 + 3].Value = "Q3";
            Grd1.Header.Cells[3, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 155 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 155 + 3].Value = "RO";
            Grd1.Header.Cells[2, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 155 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 159 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 159 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 159 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 163 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 163 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 163 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 155 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 155 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 155 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 157 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 157 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 157 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 159 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 159 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 159 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 161 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 161 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 161 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 163 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 163 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 163 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 165 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 165 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 165 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 155 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 156 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 157 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 158 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 159 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 160 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 161 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 162 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 163 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 164 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 165 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 166 + 3].Value = "Amount";

            #endregion

            #region oktober

            Grd1.Header.Cells[0, 167 + 3].Value = "Rate" + Environment.NewLine + "Oct";
            Grd1.Header.Cells[0, 167 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 167 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 168 + 3].Value = "Oct";
            Grd1.Header.Cells[3, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 168 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 168 + 3].Value = "RO";
            Grd1.Header.Cells[2, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 168 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 172 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 172 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 172 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 176 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 176 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 176 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 168 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 168 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 168 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 170 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 170 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 170 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 172 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 172 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 172 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 174 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 174 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 174 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 176 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 176 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 176 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 178 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 178 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 178 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 168 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 169 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 170 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 171 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 172 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 173 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 174 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 175 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 176 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 177 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 178 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 179 + 3].Value = "Amount";

            #endregion

            #region november

            Grd1.Header.Cells[0, 180 + 3].Value = "Rate" + Environment.NewLine + "Nov";
            Grd1.Header.Cells[0, 180 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 180 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 181 + 3].Value = "Nov";
            Grd1.Header.Cells[3, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 181 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 181 + 3].Value = "RO";
            Grd1.Header.Cells[2, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 181 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 185 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 185 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 185 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 189 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 189 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 189 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 181 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 181 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 181 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 183 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 183 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 183 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 185 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 185 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 185 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 187 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 187 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 187 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 189 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 189 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 189 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 191 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 191 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 191 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 181 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 182 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 183 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 184 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 185 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 186 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 187 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 188 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 189 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 190 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 191 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 192 + 3].Value = "Amount";

            #endregion

            #region desember

            Grd1.Header.Cells[0, 193 + 3].Value = "Rate" + Environment.NewLine + "Dec";
            Grd1.Header.Cells[0, 193 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 193 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 194 + 3].Value = "Dec";
            Grd1.Header.Cells[3, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 194 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 194 + 3].Value = "RO";
            Grd1.Header.Cells[2, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 194 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 198 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 198 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 198 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 202 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 202 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 202 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 194 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 194 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 194 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 196 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 196 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 196 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 198 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 198 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 198 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 200 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 200 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 200 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 202 + 3].Value = "This Month";
            Grd1.Header.Cells[1, 202 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 202 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 204 + 3].Value = "Up to This Month";
            Grd1.Header.Cells[1, 204 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 204 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 194 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 195 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 196 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 197 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 198 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 199 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 200 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 201 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 202 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 203 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 204 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 205 + 3].Value = "Amount";

            #endregion

            #region q4

            Grd1.Header.Cells[0, 206 + 3].Value = "Rate" + Environment.NewLine + "Q4";
            Grd1.Header.Cells[0, 206 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 206 + 3].SpanRows = 4;

            Grd1.Header.Cells[3, 207 + 3].Value = "Q4";
            Grd1.Header.Cells[3, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[3, 207 + 3].SpanCols = 12;
            Grd1.Header.Cells[2, 207 + 3].Value = "RO";
            Grd1.Header.Cells[2, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 207 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 211 + 3].Value = "Realization";
            Grd1.Header.Cells[2, 211 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 211 + 3].SpanCols = 4;
            Grd1.Header.Cells[2, 215 + 3].Value = "Remaining" + Environment.NewLine + "Budget";
            Grd1.Header.Cells[2, 215 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 215 + 3].SpanCols = 4;
            Grd1.Header.Cells[1, 207 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 207 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 207 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 209 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 209 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 209 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 211 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 211 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 211 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 213 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 213 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 213 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 215 + 3].Value = "This Quarter";
            Grd1.Header.Cells[1, 215 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 215 + 3].SpanCols = 2;
            Grd1.Header.Cells[1, 217 + 3].Value = "Up to This Quarter";
            Grd1.Header.Cells[1, 217 + 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, 217 + 3].SpanCols = 2;
            Grd1.Header.Cells[0, 207 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 208 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 209 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 210 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 211 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 212 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 213 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 214 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 215 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 216 + 3].Value = "Amount";
            Grd1.Header.Cells[0, 217 + 3].Value = "Quantity";
            Grd1.Header.Cells[0, 218 + 3].Value = "Amount";

            #endregion

            for (int i = 0; i < 16; i++)
            {
                Grd1.Cols[11 + (i * 13) + 3].Visible = false;
                for (int j = 0; j < 6; j++)
                    Grd1.Cols[12 + (i * 13) + 3 + (2 * j)].Visible = false;
            }

            Grd1.Cols[11 + 3].Visible = false;
            for (int c = 0; c < 6; c++)
                Grd1.Cols[12 + 3 + (2 * c)].Visible = false;

            Sm.GrdFormatDec(Grd1, mDecimalCols, 0);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            foreach (var x in mDecimalCols)
                Grd1.Cols[x].Width = 130;

            Sm.GrdColInvisible(Grd1, new int[] { 3 });

            if (mQuarterMenu)
            {
                Sm.GrdColInvisible(Grd1, mPartOfQ1Cols);
                Sm.GrdColInvisible(Grd1, mPartOfQ2Cols);
                Sm.GrdColInvisible(Grd1, mPartOfQ3Cols);
                Sm.GrdColInvisible(Grd1, mPartOfQ4Cols);
            }
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ml.Clear();
                Sm.ClearGrd(Grd1, false);

                var l1 = new List<CBP2>();
                var l2 = new List<CAS>();
                var l3 = new List<CBPBudgetType>();
                var l4 = new List<COAJournal>();
                var l5 = new List<RecvVdAutoDO>();

                //string ProfitCenterCode = GetCcbCode(CcbProfitCenterCode, "ProfitCenter");
                string Yr = Sm.GetLue(LueYr);
                string CCCode = string.Empty;
                string SelectedCOA = string.Empty;

                //if (CcbCCCode.Text.Length > 0)
                //    CCCode = GetCcbCode(CcbCCCode, "CostCenter");
                //else
                //    CCCode = GetCCCodeFromProfitCenter(ProfitCenterCode);

                //CCCode = GetChildCostCenter(CCCode);

                CCCode = GetCcbCCCode();
                //CCCode = Sm.GetCcb(CcbCCCode).Replace(", ",",");

                PrepData(ref ml, Yr, CCCode, mDocType, ref SelectedCOA);
                if (ml.Count > 0)
                {
                    //SelectedCOA = GetSelectedCOA(ref ml);
                    PrepDataCAS(ref l2, Yr, CCCode);
                    PrepDataRecvVdAutoDO(ref l5, Yr, CCCode);
                    PrepJournalDataTemp(ref l4, Yr, CCCode, SelectedCOA);
                    if (l4.Count > 0)
                        GetJournalDataToCBP(ref ml, ref l4);

                    ProcessRealization(ref ml, ref l2, ref l5);

                    //ml.ForEach(x =>
                    //{
                    //    if (l2.Count > 0) GetCASDataToCBP(ref x, ref l2);
                    //    if (l5.Count > 0) GetRecvVdAutoDODataToCBP(ref x, ref l5);

                    //    ProcessOtherMisc(ref x, ref l2, ref l5);
                    //});
                    //if (l2.Count > 0)
                    //    GetCASDataToCBP(ref ml, ref l2);
                    //if (l5.Count > 0)
                    //    GetRecvVdAutoDODataToCBP(ref ml, ref l5);

                    //ProcessOtherMisc(ref ml, ref l2, ref l5);
                    ProcessOtherMisc2(ref ml, ref l2, ref l5);

                    ProcessAllData(ref ml);
                    //PrepBudgetType(ref ml, ref l3, CCCode, SelectedCOA);
                    //ProcessBudgetType(ref ml, ref l3);
                    ProcessSummary(ref ml, ref l1);
                    CalculateParent(ref l1);
                    ShowData(ref l1);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ExportToExcel()
        {
            Sm.ExportToExcelCBP(Grd1);
        }

        #region Additional Method

        private void SetCostCenter()
        {
            if (!mIsRptCBPPLUseDirectorateGroupFilter) return;
            bool IsCompleted = false, IsFirst = true; ;

            mlCostCenter.Clear();

            while (!IsCompleted)
                SetCostCenter(ref IsCompleted);
        }

        private void SetCostCenter(ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, CCCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct A.CCCode, B.DeptGrpCode  ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblDirectorateGroup C On B.DeptGrpCode = C.DirectorateGroupCode ");
            if (ChkDirectorateGroup.Checked)
            {
                SQL.AppendLine("    Where Find_In_Set(B.DeptGrpCode, @DeptGrpCode) ");
                Sm.CmParam<String>(ref cm, "@DeptGrpCode", mDirectorateGroupCode);
                IsCompleted = false;
            }
            else
                IsCompleted = true;
             
            SQL.AppendLine("Order By A.CCCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCode", "DeptGrpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        CCCode = Sm.DrStr(dr, c[0]);
                        IsExisted = false;
                        foreach (var x in mlCostCenter.Where(w => Sm.CompareStr(w, CCCode)))
                            IsExisted = true;
                        if (!IsExisted)
                        {
                            mlCostCenter.Add(CCCode);
                            IsCompleted = false;
                        }
                    }
                   IsCompleted = true;
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private string GetChildCostCenter(string CCCode)
        {
            string ChildCCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct CCCode) ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null ");
            SQL.AppendLine("And NotParentInd='Y' ");
            SQL.AppendLine("And ActInd='Y' ");
            SQL.AppendLine("And Parent Is Not Null ");
            SQL.AppendLine("And Find_In_Set(Parent, @Param) ");
            SQL.AppendLine("And Not Find_In_Set(CCCode, @Param) ");
            SQL.AppendLine("; ");

            ChildCCCode = Sm.GetValue(SQL.ToString(), CCCode);

            if (ChildCCCode.Length > 0)
                ChildCCCode = string.Concat(CCCode, ",", ChildCCCode);
            else
                ChildCCCode = CCCode;

            return ChildCCCode;
        }

        private string GetCCCodeFromProfitCenter(string ProfitCenterCode)
        {
            string CCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct CCCode) ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null ");
            SQL.AppendLine("And Find_In_Set(ProfitCenterCode, @Param) ");
            SQL.AppendLine("And NotParentInd='Y' ");
            SQL.AppendLine("And ActInd='Y' ");
            SQL.AppendLine("; ");

            CCCode = Sm.GetValue(SQL.ToString(), ProfitCenterCode);

            return CCCode;
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void GetColCounts(
            int lowLim, int hiLim,
            int q1LowLim, int q1HiLim,
            int q2LowLim, int q2HiLim,
            int q3LowLim, int q3HiLim,
            int q4LowLim, int q4HiLim
            )
        {
            for (int i = lowLim; i < hiLim; ++i)
            {
                mDecimalColsList.Add(i);
            }

            mDecimalCols = mDecimalColsList.ToArray();

            for (int i = q1LowLim; i <= q1HiLim; ++i)
            {
                mPartOfQ1ColsList.Add(i);
            }

            mPartOfQ1Cols = mPartOfQ1ColsList.ToArray();

            for (int i = q2LowLim; i <= q2HiLim; ++i)
            {
                mPartOfQ2ColsList.Add(i);
            }

            mPartOfQ2Cols = mPartOfQ2ColsList.ToArray();

            for (int i = q3LowLim; i <= q3HiLim; ++i)
            {
                mPartOfQ3ColsList.Add(i);
            }

            mPartOfQ3Cols = mPartOfQ3ColsList.ToArray();

            for (int i = q4LowLim; i <= q4HiLim; ++i)
            {
                mPartOfQ4ColsList.Add(i);
            }

            mPartOfQ4Cols = mPartOfQ4ColsList.ToArray();
        }

        #region Process Data

        private string GetSelectedCOA(ref List<CBP> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        private void PrepData(ref List<CBP> l, string Yr, string CCCode, string DocType, ref string SelectedCOA)
        {
            // DocType
            // 2 = PL
            // 3 = BS
            var SQL = new StringBuilder();

            var la = PrepCBP(Yr, CCCode, DocType);
            var lb = PrepCOA();
            var lc = PrepCostCenter(CCCode);
            var ld = PrepDepartment(CCCode);
            var le = PrepOption();
            var lf = PrepItem(Yr, CCCode, DocType);
            var lg = PrepBudgetType(CCCode) ;
            var coa = new List<string>();
            var lh = PrepBudgetTransfer(Yr, CCCode);
            var li = PrepBudgetReceive(Yr, CCCode);

            if (la.Count > 0)
            {
                var t = (from A in la
                    join B in lb on A.AcNo equals B.AcNo
                    join C in lc on A.CCCode equals C.CCCode
                    join D in ld on C.DeptCode equals D.DeptCode
                    //from E in le.Where(w => w.OptDesc == D.DeptGrpCode).DefaultIfEmpty() //on D.DeptGrpCode equals E.OptCode
                    //from F in lf.Where(w => w.ItCode == A.ItCode).DefaultIfEmpty() //on A.ItCode equals F.ItCode
                    select new CBP()
                    {
                        AcDesc = B.AcDesc,
                        AcNo = A.AcNo,
                        DeptGrpName = string.Empty,
                        DeptGrpCode = D.DeptGrpCode,
                        ItCode = A.ItCode,
                        ItName = string.Empty,
                        UoM = string.Empty,
                        BudgetType = string.Empty,

                        Rate = 0m,
                        QtyCBP = 0m,
                        AmtCBP = 0m,
                        QtyCAS = 0m,
                        AmtCAS = 0m,
                        QtyRemain = 0m,
                        AmtRemain = 0m,

                        // 01
                        Rate01 = A.Rate01,
                        QtyCBP01 = A.QtyCBP01, //Sm.DrDec(dr, c[8]) - Sm.DrDec(dr, c[70]) + Sm.DrDec(dr, c[82]),
                        AmtCBP01 = A.QtyCBP01 * A.Rate01, //(Sm.DrDec(dr, c[7]) * Sm.DrDec(dr, c[8])) - Sm.DrDec(dr, c[46]) + Sm.DrDec(dr, c[58]),
                        QtyCBPTo01 = A.QtyCBP01,//Sm.DrDec(dr, c[8]) - Sm.DrDec(dr, c[70]) + Sm.DrDec(dr, c[82]),
                        AmtCBPTo01 = A.QtyCBP01 * A.Rate01,//(Sm.DrDec(dr, c[7]) * Sm.DrDec(dr, c[8])) - Sm.DrDec(dr, c[46]) + Sm.DrDec(dr, c[58]),
                        QtyCAS01 = 0m,
                        AmtCAS01 = 0m,
                        QtyCASTo01 = 0m,
                        AmtCASTo01 = 0m,
                        QtyRemain01 = 0m,
                        AmtRemain01 = 0m,
                        QtyRemainTo01 = 0m,
                        AmtRemainTo01 = 0m,

                        //02
                        Rate02 = A.Rate02,
                        QtyCBP02 = A.QtyCBP02,//Sm.DrDec(dr, c[11]) - Sm.DrDec(dr, c[71]) + Sm.DrDec(dr, c[83]),
                        AmtCBP02 = A.QtyCBP02 * A.Rate02,//(Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[11])) - Sm.DrDec(dr, c[47]) + Sm.DrDec(dr, c[59]),
                        QtyCBPTo02 = 0m,
                        AmtCBPTo02 = 0m,
                        QtyCAS02 = 0m,
                        AmtCAS02 = 0m,
                        QtyCASTo02 = 0m,
                        AmtCASTo02 = 0m,
                        QtyRemain02 = 0m,
                        AmtRemain02 = 0m,
                        QtyRemainTo02 = 0m,
                        AmtRemainTo02 = 0m,

                        //03
                        Rate03 = A.Rate03,
                        QtyCBP03 = A.QtyCBP03,//Sm.DrDec(dr, c[14]) - Sm.DrDec(dr, c[72]) + Sm.DrDec(dr, c[84]),
                        AmtCBP03 = A.QtyCBP03 * A.Rate03, //(Sm.DrDec(dr, c[13]) * Sm.DrDec(dr, c[14])) - Sm.DrDec(dr, c[48]) + Sm.DrDec(dr, c[60]),
                        QtyCBPTo03 = 0m,
                        AmtCBPTo03 = 0m,
                        QtyCAS03 = 0m,
                        AmtCAS03 = 0m,
                        QtyCASTo03 = 0m,
                        AmtCASTo03 = 0m,
                        QtyRemain03 = 0m,
                        AmtRemain03 = 0m,
                        QtyRemainTo03 = 0m,
                        AmtRemainTo03 = 0m,

                        //Q1
                        RateQ1 = 0m,
                        QtyCBPQ1 = 0m,
                        AmtCBPQ1 = 0m,
                        QtyCBPToQ1 = 0m,
                        AmtCBPToQ1 = 0m,
                        QtyCASQ1 = 0m,
                        AmtCASQ1 = 0m,
                        QtyCASToQ1 = 0m,
                        AmtCASToQ1 = 0m,
                        QtyRemainQ1 = 0m,
                        AmtRemainQ1 = 0m,
                        QtyRemainToQ1 = 0m,
                        AmtRemainToQ1 = 0m,

                        //04
                        Rate04 = A.Rate04,// Sm.DrDec(dr, c[16]),
                        QtyCBP04 = A.QtyCBP04,//Sm.DrDec(dr, c[17]) - Sm.DrDec(dr, c[73]) + Sm.DrDec(dr, c[85]),
                        AmtCBP04 = A.QtyCBP04 * A.Rate04,//(Sm.DrDec(dr, c[16]) * Sm.DrDec(dr, c[17])) - Sm.DrDec(dr, c[49]) + Sm.DrDec(dr, c[61]),
                        QtyCBPTo04 = 0m,
                        AmtCBPTo04 = 0m,
                        QtyCAS04 = 0m,
                        AmtCAS04 = 0m,
                        QtyCASTo04 = 0m,
                        AmtCASTo04 = 0m,
                        QtyRemain04 = 0m,
                        AmtRemain04 = 0m,
                        QtyRemainTo04 = 0m,
                        AmtRemainTo04 = 0m,

                        //05
                        Rate05 = A.Rate05, //Sm.DrDec(dr, c[19]),
                        QtyCBP05 = A.QtyCBP05, //Sm.DrDec(dr, c[20]) - Sm.DrDec(dr, c[74]) + Sm.DrDec(dr, c[86]),
                        AmtCBP05 = A.Rate05 * A.QtyCBP05, //(Sm.DrDec(dr, c[19]) * Sm.DrDec(dr, c[20])) - Sm.DrDec(dr, c[50]) + Sm.DrDec(dr, c[62]),
                        QtyCBPTo05 = 0m,
                        AmtCBPTo05 = 0m,
                        QtyCAS05 = 0m,
                        AmtCAS05 = 0m,
                        QtyCASTo05 = 0m,
                        AmtCASTo05 = 0m,
                        QtyRemain05 = 0m,
                        AmtRemain05 = 0m,
                        QtyRemainTo05 = 0m,
                        AmtRemainTo05 = 0m,

                        //06
                        Rate06 = A.Rate06, //Sm.DrDec(dr, c[22]),
                        QtyCBP06 = A.QtyCBP06, //Sm.DrDec(dr, c[23]) - Sm.DrDec(dr, c[75]) + Sm.DrDec(dr, c[87]),
                        AmtCBP06 = A.Rate06 * A.QtyCBP06, //(Sm.DrDec(dr, c[22]) * Sm.DrDec(dr, c[23])) - Sm.DrDec(dr, c[51]) + Sm.DrDec(dr, c[63]),
                        QtyCBPTo06 = 0m,
                        AmtCBPTo06 = 0m,
                        QtyCAS06 = 0m,
                        AmtCAS06 = 0m,
                        QtyCASTo06 = 0m,
                        AmtCASTo06 = 0m,
                        QtyRemain06 = 0m,
                        AmtRemain06 = 0m,
                        QtyRemainTo06 = 0m,
                        AmtRemainTo06 = 0m,

                        //Q2
                        RateQ2 = 0m,
                        QtyCBPQ2 = 0m,
                        AmtCBPQ2 = 0m,
                        QtyCBPToQ2 = 0m,
                        AmtCBPToQ2 = 0m,
                        QtyCASQ2 = 0m,
                        AmtCASQ2 = 0m,
                        QtyCASToQ2 = 0m,
                        AmtCASToQ2 = 0m,
                        QtyRemainQ2 = 0m,
                        AmtRemainQ2 = 0m,
                        QtyRemainToQ2 = 0m,
                        AmtRemainToQ2 = 0m,

                        //07
                        Rate07 = A.Rate07, //Sm.DrDec(dr, c[25]),
                        QtyCBP07 = A.QtyCBP07, //Sm.DrDec(dr, c[26]) - Sm.DrDec(dr, c[76]) + Sm.DrDec(dr, c[88]),
                        AmtCBP07 = A.Rate07 * A.QtyCBP07, //(Sm.DrDec(dr, c[25]) * Sm.DrDec(dr, c[26])) - Sm.DrDec(dr, c[52]) + Sm.DrDec(dr, c[64]),
                        QtyCBPTo07 = 0m,
                        AmtCBPTo07 = 0m,
                        QtyCAS07 = 0m,
                        AmtCAS07 = 0m,
                        QtyCASTo07 = 0m,
                        AmtCASTo07 = 0m,
                        QtyRemain07 = 0m,
                        AmtRemain07 = 0m,
                        QtyRemainTo07 = 0m,
                        AmtRemainTo07 = 0m,

                        //08
                        Rate08 = A.Rate08, //Sm.DrDec(dr, c[28]),
                        QtyCBP08 = A.QtyCBP08, //Sm.DrDec(dr, c[29]) - Sm.DrDec(dr, c[77]) + Sm.DrDec(dr, c[89]),
                        AmtCBP08 = A.Rate08 * A.QtyCBP08, //(Sm.DrDec(dr, c[28]) * Sm.DrDec(dr, c[29])) - Sm.DrDec(dr, c[53]) + Sm.DrDec(dr, c[65]),
                        QtyCBPTo08 = 0m,
                        AmtCBPTo08 = 0m,
                        QtyCAS08 = 0m,
                        AmtCAS08 = 0m,
                        QtyCASTo08 = 0m,
                        AmtCASTo08 = 0m,
                        QtyRemain08 = 0m,
                        AmtRemain08 = 0m,
                        QtyRemainTo08 = 0m,
                        AmtRemainTo08 = 0m,

                        //09
                        Rate09 = A.Rate09, //Sm.DrDec(dr, c[31]) - Sm.DrDec(dr, c[78]) + Sm.DrDec(dr, c[90]),
                        QtyCBP09 = A.QtyCBP09, //Sm.DrDec(dr, c[32]),
                        AmtCBP09 = A.Rate09 * A.QtyCBP09, //(Sm.DrDec(dr, c[31]) * Sm.DrDec(dr, c[32])) - Sm.DrDec(dr, c[54]) + Sm.DrDec(dr, c[66]),
                        QtyCBPTo09 = 0m,
                        AmtCBPTo09 = 0m,
                        QtyCAS09 = 0m,
                        AmtCAS09 = 0m,
                        QtyCASTo09 = 0m,
                        AmtCASTo09 = 0m,
                        QtyRemain09 = 0m,
                        AmtRemain09 = 0m,
                        QtyRemainTo09 = 0m,
                        AmtRemainTo09 = 0m,

                        //Q3
                        RateQ3 = 0m,
                        QtyCBPQ3 = 0m,
                        AmtCBPQ3 = 0m,
                        QtyCBPToQ3 = 0m,
                        AmtCBPToQ3 = 0m,
                        QtyCASQ3 = 0m,
                        AmtCASQ3 = 0m,
                        QtyCASToQ3 = 0m,
                        AmtCASToQ3 = 0m,
                        QtyRemainQ3 = 0m,
                        AmtRemainQ3 = 0m,
                        QtyRemainToQ3 = 0m,
                        AmtRemainToQ3 = 0m,

                        //10
                        Rate10 = A.Rate10, //Sm.DrDec(dr, c[34]),
                        QtyCBP10 = A.QtyCBP10, //Sm.DrDec(dr, c[35]) - Sm.DrDec(dr, c[79]) + Sm.DrDec(dr, c[91]),
                        AmtCBP10 = A.Rate10 * A.QtyCBP10, //(Sm.DrDec(dr, c[34]) * Sm.DrDec(dr, c[35])) - Sm.DrDec(dr, c[55]) + Sm.DrDec(dr, c[67]),
                        QtyCBPTo10 = 0m,
                        AmtCBPTo10 = 0m,
                        QtyCAS10 = 0m,
                        AmtCAS10 = 0m,
                        QtyCASTo10 = 0m,
                        AmtCASTo10 = 0m,
                        QtyRemain10 = 0m,
                        AmtRemain10 = 0m,
                        QtyRemainTo10 = 0m,
                        AmtRemainTo10 = 0m,

                        //11
                        Rate11 = A.Rate11, //Sm.DrDec(dr, c[37]),
                        QtyCBP11 = A.QtyCBP11, //Sm.DrDec(dr, c[38]) - Sm.DrDec(dr, c[80]) + Sm.DrDec(dr, c[92]),
                        AmtCBP11 = A.Rate11 * A.QtyCBP11, //(Sm.DrDec(dr, c[37]) * Sm.DrDec(dr, c[38])) - Sm.DrDec(dr, c[56]) + Sm.DrDec(dr, c[68]),
                        QtyCBPTo11 = 0m,
                        AmtCBPTo11 = 0m,
                        QtyCAS11 = 0m,
                        AmtCAS11 = 0m,
                        QtyCASTo11 = 0m,
                        AmtCASTo11 = 0m,
                        QtyRemain11 = 0m,
                        AmtRemain11 = 0m,
                        QtyRemainTo11 = 0m,
                        AmtRemainTo11 = 0m,

                        //12
                        Rate12 = A.Rate12, //Sm.DrDec(dr, c[40]),
                        QtyCBP12 = A.QtyCBP12, //Sm.DrDec(dr, c[41]) - Sm.DrDec(dr, c[81]) + Sm.DrDec(dr, c[93]),
                        AmtCBP12 = A.Rate12 * A.QtyCBP12, //(Sm.DrDec(dr, c[40]) * Sm.DrDec(dr, c[41])) - Sm.DrDec(dr, c[57]) + Sm.DrDec(dr, c[69]),
                        QtyCBPTo12 = 0m,
                        AmtCBPTo12 = 0m,
                        QtyCAS12 = 0m,
                        AmtCAS12 = 0m,
                        QtyCASTo12 = 0m,
                        AmtCASTo12 = 0m,
                        QtyRemain12 = 0m,
                        AmtRemain12 = 0m,
                        QtyRemainTo12 = 0m,
                        AmtRemainTo12 = 0m,

                        //Q4
                        RateQ4 = 0m,
                        QtyCBPQ4 = 0m,
                        AmtCBPQ4 = 0m,
                        QtyCBPToQ4 = 0m,
                        AmtCBPToQ4 = 0m,
                        QtyCASQ4 = 0m,
                        AmtCASQ4 = 0m,
                        QtyCASToQ4 = 0m,
                        AmtCASToQ4 = 0m,
                        QtyRemainQ4 = 0m,
                        AmtRemainQ4 = 0m,
                        QtyRemainToQ4 = 0m,
                        AmtRemainToQ4 = 0m,

                        CCCode = A.CCCode, //Sm.DrStr(dr, c[43]),
                        Parent = B.Parent, //Sm.DrStr(dr, c[44]),
                        Level = B.Level, //Sm.DrInt(dr, c[45]),
                    }).ToList();

                foreach (var x in t)
                {
                    coa.Add(x.AcNo.ToString());
                    string DeptGrpName = string.Empty;
                    string ItName = string.Empty;
                    string Uom = string.Empty;
                    string BudgetType = string.Empty;
                    decimal BudgetTransferAmt01 = 0m;
                    decimal BudgetReceiveAmt01 = 0m;
                    decimal BudgetTransferQty01 = 0m;
                    decimal BudgetReceiveQty01 = 0m;
                    decimal BudgetTransferAmt02 = 0m;
                    decimal BudgetReceiveAmt02 = 0m;
                    decimal BudgetTransferQty02 = 0m;
                    decimal BudgetReceiveQty02 = 0m;
                    decimal BudgetTransferAmt03 = 0m;
                    decimal BudgetReceiveAmt03 = 0m;
                    decimal BudgetTransferQty03 = 0m;
                    decimal BudgetReceiveQty03 = 0m;
                    decimal BudgetTransferAmt04 = 0m;
                    decimal BudgetReceiveAmt04 = 0m;
                    decimal BudgetTransferQty04 = 0m;
                    decimal BudgetReceiveQty04 = 0m;
                    decimal BudgetTransferAmt05 = 0m;
                    decimal BudgetReceiveAmt05 = 0m;
                    decimal BudgetTransferQty05 = 0m;
                    decimal BudgetReceiveQty05 = 0m;
                    decimal BudgetTransferAmt06 = 0m;
                    decimal BudgetReceiveAmt06 = 0m;
                    decimal BudgetTransferQty06 = 0m;
                    decimal BudgetReceiveQty06 = 0m;
                    decimal BudgetTransferAmt07 = 0m;
                    decimal BudgetReceiveAmt07 = 0m;
                    decimal BudgetTransferQty07 = 0m;
                    decimal BudgetReceiveQty07 = 0m;
                    decimal BudgetTransferAmt08 = 0m;
                    decimal BudgetReceiveAmt08 = 0m;
                    decimal BudgetTransferQty08 = 0m;
                    decimal BudgetReceiveQty08 = 0m;
                    decimal BudgetTransferAmt09 = 0m;
                    decimal BudgetReceiveAmt09 = 0m;
                    decimal BudgetTransferQty09 = 0m;
                    decimal BudgetReceiveQty09 = 0m;
                    decimal BudgetTransferAmt10 = 0m;
                    decimal BudgetReceiveAmt10 = 0m;
                    decimal BudgetTransferQty10 = 0m;
                    decimal BudgetReceiveQty10 = 0m;
                    decimal BudgetTransferAmt11 = 0m;
                    decimal BudgetReceiveAmt11 = 0m;
                    decimal BudgetTransferQty11 = 0m;
                    decimal BudgetReceiveQty11 = 0m;
                    decimal BudgetTransferAmt12 = 0m;
                    decimal BudgetReceiveAmt12 = 0m;
                    decimal BudgetTransferQty12 = 0m;
                    decimal BudgetReceiveQty12 = 0m;

                    if (le.Count > 0)
                    {
                        foreach (var y in le.Where(w => w.OptCode == x.DeptGrpCode)) DeptGrpName = y.OptDesc;
                    }

                    if (lf.Count > 0)
                    {
                        foreach (var z in lf.Where(w => w.ItCode == x.ItCode))
                        {
                            ItName = z.ItName;
                            Uom = z.UoM;
                        }
                    }

                    if (lg.Count > 0)
                    {
                        foreach (var a in lg.Where(w => w.AcNo == x.AcNo)) BudgetType = a.BudgetType;
                    }

                    if (lh.Count > 0)
                    {
                        foreach (var b in lh.Where(w => w.AcNo == x.AcNo && w.CCCode == x.CCCode && w.ItCode == x.ItCode))
                        {
                            BudgetTransferAmt01 += b.TransferAmt01;
                            BudgetTransferQty01 += b.TransferQty01;
                            BudgetTransferAmt02 += b.TransferAmt02;
                            BudgetTransferQty02 += b.TransferQty02;
                            BudgetTransferAmt03 += b.TransferAmt03;
                            BudgetTransferQty03 += b.TransferQty03;
                            BudgetTransferAmt04 += b.TransferAmt04;
                            BudgetTransferQty04 += b.TransferQty04;
                            BudgetTransferAmt05 += b.TransferAmt05;
                            BudgetTransferQty05 += b.TransferQty05;
                            BudgetTransferAmt06 += b.TransferAmt06;
                            BudgetTransferQty06 += b.TransferQty06;
                            BudgetTransferAmt07 += b.TransferAmt07;
                            BudgetTransferQty07 += b.TransferQty07;
                            BudgetTransferAmt08 += b.TransferAmt08;
                            BudgetTransferQty08 += b.TransferQty08;
                            BudgetTransferAmt09 += b.TransferAmt09;
                            BudgetTransferQty09 += b.TransferQty09;
                            BudgetTransferAmt10 += b.TransferAmt10;
                            BudgetTransferQty10 += b.TransferQty10;
                            BudgetTransferAmt11 += b.TransferAmt11;
                            BudgetTransferQty11 += b.TransferQty11;
                            BudgetTransferAmt12 += b.TransferAmt12;
                            BudgetTransferQty12 += b.TransferQty12;
                        }
                    }

                    if (li.Count > 0)
                    {
                        foreach (var c in li.Where(w => w.AcNo == x.AcNo && w.CCCode == x.CCCode && w.ItCode == x.ItCode))
                        {
                            BudgetReceiveAmt01 += c.ReceiveAmt01;
                            BudgetReceiveQty01 += c.ReceiveQty01;
                            BudgetReceiveAmt02 += c.ReceiveAmt02;
                            BudgetReceiveQty02 += c.ReceiveQty02;
                            BudgetReceiveAmt03 += c.ReceiveAmt03;
                            BudgetReceiveQty03 += c.ReceiveQty03;
                            BudgetReceiveAmt04 += c.ReceiveAmt04;
                            BudgetReceiveQty04 += c.ReceiveQty04;
                            BudgetReceiveAmt05 += c.ReceiveAmt05;
                            BudgetReceiveQty05 += c.ReceiveQty05;
                            BudgetReceiveAmt06 += c.ReceiveAmt06;
                            BudgetReceiveQty06 += c.ReceiveQty06;
                            BudgetReceiveAmt07 += c.ReceiveAmt07;
                            BudgetReceiveQty07 += c.ReceiveQty07;
                            BudgetReceiveAmt08 += c.ReceiveAmt08;
                            BudgetReceiveQty08 += c.ReceiveQty08;
                            BudgetReceiveAmt09 += c.ReceiveAmt09;
                            BudgetReceiveQty09 += c.ReceiveQty09;
                            BudgetReceiveAmt10 += c.ReceiveAmt10;
                            BudgetReceiveQty10 += c.ReceiveQty10;
                            BudgetReceiveAmt11 += c.ReceiveAmt11;
                            BudgetReceiveQty11 += c.ReceiveQty11;
                            BudgetReceiveAmt12 += c.ReceiveAmt12;
                            BudgetReceiveQty12 += c.ReceiveQty12;
                        }
                    }

                    l.Add(new CBP()
                    {
                        AcDesc = x.AcDesc,
                        AcNo = x.AcNo,
                        DeptGrpName = DeptGrpName,
                        ItCode = x.ItCode,
                        ItName = ItName,
                        UoM = Uom,
                        BudgetType = BudgetType,

                        Rate = x.Rate,
                        QtyCBP = x.QtyCBP,
                        AmtCBP = x.AmtCBP,
                        QtyCAS = x.QtyCAS,
                        AmtCAS = x.AmtCAS,
                        QtyRemain = x.QtyRemain,
                        AmtRemain = x.AmtRemain,

                        // 01
                        Rate01 = x.Rate01,
                        QtyCBP01 = x.QtyCBP01 - BudgetTransferQty01 + BudgetReceiveQty01,
                        AmtCBP01 = x.AmtCBP01 - BudgetTransferAmt01 + BudgetReceiveAmt01,
                        QtyCBPTo01 = x.QtyCBPTo01 - BudgetTransferQty01 + BudgetReceiveQty01,
                        AmtCBPTo01 = x.AmtCBPTo01 - BudgetTransferAmt01 + BudgetReceiveAmt01,
                        QtyCAS01 = x.QtyCAS01,
                        AmtCAS01 = x.AmtCAS01,
                        QtyCASTo01 = x.QtyCASTo01,
                        AmtCASTo01 = x.AmtCASTo01,
                        QtyRemain01 = x.QtyRemain01,
                        AmtRemain01 = x.AmtRemain01,
                        QtyRemainTo01 = x.QtyRemainTo01,
                        AmtRemainTo01 = x.AmtRemainTo01,

                        //02
                        Rate02 = x.Rate02,
                        QtyCBP02 = x.QtyCBP02 - BudgetTransferQty02 + BudgetReceiveQty02,
                        AmtCBP02 = x.AmtCBP02 - BudgetTransferAmt02 + BudgetReceiveAmt02,
                        QtyCBPTo02 = x.QtyCBPTo02,
                        AmtCBPTo02 = x.AmtCBPTo02,
                        QtyCAS02 = x.QtyCAS02,
                        AmtCAS02 = x.AmtCAS02,
                        QtyCASTo02 = x.QtyCASTo02,
                        AmtCASTo02 = x.AmtCASTo02,
                        QtyRemain02 = x.QtyRemain02,
                        AmtRemain02 = x.AmtRemain02,
                        QtyRemainTo02 = x.QtyRemainTo02,
                        AmtRemainTo02 = x.AmtRemainTo02,

                        //03
                        Rate03 = x.Rate03,
                        QtyCBP03 = x.QtyCBP03 - BudgetTransferQty03 + BudgetReceiveQty03,
                        AmtCBP03 = x.AmtCBP03 - BudgetTransferAmt03 + BudgetReceiveAmt03,
                        QtyCBPTo03 = x.QtyCBPTo03,
                        AmtCBPTo03 = x.AmtCBPTo03,
                        QtyCAS03 = x.QtyCAS03,
                        AmtCAS03 = x.AmtCAS03,
                        QtyCASTo03 = x.QtyCASTo03,
                        AmtCASTo03 = x.AmtCASTo03,
                        QtyRemain03 = x.QtyRemain03,
                        AmtRemain03 = x.AmtRemain03,
                        QtyRemainTo03 = x.QtyRemainTo03,
                        AmtRemainTo03 = x.AmtRemainTo03,

                        //Q1
                        RateQ1 = x.RateQ1,
                        QtyCBPQ1 = x.QtyCBPQ1,
                        AmtCBPQ1 = x.AmtCBPQ1,
                        QtyCBPToQ1 = x.QtyCBPToQ1,
                        AmtCBPToQ1 = x.AmtCBPToQ1,
                        QtyCASQ1 = x.QtyCASQ1,
                        AmtCASQ1 = x.AmtCASQ1,
                        QtyCASToQ1 = x.QtyCASToQ1,
                        AmtCASToQ1 = x.AmtCASToQ1,
                        QtyRemainQ1 = x.QtyRemainQ1,
                        AmtRemainQ1 = x.AmtRemainQ1,
                        QtyRemainToQ1 = x.QtyRemainToQ1,
                        AmtRemainToQ1 = x.AmtRemainToQ1,

                        //04
                        Rate04 = x.Rate04,
                        QtyCBP04 = x.QtyCBP04 - BudgetTransferQty04 + BudgetReceiveQty04,
                        AmtCBP04 = x.AmtCBP04 - BudgetTransferAmt04 + BudgetReceiveAmt04,
                        QtyCBPTo04 = x.QtyCBPTo04,
                        AmtCBPTo04 = x.AmtCBPTo04,
                        QtyCAS04 = x.QtyCAS04,
                        AmtCAS04 = x.AmtCAS04,
                        QtyCASTo04 = x.QtyCASTo04,
                        AmtCASTo04 = x.AmtCASTo04,
                        QtyRemain04 = x.QtyRemain04,
                        AmtRemain04 = x.AmtRemain04,
                        QtyRemainTo04 = x.QtyRemainTo04,
                        AmtRemainTo04 = x.AmtRemainTo04,

                        //05
                        Rate05 = x.Rate05,
                        QtyCBP05 = x.QtyCBP05 - BudgetTransferQty05 + BudgetReceiveQty05,
                        AmtCBP05 = x.AmtCBP05 - BudgetTransferAmt05 + BudgetReceiveAmt05,
                        QtyCBPTo05 = x.QtyCBPTo05,
                        AmtCBPTo05 = x.AmtCBPTo05,
                        QtyCAS05 = x.QtyCAS05,
                        AmtCAS05 = x.AmtCAS05,
                        QtyCASTo05 = x.QtyCASTo05,
                        AmtCASTo05 = x.AmtCASTo05,
                        QtyRemain05 = x.QtyRemain05,
                        AmtRemain05 = x.AmtRemain05,
                        QtyRemainTo05 = x.QtyRemainTo05,
                        AmtRemainTo05 = x.AmtRemainTo05,

                        //06
                        Rate06 = x.Rate06,
                        QtyCBP06 = x.QtyCBP06 - BudgetTransferQty06 + BudgetReceiveQty06,
                        AmtCBP06 = x.AmtCBP06 - BudgetTransferAmt06 + BudgetReceiveAmt06,
                        QtyCBPTo06 = x.QtyCBPTo06,
                        AmtCBPTo06 = x.AmtCBPTo06,
                        QtyCAS06 = x.QtyCAS06,
                        AmtCAS06 = x.AmtCAS06,
                        QtyCASTo06 = x.QtyCASTo06,
                        AmtCASTo06 = x.AmtCASTo06,
                        QtyRemain06 = x.QtyRemain06,
                        AmtRemain06 = x.AmtRemain06,
                        QtyRemainTo06 = x.QtyRemainTo06,
                        AmtRemainTo06 = x.AmtRemainTo06,

                        //Q2
                        RateQ2 = x.RateQ2,
                        QtyCBPQ2 = x.QtyCBPQ2,
                        AmtCBPQ2 = x.AmtCBPQ2,
                        QtyCBPToQ2 = x.QtyCBPToQ2,
                        AmtCBPToQ2 = x.AmtCBPToQ2,
                        QtyCASQ2 = x.QtyCASQ2,
                        AmtCASQ2 = x.AmtCASQ2,
                        QtyCASToQ2 = x.QtyCASToQ2,
                        AmtCASToQ2 = x.AmtCASToQ2,
                        QtyRemainQ2 = x.QtyRemainQ2,
                        AmtRemainQ2 = x.AmtRemainQ2,
                        QtyRemainToQ2 = x.QtyRemainToQ2,
                        AmtRemainToQ2 = x.AmtRemainToQ2,

                        //07
                        Rate07 = x.Rate07,
                        QtyCBP07 = x.QtyCBP07 - BudgetTransferQty07 + BudgetReceiveQty07,
                        AmtCBP07 = x.AmtCBP07 - BudgetTransferAmt07 + BudgetReceiveAmt07,
                        QtyCBPTo07 = x.QtyCBPTo07,
                        AmtCBPTo07 = x.AmtCBPTo07,
                        QtyCAS07 = x.QtyCAS07,
                        AmtCAS07 = x.AmtCAS07,
                        QtyCASTo07 = x.QtyCASTo07,
                        AmtCASTo07 = x.AmtCASTo07,
                        QtyRemain07 = x.QtyRemain07,
                        AmtRemain07 = x.AmtRemain07,
                        QtyRemainTo07 = x.QtyRemainTo07,
                        AmtRemainTo07 = x.AmtRemainTo07,

                        //08
                        Rate08 = x.Rate08,
                        QtyCBP08 = x.QtyCBP08 - BudgetTransferQty08 + BudgetReceiveQty08,
                        AmtCBP08 = x.AmtCBP08 - BudgetTransferAmt08 + BudgetReceiveAmt08,
                        QtyCBPTo08 = x.QtyCBPTo08,
                        AmtCBPTo08 = x.AmtCBPTo08,
                        QtyCAS08 = x.QtyCAS08,
                        AmtCAS08 = x.AmtCAS08,
                        QtyCASTo08 = x.QtyCASTo08,
                        AmtCASTo08 = x.AmtCASTo08,
                        QtyRemain08 = x.QtyRemain08,
                        AmtRemain08 = x.AmtRemain08,
                        QtyRemainTo08 = x.QtyRemainTo08,
                        AmtRemainTo08 = x.AmtRemainTo08,

                        //09
                        Rate09 = x.Rate09,
                        QtyCBP09 = x.QtyCBP09 - BudgetTransferQty09 + BudgetReceiveQty09,
                        AmtCBP09 = x.AmtCBP09 - BudgetTransferAmt09 + BudgetReceiveAmt09,
                        QtyCBPTo09 = x.QtyCBPTo09,
                        AmtCBPTo09 = x.AmtCBPTo09,
                        QtyCAS09 = x.QtyCAS09,
                        AmtCAS09 = x.AmtCAS09,
                        QtyCASTo09 = x.QtyCASTo09,
                        AmtCASTo09 = x.AmtCASTo09,
                        QtyRemain09 = x.QtyRemain09,
                        AmtRemain09 = x.AmtRemain09,
                        QtyRemainTo09 = x.QtyRemainTo09,
                        AmtRemainTo09 = x.AmtRemainTo09,

                        //Q3
                        RateQ3 = x.RateQ3,
                        QtyCBPQ3 = x.QtyCBPQ3,
                        AmtCBPQ3 = x.AmtCBPQ3,
                        QtyCBPToQ3 = x.QtyCBPToQ3,
                        AmtCBPToQ3 = x.AmtCBPToQ3,
                        QtyCASQ3 = x.QtyCASQ3,
                        AmtCASQ3 = x.AmtCASQ3,
                        QtyCASToQ3 = x.QtyCASToQ3,
                        AmtCASToQ3 = x.AmtCASToQ3,
                        QtyRemainQ3 = x.QtyRemainQ3,
                        AmtRemainQ3 = x.AmtRemainQ3,
                        QtyRemainToQ3 = x.QtyRemainToQ3,
                        AmtRemainToQ3 = x.AmtRemainToQ3,


                        //10
                        Rate10 = x.Rate10,
                        QtyCBP10 = x.QtyCBP10 - BudgetTransferQty10 + BudgetReceiveQty10,
                        AmtCBP10 = x.AmtCBP10 - BudgetTransferAmt10 + BudgetReceiveAmt10,
                        QtyCBPTo10 = x.QtyCBPTo10,
                        AmtCBPTo10 = x.AmtCBPTo10,
                        QtyCAS10 = x.QtyCAS10,
                        AmtCAS10 = x.AmtCAS10,
                        QtyCASTo10 = x.QtyCASTo10,
                        AmtCASTo10 = x.AmtCASTo10,
                        QtyRemain10 = x.QtyRemain10,
                        AmtRemain10 = x.AmtRemain10,
                        QtyRemainTo10 = x.QtyRemainTo10,
                        AmtRemainTo10 = x.AmtRemainTo10,

                        //11
                        Rate11 = x.Rate11,
                        QtyCBP11 = x.QtyCBP11 - BudgetTransferQty11 + BudgetReceiveQty11,
                        AmtCBP11 = x.AmtCBP11 - BudgetTransferAmt11 + BudgetReceiveAmt11,
                        QtyCBPTo11 = x.QtyCBPTo11,
                        AmtCBPTo11 = x.AmtCBPTo11,
                        QtyCAS11 = x.QtyCAS11,
                        AmtCAS11 = x.AmtCAS11,
                        QtyCASTo11 = x.QtyCASTo11,
                        AmtCASTo11 = x.AmtCASTo11,
                        QtyRemain11 = x.QtyRemain11,
                        AmtRemain11 = x.AmtRemain11,
                        QtyRemainTo11 = x.QtyRemainTo11,
                        AmtRemainTo11 = x.AmtRemainTo11,

                        //12
                        Rate12 = x.Rate12,
                        QtyCBP12 = x.QtyCBP12 - BudgetTransferQty12 + BudgetReceiveQty12,
                        AmtCBP12 = x.AmtCBP12 - BudgetTransferAmt12 + BudgetReceiveAmt12,
                        QtyCBPTo12 = x.QtyCBPTo12,
                        AmtCBPTo12 = x.AmtCBPTo12,
                        QtyCAS12 = x.QtyCAS12,
                        AmtCAS12 = x.AmtCAS12,
                        QtyCASTo12 = x.QtyCASTo12,
                        AmtCASTo12 = x.AmtCASTo12,
                        QtyRemain12 = x.QtyRemain12,
                        AmtRemain12 = x.AmtRemain12,
                        QtyRemainTo12 = x.QtyRemainTo12,
                        AmtRemainTo12 = x.AmtRemainTo12,

                        //Q4
                        RateQ4 = x.RateQ4,
                        QtyCBPQ4 = x.QtyCBPQ4,
                        AmtCBPQ4 = x.AmtCBPQ4,
                        QtyCBPToQ4 = x.QtyCBPToQ4,
                        AmtCBPToQ4 = x.AmtCBPToQ4,
                        QtyCASQ4 = x.QtyCASQ4,
                        AmtCASQ4 = x.AmtCASQ4,
                        QtyCASToQ4 = x.QtyCASToQ4,
                        AmtCASToQ4 = x.AmtCASToQ4,
                        QtyRemainQ4 = x.QtyRemainQ4,
                        AmtRemainQ4 = x.AmtRemainQ4,
                        QtyRemainToQ4 = x.QtyRemainToQ4,
                        AmtRemainToQ4 = x.AmtRemainToQ4,

                        CCCode = x.CCCode,
                        Parent = x.Parent,
                        Level = x.Level,
                    });
                }

                SelectedCOA = string.Join(",", coa.ToArray());
            }

            coa.Clear();la.Clear();lb.Clear();lc.Clear();ld.Clear();le.Clear();lf.Clear();lg.Clear();lh.Clear();li.Clear();

            #region Old Codes

            //SQL.AppendLine("Select T1.CCCode, T2.AcDesc, T1.AcNo, T2.Parent, T2.Level, T5.OptDesc As DeptGrpName, T1.ItCode, T6.ItName, T6.PurchaseUomCode, ");
            //SQL.AppendLine("0.00 Amt, T1.Rate01, T1.Qty01, T1.Amt01 Amt01, T1.Rate02, T1.Qty02, T1.Amt02 Amt02, ");
            //SQL.AppendLine("T1.Rate03, T1.Qty03, T1.Amt03 Amt03, T1.Rate04, T1.Qty04, T1.Amt04 Amt04, ");
            //SQL.AppendLine("T1.Rate05, T1.Qty05, T1.Amt05 Amt05, T1.Rate06, T1.Qty06, T1.Amt06 Amt06, ");
            //SQL.AppendLine("T1.Rate07, T1.Qty07, T1.Amt07 Amt07, T1.Rate08, T1.Qty08, T1.Amt08 Amt08, ");
            //SQL.AppendLine("T1.Rate09, T1.Qty09, T1.Amt09 Amt09, T1.Rate10, T1.Qty10, T1.Amt10 Amt10, ");
            //SQL.AppendLine("T1.Rate11, T1.Qty11, T1.Amt11 Amt11, T1.Rate12, T1.Qty12, T1.Amt12 Amt12, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt01, 0.00) TransferAmt01, IfNull(T7.TransferQty01, 0.00) TransferQty01, IfNull(T8.ReceiveAmt01, 0.00) ReceiveAmt01, IfNull(T8.ReceiveQty01, 0.00) ReceiveQty01, IfNull(T7.TransferAmt02, 0.00) TransferAmt02, IfNull(T7.TransferQty02, 0.00) TransferQty02, IfNull(T8.ReceiveAmt02, 0.00) ReceiveAmt02, IfNull(T8.ReceiveQty02, 0.00) ReceiveQty02, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt03, 0.00) TransferAmt03, IfNull(T7.TransferQty03, 0.00) TransferQty03, IfNull(T8.ReceiveAmt03, 0.00) ReceiveAmt03, IfNull(T8.ReceiveQty03, 0.00) ReceiveQty03, IfNull(T7.TransferAmt04, 0.00) TransferAmt04, IfNull(T7.TransferQty04, 0.00) TransferQty04, IfNull(T8.ReceiveAmt04, 0.00) ReceiveAmt04, IfNull(T8.ReceiveQty04, 0.00) ReceiveQty04, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt05, 0.00) TransferAmt05, IfNull(T7.TransferQty05, 0.00) TransferQty05, IfNull(T8.ReceiveAmt05, 0.00) ReceiveAmt05, IfNull(T8.ReceiveQty05, 0.00) ReceiveQty05, IfNull(T7.TransferAmt06, 0.00) TransferAmt06, IfNull(T7.TransferQty06, 0.00) TransferQty06, IfNull(T8.ReceiveAmt06, 0.00) ReceiveAmt06, IfNull(T8.ReceiveQty06, 0.00) ReceiveQty06, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt07, 0.00) TransferAmt07, IfNull(T7.TransferQty07, 0.00) TransferQty07, IfNull(T8.ReceiveAmt07, 0.00) ReceiveAmt07, IfNull(T8.ReceiveQty07, 0.00) ReceiveQty07, IfNull(T7.TransferAmt08, 0.00) TransferAmt08, IfNull(T7.TransferQty08, 0.00) TransferQty08, IfNull(T8.ReceiveAmt08, 0.00) ReceiveAmt08, IfNull(T8.ReceiveQty08, 0.00) ReceiveQty08, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt09, 0.00) TransferAmt09, IfNull(T7.TransferQty09, 0.00) TransferQty09, IfNull(T8.ReceiveAmt09, 0.00) ReceiveAmt09, IfNull(T8.ReceiveQty09, 0.00) ReceiveQty09, IfNull(T7.TransferAmt10, 0.00) TransferAmt10, IfNull(T7.TransferQty10, 0.00) TransferQty10, IfNull(T8.ReceiveAmt10, 0.00) ReceiveAmt10, IfNull(T8.ReceiveQty10, 0.00) ReceiveQty10, ");
            //SQL.AppendLine("IfNull(T7.TransferAmt11, 0.00) TransferAmt11, IfNull(T7.TransferQty11, 0.00) TransferQty11, IfNull(T8.ReceiveAmt11, 0.00) ReceiveAmt11, IfNull(T8.ReceiveQty11, 0.00) ReceiveQty11, IfNull(T7.TransferAmt12, 0.00) TransferAmt12, IfNull(T7.TransferQty12, 0.00) TransferQty12, IfNull(T8.ReceiveAmt12, 0.00) ReceiveAmt12, IfNull(T8.ReceiveQty12, 0.00) ReceiveQty12 ");

            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select A.CCCode, A.Yr, B.AcNo, C.ItCode, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate01, 0.00)) Rate01, Sum(IfNull(C.Qty01, 0.00)) Qty01, Sum(B.Amt01) Amt01, Sum(IfNull(C.Rate02, 0.00)) Rate02, Sum(IfNull(C.Qty02, 0.00)) Qty02, Sum(B.Amt02) Amt02, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate03, 0.00)) Rate03, Sum(IfNull(C.Qty03, 0.00)) Qty03, Sum(B.Amt03) Amt03, Sum(IfNull(C.Rate04, 0.00)) Rate04, Sum(IfNull(C.Qty04, 0.00)) Qty04, Sum(B.Amt04) Amt04, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate05, 0.00)) Rate05, Sum(IfNull(C.Qty05, 0.00)) Qty05, Sum(B.Amt05) Amt05, Sum(IfNull(C.Rate06, 0.00)) Rate06, Sum(IfNull(C.Qty06, 0.00)) Qty06, Sum(B.Amt06) Amt06, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate07, 0.00)) Rate07, Sum(IfNull(C.Qty07, 0.00)) Qty07, Sum(B.Amt07) Amt07, Sum(IfNull(C.Rate08, 0.00)) Rate08, Sum(IfNull(C.Qty08, 0.00)) Qty08, Sum(B.Amt08) Amt08, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate09, 0.00)) Rate09, Sum(IfNull(C.Qty09, 0.00)) Qty09, Sum(B.Amt09) Amt09, Sum(IfNull(C.Rate10, 0.00)) Rate10, Sum(IfNull(C.Qty10, 0.00)) Qty10, Sum(B.Amt10) Amt10, ");
            //SQL.AppendLine("    Sum(IfNull(C.Rate11, 0.00)) Rate11, Sum(IfNull(C.Qty11, 0.00)) Qty11, Sum(B.Amt11) Amt11, Sum(IfNull(C.Rate12, 0.00)) Rate12, Sum(IfNull(C.Qty12, 0.00)) Qty12, Sum(B.Amt12) Amt12 ");
            //SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
            //SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("    And A.CCCode Is Not Null ");
            ////if (!CCCode.Contains("Consolidate"))
            ////    SQL.AppendLine("    And Find_In_Set(A.CCCode, @CCCode) ");
            //if (CCCode.Length > 0)
            //    SQL.AppendLine("    And Find_In_Set(A.CCCode, @CCCode) ");
            //else
            //    SQL.AppendLine("    And 1=0 ");

            //SQL.AppendLine("        And A.DocType = @DocType ");
            //SQL.AppendLine("        And A.CompletedInd = 'Y' ");
            //SQL.AppendLine("        And A.Yr = @Yr ");
            //SQL.AppendLine("    Left Join TblCompanyBudgetPlanDtl2 C On A.DocNo = C.DocNo And B.AcNo = C.AcNo ");
            //SQL.AppendLine("    Group By A.CCCode, A.Yr, B.AcNo, C.ItCode ");
            //SQL.AppendLine(") T1 ");
            //SQL.AppendLine("Inner Join TblCOA T2 On T1.AcNo = T2.AcNo ");
            //SQL.AppendLine("Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
            //SQL.AppendLine("Inner Join TblDepartment T4 On T3.DeptCode = T4.DeptCode ");
            //SQL.AppendLine("Left Join TblOption T5 On T4.DeptGrpCode = T5.OptCode And T5.OptCat = 'DepartmentGroup' ");
            //SQL.AppendLine("Left Join TblItem T6 On T1.ItCode = T6.ItCode ");
            //SQL.AppendLine("LEFT JOIN ( ");

            //SQL.AppendLine("    Select X1.COA, X1.CCCode, X1.ItCode, Sum(X1.TransferAmt01) TransferAmt01, Sum(X1.TransferQty01) TransferQty01, Sum(X1.TransferAmt02) TransferAmt02, Sum(X1.TransferQty02) TransferQty02, ");
            //SQL.AppendLine("    Sum(X1.TransferAmt03) TransferAmt03, Sum(X1.TransferQty03) TransferQty03, Sum(X1.TransferAmt04) TransferAmt04, Sum(X1.TransferQty04) TransferQty04, ");
            //SQL.AppendLine("    Sum(X1.TransferAmt05) TransferAmt05, Sum(X1.TransferQty05) TransferQty05, Sum(X1.TransferAmt06) TransferAmt06, Sum(X1.TransferQty06) TransferQty06, ");
            //SQL.AppendLine("    Sum(X1.TransferAmt07) TransferAmt07, Sum(X1.TransferQty07) TransferQty07, Sum(X1.TransferAmt08) TransferAmt08, Sum(X1.TransferQty08) TransferQty08, ");
            //SQL.AppendLine("    Sum(X1.TransferAmt09) TransferAmt09, Sum(X1.TransferQty09) TransferQty09, Sum(X1.TransferAmt10) TransferAmt10, Sum(X1.TransferQty10) TransferQty10, ");
            //SQL.AppendLine("    Sum(X1.TransferAmt11) TransferAmt11, Sum(X1.TransferQty11) TransferQty11, Sum(X1.TransferAmt12) TransferAmt12, Sum(X1.TransferQty12) TransferQty12 From ( ");
            //for (int i = 1; i <= 12; i++)
            //{
            //    if (IsFirst == false)
            //        SQL.AppendLine("            Union All ");

            //    SQL.AppendLine("            SELECT C.AcNo as COA, A.CCCode, B.ItCode, ");
            //    for (int j = 1; j <= 12; ++j)
            //    {
            //        if (j == i) SQL.AppendLine("            IfNull(B.TransferredAmt, 0.00) As TransferAmt" + Sm.Right(string.Concat('0', j), 2) + ", IfNull(B.Qty, 0.00) As TransferQty" + Sm.Right(string.Concat('0', j), 2) + " ");
            //        else SQL.AppendLine("          0.00 As TransferAmt" + Sm.Right(string.Concat('0', j), 2) + " , 0.00 As TransferQty" + Sm.Right(string.Concat('0', j), 2) + " ");

            //        if (j != 12) SQL.AppendLine("           , ");
            //    }
            //    SQL.AppendLine("            FROM TblBudgetTransferCostCenterHdr A ");
            //    SQL.AppendLine("            INNER JOIN TblBudgetTransferCostCenterDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            //    SQL.AppendLine("            And A.Yr = @Yr ");
            //    SQL.AppendLine("            AND A.Mth = Right(Concat(0, " + i + "), 2) ");
            //    if (CCCode.Length > 0)
            //        SQL.AppendLine("        And Find_In_Set(A.CCCode, @CCCode) ");
            //    SQL.AppendLine("            INNER JOIN TblCostCategory C ON A.CCCode = C.CCCode  ");
            //    SQL.AppendLine("            INNER JOIN TblBudgetCategory D ON B.BCCode = D.BCCode ");

            //    IsFirst = false;

            //}

            //SQL.AppendLine("    ) X1 Group By X1.COA, X1.CCCode ");
            //SQL.AppendLine(")T7 ON T7.COA = T1.AcNo AND T1.CCcode = T7.CCcode And T1.ItCode = T7.ItCode");
            //SQL.AppendLine("LEFT JOIN ( ");

            //SQL.AppendLine("    Select X1.COA, X1.CCCode2, X1.ItCode2, Sum(X1.ReceiveAmt01) ReceiveAmt01, Sum(X1.ReceiveQty01) ReceiveQty01, Sum(X1.ReceiveAmt02) ReceiveAmt02, Sum(X1.ReceiveQty02) ReceiveQty02, ");
            //SQL.AppendLine("    Sum(X1.ReceiveAmt03) ReceiveAmt03, Sum(X1.ReceiveQty03) ReceiveQty03, Sum(X1.ReceiveAmt04) ReceiveAmt04, Sum(X1.ReceiveQty04) ReceiveQty04, ");
            //SQL.AppendLine("    Sum(X1.ReceiveAmt05) ReceiveAmt05, Sum(X1.ReceiveQty05) ReceiveQty05, Sum(X1.ReceiveAmt06) ReceiveAmt06, Sum(X1.ReceiveQty06) ReceiveQty06, ");
            //SQL.AppendLine("    Sum(X1.ReceiveAmt07) ReceiveAmt07, Sum(X1.ReceiveQty07) ReceiveQty07, Sum(X1.ReceiveAmt08) ReceiveAmt08, Sum(X1.ReceiveQty08) ReceiveQty08, ");
            //SQL.AppendLine("    Sum(X1.ReceiveAmt09) ReceiveAmt09, Sum(X1.ReceiveQty09) ReceiveQty09, Sum(X1.ReceiveAmt10) ReceiveAmt10, Sum(X1.ReceiveQty10) ReceiveQty10, ");
            //SQL.AppendLine("    Sum(X1.ReceiveAmt11) ReceiveAmt11, Sum(X1.ReceiveQty11) ReceiveQty11, Sum(X1.ReceiveAmt12) ReceiveAmt12, Sum(X1.ReceiveQty12) ReceiveQty12 From ( ");

            //IsFirst = true;
            //for (int i = 1; i <= 12; i++)
            //{
            //    if (IsFirst == false)
            //        SQL.AppendLine("            Union All ");
            //    SQL.AppendLine("            SELECT C.AcNo as COA , A.CCcode2, B.ItCode2, ");
            //    for (int j = 1; j <= 12; ++j)
            //    {
            //        if (j == i) SQL.AppendLine("            IfNull(B.TransferredAmt, 0.00) As ReceiveAmt" + Sm.Right(string.Concat('0', j), 2) + ", IfNull(B.Qty2, 0.00) As ReceiveQty" + Sm.Right(string.Concat('0', j), 2) + " ");
            //        else SQL.AppendLine("          0.00 As ReceiveAmt" + Sm.Right(string.Concat('0', j), 2) + " , 0.00 As ReceiveQty" + Sm.Right(string.Concat('0', j), 2) + " ");

            //        if (j != 12) SQL.AppendLine("           , ");
            //    }
            //    SQL.AppendLine("            FROM TblBudgetTransferCostCenterHdr A ");
            //    SQL.AppendLine("            INNER JOIN TblBudgetTransferCostCenterDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            //    SQL.AppendLine("            And A.Yr = @Yr ");
            //    SQL.AppendLine("            AND A.Mth2 = Right(Concat(0, " + i + "), 2) ");
            //    if (CCCode.Length > 0)
            //        SQL.AppendLine("        And Find_In_Set(A.CCCode2, @CCCode) ");
            //    SQL.AppendLine("            INNER JOIN TblCostCategory C ON A.CCCode2 = C.CCCode  ");
            //    SQL.AppendLine("            INNER JOIN TblBudgetCategory D ON B.BCCode2 = D.BCCode ");

            //    IsFirst = false;
            //}
            //SQL.AppendLine("    ) X1 Group By X1.COA, X1.CCCode2 ");
            //SQL.AppendLine(")T8 ON T8.COA = T1.AcNo AND T1.CCCode = T8.CCCode2 And T1.ItCode = T8.ItCode2; ");


            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
            //    Sm.CmParam<String>(ref cm, "@Yr", Yr);
            //    //if (!CCCode.Contains("Consolidate")) Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            //    if (CCCode.Length > 0)
            //        Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            //    Sm.CmParam<String>(ref cm, "@DocType", DocType);
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[]
            //    {
            //        //0
            //        "AcDesc", 
            //        //1-5
            //        "AcNo", "DeptGrpName", "ItCode", "ItName", "PurchaseUomCode", 
            //        //6-10
            //        "Amt", "Rate01", "Qty01", "Amt01", "Rate02", 
            //        //11-15
            //        "Qty02", "Amt02", "Rate03", "Qty03", "Amt03", 
            //        //16-20
            //        "Rate04", "Qty04", "Amt04", "Rate05", "Qty05", 
            //        //21-25
            //        "Amt05", "Rate06", "Qty06", "Amt06", "Rate07", 
            //        //26-30
            //        "Qty07", "Amt07", "Rate08", "Qty08", "Amt08", 
            //        //31-35
            //        "Rate09", "Qty09", "Amt09", "Rate10", "Qty10", 
            //        //36-40
            //        "Amt10", "Rate11", "Qty11", "Amt11", "Rate12", 
            //        //41-45
            //        "Qty12", "Amt12", "CCCode", "Parent", "Level",
            //        //46-50
            //        "TransferAmt01", "TransferAmt02","TransferAmt03","TransferAmt04","TransferAmt05",
            //        //51-55
            //        "TransferAmt06", "TransferAmt07","TransferAmt07","TransferAmt09","TransferAmt10",
            //        //56-60
            //        "TransferAmt11", "TransferAmt12","ReceiveAmt01","ReceiveAmt02","ReceiveAmt03",
            //        //61-65
            //        "ReceiveAmt04", "ReceiveAmt05","ReceiveAmt06","ReceiveAmt07","ReceiveAmt08",
            //        //66-70
            //        "ReceiveAmt09", "ReceiveAmt10","ReceiveAmt11","ReceiveAmt12", "TransferQty01",

            //        //71-75
            //        "TransferQty02", "TransferQty03", "TransferQty04", "TransferQty05", "TransferQty06",

            //        //76-80
            //        "TransferQty07", "TransferQty08", "TransferQty09", "TransferQty10", "TransferQty11",

            //        //81-85
            //        "TransferQty12", "ReceiveQty01", "ReceiveQty02", "ReceiveQty03", "ReceiveQty04", 

            //        //86-90
            //        "ReceiveQty05", "ReceiveQty06", "ReceiveQty07", "ReceiveQty08", "ReceiveQty09", 

            //        //91-93
            //        "ReceiveQty10", "ReceiveQty11", "ReceiveQty12"


            //    });
            //    if (dr.HasRows)
            //    {
            //        Grd1.BeginUpdate();
            //        while (dr.Read())
            //        {
            //            l.Add(new CBP()
            //            {
            //                AcDesc = Sm.DrStr(dr, c[0]),
            //                AcNo = Sm.DrStr(dr, c[1]),
            //                DeptGrpName = Sm.DrStr(dr, c[2]),
            //                ItCode = Sm.DrStr(dr, c[3]),
            //                ItName = Sm.DrStr(dr, c[4]),
            //                UoM = Sm.DrStr(dr, c[5]),
            //                BudgetType = string.Empty,

            //                Rate = 0m,
            //                QtyCBP = 0m,
            //                AmtCBP = 0m,
            //                QtyCAS = 0m,
            //                AmtCAS = 0m,
            //                QtyRemain = 0m,
            //                AmtRemain = 0m,

            //                // 01
            //                Rate01 = Sm.DrDec(dr, c[7]),
            //                QtyCBP01 = Sm.DrDec(dr, c[8]) - Sm.DrDec(dr, c[70]) + Sm.DrDec(dr, c[82]),
            //                AmtCBP01 = (Sm.DrDec(dr, c[7]) * Sm.DrDec(dr, c[8])) - Sm.DrDec(dr, c[46]) + Sm.DrDec(dr, c[58]),
            //                QtyCBPTo01 = Sm.DrDec(dr, c[8]) - Sm.DrDec(dr, c[70]) + Sm.DrDec(dr, c[82]),
            //                AmtCBPTo01 = (Sm.DrDec(dr, c[7]) * Sm.DrDec(dr, c[8])) - Sm.DrDec(dr, c[46]) + Sm.DrDec(dr, c[58]),
            //                QtyCAS01 = 0m,
            //                AmtCAS01 = 0m,
            //                QtyCASTo01 = 0m,
            //                AmtCASTo01 = 0m,
            //                QtyRemain01 = 0m,
            //                AmtRemain01 = 0m,
            //                QtyRemainTo01 = 0m,
            //                AmtRemainTo01 = 0m,

            //                //02
            //                Rate02 = Sm.DrDec(dr, c[10]),
            //                QtyCBP02 = Sm.DrDec(dr, c[11]) - Sm.DrDec(dr, c[71]) + Sm.DrDec(dr, c[83]),
            //                AmtCBP02 = (Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[11])) - Sm.DrDec(dr, c[47]) + Sm.DrDec(dr, c[59]),
            //                QtyCBPTo02 = 0m,
            //                AmtCBPTo02 = 0m,
            //                QtyCAS02 = 0m,
            //                AmtCAS02 = 0m,
            //                QtyCASTo02 = 0m,
            //                AmtCASTo02 = 0m,
            //                QtyRemain02 = 0m,
            //                AmtRemain02 = 0m,
            //                QtyRemainTo02 = 0m,
            //                AmtRemainTo02 = 0m,

            //                //03
            //                Rate03 = Sm.DrDec(dr, c[13]),
            //                QtyCBP03 = Sm.DrDec(dr, c[14]) - Sm.DrDec(dr, c[72]) + Sm.DrDec(dr, c[84]),
            //                AmtCBP03 = (Sm.DrDec(dr, c[13]) * Sm.DrDec(dr, c[14])) - Sm.DrDec(dr, c[48]) + Sm.DrDec(dr, c[60]),
            //                QtyCBPTo03 = 0m,
            //                AmtCBPTo03 = 0m,
            //                QtyCAS03 = 0m,
            //                AmtCAS03 = 0m,
            //                QtyCASTo03 = 0m,
            //                AmtCASTo03 = 0m,
            //                QtyRemain03 = 0m,
            //                AmtRemain03 = 0m,
            //                QtyRemainTo03 = 0m,
            //                AmtRemainTo03 = 0m,

            //                //Q1
            //                RateQ1 = 0m,
            //                QtyCBPQ1 = 0m,
            //                AmtCBPQ1 = 0m,
            //                QtyCBPToQ1 = 0m,
            //                AmtCBPToQ1 = 0m,
            //                QtyCASQ1 = 0m,
            //                AmtCASQ1 = 0m,
            //                QtyCASToQ1 = 0m,
            //                AmtCASToQ1 = 0m,
            //                QtyRemainQ1 = 0m,
            //                AmtRemainQ1 = 0m,
            //                QtyRemainToQ1 = 0m,
            //                AmtRemainToQ1 = 0m,

            //                //04
            //                Rate04 = Sm.DrDec(dr, c[16]),
            //                QtyCBP04 = Sm.DrDec(dr, c[17]) - Sm.DrDec(dr, c[73]) + Sm.DrDec(dr, c[85]),
            //                AmtCBP04 = (Sm.DrDec(dr, c[16]) * Sm.DrDec(dr, c[17])) - Sm.DrDec(dr, c[49]) + Sm.DrDec(dr, c[61]),
            //                QtyCBPTo04 = 0m,
            //                AmtCBPTo04 = 0m,
            //                QtyCAS04 = 0m,
            //                AmtCAS04 = 0m,
            //                QtyCASTo04 = 0m,
            //                AmtCASTo04 = 0m,
            //                QtyRemain04 = 0m,
            //                AmtRemain04 = 0m,
            //                QtyRemainTo04 = 0m,
            //                AmtRemainTo04 = 0m,

            //                //05
            //                Rate05 = Sm.DrDec(dr, c[19]),
            //                QtyCBP05 = Sm.DrDec(dr, c[20]) - Sm.DrDec(dr, c[74]) + Sm.DrDec(dr, c[86]),
            //                AmtCBP05 = (Sm.DrDec(dr, c[19]) * Sm.DrDec(dr, c[20])) - Sm.DrDec(dr, c[50]) + Sm.DrDec(dr, c[62]),
            //                QtyCBPTo05 = 0m,
            //                AmtCBPTo05 = 0m,
            //                QtyCAS05 = 0m,
            //                AmtCAS05 = 0m,
            //                QtyCASTo05 = 0m,
            //                AmtCASTo05 = 0m,
            //                QtyRemain05 = 0m,
            //                AmtRemain05 = 0m,
            //                QtyRemainTo05 = 0m,
            //                AmtRemainTo05 = 0m,

            //                //06
            //                Rate06 = Sm.DrDec(dr, c[22]),
            //                QtyCBP06 = Sm.DrDec(dr, c[23]) - Sm.DrDec(dr, c[75]) + Sm.DrDec(dr, c[87]),
            //                AmtCBP06 = (Sm.DrDec(dr, c[22]) * Sm.DrDec(dr, c[23])) - Sm.DrDec(dr, c[51]) + Sm.DrDec(dr, c[63]),
            //                QtyCBPTo06 = 0m,
            //                AmtCBPTo06 = 0m,
            //                QtyCAS06 = 0m,
            //                AmtCAS06 = 0m,
            //                QtyCASTo06 = 0m,
            //                AmtCASTo06 = 0m,
            //                QtyRemain06 = 0m,
            //                AmtRemain06 = 0m,
            //                QtyRemainTo06 = 0m,
            //                AmtRemainTo06 = 0m,

            //                //Q2
            //                RateQ2 = 0m,
            //                QtyCBPQ2 = 0m,
            //                AmtCBPQ2 = 0m,
            //                QtyCBPToQ2 = 0m,
            //                AmtCBPToQ2 = 0m,
            //                QtyCASQ2 = 0m,
            //                AmtCASQ2 = 0m,
            //                QtyCASToQ2 = 0m,
            //                AmtCASToQ2 = 0m,
            //                QtyRemainQ2 = 0m,
            //                AmtRemainQ2 = 0m,
            //                QtyRemainToQ2 = 0m,
            //                AmtRemainToQ2 = 0m,

            //                //07
            //                Rate07 = Sm.DrDec(dr, c[25]),
            //                QtyCBP07 = Sm.DrDec(dr, c[26]) - Sm.DrDec(dr, c[76]) + Sm.DrDec(dr, c[88]),
            //                AmtCBP07 = (Sm.DrDec(dr, c[25]) * Sm.DrDec(dr, c[26])) - Sm.DrDec(dr, c[52]) + Sm.DrDec(dr, c[64]),
            //                QtyCBPTo07 = 0m,
            //                AmtCBPTo07 = 0m,
            //                QtyCAS07 = 0m,
            //                AmtCAS07 = 0m,
            //                QtyCASTo07 = 0m,
            //                AmtCASTo07 = 0m,
            //                QtyRemain07 = 0m,
            //                AmtRemain07 = 0m,
            //                QtyRemainTo07 = 0m,
            //                AmtRemainTo07 = 0m,

            //                //08
            //                Rate08 = Sm.DrDec(dr, c[28]),
            //                QtyCBP08 = Sm.DrDec(dr, c[29]) - Sm.DrDec(dr, c[77]) + Sm.DrDec(dr, c[89]),
            //                AmtCBP08 = (Sm.DrDec(dr, c[28]) * Sm.DrDec(dr, c[29])) - Sm.DrDec(dr, c[53]) + Sm.DrDec(dr, c[65]),
            //                QtyCBPTo08 = 0m,
            //                AmtCBPTo08 = 0m,
            //                QtyCAS08 = 0m,
            //                AmtCAS08 = 0m,
            //                QtyCASTo08 = 0m,
            //                AmtCASTo08 = 0m,
            //                QtyRemain08 = 0m,
            //                AmtRemain08 = 0m,
            //                QtyRemainTo08 = 0m,
            //                AmtRemainTo08 = 0m,

            //                //09
            //                Rate09 = Sm.DrDec(dr, c[31]) - Sm.DrDec(dr, c[78]) + Sm.DrDec(dr, c[90]),
            //                QtyCBP09 = Sm.DrDec(dr, c[32]),
            //                AmtCBP09 = (Sm.DrDec(dr, c[31]) * Sm.DrDec(dr, c[32])) - Sm.DrDec(dr, c[54]) + Sm.DrDec(dr, c[66]),
            //                QtyCBPTo09 = 0m,
            //                AmtCBPTo09 = 0m,
            //                QtyCAS09 = 0m,
            //                AmtCAS09 = 0m,
            //                QtyCASTo09 = 0m,
            //                AmtCASTo09 = 0m,
            //                QtyRemain09 = 0m,
            //                AmtRemain09 = 0m,
            //                QtyRemainTo09 = 0m,
            //                AmtRemainTo09 = 0m,

            //                //Q3
            //                RateQ3 = 0m,
            //                QtyCBPQ3 = 0m,
            //                AmtCBPQ3 = 0m,
            //                QtyCBPToQ3 = 0m,
            //                AmtCBPToQ3 = 0m,
            //                QtyCASQ3 = 0m,
            //                AmtCASQ3 = 0m,
            //                QtyCASToQ3 = 0m,
            //                AmtCASToQ3 = 0m,
            //                QtyRemainQ3 = 0m,
            //                AmtRemainQ3 = 0m,
            //                QtyRemainToQ3 = 0m,
            //                AmtRemainToQ3 = 0m,


            //                //10
            //                Rate10 = Sm.DrDec(dr, c[34]),
            //                QtyCBP10 = Sm.DrDec(dr, c[35]) - Sm.DrDec(dr, c[79]) + Sm.DrDec(dr, c[91]),
            //                AmtCBP10 = (Sm.DrDec(dr, c[34]) * Sm.DrDec(dr, c[35])) - Sm.DrDec(dr, c[55]) + Sm.DrDec(dr, c[67]),
            //                QtyCBPTo10 = 0m,
            //                AmtCBPTo10 = 0m,
            //                QtyCAS10 = 0m,
            //                AmtCAS10 = 0m,
            //                QtyCASTo10 = 0m,
            //                AmtCASTo10 = 0m,
            //                QtyRemain10 = 0m,
            //                AmtRemain10 = 0m,
            //                QtyRemainTo10 = 0m,
            //                AmtRemainTo10 = 0m,

            //                //11
            //                Rate11 = Sm.DrDec(dr, c[37]),
            //                QtyCBP11 = Sm.DrDec(dr, c[38]) - Sm.DrDec(dr, c[80]) + Sm.DrDec(dr, c[92]),
            //                AmtCBP11 = (Sm.DrDec(dr, c[37]) * Sm.DrDec(dr, c[38])) - Sm.DrDec(dr, c[56]) + Sm.DrDec(dr, c[68]),
            //                QtyCBPTo11 = 0m,
            //                AmtCBPTo11 = 0m,
            //                QtyCAS11 = 0m,
            //                AmtCAS11 = 0m,
            //                QtyCASTo11 = 0m,
            //                AmtCASTo11 = 0m,
            //                QtyRemain11 = 0m,
            //                AmtRemain11 = 0m,
            //                QtyRemainTo11 = 0m,
            //                AmtRemainTo11 = 0m,

            //                //12
            //                Rate12 = Sm.DrDec(dr, c[40]),
            //                QtyCBP12 = Sm.DrDec(dr, c[41]) - Sm.DrDec(dr, c[81]) + Sm.DrDec(dr, c[93]),
            //                AmtCBP12 = (Sm.DrDec(dr, c[40]) * Sm.DrDec(dr, c[41])) - Sm.DrDec(dr, c[57]) + Sm.DrDec(dr, c[69]),
            //                QtyCBPTo12 = 0m,
            //                AmtCBPTo12 = 0m,
            //                QtyCAS12 = 0m,
            //                AmtCAS12 = 0m,
            //                QtyCASTo12 = 0m,
            //                AmtCASTo12 = 0m,
            //                QtyRemain12 = 0m,
            //                AmtRemain12 = 0m,
            //                QtyRemainTo12 = 0m,
            //                AmtRemainTo12 = 0m,

            //                //Q4
            //                RateQ4 = 0m,
            //                QtyCBPQ4 = 0m,
            //                AmtCBPQ4 = 0m,
            //                QtyCBPToQ4 = 0m,
            //                AmtCBPToQ4 = 0m,
            //                QtyCASQ4 = 0m,
            //                AmtCASQ4 = 0m,
            //                QtyCASToQ4 = 0m,
            //                AmtCASToQ4 = 0m,
            //                QtyRemainQ4 = 0m,
            //                AmtRemainQ4 = 0m,
            //                QtyRemainToQ4 = 0m,
            //                AmtRemainToQ4 = 0m,

            //                CCCode = Sm.DrStr(dr, c[43]),
            //                Parent = Sm.DrStr(dr, c[44]),
            //                Level = Sm.DrInt(dr, c[45]),
            //            });
            //        }
            //        Grd1.EndUpdate();
            //    }
            //    dr.Close();
            //}
            #endregion
        }

        private List<CBPSingle> PrepCBP(string Yr, string CCCode, string DocType)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);

            SQL.AppendLine("Select A.CCCode, A.Yr, B.AcNo, ");
            SQL.AppendLine("C.ItCode,  ");
            SQL.AppendLine("C.Rate01,  C.Qty01, B.Amt01,  C.Rate02,  C.Qty02, B.Amt02,  ");
            SQL.AppendLine("C.Rate03,  C.Qty03, B.Amt03,  C.Rate04,  C.Qty04, B.Amt04,  ");
            SQL.AppendLine("C.Rate05,  C.Qty05, B.Amt05,  C.Rate06,  C.Qty06, B.Amt06,  ");
            SQL.AppendLine("C.Rate07,  C.Qty07, B.Amt07,  C.Rate08,  C.Qty08, B.Amt08,  ");
            SQL.AppendLine("C.Rate09,  C.Qty09, B.Amt09,  C.Rate10,  C.Qty10, B.Amt10,  ");
            SQL.AppendLine("C.Rate11,  C.Qty11, B.Amt11,  C.Rate12,  C.Qty12, B.Amt12 ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.CCCode Is Not Null ");
            SQL.AppendLine("And Find_In_Set(A.CCCode, @CCCode) ");
            SQL.AppendLine("    And A.DocType = @DocType ");
            if (!mIsRptCBPPLShowNotCompletedCBPPL)
                SQL.AppendLine("    And A.CompletedInd = 'Y' ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("Left Join TblCompanyBudgetPlanDtl2 C On A.DocNo = C.DocNo And B.AcNo = C.AcNo ");

            var l = new List<CBPSingle>();
            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "CCCode",
                    //1-5
                    "Yr", "AcNo", "ItCode", "Rate01", "Qty01",
                    //6-10
                    "Amt01", "Rate02", "Qty02", "Amt02", "Rate03",
                    //11-15
                    "Qty03", "Amt03", "Rate04", "Qty04", "Amt04",
                    //16-20
                    "Rate05", "Qty05", "Amt05", "Rate06", "Qty06",
                    //21-25
                    "Amt06", "Rate07", "Qty07", "Amt07", "Rate08",
                    //26-30
                    "Qty08", "Amt08", "Rate09", "Qty09", "Amt09",
                    //31-35
                    "Rate10", "Qty10", "Amt10", "Rate11", "Qty11",
                    //36-39
                    "Amt11", "Rate12", "Qty12", "Amt12"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new CBPSingle()
                    {
                        CCCode = Sm.DrStr(dr, c[0]),
                        AcNo = Sm.DrStr(dr, c[2]),
                        ItCode = Sm.DrStr(dr, c[3]),
                        Rate01 = Sm.DrDec(dr, c[4]),
                        QtyCBP01 = Sm.DrDec(dr, c[5]),
                        AmtCBP01 = Sm.DrDec(dr, c[6]),
                        Rate02 = Sm.DrDec(dr, c[7]),
                        QtyCBP02 = Sm.DrDec(dr, c[8]),
                        AmtCBP02 = Sm.DrDec(dr, c[9]),
                        Rate03 = Sm.DrDec(dr, c[10]),
                        QtyCBP03 = Sm.DrDec(dr, c[11]),
                        AmtCBP03 = Sm.DrDec(dr, c[12]),
                        Rate04 = Sm.DrDec(dr, c[13]),
                        QtyCBP04 = Sm.DrDec(dr, c[14]),
                        AmtCBP04 = Sm.DrDec(dr, c[15]),
                        Rate05 = Sm.DrDec(dr, c[16]),
                        QtyCBP05 = Sm.DrDec(dr, c[17]),
                        AmtCBP05 = Sm.DrDec(dr, c[18]),
                        Rate06 = Sm.DrDec(dr, c[19]),
                        QtyCBP06 = Sm.DrDec(dr, c[20]),
                        AmtCBP06 = Sm.DrDec(dr, c[21]),
                        Rate07 = Sm.DrDec(dr, c[22]),
                        QtyCBP07 = Sm.DrDec(dr, c[23]),
                        AmtCBP07 = Sm.DrDec(dr, c[24]),
                        Rate08 = Sm.DrDec(dr, c[25]),
                        QtyCBP08 = Sm.DrDec(dr, c[26]),
                        AmtCBP08 = Sm.DrDec(dr, c[27]),
                        Rate09 = Sm.DrDec(dr, c[28]),
                        QtyCBP09 = Sm.DrDec(dr, c[29]),
                        AmtCBP09 = Sm.DrDec(dr, c[30]),
                        Rate10 = Sm.DrDec(dr, c[31]),
                        QtyCBP10 = Sm.DrDec(dr, c[32]),
                        AmtCBP10 = Sm.DrDec(dr, c[33]),
                        Rate11 = Sm.DrDec(dr, c[34]),
                        QtyCBP11 = Sm.DrDec(dr, c[35]),
                        AmtCBP11 = Sm.DrDec(dr, c[36]),
                        Rate12 = Sm.DrDec(dr, c[37]),
                        QtyCBP12 = Sm.DrDec(dr, c[38]),
                        AmtCBP12 = Sm.DrDec(dr, c[39])
                    });
                }, false
                );

            var lx = l.GroupBy(s => new { s.CCCode, s.AcNo, s.ItCode })
                .Select(t => new CBPSingle()
                {
                    CCCode = t.Key.CCCode,
                    AcNo = t.Key.AcNo,
                    ItCode = t.Key.ItCode,
                    Rate01 = t.Sum(d => d.Rate01),
                    QtyCBP01 = t.Sum(d => d.QtyCBP01),
                    AmtCBP01 = t.Sum(d => d.AmtCBP01),
                    Rate02 = t.Sum(d => d.Rate02),
                    QtyCBP02 = t.Sum(d => d.QtyCBP02),
                    AmtCBP02 = t.Sum(d => d.AmtCBP02),
                    Rate03 = t.Sum(d => d.Rate03),
                    QtyCBP03 = t.Sum(d => d.QtyCBP03),
                    AmtCBP03 = t.Sum(d => d.AmtCBP03),
                    Rate04 = t.Sum(d => d.Rate04),
                    QtyCBP04 = t.Sum(d => d.QtyCBP04),
                    AmtCBP04 = t.Sum(d => d.AmtCBP04),
                    Rate05 = t.Sum(d => d.Rate05),
                    QtyCBP05 = t.Sum(d => d.QtyCBP05),
                    AmtCBP05 = t.Sum(d => d.AmtCBP05),
                    Rate06 = t.Sum(d => d.Rate06),
                    QtyCBP06 = t.Sum(d => d.QtyCBP06),
                    AmtCBP06 = t.Sum(d => d.AmtCBP06),
                    Rate07 = t.Sum(d => d.Rate07),
                    QtyCBP07 = t.Sum(d => d.QtyCBP07),
                    AmtCBP07 = t.Sum(d => d.AmtCBP07),
                    Rate08 = t.Sum(d => d.Rate08),
                    QtyCBP08 = t.Sum(d => d.QtyCBP08),
                    AmtCBP08 = t.Sum(d => d.AmtCBP08),
                    Rate09 = t.Sum(d => d.Rate09),
                    QtyCBP09 = t.Sum(d => d.QtyCBP09),
                    AmtCBP09 = t.Sum(d => d.AmtCBP09),
                    Rate10 = t.Sum(d => d.Rate10),
                    QtyCBP10 = t.Sum(d => d.QtyCBP10),
                    AmtCBP10 = t.Sum(d => d.AmtCBP10),
                    Rate11 = t.Sum(d => d.Rate11),
                    QtyCBP11 = t.Sum(d => d.QtyCBP11),
                    AmtCBP11 = t.Sum(d => d.AmtCBP11),
                    Rate12 = t.Sum(d => d.Rate12),
                    QtyCBP12 = t.Sum(d => d.QtyCBP12),
                    AmtCBP12 = t.Sum(d => d.AmtCBP12),
                }).ToList();

            return lx;
        }

        private List<COASingle> PrepCOA()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, AcDesc, Parent, Level ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("; ");

            var l = new List<COASingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "AcNo", "AcDesc", "Parent", "Level"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new COASingle()
                    {
                        AcNo = Sm.DrStr(dr, c[0]),
                        AcDesc = Sm.DrStr(dr, c[1]),
                        Parent = Sm.DrStr(dr, c[2]),
                        Level = Sm.DrInt(dr, c[3]),
                    });
                },
                false);

            return l;
        }
        
        private List<CostCenterSingle> PrepCostCenter(string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            SQL.AppendLine("Select CCCode, CCName, DeptCode ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("And Find_In_Set(CCCode, @CCCode); ");

            var l = new List<CostCenterSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "CCCode", "CCName", "DeptCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new CostCenterSingle()
                    {
                        CCCode = Sm.DrStr(dr, c[0]),
                        CCName = Sm.DrStr(dr, c[1]),
                        DeptCode = Sm.DrStr(dr, c[2])
                    });
                },
                false);

            return l;
        }
        
        private List<DepartmentSingle> PrepDepartment(string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            SQL.AppendLine("Select DeptCode, DeptGrpCode ");
            SQL.AppendLine("From TblDepartment ");
            SQL.AppendLine("Where DeptCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct DeptCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where Find_In_Set(CCCode, @CCCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var l = new List<DepartmentSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "DeptCode", "DeptGrpCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new DepartmentSingle()
                    {
                        DeptCode = Sm.DrStr(dr, c[0]),
                        DeptGrpCode = Sm.DrStr(dr, c[1])
                    });
                },
                false);

            return l;
        }
        
        private List<OptionSingle> PrepOption()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select OptCode, OptDesc ");
            SQL.AppendLine("From TblOption ");
            SQL.AppendLine("Where OptCat = 'DepartmentGroup' ");

            var l = new List<OptionSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "OptCode", "OptDesc"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new OptionSingle()
                    {
                        OptCode = Sm.DrStr(dr, c[0]),
                        OptDesc = Sm.DrStr(dr, c[1])
                    });
                },
                false);

            return l;
        }

        private List<ItemSingle> PrepItem(string Yr, string CCCode, string DocType)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);

            SQL.AppendLine("Select ItCode, ItName, PurchaseUomCode Uom ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where ItCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct X2.ItCode ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr X1 ");
            SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl2 X2 On X1.DocNo = X2.DocNo "); 
            SQL.AppendLine("        And X1.DocType = @DocType ");
            SQL.AppendLine("        And X1.Yr = @Yr ");
            SQL.AppendLine("        And X1.CancelInd = 'N' ");
            if (!mIsRptCBPPLShowNotCompletedCBPPL)
                SQL.AppendLine("        And X1.CompletedInd = 'Y' ");
            SQL.AppendLine("        And X1.CCCode Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(X1.CCCode, @CCCode) ");
            SQL.AppendLine("); ");

            var l = new List<ItemSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "ItCode", "ItName", "Uom"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new ItemSingle()
                    {
                        ItCode = Sm.DrStr(dr, c[0]),
                        ItName = Sm.DrStr(dr, c[1]),
                        UoM = Sm.DrStr(dr, c[2])
                    });
                },
                false);

            return l;
        }

        private List<BudgetTypeSingle> PrepBudgetType(string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            SQL.AppendLine("Select Distinct A.AcNo, D.OptDesc BudgetType ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Inner Join TblCostcategory B On A.AcNo = B.AcNo And B.CCCode = @CCCode ");
            SQL.AppendLine("    And A.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
            SQL.AppendLine("Inner Join TblOption D On C.BudgetType = D.OptCode And D.OptCat = 'BudgetType' ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("  Select Distinct (AcNo) As AcNo From TblCOA Where AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) And Find_In_Set(AcNo, '" + SelectedCOA + "')");
            //SQL.AppendLine(")E On A.Acno = E.ACNo ");
            SQL.AppendLine("; ");

            var l = new List<BudgetTypeSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "AcNo", "BudgetType"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new BudgetTypeSingle()
                    {
                        AcNo = Sm.DrStr(dr, c[0]),
                        BudgetType = Sm.DrStr(dr, c[1]),
                    });
                },
                false);

            return l;
        }

        private List<BudgetTransferSingle> PrepBudgetTransfer(string Yr, string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            for (int i = 1; i <= 12; i++)
            {
                if (i != 1)
                    SQL.AppendLine("            Union All ");

                SQL.AppendLine("            SELECT C.AcNo, A.CCCode, B.ItCode, ");
                for (int j = 1; j <= 12; ++j)
                {
                    if (j == i) SQL.AppendLine("            B.TransferredAmt As TransferAmt" + Sm.Right(string.Concat('0', j), 2) + ", B.Qty As TransferQty" + Sm.Right(string.Concat('0', j), 2) + " ");
                    else SQL.AppendLine("          0.00 As TransferAmt" + Sm.Right(string.Concat('0', j), 2) + " , 0.00 As TransferQty" + Sm.Right(string.Concat('0', j), 2) + " ");

                    if (j != 12) SQL.AppendLine("           , ");
                }
                SQL.AppendLine("            FROM TblBudgetTransferCostCenterHdr A ");
                SQL.AppendLine("            INNER JOIN TblBudgetTransferCostCenterDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
                SQL.AppendLine("            And A.Yr = @Yr ");
                SQL.AppendLine("            AND A.Mth = Right(Concat(0, " + i + "), 2) ");
                if (CCCode.Length > 0)
                    SQL.AppendLine("        And Find_In_Set(A.CCCode, @CCCode) ");
                else
                    SQL.AppendLine("        And 1=0 ");
                SQL.AppendLine("            INNER JOIN TblCostCategory C ON A.CCCode = C.CCCode  ");
                SQL.AppendLine("            INNER JOIN TblBudgetCategory D ON B.BCCode = D.BCCode ");
            }

            var l = new List<BudgetTransferSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "AcNo", 
                    "CCCode", "ItCode", "TransferAmt01", "TransferQty01", "TransferAmt02", 
                    "TransferQty02", "TransferAmt03", "TransferQty03", "TransferAmt04", "TransferQty04", 
                    "TransferAmt05", "TransferQty05", "TransferAmt06", "TransferQty06", "TransferAmt07", 
                    "TransferQty07", "TransferAmt08", "TransferQty08", "TransferAmt09", "TransferQty09", 
                    "TransferAmt10", "TransferQty10", "TransferAmt11", "TransferQty11", "TransferAmt12", 
                    "TransferQty12"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new BudgetTransferSingle()
                    {
                        AcNo = Sm.DrStr(dr, c[0]),
                        CCCode = Sm.DrStr(dr, c[1]),
                        ItCode = Sm.DrStr(dr, c[2]),
                        TransferAmt01 = Sm.DrDec(dr, c[3]),
                        TransferQty01 = Sm.DrDec(dr, c[4]),
                        TransferAmt02 = Sm.DrDec(dr, c[5]),
                        TransferQty02 = Sm.DrDec(dr, c[6]),
                        TransferAmt03 = Sm.DrDec(dr, c[7]),
                        TransferQty03 = Sm.DrDec(dr, c[8]),
                        TransferAmt04 = Sm.DrDec(dr, c[9]),
                        TransferQty04 = Sm.DrDec(dr, c[10]),
                        TransferAmt05 = Sm.DrDec(dr, c[11]),
                        TransferQty05 = Sm.DrDec(dr, c[12]),
                        TransferAmt06 = Sm.DrDec(dr, c[13]),
                        TransferQty06 = Sm.DrDec(dr, c[14]),
                        TransferAmt07 = Sm.DrDec(dr, c[15]),
                        TransferQty07 = Sm.DrDec(dr, c[16]),
                        TransferAmt08 = Sm.DrDec(dr, c[17]),
                        TransferQty08 = Sm.DrDec(dr, c[18]),
                        TransferAmt09 = Sm.DrDec(dr, c[19]),
                        TransferQty09 = Sm.DrDec(dr, c[20]),
                        TransferAmt10 = Sm.DrDec(dr, c[21]),
                        TransferQty10 = Sm.DrDec(dr, c[22]),
                        TransferAmt11 = Sm.DrDec(dr, c[23]),
                        TransferQty11 = Sm.DrDec(dr, c[24]),
                        TransferAmt12 = Sm.DrDec(dr, c[25]),
                        TransferQty12 = Sm.DrDec(dr, c[26]),
                    });
                },
                false);

            var lx = l.GroupBy(g => new { g.AcNo, g.CCCode, g.ItCode })
                .Select(s => new BudgetTransferSingle()
                {
                    CCCode = s.Key.CCCode,
                    AcNo = s.Key.AcNo,
                    ItCode = s.Key.ItCode,
                    TransferAmt01 = s.Sum(t => t.TransferAmt01),
                    TransferQty01 = s.Sum(t => t.TransferQty01),
                    TransferAmt02 = s.Sum(t => t.TransferAmt02),
                    TransferQty02 = s.Sum(t => t.TransferQty02),
                    TransferAmt03 = s.Sum(t => t.TransferAmt03),
                    TransferQty03 = s.Sum(t => t.TransferQty03),
                    TransferAmt04 = s.Sum(t => t.TransferAmt04),
                    TransferQty04 = s.Sum(t => t.TransferQty04),
                    TransferAmt05 = s.Sum(t => t.TransferAmt05),
                    TransferQty05 = s.Sum(t => t.TransferQty05),
                    TransferAmt06 = s.Sum(t => t.TransferAmt06),
                    TransferQty06 = s.Sum(t => t.TransferQty06),
                    TransferAmt07 = s.Sum(t => t.TransferAmt07),
                    TransferQty07 = s.Sum(t => t.TransferQty07),
                    TransferAmt08 = s.Sum(t => t.TransferAmt08),
                    TransferQty08 = s.Sum(t => t.TransferQty08),
                    TransferAmt09 = s.Sum(t => t.TransferAmt09),
                    TransferQty09 = s.Sum(t => t.TransferQty09),
                    TransferAmt10 = s.Sum(t => t.TransferAmt10),
                    TransferQty10 = s.Sum(t => t.TransferQty10),
                    TransferAmt11 = s.Sum(t => t.TransferAmt11),
                    TransferQty11 = s.Sum(t => t.TransferQty11),
                    TransferAmt12 = s.Sum(t => t.TransferAmt12),
                    TransferQty12 = s.Sum(t => t.TransferQty12),
                }).ToList();

            return lx;
        }

        private List<BudgetReceiveSingle> PrepBudgetReceive(string Yr, string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            for (int i = 1; i <= 12; i++)
            {
                if (i != 1)
                    SQL.AppendLine("            Union All ");

                SQL.AppendLine("            SELECT C.AcNo, A.CCCode2 As CCCode, B.ItCode2 As ItCode, ");
                for (int j = 1; j <= 12; ++j)
                {
                    if (j == i) SQL.AppendLine("            B.TransferredAmt As ReceiveAmt" + Sm.Right(string.Concat('0', j), 2) + ", B.Qty As ReceiveQty" + Sm.Right(string.Concat('0', j), 2) + " ");
                    else SQL.AppendLine("          0.00 As ReceiveAmt" + Sm.Right(string.Concat('0', j), 2) + " , 0.00 As ReceiveQty" + Sm.Right(string.Concat('0', j), 2) + " ");

                    if (j != 12) SQL.AppendLine("           , ");
                }
                SQL.AppendLine("            FROM TblBudgetTransferCostCenterHdr A ");
                SQL.AppendLine("            INNER JOIN TblBudgetTransferCostCenterDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
                SQL.AppendLine("            And A.Yr = @Yr ");
                SQL.AppendLine("            AND A.Mth2 = Right(Concat(0, " + i + "), 2) ");
                if (CCCode.Length > 0)
                    SQL.AppendLine("        And Find_In_Set(A.CCCode2, @CCCode) ");
                else
                    SQL.AppendLine("        And 1=0 ");
                SQL.AppendLine("            INNER JOIN TblCostCategory C ON A.CCCode2 = C.CCCode  ");
                SQL.AppendLine("            INNER JOIN TblBudgetCategory D ON B.BCCode = D.BCCode ");
            }

            var l = new List<BudgetReceiveSingle>();

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(),
                new string[]
                {
                    "AcNo",
                    "CCCode", "ItCode", "ReceiveAmt01", "ReceiveQty01", "ReceiveAmt02",
                    "ReceiveQty02", "ReceiveAmt03", "ReceiveQty03", "ReceiveAmt04", "ReceiveQty04",
                    "ReceiveAmt05", "ReceiveQty05", "ReceiveAmt06", "ReceiveQty06", "ReceiveAmt07",
                    "ReceiveQty07", "ReceiveAmt08", "ReceiveQty08", "ReceiveAmt09", "ReceiveQty09",
                    "ReceiveAmt10", "ReceiveQty10", "ReceiveAmt11", "ReceiveQty11", "ReceiveAmt12",
                    "ReceiveQty12"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    l.Add(new BudgetReceiveSingle()
                    {
                        AcNo = Sm.DrStr(dr, c[0]),
                        CCCode = Sm.DrStr(dr, c[1]),
                        ItCode = Sm.DrStr(dr, c[2]),
                        ReceiveAmt01 = Sm.DrDec(dr, c[3]),
                        ReceiveQty01 = Sm.DrDec(dr, c[4]),
                        ReceiveAmt02 = Sm.DrDec(dr, c[5]),
                        ReceiveQty02 = Sm.DrDec(dr, c[6]),
                        ReceiveAmt03 = Sm.DrDec(dr, c[7]),
                        ReceiveQty03 = Sm.DrDec(dr, c[8]),
                        ReceiveAmt04 = Sm.DrDec(dr, c[9]),
                        ReceiveQty04 = Sm.DrDec(dr, c[10]),
                        ReceiveAmt05 = Sm.DrDec(dr, c[11]),
                        ReceiveQty05 = Sm.DrDec(dr, c[12]),
                        ReceiveAmt06 = Sm.DrDec(dr, c[13]),
                        ReceiveQty06 = Sm.DrDec(dr, c[14]),
                        ReceiveAmt07 = Sm.DrDec(dr, c[15]),
                        ReceiveQty07 = Sm.DrDec(dr, c[16]),
                        ReceiveAmt08 = Sm.DrDec(dr, c[17]),
                        ReceiveQty08 = Sm.DrDec(dr, c[18]),
                        ReceiveAmt09 = Sm.DrDec(dr, c[19]),
                        ReceiveQty09 = Sm.DrDec(dr, c[20]),
                        ReceiveAmt10 = Sm.DrDec(dr, c[21]),
                        ReceiveQty10 = Sm.DrDec(dr, c[22]),
                        ReceiveAmt11 = Sm.DrDec(dr, c[23]),
                        ReceiveQty11 = Sm.DrDec(dr, c[24]),
                        ReceiveAmt12 = Sm.DrDec(dr, c[25]),
                        ReceiveQty12 = Sm.DrDec(dr, c[26]),
                    });
                },
                false);

            var lx = l.GroupBy(g => new { g.AcNo, g.CCCode, g.ItCode })
                .Select(s => new BudgetReceiveSingle()
                {
                    CCCode = s.Key.CCCode,
                    AcNo = s.Key.AcNo,
                    ItCode = s.Key.ItCode,
                    ReceiveAmt01 = s.Sum(t => t.ReceiveAmt01),
                    ReceiveQty01 = s.Sum(t => t.ReceiveQty01),
                    ReceiveAmt02 = s.Sum(t => t.ReceiveAmt02),
                    ReceiveQty02 = s.Sum(t => t.ReceiveQty02),
                    ReceiveAmt03 = s.Sum(t => t.ReceiveAmt03),
                    ReceiveQty03 = s.Sum(t => t.ReceiveQty03),
                    ReceiveAmt04 = s.Sum(t => t.ReceiveAmt04),
                    ReceiveQty04 = s.Sum(t => t.ReceiveQty04),
                    ReceiveAmt05 = s.Sum(t => t.ReceiveAmt05),
                    ReceiveQty05 = s.Sum(t => t.ReceiveQty05),
                    ReceiveAmt06 = s.Sum(t => t.ReceiveAmt06),
                    ReceiveQty06 = s.Sum(t => t.ReceiveQty06),
                    ReceiveAmt07 = s.Sum(t => t.ReceiveAmt07),
                    ReceiveQty07 = s.Sum(t => t.ReceiveQty07),
                    ReceiveAmt08 = s.Sum(t => t.ReceiveAmt08),
                    ReceiveQty08 = s.Sum(t => t.ReceiveQty08),
                    ReceiveAmt09 = s.Sum(t => t.ReceiveAmt09),
                    ReceiveQty09 = s.Sum(t => t.ReceiveQty09),
                    ReceiveAmt10 = s.Sum(t => t.ReceiveAmt10),
                    ReceiveQty10 = s.Sum(t => t.ReceiveQty10),
                    ReceiveAmt11 = s.Sum(t => t.ReceiveAmt11),
                    ReceiveQty11 = s.Sum(t => t.ReceiveQty11),
                    ReceiveAmt12 = s.Sum(t => t.ReceiveAmt12),
                    ReceiveQty12 = s.Sum(t => t.ReceiveQty12),
                }).ToList();

            return lx;
        }

        private void PrepDataCAS(ref List<CAS> l2, string Yr, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, Substring(A.DocDt, 5, 2) Mth, B.ItCode, C.AcNo, Sum(B.Qty) Qty, Sum(B.Rate) Rate ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo And B.ItCode Is Not Null ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("    And A.CCCode Is Not Null ");
            //if (!CCCode.Contains("Consolidate"))
            //    SQL.AppendLine("    And Find_In_Set(A.CCCode, @CCCode) ");
            if (CCCode.Length > 0)
                SQL.AppendLine("    And Find_In_Set(A.CCCode, @CCCode) ");
            else
                SQL.AppendLine("    And 1=0 ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblCostCategory C On B.CCtCode = C.CCtCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCode=D.CCCode ");
            SQL.AppendLine("Group By A.CCCode, Substring(A.DocDt, 5, 2), B.ItCode, C.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                //if (!CCCode.Contains("Consolidate")) Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                if (CCCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCode", "Mth", "ItCode", "AcNo", "Qty", "Rate" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        l2.Add(new CAS()
                        {
                            CCCode = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            AcNo = Sm.DrStr(dr, c[3]),
                            Qty = Sm.DrDec(dr, c[4]),
                            Rate = Sm.DrDec(dr, c[5]),
                            IsTaken = false,
                        });
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void PrepDataRecvVdAutoDO(ref List<RecvVdAutoDO> l5, string Yr, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.CCCode, Substring(A.DocDt, 5, 2) Mth, B.ItCode, E.AcNo, B.Qty,  ");
            SQL.AppendLine("If(A.POInd = 'Y', H.UPrice, B.UPrice) Rate ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("    And B.Status = 'A' ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblDODeptHdr C On A.DocNo = C.RecvVdDocNo And C.RecvVdDocNo Is Not Null ");
            SQL.AppendLine("    And C.CCCode Is Not Null ");
            if (CCCode.Length > 0)
                SQL.AppendLine("    And Find_In_Set(C.CCCode, @CCCode) ");
            else
                SQL.AppendLine("    And 1=0 ");
            SQL.AppendLine("Inner Join TblItemCostCategory D On B.ItCode = D.ItCode And C.CCCode = D.CCCode ");
            SQL.AppendLine("Inner Join TblCostCategory E On D.CCtCode = E.CCtCode And E.AcNo Is Not Null ");
            //SQL.AppendLine("Inner Join TblCostCenter F On C.CCCode = F.CCCode ");
            //SQL.AppendLine("Left Join TblJournalHdr G On A.JournalDocNo = G.DocNo ");
            //SQL.AppendLine("Left Join TblJournalHdr G2 On B.JournalDocNo = G2.DocNo ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, T3.UPrice ");
            SQL.AppendLine("    From TblPODtl T1 ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo= T2.DocNo And T1.PORequestDNo= T2.DNo And T2.Status = 'A' And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And Concat(T1.DocNo, T1.DNo) In ( ");
            SQL.AppendLine("            Select Distinct Concat(X.PODocNo, X.PODNo) ");
            SQL.AppendLine("            From TblRecvVdDtl X ");
            SQL.AppendLine("            Inner Join TblRecvVdHdr X1 On X.DocNo = X1.DocNo ");
            SQL.AppendLine("                And Left(X1.DocDt, 4) = @Yr ");
            SQL.AppendLine("                And X.Status = 'A' ");
            SQL.AppendLine("                And X.CancelInd = 'N' ");
            SQL.AppendLine("            Inner Join TblDODeptHdr X2 On X1.DocNo = X2.RecvVdDocNo And X2.RecvVdDocNo Is Not Null ");
            SQL.AppendLine("                And X2.CCCode Is Not Null ");
            if (CCCode.Length > 0)
                SQL.AppendLine("                And Find_In_Set(X2.CCCode, @CCCode) ");
            else
                SQL.AppendLine("                And 1=0 ");

            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblQtDtl T3 On T2.QtDocNo= T3.DocNo And T2.QtDNo= T3.DNo ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine(") H On B.PODocNo = H.DocNo And B.PODNo = H.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                //if (!CCCode.Contains("Consolidate")) Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                if (CCCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCode", "Mth", "ItCode", "AcNo", "Qty", "Rate" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        l5.Add(new RecvVdAutoDO()
                        {
                            CCCode = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            AcNo = Sm.DrStr(dr, c[3]),
                            Qty = Sm.DrDec(dr, c[4]),
                            Rate = Sm.DrDec(dr, c[5]),
                            IsTaken = false,
                        });
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void PrepJournalDataTemp(ref List<COAJournal> l4, string Yr, string CCCode, string SelectedCOA)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT A.AcNo, C.AcDesc, C.AcType, C.Parent, C.Level, B.CCCode, Substring(B.DocDt, 5, 2) Mth, ");
            //SQL.AppendLine("Case When C.AcType = 'D' Then Sum(A.DAmt - A.CAmt) When C.AcType = 'C' Then Sum(A.CAmt- A.DAmt) End As Amt ");
            //SQL.AppendLine("FROM TblJournalDtl A ");
            //SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    AND LEFT(B.DocDt, 4) = @Yr ");
            //SQL.AppendLine("    And B.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
            //SQL.AppendLine("    And B.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58') ");
            //SQL.AppendLine("    And B.CCCode Is Not Null ");
            //if (!CCCode.Contains("Consolidate"))
            //    SQL.AppendLine("    And Find_In_Set(B.CCCode, @CCCode) ");
            //SQL.AppendLine("INNER JOIN TblCOA C On A.AcNo = C.AcNo ");
            //SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            //SQL.AppendLine("Group By B.CCCode, Substring(B.DocDt, 5, 2), A.AcNo; ");

            SQL.AppendLine("Select X1.AcNo, X2.AcDesc, X2.AcType, X2.Parent, X2.Level, ");
            SQL.AppendLine("X1.CCCode, X1.Mth, X1.Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select B.AcNo, A.CCCode, Substring(A.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("    Sum(Case ");
            SQL.AppendLine("        When C.AcType = 'D' Then B.DAmt-B.CAmt ");
            SQL.AppendLine("        When C.AcType = 'C' Then B.CAmt-B.DAmt ");
            SQL.AppendLine("    End) As Amt ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Find_In_Set(B.AcNo, @SelectedCOA) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    Where Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("    And A.MenuCode Not In (Select Menucode From TblMenu Where Param In ('FrmCashAdvanceSettlement') ) ");
            SQL.AppendLine("    And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58') ");
            SQL.AppendLine("    And A.DocNo Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.JournalDocNo ");
            SQL.AppendLine("        From TblRecvVdHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null And A.JournalDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A2.JournalDocNo ");
            SQL.AppendLine("        From TblRecvVdHdr A ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl A2 On A.DocNo = A2.DocNo ");
            SQL.AppendLine("        Inner Join TblDODeptHdr B On A.DocNo = B.RecvVdDocNo And B.RecvVdDocNo Is Not Null And A2.JournalDocNo Is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.CCCode Is Not Null ");
            //if (!CCCode.Contains("Consolidate"))
            if (CCCode.Length > 0)
                SQL.AppendLine("    And Find_In_Set(A.CCCode, @CCCode) ");
            else
                SQL.AppendLine("    And 1=0 ");
            SQL.AppendLine("    Group By B.AcNo, A.CCCode, Substring(A.DocDt, 5, 2) ");
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("Inner Join TblCOA X2 On X1.AcNo=X2.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                //if (!CCCode.Contains("Consolidate")) 
                if (CCCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    "AcNo",
                    "AcDesc", "Actype", "Parent", "Level" ,"CCCode",
                    "Mth", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrInt(dr, c[4]),
                            CCCode = Sm.DrStr(dr, c[5]),
                            Mth = Sm.DrStr(dr, c[6]),
                            Amt = Sm.DrDec(dr, c[7]),
                            ItCode = "Misc",
                            ItName = "Misc"
                        });
                    }
                }
                dr.Close();
            }
        }

        //private void GetCASDataToCBP(ref CBP x, ref List<CAS> l2)
        //{
            
        //    foreach (var y in l2.Where(w =>
        //        x.AcNo == w.AcNo &&
        //        x.ItCode == w.ItCode &&
        //        x.CCCode == w.CCCode &&
        //        !w.IsTaken
        //        ))
        //    {
        //        if (y.Mth == "01")
        //        {
        //            x.QtyCAS01 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS01 = y.Rate * y.Qty;
        //            else x.AmtCAS01 = x.Rate01 * y.Qty;
        //            x.QtyCASTo01 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCASTo01 = y.Rate * y.Qty;
        //            else x.AmtCASTo01 = x.Rate01 * y.Qty;
        //        }

        //        if (y.Mth == "02")
        //        {
        //            x.QtyCAS02 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS02 = y.Rate * y.Qty;
        //            else x.AmtCAS02 = x.Rate02 * y.Qty;
        //        }

        //        if (y.Mth == "03")
        //        {
        //            x.QtyCAS03 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS03 = y.Rate * y.Qty;
        //            else x.AmtCAS03 = x.Rate03 * y.Qty;
        //        }

        //        if (y.Mth == "04")
        //        {
        //            x.QtyCAS04 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS04 = y.Rate * y.Qty;
        //            else x.AmtCAS04 = x.Rate04 * y.Qty;
        //        }

        //        if (y.Mth == "05")
        //        {
        //            x.QtyCAS05 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS05 = y.Rate * y.Qty;
        //            else x.AmtCAS05 = x.Rate05 * y.Qty;
        //        }

        //        if (y.Mth == "06")
        //        {
        //            x.QtyCAS06 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS06 = y.Rate * y.Qty;
        //            else x.AmtCAS06 = x.Rate06 * y.Qty;
        //        }

        //        if (y.Mth == "07")
        //        {
        //            x.QtyCAS07 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS07 = y.Rate * y.Qty;
        //            else x.AmtCAS07 = x.Rate07 * y.Qty;
        //        }

        //        if (y.Mth == "08")
        //        {
        //            x.QtyCAS08 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS08 = y.Rate * y.Qty;
        //            else x.AmtCAS08 = x.Rate08 * y.Qty;
        //        }

        //        if (y.Mth == "09")
        //        {
        //            x.QtyCAS09 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS09 = y.Rate * y.Qty;
        //            else x.AmtCAS09 = x.Rate09 * y.Qty;
        //        }

        //        if (y.Mth == "10")
        //        {
        //            x.QtyCAS10 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS10 = y.Rate * y.Qty;
        //            else x.AmtCAS10 = x.Rate10 * y.Qty;
        //        }

        //        if (y.Mth == "11")
        //        {
        //            x.QtyCAS11 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS11 = y.Rate * y.Qty;
        //            else x.AmtCAS11 = x.Rate11 * y.Qty;
        //        }

        //        if (y.Mth == "12")
        //        {
        //            x.QtyCAS12 = y.Qty;
        //            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS12 = y.Rate * y.Qty;
        //            else x.AmtCAS12 = x.Rate12 * y.Qty;
        //        }

        //        y.IsTaken = true;
        //    }
        //}

        //private void GetRecvVdAutoDODataToCBP(ref CBP x, ref List<RecvVdAutoDO> l5)
        //{
        //    foreach (var y in l5.Where(w =>
        //        x.AcNo == w.AcNo &&
        //        x.ItCode == w.ItCode &&
        //        x.CCCode == w.CCCode &&
        //        !w.IsTaken
        //        ))
        //    {
        //        if (y.Mth == "01")
        //        {
        //            x.QtyCAS01 += y.Qty;
        //            x.AmtCAS01 += y.Rate * y.Qty;
        //            x.QtyCASTo01 += y.Qty;
        //            x.AmtCASTo01 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "02")
        //        {
        //            x.QtyCAS02 += y.Qty;
        //            x.AmtCAS02 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "03")
        //        {
        //            x.QtyCAS03 += y.Qty;
        //            x.AmtCAS03 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "04")
        //        {
        //            x.QtyCAS04 += y.Qty;
        //            x.AmtCAS04 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "05")
        //        {
        //            x.QtyCAS05 += y.Qty;
        //            x.AmtCAS05 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "06")
        //        {
        //            x.QtyCAS06 += y.Qty;
        //            x.AmtCAS06 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "07")
        //        {
        //            x.QtyCAS07 += y.Qty;
        //            x.AmtCAS07 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "08")
        //        {
        //            x.QtyCAS08 += y.Qty;
        //            x.AmtCAS08 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "09")
        //        {
        //            x.QtyCAS09 += y.Qty;
        //            x.AmtCAS09 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "10")
        //        {
        //            x.QtyCAS10 += y.Qty;
        //            x.AmtCAS10 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "11")
        //        {
        //            x.QtyCAS11 += y.Qty;
        //            x.AmtCAS11 += y.Rate * y.Qty;
        //        }

        //        if (y.Mth == "12")
        //        {
        //            x.QtyCAS12 += y.Qty;
        //            x.AmtCAS12 += y.Rate * y.Qty;
        //        }

        //        y.IsTaken = true;
        //    }
        //}

        private void ProcessOtherMisc(ref CBP x, ref List<CAS> l2, ref List<RecvVdAutoDO> l5)
        {
            foreach (var y in l2.Where(w => w.IsTaken == false))
            {
                if (x.ItCode.ToLower() == "misc" &&
                    x.AcNo == y.AcNo &&
                    x.CCCode == y.CCCode)
                {
                    if (y.Mth == "01")
                    {
                        x.QtyCAS01 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS01 += y.Rate * y.Qty;
                        else x.AmtCAS01 += x.Rate01 * y.Qty;
                        x.QtyCASTo01 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCASTo01 += y.Rate * y.Qty;
                        else x.AmtCASTo01 += x.Rate01 * y.Qty;
                    }

                    if (y.Mth == "02")
                    {
                        x.QtyCAS02 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS02 += y.Rate * y.Qty;
                        else x.AmtCAS02 += x.Rate02 * y.Qty;
                    }

                    if (y.Mth == "03")
                    {
                        x.QtyCAS03 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS03 += y.Rate * y.Qty;
                        else x.AmtCAS03 += x.Rate03 * y.Qty;
                    }

                    if (y.Mth == "04")
                    {
                        x.QtyCAS04 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS04 += y.Rate * y.Qty;
                        else x.AmtCAS04 += x.Rate04 * y.Qty;
                    }

                    if (y.Mth == "05")
                    {
                        x.QtyCAS05 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS05 += y.Rate * y.Qty;
                        else x.AmtCAS05 += x.Rate05 * y.Qty;
                    }

                    if (y.Mth == "06")
                    {
                        x.QtyCAS06 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS06 += y.Rate * y.Qty;
                        else x.AmtCAS06 += x.Rate06 * y.Qty;
                    }

                    if (y.Mth == "07")
                    {
                        x.QtyCAS07 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS07 += y.Rate * y.Qty;
                        else x.AmtCAS07 += x.Rate07 * y.Qty;
                    }

                    if (y.Mth == "08")
                    {
                        x.QtyCAS08 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS08 += y.Rate * y.Qty;
                        else x.AmtCAS08 += x.Rate08 * y.Qty;
                    }

                    if (y.Mth == "09")
                    {
                        x.QtyCAS09 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS09 += y.Rate * y.Qty;
                        else x.AmtCAS09 += x.Rate09 * y.Qty;
                    }

                    if (y.Mth == "10")
                    {
                        x.QtyCAS10 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS10 += y.Rate * y.Qty;
                        else x.AmtCAS10 += x.Rate10 * y.Qty;
                    }

                    if (y.Mth == "11")
                    {
                        x.QtyCAS11 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS11 += y.Rate * y.Qty;
                        else x.AmtCAS11 += x.Rate11 * y.Qty;
                    }

                    if (y.Mth == "12")
                    {
                        x.QtyCAS12 += y.Qty;
                        if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS12 += y.Rate * y.Qty;
                        else x.AmtCAS12 += x.Rate12 * y.Qty;
                    }

                    y.IsTaken = true;
                }
            }

            foreach (var y in l5.Where(w => w.IsTaken == false))
            {
                if (x.ItCode.ToLower() == "misc" &&
                    x.AcNo == y.AcNo &&
                    x.CCCode == y.CCCode)
                {
                    if (y.Mth == "01")
                    {
                        x.QtyCAS01 += y.Qty;
                        x.AmtCAS01 += y.Rate * y.Qty;
                        x.QtyCASTo01 += y.Qty;
                        x.AmtCASTo01 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "02")
                    {
                        x.QtyCAS02 += y.Qty;
                        x.AmtCAS02 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "03")
                    {
                        x.QtyCAS03 += y.Qty;
                        x.AmtCAS03 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "04")
                    {
                        x.QtyCAS04 += y.Qty;
                        x.AmtCAS04 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "05")
                    {
                        x.QtyCAS05 += y.Qty;
                        x.AmtCAS05 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "06")
                    {
                        x.QtyCAS06 += y.Qty;
                        x.AmtCAS06 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "07")
                    {
                        x.QtyCAS07 += y.Qty;
                        x.AmtCAS07 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "08")
                    {
                        x.QtyCAS08 += y.Qty;
                        x.AmtCAS08 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "09")
                    {
                        x.QtyCAS09 += y.Qty;
                        x.AmtCAS09 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "10")
                    {
                        x.QtyCAS10 += y.Qty;
                        x.AmtCAS10 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "11")
                    {
                        x.QtyCAS11 += y.Qty;
                        x.AmtCAS11 += y.Rate * y.Qty;
                    }

                    if (y.Mth == "12")
                    {
                        x.QtyCAS12 += y.Qty;
                        x.AmtCAS12 += y.Rate * y.Qty;
                    }

                    y.IsTaken = true;
                }
            }
        }

        private void ProcessOtherMisc2(ref List<CBP> l, ref List<CAS> l2, ref List<RecvVdAutoDO> l5)
        {
            foreach (var y in l2.Where(w => w.IsTaken == false))
            {
                l.Add(new CBP()
                {
                    AcDesc = Sm.GetValue("Select AcDesc From TblCOA Where AcNo = @Param", y.AcNo),
                    AcNo = y.AcNo,
                    DeptGrpName = string.Empty,
                    ItCode = "Misc",
                    ItName = "Misc",
                    UoM = string.Empty,
                    BudgetType = string.Empty,

                    Rate = 1m,
                    QtyCBP = 0m,
                    AmtCBP = 0m,
                    QtyCAS = 0m,
                    AmtCAS = 0m,
                    QtyRemain = 0m,
                    AmtRemain = 0m,

                    // 01
                    Rate01 = 1m,
                    QtyCBP01 = 0m,
                    AmtCBP01 = 0m,
                    QtyCBPTo01 = 0m,
                    AmtCBPTo01 = 0m,
                    QtyCAS01 = (y.Mth == "01" ? y.Qty : 0m),
                    AmtCAS01 = (y.Mth == "01" ? y.Rate * y.Qty : 0m),
                    QtyCASTo01 = (y.Mth == "01" ? y.Qty : 0m),
                    AmtCASTo01 = (y.Mth == "01" ? y.Rate * y.Qty : 0m),
                    QtyRemain01 = 0m,
                    AmtRemain01 = 0m,
                    QtyRemainTo01 = 0m,
                    AmtRemainTo01 = 0m,

                    //02
                    Rate02 = 1m,
                    QtyCBP02 = 0m,
                    AmtCBP02 = 0m,
                    QtyCBPTo02 = 0m,
                    AmtCBPTo02 = 0m,
                    QtyCAS02 = (y.Mth == "02" ? y.Qty : 0m),
                    AmtCAS02 = (y.Mth == "02" ? y.Rate * y.Qty : 0m),
                    QtyCASTo02 = 0m,
                    AmtCASTo02 = 0m,
                    QtyRemain02 = 0m,
                    AmtRemain02 = 0m,
                    QtyRemainTo02 = 0m,
                    AmtRemainTo02 = 0m,

                    //03
                    Rate03 = 1m,
                    QtyCBP03 = 0m,
                    AmtCBP03 = 0m,
                    QtyCBPTo03 = 0m,
                    AmtCBPTo03 = 0m,
                    QtyCAS03 = (y.Mth == "03" ? y.Qty : 0m),
                    AmtCAS03 = (y.Mth == "03" ? y.Rate * y.Qty : 0m),
                    QtyCASTo03 = 0m,
                    AmtCASTo03 = 0m,
                    QtyRemain03 = 0m,
                    AmtRemain03 = 0m,
                    QtyRemainTo03 = 0m,
                    AmtRemainTo03 = 0m,

                    //Q1
                    RateQ1 = 0m,
                    QtyCBPQ1 = 0m,
                    AmtCBPQ1 = 0m,
                    QtyCBPToQ1 = 0m,
                    AmtCBPToQ1 = 0m,
                    QtyCASQ1 = 0m,
                    AmtCASQ1 = 0m,
                    QtyCASToQ1 = 0m,
                    AmtCASToQ1 = 0m,
                    QtyRemainQ1 = 0m,
                    AmtRemainQ1 = 0m,
                    QtyRemainToQ1 = 0m,
                    AmtRemainToQ1 = 0m,

                    //04
                    Rate04 = 1m,
                    QtyCBP04 = 0m,
                    AmtCBP04 = 0m,
                    QtyCBPTo04 = 0m,
                    AmtCBPTo04 = 0m,
                    QtyCAS04 = (y.Mth == "04" ? y.Qty : 0m),
                    AmtCAS04 = (y.Mth == "04" ? y.Rate * y.Qty : 0m),
                    QtyCASTo04 = 0m,
                    AmtCASTo04 = 0m,
                    QtyRemain04 = 0m,
                    AmtRemain04 = 0m,
                    QtyRemainTo04 = 0m,
                    AmtRemainTo04 = 0m,

                    //05
                    Rate05 = 1m,
                    QtyCBP05 = 0m,
                    AmtCBP05 = 0m,
                    QtyCBPTo05 = 0m,
                    AmtCBPTo05 = 0m,
                    QtyCAS05 = y.Qty,
                    AmtCAS05 = (y.Mth == "05" ? y.Rate * y.Qty : 0m),
                    QtyCASTo05 = 0m,
                    AmtCASTo05 = 0m,
                    QtyRemain05 = 0m,
                    AmtRemain05 = 0m,
                    QtyRemainTo05 = 0m,
                    AmtRemainTo05 = 0m,

                    //06
                    Rate06 = 1m,
                    QtyCBP06 = 0m,
                    AmtCBP06 = 0m,
                    QtyCBPTo06 = 0m,
                    AmtCBPTo06 = 0m,
                    QtyCAS06 = (y.Mth == "05" ? y.Qty : 0m),
                    AmtCAS06 = (y.Mth == "06" ? y.Rate * y.Qty : 0m),
                    QtyCASTo06 = 0m,
                    AmtCASTo06 = 0m,
                    QtyRemain06 = 0m,
                    AmtRemain06 = 0m,
                    QtyRemainTo06 = 0m,
                    AmtRemainTo06 = 0m,

                    //Q2
                    RateQ2 = 0m,
                    QtyCBPQ2 = 0m,
                    AmtCBPQ2 = 0m,
                    QtyCBPToQ2 = 0m,
                    AmtCBPToQ2 = 0m,
                    QtyCASQ2 = 0m,
                    AmtCASQ2 = 0m,
                    QtyCASToQ2 = 0m,
                    AmtCASToQ2 = 0m,
                    QtyRemainQ2 = 0m,
                    AmtRemainQ2 = 0m,
                    QtyRemainToQ2 = 0m,
                    AmtRemainToQ2 = 0m,

                    //07
                    Rate07 = 1m,
                    QtyCBP07 = 0m,
                    AmtCBP07 = 0m,
                    QtyCBPTo07 = 0m,
                    AmtCBPTo07 = 0m,
                    QtyCAS07 = (y.Mth == "07" ? y.Qty : 0m),
                    AmtCAS07 = (y.Mth == "07" ? y.Rate * y.Qty : 0m),
                    QtyCASTo07 = 0m,
                    AmtCASTo07 = 0m,
                    QtyRemain07 = 0m,
                    AmtRemain07 = 0m,
                    QtyRemainTo07 = 0m,
                    AmtRemainTo07 = 0m,

                    //08
                    Rate08 = 1m,
                    QtyCBP08 = 0m,
                    AmtCBP08 = 0m,
                    AmtCBPTo08 = 0m,
                    QtyCAS08 = (y.Mth == "08" ? y.Qty : 0m),
                    AmtCAS08 = (y.Mth == "08" ? y.Rate * y.Qty : 0m),
                    QtyCASTo08 = 0m,
                    AmtCASTo08 = 0m,
                    QtyRemain08 = 0m,
                    AmtRemain08 = 0m,
                    QtyRemainTo08 = 0m,
                    AmtRemainTo08 = 0m,

                    //09
                    Rate09 = 1m,
                    QtyCBP09 = 0m,
                    AmtCBP09 = 0m,
                    QtyCBPTo09 = 0m,
                    AmtCBPTo09 = 0m,
                    QtyCAS09 = (y.Mth == "09" ? y.Qty : 0m),
                    AmtCAS09 = (y.Mth == "09" ? y.Rate * y.Qty : 0m),
                    QtyCASTo09 = 0m,
                    AmtCASTo09 = 0m,
                    QtyRemain09 = 0m,
                    AmtRemain09 = 0m,
                    QtyRemainTo09 = 0m,
                    AmtRemainTo09 = 0m,

                    //Q3
                    RateQ3 = 0m,
                    QtyCBPQ3 = 0m,
                    AmtCBPQ3 = 0m,
                    QtyCBPToQ3 = 0m,
                    AmtCBPToQ3 = 0m,
                    QtyCASQ3 = 0m,
                    AmtCASQ3 = 0m,
                    QtyCASToQ3 = 0m,
                    AmtCASToQ3 = 0m,
                    QtyRemainQ3 = 0m,
                    AmtRemainQ3 = 0m,
                    QtyRemainToQ3 = 0m,
                    AmtRemainToQ3 = 0m,


                    //10
                    Rate10 = 1m,
                    QtyCBP10 = 0m,
                    AmtCBP10 = 0m,
                    QtyCBPTo10 = 0m,
                    AmtCBPTo10 = 0m,
                    QtyCAS10 = (y.Mth == "10" ? y.Qty : 0m),
                    AmtCAS10 = (y.Mth == "10" ? y.Rate * y.Qty : 0m),
                    QtyCASTo10 = 0m,
                    AmtCASTo10 = 0m,
                    QtyRemain10 = 0m,
                    AmtRemain10 = 0m,
                    QtyRemainTo10 = 0m,
                    AmtRemainTo10 = 0m,

                    //11
                    Rate11 = 1m,
                    QtyCBP11 = 0m,
                    AmtCBP11 = 0m,
                    QtyCBPTo11 = 0m,
                    AmtCBPTo11 = 0m,
                    QtyCAS11 = (y.Mth == "11" ? y.Qty : 0m),
                    AmtCAS11 = (y.Mth == "11" ? y.Rate * y.Qty : 0m),
                    QtyCASTo11 = 0m,
                    AmtCASTo11 = 0m,
                    QtyRemain11 = 0m,
                    AmtRemain11 = 0m,
                    QtyRemainTo11 = 0m,
                    AmtRemainTo11 = 0m,

                    //12
                    Rate12 = 1m,
                    QtyCBP12 = 0m,
                    AmtCBP12 = 0m,
                    QtyCBPTo12 = 0m,
                    AmtCBPTo12 = 0m,
                    QtyCAS12 = (y.Mth == "12" ? y.Qty : 0m),
                    AmtCAS12 = (y.Mth == "12" ? y.Rate * y.Qty : 0m),
                    QtyCASTo12 = 0m,
                    AmtCASTo12 = 0m,
                    QtyRemain12 = 0m,
                    AmtRemain12 = 0m,
                    QtyRemainTo12 = 0m,
                    AmtRemainTo12 = 0m,

                    //Q4
                    RateQ4 = 0m,
                    QtyCBPQ4 = 0m,
                    AmtCBPQ4 = 0m,
                    QtyCBPToQ4 = 0m,
                    AmtCBPToQ4 = 0m,
                    QtyCASQ4 = 0m,
                    AmtCASQ4 = 0m,
                    QtyCASToQ4 = 0m,
                    AmtCASToQ4 = 0m,
                    QtyRemainQ4 = 0m,
                    AmtRemainQ4 = 0m,
                    QtyRemainToQ4 = 0m,
                    AmtRemainToQ4 = 0m,

                    CCCode = y.CCCode,
                    Parent = Sm.GetValue("Select Parent From TblCOA Where AcNo = @Param", y.AcNo),
                    Level = Int32.Parse(Sm.GetValue("Select Level From TblCOA Where AcNo = @Param", y.AcNo)),
                });
            }

            foreach (var y in l5.Where(w => w.IsTaken == false))
            {
                l.Add(new CBP()
                {
                    AcDesc = Sm.GetValue("Select AcDesc From TblCOA Where AcNo = @Param", y.AcNo),
                    AcNo = y.AcNo,
                    DeptGrpName = string.Empty,
                    ItCode = "Misc",
                    ItName = "Misc",
                    UoM = string.Empty,
                    BudgetType = string.Empty,

                    Rate = 1m,
                    QtyCBP = 0m,
                    AmtCBP = 0m,
                    QtyCAS = 0m,
                    AmtCAS = 0m,
                    QtyRemain = 0m,
                    AmtRemain = 0m,

                    // 01
                    Rate01 = 1m,
                    QtyCBP01 = 0m,
                    AmtCBP01 = 0m,
                    QtyCBPTo01 = 0m,
                    AmtCBPTo01 = 0m,
                    QtyCAS01 = (y.Mth == "01" ? y.Qty : 0m),
                    AmtCAS01 = (y.Mth == "01" ? y.Rate * y.Qty : 0m),
                    QtyCASTo01 = (y.Mth == "01" ? y.Qty : 0m),
                    AmtCASTo01 = (y.Mth == "01" ? y.Rate * y.Qty : 0m),
                    QtyRemain01 = 0m,
                    AmtRemain01 = 0m,
                    QtyRemainTo01 = 0m,
                    AmtRemainTo01 = 0m,

                    //02
                    Rate02 = 1m,
                    QtyCBP02 = 0m,
                    AmtCBP02 = 0m,
                    QtyCBPTo02 = 0m,
                    AmtCBPTo02 = 0m,
                    QtyCAS02 = (y.Mth == "02" ? y.Qty : 0m),
                    AmtCAS02 = (y.Mth == "02" ? y.Rate * y.Qty : 0m),
                    QtyCASTo02 = 0m,
                    AmtCASTo02 = 0m,
                    QtyRemain02 = 0m,
                    AmtRemain02 = 0m,
                    QtyRemainTo02 = 0m,
                    AmtRemainTo02 = 0m,

                    //03
                    Rate03 = 1m,
                    QtyCBP03 = 0m,
                    AmtCBP03 = 0m,
                    QtyCBPTo03 = 0m,
                    AmtCBPTo03 = 0m,
                    QtyCAS03 = (y.Mth == "03" ? y.Qty : 0m),
                    AmtCAS03 = (y.Mth == "03" ? y.Rate * y.Qty : 0m),
                    QtyCASTo03 = 0m,
                    AmtCASTo03 = 0m,
                    QtyRemain03 = 0m,
                    AmtRemain03 = 0m,
                    QtyRemainTo03 = 0m,
                    AmtRemainTo03 = 0m,

                    //Q1
                    RateQ1 = 0m,
                    QtyCBPQ1 = 0m,
                    AmtCBPQ1 = 0m,
                    QtyCBPToQ1 = 0m,
                    AmtCBPToQ1 = 0m,
                    QtyCASQ1 = 0m,
                    AmtCASQ1 = 0m,
                    QtyCASToQ1 = 0m,
                    AmtCASToQ1 = 0m,
                    QtyRemainQ1 = 0m,
                    AmtRemainQ1 = 0m,
                    QtyRemainToQ1 = 0m,
                    AmtRemainToQ1 = 0m,

                    //04
                    Rate04 = 1m,
                    QtyCBP04 = 0m,
                    AmtCBP04 = 0m,
                    QtyCBPTo04 = 0m,
                    AmtCBPTo04 = 0m,
                    QtyCAS04 = (y.Mth == "04" ? y.Qty : 0m),
                    AmtCAS04 = (y.Mth == "04" ? y.Rate * y.Qty : 0m),
                    QtyCASTo04 = 0m,
                    AmtCASTo04 = 0m,
                    QtyRemain04 = 0m,
                    AmtRemain04 = 0m,
                    QtyRemainTo04 = 0m,
                    AmtRemainTo04 = 0m,

                    //05
                    Rate05 = 1m,
                    QtyCBP05 = 0m,
                    AmtCBP05 = 0m,
                    QtyCBPTo05 = 0m,
                    AmtCBPTo05 = 0m,
                    QtyCAS05 = (y.Mth == "05" ? y.Qty : 0m),
                    AmtCAS05 = (y.Mth == "05" ? y.Rate * y.Qty : 0m),
                    QtyCASTo05 = 0m,
                    AmtCASTo05 = 0m,
                    QtyRemain05 = 0m,
                    AmtRemain05 = 0m,
                    QtyRemainTo05 = 0m,
                    AmtRemainTo05 = 0m,

                    //06
                    Rate06 = 1m,
                    QtyCBP06 = 0m,
                    AmtCBP06 = 0m,
                    QtyCBPTo06 = 0m,
                    AmtCBPTo06 = 0m,
                    QtyCAS06 = (y.Mth == "06" ? y.Qty : 0m),
                    AmtCAS06 = (y.Mth == "06" ? y.Rate * y.Qty : 0m),
                    QtyCASTo06 = 0m,
                    AmtCASTo06 = 0m,
                    QtyRemain06 = 0m,
                    AmtRemain06 = 0m,
                    QtyRemainTo06 = 0m,
                    AmtRemainTo06 = 0m,

                    //Q2
                    RateQ2 = 0m,
                    QtyCBPQ2 = 0m,
                    AmtCBPQ2 = 0m,
                    QtyCBPToQ2 = 0m,
                    AmtCBPToQ2 = 0m,
                    QtyCASQ2 = 0m,
                    AmtCASQ2 = 0m,
                    QtyCASToQ2 = 0m,
                    AmtCASToQ2 = 0m,
                    QtyRemainQ2 = 0m,
                    AmtRemainQ2 = 0m,
                    QtyRemainToQ2 = 0m,
                    AmtRemainToQ2 = 0m,

                    //07
                    Rate07 = 1m,
                    QtyCBP07 = 0m,
                    AmtCBP07 = 0m,
                    QtyCBPTo07 = 0m,
                    AmtCBPTo07 = 0m,
                    QtyCAS07 = (y.Mth == "07" ? y.Qty : 0m),
                    AmtCAS07 = (y.Mth == "07" ? y.Rate * y.Qty : 0m),
                    QtyCASTo07 = 0m,
                    AmtCASTo07 = 0m,
                    QtyRemain07 = 0m,
                    AmtRemain07 = 0m,
                    QtyRemainTo07 = 0m,
                    AmtRemainTo07 = 0m,

                    //08
                    Rate08 = 1m,
                    QtyCBP08 = 0m,
                    AmtCBP08 = 0m,
                    AmtCBPTo08 = 0m,
                    QtyCAS08 = (y.Mth == "08" ? y.Qty : 0m),
                    AmtCAS08 = (y.Mth == "08" ? y.Rate * y.Qty : 0m),
                    QtyCASTo08 = 0m,
                    AmtCASTo08 = 0m,
                    QtyRemain08 = 0m,
                    AmtRemain08 = 0m,
                    QtyRemainTo08 = 0m,
                    AmtRemainTo08 = 0m,

                    //09
                    Rate09 = 1m,
                    QtyCBP09 = 0m,
                    AmtCBP09 = 0m,
                    QtyCBPTo09 = 0m,
                    AmtCBPTo09 = 0m,
                    QtyCAS09 = (y.Mth == "09" ? y.Qty : 0m),
                    AmtCAS09 = (y.Mth == "09" ? y.Rate * y.Qty : 0m),
                    QtyCASTo09 = 0m,
                    AmtCASTo09 = 0m,
                    QtyRemain09 = 0m,
                    AmtRemain09 = 0m,
                    QtyRemainTo09 = 0m,
                    AmtRemainTo09 = 0m,

                    //Q3
                    RateQ3 = 0m,
                    QtyCBPQ3 = 0m,
                    AmtCBPQ3 = 0m,
                    QtyCBPToQ3 = 0m,
                    AmtCBPToQ3 = 0m,
                    QtyCASQ3 = 0m,
                    AmtCASQ3 = 0m,
                    QtyCASToQ3 = 0m,
                    AmtCASToQ3 = 0m,
                    QtyRemainQ3 = 0m,
                    AmtRemainQ3 = 0m,
                    QtyRemainToQ3 = 0m,
                    AmtRemainToQ3 = 0m,


                    //10
                    Rate10 = 1m,
                    QtyCBP10 = 0m,
                    AmtCBP10 = 0m,
                    QtyCBPTo10 = 0m,
                    AmtCBPTo10 = 0m,
                    QtyCAS10 = (y.Mth == "10" ? y.Qty : 0m),
                    AmtCAS10 = (y.Mth == "10" ? y.Rate * y.Qty : 0m),
                    QtyCASTo10 = 0m,
                    AmtCASTo10 = 0m,
                    QtyRemain10 = 0m,
                    AmtRemain10 = 0m,
                    QtyRemainTo10 = 0m,
                    AmtRemainTo10 = 0m,

                    //11
                    Rate11 = 1m,
                    QtyCBP11 = 0m,
                    AmtCBP11 = 0m,
                    QtyCBPTo11 = 0m,
                    AmtCBPTo11 = 0m,
                    QtyCAS11 = (y.Mth == "11" ? y.Qty : 0m),
                    AmtCAS11 = (y.Mth == "11" ? y.Rate * y.Qty : 0m),
                    QtyCASTo11 = 0m,
                    AmtCASTo11 = 0m,
                    QtyRemain11 = 0m,
                    AmtRemain11 = 0m,
                    QtyRemainTo11 = 0m,
                    AmtRemainTo11 = 0m,

                    //12
                    Rate12 = 1m,
                    QtyCBP12 = 0m,
                    AmtCBP12 = 0m,
                    QtyCBPTo12 = 0m,
                    AmtCBPTo12 = 0m,
                    QtyCAS12 = (y.Mth == "12" ? y.Qty : 0m),
                    AmtCAS12 = (y.Mth == "12" ? y.Rate * y.Qty : 0m),
                    QtyCASTo12 = 0m,
                    AmtCASTo12 = 0m,
                    QtyRemain12 = 0m,
                    AmtRemain12 = 0m,
                    QtyRemainTo12 = 0m,
                    AmtRemainTo12 = 0m,

                    //Q4
                    RateQ4 = 0m,
                    QtyCBPQ4 = 0m,
                    AmtCBPQ4 = 0m,
                    QtyCBPToQ4 = 0m,
                    AmtCBPToQ4 = 0m,
                    QtyCASQ4 = 0m,
                    AmtCASQ4 = 0m,
                    QtyCASToQ4 = 0m,
                    AmtCASToQ4 = 0m,
                    QtyRemainQ4 = 0m,
                    AmtRemainQ4 = 0m,
                    QtyRemainToQ4 = 0m,
                    AmtRemainToQ4 = 0m,

                    CCCode = y.CCCode,
                    Parent = Sm.GetValue("Select Parent From TblCOA Where AcNo = @Param", y.AcNo),
                    Level = Int32.Parse(Sm.GetValue("Select Level From TblCOA Where AcNo = @Param", y.AcNo)),
                });
            }
        }

        private void GetJournalDataToCBP(ref List<CBP> l, ref List<COAJournal> l4)
        {
            //foreach (var x in l)
            //{
            foreach (var y in l4)
            {
                l.Add(new CBP()
                {
                    AcDesc = y.AcDesc,
                    AcNo = y.AcNo,
                    DeptGrpName = string.Empty,
                    ItCode = y.ItCode,
                    ItName = y.ItName,
                    UoM = string.Empty,
                    BudgetType = string.Empty,

                    Rate = 1m,
                    QtyCBP = 0m,
                    AmtCBP = 0m,
                    QtyCAS = 1m,
                    AmtCAS = 0m,
                    QtyRemain = 1m,
                    AmtRemain = 0m,

                    // 01
                    Rate01 = 1m,
                    QtyCBP01 = 0m,
                    AmtCBP01 = 0m,
                    QtyCBPTo01 = 0m,
                    AmtCBPTo01 = 0m,
                    QtyCAS01 = 1m,
                    AmtCAS01 = (y.Mth == "01" ? y.Amt : 0m),
                    QtyCASTo01 = 1m,
                    AmtCASTo01 = (y.Mth == "01" ? y.Amt : 0m),
                    QtyRemain01 = 1m,
                    AmtRemain01 = 0m,
                    QtyRemainTo01 = 1m,
                    AmtRemainTo01 = 0m,

                    //02
                    Rate02 = 1m,
                    QtyCBP02 = 0m,
                    AmtCBP02 = 0m,
                    QtyCBPTo02 = 0m,
                    AmtCBPTo02 = 0m,
                    QtyCAS02 = 1m,
                    AmtCAS02 = (y.Mth == "02" ? y.Amt : 0m),
                    QtyCASTo02 = 1m,
                    AmtCASTo02 = 0m,
                    QtyRemain02 = 1m,
                    AmtRemain02 = 0m,
                    QtyRemainTo02 = 1m,
                    AmtRemainTo02 = 0m,

                    //03
                    Rate03 = 1m,
                    QtyCBP03 = 0m,
                    AmtCBP03 = 0m,
                    QtyCBPTo03 = 0m,
                    AmtCBPTo03 = 0m,
                    QtyCAS03 = 1m,
                    AmtCAS03 = (y.Mth == "03" ? y.Amt : 0m),
                    QtyCASTo03 = 1m,
                    AmtCASTo03 = 0m,
                    QtyRemain03 = 1m,
                    AmtRemain03 = 0m,
                    QtyRemainTo03 = 1m,
                    AmtRemainTo03 = 0m,

                    //Q1
                    RateQ1 = 0m,
                    QtyCBPQ1 = 0m,
                    AmtCBPQ1 = 0m,
                    QtyCBPToQ1 = 0m,
                    AmtCBPToQ1 = 0m,
                    QtyCASQ1 = 1m,
                    AmtCASQ1 = 0m,
                    QtyCASToQ1 = 1m,
                    AmtCASToQ1 = 0m,
                    QtyRemainQ1 = 1m,
                    AmtRemainQ1 = 0m,
                    QtyRemainToQ1 = 1m,
                    AmtRemainToQ1 = 0m,

                    //04
                    Rate04 = 1m,
                    QtyCBP04 = 0m,
                    AmtCBP04 = 0m,
                    QtyCBPTo04 = 0m,
                    AmtCBPTo04 = 0m,
                    QtyCAS04 = 1m,
                    AmtCAS04 = (y.Mth == "04" ? y.Amt : 0m),
                    QtyCASTo04 = 1m,
                    AmtCASTo04 = 0m,
                    QtyRemain04 = 1m,
                    AmtRemain04 = 0m,
                    QtyRemainTo04 = 1m,
                    AmtRemainTo04 = 0m,

                    //05
                    Rate05 = 1m,
                    QtyCBP05 = 0m,
                    AmtCBP05 = 0m,
                    QtyCBPTo05 = 0m,
                    AmtCBPTo05 = 0m,
                    QtyCAS05 = 1m,
                    AmtCAS05 = (y.Mth == "05" ? y.Amt : 0m),
                    QtyCASTo05 = 1m,
                    AmtCASTo05 = 0m,
                    QtyRemain05 = 1m,
                    AmtRemain05 = 0m,
                    QtyRemainTo05 = 1m,
                    AmtRemainTo05 = 0m,

                    //06
                    Rate06 = 1m,
                    QtyCBP06 = 0m,
                    AmtCBP06 = 0m,
                    QtyCBPTo06 = 0m,
                    AmtCBPTo06 = 0m,
                    QtyCAS06 = 1m,
                    AmtCAS06 = (y.Mth == "06" ? y.Amt : 0m),
                    QtyCASTo06 = 1m,
                    AmtCASTo06 = 0m,
                    QtyRemain06 = 1m,
                    AmtRemain06 = 0m,
                    QtyRemainTo06 = 1m,
                    AmtRemainTo06 = 0m,

                    //Q2
                    RateQ2 = 0m,
                    QtyCBPQ2 = 0m,
                    AmtCBPQ2 = 0m,
                    QtyCBPToQ2 = 0m,
                    AmtCBPToQ2 = 0m,
                    QtyCASQ2 = 1m,
                    AmtCASQ2 = 0m,
                    QtyCASToQ2 = 1m,
                    AmtCASToQ2 = 0m,
                    QtyRemainQ2 = 1m,
                    AmtRemainQ2 = 0m,
                    QtyRemainToQ2 = 1m,
                    AmtRemainToQ2 = 0m,

                    //07
                    Rate07 = 1m,
                    QtyCBP07 = 0m,
                    AmtCBP07 = 0m,
                    QtyCBPTo07 = 0m,
                    AmtCBPTo07 = 0m,
                    QtyCAS07 = 1m,
                    AmtCAS07 = (y.Mth == "07" ? y.Amt : 0m),
                    QtyCASTo07 = 1m,
                    AmtCASTo07 = 0m,
                    QtyRemain07 = 1m,
                    AmtRemain07 = 0m,
                    QtyRemainTo07 = 1m,
                    AmtRemainTo07 = 0m,

                    //08
                    Rate08 = 1m,
                    QtyCBP08 = 0m,
                    AmtCBP08 = 0m,
                    AmtCBPTo08 = 0m,
                    QtyCAS08 = 1m,
                    AmtCAS08 = (y.Mth == "08" ? y.Amt : 0m),
                    QtyCASTo08 = 1m,
                    AmtCASTo08 = 0m,
                    QtyRemain08 = 1m,
                    AmtRemain08 = 0m,
                    QtyRemainTo08 = 1m,
                    AmtRemainTo08 = 0m,

                    //09
                    Rate09 = 1m,
                    QtyCBP09 = 0m,
                    AmtCBP09 = 0m,
                    QtyCBPTo09 = 0m,
                    AmtCBPTo09 = 0m,
                    QtyCAS09 = 1m,
                    AmtCAS09 = (y.Mth == "09" ? y.Amt : 0m),
                    QtyCASTo09 = 1m,
                    AmtCASTo09 = 0m,
                    QtyRemain09 = 1m,
                    AmtRemain09 = 0m,
                    QtyRemainTo09 = 1m,
                    AmtRemainTo09 = 0m,

                    //Q3
                    RateQ3 = 0m,
                    QtyCBPQ3 = 0m,
                    AmtCBPQ3 = 0m,
                    QtyCBPToQ3 = 0m,
                    AmtCBPToQ3 = 0m,
                    QtyCASQ3 = 1m,
                    AmtCASQ3 = 0m,
                    QtyCASToQ3 = 1m,
                    AmtCASToQ3 = 0m,
                    QtyRemainQ3 = 1m,
                    AmtRemainQ3 = 0m,
                    QtyRemainToQ3 = 1m,
                    AmtRemainToQ3 = 0m,


                    //10
                    Rate10 = 1m,
                    QtyCBP10 = 0m,
                    AmtCBP10 = 0m,
                    QtyCBPTo10 = 0m,
                    AmtCBPTo10 = 0m,
                    QtyCAS10 = 1m,
                    AmtCAS10 = (y.Mth == "10" ? y.Amt : 0m),
                    QtyCASTo10 = 1m,
                    AmtCASTo10 = 0m,
                    QtyRemain10 = 1m,
                    AmtRemain10 = 0m,
                    QtyRemainTo10 = 1m,
                    AmtRemainTo10 = 0m,

                    //11
                    Rate11 = 1m,
                    QtyCBP11 = 0m,
                    AmtCBP11 = 0m,
                    QtyCBPTo11 = 0m,
                    AmtCBPTo11 = 0m,
                    QtyCAS11 = 1m,
                    AmtCAS11 = (y.Mth == "11" ? y.Amt : 0m),
                    QtyCASTo11 = 1m,
                    AmtCASTo11 = 0m,
                    QtyRemain11 = 1m,
                    AmtRemain11 = 0m,
                    QtyRemainTo11 = 1m,
                    AmtRemainTo11 = 0m,

                    //12
                    Rate12 = 1m,
                    QtyCBP12 = 0m,
                    AmtCBP12 = 0m,
                    QtyCBPTo12 = 0m,
                    AmtCBPTo12 = 0m,
                    QtyCAS12 = 1m,
                    AmtCAS12 = (y.Mth == "12" ? y.Amt : 0m),
                    QtyCASTo12 = 1m,
                    AmtCASTo12 = 0m,
                    QtyRemain12 = 1m,
                    AmtRemain12 = 0m,
                    QtyRemainTo12 = 1m,
                    AmtRemainTo12 = 0m,

                    //Q4
                    RateQ4 = 0m,
                    QtyCBPQ4 = 0m,
                    AmtCBPQ4 = 0m,
                    QtyCBPToQ4 = 0m,
                    AmtCBPToQ4 = 0m,
                    QtyCASQ4 = 1m,
                    AmtCASQ4 = 0m,
                    QtyCASToQ4 = 1m,
                    AmtCASToQ4 = 0m,
                    QtyRemainQ4 = 1m,
                    AmtRemainQ4 = 0m,
                    QtyRemainToQ4 = 1m,
                    AmtRemainToQ4 = 0m,

                    CCCode = y.CCCode,
                    Parent = y.Parent,
                    Level = y.Level,


                });
            }
        }

        private void ProcessRealization(ref List<CBP> l, ref List<CAS> l2, ref List<RecvVdAutoDO> l5)
        {
            foreach (var x in l)
            {
                if (l2.Count > 0)
                {
                    #region Get CAS To CBP
                    foreach (var y in l2.Where(w =>
                        x.AcNo == w.AcNo &&
                        x.ItCode == w.ItCode &&
                        x.CCCode == w.CCCode &&
                        !w.IsTaken
                        ))
                    {
                        if (y.Mth == "01")
                        {
                            x.QtyCAS01 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS01 = y.Rate * y.Qty;
                            else x.AmtCAS01 = x.Rate01 * y.Qty;
                            x.QtyCASTo01 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCASTo01 = y.Rate * y.Qty;
                            else x.AmtCASTo01 = x.Rate01 * y.Qty;
                        }

                        if (y.Mth == "02")
                        {
                            x.QtyCAS02 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS02 = y.Rate * y.Qty;
                            else x.AmtCAS02 = x.Rate02 * y.Qty;
                        }

                        if (y.Mth == "03")
                        {
                            x.QtyCAS03 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS03 = y.Rate * y.Qty;
                            else x.AmtCAS03 = x.Rate03 * y.Qty;
                        }

                        if (y.Mth == "04")
                        {
                            x.QtyCAS04 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS04 = y.Rate * y.Qty;
                            else x.AmtCAS04 = x.Rate04 * y.Qty;
                        }

                        if (y.Mth == "05")
                        {
                            x.QtyCAS05 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS05 = y.Rate * y.Qty;
                            else x.AmtCAS05 = x.Rate05 * y.Qty;
                        }

                        if (y.Mth == "06")
                        {
                            x.QtyCAS06 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS06 = y.Rate * y.Qty;
                            else x.AmtCAS06 = x.Rate06 * y.Qty;
                        }

                        if (y.Mth == "07")
                        {
                            x.QtyCAS07 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS07 = y.Rate * y.Qty;
                            else x.AmtCAS07 = x.Rate07 * y.Qty;
                        }

                        if (y.Mth == "08")
                        {
                            x.QtyCAS08 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS08 = y.Rate * y.Qty;
                            else x.AmtCAS08 = x.Rate08 * y.Qty;
                        }

                        if (y.Mth == "09")
                        {
                            x.QtyCAS09 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS09 = y.Rate * y.Qty;
                            else x.AmtCAS09 = x.Rate09 * y.Qty;
                        }

                        if (y.Mth == "10")
                        {
                            x.QtyCAS10 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS10 = y.Rate * y.Qty;
                            else x.AmtCAS10 = x.Rate10 * y.Qty;
                        }

                        if (y.Mth == "11")
                        {
                            x.QtyCAS11 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS11 = y.Rate * y.Qty;
                            else x.AmtCAS11 = x.Rate11 * y.Qty;
                        }

                        if (y.Mth == "12")
                        {
                            x.QtyCAS12 = y.Qty;
                            if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS12 = y.Rate * y.Qty;
                            else x.AmtCAS12 = x.Rate12 * y.Qty;
                        }

                        y.IsTaken = true;
                    }
                    #endregion

                    # region Process Other Misc
                    foreach (var y in l2.Where(w => w.IsTaken == false))
                    {
                        if (x.ItCode.ToLower() == "misc" &&
                            x.AcNo == y.AcNo &&
                            x.CCCode == y.CCCode)
                        {
                            if (y.Mth == "01")
                            {
                                x.QtyCAS01 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS01 += y.Rate * y.Qty;
                                else x.AmtCAS01 += x.Rate01 * y.Qty;
                                x.QtyCASTo01 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCASTo01 += y.Rate * y.Qty;
                                else x.AmtCASTo01 += x.Rate01 * y.Qty;
                            }

                            if (y.Mth == "02")
                            {
                                x.QtyCAS02 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS02 += y.Rate * y.Qty;
                                else x.AmtCAS02 += x.Rate02 * y.Qty;
                            }

                            if (y.Mth == "03")
                            {
                                x.QtyCAS03 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS03 += y.Rate * y.Qty;
                                else x.AmtCAS03 += x.Rate03 * y.Qty;
                            }

                            if (y.Mth == "04")
                            {
                                x.QtyCAS04 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS04 += y.Rate * y.Qty;
                                else x.AmtCAS04 += x.Rate04 * y.Qty;
                            }

                            if (y.Mth == "05")
                            {
                                x.QtyCAS05 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS05 += y.Rate * y.Qty;
                                else x.AmtCAS05 += x.Rate05 * y.Qty;
                            }

                            if (y.Mth == "06")
                            {
                                x.QtyCAS06 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS06 += y.Rate * y.Qty;
                                else x.AmtCAS06 += x.Rate06 * y.Qty;
                            }

                            if (y.Mth == "07")
                            {
                                x.QtyCAS07 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS07 += y.Rate * y.Qty;
                                else x.AmtCAS07 += x.Rate07 * y.Qty;
                            }

                            if (y.Mth == "08")
                            {
                                x.QtyCAS08 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS08 += y.Rate * y.Qty;
                                else x.AmtCAS08 += x.Rate08 * y.Qty;
                            }

                            if (y.Mth == "09")
                            {
                                x.QtyCAS09 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS09 += y.Rate * y.Qty;
                                else x.AmtCAS09 += x.Rate09 * y.Qty;
                            }

                            if (y.Mth == "10")
                            {
                                x.QtyCAS10 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS10 += y.Rate * y.Qty;
                                else x.AmtCAS10 += x.Rate10 * y.Qty;
                            }

                            if (y.Mth == "11")
                            {
                                x.QtyCAS11 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS11 += y.Rate * y.Qty;
                                else x.AmtCAS11 += x.Rate11 * y.Qty;
                            }

                            if (y.Mth == "12")
                            {
                                x.QtyCAS12 += y.Qty;
                                if (mIsCASUseItemRateNotBasedOnCBP) x.AmtCAS12 += y.Rate * y.Qty;
                                else x.AmtCAS12 += x.Rate12 * y.Qty;
                            }

                            y.IsTaken = true;
                        }
                    }
                    #endregion
                }

                if (l5.Count > 0)
                {
                    # region Get RecvVd Auto DO To CBP
                    foreach (var y in l5.Where(w =>
                        x.AcNo == w.AcNo &&
                        x.ItCode == w.ItCode &&
                        x.CCCode == w.CCCode &&
                        !w.IsTaken
                        ))
                    {
                        if (y.Mth == "01")
                        {
                            x.QtyCAS01 += y.Qty;
                            x.AmtCAS01 += y.Rate * y.Qty;
                            x.QtyCASTo01 += y.Qty;
                            x.AmtCASTo01 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "02")
                        {
                            x.QtyCAS02 += y.Qty;
                            x.AmtCAS02 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "03")
                        {
                            x.QtyCAS03 += y.Qty;
                            x.AmtCAS03 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "04")
                        {
                            x.QtyCAS04 += y.Qty;
                            x.AmtCAS04 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "05")
                        {
                            x.QtyCAS05 += y.Qty;
                            x.AmtCAS05 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "06")
                        {
                            x.QtyCAS06 += y.Qty;
                            x.AmtCAS06 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "07")
                        {
                            x.QtyCAS07 += y.Qty;
                            x.AmtCAS07 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "08")
                        {
                            x.QtyCAS08 += y.Qty;
                            x.AmtCAS08 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "09")
                        {
                            x.QtyCAS09 += y.Qty;
                            x.AmtCAS09 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "10")
                        {
                            x.QtyCAS10 += y.Qty;
                            x.AmtCAS10 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "11")
                        {
                            x.QtyCAS11 += y.Qty;
                            x.AmtCAS11 += y.Rate * y.Qty;
                        }

                        if (y.Mth == "12")
                        {
                            x.QtyCAS12 += y.Qty;
                            x.AmtCAS12 += y.Rate * y.Qty;
                        }

                        y.IsTaken = true;
                    }
                    #endregion

                    #region Process Other Misc
                    foreach (var y in l5.Where(w => w.IsTaken == false))
                    {
                        if (x.ItCode.ToLower() == "misc" &&
                            x.AcNo == y.AcNo &&
                            x.CCCode == y.CCCode)
                        {
                            if (y.Mth == "01")
                            {
                                x.QtyCAS01 += y.Qty;
                                x.AmtCAS01 += y.Rate * y.Qty;
                                x.QtyCASTo01 += y.Qty;
                                x.AmtCASTo01 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "02")
                            {
                                x.QtyCAS02 += y.Qty;
                                x.AmtCAS02 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "03")
                            {
                                x.QtyCAS03 += y.Qty;
                                x.AmtCAS03 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "04")
                            {
                                x.QtyCAS04 += y.Qty;
                                x.AmtCAS04 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "05")
                            {
                                x.QtyCAS05 += y.Qty;
                                x.AmtCAS05 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "06")
                            {
                                x.QtyCAS06 += y.Qty;
                                x.AmtCAS06 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "07")
                            {
                                x.QtyCAS07 += y.Qty;
                                x.AmtCAS07 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "08")
                            {
                                x.QtyCAS08 += y.Qty;
                                x.AmtCAS08 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "09")
                            {
                                x.QtyCAS09 += y.Qty;
                                x.AmtCAS09 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "10")
                            {
                                x.QtyCAS10 += y.Qty;
                                x.AmtCAS10 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "11")
                            {
                                x.QtyCAS11 += y.Qty;
                                x.AmtCAS11 += y.Rate * y.Qty;
                            }

                            if (y.Mth == "12")
                            {
                                x.QtyCAS12 += y.Qty;
                                x.AmtCAS12 += y.Rate * y.Qty;
                            }

                            y.IsTaken = true;
                        }
                    }
                    #endregion
                }
            }
        }

        private void ProcessAllData(ref List<CBP> l)
        {
            foreach (var x in l)
            {
                #region Calculate up to 
                x.QtyCBPTo02 = x.QtyCBPTo01 + x.QtyCBP02;
                x.QtyCBPTo03 = x.QtyCBPTo02 + x.QtyCBP03;
                x.QtyCBPTo04 = x.QtyCBPTo03 + x.QtyCBP04;
                x.QtyCBPTo05 = x.QtyCBPTo04 + x.QtyCBP05;
                x.QtyCBPTo06 = x.QtyCBPTo05 + x.QtyCBP06;
                x.QtyCBPTo07 = x.QtyCBPTo06 + x.QtyCBP07;
                x.QtyCBPTo08 = x.QtyCBPTo07 + x.QtyCBP08;
                x.QtyCBPTo09 = x.QtyCBPTo08 + x.QtyCBP09;
                x.QtyCBPTo10 = x.QtyCBPTo09 + x.QtyCBP10;
                x.QtyCBPTo11 = x.QtyCBPTo10 + x.QtyCBP11;
                x.QtyCBPTo12 = x.QtyCBPTo11 + x.QtyCBP12;

                x.QtyCASTo02 = x.QtyCASTo01 + x.QtyCAS02;
                x.QtyCASTo03 = x.QtyCASTo02 + x.QtyCAS03;
                x.QtyCASTo04 = x.QtyCASTo03 + x.QtyCAS04;
                x.QtyCASTo05 = x.QtyCASTo04 + x.QtyCAS05;
                x.QtyCASTo06 = x.QtyCASTo05 + x.QtyCAS06;
                x.QtyCASTo07 = x.QtyCASTo06 + x.QtyCAS07;
                x.QtyCASTo08 = x.QtyCASTo07 + x.QtyCAS08;
                x.QtyCASTo09 = x.QtyCASTo08 + x.QtyCAS09;
                x.QtyCASTo10 = x.QtyCASTo09 + x.QtyCAS10;
                x.QtyCASTo11 = x.QtyCASTo10 + x.QtyCAS11;
                x.QtyCASTo12 = x.QtyCASTo11 + x.QtyCAS12;

                /*
                x.AmtCBPTo02 = x.QtyCBPTo02 * x.Rate02;
                x.AmtCBPTo03 = x.QtyCBPTo03 * x.Rate03;
                x.AmtCBPTo04 = x.QtyCBPTo04 * x.Rate04;
                x.AmtCBPTo05 = x.QtyCBPTo05 * x.Rate05;
                x.AmtCBPTo06 = x.QtyCBPTo06 * x.Rate06;
                x.AmtCBPTo07 = x.QtyCBPTo07 * x.Rate07;
                x.AmtCBPTo08 = x.QtyCBPTo08 * x.Rate08;
                x.AmtCBPTo09 = x.QtyCBPTo09 * x.Rate09;
                x.AmtCBPTo10 = x.QtyCBPTo10 * x.Rate10;
                x.AmtCBPTo11 = x.QtyCBPTo11 * x.Rate11;
                x.AmtCBPTo12 = x.QtyCBPTo12 * x.Rate12;

                x.AmtCASTo02 = x.QtyCASTo02 * x.Rate02;
                x.AmtCASTo03 = x.QtyCASTo03 * x.Rate03;
                x.AmtCASTo04 = x.QtyCASTo04 * x.Rate04;
                x.AmtCASTo05 = x.QtyCASTo05 * x.Rate05;
                x.AmtCASTo06 = x.QtyCASTo06 * x.Rate06;
                x.AmtCASTo07 = x.QtyCASTo07 * x.Rate07;
                x.AmtCASTo08 = x.QtyCASTo08 * x.Rate08;
                x.AmtCASTo09 = x.QtyCASTo09 * x.Rate09;
                x.AmtCASTo10 = x.QtyCASTo10 * x.Rate10;
                x.AmtCASTo11 = x.QtyCASTo11 * x.Rate11;
                x.AmtCASTo12 = x.QtyCASTo12 * x.Rate12;
                */

                x.AmtCBPTo02 = x.AmtCBPTo01 + x.AmtCBP02;
                x.AmtCBPTo03 = x.AmtCBPTo02 + x.AmtCBP03;
                x.AmtCBPTo04 = x.AmtCBPTo03 + x.AmtCBP04;
                x.AmtCBPTo05 = x.AmtCBPTo04 + x.AmtCBP05;
                x.AmtCBPTo06 = x.AmtCBPTo05 + x.AmtCBP06;
                x.AmtCBPTo07 = x.AmtCBPTo06 + x.AmtCBP07;
                x.AmtCBPTo08 = x.AmtCBPTo07 + x.AmtCBP08;
                x.AmtCBPTo09 = x.AmtCBPTo08 + x.AmtCBP09;
                x.AmtCBPTo10 = x.AmtCBPTo09 + x.AmtCBP10;
                x.AmtCBPTo11 = x.AmtCBPTo10 + x.AmtCBP11;
                x.AmtCBPTo12 = x.AmtCBPTo11 + x.AmtCBP12;

                x.AmtCASTo02 = x.AmtCASTo01 + x.AmtCAS02;
                x.AmtCASTo03 = x.AmtCASTo02 + x.AmtCAS03;
                x.AmtCASTo04 = x.AmtCASTo03 + x.AmtCAS04;
                x.AmtCASTo05 = x.AmtCASTo04 + x.AmtCAS05;
                x.AmtCASTo06 = x.AmtCASTo05 + x.AmtCAS06;
                x.AmtCASTo07 = x.AmtCASTo06 + x.AmtCAS07;
                x.AmtCASTo08 = x.AmtCASTo07 + x.AmtCAS08;
                x.AmtCASTo09 = x.AmtCASTo08 + x.AmtCAS09;
                x.AmtCASTo10 = x.AmtCASTo09 + x.AmtCAS10;
                x.AmtCASTo11 = x.AmtCASTo10 + x.AmtCAS11;
                x.AmtCASTo12 = x.AmtCASTo11 + x.AmtCAS12;
                #endregion

                #region Calculate Quarter
                x.RateQ1 = (x.Rate01 + x.Rate02 + x.Rate03) / 3;
                x.RateQ2 = (x.Rate04 + x.Rate05 + x.Rate06) / 3;
                x.RateQ3 = (x.Rate07 + x.Rate08 + x.Rate09) / 3;
                x.RateQ4 = (x.Rate10 + x.Rate11 + x.Rate12) / 3;

                x.QtyCBPQ1 = (x.QtyCBP01 + x.QtyCBP02 + x.QtyCBP03);
                x.QtyCBPQ2 = (x.QtyCBP04 + x.QtyCBP05 + x.QtyCBP06);
                x.QtyCBPQ3 = (x.QtyCBP07 + x.QtyCBP08 + x.QtyCBP09);
                x.QtyCBPQ4 = (x.QtyCBP10 + x.QtyCBP11 + x.QtyCBP12);

                x.QtyCASQ1 = (x.QtyCAS01 + x.QtyCAS02 + x.QtyCAS03);
                x.QtyCASQ2 = (x.QtyCAS04 + x.QtyCAS05 + x.QtyCAS06);
                x.QtyCASQ3 = (x.QtyCAS07 + x.QtyCAS08 + x.QtyCAS09);
                x.QtyCASQ4 = (x.QtyCAS10 + x.QtyCAS11 + x.QtyCAS12);

                //x.QtyCBPToQ1 = (x.QtyCBPTo01 + x.QtyCBPTo02 + x.QtyCBPTo03);
                //x.QtyCBPToQ2 = (x.QtyCBPTo04 + x.QtyCBPTo05 + x.QtyCBPTo06);
                //x.QtyCBPToQ3 = (x.QtyCBPTo07 + x.QtyCBPTo08 + x.QtyCBPTo09);
                //x.QtyCBPToQ4 = (x.QtyCBPTo10 + x.QtyCBPTo11 + x.QtyCBPTo12);

                //x.QtyCASToQ1 = (x.QtyCASTo01 + x.QtyCASTo02 + x.QtyCASTo03);
                //x.QtyCASToQ2 = (x.QtyCASTo04 + x.QtyCASTo05 + x.QtyCASTo06);
                //x.QtyCASToQ3 = (x.QtyCASTo07 + x.QtyCASTo08 + x.QtyCASTo09);
                //x.QtyCASToQ4 = (x.QtyCASTo10 + x.QtyCASTo11 + x.QtyCASTo12);

                x.QtyCBPToQ1 = x.QtyCBPTo03;
                x.QtyCBPToQ2 = x.QtyCBPTo06;
                x.QtyCBPToQ3 = x.QtyCBPTo09;
                x.QtyCBPToQ4 = x.QtyCBPTo12;

                x.QtyCASToQ1 = x.QtyCASTo03;
                x.QtyCASToQ2 = x.QtyCASTo06;
                x.QtyCASToQ3 = x.QtyCASTo09;
                x.QtyCASToQ4 = x.QtyCASTo12;

                /*
                x.AmtCBPQ1 = x.RateQ1 * x.QtyCBPQ1;
                x.AmtCBPQ2 = x.RateQ2 * x.QtyCBPQ2;
                x.AmtCBPQ3 = x.RateQ3 * x.QtyCBPQ3;
                x.AmtCBPQ4 = x.RateQ4 * x.QtyCBPQ4;

                x.AmtCASQ1 = x.RateQ1 * x.QtyCASQ1;
                x.AmtCASQ2 = x.RateQ2 * x.QtyCASQ2;
                x.AmtCASQ3 = x.RateQ3 * x.QtyCASQ3;
                x.AmtCASQ4 = x.RateQ4 * x.QtyCASQ4;

                x.AmtCBPToQ1 = x.RateQ1 * x.QtyCBPToQ1;
                x.AmtCBPToQ2 = x.RateQ2 * x.QtyCBPToQ2;
                x.AmtCBPToQ3 = x.RateQ3 * x.QtyCBPToQ3;
                x.AmtCBPToQ4 = x.RateQ4 * x.QtyCBPToQ4;

                x.AmtCASToQ1 = x.RateQ1 * x.QtyCASToQ1;
                x.AmtCASToQ2 = x.RateQ2 * x.QtyCASToQ2;
                x.AmtCASToQ3 = x.RateQ3 * x.QtyCASToQ3;
                x.AmtCASToQ4 = x.RateQ4 * x.QtyCASToQ4;
                */

                x.AmtCBPQ1 = (x.AmtCBP01 + x.AmtCBP02 + x.AmtCBP03);
                x.AmtCBPQ2 = (x.AmtCBP04 + x.AmtCBP05 + x.AmtCBP06);
                x.AmtCBPQ3 = (x.AmtCBP07 + x.AmtCBP08 + x.AmtCBP09);
                x.AmtCBPQ4 = (x.AmtCBP10 + x.AmtCBP11 + x.AmtCBP12);

                x.AmtCASQ1 = (x.AmtCAS01 + x.AmtCAS02 + x.AmtCAS03);
                x.AmtCASQ2 = (x.AmtCAS04 + x.AmtCAS05 + x.AmtCAS06);
                x.AmtCASQ3 = (x.AmtCAS07 + x.AmtCAS08 + x.AmtCAS09);
                x.AmtCASQ4 = (x.AmtCAS10 + x.AmtCAS11 + x.AmtCAS12);

                x.AmtCBPToQ1 = x.AmtCBPQ1;//(x.AmtCBPTo01 + x.AmtCBPTo02 + x.AmtCBPTo03);
                x.AmtCBPToQ2 = x.AmtCBPQ1 + x.AmtCBPQ2;//(x.AmtCBPTo04 + x.AmtCBPTo05 + x.AmtCBPTo06);
                x.AmtCBPToQ3 = x.AmtCBPQ1 + x.AmtCBPQ2 + x.AmtCBPQ3;//(x.AmtCBPTo07 + x.AmtCBPTo08 + x.AmtCBPTo09);
                x.AmtCBPToQ4 = x.AmtCBPQ1 + x.AmtCBPQ2 + x.AmtCBPQ3 + x.AmtCBPQ4;//(x.AmtCBPTo10 + x.AmtCBPTo11 + x.AmtCBPTo12);

                x.AmtCASToQ1 = x.AmtCASQ1;//(x.AmtCASTo01 + x.AmtCASTo02 + x.AmtCASTo03);
                x.AmtCASToQ2 = x.AmtCASQ1 + x.AmtCASQ2;//(x.AmtCASTo04 + x.AmtCASTo05 + x.AmtCASTo06);
                x.AmtCASToQ3 = x.AmtCASQ1 + x.AmtCASQ2 + x.AmtCASQ3;//(x.AmtCASTo07 + x.AmtCASTo08 + x.AmtCASTo09);
                x.AmtCASToQ4 = x.AmtCASQ1 + x.AmtCASQ2 + x.AmtCASQ3 + x.AmtCASQ4;//(x.AmtCASTo10 + x.AmtCASTo11 + x.AmtCASTo12);
                #endregion

                #region Calculate Year
                x.Rate = (x.RateQ1 + x.RateQ2 + x.RateQ3 + x.RateQ4) / 4;
                x.QtyCBP = (x.QtyCBPQ1 + x.QtyCBPQ2 + x.QtyCBPQ3 + x.QtyCBPQ4);
                x.QtyCAS = (x.QtyCASQ1 + x.QtyCASQ2 + x.QtyCASQ3 + x.QtyCASQ4);
                x.AmtCBP = (x.AmtCBPQ1 + x.AmtCBPQ2 + x.AmtCBPQ3 + x.AmtCBPQ4); //x.QtyCBP * x.Rate;
                x.AmtCAS = (x.AmtCASQ1 + x.AmtCASQ2 + x.AmtCASQ3 + x.AmtCASQ4);//x.QtyCAS * x.Rate;
                #endregion

                #region Calculate Remain
                x.QtyRemain = x.QtyCBP - x.QtyCAS;
                x.QtyRemain01 = x.QtyCBP01 - x.QtyCAS01;
                x.QtyRemain02 = x.QtyCBP02 - x.QtyCAS02;
                x.QtyRemain03 = x.QtyCBP03 - x.QtyCAS03;
                x.QtyRemainQ1 = x.QtyCBPQ1 - x.QtyCASQ1;
                x.QtyRemain04 = x.QtyCBP04 - x.QtyCAS04;
                x.QtyRemain05 = x.QtyCBP05 - x.QtyCAS05;
                x.QtyRemain06 = x.QtyCBP06 - x.QtyCAS06;
                x.QtyRemainQ2 = x.QtyCBPQ2 - x.QtyCASQ2;
                x.QtyRemain07 = x.QtyCBP07 - x.QtyCAS07;
                x.QtyRemain08 = x.QtyCBP08 - x.QtyCAS08;
                x.QtyRemain09 = x.QtyCBP09 - x.QtyCAS09;
                x.QtyRemainQ3 = x.QtyCBPQ3 - x.QtyCASQ3;
                x.QtyRemain10 = x.QtyCBP10 - x.QtyCAS10;
                x.QtyRemain11 = x.QtyCBP11 - x.QtyCAS11;
                x.QtyRemain12 = x.QtyCBP12 - x.QtyCAS12;
                x.QtyRemainQ4 = x.QtyCBPQ4 - x.QtyCASQ4;

                x.AmtRemain = x.AmtCBP - x.AmtCAS;


                x.AmtRemain01 = x.AmtCBP01 - x.AmtCAS01;
                x.AmtRemain02 = x.AmtCBP02 - x.AmtCAS02;
                x.AmtRemain03 = x.AmtCBP03 - x.AmtCAS03;
                x.AmtRemainQ1 = x.AmtCBPQ1 - x.AmtCASQ1;
                x.AmtRemain04 = x.AmtCBP04 - x.AmtCAS04;
                x.AmtRemain05 = x.AmtCBP05 - x.AmtCAS05;
                x.AmtRemain06 = x.AmtCBP06 - x.AmtCAS06;
                x.AmtRemainQ2 = x.AmtCBPQ2 - x.AmtCASQ2;
                x.AmtRemain07 = x.AmtCBP07 - x.AmtCAS07;
                x.AmtRemain08 = x.AmtCBP08 - x.AmtCAS08;
                x.AmtRemain09 = x.AmtCBP09 - x.AmtCAS09;
                x.AmtRemainQ3 = x.AmtCBPQ3 - x.AmtCASQ3;
                x.AmtRemain10 = x.AmtCBP10 - x.AmtCAS10;
                x.AmtRemain11 = x.AmtCBP11 - x.AmtCAS11;
                x.AmtRemain12 = x.AmtCBP12 - x.AmtCAS12;
                x.AmtRemainQ4 = x.AmtCBPQ4 - x.AmtCASQ4;


                x.QtyRemainTo01 = x.QtyCBPTo01 - x.QtyCASTo01;
                x.QtyRemainTo02 = x.QtyCBPTo02 - x.QtyCASTo02;
                x.QtyRemainTo03 = x.QtyCBPTo03 - x.QtyCASTo03;
                x.QtyRemainToQ1 = x.QtyCBPToQ1 - x.QtyCASToQ1;
                x.QtyRemainTo04 = x.QtyCBPTo04 - x.QtyCASTo04;
                x.QtyRemainTo05 = x.QtyCBPTo05 - x.QtyCASTo05;
                x.QtyRemainTo06 = x.QtyCBPTo06 - x.QtyCASTo06;
                x.QtyRemainToQ2 = x.QtyCBPToQ2 - x.QtyCASToQ2;
                x.QtyRemainTo07 = x.QtyCBPTo07 - x.QtyCASTo07;
                x.QtyRemainTo08 = x.QtyCBPTo08 - x.QtyCASTo08;
                x.QtyRemainTo09 = x.QtyCBPTo09 - x.QtyCASTo09;
                x.QtyRemainToQ3 = x.QtyCBPToQ3 - x.QtyCASToQ3;
                x.QtyRemainTo10 = x.QtyCBPTo10 - x.QtyCASTo10;
                x.QtyRemainTo11 = x.QtyCBPTo11 - x.QtyCASTo11;
                x.QtyRemainTo12 = x.QtyCBPTo12 - x.QtyCASTo12;
                x.QtyRemainToQ4 = x.QtyCBPToQ4 - x.QtyCASToQ4;

                x.AmtRemainTo01 = x.AmtCBPTo01 - x.AmtCASTo01;
                x.AmtRemainTo02 = x.AmtCBPTo02 - x.AmtCASTo02;
                x.AmtRemainTo03 = x.AmtCBPTo03 - x.AmtCASTo03;
                x.AmtRemainToQ1 = x.AmtCBPToQ1 - x.AmtCASToQ1;
                x.AmtRemainTo04 = x.AmtCBPTo04 - x.AmtCASTo04;
                x.AmtRemainTo05 = x.AmtCBPTo05 - x.AmtCASTo05;
                x.AmtRemainTo06 = x.AmtCBPTo06 - x.AmtCASTo06;
                x.AmtRemainToQ2 = x.AmtCBPToQ2 - x.AmtCASToQ2;
                x.AmtRemainTo07 = x.AmtCBPTo07 - x.AmtCASTo07;
                x.AmtRemainTo08 = x.AmtCBPTo08 - x.AmtCASTo08;
                x.AmtRemainTo09 = x.AmtCBPTo09 - x.AmtCASTo09;
                x.AmtRemainToQ3 = x.AmtCBPToQ3 - x.AmtCASToQ3;
                x.AmtRemainTo10 = x.AmtCBPTo10 - x.AmtCASTo10;
                x.AmtRemainTo11 = x.AmtCBPTo11 - x.AmtCASTo11;
                x.AmtRemainTo12 = x.AmtCBPTo12 - x.AmtCASTo12;
                x.AmtRemainToQ4 = x.AmtCBPToQ4 - x.AmtCASToQ4;
                #endregion

                //if (x.ItCode == "Misc")
                //{
                //    x.QtyCAS = 1m;
                //    x.QtyCAS01 = 1m;
                //    x.QtyCAS02 = 1m;
                //    x.QtyCAS03 = 1m;
                //    x.QtyCAS04 = 1m;
                //    x.QtyCAS05 = 1m;
                //    x.QtyCAS06 = 1m;
                //    x.QtyCAS07 = 1m;
                //    x.QtyCAS08 = 1m;
                //    x.QtyCAS09 = 1m;
                //    x.QtyCAS10 = 1m;
                //    x.QtyCAS11 = 1m;
                //    x.QtyCAS12 = 1m;
                //    x.QtyCASQ1 = 1m;
                //    x.QtyCASQ2 = 1m;
                //    x.QtyCASQ3 = 1m;
                //    x.QtyCASQ4 = 1m;
                //    x.QtyCASTo01 = 1m;
                //    x.QtyCASTo02 = 1m;
                //    x.QtyCASTo03 = 1m;
                //    x.QtyCASTo04 = 1m;
                //    x.QtyCASTo05 = 1m;
                //    x.QtyCASTo06 = 1m;
                //    x.QtyCASTo07 = 1m;
                //    x.QtyCASTo08 = 1m;
                //    x.QtyCASTo09 = 1m;
                //    x.QtyCASTo10 = 1m;
                //    x.QtyCASTo11 = 1m;
                //    x.QtyCASTo12 = 1m;
                //    x.QtyCASToQ1 = 1m;
                //    x.QtyCASToQ2 = 1m;
                //    x.QtyCASToQ3 = 1m;
                //    x.QtyCASToQ4 = 1m;
                //    x.QtyRemain = 1m;
                //    x.QtyRemain01 = 1m;
                //    x.QtyRemain02 = 1m;
                //    x.QtyRemain03 = 1m;
                //    x.QtyRemain04 = 1m;
                //    x.QtyRemain05 = 1m;
                //    x.QtyRemain06 = 1m;
                //    x.QtyRemain07 = 1m;
                //    x.QtyRemain08 = 1m;
                //    x.QtyRemain09 = 1m;
                //    x.QtyRemain10 = 1m;
                //    x.QtyRemain11 = 1m;
                //    x.QtyRemain12 = 1m;
                //    x.QtyRemainQ1 = 1m;
                //    x.QtyRemainQ2 = 1m;
                //    x.QtyRemainQ3 = 1m;
                //    x.QtyRemainQ4 = 1m;
                //    x.QtyRemainTo01 = 1m;
                //    x.QtyRemainTo02 = 1m;
                //    x.QtyRemainTo03 = 1m;
                //    x.QtyRemainTo04 = 1m;
                //    x.QtyRemainTo05 = 1m;
                //    x.QtyRemainTo06 = 1m;
                //    x.QtyRemainTo07 = 1m;
                //    x.QtyRemainTo08 = 1m;
                //    x.QtyRemainTo09 = 1m;
                //    x.QtyRemainTo10 = 1m;
                //    x.QtyRemainTo11 = 1m;
                //    x.QtyRemainTo12 = 1m;
                //    x.QtyRemainToQ1 = 1m;
                //    x.QtyRemainToQ2 = 1m;
                //    x.QtyRemainToQ3 = 1m;
                //    x.QtyRemainToQ4 = 1m;
                //}
            }
        }

        private void ProcessBudgetType(ref List<CBP> l, ref List<CBPBudgetType> l3)
        {
            foreach (var x in l)
            {
                foreach (var y in l3.Where(w => w.AcNo == x.AcNo))
                {
                    x.BudgetType = y.BudgetType;
                }
            }
        }

        private void ProcessSummary(ref List<CBP> l1, ref List<CBP2> l2)
        {
            var l = l1
                .GroupBy(s => new { s.AcNo, s.AcDesc, s.Parent, s.Level })
                .Select(g =>
                   new {
                       AcNo = g.Key.AcNo,
                       AcDesc = g.Key.AcDesc,
                       Parent = g.Key.Parent,
                       Level = g.Key.Level,

                       AmtCBP = g.Sum(x => x.AmtCBP),
                       AmtCAS = g.Sum(x => x.AmtCAS),
                       AmtRemain = g.Sum(x => x.AmtRemain),
                       AmtJN = g.Sum(x => x.AmtJN),

                       AmtCBP01 = g.Sum(x => x.AmtCBP01),
                       AmtCBPTo01 = g.Sum(x => x.AmtCBPTo01),
                       AmtCAS01 = g.Sum(x => x.AmtCAS01),
                       AmtCASTo01 = g.Sum(x => x.AmtCASTo01),
                       AmtRemain01 = g.Sum(x => x.AmtRemain01),
                       AmtRemainTo01 = g.Sum(x => x.AmtRemainTo01),
                       AmtJN01 = g.Sum(x => x.AmtJN01),

                       AmtCBP02 = g.Sum(x => x.AmtCBP02),
                       AmtCBPTo02 = g.Sum(x => x.AmtCBPTo02),
                       AmtCAS02 = g.Sum(x => x.AmtCAS02),
                       AmtCASTo02 = g.Sum(x => x.AmtCASTo02),
                       AmtRemain02 = g.Sum(x => x.AmtRemain02),
                       AmtRemainTo02 = g.Sum(x => x.AmtRemainTo02),
                       AmtJN02 = g.Sum(x => x.AmtJN02),

                       AmtCBP03 = g.Sum(x => x.AmtCBP03),
                       AmtCBPTo03 = g.Sum(x => x.AmtCBPTo03),
                       AmtCAS03 = g.Sum(x => x.AmtCAS03),
                       AmtCASTo03 = g.Sum(x => x.AmtCASTo03),
                       AmtRemain03 = g.Sum(x => x.AmtRemain03),
                       AmtRemainTo03 = g.Sum(x => x.AmtRemainTo03),
                       AmtJN03 = g.Sum(x => x.AmtJN03),

                       AmtCBP04 = g.Sum(x => x.AmtCBP04),
                       AmtCBPTo04 = g.Sum(x => x.AmtCBPTo04),
                       AmtCAS04 = g.Sum(x => x.AmtCAS04),
                       AmtCASTo04 = g.Sum(x => x.AmtCASTo04),
                       AmtRemain04 = g.Sum(x => x.AmtRemain04),
                       AmtRemainTo04 = g.Sum(x => x.AmtRemainTo04),
                       AmtJN04 = g.Sum(x => x.AmtJN04),

                       AmtCBP05 = g.Sum(x => x.AmtCBP05),
                       AmtCBPTo05 = g.Sum(x => x.AmtCBPTo05),
                       AmtCAS05 = g.Sum(x => x.AmtCAS05),
                       AmtCASTo05 = g.Sum(x => x.AmtCASTo05),
                       AmtRemain05 = g.Sum(x => x.AmtRemain05),
                       AmtRemainTo05 = g.Sum(x => x.AmtRemainTo05),
                       AmtJN05 = g.Sum(x => x.AmtJN05),

                       AmtCBP06 = g.Sum(x => x.AmtCBP06),
                       AmtCBPTo06 = g.Sum(x => x.AmtCBPTo06),
                       AmtCAS06 = g.Sum(x => x.AmtCAS06),
                       AmtCASTo06 = g.Sum(x => x.AmtCASTo06),
                       AmtRemain06 = g.Sum(x => x.AmtRemain06),
                       AmtRemainTo06 = g.Sum(x => x.AmtRemainTo06),
                       AmtJN06 = g.Sum(x => x.AmtJN06),

                       AmtCBP07 = g.Sum(x => x.AmtCBP07),
                       AmtCBPTo07 = g.Sum(x => x.AmtCBPTo07),
                       AmtCAS07 = g.Sum(x => x.AmtCAS07),
                       AmtCASTo07 = g.Sum(x => x.AmtCASTo07),
                       AmtRemain07 = g.Sum(x => x.AmtRemain07),
                       AmtRemainTo07 = g.Sum(x => x.AmtRemainTo07),
                       AmtJN07 = g.Sum(x => x.AmtJN07),

                       AmtCBP08 = g.Sum(x => x.AmtCBP08),
                       AmtCBPTo08 = g.Sum(x => x.AmtCBPTo08),
                       AmtCAS08 = g.Sum(x => x.AmtCAS08),
                       AmtCASTo08 = g.Sum(x => x.AmtCASTo08),
                       AmtRemain08 = g.Sum(x => x.AmtRemain08),
                       AmtRemainTo08 = g.Sum(x => x.AmtRemainTo08),
                       AmtJN08 = g.Sum(x => x.AmtJN08),

                       AmtCBP09 = g.Sum(x => x.AmtCBP09),
                       AmtCBPTo09 = g.Sum(x => x.AmtCBPTo09),
                       AmtCAS09 = g.Sum(x => x.AmtCAS09),
                       AmtCASTo09 = g.Sum(x => x.AmtCASTo09),
                       AmtRemain09 = g.Sum(x => x.AmtRemain09),
                       AmtRemainTo09 = g.Sum(x => x.AmtRemainTo09),
                       AmtJN09 = g.Sum(x => x.AmtJN09),

                       AmtCBP10 = g.Sum(x => x.AmtCBP10),
                       AmtCBPTo10 = g.Sum(x => x.AmtCBPTo10),
                       AmtCAS10 = g.Sum(x => x.AmtCAS10),
                       AmtCASTo10 = g.Sum(x => x.AmtCASTo10),
                       AmtRemain10 = g.Sum(x => x.AmtRemain10),
                       AmtRemainTo10 = g.Sum(x => x.AmtRemainTo10),
                       AmtJN10 = g.Sum(x => x.AmtJN10),

                       AmtCBP11 = g.Sum(x => x.AmtCBP11),
                       AmtCBPTo11 = g.Sum(x => x.AmtCBPTo11),
                       AmtCAS11 = g.Sum(x => x.AmtCAS11),
                       AmtCASTo11 = g.Sum(x => x.AmtCASTo11),
                       AmtRemain11 = g.Sum(x => x.AmtRemain11),
                       AmtRemainTo11 = g.Sum(x => x.AmtRemainTo11),
                       AmtJN11 = g.Sum(x => x.AmtJN11),

                       AmtCBP12 = g.Sum(x => x.AmtCBP12),
                       AmtCBPTo12 = g.Sum(x => x.AmtCBPTo12),
                       AmtCAS12 = g.Sum(x => x.AmtCAS12),
                       AmtCASTo12 = g.Sum(x => x.AmtCASTo12),
                       AmtRemain12 = g.Sum(x => x.AmtRemain12),
                       AmtRemainTo12 = g.Sum(x => x.AmtRemainTo12),
                       AmtJN12 = g.Sum(x => x.AmtJN12),

                       AmtCBPQ1 = g.Sum(x => x.AmtCBPQ1),
                       AmtCBPToQ1 = g.Sum(x => x.AmtCBPToQ1),
                       AmtCASQ1 = g.Sum(x => x.AmtCASQ1),
                       AmtCASToQ1 = g.Sum(x => x.AmtCASToQ1),
                       AmtRemainQ1 = g.Sum(x => x.AmtRemainQ1),
                       AmtRemainToQ1 = g.Sum(x => x.AmtRemainToQ1),
                       AmtJNQ1 = g.Sum(x => x.AmtJNQ1),

                       AmtCBPQ2 = g.Sum(x => x.AmtCBPQ2),
                       AmtCBPToQ2 = g.Sum(x => x.AmtCBPToQ2),
                       AmtCASQ2 = g.Sum(x => x.AmtCASQ2),
                       AmtCASToQ2 = g.Sum(x => x.AmtCASToQ2),
                       AmtRemainQ2 = g.Sum(x => x.AmtRemainQ2),
                       AmtRemainToQ2 = g.Sum(x => x.AmtRemainToQ2),
                       AmtJNQ2 = g.Sum(x => x.AmtJNQ2),

                       AmtCBPQ3 = g.Sum(x => x.AmtCBPQ3),
                       AmtCBPToQ3 = g.Sum(x => x.AmtCBPToQ3),
                       AmtCASQ3 = g.Sum(x => x.AmtCASQ3),
                       AmtCASToQ3 = g.Sum(x => x.AmtCASToQ3),
                       AmtRemainQ3 = g.Sum(x => x.AmtRemainQ3),
                       AmtRemainToQ3 = g.Sum(x => x.AmtRemainToQ3),
                       AmtJNQ3 = g.Sum(x => x.AmtJNQ3),

                       AmtCBPQ4 = g.Sum(x => x.AmtCBPQ4),
                       AmtCBPToQ4 = g.Sum(x => x.AmtCBPToQ4),
                       AmtCASQ4 = g.Sum(x => x.AmtCASQ4),
                       AmtCASToQ4 = g.Sum(x => x.AmtCASToQ4),
                       AmtRemainQ4 = g.Sum(x => x.AmtRemainQ4),
                       AmtRemainToQ4 = g.Sum(x => x.AmtRemainToQ4),
                       AmtJNQ4 = g.Sum(x => x.AmtJNQ4)
                   });

            foreach (var i in l)
            {
                l2.Add(new CBP2()
                {
                    AcNo = i.AcNo,
                    AcDesc = i.AcDesc,
                    Parent = i.Parent,
                    Level = i.Level,

                    AmtCBP = i.AmtCBP,
                    AmtCAS = i.AmtCAS,
                    AmtRemain = i.AmtRemain,
                    AmtJN = i.AmtJN,

                    AmtCBP01 = i.AmtCBP01,
                    AmtCBPTo01 = i.AmtCBP01,
                    AmtCAS01 = i.AmtCAS01,
                    AmtCASTo01 = i.AmtCASTo01,
                    AmtRemain01 = i.AmtRemain01,
                    AmtRemainTo01 = i.AmtRemainTo01,
                    AmtJN01 = i.AmtJN01,

                    AmtCBP02 = i.AmtCBP02,
                    AmtCBPTo02 = i.AmtCBPTo02,
                    AmtCAS02 = i.AmtCAS02,
                    AmtCASTo02 = i.AmtCASTo02,
                    AmtRemain02 = i.AmtRemain02,
                    AmtRemainTo02 = i.AmtRemainTo02,
                    AmtJN02 = i.AmtJN02,

                    AmtCBP03 = i.AmtCBP03,
                    AmtCBPTo03 = i.AmtCBPTo03,
                    AmtCAS03 = i.AmtCAS03,
                    AmtCASTo03 = i.AmtCASTo03,
                    AmtRemain03 = i.AmtRemain03,
                    AmtRemainTo03 = i.AmtRemainTo03,
                    AmtJN03 = i.AmtJN03,

                    AmtCBP04 = i.AmtCBP04,
                    AmtCBPTo04 = i.AmtCBPTo04,
                    AmtCAS04 = i.AmtCAS04,
                    AmtCASTo04 = i.AmtCASTo04,
                    AmtRemain04 = i.AmtRemain04,
                    AmtRemainTo04 = i.AmtRemainTo04,
                    AmtJN04 = i.AmtJN04,

                    AmtCBP05 = i.AmtCBP05,
                    AmtCBPTo05 = i.AmtCBPTo05,
                    AmtCAS05 = i.AmtCAS05,
                    AmtCASTo05 = i.AmtCASTo05,
                    AmtRemain05 = i.AmtRemain05,
                    AmtRemainTo05 = i.AmtRemainTo05,
                    AmtJN05 = i.AmtJN05,

                    AmtCBP06 = i.AmtCBP06,
                    AmtCBPTo06 = i.AmtCBPTo06,
                    AmtCAS06 = i.AmtCAS06,
                    AmtCASTo06 = i.AmtCASTo06,
                    AmtRemain06 = i.AmtRemain06,
                    AmtRemainTo06 = i.AmtRemainTo06,
                    AmtJN06 = i.AmtJN06,

                    AmtCBP07 = i.AmtCBP07,
                    AmtCBPTo07 = i.AmtCBPTo07,
                    AmtCAS07 = i.AmtCAS07,
                    AmtCASTo07 = i.AmtCASTo07,
                    AmtRemain07 = i.AmtRemain07,
                    AmtRemainTo07 = i.AmtRemainTo07,
                    AmtJN07 = i.AmtJN07,

                    AmtCBP08 = i.AmtCBP08,
                    AmtCBPTo08 = i.AmtCBPTo08,
                    AmtCAS08 = i.AmtCAS08,
                    AmtCASTo08 = i.AmtCASTo08,
                    AmtRemain08 = i.AmtRemain08,
                    AmtRemainTo08 = i.AmtRemainTo08,
                    AmtJN08 = i.AmtJN08,

                    AmtCBP09 = i.AmtCBP09,
                    AmtCBPTo09 = i.AmtCBPTo09,
                    AmtCAS09 = i.AmtCAS09,
                    AmtCASTo09 = i.AmtCASTo09,
                    AmtRemain09 = i.AmtRemain09,
                    AmtRemainTo09 = i.AmtRemainTo09,
                    AmtJN09 = i.AmtJN09,

                    AmtCBP10 = i.AmtCBP10,
                    AmtCBPTo10 = i.AmtCBPTo10,
                    AmtCAS10 = i.AmtCAS10,
                    AmtCASTo10 = i.AmtCASTo10,
                    AmtRemain10 = i.AmtRemain10,
                    AmtRemainTo10 = i.AmtRemainTo10,
                    AmtJN10 = i.AmtJN10,

                    AmtCBP11 = i.AmtCBP11,
                    AmtCBPTo11 = i.AmtCBPTo11,
                    AmtCAS11 = i.AmtCAS11,
                    AmtCASTo11 = i.AmtCASTo11,
                    AmtRemain11 = i.AmtRemain11,
                    AmtRemainTo11 = i.AmtRemainTo11,
                    AmtJN11 = i.AmtJN11,

                    AmtCBP12 = i.AmtCBP12,
                    AmtCBPTo12 = i.AmtCBPTo12,
                    AmtCAS12 = i.AmtCAS12,
                    AmtCASTo12 = i.AmtCASTo12,
                    AmtRemain12 = i.AmtRemain12,
                    AmtRemainTo12 = i.AmtRemainTo12,
                    AmtJN12 = i.AmtJN12,

                    AmtCBPQ1 = i.AmtCBPQ1,
                    AmtCBPToQ1 = i.AmtCBPToQ1,
                    AmtCASQ1 = i.AmtCASQ1,
                    AmtCASToQ1 = i.AmtCASToQ1,
                    AmtRemainQ1 = i.AmtRemainQ1,
                    AmtRemainToQ1 = i.AmtRemainToQ1,
                    AmtJNQ1 = i.AmtJNQ1,

                    AmtCBPQ2 = i.AmtCBPQ2,
                    AmtCBPToQ2 = i.AmtCBPToQ2,
                    AmtCASQ2 = i.AmtCASQ2,
                    AmtCASToQ2 = i.AmtCASToQ2,
                    AmtRemainQ2 = i.AmtRemainQ2,
                    AmtRemainToQ2 = i.AmtRemainToQ2,
                    AmtJNQ2 = i.AmtJNQ2,

                    AmtCBPQ3 = i.AmtCBPQ3,
                    AmtCBPToQ3 = i.AmtCBPToQ3,
                    AmtCASQ3 = i.AmtCASQ3,
                    AmtCASToQ3 = i.AmtCASToQ3,
                    AmtRemainQ3 = i.AmtRemainQ3,
                    AmtRemainToQ3 = i.AmtRemainToQ3,
                    AmtJNQ3 = i.AmtJNQ3,

                    AmtCBPQ4 = i.AmtCBPQ4,
                    AmtCBPToQ4 = i.AmtCBPToQ4,
                    AmtCASQ4 = i.AmtCASQ4,
                    AmtCASToQ4 = i.AmtCASToQ4,
                    AmtRemainQ4 = i.AmtRemainQ4,
                    AmtRemainToQ4 = i.AmtRemainToQ4,
                    AmtJNQ4 = i.AmtJNQ4
                });
            }
        }

        private void CalculateParent(ref List<CBP2> l)
        {
            string MaxLevel = Sm.GetValue("Select Max(Level) From TblCOA; ");
            int mMaxLevel = 0;
            if (MaxLevel.Length > 0) mMaxLevel = Int32.Parse(MaxLevel);
            string Parent = string.Empty;
            decimal
                AmtCBP = 0m,
                AmtCAS = 0m,
                AmtRemain = 0m,
                AmtCBP01 = 0m,
                AmtCAS01 = 0m,
                AmtRemain01 = 0m,
                AmtCBP02 = 0m,
                AmtCAS02 = 0m,
                AmtRemain02 = 0m,
                AmtCBP03 = 0m,
                AmtCAS03 = 0m,
                AmtRemain03 = 0m,
                AmtCBPQ1 = 0m,
                AmtCASQ1 = 0m,
                AmtRemainQ1 = 0m,
                AmtCBP04 = 0m,
                AmtCAS04 = 0m,
                AmtRemain04 = 0m,
                AmtCBP05 = 0m,
                AmtCAS05 = 0m,
                AmtRemain05 = 0m,
                AmtCBP06 = 0m,
                AmtCAS06 = 0m,
                AmtRemain06 = 0m,
                AmtCBPQ2 = 0m,
                AmtCASQ2 = 0m,
                AmtRemainQ2 = 0m,
                AmtCBP07 = 0m,
                AmtCAS07 = 0m,
                AmtRemain07 = 0m,
                AmtCBP08 = 0m,
                AmtCAS08 = 0m,
                AmtRemain08 = 0m,
                AmtCBP09 = 0m,
                AmtCAS09 = 0m,
                AmtRemain09 = 0m,
                AmtCBPQ3 = 0m,
                AmtCASQ3 = 0m,
                AmtRemainQ3 = 0m,
                AmtCBP10 = 0m,
                AmtCAS10 = 0m,
                AmtRemain10 = 0m,
                AmtCBP11 = 0m,
                AmtCAS11 = 0m,
                AmtRemain11 = 0m,
                AmtCBP12 = 0m,
                AmtCAS12 = 0m,
                AmtRemain12 = 0m,
                AmtCBPQ4 = 0m,
                AmtCASQ4 = 0m,
                AmtRemainQ4 = 0m,
                AmtCBPTo01 = 0m,
                AmtCASTo01 = 0m,
                AmtRemainTo01 = 0m,
                AmtCBPTo02 = 0m,
                AmtCASTo02 = 0m,
                AmtRemainTo02 = 0m,
                AmtCBPTo03 = 0m,
                AmtCASTo03 = 0m,
                AmtRemainTo03 = 0m,
                AmtCBPToQ1 = 0m,
                AmtCASToQ1 = 0m,
                AmtRemainToQ1 = 0m,
                AmtCBPTo04 = 0m,
                AmtCASTo04 = 0m,
                AmtRemainTo04 = 0m,
                AmtCBPTo05 = 0m,
                AmtCASTo05 = 0m,
                AmtRemainTo05 = 0m,
                AmtCBPTo06 = 0m,
                AmtCASTo06 = 0m,
                AmtRemainTo06 = 0m,
                AmtCBPToQ2 = 0m,
                AmtCASToQ2 = 0m,
                AmtRemainToQ2 = 0m,
                AmtCBPTo07 = 0m,
                AmtCASTo07 = 0m,
                AmtRemainTo07 = 0m,
                AmtCBPTo08 = 0m,
                AmtCASTo08 = 0m,
                AmtRemainTo08 = 0m,
                AmtCBPTo09 = 0m,
                AmtCASTo09 = 0m,
                AmtRemainTo09 = 0m,
                AmtCBPToQ3 = 0m,
                AmtCASToQ3 = 0m,
                AmtRemainToQ3 = 0m,
                AmtCBPTo10 = 0m,
                AmtCASTo10 = 0m,
                AmtRemainTo10 = 0m,
                AmtCBPTo11 = 0m,
                AmtCASTo11 = 0m,
                AmtRemainTo11 = 0m,
                AmtCBPTo12 = 0m,
                AmtCASTo12 = 0m,
                AmtRemainTo12 = 0m,
                AmtCBPToQ4 = 0m,
                AmtCASToQ4 = 0m,
                AmtRemainToQ4 = 0m
                ;

            for (int i = mMaxLevel; i > 0; --i)
            {
                foreach (var x in l.Where(w => w.Level == i).OrderBy(o => o.AcNo))
                {
                    if (Parent.Length == 0) Parent = x.Parent;

                    if (Parent == x.Parent)
                    {
                        AmtCBP += x.AmtCBP;
                        AmtCAS += x.AmtCAS;
                        AmtRemain += x.AmtRemain;
                        AmtCBP01 += x.AmtCBP01;
                        AmtCAS01 += x.AmtCAS01;
                        AmtRemain01 += x.AmtRemain01;
                        AmtCBP02 += x.AmtCBP02;
                        AmtCAS02 += x.AmtCAS02;
                        AmtRemain02 += x.AmtRemain02;
                        AmtCBP03 += x.AmtCBP03;
                        AmtCAS03 += x.AmtCAS03;
                        AmtRemain03 += x.AmtRemain03;
                        AmtCBPQ1 += x.AmtCBPQ1;
                        AmtCASQ1 += x.AmtCASQ1;
                        AmtRemainQ1 += x.AmtRemainQ1;
                        AmtCBP04 += x.AmtCBP04;
                        AmtCAS04 += x.AmtCAS04;
                        AmtRemain04 += x.AmtRemain04;
                        AmtCBP05 += x.AmtCBP05;
                        AmtCAS05 += x.AmtCAS05;
                        AmtRemain05 += x.AmtRemain05;
                        AmtCBP06 += x.AmtCBP06;
                        AmtCAS06 += x.AmtCAS06;
                        AmtRemain06 += x.AmtRemain06;
                        AmtCBPQ2 += x.AmtCBPQ2;
                        AmtCASQ2 += x.AmtCASQ2;
                        AmtRemainQ2 += x.AmtRemainQ2;
                        AmtCBP07 += x.AmtCBP07;
                        AmtCAS07 += x.AmtCAS07;
                        AmtRemain07 += x.AmtRemain07;
                        AmtCBP08 += x.AmtCBP08;
                        AmtCAS08 += x.AmtCAS08;
                        AmtRemain08 += x.AmtRemain08;
                        AmtCBP09 += x.AmtCBP09;
                        AmtCAS09 += x.AmtCAS09;
                        AmtRemain09 += x.AmtRemain09;
                        AmtCBPQ3 += x.AmtCBPQ3;
                        AmtCASQ3 += x.AmtCASQ3;
                        AmtRemainQ3 += x.AmtRemainQ3;
                        AmtCBP10 += x.AmtCBP10;
                        AmtCAS10 += x.AmtCAS10;
                        AmtRemain10 += x.AmtRemain10;
                        AmtCBP11 += x.AmtCBP11;
                        AmtCAS11 += x.AmtCAS11;
                        AmtRemain11 += x.AmtRemain11;
                        AmtCBP12 += x.AmtCBP12;
                        AmtCAS12 += x.AmtCAS12;
                        AmtRemain12 += x.AmtRemain12;
                        AmtCBPQ4 += x.AmtCBPQ4;
                        AmtCASQ4 += x.AmtCASQ4;
                        AmtRemainQ4 += x.AmtRemainQ4;
                        AmtCBPTo01 += x.AmtCBPTo01;
                        AmtCASTo01 += x.AmtCASTo01;
                        AmtRemainTo01 += x.AmtRemainTo01;
                        AmtCBPTo02 += x.AmtCBPTo02;
                        AmtCASTo02 += x.AmtCASTo02;
                        AmtRemainTo02 += x.AmtRemainTo02;
                        AmtCBPTo03 += x.AmtCBPTo03;
                        AmtCASTo03 += x.AmtCASTo03;
                        AmtRemainTo03 += x.AmtRemainTo03;
                        AmtCBPToQ1 += x.AmtCBPToQ1;
                        AmtCASToQ1 += x.AmtCASToQ1;
                        AmtRemainToQ1 += x.AmtRemainToQ1;
                        AmtCBPTo04 += x.AmtCBPTo04;
                        AmtCASTo04 += x.AmtCASTo04;
                        AmtRemainTo04 += x.AmtRemainTo04;
                        AmtCBPTo05 += x.AmtCBPTo05;
                        AmtCASTo05 += x.AmtCASTo05;
                        AmtRemainTo05 += x.AmtRemainTo05;
                        AmtCBPTo06 += x.AmtCBPTo06;
                        AmtCASTo06 += x.AmtCASTo06;
                        AmtRemainTo06 += x.AmtRemainTo06;
                        AmtCBPToQ2 += x.AmtCBPToQ2;
                        AmtCASToQ2 += x.AmtCASToQ2;
                        AmtRemainToQ2 += x.AmtRemainToQ2;
                        AmtCBPTo07 += x.AmtCBPTo07;
                        AmtCASTo07 += x.AmtCASTo07;
                        AmtRemainTo07 += x.AmtRemainTo07;
                        AmtCBPTo08 += x.AmtCBPTo08;
                        AmtCASTo08 += x.AmtCASTo08;
                        AmtRemainTo08 += x.AmtRemainTo08;
                        AmtCBPTo09 += x.AmtCBPTo09;
                        AmtCASTo09 += x.AmtCASTo09;
                        AmtRemainTo09 += x.AmtRemainTo09;
                        AmtCBPToQ3 += x.AmtCBPToQ3;
                        AmtCASToQ3 += x.AmtCASToQ3;
                        AmtRemainToQ3 += x.AmtRemainToQ3;
                        AmtCBPTo10 += x.AmtCBPTo10;
                        AmtCASTo10 += x.AmtCASTo10;
                        AmtRemainTo10 += x.AmtRemainTo10;
                        AmtCBPTo11 += x.AmtCBPTo11;
                        AmtCASTo11 += x.AmtCASTo11;
                        AmtRemainTo11 += x.AmtRemainTo11;
                        AmtCBPTo12 += x.AmtCBPTo12;
                        AmtCASTo12 += x.AmtCASTo12;
                        AmtRemainTo12 += x.AmtRemainTo12;
                        AmtCBPToQ4 += x.AmtCBPToQ4;
                        AmtCASToQ4 += x.AmtCASToQ4;
                        AmtRemainToQ4 += x.AmtRemainToQ4;
                    }
                    else
                    {
                        foreach (var y in l.Where(w => w.AcNo == Parent))
                        {
                            y.AmtCBP = AmtCBP;
                            y.AmtCAS = AmtCAS;
                            y.AmtRemain = AmtRemain;
                            y.AmtCBP01 = AmtCBP01;
                            y.AmtCAS01 = AmtCAS01;
                            y.AmtRemain01 = AmtRemain01;
                            y.AmtCBP02 = AmtCBP02;
                            y.AmtCAS02 = AmtCAS02;
                            y.AmtRemain02 = AmtRemain02;
                            y.AmtCBP03 = AmtCBP03;
                            y.AmtCAS03 = AmtCAS03;
                            y.AmtRemain03 = AmtRemain03;
                            y.AmtCBPQ1 = AmtCBPQ1;
                            y.AmtCASQ1 = AmtCASQ1;
                            y.AmtRemainQ1 = AmtRemainQ1;
                            y.AmtCBP04 = AmtCBP04;
                            y.AmtCAS04 = AmtCAS04;
                            y.AmtRemain04 = AmtRemain04;
                            y.AmtCBP05 = AmtCBP05;
                            y.AmtCAS05 = AmtCAS05;
                            y.AmtRemain05 = AmtRemain05;
                            y.AmtCBP06 = AmtCBP06;
                            y.AmtCAS06 = AmtCAS06;
                            y.AmtRemain06 = AmtRemain06;
                            y.AmtCBPQ2 = AmtCBPQ2;
                            y.AmtCASQ2 = AmtCASQ2;
                            y.AmtRemainQ2 = AmtRemainQ2;
                            y.AmtCBP07 = AmtCBP07;
                            y.AmtCAS07 = AmtCAS07;
                            y.AmtRemain07 = AmtRemain07;
                            y.AmtCBP08 = AmtCBP08;
                            y.AmtCAS08 = AmtCAS08;
                            y.AmtRemain08 = AmtRemain08;
                            y.AmtCBP09 = AmtCBP09;
                            y.AmtCAS09 = AmtCAS09;
                            y.AmtRemain09 = AmtRemain09;
                            y.AmtCBPQ3 = AmtCBPQ3;
                            y.AmtCASQ3 = AmtCASQ3;
                            y.AmtRemainQ3 = AmtRemainQ3;
                            y.AmtCBP10 = AmtCBP10;
                            y.AmtCAS10 = AmtCAS10;
                            y.AmtRemain10 = AmtRemain10;
                            y.AmtCBP11 = AmtCBP11;
                            y.AmtCAS11 = AmtCAS11;
                            y.AmtRemain11 = AmtRemain11;
                            y.AmtCBP12 = AmtCBP12;
                            y.AmtCAS12 = AmtCAS12;
                            y.AmtRemain12 = AmtRemain12;
                            y.AmtCBPQ4 = AmtCBPQ4;
                            y.AmtCASQ4 = AmtCASQ4;
                            y.AmtRemainQ4 = AmtRemainQ4;
                            y.AmtCBPTo01 = AmtCBPTo01;
                            y.AmtCASTo01 = AmtCASTo01;
                            y.AmtRemainTo01 = AmtRemainTo01;
                            y.AmtCBPTo02 = AmtCBPTo02;
                            y.AmtCASTo02 = AmtCASTo02;
                            y.AmtRemainTo02 = AmtRemainTo02;
                            y.AmtCBPTo03 = AmtCBPTo03;
                            y.AmtCASTo03 = AmtCASTo03;
                            y.AmtRemainTo03 = AmtRemainTo03;
                            y.AmtCBPToQ1 = AmtCBPToQ1;
                            y.AmtCASToQ1 = AmtCASToQ1;
                            y.AmtRemainToQ1 = AmtRemainToQ1;
                            y.AmtCBPTo04 = AmtCBPTo04;
                            y.AmtCASTo04 = AmtCASTo04;
                            y.AmtRemainTo04 = AmtRemainTo04;
                            y.AmtCBPTo05 = AmtCBPTo05;
                            y.AmtCASTo05 = AmtCASTo05;
                            y.AmtRemainTo05 = AmtRemainTo05;
                            y.AmtCBPTo06 = AmtCBPTo06;
                            y.AmtCASTo06 = AmtCASTo06;
                            y.AmtRemainTo06 = AmtRemainTo06;
                            y.AmtCBPToQ2 = AmtCBPToQ2;
                            y.AmtCASToQ2 = AmtCASToQ2;
                            y.AmtRemainToQ2 = AmtRemainToQ2;
                            y.AmtCBPTo07 = AmtCBPTo07;
                            y.AmtCASTo07 = AmtCASTo07;
                            y.AmtRemainTo07 = AmtRemainTo07;
                            y.AmtCBPTo08 = AmtCBPTo08;
                            y.AmtCASTo08 = AmtCASTo08;
                            y.AmtRemainTo08 = AmtRemainTo08;
                            y.AmtCBPTo09 = AmtCBPTo09;
                            y.AmtCASTo09 = AmtCASTo09;
                            y.AmtRemainTo09 = AmtRemainTo09;
                            y.AmtCBPToQ3 = AmtCBPToQ3;
                            y.AmtCASToQ3 = AmtCASToQ3;
                            y.AmtRemainToQ3 = AmtRemainToQ3;
                            y.AmtCBPTo10 = AmtCBPTo10;
                            y.AmtCASTo10 = AmtCASTo10;
                            y.AmtRemainTo10 = AmtRemainTo10;
                            y.AmtCBPTo11 = AmtCBPTo11;
                            y.AmtCASTo11 = AmtCASTo11;
                            y.AmtRemainTo11 = AmtRemainTo11;
                            y.AmtCBPTo12 = AmtCBPTo12;
                            y.AmtCASTo12 = AmtCASTo12;
                            y.AmtRemainTo12 = AmtRemainTo12;
                            y.AmtCBPToQ4 = AmtCBPToQ4;
                            y.AmtCASToQ4 = AmtCASToQ4;
                            y.AmtRemainToQ4 = AmtRemainToQ4;
                        }

                        AmtCBP = x.AmtCBP;
                        AmtCAS = x.AmtCAS;
                        AmtRemain = x.AmtRemain;
                        AmtCBP01 = x.AmtCBP01;
                        AmtCAS01 = x.AmtCAS01;
                        AmtRemain01 = x.AmtRemain01;
                        AmtCBP02 = x.AmtCBP02;
                        AmtCAS02 = x.AmtCAS02;
                        AmtRemain02 = x.AmtRemain02;
                        AmtCBP03 = x.AmtCBP03;
                        AmtCAS03 = x.AmtCAS03;
                        AmtRemain03 = x.AmtRemain03;
                        AmtCBPQ1 = x.AmtCBPQ1;
                        AmtCASQ1 = x.AmtCASQ1;
                        AmtRemainQ1 = x.AmtRemainQ1;
                        AmtCBP04 = x.AmtCBP04;
                        AmtCAS04 = x.AmtCAS04;
                        AmtRemain04 = x.AmtRemain04;
                        AmtCBP05 = x.AmtCBP05;
                        AmtCAS05 = x.AmtCAS05;
                        AmtRemain05 = x.AmtRemain05;
                        AmtCBP06 = x.AmtCBP06;
                        AmtCAS06 = x.AmtCAS06;
                        AmtRemain06 = x.AmtRemain06;
                        AmtCBPQ2 = x.AmtCBPQ2;
                        AmtCASQ2 = x.AmtCASQ2;
                        AmtRemainQ2 = x.AmtRemainQ2;
                        AmtCBP07 = x.AmtCBP07;
                        AmtCAS07 = x.AmtCAS07;
                        AmtRemain07 = x.AmtRemain07;
                        AmtCBP08 = x.AmtCBP08;
                        AmtCAS08 = x.AmtCAS08;
                        AmtRemain08 = x.AmtRemain08;
                        AmtCBP09 = x.AmtCBP09;
                        AmtCAS09 = x.AmtCAS09;
                        AmtRemain09 = x.AmtRemain09;
                        AmtCBPQ3 = x.AmtCBPQ3;
                        AmtCASQ3 = x.AmtCASQ3;
                        AmtRemainQ3 = x.AmtRemainQ3;
                        AmtCBP10 = x.AmtCBP10;
                        AmtCAS10 = x.AmtCAS10;
                        AmtRemain10 = x.AmtRemain10;
                        AmtCBP11 = x.AmtCBP11;
                        AmtCAS11 = x.AmtCAS11;
                        AmtRemain11 = x.AmtRemain11;
                        AmtCBP12 = x.AmtCBP12;
                        AmtCAS12 = x.AmtCAS12;
                        AmtRemain12 = x.AmtRemain12;
                        AmtCBPQ4 = x.AmtCBPQ4;
                        AmtCASQ4 = x.AmtCASQ4;
                        AmtRemainQ4 = x.AmtRemainQ4;
                        AmtCBPTo01 = x.AmtCBPTo01;
                        AmtCASTo01 = x.AmtCASTo01;
                        AmtRemainTo01 = x.AmtRemainTo01;
                        AmtCBPTo02 = x.AmtCBPTo02;
                        AmtCASTo02 = x.AmtCASTo02;
                        AmtRemainTo02 = x.AmtRemainTo02;
                        AmtCBPTo03 = x.AmtCBPTo03;
                        AmtCASTo03 = x.AmtCASTo03;
                        AmtRemainTo03 = x.AmtRemainTo03;
                        AmtCBPToQ1 = x.AmtCBPToQ1;
                        AmtCASToQ1 = x.AmtCASToQ1;
                        AmtRemainToQ1 = x.AmtRemainToQ1;
                        AmtCBPTo04 = x.AmtCBPTo04;
                        AmtCASTo04 = x.AmtCASTo04;
                        AmtRemainTo04 = x.AmtRemainTo04;
                        AmtCBPTo05 = x.AmtCBPTo05;
                        AmtCASTo05 = x.AmtCASTo05;
                        AmtRemainTo05 = x.AmtRemainTo05;
                        AmtCBPTo06 = x.AmtCBPTo06;
                        AmtCASTo06 = x.AmtCASTo06;
                        AmtRemainTo06 = x.AmtRemainTo06;
                        AmtCBPToQ2 = x.AmtCBPToQ2;
                        AmtCASToQ2 = x.AmtCASToQ2;
                        AmtRemainToQ2 = x.AmtRemainToQ2;
                        AmtCBPTo07 = x.AmtCBPTo07;
                        AmtCASTo07 = x.AmtCASTo07;
                        AmtRemainTo07 = x.AmtRemainTo07;
                        AmtCBPTo08 = x.AmtCBPTo08;
                        AmtCASTo08 = x.AmtCASTo08;
                        AmtRemainTo08 = x.AmtRemainTo08;
                        AmtCBPTo09 = x.AmtCBPTo09;
                        AmtCASTo09 = x.AmtCASTo09;
                        AmtRemainTo09 = x.AmtRemainTo09;
                        AmtCBPToQ3 = x.AmtCBPToQ3;
                        AmtCASToQ3 = x.AmtCASToQ3;
                        AmtRemainToQ3 = x.AmtRemainToQ3;
                        AmtCBPTo10 = x.AmtCBPTo10;
                        AmtCASTo10 = x.AmtCASTo10;
                        AmtRemainTo10 = x.AmtRemainTo10;
                        AmtCBPTo11 = x.AmtCBPTo11;
                        AmtCASTo11 = x.AmtCASTo11;
                        AmtRemainTo11 = x.AmtRemainTo11;
                        AmtCBPTo12 = x.AmtCBPTo12;
                        AmtCASTo12 = x.AmtCASTo12;
                        AmtRemainTo12 = x.AmtRemainTo12;
                        AmtCBPToQ4 = x.AmtCBPToQ4;
                        AmtCASToQ4 = x.AmtCASToQ4;
                        AmtRemainToQ4 = x.AmtRemainToQ4;
                        Parent = x.Parent;
                    }
                }
            }
        }

        private void ShowData(ref List<CBP2> l)
        {
            Sm.ClearGrd(Grd1, false);

            Grd1.BeginUpdate();
            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                Grd1.Rows.Add();

                #region general
                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                #endregion

                #region year
                Grd1.Cells[Grd1.Rows.Count - 1, 4 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 5 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 6 + 3].Value = x.AmtCBP;
                Grd1.Cells[Grd1.Rows.Count - 1, 7 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 8 + 3].Value = x.AmtCAS;
                Grd1.Cells[Grd1.Rows.Count - 1, 9 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 10 + 3].Value = x.AmtRemain;
                #endregion

                #region januari
                Grd1.Cells[Grd1.Rows.Count - 1, 11 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 12 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 13 + 3].Value = x.AmtCBP01;
                Grd1.Cells[Grd1.Rows.Count - 1, 14 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 15 + 3].Value = x.AmtCBPTo01;
                Grd1.Cells[Grd1.Rows.Count - 1, 16 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 17 + 3].Value = x.AmtCAS01;
                Grd1.Cells[Grd1.Rows.Count - 1, 18 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 19 + 3].Value = x.AmtCASTo01;
                Grd1.Cells[Grd1.Rows.Count - 1, 20 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 21 + 3].Value = x.AmtRemain01;
                Grd1.Cells[Grd1.Rows.Count - 1, 22 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 23 + 3].Value = x.AmtRemainTo01;
                #endregion

                #region februari
                Grd1.Cells[Grd1.Rows.Count - 1, 24 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 25 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 26 + 3].Value = x.AmtCBP02;
                Grd1.Cells[Grd1.Rows.Count - 1, 27 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 28 + 3].Value = x.AmtCBPTo02;
                Grd1.Cells[Grd1.Rows.Count - 1, 29 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 30 + 3].Value = x.AmtCAS02;
                Grd1.Cells[Grd1.Rows.Count - 1, 31 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 32 + 3].Value = x.AmtCASTo02;
                Grd1.Cells[Grd1.Rows.Count - 1, 33 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 34 + 3].Value = x.AmtRemain02;
                Grd1.Cells[Grd1.Rows.Count - 1, 35 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 36 + 3].Value = x.AmtRemainTo02;
                #endregion

                #region maret
                Grd1.Cells[Grd1.Rows.Count - 1, 37 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 38 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 39 + 3].Value = x.AmtCBP03;
                Grd1.Cells[Grd1.Rows.Count - 1, 40 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 41 + 3].Value = x.AmtCBPTo03;
                Grd1.Cells[Grd1.Rows.Count - 1, 42 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 43 + 3].Value = x.AmtCAS03;
                Grd1.Cells[Grd1.Rows.Count - 1, 44 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 45 + 3].Value = x.AmtCASTo03;
                Grd1.Cells[Grd1.Rows.Count - 1, 46 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 47 + 3].Value = x.AmtRemain03;
                Grd1.Cells[Grd1.Rows.Count - 1, 48 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 49 + 3].Value = x.AmtRemainTo03;
                #endregion

                #region q1
                Grd1.Cells[Grd1.Rows.Count - 1, 50 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 51 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 52 + 3].Value = x.AmtCBPQ1;
                Grd1.Cells[Grd1.Rows.Count - 1, 53 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 54 + 3].Value = x.AmtCBPToQ1;
                Grd1.Cells[Grd1.Rows.Count - 1, 55 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 56 + 3].Value = x.AmtCASQ1;
                Grd1.Cells[Grd1.Rows.Count - 1, 57 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 58 + 3].Value = x.AmtCASToQ1;
                Grd1.Cells[Grd1.Rows.Count - 1, 59 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 60 + 3].Value = x.AmtRemainQ1;
                Grd1.Cells[Grd1.Rows.Count - 1, 61 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 62 + 3].Value = x.AmtRemainToQ1;
                #endregion

                #region april
                Grd1.Cells[Grd1.Rows.Count - 1, 63 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 64 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 65 + 3].Value = x.AmtCBP04;
                Grd1.Cells[Grd1.Rows.Count - 1, 66 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 67 + 3].Value = x.AmtCBPTo04;
                Grd1.Cells[Grd1.Rows.Count - 1, 68 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 69 + 3].Value = x.AmtCAS04;
                Grd1.Cells[Grd1.Rows.Count - 1, 70 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 71 + 3].Value = x.AmtCASTo04;
                Grd1.Cells[Grd1.Rows.Count - 1, 72 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 73 + 3].Value = x.AmtRemain04;
                Grd1.Cells[Grd1.Rows.Count - 1, 74 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 75 + 3].Value = x.AmtRemainTo04;
                #endregion

                #region mei
                Grd1.Cells[Grd1.Rows.Count - 1, 76 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 77 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 78 + 3].Value = x.AmtCBP05;
                Grd1.Cells[Grd1.Rows.Count - 1, 79 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 80 + 3].Value = x.AmtCBPTo05;
                Grd1.Cells[Grd1.Rows.Count - 1, 81 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 82 + 3].Value = x.AmtCAS05;
                Grd1.Cells[Grd1.Rows.Count - 1, 83 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 84 + 3].Value = x.AmtCASTo05;
                Grd1.Cells[Grd1.Rows.Count - 1, 85 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 86 + 3].Value = x.AmtRemain05;
                Grd1.Cells[Grd1.Rows.Count - 1, 87 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 88 + 3].Value = x.AmtRemainTo05;
                #endregion

                #region juni
                Grd1.Cells[Grd1.Rows.Count - 1, 89 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 90 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 91 + 3].Value = x.AmtCBP06;
                Grd1.Cells[Grd1.Rows.Count - 1, 92 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 93 + 3].Value = x.AmtCBPTo06;
                Grd1.Cells[Grd1.Rows.Count - 1, 94 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 95 + 3].Value = x.AmtCAS06;
                Grd1.Cells[Grd1.Rows.Count - 1, 96 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 97 + 3].Value = x.AmtCASTo06;
                Grd1.Cells[Grd1.Rows.Count - 1, 98 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 99 + 3].Value = x.AmtRemain06;
                Grd1.Cells[Grd1.Rows.Count - 1, 100 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 101 + 3].Value = x.AmtRemainTo06;
                #endregion

                #region q2
                Grd1.Cells[Grd1.Rows.Count - 1, 102 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 103 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 104 + 3].Value = x.AmtCBPQ2;
                Grd1.Cells[Grd1.Rows.Count - 1, 105 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 106 + 3].Value = x.AmtCBPToQ2;
                Grd1.Cells[Grd1.Rows.Count - 1, 107 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 108 + 3].Value = x.AmtCASQ2;
                Grd1.Cells[Grd1.Rows.Count - 1, 109 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 110 + 3].Value = x.AmtCASToQ2;
                Grd1.Cells[Grd1.Rows.Count - 1, 111 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 112 + 3].Value = x.AmtRemainQ2;
                Grd1.Cells[Grd1.Rows.Count - 1, 113 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 114 + 3].Value = x.AmtRemainToQ2;
                #endregion

                #region juli
                Grd1.Cells[Grd1.Rows.Count - 1, 115 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 116 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 117 + 3].Value = x.AmtCBP07;
                Grd1.Cells[Grd1.Rows.Count - 1, 118 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 119 + 3].Value = x.AmtCBPTo07;
                Grd1.Cells[Grd1.Rows.Count - 1, 120 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 121 + 3].Value = x.AmtCAS07;
                Grd1.Cells[Grd1.Rows.Count - 1, 122 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 123 + 3].Value = x.AmtCASTo07;
                Grd1.Cells[Grd1.Rows.Count - 1, 124 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 125 + 3].Value = x.AmtRemain07;
                Grd1.Cells[Grd1.Rows.Count - 1, 126 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 127 + 3].Value = x.AmtRemainTo07;
                #endregion

                #region agustus
                Grd1.Cells[Grd1.Rows.Count - 1, 128 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 129 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 130 + 3].Value = x.AmtCBP08;
                Grd1.Cells[Grd1.Rows.Count - 1, 131 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 132 + 3].Value = x.AmtCBPTo08;
                Grd1.Cells[Grd1.Rows.Count - 1, 133 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 134 + 3].Value = x.AmtCAS08;
                Grd1.Cells[Grd1.Rows.Count - 1, 135 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 136 + 3].Value = x.AmtCASTo08;
                Grd1.Cells[Grd1.Rows.Count - 1, 137 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 138 + 3].Value = x.AmtRemain08;
                Grd1.Cells[Grd1.Rows.Count - 1, 139 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 140 + 3].Value = x.AmtRemainTo08;
                #endregion

                #region september
                Grd1.Cells[Grd1.Rows.Count - 1, 141 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 142 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 143 + 3].Value = x.AmtCBP09;
                Grd1.Cells[Grd1.Rows.Count - 1, 144 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 145 + 3].Value = x.AmtCBPTo09;
                Grd1.Cells[Grd1.Rows.Count - 1, 146 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 147 + 3].Value = x.AmtCAS09;
                Grd1.Cells[Grd1.Rows.Count - 1, 148 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 149 + 3].Value = x.AmtCASTo09;
                Grd1.Cells[Grd1.Rows.Count - 1, 150 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 151 + 3].Value = x.AmtRemain09;
                Grd1.Cells[Grd1.Rows.Count - 1, 152 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 153 + 3].Value = x.AmtRemainTo09;
                #endregion

                #region q3
                Grd1.Cells[Grd1.Rows.Count - 1, 154 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 155 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 156 + 3].Value = x.AmtCBPQ3;
                Grd1.Cells[Grd1.Rows.Count - 1, 157 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 158 + 3].Value = x.AmtCBPToQ3;
                Grd1.Cells[Grd1.Rows.Count - 1, 159 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 160 + 3].Value = x.AmtCASQ3;
                Grd1.Cells[Grd1.Rows.Count - 1, 161 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 162 + 3].Value = x.AmtCASToQ3;
                Grd1.Cells[Grd1.Rows.Count - 1, 163 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 164 + 3].Value = x.AmtRemainQ3;
                Grd1.Cells[Grd1.Rows.Count - 1, 165 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 166 + 3].Value = x.AmtRemainToQ3;
                #endregion

                #region oktober
                Grd1.Cells[Grd1.Rows.Count - 1, 167 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 168 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 169 + 3].Value = x.AmtCBP10;
                Grd1.Cells[Grd1.Rows.Count - 1, 170 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 171 + 3].Value = x.AmtCBPTo10;
                Grd1.Cells[Grd1.Rows.Count - 1, 172 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 173 + 3].Value = x.AmtCAS10;
                Grd1.Cells[Grd1.Rows.Count - 1, 174 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 175 + 3].Value = x.AmtCASTo10;
                Grd1.Cells[Grd1.Rows.Count - 1, 176 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 177 + 3].Value = x.AmtRemain10;
                Grd1.Cells[Grd1.Rows.Count - 1, 178 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 179 + 3].Value = x.AmtRemainTo10;
                #endregion

                #region november
                Grd1.Cells[Grd1.Rows.Count - 1, 180 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 181 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 182 + 3].Value = x.AmtCBP11;
                Grd1.Cells[Grd1.Rows.Count - 1, 183 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 184 + 3].Value = x.AmtCBPTo11;
                Grd1.Cells[Grd1.Rows.Count - 1, 185 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 186 + 3].Value = x.AmtCAS11;
                Grd1.Cells[Grd1.Rows.Count - 1, 187 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 188 + 3].Value = x.AmtCASTo11;
                Grd1.Cells[Grd1.Rows.Count - 1, 189 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 190 + 3].Value = x.AmtRemain11;
                Grd1.Cells[Grd1.Rows.Count - 1, 191 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 192 + 3].Value = x.AmtRemainTo11;
                #endregion

                #region desember
                Grd1.Cells[Grd1.Rows.Count - 1, 193 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 194 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 195 + 3].Value = x.AmtCBP12;
                Grd1.Cells[Grd1.Rows.Count - 1, 196 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 197 + 3].Value = x.AmtCBPTo12;
                Grd1.Cells[Grd1.Rows.Count - 1, 198 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 199 + 3].Value = x.AmtCAS12;
                Grd1.Cells[Grd1.Rows.Count - 1, 200 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 201 + 3].Value = x.AmtCASTo12;
                Grd1.Cells[Grd1.Rows.Count - 1, 202 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 203 + 3].Value = x.AmtRemain12;
                Grd1.Cells[Grd1.Rows.Count - 1, 204 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 205 + 3].Value = x.AmtRemainTo12;
                #endregion

                #region q4
                Grd1.Cells[Grd1.Rows.Count - 1, 206 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 207 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 208 + 3].Value = x.AmtCBPQ4;
                Grd1.Cells[Grd1.Rows.Count - 1, 209 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 210 + 3].Value = x.AmtCBPToQ4;
                Grd1.Cells[Grd1.Rows.Count - 1, 211 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 212 + 3].Value = x.AmtCASQ4;
                Grd1.Cells[Grd1.Rows.Count - 1, 213 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 214 + 3].Value = x.AmtCASToQ4;
                Grd1.Cells[Grd1.Rows.Count - 1, 215 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 216 + 3].Value = x.AmtRemainQ4;
                Grd1.Cells[Grd1.Rows.Count - 1, 217 + 3].Value = 0m;
                Grd1.Cells[Grd1.Rows.Count - 1, 218 + 3].Value = x.AmtRemainToQ4;
                #endregion

            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Multi Filter

        private void ReloadCCCode()
        {
            CcbCCCode.Properties.Items.Clear();
            if (CcbProfitCenterCode.Text.Length > 0)
                SetCcbCCCode(ref CcbCCCode, string.Empty, GetCcbCode(CcbProfitCenterCode, "ProfitCenter"));
            else
                SetCcbCCCode(ref CcbCCCode, string.Empty, string.Empty);
        }

        private void SetCcbCCCode(ref CheckedComboBoxEdit Ccb, string CCName, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mIsRptCBPPLUseDirectorateGroupFilter && mDirectorateGroupCode.Length > 0)
                SetCostCenter();

            SQL.AppendLine("Select Col From ( ");
            if (ProfitCenterCode.Length > 0 && !ProfitCenterCode.Contains("Consolidate"))
            {
                if (!mIsRptCBPPLShowNotCompletedCBPPL)
                    SQL.AppendLine("Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode) AS Col, '02' As Col2 ");
                else
                    SQL.AppendLine("	Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode, if(B.CompletedInd = 'Y', ' (Completed)', '')) AS Col, '02' As Col2 ");
                // SQL.AppendLine("Select CCName AS Col, '02' As Col2 "); // commnet by rdh
                SQL.AppendLine("From TblCostCenter A ");
                SQL.AppendLine("LEFT JOIN (");
                SQL.AppendLine("    Select Distinct CCCode, CompletedInd From TblCompanyBudgetPlanHdr ");
                SQL.AppendLine("    Where CancelInd = 'N' ");
                SQL.AppendLine("    And CCCode Is Not Null ");
                SQL.AppendLine("    And DocType = @DocType ");
                SQL.AppendLine("    And Yr = @Yr ");
                SQL.AppendLine(")B ON A.CCCode = B.CCCode ");
                //SQL.AppendLine("Where CCCode In ( ");
                //SQL.AppendLine("    Select Distinct CCCode From TblGroupCostCenter T ");
                //SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                //SQL.AppendLine(") ");
                SQL.AppendLine("Where A.NotParentInd='Y' ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("And A.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("And (Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                //SQL.AppendLine(" OR parent IN (SELECT cccode FROM tblcostcenter WHERE Find_In_Set(ProfitCenterCode, @ProfitCenterCode)))");
                SQL.AppendLine("OR A.profitcentercode IN (SELECT b.profitcentercode FROM tblprofitcenter a");
                SQL.AppendLine("LEFT JOIN tblprofitcenter b ON b.Parent LIKE CONCAT(a.profitcentercode,'%')");
                SQL.AppendLine("WHERE a.profitcentercode = @profitcentercode)");
                SQL.AppendLine(")");
                if (mIsRptCBPPLUseDirectorateGroupFilter && mDirectorateGroupCode.Length > 0)
                {
                    var Filter_2 = string.Empty;
                    int i = 0;
                    foreach (var x in mlCostCenter.Distinct())
                    {
                        if (Filter_2.Length > 0) Filter_2 += " Or ";
                        Filter_2 += " (A.CCCode=@CCCode2_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@CCCode2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_2.Length > 0)
                        SQL.AppendLine("    And (" + Filter_2 + ") ");
                }


                SQL.AppendLine("And A.CCCode In ( ");
                SQL.AppendLine("    Select Distinct CCCode From TblCompanyBudgetPlanHdr ");
                SQL.AppendLine("    Where CancelInd = 'N' ");
                SQL.AppendLine("    And CCCode Is Not Null ");
                SQL.AppendLine("    And DocType = @DocType ");
                if(!mIsRptCBPPLShowNotCompletedCBPPL)
                    SQL.AppendLine("    And CompletedInd = 'Y' ");
                SQL.AppendLine("    And Yr = @Yr ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                //SQL.AppendLine("Select 'Consolidate' Col, '01' Col2  ");
                //SQL.AppendLine("Union All ");
                if (CCName.Length > 0)
                {
                    if (!mIsRptCBPPLShowNotCompletedCBPPL)
                        SQL.AppendLine("Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode) AS Col, '02' As Col2 ");
                    else
                        SQL.AppendLine("	Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode, if(B.CompletedInd = 'Y', ' (Completed)', '')) AS Col, '02' As Col2 ");
                    //SQL.AppendLine("Select CCName AS Col, '02' As Col2  ");
                    SQL.AppendLine("From TblCostCenter A ");
                    SQL.AppendLine("LEFT JOIN (");
                    SQL.AppendLine("    Select Distinct CCCode, CompletedInd From TblCompanyBudgetPlanHdr ");
                    SQL.AppendLine("    Where CancelInd = 'N' ");
                    SQL.AppendLine("    And CCCode Is Not Null ");
                    SQL.AppendLine("    And DocType = @DocType ");
                    SQL.AppendLine("    And Yr = @Yr ");
                    SQL.AppendLine(")B ON A.CCCode = B.CCCode ");
                    SQL.AppendLine("Where A.CCName=@Param ");
                    SQL.AppendLine("And A.NotParentInd='Y' ");
                    SQL.AppendLine("And A.ActInd='Y' ");
                    SQL.AppendLine("And A.CCCode In ( ");
                    SQL.AppendLine("    Select Distinct CCCode From TblCompanyBudgetPlanHdr ");
                    SQL.AppendLine("    Where CancelInd = 'N' ");
                    SQL.AppendLine("    And CCCode Is Not Null ");
                    SQL.AppendLine("    And DocType = @DocType ");
                    if (!mIsRptCBPPLShowNotCompletedCBPPL)
                        SQL.AppendLine("    And CompletedInd = 'Y' ");
                    SQL.AppendLine("    And Yr = @Yr ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (!mIsRptCBPPLShowNotCompletedCBPPL)
                        SQL.AppendLine("Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode) AS Col, '02' As Col2 ");
                    else
                        SQL.AppendLine("	Select CONCAT(REPLACE(A.CCName,',','-'),'-',A.CCCode, if(B.CompletedInd = 'Y', ' (Completed)', '')) AS Col, '02' As Col2 ");
                    //SQL.AppendLine("Select CCName AS Col, '02' As Col2  ");
                    SQL.AppendLine("From TblCostCenter A ");
                    SQL.AppendLine("LEFT JOIN (");
                    SQL.AppendLine("    Select Distinct CCCode, CompletedInd From TblCompanyBudgetPlanHdr ");
                    SQL.AppendLine("    Where CancelInd = 'N' ");
                    SQL.AppendLine("    And CCCode Is Not Null ");
                    SQL.AppendLine("    And DocType = @DocType ");
                    SQL.AppendLine("    And Yr = @Yr ");
                    SQL.AppendLine(")B ON A.CCCode = B.CCCode ");
                    //SQL.AppendLine("Where CCCode In ( ");
                    //SQL.AppendLine("    Select Distinct CCCode From TblGroupCostCenter T ");
                    //SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //SQL.AppendLine(") ");
                    SQL.AppendLine("Where A.ActInd='Y' ");
                    if (mIsRptCBPPLUseDirectorateGroupFilter && mDirectorateGroupCode.Length > 0)
                    {
                        var Filter_2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlCostCenter.Distinct())
                        {
                            if (Filter_2.Length > 0) Filter_2 += " Or ";
                            Filter_2 += " (A.CCCode=@CCCode2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@CCCode2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_2.Length > 0)
                            SQL.AppendLine("    And (" + Filter_2 + ") ");
                        else
                            SQL.AppendLine("    And 1 = 0 ");
                    }
                    else
                        SQL.AppendLine("And A.NotParentInd='Y' ");
                    SQL.AppendLine("And A.CCCode In ( ");
                    SQL.AppendLine("    Select Distinct CCCode From TblCompanyBudgetPlanHdr ");
                    SQL.AppendLine("    Where CancelInd = 'N' ");
                    SQL.AppendLine("    And CCCode Is Not Null ");
                    SQL.AppendLine("    And DocType = @DocType ");
                    if (!mIsRptCBPPLShowNotCompletedCBPPL)
                        SQL.AppendLine("    And CompletedInd = 'Y' ");
                    SQL.AppendLine("    And Yr = @Yr ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By Col2, Col");
            SQL.AppendLine(") Tbl ");
            if (ProfitCenterCode.Length > 0 && !ProfitCenterCode.Contains("Consolidate"))
                SQL.AppendLine("Order by Col ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@Param", CCName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            if (ProfitCenterCode.Length > 0 && !ProfitCenterCode.Contains("Consolidate"))
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
            //SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        //private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb, string ProfitCenterName)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Col From (");
        //    SQL.AppendLine("Select 'Consolidate' Col, '01' Col2  ");
        //    SQL.AppendLine("Union All ");
        //    if (ProfitCenterName.Length > 0)
        //    {
        //        SQL.AppendLine("Select ProfitCenterName AS Col, '02' As Col2 From TblProfitCenter ");
        //        SQL.AppendLine("Where ProfitCenterName=@Param ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Select ProfitCenterName AS Col, '02' As Col2 ");
        //        SQL.AppendLine("From TblProfitCenter ");
        //        SQL.AppendLine("Where ProfitCenterCode In ( ");
        //        SQL.AppendLine("    Select ProfitCenterCode ");
        //        SQL.AppendLine("    From TblCostCenter ");
        //        SQL.AppendLine("    Where ProfitCenterCode Is Not Null ");
        //        SQL.AppendLine("    And ActInd='Y' ");
        //        SQL.AppendLine("    And CCCode In ( ");
        //        SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter T ");
        //        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //        SQL.AppendLine("    ) ");
        //        SQL.AppendLine(") ");
        //    }
        //    SQL.AppendLine("Order By Col2,  Col");
        //    SQL.AppendLine(") Tbl; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Param", ProfitCenterName);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    Sm.SetCcb(ref Ccb, cm);
        //}

        //private string GetProfitCenterCode(string Value)
        //{
        //    if (Value.Length != 0)
        //    {
        //        string initValue = Value;
        //        Value = Value.Replace(", ", ",");

        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select Group_Concat(T.ProfitCenterCode Separator ', ') ProfitCenterCode ");
        //        SQL.AppendLine("From ");
        //        SQL.AppendLine("( ");
        //        SQL.AppendLine("    Select ProfitCenterCode ");
        //        SQL.AppendLine("    From TblProfitCenter ");
        //        SQL.AppendLine("    Where Find_In_Set(ProfitCenterName, @Param) ");
        //        SQL.AppendLine(")T; ");

        //        Value = Sm.GetValue(SQL.ToString(), Value);
        //        if (initValue.Contains("Consolidate"))
        //            Value = string.Concat("Consolidate, ", Value);
        //    }

        //    return Value;
        //}

        private string GetCcbCCCode()
        {
            return GetCCCode(Sm.GetCcb(CcbCCCode));
        }


        private string GetCcbCode(CheckedComboBoxEdit Ccb, string Type)
        {
            string Value = string.Empty;

            Value = Sm.GetCcb(Ccb);
            if (Value.Length > 0)
            {
                if (Type == "ProfitCenter")
                    Value = GetProfitCenterCode(Value);
                else
                    Value = GetCCCode(Value);
                Value = Value.Replace(", ", ",");
            }

            return Value;
        }

        private string ProcessCcb(string Value, string Type)
        {
            if (Value.Length != 0)
            {
                if (Type == "ProfitCenter")
                    Value = GetProfitCenterCode(Value);
                else
                    Value = GetCCCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetCCCode(string Value)
        {
            var SQL = new StringBuilder();

            //if (Value.Length != 0)
            //{
            //    string initValue = Value;
            //    Value = Value.Replace(", ", ",");

            //    SQL.AppendLine("Select Group_Concat(T.CCCode Separator ', ') CCCode ");
            //    SQL.AppendLine("From ");
            //    SQL.AppendLine("( ");
            //    SQL.AppendLine("    Select CCCode ");
            //    SQL.AppendLine("    From TblCostCenter ");
            //    SQL.AppendLine("    Where Find_In_Set(CCName, @Param) ");
            //    SQL.AppendLine("    And NotParentInd='Y' ");
            //    SQL.AppendLine("    And ActInd='Y' ");
            //    SQL.AppendLine(")T; ");

            //    Value = Sm.GetValue(SQL.ToString(), Value);
            //    if (initValue.Contains("Consolidate"))
            //        Value = string.Concat("Consolidate, ", Value);
            //}


            // Value = Value.Replace(", ", " ");  // comment by rdh
            string Value2 = string.Empty;
            if(mIsRptCBPPLShowNotCompletedCBPPL) Value = Value.Replace(" (Completed)", string.Empty);
            var mValue = Value.Split(',');

            for (int i = 0; i < mValue.Length; i++)
            {
                var lastIndex = mValue[i].LastIndexOf('-') + 1;
                var lengthIndex = mValue[i].Length;
                Value2 += mValue[i].Substring(lastIndex, (lengthIndex - lastIndex)) + ',';
            }

            return Value2.Substring(0, Value2.LastIndexOf(','));

            // comment by rdh

            //SQL.AppendLine("    Select Group_Concat(CCCode Separator ', ') CCCode ");
            //SQL.AppendLine("    From TblCostCenter ");
            //if (Value.Length > 0)
            //{
            //    SQL.AppendLine("    Where Find_In_Set(REPLACE(CCName,', ',' '), @Param) ");

            //    return Sm.GetValue(SQL.ToString(), Value);
            //}
            //else
            //{
            //    SQL.AppendLine("    Where CCCode In ( ");
            //    SQL.AppendLine("        Select Distinct CCCode From TblCompanyBudgetPlanHdr ");
            //    SQL.AppendLine("        Where CancelInd='N' ");
            //    SQL.AppendLine("        And CCCode Is Not Null ");
            //    SQL.AppendLine("        And DocType=@Param2 ");
            //    SQL.AppendLine("        And CompletedInd='Y' ");
            //    SQL.AppendLine("        And Yr=@Param1 ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine("    And NotParentInd='Y' ");
            //    SQL.AppendLine("    And ActInd='Y' ");

            //    return Sm.GetValue(SQL.ToString(), Sm.GetLue(LueYr), mDocType, string.Empty);
            //}
        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.IsDataExist("Select 1 from TblCOA Where AcNo=@Param And AcNo Not In (Select Parent from TblCOA Where parent Is Not Null);", Sm.GetGrdStr(Grd1, e.RowIndex, 1)))
                        Sm.FormShowDialog(
                        new FrmRptCompanyBudgetPlanDlg(
                            this,
                            Sm.GetGrdStr(Grd1, e.RowIndex, 1),
                            Sm.GetGrdStr(Grd1, e.RowIndex, 0)
                            ));
                    else
                        Sm.StdMsg(mMsgType.Warning, "Not available information.");
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                if (Sm.IsDataExist("Select 1 from TblCOA Where AcNo=@Param And AcNo Not In (Select Parent from TblCOA Where parent Is Not Null);", Sm.GetGrdStr(Grd1, e.RowIndex, 1)))
                    Sm.FormShowDialog(
                        new FrmRptCompanyBudgetPlanDlg(
                            this,
                            Sm.GetGrdStr(Grd1, e.RowIndex, 1),
                            Sm.GetGrdStr(Grd1, e.RowIndex, 0)
                            ));
                else
                    Sm.StdMsg(mMsgType.Warning, "Not available information.");
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterContentSorted(object sender, EventArgs e)
        {
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnRefreshCostCenter_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            ReloadCCCode();
        }

        private void BtnDirectorateGroup_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptCompanyBudgetPlanDlg2(this));
        }

        private void TxtDirectorateGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDirectorateGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkDirectorateGroup.Checked)
                mDirectorateGroupCode = string.Empty;
            Sm.FilterSetTextEdit(this, sender, "Directorate Group");
        }
        

        #endregion

        #endregion

        #region Class

        private class CBPSingle
        {
            public string AcNo { get; set; }
            public string ItCode { get; set; }
            public string CCCode { get; set; }

            public decimal Rate01 { get; set; }
            public decimal QtyCBP01 { get; set; }
            public decimal AmtCBP01 { get; set; }
            public decimal QtyCAS01 { get; set; }
            public decimal AmtCAS01 { get; set; }
            public decimal QtyJN01 { get; set; }
            public decimal AmtJN01 { get; set; }


            //02
            public decimal Rate02 { get; set; }
            public decimal QtyCBP02 { get; set; }
            public decimal AmtCBP02 { get; set; }
            public decimal QtyCAS02 { get; set; }
            public decimal AmtCAS02 { get; set; }
            public decimal QtyJN02 { get; set; }
            public decimal AmtJN02 { get; set; }

            //03
            public decimal Rate03 { get; set; }
            public decimal QtyCBP03 { get; set; }
            public decimal AmtCBP03 { get; set; }
            public decimal QtyCAS03 { get; set; }
            public decimal AmtCAS03 { get; set; }
            public decimal QtyJN03 { get; set; }
            public decimal AmtJN03 { get; set; }

            //04
            public decimal Rate04 { get; set; }
            public decimal QtyCBP04 { get; set; }
            public decimal AmtCBP04 { get; set; }
            public decimal QtyCAS04 { get; set; }
            public decimal AmtCAS04 { get; set; }
            public decimal QtyJN04 { get; set; }
            public decimal AmtJN04 { get; set; }

            //05
            public decimal Rate05 { get; set; }
            public decimal QtyCBP05 { get; set; }
            public decimal AmtCBP05 { get; set; }
            public decimal QtyCAS05 { get; set; }
            public decimal AmtCAS05 { get; set; }
            public decimal QtyJN05 { get; set; }
            public decimal AmtJN05 { get; set; }

            //06
            public decimal Rate06 { get; set; }
            public decimal QtyCBP06 { get; set; }
            public decimal AmtCBP06 { get; set; }
            public decimal QtyCAS06 { get; set; }
            public decimal AmtCAS06 { get; set; }
            public decimal QtyJN06 { get; set; }
            public decimal AmtJN06 { get; set; }

            //07
            public decimal Rate07 { get; set; }
            public decimal QtyCBP07 { get; set; }
            public decimal AmtCBP07 { get; set; }
            public decimal QtyCAS07 { get; set; }
            public decimal AmtCAS07 { get; set; }
            public decimal QtyJN07 { get; set; }
            public decimal AmtJN07 { get; set; }

            //08
            public decimal Rate08 { get; set; }
            public decimal QtyCBP08 { get; set; }
            public decimal AmtCBP08 { get; set; }
            public decimal QtyCAS08 { get; set; }
            public decimal AmtCAS08 { get; set; }
            public decimal QtyJN08 { get; set; }
            public decimal AmtJN08 { get; set; }

            //09
            public decimal Rate09 { get; set; }
            public decimal QtyCBP09 { get; set; }
            public decimal AmtCBP09 { get; set; }
            public decimal QtyCAS09 { get; set; }
            public decimal AmtCAS09 { get; set; }
            public decimal QtyJN09 { get; set; }
            public decimal AmtJN09 { get; set; }

            //10
            public decimal Rate10 { get; set; }
            public decimal QtyCBP10 { get; set; }
            public decimal AmtCBP10 { get; set; }
            public decimal QtyCAS10 { get; set; }
            public decimal AmtCAS10 { get; set; }
            public decimal QtyJN10 { get; set; }
            public decimal AmtJN10 { get; set; }

            //11
            public decimal Rate11 { get; set; }
            public decimal QtyCBP11 { get; set; }
            public decimal AmtCBP11 { get; set; }
            public decimal QtyCAS11 { get; set; }
            public decimal AmtCAS11 { get; set; }
            public decimal QtyJN11 { get; set; }
            public decimal AmtJN11 { get; set; }

            //12
            public decimal Rate12 { get; set; }
            public decimal QtyCBP12 { get; set; }
            public decimal AmtCBP12 { get; set; }
            public decimal QtyCAS12 { get; set; }
            public decimal AmtCAS12 { get; set; }
            public decimal QtyJN12 { get; set; }
            public decimal AmtJN12 { get; set; }
        }

        private class COASingle
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
        }

        private class CostCenterSingle
        {
            public string CCCode { get; set; }
            public string CCName { get; set; }
            public string DeptCode { get; set; }
        }

        private class DepartmentSingle
        {
            public string DeptCode { get; set; }
            public string DeptGrpCode { get; set; }
        }

        private class OptionSingle
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
        }

        private class ItemSingle
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string UoM { get; set; }
        }

        private class BudgetTypeSingle
        {
            public string AcNo { get; set; }
            public string BudgetType { get; set; }
        }

        private class BudgetTransferSingle
        {
            public string AcNo { get; set; }
            public string CCCode { get; set; }
            public string ItCode { get; set; }
            public decimal TransferAmt01 { get; set; }
            public decimal TransferQty01 { get; set; }
            public decimal TransferAmt02 { get; set; }
            public decimal TransferQty02 { get; set; }
            public decimal TransferAmt03 { get; set; }
            public decimal TransferQty03 { get; set; }
            public decimal TransferAmt04 { get; set; }
            public decimal TransferQty04 { get; set; }
            public decimal TransferAmt05 { get; set; }
            public decimal TransferQty05 { get; set; }
            public decimal TransferAmt06 { get; set; }
            public decimal TransferQty06 { get; set; }
            public decimal TransferAmt07 { get; set; }
            public decimal TransferQty07 { get; set; }
            public decimal TransferAmt08 { get; set; }
            public decimal TransferQty08 { get; set; }
            public decimal TransferAmt09 { get; set; }
            public decimal TransferQty09 { get; set; }
            public decimal TransferAmt10 { get; set; }
            public decimal TransferQty10 { get; set; }
            public decimal TransferAmt11 { get; set; }
            public decimal TransferQty11 { get; set; }
            public decimal TransferAmt12 { get; set; }
            public decimal TransferQty12 { get; set; }
        }
        private class BudgetReceiveSingle
        {
            public string AcNo { get; set; }
            public string CCCode { get; set; }
            public string ItCode { get; set; }
            public decimal ReceiveAmt01 { get; set; }
            public decimal ReceiveQty01 { get; set; }
            public decimal ReceiveAmt02 { get; set; }
            public decimal ReceiveQty02 { get; set; }
            public decimal ReceiveAmt03 { get; set; }
            public decimal ReceiveQty03 { get; set; }
            public decimal ReceiveAmt04 { get; set; }
            public decimal ReceiveQty04 { get; set; }
            public decimal ReceiveAmt05 { get; set; }
            public decimal ReceiveQty05 { get; set; }
            public decimal ReceiveAmt06 { get; set; }
            public decimal ReceiveQty06 { get; set; }
            public decimal ReceiveAmt07 { get; set; }
            public decimal ReceiveQty07 { get; set; }
            public decimal ReceiveAmt08 { get; set; }
            public decimal ReceiveQty08 { get; set; }
            public decimal ReceiveAmt09 { get; set; }
            public decimal ReceiveQty09 { get; set; }
            public decimal ReceiveAmt10 { get; set; }
            public decimal ReceiveQty10 { get; set; }
            public decimal ReceiveAmt11 { get; set; }
            public decimal ReceiveQty11 { get; set; }
            public decimal ReceiveAmt12 { get; set; }
            public decimal ReceiveQty12 { get; set; }
        }

        internal class CBP
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public string DeptGrpName { get; set; }
            public string DeptGrpCode { get; set; }
            public string UoM { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string CCCode { get; set; }
            public string BudgetType { get; set; }


            // 1 tahun
            public decimal Rate { get; set; }
            public decimal QtyCBP{ get; set; }
            public decimal AmtCBP { get; set; }
            public decimal QtyCAS { get; set; }
            public decimal AmtCAS { get; set; }
            public decimal QtyRemain { get; set; }
            public decimal AmtRemain { get; set; }
            public decimal QtyJN { get; set; }
            public decimal AmtJN { get; set; }

            // 01
            public decimal Rate01 { get; set; }
            public decimal QtyCBP01 { get; set; }
            public decimal AmtCBP01 { get; set; }
            public decimal QtyCBPTo01 { get; set; }
            public decimal AmtCBPTo01 { get; set; }
            public decimal QtyCAS01 { get; set; }
            public decimal AmtCAS01 { get; set; }
            public decimal QtyCASTo01 { get; set; }
            public decimal AmtCASTo01 { get; set; }
            public decimal QtyRemain01 { get; set; }
            public decimal AmtRemain01 { get; set; }
            public decimal QtyRemainTo01 { get; set; }
            public decimal AmtRemainTo01 { get; set; }
            public decimal QtyJN01 { get; set; }
            public decimal AmtJN01 { get; set; }
            

            //02
            public decimal Rate02 { get; set; }
            public decimal QtyCBP02 { get; set; }
            public decimal AmtCBP02 { get; set; }
            public decimal QtyCBPTo02 { get; set; }
            public decimal AmtCBPTo02 { get; set; }
            public decimal QtyCAS02 { get; set; }
            public decimal AmtCAS02 { get; set; }
            public decimal QtyCASTo02 { get; set; }
            public decimal AmtCASTo02 { get; set; }
            public decimal QtyRemain02 { get; set; }
            public decimal AmtRemain02 { get; set; }
            public decimal QtyRemainTo02 { get; set; }
            public decimal AmtRemainTo02 { get; set; }
            public decimal QtyJN02 { get; set; }
            public decimal AmtJN02 { get; set; }

            //03
            public decimal Rate03 { get; set; }
            public decimal QtyCBP03 { get; set; }
            public decimal AmtCBP03 { get; set; }
            public decimal QtyCBPTo03 { get; set; }
            public decimal AmtCBPTo03 { get; set; }
            public decimal QtyCAS03 { get; set; }
            public decimal AmtCAS03 { get; set; }
            public decimal QtyCASTo03 { get; set; }
            public decimal AmtCASTo03 { get; set; }
            public decimal QtyRemain03 { get; set; }
            public decimal AmtRemain03 { get; set; }
            public decimal QtyRemainTo03 { get; set; }
            public decimal AmtRemainTo03 { get; set; }
            public decimal QtyJN03 { get; set; }
            public decimal AmtJN03 { get; set; }

            //Q1
            public decimal RateQ1 { get; set; }
            public decimal QtyCBPQ1 { get; set; }
            public decimal AmtCBPQ1 { get; set; }
            public decimal QtyCBPToQ1 { get; set; }
            public decimal AmtCBPToQ1 { get; set; }
            public decimal QtyCASQ1 { get; set; }
            public decimal AmtCASQ1 { get; set; }
            public decimal QtyCASToQ1 { get; set; }
            public decimal AmtCASToQ1 { get; set; }
            public decimal QtyRemainQ1 { get; set; }
            public decimal AmtRemainQ1 { get; set; }
            public decimal QtyRemainToQ1 { get; set; }
            public decimal AmtRemainToQ1 { get; set; }
            public decimal QtyJNQ1 { get; set; }
            public decimal AmtJNQ1 { get; set; }

            //04
            public decimal Rate04 { get; set; }
            public decimal QtyCBP04 { get; set; }
            public decimal AmtCBP04 { get; set; }
            public decimal QtyCBPTo04 { get; set; }
            public decimal AmtCBPTo04 { get; set; }
            public decimal QtyCAS04 { get; set; }
            public decimal AmtCAS04 { get; set; }
            public decimal QtyCASTo04 { get; set; }
            public decimal AmtCASTo04 { get; set; }
            public decimal QtyRemain04 { get; set; }
            public decimal AmtRemain04 { get; set; }
            public decimal QtyRemainTo04 { get; set; }
            public decimal AmtRemainTo04 { get; set; }
            public decimal QtyJN04 { get; set; }
            public decimal AmtJN04 { get; set; }

            //05
            public decimal Rate05 { get; set; }
            public decimal QtyCBP05 { get; set; }
            public decimal AmtCBP05 { get; set; }
            public decimal QtyCBPTo05 { get; set; }
            public decimal AmtCBPTo05 { get; set; }
            public decimal QtyCAS05 { get; set; }
            public decimal AmtCAS05 { get; set; }
            public decimal QtyCASTo05 { get; set; }
            public decimal AmtCASTo05 { get; set; }
            public decimal QtyRemain05 { get; set; }
            public decimal AmtRemain05 { get; set; }
            public decimal QtyRemainTo05 { get; set; }
            public decimal AmtRemainTo05 { get; set; }
            public decimal QtyJN05 { get; set; }
            public decimal AmtJN05 { get; set; }

            //06
            public decimal Rate06 { get; set; }
            public decimal QtyCBP06 { get; set; }
            public decimal AmtCBP06 { get; set; }
            public decimal QtyCBPTo06 { get; set; }
            public decimal AmtCBPTo06 { get; set; }
            public decimal QtyCAS06 { get; set; }
            public decimal AmtCAS06 { get; set; }
            public decimal QtyCASTo06 { get; set; }
            public decimal AmtCASTo06 { get; set; }
            public decimal QtyRemain06 { get; set; }
            public decimal AmtRemain06 { get; set; }
            public decimal QtyRemainTo06 { get; set; }
            public decimal AmtRemainTo06 { get; set; }
            public decimal QtyJN06 { get; set; }
            public decimal AmtJN06 { get; set; }

            //Q2
            public decimal RateQ2 { get; set; }
            public decimal QtyCBPQ2 { get; set; }
            public decimal AmtCBPQ2 { get; set; }
            public decimal QtyCBPToQ2 { get; set; }
            public decimal AmtCBPToQ2 { get; set; }
            public decimal QtyCASQ2 { get; set; }
            public decimal AmtCASQ2 { get; set; }
            public decimal QtyCASToQ2 { get; set; }
            public decimal AmtCASToQ2 { get; set; }
            public decimal QtyRemainQ2 { get; set; }
            public decimal AmtRemainQ2 { get; set; }
            public decimal QtyRemainToQ2 { get; set; }
            public decimal AmtRemainToQ2 { get; set; }
            public decimal QtyJNQ2 { get; set; }
            public decimal AmtJNQ2 { get; set; }

            //07
            public decimal Rate07 { get; set; }
            public decimal QtyCBP07 { get; set; }
            public decimal AmtCBP07 { get; set; }
            public decimal QtyCBPTo07 { get; set; }
            public decimal AmtCBPTo07 { get; set; }
            public decimal QtyCAS07 { get; set; }
            public decimal AmtCAS07 { get; set; }
            public decimal QtyCASTo07 { get; set; }
            public decimal AmtCASTo07 { get; set; }
            public decimal QtyRemain07 { get; set; }
            public decimal AmtRemain07 { get; set; }
            public decimal QtyRemainTo07 { get; set; }
            public decimal AmtRemainTo07 { get; set; }
            public decimal QtyJN07 { get; set; }
            public decimal AmtJN07 { get; set; }

            //08
            public decimal Rate08 { get; set; }
            public decimal QtyCBP08 { get; set; }
            public decimal AmtCBP08 { get; set; }
            public decimal QtyCBPTo08 { get; set; }
            public decimal AmtCBPTo08 { get; set; }
            public decimal QtyCAS08 { get; set; }
            public decimal AmtCAS08 { get; set; }
            public decimal QtyCASTo08 { get; set; }
            public decimal AmtCASTo08 { get; set; }
            public decimal QtyRemain08 { get; set; }
            public decimal AmtRemain08 { get; set; }
            public decimal QtyRemainTo08 { get; set; }
            public decimal AmtRemainTo08 { get; set; }
            public decimal QtyJN08 { get; set; }
            public decimal AmtJN08 { get; set; }

            //09
            public decimal Rate09 { get; set; }
            public decimal QtyCBP09 { get; set; }
            public decimal AmtCBP09 { get; set; }
            public decimal QtyCBPTo09 { get; set; }
            public decimal AmtCBPTo09 { get; set; }
            public decimal QtyCAS09 { get; set; }
            public decimal AmtCAS09 { get; set; }
            public decimal QtyCASTo09 { get; set; }
            public decimal AmtCASTo09 { get; set; }
            public decimal QtyRemain09 { get; set; }
            public decimal AmtRemain09 { get; set; }
            public decimal QtyRemainTo09 { get; set; }
            public decimal AmtRemainTo09 { get; set; }
            public decimal QtyJN09 { get; set; }
            public decimal AmtJN09 { get; set; }

            //Q3
            public decimal RateQ3 { get; set; }
            public decimal QtyCBPQ3 { get; set; }
            public decimal AmtCBPQ3 { get; set; }
            public decimal QtyCBPToQ3 { get; set; }
            public decimal AmtCBPToQ3 { get; set; }
            public decimal QtyCASQ3 { get; set; }
            public decimal AmtCASQ3 { get; set; }
            public decimal QtyCASToQ3 { get; set; }
            public decimal AmtCASToQ3 { get; set; }
            public decimal QtyRemainQ3 { get; set; }
            public decimal AmtRemainQ3 { get; set; }
            public decimal QtyRemainToQ3 { get; set; }
            public decimal AmtRemainToQ3 { get; set; }
            public decimal QtyJNQ3 { get; set; }
            public decimal AmtJNQ3 { get; set; }


            //10
            public decimal Rate10 { get; set; }
            public decimal QtyCBP10 { get; set; }
            public decimal AmtCBP10 { get; set; }
            public decimal QtyCBPTo10 { get; set; }
            public decimal AmtCBPTo10 { get; set; }
            public decimal QtyCAS10 { get; set; }
            public decimal AmtCAS10 { get; set; }
            public decimal QtyCASTo10 { get; set; }
            public decimal AmtCASTo10 { get; set; }
            public decimal QtyRemain10 { get; set; }
            public decimal AmtRemain10 { get; set; }
            public decimal QtyRemainTo10 { get; set; }
            public decimal AmtRemainTo10 { get; set; }
            public decimal QtyJN10 { get; set; }
            public decimal AmtJN10 { get; set; }

            //11
            public decimal Rate11 { get; set; }
            public decimal QtyCBP11 { get; set; }
            public decimal AmtCBP11 { get; set; }
            public decimal QtyCBPTo11 { get; set; }
            public decimal AmtCBPTo11 { get; set; }
            public decimal QtyCAS11 { get; set; }
            public decimal AmtCAS11 { get; set; }
            public decimal QtyCASTo11 { get; set; }
            public decimal AmtCASTo11 { get; set; }
            public decimal QtyRemain11 { get; set; }
            public decimal AmtRemain11 { get; set; }
            public decimal QtyRemainTo11 { get; set; }
            public decimal AmtRemainTo11 { get; set; }
            public decimal QtyJN11 { get; set; }
            public decimal AmtJN11 { get; set; }

            //12
            public decimal Rate12 { get; set; }
            public decimal QtyCBP12 { get; set; }
            public decimal AmtCBP12 { get; set; }
            public decimal QtyCBPTo12 { get; set; }
            public decimal AmtCBPTo12 { get; set; }
            public decimal QtyCAS12 { get; set; }
            public decimal AmtCAS12 { get; set; }
            public decimal QtyCASTo12 { get; set; }
            public decimal AmtCASTo12 { get; set; }
            public decimal QtyRemain12 { get; set; }
            public decimal AmtRemain12 { get; set; }
            public decimal QtyRemainTo12 { get; set; }
            public decimal AmtRemainTo12 { get; set; }
            public decimal QtyJN12 { get; set; }
            public decimal AmtJN12 { get; set; }

            //Q4
            public decimal RateQ4 { get; set; }
            public decimal QtyCBPQ4 { get; set; }
            public decimal AmtCBPQ4 { get; set; }
            public decimal QtyCBPToQ4 { get; set; }
            public decimal AmtCBPToQ4 { get; set; }
            public decimal QtyCASQ4 { get; set; }
            public decimal AmtCASQ4 { get; set; }
            public decimal QtyCASToQ4 { get; set; }
            public decimal AmtCASToQ4 { get; set; }
            public decimal QtyRemainQ4 { get; set; }
            public decimal AmtRemainQ4 { get; set; }
            public decimal QtyRemainToQ4 { get; set; }
            public decimal AmtRemainToQ4 { get; set; }
            public decimal QtyJNQ4 { get; set; }
            public decimal AmtJNQ4 { get; set; }

            // transfer and receive Budget transfer
            public decimal TransferAmt { get; set; }
            public decimal ReceiveAmt { get; set; }
            public string Mth { get; set; }
            public string Mth2 { get; set; }
            public string TransType { get; set; }

        }

        private class CAS
        {
            public string CCCode { get; set; }
            public string ItCode { get; set; }
            public string AcNo { get; set; }
            public string Mth { get; set; }
            public decimal Qty { get; set; }
            public decimal Rate { get; set; }
            public bool IsTaken { get; set; }
        }

        private class RecvVdAutoDO
        {
            public string CCCode { get; set; }
            public string ItCode { get; set; }
            public string AcNo { get; set; }
            public string Mth { get; set; }
            public decimal Qty { get; set; }
            public decimal Rate { get; set; }
            public bool IsTaken { get; set; }
        }

      

        private class CBPBudgetType
        {
            public string AcNo { get; set; }
            public string BudgetType { get; set;  }
        }

        private class COAJournal
        {
            public string CCCode { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string AcType { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public decimal Amt { get; set; }
            public string Mth { get; set; }
        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public string Mth { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string CCCode { get; set; }
        }

        private class CBP2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            
            // 1 tahun
            public decimal AmtCBP { get; set; }
            public decimal AmtCAS { get; set; }
            public decimal AmtRemain { get; set; }
            public decimal AmtJN { get; set; }

            // 01
            public decimal AmtCBP01 { get; set; }
            public decimal AmtCBPTo01 { get; set; }
            public decimal AmtCAS01 { get; set; }
            public decimal AmtCASTo01 { get; set; }
            public decimal AmtRemain01 { get; set; }
            public decimal AmtRemainTo01 { get; set; }
            public decimal AmtJN01 { get; set; }

            //02
            public decimal AmtCBP02 { get; set; }
            public decimal AmtCBPTo02 { get; set; }
            public decimal AmtCAS02 { get; set; }
            public decimal AmtCASTo02 { get; set; }
            public decimal AmtRemain02 { get; set; }
            public decimal AmtRemainTo02 { get; set; }
            public decimal AmtJN02 { get; set; }

            //03
            public decimal AmtCBP03 { get; set; }
            public decimal AmtCBPTo03 { get; set; }
            public decimal AmtCAS03 { get; set; }
            public decimal AmtCASTo03 { get; set; }
            public decimal AmtRemain03 { get; set; }
            public decimal AmtRemainTo03 { get; set; }
            public decimal AmtJN03 { get; set; }

            //Q1
            public decimal AmtCBPQ1 { get; set; }
            public decimal AmtCBPToQ1 { get; set; }
            public decimal AmtCASQ1 { get; set; }
            public decimal AmtCASToQ1 { get; set; }
            public decimal AmtRemainQ1 { get; set; }
            public decimal AmtRemainToQ1 { get; set; }
            public decimal AmtJNQ1 { get; set; }

            //04
            public decimal AmtCBP04 { get; set; }
            public decimal AmtCBPTo04 { get; set; }
            public decimal AmtCAS04 { get; set; }
            public decimal AmtCASTo04 { get; set; }
            public decimal AmtRemain04 { get; set; }
            public decimal AmtRemainTo04 { get; set; }
            public decimal AmtJN04 { get; set; }

            //05
            public decimal AmtCBP05 { get; set; }
            public decimal AmtCBPTo05 { get; set; }
            public decimal AmtCAS05 { get; set; }
            public decimal AmtCASTo05 { get; set; }
            public decimal AmtRemain05 { get; set; }
            public decimal AmtRemainTo05 { get; set; }
            public decimal AmtJN05 { get; set; }

            //06
            public decimal AmtCBP06 { get; set; }
            public decimal AmtCBPTo06 { get; set; }
            public decimal AmtCAS06 { get; set; }
            public decimal AmtCASTo06 { get; set; }
            public decimal AmtRemain06 { get; set; }
            public decimal AmtRemainTo06 { get; set; }
            public decimal AmtJN06 { get; set; }

            //Q2
            public decimal AmtCBPQ2 { get; set; }
            public decimal AmtCBPToQ2 { get; set; }
            public decimal AmtCASQ2 { get; set; }
            public decimal AmtCASToQ2 { get; set; }
            public decimal AmtRemainQ2 { get; set; }
            public decimal AmtRemainToQ2 { get; set; }
            public decimal AmtJNQ2 { get; set; }

            //07
            public decimal AmtCBP07 { get; set; }
            public decimal AmtCBPTo07 { get; set; }
            public decimal AmtCAS07 { get; set; }
            public decimal AmtCASTo07 { get; set; }
            public decimal AmtRemain07 { get; set; }
            public decimal AmtRemainTo07 { get; set; }
            public decimal AmtJN07 { get; set; }

            //08
            public decimal AmtCBP08 { get; set; }
            public decimal AmtCBPTo08 { get; set; }
            public decimal AmtCAS08 { get; set; }
            public decimal AmtCASTo08 { get; set; }
            public decimal AmtRemain08 { get; set; }
            public decimal AmtRemainTo08 { get; set; }
            public decimal AmtJN08 { get; set; }

            //09
            public decimal AmtCBP09 { get; set; }
            public decimal AmtCBPTo09 { get; set; }
            public decimal AmtCAS09 { get; set; }
            public decimal AmtCASTo09 { get; set; }
            public decimal AmtRemain09 { get; set; }
            public decimal AmtRemainTo09 { get; set; }
            public decimal AmtJN09 { get; set; }

            //Q3
            public decimal AmtCBPQ3 { get; set; }
            public decimal AmtCBPToQ3 { get; set; }
            public decimal AmtCASQ3 { get; set; }
            public decimal AmtCASToQ3 { get; set; }
            public decimal AmtRemainQ3 { get; set; }
            public decimal AmtRemainToQ3 { get; set; }
            public decimal AmtJNQ3 { get; set; }


            //10
            public decimal AmtCBP10 { get; set; }
            public decimal AmtCBPTo10 { get; set; }
            public decimal AmtCAS10 { get; set; }
            public decimal AmtCASTo10 { get; set; }
            public decimal AmtRemain10 { get; set; }
            public decimal AmtRemainTo10 { get; set; }
            public decimal AmtJN10 { get; set; }

            //11
            public decimal AmtCBP11 { get; set; }
            public decimal AmtCBPTo11 { get; set; }
            public decimal AmtCAS11 { get; set; }
            public decimal AmtCASTo11 { get; set; }
            public decimal AmtRemain11 { get; set; }
            public decimal AmtRemainTo11 { get; set; }
            public decimal AmtJN11 { get; set; }

            //12
            public decimal AmtCBP12 { get; set; }
            public decimal AmtCBPTo12 { get; set; }
            public decimal AmtCAS12 { get; set; }
            public decimal AmtCASTo12 { get; set; }
            public decimal AmtRemain12 { get; set; }
            public decimal AmtRemainTo12 { get; set; }
            public decimal AmtJN12 { get; set; }

            //Q4
            public decimal AmtCBPQ4 { get; set; }
            public decimal AmtCBPToQ4 { get; set; }
            public decimal AmtCASQ4 { get; set; }
            public decimal AmtCASToQ4 { get; set; }
            public decimal AmtRemainQ4 { get; set; }
            public decimal AmtRemainToQ4 { get; set; }
            public decimal AmtJNQ4 { get; set; }

            //// transfer and receive Budget transfer
            //public decimal TransferAmt { get; set; }
            //public decimal ReceiveAmt { get; set; }
            //public string Mth { get; set; }
            //public string Mth2 { get; set; }
        }

        #endregion
    }
}

