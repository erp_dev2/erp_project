﻿namespace RunSystem
{
    partial class FrmLeave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.LueLGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkPaidInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtNoOfDayExpired = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtNoOfDay = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtLeaveName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtLeaveCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueRLPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPoint = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtShortName = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LueLeaveColor = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMinLeave = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPaidInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDayExpired.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRLPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLeaveColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinLeave.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtMinLeave);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueLeaveColor);
            this.panel2.Controls.Add(this.TxtShortName);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtPoint);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueRLPCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueLGCode);
            this.panel2.Controls.Add(this.ChkPaidInd);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtNoOfDayExpired);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtNoOfDay);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtLeaveName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtLeaveCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 257);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 257);
            this.panel3.Size = new System.Drawing.Size(772, 216);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 216);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(28, 75);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "Leave Group";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLGCode
            // 
            this.LueLGCode.EnterMoveNextControl = true;
            this.LueLGCode.Location = new System.Drawing.Point(111, 72);
            this.LueLGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLGCode.Name = "LueLGCode";
            this.LueLGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLGCode.Properties.Appearance.Options.UseFont = true;
            this.LueLGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLGCode.Properties.DropDownRows = 30;
            this.LueLGCode.Properties.NullText = "[Empty]";
            this.LueLGCode.Properties.PopupWidth = 300;
            this.LueLGCode.Size = new System.Drawing.Size(319, 20);
            this.LueLGCode.TabIndex = 19;
            this.LueLGCode.ToolTip = "F4 : Show/hide list";
            this.LueLGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLGCode.EditValueChanged += new System.EventHandler(this.LueLGCode_EditValueChanged);
            // 
            // ChkPaidInd
            // 
            this.ChkPaidInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPaidInd.Location = new System.Drawing.Point(303, 6);
            this.ChkPaidInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPaidInd.Name = "ChkPaidInd";
            this.ChkPaidInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPaidInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPaidInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPaidInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPaidInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPaidInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPaidInd.Properties.Caption = "Paid Leave";
            this.ChkPaidInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPaidInd.Size = new System.Drawing.Size(89, 22);
            this.ChkPaidInd.TabIndex = 13;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(233, 6);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(68, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(111, 227);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 350;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(350, 20);
            this.MeeRemark.TabIndex = 34;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(57, 230);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 33;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(196, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 14);
            this.label4.TabIndex = 26;
            this.label4.Text = "days";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoOfDayExpired
            // 
            this.TxtNoOfDayExpired.EnterMoveNextControl = true;
            this.TxtNoOfDayExpired.Location = new System.Drawing.Point(111, 138);
            this.TxtNoOfDayExpired.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoOfDayExpired.Name = "TxtNoOfDayExpired";
            this.TxtNoOfDayExpired.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoOfDayExpired.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoOfDayExpired.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoOfDayExpired.Properties.Appearance.Options.UseFont = true;
            this.TxtNoOfDayExpired.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNoOfDayExpired.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNoOfDayExpired.Size = new System.Drawing.Size(82, 20);
            this.TxtNoOfDayExpired.TabIndex = 25;
            this.TxtNoOfDayExpired.Validated += new System.EventHandler(this.TxtNoOfDayExpired_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(27, 141);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Expired after";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoOfDay
            // 
            this.TxtNoOfDay.EnterMoveNextControl = true;
            this.TxtNoOfDay.Location = new System.Drawing.Point(111, 94);
            this.TxtNoOfDay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoOfDay.Name = "TxtNoOfDay";
            this.TxtNoOfDay.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoOfDay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoOfDay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoOfDay.Properties.Appearance.Options.UseFont = true;
            this.TxtNoOfDay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNoOfDay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNoOfDay.Size = new System.Drawing.Size(82, 20);
            this.TxtNoOfDay.TabIndex = 21;
            this.TxtNoOfDay.Validated += new System.EventHandler(this.TxtNoOfDay_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(10, 97);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 14);
            this.label16.TabIndex = 20;
            this.label16.Text = "Number of Days";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLeaveName
            // 
            this.TxtLeaveName.EnterMoveNextControl = true;
            this.TxtLeaveName.Location = new System.Drawing.Point(111, 28);
            this.TxtLeaveName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLeaveName.Name = "TxtLeaveName";
            this.TxtLeaveName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLeaveName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLeaveName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLeaveName.Properties.Appearance.Options.UseFont = true;
            this.TxtLeaveName.Properties.MaxLength = 80;
            this.TxtLeaveName.Size = new System.Drawing.Size(319, 20);
            this.TxtLeaveName.TabIndex = 15;
            this.TxtLeaveName.Validated += new System.EventHandler(this.TxtLeaveName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(30, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Leave Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLeaveCode
            // 
            this.TxtLeaveCode.EnterMoveNextControl = true;
            this.TxtLeaveCode.Location = new System.Drawing.Point(111, 6);
            this.TxtLeaveCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLeaveCode.Name = "TxtLeaveCode";
            this.TxtLeaveCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLeaveCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLeaveCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLeaveCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLeaveCode.Properties.MaxLength = 16;
            this.TxtLeaveCode.Size = new System.Drawing.Size(115, 20);
            this.TxtLeaveCode.TabIndex = 11;
            this.TxtLeaveCode.Validated += new System.EventHandler(this.TxtLeaveCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(33, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Leave Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(18, 163);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 27;
            this.label7.Text = "Residual Leave";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueRLPCode
            // 
            this.LueRLPCode.EnterMoveNextControl = true;
            this.LueRLPCode.Location = new System.Drawing.Point(111, 160);
            this.LueRLPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRLPCode.Name = "LueRLPCode";
            this.LueRLPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRLPCode.Properties.Appearance.Options.UseFont = true;
            this.LueRLPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRLPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRLPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRLPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRLPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRLPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRLPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRLPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRLPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRLPCode.Properties.DropDownRows = 30;
            this.LueRLPCode.Properties.NullText = "[Empty]";
            this.LueRLPCode.Properties.PopupWidth = 300;
            this.LueRLPCode.Size = new System.Drawing.Size(319, 20);
            this.LueRLPCode.TabIndex = 28;
            this.LueRLPCode.ToolTip = "F4 : Show/hide list";
            this.LueRLPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRLPCode.EditValueChanged += new System.EventHandler(this.LueRLPCode_EditValueChanged);
            // 
            // TxtPoint
            // 
            this.TxtPoint.EnterMoveNextControl = true;
            this.TxtPoint.Location = new System.Drawing.Point(111, 182);
            this.TxtPoint.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPoint.Name = "TxtPoint";
            this.TxtPoint.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPoint.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPoint.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPoint.Properties.Appearance.Options.UseFont = true;
            this.TxtPoint.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPoint.Size = new System.Drawing.Size(82, 20);
            this.TxtPoint.TabIndex = 30;
            this.TxtPoint.Validated += new System.EventHandler(this.TxtPoint_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(69, 185);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 29;
            this.label8.Text = "Point";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortName
            // 
            this.TxtShortName.EnterMoveNextControl = true;
            this.TxtShortName.Location = new System.Drawing.Point(111, 50);
            this.TxtShortName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortName.Properties.Appearance.Options.UseFont = true;
            this.TxtShortName.Properties.MaxLength = 5;
            this.TxtShortName.Size = new System.Drawing.Size(82, 20);
            this.TxtShortName.TabIndex = 17;
            this.TxtShortName.Validated += new System.EventHandler(this.TxtShortName_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(32, 53);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 14);
            this.label9.TabIndex = 16;
            this.label9.Text = "Short Name";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(34, 207);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 14);
            this.label10.TabIndex = 31;
            this.label10.Text = "Leave Color";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLeaveColor
            // 
            this.LueLeaveColor.EnterMoveNextControl = true;
            this.LueLeaveColor.Location = new System.Drawing.Point(111, 204);
            this.LueLeaveColor.Margin = new System.Windows.Forms.Padding(5);
            this.LueLeaveColor.Name = "LueLeaveColor";
            this.LueLeaveColor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveColor.Properties.Appearance.Options.UseFont = true;
            this.LueLeaveColor.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveColor.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLeaveColor.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveColor.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLeaveColor.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveColor.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLeaveColor.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLeaveColor.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLeaveColor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLeaveColor.Properties.DropDownRows = 30;
            this.LueLeaveColor.Properties.NullText = "[Empty]";
            this.LueLeaveColor.Properties.PopupWidth = 300;
            this.LueLeaveColor.Size = new System.Drawing.Size(319, 20);
            this.LueLeaveColor.TabIndex = 32;
            this.LueLeaveColor.ToolTip = "F4 : Show/hide list";
            this.LueLeaveColor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLeaveColor.EditValueChanged += new System.EventHandler(this.LueLeaveColor_EditValueChanged);
            // 
            // TxtMinLeave
            // 
            this.TxtMinLeave.EnterMoveNextControl = true;
            this.TxtMinLeave.Location = new System.Drawing.Point(111, 116);
            this.TxtMinLeave.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinLeave.Name = "TxtMinLeave";
            this.TxtMinLeave.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinLeave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinLeave.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinLeave.Properties.Appearance.Options.UseFont = true;
            this.TxtMinLeave.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinLeave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinLeave.Size = new System.Drawing.Size(82, 20);
            this.TxtMinLeave.TabIndex = 23;
            this.TxtMinLeave.Validated += new System.EventHandler(this.TxtMinLeave_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(14, 119);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 14);
            this.label11.TabIndex = 22;
            this.label11.Text = "Minimum Leave";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmLeave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmLeave";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPaidInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDayExpired.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRLPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLeaveColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinLeave.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.LookUpEdit LueLGCode;
        private DevExpress.XtraEditors.CheckEdit ChkPaidInd;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtNoOfDayExpired;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtNoOfDay;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.TextEdit TxtLeaveName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtLeaveCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.LookUpEdit LueRLPCode;
        internal DevExpress.XtraEditors.TextEdit TxtPoint;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit TxtShortName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.LookUpEdit LueLeaveColor;
        internal DevExpress.XtraEditors.TextEdit TxtMinLeave;
        private System.Windows.Forms.Label label11;
    }
}