﻿namespace RunSystem
{
    partial class FrmItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItem));
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtForeignName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.TxtHSCode = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.LblItGrpCode = new System.Windows.Forms.Label();
            this.LueItGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeSpecification = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkTaxLiableInd = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueItBrCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueItScCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgPurchasing = new System.Windows.Forms.TabPage();
            this.LueWeightUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtWeight = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueDiameterUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDiameter = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueVolumeUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueWidthUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueHeightUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLengthUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtVolume = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtWidth = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtHeight = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtLength = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtMinOrder = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LuePurchaseUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgInventory = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.TpgConversion1 = new System.Windows.Forms.TabPage();
            this.TxtInvConvertUOM13 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtInvConvertUOM12 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TpgConversion2 = new System.Windows.Forms.TabPage();
            this.TxtInvConvertUOM23 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtInvConvertUOM21 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.TpgConversion3 = new System.Windows.Forms.TabPage();
            this.TxtInvConvertUOM32 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtInvConvertUOM31 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.LueControlByDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkControlByDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueInventoryUomCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueInventoryUomCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtRequestedQty = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.TxtOrderredQty = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtCommittedQty = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtInStockQty = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtReOrderStock = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtMaxStock = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtMinStock = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LueInventoryUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgSales = new System.Windows.Forms.TabPage();
            this.LueUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.GrdSalesPckUnit = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.LblSalesUOM1 = new System.Windows.Forms.Label();
            this.LblSalesUOM2 = new System.Windows.Forms.Label();
            this.TxtSalesConvertUOM2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesConvertUOM1 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.LueSalesUomCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueSalesUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgPlanning = new System.Windows.Forms.TabPage();
            this.TxtPlanningUomCodeConvert21 = new DevExpress.XtraEditors.TextEdit();
            this.TxtPlanningUomCodeConvert12 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LuePlanningUomCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LuePlanningUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgFixedAsset = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgProperty = new System.Windows.Forms.TabPage();
            this.LueProperty = new DevExpress.XtraEditors.LookUpEdit();
            this.GrdProperty = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.LblInformation5 = new System.Windows.Forms.Label();
            this.LueInformation5 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblInformation4 = new System.Windows.Forms.Label();
            this.LueInformation4 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblInformation3 = new System.Windows.Forms.Label();
            this.LueInformation3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblInformation2 = new System.Windows.Forms.Label();
            this.LueInformation2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblInformation1 = new System.Windows.Forms.Label();
            this.LueInformation1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgAttachment = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.PicItem = new System.Windows.Forms.PictureBox();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPicture = new DevExpress.XtraEditors.SimpleButton();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.button2 = new System.Windows.Forms.Button();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkServiceItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPlanningItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFixedItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPurchaseItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSalesItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkInventoryItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtItCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.LblItCodeInternal = new System.Windows.Forms.Label();
            this.BtnItem = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.TxtItemRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.TxtItCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxLiableInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItBrCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            this.TpgPurchasing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWeightUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDiameterUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiameter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVolumeUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWidthUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueHeightUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePurchaseUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            this.TpgInventory.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.TpgConversion1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM12.Properties)).BeginInit();
            this.TpgConversion2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM21.Properties)).BeginInit();
            this.TpgConversion3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueControlByDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkControlByDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode2.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRequestedQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOrderredQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCommittedQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInStockQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReOrderStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode.Properties)).BeginInit();
            this.TpgSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdSalesPckUnit)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesConvertUOM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesConvertUOM1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSalesUomCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSalesUomCode.Properties)).BeginInit();
            this.TpgPlanning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCodeConvert21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCodeConvert12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePlanningUomCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePlanningUomCode.Properties)).BeginInit();
            this.TpgFixedAsset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgProperty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProperty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdProperty)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation1.Properties)).BeginInit();
            this.TpgAttachment.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPurchaseItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInventoryItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItemRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeOld.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(829, 0);
            this.panel1.Size = new System.Drawing.Size(70, 472);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtItCodeOld);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtItemRequestDocNo);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.BtnItem);
            this.panel2.Controls.Add(this.TxtItCodeInternal);
            this.panel2.Controls.Add(this.TxtSource);
            this.panel2.Controls.Add(this.LblItCodeInternal);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.TxtForeignName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(829, 472);
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(105, 26);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(451, 20);
            this.TxtItName.TabIndex = 16;
            this.TxtItName.Validated += new System.EventHandler(this.TxtItName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(34, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Item Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(105, 5);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(167, 20);
            this.TxtItCode.TabIndex = 10;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(37, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Item Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtForeignName
            // 
            this.TxtForeignName.EnterMoveNextControl = true;
            this.TxtForeignName.Location = new System.Drawing.Point(105, 68);
            this.TxtForeignName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtForeignName.Name = "TxtForeignName";
            this.TxtForeignName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtForeignName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtForeignName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtForeignName.Properties.Appearance.Options.UseFont = true;
            this.TxtForeignName.Properties.MaxLength = 250;
            this.TxtForeignName.Size = new System.Drawing.Size(451, 20);
            this.TxtForeignName.TabIndex = 20;
            this.TxtForeignName.Validated += new System.EventHandler(this.TxtForeignName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(20, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Foreign Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Controls.Add(this.TpgPurchasing);
            this.tabControl1.Controls.Add(this.TpgInventory);
            this.tabControl1.Controls.Add(this.TpgSales);
            this.tabControl1.Controls.Add(this.TpgPlanning);
            this.tabControl1.Controls.Add(this.TpgFixedAsset);
            this.tabControl1.Controls.Add(this.TpgProperty);
            this.tabControl1.Controls.Add(this.TpgAttachment);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 139);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(829, 333);
            this.tabControl1.TabIndex = 30;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.TxtHSCode);
            this.TpgGeneral.Controls.Add(this.label51);
            this.TpgGeneral.Controls.Add(this.LblItGrpCode);
            this.TpgGeneral.Controls.Add(this.LueItGrpCode);
            this.TpgGeneral.Controls.Add(this.MeeSpecification);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.MeeRemark);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.ChkTaxLiableInd);
            this.TpgGeneral.Controls.Add(this.label7);
            this.TpgGeneral.Controls.Add(this.LueItBrCode);
            this.TpgGeneral.Controls.Add(this.label5);
            this.TpgGeneral.Controls.Add(this.LueItScCode);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.LueItCtCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(821, 303);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // TxtHSCode
            // 
            this.TxtHSCode.EnterMoveNextControl = true;
            this.TxtHSCode.Location = new System.Drawing.Point(113, 130);
            this.TxtHSCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHSCode.Name = "TxtHSCode";
            this.TxtHSCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHSCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHSCode.Properties.Appearance.Options.UseFont = true;
            this.TxtHSCode.Properties.MaxLength = 250;
            this.TxtHSCode.Properties.ReadOnly = true;
            this.TxtHSCode.Size = new System.Drawing.Size(319, 20);
            this.TxtHSCode.TabIndex = 42;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(54, 133);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(54, 14);
            this.label51.TabIndex = 41;
            this.label51.Text = "HS Code";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblItGrpCode
            // 
            this.LblItGrpCode.AutoSize = true;
            this.LblItGrpCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItGrpCode.ForeColor = System.Drawing.Color.Black;
            this.LblItGrpCode.Location = new System.Drawing.Point(68, 90);
            this.LblItGrpCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItGrpCode.Name = "LblItGrpCode";
            this.LblItGrpCode.Size = new System.Drawing.Size(40, 14);
            this.LblItGrpCode.TabIndex = 37;
            this.LblItGrpCode.Text = "Group";
            this.LblItGrpCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItGrpCode
            // 
            this.LueItGrpCode.EnterMoveNextControl = true;
            this.LueItGrpCode.Location = new System.Drawing.Point(113, 87);
            this.LueItGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItGrpCode.Name = "LueItGrpCode";
            this.LueItGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItGrpCode.Properties.DropDownRows = 30;
            this.LueItGrpCode.Properties.NullText = "[Empty]";
            this.LueItGrpCode.Properties.PopupWidth = 350;
            this.LueItGrpCode.Size = new System.Drawing.Size(319, 20);
            this.LueItGrpCode.TabIndex = 38;
            this.LueItGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueItGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItGrpCode.EditValueChanged += new System.EventHandler(this.LueItGrpCode_EditValueChanged);
            // 
            // MeeSpecification
            // 
            this.MeeSpecification.EnterMoveNextControl = true;
            this.MeeSpecification.Location = new System.Drawing.Point(113, 109);
            this.MeeSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSpecification.Name = "MeeSpecification";
            this.MeeSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.Appearance.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSpecification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSpecification.Properties.MaxLength = 1500;
            this.MeeSpecification.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSpecification.Properties.ShowIcon = false;
            this.MeeSpecification.Size = new System.Drawing.Size(592, 20);
            this.MeeSpecification.TabIndex = 40;
            this.MeeSpecification.ToolTip = "F4 : Show/hide text";
            this.MeeSpecification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSpecification.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(33, 112);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 39;
            this.label9.Text = "Specification";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(113, 151);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(592, 20);
            this.MeeRemark.TabIndex = 44;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(61, 154);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 43;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTaxLiableInd
            // 
            this.ChkTaxLiableInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxLiableInd.Location = new System.Drawing.Point(113, 174);
            this.ChkTaxLiableInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxLiableInd.Name = "ChkTaxLiableInd";
            this.ChkTaxLiableInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxLiableInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxLiableInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxLiableInd.Properties.Caption = "Tax Liable";
            this.ChkTaxLiableInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxLiableInd.Size = new System.Drawing.Size(85, 22);
            this.ChkTaxLiableInd.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(70, 68);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 35;
            this.label7.Text = "Brand";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItBrCode
            // 
            this.LueItBrCode.EnterMoveNextControl = true;
            this.LueItBrCode.Location = new System.Drawing.Point(113, 65);
            this.LueItBrCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItBrCode.Name = "LueItBrCode";
            this.LueItBrCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.Appearance.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItBrCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItBrCode.Properties.DropDownRows = 30;
            this.LueItBrCode.Properties.NullText = "[Empty]";
            this.LueItBrCode.Properties.PopupWidth = 350;
            this.LueItBrCode.Size = new System.Drawing.Size(319, 20);
            this.LueItBrCode.TabIndex = 36;
            this.LueItBrCode.ToolTip = "F4 : Show/hide list";
            this.LueItBrCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItBrCode.EditValueChanged += new System.EventHandler(this.LueItBrCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(27, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 14);
            this.label5.TabIndex = 33;
            this.label5.Text = "Sub Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItScCode
            // 
            this.LueItScCode.EnterMoveNextControl = true;
            this.LueItScCode.Location = new System.Drawing.Point(113, 43);
            this.LueItScCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItScCode.Name = "LueItScCode";
            this.LueItScCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.Appearance.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItScCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItScCode.Properties.DropDownRows = 30;
            this.LueItScCode.Properties.NullText = "[Empty]";
            this.LueItScCode.Properties.PopupWidth = 350;
            this.LueItScCode.Size = new System.Drawing.Size(319, 20);
            this.LueItScCode.TabIndex = 34;
            this.LueItScCode.ToolTip = "F4 : Show/hide list";
            this.LueItScCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItScCode.EditValueChanged += new System.EventHandler(this.LueItScCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(52, 24);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 31;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(113, 21);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 30;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 350;
            this.LueItCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueItCtCode.TabIndex = 32;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // TpgPurchasing
            // 
            this.TpgPurchasing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgPurchasing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgPurchasing.Controls.Add(this.LueWeightUomCode);
            this.TpgPurchasing.Controls.Add(this.TxtWeight);
            this.TpgPurchasing.Controls.Add(this.label36);
            this.TpgPurchasing.Controls.Add(this.label37);
            this.TpgPurchasing.Controls.Add(this.LuePGCode);
            this.TpgPurchasing.Controls.Add(this.LueDiameterUomCode);
            this.TpgPurchasing.Controls.Add(this.TxtDiameter);
            this.TpgPurchasing.Controls.Add(this.label35);
            this.TpgPurchasing.Controls.Add(this.LueVolumeUomCode);
            this.TpgPurchasing.Controls.Add(this.LueWidthUomCode);
            this.TpgPurchasing.Controls.Add(this.LueHeightUomCode);
            this.TpgPurchasing.Controls.Add(this.LueLengthUomCode);
            this.TpgPurchasing.Controls.Add(this.TxtVolume);
            this.TpgPurchasing.Controls.Add(this.label18);
            this.TpgPurchasing.Controls.Add(this.TxtWidth);
            this.TpgPurchasing.Controls.Add(this.label19);
            this.TpgPurchasing.Controls.Add(this.TxtHeight);
            this.TpgPurchasing.Controls.Add(this.label17);
            this.TpgPurchasing.Controls.Add(this.TxtLength);
            this.TpgPurchasing.Controls.Add(this.label16);
            this.TpgPurchasing.Controls.Add(this.TxtMinOrder);
            this.TpgPurchasing.Controls.Add(this.label15);
            this.TpgPurchasing.Controls.Add(this.LueTaxCode3);
            this.TpgPurchasing.Controls.Add(this.LueTaxCode2);
            this.TpgPurchasing.Controls.Add(this.label12);
            this.TpgPurchasing.Controls.Add(this.LueTaxCode1);
            this.TpgPurchasing.Controls.Add(this.label11);
            this.TpgPurchasing.Controls.Add(this.LuePurchaseUomCode);
            this.TpgPurchasing.Controls.Add(this.label10);
            this.TpgPurchasing.Controls.Add(this.LueVdCode);
            this.TpgPurchasing.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchasing.Name = "TpgPurchasing";
            this.TpgPurchasing.Size = new System.Drawing.Size(764, 303);
            this.TpgPurchasing.TabIndex = 1;
            this.TpgPurchasing.Text = "Purchasing Data";
            this.TpgPurchasing.UseVisualStyleBackColor = true;
            // 
            // LueWeightUomCode
            // 
            this.LueWeightUomCode.EnterMoveNextControl = true;
            this.LueWeightUomCode.Location = new System.Drawing.Point(337, 269);
            this.LueWeightUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWeightUomCode.Name = "LueWeightUomCode";
            this.LueWeightUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWeightUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWeightUomCode.Properties.DropDownRows = 30;
            this.LueWeightUomCode.Properties.NullText = "[Empty]";
            this.LueWeightUomCode.Properties.PopupWidth = 200;
            this.LueWeightUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueWeightUomCode.TabIndex = 60;
            this.LueWeightUomCode.ToolTip = "F4 : Show/hide list";
            this.LueWeightUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWeightUomCode.EditValueChanged += new System.EventHandler(this.LueWeightUomCode_EditValueChanged);
            // 
            // TxtWeight
            // 
            this.TxtWeight.EnterMoveNextControl = true;
            this.TxtWeight.Location = new System.Drawing.Point(167, 269);
            this.TxtWeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWeight.Name = "TxtWeight";
            this.TxtWeight.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeight.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWeight.Properties.Appearance.Options.UseFont = true;
            this.TxtWeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWeight.Size = new System.Drawing.Size(166, 20);
            this.TxtWeight.TabIndex = 59;
            this.TxtWeight.Validated += new System.EventHandler(this.TxtWeight_Validated);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(113, 273);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 14);
            this.label36.TabIndex = 58;
            this.label36.Text = "Weight";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(81, 74);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(79, 14);
            this.label37.TabIndex = 37;
            this.label37.Text = "Pricing Group";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(167, 71);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 350;
            this.LuePGCode.Size = new System.Drawing.Size(319, 20);
            this.LuePGCode.TabIndex = 38;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // LueDiameterUomCode
            // 
            this.LueDiameterUomCode.EnterMoveNextControl = true;
            this.LueDiameterUomCode.Location = new System.Drawing.Point(337, 225);
            this.LueDiameterUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDiameterUomCode.Name = "LueDiameterUomCode";
            this.LueDiameterUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDiameterUomCode.Properties.DropDownRows = 30;
            this.LueDiameterUomCode.Properties.NullText = "[Empty]";
            this.LueDiameterUomCode.Properties.PopupWidth = 200;
            this.LueDiameterUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueDiameterUomCode.TabIndex = 54;
            this.LueDiameterUomCode.ToolTip = "F4 : Show/hide list";
            this.LueDiameterUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDiameterUomCode.EditValueChanged += new System.EventHandler(this.LueDiameterUomCode_EditValueChanged);
            // 
            // TxtDiameter
            // 
            this.TxtDiameter.EnterMoveNextControl = true;
            this.TxtDiameter.Location = new System.Drawing.Point(167, 225);
            this.TxtDiameter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiameter.Name = "TxtDiameter";
            this.TxtDiameter.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiameter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiameter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiameter.Properties.Appearance.Options.UseFont = true;
            this.TxtDiameter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiameter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiameter.Size = new System.Drawing.Size(166, 20);
            this.TxtDiameter.TabIndex = 53;
            this.TxtDiameter.Validated += new System.EventHandler(this.TxtDiameter_Validated);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(104, 228);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 14);
            this.label35.TabIndex = 52;
            this.label35.Text = "Diameter";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVolumeUomCode
            // 
            this.LueVolumeUomCode.EnterMoveNextControl = true;
            this.LueVolumeUomCode.Location = new System.Drawing.Point(337, 247);
            this.LueVolumeUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVolumeUomCode.Name = "LueVolumeUomCode";
            this.LueVolumeUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVolumeUomCode.Properties.DropDownRows = 30;
            this.LueVolumeUomCode.Properties.NullText = "[Empty]";
            this.LueVolumeUomCode.Properties.PopupWidth = 200;
            this.LueVolumeUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueVolumeUomCode.TabIndex = 57;
            this.LueVolumeUomCode.ToolTip = "F4 : Show/hide list";
            this.LueVolumeUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVolumeUomCode.EditValueChanged += new System.EventHandler(this.LueVolumeUomCode_EditValueChanged);
            // 
            // LueWidthUomCode
            // 
            this.LueWidthUomCode.EnterMoveNextControl = true;
            this.LueWidthUomCode.Location = new System.Drawing.Point(337, 203);
            this.LueWidthUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWidthUomCode.Name = "LueWidthUomCode";
            this.LueWidthUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWidthUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWidthUomCode.Properties.DropDownRows = 30;
            this.LueWidthUomCode.Properties.NullText = "[Empty]";
            this.LueWidthUomCode.Properties.PopupWidth = 200;
            this.LueWidthUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueWidthUomCode.TabIndex = 51;
            this.LueWidthUomCode.ToolTip = "F4 : Show/hide list";
            this.LueWidthUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWidthUomCode.EditValueChanged += new System.EventHandler(this.LueWidthUomCode_EditValueChanged);
            // 
            // LueHeightUomCode
            // 
            this.LueHeightUomCode.EnterMoveNextControl = true;
            this.LueHeightUomCode.Location = new System.Drawing.Point(337, 181);
            this.LueHeightUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueHeightUomCode.Name = "LueHeightUomCode";
            this.LueHeightUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueHeightUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueHeightUomCode.Properties.DropDownRows = 30;
            this.LueHeightUomCode.Properties.NullText = "[Empty]";
            this.LueHeightUomCode.Properties.PopupWidth = 200;
            this.LueHeightUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueHeightUomCode.TabIndex = 48;
            this.LueHeightUomCode.ToolTip = "F4 : Show/hide list";
            this.LueHeightUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueHeightUomCode.EditValueChanged += new System.EventHandler(this.LueHeightUomCode_EditValueChanged);
            // 
            // LueLengthUomCode
            // 
            this.LueLengthUomCode.EnterMoveNextControl = true;
            this.LueLengthUomCode.Location = new System.Drawing.Point(337, 159);
            this.LueLengthUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLengthUomCode.Name = "LueLengthUomCode";
            this.LueLengthUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLengthUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLengthUomCode.Properties.DropDownRows = 30;
            this.LueLengthUomCode.Properties.NullText = "[Empty]";
            this.LueLengthUomCode.Properties.PopupWidth = 200;
            this.LueLengthUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueLengthUomCode.TabIndex = 45;
            this.LueLengthUomCode.ToolTip = "F4 : Show/hide list";
            this.LueLengthUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLengthUomCode.EditValueChanged += new System.EventHandler(this.LueLengthUomCode_EditValueChanged);
            // 
            // TxtVolume
            // 
            this.TxtVolume.EnterMoveNextControl = true;
            this.TxtVolume.Location = new System.Drawing.Point(167, 247);
            this.TxtVolume.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVolume.Name = "TxtVolume";
            this.TxtVolume.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVolume.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVolume.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVolume.Properties.Appearance.Options.UseFont = true;
            this.TxtVolume.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVolume.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVolume.Size = new System.Drawing.Size(166, 20);
            this.TxtVolume.TabIndex = 56;
            this.TxtVolume.Validated += new System.EventHandler(this.TxtVolume_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(112, 250);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 14);
            this.label18.TabIndex = 55;
            this.label18.Text = "Volume";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWidth
            // 
            this.TxtWidth.EnterMoveNextControl = true;
            this.TxtWidth.Location = new System.Drawing.Point(167, 203);
            this.TxtWidth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWidth.Name = "TxtWidth";
            this.TxtWidth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWidth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWidth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWidth.Properties.Appearance.Options.UseFont = true;
            this.TxtWidth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWidth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWidth.Size = new System.Drawing.Size(166, 20);
            this.TxtWidth.TabIndex = 50;
            this.TxtWidth.Validated += new System.EventHandler(this.TxtWidth_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(120, 206);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 14);
            this.label19.TabIndex = 49;
            this.label19.Text = "Width";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHeight
            // 
            this.TxtHeight.EnterMoveNextControl = true;
            this.TxtHeight.Location = new System.Drawing.Point(167, 181);
            this.TxtHeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHeight.Name = "TxtHeight";
            this.TxtHeight.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHeight.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHeight.Properties.Appearance.Options.UseFont = true;
            this.TxtHeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHeight.Size = new System.Drawing.Size(166, 20);
            this.TxtHeight.TabIndex = 47;
            this.TxtHeight.Validated += new System.EventHandler(this.TxtHeight_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(117, 184);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 14);
            this.label17.TabIndex = 46;
            this.label17.Text = "Height";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLength
            // 
            this.TxtLength.EnterMoveNextControl = true;
            this.TxtLength.Location = new System.Drawing.Point(167, 159);
            this.TxtLength.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLength.Name = "TxtLength";
            this.TxtLength.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLength.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLength.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLength.Properties.Appearance.Options.UseFont = true;
            this.TxtLength.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLength.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLength.Size = new System.Drawing.Size(166, 20);
            this.TxtLength.TabIndex = 44;
            this.TxtLength.Validated += new System.EventHandler(this.TxtLength_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(114, 162);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 14);
            this.label16.TabIndex = 43;
            this.label16.Text = "Length";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinOrder
            // 
            this.TxtMinOrder.EnterMoveNextControl = true;
            this.TxtMinOrder.Location = new System.Drawing.Point(167, 49);
            this.TxtMinOrder.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinOrder.Name = "TxtMinOrder";
            this.TxtMinOrder.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinOrder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinOrder.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinOrder.Properties.Appearance.Options.UseFont = true;
            this.TxtMinOrder.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinOrder.Size = new System.Drawing.Size(166, 20);
            this.TxtMinOrder.TabIndex = 36;
            this.TxtMinOrder.Validated += new System.EventHandler(this.TxtMinOrder_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(20, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 14);
            this.label15.TabIndex = 35;
            this.label15.Text = "Minimum Order Quantity";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(167, 137);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 30;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 350;
            this.LueTaxCode3.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode3.TabIndex = 42;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode3.EditValueChanged += new System.EventHandler(this.LueTaxCode3_EditValueChanged);
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(167, 115);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 30;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 350;
            this.LueTaxCode2.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode2.TabIndex = 41;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(101, 96);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 39;
            this.label12.Text = "Tax Code";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(167, 93);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 30;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 350;
            this.LueTaxCode1.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode1.TabIndex = 40;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(75, 30);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 14);
            this.label11.TabIndex = 33;
            this.label11.Text = "Purchase UoM";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePurchaseUomCode
            // 
            this.LuePurchaseUomCode.EnterMoveNextControl = true;
            this.LuePurchaseUomCode.Location = new System.Drawing.Point(167, 27);
            this.LuePurchaseUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePurchaseUomCode.Name = "LuePurchaseUomCode";
            this.LuePurchaseUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePurchaseUomCode.Properties.Appearance.Options.UseFont = true;
            this.LuePurchaseUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePurchaseUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePurchaseUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePurchaseUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePurchaseUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePurchaseUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePurchaseUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePurchaseUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePurchaseUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePurchaseUomCode.Properties.DropDownRows = 30;
            this.LuePurchaseUomCode.Properties.NullText = "[Empty]";
            this.LuePurchaseUomCode.Properties.PopupWidth = 350;
            this.LuePurchaseUomCode.Size = new System.Drawing.Size(319, 20);
            this.LuePurchaseUomCode.TabIndex = 34;
            this.LuePurchaseUomCode.ToolTip = "F4 : Show/hide list";
            this.LuePurchaseUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePurchaseUomCode.EditValueChanged += new System.EventHandler(this.LuePurchaseUomCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(58, 8);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 14);
            this.label10.TabIndex = 31;
            this.label10.Text = "Preferred Vendor";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(167, 5);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 350;
            this.LueVdCode.Size = new System.Drawing.Size(319, 20);
            this.LueVdCode.TabIndex = 32;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            // 
            // TpgInventory
            // 
            this.TpgInventory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgInventory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgInventory.Controls.Add(this.panel8);
            this.TpgInventory.Controls.Add(this.LueControlByDeptCode);
            this.TpgInventory.Controls.Add(this.ChkControlByDeptCode);
            this.TpgInventory.Controls.Add(this.label13);
            this.TpgInventory.Controls.Add(this.LueInventoryUomCode3);
            this.TpgInventory.Controls.Add(this.LueInventoryUomCode2);
            this.TpgInventory.Controls.Add(this.panel3);
            this.TpgInventory.Controls.Add(this.TxtReOrderStock);
            this.TpgInventory.Controls.Add(this.label25);
            this.TpgInventory.Controls.Add(this.TxtMaxStock);
            this.TpgInventory.Controls.Add(this.label24);
            this.TpgInventory.Controls.Add(this.TxtMinStock);
            this.TpgInventory.Controls.Add(this.label22);
            this.TpgInventory.Controls.Add(this.label23);
            this.TpgInventory.Controls.Add(this.LueInventoryUomCode);
            this.TpgInventory.Location = new System.Drawing.Point(4, 26);
            this.TpgInventory.Name = "TpgInventory";
            this.TpgInventory.Size = new System.Drawing.Size(764, 303);
            this.TpgInventory.TabIndex = 2;
            this.TpgInventory.Text = "Inventory Data";
            this.TpgInventory.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.tabControl2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(479, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(281, 120);
            this.panel8.TabIndex = 37;
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl2.Controls.Add(this.TpgConversion1);
            this.tabControl2.Controls.Add(this.TpgConversion2);
            this.tabControl2.Controls.Add(this.TpgConversion3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.ShowToolTips = true;
            this.tabControl2.Size = new System.Drawing.Size(281, 120);
            this.tabControl2.TabIndex = 45;
            // 
            // TpgConversion1
            // 
            this.TpgConversion1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgConversion1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgConversion1.Controls.Add(this.TxtInvConvertUOM13);
            this.TpgConversion1.Controls.Add(this.label41);
            this.TpgConversion1.Controls.Add(this.TxtInvConvertUOM12);
            this.TpgConversion1.Controls.Add(this.label40);
            this.TpgConversion1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgConversion1.Location = new System.Drawing.Point(4, 26);
            this.TpgConversion1.Name = "TpgConversion1";
            this.TpgConversion1.Size = new System.Drawing.Size(273, 90);
            this.TpgConversion1.TabIndex = 0;
            this.TpgConversion1.Text = "Conversion 1";
            this.TpgConversion1.UseVisualStyleBackColor = true;
            // 
            // TxtInvConvertUOM13
            // 
            this.TxtInvConvertUOM13.EnterMoveNextControl = true;
            this.TxtInvConvertUOM13.Location = new System.Drawing.Point(98, 26);
            this.TxtInvConvertUOM13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM13.Name = "TxtInvConvertUOM13";
            this.TxtInvConvertUOM13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM13.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM13.Size = new System.Drawing.Size(165, 20);
            this.TxtInvConvertUOM13.TabIndex = 49;
            this.TxtInvConvertUOM13.Validated += new System.EventHandler(this.TxtInvConvertUOM13_Validated);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(2, 28);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(93, 14);
            this.label41.TabIndex = 48;
            this.label41.Text = "Value to Uom 3";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvConvertUOM12
            // 
            this.TxtInvConvertUOM12.EnterMoveNextControl = true;
            this.TxtInvConvertUOM12.Location = new System.Drawing.Point(98, 4);
            this.TxtInvConvertUOM12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM12.Name = "TxtInvConvertUOM12";
            this.TxtInvConvertUOM12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM12.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM12.Size = new System.Drawing.Size(165, 20);
            this.TxtInvConvertUOM12.TabIndex = 47;
            this.TxtInvConvertUOM12.Validated += new System.EventHandler(this.TxtInvConvertUOM12_Validated);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(2, 8);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(93, 14);
            this.label40.TabIndex = 46;
            this.label40.Text = "Value to Uom 2";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgConversion2
            // 
            this.TpgConversion2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgConversion2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgConversion2.Controls.Add(this.TxtInvConvertUOM23);
            this.TpgConversion2.Controls.Add(this.label42);
            this.TpgConversion2.Controls.Add(this.TxtInvConvertUOM21);
            this.TpgConversion2.Controls.Add(this.label43);
            this.TpgConversion2.Location = new System.Drawing.Point(4, 26);
            this.TpgConversion2.Name = "TpgConversion2";
            this.TpgConversion2.Size = new System.Drawing.Size(273, 90);
            this.TpgConversion2.TabIndex = 1;
            this.TpgConversion2.Text = "Conversion 2";
            this.TpgConversion2.UseVisualStyleBackColor = true;
            // 
            // TxtInvConvertUOM23
            // 
            this.TxtInvConvertUOM23.EnterMoveNextControl = true;
            this.TxtInvConvertUOM23.Location = new System.Drawing.Point(100, 25);
            this.TxtInvConvertUOM23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM23.Name = "TxtInvConvertUOM23";
            this.TxtInvConvertUOM23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM23.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM23.Size = new System.Drawing.Size(163, 20);
            this.TxtInvConvertUOM23.TabIndex = 49;
            this.TxtInvConvertUOM23.Validated += new System.EventHandler(this.TxtInvConvertUOM23_Validated);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(4, 27);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(93, 14);
            this.label42.TabIndex = 48;
            this.label42.Text = "Value to Uom 3";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvConvertUOM21
            // 
            this.TxtInvConvertUOM21.EnterMoveNextControl = true;
            this.TxtInvConvertUOM21.Location = new System.Drawing.Point(100, 3);
            this.TxtInvConvertUOM21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM21.Name = "TxtInvConvertUOM21";
            this.TxtInvConvertUOM21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM21.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM21.Size = new System.Drawing.Size(163, 20);
            this.TxtInvConvertUOM21.TabIndex = 47;
            this.TxtInvConvertUOM21.Validated += new System.EventHandler(this.TxtInvConvertUOM21_Validated);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(4, 7);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(93, 14);
            this.label43.TabIndex = 46;
            this.label43.Text = "Value to Uom 1";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgConversion3
            // 
            this.TpgConversion3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgConversion3.Controls.Add(this.TxtInvConvertUOM32);
            this.TpgConversion3.Controls.Add(this.label44);
            this.TpgConversion3.Controls.Add(this.TxtInvConvertUOM31);
            this.TpgConversion3.Controls.Add(this.label45);
            this.TpgConversion3.Location = new System.Drawing.Point(4, 26);
            this.TpgConversion3.Name = "TpgConversion3";
            this.TpgConversion3.Size = new System.Drawing.Size(273, 90);
            this.TpgConversion3.TabIndex = 3;
            this.TpgConversion3.Text = "Conversion 3";
            this.TpgConversion3.UseVisualStyleBackColor = true;
            // 
            // TxtInvConvertUOM32
            // 
            this.TxtInvConvertUOM32.EnterMoveNextControl = true;
            this.TxtInvConvertUOM32.Location = new System.Drawing.Point(102, 28);
            this.TxtInvConvertUOM32.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM32.Name = "TxtInvConvertUOM32";
            this.TxtInvConvertUOM32.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM32.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM32.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM32.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM32.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM32.Size = new System.Drawing.Size(163, 20);
            this.TxtInvConvertUOM32.TabIndex = 49;
            this.TxtInvConvertUOM32.Validated += new System.EventHandler(this.TxtInvConvertUOM32_Validated);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(6, 30);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(93, 14);
            this.label44.TabIndex = 48;
            this.label44.Text = "Value to Uom 2";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvConvertUOM31
            // 
            this.TxtInvConvertUOM31.EnterMoveNextControl = true;
            this.TxtInvConvertUOM31.Location = new System.Drawing.Point(102, 6);
            this.TxtInvConvertUOM31.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvConvertUOM31.Name = "TxtInvConvertUOM31";
            this.TxtInvConvertUOM31.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvConvertUOM31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvConvertUOM31.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvConvertUOM31.Properties.Appearance.Options.UseFont = true;
            this.TxtInvConvertUOM31.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvConvertUOM31.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvConvertUOM31.Size = new System.Drawing.Size(163, 20);
            this.TxtInvConvertUOM31.TabIndex = 47;
            this.TxtInvConvertUOM31.Validated += new System.EventHandler(this.TxtInvConvertUOM31_Validated);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(6, 10);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(93, 14);
            this.label45.TabIndex = 46;
            this.label45.Text = "Value to Uom 1";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueControlByDeptCode
            // 
            this.LueControlByDeptCode.EnterMoveNextControl = true;
            this.LueControlByDeptCode.Location = new System.Drawing.Point(188, 92);
            this.LueControlByDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueControlByDeptCode.Name = "LueControlByDeptCode";
            this.LueControlByDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueControlByDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueControlByDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueControlByDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueControlByDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueControlByDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueControlByDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueControlByDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueControlByDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueControlByDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueControlByDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueControlByDeptCode.Properties.DropDownRows = 30;
            this.LueControlByDeptCode.Properties.NullText = "[Empty]";
            this.LueControlByDeptCode.Properties.PopupWidth = 300;
            this.LueControlByDeptCode.Size = new System.Drawing.Size(245, 20);
            this.LueControlByDeptCode.TabIndex = 43;
            this.LueControlByDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueControlByDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueControlByDeptCode.EditValueChanged += new System.EventHandler(this.LueControlByDeptCode_EditValueChanged);
            // 
            // ChkControlByDeptCode
            // 
            this.ChkControlByDeptCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkControlByDeptCode.Location = new System.Drawing.Point(2, 91);
            this.ChkControlByDeptCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkControlByDeptCode.Name = "ChkControlByDeptCode";
            this.ChkControlByDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkControlByDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkControlByDeptCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkControlByDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkControlByDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkControlByDeptCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkControlByDeptCode.Properties.Caption = "Material Request is issued by";
            this.ChkControlByDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkControlByDeptCode.Properties.ReadOnly = true;
            this.ChkControlByDeptCode.Size = new System.Drawing.Size(186, 22);
            this.ChkControlByDeptCode.TabIndex = 42;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(239, 17);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 14);
            this.label13.TabIndex = 33;
            this.label13.Text = "(Primary)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInventoryUomCode3
            // 
            this.LueInventoryUomCode3.EnterMoveNextControl = true;
            this.LueInventoryUomCode3.Location = new System.Drawing.Point(95, 60);
            this.LueInventoryUomCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueInventoryUomCode3.Name = "LueInventoryUomCode3";
            this.LueInventoryUomCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode3.Properties.Appearance.Options.UseFont = true;
            this.LueInventoryUomCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInventoryUomCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInventoryUomCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInventoryUomCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInventoryUomCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInventoryUomCode3.Properties.DropDownRows = 30;
            this.LueInventoryUomCode3.Properties.NullText = "[Empty]";
            this.LueInventoryUomCode3.Properties.PopupWidth = 200;
            this.LueInventoryUomCode3.Size = new System.Drawing.Size(141, 20);
            this.LueInventoryUomCode3.TabIndex = 35;
            this.LueInventoryUomCode3.ToolTip = "F4 : Show/hide list";
            this.LueInventoryUomCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInventoryUomCode3.EditValueChanged += new System.EventHandler(this.LueInventoryUomCode3_EditValueChanged);
            // 
            // LueInventoryUomCode2
            // 
            this.LueInventoryUomCode2.EnterMoveNextControl = true;
            this.LueInventoryUomCode2.Location = new System.Drawing.Point(95, 37);
            this.LueInventoryUomCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueInventoryUomCode2.Name = "LueInventoryUomCode2";
            this.LueInventoryUomCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode2.Properties.Appearance.Options.UseFont = true;
            this.LueInventoryUomCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInventoryUomCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInventoryUomCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInventoryUomCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInventoryUomCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInventoryUomCode2.Properties.DropDownRows = 30;
            this.LueInventoryUomCode2.Properties.NullText = "[Empty]";
            this.LueInventoryUomCode2.Properties.PopupWidth = 200;
            this.LueInventoryUomCode2.Size = new System.Drawing.Size(141, 20);
            this.LueInventoryUomCode2.TabIndex = 34;
            this.LueInventoryUomCode2.ToolTip = "F4 : Show/hide list";
            this.LueInventoryUomCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInventoryUomCode2.EditValueChanged += new System.EventHandler(this.LueInventoryUomCode2_EditValueChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SkyBlue;
            this.panel3.Controls.Add(this.Grd1);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 120);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(760, 179);
            this.panel3.TabIndex = 33;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(478, 179);
            this.Grd1.TabIndex = 44;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtRequestedQty);
            this.panel6.Controls.Add(this.label47);
            this.panel6.Controls.Add(this.TxtOrderredQty);
            this.panel6.Controls.Add(this.label34);
            this.panel6.Controls.Add(this.TxtCommittedQty);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.TxtInStockQty);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(478, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(282, 179);
            this.panel6.TabIndex = 50;
            // 
            // TxtRequestedQty
            // 
            this.TxtRequestedQty.EnterMoveNextControl = true;
            this.TxtRequestedQty.Location = new System.Drawing.Point(163, 56);
            this.TxtRequestedQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRequestedQty.Name = "TxtRequestedQty";
            this.TxtRequestedQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRequestedQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRequestedQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRequestedQty.Properties.Appearance.Options.UseFont = true;
            this.TxtRequestedQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRequestedQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRequestedQty.Size = new System.Drawing.Size(112, 20);
            this.TxtRequestedQty.TabIndex = 56;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(50, 82);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(107, 14);
            this.label47.TabIndex = 57;
            this.label47.Text = "Orderred Quantity";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOrderredQty
            // 
            this.TxtOrderredQty.EnterMoveNextControl = true;
            this.TxtOrderredQty.Location = new System.Drawing.Point(163, 79);
            this.TxtOrderredQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOrderredQty.Name = "TxtOrderredQty";
            this.TxtOrderredQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOrderredQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOrderredQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOrderredQty.Properties.Appearance.Options.UseFont = true;
            this.TxtOrderredQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOrderredQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOrderredQty.Size = new System.Drawing.Size(112, 20);
            this.TxtOrderredQty.TabIndex = 58;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(40, 60);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(117, 14);
            this.label34.TabIndex = 55;
            this.label34.Text = "Requested Quantity";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCommittedQty
            // 
            this.TxtCommittedQty.EnterMoveNextControl = true;
            this.TxtCommittedQty.Location = new System.Drawing.Point(163, 33);
            this.TxtCommittedQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCommittedQty.Name = "TxtCommittedQty";
            this.TxtCommittedQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCommittedQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCommittedQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCommittedQty.Properties.Appearance.Options.UseFont = true;
            this.TxtCommittedQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCommittedQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCommittedQty.Size = new System.Drawing.Size(112, 20);
            this.TxtCommittedQty.TabIndex = 54;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(7, 36);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(150, 14);
            this.label33.TabIndex = 53;
            this.label33.Text = "Total Committed Quantity";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInStockQty
            // 
            this.TxtInStockQty.EnterMoveNextControl = true;
            this.TxtInStockQty.Location = new System.Drawing.Point(163, 10);
            this.TxtInStockQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInStockQty.Name = "TxtInStockQty";
            this.TxtInStockQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInStockQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInStockQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInStockQty.Properties.Appearance.Options.UseFont = true;
            this.TxtInStockQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInStockQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInStockQty.Size = new System.Drawing.Size(112, 20);
            this.TxtInStockQty.TabIndex = 52;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(21, 13);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(136, 14);
            this.label32.TabIndex = 51;
            this.label32.Text = "Total Quantity In Stock";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReOrderStock
            // 
            this.TxtReOrderStock.EnterMoveNextControl = true;
            this.TxtReOrderStock.Location = new System.Drawing.Point(389, 60);
            this.TxtReOrderStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReOrderStock.Name = "TxtReOrderStock";
            this.TxtReOrderStock.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtReOrderStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReOrderStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReOrderStock.Properties.Appearance.Options.UseFont = true;
            this.TxtReOrderStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtReOrderStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtReOrderStock.Size = new System.Drawing.Size(112, 20);
            this.TxtReOrderStock.TabIndex = 41;
            this.TxtReOrderStock.Validated += new System.EventHandler(this.TxtReOrderStock_Validated);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(300, 63);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(85, 14);
            this.label25.TabIndex = 40;
            this.label25.Text = "Reorder Stock";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMaxStock
            // 
            this.TxtMaxStock.EnterMoveNextControl = true;
            this.TxtMaxStock.Location = new System.Drawing.Point(389, 37);
            this.TxtMaxStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMaxStock.Name = "TxtMaxStock";
            this.TxtMaxStock.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMaxStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMaxStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMaxStock.Properties.Appearance.Options.UseFont = true;
            this.TxtMaxStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMaxStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMaxStock.Size = new System.Drawing.Size(112, 20);
            this.TxtMaxStock.TabIndex = 39;
            this.TxtMaxStock.Validated += new System.EventHandler(this.TxtMaxStock_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(293, 40);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 14);
            this.label24.TabIndex = 38;
            this.label24.Text = "Maximum Stock";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinStock
            // 
            this.TxtMinStock.EnterMoveNextControl = true;
            this.TxtMinStock.Location = new System.Drawing.Point(389, 14);
            this.TxtMinStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinStock.Name = "TxtMinStock";
            this.TxtMinStock.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinStock.Properties.Appearance.Options.UseFont = true;
            this.TxtMinStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinStock.Size = new System.Drawing.Size(112, 20);
            this.TxtMinStock.TabIndex = 37;
            this.TxtMinStock.Validated += new System.EventHandler(this.TxtMinStock_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(296, 17);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 14);
            this.label22.TabIndex = 36;
            this.label22.Text = "Minimum Stock";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(2, 17);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 14);
            this.label23.TabIndex = 31;
            this.label23.Text = "Inventory UoM";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInventoryUomCode
            // 
            this.LueInventoryUomCode.EnterMoveNextControl = true;
            this.LueInventoryUomCode.Location = new System.Drawing.Point(95, 14);
            this.LueInventoryUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueInventoryUomCode.Name = "LueInventoryUomCode";
            this.LueInventoryUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInventoryUomCode.Properties.DropDownRows = 30;
            this.LueInventoryUomCode.Properties.NullText = "[Empty]";
            this.LueInventoryUomCode.Properties.PopupWidth = 200;
            this.LueInventoryUomCode.Size = new System.Drawing.Size(141, 20);
            this.LueInventoryUomCode.TabIndex = 32;
            this.LueInventoryUomCode.ToolTip = "F4 : Show/hide list";
            this.LueInventoryUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInventoryUomCode.EditValueChanged += new System.EventHandler(this.LueInventoryUomCode_EditValueChanged);
            // 
            // TpgSales
            // 
            this.TpgSales.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgSales.Controls.Add(this.LueUomCode);
            this.TpgSales.Controls.Add(this.GrdSalesPckUnit);
            this.TpgSales.Controls.Add(this.panel7);
            this.TpgSales.Location = new System.Drawing.Point(4, 26);
            this.TpgSales.Name = "TpgSales";
            this.TpgSales.Size = new System.Drawing.Size(764, 303);
            this.TpgSales.TabIndex = 3;
            this.TpgSales.Text = "Sales Data";
            this.TpgSales.UseVisualStyleBackColor = true;
            // 
            // LueUomCode
            // 
            this.LueUomCode.EnterMoveNextControl = true;
            this.LueUomCode.Location = new System.Drawing.Point(201, 100);
            this.LueUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueUomCode.Name = "LueUomCode";
            this.LueUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUomCode.Properties.DropDownRows = 12;
            this.LueUomCode.Properties.NullText = "[Empty]";
            this.LueUomCode.Properties.PopupWidth = 500;
            this.LueUomCode.Size = new System.Drawing.Size(249, 20);
            this.LueUomCode.TabIndex = 39;
            this.LueUomCode.ToolTip = "F4 : Show/hide list";
            this.LueUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUomCode.EditValueChanged += new System.EventHandler(this.LueUomCode_EditValueChanged);
            this.LueUomCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUomCode_KeyDown);
            this.LueUomCode.Leave += new System.EventHandler(this.LueUomCode_Leave);
            this.LueUomCode.Validated += new System.EventHandler(this.LueUomCode_Validated);
            // 
            // GrdSalesPckUnit
            // 
            this.GrdSalesPckUnit.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.GrdSalesPckUnit.DefaultRow.Height = 20;
            this.GrdSalesPckUnit.DefaultRow.Sortable = false;
            this.GrdSalesPckUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdSalesPckUnit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdSalesPckUnit.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.GrdSalesPckUnit.Header.Height = 21;
            this.GrdSalesPckUnit.Location = new System.Drawing.Point(0, 79);
            this.GrdSalesPckUnit.Name = "GrdSalesPckUnit";
            this.GrdSalesPckUnit.RowHeader.Visible = true;
            this.GrdSalesPckUnit.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.GrdSalesPckUnit.SingleClickEdit = true;
            this.GrdSalesPckUnit.Size = new System.Drawing.Size(764, 224);
            this.GrdSalesPckUnit.TabIndex = 38;
            this.GrdSalesPckUnit.TreeCol = null;
            this.GrdSalesPckUnit.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.GrdSalesPckUnit.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdSalesPckUnit_RequestEdit);
            this.GrdSalesPckUnit.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdSalesPckUnit_AfterCommitEdit);
            this.GrdSalesPckUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdSalesPckUnit_KeyDown);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.LblSalesUOM1);
            this.panel7.Controls.Add(this.LblSalesUOM2);
            this.panel7.Controls.Add(this.TxtSalesConvertUOM2);
            this.panel7.Controls.Add(this.TxtSalesConvertUOM1);
            this.panel7.Controls.Add(this.label38);
            this.panel7.Controls.Add(this.label39);
            this.panel7.Controls.Add(this.LueSalesUomCode2);
            this.panel7.Controls.Add(this.label20);
            this.panel7.Controls.Add(this.LueSalesUomCode);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(764, 79);
            this.panel7.TabIndex = 67;
            // 
            // LblSalesUOM1
            // 
            this.LblSalesUOM1.AutoSize = true;
            this.LblSalesUOM1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSalesUOM1.ForeColor = System.Drawing.Color.Black;
            this.LblSalesUOM1.Location = new System.Drawing.Point(492, 54);
            this.LblSalesUOM1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSalesUOM1.Name = "LblSalesUOM1";
            this.LblSalesUOM1.Size = new System.Drawing.Size(0, 14);
            this.LblSalesUOM1.TabIndex = 74;
            this.LblSalesUOM1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSalesUOM2
            // 
            this.LblSalesUOM2.AutoSize = true;
            this.LblSalesUOM2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSalesUOM2.ForeColor = System.Drawing.Color.Black;
            this.LblSalesUOM2.Location = new System.Drawing.Point(492, 30);
            this.LblSalesUOM2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSalesUOM2.Name = "LblSalesUOM2";
            this.LblSalesUOM2.Size = new System.Drawing.Size(0, 14);
            this.LblSalesUOM2.TabIndex = 73;
            this.LblSalesUOM2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalesConvertUOM2
            // 
            this.TxtSalesConvertUOM2.EnterMoveNextControl = true;
            this.TxtSalesConvertUOM2.Location = new System.Drawing.Point(357, 51);
            this.TxtSalesConvertUOM2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesConvertUOM2.Name = "TxtSalesConvertUOM2";
            this.TxtSalesConvertUOM2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesConvertUOM2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesConvertUOM2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesConvertUOM2.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesConvertUOM2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesConvertUOM2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesConvertUOM2.Size = new System.Drawing.Size(125, 20);
            this.TxtSalesConvertUOM2.TabIndex = 36;
            // 
            // TxtSalesConvertUOM1
            // 
            this.TxtSalesConvertUOM1.EnterMoveNextControl = true;
            this.TxtSalesConvertUOM1.Location = new System.Drawing.Point(357, 28);
            this.TxtSalesConvertUOM1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesConvertUOM1.Name = "TxtSalesConvertUOM1";
            this.TxtSalesConvertUOM1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesConvertUOM1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesConvertUOM1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesConvertUOM1.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesConvertUOM1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesConvertUOM1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesConvertUOM1.Size = new System.Drawing.Size(125, 20);
            this.TxtSalesConvertUOM1.TabIndex = 34;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(388, 6);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(66, 14);
            this.label38.TabIndex = 37;
            this.label38.Text = "Conversion";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(292, 31);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(64, 14);
            this.label39.TabIndex = 33;
            this.label39.Text = "( Primary )";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSalesUomCode2
            // 
            this.LueSalesUomCode2.EnterMoveNextControl = true;
            this.LueSalesUomCode2.Location = new System.Drawing.Point(85, 51);
            this.LueSalesUomCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueSalesUomCode2.Name = "LueSalesUomCode2";
            this.LueSalesUomCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode2.Properties.Appearance.Options.UseFont = true;
            this.LueSalesUomCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSalesUomCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSalesUomCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSalesUomCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSalesUomCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSalesUomCode2.Properties.DropDownRows = 30;
            this.LueSalesUomCode2.Properties.NullText = "[Empty]";
            this.LueSalesUomCode2.Properties.PopupWidth = 300;
            this.LueSalesUomCode2.Size = new System.Drawing.Size(205, 20);
            this.LueSalesUomCode2.TabIndex = 35;
            this.LueSalesUomCode2.ToolTip = "F4 : Show/hide list";
            this.LueSalesUomCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSalesUomCode2.EditValueChanged += new System.EventHandler(this.LueSalesUomCode2_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(16, 31);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 14);
            this.label20.TabIndex = 31;
            this.label20.Text = "Sales UoM";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSalesUomCode
            // 
            this.LueSalesUomCode.EnterMoveNextControl = true;
            this.LueSalesUomCode.Location = new System.Drawing.Point(85, 28);
            this.LueSalesUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSalesUomCode.Name = "LueSalesUomCode";
            this.LueSalesUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueSalesUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSalesUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSalesUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSalesUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSalesUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSalesUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSalesUomCode.Properties.DropDownRows = 30;
            this.LueSalesUomCode.Properties.NullText = "[Empty]";
            this.LueSalesUomCode.Properties.PopupWidth = 300;
            this.LueSalesUomCode.Size = new System.Drawing.Size(205, 20);
            this.LueSalesUomCode.TabIndex = 32;
            this.LueSalesUomCode.ToolTip = "F4 : Show/hide list";
            this.LueSalesUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSalesUomCode.EditValueChanged += new System.EventHandler(this.LueSalesUomCode_EditValueChanged);
            // 
            // TpgPlanning
            // 
            this.TpgPlanning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgPlanning.Controls.Add(this.TxtPlanningUomCodeConvert21);
            this.TpgPlanning.Controls.Add(this.TxtPlanningUomCodeConvert12);
            this.TpgPlanning.Controls.Add(this.label50);
            this.TpgPlanning.Controls.Add(this.LuePlanningUomCode2);
            this.TpgPlanning.Controls.Add(this.label46);
            this.TpgPlanning.Controls.Add(this.label21);
            this.TpgPlanning.Controls.Add(this.LuePlanningUomCode);
            this.TpgPlanning.Location = new System.Drawing.Point(4, 26);
            this.TpgPlanning.Name = "TpgPlanning";
            this.TpgPlanning.Size = new System.Drawing.Size(764, 303);
            this.TpgPlanning.TabIndex = 4;
            this.TpgPlanning.Text = "Planning Data";
            this.TpgPlanning.UseVisualStyleBackColor = true;
            // 
            // TxtPlanningUomCodeConvert21
            // 
            this.TxtPlanningUomCodeConvert21.EnterMoveNextControl = true;
            this.TxtPlanningUomCodeConvert21.Location = new System.Drawing.Point(501, 59);
            this.TxtPlanningUomCodeConvert21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlanningUomCodeConvert21.Name = "TxtPlanningUomCodeConvert21";
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPlanningUomCodeConvert21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPlanningUomCodeConvert21.Size = new System.Drawing.Size(125, 20);
            this.TxtPlanningUomCodeConvert21.TabIndex = 36;
            this.TxtPlanningUomCodeConvert21.Validated += new System.EventHandler(this.TxtPlanningUomCodeConvert21_Validated);
            // 
            // TxtPlanningUomCodeConvert12
            // 
            this.TxtPlanningUomCodeConvert12.EnterMoveNextControl = true;
            this.TxtPlanningUomCodeConvert12.Location = new System.Drawing.Point(501, 37);
            this.TxtPlanningUomCodeConvert12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlanningUomCodeConvert12.Name = "TxtPlanningUomCodeConvert12";
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPlanningUomCodeConvert12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPlanningUomCodeConvert12.Size = new System.Drawing.Size(125, 20);
            this.TxtPlanningUomCodeConvert12.TabIndex = 34;
            this.TxtPlanningUomCodeConvert12.Validated += new System.EventHandler(this.TxtPlanningUomCodeConvert12_Validated);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(532, 18);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(66, 14);
            this.label50.TabIndex = 37;
            this.label50.Text = "Conversion";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePlanningUomCode2
            // 
            this.LuePlanningUomCode2.EnterMoveNextControl = true;
            this.LuePlanningUomCode2.Location = new System.Drawing.Point(105, 59);
            this.LuePlanningUomCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LuePlanningUomCode2.Name = "LuePlanningUomCode2";
            this.LuePlanningUomCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode2.Properties.Appearance.Options.UseFont = true;
            this.LuePlanningUomCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePlanningUomCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePlanningUomCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePlanningUomCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePlanningUomCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePlanningUomCode2.Properties.DropDownRows = 30;
            this.LuePlanningUomCode2.Properties.NullText = "[Empty]";
            this.LuePlanningUomCode2.Properties.PopupWidth = 350;
            this.LuePlanningUomCode2.Size = new System.Drawing.Size(319, 20);
            this.LuePlanningUomCode2.TabIndex = 35;
            this.LuePlanningUomCode2.ToolTip = "F4 : Show/hide list";
            this.LuePlanningUomCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePlanningUomCode2.EditValueChanged += new System.EventHandler(this.LuePlanningUomCode2_EditValueChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(427, 39);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(64, 14);
            this.label46.TabIndex = 33;
            this.label46.Text = "( Primary )";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(20, 40);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 14);
            this.label21.TabIndex = 31;
            this.label21.Text = "Planning UoM";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePlanningUomCode
            // 
            this.LuePlanningUomCode.EnterMoveNextControl = true;
            this.LuePlanningUomCode.Location = new System.Drawing.Point(105, 37);
            this.LuePlanningUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePlanningUomCode.Name = "LuePlanningUomCode";
            this.LuePlanningUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode.Properties.Appearance.Options.UseFont = true;
            this.LuePlanningUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePlanningUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePlanningUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePlanningUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePlanningUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePlanningUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePlanningUomCode.Properties.DropDownRows = 30;
            this.LuePlanningUomCode.Properties.NullText = "[Empty]";
            this.LuePlanningUomCode.Properties.PopupWidth = 350;
            this.LuePlanningUomCode.Size = new System.Drawing.Size(319, 20);
            this.LuePlanningUomCode.TabIndex = 32;
            this.LuePlanningUomCode.ToolTip = "F4 : Show/hide list";
            this.LuePlanningUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePlanningUomCode.EditValueChanged += new System.EventHandler(this.LuePlanningUomCode_EditValueChanged);
            // 
            // TpgFixedAsset
            // 
            this.TpgFixedAsset.Controls.Add(this.Grd2);
            this.TpgFixedAsset.Location = new System.Drawing.Point(4, 26);
            this.TpgFixedAsset.Name = "TpgFixedAsset";
            this.TpgFixedAsset.Size = new System.Drawing.Size(764, 303);
            this.TpgFixedAsset.TabIndex = 7;
            this.TpgFixedAsset.Text = "Fixed Asset";
            this.TpgFixedAsset.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.GroupBox.Visible = true;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 303);
            this.Grd2.TabIndex = 31;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // TpgProperty
            // 
            this.TpgProperty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgProperty.Controls.Add(this.LueProperty);
            this.TpgProperty.Controls.Add(this.GrdProperty);
            this.TpgProperty.Controls.Add(this.panel9);
            this.TpgProperty.Location = new System.Drawing.Point(4, 26);
            this.TpgProperty.Name = "TpgProperty";
            this.TpgProperty.Size = new System.Drawing.Size(764, 303);
            this.TpgProperty.TabIndex = 5;
            this.TpgProperty.Text = "Property";
            this.TpgProperty.UseVisualStyleBackColor = true;
            // 
            // LueProperty
            // 
            this.LueProperty.EnterMoveNextControl = true;
            this.LueProperty.Location = new System.Drawing.Point(182, 137);
            this.LueProperty.Margin = new System.Windows.Forms.Padding(5);
            this.LueProperty.Name = "LueProperty";
            this.LueProperty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProperty.Properties.Appearance.Options.UseFont = true;
            this.LueProperty.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProperty.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProperty.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProperty.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProperty.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProperty.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProperty.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProperty.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProperty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProperty.Properties.DropDownRows = 12;
            this.LueProperty.Properties.NullText = "[Empty]";
            this.LueProperty.Properties.PopupWidth = 500;
            this.LueProperty.Size = new System.Drawing.Size(249, 20);
            this.LueProperty.TabIndex = 42;
            this.LueProperty.ToolTip = "F4 : Show/hide list";
            this.LueProperty.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProperty.EditValueChanged += new System.EventHandler(this.LueProperty_EditValueChanged);
            this.LueProperty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProperty_KeyDown);
            this.LueProperty.Leave += new System.EventHandler(this.LueProperty_Leave);
            // 
            // GrdProperty
            // 
            this.GrdProperty.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.GrdProperty.DefaultRow.Height = 20;
            this.GrdProperty.DefaultRow.Sortable = false;
            this.GrdProperty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdProperty.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdProperty.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.GrdProperty.Header.Height = 21;
            this.GrdProperty.Location = new System.Drawing.Point(0, 116);
            this.GrdProperty.Name = "GrdProperty";
            this.GrdProperty.RowHeader.Visible = true;
            this.GrdProperty.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.GrdProperty.SingleClickEdit = true;
            this.GrdProperty.Size = new System.Drawing.Size(764, 187);
            this.GrdProperty.TabIndex = 41;
            this.GrdProperty.TreeCol = null;
            this.GrdProperty.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.GrdProperty.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdProperty_RequestEdit);
            this.GrdProperty.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdProperty_AfterCommitEdit);
            this.GrdProperty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdProperty_KeyDown);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.LblInformation5);
            this.panel9.Controls.Add(this.LueInformation5);
            this.panel9.Controls.Add(this.LblInformation4);
            this.panel9.Controls.Add(this.LueInformation4);
            this.panel9.Controls.Add(this.LblInformation3);
            this.panel9.Controls.Add(this.LueInformation3);
            this.panel9.Controls.Add(this.LblInformation2);
            this.panel9.Controls.Add(this.LueInformation2);
            this.panel9.Controls.Add(this.LblInformation1);
            this.panel9.Controls.Add(this.LueInformation1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(764, 116);
            this.panel9.TabIndex = 24;
            // 
            // LblInformation5
            // 
            this.LblInformation5.AutoSize = true;
            this.LblInformation5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInformation5.ForeColor = System.Drawing.Color.Black;
            this.LblInformation5.Location = new System.Drawing.Point(12, 93);
            this.LblInformation5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInformation5.Name = "LblInformation5";
            this.LblInformation5.Size = new System.Drawing.Size(81, 14);
            this.LblInformation5.TabIndex = 39;
            this.LblInformation5.Text = "Information 5";
            this.LblInformation5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInformation5
            // 
            this.LueInformation5.EnterMoveNextControl = true;
            this.LueInformation5.Location = new System.Drawing.Point(97, 90);
            this.LueInformation5.Margin = new System.Windows.Forms.Padding(5);
            this.LueInformation5.Name = "LueInformation5";
            this.LueInformation5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation5.Properties.Appearance.Options.UseFont = true;
            this.LueInformation5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInformation5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInformation5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInformation5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInformation5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInformation5.Properties.DropDownRows = 30;
            this.LueInformation5.Properties.NullText = "[Empty]";
            this.LueInformation5.Properties.PopupWidth = 500;
            this.LueInformation5.Size = new System.Drawing.Size(319, 20);
            this.LueInformation5.TabIndex = 40;
            this.LueInformation5.ToolTip = "F4 : Show/hide list";
            this.LueInformation5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInformation5.EditValueChanged += new System.EventHandler(this.LueInformation5_EditValueChanged);
            // 
            // LblInformation4
            // 
            this.LblInformation4.AutoSize = true;
            this.LblInformation4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInformation4.ForeColor = System.Drawing.Color.Black;
            this.LblInformation4.Location = new System.Drawing.Point(12, 72);
            this.LblInformation4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInformation4.Name = "LblInformation4";
            this.LblInformation4.Size = new System.Drawing.Size(81, 14);
            this.LblInformation4.TabIndex = 37;
            this.LblInformation4.Text = "Information 4";
            this.LblInformation4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInformation4
            // 
            this.LueInformation4.EnterMoveNextControl = true;
            this.LueInformation4.Location = new System.Drawing.Point(97, 69);
            this.LueInformation4.Margin = new System.Windows.Forms.Padding(5);
            this.LueInformation4.Name = "LueInformation4";
            this.LueInformation4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation4.Properties.Appearance.Options.UseFont = true;
            this.LueInformation4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInformation4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInformation4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInformation4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInformation4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInformation4.Properties.DropDownRows = 30;
            this.LueInformation4.Properties.NullText = "[Empty]";
            this.LueInformation4.Properties.PopupWidth = 500;
            this.LueInformation4.Size = new System.Drawing.Size(319, 20);
            this.LueInformation4.TabIndex = 38;
            this.LueInformation4.ToolTip = "F4 : Show/hide list";
            this.LueInformation4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInformation4.EditValueChanged += new System.EventHandler(this.LueInformation4_EditValueChanged);
            // 
            // LblInformation3
            // 
            this.LblInformation3.AutoSize = true;
            this.LblInformation3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInformation3.ForeColor = System.Drawing.Color.Black;
            this.LblInformation3.Location = new System.Drawing.Point(12, 51);
            this.LblInformation3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInformation3.Name = "LblInformation3";
            this.LblInformation3.Size = new System.Drawing.Size(81, 14);
            this.LblInformation3.TabIndex = 35;
            this.LblInformation3.Text = "Information 3";
            this.LblInformation3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInformation3
            // 
            this.LueInformation3.EnterMoveNextControl = true;
            this.LueInformation3.Location = new System.Drawing.Point(97, 48);
            this.LueInformation3.Margin = new System.Windows.Forms.Padding(5);
            this.LueInformation3.Name = "LueInformation3";
            this.LueInformation3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation3.Properties.Appearance.Options.UseFont = true;
            this.LueInformation3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInformation3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInformation3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInformation3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInformation3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInformation3.Properties.DropDownRows = 30;
            this.LueInformation3.Properties.NullText = "[Empty]";
            this.LueInformation3.Properties.PopupWidth = 500;
            this.LueInformation3.Size = new System.Drawing.Size(319, 20);
            this.LueInformation3.TabIndex = 36;
            this.LueInformation3.ToolTip = "F4 : Show/hide list";
            this.LueInformation3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInformation3.EditValueChanged += new System.EventHandler(this.LueInformation3_EditValueChanged);
            // 
            // LblInformation2
            // 
            this.LblInformation2.AutoSize = true;
            this.LblInformation2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInformation2.ForeColor = System.Drawing.Color.Black;
            this.LblInformation2.Location = new System.Drawing.Point(12, 30);
            this.LblInformation2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInformation2.Name = "LblInformation2";
            this.LblInformation2.Size = new System.Drawing.Size(81, 14);
            this.LblInformation2.TabIndex = 33;
            this.LblInformation2.Text = "Information 2";
            this.LblInformation2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInformation2
            // 
            this.LueInformation2.EnterMoveNextControl = true;
            this.LueInformation2.Location = new System.Drawing.Point(97, 27);
            this.LueInformation2.Margin = new System.Windows.Forms.Padding(5);
            this.LueInformation2.Name = "LueInformation2";
            this.LueInformation2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation2.Properties.Appearance.Options.UseFont = true;
            this.LueInformation2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInformation2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInformation2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInformation2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInformation2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInformation2.Properties.DropDownRows = 30;
            this.LueInformation2.Properties.NullText = "[Empty]";
            this.LueInformation2.Properties.PopupWidth = 500;
            this.LueInformation2.Size = new System.Drawing.Size(319, 20);
            this.LueInformation2.TabIndex = 34;
            this.LueInformation2.ToolTip = "F4 : Show/hide list";
            this.LueInformation2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInformation2.EditValueChanged += new System.EventHandler(this.LueInformation2_EditValueChanged);
            // 
            // LblInformation1
            // 
            this.LblInformation1.AutoSize = true;
            this.LblInformation1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInformation1.ForeColor = System.Drawing.Color.Black;
            this.LblInformation1.Location = new System.Drawing.Point(12, 9);
            this.LblInformation1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInformation1.Name = "LblInformation1";
            this.LblInformation1.Size = new System.Drawing.Size(81, 14);
            this.LblInformation1.TabIndex = 31;
            this.LblInformation1.Text = "Information 1";
            this.LblInformation1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInformation1
            // 
            this.LueInformation1.EnterMoveNextControl = true;
            this.LueInformation1.Location = new System.Drawing.Point(97, 6);
            this.LueInformation1.Margin = new System.Windows.Forms.Padding(5);
            this.LueInformation1.Name = "LueInformation1";
            this.LueInformation1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation1.Properties.Appearance.Options.UseFont = true;
            this.LueInformation1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInformation1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInformation1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInformation1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInformation1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInformation1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInformation1.Properties.DropDownRows = 30;
            this.LueInformation1.Properties.NullText = "[Empty]";
            this.LueInformation1.Properties.PopupWidth = 500;
            this.LueInformation1.Size = new System.Drawing.Size(319, 20);
            this.LueInformation1.TabIndex = 32;
            this.LueInformation1.ToolTip = "F4 : Show/hide list";
            this.LueInformation1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInformation1.EditValueChanged += new System.EventHandler(this.LueInformation1_EditValueChanged);
            // 
            // TpgAttachment
            // 
            this.TpgAttachment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgAttachment.Controls.Add(this.splitContainer4);
            this.TpgAttachment.Location = new System.Drawing.Point(4, 26);
            this.TpgAttachment.Name = "TpgAttachment";
            this.TpgAttachment.Size = new System.Drawing.Size(764, 303);
            this.TpgAttachment.TabIndex = 6;
            this.TpgAttachment.Text = "Attachment";
            this.TpgAttachment.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.PicItem);
            this.splitContainer4.Panel1.Controls.Add(this.BtnDownload);
            this.splitContainer4.Panel1.Controls.Add(this.BtnPicture);
            this.splitContainer4.Panel1.Controls.Add(this.button4);
            this.splitContainer4.Panel1.Controls.Add(this.button3);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.Grd3);
            this.splitContainer4.Panel2.Controls.Add(this.button2);
            this.splitContainer4.Size = new System.Drawing.Size(764, 303);
            this.splitContainer4.SplitterDistance = 364;
            this.splitContainer4.TabIndex = 35;
            // 
            // PicItem
            // 
            this.PicItem.Location = new System.Drawing.Point(12, 72);
            this.PicItem.Name = "PicItem";
            this.PicItem.Size = new System.Drawing.Size(368, 223);
            this.PicItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicItem.TabIndex = 36;
            this.PicItem.TabStop = false;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(50, 31);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 33;
            this.BtnDownload.ToolTip = "Download Item\'s Picture";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // BtnPicture
            // 
            this.BtnPicture.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPicture.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPicture.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPicture.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPicture.Appearance.Options.UseBackColor = true;
            this.BtnPicture.Appearance.Options.UseFont = true;
            this.BtnPicture.Appearance.Options.UseForeColor = true;
            this.BtnPicture.Appearance.Options.UseTextOptions = true;
            this.BtnPicture.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPicture.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPicture.Image = ((System.Drawing.Image)(resources.GetObject("BtnPicture.Image")));
            this.BtnPicture.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPicture.Location = new System.Drawing.Point(9, 31);
            this.BtnPicture.Name = "BtnPicture";
            this.BtnPicture.Size = new System.Drawing.Size(24, 21);
            this.BtnPicture.TabIndex = 32;
            this.BtnPicture.ToolTip = "Show Item\'s Picture";
            this.BtnPicture.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPicture.ToolTipTitle = "Run System";
            this.BtnPicture.Click += new System.EventHandler(this.BtnPicture_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.AliceBlue;
            this.button4.Location = new System.Drawing.Point(0, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(364, 40);
            this.button4.TabIndex = 23;
            this.button4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.SteelBlue;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.AliceBlue;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(364, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "Picture";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 23);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(396, 280);
            this.Grd3.TabIndex = 34;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd3_RequestCellToolTipText);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.AliceBlue;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(396, 23);
            this.button2.TabIndex = 31;
            this.button2.Text = "Attachment File";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(276, 4);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.ChkServiceItemInd);
            this.panel4.Controls.Add(this.ChkPlanningItemInd);
            this.panel4.Controls.Add(this.ChkFixedItemInd);
            this.panel4.Controls.Add(this.ChkPurchaseItemInd);
            this.panel4.Controls.Add(this.ChkSalesItemInd);
            this.panel4.Controls.Add(this.ChkInventoryItemInd);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(631, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 139);
            this.panel4.TabIndex = 23;
            // 
            // ChkServiceItemInd
            // 
            this.ChkServiceItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkServiceItemInd.Location = new System.Drawing.Point(79, 114);
            this.ChkServiceItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkServiceItemInd.Name = "ChkServiceItemInd";
            this.ChkServiceItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkServiceItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkServiceItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkServiceItemInd.Properties.Caption = "Service Item";
            this.ChkServiceItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkServiceItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkServiceItemInd.TabIndex = 30;
            // 
            // ChkPlanningItemInd
            // 
            this.ChkPlanningItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPlanningItemInd.Location = new System.Drawing.Point(79, 92);
            this.ChkPlanningItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPlanningItemInd.Name = "ChkPlanningItemInd";
            this.ChkPlanningItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPlanningItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPlanningItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPlanningItemInd.Properties.Caption = "Planning Item";
            this.ChkPlanningItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPlanningItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkPlanningItemInd.TabIndex = 29;
            // 
            // ChkFixedItemInd
            // 
            this.ChkFixedItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFixedItemInd.Location = new System.Drawing.Point(79, 70);
            this.ChkFixedItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFixedItemInd.Name = "ChkFixedItemInd";
            this.ChkFixedItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFixedItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFixedItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFixedItemInd.Properties.Caption = "Fixed Item";
            this.ChkFixedItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFixedItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkFixedItemInd.TabIndex = 28;
            // 
            // ChkPurchaseItemInd
            // 
            this.ChkPurchaseItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPurchaseItemInd.Location = new System.Drawing.Point(79, 48);
            this.ChkPurchaseItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPurchaseItemInd.Name = "ChkPurchaseItemInd";
            this.ChkPurchaseItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPurchaseItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPurchaseItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPurchaseItemInd.Properties.Caption = "Purchase Item";
            this.ChkPurchaseItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPurchaseItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkPurchaseItemInd.TabIndex = 27;
            // 
            // ChkSalesItemInd
            // 
            this.ChkSalesItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSalesItemInd.Location = new System.Drawing.Point(79, 26);
            this.ChkSalesItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSalesItemInd.Name = "ChkSalesItemInd";
            this.ChkSalesItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSalesItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSalesItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSalesItemInd.Properties.Caption = "Sales Item";
            this.ChkSalesItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSalesItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkSalesItemInd.TabIndex = 26;
            // 
            // ChkInventoryItemInd
            // 
            this.ChkInventoryItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkInventoryItemInd.Location = new System.Drawing.Point(79, 4);
            this.ChkInventoryItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkInventoryItemInd.Name = "ChkInventoryItemInd";
            this.ChkInventoryItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkInventoryItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInventoryItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkInventoryItemInd.Properties.Caption = "Inventory Item";
            this.ChkInventoryItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInventoryItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkInventoryItemInd.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 24;
            this.label6.Text = "Item Type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(206, 80);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 12;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit1.TabIndex = 34;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // checkEdit1
            // 
            this.checkEdit1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkEdit1.Location = new System.Drawing.Point(20, 79);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.checkEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "Material Request is issued by";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.checkEdit1.Properties.ReadOnly = true;
            this.checkEdit1.Size = new System.Drawing.Size(186, 22);
            this.checkEdit1.TabIndex = 33;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(282, 8);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 14);
            this.label27.TabIndex = 34;
            this.label27.Text = "( Primary )";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(113, 51);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 12;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit2.TabIndex = 26;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(113, 28);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit3.TabIndex = 25;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.iGrid1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 123);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(760, 197);
            this.panel5.TabIndex = 33;
            // 
            // iGrid1
            // 
            this.iGrid1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.iGrid1.BackColorOddRows = System.Drawing.Color.White;
            this.iGrid1.DefaultAutoGroupRow.Height = 21;
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.FrozenArea.ColCount = 3;
            this.iGrid1.FrozenArea.SortFrozenRows = true;
            this.iGrid1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.iGrid1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.iGrid1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.iGrid1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 16;
            this.iGrid1.Header.UseXPStyles = false;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.ProcessTab = false;
            this.iGrid1.ReadOnly = true;
            this.iGrid1.RowTextStartColNear = 3;
            this.iGrid1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.iGrid1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.iGrid1.SearchAsType.SearchCol = null;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(760, 101);
            this.iGrid1.TabIndex = 35;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(451, 51);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit1.Size = new System.Drawing.Size(166, 20);
            this.textEdit1.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(362, 54);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 14);
            this.label28.TabIndex = 31;
            this.label28.Text = "Reorder Stock";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(451, 28);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit2.Size = new System.Drawing.Size(166, 20);
            this.textEdit2.TabIndex = 30;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(355, 31);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 14);
            this.label29.TabIndex = 29;
            this.label29.Text = "Maximum Stock";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(451, 5);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit3.Size = new System.Drawing.Size(166, 20);
            this.textEdit3.TabIndex = 28;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(358, 8);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 14);
            this.label30.TabIndex = 27;
            this.label30.Text = "Minimum Stock";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(20, 8);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 14);
            this.label31.TabIndex = 23;
            this.label31.Text = "Inventory UoM";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(113, 5);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit4.TabIndex = 24;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtItCodeInternal
            // 
            this.TxtItCodeInternal.EnterMoveNextControl = true;
            this.TxtItCodeInternal.Location = new System.Drawing.Point(105, 47);
            this.TxtItCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeInternal.Name = "TxtItCodeInternal";
            this.TxtItCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeInternal.Properties.MaxLength = 30;
            this.TxtItCodeInternal.Size = new System.Drawing.Size(451, 20);
            this.TxtItCodeInternal.TabIndex = 18;
            this.TxtItCodeInternal.Validated += new System.EventHandler(this.TxtItCodeInternal_Validated);
            // 
            // LblItCodeInternal
            // 
            this.LblItCodeInternal.AutoSize = true;
            this.LblItCodeInternal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItCodeInternal.ForeColor = System.Drawing.Color.Black;
            this.LblItCodeInternal.Location = new System.Drawing.Point(6, 50);
            this.LblItCodeInternal.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItCodeInternal.Name = "LblItCodeInternal";
            this.LblItCodeInternal.Size = new System.Drawing.Size(96, 14);
            this.LblItCodeInternal.TabIndex = 17;
            this.LblItCodeInternal.Text = "Item Local Code";
            this.LblItCodeInternal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnItem
            // 
            this.BtnItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItem.Appearance.Options.UseBackColor = true;
            this.BtnItem.Appearance.Options.UseFont = true;
            this.BtnItem.Appearance.Options.UseForeColor = true;
            this.BtnItem.Appearance.Options.UseTextOptions = true;
            this.BtnItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItem.Location = new System.Drawing.Point(559, 4);
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Size = new System.Drawing.Size(24, 21);
            this.BtnItem.TabIndex = 14;
            this.BtnItem.ToolTip = "Find Item";
            this.BtnItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItem.ToolTipTitle = "Run System";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(411, 5);
            this.TxtSource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Properties.MaxLength = 80;
            this.TxtSource.Properties.ReadOnly = true;
            this.TxtSource.Size = new System.Drawing.Size(145, 20);
            this.TxtSource.TabIndex = 13;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(364, 7);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(45, 14);
            this.label48.TabIndex = 12;
            this.label48.Text = "Source";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItemRequestDocNo
            // 
            this.TxtItemRequestDocNo.EnterMoveNextControl = true;
            this.TxtItemRequestDocNo.Location = new System.Drawing.Point(105, 110);
            this.TxtItemRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItemRequestDocNo.Name = "TxtItemRequestDocNo";
            this.TxtItemRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItemRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItemRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItemRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtItemRequestDocNo.Properties.MaxLength = 250;
            this.TxtItemRequestDocNo.Properties.ReadOnly = true;
            this.TxtItemRequestDocNo.Size = new System.Drawing.Size(451, 20);
            this.TxtItemRequestDocNo.TabIndex = 24;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(11, 113);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(91, 14);
            this.label49.TabIndex = 23;
            this.label49.Text = "Item Request#";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // TxtItCodeOld
            // 
            this.TxtItCodeOld.EnterMoveNextControl = true;
            this.TxtItCodeOld.Location = new System.Drawing.Point(105, 89);
            this.TxtItCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeOld.Name = "TxtItCodeOld";
            this.TxtItCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeOld.Properties.MaxLength = 30;
            this.TxtItCodeOld.Size = new System.Drawing.Size(451, 20);
            this.TxtItCodeOld.TabIndex = 22;
            this.TxtItCodeOld.Validated += new System.EventHandler(this.TxtItCodeOld_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(45, 92);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 14);
            this.label14.TabIndex = 21;
            this.label14.Text = "Old Code";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 472);
            this.Name = "FrmItem";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxLiableInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItBrCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            this.TpgPurchasing.ResumeLayout(false);
            this.TpgPurchasing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWeightUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDiameterUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiameter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVolumeUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWidthUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueHeightUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePurchaseUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            this.TpgInventory.ResumeLayout(false);
            this.TpgInventory.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.TpgConversion1.ResumeLayout(false);
            this.TpgConversion1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM12.Properties)).EndInit();
            this.TpgConversion2.ResumeLayout(false);
            this.TpgConversion2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM21.Properties)).EndInit();
            this.TpgConversion3.ResumeLayout(false);
            this.TpgConversion3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvConvertUOM31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueControlByDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkControlByDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode2.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRequestedQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOrderredQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCommittedQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInStockQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReOrderStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode.Properties)).EndInit();
            this.TpgSales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdSalesPckUnit)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesConvertUOM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesConvertUOM1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSalesUomCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSalesUomCode.Properties)).EndInit();
            this.TpgPlanning.ResumeLayout(false);
            this.TpgPlanning.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCodeConvert21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCodeConvert12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePlanningUomCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePlanningUomCode.Properties)).EndInit();
            this.TpgFixedAsset.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgProperty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueProperty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdProperty)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInformation1.Properties)).EndInit();
            this.TpgAttachment.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPurchaseItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInventoryItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItemRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeOld.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        private System.Windows.Forms.TabPage TpgPurchasing;
        private System.Windows.Forms.TabPage TpgInventory;
        private System.Windows.Forms.TabPage TpgSales;
        private System.Windows.Forms.TabPage TpgPlanning;
        private System.Windows.Forms.TabPage TpgProperty;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.CheckEdit ChkTaxLiableInd;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueItBrCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueItScCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        private DevExpress.XtraEditors.MemoExEdit MeeSpecification;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LuePurchaseUomCode;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueVdCode;
        internal DevExpress.XtraEditors.TextEdit TxtVolume;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtWidth;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtHeight;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtLength;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtMinOrder;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueVolumeUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueWidthUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueHeightUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueLengthUomCode;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LuePlanningUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtReOrderStock;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtMaxStock;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtMinStock;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.LookUpEdit LueInventoryUomCode;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkFixedItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkPurchaseItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkSalesItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkInventoryItemInd;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueInventoryUomCode3;
        private DevExpress.XtraEditors.LookUpEdit LueInventoryUomCode2;
        private DevExpress.XtraEditors.CheckEdit ChkPlanningItemInd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage TpgFixedAsset;
        protected TenTec.Windows.iGridLib.iGrid Grd1;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LueControlByDeptCode;
        private DevExpress.XtraEditors.CheckEdit ChkControlByDeptCode;
        protected System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        protected System.Windows.Forms.Panel panel5;
        protected TenTec.Windows.iGridLib.iGrid iGrid1;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        internal DevExpress.XtraEditors.TextEdit TxtCommittedQty;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtInStockQty;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtOrderredQty;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueDiameterUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtDiameter;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label LblItCodeInternal;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label LblSalesUOM1;
        private System.Windows.Forms.Label LblSalesUOM2;
        internal DevExpress.XtraEditors.TextEdit TxtSalesConvertUOM2;
        internal DevExpress.XtraEditors.TextEdit TxtSalesConvertUOM1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private DevExpress.XtraEditors.LookUpEdit LueSalesUomCode2;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.LookUpEdit LueSalesUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueUomCode;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage TpgConversion1;
        private System.Windows.Forms.TabPage TpgConversion2;
        private System.Windows.Forms.TabPage TpgConversion3;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM13;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM12;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM23;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM21;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM32;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtInvConvertUOM31;
        private System.Windows.Forms.Label label45;
        private DevExpress.XtraEditors.LookUpEdit LueProperty;
        protected internal TenTec.Windows.iGridLib.iGrid GrdProperty;
        protected internal TenTec.Windows.iGridLib.iGrid GrdSalesPckUnit;
        private DevExpress.XtraEditors.LookUpEdit LuePlanningUomCode2;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtRequestedQty;
        private System.Windows.Forms.Label label47;
        public DevExpress.XtraEditors.SimpleButton BtnItem;
        internal DevExpress.XtraEditors.TextEdit TxtSource;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label LblItGrpCode;
        private DevExpress.XtraEditors.LookUpEdit LueItGrpCode;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label LblInformation1;
        private DevExpress.XtraEditors.LookUpEdit LueInformation1;
        private System.Windows.Forms.Label LblInformation5;
        private DevExpress.XtraEditors.LookUpEdit LueInformation5;
        private System.Windows.Forms.Label LblInformation4;
        private DevExpress.XtraEditors.LookUpEdit LueInformation4;
        private System.Windows.Forms.Label LblInformation3;
        private DevExpress.XtraEditors.LookUpEdit LueInformation3;
        private System.Windows.Forms.Label LblInformation2;
        private DevExpress.XtraEditors.LookUpEdit LueInformation2;
        internal DevExpress.XtraEditors.TextEdit TxtPlanningUomCodeConvert21;
        internal DevExpress.XtraEditors.TextEdit TxtPlanningUomCodeConvert12;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.TextEdit TxtItemRequestDocNo;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.TextEdit TxtForeignName;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        internal DevExpress.XtraEditors.TextEdit TxtItCodeInternal;
        private DevExpress.XtraEditors.TextEdit TxtHSCode;
        private System.Windows.Forms.Label label51;
        private DevExpress.XtraEditors.CheckEdit ChkServiceItemInd;
        private DevExpress.XtraEditors.LookUpEdit LueWeightUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtWeight;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TabPage TpgAttachment;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.Button button2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        public DevExpress.XtraEditors.SimpleButton BtnPicture;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.PictureBox PicItem;
        internal DevExpress.XtraEditors.TextEdit TxtItCodeOld;
        private System.Windows.Forms.Label label14;
    }
}