﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSubDistrict : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSubDistrictFind FrmFind;

        #endregion

        #region Constructor

        public FrmSubDistrict(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            Sl.SetLueCityCode(ref LueCityCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSDCode, TxtSDName, LueCityCode
                    }, true);
                    TxtSDCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSDCode, TxtSDName, LueCityCode
                    }, false);
                    TxtSDCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSDName, LueCityCode
                    }, false);
                    TxtSDName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSDCode, TxtSDName, LueCityCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSubDistrictFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSDCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSDCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSubDistrict Where SDCode=@SDCode" };
                Sm.CmParam<String>(ref cm, "@SDCode", TxtSDCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (ICityataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSubDistrict(SDCode, SDName, CityCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SDCode, @SDName, @CityCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update SDName=@SDName, CityCode=@CityCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@SDCode", TxtSDCode.Text);
                Sm.CmParam<String>(ref cm, "@SDName", TxtSDName.Text);
                Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtSDCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SDCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SDCode", SDCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select SDCode, SDName, CityCode From TblSubDistrict Where SDCode=@SDCode",
                        new string[] 
                        {
                            "SDCode", "SDName", "CityCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtSDCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSDName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[2]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool ICityataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSDCode, "Sub district code", false) ||
                Sm.IsTxtEmpty(TxtSDName, "Sub district name", false) ||
                Sm.IsLueEmpty(LueCityCode, "Country") ||
                IsSDCodeExisted();
        }

        private bool IsSDCodeExisted()
        {
            if (!TxtSDCode.Properties.ReadOnly && Sm.IsDataExist("Select SDCode From TblSubDistrict Where SDCode='" + TxtSDCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Sub district code ( " + TxtSDCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSDCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSDCode);
        }

        private void TxtSDName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSDName);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        #endregion

        #endregion
    }
}
