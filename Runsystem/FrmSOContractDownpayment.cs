﻿#region Update
/*
    14/04/2020 [TKG/IMS] tambah so contract's amount, bank account, tambahan tax untuk inventory
    06/05/2020 [TKG/IMS] bug saat menampilkan boq inventory
    04/06/2020 [IBL/IMS] tambah field remark SKBDN, Origin Country, dan Quality of Goods
    04/06/2020 [IBL/IMS] jika doctype = Termin, Quantity di tab service dpt diubah. Menghitung ulang amount pd setiap perubahan qty.
                         jika doctype = Downpayment, seluruh detail di clear.
    10/06/2020 [DITA/IMS] Validasi ketika data sudah di incoming payment kan maka tidak bisa di cancel
    18/06/2020 [WED/IMS] journal baru
    09/07/2020 [VIN/IMS] printout baru
    10/08/2020 [DITA/IMS] hapus method auto bkin voucher request karna perubahan alur (termindp1->incomingpayment->vouchererequest)
    23/09/2020 [IBL/IMS] tambah field local document number
    25/09/2020 [DITA/IMS] tambah informasi No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber. Remark header bisa di edit berkali kali berdasarkan parameter IsRemarkHdrEditableOvertime
    25/09/2020 [VIN/IMS] penyesuaian PrintOut
    20/10/2020 [VIN/IMS] penyesuaian PrintOut
    27/10/2020 [DITA/IMS] nama list printout item code diganti -> no item
    02/11/2020 [DITA/IMS] Penambahan quality of goods di printout
    18/11/2020 [ICA/IMS] Saat Insert DocType auto terpilih downpayment dan readonly
    25/11/2020 [ICA/IMS] Mengubah description journal menjadi "ARDP for Project : DocNo"
    07/12/2020 [DITA/IMS] ubah nama menu + bug di set grid penamaan kolom belom berubah
    16/12/2020 [DITA/IMS] Specification ambil dari remark detail so contract
    16/12/2020 [DITA/IMS] Printout -> ttd berdasarkan approval tertinggi
    17/12/2020 [DITA/IMS] Tambah approval
    20/05/2021 [BRI/IMS] Penambahan kolom rate di header
    21/05/2021 [BRI/IMS] validasi curency berdasarkan SOC
    11/06/2021 [RDH/IMS] Mengotomatiskan tax di tab service/iventory sesuai dengan BOQ nya dari SOC namun masih bisa di edit (ARDP)
    11/06/2021 [RDH/IMS] Menambahkan "SO Contract amount after tax" diambil dari total amount after tax di soc (ditaruh di SOC Information) (ARDP for Project)
    13/06/2021 [TKG/IMS] tambah amount before tax
    17/06/2021 [TKG/IMS] merubah tampilan layar
    27/07/2021 [RDH/IMS] menambahkan Parameter untuk mentrigger keseluruhan transaksi AP AR yang membentuk beban (5)
    29/07/2021 [IBL/IMS] BUG: journal cancel belum menyimpan CCCode
 *  15/09/2021 [ICA/ALL] Validasi COA Journal 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mMainCurCode = string.Empty;
        private bool 
            mIsAutoJournalActived = false, 
            mIsRemarkHdrEditableOvertime = false,
            mIsCheckCOAJournalNotExists = false;
        internal bool
            mIsDetailShowColumnNumber = false;
        internal string
            mCCCodeForJournalAPAR = string.Empty,
            mCostCenterFormulaForAPAR = string.Empty;
        private decimal mMaxLimitAmtForSOContractDPSign = 0m, mMinLimitAmtForSOContractDPSign = 0m;
        private string
            mSOContractTerminDPPrintOutSignName1 = string.Empty,
            mSOContractTerminDPPrintOutSignName2 = string.Empty,
            mSOContractTerminDPPrintOutSignName3 = string.Empty;
        internal FrmSOContractDownpaymentFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContractDownpayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "ARDP For Project";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                SetLueDocType(ref LueDocType);
                Sl.SetLueCurCode(ref LueCurCode);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);

                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode2, LueTaxCode3 });

                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode, LueTaxCode4 });

                Tc1.SelectedTabPage = Tp3;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;

                Tc1.SelectedTabPage = Tp0;
                
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsRemarkHdrEditableOvertime = Sm.GetParameterBoo("IsRemarkHdrEditableOvertime");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
            mMaxLimitAmtForSOContractDPSign = Sm.GetParameterDec("MaxLimitAmtForSOContractDPSign");
            mMinLimitAmtForSOContractDPSign = Sm.GetParameterDec("MinLimitAmtForSOContractDPSign");
            mSOContractTerminDPPrintOutSignName1 = Sm.GetParameter("SOContractTerminDPPrintOutSignName1");
            mSOContractTerminDPPrintOutSignName2 = Sm.GetParameter("SOContractTerminDPPrintOutSignName2");
            mSOContractTerminDPPrintOutSignName3 = Sm.GetParameter("SOContractTerminDPPrintOutSignName3");
            mCostCenterFormulaForAPAR = Sm.GetParameter("CostCenterFormulaForAPAR");
            mCCCodeForJournalAPAR = Sm.GetParameter("CCCodeForJournalAPAR");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Specification",
                        "Quantity",

                        //6-10
                        "UoM",
                        "SO Contract's Price",
                        "DP's Price",
                        "Amount",
                        "Tax",

                        //11-14
                        "Tax",
                        "After Tax",
                        "SO Contract's"+Environment.NewLine+"No",
                        "SO Contract DNo"
                    },
                   new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        100, 200, 100, 200, 130, 

                        //6-10
                        80, 150, 150, 130, 130,
                        
                        //11-14
                        130, 130, 100, 0
                    }
              );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 8, 9, 10, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 14 }, false);
            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd1, new int[] { 13 });
            Grd1.Cols[13].Move(1);

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd2,
                  new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Specification",
                        "Quantity",

                        //6-10
                        "UoM",
                        "SO Contract's Price",
                        "DP's Price",
                        "Amount",
                        "Tax",

                        //11-14
                        "Tax",
                        "After Tax",
                        "SO Contract's"+Environment.NewLine+"No",
                        "SO Contract DNo"
                    },
                   new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        100, 200, 100, 200, 130, 

                        //6-10
                        80, 150, 150, 130, 130,
                        
                        //11-14
                        130, 130, 100, 0
                    }
              );
            Sm.GrdFormatDec(Grd2, new int[] { 5, 7, 8, 9, 10, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 10, 11, 12, 14 }, !ChkHideInfoInGrd.Checked);
            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd2, new int[] { 13 });
            Grd2.Cols[13].Move(1);

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 0, 0, 400, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueDocType, TxtPercentage, 
                        LueBankAcCode, MeeRemark, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, 
                        TxtTaxInvoiceNo4, DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, DteTaxInvoiceDt4, 
                        LueTaxCode, LueTaxCode2, LueTaxCode3, LueTaxCode4, MeeSKBDN,
                        MeeOriginCountry, MeeQualityOfGoods, TxtLocalDocNo, TxtRate
                    }, true);
                    BtnSOContractDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtPercentage, LueBankAcCode, MeeRemark, 
                        TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, TxtTaxInvoiceNo4, DteTaxInvoiceDt, 
                        DteTaxInvoiceDt2, DteTaxInvoiceDt3, DteTaxInvoiceDt4, LueTaxCode, LueTaxCode2, 
                        LueTaxCode3, LueTaxCode4, MeeSKBDN, MeeOriginCountry,
                        MeeQualityOfGoods, TxtLocalDocNo, TxtRate
                    }, false);
                    TxtRate.EditValue = Sm.FormatNum(1,0);
                    BtnSOContractDocNo.Enabled = true;
                    LueDocType.EditValue = "2"; 
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (mIsRemarkHdrEditableOvertime && ChkCancelInd.Checked == false) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueDocType, LueBankAcCode, 
                MeeRemark, TxtSOContractDocNo, TxtCtCode, TxtProjectCode, TxtProjectName, 
                TxtPONo, TxtPtCode, MeeSOContractRemark, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, 
                TxtTaxInvoiceNo3, TxtTaxInvoiceNo4, DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, 
                DteTaxInvoiceDt4, LueTaxCode, LueTaxCode2, LueTaxCode3, LueTaxCode4 ,
                MeeSKBDN, MeeOriginCountry, MeeQualityOfGoods, TxtJournalDocNo, TxtJournalDocNo2,
                TxtLocalDocNo
            });
            LueCurCode.EditValue = "IDR";
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtPercentage, TxtSOContractAmt, TxtAmt, TxtTaxAmt, TxtTaxAmt2, 
                TxtTaxAmt3, TxtRate
            }, 0);
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        private void ClearSOContractInfo()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSOContractDocNo, TxtCtCode, TxtProjectCode, TxtProjectName, 
                TxtPONo, TxtSOContractAmt, TxtPtCode, MeeSOContractRemark
            });
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractDownpaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

         private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContractDownpayment", "TblSOContractDownpaymentHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractDownpayment(DocNo));
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSOContractDownpaymentDtl3(DocNo, Row));
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);

            SetFormControl(mState.View);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDocType, "Document type") ||
                Sm.IsTxtEmpty(TxtPercentage, "Termin/Downpayment Percentage", true) ||
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO contract#", false) ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                IsCurCodeInvalid() ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsJournalSettingInvalid();
        }

        private bool IsCurCodeInvalid()
        {
            if (Sm.GetLue(LueCurCode) == mMainCurCode)
            {
                if (decimal.Parse(TxtRate.Text) != 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Rate must be 1,00");
                    return true;
                }
                return false;
            }
            
            if (Sm.GetLue(LueCurCode) != mMainCurCode)
            {
                if (decimal.Parse(TxtRate.Text) == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Rate can't be 1,00");
                    return true;
                }
                return false;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            string mCustomerAcNoAR = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoAR'"),
                mCustomerAcNoDownpayment = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoDownpayment'");
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //piutang usaha
            if (mCustomerAcNoAR.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
            }

            //uang muka
            if (mCustomerAcNoDownpayment.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoDownpayment is empty.");
                return true;
            }

            //pendapatan proyek
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            //PPN keluaran
            if (Sm.GetLue(LueTaxCode).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Inventory Tax1's COA Account# is empty.");
                    return true;
                }
            }
            if (Sm.GetLue(LueTaxCode2).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode2)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Service Tax1's COA Account# is empty.");
                    return true;
                }
            }
            if (Sm.GetLue(LueTaxCode3).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode3)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Service Tax2's COA Account# is empty.");
                    return true;
                }
            }
            if (Sm.GetLue(LueTaxCode4).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode4)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Inventory Tax2's COA Account# is empty.");
                    return true;
                }
            }
            
            //Disc, Cost, Etc
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdStr(Grd3, row, 1).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "COA Account# at row"+(row+1)+" in tab disc, cost, etc is empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo4 Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (ItCode.Length > 0)
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine("@ItCode_" + r.ToString());
                        Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    }
                }
            }
            else
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd2, r, 1);
                    if (ItCode.Length > 0)
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine("@ItCode_" + r.ToString());
                        Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    }
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSOContractDownpayment(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string r = string.Empty;

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");
            
            SQL.AppendLine("Insert Into TblSOContractDownpaymentHdr(DocNo, DocDt, Status, CancelReason, CancelInd, DocType, CurCode, Percentage, BankAcCode, SOContractAmt, Amt, SOContractDocNo, Remark, ");
            SQL.AppendLine("SKBDN, OriginCountry, QualityOfGoods, LocalDocNo, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, TaxInvoiceNo4, ");
            SQL.AppendLine("TaxInvoiceDt, TaxInvoiceDt2, TaxInvoiceDt3, TaxInvoiceDt4, ");
            SQL.AppendLine("TaxCode, TaxCode2, TaxCode3, TaxCode4, ");
            SQL.AppendLine("TaxAmt, TaxAmt2, TaxAmt3, TaxAmt4, ExcRate, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', Null, 'N', @DocType, @CurCode, @Percentage, @BankAcCode, @SOContractAmt, @Amt, @SOContractDocNo, @Remark, ");
            SQL.AppendLine("@SKBDN, @OriginCountry, @QualityOfGoods, @LocalDocNo, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceNo2, @TaxInvoiceNo3, @TaxInvoiceNo4, ");
            SQL.AppendLine("@TaxInvoiceDt, @TaxInvoiceDt2, @TaxInvoiceDt3, @TaxInvoiceDt4, ");
            SQL.AppendLine("@TaxCode, @TaxCode2, @TaxCode3, @TaxCode4, ");
            SQL.AppendLine("@TaxAmt, @TaxAmt2, @TaxAmt3, @TaxAmt4, @ExcRate, ");
            SQL.AppendLine("@UserCode, @Dt);");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='SOCD' ");

                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select (ifnull(B.Amt, 0)+ ifnull(B.AmtBOM, 0)) SOContractAmt ");
                SQL.AppendLine("    From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("    Inner Join TblSOContractHdr B On A.SOContractDocno = B.DocNo  ");
                SQL.AppendLine("    Where A.DocNo = @DocNo ");
                SQL.AppendLine("), 0)) ");
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblSOContractDownpaymentHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'SOCD' ");
            SQL.AppendLine(");     ");

            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 1).Length > 0)
                {
                    r = row.ToString();

                    SQL.AppendLine("Insert Into TblSOContractDownpaymentDtl(DocNo, DNo, ItCode, SOContractDNo ,Qty, SOContractUPrice, UPrice, Amt, TaxAmt, TaxAmt2, AfterTax, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @DNo_1_" + r + ", @ItCode_1_" + r + ", @SOContractDNo_1_" + r + ", @Qty_1_" + r + ", @SOContractUPrice_1_" + r + ", @UPrice_1_" + r + ", @Amt_1_" + r + ", @TaxAmt_1_" + r + ", @TaxAmt2_1_" + r + ", @AfterTax_1_" + r + ", @UserCode, @Dt);");

                    Sm.CmParam<String>(ref cm, "@DNo_1_" + r, Sm.Right(string.Concat("00000", (row + 1).ToString()), 6));
                    Sm.CmParam<String>(ref cm, "@ItCode_1_" + r, Sm.GetGrdStr(Grd1, row, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_1_" + r, Sm.GetGrdDec(Grd1, row, 5));
                    Sm.CmParam<Decimal>(ref cm, "@SOContractUPrice_1_" + r, Sm.GetGrdDec(Grd1, row, 7));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_1_" + r, Sm.GetGrdDec(Grd1, row, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_1_" + r, Sm.GetGrdDec(Grd1, row, 9));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_1_" + r, Sm.GetGrdDec(Grd1, row, 10));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_1_" + r, Sm.GetGrdDec(Grd1, row, 11));
                    Sm.CmParam<Decimal>(ref cm, "@AfterTax_1_" + r, Sm.GetGrdDec(Grd1, row, 12));
                    Sm.CmParam<String>(ref cm, "@SOContractDNo_1_" + r, Sm.GetGrdStr(Grd1, row, 14));
                }
            }

            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 1).Length > 0)
                {
                    r = row.ToString();

                    SQL.AppendLine("Insert Into TblSOContractDownpaymentDtl2(DocNo, DNo, ItCode, SOContractDNo ,Qty, SOContractUPrice, UPrice, Amt, TaxAmt, TaxAmt2, AfterTax, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @DNo_2_" + r + ", @ItCode_2_" + r + ", @SOContractDNo_2_" + r + ", @Qty_2_" + r + ", @SOContractUPrice_2_" + r + ", @UPrice_2_" + r + ", @Amt_2_" + r + ", @TaxAmt_2_" + r + ", @TaxAmt2_2_" + r + ", @AfterTax_2_" + r + ", @UserCode, @Dt);");

                    Sm.CmParam<String>(ref cm, "@DNo_2_" + r, Sm.Right(string.Concat("00000", (row + 1).ToString()), 6));
                    Sm.CmParam<String>(ref cm, "@ItCode_2_" + r, Sm.GetGrdStr(Grd2, row, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_2_" + r, Sm.GetGrdDec(Grd2, row, 5));
                    Sm.CmParam<Decimal>(ref cm, "@SOContractUPrice_2_" + r, Sm.GetGrdDec(Grd2, row, 7));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_2_" + r, Sm.GetGrdDec(Grd2, row, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_2_" + r, Sm.GetGrdDec(Grd2, row, 9));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_2_" + r, Sm.GetGrdDec(Grd2, row, 10));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_2_" + r, Sm.GetGrdDec(Grd2, row, 11));
                    Sm.CmParam<Decimal>(ref cm, "@AfterTax_2_" + r, Sm.GetGrdDec(Grd2, row, 12));
                    Sm.CmParam<String>(ref cm, "@SOContractDNo_2_" + r, Sm.GetGrdStr(Grd2, row, 14));
                }
            }

      
            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtName", TxtCtCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<Decimal>(ref cm, "@Percentage", decimal.Parse(TxtPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@SOContractAmt", decimal.Parse(TxtSOContractAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode4", Sm.GetLue(LueTaxCode4));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo4", TxtTaxInvoiceNo4.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt4", Sm.GetDte(DteTaxInvoiceDt4));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt4", decimal.Parse(TxtTaxAmt4.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtRate.Text));
            Sm.CmParam<String>(ref cm, "@SKBDN", MeeSKBDN.Text);
            Sm.CmParam<String>(ref cm, "@OriginCountry", MeeOriginCountry.Text);
            Sm.CmParam<String>(ref cm, "@QualityOfGoods", MeeQualityOfGoods.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDownpaymentDtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOContractDownpaymentDtl3(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            string Type = LueDocType.Text;
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSOContractDownpaymentHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo; ");


            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('ARDP for Project ', ' : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            // Piutang Usaha
            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                SQL.AppendLine("        D.Amt + ((A.TaxAmt + A.TaxAmt2 + A.TaxAmt3 + A.TaxAmt4) * A.ExcRate) DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("            And A.DocNo=@DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr C On A.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, Sum(T.Amt) Amt ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.DocNo, ((T1.Qty * T1.UPrice) * T2.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl T1 ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("                Where T1.DocNo = @DocNo ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select T1.DocNo, ((T1.Qty * T1.UPrice) * T2.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 T1 ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("                Where T1.DocNo = @DocNo ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo ");
                SQL.AppendLine("        ) D On A.DocNo = D.DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                //SQL.AppendLine("        IfNull(( ");
                //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                //SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        D.Amt + ((A.TaxAmt + A.TaxAmt2 + A.TaxAmt3 + A.TaxAmt4) * A.ExcRate) DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("            And A.DocNo=@DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr C On A.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, Sum(T.Amt) Amt ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.DocNo, ((T1.Qty * T1.UPrice) * T2.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl T1 ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("                Where T1.DocNo = @DocNo ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select T1.DocNo, ((T1.Qty * T1.UPrice) * T2.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 T1 ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("                Where T1.DocNo = @DocNo ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo ");
                SQL.AppendLine("        ) D On A.DocNo = D.DocNo ");
            }

            SQL.AppendLine("        Union All ");

            #region Downpayment

            // Uang Muka Customer            

            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        D.Amt As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownpayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("            And A.DocNo=@DocNo And A.DocType = '2' ");
                SQL.AppendLine("        Inner Join TblSOContractHdr C On A.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("                Select A.DocNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '2' ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DocNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '2' "); 
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo ");
                SQL.AppendLine("        ) D On A.DocNo = D.DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                //SQL.AppendLine("        IfNull(( ");
                //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                //SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        D.Amt As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownpayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("            And A.DocNo=@DocNo And A.DocType = '2' ");
                SQL.AppendLine("        Inner Join TblSOContractHdr C On A.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("                Select A.DocNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '2' ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DocNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '2' ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo ");
                SQL.AppendLine("        ) D On A.DocNo = D.DocNo ");
            }

            #endregion

            #region Termin

            // Pendapatan Proyek

            SQL.AppendLine("        Union All ");

            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        Select B.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Amt As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, T.AcNo4 AcNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("                Select A.DocNo, D.AcNo4, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '1' ");
                SQL.AppendLine("                Inner Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("                Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo4 Is Not Null ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DocNo, D.AcNo4 AcNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '1' ");
                SQL.AppendLine("                Inner Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("                Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo4 Is Not Null ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo, T.AcNo4 ");
                SQL.AppendLine("        ) B On A.DocNo = B.DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select B.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                //SQL.AppendLine("        IfNull(( ");
                //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                //SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.Amt As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.DocNo, T.AcNo4 AcNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("                Select A.DocNo, D.AcNo4, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '1' ");
                SQL.AppendLine("                Inner Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("                Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo4 Is Not Null ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DocNo, D.AcNo4 AcNo, ((A.Qty * A.UPrice) * B.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentDtl2 A ");
                SQL.AppendLine("                Inner Join TblSOContractDownpaymentHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                    And A.DocNo = @DocNo And B.DocType = '1' ");
                SQL.AppendLine("                Inner Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("                Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo4 Is Not Null ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.DocNo, T.AcNo4 ");
                SQL.AppendLine("        ) B On A.DocNo = B.DocNo ");
            }

            #endregion

            SQL.AppendLine("        Union All ");

            // Hutang PPN Keluaran
            
            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        Select T.AcNo, 0.00 As DAmt, Sum(T.Amt) As CAmt ");
                SQL.AppendLine("        From ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select B.AcNo2 AcNo, (A.TaxAmt * A.ExcRate) Amt ");
                SQL.AppendLine("            From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("            Inner Join TblTax B On A.TaxCode = B.TaxCode And A.TaxCode Is Not Null ");
                SQL.AppendLine("                And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                And A.DocNo = @DocNo ");
                SQL.AppendLine("            Union All ");
                SQL.AppendLine("            Select B.AcNo2 AcNo, (A.TaxAmt2 * A.ExcRate) Amt ");
                SQL.AppendLine("            From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("            Inner Join TblTax B On A.TaxCode2 = B.TaxCode And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("                And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                And A.DocNo = @DocNo ");
                SQL.AppendLine("            Union All ");
                SQL.AppendLine("            Select B.AcNo2 AcNo, (A.TaxAmt3 * A.ExcRate) Amt ");
                SQL.AppendLine("            From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("            Inner Join TblTax B On A.TaxCode3 = B.TaxCode And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("                And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                And A.DocNo = @DocNo ");
                SQL.AppendLine("            Union All ");
                SQL.AppendLine("            Select B.AcNo2 AcNo, (A.TaxAmt4 * A.ExcRate) Amt ");
                SQL.AppendLine("            From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("            Inner Join TblTax B On A.TaxCode4 = B.TaxCode And A.TaxCode4 Is Not Null ");
                SQL.AppendLine("                And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                And A.DocNo = @DocNo ");
                SQL.AppendLine("        ) T ");
                SQL.AppendLine("        Group By T.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select B.AcNo, 0.00 As DAmt, ");
                //SQL.AppendLine("        IfNull(( ");
                //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                //SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.Amt As CAmt ");
                SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select T.AcNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("                Select B.AcNo2 AcNo, (A.TaxAmt * A.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("                Inner Join TblTax B On A.TaxCode = B.TaxCode And A.TaxCode Is Not Null ");
                SQL.AppendLine("                    And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                    And A.DocNo = @DocNo ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select B.AcNo2 AcNo, (A.TaxAmt2 * A.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("                Inner Join TblTax B On A.TaxCode2 = B.TaxCode And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("                    And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                    And A.DocNo = @DocNo ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select B.AcNo2 AcNo, (A.TaxAmt3 * A.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("                Inner Join TblTax B On A.TaxCode3 = B.TaxCode And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("                    And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                    And A.DocNo = @DocNo ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select B.AcNo2 AcNo, (A.TaxAmt4 * A.ExcRate) Amt ");
                SQL.AppendLine("                From TblSOContractDownpaymentHdr A ");
                SQL.AppendLine("                Inner Join TblTax B On A.TaxCode4 = B.TaxCode And A.TaxCode4 Is Not Null ");
                SQL.AppendLine("                    And B.AcNo2 Is Not Null ");
                SQL.AppendLine("                    And A.DocNo = @DocNo ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("            Group By T.AcNo ");
                SQL.AppendLine("        ) B On 0 = 0 ");
            }

            SQL.AppendLine("        Union All ");

            //List of COA

            SQL.AppendLine("        Select B.AcNo, ");
            //if (!Sm.CompareStr(mMainCurCode, CurCode))
            //{
            //    SQL.AppendLine("        IfNull(( ");
            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //    SQL.AppendLine("        ), 0) * ");
            //}
            SQL.AppendLine("        (B.DAmt * A.ExcRate) DAmt, ");
            //if (!Sm.CompareStr(mMainCurCode, CurCode))
            //{
            //    SQL.AppendLine("        IfNull(( ");
            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //    SQL.AppendLine("        ), 0) * ");
            //}
            SQL.AppendLine("        (B.CAmt * A.ExcRate) CAmt ");
            SQL.AppendLine("        From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("        Inner Join TblSOContractDownpaymentDtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.DocNo=@DocNo ");


            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            if (mCostCenterFormulaForAPAR == "2")
            {

                SQL.AppendLine("Update TblJournalHdr A ");
                SQL.AppendLine("INNER JOIN TblSOContractDownpaymentHdr B ON A.DocNo = B.JournalDocno");
                SQL.AppendLine("INNER JOIN tblparameter C ON C.parcode = 'CCCodeForJournalAPAR' AND c.Parvalue is not null");
                SQL.AppendLine("SET A.CCCode = C.Parvalue ");
                SQL.AppendLine("Where B.Docno = @DocNo ");
                SQL.AppendLine("AND Exists(");
                SQL.AppendLine("    select 1 from tbljournaldtl ");
                SQL.AppendLine("    where docno = A.DocNo And LEFT(AcNo,1) = '5' ");
                SQL.AppendLine("); ");

            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(MeeRemark.Text, " | SO Contract# : ", TxtSOContractDocNo.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSOContractDownpaymentHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CCCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSOContractDownpaymentHdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSOContractDownpaymentHdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSOContractDownpaymentHdr());

            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            bool IsDataAlreadyCancelled = Sm.IsDataExist("Select 1 From TblSOContractDownpayment2Hdr Where CancelInd = 'Y' And DocNo = @Param; ", TxtDocNo.Text);
            bool ChangeToCancel = ChkCancelInd.Checked && !IsDataAlreadyCancelled;

            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment(ChangeToCancel);
        }

        private bool IsDataAlreadyProcessedToIncomingPayment(bool ChangeToCancel)
        {
            var SQL = new StringBuilder();

            if (!ChangeToCancel) return false;

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where B.InvoiceDocNo = @Param ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to Incoming Payment.");
                return true;
            }

            return false;
        }

       
        private bool IsDocNotCancelled()
        {
            if (mIsRemarkHdrEditableOvertime) return false;

            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSOContractDownpaymentHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }


        private MySqlCommand EditSOContractDownpaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractDownpaymentHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (mIsRemarkHdrEditableOvertime)
            {
                SQL.AppendLine("Update TblSOContractDownpaymentHdr Set Remark = @Remark Where DocNo = @DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSOContractDownpaymentHdr(DocNo);
                ShowSOContractDownpaymentDtl(DocNo);
                ShowSOContractDownpaymentDtl2(DocNo);
                ShowSOContractDownpaymentDtl3(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractDownpaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.DocType, B.CurCode, A.Percentage, A.Amt, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc,   ");
            SQL.AppendLine("A.SOContractDocNo, C.CtName, G.ProjectCode, G.ProjectName, B.PONo, D.PtName, B.Remark As SOContractRemark, A.SOContractAmt, A.BankAcCode, ");
            SQL.AppendLine("A.SKBDN, A.OriginCountry, A.QualityOfGoods, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceNo2, A.TaxInvoiceNo3, A.TaxInvoiceNo4, ");
            SQL.AppendLine("A.TaxInvoiceDt, A.TaxInvoiceDt2, A.TaxInvoiceDt3, A.TaxInvoiceDt4, ");
            SQL.AppendLine("A.TaxCode, A.TaxCode2, A.TaxCode3, A.TaxCode4, ");
            SQL.AppendLine("A.TaxAmt, A.TaxAmt2, A.TaxAmt3, A.TaxAmt4, A.JournalDocNo, A.JournalDocNo2, A.LocalDocNo, A.ExcRate ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join TblBOQHdr E On B.BOQDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr F On E.LOPDocNo=F.DocNO ");
            SQL.AppendLine("Left Join TblProjectGroup G On F.PGCode=G.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "DocType", "CurCode", 
                        //6-10
                        "Percentage", "Amt", "Remark", "SOContractDocNo", "CtName", 
                        //11-15
                        "ProjectCode", "ProjectName", "PONo", "PtName", "SOContractRemark", 
                        //16-20
                        "SOContractAmt", "BankAcCode", "TaxInvoiceNo", "TaxInvoiceNo2", "TaxInvoiceNo3", 
                        //21-25
                        "TaxInvoiceNo4", "TaxInvoiceDt", "TaxInvoiceDt2", "TaxInvoiceDt3", "TaxInvoiceDt4", 
                        //26-30
                        "TaxCode", "TaxCode2", "TaxCode3", "TaxCode4", "TaxAmt", 
                        //31-35
                        "TaxAmt2", "TaxAmt3", "TaxAmt4", "SKBDN", "OriginCountry",
                        //36-40
                        "QualityOfGoods", "JournalDocNo", "JournalDocNo2", "LocalDocNo", "StatusDesc",
                        //41
                        "ExcRate"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        Sm.SetLue(LueDocType, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[5]));
                        TxtPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[9]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[11]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[12]);
                        TxtPONo.EditValue = Sm.DrStr(dr, c[13]);
                        TxtPtCode.EditValue = Sm.DrStr(dr, c[14]);
                        MeeSOContractRemark.EditValue = Sm.DrStr(dr, c[15]);
                        TxtSOContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[17]));
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[18]);
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[19]);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[20]);
                        TxtTaxInvoiceNo4.EditValue = Sm.DrStr(dr, c[21]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[22]));
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[23]));
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[24]));
                        Sm.SetDte(DteTaxInvoiceDt4, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueTaxCode4, Sm.DrStr(dr, c[29]));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[30]), 0);
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[32]), 0);
                        TxtTaxAmt4.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[33]), 0);
                        MeeSKBDN.EditValue = Sm.DrStr(dr, c[34]);
                        MeeOriginCountry.EditValue = Sm.DrStr(dr, c[35]);
                        MeeQualityOfGoods.EditValue = Sm.DrStr(dr, c[36]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[37]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[38]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[39]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[40]);
                        TxtRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                    }, true
                );
            ComputeAmtBeforeTax();
        }

        private void ShowSOContractDownpaymentDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A1.DNo, A1.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
            SQL.AppendLine("A1.Qty, B.SalesUomCode, A1.SOContractUPrice, A1.UPrice, A1.Amt, A1.TaxAmt, A1.TaxAmt2, A1.AfterTax, C.No ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpaymentDtl A1 On A.DocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblItem B On A1.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblSOContractDtl4 C On A.SOContractDocNo = C.DocNo And A1.SOContractDNo = C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A1.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Specification", "Qty", 
                    
                    //6-10
                    "SalesUomCode", "SOContractUPrice", "UPrice", "Amt", "TaxAmt", 
                    
                    //11-13
                    "TaxAmt2", "AfterTax", "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSOContractDownpaymentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A1.DNo, A1.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
            SQL.AppendLine("A1.Qty, B.SalesUomCode, A1.SOContractUPrice, A1.UPrice, A1.Amt, A1.TaxAmt, A1.TaxAmt2, A1.AfterTax, C.No ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpaymentDtl2 A1 On A.DocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblItem B On A1.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblSOContractDtl5 C On A.SOContractDocNo = C.DocNo And A1.SOContractDNo = C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A1.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Specification", "Qty", 
                    
                    //6-10
                    "SalesUomCode", "SOContractUPrice", "UPrice", "Amt", "TaxAmt", 
                    
                    //11-13
                    "TaxAmt2", "AfterTax", "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowSOContractDownpaymentDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSOContractDownpaymentDtl3 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc=C.OptCode And OptCat='AccountDescriptionOnSalesInvoice' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10))
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ComputeAmtBeforeTax()
        {
            decimal SOContractAmt = 0m, Percentage = 0m;

            if (TxtSOContractAmt.Text.Length > 0) SOContractAmt = decimal.Parse(TxtSOContractAmt.Text);
            if (TxtPercentage.Text.Length > 0) Percentage = decimal.Parse(TxtPercentage.Text);

            TxtAmtBeforeTax.Text = Sm.FormatNum(SOContractAmt * Percentage * 0.01m, 0);
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'SOCD' ");

            return Sm.IsDataExist(SQL.ToString());
        }


        private void ParPrint()
        {
            var l = new List<SOCDHdr>();
            var ldtl = new List<SOCDDtl>();
            var ldtl2 = new List<SOCDDtl2>();
            var lsign = new List<SOCDSign>();
            string[] TableName = { "SOCDHdr", "SOCDDtl", "SOCDDtl2", "SOCDSign" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;
          
            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("@CompanyFooterImage As CompanyFooterImage, ");

            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhoneNumber', I.BankName, H.CurCode, H.BankAcNo,  ");

            SQL.AppendLine("B.CtName, B.Address CtAddress, DATE_FORMAT(A.DocDt,'%d %M %Y') As PODate ");
            SQL.AppendLine(",  ");
            SQL.AppendLine("ifnull(D.TaxName,' ') TaxName, ifnull(E.TaxName, ' ') TaxName2, ");
            SQL.AppendLine("ifnull(F.TaxName,' ') TaxName3, ifnull(G.TaxName, ' ') TaxName4, ");
            SQL.AppendLine("DATE_FORMAT(A.DocDt, '%d') Dt, ");
            SQL.AppendLine("Case monthname(A.DocDt)  ");
            SQL.AppendLine("When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
            SQL.AppendLine("When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
            SQL.AppendLine("When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember' ");
            SQL.AppendLine("END AS MONTH ");
            SQL.AppendLine(", DATE_FORMAT(A.DocDt, '%Y') Year, C.QualityOfGoods, ");

            #region Sign
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName1 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName1') AS 'SOContractTerminDPPrintOutSignName1' ,  ");
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName2 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName2') AS 'SOContractTerminDPPrintOutSignName2' ,   ");
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName3 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName3') AS 'SOContractTerminDPPrintOutSignName3',  ");
            SQL.AppendLine("(SELECT parvalue MaxLimitAmtForSOContractDPSign FROM tblparameter WHERE parcode ='MaxLimitAmtForSOContractDPSign') AS 'MaxLimitAmtForSOContractDPSign',  ");
            SQL.AppendLine("(SELECT parvalue MinLimitAmtForSOContractDPSign FROM tblparameter WHERE parcode ='MinLimitAmtForSOContractDPSign') AS 'MinLimitAmtForSOContractDPSign', ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName1') As PosName1, ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName2') As PosName2, ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName3') As PosName3 ");
            #endregion

            SQL.AppendLine("FROM tblsocontracthdr A ");
            SQL.AppendLine("inner join tblcustomer B ON A.CtCode=B.CtCode ");
            SQL.AppendLine("inner JOIN tblsocontractdownpaymenthdr C ON A.DocNo=C.SOContractDocNo ");
            SQL.AppendLine("left JOIN tbltax D ON C.TaxCode=D.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax E ON C.TaxCode2=E.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax F ON C.TaxCode3=F.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax G ON C.TaxCode4=G.TaxCode  ");
            SQL.AppendLine("inner JOIN tblbankaccount H ON C.BankAcCode=H.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblbank I ON H.BankCode=I.BankCode ");
            SQL.AppendLine("WHERE A.DocNo=@SOContractDocNo AND C.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@CompanyFooterImage", "FooterImage.png");
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);


                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity", 
                    "CtAddress",
                    "PODate",

                    //6-10
                    "TaxName",
                    "TaxName2",
                    "TaxName3",
                    "TaxName4",
                    "CompanyPhoneNumber",

                    //11-15
                    "BankName",
                    "CurCode",
                    "BankAcNo",
                    "CompanyFooterImage",
                    "Dt",

                    //16-20
                    "MONTH",
                    "YEAR",
                    "SOContractTerminDPPrintOutSignName1",
                    "SOContractTerminDPPrintOutSignName2",
                    "SOContractTerminDPPrintOutSignName3",

                    //21-25
                    "MaxLimitAmtForSOContractDPSign",
                    "MinLimitAmtForSOContractDPSign",
                    "PosName1",
                    "PosName2",
                    "PosName3",

                    //26
                    "QualityOfGoods"

                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SOCDHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CtAddress = Sm.DrStr(dr, c[4]),
                            PODate = Sm.DrStr(dr, c[5]),

                            CtName = TxtCtCode.Text,
                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            CustomerPO = TxtPONo.Text,
                            Quality = MeeQualityOfGoods.Text,

                            OriginCountry = MeeOriginCountry.Text,
                            SKBDN = MeeSKBDN.Text,
                            Remark = MeeRemark.Text,
                            Amount = decimal.Parse(TxtAmt.Text),
                            Terbilang = Sm.Terbilang(decimal.Parse(TxtAmt.Text)),

                            TaxName = LueTaxCode2.Text,
                            TaxName2 = LueTaxCode3.Text,
                            TaxName3 = LueTaxCode.Text,
                            TaxName4 = LueTaxCode4.Text,

                            TaxAmount = decimal.Parse(TxtTaxAmt2.Text),
                            CompanyPhoneNumber = Sm.DrStr(dr, c[10]),
                            BankName = Sm.DrStr(dr, c[11]),
                            CurCode = Sm.DrStr(dr, c[12]),
                            BankAcNo = Sm.DrStr(dr, c[13]),


                            TaxAmount2 = decimal.Parse(TxtTaxAmt3.Text),
                            TaxAmount3 = decimal.Parse(TxtTaxAmt.Text),
                            TaxAmount4 = decimal.Parse(TxtTaxAmt4.Text),
                            CompanyFooterImage = Sm.DrStr(dr, c[14]),
                            Date = Sm.DrStr(dr, c[15]),

                            Month = Sm.DrStr(dr, c[16]),
                            Year = Sm.DrStr(dr, c[17]),
                            SOContractTerminDPPrintOutSignName1 = Sm.DrStr(dr, c[18]),
                            SOContractTerminDPPrintOutSignName2 = Sm.DrStr(dr, c[19]),
                            SOContractTerminDPPrintOutSignName3 = Sm.DrStr(dr, c[20]),

                            mMaxLimitAmtForSOContractDPSign = Sm.DrDec(dr, c[21]),
                            mMinLimitAmtForSOContractDPSign = Sm.DrDec(dr, c[22]),
                            PosName1 = Sm.DrStr(dr, c[23]),
                            PosName2 = Sm.DrStr(dr, c[24]),
                            PosName3 = Sm.DrStr(dr, c[25]),

                            QualityofGoods = Sm.DrStr(dr, c[26])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {

                    ldtl.Add(new SOCDDtl()
                    {
                        No = Nomor,

                        SOContractNo = Sm.GetGrdStr(Grd1, i, 13),
                        LocalCode = Sm.GetGrdStr(Grd1, i, 3),
                        ItName = Sm.GetGrdStr(Grd1, i, 2),
                        Specification = Sm.GetGrdStr(Grd1, i, 4),
                        Qty = Sm.GetGrdDec(Grd1, i, 5),
                        Uom = Sm.GetGrdStr(Grd1, i, 6),

                        Price = Sm.GetGrdDec(Grd1, i, 8),
                        TotalPrice = Sm.GetGrdDec(Grd1, i, 8),
                        TotalAmount = Sm.GetGrdDec(Grd1, i, 9)

                    });
                    Nomor += 1;

                }
            }
            else
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {

                    ldtl.Add(new SOCDDtl()
                    {
                        No = Nomor,

                        SOContractNo = Sm.GetGrdStr(Grd2, i, 13),
                        LocalCode = Sm.GetGrdStr(Grd2, i, 3),
                        ItName = Sm.GetGrdStr(Grd2, i, 2),
                        Specification = Sm.GetGrdStr(Grd2, i, 4),
                        Qty = Sm.GetGrdDec(Grd2, i, 5),
                        Uom = Sm.GetGrdStr(Grd2, i, 6),

                        Price = Sm.GetGrdDec(Grd2, i, 8),
                        TotalPrice = Sm.GetGrdDec(Grd2, i, 8),
                        TotalAmount = Sm.GetGrdDec(Grd2, i, 9)

                    });
                    Nomor += 1;

                }
            }


            myLists.Add(ldtl);

            #region Spec Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT C.Remark Specification ");
                SQLDtl.AppendLine("FROM tblsocontracthdr A ");
                SQLDtl.AppendLine("inner JOIN tblsocontractdownpaymenthdr B ON A.DocNo=B.SOContractDocNo ");
                SQLDtl.AppendLine("left JOIN TblSOContractdtl C ON A.DocNo=C.DocNo ");
                SQLDtl.AppendLine("Where B.DocNo=@DocNo Order By C.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "Specification" 
                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl2.Add(new SOCDDtl2()
                        {
                            Specification = Sm.DrStr(drDtl, cDtl[0])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl2);
            #endregion 

            #endregion

            #region Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select Concat(Upper(left(T4.UserName,1)),Substring(Lower(T4.UserName), 2, Length(T4.UserName))) As UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(T7.ParValue, ''), T2.UserCode, '.JPG') As Signature, T6.PosName ");
                SQLDtl2.AppendLine("From ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select A.DocNo, Max(C.Level) MaxLvl ");
                SQLDtl2.AppendLine("    From TblSOContractDownpaymentHdr A ");
                SQLDtl2.AppendLine("    Inner Join TblDocapproval B On A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("    Inner JOin TblDocApprovalSetting C On B.DocType = C.DocType And B.ApprovalDNo = C.DNo ");
                SQLDtl2.AppendLine("    Group By A.DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Inner Join TblDocapproval T2 On T1.DocNo = T2.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblDocApprovalSetting T3 On T2.DocType = T3.DocType  ");
                SQLDtl2.AppendLine("    And T2.ApprovalDNo = T3.DNo And T3.`Level` = T1.MaxLvl ");
                SQLDtl2.AppendLine("Inner Join TblUser T4 On T2.UserCode = T4.UserCode ");
                SQLDtl2.AppendLine("Left Join TblEmployee T5 On T4.UserCode=T5.UserCode  ");
                SQLDtl2.AppendLine("Left Join TblPosition T6 On T5.PosCode=T6.PosCode  ");
                SQLDtl2.AppendLine("Left Join TblParameter T7 On T7.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Where T1.DocNo = @DocNo ");
                SQLDtl2.AppendLine("Group  By T7.ParValue, T2.UserCode, T4.UserName, T6.PosName; ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "PosName"
                             
            
                            });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        lsign.Add(new SOCDSign()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            POSName = Sm.DrStr(drDtl2, cDtl2[2])

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(lsign);


            #endregion


            Sm.PrintReport("SOContractDp_1", myLists, TableName, false);
            Sm.PrintReport("SOContractDp_2", myLists, TableName, false);
            Sm.PrintReport("SOContractDp_3", myLists, TableName, false);

        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sm.SetLue2(ref Lue, "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice';", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sm.SetLue2(ref Lue, "Select '1' As Col1, 'Termin' As Col2 Union All Select '2' As Col1, 'Downpayment' As Col2;", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowSOContractInfo(string DocNo)
        {
            ClearGrd1();
            ClearGrd2();
            ShowSOContractServiceInfo(DocNo);
            ShowSOContractInventoryInfo(DocNo);
            ComputeAmt();
            ComputeAmtBeforeTax();
        }

        private void ShowSOContractServiceInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
            SQL.AppendLine("A.Qty, B.SalesUomCode, A.UPrice, A.Amt, A.No ");
            SQL.AppendLine("From TblSOContractDtl4 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",  
                    //1-5
                    "ItName", "ItCodeInternal", "Specification", "Qty", "SalesUomCode", 
                    //6-9
                    "UPrice", "Amt", "No", "DNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = 0m;
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSOContractInventoryInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, ");
            SQL.AppendLine("A.Qty, B.SalesUomCode, A.UPrice, A.Amt, A.No ");
            SQL.AppendLine("From TblSOContractDtl5 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",  
                    //1-5
                    "ItName", "ItCodeInternal", "Specification", "Qty", "SalesUomCode", 
                    //6-9
                    "UPrice", "Amt", "No", "DNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = 0m;
                    Grd.Cells[Row, 11].Value = 0m;
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ComputeAmt()
        {
            decimal
                Percentage = 0m,
                Amt = 0m,
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m,
                TaxAmt4 = 0m,
                TaxRate = 0m,
                TaxRate2 = 0m,
                TaxRate3 = 0m,
                TaxRate4 = 0m
                ;
            string 
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3),
                TaxCode4 = Sm.GetLue(LueTaxCode4);

            if (TxtPercentage.Text.Length > 0) Percentage = decimal.Parse(TxtPercentage.Text);
            if (TaxCode.Length != 0) TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxCode2.Length != 0) TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
            if (TaxCode3.Length != 0) TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
            if (TaxCode4.Length != 0) TaxRate4 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode4);

            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        Grd1.Cells[r, 8].Value = Percentage * 0.01m * Sm.GetGrdDec(Grd1, r, 7);
                        Grd1.Cells[r, 9].Value = Sm.GetGrdDec(Grd1, r, 5)*Sm.GetGrdDec(Grd1, r, 8);
                        Grd1.Cells[r, 10].Value = TaxRate2*0.01m * Sm.GetGrdDec(Grd1, r, 9);
                        Grd1.Cells[r, 11].Value = TaxRate3*0.01m * Sm.GetGrdDec(Grd1, r, 9);
                        Grd1.Cells[r, 12].Value = Sm.GetGrdDec(Grd1, r, 9) + Sm.GetGrdDec(Grd1, r, 10) + Sm.GetGrdDec(Grd1, r, 11);
                        Amt += Sm.GetGrdDec(Grd1, r, 9);
                        TaxAmt2 += Sm.GetGrdDec(Grd1, r, 10);
                        TaxAmt3 += Sm.GetGrdDec(Grd1, r, 11);
                    }
                }       
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                    {
                        Grd2.Cells[r, 8].Value = Percentage * 0.01m * Sm.GetGrdDec(Grd2, r, 7);
                        Grd2.Cells[r, 9].Value = Sm.GetGrdDec(Grd2, r, 5) * Sm.GetGrdDec(Grd2, r, 8);
                        Grd2.Cells[r, 10].Value = TaxRate * 0.01m * Sm.GetGrdDec(Grd2, r, 9);
                        Grd2.Cells[r, 11].Value = TaxRate4 * 0.01m * Sm.GetGrdDec(Grd2, r, 9);
                        Grd2.Cells[r, 12].Value = Sm.GetGrdDec(Grd2, r, 9) + Sm.GetGrdDec(Grd2, r, 10) + Sm.GetGrdDec(Grd2, r, 11);
                        Amt += Sm.GetGrdDec(Grd2, r, 9);
                        TaxAmt += Sm.GetGrdDec(Grd2, r, 10);
                        TaxAmt4 += Sm.GetGrdDec(Grd2, r, 11);
                    }
                }
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd3, Row, 1));
                if (Sm.GetGrdBool(Grd3, Row, 3))
                {
                    if (AcType == "D")
                        Amt += Sm.GetGrdDec(Grd3, Row, 4);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Sm.GetGrdBool(Grd3, Row, 5))
                {
                    if (AcType == "C")
                        Amt += Sm.GetGrdDec(Grd3, Row, 6);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 6);
                }
            }
            Amt += (TaxAmt + TaxAmt2 + TaxAmt3 + TaxAmt4);

            TxtTaxAmt.Text = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
            TxtTaxAmt4.Text = Sm.FormatNum(TaxAmt4, 0);
            TxtAmt.Text = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Grd1

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                ComputeAmt();
            }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4) > 0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0m;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6) > 0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0m;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                ComputeAmt();
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSOContractDownpaymentDlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSOContractDownpaymentDlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmSOContractDownpaymentDlg(this));
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO contract#", false))
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo4_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo4);
        }

        private void LueTaxCode4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode4, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtRate_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.FormatNumTxt(TxtRate, 0);
        }

        private void TxtPercentage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtPercentage, 0);
                ComputeAmt();
                ComputeAmtBeforeTax();
            }
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && LueDocType.Text == "Termin")
            {
                Sm.GrdColReadOnly(false, false, Grd1, new int[] { 5 });
            }
            else
            {
                Sm.GrdColReadOnly(true, false, Grd1, new int[] { 5 });
                ClearSOContractInfo();
                ClearGrd1();
                ClearGrd2();
                ClearGrd3();
            }
        }

        #endregion

        #region Button Event

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region report class

        public class SOCDHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhoneNumber { get; set; }

            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CustomerPO { get; set; }
            public string PODate { get; set; }
            public string Quality { get; set; }
            public string OriginCountry { get; set; }
            public string Remark { get; set; }
            public string CtAddressCity { get; set; }
            public string CtAddress { get; set; }
            public string CtName { get; set; }
            public string Terbilang { get; set; }
            public string SKBDN { get; set; }
            public decimal Amount { get; set; }
            public decimal TaxAmount { get; set; }
            public decimal TaxAmount2 { get; set; }
            public decimal TaxAmount3 { get; set; }
            public decimal TaxAmount4 { get; set; }

            public string TaxName { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string TaxName4 { get; set; }
            public string BankName { get; set; }
            public string CurCode { get; set; }
            public string BankAcNo { get; set; }
            public string CompanyFooterImage { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public decimal mMaxLimitAmtForSOContractDPSign { get; set; }
            public decimal mMinLimitAmtForSOContractDPSign { get; set; }
            public string SOContractTerminDPPrintOutSignName1 { get; set; }
            public string SOContractTerminDPPrintOutSignName2 { get; set; }
            public string SOContractTerminDPPrintOutSignName3 { get; set; }
            public string PosName1 { get; set; }
            public string PosName2 { get; set; }
            public string PosName3 { get; set; }
            public string QualityofGoods { get; set; }

        }
        public class SOCDDtl
        {
            public int No { get; set; }
            public string LocalCode { get; set; }
            public string SOContractNo { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public decimal Price { get; set; }
            public decimal TotalPrice { get; set; }
            public decimal TotalAmount { get; set; }

        }
        public class SOCDDtl2
        {
            public string Specification { get; set; }

        }

        private class SOCDSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string POSName { get; set; }

        }
        #endregion
    }
}
