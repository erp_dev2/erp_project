﻿#region Update
    // 11/06/2021 [ICA/PHT] new Apps.
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpWorkPeriod : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpWorkPeriod(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.EmpCode, C.EmpName, D.SiteName, E.DeptName, C.EmpCodeOld, B.startdt, F.GrdLvlName OldGrade, G.GrdLvlName NewGrade, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When TIMESTAMPDIFF(YEAR, B.StartDt, NOW()) < 1 Then CONCAT(TIMESTAMPDIFF(MONTH,B.StartDt, NOW()), ' Bulan') ");
            SQL.AppendLine("    else ");
            SQL.AppendLine("        CONCAT(TIMESTAMPDIFF(YEAR, B.StartDt, NOW()), ' Tahun ', TIMESTAMPDIFF(MONTH,B.StartDt, NOW()), ' Bulan') ");
            SQL.AppendLine("END WorkPeriod ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("    SELECT EmpCode, MAX(CreateDt) CreateDt ");
            SQL.AppendLine("    FROM TblPPS ");
            SQL.AppendLine("    WHERE CancelInd = 'N' AND STATUS = 'A' ");
            SQL.AppendLine("        AND GrdLvlCodeOld <> GrdLvlCodeNew ");
            SQL.AppendLine("    GROUP BY EmpCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblPPS B ON A.EmpCode = B.EmpCode AND A.CreateDt = B.CreateDt ");
            SQL.AppendLine("INNER JOIN TblEmployee C ON B.EmpCode = C.EmpCode ");
            SQL.AppendLine("INNER JOIN TblSite D ON C.SiteCode = D.SiteCode ");
            SQL.AppendLine("INNER JOIN TblDepartment E ON C.DeptCode = E.DeptCode ");
            SQL.AppendLine("LEFT JOIN TblGradeLevelhdr F ON B.GrdLvlCodeOld = F.GrdLvlCode ");
            SQL.AppendLine("LEFT JOIN TblGradeLevelhdr G ON B.GrdLvlCodeNew = G.GrdLvlCode ");
            SQL.AppendLine("Where 0=0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name",
                        "Site", 
                        "Department",
                        "Old Code",
                        
                        //6-10
                        "PPS"+Environment.NewLine+"Date",
                        "Old Grade",
                        "New Grade",
                        "Work Period"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 200, 150, 
                        
                        //6-9
                        100, 150, 150, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
           try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0";

                var cm = new MySqlCommand();


                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "C.EmpName", "C.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGrdLvlCode), "B.GrdLvlCodeNew", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By C.EmpName",
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName", "SiteName", "DeptName", "EmpCodeOld", "StartDt",
                            //6-7
                            "OldGrade", "NewGrade", "WorkPeriod"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkGrdLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Grade");
        }
        #endregion

    }
}
