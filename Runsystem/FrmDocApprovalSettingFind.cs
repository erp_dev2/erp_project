﻿#region Update
/* 
    11/02/2019 [TKG] tambah document approval group
    11/02/2021 [TKG/KIM] tambah end amount
    16/07/2021 [TKG/PHT] judul level diubah menjadi Setting level
    13/10/2021 [AJH/PHT] Menambahkan kolom employee's level dan leave code berdasarkan parameter
    01/03/2023 [MAU/HEX] menampilkan kolom Item's Category berdasarkan parameter IsDocApprovalSettingUseItemCt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSettingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDocApprovalSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalSettingFind(FrmDocApprovalSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.DNo, A.UserCode, A.Level, ");
            SQL.AppendLine("B.DeptName, C.SiteName, D.WhsName, A.StartAmt, A.EndAmt, A.PaymentType, E.DAGName, ");
            SQL.AppendLine("A.LevelCode, A.LeaveCode, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.ItCtCode ");
            SQL.AppendLine("From TblDocApprovalSetting A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Left Join TblDocApprovalGroupHdr E On A.DAGCode=E.DAGCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type", 
                        "Number",
                        "User", 
                        "Setting Level",
                        "Departement",
                        
                        //6-10
                        "Site",
                        "Warehouse",
                        "Started Amount",
                        "End Amount",
                        "Payment Type",

                        //11-15
                        "Approval Group",
                        "Employee's Level",
                        "Leave Code",
                        "Created By",   
                        "Created Date", 
                        
                        
                        //16-20
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time",
                        "Item's Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 250, 150, 200, 
                        
                        //6-10
                        200, 200, 130, 130, 200, 
                        
                        //11-15
                        200, 130, 130, 130, 130,

                        //16-20
                        130, 130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 2);
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19}, false);
            Sm.GrdColInvisible(Grd1, new int[] { 12 }, mFrmParent.mIsDocApprovalSettingUseEmpLevelCode);
            Sm.GrdColInvisible(Grd1, new int[] { 13 }, mFrmParent.mIsDocApprovalSettingUseLeaveCode);
            Sm.SetGrdProperty(Grd1 , false);
            if(mFrmParent.mIsDocApprovalSettingUseItemCt)
                Grd1.Cols[20].Move(14);
            Sm.GrdColInvisible(Grd1, new int[] { 20 }, mFrmParent.mIsDocApprovalSettingUseItemCt);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocType.Text, "A.DocType", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocType, B.DeptName, C.SiteName, D.WhsName, A.Level;",
                        new string[]
                        {
                            //0
                            "DocType", 

                            //1-5
                            "DNo", "UserCode", "Level", "DeptName", "SiteName", 
                            
                            //6-10
                            "WhsName", "StartAmt", "EndAmt", "PaymentType", "DAGName", 

                            //11-15
                            "LevelCode", "LeaveCode", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //16-17
                            "LastUpDt", "ItCtCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0); 
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        }, true, true, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocType_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Type");
        }

        #endregion

        #endregion
    }
}
