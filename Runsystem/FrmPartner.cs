﻿#region Update
/*
    05/07/2018 [TKG] New Application
    24/07/2018 [WED] Asset nggak mandatory, ID Number & Address mandatory. Tambah company name, company address, company province, company city, company postal code
    25/07/2018 [WED] Partner bisa dipanggil di form lain
    14/11/2018 [TKG] Tambah otomatis buat nomor rekening coa piutang mitra
    21/04/2020 [TKG] menghilangkan proses buat coa otomatis
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPartner : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mPnCode = string.Empty;
        internal FrmPartnerFind FrmFind;
        private bool mIsInsert = false;
        private string mPartnerAcNoAR = string.Empty, mPartnerAcDescAR = string.Empty;

        #endregion

        #region Constructor

        public FrmPartner(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueProvCode(ref LueProvCode);
                Sl.SetLueCityCode(ref LueCompanyCityCode);
                Sl.SetLueProvCode(ref LueCompanyProvCode);
                Sl.SetLueOption(ref LueGender, "Gender");
                Sl.SetLueBCCode(ref LueBCCode);
                Sl.SetLueBSCode(ref LueBSCode);

                if (mPnCode.Length != 0)
                {
                    ShowData(mPnCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPnCode, TxtPnName, ChkActInd, TxtShortName, MeeAddress, 
                        LueCityCode, LueProvCode, TxtPostalCd, TxtIdentityNo, LueGender, 
                        DteBirthDt, TxtTIN, TxtPhone, TxtFax, TxtEmail, 
                        TxtMobile, TxtManPower, TxtAsset, TxtTurnover, LueBCCode, 
                        LueBSCode, MeeRemark, TxtCompanyName, MeeCompanyAddress, LueCompanyCityCode, 
                        LueCompanyProvCode, TxtCompanyPostalCd
                    }, true);
                    TxtPnCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPnName, TxtShortName, MeeAddress, LueCityCode, 
                        LueProvCode, TxtPostalCd, TxtIdentityNo, LueGender, DteBirthDt, 
                        TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        TxtManPower, TxtAsset, TxtTurnover, LueBCCode, LueBSCode, 
                        MeeRemark, TxtCompanyName, MeeCompanyAddress, LueCompanyCityCode, 
                        LueCompanyProvCode, TxtCompanyPostalCd
                    }, false);
                    TxtPnCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPnName, ChkActInd, TxtShortName, MeeAddress, LueCityCode, 
                        LueProvCode, TxtPostalCd, TxtIdentityNo, LueGender, DteBirthDt, 
                        TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        TxtManPower, TxtAsset, TxtTurnover, LueBCCode, LueBSCode, 
                        MeeRemark, TxtCompanyName, MeeCompanyAddress, LueCompanyCityCode, 
                        LueCompanyProvCode, TxtCompanyPostalCd
                    }, false);
                    TxtPnName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtPnCode, TxtPnName, TxtShortName, MeeAddress, LueCityCode, 
                LueProvCode, TxtPostalCd, TxtIdentityNo, LueGender, DteBirthDt, 
                TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                LueBCCode, LueBSCode, MeeRemark, TxtCompanyName, MeeCompanyAddress, LueCompanyCityCode, 
                LueCompanyProvCode, TxtCompanyPostalCd
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAsset, TxtTurnover }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtManPower }, 11);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPartnerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            mIsInsert = true;
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPnCode, "", false)) return;
            SetFormControl(mState.Edit);
            mIsInsert = false;
        }

        private string GeneratePnCode()
        {
            string Zero = new String('0', 8); 
            return Sm.GetValue(
                "Select Right(Concat('"+Zero+"', PnCodeMax) , "+8+") As PnCodeTemp " +
                "From (Select IfNull(Max(Cast(Trim(PnCode) As Decimal)), "+Zero+")+1 PnCodeMax From TblPartner) T;"
                );
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                if (mIsInsert) TxtPnCode.EditValue = GeneratePnCode();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Insert Into TblPartner ");
                SQL.AppendLine("(PnCode, PnName, ActInd, ShortName, Address, ");
                SQL.AppendLine("CityCode, ProvCode, PostalCd, ");
                SQL.AppendLine("CompanyName, CompanyAddress, CompanyCityCode, CompanyProvCode, CompanyPostalCd, ");
                SQL.AppendLine("IdentityNo, Gender, BirthDt, TIN, Phone, Fax, Email, ");
                SQL.AppendLine("Mobile, ManPower, Asset, Turnover, BCCode, ");
                SQL.AppendLine("BSCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PnCode, @PnName, @ActInd, @ShortName, @Address, ");
                SQL.AppendLine("@CityCode, @ProvCode, @PostalCd, ");
                SQL.AppendLine("@CompanyName, @CompanyAddress, @CompanyCityCode, @CompanyProvCode, @CompanyPostalCd, ");
                SQL.AppendLine("@IdentityNo, @Gender, @BirthDt, @TIN, @Phone, @Fax, @Email, ");
                SQL.AppendLine("@Mobile, @ManPower, @Asset, @Turnover, @BCCode, ");
                SQL.AppendLine("@BSCode, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update ");
                SQL.AppendLine("    PnName=@PnName, ActInd=@ActInd, ShortName=@ShortName, Address=@Address, ");
                SQL.AppendLine("    CityCode=@CityCode, ProvCode=@ProvCode, PostalCd=@PostalCd, IdentityNo=@IdentityNo, Gender=@Gender, ");
                SQL.AppendLine("    CompanyName=@CompanyName, CompanyAddress=@CompanyAddress, CompanyCityCode=@CompanyCityCode, CompanyProvCode=@CompanyProvCode, CompanyPostalCd=@CompanyPostalCd, ");
                SQL.AppendLine("    BirthDt=@BirthDt, TIN=@TIN, Phone=@Phone, Fax=@Fax, Email=@Email, ");
                SQL.AppendLine("    Mobile=@Mobile, ManPower=@ManPower, Asset=@Asset, Turnover=@Turnover, BCCode=@BCCode, ");
                SQL.AppendLine("    BSCode=@BSCode, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                //if (mIsInsert && mPartnerAcNoAR.Length > 0)
                //{
                //    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                //    SQL.AppendLine("Select Concat(@PartnerAcNoAR, @PnCode) As AcNo, ");
                //    SQL.AppendLine("Trim(Concat(@PartnerAcDescAR, ' ', @PnName)) As AcDesc, ");
                //    SQL.AppendLine("If(Length(@PartnerAcNoAR)<=0, '', Substring(@PartnerAcNoAR, 1, (Length(@PartnerAcNoAR)-1))) As Parent, ");
                //    SQL.AppendLine("If(Length(@PartnerAcNoAR)<=0, '', Length(@PartnerAcNoAR)-Length(Replace(@PartnerAcNoAR, '.', ''))+1) As Level, ");
                //    SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");

                //    Sm.CmParam<String>(ref cm, "@PartnerAcNoAR", mPartnerAcNoAR);
                //    Sm.CmParam<String>(ref cm, "@PartnerAcDescAR", mPartnerAcDescAR);
                //}

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PnCode", TxtPnCode.Text);
                Sm.CmParam<String>(ref cm, "@PnName", TxtPnName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@ShortName", TxtShortName.Text);
                Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
                Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
                Sm.CmParam<String>(ref cm, "@ProvCode", Sm.GetLue(LueProvCode));
                Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
                Sm.CmParam<String>(ref cm, "@CompanyName", TxtCompanyName.Text);
                Sm.CmParam<String>(ref cm, "@CompanyAddress", MeeCompanyAddress.Text);
                Sm.CmParam<String>(ref cm, "@CompanyCityCode", Sm.GetLue(LueCompanyCityCode));
                Sm.CmParam<String>(ref cm, "@CompanyProvCode", Sm.GetLue(LueCompanyProvCode));
                Sm.CmParam<String>(ref cm, "@CompanyPostalCd", TxtCompanyPostalCd.Text);
                Sm.CmParam<String>(ref cm, "@IdentityNo", TxtIdentityNo.Text);
                Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
                Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteBirthDt));
                Sm.CmParam<String>(ref cm, "@TIN", TxtTIN.Text);
                Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
                Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
                Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
                Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@BSCode", Sm.GetLue(LueBSCode));
                Sm.CmParam<Decimal>(ref cm, "@Manpower", decimal.Parse(TxtManPower.Text));
                Sm.CmParam<Decimal>(ref cm, "@Asset", decimal.Parse(TxtAsset.Text));
                Sm.CmParam<Decimal>(ref cm, "@Turnover", decimal.Parse(TxtTurnover.Text));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPnCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PnCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select PnName, ActInd, ShortName, Address, ");
                SQL.AppendLine("CityCode, ProvCode, PostalCd, IdentityNo, Gender, ");
                SQL.AppendLine("BirthDt, TIN, Phone, Fax, Email, ");
                SQL.AppendLine("Mobile, ManPower, Asset, Turnover, BCCode, ");
                SQL.AppendLine("BSCode, Remark, CompanyName, CompanyAddress, CompanyCityCode, ");
                SQL.AppendLine("CompanyProvCode, CompanyPostalCd ");
                SQL.AppendLine("From TblPartner Where PnCode=@PnCode;");

                Sm.CmParam<String>(ref cm, "@PnCode", PnCode);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        { 
                            "PnName", 
                            "ActInd", "ShortName", "Address", "CityCode", "ProvCode", 
                            "PostalCd", "IdentityNo", "Gender", "BirthDt", "TIN", 
                            "Phone", "Fax", "Email", "Mobile", "ManPower", 
                            "Asset", "Turnover", "BCCode", "BSCode", "Remark",
                            "CompanyName", "CompanyAddress", "CompanyCityCode", "CompanyProvCode", "CompanyPostalCd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPnCode.EditValue = PnCode;
                            TxtPnName.EditValue = Sm.DrStr(dr, c[0]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[1])=="Y";
                            TxtShortName.EditValue = Sm.DrStr(dr, c[2]);
                            MeeAddress.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueProvCode, Sm.DrStr(dr, c[5]));
                            TxtPostalCd.EditValue = Sm.DrStr(dr, c[6]);
                            TxtIdentityNo.EditValue = Sm.DrStr(dr, c[7]);
                            Sm.SetLue(LueGender, Sm.DrStr(dr, c[8]));
                            Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[9]));
                            TxtTIN.EditValue = Sm.DrStr(dr, c[10]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[11]);
                            TxtFax.EditValue = Sm.DrStr(dr, c[12]);
                            TxtEmail.EditValue = Sm.DrStr(dr, c[13]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[14]);
                            TxtManPower.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 11);
                            TxtAsset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                            TxtTurnover.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                            Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[18]));
                            Sm.SetLue(LueBSCode, Sm.DrStr(dr, c[19]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                            TxtCompanyName.EditValue = Sm.DrStr(dr, c[21]);
                            MeeCompanyAddress.EditValue = Sm.DrStr(dr, c[22]);
                            Sm.SetLue(LueCompanyCityCode, Sm.DrStr(dr, c[23]));
                            Sm.SetLue(LueCompanyProvCode, Sm.DrStr(dr, c[24]));
                            TxtCompanyPostalCd.EditValue = Sm.DrStr(dr, c[25]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                (!mIsInsert && Sm.IsTxtEmpty(TxtPnCode, "Partner code", false)) ||
                Sm.IsTxtEmpty(TxtPnName, "Partner name", false) ||
                Sm.IsMeeEmpty(MeeAddress, "Address") ||
                Sm.IsTxtEmpty(TxtIdentityNo, "Identity Number", false) ||
                Sm.IsTxtEmpty(TxtTurnover, "Turnover", true) ||
                Sm.IsLueEmpty(LueBCCode, "Business category") ||
                Sm.IsLueEmpty(LueBSCode, "Business sector");
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mPartnerAcNoAR = Sm.GetParameter("PartnerAcNoAR");
            mPartnerAcDescAR = Sm.GetParameter("PartnerAcDescAR");
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPnCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPnCode);
        }

        private void TxtPnName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPnName);
        }

        private void TxtShortName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtShortName);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                LueProvCode.EditValue = null;
                Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
                var CityCode = Sm.GetLue(LueCityCode);
                if (CityCode.Length > 0)
                {
                    var ProvCode = Sm.GetValue("Select ProvCode From TblCity Where CityCode=@Param;", CityCode);
                    if (ProvCode.Length>0) Sm.SetLue(LueProvCode, ProvCode);
                }
            }
        }

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtCompanyName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCompanyName);
        }

        private void LueCompanyCityCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                LueCompanyProvCode.EditValue = null;
                Sm.RefreshLookUpEdit(LueCompanyCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
                var CityCode = Sm.GetLue(LueCompanyCityCode);
                if (CityCode.Length > 0)
                {
                    var ProvCode = Sm.GetValue("Select ProvCode From TblCity Where CityCode=@Param;", CityCode);
                    if (ProvCode.Length > 0) Sm.SetLue(LueCompanyProvCode, ProvCode);
                }
            }
        }

        private void LueCompanyProvCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCompanyProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
        }

        private void TxtCompanyPostalCd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCompanyPostalCd);
        }

        private void TxtIdentityNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtIdentityNo);
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue2(Sl.SetLueOption), "Gender");
        }

        private void TxtTIN_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTIN);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMobile);
        }

        private void TxtManPower_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtManPower, 11);
        }

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAsset, 0);
        }

        private void TxtTurnover_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTurnover, 0);
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue1(Sl.SetLueBCCode));
        }

        private void LueBSCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBSCode, new Sm.RefreshLue1(Sl.SetLueBSCode));
        }

        #endregion

        #endregion
    }
}
