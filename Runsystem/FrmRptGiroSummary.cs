﻿#region Update
/*
    12/07/2017 [TKG] tambah entity
    18/07/2018 [TKG] tambah informasi site PO
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptGiroSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsEntityMandatory = false,
            mIsSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmRptGiroSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueEntCode(ref LueEntCode);
                SetLueYr(ref LueStatus);
                Sm.SetLue(LueStatus, "Active");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.*, Tbl2.EntName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T.*, (");
            SQL.AppendLine("        Select C.EntCode ");
            SQL.AppendLine("        From TblGiroMovement A ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("        Where A.BusinessPartnerCode=T.BusinessPartnerCode ");
            SQL.AppendLine("        And A.BusinessPartnerType=T.BusinessPartnerType ");
            SQL.AppendLine("        And A.BankCode=T.BankCode ");
            SQL.AppendLine("        And A.GiroNo=T.GiroNo ");
            SQL.AppendLine("        And A.DocType='01' ");
            SQL.AppendLine("        Order By B.DocDt Desc Limit 1 ");
            SQL.AppendLine("        ) As EntCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.BusinessPartnerCode, A.BusinessPartnerType, A.BankCode, B.VdName As Name, 'Vendor' As Type, C.BankName, A.GiroNo, A.ActInd, A.OpeningDt, A.DueDt, A.CurCode, A.Amt, D.PODocNo ");
            SQL.AppendLine("        From TblGiroSummary A ");
            SQL.AppendLine("        Inner Join TblVendor B On A.BusinessPartnerCode=B.VdCode " + Filter1);
            SQL.AppendLine("        Inner Join TblBank C On A.BankCode=C.BankCode ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select T.VdCode, T.GiroNo, ");
            if (mIsSiteMandatory)
                SQL.AppendLine("        Group_Concat(Distinct Concat(T.PODocNo, Case When T.SiteName Is Null Then '' Else Concat(' (', T.SiteName, ')') End) Order By T.SiteName, T.PODocNo Separator ', ') As PODocNo ");
            else
                SQL.AppendLine("        Group_Concat(Distinct T.PODocNo Order By T.PODocNo Separator ', ') As PODocNo ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select T3.VdCode, T1.GiroNo, T2.PODocNo, T4.SiteName ");
            SQL.AppendLine("                From TblVoucherHdr T1 ");
            SQL.AppendLine("                Inner Join TblAPDownpayment T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo And T2.PODocNo Is Not Null ");
            SQL.AppendLine("                Inner Join TblPOHdr T3 On T2.PODocNo=T3.DocNo ");
            SQL.AppendLine("                Left Join TblSite T4 On T3.SiteCode=T4.SiteCode ");
            SQL.AppendLine("                Where T1.GiroNo Is Not Null And T1.DocType='04' ");
            SQL.AppendLine("                Union All ");
            SQL.AppendLine("                Select T1.VdCode, T5.GiroNo, T4.PODocNo, T7.SiteName ");
            SQL.AppendLine("                From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("                Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("                Inner Join TblPurchaseInvoiceDtl T3 On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("                Inner Join TblRecvVdDtl T4 On T3.RecvVdDocNo=T4.DocNo And T3.RecvVdDNo=T4.DNo ");
            SQL.AppendLine("                Inner Join TblOutgoingPaymentDtl3 T5 On T1.DocNo=T5.DocNo ");
            SQL.AppendLine("                Left Join TblPOHdr T6 On T4.PODocNo=T6.DocNo ");
            SQL.AppendLine("                Left Join TblSite T7 On T6.SiteCode=T7.SiteCode ");
            SQL.AppendLine("            ) T Group By T.VdCode, T.GiroNo ");
            SQL.AppendLine("        ) D On A.BusinessPartnerCode=D.VdCode And A.GiroNo=D.GiroNo ");
            SQL.AppendLine("        Where A.BusinessPartnerType='1' " + Filter3);
            SQL.AppendLine("    Union all ");
            SQL.AppendLine("        Select A.BusinessPartnerCode, A.BusinessPartnerType, A.BankCode, B.CtName As Name, 'Customer' As Type, C.BankName, A.GiroNo, A.ActInd, A.OpeningDt, A.DueDt, A.CurCode, A.Amt, Null As PODocNo ");
            SQL.AppendLine("        From TblGiroSummary A ");
            SQL.AppendLine("        Inner Join TblCustomer B On A.BusinessPartnerCode=B.CtCode " + Filter2);
            SQL.AppendLine("        Left Join TblBank C On A.BankCode=C.BankCode ");
            SQL.AppendLine("        Where A.BusinessPartnerType='2' " + Filter3);
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Left Join TblEntity Tbl2 On Tbl1.EntCode=Tbl2.EntCode ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Name",
                        "Type",
                        "Bank",
                        "Giro#",
                        "Active",

                        //6-10
                        "Opening Date",
                        "Due Date",
                        "Currency",
                        "Amount",
                        "Entity",

                        //11
                        "PO#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 100, 270, 150, 60, 
                        
                        //6-10
                        100, 100, 60, 130, 200,

                        //11
                        500
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter3 = " ", Filter1 = " ", Filter2 = " ", Filter = string.Empty;

                var cm = new MySqlCommand();

                if (Sm.GetLue(LueStatus)=="Active") Filter3 = " And A.ActInd='Y' ";
                if (Sm.GetLue(LueStatus) == "Inactive") Filter3 = " And A.ActInd='N' ";
                
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter3, ref cm, Sm.GetLue(LueBankCode), "A.BankCode", true);
                Sm.FilterStr(ref Filter3, ref cm, TxtGiroNo.Text, "A.GiroNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "Tbl1.EntCode", true);

                if (!(Filter1 == " " && Filter2 == " "))
                {
                    if (Filter1 == " ") Filter1 = " And 1=0 ";
                    if (Filter2 == " ") Filter2 = " And 1=0 ";
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter1, Filter2, Filter3) + Filter +
                        " Order By Tbl1.Type, Tbl1.Name, Tbl1.BankName;",
                        new string[]
                        {
                            //0
                            "Name",

                            //1-5
                            "Type", "BankName", "GiroNo", "ActInd", "OpeningDt", 
                            
                            //6-10
                            "DueDt", "CurCode", "Amt", "EntName", "PODocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueYr(ref LookUpEdit Lue)
        {       
            string[] Status = new string[3];
            Status[0] = null;
            Status[1] = "Active";
            Status[2] = "Inactive";
            Sl.SetLookUpEdit(Lue, Status);
        }

        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGiroNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Giro#");
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bank");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion
    }
}
