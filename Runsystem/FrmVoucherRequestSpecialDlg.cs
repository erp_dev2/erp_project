﻿#region Update
/*
    10/06/2020 [VIN/IMS] new apps 
    06/08/2020 [TKG/IMS] ubah nilai amount utk ot
    12/08/2020 [VIN/IMS] bug - lup show dokumen OT verification tidak bisa 
    03/11/2020 [TKG/IMS] tambah payroll group
    11/02/2021 [WED/IMS] filter tanggal belom jalan
    18/11/2021 [TRI/IMS] filter payroll group belom jalan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestSpecialDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestSpecial mFrmParent;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestSpecialDlg(FrmVoucherRequestSpecial FrmParent) 
        
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            if (mFrmParent.mIsVRSRHA)
            {
                SQL.AppendLine("SELECT A.DocNo,  ");
                SQL.AppendLine("Concat(GROUP_CONCAT(Distinct D.DeptName SEPARATOR ', '), (DATE_FORMAT(A.HolidayDt, ' %d %M %Y'))) Notes, E.PGName, ");
                SQL.AppendLine("A.Amt, A.Remark  ");
                SQL.AppendLine("FROM TblRHAHdr A  ");
                SQL.AppendLine("INner Join TblRHADtl B ON A.DocNo=B.DocNo  ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode  ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode  ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr E ON A.PGCode=E.PGCode ");
                SQL.AppendLine("WHERE A.CancelInd='N' and A.DocNo NOT IN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T2.DocNo2 ");
                SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.Docno ");
                SQL.AppendLine("        And T1.DocType = 'VRRHA' ");
                SQL.AppendLine("        And T1.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Group By A.DocNo, A.Remark Order By A.DocNo; ");   
            }
            if (mFrmParent.mIsVRSIncentive)
            {
                SQL.AppendLine("Select A.DocNo, sum(B.Amount) Amt, A.Remark , ");
                SQL.AppendLine("Concat((DATE_FORMAT(A.DocDt, '%M %Y ')), GROUP_CONCAT(Distinct D.DeptName SEPARATOR ', ')) Notes, E.PGName ");
                SQL.AppendLine("FROM tblempincentivehdr A  ");
                SQL.AppendLine("Inner Join tblempincentivedtl B ON A.DocNo=B.DocNo  ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode  ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode  ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr E ON A.PGCode=E.PGCode ");
                SQL.AppendLine("WHERE A.CancelInd='N' and A.DocNo NOT IN  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T2.DocNo2 ");
                SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.Docno ");
                SQL.AppendLine("        And T1.DocType = 'VRINC' ");
                SQL.AppendLine("        And T1.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Group By A.DocNo, A.Remark Order By A.DocNo; ");
            }
            if (mFrmParent.mIsVRSOT)
            {
                SQL.AppendLine("Select A.DocNo, A.Remark, Sum(B.Amt) As Amt, ");
                SQL.AppendLine("Concat(GROUP_CONCAT(Distinct D.DeptName SEPARATOR ', '), (DATE_FORMAT(B.OTDt, ' %d %M %Y'))) Notes, E.PGName "); 
                SQL.AppendLine("FROM tblotverificationhdr A  ");
                SQL.AppendLine("Inner Join Tblotverificationdtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode  ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr E ON A.PGCode=E.PGCode ");
                SQL.AppendLine("WHERE A.CancelInd='N'  ");
                SQL.AppendLine("AND A.`Status`='A'  ");
                SQL.AppendLine("AND  A.DocNo NOT IN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T2.DocNo2 ");
                SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.Docno ");
                SQL.AppendLine("        And T1.DocType = 'VROT' ");
                SQL.AppendLine("        And T1.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                SQL.AppendLine(") ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Group By A.DocNo, A.Remark Order By A.DocNo; ");
            }
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",
                    

                    //1-5
                    "",
                    "Document#",
                    "",
                    "Notes",
                    "Amount",

                    //6-7
                    "Remark",
                    "Payroll's Group"
                },
                new int[] { 30, 20, 150, 20, 200, 150, 200, 200 }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7 });
            Grd1.Cols[7].Move(6);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();

                string Filter = string.Empty, DocNo = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " A.DocNo=@DocNo0" + r.ToString();
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not ( " + Filter + ") ";
                else
                    Filter = " "; 


                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePGCode), "E.PGCode", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[] 
                        { 
                            //0
                            "DocNo", 

                            //1-4
                            "Notes", "Amt", "Remark", "PGName"
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.ComputeTotalAmt();
                        
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;

            return false;
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payroll's Group");
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mFrmParent.mIsVRSRHA)
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f = new FrmRHA16("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
            if (mFrmParent.mIsVRSIncentive)
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f = new FrmEmpIncentive2("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
            if (mFrmParent.mIsVRSOT)
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f = new FrmOTVerification("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #endregion
    }
}
