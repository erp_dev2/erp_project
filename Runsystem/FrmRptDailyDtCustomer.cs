﻿#region Update
/*
     04/11/2019 [BRI+RDA/KSM] menu baru Reporting Daily DO to Customer
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDailyDtCustomer : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptDailyDtCustomer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
        
        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -6);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt2
                    }, true);
                Sl.SetLueWhsCode(ref LueWarehouse);
                SetLueItCt(ref LueItCt);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("SELECT B.ItName, A.* ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("    SELECT T1.ItCode, T1.Qty, T1.Qty2, T1.UPrice AS Price, T1.Qty*T1.UPrice AS Total, (T1.Qty*T1.UPrice)/T1.Qty AS Average, ");
            SQL.AppendLine("    T2.QtyDOCt, T1.UPrice AS Price2, T2.QtyDOCt*T1.UPrice AS Total2, (T2.QtyDOCt*T1.UPrice)/T2.QtyDOCt AS Average2, ");
            SQL.AppendLine("    T1.WhsCode, T1.DocDt As Daily ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        SELECT C.ItCode, SUM(IFNULL(C.Qty, 0)) Qty, SUM(IFNULL(C.UPriceAfterTax, 0)) UPrice, SUM(IFNULL(D.Qty, 0)) Qty2, B.DocDt, E.WhsCode ");
            SQL.AppendLine("        FROM tblsalescontract A ");
            SQL.AppendLine("        INNER JOIN tblsalesmemohdr B ON A.SalesMemoDocNo=B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tblsalesmemodtl C ON B.DocNo=C.DocNo ");
            SQL.AppendLine("        INNER JOIN tblstocksummary D ON C.ItCode=D.ItCode ");
            SQL.AppendLine("        INNER JOIN tblwarehouse E ON D.WhsCode=E.WhsCode AND FIND_IN_SET(E.WhsCode,'G.SPN,GBJ-K') ");
            SQL.AppendLine("        WHERE (A.DocDt BETWEEN @DocDt1 AND @DocDt2) AND A.CancelInd = 'N' ");
            SQL.AppendLine("        GROUP BY C.ItCode ");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT B.ItCode, SUM(IFNULL(B.Qty, 0)) QtyDOCt");
            SQL.AppendLine("        FROM tbldoct2hdr A ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tbldrhdr C ON A.DRDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tbldrdtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        INNER JOIN tblsalescontract E ON D.SCDocNo = E.DOcNo ");
            SQL.AppendLine("        WHERE A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("        GROUP BY B.ItCode ");
            SQL.AppendLine("    )T2 ON T1.ItCode = T2.ItCode ");
            SQL.AppendLine(")A ");
            SQL.AppendLine("INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
             

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Description",
                    "Item (In)",
                    "Entry Sale Price", 
                    "Total (In)",
                    "Average (In)",

                    //6-10
                    "Item (Out)",
                    "Out Sale Price",
                    "Total (Out)",
                    "Average (Out)",
                    "Daily"
                    
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 100, 180, 180, 180,

                    //6-10
                    100, 180, 180, 180, 50

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.GroupObject.Add(10);
            Grd1.GroupObject.Add(1);
            Grd1.Group();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count > 0)
                Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = String.Empty;
                DateTime d = DateTime.Now;
                String
                    CurrentDt = Sm.ServerCurrentDate(),
                    Dt = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWarehouse), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItem.Text, new string[] { "B.ItName", "B.ItCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCt), "A.WhsCode", true);
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " ; ", 
                new string[]
                    {
                        //0
                        "ItName",

                        //1-5
                        "Qty",
                        "Price", 
                        "Total",
                        "Average",
                        "QtyDOCt",

                        //6-9
                        "Price2",
                        "Total2",
                        "Average2",
                        "Daily"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                        
                        
                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(10);
                Grd1.GroupObject.Add(1);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueItCt(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sm.SetLue2(
                   ref Lue,
                   "Select 'G.SPN' As Col1, 'SPINNING' As Col2 Union All " +
                   "Select 'GBJ-K' As Col1, 'WEAVING' As Col2 ; ",
                   0, 35, false, true, "Code", "[Empty]", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion
        
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.DateTime.AddDays(6);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueWarehouse_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWarehouse, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWarehouse_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCt, new Sm.RefreshLue1(SetLueItCt));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item Category");
        }
       
        #endregion

        #endregion
    }
}
