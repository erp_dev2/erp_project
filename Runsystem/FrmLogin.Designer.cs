﻿namespace RunSystem
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnSetting = new System.Windows.Forms.Button();
            this.PnlSetting = new System.Windows.Forms.Panel();
            this.TxtServer = new System.Windows.Forms.TextBox();
            this.LblServer = new System.Windows.Forms.Label();
            this.LblDb = new System.Windows.Forms.Label();
            this.TxtDatabase = new System.Windows.Forms.TextBox();
            this.LblVersionNo = new System.Windows.Forms.Label();
            this.LblCompanyName = new System.Windows.Forms.Label();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.ChkKeepUserCode = new DevExpress.XtraEditors.CheckEdit();
            this.TxtPwd = new System.Windows.Forms.TextBox();
            this.TxtUserCode = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.PnlSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKeepUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.BtnSetting);
            this.panel1.Controls.Add(this.PnlSetting);
            this.panel1.Controls.Add(this.LblVersionNo);
            this.panel1.Controls.Add(this.LblCompanyName);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.ChkKeepUserCode);
            this.panel1.Controls.Add(this.TxtPwd);
            this.panel1.Controls.Add(this.TxtUserCode);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 110);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(1004, 361);
            this.panel1.TabIndex = 0;
            // 
            // BtnSetting
            // 
            this.BtnSetting.BackColor = System.Drawing.Color.Transparent;
            this.BtnSetting.BackgroundImage = global::RunSystem.Properties.Resources.Setting;
            this.BtnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtnSetting.FlatAppearance.BorderSize = 0;
            this.BtnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSetting.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSetting.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnSetting.Location = new System.Drawing.Point(685, 292);
            this.BtnSetting.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnSetting.Name = "BtnSetting";
            this.BtnSetting.Size = new System.Drawing.Size(27, 28);
            this.BtnSetting.TabIndex = 21;
            this.BtnSetting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSetting.UseVisualStyleBackColor = false;
            this.BtnSetting.Click += new System.EventHandler(this.BtnSetting_Click);
            // 
            // PnlSetting
            // 
            this.PnlSetting.Controls.Add(this.TxtServer);
            this.PnlSetting.Controls.Add(this.LblServer);
            this.PnlSetting.Controls.Add(this.LblDb);
            this.PnlSetting.Controls.Add(this.TxtDatabase);
            this.PnlSetting.Location = new System.Drawing.Point(728, 260);
            this.PnlSetting.Name = "PnlSetting";
            this.PnlSetting.Size = new System.Drawing.Size(266, 78);
            this.PnlSetting.TabIndex = 22;
            this.PnlSetting.Visible = false;
            // 
            // TxtServer
            // 
            this.TxtServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtServer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtServer.Location = new System.Drawing.Point(90, 45);
            this.TxtServer.Name = "TxtServer";
            this.TxtServer.Size = new System.Drawing.Size(166, 22);
            this.TxtServer.TabIndex = 8;
            this.TxtServer.Validated += new System.EventHandler(this.TxtServer_Validated);
            this.TxtServer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtServer_KeyDown);
            // 
            // LblServer
            // 
            this.LblServer.AutoSize = true;
            this.LblServer.BackColor = System.Drawing.Color.Transparent;
            this.LblServer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblServer.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LblServer.Location = new System.Drawing.Point(32, 47);
            this.LblServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblServer.Name = "LblServer";
            this.LblServer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblServer.Size = new System.Drawing.Size(52, 16);
            this.LblServer.TabIndex = 7;
            this.LblServer.Text = "Server";
            this.LblServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDb
            // 
            this.LblDb.AutoSize = true;
            this.LblDb.BackColor = System.Drawing.Color.Transparent;
            this.LblDb.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDb.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LblDb.Location = new System.Drawing.Point(14, 24);
            this.LblDb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblDb.Name = "LblDb";
            this.LblDb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblDb.Size = new System.Drawing.Size(70, 16);
            this.LblDb.TabIndex = 5;
            this.LblDb.Text = "Database";
            this.LblDb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDatabase
            // 
            this.TxtDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtDatabase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDatabase.Location = new System.Drawing.Point(90, 21);
            this.TxtDatabase.Name = "TxtDatabase";
            this.TxtDatabase.Size = new System.Drawing.Size(166, 22);
            this.TxtDatabase.TabIndex = 6;
            this.TxtDatabase.Validated += new System.EventHandler(this.TxtDatabase_Validated);
            this.TxtDatabase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtDatabase_KeyDown);
            // 
            // LblVersionNo
            // 
            this.LblVersionNo.AutoSize = true;
            this.LblVersionNo.BackColor = System.Drawing.Color.Transparent;
            this.LblVersionNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVersionNo.ForeColor = System.Drawing.Color.SteelBlue;
            this.LblVersionNo.Location = new System.Drawing.Point(729, 159);
            this.LblVersionNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblVersionNo.Name = "LblVersionNo";
            this.LblVersionNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblVersionNo.Size = new System.Drawing.Size(97, 16);
            this.LblVersionNo.TabIndex = 20;
            this.LblVersionNo.Text = "Version 9.999";
            this.LblVersionNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblCompanyName
            // 
            this.LblCompanyName.AutoSize = true;
            this.LblCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.LblCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCompanyName.ForeColor = System.Drawing.Color.SteelBlue;
            this.LblCompanyName.Location = new System.Drawing.Point(729, 136);
            this.LblCompanyName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCompanyName.Name = "LblCompanyName";
            this.LblCompanyName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblCompanyName.Size = new System.Drawing.Size(109, 16);
            this.LblCompanyName.TabIndex = 19;
            this.LblCompanyName.Text = "RUN System Inc";
            this.LblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLogin
            // 
            this.BtnLogin.BackColor = System.Drawing.Color.DodgerBlue;
            this.BtnLogin.FlatAppearance.BorderSize = 0;
            this.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLogin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLogin.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnLogin.Location = new System.Drawing.Point(893, 95);
            this.BtnLogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(59, 31);
            this.BtnLogin.TabIndex = 3;
            this.BtnLogin.Text = "&Log in";
            this.BtnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnLogin.UseVisualStyleBackColor = false;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // ChkKeepUserCode
            // 
            this.ChkKeepUserCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkKeepUserCode.Location = new System.Drawing.Point(729, 100);
            this.ChkKeepUserCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkKeepUserCode.Name = "ChkKeepUserCode";
            this.ChkKeepUserCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkKeepUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkKeepUserCode.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseFont = true;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkKeepUserCode.Properties.Caption = "Keep user id";
            this.ChkKeepUserCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkKeepUserCode.Size = new System.Drawing.Size(115, 22);
            this.ChkKeepUserCode.TabIndex = 4;
            // 
            // TxtPwd
            // 
            this.TxtPwd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtPwd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Location = new System.Drawing.Point(730, 65);
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.PasswordChar = '*';
            this.TxtPwd.Size = new System.Drawing.Size(221, 22);
            this.TxtPwd.TabIndex = 2;
            this.TxtPwd.UseSystemPasswordChar = true;
            this.TxtPwd.Validated += new System.EventHandler(this.TxtPwd_Validated);
            this.TxtPwd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtPwd_KeyDown);
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtUserCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Location = new System.Drawing.Point(730, 38);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Size = new System.Drawing.Size(221, 22);
            this.TxtUserCode.TabIndex = 1;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            this.TxtUserCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtUserCode_KeyDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::RunSystem.Properties.Resources.Pass;
            this.pictureBox2.Location = new System.Drawing.Point(664, 67);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 26);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RunSystem.Properties.Resources.User;
            this.pictureBox1.Location = new System.Drawing.Point(664, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.BackgroundImage = global::RunSystem.Properties.Resources.BGLogin;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(658, 361);
            this.panel2.TabIndex = 25;
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1028, 525);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Run System";
            //this.TopMost = true; //SET
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PnlSetting.ResumeLayout(false);
            this.PnlSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKeepUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PnlSetting;
        private System.Windows.Forms.Label LblDb;
        private System.Windows.Forms.Label LblServer;
        private System.Windows.Forms.Button BtnSetting;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Label LblCompanyName;
        private System.Windows.Forms.Label LblVersionNo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.CheckEdit ChkKeepUserCode;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox TxtDatabase;
        private System.Windows.Forms.TextBox TxtServer;
        private System.Windows.Forms.TextBox TxtUserCode;
        private System.Windows.Forms.TextBox TxtPwd;

    }
}