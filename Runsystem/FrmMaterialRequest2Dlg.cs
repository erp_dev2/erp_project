﻿#region Update
/* 
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    07/04/2020 [WED/SRN] benerin variabel SQL
    15/02/2022 [VIN/ALL] BUG : kolom tidak readonly

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest2 mFrmParent;
        private string mSQL = string.Empty, nSQL = string.Empty, oSQL = string.Empty, mDeptCode = string.Empty, mReqType = string.Empty;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmMaterialRequest2Dlg(FrmMaterialRequest2 FrmParent, string ReqType, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mReqType = ReqType;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            SetGrd();
            SetSQL();
            Sl.SetLueItCtCode(ref LueItCtCode);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's Name", 
                        "Item's Category",

                        //6-10
                        "UoM",
                        "Quotation"+Environment.NewLine+"Number",
                        "Quotation"+Environment.NewLine+"D No",
                        "Quotation"+Environment.NewLine+"Date",
                        "Estimated"+Environment.NewLine+"Price",

                        //11-15
                        "Minimum"+Environment.NewLine+"Stock",
                        "Reorder"+Environment.NewLine+"Point",
                        "Item Consumption"+Environment.NewLine+"last 1 Month",
                        "Item Consumption"+Environment.NewLine+"last 3 Month",
                        "Item Consumption"+Environment.NewLine+"last 6 Month",
                        
                        //16-20
                        "Item Consumption"+Environment.NewLine+"last 9 Month",
                        "Item Consumption"+Environment.NewLine+"last 12 Month",
                        "ItScCode",
                        "Sub Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 20, 250, 150,
                        
                        //6-10
                        80, 130, 80, 80, 80, 
                        
                        //11-15
                        100, 100, 120, 120, 120,

                        //16-20
                        120, 120, 80, 150, 80,

                        //21
                        150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] {0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            if (Sm.GetParameterBoo("IsUseItemConsumption"))
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 7, 8, 9, 10, 18, 19 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 7, 8, 9, 10, 18 }, false);
                }
            }
            else
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, false);
                }
            }
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL1 = new StringBuilder();

            #region query without reorderpoint
            if (Sm.CompareStr(mReqType, "2"))
            {
                    SQL.AppendLine("Select A.ItCode, A.ItName, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                    SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, A.ItScCode, D.ItScName "); 
                    SQL.AppendLine("From TblItem A ");
                    SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                    SQL.AppendLine(" Left Join ( ");
                    SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("        select Z1.itCode, ");
                    SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                    SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                    SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                    SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                    SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                    SQL.AppendLine("            From ");
                    SQL.AppendLine("            ( ");
                    SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                    SQL.AppendLine("                From ");
                    SQL.AppendLine("                ( ");
                    SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                    SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                    SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                    SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                    SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                    SQL.AppendLine("                )T1 ");
                    SQL.AppendLine("                Inner Join ");
                    SQL.AppendLine("                ( ");
                    SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                    SQL.AppendLine("	                From TblDODeptHdr A ");
                    SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                    SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                    SQL.AppendLine("	                Union ALL ");
                    SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                    SQL.AppendLine("	                From TblDODeptHdr A ");
                    SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                    SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                    SQL.AppendLine("	                Union All ");
                    SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                    SQL.AppendLine("	                From TblDODeptHdr A ");
                    SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                    SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                    SQL.AppendLine("	                Union All ");
                    SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                    SQL.AppendLine("	                From TblDODeptHdr A ");
                    SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                    SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                    SQL.AppendLine("	                Union All ");
                    SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                    SQL.AppendLine("	                From TblDODeptHdr A ");
                    SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                    SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                    SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                    SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                    SQL.AppendLine("        )Z1 ");
                    SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                    SQL.AppendLine(" )C On C.ItCode = A.ItCode ");
                    SQL.AppendLine(" Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, A.ItName, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("    ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, A.ItScCode, G.ItScName "); 
                SQL.AppendLine("From TblItem A " );
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                SQL.AppendLine("Left Join ( " );
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 " );
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' And T3a.Status='A' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
	            SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
	            SQL.AppendLine("        From TblCurrencyRate T3a  ");
	            SQL.AppendLine("        Inner Join  ");
	            SQL.AppendLine("        ( ");	
		        SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
		        SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
	            SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                //SQL.AppendLine("                        And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL.AppendLine("                        And Find_In_Set( ");
                SQL.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL.AppendLine("                        ) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                //SQL.AppendLine("                        And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL.AppendLine("                        And Find_In_Set( ");
                SQL.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL.AppendLine("                        ) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                //SQL.AppendLine("                        And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL.AppendLine("                        And Find_In_Set( ");
                SQL.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL.AppendLine("                        ) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                //SQL.AppendLine("                        And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL.AppendLine("                        And Find_In_Set( ");
                SQL.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL.AppendLine("                        ) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                //SQL.AppendLine("                        And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL.AppendLine("                        And Find_In_Set( ");
                SQL.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL.AppendLine("                        ) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" )F On F.ItCode = A.ItCode ");
                SQL.AppendLine(" Left Join TblItemSubCategory G On A.ItScCode = G.ItScCode ");
            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");
            //SQL.AppendLine("And A.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
            SQL.AppendLine("And Find_In_Set( ");
	        SQL.AppendLine("    IfNull(A.ItCtCode, ''), ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
            SQL.AppendLine("    ) ");
            
            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");
                
            SQL.AppendLine("And Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");

            if (mFrmParent.mIsDORequestUseItemIssued) SQL.AppendLine("And A.ControlByDeptCode is Not null ");

            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
            #endregion

            #region query with reorderpoint
            if (Sm.CompareStr(mReqType, "2"))
            {
                SQL1.AppendLine("Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItName,  A.ItCtCode, A.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, A.QtyStock, A.QtyOrder, A.Mth01, A.Mth03, A.Mth06, A.Mth09, A.Mth12, A.ItScCode, A.ItScName From ");
                SQL1.AppendLine("(Select A.ActInd,  A.ControlByDeptCode, A.ItCode, A.ItName, A.ItCtCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ifnull(C.Qty, 0) AS QtyStock, ifnull(D.QtyOrder, 0) As QtyOrder, ");
                SQL1.AppendLine("ifnull(E.Qty01, 0) As Mth01, ifnull(E.Qty03, 0) As Mth03, ifnull(E.Qty06, 0) As Mth06, ifnull(E.Qty09, 0) As Mth09, ifnull(E.Qty12, 0) As Mth12, A.ItScCode, F.ItScName "); 
                SQL1.AppendLine("From TblItem A ");
                SQL1.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                SQL1.AppendLine("Left Join ");
                SQL1.AppendLine(" (Select ItCode, SUM(QTY) As Qty from TblStockSummary Group By ItCode) ");
                SQL1.AppendLine(" C On A.ItCode = C.ItCode ");
                SQL1.AppendLine("Left Join ");
				SQL1.AppendLine("		(Select Sum(C.Qty)As QtyOrder, C.ItCode ");
		        SQL1.AppendLine("    From TblPODtl A ");
		        SQL1.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
		        SQL1.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
		        SQL1.AppendLine("    Where A.CancelInd='N' ");
		        SQL1.AppendLine("    And Concat(A.DocNo, A.DNo) Not In ( ");
		        SQL1.AppendLine("        Select Concat(PODocNo, PODNo) from TblRecvVdDtl Where CancelInd='N')");
				SQL1.AppendLine("			 Group By C.ItCode ");
		        SQL1.AppendLine("    )D On A.ItCode = D.ItCode ");
                SQL1.AppendLine(" Left Join ( ");
                SQL1.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL1.AppendLine("        From ( ");
                SQL1.AppendLine("        select Z1.itCode, ");
                SQL1.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL1.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL1.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL1.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL1.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL1.AppendLine("            From ");
                SQL1.AppendLine("            ( ");
                SQL1.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL1.AppendLine("                From ");
                SQL1.AppendLine("                ( ");
                SQL1.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL1.AppendLine("                )T1 ");
                SQL1.AppendLine("                Inner Join ");
                SQL1.AppendLine("                ( ");
                SQL1.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL1.AppendLine("	                Union ALL ");
                SQL1.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL1.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL1.AppendLine("        )Z1 ");
                SQL1.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL1.AppendLine(" )E On E.ItCode = A.ItCode ");
                SQL1.AppendLine(" Left Join TblItemSubCategory F On A.ItScCode = F.ItScCode ");
                SQL1.AppendLine(" Where (ifnull(C.Qty, 0)+ ifnull(D.QtyOrder, 0)) <= A.ReorderStock  And ");
                SQL1.AppendLine("  A.ReorderStock != 0 ) A ");
            }
            else
            {
                SQL1.AppendLine("Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItName, A.ItCtCode, A.ItCtName, A.PurchaseUomCode, A.DocNo, A.DNo, A.DocDt, ");
                SQL1.AppendLine("A.UPrice, A.MinStock, A.ReorderStock, A.QtyStock, A.QtyOrder, A.Mth01, A.Mth03, A.Mth06, A.Mth09, A.Mth12, A.ItScCode, A.ItScName From ( ");
                SQL1.AppendLine("Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItName, A.ItCtCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt,  ");
                SQL1.AppendLine("C.UPrice*");
                SQL1.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL1.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL1.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ifnull(Z1.Qty, 0) AS QtyStock, ifnull(Z2.QtyOrder, 0) As QtyOrder, ");
                SQL1.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, A.ItScCode, G.ItScName "); 
                SQL1.AppendLine("From TblItem A ");
                SQL1.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");                
                SQL1.AppendLine("Left Join ( ");
                SQL1.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL1.AppendLine("    From TblQtHdr T1 ");
                SQL1.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL1.AppendLine("    Inner Join ( ");
                SQL1.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL1.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL1.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL1.AppendLine("        Group By T3b.ItCode ");
                SQL1.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL1.AppendLine(") C On A.ItCode=C.ItCode ");                
                SQL1.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");                
                SQL1.AppendLine("Left Join ( ");
                SQL1.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL1.AppendLine("    From TblCurrency T1 ");
                SQL1.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL1.AppendLine("    Left Join ( ");
                SQL1.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL1.AppendLine("        From TblCurrencyRate T3a  ");
                SQL1.AppendLine("        Inner Join  ");
                SQL1.AppendLine("        ( ");
                SQL1.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL1.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL1.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL1.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL1.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");              
                SQL1.AppendLine("Left Join ");
                SQL1.AppendLine(" (Select ItCode, SUM(QTY) As Qty from TblStockSummary Group By ItCode) ");
                SQL1.AppendLine(" Z1 On A.ItCode = Z1.ItCode ");               
                SQL1.AppendLine("Left Join ");
                SQL1.AppendLine("		(Select Sum(C.Qty)As QtyOrder, C.ItCode ");
                SQL1.AppendLine("    From TblPODtl A ");
                SQL1.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                SQL1.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
                SQL1.AppendLine("    Where A.CancelInd='N' ");
                SQL1.AppendLine("    And Concat(A.DocNo, A.DNo) Not In ( ");
                SQL1.AppendLine("        Select Concat(PODocNo, PODNo) from TblRecvVdDtl Where CancelInd='N')");
                SQL1.AppendLine("			 Group By C.ItCode ");
                SQL1.AppendLine("    )Z2 On A.ItCode = Z2.ItCode ");
                SQL1.AppendLine(" Left Join ( ");
                SQL1.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL1.AppendLine("        From ( ");
                SQL1.AppendLine("        select Z1.itCode, ");
                SQL1.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL1.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL1.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL1.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL1.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL1.AppendLine("            From ");
                SQL1.AppendLine("            ( ");
                SQL1.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL1.AppendLine("                From ");
                SQL1.AppendLine("                ( ");
                SQL1.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL1.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL1.AppendLine("                )T1 ");
                SQL1.AppendLine("                Inner Join ");
                SQL1.AppendLine("                ( ");
                SQL1.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL1.AppendLine("	                Union ALL ");
                SQL1.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("	                Union All ");
                SQL1.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL1.AppendLine("	                From TblDODeptHdr A ");
                SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                //SQL1.AppendLine("                       And D.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
                SQL1.AppendLine("                        And Find_In_Set( ");
                SQL1.AppendLine("                        IfNull(D.ItCtCode, ''), ");
                SQL1.AppendLine("                        IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
                SQL1.AppendLine("                        ) ");
                SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL1.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL1.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL1.AppendLine("        )Z1 ");
                SQL1.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL1.AppendLine(" )F On F.ItCode = A.ItCode ");
                SQL1.AppendLine(" Left Join TblItemSubCategory G On A.ItScCode = G.ItScCode ");
                SQL1.AppendLine(" Where (ifnull(Z1.Qty, 0)+ ifnull(Z2.QtyOrder, 0)) <= A.ReorderStock  And ");
                SQL1.AppendLine("  A.ReorderStock != 0 ) A ");
            }

            SQL1.AppendLine("Where A.ActInd = 'Y' ");
            //And A.ItCtCode = (Select parValue From Tblparameter Where Parcode = 'ItCtExim' ) ");
            SQL1.AppendLine("And Find_In_Set( ");
            SQL1.AppendLine("IfNull(A.ItCtCode, ''), ");
            SQL1.AppendLine("IfNull((Select ParValue From TblParameter Where ParCode='ItCtExim'), '') ");
            SQL1.AppendLine(") ");
                if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL1.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            SQL1.AppendLine("And Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");

            nSQL = SQL1.ToString();
            #endregion

        }

        override protected void HideInfoInGrd()
        {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (ChkROP.Checked == true)
                {
                    oSQL = nSQL.ToString();
                }
                else
                {
                    oSQL = mSQL.ToString();
                }
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
                Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        oSQL.ToString() + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ItCtName", "PurchaseUomCode", "DocNo", "DNo", 
                            
                            //6-7
                            "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",

                            //11-15
                            "Mth03", "Mth06", "Mth09", "Mth12", "ItScCode",
  
                            //16
                            "ItScName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 6);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 7);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 8);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 9);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 19);

                        mFrmParent.ComputeTotal(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index,6), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }

        #endregion 

        #endregion        

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
