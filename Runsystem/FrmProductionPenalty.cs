﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionPenalty : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = "", mAccessInd = "",
            mDocNo = ""; //if this application is called from other application;
        internal int mNumberOfPlanningUomCode = 1;
        internal FrmProductionPenaltyFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionPenalty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Penalty";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetNumberOfPlanningUomCode();
                Sl.SetLueProductionWagesSource(ref LueProductionWagesSource);
                Sl.SetLueWagesFormulationCode(ref LueWagesFormulationCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, EventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "SFC DNo",
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        
                        //6-10
                        "Item's Name",
                        "Batch Number",
                        "SFC"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "Uom",
                        
                        //11-15
                        "SFC"+Environment.NewLine+"Quantity 2",
                        "Quantity 2",
                        "Uom 2",
                        "Index",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 0, 80, 20, 80, 
                        
                        //6-10
                        200, 200, 80, 80, 80,  
                        
                        //11-15
                        80, 80, 80, 80, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 11, 12, 13, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            ShowPlanningUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 4;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-3
                        "Value",
                        "Penalty/Unit",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-3
                        120, 120, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 1, 2, 3 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 9;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        "Grade Level",

                        //6-8
                        "Index",
                        "Quantity",
                        "Penalty"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        0, 200, 150, 150, 120,  
                        
                        //6-8
                        100, 80, 100
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 5, 6, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });

            #endregion

            #region Grd4

            Grd4.Cols.Count = 10;
            Grd4.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        
                        //6-9
                        "Grade Level Code",
                        "Grade Level",
                        "Index",
                        "Penalty"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 0, 120, 120, 120, 
                        
                        //6-8
                        0, 150, 80, 100
                    }
                );
            Sm.GrdColButton(Grd4, new int[] { 1 });
            Sm.GrdFormatDec(Grd4, new int[] { 8, 9 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 2, 6, 7, 8, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

            #endregion
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 14, 15 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 2, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);

            //Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5 }, !ChkHideInfoInGrd.Checked);
            //Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            //Sm.GrdColInvisible(Grd4, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 

                        TxtDocNo, ChkCancelInd, DteDocDt, TxtShopFloorControlDocNo, DteShopFloorControlDocDt, 
                        TxtWorkCenterDocNo, TxtProductionShiftCode, LueProductionWagesSource, LueWagesFormulationCode, TxtUomCode, 
                        LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 9, 12 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1 });
                    BtnWorkCenterDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWagesFormulationCode, LueCurCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 9, 12 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1 });
                    BtnWorkCenterDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd
                    }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtShopFloorControlDocNo, DteShopFloorControlDocDt, TxtProductionShiftCode, 
                 TxtWorkCenterDocNo, LueProductionWagesSource, LueWagesFormulationCode, TxtUomCode, LueCurCode, 
                 MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 2);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        internal void ClearGrd()
        {
            //iGSubtotalManager.HideSubtotals(Grd1);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 11, 12, 14, 15 });

            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
        }

        private void ClearGrd2()
        {
            iGSubtotalManager.HideSubtotals(Grd2);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1, 2, 3 });
        }

        private void ClearGrd3()
        {
            iGSubtotalManager.HideSubtotals(Grd3);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6, 7, 8 });
        }

        private void ClearGrd4()
        {
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 8, 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionPenaltyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sm.SetLue(LueProductionWagesSource, "1");
                BtnWorkCenterDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && 
                        !Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "SFC#", false) &&
                        !Sm.IsTxtEmpty(TxtUomCode, "Uom", false))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmProductionPenaltyDlg2(this, TxtShopFloorControlDocNo.Text));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 9, 11 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 14, 15 });
                    }
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 3));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ShowGrdInfo2();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && 
                BtnSave.Enabled && 
                !Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "SFC#", false) &&
                !Sm.IsTxtEmpty(TxtUomCode, "Uom", false) &&
                TxtDocNo.Text.Length == 0)
                    Sm.FormShowDialog(new FrmProductionPenaltyDlg2(this, TxtShopFloorControlDocNo.Text));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 3));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 9, 12 }, e);

            if (e.ColIndex == 9)
                ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 3, 9, 12, 10, 13);

            if (e.ColIndex == 12)
                ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 3, 12, 9, 13, 10);

            if (e.ColIndex == 9 || e.ColIndex == 12)
            {
                ComputeValue(e.RowIndex);
                ShowGrdInfo2();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionPenalty", "TblProductionPenaltyHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProductionPenaltyHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) cml.Add(SaveProductionPenaltyDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Row!=Grd2.Rows.Count-1) 
                    cml.Add(SaveProductionPenaltyDtl2(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveProductionPenaltyDtl3(DocNo, Row));

            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0) cml.Add(SaveProductionPenaltyDtl4(DocNo, Row));

            cml.Add(UpdateSFCProcessInd());
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "SFC#", false) ||
                Sm.IsLueEmpty(LueProductionWagesSource, "Source") ||
                Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") ||
                Sm.IsTxtEmpty(TxtUomCode, "Uom", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsSFCProcessInd2AlreadyFulfilled() ||
                IsSFCAlreadyCancelled() ||
                IsGrd1ValueNotValid() ||
                IsGrd1ExceedMaxRecords() ||
                IsGrd4ValueNotValid() ||
                IsGrd4ExceedMaxRecords();
            ;
        }

        private bool IsSFCProcessInd2AlreadyFulfilled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblShopFloorControlHdr " +
                    "Where ProcessInd2='F' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SFC# : " + TxtShopFloorControlDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This SFC# already had penalties.");
                return true;
            }
            return false;
        }

        private bool IsSFCAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblShopFloorControlHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SFC# : " + TxtShopFloorControlDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This SFC# already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            string Msg = "";

            ReComputeOutstandingQty();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch number : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine;
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, true, Msg + "Quantity should not be 0.")
                ) return true;

                if (Sm.GetGrdDec(Grd1, Row, 9) > Sm.GetGrdDec(Grd1, Row, 8))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than SFC quantity.");
                    return true;
                }

                if (Grd1.Cols[12].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 11))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should not be bigger than SFC quantity (2).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrd4ExceedMaxRecords()
        {
            if (Grd4.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd4.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd4ValueNotValid()
        {
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd4, Row, 2, false, "Employee is empty.")) return true;
            return false;
        }

        private MySqlCommand SaveProductionPenaltyHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProductionPenaltyHdr(DocNo, DocDt, CancelInd, ShopFloorControlDocNo, ProductionWagesSource, WagesFormulationCode, UomCode, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @ShopFloorControlDocNo, @ProductionWagesSource, @WagesFormulationCode, @UomCode, @CurCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWagesSource", Sm.GetLue(LueProductionWagesSource));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionPenaltyDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionPenaltyDtl(DocNo, DNo, ShopFloorControlDNo, Qty, Qty2, ItIndex, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ShopFloorControlDNo, @Qty, @Qty2, @ItIndex, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionPenaltyDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionPenaltyDtl2(DocNo, DNo, Value, Penalty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Value, @Penalty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionPenaltyDtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionPenaltyDtl3(DocNo, DNo, EmpCode, GrdLvlIndex, Qty, Penalty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @GrdLvlIndex, @Qty, @Penalty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionPenaltyDtl4(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionPenaltyDtl4(DocNo, DNo, EmpCode, GrdLvlIndex, Penalty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @GrdLvlIndex, @Penalty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd4, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd4, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSFCProcessInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlDtl Tbl1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DocNo, DNo, "); 
            SQL.AppendLine("    Case When Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
	        SQL.AppendLine("        If(Qty<=Qty2, 'F', 'P') ");
            SQL.AppendLine("    End As ProcessInd2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocNo, A.DNo, "); 
            SQL.AppendLine("        A.Qty+A.Qty2 As Qty, ");
            SQL.AppendLine("        IfNull(B.Qty, 0)+IfNull(B.Qty2, 0) As Qty2 ");    
            SQL.AppendLine("        From TblShopFloorControlDtl A ");
            SQL.AppendLine("        Left Join ( ");
	        SQL.AppendLine("            Select T1.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
	        SQL.AppendLine("            Sum(Qty) Qty, Sum(Qty2) Qty2 ");
	        SQL.AppendLine("            From TblProductionPenaltyHdr T1 "); 
	        SQL.AppendLine("            Inner Join TblProductionPenaltyDtl T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("            Where T1.CancelInd='N' ");
	        SQL.AppendLine("            And T1.ShopFloorControlDocNo=@ShopFloorControlDocNo ");
	        SQL.AppendLine("            Group By T1.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("        Where A.DocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl1.DNo=Tbl2.DNo ");
            SQL.AppendLine("Set Tbl1.ProcessInd2=Tbl2.ProcessInd2; ");

            SQL.AppendLine("Update TblShopFloorControlHdr T Set ");
	        SQL.AppendLine("    T.ProcessInd2 = "); 
		    SQL.AppendLine("        Case When Not Exists( ");
			SQL.AppendLine("            Select DocNo From TblShopFloorControlDtl "); 
			SQL.AppendLine("            Where ProcessInd2<>'O' And DocNo=T.DocNo Limit 1 ");
			SQL.AppendLine("        ) Then 'O' ");
		    SQL.AppendLine("        Else ");
			SQL.AppendLine("            Case When Not Exists( ");
			SQL.AppendLine("                Select DocNo From TblShopFloorControlDtl "); 
			SQL.AppendLine("                Where ProcessInd2<>'F' And DocNo=T.DocNo Limit 1 ");
			SQL.AppendLine("            ) Then 'F' Else 'P' End ");
		    SQL.AppendLine("        End ");
            SQL.AppendLine("Where T.DocNo=@ShopFloorControlDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelProductionPenalty());
            cml.Add(UpdateSFCProcessInd());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToPPR();
        }

        private bool IsDataAlreadyProcessedToPPR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProductionPenaltyDtl3 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo From TblProductionPenaltyDtl4 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to production payrun.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProductionPenaltyHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled .");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelProductionPenalty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionPenaltyHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProductionPenaltyHdr(DocNo);
                ShowProductionPenaltyDtl(DocNo);
                ShowProductionPenaltyDtl2(DocNo);
                ShowProductionPenaltyDtl3(DocNo);
                ShowProductionPenaltyDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProductionPenaltyHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ShopFloorControlDocNo, C.DocName, B.DocDt As ShopFloorControlDocDt, ");
            SQL.AppendLine("D.ProductionShiftName, A.ProductionWagesSource, A.WagesFormulationCode, A.UomCode, A.CurCode, A.Remark ");
            SQL.AppendLine("From TblProductionPenaltyHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblProductionShift D On B.ProductionShiftCode=D.ProductionShiftCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "DocName", "ShopFloorControlDocNo", "ShopFloorControlDocDt", 
                        
                        //6-10
                        "ProductionShiftName", "ProductionWagesSource", "WagesFormulationCode", "UomCode", "CurCode", 
                        
                        //11
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtWorkCenterDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtShopFloorControlDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetDte(DteShopFloorControlDocDt, Sm.DrStr(dr, c[5]));
                        TxtProductionShiftCode.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueProductionWagesSource, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueWagesFormulationCode, Sm.DrStr(dr, c[8]));
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[10]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowProductionPenaltyDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDNo, B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, ");
            SQL.AppendLine("B.Qty-IfNull(D.Qty, 0) As SFCQty, A.Qty, C.PlanningUomCode, B.Qty2-IfNull(D.Qty2, 0) As SFCQty2, A.Qty2, C.PlanningUomCode2, IfNull(A.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("Case When @UomCode=C.PlanningUomCode2 Then (A.Qty2*IfNull(A.ItIndex, 0)) ");
            SQL.AppendLine("Else (A.Qty*IfNull(A.ItIndex, 0)) ");
            SQL.AppendLine("End As Value ");
            SQL.AppendLine("From TblProductionPenaltyDtl A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDNo=B.DNo And B.DocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDNo, Sum(Qty) Qty, Sum(Qty2) Qty2 ");
            SQL.AppendLine("    From TblProductionPenaltyHdr T1 ");
            SQL.AppendLine("    Inner Join TblProductionPenaltyDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.ShopFloorControlDocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocNo<>@DocNo ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DNo=D.ShopFloorControlDNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", (TxtUomCode.Text.Length == 0 ? "XXX" : TxtUomCode.Text));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDNo", "ItCode", "ItCodeInternal", "ItName", "BatchNo",   
                    
                    //6-10
                    "SFCQty", "Qty", "PlanningUomCode", "SFCQty2", "Qty2",  
                    
                    //11-13
                    "PlanningUomCode2", "ItIndex", "Value" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 14, 15 });
            //if (Grd1.Rows.Count > 1)
            //{
            //    Grd1.Rows.Count -= 1;
            //    iGSubtotalManager.BackColor = Color.LightSalmon;
            //    iGSubtotalManager.ShowSubtotalsInCells = true;
            //    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 8 });
            //}
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowProductionPenaltyDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, Value, Penalty, (Value*Penalty) As Amt ");
            SQL.AppendLine("From TblProductionPenaltyDtl2 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-3
                   "Value", "Penalty", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            if (Grd2.Rows.Count > 1)
            {
                Grd2.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd2, new int[] { 1, 3 });
            }
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowProductionPenaltyDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, D.PosName, E.DeptName, ");
            SQL.AppendLine("C.GrdLvlName, A.GrdLvlIndex, A.Qty, A.Penalty ");
            SQL.AppendLine("From TblProductionPenaltyDtl3 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On B.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "EmpCode", "EmpName", "PosName", "DeptName", "GrdLvlName",  
                   
                   //6-8
                   "GrdLvlIndex", "Qty", "Penalty" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            if (Grd3.Rows.Count > 1)
            {
                Grd3.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd3, new int[] { 6, 7, 8 });
            }
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowProductionPenaltyDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, B.GrdLvlCode, E.GrdLvlName, ");
            SQL.AppendLine("A.GrdLvlIndex, A.Penalty ");
            SQL.AppendLine("From TblProductionPenaltyDtl4 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "EmpCode", "EmpName", "PosName", "DeptName", "GrdLvlCode",   
                   
                   //6-8
                   "GrdLvlName", "GrdLvlIndex", "Penalty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 8, 9 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ComputeValue(int Row)
        {
            string UomCode = TxtUomCode.Text;

            Decimal Qty = 0m, ItemIndex = 0m;

            if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 10)))
            {
                if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 9);
            }
            else
            {
                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 13)))
                {
                    if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 12);
                }
            }

            if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0) ItemIndex = Sm.GetGrdDec(Grd1, Row, 14);

            Grd1.Cells[Row, 15].Value = Qty * ItemIndex;
        }

        private void SetNumberOfPlanningUomCode()
        {
            string Param = Sm.GetParameter("NumberOfPlanningUomCode");
            if (Param.Length != 0) mNumberOfPlanningUomCode = int.Parse(Param);
        }

        internal string GetSelectedDNo()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedEmployee()
        {
            var SQL = "";
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowGrdInfo2()
        {
            decimal Value = 0m, From = 0m, To = 0m, Penalty = 0m, Temp = 0m;

            ClearGrd2();
            ClearGrd3();
            ClearGrd4();

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 15).Length != 0)
                        Value += Sm.GetGrdDec(Grd1, Row, 15);
                }
            }

            if (Value <= 0 || Sm.GetLue(LueWagesFormulationCode).Length == 0)
            {
                ShowGrdInfo3();
                ShowGrdInfo4();
                ReComputeOtherEmployeePenalty();
                return;
            }

            Temp = Value;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ValueFrom, ValueTo, Penalty ");
            SQL.AppendLine("From TblWagesFormulationDtl ");
            SQL.AppendLine("Where WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Order By ValueFrom;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 0;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "ValueFrom", "ValueTo", "Penalty" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd2.ProcessTab = true;
                        Grd2.BeginUpdate();
                        while (dr.Read())
                        {
                            From = Sm.DrDec(dr, 0);
                            To = Sm.DrDec(dr, 1);
                            Penalty = Sm.DrDec(dr, 2);

                            Grd2.Rows.Add();

                            if (Value >= From)
                            {
                                if (Temp <= To)
                                {
                                    Grd2.Cells[Row, 1].Value = Temp;
                                    Grd2.Cells[Row, 2].Value = Penalty;
                                    Grd2.Cells[Row, 3].Value = Temp * Penalty;
                                    Temp = 0m;
                                }
                                else
                                {
                                    Grd2.Cells[Row, 1].Value = To;
                                    Grd2.Cells[Row, 2].Value = Penalty;
                                    Grd2.Cells[Row, 3].Value = To * Penalty;
                                    Temp -= To;
                                }
                            }
                            else
                                Temp = 0m;
                            if (Temp == 0m) break;
                            Row++;


                        }
                    }

                    if (Grd2.Rows.Count > 1)
                    {
                        Grd2.Rows.Count -= 1;
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd2, new int[] { 1, 3 });
                    }
                    Grd2.EndUpdate();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
            ShowGrdInfo3();
            ShowGrdInfo4();
            ReComputeOtherEmployeePenalty();
        }

        private void ShowGrdInfo3()
        {
            decimal Total = 0m;

            if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BomCode As EmpCode, B.EmpName, F.PosName, G.DeptName, E.GrdLvlName, ");
            SQL.AppendLine("IfNull(D.GrdLvlIndex, 1) As GrdLvlIndex, A.Qty, ");
            SQL.AppendLine("Case When C.TotalQty=0 Then 0 Else (A.Qty/C.TotalQty)*@Total*IfNull(D.GrdLvlIndex, 1) End As Wages ");
            SQL.AppendLine("From TblShopFloorControl2Dtl A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.BomCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            //SQL.AppendLine("    And B.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DocNo, Sum(Qty) As TotalQty ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And BomType='3' And CoordinatorInd='N' ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 D On B.GrdLvlCode=D.GrdLvlCode And D.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblPosition F On B.PosCode=F.PosCode ");
            SQL.AppendLine("Left Join TblDepartment G On B.DeptCode=G.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.BomType='3' And A.CoordinatorInd='N' ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Total", Total);

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-5
                               "EmpName",  "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", 

                               //6-7
                              "Qty", "Wages"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            if (Grd3.Rows.Count > 1)
            {
                Grd3.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd3, new int[] { 6, 7, 8 });
            }
        }

        private void ShowGrdInfo4()
        {
            decimal TotalEmployee = 0m, Total = 0m;

            TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
            if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, D.GrdLvlName, A.GrdLvlCode, E.GrdLvlIndex, ");
            SQL.AppendLine("Case When @TotalEmployee=0 Then 0 Else (1/@TotalEmployee)*@Total*IfNull(E.GrdLvlIndex, 1) End As Penalty ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 E On A.GrdLvlCode=E.GrdLvlCode And E.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Where A.ResignDt Is Null ");
            //SQL.AppendLine("And A.PosCode=IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("And A.EmpCode In (Select BomCode From TblShopFloorControl2Dtl Where DocNo=@ShopFloorControlDocNo And BomType='3' And CoordinatorInd='Y') ");
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<decimal>(ref cm, "@TotalEmployee", TotalEmployee);
            Sm.CmParam<decimal>(ref cm, "@Total", Total);

            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                            //0
                            "EmpCode",
 
                            //1-5
                            "EmpName", "PosName", "DeptName", "GrdLvlCode", "GrdLvlName",  
                            
                            //6-7
                            "GrdLvlIndex", "Penalty"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    }, false, false, true, false
            );
            //if (!(Grd4.Rows.Count==1 && Sm.GetGrdStr(Grd4, Grd4.Rows.Count-1, 2).Length==0))
            //{
            //    Grd4.Rows.Add();
            //    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 8, 9 });
            //}
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 8, 9 });
        }

        private void ReComputeOtherEmployeePenalty()
        {
            try
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    Grd4.Cells[Row, 6].Value = null;
                    Grd4.Cells[Row, 7].Value = null;
                    Grd4.Cells[Row, 8].Value = 0m;
                    Grd4.Cells[Row, 9].Value = 0m;
                }

                if (Grd4.Rows.Count != 1 && Sm.GetLue(LueWagesFormulationCode).Length!=0)
                {
                    SetOtherEmployeeGrdLvlIndex();
                    ComputeOtherEmployeePenalty();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeOtherEmployeePenalty()
        {
            decimal TotalEmployee = 0m, Total = 0m, GrdLvlIndex = 0m;

            TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
            if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                GrdLvlIndex = Sm.GetGrdDec(Grd4, Row, 8);
                if (TotalEmployee == 0m)
                    Grd4.Cells[Row, 9].Value = 0m;
                else
                {
                    if (GrdLvlIndex == 0m) GrdLvlIndex = 1;
                    Grd4.Cells[Row, 9].Value = (1 / TotalEmployee) * Total * GrdLvlIndex;
                }
            }
        }

        private void SetOtherEmployeeGrdLvlIndex()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.GrdLvlCode, B.GrdLvlName, C.GrdLvlIndex ");
            //SQL.AppendLine("Case When @TotalEmployee=0 Then 0 Else (1/@TotalEmployee)*@Total*IfNull(E.GrdLvlIndex, 1) End As Penalty ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 C ");
            SQL.AppendLine("    On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("    And C.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Where Position(Concat('##', A.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("Order By A.EmpCode; ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedEmployee", GetSelectedEmployee());
                Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "EmpCode", 
                        //1-3
                        "GrdLvlCode", "GrdLvlName", "GrdLvlIndex" 
                    });

                if (dr.HasRows)
                {
                    Grd4.ProcessTab = true;
                    Grd4.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd4, Row, 2), Sm.DrStr(dr, 0)))
                            {
                                Sm.SetGrdValue("S", Grd4, dr, c, Row, 6, 1);
                                Sm.SetGrdValue("S", Grd4, dr, c, Row, 7, 2);
                                Sm.SetGrdValue("N", Grd4, dr, c, Row, 8, 3);
                                break;
                            }
                        }
                    }
                    Grd4.EndUpdate();
                }
                dr.Close();
            }
        }

        private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            return Sm.GetValueDec(cm);
        }

        private void ComputeQtyBasedOnConvertionFormula(
            string ConvertType, iGrid Grd, int Row, int ColItCode,
            int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        {
            try
            {
                if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
                {
                    decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
                    if (Convert != 0)
                        Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        

        private void ShowUom()
        {
            TxtUomCode.EditValue = null;
            var WagesFormulationCode = Sm.GetLue(LueWagesFormulationCode);

            if (WagesFormulationCode.Length != 0)
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Select UomCode From TblWagesFormulationHdr " +
                        "Where WagesFormulationCode=@Param;"
                };
                Sm.CmParam<String>(ref cm, "@Param", WagesFormulationCode);
                TxtUomCode.EditValue = Sm.GetValue(cm);
            }
        }

        private void ReComputeOutstandingQty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, ");
            SQL.AppendLine("A.Qty-IfNull(B.Qty, 0) As Qty, A.Qty2-IfNull(B.Qty2, 0) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDNo, Sum(Qty) Qty, Sum(Qty2) Qty2 ");
            SQL.AppendLine("    From TblProductionPenaltyHdr T1 ");
            SQL.AppendLine("    Inner Join TblProductionPenaltyDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.ShopFloorControlDocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDNo ");
            SQL.AppendLine(") B On A.DNo=B.ShopFloorControlDNo ");
            SQL.AppendLine("Where A.DocNo=@ShopFloorControlDocNo");
            SQL.AppendLine("And Locate(Concat('##', A.DNo, '##'), @SelectedSFC)>0 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SelectedSFC", GetSelectedSFC());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "DNo", "Qty", "Qty2" });
                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 2);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal string GetSelectedSFC()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueProductionWagesSource_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionWagesSource, new Sm.RefreshLue1(Sl.SetLueProductionWagesSource));
        }

        private void LueWagesFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueWagesFormulationCode, new Sm.RefreshLue1(Sl.SetLueWagesFormulationCode));
                ShowUom();
                ShowGrdInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        #endregion

        #region Grid Event

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueWagesFormulationCode, "Penalty formulation"))
                {
                    e.DoDefault = false;

                    decimal TotalEmployee = 0m, Total = 0m;

                    TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
                    if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                        Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

                    Sm.FormShowDialog(new FrmProductionPenaltyDlg3(this, TotalEmployee, Total, Sm.GetLue(LueWagesFormulationCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 8, 9 });
                }
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                decimal TotalEmployee = 0m, Total = 0m;

                TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
                if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                    Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

                Sm.FormShowDialog(new FrmProductionPenaltyDlg3(this, TotalEmployee, Total, Sm.GetLue(LueWagesFormulationCode)));
            }
        }

        #endregion

        #region Button Event

        private void BtnWorkCenterDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProductionPenaltyDlg(this));
        }

        private void BtnShopFloorControlDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "SFC#", false))
            {
                var f1 = new FrmShopFloorControl2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtShopFloorControlDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
