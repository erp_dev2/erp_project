﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFormula : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
        mDocNo = string.Empty;
        internal FrmFormulaFind FrmFind;

        #endregion

        #region Constructor

        public FrmFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Formula";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            SetGrd();
            base.FrmLoad(sender, e);

            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Find",
                        //1-4
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "Quantity",
                        "UoM"
                    },
                    new int[] 
                    {
                        40, 
                        100, 200, 100, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtDocName, TxtQty, MeeDescForm
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtDocName, TxtQty, MeeDescForm
                    }, false);
                    Grd1.ReadOnly = false;
                    ChkActiveInd.Checked = true;
                    TxtDocName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtDocName, TxtQty, MeeDescForm
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtDocName, MeeDescForm
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
               TxtQty
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFormulaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);

        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmFormulaDlg(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }


        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmFormulaDlg(this));
        }

        #endregion

        #region Insert Data


        private void InsertData()
        {
            try
            {
                if (IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Formula", "TblFormulaHdr");
                var cml = new List<MySqlCommand>();

                cml.Add(SaveFormulaHdr(DocNo));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveFormulaDtl(DocNo, Row));

                Sm.ExecCommands(cml);
                
                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocName, "Formula Name", false) ||
                Sm.IsDteEmpty(DteDocDt, "Formula date") ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsUOmNotValid();
        }

        private bool IsCancelledProcessedAlreadyYes(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblFormulaHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And ActiveInd = 'Y'  ");
            SQL.AppendLine("Limit 1 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is processed already.");
                return true;
            }
            return false;
        }

        private bool IsCancelledProcessedAlreadyNo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblFormulaHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And ActiveInd = 'N'  ");
            SQL.AppendLine("Limit 1 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is processed already.");
                return true;
            }
            return false;
        }

        private bool IsUOmNotValid()
        {
            bool NotValid = false;
            string CurCode = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        if (CurCode.Length != 0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            NotValid = true;
                            break;
                        }
                        CurCode = Sm.GetGrdStr(Grd1, Row, 4);
                    }
                }
            } 
            if (NotValid) Sm.StdMsg(mMsgType.Warning, " Only allowed 1 Uom type");
            return NotValid;
        }

       

        private void CancelData()
        {
            try
            {
                string DocNo = TxtDocNo.Text;
                if (ChkActiveInd.Checked == false)
                {
                    if (IsCancelledProcessedAlreadyNo(DocNo)) return;
                    Cursor.Current = Cursors.WaitCursor;

                    var cml = new List<MySqlCommand>();
                    cml.Add(CancelActive(DocNo));

                    Sm.ExecCommands(cml);

                    ShowData(DocNo);
                }

                else if (ChkActiveInd.Checked == true)
                {
                    if (IsCancelledProcessedAlreadyYes(DocNo)) return;
                    Cursor.Current = Cursors.WaitCursor;

                    var cml = new List<MySqlCommand>();
                    cml.Add(CancelActive(DocNo));

                    Sm.ExecCommands(cml);

                    ShowData(DocNo);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, 
                        "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "cannot be 0."))
                    return true;
            }
            return false;
        }

        private MySqlCommand CancelActive(string DocNo)
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblFormulaHdr Set ");
            SQL.AppendLine("ActiveInd=@ActiveInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFormulaHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblFormulaHdr(DocNo, DocDt, DocName, ActiveInd, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocName, @ActiveInd, @Qty, @Remark, @CreateBy, CurrentDateTime()) ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeDescForm.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFormulaDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblFormulaDtl(DocNo, DNo, ItCode, Qty, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @ItCode, @Qty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowFormulaHdr(DocNo);
                ShowFormulaDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFormulaHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, DocName, ActiveInd, Qty, Remark  From TblFormulaHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "DocName", "ActiveInd", "Qty", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtDocName.EditValue = Sm.DrStr(dr, c[2]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        MeeDescForm.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowFormulaDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PlanningUomCode ");
                SQL.AppendLine("From TblFormulaDtl A Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "ItCode", "ItName", "Qty", "PlanningUomCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event
 
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }


        private void TxtDocName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocName);
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
        }

        #endregion

    }
}
