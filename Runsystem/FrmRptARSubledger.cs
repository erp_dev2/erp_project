﻿#region Update
/*
    25/08/2021 [DITA/KIM] New Application
    01/09/2021 [DITA/KIM] Aging days dibuat ga mandatory
    20/09/2021 [DITA/KIM] Rombak reporting
    21/09/2021 [TKG/KIM] ubah query dan proses perhitungan 
    23/09/2021 [TKG/KIM] Buq pembayaran invoice setelah bln/thn yg dipilih masih keluar
    28/09/2021 [TKG/KIM] menampilkan data voucher incoming payment before start from
    29/09/2021 [TKG/KIM] menambah total semua customer, merubah query
    01/10/2021 [TKG/KIM] ar settlement before masih muncul thn sebelon start from
    11/10/2021 [TKG/KIM] tambah sales invoice tanpa journal
    21/10/2021 [TKG/KIM] merinci incoming payment dengan deposit
    03/11/2021 [TKG/KIM] memproses incoming payment dengan deposit dengan sales invoice before start date
    04/11/2021 [TKG/KIM] untuk data dari ar settlement, hanya memproses nomo rekening coa 1.1.2.1 dengan customer yg sama saja (ada kemungkinan 1.1.2.1 dengan lbh dari 1 customer)
    08/03/2022 [VIN/KIM] Process 1-> pengurang row -1 , tambah order by docdt2
    10/03/2022 [VIN/KIM] Bug lagi: Process 1-> pengurang row -1 , tambah order by docdt2
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptARSubledger : RunSystem.FrmBase6
    {
        #region Field

        public string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mCustomerAcNoAR = string.Empty, 
            mAccountingRptStartFrom = string.Empty;

        #endregion

        #region Constructor

        public FrmRptARSubledger(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                TxtTotal.EditValue = Sm.FormatNum(0m, 0);
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(Left(X1.Period, 4), '-', Right(X1.Period, 2)) As Period, ");
            SQL.AppendLine("X1.SalesInvoiceDocNo, X1.DocDt,  ");
            SQL.AppendLine("X2.CtName, X2.CtCode, X1.SalesInvoiceAmt, X1.TransType,  ");
            SQL.AppendLine("Case X1.TransType ");
            SQL.AppendLine("	When '1' Then 'Opening Balance' ");
            SQL.AppendLine("	When '2' Then ");
            SQL.AppendLine("	    Case SubType ");
            SQL.AppendLine("	        When '1' Then 'Voucher (Incoming Payment)' ");
            SQL.AppendLine("	        When '2' Then 'AR Settlement' ");
            SQL.AppendLine("	        When '3' Then 'Voucher (Incoming Payment for Invoice Without Journal)' ");
            SQL.AppendLine("	        When '4' Then 'Voucher (Incoming Payment With Deposit)' ");
            SQL.AppendLine("	    End ");
            SQL.AppendLine("	When '3' Then Concat('Voucher (Incoming Payment) Before ', @StartFrom) ");
            SQL.AppendLine("	When '4' Then Concat('AR Settlement Before ', @StartFrom) ");
            SQL.AppendLine("	When '5' Then  ");
            SQL.AppendLine("		Case SubType  ");
            //SQL.AppendLine("			When '1' Then 'Deposit (Incoming Payment)'  ");
            SQL.AppendLine("			When '1' Then 'Journal Transaction'  ");
            SQL.AppendLine("			When '2' Then 'Journal Voucher'  ");
            SQL.AppendLine("		End ");
            SQL.AppendLine("	When '6' Then 'AR Settlement Reverse' ");
            SQL.AppendLine("	When '7' Then Concat('Voucher (Incoming Payment With Deposit) Before ', @StartFrom) ");
            SQL.AppendLine("End As TransTypeDesc,  ");
            SQL.AppendLine("X1.DocNo, X1.DocDt2, X1.Amt  ");
            SQL.AppendLine("From ( ");

            // Type 1 : coa opening balance
            SQL.AppendLine("Select Concat(@StartFrom, '0101') As Period, '1' As TransType, '0' As SubType, Null As SalesInvoiceDocNo, Null As DocDt, "); 
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When C.AcType='D' Then 1.00 Else -1.00 End * B.Amt As Amt  ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And B.Amt<>0.00  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.Yr=@StartFrom  ");
            SQL.AppendLine("And A.CancelInd='N' ");

            SQL.AppendLine("Union All ");

            // Type 2 : sales invoice (voucher (incoming payment) + ar settlement + incoming payment with deposit)
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '2' As TransType, D.SubType, ");
            SQL.AppendLine("B.DocNo As SalesInvoiceDocNo, A.DocDt, B.CtCode, C.DAmt As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt2, IfNull(D.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo=B.JournalDocNo And B.JournalDocNo is Not Null And B.CancelInd='N'  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "B."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And B.CtCode=@CtCode ");
            SQL.AppendLine("Inner Join TblJournalDtl C On A.DocNo=C.DocNo And Left(C.AcNo, @AcNoLength)=@AcNo And C.DAmt>0.00 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select '1' As SubType, T2.DocNo As SalesInvoiceDocNo, T6.DocNo, T6.DocDt As DocDt2, T4.Amt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo Is Not Null ");
            if (Filter!=" ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblJournalDtl T3 On T1.DocNo=T3.DocNo And Left(T3.AcNo, @AcNoLength)=@AcNo And T3.DAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T4 On T2.DocNo=T4.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T5 On T4.DocNo=T5.DocNo And T5.CancelInd='N' And T5.Status='A' And T5.Amt>0.00 And T5.DepositAmt=0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T6 On T5.VoucherRequestDocNo=T6.VoucherRequestDocNo And T6.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T7 On T6.JournalDocNo=T7.DocNo And T7.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '2' As SubType, T2.DocNo As SalesInvoiceDocNo, T3.DocNo, T3.DocDt As DocDt2, (-1.00*T4.DAmt)+T4.CAmt As Amt "); 
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblARSHdr T3 On T2.DocNo=T3.SalesInvoiceDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalDtl T4 On T3.JournalDocNo=T4.DocNo And Left(T4.AcNo, @AcNoLength)=@AcNo  ");
            SQL.AppendLine("	    And T2.CtCode=Right(T4.AcNo, Length(T4.AcNo)-@AcNoLength) ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.DocNo=T5.DocNo And T5.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '4' As SubType, T2.DocNo As SalesInvoiceDocNo, T6.DocNo, T6.DocDt As DocDt2, T4.Amt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblJournalDtl T3 On T1.DocNo=T3.DocNo And Left(T3.AcNo, @AcNoLength)=@AcNo And T3.DAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T4 On T2.DocNo=T4.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T5 On T4.DocNo=T5.DocNo And T5.CancelInd='N' And T5.Status='A' And T5.DepositAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T6 On T5.VoucherRequestDocNo=T6.VoucherRequestDocNo And T6.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T7 On T5.JournalDepositDocNo=T7.DocNo And T7.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine(") D On B.DocNo=D.SalesInvoiceDocNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");

            SQL.AppendLine("Union All ");

            // Type 2 : sales invoice tanpa journal (voucher (incoming payment))
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '2' As TransType, B.SubType, ");
            SQL.AppendLine("A.DocNo As SalesInvoiceDocNo, A.DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("B.DocNo, B.DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select '3' As SubType, T1.DocNo As SalesInvoiceDocNo, T4.DocNo, T4.DocDt As DocDt2, T2.Amt As Amt  ");
            SQL.AppendLine("	From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status='A' And T3.Amt>0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.JournalDocNo=T5.DocNo And T5.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("    And T1.JournalDocNo Is Null ");
            SQL.AppendLine("    And T1.CancelInd='N'  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T1."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T1.CtCode=@CtCode ");
            SQL.AppendLine(") B On A.DocNo=B.SalesInvoiceDocNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.JournalDocNo Is Null ");
            SQL.AppendLine("And A.CancelInd='N' ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "A."));

            SQL.AppendLine("Union All ");

            // Type 3 : sales invoice before start date (voucher (incoming payment))
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '3' As TransType, '1' As SubType, ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt As DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr C On B.DocNo=C.DocNo And C.CancelInd='N' And C.Status='A' And C.Amt>0.00 ");
            SQL.AppendLine("Inner Join TblVoucherHdr D On C.VoucherRequestDocNo=D.VoucherRequestDocNo And D.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblJournalHdr E On D.JournalDocNo=E.DocNo And E.DocDt>=@Dt1 And E.DocDt<=@Dt2  ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //AR Settlement before start date

            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select Left(A.DocDt, 6) As Period, '4' As TransType, '1' As SubType, ");
            SQL.AppendLine("    Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("    B.DocNo, B.DocDt As DocDt2, (-1.00*C.DAmt)+C.CAmt As Amt  ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("	Inner Join TblARSHdr B On A.DocNo=B.SalesInvoiceDocNo And B.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalDtl C On B.JournalDocNo=C.DocNo And Left(C.AcNo, @AcNoLength)=@AcNo  ");
            SQL.AppendLine("	    And A.CtCode=Right(C.AcNo, Length(C.AcNo)-@AcNoLength)  ");
            SQL.AppendLine("	Inner Join TblJournalHdr D On C.DocNo=D.DocNo And D.DocDt>=@Dt1 And D.DocDt<=@Dt2  ");
            SQL.AppendLine("    Where A.CancelInd='N'  ");
            SQL.AppendLine("    And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //Incoming Payment With Deposit before start date
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '7' As TransType, '1' As SubType, ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt As DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr C On B.DocNo=C.DocNo And C.CancelInd='N' And C.Status='A' And C.DepositAmt>0.00 ");
            SQL.AppendLine("Inner Join TblJournalHdr D On C.JournalDepositDocNo=D.DocNo And D.DocDt>=@Dt1 And D.DocDt<=@Dt2  ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //AR Settlement reverse
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select Left(A.DocDt, 6) As Period, '6' As TransType, '1' As SubType, ");
            SQL.AppendLine("    Null As SalesInvoiceDocNo, Null As DocDt, Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("    A.DocNo, A.DocDt As DocDt2, (-1.00*B.DAmt)+B.CAmt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr A ");
            SQL.AppendLine("	Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("	Inner Join TblARSHdr C On A.DocNo=C.JournalDocNo And C.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr D On C.SalesInvoiceDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("	    And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)<>D.CtCode  ");
            SQL.AppendLine("    Where A.DocDt Between @Dt1 And @Dt2 ");

            SQL.AppendLine("Union All ");

            //Type 4 : journal + journal voucher
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '5' As TransType,  ");
            SQL.AppendLine("Case A.MenuCode  ");
            //SQL.AppendLine("	When '0102023003' Then '1' ");
            SQL.AppendLine("	When '0102010106' Then '1' ");
            SQL.AppendLine("	When '0102010107' Then '2' ");
            SQL.AppendLine("End As SubType,  ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt,  ");
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When B.DAmt<>0.00 Then  ");
            SQL.AppendLine("	Case When C.AcType='D' Then 1.00 Else -1.00 End * B.DAmt ");
            SQL.AppendLine("Else  ");
            SQL.AppendLine("	Case When C.AcType='D' Then -1.00 Else 1.00 End * B.CAmt ");
            SQL.AppendLine("End As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And (B.DAmt<>0.00 Or B.CAmt<>0.00) ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            //SQL.AppendLine("And A.MenuCode In ('0102023003', '0102010106', '0102010107') ");
            SQL.AppendLine("And A.MenuCode In ('0102010106', '0102010107') ");
            SQL.AppendLine(") X1, TblCustomer X2 ");
            SQL.AppendLine("Where X1.CtCode=X2.CtCode ");
            SQL.AppendLine("Order By X2.CtName, X1.CtCode, X1.TransType, X1.DocDt,  X1.SalesInvoiceDocNo, X1.SubType, X1.DocDt2;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Customer", 
                        "Invoice#", 
                        "Invoice Date", 
                        "Invoice Amount",

                        //6-10
                        "Type Code",
                        "Type",  
                        "Document#", 
                        "Document Date",
                        "Document Amount",
                       

                        //11-13
                        "Balance",
                        "AR Balance",
                        "Customer's Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 150, 120, 150, 
                        
                        //6-10
                        0, 250, 150, 100, 150,   

                        //11-13
                        150, 150, 130
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] {5, 10, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 13 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd1.Cols[13].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            TxtTotal.EditValue = Sm.FormatNum(0m, 0);
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || 
               Sm.IsLueEmpty(LueMth, "Month") ||
               Sm.IsLueEmpty(LueStartFrom, "Start From")
               )return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                
                Sm.FilterStr(ref Filter, ref cm, TxtSLIDocNo.Text, "X.DocNo", false);
                if (ChkCtCode.Checked) Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@StartFrom", Sm.GetLue(LueStartFrom));
                Sm.CmParam<String>(ref cm, "@AcNo", mCustomerAcNoAR);
                Sm.CmParam<String>(ref cm, "@AcNoLength", mCustomerAcNoAR.Length.ToString());
                Sm.CmParam<String>(ref cm, "@Dt1", string.Concat(Sm.GetLue(LueStartFrom), "0101"));
                Sm.CmParam<String>(ref cm, "@Dt2", Sm.GetValue("Select DATE_FORMAT(Date_Add(Date_Add((Select Str_To_Date(Concat('" + Sm.GetLue(LueYr) + "','" + Sm.GetLue(LueMth) + "', '01'), '%Y%m%d')), Interval 1 Month), Interval -1 Day), '%Y%m%d');"));
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        { 
                            //0
                            "Period", 

                            //1-5
                            "CtName", "SalesInvoiceDocNo", "DocDt", "SalesInvoiceAmt", "TransType",

                            //6-10
                             "TransTypeDesc", "DocNo", "DocDt2", "Amt", "CtCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 11, 12 });
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                Process1();
                Process2();
                Process3();
                Process4();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 5, 10 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'CustomerAcNoAR', 'AccountingRptStartFrom' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "CustomerAcNoAR": mCustomerAcNoAR = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process1()
        {
            var SalesInvoiceDocNo = string.Empty;
            var Amt = 0m;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 6) == "2" && Sm.CompareStr(SalesInvoiceDocNo, Sm.GetGrdStr(Grd1, i, 3)))
                    Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i - 1, 5) - Amt;
                SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, i, 3);
                Amt = Sm.GetGrdDec(Grd1, i, 10);
            }
        }

        private void Process2()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 6) == "1") //Opening Balance
                    Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 10);
                else
                {
                    if (Sm.GetGrdStr(Grd1, i, 6) == "3" || Sm.GetGrdStr(Grd1, i, 6) == "4" || Sm.GetGrdStr(Grd1, i, 6) == "6" || Sm.GetGrdStr(Grd1, i, 6) == "7")
                        Grd1.Cells[i, 11].Value = 0m;
                    else
                    {
                        if (Sm.GetGrdStr(Grd1, i, 6) == "5")
                            Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 10);
                        else
                            Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 5) - Sm.GetGrdDec(Grd1, i, 10);
                    }
                }
            }
        }

        private void Process3()
        {
            string CtCode = string.Empty, TransType = string.Empty, SalesInvoiceDocNo = string.Empty;
            var ARBalance = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                TransType = Sm.GetGrdStr(Grd1, i, 6);
                if (Sm.CompareStr(CtCode, Sm.GetGrdStr(Grd1, i, 13)))
                {
                    if (TransType == "1")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "2")
                    {
                        if (Sm.CompareStr(SalesInvoiceDocNo, Sm.GetGrdStr(Grd1, i, 3)))
                            ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                        else
                            ARBalance += Sm.GetGrdDec(Grd1, i, 11);
                    }
                    if (TransType == "3" || TransType == "4" || TransType == "6" || TransType == "7")
                        ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "5")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                }
                else
                {
                    ARBalance = 0m;
                    if (TransType == "1")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "2")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 11);
                    if (TransType == "3" || TransType == "4" || TransType == "6" || TransType == "7")
                        ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "5")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                }
                Grd1.Cells[i, 12].Value = ARBalance;
                CtCode = Sm.GetGrdStr(Grd1, i, 13);
                SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, i, 3);
            }
        }

        private void Process4()
        {
            var CtCode = string.Empty;
            decimal Amt = 0m, Total = 0m;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (!Sm.CompareStr(CtCode, Sm.GetGrdStr(Grd1, i, 13)))
                {
                    Total += Amt;
                }
                if (Sm.GetGrdStr(Grd1, i, 13).Length > 0)
                {
                    CtCode = Sm.GetGrdStr(Grd1, i, 13);
                    Amt = Sm.GetGrdDec(Grd1, i, 12);
                }
            }
            Total += Amt;
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtSLIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSLIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Sales Invoice#");
        }

        #endregion

        #region untuk testing aja

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            var CtCode = string.Empty;
            decimal Amt = 0m, Total = 0m, Min = 0m, Max = 0m;

            if (TxtMin.Text.Length>0)
                Min = decimal.Parse(TxtMin.Text);

            if (TxtMax.Text.Length>0)
                Max = decimal.Parse(TxtMax.Text);

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 13).Length > 0)
                {
                    if (decimal.Parse(Sm.GetGrdStr(Grd1, i, 13)) >= Min && decimal.Parse(Sm.GetGrdStr(Grd1, i, 13)) <= Max)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 13) != CtCode)
                            Amt += Total;

                        Total = Sm.GetGrdDec(Grd1, i, 12);
                        CtCode = Sm.GetGrdStr(Grd1, i, 13);
                    }
                }
                else
                    Amt += Total;
            }
            
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion
    }
}
