﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaintenanceScheduleDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaintenanceSchedule mFrmParent;
        private string mSQL = string.Empty;
        private string mLocCode = string.Empty;

        #endregion

        #region Constructor

        public FrmMaintenanceScheduleDlg(FrmMaintenanceSchedule FrmParent, string LocCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mLocCode = LocCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                //Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.AssetCode, C.AssetName, B.EquipmentCode, D.EquipmentName, ifnull(E.HoursMeter, 0) As HoursMeter ");
            SQL.AppendLine("From TblTOHdr A ");
            SQL.AppendLine("Inner Join TblTODtl B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Inner Join TblAsset C On A.AssetCode=C.AssetCode ");
            SQL.AppendLine("Inner Join TblEquipment D On B.EquipmentCode=D.EquipmentCode ");
            SQL.AppendLine("Left Join TblHoursMeter E On A.HmDOcNo = E.DocNo");
            SQL.AppendLine("Where A.LocCode=@LocCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        //1-2
                        "",
                        "Asset Code", 
                        "Asset Name", 
                        "Equipment Code", 
                        "Equipment Name",
                        //6
                        "Hours Meter"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 120, 200, 150, 250,
                        //6
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 4, 5, 6  });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "C.AssetName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By C.AssetName, D.EquipmentName;",
                        new string[] 
                        { 
                            "AssetCode", 
                            "AssetName", "EquipmentCode", "EquipmentName", "HoursMeter"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 7].Value = 0;
                    }
                }
            }
        }

        private bool IsEquipmentCodeAlreadyChosen(int Row)
        {
            //string EquipmentCode = Sm.GetGrdStr(Grd1, Row, 2);
            //for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            //    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), EquipmentCode))
            //        return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }
        #endregion
    }
}
