﻿#region Update
/*
    20/11/2020 [WED/IMS] new apps
    20/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    29/01/2021 [WED/IMS] tambah informasi ProgressInd, ServiceInd, SOContract#, CustomerPO#, Project Code, Project Name
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt8Find : RunSystem.FrmBase2
    {
        #region Field
        private iGRichTextManager fManager;
        private FrmDOCt8 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;
        internal bool
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmDOCt8Find(FrmDOCt8 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueItCtCode(ref LueItCtCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("B.CancelInd, A.DocNoInternal, C.WhsName, E.CtName, ");
            SQL.AppendLine("B.ItCode, D.ItCodeInternal, D.ItName, D.ForeignName, D.Specification, D.ItGrpCode, ");
            SQL.AppendLine("F.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPackagingUnit, B.PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, A.CurCode, ");
            SQL.AppendLine("A.ServiceInd, A.ProgressInd, A.SOContractDocNo, G.ProjectCode, G.ProjectName, G.PONo, ");
            SQL.AppendLine("If(A.ProgressInd = 'Y', 0.00, B.UPrice) As UPrice, A.Remark, B.Remark RemarkDtl, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Left Join TblProperty F On B.PropCode=F.PropCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select T1.DocNo, T1.PONo, T4.ProjectCode, T4.ProjectName ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblBOQHdr T2 On T1.BOQDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo In  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct SOContractDocNo ");
            SQL.AppendLine("            From TblDOCtHdr ");
            SQL.AppendLine("            Where SOContractDocNo Is Not Null ");
            SQL.AppendLine("            And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblLOPHdr T3 On T2.LOPDocNo = T3.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup T4 On T3.PGCode = T4.PGCode ");
            SQL.AppendLine(") G On A.SOContractDocNo = G.DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 44;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Cancel", 
                        "Local"+Environment.NewLine+"Document#",

                        //6-10
                        "Warehouse",
                        "Customer",
                        "Item's Code",
                        "Local Code",
                        "Item's Name",

                        //11-15
                        "Foreign Name",
                        "Specification",
                        "Group",
                        "Property",
                        "Batch#",

                        //16-20
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity"+Environment.NewLine+"(Packaging)",
                        "UoM"+Environment.NewLine+"(Packaging)",

                        //21-25
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //26-30
                        "UoM",
                        "Currency",
                        "Service",
                        "Progress",
                        "SO Contract#",

                        //31-35
                        "Project Code",
                        "Project Name",
                        "Customer PO#",
                        "Unit Price",
                        "Total",

                        //36-40
                        "Remark",
                        "Remark Detail",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 

                        //41-43
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 50, 130, 
                        
                        //6-10
                        200, 200, 100, 100, 200, 
                        
                        //11-15
                        150, 300, 150, 150, 180, 
                        
                        //16-20
                        150, 100, 100, 100, 100, 
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-30
                        100, 80, 60, 60, 140, 
                        
                        //31-35
                        120, 400, 120, 150, 150, 
                        
                        //36-40
                        200, 200, 100, 100, 100, 
                        
                        //41-43
                        100, 100, 100
                    }
                );

            fManager = new iGRichTextManager(Grd1);
            //Grd1.Cols[43].DefaultCellValue = @"{\rtf1\ansi\ansicpg1251\deff0\deflang1049{\fonttbl{\f0\fswiss\fcharset0 Lucida Console;}}{\colortbl ;\red255\green0\blue0;}{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1\pard\lang1033\f0\fs20 This \i is\i0  a \b rich \cf1\b0 text \cf0\ul format\ulnone .\lang1049\f1\par}";
            Sm.GrdColCheck(Grd1, new int[] { 4, 28, 29 });
            Sm.GrdFormatDec(Grd1, new int[] { 19, 21, 23, 25, 34, 35 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 39, 42 });
            Sm.GrdFormatTime(Grd1, new int[] { 40, 43 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 13, 14, 16, 17, 18, 23, 24, 25, 26, 38, 39, 40, 41, 42, 43 }, false);
            if (!mFrmParent.mIsDOCtApprovalActived) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            if (!mFrmParent.mIsShowLocalDocNo) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mFrmParent.mIsItGrpCodeShow) Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            if (!mFrmParent.mIsDOCtShowPackagingUnit) Sm.GrdColInvisible(Grd1, new int[] { 19, 20 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            if (!mFrmParent.mIsDOCtShowRemarkDetail) Sm.GrdColInvisible(Grd1, new int[] { 37 });
            ShowInventoryUomCode();
            
            Grd1.Cols[4].CellStyle.ImageAlign = iGContentAlignment.TopCenter;
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 13, 14, 16, 17, 18, 38, 39, 40, 41, 42, 43 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 25, 26 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And (B.CancelInd='N' Or A.Status='C') ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.DocNoInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItCodeInternal", "D.ItName", "D.Specification" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "DocNoInternal", "WhsName", 
                            
                            //6-10
                            "CtName", "ItCode", "ItCodeInternal", "ItName", "ForeignName", 
                            
                            //11-15
                            "Specification", "ItGrpCode", "PropName", "BatchNo", "Source", 
                            
                            //16-20
                            "Lot", "Bin", "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", 

                            //21-25
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 

                            //26-30
                            "CurCode", "ServiceInd", "ProgressInd", "SOContractDocNo", "ProjectCode", 
                            
                            //31-35
                            "ProjectName", "PONo", "UPrice", "Remark", "RemarkDtl", 
                            
                            //36-39
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 35].Value = decimal.Truncate(Sm.GetGrdDec(Grd1, Row, 21) * Sm.GetGrdDec(Grd1, Row, 34));
                            else
                                Grd.Cells[Row, 35].Value = Sm.GetGrdDec(Grd1, Row, 21) * Sm.GetGrdDec(Grd1, Row, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 36);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 39, 37);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 40, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 38);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 42, 39);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 43, 39);
                        }, true, false, false, false
                    );
                if (mFrmParent.mIsDOCtShowRemarkDetail) Grd1.Rows.AutoHeight();
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 19, 21, 23, 25, 34, 35 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void GetParameter()
        {
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
