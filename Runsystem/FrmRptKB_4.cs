﻿#region Update
/*
    20/03/2019 [WED/MAI] new reporting
    27/03/2019 [WED/MAI] tambah aggregate function di query nya
    01/04/2019 [WED/MAI] Saldo Akhir = Saldo Awal + Pemasukkan - Pengeluaran
                     Selisih = Saldo Akhir - Stock Opname
    01/04/2019 [WED/MAI] Uraian barang ambil dari ItGrpName
    01/04/2019 [WED/MAI] tambah checkbox LueType
    15/04/2019 [WED/MAI] dibedakan antara BC27 dan BC40
    02/05/2019 [WED/MAI] tambah nominal Saldo awal, diambil dari transaksi KBOpeningBalance
    17/06/2019 [DITA/MAI] Bug saat refresh Laporan Mutasi
    03/07/2019 [DITA/MAI] Printout Laporan Mutasi
    11/07/2019 [WED/MAI] BC25 ditambah dari DOCt2 yang CustomsDocCode = '25'
    24/10/2019 [DITA/MAI] Ubah data, ambil dari stock movement
    01/11/2019 [WED/MAI] Saldo awal diambil dari stock movement
    01/11/2019 [WED/MAI] nominal pemasukkan dan pengeluaran melihat ke gudang (tidak melihat ke BC)
    18/11/2019 [WED/MAI] melihat gudang saja
    25/11/2019 [WED/MAI] tidak melihat kolom KBRegistrationNo
    29/11/2019 [WED/MAI] uom ada yg tidak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
using DevExpress.XtraEditors;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_4 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mStartDt = string.Empty,
            mEndDt = string.Empty,
            mItCtCodeForRawMaterial = string.Empty,
            mItCtCodeForWIP = string.Empty,
            mItCtCodeForFinishedGood = string.Empty,
            mItCtCodeForScrap = string.Empty,
            mItCtCodeForSparepartMachine = string.Empty,
            mCustomsDocCodeForRawMaterialQtyIn = string.Empty,
            mCustomsDocCodeForRawMaterialQtyOut = string.Empty,
            mCustomsDocCodeForScrapQtyIn = string.Empty,
            mCustomsDocCodeForScrapQtyOut = string.Empty,
            mCustomsDocCodeForFinishedGoodQtyIn = string.Empty,
            mCustomsDocCodeForFinishedGoodQtyOut = string.Empty,
            mCustomsDocCodeForWIPQtyIn = string.Empty,
            mCustomsDocCodeForWIPQtyOut = string.Empty,
            mCustomsDocCodeForSparepartMachineQtyIn = string.Empty,
            mCustomsDocCodeForSparepartMachineQtyOut= string.Empty,
            mDocTypeForRawMaterialQtyIn= string.Empty,
            mDocTypeForRawMaterialQtyOut= string.Empty,
            mDocTypeForScrapQtyIn= string.Empty,
            mDocTypeForScrapQtyOut= string.Empty,
            mDocTypeForFinishedGoodQtyIn= string.Empty,
            mDocTypeForFinishedGoodQtyOut= string.Empty,
            mDocTypeForWIPQtyIn= string.Empty,
            mDocTypeForWIPQtyOut= string.Empty,
            mDocTypeForSparepartMachineQtyIn = string.Empty,
            mDocTypeForSparepartMachineQtyOut= string.Empty,
            mWhsCodeForRawMaterial = string.Empty,
            mWhsCodeForScrap = string.Empty,
            mWhsCodeForFinishedGood = string.Empty,
            mWhsCodeForWIP = string.Empty,
            mWhsCodeForSparepartMachine = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmRptKB_4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetLueType(ref LueType);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQLOB()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT X.DocType, X.ItGrpCode, SUM(X.Qty) Qty ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT 'Raw Material' As DocType, B.ItGrpCode, A.Qty  ");
	        SQL.AppendLine("    FROM tblstockmovement A ");
	        SQL.AppendLine("    INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
	        SQL.AppendLine("        AND FIND_IN_SET(B.ItCtCode, @ItCtCodeForRawMaterial) ");
	        //SQL.AppendLine("        AND A.KBRegistrationNo IS NOT NULL ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
	        SQL.AppendLine("        AND FIND_IN_SET(A.WhsCode, @WhsCodeForRawMaterial) ");
	        //SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("        AND (A.DocDt <= @DocDt1) ");
	        SQL.AppendLine("        AND A.Qty <> 0.00 ");
	        SQL.AppendLine("    UNION ALL ");
	        SQL.AppendLine("    SELECT 'Scrap' As DocType, B.ItGrpCode, A.Qty  ");
	        SQL.AppendLine("    FROM tblstockmovement A ");
	        SQL.AppendLine("    INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
	        SQL.AppendLine("        AND FIND_IN_SET(B.ItCtCode, @ItCtCodeForScrap) ");
	        //SQL.AppendLine("        AND A.KBRegistrationNo IS NOT NULL ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
	        SQL.AppendLine("        AND FIND_IN_SET(A.WhsCode, @WhsCodeForScrap) ");
	        //SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("        AND (A.DocDt <= @DocDt1) ");
	        SQL.AppendLine("        AND A.Qty <> 0.00 ");
	        SQL.AppendLine("    UNION ALL ");
	        SQL.AppendLine("    SELECT 'Finish Good' As DocType, B.ItGrpCode, A.Qty  ");
	        SQL.AppendLine("    FROM tblstockmovement A ");
	        SQL.AppendLine("    INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
	        SQL.AppendLine("        AND FIND_IN_SET(B.ItCtCode, @ItCtCodeForFinishedGood) ");
	        //SQL.AppendLine("        AND A.KBRegistrationNo IS NOT NULL ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
	        SQL.AppendLine("        AND FIND_IN_SET(A.WhsCode, @WhsCodeForFinishedGood) ");
	        //SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("        AND (A.DocDt <= @DocDt1) ");
	        SQL.AppendLine("        AND A.Qty <> 0.00 ");
	        SQL.AppendLine("    UNION ALL ");
	        SQL.AppendLine("    SELECT 'WIP' As DocType, B.ItGrpCode, A.Qty  ");
	        SQL.AppendLine("    FROM tblstockmovement A ");
	        SQL.AppendLine("    INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
	        SQL.AppendLine("        AND FIND_IN_SET(B.ItCtCode, @ItCtCodeForWIP) ");
	        //SQL.AppendLine("        AND A.KBRegistrationNo IS NOT NULL ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
	        SQL.AppendLine("        AND FIND_IN_SET(A.WhsCode, @WhsCodeForWIP) ");
	        //SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("        AND (A.DocDt <= @DocDt1) ");
	        SQL.AppendLine("        AND A.Qty <> 0.00 ");
	        SQL.AppendLine("    UNION ALL ");
	        SQL.AppendLine("    SELECT 'Machine Sparepart' As DocType, B.ItGrpCode, A.Qty  ");
	        SQL.AppendLine("    FROM tblstockmovement A ");
	        SQL.AppendLine("    INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine("        AND FIND_IN_SET(B.ItCtCode, @ItCtCodeForSparepartMachine) ");
	        //SQL.AppendLine("        AND A.KBRegistrationNo IS NOT NULL ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("        AND FIND_IN_SET(A.WhsCode, @WhsCodeForSparepartMachine) ");
	        //SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("        AND (A.DocDt <= @DocDt1) ");
	        SQL.AppendLine("        AND A.Qty <> 0.00 ");
            SQL.AppendLine(") X ");

            return SQL.ToString();
        }

        private string PrepItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType, T.ItGrpCode, T2.ItGrpName, T.Uom ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct B.ItGrpCode, 'Raw Material' As DocType, B.InventoryUomCode Uom ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForRawMaterial) ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForRawMaterial) ");
            SQL.AppendLine("    And A.DocDt <= @DocDt1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct B.ItGrpCode, 'Scrap' As DocType, B.InventoryUomCode Uom ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForScrap) ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForScrap) ");
            SQL.AppendLine("    And A.DocDt <= @DocDt1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct B.ItGrpCode, 'Finish Good' As DocType, B.InventoryUomCode Uom ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForFinishedGood) ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForFinishedGood) ");
            SQL.AppendLine("    And A.DocDt <= @DocDt1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct B.ItGrpCode, 'WIP' As DocType, B.InventoryUomCode Uom ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForWIP) ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForWIP) ");
            SQL.AppendLine("    And A.DocDt <= @DocDt1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct B.ItGrpCode, 'Machine Sparepart' As DocType, B.InventoryUomCode Uom ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForSparepartMachine) ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForSparepartMachine) ");
            SQL.AppendLine("    And A.DocDt <= @DocDt1 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblItemGroup T2 On T.ItGrpCode = T2.ItGrpCode ");

            return SQL.ToString();
        }

        private string GetSQL(string Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.DocType, X.ItGrpCode, X2.ItGrpName As ItName, X.Uom, Sum(X.QtyIn) QtyIn, ");
            SQL.AppendLine("Sum(X.QtyOut) QtyOut ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select 'Raw Material' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, A.Qty QtyIn, ");
            SQL.AppendLine("    0 As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForRawMaterial) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForRawMaterialQtyIn) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForRawMaterial) And A.Qty > 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForRawMaterialQtyIn) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine(" Union All ");
            SQL.AppendLine("    Select 'Raw Material' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, 0 QtyIn, ");
            SQL.AppendLine("    A.Qty As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForRawMaterial) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForRawMaterialQtyOut) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForRawMaterial) And A.Qty < 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForRawMaterialQtyOut) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Scrap' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, A.Qty QtyIn, ");
            SQL.AppendLine("    0 As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForScrap) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForScrapQtyIn) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForScrap) And A.Qty > 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForScrapQtyIn) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Scrap' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, 0 QtyIn, ");
            SQL.AppendLine("    A.Qty As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForScrap) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForScrapQtyOut) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForScrap) And A.Qty < 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForScrapQtyOut) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Finish Good' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, A.Qty QtyIn, ");
            SQL.AppendLine("    0 As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForFinishedGood) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForFinishedGoodQtyIn) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForFinishedGood) And A.Qty > 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForFinishedGoodQtyIn) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Finish Good' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, 0 QtyIn, ");
            SQL.AppendLine("    A.Qty As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForFinishedGood) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForFinishedGoodQtyOut) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForFinishedGood) And A.Qty < 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForFinishedGoodQtyOut) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'WIP' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, A.Qty QtyIn, ");
            SQL.AppendLine("    0 As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForWIP) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForWIPQtyIn) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForWIP) And A.Qty > 0 ");
	        SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForWIPQtyIn) ");
	        SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'WIP' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, 0 QtyIn, ");
            SQL.AppendLine("    A.Qty As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForWIP) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForWIPQtyOut) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForWIP) And A.Qty < 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForWIPQtyOut) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Machine Sparepart' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, A.Qty QtyIn, ");
            SQL.AppendLine("    0 As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForSparepartMachine) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForSparepartMachineQtyIn) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForSparepartMachine) And A.Qty > 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForSparepartMachineQtyIn) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select 'Machine Sparepart' As DocType, B.ItGrpCode, B.InventoryUomCode Uom, 0 QtyIn, ");
            SQL.AppendLine("    A.Qty As QtyOut, A.KBRegistrationNo, A.KBContractNo ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(B.ItCtCode, @ItCtCodeForSparepartMachine) ");
            //SQL.AppendLine("    And Find_In_Set(A.CustomsDocCode, @CustomsDocCodeForSparepartMachineQtyOut) ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCodeForSparepartMachine) And A.Qty < 0 ");
            SQL.AppendLine("    And Find_In_Set(A.DocType, @DocTypeForSparepartMachineQtyOut) ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBContractNo Like @KBContractNo ");
            if (TxtRegistrationNo.Text.Length > 0)
                SQL.AppendLine("    And A.KBRegistrationNo Like @KBRegistrationNo ");
            SQL.AppendLine(") X ");
            SQL.AppendLine("Left Join TblItemGroup X2 On X.ItGrpCode = X2.ItGrpCode ");
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Type",
                    "Kode Barang", 
                    "Uraian Barang",
                    "Satuan",
                    "Saldo Awal",

                    //6-10
                    "Pemasukkan",
                    "Pengeluaran",
                    "Penyesuaian",
                    "Saldo Akhir",
                    "Stok Opname",

                    //11-12
                    "Selisih",
                    "Keterangan"                  
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 120, 300, 100, 150, 

                    //6-10
                    120, 120, 120, 120, 120, 

                    //11-12
                    120, 180
                }
            );

            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 11 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var l = new List<KB_4>();
                var l2 = new List<KBOpeningBalance>();

                string DocNoKBOpeningBalance = GetDocNoKBOpeningBalance();
                string mStartDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
                string mEndDt1 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
                string mStartDt2 = string.Empty;
                string mEndDt2 = string.Empty;
                if (DocNoKBOpeningBalance.Length > 0)
                {
                    mStartDt2 = Sm.GetValue("Select Concat(Yr, Mth, '01') From TblKBOpeningBalanceHdr Where DocNo = @Param; ", DocNoKBOpeningBalance);
                    mEndDt2 = Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(-1).ToString("yyyyMMdd");
                }

                Process0(ref l, mStartDt1);
                Process1(ref l, mStartDt1, mEndDt1);
                if (l.Count > 0)
                {
                    //if (DocNoKBOpeningBalance.Length > 0)
                    //{
                    //    Process2(ref l2, DocNoKBOpeningBalance);
                    //    if (l2.Count > 0)
                    //    {
                    //        Process2_1(ref l2, mStartDt2, mEndDt2);
                    //        Process3(ref l, ref l2);
                    //    }
                    //}
                    //Process3_1(ref l);
                    Process2_2(ref l2, mStartDt1);
                    if (l2.Count > 0)
                    {
                        Process3(ref l, ref l2);
                    }
                    Process3_1(ref l);
                    Process4(ref l);
                }
                else Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 11 });

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0 || mStartDt.Length == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                ParPrint();
                //iGSubtotalManager.ForeColor = Color.Black;

                //string StartDt = string.Concat(Sm.Right(mStartDt, 2), "/", mStartDt.Substring(4, 2), "/", Sm.Left(mStartDt, 4));
                //string EndDt = string.Concat(Sm.Right(mEndDt, 2), "/", mEndDt.Substring(4, 2), "/", Sm.Left(mEndDt, 4));
                //string mTitle = string.Empty;

                //if (Sm.GetLue(LueType) == "BC23")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC25")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC40" || Sm.GetLue(LueType) == "BC27")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC41")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //PM1.PageHeader.MiddleSection.Text = mTitle;

                //PM1.PageHeader.LeftSection.Text = Environment.NewLine + string.Concat("Periode : ", StartDt, " s/d ", EndDt); ;
                //Sm.SetPrintManagerProperty(PM1, Grd1, ChkAutoWidth.Checked);
                //iGSubtotalManager.ForeColor = Color.White;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<KB_4Hdr>();
            var ldtl = new List<KB_4>();

            string[] TableName = { "KB_4Hdr", "KB_4" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            //  DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            string mTitle = "LAPORAN MUTASI";
            if (Sm.GetLue(LueType) == "Raw Material") mTitle = "LAPORAN PERTANGGUNGJAWABAN MUTASI BAHAN BAKU DAN BAHAN PENOLONG ";
            if (Sm.GetLue(LueType) == "Scrap") mTitle = "LAPORAN PERTANGGUNGJAWABAN MUTASI BARANG SISA DAN SCRAP  ";
            if (Sm.GetLue(LueType) == "Finish Good") mTitle = "LAPORAN PERTANGGUNGJAWABAN MUTASI BARANG JADI  ";
            if (Sm.GetLue(LueType) == "WIP") mTitle = "LAPORAN POSISI BARANG DALAM PROSES (WIP)  ";
            if (Sm.GetLue(LueType) == "Machine Sparepart") mTitle = "LAPORAN PERTANGGUNGJAWABAN MUTASI MESIN DAN PERALATAN PERKANTORAN  ";

            l.Add(new KB_4Hdr()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
                Periode1 = DteDocDt1.Text.Replace("/", "-"),
                Periode2 = DteDocDt2.Text.Replace("/", "-"),
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                Title = mTitle,

            });

            myLists.Add(l);

            #endregion

            #region Detail

            int nomor = 0;
            
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new KB_4()
                {
                    No = nomor,
                    ItGrpCode = Sm.GetGrdStr(Grd1, i, 1),
                    ItName = Sm.GetGrdStr(Grd1, i, 2),
                    UoM = Sm.GetGrdText(Grd1, i, 3),
                    QtyOpeningBalance = Sm.GetGrdDec(Grd1, i, 4),
                    QtyIn = Sm.GetGrdDec(Grd1, i, 5),
                    QtyOut = Sm.GetGrdDec(Grd1, i, 6),
                    QtyStockAdjustment = Sm.GetGrdDec(Grd1, i, 7),
                    QtyClosingBalance = Sm.GetGrdDec(Grd1, i, 8),
                    QtyStockOpname = Sm.GetGrdStr(Grd1, i, 9),
                    QtyBalance = Sm.GetGrdDec(Grd1, i, 10),
                    Remark = Sm.GetGrdStr(Grd1, i, 11),
                   
                });

            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("KB_4", myLists, TableName, false);

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string GetDocNoKBOpeningBalance()
        {
            var SQL = new StringBuilder();
            string mYr = Sm.Left(Sm.GetDte(DteDocDt1), 4);
            string mDt = Sm.GetDte(DteDocDt1).Substring(4, 2);

            SQL.AppendLine("SELECT DocNo ");
            SQL.AppendLine("FROM TblKBOpeningBalanceHdr ");
            SQL.AppendLine("WHERE Mth IS NOT NULL ");
            SQL.AppendLine("And Yr Is Not Null ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("AND CONCAT(Yr, Mth) <= CONCAT(@Param1, @Param2) ");
            SQL.AppendLine("ORDER BY CONCAT(Yr, Mth) DESC ");
            SQL.AppendLine("LIMIT 1 ");

            return Sm.GetValue(SQL.ToString(), mYr, mDt, string.Empty);
        }

        private void GetParameter()
        {
            mItCtCodeForRawMaterial = Sm.GetParameter("ItCtCodeForRawMaterial");
            mItCtCodeForWIP = Sm.GetParameter("ItCtCodeForWIP");
            mItCtCodeForFinishedGood = Sm.GetParameter("ItCtCodeForFinishedGood");
            mItCtCodeForScrap = Sm.GetParameter("ItCtCodeForScrap");
            mItCtCodeForSparepartMachine = Sm.GetParameter("ItCtCodeForSparepartMachine");
            mCustomsDocCodeForRawMaterialQtyIn  = Sm.GetParameter("CustomsDocCodeForRawMaterialQtyIn");
            mCustomsDocCodeForRawMaterialQtyOut = Sm.GetParameter("CustomsDocCodeForRawMaterialQtyOut");
            mCustomsDocCodeForScrapQtyIn = Sm.GetParameter("CustomsDocCodeForScrapQtyIn");
            mCustomsDocCodeForScrapQtyOut = Sm.GetParameter("CustomsDocCodeForScrapQtyOut");
            mCustomsDocCodeForFinishedGoodQtyIn = Sm.GetParameter("CustomsDocCodeForFinishedGoodQtyIn");
            mCustomsDocCodeForFinishedGoodQtyOut = Sm.GetParameter("CustomsDocCodeForFinishedGoodQtyOut");
            mCustomsDocCodeForWIPQtyIn = Sm.GetParameter("CustomsDocCodeForWIPQtyIn");
            mCustomsDocCodeForWIPQtyOut = Sm.GetParameter("CustomsDocCodeForWIPQtyOut");
            mCustomsDocCodeForSparepartMachineQtyIn = Sm.GetParameter("CustomsDocCodeForSparepartMachineQtyIn");
            mCustomsDocCodeForSparepartMachineQtyOut = Sm.GetParameter("CustomsDocCodeForSparepartMachineQtyOut");
            mDocTypeForRawMaterialQtyIn = Sm.GetParameter("DocTypeForRawMaterialQtyIn");
            mDocTypeForRawMaterialQtyOut = Sm.GetParameter("DocTypeForRawMaterialQtyOut");
            mDocTypeForScrapQtyIn = Sm.GetParameter("DocTypeForScrapQtyIn");
            mDocTypeForScrapQtyOut = Sm.GetParameter("DocTypeForScrapQtyOut");
            mDocTypeForFinishedGoodQtyIn = Sm.GetParameter("DocTypeForFinishedGoodQtyIn");
            mDocTypeForFinishedGoodQtyOut = Sm.GetParameter("DocTypeForFinishedGoodQtyOut");
            mDocTypeForWIPQtyIn = Sm.GetParameter("DocTypeForWIPQtyIn");
            mDocTypeForWIPQtyOut = Sm.GetParameter("DocTypeForWIPQtyOut");
            mDocTypeForSparepartMachineQtyIn = Sm.GetParameter("DocTypeForSparepartMachineQtyIn");
            mDocTypeForSparepartMachineQtyOut = Sm.GetParameter("DocTypeForSparepartMachineQtyOut");
            mWhsCodeForRawMaterial = Sm.GetParameter("WhsCodeForRawMaterial");
            mWhsCodeForScrap = Sm.GetParameter("WhsCodeForScrap");
            mWhsCodeForFinishedGood = Sm.GetParameter("WhsCodeForFinishedGood");
            mWhsCodeForWIP = Sm.GetParameter("WhsCodeForWIP");
            mWhsCodeForSparepartMachine = Sm.GetParameter("WhsCodeForSparepartMachine");
        }

        private string MonthNameIDN(string Mth)
        {
            string[] mMonthNames = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Novemeber", "Desember" };
            return mMonthNames[Int32.Parse(Mth) - 1];
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Raw Material' As Col1, 'Raw Material' As Col2 ");
            SQL.AppendLine("Union All Select 'Scrap' Col1, 'Scrap' Col2 ");
            SQL.AppendLine("Union All Select 'Finish Good' Col1, 'Finish Good' Col2 ");
            SQL.AppendLine("Union All Select 'WIP' Col1, 'WIP' Col2 ");
            SQL.AppendLine("Union All Select 'Machine Sparepart' Col1, 'Machine Sparepart' Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process0(ref List<KB_4> l, string StartDt)
        {
            string Filter = " Where 0 = 0 ";

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", StartDt);
            Sm.CmParam<String>(ref cm, "@WhsCodeForRawMaterial", mWhsCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@WhsCodeForScrap", mWhsCodeForScrap);
            Sm.CmParam<String>(ref cm, "@WhsCodeForFinishedGood", mWhsCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@WhsCodeForWIP", mWhsCodeForWIP);
            Sm.CmParam<String>(ref cm, "@WhsCodeForSparepartMachine", mWhsCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForRawMaterial", mItCtCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForWIP", mItCtCodeForWIP);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForFinishedGood", mItCtCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForScrap", mItCtCodeForScrap);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForSparepartMachine", mItCtCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@KBContractNo", string.Concat("%", TxtDocNo.Text, "%"));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", string.Concat("%", TxtRegistrationNo.Text, "%"));

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "T.DocType", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = PrepItem() + Filter + " Group By T.DocType, T.ItGrpCode, T2.ItGrpName; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocType", 

                    //1-5
                    "ItGrpCode", "ItGrpName", "Uom"

                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_4()
                        {
                            Type = Sm.DrStr(dr, c[0]),
                            ItGrpCode = Sm.DrStr(dr, c[1]).Trim(),
                            ItName = Sm.DrStr(dr, c[2]),
                            UoM = Sm.DrStr(dr, c[3]),
                            QtyIn = 0m,
                            QtyOut = 0m,
                            QtyBalance = 0m,
                            QtyClosingBalance = 0m,
                            QtyOpeningBalance = 0m,
                            QtyStockAdjustment = 0m,
                            QtyStockOpname = string.Empty,
                            Remark = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1(ref List<KB_4> l, string StartDt, string EndDt)
        {
            string Filter = " Where 0 = 0 ";

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", StartDt);
            Sm.CmParamDt(ref cm, "@DocDt2", EndDt);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForRawMaterial", mItCtCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForWIP", mItCtCodeForWIP);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForFinishedGood", mItCtCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForScrap", mItCtCodeForScrap);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForSparepartMachine", mItCtCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForRawMaterialQtyIn", mCustomsDocCodeForRawMaterialQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForRawMaterialQtyOut", mCustomsDocCodeForRawMaterialQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForScrapQtyIn", mCustomsDocCodeForScrapQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForScrapQtyOut", mCustomsDocCodeForScrapQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForFinishedGoodQtyIn", mCustomsDocCodeForFinishedGoodQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForFinishedGoodQtyOut", mCustomsDocCodeForFinishedGoodQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForWIPQtyIn", mCustomsDocCodeForWIPQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForWIPQtyOut", mCustomsDocCodeForWIPQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForSparepartMachineQtyIn", mCustomsDocCodeForSparepartMachineQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForSparepartMachineQtyOut", mCustomsDocCodeForSparepartMachineQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForRawMaterialQtyIn", mDocTypeForRawMaterialQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForRawMaterialQtyOut", mDocTypeForRawMaterialQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForScrapQtyIn", mDocTypeForScrapQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForScrapQtyOut", mDocTypeForScrapQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForFinishedGoodQtyIn", mDocTypeForFinishedGoodQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForFinishedGoodQtyOut", mDocTypeForFinishedGoodQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForWIPQtyIn", mDocTypeForWIPQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForWIPQtyOut", mDocTypeForWIPQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForSparepartMachineQtyIn", mDocTypeForSparepartMachineQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForSparepartMachineQtyOut", mDocTypeForSparepartMachineQtyOut);
            Sm.CmParam<String>(ref cm, "@WhsCodeForRawMaterial", mWhsCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@WhsCodeForScrap", mWhsCodeForScrap);
            Sm.CmParam<String>(ref cm, "@WhsCodeForFinishedGood", mWhsCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@WhsCodeForWIP", mWhsCodeForWIP);
            Sm.CmParam<String>(ref cm, "@WhsCodeForSparepartMachine", mWhsCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@KBContractNo", string.Concat("%", TxtDocNo.Text, "%"));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", string.Concat("%", TxtRegistrationNo.Text, "%"));
            
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "X.DocType", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQL(Sm.GetLue(LueType)) + Filter + " Group By X.DocType, X.ItGrpCode; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocType", 

                    //1-5
                    "ItGrpCode", "ItName", "Uom", "QtyIn", "QtyOut", 

                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (int i = 0; i < l.Count; ++i)
                        {
                            if (l[i].Type == Sm.DrStr(dr, c[0]) && l[i].ItGrpCode == Sm.DrStr(dr, c[1]).Trim())
                            {
                                //l[i].UoM = Sm.DrStr(dr, c[3]);
                                l[i].QtyIn = Sm.DrDec(dr, c[4]);
                                l[i].QtyOut = Sm.DrDec(dr, c[5]);
                            }
                        }
                        //l.Add(new KB_4()
                        //{
                        //    Type = Sm.DrStr(dr, c[0]),
                        //    ItGrpCode = Sm.DrStr(dr, c[1]).Trim(),
                        //    ItName = Sm.DrStr(dr, c[2]),
                        //    UoM = Sm.DrStr(dr, c[3]),
                        //    QtyIn = Sm.DrDec(dr, c[4]),
                        //    QtyOut = Sm.DrDec(dr, c[5]),
                        //    QtyBalance = 0m,
                        //    QtyClosingBalance = 0m,
                        //    QtyOpeningBalance = 0m,
                        //    QtyStockAdjustment = 0m,
                        //    QtyStockOpname = string.Empty,
                        //    Remark = string.Empty
                        //});
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<KBOpeningBalance> l2, string DocNoKBOpeningBalance)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.ItGrpCode, Sum(A.Qty) Qty ");
            SQL.AppendLine("From TblKBOpeningBalanceDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Group By B.ItGrpCode; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNoKBOpeningBalance);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItGrpCode", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new KBOpeningBalance()
                        {
                            ItGrpCode = Sm.DrStr(dr, c[0]).Trim(),
                            Qty = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2_1(ref List<KBOpeningBalance> l2, string StartDt, string EndDt)
        {
            string Filter = " Where 0 = 0 ";

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", StartDt);
            Sm.CmParamDt(ref cm, "@DocDt2", EndDt);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForRawMaterial", mItCtCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForWIP", mItCtCodeForWIP);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForFinishedGood", mItCtCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForScrap", mItCtCodeForScrap);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForSparepartMachine", mItCtCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForRawMaterialQtyIn", mCustomsDocCodeForRawMaterialQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForRawMaterialQtyOut", mCustomsDocCodeForRawMaterialQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForScrapQtyIn", mCustomsDocCodeForScrapQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForScrapQtyOut", mCustomsDocCodeForScrapQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForFinishedGoodQtyIn", mCustomsDocCodeForFinishedGoodQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForFinishedGoodQtyOut", mCustomsDocCodeForFinishedGoodQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForWIPQtyIn", mCustomsDocCodeForWIPQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForWIPQtyOut", mCustomsDocCodeForWIPQtyOut);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForSparepartMachineQtyIn", mCustomsDocCodeForSparepartMachineQtyIn);
            Sm.CmParam<String>(ref cm, "@CustomsDocCodeForSparepartMachineQtyOut", mCustomsDocCodeForSparepartMachineQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForRawMaterialQtyIn", mDocTypeForRawMaterialQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForRawMaterialQtyOut", mDocTypeForRawMaterialQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForScrapQtyIn", mDocTypeForScrapQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForScrapQtyOut", mDocTypeForScrapQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForFinishedGoodQtyIn", mDocTypeForFinishedGoodQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForFinishedGoodQtyOut", mDocTypeForFinishedGoodQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForWIPQtyIn", mDocTypeForWIPQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForWIPQtyOut", mDocTypeForWIPQtyOut);
            Sm.CmParam<String>(ref cm, "@DocTypeForSparepartMachineQtyIn", mDocTypeForSparepartMachineQtyIn);
            Sm.CmParam<String>(ref cm, "@DocTypeForSparepartMachineQtyOut", mDocTypeForSparepartMachineQtyOut);
            Sm.CmParam<String>(ref cm, "@KBContractNo", string.Concat("%", TxtDocNo.Text, "%"));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", string.Concat("%", TxtRegistrationNo.Text, "%"));

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "X.DocType", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQL(Sm.GetLue(LueType)) + Filter + " Group By X.ItGrpCode; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItGrpCode", "QtyIn", "QtyOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (int i = 0; i < l2.Count; i++)
                        {
                            if (l2[i].ItGrpCode == Sm.DrStr(dr, c[0]).Trim())
                            {
                                l2[i].Qty = l2[i].Qty + Sm.DrDec(dr, c[1]) - Sm.DrDec(dr, c[2]);
                                //break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process2_2(ref List<KBOpeningBalance> l2, string StartDt)
        {
            string Filter = " Where 0 = 0 ";

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", StartDt);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForRawMaterial", mItCtCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForWIP", mItCtCodeForWIP);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForFinishedGood", mItCtCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForScrap", mItCtCodeForScrap);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForSparepartMachine", mItCtCodeForSparepartMachine);
            Sm.CmParam<String>(ref cm, "@WhsCodeForRawMaterial", mWhsCodeForRawMaterial);
            Sm.CmParam<String>(ref cm, "@WhsCodeForScrap", mWhsCodeForScrap);
            Sm.CmParam<String>(ref cm, "@WhsCodeForFinishedGood", mWhsCodeForFinishedGood);
            Sm.CmParam<String>(ref cm, "@WhsCodeForWIP", mWhsCodeForWIP);
            Sm.CmParam<String>(ref cm, "@WhsCodeForSparepartMachine", mWhsCodeForSparepartMachine);
            
            Sm.CmParam<String>(ref cm, "@KBContractNo", string.Concat("%", TxtDocNo.Text, "%"));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", string.Concat("%", TxtRegistrationNo.Text, "%"));

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "X.DocType", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQLOB() + Filter + " Group By X.DocType, X.ItGrpCode; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocType", 

                    //1-2
                    "ItGrpCode", "Qty"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new KBOpeningBalance()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            ItGrpCode = Sm.DrStr(dr, c[1]).Trim(),
                            Qty = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<KB_4> l, ref List<KBOpeningBalance> l2)
        {
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < l2.Count; j++)
                {
                    if (l[i].ItGrpCode == l2[j].ItGrpCode && l[i].Type == l2[j].DocType)
                    {
                        l[i].QtyOpeningBalance = l2[j].Qty;
                        break;
                    }
                }

                //l[i].QtyClosingBalance = l[i].QtyOpeningBalance + l[i].QtyIn - l[i].QtyOut;
                //decimal mQtyStockOpname = 0m;
                //if (l[i].QtyStockOpname.Length > 0) mQtyStockOpname = Decimal.Parse(l[i].QtyStockOpname);
                //l[i].QtyBalance = l[i].QtyOpeningBalance + l[i].QtyIn - l[i].QtyOut - mQtyStockOpname;
            }
        }

        private void Process3_1(ref List<KB_4> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                decimal mQtyOut = 0m;
                if (l[i].QtyOut < 0m) mQtyOut = l[i].QtyOut * -1;
                else mQtyOut = l[i].QtyOut;
                l[i].QtyClosingBalance = l[i].QtyOpeningBalance + l[i].QtyIn - mQtyOut;
                decimal mQtyStockOpname = 0m;
                if (l[i].QtyStockOpname.Length > 0) mQtyStockOpname = Decimal.Parse(l[i].QtyStockOpname);
                l[i].QtyBalance = l[i].QtyClosingBalance - l[i].QtyOpeningBalance; //l[i].QtyOpeningBalance + l[i].QtyIn - mQtyOut - mQtyStockOpname;
            }
        }

        private void Process4(ref List<KB_4> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 1].Value = l[i].Type;
                Grd1.Cells[No, 2].Value = l[i].ItGrpCode;
                Grd1.Cells[No, 3].Value = l[i].ItName;
                Grd1.Cells[No, 4].Value = l[i].UoM;
                Grd1.Cells[No, 5].Value = l[i].QtyOpeningBalance;
                Grd1.Cells[No, 6].Value = l[i].QtyIn;
                Grd1.Cells[No, 7].Value = l[i].QtyOut;
                Grd1.Cells[No, 8].Value = l[i].QtyStockAdjustment;
                Grd1.Cells[No, 9].Value = l[i].QtyClosingBalance;
                Grd1.Cells[No, 10].Value = l[i].QtyStockOpname;
                Grd1.Cells[No, 11].Value = l[i].QtyBalance;
                Grd1.Cells[No, 12].Value = l[i].Remark;
                No += 1;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Contract#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtRegistrationNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRegistrationNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Registration#");
        }

        #endregion

        #endregion

        #region Class

        private class KB_4
        {
            public int No { get; set; }
            public string Type { get; set; }
            public string ItGrpCode { get; set; }
            public string ItName { get; set; }
            public string UoM { get; set; }
            public decimal QtyOpeningBalance { get; set; }
            public decimal QtyIn { get; set; }
            public decimal QtyOut { get; set; }
            public decimal QtyStockAdjustment { get; set; }
            public decimal QtyClosingBalance { get; set; }
            public string QtyStockOpname { get; set; }
            public decimal QtyBalance { get; set; }
            public string Remark { get; set; }
        }

        private class KBOpeningBalance
        {
            public string DocType { get; set; }
            public string ItGrpCode { get; set; }
            public decimal Qty { get; set; }
        }

        #region Class
        private class KB_4Hdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string Periode1 { get; set; }
            public string Periode2 { get; set; }
            public string Title { get; set; }
            public string PrintBy { get; set; }
        }
       
        #endregion

        #endregion

    }
}