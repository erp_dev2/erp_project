﻿#region Update
/*
    28/01/2020 [HAR/TWC] penggunaan used budget yang dari amt di header diubah berdasarkan qty * unit price di MR detail
    20/02/2020 [HAR/IMS] budget used ambil dari MR yang tidak cancel saja (tanpa melihat sdh di aprove atau sdh di POR kan)
    05/03/2020 [WED/SIER] request nya ambil dari perkalian dengan estimated price, berdasarkan parameter IsBudgetCalculateFromEstimatedPrice & IsMRShowEstimatedPrice
    04/05/2020 [DITA/VIR] Tambah kolom dan filter budget category code internal
    06/05/2020 [WED/IMS] available budget menghitung juga dari VR Budget
    11/05/2020 [WED/SIER] budget tahunan berdasarkan parameter IsBudget2YearlyFormat
    06/05/2020 [DITA/IMS] used amount ambil meghitung juga dari travel request total
    01/07/2020 [DITA/IMS] budget bisa menyimpan tipe cash advance di voucher request param --> VoucherDocTypeBudget
    01/07/2020 [DITA/IMS] menghubungkan budget dengan cash advance settlement
    03/07/2020 [DITA/IMS] rumus pada cash advance settlement, sum nya diluar subquery
    08/07/2020 [TKG/IMS] berdasarkan parameter IsRptBudgetSummaryShowCCGrp+IsRptBudgetSummaryShowCostOfGroup, reporting ini menampilkan cost center group dan cost of group
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    19/07/2021 [WED/PHT] used budget ambil dari ComputeUsedBudget StdMtd
 *  24/08/2021 [ICA/AMKA] menambah filter multi profit center berdasarkan parameter IsBudgetSummaryUseProfitCenter
    03/09/2021 [DITA/AMKA] ubah format computeusedbudget
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;


#endregion

namespace RunSystem
{
    public partial class FrmRptBudgetSummary : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mReqTypeForNonBudget = string.Empty;
        private bool 
            mIsBudgetCalculateFromEstimatedPrice = false, 
            mIsMRShowEstimatedPrice = false, 
            mIsBudget2YearlyFormat = false,
            mIsRptBudgetSummaryShowCCGrp = false, 
            mIsRptBudgetSummaryShowCostOfGroup = false,
            mIsCASUsedForBudget = false,
            mIsRptBudgetSummaryUseProfitCenter = false,
            mIsAllProfitCenterSelected = false
            ;

        #endregion

        #region Constructor

        public FrmRptBudgetSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueYr(LueYr, string.Empty);
                if (mIsBudget2YearlyFormat)
                {
                    LblMth.Visible = LueMth.Visible = ChkMth.Visible = false;
                }
                else
                {
                    Sl.SetLueMth(LueMth);
                }
                LblMultiProfitCenterCode.Visible = CcbProfitCenterCode.Visible = ChkProfitCenterCode.Visible = mIsRptBudgetSummaryUseProfitCenter;
                if (mIsRptBudgetSummaryUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsRptBudgetSummaryShowCCGrp = Sm.GetParameterBoo("IsRptBudgetSummaryShowCCGrp");
            mIsRptBudgetSummaryShowCostOfGroup = Sm.GetParameterBoo("IsRptBudgetSummaryShowCostOfGroup");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsRptBudgetSummaryUseProfitCenter = Sm.GetParameterBoo("IsRptBudgetSummaryUseProfitCenter");
        }

        private string GetSQL(string Filter1, string Filter2, String Query, string DeptCode, string Yr, string Mth, string BCCode)
        {
            var SQL = new StringBuilder();

            if (mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("Select A.Yr, '00' As Mth, B.DeptName, C.BCName, ");
                SQL.AppendLine("A.Amt1, A.Amt2, L.ProfitCenterName, ");
                //SQL.AppendLine("(IfNull(D.Amt3, 0.00) + IfNull(E.Amt, 0.00) + IfNull(F.Amt, 0.00)+ IfNull(G.Amt, 0.00) ");
                //if (mIsCASUsedForBudget)
                //    SQL.AppendLine(" + IfNull(K.Amt, 0.00) ");
                //SQL.AppendLine(") As Amt3, ");
                SQL.AppendLine(Sm.SelectUsedBudget());
                SQL.AppendLine("As Amt3, ");
                SQL.AppendLine("C.LocalCode, I.OptDesc As CCGrpDesc, J.OptDesc As CostOfGroupDesc ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
                SQL.AppendLine("    From TblBudgetSummary A ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine(Filter1);
                SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine(Filter2);
                SQL.AppendLine(Sm.ComputeUsedBudget(2, DeptCode, Yr, Mth, BCCode));
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 4) As Yr, ");
                //if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                //    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                //else
                //    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                //SQL.AppendLine("    From TblMaterialRequestHdr A ");
                //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("    Where B.cancelind = 'N'  ");
                //SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                //SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr ");
                //SQL.AppendLine("Left Join ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, ");
                //SQL.AppendLine("    Sum(Amt) Amt ");
                //SQL.AppendLine("    From TblVoucherRequestHdr ");
                //SQL.AppendLine("    Where ReqType Is Not Null ");
                //SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                //SQL.AppendLine("    And CancelInd = 'N' ");
                //SQL.AppendLine("    And Status In ('O', 'A') ");
                //SQL.AppendLine("    And Find_In_Set(DocType, ");
                //SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                //if (Sm.GetLue(LueYr).Length > 0)
                //    SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                //if (Sm.GetLue(LueDeptCode).Length > 0)
                //    SQL.AppendLine("    And DeptCode = @DeptCode ");
                //SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 4) ");
                //SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, ");
                //SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                //SQL.AppendLine("    From TblTravelRequestHdr A ");
                //SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                //SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                //SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4) ");
                //SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, SUM(T.Amt) As Amt From( ");
                //SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, ");
                //SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                //SQL.AppendLine("        From tblvoucherhdr A ");
                //SQL.AppendLine("        Inner Join ");
                //SQL.AppendLine("	    (");
                //SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                //SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                //SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                //SQL.AppendLine("			AND X1.DocType = '58' ");
                //SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                //SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                //SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                //SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                //SQL.AppendLine("       Inner Join ");
                //SQL.AppendLine("		  ( ");
                //SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                //SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                //SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                //SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                //SQL.AppendLine("			AND X1.DocType = '56' ");
                //SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                //SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                //SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                //SQL.AppendLine("		  ) T ");
                //SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4) ");
                //SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr ");
                SQL.AppendLine("Left Join TblCostCategory H On C.LocalCode=H.CCtCode And C.LocalCode Is Not Null ");
                SQL.AppendLine("Left Join TblOption I On I.OptCat='CostCenterGroup' And H.CCGrpCode=I.OptCode ");
                SQL.AppendLine("Left Join TblOption J On J.OptCat='CostOfGroup' And H.CostOfGroupCode=J.OptCode ");
                SQL.AppendLine("Left Join TblCostCenter K On C.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter L On K.ProfitCenterCode = L.ProfitCenterCode ");
                //if (mIsCASUsedForBudget)
                //{
                //    SQL.AppendLine("Left Join ");
                //    SQL.AppendLine("( ");
                //    SQL.AppendLine("    Select Left(A.DocDt, 4) Yr, '00' As Mth, C.BCCode, C.DeptCode, Sum(B.Amt) Amt ");
                //    SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                //    SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                //    SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                //    SQL.AppendLine("        And A.DocStatus = 'F' ");
                //    SQL.AppendLine("        And B.CCtCode Is Not Null ");
                //    SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                //    SQL.AppendLine("        And C.CCtCode Is Not Null ");
                //    SQL.AppendLine("    Group By Left(A.DocDt, 4), C.BCCode, C.DeptCode ");
                //    SQL.AppendLine(") K On A.BCCode = K.BCCode And A.DeptCode = K.DeptCode And A.Yr = K.Yr ");
                //}
                SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
            }
            else
            {
                SQL.AppendLine("Select A.Yr, A.Mth, B.DeptName, C.BCName, ");
                SQL.AppendLine("A.Amt1, A.Amt2, L.ProfitCenterName, ");
                //SQL.AppendLine("(IfNull(D.Amt3, 0) + IfNull(E.Amt, 0) + IfNull(F.Amt, 0)+ IfNull(G.Amt, 0)) As Amt3, ");
                SQL.AppendLine(Sm.SelectUsedBudget());
                SQL.AppendLine("As Amt3, ");
                SQL.AppendLine("C.LocalCode, I.OptDesc As CCGrpDesc, J.OptDesc As CostOfGroupDesc ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine(Filter2);
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 4) As Yr, ");
                //SQL.AppendLine("    Substring(DocDt, 5, 2) As Mth, ");
                //if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                //    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                //else
                //    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                //SQL.AppendLine("    From TblMaterialRequestHdr A ");
                //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("    Where B.cancelind = 'N'  ");
                //SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                //SQL.AppendLine("    , Substring(A.DocDt, 5, 2) ");
                //SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr And A.Mth=D.Mth ");
                //SQL.AppendLine("Left Join ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, Substr(DocDt, 5, 2) Mth, ");
                //SQL.AppendLine("    Sum(Amt) Amt ");
                //SQL.AppendLine("    From TblVoucherRequestHdr ");
                //SQL.AppendLine("    Where ReqType Is Not Null ");
                //SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                //SQL.AppendLine("    And CancelInd = 'N' ");
                //SQL.AppendLine("    And Status In ('O', 'A') ");
                //SQL.AppendLine("    And Find_In_Set(DocType, ");
                //SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                //if (Sm.GetLue(LueYr).Length > 0)
                //    SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                //if (Sm.GetLue(LueMth).Length > 0)
                //    SQL.AppendLine("    And Substr(DocDt, 5, 2) = @Mth ");
                //if (Sm.GetLue(LueDeptCode).Length > 0)
                //    SQL.AppendLine("    And DeptCode = @DeptCode ");
                //SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 6) ");
                //SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr And A.Mth=E.Mth ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                //SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                //SQL.AppendLine("    From TblTravelRequestHdr A ");
                //SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                //SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                //SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");
                //SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr And A.Mth=F.Mth ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, T.Mth, SUM(T.Amt) As Amt From( ");
                //SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                //SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                //SQL.AppendLine("        From tblvoucherhdr A ");
                //SQL.AppendLine("        Inner Join ");
                //SQL.AppendLine("	    (");
                //SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                //SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                //SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                //SQL.AppendLine("			AND X1.DocType = '58' ");
                //SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                //SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                //SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                //SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                //SQL.AppendLine("       Inner Join ");
                //SQL.AppendLine("		  ( ");
                //SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                //SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                //SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                //SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                //SQL.AppendLine("			AND X1.DocType = '56' ");
                //SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                //SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                //SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                //SQL.AppendLine("		  ) T ");
                //SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4), Substring(T.DocDt, 5, 2) ");
                //SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr And A.Mth=G.Mth ");
                SQL.AppendLine("Left Join TblCostCategory H On C.LocalCode=H.CCtCode And C.LocalCode Is Not Null ");
                SQL.AppendLine("Left Join TblOption I On I.OptCat='CostCenterGroup' And H.CCGrpCode=I.OptCode ");
                SQL.AppendLine("Left Join TblOption J On J.OptCat='CostOfGroup' And H.CostOfGroupCode=J.OptCode ");
                SQL.AppendLine("Left Join TblCostCenter K On C.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter L On K.ProfitCenterCode = L.ProfitCenterCode ");
                //if (mIsCASUsedForBudget)
                //{
                //    SQL.AppendLine("Left Join ");
                //    SQL.AppendLine("( ");
                //    SQL.AppendLine("    Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, Sum(B.Amt) Amt ");
                //    SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                //    SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                //    SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                //    SQL.AppendLine("        And A.DocStatus = 'F' ");
                //    SQL.AppendLine("        And B.CCtCode Is Not Null ");
                //    SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                //    SQL.AppendLine("        And C.CCtCode Is Not Null ");
                //    SQL.AppendLine("    Group By Left(A.DocDt, 4), Substring(A.DocDt, 5, 2), C.BCCode, C.DeptCode ");
                //    SQL.AppendLine(") K On A.BCCode = K.BCCode And A.DeptCode = K.DeptCode And A.Yr = K.Yr And A.Mth = K.Mth ");
                //}
                SQL.AppendLine(Sm.ComputeUsedBudget(2, DeptCode, Yr, Mth, BCCode));
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine(Filter1);
                SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
            }

            #region Old Code

            //SQL.AppendLine("Select A.Yr, A.Mth, B.DeptName, C.BCName, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2, ");
            //else
            //    SQL.AppendLine("A.Amt1, A.Amt2, ");
            //SQL.AppendLine("(IfNull(D.Amt3, 0) + IfNull(E.Amt, 0) + IfNull(F.Amt, 0)+ IfNull(G.Amt, 0)) As Amt3, C.LocalCode ");
            //SQL.AppendLine("From TblBudgetSummary A ");
            //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            //SQL.AppendLine("    And Exists( ");
            //SQL.AppendLine("        Select 1 From TblGroupDepartment ");
            //SQL.AppendLine("        Where DeptCode=B.DeptCode ");
            //SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 4) As Yr, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    '00' As Mth, ");
            //else
            //    SQL.AppendLine("    Substring(DocDt, 5, 2) As Mth, ");
            //if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
            //    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
            //else
            //    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
            //SQL.AppendLine("    From TblMaterialRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Where B.cancelind = 'N'  ");
            //SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    , Substring(A.DocDt, 5, 2) ");
            //SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    And A.Mth=D.Mth ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    '00' As Mth, ");
            //else
            //    SQL.AppendLine("    Substr(DocDt, 5, 2) Mth, ");
            //SQL.AppendLine("    Sum(Amt) Amt ");
            //SQL.AppendLine("    From TblVoucherRequestHdr ");
            //SQL.AppendLine("    Where ReqType Is Not Null ");
            //SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
            //SQL.AppendLine("    And CancelInd = 'N' ");
            //SQL.AppendLine("    And Status In ('O', 'A') ");
            //SQL.AppendLine("    And Find_In_Set(DocType, ");
            //SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            //if (Sm.GetLue(LueYr).Length > 0)
            //    SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
            //if (Sm.GetLue(LueMth).Length > 0 && !mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    And Substr(DocDt, 5, 2) = @Mth ");
            //if (Sm.GetLue(LueDeptCode).Length > 0)
            //    SQL.AppendLine("    And DeptCode = @DeptCode ");
            //SQL.AppendLine("    Group By DeptCode, BCCode, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    Left(DocDt, 4) ");
            //else
            //    SQL.AppendLine("    Left(DocDt, 6) ");
            //SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr And A.Mth=E.Mth ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    '00' As Mth, ");
            //else
            //    SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth, ");
            //SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
            //SQL.AppendLine("    From TblTravelRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            //SQL.AppendLine("    Where A.Cancelind = 'N'  ");
            //SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4) ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    , Substring(A.DocDt, 5, 2) ");
            //SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    And A.Mth=F.Mth ");

            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, T.Mth, SUM(T.Amt) As Amt From( ");
            //SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, ");
            //if (mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    '00' As Mth, ");
            //else
            //    SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth, ");
            //SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
            //SQL.AppendLine("        From tblvoucherhdr A ");
            //SQL.AppendLine("        Inner Join ");
            //SQL.AppendLine("	    (");
            //SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
            //SQL.AppendLine("		  	From tblvoucherhdr X1 ");
            //SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            //SQL.AppendLine("			AND X1.DocType = '58' ");
            //SQL.AppendLine("            AND X1.CancelInd = 'N' ");
            //SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
            //SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            //SQL.AppendLine("       Inner Join ");
            //SQL.AppendLine("		  ( ");
            //SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            //SQL.AppendLine("		  	From tblvoucherhdr X1  ");
            //SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            //SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            //SQL.AppendLine("			AND X1.DocType = '56' ");
            //SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
            //SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
            //SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
            //SQL.AppendLine("		  ) T ");
            //SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4) ");
            //if (!mIsBudget2YearlyFormat) SQL.AppendLine("    , Substring(T.DocDt, 5, 2) ");
            //SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr ");
            //if (!mIsBudget2YearlyFormat) SQL.AppendLine("    And A.Mth=G.Mth ");

            //SQL.AppendLine(Filter);
            //if (mIsBudget2YearlyFormat) SQL.AppendLine("Group By A.Yr, B.DeptName, C.BCName ");
            //SQL.AppendLine("Order By A.Yr, ");
            //if (!mIsBudget2YearlyFormat) SQL.AppendLine("A.Mth, ");
            //SQL.AppendLine("B.DeptName, C.BCName; ");

            #endregion

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 1;
            
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Year",
                    "Month",
                    "Department", 
                    "Category",
                    "Profit Center",
                    
                    //6-10
                    "Requested",
                    "Budget",
                    "Used",
                    "Budget Category's" + Environment.NewLine + "Local Code",
                    "Group",

                    //11
                    "Cost of Group", 

                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    80, 80, 200, 200, 200, 

                    //6-10
                    130, 130, 130, 130, 200, 

                    //11
                    200, 
                }
            );
            Grd1.Cols[10].Visible = mIsRptBudgetSummaryShowCCGrp;
            Grd1.Cols[11].Visible = mIsRptBudgetSummaryShowCostOfGroup;
            Grd1.Cols[5].Visible = mIsRptBudgetSummaryUseProfitCenter;
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 }, 0);
            if (mIsBudget2YearlyFormat) Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter1 = " ", Filter2 = " ", Filter3 = " ", Query = string.Empty;
                int i = 0;
                var cm = new MySqlCommand();

                SetProfitCenter();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);

                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                if (!mIsBudget2YearlyFormat) Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtBCLocalCode.Text, "C.LocalCode", false);

                string BCCode = string.Empty;
                if (TxtBCLocalCode.Text.Length > 0) BCCode = Sm.GetValue("Select Group_Concat(Distinct Ifnull(BCCode, '')) From TblBudgetCategory Where LocalCode Like @Param ", string.Concat("%", TxtBCLocalCode.Text, "%"));

                #region Filter Multi ProfitCenter
                //filter CCbProfitCenter
                if (mIsRptBudgetSummaryUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        Filter3 = string.Empty;
                        i = 0;

                        Query ="And C.CCCode Is Not Null ";
                        Query +="And C.CCCode In ( ";
                        Query +="   Select Distinct CCCode ";
                        Query +="   From TblCostCenter ";
                        Query +="   Where ActInd='Y' ";
                        Query +="   And ProfitCenterCode Is Not Null ";
                        Query +="   And ProfitCenterCode In ( ";
                        Query +="       Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                        Query +="       Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        Query +="   ) ";
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter3.Length > 0) Filter3 += " Or ";
                            Filter3 += "(ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter3.Length == 0)
                            Query +="And 1=0 ";
                        else
                            Query +="And (" + Filter3 + ") ";
                        Query +=") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Query ="And C.CCCode Is Not Null ";
                            Query +="And C.CCCode In ( ";
                            Query +="   Select Distinct CCCode ";
                            Query +="   From TblCostCenter ";
                            Query +="   Where ActInd='Y' ";
                            Query +="   And ProfitCenterCode Is Not Null ";
                            Query +="   And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            Query +=") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Query +="And (C.CCCode Is Null Or (C.CCCode Is Not Null And C.CCCode In ( ";
                            Query +="   Select Distinct CCCode ";
                            Query +="   From TblCostCenter ";
                            Query +="   Where ActInd='Y' ";
                            Query +="   And ProfitCenterCode Is Not Null ";
                            Query +="   And ProfitCenterCode In (";
                            Query +="       Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                            Query +="       Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Query +="   )))) ";
                        }
                    }
                }
                #endregion
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL(Filter1, Filter2, Query, Sm.GetLue(LueDeptCode), Sm.GetLue(LueYr), Sm.GetLue(LueMth), BCCode),
                new string[]
                    {
                        //0
                        "Yr", 

                        //1-5
                        "Mth", "DeptName", "BCName", "Amt1", "Amt2",

                        //6-9
                        "Amt3", "LocalCode", "CCGrpDesc", "CostOfGroupDesc", "ProfitCenterName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptBudgetSummaryUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
            //SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));

            //var IsIncludeConsolidate = Value.Contains("Consolidate");
            //return string.Concat(
            //    (IsIncludeConsolidate?"Consolidate, ":string.Empty), 
            //    Sm.GetValue(
            //        "Select Group_Concat(T.Code Separator ', ') As Code " +
            //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ", 
            //        Value.Replace(", ", ",")));
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBCLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget Category Code Internal");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void ChkMth_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Month");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion
    }
}
