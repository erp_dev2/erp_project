﻿#region Update
/*
    12/11/2018 [WED] tambah kolom remark, dan loop ke FrmSO2
    19/11/2018 [WED] tambah kolom UoM SO
    22/11/2018 [WED] kolom UoM SO ambil dari Packaging
    18/12/2018 [HAR] tambah informasi quantity container group dan delivery date
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSONoAmt : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mLocalDocument = string.Empty,
            mLblLocalDocument = string.Empty;

        private bool mIsCustomerItemNameMandatory = false;

        #endregion

        #region Constructor

        public FrmRptSONoAmt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mLocalDocument == "1") mLblLocalDocument = "Sales" + Environment.NewLine + "Contract#";
                else mLblLocalDocument = "Local" + Environment.NewLine + "Document#";
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mLocalDocument = Sm.GetParameter("LocalDocument");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Case Status When 'O' Then 'Outstanding' When 'P' Then 'Partial Fullfiled' When 'M' Then 'Manual Fullfiled' When 'F' Then 'FullFilled' End As Status, CancelInd, ");
            SQL.AppendLine("A.SAAddress, B.CtName, A.CtPONo, A.CtQtDocNo, ");
            SQL.AppendLine("F.ItCode, H.ItCodeInternal, H.ItName, H.Specification, J.CtItCode, J.CtItName, C.DeliveryDt, ");
            SQL.AppendLine("C.QtyPackagingUnit, C.PackagingUnitUomCode, C.ContainerGroup, C.ContainerGroupQty, C.DeliveryDt, A.Remark ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode ");
            SQL.AppendLine("Inner Join TblSODtl C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl D On A.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceHdr E On D.ItemPriceDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl F On D.ItemPriceDocNo=F.DocNo And D.ItemPriceDNo=F.DNo ");
            SQL.AppendLine("Left Join TblSOQuotPromoItem G On A.SOQuotPromoDocNo=G.DocNo And F.ItCode=G.ItCode  ");
            SQL.AppendLine("Inner Join TblItem H On F.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblAgent I On C.AgtCode=I.AgtCode ");
            SQL.AppendLine("Left Join TblCustomerItem J On F.ItCode=J.ItCode And A.CtCode=J.CtCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Status",
                    "Cancel",
                    
                    //6-10
                    mLblLocalDocument,
                    "Customer",
                    "Customer's"+Environment.NewLine+"PO#",
                    "Customer's"+Environment.NewLine+"Quotation",
                    "Shipping"+Environment.NewLine+"Address",
                    
                    //11-15
                    "Item's Code",
                    "Local Code",
                    "Item's Name",
                    "Customer's"+Environment.NewLine+"Item Code",
                    "Customer's"+Environment.NewLine+"Item Name",

                    //16-20
                    "Specification",
                    "Packaging"+Environment.NewLine+"Quantity",
                    "Packaging",
                    "Container's"+Environment.NewLine+"Group",
                    "Container's Group"+Environment.NewLine+"Quantity",

                    //21-22
                    "Delivery"+Environment.NewLine+"Date",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 20, 80, 80, 60, 
                    
                    //6-10
                    130, 200, 130, 130, 200, 

                    //11-15
                    100, 100, 250, 100, 200, 

                    //16-20
                    200, 120, 100, 100, 100, 
                    
                    //21-22
                    100, 250
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 20}, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 11, 12 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 1=1 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "H.ItCode", "H.ItName", "H.ItCodeInternal" }); ;

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "Status", "CancelInd", "LocalDocNo", "CtName",                          
                        //6-10
                        "CtPONo", "CtQtDocNo", "SAAddress", "ItCode", "ItCodeInternal", 
                        //11-15
                        "ItName", "CtItCode", "CtItName", "Specification","QtyPackagingUnit", 
                        //16-20
                        "PackagingUnitUomCode", "ContainerGroup", "ContainerGroupQty", "DeliveryDt","Remark"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
