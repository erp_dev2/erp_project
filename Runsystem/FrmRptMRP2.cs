﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMRP2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMRP2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T2.ItCode, T5.ItName, T2.Qty, T5.PlanningUomCode, ");
            SQL.AppendLine("(T2.Qty*IfNull(T5.PlanningUomCodeConvert12, 1)) As Qty2, T5.PlanningUomCode2, T1.ProductionOrderDocNo, T4.PPDocNo ");
            SQL.AppendLine("From TblMRP2Hdr T1 ");
            SQL.AppendLine("Inner Join TblMRP2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr T3 On T1.ProductionOrderDocNo = T3.DocNo ");
            SQL.AppendLine("Inner Join (Select DocNo As PPDocNo, ProductionOrderDocNo From TblPPDtl) T4 On T3.DocNo = T4.ProductionOrderDocNo ");
            SQL.AppendLine("Inner Join TblItem T5 On T2.ItCode = T5.ItCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "MRP#",
                    "Date",
                    "Item Code",
                    "Item Name",
                    "Quantity",

                    //6-10
                    "UoM",
                    "Quantity 2",
                    "UoM 2",
                    "Order#",
                    "",

                    //11-12
                    "Planning#",
                    ""
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    140, 100, 100, 180, 100, 
                    
                    //6-10
                    120, 100, 120, 120, 20, 
                    
                    //11-12
                    120, 20
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 10, 12 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 12 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T2.ItCode", "T5.ItName", "T5.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProductionOrderDocNo.Text, "T1.ProductionOrderDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPPDocNo.Text, "T4.PPDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By T1.DocNo, T5.ItName; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ItCode", "ItName", "Qty", "PlanningUomCode", 
                        
                        //6-9
                        "Qty2", "PlanningUomCode2", "ProductionOrderDocNo", "PPDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProductionOrder");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmPP");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProductionOrder");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmPP");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "MRP#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtPPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planning#");
        }

        private void TxtProductionOrderDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProductionOrderDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Order#");
        }

        #endregion

        #endregion
    }
}
