﻿#region Update
/*
    08/07/2020 [TKG/YK] AR Settlement For Project
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmARS2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmARS2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmARS2Dlg2(FrmARS2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "List Of Sales Invoice (Project)";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, Null As TaxInvDocument, A.CtCode, C.Ctname, A.CurCode, ");
            SQL.AppendLine("A.Amt-IfNull(B.Amt2, 0.00)-IfNull(D.Amt3, 0.00) As Outstanding,  ");
            if (mFrmParent.mProjectAcNoFormula == "2")
                SQL.AppendLine("Concat(E.ParValue, J.ProjectCode2) As AcNo ");
            else
                SQL.AppendLine("Concat(E.ParValue, J.ProjectCode) As AcNo ");
            SQL.AppendLine("From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.InvoiceDocNo, Sum(A.Amt) AS Amt2 ");
            SQL.AppendLine("    From TblIncomingPaymentDtl A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.Docno=B.Docno And B.CancelInd='N' And IfNull(B.Status, 'O')<>'C' ");
            SQL.AppendLine("    Where A.InvoiceDocNo In (Select DocNo From TblSalesInvoice5Hdr Where CancelInd='N' And ProcessInd<>'F') ");
            SQL.AppendLine("    Group By A.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  SalesInvoiceDocNo, Sum(Amt) As Amt3 ");
            SQL.AppendLine("    From TblARSHdr A ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And SalesInvoiceDocNo In (Select DocNo From TblSalesInvoice5Hdr Where CancelInd='N' And ProcessInd<>'F') ");
            SQL.AppendLine("    Group By SalesInvoiceDocNo ");
            SQL.AppendLine(") D On A.DocNo=D.SalesInvoiceDocNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("Left Join TblSalesInvoice5Dtl F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl G On F.ProjectImplementationDocNo=G.DocNo And F.ProjectImplementationDNo=G.DNo ");
            SQL.AppendLine("Left Join TblProjectImplementationHdr H On G.DocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblSOContractRevisionHdr I On H.SOContractDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr J On I.SOCDocNo=J.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.ProcessInd <> 'F' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.Amt-IfNull(B.Amt2, 0.00)-IfNull(D.Amt3, 0.00)>0.00 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "",
                        "Date",
                        "Customer's"+Environment.NewLine+"Invoice#",
                        "Customer",
                        
                        //6-8
                        "Currency",
                        "Outstanding",
                        "Project Code (Account#)"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 150, 200, 
                        
                        //6-8
                        80, 130, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocDt, DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "TaxInvDocument", "CtName", "CurCode", "Outstanding",
                  
                            //6
                            "AcNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.ShowDataSalesInvoice(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Contains("SIPR"))
                {
                    var f1 = new FrmSalesInvoice5(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
                else
                {
                    var f1 = new FrmSalesInvoice(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSalesInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

    }
}
