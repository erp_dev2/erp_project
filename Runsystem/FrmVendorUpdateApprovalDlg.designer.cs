﻿namespace RunSystem
{
    partial class FrmVendorUpdateApprovalDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtShortName = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.LueSDCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueVilCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LblVdCtCode = new System.Windows.Forms.Label();
            this.LueVdCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtVdName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgContactPerson = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgBank = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgItemCategory = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVdSector = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkTaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTIN = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtIdentityNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.BtnVendor = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtEstablishedYr = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSDCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVilCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgContactPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            this.TpgItemCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgVdSector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstablishedYr.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(778, 0);
            this.panel1.Size = new System.Drawing.Size(70, 390);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.LueParent);
            this.panel2.Controls.Add(this.TxtWebsite);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.BtnVendor);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.TxtShortName);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.LueSDCode);
            this.panel2.Controls.Add(this.LueVilCode);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtPostalCd);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCityCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LblVdCtCode);
            this.panel2.Controls.Add(this.LueVdCtCode);
            this.panel2.Controls.Add(this.MeeAddress);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtVdName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(778, 390);
            // 
            // TxtShortName
            // 
            this.TxtShortName.EnterMoveNextControl = true;
            this.TxtShortName.Location = new System.Drawing.Point(91, 46);
            this.TxtShortName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortName.Properties.Appearance.Options.UseFont = true;
            this.TxtShortName.Properties.MaxLength = 40;
            this.TxtShortName.Size = new System.Drawing.Size(319, 20);
            this.TxtShortName.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(13, 49);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Short Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSDCode
            // 
            this.LueSDCode.EnterMoveNextControl = true;
            this.LueSDCode.Location = new System.Drawing.Point(91, 130);
            this.LueSDCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSDCode.Name = "LueSDCode";
            this.LueSDCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.Appearance.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSDCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSDCode.Properties.DropDownRows = 30;
            this.LueSDCode.Properties.NullText = "[Empty]";
            this.LueSDCode.Properties.PopupWidth = 350;
            this.LueSDCode.Size = new System.Drawing.Size(319, 20);
            this.LueSDCode.TabIndex = 23;
            this.LueSDCode.ToolTip = "F4 : Show/hide list";
            this.LueSDCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSDCode.EditValueChanged += new System.EventHandler(this.LueSDCode_EditValueChanged);
            // 
            // LueVilCode
            // 
            this.LueVilCode.EnterMoveNextControl = true;
            this.LueVilCode.Location = new System.Drawing.Point(91, 151);
            this.LueVilCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVilCode.Name = "LueVilCode";
            this.LueVilCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.Appearance.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVilCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVilCode.Properties.DropDownRows = 30;
            this.LueVilCode.Properties.NullText = "[Empty]";
            this.LueVilCode.Properties.PopupWidth = 350;
            this.LueVilCode.Size = new System.Drawing.Size(319, 20);
            this.LueVilCode.TabIndex = 25;
            this.LueVilCode.ToolTip = "F4 : Show/hide list";
            this.LueVilCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVilCode.EditValueChanged += new System.EventHandler(this.LueVilCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 134);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 14);
            this.label16.TabIndex = 22;
            this.label16.Text = "Sub District";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(44, 155);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 14);
            this.label14.TabIndex = 24;
            this.label14.Text = "Village";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(91, 172);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Size = new System.Drawing.Size(319, 20);
            this.TxtPostalCd.TabIndex = 72;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(58, 113);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 20;
            this.label4.Text = "City";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(91, 109);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 350;
            this.LueCityCode.Size = new System.Drawing.Size(319, 20);
            this.LueCityCode.TabIndex = 21;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(14, 176);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 26;
            this.label6.Text = "Postal Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblVdCtCode
            // 
            this.LblVdCtCode.AutoSize = true;
            this.LblVdCtCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVdCtCode.ForeColor = System.Drawing.Color.Black;
            this.LblVdCtCode.Location = new System.Drawing.Point(29, 70);
            this.LblVdCtCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVdCtCode.Name = "LblVdCtCode";
            this.LblVdCtCode.Size = new System.Drawing.Size(56, 14);
            this.LblVdCtCode.TabIndex = 16;
            this.LblVdCtCode.Text = "Category";
            this.LblVdCtCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCtCode
            // 
            this.LueVdCtCode.EnterMoveNextControl = true;
            this.LueVdCtCode.Location = new System.Drawing.Point(91, 67);
            this.LueVdCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCtCode.Name = "LueVdCtCode";
            this.LueVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCtCode.Properties.DropDownRows = 30;
            this.LueVdCtCode.Properties.NullText = "[Empty]";
            this.LueVdCtCode.Properties.PopupWidth = 350;
            this.LueVdCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueVdCtCode.TabIndex = 17;
            this.LueVdCtCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCtCode.EditValueChanged += new System.EventHandler(this.LueVdCtCode_EditValueChanged);
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(91, 88);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(319, 20);
            this.MeeAddress.TabIndex = 19;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(35, 91);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 18;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdName
            // 
            this.TxtVdName.EnterMoveNextControl = true;
            this.TxtVdName.Location = new System.Drawing.Point(91, 25);
            this.TxtVdName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdName.Name = "TxtVdName";
            this.TxtVdName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdName.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName.Properties.MaxLength = 40;
            this.TxtVdName.Size = new System.Drawing.Size(319, 20);
            this.TxtVdName.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Vendor Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(91, 4);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Size = new System.Drawing.Size(154, 20);
            this.TxtVdCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Vendor Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgContactPerson);
            this.tabControl1.Controls.Add(this.TpgBank);
            this.tabControl1.Controls.Add(this.TpgItemCategory);
            this.tabControl1.Controls.Add(this.TpgVdSector);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 242);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(778, 148);
            this.tabControl1.TabIndex = 48;
            // 
            // TpgContactPerson
            // 
            this.TpgContactPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgContactPerson.Controls.Add(this.Grd1);
            this.TpgContactPerson.Location = new System.Drawing.Point(4, 26);
            this.TpgContactPerson.Name = "TpgContactPerson";
            this.TpgContactPerson.Size = new System.Drawing.Size(770, 118);
            this.TpgContactPerson.TabIndex = 3;
            this.TpgContactPerson.Text = "Contact Person";
            this.TpgContactPerson.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(770, 118);
            this.Grd1.TabIndex = 48;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgBank
            // 
            this.TpgBank.Controls.Add(this.Grd2);
            this.TpgBank.Controls.Add(this.LueLevel);
            this.TpgBank.Location = new System.Drawing.Point(4, 26);
            this.TpgBank.Name = "TpgBank";
            this.TpgBank.Size = new System.Drawing.Size(770, 118);
            this.TpgBank.TabIndex = 4;
            this.TpgBank.Text = "Bank";
            this.TpgBank.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(770, 118);
            this.Grd2.TabIndex = 47;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(72, 22);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 12;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(210, 20);
            this.LueLevel.TabIndex = 37;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TpgItemCategory
            // 
            this.TpgItemCategory.Controls.Add(this.Grd3);
            this.TpgItemCategory.Location = new System.Drawing.Point(4, 26);
            this.TpgItemCategory.Name = "TpgItemCategory";
            this.TpgItemCategory.Size = new System.Drawing.Size(770, 118);
            this.TpgItemCategory.TabIndex = 10;
            this.TpgItemCategory.Text = "Item\'s Category";
            this.TpgItemCategory.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(770, 118);
            this.Grd3.TabIndex = 48;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgVdSector
            // 
            this.TpgVdSector.Controls.Add(this.Grd4);
            this.TpgVdSector.Location = new System.Drawing.Point(4, 26);
            this.TpgVdSector.Name = "TpgVdSector";
            this.TpgVdSector.Size = new System.Drawing.Size(770, 118);
            this.TpgVdSector.TabIndex = 11;
            this.TpgVdSector.Text = "Vendor\'s Sector";
            this.TpgVdSector.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(770, 118);
            this.Grd4.TabIndex = 48;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.TxtEstablishedYr);
            this.panel4.Controls.Add(this.ChkTaxInd);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.TxtTIN);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtIdentityNo);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtCreditLimit);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtMobile);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtPhone);
            this.panel4.Controls.Add(this.TxtEmail);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtFax);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(425, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(353, 242);
            this.panel4.TabIndex = 27;
            // 
            // ChkTaxInd
            // 
            this.ChkTaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInd.Location = new System.Drawing.Point(297, 44);
            this.ChkTaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInd.Name = "ChkTaxInd";
            this.ChkTaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInd.Properties.Caption = "Tax";
            this.ChkTaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInd.Size = new System.Drawing.Size(50, 22);
            this.ChkTaxInd.TabIndex = 34;
            this.ChkTaxInd.ToolTip = "Tax Indicator";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(7, 50);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 14);
            this.label13.TabIndex = 32;
            this.label13.Text = "Taxpayer Identification#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTIN
            // 
            this.TxtTIN.EnterMoveNextControl = true;
            this.TxtTIN.Location = new System.Drawing.Point(153, 45);
            this.TxtTIN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTIN.Name = "TxtTIN";
            this.TxtTIN.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTIN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTIN.Properties.Appearance.Options.UseFont = true;
            this.TxtTIN.Properties.MaxLength = 40;
            this.TxtTIN.Size = new System.Drawing.Size(140, 20);
            this.TxtTIN.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(51, 28);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 14);
            this.label12.TabIndex = 30;
            this.label12.Text = "Identity Number";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo
            // 
            this.TxtIdentityNo.EnterMoveNextControl = true;
            this.TxtIdentityNo.Location = new System.Drawing.Point(153, 24);
            this.TxtIdentityNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo.Name = "TxtIdentityNo";
            this.TxtIdentityNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo.Properties.MaxLength = 40;
            this.TxtIdentityNo.Size = new System.Drawing.Size(194, 20);
            this.TxtIdentityNo.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(101, 175);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 45;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(153, 150);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Size = new System.Drawing.Size(194, 20);
            this.TxtCreditLimit.TabIndex = 44;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(153, 171);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(194, 20);
            this.MeeRemark.TabIndex = 46;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(80, 154);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 14);
            this.label11.TabIndex = 43;
            this.label11.Text = "Credit Limit";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(153, 129);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Size = new System.Drawing.Size(194, 20);
            this.TxtMobile.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(106, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 35;
            this.label7.Text = "Phone";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(107, 133);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 41;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(153, 66);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Size = new System.Drawing.Size(194, 20);
            this.TxtPhone.TabIndex = 36;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(153, 108);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Size = new System.Drawing.Size(194, 20);
            this.TxtEmail.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(67, 112);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 14);
            this.label8.TabIndex = 39;
            this.label8.Text = "Email Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(123, 91);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 14);
            this.label9.TabIndex = 37;
            this.label9.Text = "Fax";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(153, 87);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Size = new System.Drawing.Size(194, 20);
            this.TxtFax.TabIndex = 38;
            // 
            // BtnVendor
            // 
            this.BtnVendor.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVendor.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVendor.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVendor.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVendor.Appearance.Options.UseBackColor = true;
            this.BtnVendor.Appearance.Options.UseFont = true;
            this.BtnVendor.Appearance.Options.UseForeColor = true;
            this.BtnVendor.Appearance.Options.UseTextOptions = true;
            this.BtnVendor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVendor.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVendor.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVendor.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVendor.Location = new System.Drawing.Point(387, 2);
            this.BtnVendor.Name = "BtnVendor";
            this.BtnVendor.Size = new System.Drawing.Size(19, 20);
            this.BtnVendor.TabIndex = 73;
            this.BtnVendor.ToolTip = "Find Voucher Request";
            this.BtnVendor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVendor.ToolTipTitle = "Run System";
            this.BtnVendor.Click += new System.EventHandler(this.BtnVendor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(265, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 14);
            this.label3.TabIndex = 46;
            this.label3.Text = "Existing Vendor Data";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWebsite
            // 
            this.TxtWebsite.EnterMoveNextControl = true;
            this.TxtWebsite.Location = new System.Drawing.Point(91, 193);
            this.TxtWebsite.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWebsite.Name = "TxtWebsite";
            this.TxtWebsite.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWebsite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWebsite.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWebsite.Properties.Appearance.Options.UseFont = true;
            this.TxtWebsite.Properties.MaxLength = 40;
            this.TxtWebsite.Size = new System.Drawing.Size(319, 20);
            this.TxtWebsite.TabIndex = 75;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(35, 196);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 14);
            this.label18.TabIndex = 74;
            this.label18.Text = "Website";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(16, 216);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 14);
            this.label19.TabIndex = 76;
            this.label19.Text = "Head Office";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(91, 214);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 350;
            this.LueParent.Size = new System.Drawing.Size(319, 20);
            this.LueParent.TabIndex = 77;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(37, 7);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 14);
            this.label20.TabIndex = 28;
            this.label20.Text = "Establishment Year";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEstablishedYr
            // 
            this.TxtEstablishedYr.EnterMoveNextControl = true;
            this.TxtEstablishedYr.Location = new System.Drawing.Point(153, 3);
            this.TxtEstablishedYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstablishedYr.Name = "TxtEstablishedYr";
            this.TxtEstablishedYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEstablishedYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstablishedYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstablishedYr.Properties.Appearance.Options.UseFont = true;
            this.TxtEstablishedYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstablishedYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstablishedYr.Properties.MaxLength = 40;
            this.TxtEstablishedYr.Size = new System.Drawing.Size(194, 20);
            this.TxtEstablishedYr.TabIndex = 29;
            // 
            // FrmVendorUpdateApprovalDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 390);
            this.Name = "FrmVendorUpdateApprovalDlg";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSDCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVilCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgContactPerson.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            this.TpgItemCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgVdSector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstablishedYr.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal DevExpress.XtraEditors.TextEdit TxtShortName;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.LookUpEdit LueSDCode;
        private DevExpress.XtraEditors.LookUpEdit LueVilCode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCityCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LblVdCtCode;
        private DevExpress.XtraEditors.LookUpEdit LueVdCtCode;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label15;
        protected internal DevExpress.XtraEditors.TextEdit TxtVdName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgContactPerson;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgBank;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.TabPage TpgItemCategory;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkTaxInd;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit TxtTIN;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtIdentityNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit TxtPhone;
        protected internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit TxtFax;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnVendor;
        private System.Windows.Forms.TabPage TpgVdSector;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal DevExpress.XtraEditors.TextEdit TxtWebsite;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        protected internal DevExpress.XtraEditors.LookUpEdit LueParent;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.TextEdit TxtEstablishedYr;
    }
}