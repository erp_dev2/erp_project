﻿#region Update
/*
    16/01/2018 try catch kena comment 
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    07/02/2019 [DITA] Tambah kolom property
    09/10/2019 [DITA/MAI] Saat insert buat PO baru Po request yang muncul hanya yang sesuai pic nya
    07/11/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom project name, project code
    17/02/2020 [TKG/IMS] tambah project code, project name
    27/05/2020 [TKG/IMS] tambah filter Local Document#, Item's Local Code
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    23/06/2021 [RDA/IMS] tambah param IsItCtFilteredByGroup untuk menampilkan item sesuai group login berdasarkan parameter
    25/10/2021 [WED/RM] tambah validasi TakeOrderInd berdasarkan parameter IsUseECatalog
    06/01/2022 [ISD/PHT] filterbygroup untuk site, department, dan itemcategory dengan parameter IsFilterBySite, IsFilterByDept, IsFilterByItCt
    20/01/2022 [RIS/PHT] Menambahkan addendum rate dan addendum availability
    02/02/2022 [RIS/PHT] Menambah field grand total after revision
    09/03/2023 [MAU/HEX] Menambahkan kolom MRType & tab Asset Register
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPO2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPO2 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;
        private bool mUseMaterialRequestRemarkInPO = false,
          mIsPORequestByPIC = false;

        #endregion

        #region Constructor

        public FrmPO2Dlg(FrmPO2 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",
                    //1-5
                    "", 
                    "Site Code",
                    "Site",
                    "Document#",
                    "DNo",
                    //6-10
                    "Local#",
                    "Date",
                    "Requested"+Environment.NewLine+"Date",
                    "Department",
                    "Item's Code",
                    //11-15
                    "Local Code",
                    "Item's Name",
                    "Foreign Name",
                    "ItScCode",
                    "Sub-Category",
                    //16-20
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "UoM",
                    "Quotation#",
                    "Quotation D No",
                    "Vendor Code",
                    //21-25
                    "Vendor", 
                    "Term of"+Environment.NewLine+"Payment",
                    "Currency",
                    "Unit Price",
                    "Delivery Type",
                    //26-30
                    "Remark",
                    "Property",
                    "Specification",
                    "Project's Code",
                    "Project's Name",
                    //31-33
                    "Production",
                    "Addendum",
                    "MR Type"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    20, 100, 120, 180, 80,
                    //6-10
                    150, 100, 120, 150, 120,
                    //11-15
                    180, 200, 200, 80, 150,
                    //16-20
                    100, 80, 150, 80, 80,
                    //21-25
                    200, 120, 100, 150, 150,
                    //26-30
                    400, 150, 300, 100, 200,
                    //31-33
                    130, 0, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 31 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,27,28, 29, 30, 31, 32, 33 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 24 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            
            if (mFrmParent.mIsShowForeignName)
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 18, 19, 20, 21, 22, }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 7, 8, 9, 10, 11, 14, 18, 19, 20, 21, 22, }, false);
                }
            }
            else
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 7, 8, 9, 10, 11, 14, 18, 19, 20, 21, 22 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 7, 8, 9, 10, 14, 16, 18, 19, 20, 21, 22 }, false);
                }
            }
            if (!mFrmParent.mPORequestPropCodeEnabled)
            {
               Sm.GrdColInvisible(Grd1, new int[] { 27 }, false);
            }
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            if (mFrmParent.mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
            Grd1.Cols[27].Move(14);
            if (mFrmParent.mIsPurchaseRequestForProductionEnabled)
                Sm.GrdColInvisible(Grd1, new int[] { 31 }, true);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 31 }, false);
            Sm.SetGrdProperty(Grd1, false);

            if (!mFrmParent.mIsBOMShowSpecifications)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29, 30 });
                Grd1.Cols[28].Move(15);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 11, 28, 29, 30 }, true);
                Grd1.Cols[28].Move(13);
            }

            
            Sm.GrdColInvisible(Grd1, new int[] { 33 }, false);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8, 9, 10, 18, 22 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, J.DeptName, D.ItCode, I.PurchaseUomCode, ");
            SQL.AppendLine("I.ItName, I.ForeignName, ");
            SQL.AppendLine("B.QtDocNo, B.QtDNo, E.VdCode, G.VdName, H.PtName, E.CurCode, F.UPrice, C.DocDt As MaterialRequestDocDt, K.DTName, ");
            SQL.AppendLine("B.Qty-IfNull(M.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("D.Remark, I.ItScCode, L.ItScName, A.LocalDocNo, A.SiteCode, O.SiteName, I.ItCodeInternal, P.PropName, I.Specification, ");
            SQL.AppendLine("Q.ProjectCode, Q.ProjectName, C.IsProduction, S.AddendumInd, T.OptDesc As MRType ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");

            if (mFrmParent.mIsUseECatalog) SQL.AppendLine("    And B.TakeOrderInd = 'Y' ");

            SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (C.SiteCode Is Null Or ( ");
                SQL.AppendLine("    C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D ");
            SQL.AppendLine("    On B.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("    And B.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    And IfNull(D.ProcessInd, '')<>'F' ");
            SQL.AppendLine("Inner Join TblQtHdr E On B.QtDocNo=E.DocNo And E.VdCode=@VdCode ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And E.PtCode Is Not Null ");
                SQL.AppendLine("And E.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl F On B.QtDocNo=F.DocNo And B.QtDNo=F.DNo ");
            SQL.AppendLine("Left Join TblVendor G On E.VdCode=G.VdCode ");
            SQL.AppendLine("Left Join TblPaymentTerm H On E.PtCode=H.PtCode ");
            SQL.AppendLine("Left Join TblItem I On D.ItCode=I.ItCode ");
            SQL.AppendLine("Left Join TblDepartment J On C.DeptCode=J.DeptCode ");
            SQL.AppendLine("Left Join TblDeliveryType K On E.DTCode=K.DTCode ");
            SQL.AppendLine("Left Join TblItemSubCategory L On I.ItScCode=L.ItScCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PORequestDocNo As DocNo, T1.PORequestDNo As DNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("    From TblPODtl T1 ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T3 ");
            SQL.AppendLine("        On T2.MaterialRequestDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.MaterialRequestDNo=T3.DNo ");
            SQL.AppendLine("        And IfNull(T3.ProcessInd, '')<>'F' ");
            SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblQtHdr T4 On T2.QtDocNo=T4.DocNo And T4.VdCode=@VdCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.PORequestDocNo, T1.PORequestDNo ");
            SQL.AppendLine(") M On A.DocNo=M.DocNo And B.DNo=M.DNo ");
            SQL.AppendLine("Left Join TblSite O On A.SiteCode=O.SiteCode ");
            SQL.AppendLine("Left Join TblProperty P On B.PropCode=P.PropCode ");
            SQL.AppendLine("Left Join TblProjectGroup Q On C.PGCode=Q.PGCode ");
            SQL.AppendLine("Left Join TblItemCategory R On I.ItCtCode=R.ItCtCode ");
            SQL.AppendLine("Left Join TblIndependentEstimatedPriceHdr S On D.DocNo = S.MRDocNo");
            SQL.AppendLine("LEFT JOIN tbloption T ON C.MRType = T.OptCode AND T.OptCat = 'MRType' ");


            SQL.AppendLine("Where B.Qty-IfNull(M.Qty, 0)>0  ");
            if (mIsPORequestByPIC)
                SQL.AppendLine(" And C.PICCode = @UserCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=I.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=J.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=O.SiteCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedPORequest() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "E.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "I.ItName", "I.ForeignName", "I.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                        //0
                        "SiteCode", 
                        //1-5
                        "SiteName", "DocNo", "DNo", "LocalDocNo", "DocDt",   
                        //6-10
                        "MaterialRequestDocDt", "DeptName", "ItCode", "ItCodeInternal", "ItName",
                        //11-15
                        "ForeignName",  "ItScCode", "ItScName", "OutstandingQty", "PurchaseUomCode",
                        //16-20
                        "QtDocNo", "QtDNo", "VdCode", "VdName", "PtName",   
                        //21-25
                        "CurCode", "UPrice", "DTName", "Remark", "PropName",
                        //26-30
                        "Specification", "ProjectCode", "ProjectName", "IsProduction", "AddendumInd",

                        //31
                        "MRType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        if (mUseMaterialRequestRemarkInPO)
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        else
                            Grd.Cells[Row, 26].Value = null;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 31, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);

                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }



        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 47, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 41, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 42, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 43, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 46, Grd1, Row2, 32);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 53, Grd1, Row2, 33);

                        mFrmParent.Grd1.Cells[Row1, 0].Value = null;

                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, Row1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, Row1, new int[] { 26, 27, 28, 29, 30, 48, 49, 50, 51, 52 });
                    
                        mFrmParent.ComputeTotal(Row1);
                        mFrmParent.ComputeTaxAmt();

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 19, 26, 27, 28, 29, 30, 43, 47, 48, 49, 50, 51, 52 });
                        mFrmParent.AddendumCheck();

                        var mMRType = Sm.GetGrdStr(Grd1, Row, 33);
                        if (mMRType == "IR")
                        {
                            mFrmParent.SetAssetReg();
                            
                        }
                        else
                        {
                            mFrmParent.SetAssetRegisterReadOnly();
                        }
                            
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

            mFrmParent.SetSeqNo();
        }


        private bool IsItCodeAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 4) + Sm.GetGrdStr(Grd1, Row, 5);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    key, Sm.GetGrdStr(mFrmParent.Grd1, Index, 7) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 8)
                    )) return true;
            return false;
        }

        private void GetParameter()
        {
            mUseMaterialRequestRemarkInPO = Sm.GetParameterBoo("UseMaterialRequestRemarkInPO");
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsPORequestByPIC = Sm.GetParameterBoo("IsPORequestByPIC");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtMaterialRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMaterialRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO request document number");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

        #region Class
        private class AssetRegister
        {
            public string ItName { set; get; }
            public decimal Qty { set; get; }
            public string DocNo { set; get; }
            public string DNo { set; get; }
            
            
        }
        #endregion
    }
}
