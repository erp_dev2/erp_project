﻿#region Update
/*
    20/02/2020 [WED/IMS] bikin revision
    03/03/2020 [TKG/IMS] diambil bukan dari bom
    17/06/2021 [VIN/IMS] penyesuaian GetSelectedBOQData, & GetSelectedBOQData2
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract2Dlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContract2 mFrmParent;
        private string mSQL = string.Empty, mBOQDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract2Dlg6(FrmSOContract2 FrmParent, string BOQDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBOQDocNo = BOQDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "BOM#",
                    "BOMDNo",
                    "Item Code",
                    "Item Name",

                    //6-10
                    "Item Local Code",
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Cost"+Environment.NewLine+"of Goods",
                    "After Sales",

                    //11-15
                    "COM",
                    "OH",
                    "Margin",
                    "Design Cost",
                    "Unit Price"+Environment.NewLine+"(+ PPH)",

                    //16-20
                    "Space Nego",
                    "Unit Price"+Environment.NewLine+"(SPH)",
                    "Total Price"+Environment.NewLine+"(SPH)",
                    "Remark",
                    "SeqNo"
                },
                new int[] 
                {
                    50,
                    20, 150, 80, 100, 180, 
                    180, 120, 100, 120, 120, 
                    120, 120, 120, 120, 120, 
                    120, 120, 120, 200, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 20 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BOMDocNo, A.SeqNo, A.BOMDNo, B.ItCode, B.ItName, B.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("B.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQL.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, A.Remark ");
            SQL.AppendLine("From TblBOQDtl3 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.ItCode, A.SeqNo, '##'), @SelectedData) < 1 ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mBOQDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedData", mFrmParent.GetSelectedBOQData2());

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "B.ItName", "B.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " ;",
                    new string[] 
                    { 
                        //0
                        "BOMDocNo",
                    
                        //1-5
                        "BOMDNo", "ItCode", "ItName", "ItCodeInternal", "Qty", 
            
                        //6-10
                        "PurchaseUomCode", "CostOfGoodsAmt", "AfterSalesAmt", "COMAmt", "OHAmt", 
            
                        //11-15
                        "MarginAmt", "DesignCostAmt", "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", 

                        //16-17
                        "TotalPriceSPHAmt", "Remark", "SeqNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsBomDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd6.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 16, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 17, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 18, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 19, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 20, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 21, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 26, Grd1, Row2, 20);

                        mFrmParent.ComputeTotalBOM(mFrmParent.IsInsert);
                        mFrmParent.ComputeItem(mFrmParent.IsInsert);
                        mFrmParent.Grd6.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd6, mFrmParent.Grd6.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 19, 20, 21 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsBomDataAlreadyChosen(int Row)
        {
            string key = string.Concat(mBOQDocNo, Sm.GetGrdStr(Grd1, Row, 4));
            for (int Index = 0; Index < mFrmParent.Grd6.Rows.Count - 1; Index++)
                if (Sm.CompareStr(string.Concat(mFrmParent.TxtBOQDocNo.Text, Sm.GetGrdStr(mFrmParent.Grd6, Index, 3), Sm.GetGrdStr(mFrmParent.Grd6, Index, 26)), key)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
