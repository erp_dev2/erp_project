﻿#region Update
/*
    21/07/2022 [RIS/SIER] New Apps
    25/08/2022 [RDA/SIER] feedback dept for budget seharusnya editable sehabis copy data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5Dlg10 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest5 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmMaterialRequest5Dlg10(FrmMaterialRequest5 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDept ? "Y" : "N");
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Cancel",
                        "Status",
                        "Process",

                        //6-10
                        "Local Document#",
                        "Local Code",
                        "Foreign Name",
                        "Site",
                        "Division",
 
                        //11-15
                        "Department",
                        "PIC",
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Item's Name",
                        
                        //16-20
                        "Location",
                        "Specification",
                        "Quantity",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "UoM",

                        //21-25
                        "Estimated Price",
                        "Total Price",
                        "Total"+Environment.NewLine+"Estimated Price",
                        "Budget Category",
                        "Usage"+Environment.NewLine+"Date",

                        //26-30
                        "Item's Remark",
                        "Document's Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",

                        //31-33
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"

                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 100, 100, 
                        
                        //6-10
                        150, 150, 150, 150, 150, 
                        
                        //11-15
                        200, 150, 100, 20, 250,

                        //16-20
                        200, 300, 100, 100, 80, 
                        
                        // 21-25
                        100, 100, 100, 150, 100,

                        //26-30
                        300, 300, 100, 100, 100,

                        //31-33
                        100, 100, 100,

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 21, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 25, 29, 32 });
            Sm.GrdFormatTime(Grd1, new int[] { 30, 33 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 14, 21, 22, 28, 29, 30, 31, 32, 33 }, false);

            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 17, 19 });
            if (!mIsFilterBySite) Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            if (!mFrmParent.mIsMRShowEstimatedPrice) Sm.GrdColInvisible(Grd1, new int[] { 21 }, false);
            if (!mFrmParent.mIsMRShowTotalPrice) Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            if (!mFrmParent.mIsMRShowTotalEstimatedPrice) Sm.GrdColInvisible(Grd1, new int[] { 23 }, false);
            if (!mFrmParent.mIsMRSPPJB) Sm.GrdColInvisible(Grd1, new int[] { 16 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 13, 14, 21, 22, 28, 29, 30, 31, 32, 33 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DeptName, B.CancelInd, G.BCName, A.LocalDocNo, ");
            SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("Case A.ProcessInd When 'D' Then 'Draft' Else 'Final' End As ProcessInd, ");
            SQL.AppendLine("B.ItCode, B.Qty, D.PurchaseUomCode, ");
            SQL.AppendLine("D.ItName, E.SiteName, B.UsageDt, B.Remark As DRemark, A.Remark As HRemark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, D.ForeignName, D.ItCodeInternal, F.UserName, D.Specification, H.Qty As CancelledQty,  ");
            SQL.AppendLine("B.EstPrice, B.TotalPrice, I.TotalEstPrice, J.DivisionName, B.Location ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And A.DocType = '1' ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblBudgetCategory G on A.BCCode=G.BCCode ");

            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblUser F On A.PICCode=F.UserCode ");
            SQL.AppendLine("Left Join TblMRQtyCancel H ON H.MRDocNo=B.DocNo AND H.MRDNo=B.DNo AND H.CancelInd='N' ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT A.DocNo, SUM(B.TotalPrice) AS TotalEstPrice ");
            SQL.AppendLine("    FROM tblmaterialrequesthdr A ");
            SQL.AppendLine("    INNER JOIN TblMaterialRequestDtl B On A.DocNo=B.DocNo And A.DocType = '1' ");
            SQL.AppendLine("    GROUP BY A.DocNo ");
            SQL.AppendLine(")I ON A.DocNo = I.DocNo ");
            SQL.AppendLine("Left Join TblDivision J On A.DivisionCode=J.DivisionCode ");
            SQL.AppendLine("Where A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsMRSPPJBEnabled)
            {
                if (mFrmParent.mIsMRSPPJB)
                    SQL.AppendLine("And A.SPPJBInd='Y' ");
                else
                    SQL.AppendLine("And A.SPPJBInd='N' ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And A.Status In ('O', 'A') And A.CancelInd='N' And B.Status In ('O', 'A') And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "ProcessInd", "LocalDocNo", 

                            //6-10
                            "ItCodeInternal", "ForeignName", "SiteName", "DivisionName", "DeptName",  
                            
                            //11-15
                            "UserName", "ItCode", "ItName", "Location", "Specification",
                            
                            //16-20
                            "Qty", "CancelledQty", "PurchaseUomCode", "EstPrice", "TotalPrice", 
                            
                            //21-25
                            "TotalEstPrice", "BCName", "UsageDt", "DRemark", "HRemark",   

                            //26-29
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 33, 29);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.CopyData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> {  mFrmParent.LueDeptCode2 }, false);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 13));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 13));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(
                Sl.SetLueDeptCode),
                string.Empty,
                mFrmParent.mIsFilterByDept ? "Y" : "N"
                );
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Document#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
