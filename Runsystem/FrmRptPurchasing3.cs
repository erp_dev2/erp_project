﻿#region Update
/*
    17/05/2017 [ARI] tambah kolom MR Date, PORequest Date, PO Date, ReceivVd Date, dan Site
    29/05/2018 [TKG] Data PO yg ditampilkan hanya yang masih outstanding atau yg sudah di-approved
    08/05/2019 [TKG] Jika IsNoOfDaysPurchasedItemComparisonReportShowDoc=N, department akan ikut diproses berdasarkan filter department.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasing3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = false,
            mIsShowForeignName = false, 
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasing3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                panel4.Visible = mIsNoOfDaysPurchasedItemComparisonReportShowDoc;
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode);
                if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
                    LblMth.ForeColor = Color.Black;
                else
                    LblMth.ForeColor = Color.Red;
                base.FrmLoad(sender, e);
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = Sm.GetParameterBoo("IsNoOfDaysPurchasedItemComparisonReportShowDoc");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                SQL.AppendLine("Select T1.ItCode, T6.ItName, T6.ForeignName, ");
                SQL.AppendLine("T1.MaterialRequestDocNo, T1.MaterialRequestDocDt, T3.PORequestDocNo, T3.PORequestDocDt, T4.PODocNo, T4.PODocDt, ");
                SQL.AppendLine("T5.RecvVdDocNo, T5.RecvVdDocDt,");
                SQL.AppendLine("IfNull(T2.Value, 0) As MRvsUsage, ");
                SQL.AppendLine("IfNull(T3.Value, 0) As MRvsPORequest, ");
                SQL.AppendLine("IfNull(T4.Value, 0) As MRvsPO, ");
                SQL.AppendLine("IfNull(T4.Value2, 0) As MRvsETA, ");
                SQL.AppendLine("IfNull(T5.Value, 0) As MRvsRecv, ");
                SQL.AppendLine("IfNull(T3.Value2, 0) As UsagevsPORequest, ");
                SQL.AppendLine("IfNull(T4.Value3, 0) As UsagevsPO, ");
                SQL.AppendLine("IfNull(T4.Value4, 0) As UsagevsETA, ");
                SQL.AppendLine("IfNull(T5.Value2, 0) As UsagevsRecv, ");
                SQL.AppendLine("IfNull(T4.Value5, 0) As PORequestvsPO, ");
                SQL.AppendLine("IfNull(T4.Value6, 0) As PORequestvsETA, ");
                SQL.AppendLine("IfNull(T5.Value3, 0) As PORequestvsRecv, ");
                SQL.AppendLine("IfNull(T5.Value4, 0) As RecvvsETA, ");
                SQL.AppendLine("IfNull(T5.Value5, 0) As PovsRecv, ");
                SQL.AppendLine("T1.SiteName ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                if(LueMth.EditValue == null)
                    SQL.AppendLine("    Where Left(A.DocDt, 4)=@YrMth ");
                else
                    SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");

                if (LueMth.EditValue == null)
                    SQL.AppendLine("    Where Left(A.DocDt, 4)=@YrMth ");
                else
                    SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");

                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");

                if (LueMth.EditValue == null)
                    SQL.AppendLine("    Where Left(A.DocDt, 4)=@YrMth ");
                else
                    SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");

                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo And F.Status In ('O', 'A') ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");

                if (LueMth.EditValue == null)
                    SQL.AppendLine("    Where Left(A.DocDt, 4)=@YrMth ");
                else
                    SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");

                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo And F.Status In ('O', 'A') ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");

                if (LueMth.EditValue == null)
                    SQL.AppendLine("    Where Left(A.DocDt, 4)=@YrMth ");
                else
                    SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");

                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");
                SQL.AppendLine("Inner Join TblItem T6 On T1.ItCode=T6.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T6.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("Select T1.ItCode, T6.ItName, T6.ForeignName, ");
                SQL.AppendLine("Null As MaterialRequestDocNo, Null As MaterialRequestDocDt, Null As PORequestDocNo, Null As PORequestDocDt, Null As PODocNo, ");
                SQL.AppendLine("Null As PODocDt, Null As RecvVdDocNo, Null As RecvVdDocDt,");
                SQL.AppendLine("IfNull(T2.Value, 0) As MRvsUsage, ");
                SQL.AppendLine("IfNull(T3.Value, 0) As MRvsPORequest, ");
                SQL.AppendLine("IfNull(T4.Value, 0) As MRvsPO, ");
                SQL.AppendLine("IfNull(T4.Value2, 0) As MRvsETA, ");
                SQL.AppendLine("IfNull(T5.Value, 0) As MRvsRecv, ");
                SQL.AppendLine("IfNull(T3.Value2, 0) As UsagevsPORequest, ");
                SQL.AppendLine("IfNull(T4.Value3, 0) As UsagevsPO, ");
                SQL.AppendLine("IfNull(T4.Value4, 0) As UsagevsETA, ");
                SQL.AppendLine("IfNull(T5.Value2, 0) As UsagevsRecv, ");
                SQL.AppendLine("IfNull(T4.Value5, 0) As PORequestvsPO, ");
                SQL.AppendLine("IfNull(T4.Value6, 0) As PORequestvsETA, ");
                SQL.AppendLine("IfNull(T5.Value3, 0) As PORequestvsRecv, ");
                SQL.AppendLine("IfNull(T5.Value4, 0) As RecvvsETA, ");
                SQL.AppendLine("IfNull(T5.Value5, 0) As POvsRecv, Null As SiteName ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct B.ItCode ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.CancelInd='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo And F.Status In ('O', 'A') ");
                SQL.AppendLine("        Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo And F.Status In ('O', 'A') ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where Left(A.DocDt, 6)=@YrMth ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");
                SQL.AppendLine("Inner Join TblItem T6 On T1.ItCode=T6.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T6.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Year",
                        "Month", 
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "MR"+Environment.NewLine+"vs"+Environment.NewLine+"Usage",   
                        
                        //6-10
                        "MR"+Environment.NewLine+"vs"+Environment.NewLine+"PO Request", 
                        "MR"+Environment.NewLine+"vs"+Environment.NewLine+"PO",
                        "MR"+Environment.NewLine+"vs"+Environment.NewLine+"ETA", 
                        "MR"+Environment.NewLine+"vs"+Environment.NewLine+"Received", 
                        "Usage"+Environment.NewLine+"vs"+Environment.NewLine+"PO Request", 

                        //11-15
                        "Usage"+Environment.NewLine+"vs"+Environment.NewLine+"PO", 
                        "Usage"+Environment.NewLine+"vs"+Environment.NewLine+"ETA",
                        "Usage"+Environment.NewLine+"vs"+Environment.NewLine+"Received", 
                        "PO Request"+Environment.NewLine+"vs"+Environment.NewLine+"PO", 
                        "PO Request"+Environment.NewLine+"vs"+Environment.NewLine+"ETA", 

                        //16-20
                        "PO Request"+Environment.NewLine+"vs"+Environment.NewLine+"Received", 
                        "Received"+Environment.NewLine+"vs"+Environment.NewLine+" ETA",  
                        "Material Request#",
                        "PO Request#",
                        "PO#",

                        //21-25
                        "Received#",
                        "Foreign Name",
                        "PO"+Environment.NewLine+"vs"+Environment.NewLine+"Received",
                        "Material Request"+Environment.NewLine+"Date",
                        "PO Request"+Environment.NewLine+"Date",
                        
                        //26-28
                        "PO Date",
                        "Received"+Environment.NewLine+"Date",
                        "Site"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        50, 50, 100, 250, 60,
                        
                        //6-10
                        80, 60, 60, 80, 80, 
                        
                        //11-15
                        80, 80, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 130, 130, 130, 
                        
                        //21-25
                        150, 130, 80, 100, 100,

                        //26-28
                        100, 100, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 24, 25, 26, 27 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 23 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, false);
            Grd1.Cols[23].Move(17);
            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                Grd1.Cols[28].Move(5);
                Grd1.Cols[18].Move(6);
                Grd1.Cols[24].Move(7);
                Grd1.Cols[19].Move(8);
                Grd1.Cols[25].Move(9);
                Grd1.Cols[20].Move(10);
                Grd1.Cols[26].Move(11);
                Grd1.Cols[21].Move(12);
                Grd1.Cols[27].Move(13);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 24, 25, 26, 27, 28 }, false);
            if (mIsShowForeignName)
                Grd1.Cols[22].Move(5);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);
            
            if (Yr.Length==0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }
            if (Mth.Length == 0 && (!mIsNoOfDaysPurchasedItemComparisonReportShowDoc))
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter1 = string.Empty, Filter2 = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@YrMth", Yr+Mth);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueItCtCode), "T6.ItCtCode", true);
                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "T1.ItCode", "T6.ItName", "T6.ForeignName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter1, ref cm, TxtDocNo.Text, "T1.MaterialRequestDocNo", false);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter2) + Filter1 + " Order By T6.ItName;",
                    new string[]
                    { 
                        //0
                        "ItCode", 
                        
                        //1-5
                        "ItName", "MRvsUsage", "MRvsPORequest", "MRvsPO", "MRvsETA", 
                        
                        //6-10
                        "MRvsRecv", "UsagevsPORequest", "UsagevsPO", "UsagevsETA", "UsagevsRecv", 
                        
                        //11-15
                        "PORequestvsPO", "PORequestvsETA", "PORequestvsRecv", "RecvVsETA", "MaterialRequestDocNo", 
                        
                        //16-20
                        "PORequestDocNo", "PODocNo", "RecvVdDocNo", "ForeignName", "POvsRecv",

                        //21-25
                        "MaterialRequestDocDt", "PORequestDocDt", "PODocDt", "RecvVdDocDt", "SiteName"
                    
                    },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = Yr;
                            Grd.Cells[Row, 2].Value = Mth;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material Request#");
        }

        #endregion       
    }
}
