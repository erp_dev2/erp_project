﻿#region Update
/*
    17/05/2018 [TKG] tambah coa utk depresiasi aset
    22/05/2018 [TKG] menghilangkan coa
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAssetCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmAssetCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Asset Category";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAssetCategoryCode, TxtAssetCategoryName, ChkActInd }, true);
                    TxtAssetCategoryCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAssetCategoryCode, TxtAssetCategoryName }, false);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    TxtAssetCategoryCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtAssetCategoryCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd, TxtAssetCategoryName }, false);
                    TxtAssetCategoryName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtAssetCategoryCode, TxtAssetCategoryName });
            ChkActInd.Checked = false;
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssetCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAssetCategoryCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtAssetCategoryCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAssetCategory Where AssetCategoryCode=@AssetCategoryCode" };
                Sm.CmParam<String>(ref cm, "@AssetCategoryCode", TxtAssetCategoryCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblAssetCategory(AssetCategoryCode, AssetCategoryName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@AssetCategoryCode, @AssetCategoryName, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update AssetCategoryName=@AssetCategoryName, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@AssetCategoryCode", TxtAssetCategoryCode.Text);
                Sm.CmParam<String>(ref cm, "@AssetCategoryName", TxtAssetCategoryName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtAssetCategoryCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AssetCategoryCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@AssetCategoryCode", AssetCategoryCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select AssetCategoryCode, AssetCategoryName, ActInd ");
                SQL.AppendLine("From TblAssetCategory ");
                SQL.AppendLine("Where AssetCategoryCode=@AssetCategoryCode;");
                
                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        {
                            "AssetCategoryCode", 
                            "AssetCategoryName", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtAssetCategoryCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtAssetCategoryName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAssetCategoryCode, "Asset category code", false) ||
                Sm.IsTxtEmpty(TxtAssetCategoryName, "Asset category name", false) ||
                IsAssetCategoryCodeExisted();
        }

        private bool IsAssetCategoryCodeExisted()
        {
            if (!TxtAssetCategoryCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select AssetCategoryCode From TblAssetCategory Where AssetCategoryCode=@AssetCategoryCode;";
                Sm.CmParam<String>(ref cm, "@AssetCategoryCode", TxtAssetCategoryCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Asset category code ( " + TxtAssetCategoryCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtAssetCategoryCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAssetCategoryCode);
        }

        private void TxtAssetCategoryName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAssetCategoryName);
        }

        #endregion

        #endregion
    }
}
