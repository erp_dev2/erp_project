﻿namespace RunSystem
{
    partial class FrmPOQtyCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPOQtyCancel));
            this.BtnPODocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPODocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPODNo = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtQtyPO = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtCancelledQtyPO = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtOutstandingQtyPO = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkNewMRInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.BtnMRDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtMRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyPO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCancelledQtyPO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingQtyPO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewMRInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(619, 0);
            this.panel1.Size = new System.Drawing.Size(70, 323);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnMRDocNo);
            this.panel2.Controls.Add(this.TxtMRDocNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.ChkNewMRInd);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtQtyPO);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtCancelledQtyPO);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtOutstandingQtyPO);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtPODNo);
            this.panel2.Controls.Add(this.BtnPODocNo);
            this.panel2.Controls.Add(this.TxtPODocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(619, 323);
            // 
            // BtnPODocNo
            // 
            this.BtnPODocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPODocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPODocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPODocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPODocNo.Appearance.Options.UseBackColor = true;
            this.BtnPODocNo.Appearance.Options.UseFont = true;
            this.BtnPODocNo.Appearance.Options.UseForeColor = true;
            this.BtnPODocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPODocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPODocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPODocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPODocNo.Image")));
            this.BtnPODocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPODocNo.Location = new System.Drawing.Point(321, 75);
            this.BtnPODocNo.Name = "BtnPODocNo";
            this.BtnPODocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPODocNo.TabIndex = 19;
            this.BtnPODocNo.ToolTip = "Find PO";
            this.BtnPODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPODocNo.ToolTipTitle = "Run System";
            this.BtnPODocNo.Click += new System.EventHandler(this.BtnPODocNo_Click);
            // 
            // TxtPODocNo
            // 
            this.TxtPODocNo.EnterMoveNextControl = true;
            this.TxtPODocNo.Location = new System.Drawing.Point(137, 76);
            this.TxtPODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPODocNo.Name = "TxtPODocNo";
            this.TxtPODocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPODocNo.Properties.MaxLength = 30;
            this.TxtPODocNo.Properties.ReadOnly = true;
            this.TxtPODocNo.Size = new System.Drawing.Size(181, 20);
            this.TxtPODocNo.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(101, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "PO#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(323, 10);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(137, 32);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(113, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(100, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(137, 10);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(181, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(60, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPODNo
            // 
            this.TxtPODNo.AutoSize = true;
            this.TxtPODNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPODNo.ForeColor = System.Drawing.Color.Red;
            this.TxtPODNo.Location = new System.Drawing.Point(394, 14);
            this.TxtPODNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtPODNo.Name = "TxtPODNo";
            this.TxtPODNo.Size = new System.Drawing.Size(46, 14);
            this.TxtPODNo.TabIndex = 9;
            this.TxtPODNo.Text = "PODNo";
            this.TxtPODNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(137, 98);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(450, 20);
            this.TxtVdCode.TabIndex = 21;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(86, 100);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 14);
            this.label22.TabIndex = 20;
            this.label22.Text = "Vendor";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(137, 142);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(450, 20);
            this.TxtItName.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(65, 145);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 14);
            this.label10.TabIndex = 24;
            this.label10.Text = "Item Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(137, 120);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(181, 20);
            this.TxtItCode.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(68, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 14);
            this.label5.TabIndex = 22;
            this.label5.Text = "Item Code";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtyPO
            // 
            this.TxtQtyPO.EnterMoveNextControl = true;
            this.TxtQtyPO.Location = new System.Drawing.Point(137, 164);
            this.TxtQtyPO.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtyPO.Name = "TxtQtyPO";
            this.TxtQtyPO.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQtyPO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtyPO.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtyPO.Properties.Appearance.Options.UseFont = true;
            this.TxtQtyPO.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtyPO.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtyPO.Properties.ReadOnly = true;
            this.TxtQtyPO.Size = new System.Drawing.Size(113, 20);
            this.TxtQtyPO.TabIndex = 27;
            this.TxtQtyPO.Validated += new System.EventHandler(this.TxtQtyPO_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(79, 167);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 14);
            this.label20.TabIndex = 26;
            this.label20.Text = "Quantity";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCancelledQtyPO
            // 
            this.TxtCancelledQtyPO.EnterMoveNextControl = true;
            this.TxtCancelledQtyPO.Location = new System.Drawing.Point(137, 208);
            this.TxtCancelledQtyPO.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCancelledQtyPO.Name = "TxtCancelledQtyPO";
            this.TxtCancelledQtyPO.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCancelledQtyPO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCancelledQtyPO.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCancelledQtyPO.Properties.Appearance.Options.UseFont = true;
            this.TxtCancelledQtyPO.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCancelledQtyPO.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCancelledQtyPO.Size = new System.Drawing.Size(113, 20);
            this.TxtCancelledQtyPO.TabIndex = 31;
            this.TxtCancelledQtyPO.Validated += new System.EventHandler(this.TxtCancelledPO_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(24, 211);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 14);
            this.label13.TabIndex = 30;
            this.label13.Text = "Cancelled Quantity";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOutstandingQtyPO
            // 
            this.TxtOutstandingQtyPO.EnterMoveNextControl = true;
            this.TxtOutstandingQtyPO.Location = new System.Drawing.Point(137, 186);
            this.TxtOutstandingQtyPO.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstandingQtyPO.Name = "TxtOutstandingQtyPO";
            this.TxtOutstandingQtyPO.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstandingQtyPO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingQtyPO.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstandingQtyPO.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstandingQtyPO.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstandingQtyPO.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstandingQtyPO.Properties.ReadOnly = true;
            this.TxtOutstandingQtyPO.Size = new System.Drawing.Size(113, 20);
            this.TxtOutstandingQtyPO.TabIndex = 29;
            this.TxtOutstandingQtyPO.Validated += new System.EventHandler(this.TxtOutstandingQtyPO_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(8, 190);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 14);
            this.label9.TabIndex = 28;
            this.label9.Text = "Outstanding Quantity";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(137, 252);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(450, 20);
            this.MeeRemark.TabIndex = 33;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(86, 255);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkNewMRInd
            // 
            this.ChkNewMRInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkNewMRInd.Location = new System.Drawing.Point(137, 274);
            this.ChkNewMRInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkNewMRInd.Name = "ChkNewMRInd";
            this.ChkNewMRInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkNewMRInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNewMRInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkNewMRInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkNewMRInd.Properties.Appearance.Options.UseFont = true;
            this.ChkNewMRInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkNewMRInd.Properties.Caption = "Need To Issue Material Request";
            this.ChkNewMRInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNewMRInd.ShowToolTips = false;
            this.ChkNewMRInd.Size = new System.Drawing.Size(204, 22);
            this.ChkNewMRInd.TabIndex = 34;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(137, 54);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(181, 20);
            this.TxtLocalDocNo.TabIndex = 16;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(90, 57);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 14);
            this.label11.TabIndex = 15;
            this.label11.Text = "Local#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnMRDocNo
            // 
            this.BtnMRDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMRDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMRDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMRDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMRDocNo.Appearance.Options.UseBackColor = true;
            this.BtnMRDocNo.Appearance.Options.UseFont = true;
            this.BtnMRDocNo.Appearance.Options.UseForeColor = true;
            this.BtnMRDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnMRDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMRDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMRDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnMRDocNo.Image")));
            this.BtnMRDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMRDocNo.Location = new System.Drawing.Point(321, 229);
            this.BtnMRDocNo.Name = "BtnMRDocNo";
            this.BtnMRDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnMRDocNo.TabIndex = 40;
            this.BtnMRDocNo.ToolTip = "Show Material Request";
            this.BtnMRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMRDocNo.ToolTipTitle = "Run System";
            this.BtnMRDocNo.Click += new System.EventHandler(this.BtnMRDocNo_Click);
            // 
            // TxtMRDocNo
            // 
            this.TxtMRDocNo.EnterMoveNextControl = true;
            this.TxtMRDocNo.Location = new System.Drawing.Point(137, 230);
            this.TxtMRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMRDocNo.Name = "TxtMRDocNo";
            this.TxtMRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtMRDocNo.Properties.MaxLength = 30;
            this.TxtMRDocNo.Properties.ReadOnly = true;
            this.TxtMRDocNo.Size = new System.Drawing.Size(181, 20);
            this.TxtMRDocNo.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(27, 233);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 14);
            this.label3.TabIndex = 38;
            this.label3.Text = "Material Request#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmPOQtyCancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 323);
            this.Name = "FrmPOQtyCancel";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyPO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCancelledQtyPO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingQtyPO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewMRInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnPODocNo;
        public DevExpress.XtraEditors.TextEdit TxtPODocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.TextEdit TxtQtyPO;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.TextEdit TxtCancelledQtyPO;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.TextEdit TxtOutstandingQtyPO;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label TxtPODNo;
        private DevExpress.XtraEditors.CheckEdit ChkNewMRInd;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.SimpleButton BtnMRDocNo;
        public DevExpress.XtraEditors.TextEdit TxtMRDocNo;
        private System.Windows.Forms.Label label3;
    }
}