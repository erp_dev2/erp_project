﻿#region Update
/*
    06/07/2022 [BRI/PRODUCT] new apps base on COA
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCOA2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mAcNo = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmCOA2Find FrmFind;
        private bool mIsCOAUseAlias = false;
        internal bool mIsCOAFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmCOA2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                BtnPrint.Visible = false;
                if (mIsCOAUseAlias)
                {
                    LblAlias.ForeColor = Color.Red;
                }
                SetFormControl(mState.View);
                Sl.SetLueAcNo(ref LueParent);
                Sl.SetLueAcType(ref LueAcType);
                SetLueEntCode(ref LueEntCode);
                SetGrd();
                if (mAcNo.Length != 0)
                {
                    ShowData(mAcNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Entity Code", "Entity Name", "", "Local Account#", "Local Account Description" },
                    new int[]{ 0, 200, 20, 200, 200 }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAcNo, TxtAcDesc, LueParent, TxtLevel, LueAcType, ChkActInd, TxtAlias, LueEntCode
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtAcNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAcNo,TxtAcDesc, LueParent, TxtLevel, LueAcType, TxtAlias, LueEntCode
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtAcNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtAcNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtAcDesc, LueParent, TxtLevel, LueAcType, ChkActInd, TxtAlias, LueEntCode
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtAcDesc.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtAcNo,TxtAcDesc, LueParent, TxtLevel, LueAcType, TxtAlias, LueEntCode
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLevel }, 11);
        }
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCOA2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueAcNo(ref LueParent);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAcNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            try
            {
                if (IsJournalExisted()) return;
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(DeleteCOA(TxtAcNo.Text));

                Sm.ExecCommands(cml);

                BtnCancelClick(sender, e);
                               
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveCOA());

                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        cml.Add(SaveCOADtl(ref Grd1, TxtAcNo.Text, Row));
                }

                if (ChkActInd.Checked == false)
                    cml.Add(UpdateChildActInd());

                Sm.ExecCommands(cml);

                ShowData(TxtAcNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AcNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                Sl.SetLueAcNo(ref LueParent);
                ShowCOA(AcNo);
                ShowCOADtl(ref Grd1, AcNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCOA(string AcNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "SELECT AcNo, AcDesc, Alias, EntCode, ActInd, Parent, Level, AcType FROM TblCOA WHERE AcNo=@AcNo;",
                    new string[] { "AcNo", "AcDesc", "EntCode", "Alias", "ActInd", "Parent", "Level", "AcType" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[2]));
                        TxtAlias.EditValue = Sm.DrStr(dr, c[3]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[4])=="Y";
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[5]));
                        TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 11);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[7]));
                    }, true
                );
        }

        private void ShowCOADtl(ref iGrid GrdTemp, string AcNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.EntCode, B.EntName, A.LocalAcNo, A.LocalAcDesc ");
            SQL.AppendLine("FROM TblCOADtl2 A ");
            SQL.AppendLine("INNER JOIN TblEntity B ON A.EntCode = B.EntCode ");
            SQL.AppendLine("WHERE A.AcNo=@AcNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[] { "EntCode", "EntName", "LocalAcNo", "LocalAcDesc" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAcNo, "Account#", false) ||
                Sm.IsTxtEmpty(TxtAcDesc, "Account description", false) ||
                Sm.IsLueEmpty(LueEntCode, "Entity") ||
                (mIsCOAUseAlias && Sm.IsTxtEmpty(TxtAlias, "Alias", false)) ||
                Sm.IsTxtEmpty(TxtLevel, "Level", true) ||
                Sm.IsLueEmpty(LueAcType, "Account type") ||
                IsAcNoExisted() ||
                IsInactiveCOAInvalid1() ||
                IsInactiveCOAInvalid2() ||
                IsParentInvalid1() ||
                IsParentInvalid2() ||
                IsLocalCOAIsInvalid() ||
                IsLocalCOAIsInvalid2();
        }

        private bool IsParentInvalid1()
        {
            var Parent = Sm.GetLue(LueParent);
            if (Parent.Length == 0) return false;

            if (Sm.IsDataExist("Select 1 From TblJournalDtl Where AcNo=@Param Limit 1;", Parent))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Parent# : " + Sm.GetLue(LueParent) + Environment.NewLine +
                    "Parent Description : " + LueParent.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This account's parent# already used in existing journal.");
                return true;
            }
            return false;
        }

        private bool IsParentInvalid2()
        {
            var Parent = Sm.GetLue(LueParent);
            if (Parent.Length == 0) return false;

            if (Sm.IsDataExist(
                "Select 1 From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B " +
                "Where A.DocNo=B.DocNo " +
                "And A.CancelInd='N' " +
                "And B.AcNo=@Param " +
                "And B.AcNo Not In (Select Parent From TblCOA Where IfNull(Parent, '')<>'') " +
                "Limit 1;", Parent))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Parent# : " + Sm.GetLue(LueParent) + Environment.NewLine +
                    "Parent Description : " + LueParent.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This account's parent# already used in existing opening balance.");
                return true;
            }
            return false;
        }

        private bool IsInactiveCOAInvalid2()
        {
            if (!ChkActInd.Checked)
            {
                if (Sm.IsDataExist("Select 1 From TblJournalDtl Where AcNo=@Param Limit 1;", TxtAcNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This account# already used in existing journal.");
                    return true;
                }
            }
            return false;
        }

        private bool IsInactiveCOAInvalid1()
        {
            if (!ChkActInd.Checked)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblJournalHdr A, TblJournalDtl B " +
                    "Where A.DocNo=B.DocNo And Left(A.DocDt, 4)=Left(CurDate(), 4) And B.AcNo=@Param " +
                    "Limit 1;",
                    TxtAcNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This account# already used in existing current year journal.");
                    return true;
                }
            }
            return false;
        }

        private bool IsAcNoExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo FROM TblCoa ");
            SQL.AppendLine("WHERE AcNo =@AcNo ");
            SQL.AppendLine("  AND AcDesc=@AcDesc ");
            SQL.AppendLine("  AND Parent=@Parent ");
            SQL.AppendLine("  AND Level=@Level ");
            SQL.AppendLine("  AND AcType=@AcType ");
            SQL.AppendLine("LIMIT 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcDesc", TxtAcDesc.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@Level", TxtLevel.Text);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));

            if (!TxtAcNo.Properties.ReadOnly && Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Account number already existed.");
                return true;
            }
            return false;
        }

        private bool IsLocalCOAIsInvalid()
        {
            if (ChkActInd.Checked)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    if (Sm.IsDataExist("Select 1 From TblCoaDtl2 Where AcNo!=@Param1 And EntCode=@Param2 And LocalAcNo=@Param3 Limit 1;", TxtAcNo.Text, Sm.GetGrdStr(Grd1, i, 0), Sm.GetGrdStr(Grd1, i, 3)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "This local account already used in another account#.");
                        Sm.FocusGrd(Grd1, i, 1);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsLocalCOAIsInvalid2()
        {
            if (ChkActInd.Checked)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (i!=j && Sm.GetGrdStr(Grd1, i, 0) == Sm.GetGrdStr(Grd1, j, 0) && Sm.GetGrdStr(Grd1, i, 3)==Sm.GetGrdStr(Grd1, j, 3))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate local account in this account#.");
                            Sm.FocusGrd(Grd1, i, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveCOA()
        {
            var SQL = new StringBuilder();

            #region Old Code (1 AcNo 1 EntCode)
            //SQL.AppendLine("Insert Into TblCoa(AcNo, AcDesc, Parent, Level, AcType, EntCode, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@AcNo, @AcDesc, @Parent, @Level,@AcType, @EntCode, @UserCode, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("   Update AcDesc=@AcDesc,Parent=@Parent,Level=@Level,AcType=@AcType,EntCode=@EntCode,LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            #endregion

            #region New Code (1 AcNo could have many EntCode)
            
            SQL.AppendLine("Insert Into TblCoa(AcNo, AcDesc, EntCode, Alias, ActInd, Parent, Level, AcType, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AcNo, @AcDesc, @EntCode, @Alias, @ActInd, @Parent, @Level, @AcType, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY ");
            SQL.AppendLine("   UPDATE AcDesc=@AcDesc, EntCode=@EntCode, Alias = @Alias, ActInd=@ActInd, Parent=@Parent,Level=@Level,AcType=@AcType,LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("DELETE FROM TblCoaDtl2 WHERE AcNo=@AcNo; ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcDesc", TxtAcDesc.Text);
            Sm.CmParam<String>(ref cm, "@Alias", TxtAlias.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCOADtl(ref iGrid Grd, string AcNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCoaDtl2(AcNo, DNo, EntCode, LocalAcNo, LocalAcDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AcNo, @DNo, @EntCode, @LocalAcNo, @LocalAcDesc, @UserCode, CurrentDateTime()) ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@LocalAcNo", Sm.GetGrdStr(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@LocalAcDesc", Sm.GetGrdStr(Grd, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateChildActInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UPDATE TblCoa SET ActInd='N',LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Parent Like @AcNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text + "%");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Delete Data

        private bool IsJournalExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM TblJournalDtl ");
            SQL.AppendLine("WHERE AcNo=@AcNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This account is already used.");
                return true;
            }
            return false;
        }

        private MySqlCommand DeleteCOA(string AcNo)
        {
            #region Old Code (1 AcNo 1 EntCode)
            //var cm = new MySqlCommand() { CommandText = "Delete From TblCoa Where AcNo=@AcNo And AcDesc=@AcDesc And Parent=@Parent And AcType=@AcType And EntCode=@EntCode" };
            #endregion

            #region New Code (1 AcNo could have many EntCode)
            
            var SQL = new StringBuilder();
            //hapus dari tabel coa detail dulu, baru ke tabel coa
            SQL.AppendLine("DELETE FROM TblCoaDtl2 WHERE AcNo=@AcNo;");

            SQL.AppendLine("DELETE FROM TblCoa WHERE AcNo=@AcNo And AcDesc=@AcDesc And EntCode=@EntCode And Alias=@Alias And Parent=@Parent And AcType=@AcType; ");
            
            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcDesc", TxtAcDesc.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Alias", TxtAlias.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));

            return cm;
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
            mIsCOAFilteredByGroup = Sm.GetParameterBoo("IsCOAFilteredByGroup");
        }

        internal string GetSelectedEntity()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void SetLueEntCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y' And AutoCreateInd='N' Order By EntName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAlias_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.TxtTrim(TxtAlias);
            }
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                try
                {
                    Sm.TxtTrim(TxtAcNo);
                    LueParent.EditValue = null;
                    TxtLevel.EditValue = 1 + (TxtAcNo.Text).Length - (TxtAcNo.Text.Replace(".", string.Empty)).Length;
                    if (TxtAcNo.Text.Length != 0)
                    {
                        int index = TxtAcNo.Text.LastIndexOf('.');
                        if (index != -1)
                        {
                            var Parent = Sm.Left(TxtAcNo.Text, index);
                            if (Sm.IsDataExist("Select 1 From TblCOA Where AcNo=@Param;", Parent))
                                Sm.SetLue(LueParent, Parent);
                        }
                    }
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAcDesc);
        }
        
        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueAcNo));
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtLevel, 11);
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        #endregion        

        #region Grid Control Events

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2) Sm.FormShowDialog(new FrmCOA2Dlg(this));
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmCOA2Dlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion            

        #endregion
    }
}
