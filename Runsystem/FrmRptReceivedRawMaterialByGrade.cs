﻿#region Update
/*
    28/08/2019 [TKG] ubah query menggunakan tanggal verfikasi sbg filter
    31/08/2019 [TKG] bug filter tidak berfungsi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptReceivedRawMaterialByGrade : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mItCtRMPActual = string.Empty, mItCtRMPActual2 = string.Empty;
        private bool mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmRptReceivedRawMaterialByGrade(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();

                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mItCtRMPActual = Sm.GetParameter("ItCtRMPActual");
            mItCtRMPActual2 = Sm.GetParameter("ItCtRMPActual2");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select QueueNo, Grader, DocNo, DocDt, Shelf, ItCode, ItName, ");
            SQL.AppendLine("Item, LengthItem, DiameterItem, WidthItem, HeightItem, PurchaseUomCode, ");
            SQL.AppendLine("Sum(Qty1) Qty1, Sum(Qty2) Qty2 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select B.QueueNo, C.Grader, C.DocNo, C.DocDt, C.Shelf, C.ItCode, D.ItName, ");
            SQL.AppendLine("Concat(D.ItName, ' ( ', ");
            SQL.AppendLine("    Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then  ");
            SQL.AppendLine("        Concat(  ");
            SQL.AppendLine("        'L: ', Convert(Format(D.Length, 2) Using utf8), ");
            SQL.AppendLine("        'D: ', Convert(Format(D.Diameter, 2) Using utf8)  ");
            SQL.AppendLine("        )  ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then  ");
            SQL.AppendLine("        Concat(  ");
            SQL.AppendLine("        'L: ', Convert(Format(D.Length, 2) Using utf8),  ");
            SQL.AppendLine("        'W: ', Convert(Format(D.Width, 2) Using utf8),  ");
            SQL.AppendLine("        ', H: ', Convert(Format(D.Height, 2) Using utf8)  ");
            SQL.AppendLine("        )  ");
            SQL.AppendLine("    End, ");
            SQL.AppendLine("    ' )') As Item, ");
            SQL.AppendLine("C.Qty1, ");
            SQL.AppendLine("Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then Convert(Format(D.Length, 2) Using utf8) ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then Convert(Format(D.Length, 2) Using utf8)  ");
            SQL.AppendLine("    Else 0.00 End As LengthItem, ");
            SQL.AppendLine("Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then Convert(Format(D.Diameter, 2) Using utf8)  ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then 0.00 ");
            SQL.AppendLine("    Else 0.00 End As DiameterItem, ");
            SQL.AppendLine("Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then 0.00 ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then Convert(Format(D.Width, 2) Using utf8)  ");
            SQL.AppendLine("    Else 0.00 End As WidthItem, ");
            SQL.AppendLine("Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then 0.00 ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then Convert(Format(D.Height, 2) Using utf8)  ");
            SQL.AppendLine("    Else 0.00 End As HeightItem, ");
            SQL.AppendLine("Case D.ItCtCode ");
            SQL.AppendLine("    When @ItCtRMPActual Then Truncate(((0.25*3.1415926*D.Diameter*D.Diameter*D.Length)/1000000.00), 4)*C.Qty1 ");
            SQL.AppendLine("    When @ItCtRMPActual2 Then Truncate(((D.Length*D.Width*D.Height)/1000000.00), 4)*C.Qty1 ");
            SQL.AppendLine("    Else 0.00 End  As Qty2, ");
            SQL.AppendLine("D.PurchaseUomCode ");
            SQL.AppendLine("From TblRawMaterialVerify A ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When T2.SectionNo = '1' Then T1.Shelf1 ");
            SQL.AppendLine("    When T2.SectionNo = '2' Then T1.Shelf2 ");
            SQL.AppendLine("	When T2.SectionNo = '3' Then T1.Shelf3 ");
            SQL.AppendLine("    When T2.SectionNo = '4' Then T1.Shelf4 ");
            SQL.AppendLine("    When T2.SectionNo = '5' Then T1.Shelf5 ");
            SQL.AppendLine("    When T2.SectionNo = '6' Then T1.Shelf6 ");
            SQL.AppendLine("    When T2.SectionNo = '7' Then T1.Shelf7 ");
            SQL.AppendLine("    When T2.SectionNo = '8' Then T1.Shelf8 ");
            SQL.AppendLine("	When T2.SectionNo = '9' Then T1.Shelf9 ");
            SQL.AppendLine("    When T2.SectionNo = '10' Then T1.Shelf10 ");
            SQL.AppendLine("	When T2.SectionNo = '11' Then T1.Shelf11 ");
            SQL.AppendLine("    When T2.SectionNo = '12' Then T1.Shelf12 ");
            SQL.AppendLine("	When T2.SectionNo = '13' Then T1.Shelf13 ");
            SQL.AppendLine("    When T2.SectionNo = '14' Then T1.Shelf14 ");
            SQL.AppendLine("	When T2.SectionNo = '15' Then T1.Shelf15 ");
            SQL.AppendLine("    Else Null End As Shelf, ");
            SQL.AppendLine("	T1.DocDt, T1.LegalDocVerifyDocNo, T2.ItCode, ");
            SQL.AppendLine("	Sum(T2.Qty) As Qty1, ");
            SQL.AppendLine("    Group_Concat(Distinct(T4.EmpName)) As Grader ");
            SQL.AppendLine("    From TblRecvRawMaterialHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvrawMaterialDtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblRecvrawMaterialDtl T3 On T1.DocNo=T3.DocNo And T3.EmpType='1' ");
            SQL.AppendLine("    Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL.AppendLine("    Where T1.ProcessInd='F' ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.LegalDocVerifyDocNo In ( ");
            SQL.AppendLine("        Select LegalDocVerifyDocNo ");
            SQL.AppendLine("        From TblRawMaterialVerify ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By ");
            SQL.AppendLine("    T1.DocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When T2.SectionNo = '1' Then T1.Shelf1 ");
            SQL.AppendLine("    When T2.SectionNo = '2' Then T1.Shelf2 ");
            SQL.AppendLine("	When T2.SectionNo = '3' Then T1.Shelf3 ");
            SQL.AppendLine("    When T2.SectionNo = '4' Then T1.Shelf4 ");
            SQL.AppendLine("    When T2.SectionNo = '5' Then T1.Shelf5 ");
            SQL.AppendLine("    When T2.SectionNo = '6' Then T1.Shelf6 ");
            SQL.AppendLine("    When T2.SectionNo = '7' Then T1.Shelf7 ");
            SQL.AppendLine("    When T2.SectionNo = '8' Then T1.Shelf8 ");
            SQL.AppendLine("	When T2.SectionNo = '9' Then T1.Shelf9 ");
            SQL.AppendLine("    When T2.SectionNo = '10' Then T1.Shelf10 ");
            SQL.AppendLine("	When T2.SectionNo = '11' Then T1.Shelf11 ");
            SQL.AppendLine("    When T2.SectionNo = '12' Then T1.Shelf12 ");
            SQL.AppendLine("	When T2.SectionNo = '13' Then T1.Shelf13 ");
            SQL.AppendLine("    When T2.SectionNo = '14' Then T1.Shelf14 ");
            SQL.AppendLine("	When T2.SectionNo = '15' Then T1.Shelf15 ");
            SQL.AppendLine("    Else Null End, ");
            SQL.AppendLine("	T1.DocDt, T1.LegalDocVerifyDocNo, T2.ItCode ");
            SQL.AppendLine(") C On B.DocNo=C.LegalDocVerifyDocNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCOde=D.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By ");
            SQL.AppendLine("    QueueNo, Grader, DocNo, DocDt, Shelf, ItCode, ItName, ");
            SQL.AppendLine("    Item, LengthItem, DiameterItem, WidthItem, HeightItem, PurchaseUomCode ");
            SQL.AppendLine("Order By Shelf;");

            //SQL.AppendLine("Select * From ( ");
            //SQL.AppendLine("select  D.QueueNo, G.Grader, G.DocNo, G.DocDt, G.Shelf, G.ItCode, H.ItName, ");
            //SQL.AppendLine(" Concat(H.ItName, ' ( ', ");
            //SQL.AppendLine("               Case H.ItCtCode ");
            //SQL.AppendLine("                   When @ItCtRMPActual Then  ");
            //SQL.AppendLine("                       Concat(  ");
            //SQL.AppendLine("                       'L: ', Convert(Format(H.Length, 2) Using utf8), ");
            //SQL.AppendLine("                       'D: ', Convert(Format(H.Diameter, 2) Using utf8)  ");
            //SQL.AppendLine("                       )  ");
            //SQL.AppendLine("                   When @ItCtRMPActual2 Then  ");
            //SQL.AppendLine("                       Concat(  ");
            //SQL.AppendLine("                       'L: ', Convert(Format(H.Length, 2) Using utf8),  ");
            //SQL.AppendLine("                       'W: ', Convert(Format(H.Width, 2) Using utf8),  ");
            //SQL.AppendLine("                       ', H: ', Convert(Format(H.Height, 2) Using utf8)  ");
            //SQL.AppendLine("                       )  ");
            //SQL.AppendLine("               End, ");
            //SQL.AppendLine("           ' )') As Item,  G.Qty1, ");

            //SQL.AppendLine("               Case H.ItCtCode ");
            //SQL.AppendLine("                   When @ItCtRMPActual Then  ");
            //SQL.AppendLine("                       Convert(Format(H.Length, 2) Using utf8) ");
            //SQL.AppendLine("                   When @ItCtRMPActual2 Then  ");
            //SQL.AppendLine("                       Convert(Format(H.Length, 2) Using utf8)  ");
            //SQL.AppendLine("               else 0 End  As LengthItem, ");

            //SQL.AppendLine("               Case H.ItCtCode ");
            //SQL.AppendLine("                   When @ItCtRMPActual Then  ");
            //SQL.AppendLine("                        Convert(Format(H.Diameter, 2) Using utf8)  ");
            //SQL.AppendLine("                   When @ItCtRMPActual2 Then 0  ");
            //SQL.AppendLine("               else 0 End As DiameterItem, ");

            //SQL.AppendLine("               Case H.ItCtCode ");
            //SQL.AppendLine("                   When @ItCtRMPActual Then 0 ");
            //SQL.AppendLine("                   When @ItCtRMPActual2 Then Convert(Format(H.Width, 2) Using utf8)  ");
            //SQL.AppendLine("               else 0 End As WidthItem, ");

            //SQL.AppendLine("               Case H.ItCtCode ");
            //SQL.AppendLine("                   When @ItCtRMPActual Then 0 ");
            //SQL.AppendLine("                   When @ItCtRMPActual2 Then Convert(Format(H.Height, 2) Using utf8)  ");
            //SQL.AppendLine("               else 0 End As HeightItem, ");

            //SQL.AppendLine("			  Case H.ItCtCode ");
            //SQL.AppendLine("When @ItCtRMPActual Then Truncate(((0.25*3.1415926*H.Diameter*H.Diameter*H.Length)/1000000), 4) *G.Qty1 ");
            //SQL.AppendLine("When @ItCtRMPActual2 Then Truncate(((H.Length*H.Width*H.Height)/1000000), 4) * G.Qty1 ");
            //SQL.AppendLine("Else 0 End  As Qty2, H.PurchaseUomCode ");
            //SQL.AppendLine("From TblRawMaterialVerify C ");
            //SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegaldocVerifyDocNo = D.DocNo And D.CancelInd = 'N' ");
            //SQL.AppendLine(" Inner Join ( ");
            //SQL.AppendLine("   Select G1.DocNo, Group_Concat(Distinct(G4.EmpName)) As Grader, ");
            //SQL.AppendLine("   Case ");
            //SQL.AppendLine("   When G2.SectionNo = '1' Then G1.Shelf1 ");
            //SQL.AppendLine("   When G2.SectionNo = '2' Then G1.Shelf2 ");
            //SQL.AppendLine("	When G2.SectionNo = '3' Then G1.Shelf3 ");
            //SQL.AppendLine("   When G2.SectionNo = '4' Then G1.Shelf4 ");
            //SQL.AppendLine("   When G2.SectionNo = '5' Then G1.Shelf5 ");
            //SQL.AppendLine("   When G2.SectionNo = '6' Then G1.Shelf6 ");
            //SQL.AppendLine("   When G2.SectionNo = '7' Then G1.Shelf7 ");
            //SQL.AppendLine("   When G2.SectionNo = '8' Then G1.Shelf8 ");
            //SQL.AppendLine("	When G2.SectionNo = '9' Then G1.Shelf9 ");
            //SQL.AppendLine("   When G2.SectionNo = '10' Then G1.Shelf10 ");
            //SQL.AppendLine("	When G2.SectionNo = '11' Then G1.Shelf11 ");
            //SQL.AppendLine("   When G2.SectionNo = '12' Then G1.Shelf12 ");
            //SQL.AppendLine("	When G2.SectionNo = '13' Then G1.Shelf13 ");
            //SQL.AppendLine("   When G2.SectionNo = '14' Then G1.Shelf14 ");
            //SQL.AppendLine("	When G2.SectionNo = '15' Then G1.Shelf15 ");
            //SQL.AppendLine("   Else Null End As Shelf, ");
            //SQL.AppendLine("	G1.DocDt, G1.LegalDOcVerifyDocNo, G2.ItCode, Sum(G2.Qty) As Qty1 ");
            //SQL.AppendLine("   From TblRecvRawMaterialHdr G1 ");
            //SQL.AppendLine("   Inner Join TblRecvrawMaterialDtl2 G2 On G1.DocNo = G2.DocNo ");
            //SQL.AppendLine("   Inner Join TblRecvrawMaterialDtl G3 On G1.DocNo = G3.DocNo And G3.EmpType = '1' ");
            //SQL.AppendLine("   Inner Join TblEmployee G4 On G3.EmpCode = G4.EmpCode  ");
            //SQL.AppendLine("   Where G1.ProcessInd='F' And G1.CancelInd = 'N' And G1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("   Group By G1.DocNo, G1.DocDt, G1.LegalDOcVerifyDocNo, Shelf, G2.ItCode, G4.EmpCode ");
            //SQL.AppendLine("   ) G On D.DocNo=G.LegaldocVerifyDocNo ");
            //SQL.AppendLine("Inner Join TblItem H On G.ItCOde= H.ItCode ");
            //SQL.AppendLine("Where C.CancelInd = 'N'");
            //SQL.AppendLine("Group By G.DocNo, G.DocDt, G.Shelf, G.ItCode, H.ItName ");
            //SQL.AppendLine(")Y1 ");
     
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Bin",
                        "Antrian#",
                        "Grader",
                        "Penerimaan#",
                        "Verifikasi",
                       
                        //6-10
                        "Kategori",
                        "Kode Item",
                        "Nama Item",
                        "Panjang",
                        "Diameter",
                        
                        //11-14
                        "Lebar",
                        "Tinggi",
                        "Jumlah"+Environment.NewLine+"(Batang)",
                        "Volume"+Environment.NewLine+"(Kubik)",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 150, 100, 100, 

                        //6-10
                        150, 100, 250, 80, 80, 

                        //11-14
                        80, 80, 100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
               
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@ItCtRMPActual", mItCtRMPActual);
                Sm.CmParam<string>(ref cm, "@ItCtRMPActual2", mItCtRMPActual2);
                Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, "QueueNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtGrader.Text, "Grader", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "Shelf", false);

                mIsReCompute = false;
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "Shelf",   

                            //1-5
                            "QueueNo", "Grader", "DocNo", "DocDt", "ItName", 
                            
                            //6-10
                            "ItCode", "Item", "LengthItem", "DiameterItem", "WidthItem", 
                            
                            //11-13
                            "HeightItem", "Qty1", "Qty2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        }, true, true, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 14 });
                Grd1.EndUpdate();
                //Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                //mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //if (mIsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Antrian#");
        }

        private void ChkGrader_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Grader");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtGrader_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
