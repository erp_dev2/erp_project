﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using SyDocDLS = Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBase10 : Form
    {
        #region Constructor

        public FrmBase10()
        {
            InitializeComponent();
        }

        #endregion

        #region Button Methods

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {

        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {

        }

        private void BtnWord_Click(object sender, EventArgs e)
        {

        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Methods

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
            Color CaptionBarColor = Color.FromArgb(0xFF, 0x1B, 0xA1, 0xE2);
            Font CaptionFont = new Font("Segoe UI", 22.0f);
            Color CaptionForeColor = Color.White;
            HorizontalAlignment CaptionAlign = HorizontalAlignment.Left;
        }

        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Chart.Font = new Font(
                    Chart.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return "";
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void ChartExportToExcel()
        {
            string exportFileName = Application.StartupPath + "\\chartexport" + ".xls";

            // A new workbook with a worksheet should be created. 
            SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
            SyXL.IWorksheet sheet = chartBook.Worksheets[0];

            // Fill the worksheet with chart data. 
            for (int i = 1; i <= 5; i++)
            {
                sheet.Range[i, 1].Number = this.Chart.Series[0].Points[i - 1].X;
                sheet.Range[i, 2].Number = this.Chart.Series[0].Points[i - 1].YValues[0];
            }

            // Create a chart worksheet. 
            SyXL.IChart chart = chartBook.Charts.Add("Essential Chart");

            // Specify the title of the Chart. 
            chart.ChartTitle = "Essential Chart";

            // Initialize a new series instance and add it to the series collection of the chart. 
            SyXL.IChartSerie series = chart.Series.Add();

            // Specify the chart type of the series. 
            series.SerieType = SyXL.ExcelChartType.Column_Clustered;

            // Specify the name of the series. This will be displayed as the text of the legend. 
            series.Name = "Sample Series";

            // Specify the value ranges for the series. 
            series.Values = sheet.Range["B1:B5"];

            // Specify the Category labels for the series. 
            series.CategoryLabels = sheet.Range["A1:A5"];

            // Make the chart as active sheet. 
            chart.Activate();

            // Save the Chart book. 
            chartBook.SaveAs(exportFileName); chartBook.Close();
            SyXL.ExcelUtils.Close();

            // Launches the file. 
            System.Diagnostics.Process.Start(exportFileName);
        }

        virtual protected void ChartExportToWord()
        {
            string fileName = Application.StartupPath + "\\chartexport";
            string exportFileName = fileName + ".doc";
            string file = fileName + ".gif"; this.Chart.SaveImage(file);
            
            // Create a new document. 
            SyDocDLS.WordDocument document = new SyDocDLS.WordDocument();
            
            // Adding a new section to the document.
            SyDocDLS.IWSection section = document.AddSection();
            
            // Adding a paragraph to the section.
            SyDocDLS.IWParagraph paragraph = section.AddParagraph();
            
            // Writing text.
            paragraph.AppendText( "Essential Chart" );
            
            // Adding a new paragraph.
            paragraph = section.AddParagraph();
            paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
            
            // Inserting chart.
            paragraph.AppendPicture( Image.FromFile(file));
            
            // Save the Document to disk.
            document.Save(exportFileName , Syncfusion.DocIO.FormatType.Doc );
            
            // Launches the file.
            System.Diagnostics.Process.Start(exportFileName); 
        }

        virtual protected void ChartExportToPDF()
        {

        }

        #endregion

        #region Events

        #region Form Method

        private void Form1_Load(object sender, EventArgs e)
        {
            //BtnExcel.Visible = false;
            //BtnWord.Visible = false;
            //BtnPDF.Visible = false;
            FrmLoad(sender, e);
        }

        #endregion

        #region Misc Control Events

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        #endregion

    }
}
