﻿#region Update

/*
    11/04/2022 [SET/PRODUCT] Menu baru
    18/04/2022 [IBL/PRODUCT] Simpan InvestmentType di StockMovement, dan simpan Base Price di StockPrice utk keperluan perhitungan di reporting
    11/05/2022 [IBL/PRODUCT] Tambah parameter BankAccountTypeForInvestment
    11/05/2022 [IBL/PRODUCT] Field Total Expenses diambil dari Expenses yg ngga kecentang tax nya di master Type of Expenses and Other
                             Field Tax diambil dari Expenses yg kecentang tax nya di master Type of Expenses and Other
                             Sebelum disave, statusnya = Outsanding
                             Tab Expenses & Other Detail bisa dihapus datanya setelah dipilih.
    12/05/2022 [IBL/PRODUCT] Bug: kolom total cost jadi checkbox
    17/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
    19/05/2022 [IBL/PRODUCT] Memindahkan kolom Investment Code ke paling kiri
    20/05/2022 [IBL/PRODUCT] Kolom Doc Date dan Amount di tab Expenses & Other detail bersifat editable.
                             Method ComputeAmtDtl2 -> Perhitungan yg menggunakan rate disamakan semua. Tidak dibedakan utk Expenses Type Prepaid VAT (booker fee).
    30/05/2022 [SET/PRODUCT] Field Investment Code belum masuk dalam save data
    02/06/2022 [IBL/PRODUCT] Tab expenses : Tambah kolom tax, otomatis munculin data dari master expenses type, bisa isi manual.
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionEquitySecurities : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        iGCell fCell;
        bool fAccept;
        internal FrmAcquisitionEquitySecuritiesFind FrmFind;
        private string
            mDocType = "02",
            mMainCurCode = string.Empty;
        internal string
            mBankAccountTypeForInvestment = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionEquitySecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueFinancialInstitutionCode(ref LueFinancialInstitution, string.Empty);
                Sl.SetLueOption(ref LueType, "InvestmentType");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Investment Code", "Investment Name", "OptCode", "Type", "InvestmentCtCode", 

                    //6-10
                    "Category", "Quantity", "UoM", "Base Price", "Investment Cost", 

                    //11-13
                    "Total Cost", "Acquisition Price", "InvestmentEquity"

                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    150, 150, 150, 150, 120,  
                    
                    //6-10
                    100, 100, 100, 120, 120, 

                    //11-12
                    120, 120, 120

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10, 11, 12 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 13 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] {  });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Doc Type Code", "Document Type", "CoA Account", "CoA Name", "Doc Ref#", 

                    //6-10
                    "Doc Date", "Amount", "Remarks", "Rate", "Amt",

                    //11-14
                    "Tax", "DNo", "MultiplierField", ""

                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    100, 150, 100, 150, 120, 
                    
                    //6-10
                    100, 100, 100, 100, 100,

                    //11-14
                    30, 0, 0, 20

                }
            );
            Sm.GrdColCheck(Grd2, new int[] { 11 });
            Sm.GrdColButton(Grd2, new int[] { 14 });
            Sm.GrdFormatDate(Grd2, new int[] { 6 });
            Sm.GrdFormatDec(Grd2, new int[] { 7, 9, 10 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 9, 10, 12, 13 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { });
            Grd2.Cols[14].Move(3);
            Grd2.Cols[11].Move(7);
            #endregion

            #region Grd3

            Grd3.Cols.Count = 5;
            //Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "No",
                    
                    //1-4
                    "User", "Status", "Date", "Remark",

                },
                new int[]
                {
                    //0
                    25,

                    //1-5
                    100, 100, 100, 100,

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdFormatDec(Grd3, new int[] {  }, 0);
            Sm.GrdColButton(Grd3, new int[] {  });
            Sm.GrdColInvisible(Grd3, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtDocNo, MeeCancelReason, LueFinancialInstitution, TxtInvestmentBankAcc, TxtStatus, TxtInvestmentCost, TxtTax,
                        DteDocDt2, LueFinancialInstitution, MeeRemark, DteDocDt3, TxtInvestmentBankAcc, TxtCurCode, TxtRate, TxtTotalExpenses,
                        TxtTotalCost, TxtDocNoVoucher, ChkCancelInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    BtnInvestmentBankAcc.Enabled = false;
                    LueType.Visible = false;
                    BtnRefreshData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueFinancialInstitution, DteDocDt2, DteDocDt3, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 4, 7, 9 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 5, 6, 7, 8 });
                    BtnInvestmentBankAcc.Enabled = true;
                    TxtRate.Text = "1";
                    TxtStatus.Text = "Outstanding";
                    Sm.FormatNumTxt(TxtRate, 0);
                    Sm.FormatNumTxt(TxtInvestmentCost, 0);
                    Sm.FormatNumTxt(TxtTotalExpenses, 0);
                    Sm.FormatNumTxt(TxtTax, 0);
                    Sm.FormatNumTxt(TxtTotalCost, 0);
                    BtnRefreshData.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    //Sm.GrdColReadOnly(false, true, Grd1, new int[] {  });
                    MeeCancelReason.Focus();
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueFinancialInstitution, TxtInvestmentBankAcc, DteDocDt2, DteDocDt3, TxtStatus,
                TxtCurCode, TxtRate, TxtInvestmentCost, TxtTotalExpenses, TxtTax, TxtTotalCost, MeeRemark, TxtDocNoVoucher
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            //Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 8 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 9, 10, 11, 12 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 9, 10 });

            Sm.ClearGrd(Grd3, true);
        }

        #endregion

        #region Button Method

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteDocDt2);
                SetSettlementDate(Sm.GetDte(DteDocDt2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnInvestmentBankAcc_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAcquisitionEquitySecuritiesDlg(this));
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAcquisitionEquitySecuritiesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) ||
                ChkCancelInd.Checked) return;
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AcquisitionEquitySecurities", "TblAcquisitionEquitySecuritiesHdr");

            cml.Add(SaveAcquisitionEquitySecurities(DocNo));
            cml.Add(SaveAcquisitionEquitySecuritiesDtl2(DocNo));

            cml.Add(SaveStock(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueFinancialInstitution, "Financial Institution") ||
                Sm.IsTxtEmpty(TxtInvestmentBankAcc, "Investment Bank Account", false) ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Investment is null")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Investment type is null")) return true;
            }

            return false;
        }

        private MySqlCommand SaveAcquisitionEquitySecurities(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            //Cancel Document
            //SQL.AppendLine("UPDATE tblpositionadjustmenthdr ");
            //SQL.AppendLine("SET CancelInd = 'Y', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            //SQL.AppendLine("WHERE CancelInd = 'N'; ");

            //Hdr
            SQL.AppendLine("Insert Into TblAcquisitionEquitySecuritiesHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, SekuritasCode, BankAcCode, TradeDt, SettlementDt, Status, CurCode, Rate, InvestmentCost, ");
            SQL.AppendLine("TotalExpenses, TaxAmt, TotalCost, Remark, VoucherDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @SekuritasCode, @BankAcCode, @TradeDt, @SettlementDt, @Status, @CurCode, @Rate, ");
            SQL.AppendLine("@InvestmentCost, @TotalExpenses, @TaxAmt, @TotalCost, @Remark, @VoucherDocNo, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            //Doc Approval
            //SQL.AppendLine("INSERT INTO tbldocapproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("SELECT T.DocType, @DocNo, '001', T.DNo, @Usercode, CurrentDateTime() ");
            //SQL.AppendLine("FROM tbldocapprovalsetting T ");
            //SQL.AppendLine("WHERE DocType = ''; ");
            //SQL.AppendLine("");

            //Update Status (if DocApproval Not Exists)
            SQL.AppendLine("Update TblAcquisitionEquitySecuritiesHdr Set Status='Purchased' ");
            SQL.AppendLine("Where DocNo=@DocNo; ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select 1 From TblDocApproval ");
            //SQL.AppendLine("    Where DocType='TblAcquisitionEquitySecuritiesHdr' ");
            //SQL.AppendLine("    And DocNo=@DocNo ");
            //SQL.AppendLine("    ); ");

            //Dtl
            SQLDtl.AppendLine("Insert Into tblacquisitionequitysecuritiesdtl ");
            SQLDtl.AppendLine("(DocNo, DNo, Status, InvestmentCode, InvestmentType, Qty, BasePrice, InvestmentCost, TotalCost, AcquisitionPrice, Source, ");
            SQLDtl.AppendLine("InvestmentEquityCode, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for(int r = 0; r < Grd1.Rows.Count; r++)
            {
                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", 'O', @InvestmentCode_" + r.ToString() + ", @InvestmentType_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@Qty_" + r.ToString() + ", @BasePrice_" + r.ToString() + ", @InvestmentCost_" + r.ToString() + ", @TotalCost_" + r.ToString() + ", @AcquisitionPrice_" + r.ToString() + ", ");
                SQLDtl.AppendLine("CONCAT(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() + "), @InvestmentEquityCode_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                Sm.CmParam<Decimal>(ref cm, "@BasePrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                Sm.CmParam<Decimal>(ref cm, "@InvestmentCost_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                Sm.CmParam<Decimal>(ref cm, "@TotalCost_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                Sm.CmParam<Decimal>(ref cm, "@AcquisitionPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
                Sm.CmParam<String>(ref cm, "@InvestmentEquityCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
            }
            SQLDtl.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SekuritasCode", Sm.GetLue(LueFinancialInstitution));
            Sm.CmParam<String>(ref cm, "@BankAcCode", TxtInvestmentBankAcc.Text);
            Sm.CmParamDt(ref cm, "@TradeDt", Sm.GetDte(DteDocDt2));
            Sm.CmParamDt(ref cm, "@SettlementDt", Sm.GetDte(DteDocDt3));
            Sm.CmParam<String>(ref cm, "@Status", TxtStatus.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Rate", decimal.Parse(TxtRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@InvestmentCost", decimal.Parse(TxtInvestmentCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalExpenses", decimal.Parse(TxtTotalExpenses.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCost", decimal.Parse(TxtTotalCost.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAcquisitionEquitySecuritiesDtl2(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into tblacquisitionequitysecuritiesdtl2 ");
            SQL.AppendLine("(DocNo, DNo, ExpensesCode, ExpensesDNo, ExpensesDocType, AcNo, TaxInd, DocRef, DocDt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            //Dtl2
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +", @ExpensesCode_"+r.ToString()+", @ExpensesDNo_"+r.ToString()+", ");
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                    SQL.AppendLine("Null, Null, Null, ");
                else
                    SQL.AppendLine("@ExpensesDocType_"+r.ToString()+", @AcNo_"+r.ToString()+", @TaxInd_"+r.ToString() + ", ");
                SQL.AppendLine("@DocRef_" + r.ToString() + ", @DocDt_" + r.ToString() + ", @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ExpensesCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                Sm.CmParam<String>(ref cm, "@ExpensesDNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 12));
                Sm.CmParam<String>(ref cm, "@ExpensesDocType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                Sm.CmParam<String>(ref cm, "@TaxInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 11) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@DocRef_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                Sm.CmParamDt(ref cm, "@DocDt_" + r.ToString(), Sm.GetGrdDate(Grd2, r, 6));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 7));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 8));
            }
            SQL.AppendLine("; ");
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* AcquisitionEquitySecurities - Stock */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, NULL, A.BankAcCode, '-', '-', B.InvestmentEquityCode, B.InvestmentType, '-', B.Source, B.Qty, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblAcquisitionEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */And A.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity C On B.InvestmentEquityCode=C.InvestmentEquityCode And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockSummary(WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, Source, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '-', BankAcCode, '-', '-', B.InvestmentEquityCode, '-', B.Source, B.Qty, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblAcquisitionEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */And A.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity C On B.InvestmentEquityCode=C.InvestmentEquityCode AND C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockPrice(InvestmentCode, BatchNo, Source, CurCode, UPrice, BasePrice , ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.InvestmentEquityCode, '-', A.Source, B.CurCode, ");
            SQL.AppendLine("A.AcquisitionPrice As UPrice, A.BasePrice, ");
            SQL.AppendLine("B.Rate As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionEquitySecuritiesDtl A ");
            SQL.AppendLine("Inner Join TblAcquisitionEquitySecuritiesHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAcquisitionEquitySecurities());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditAcquisitionEquitySecurities()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update Tblacquisitionequitysecuritieshdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, NULL, A.BankAcCode, '-', '-', B.InvestmentEquityCode, B.InvestmentType, '-', B.Source, -1*B.Qty, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblAcquisitionEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblAcquisitionEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */And A.CancelInd='Y' ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity C On B.InvestmentEquityCode=C.InvestmentEquityCode And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("UPDATE tblinvestmentstocksummary T ");
            SQL.AppendLine("    INNER JOIN tblacquisitionequitysecuritiesdtl X ON T.Source = X.Source AND X.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN tblacquisitionequitysecuritieshdr Y ON X.DocNo = Y.DocNo ");
            SQL.AppendLine("    SET T.Qty=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("    WHERE T.BankAcCode = Y.BankAcCode AND T.Source = X.Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        #endregion

        #endregion

        #region

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SekuritasCode, C.BankAcNm, A.TradeDt, A.SettlementDt, A.Status, ");
                SQL.AppendLine("A.CurCode, A.Rate, A.InvestmentCost, A.TotalExpenses, A.TaxAmt, A.TotalCost, A.Remark, A.VoucherDocNo ");
                SQL.AppendLine("FROM tblacquisitionequitysecuritieshdr A ");
                SQL.AppendLine("INNER JOIN tblinvestmentsekuritas B ON A.SekuritasCode = B.SekuritasCode ");
                SQL.AppendLine("INNER JOIN tblbankaccount C ON A.BankAcCode = C.BankAcCode ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "CancelReason", "SekuritasCode", "BankAcNm", 

                            //6-10
                            "TradeDt", "SettlementDt", "Status", "CurCode", "Rate", 

                            //11-15
                            "InvestmentCost", "TotalExpenses", "TaxAmt", "TotalCost", "Remark", 
                            
                            //16
                            "VoucherDocNo",

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            MeeCancelReason.Text = Sm.DrStr(dr, c[3]);
                            SetLueFinancialInstitutionCode(ref LueFinancialInstitution, Sm.DrStr(dr, c[4]));
                            TxtInvestmentBankAcc.Text = Sm.DrStr(dr, c[5]);
                            Sm.SetDte(DteDocDt2, Sm.DrStr(dr, c[6]));
                            Sm.SetDte(DteDocDt3, Sm.DrStr(dr, c[7]));
                            TxtStatus.Text = Sm.DrStr(dr, c[8]);
                            TxtCurCode.Text = Sm.DrStr(dr, c[9]);
                            TxtRate.Text = Sm.DrStr(dr, c[10]);
                            TxtInvestmentCost.Text = Sm.DrStr(dr, c[11]);
                            TxtTotalExpenses.Text = Sm.DrStr(dr, c[12]);
                            TxtTax.Text = Sm.DrStr(dr, c[13]);
                            TxtTotalCost.Text = Sm.DrStr(dr, c[14]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                            TxtDocNoVoucher.Text = Sm.DrStr(dr, c[16]);
                        }, true
                    );
                Sm.FormatNumTxt(TxtRate, 0);
                Sm.FormatNumTxt(TxtInvestmentCost, 0);
                Sm.FormatNumTxt(TxtTotalExpenses, 0);
                Sm.FormatNumTxt(TxtTax, 0);
                Sm.FormatNumTxt(TxtTotalCost, 0);
                ShowAcquisitionEquitySecuritiesDtl(DocNo);
                ShowAcquisitionEquitySecuritiesDtl2(DocNo);
                //ComputeInvestmentCost();
                //ComputeTotalExpenses();
                //ComputeTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAcquisitionEquitySecuritiesDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT C.PortofolioName InvestmentName, B.PortofolioId InvestmentCode, A.InvestmentType, D.OptDesc, E.InvestmentCtName, A.Qty, B.UomCode, A.BasePrice, A.InvestmentCost, A.TotalCost, A.AcquisitionPrice, A.InvestmentEquityCode ");
            SQL.AppendLine("From tblacquisitionequitysecuritiesdtl A ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemequity B ON A.InvestmentEquityCode = B.InvestmentEquityCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio C ON B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.InvestmentType = D.OptCode AND D.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory E ON C.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "InvestmentCode", "InvestmentName", "InvestmentType", "OptDesc", "InvestmentCtName", "Qty", "UomCode", "BasePrice", "InvestmentCost", "TotalCost", "AcquisitionPrice", "InvestmentEquityCode" },
                (MySqlDataReader dr, iGrid Grd1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            TxtInvestmentCost.Text = Sm.GetGrdStr(Grd1, 0, 10);
            Sm.FormatNumTxt(TxtInvestmentCost, 0);
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowAcquisitionEquitySecuritiesDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ExpensesCode, IfNull(B.DocType, A.ExpensesDocType) As DocType, IfNull(B.AcNo, A.AcNo) As AcNo, ");
            SQL.AppendLine("IfNull(C.AcDesc, D.AcDesc) As AcDesc, A.DocRef, A.DocDt, A.Amt, A.Remark, B.Rate, B.Amt As Amount, IfNull(B.TaxInd, A.TaxInd) As TaxInd, ");
            SQL.AppendLine("A.ExpensesDNo, B.Formula ");
            SQL.AppendLine("From TblAcquisitionEquitySecuritiesDtl2 A ");
            SQL.AppendLine("Left Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("	And A.ExpensesDNo = B.DNo ");
            SQL.AppendLine("Left Join TblCoa C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] {
                    "ExpensesCode",
                    
                    "DocType", "AcNo", "AcDesc", "DocRef", "DocDt",
                    
                    "Amt", "Remark", "Rate", "Amount", "TaxInd",

                    "ExpensesDNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grd2, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0); 
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mBankAccountTypeForInvestment = Sm.GetParameter("BankAccountTypeForInvestment");
        }

        internal void SetLueFinancialInstitutionCode(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SekuritasCode Col1, SekuritasName Col2 ");
                SQL.AppendLine("From TblInvestmentSekuritas ");
                if (Code.Length > 0)
                    SQL.AppendLine("Where SekuritasCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSettlementDate(string TradeDt)
        {
            String mTradeDt = Sm.Left(TradeDt, 8);
            String mSettlementDt = Sm.ConvertDate(mTradeDt).AddDays(2).ToString("yyyyMMdd");
            Sm.SetDte(DteDocDt3, mSettlementDt);
        }

        private void ShowExpensesData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ExpensesCode, B.DocType, B.AcNo, C.AcDesc, B.Rate, B.Amt, B.TaxInd, B.Formula, B.DNo ");
            SQL.AppendLine("From TblExpensesTypeHdr A ");
            SQL.AppendLine("Inner Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.TransactionCode = @TransactionCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TransactionCode", mMenuCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {
                        //0
                        "ExpensesCode",

                        //1-5
                        "DocType", "AcNo", "AcDesc", "Rate",  "Amt", 

                        //6-8
                        "TaxInd", "DNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grid1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 5);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 13, 8);
                    Grd2.Cells[Row, 6].Value = DteDocDt2.Text;
                    Grd2.Cells[Row, 7].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd2, Grd2.Rows.Count - 1, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 9, 10 });
            IsExpensesCodeExists();
            ComputeAmtDtl2();
        }

        internal void IsExpensesCodeExists()
        {
            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length == 0)
                    GrdCellReadOnly(false, true, Grd2, r, new int[] { 0, 2, 11, 14 });
                else
                    GrdCellReadOnly(true, true, Grd2, r, new int[] { 0, 2, 11, 14 });
            }
        }

        private void GrdCellReadOnly(bool ReadOnly, bool IsChangeBackColor, iGrid Grd, int RowIndex, int[] ColIndex)
        {
            if (ReadOnly)
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.FromArgb(224, 224, 224);
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.True;
                }
            }
            else
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.White;
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.False;
                }
            }
        }

        internal string GetSelectedExpenses()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            '#' +
                            Sm.GetGrdStr(Grd2, Row, 12) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeInvestmentCost()
        {
            decimal Qty = 0m, BasePrice = 0m;

            if (Sm.GetGrdStr(Grd1, 0, 7).Length != 0) Qty = Sm.GetGrdDec(Grd1, 0, 7);
            if (Sm.GetGrdStr(Grd1, 0, 9).Length != 0) BasePrice = Sm.GetGrdDec(Grd1, 0, 9);

            Grd1.Cells[0, 10].Value = Qty * BasePrice;
            TxtInvestmentCost.Text = Convert.ToString(Qty * BasePrice);
            Sm.FormatNumTxt(TxtInvestmentCost, 0);
            if(BtnSave.Enabled)
                ComputeTotalCost();
            //ComputeAcquisitionPrice();
        }

        internal void ComputeAcquisitionPrice()
        {
            decimal Qty = 0m, TotalCost = 0m;

            if (Sm.GetGrdStr(Grd1, 0, 7).Length != 0) Qty = Sm.GetGrdDec(Grd1, 0, 7);
            if (Sm.GetGrdStr(Grd1, 0, 11).Length != 0) TotalCost = Sm.GetGrdDec(Grd1, 0, 11);

            if(Sm.GetGrdDec(Grd1, 0, 7) != 0)
                Grd1.Cells[0, 12].Value = TotalCost / Qty;
        }

        internal void ComputeTotalExpenses()
        {
            decimal TotalExpenses = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if(!Sm.GetGrdBool(Grd2, Row, 11))
                    TotalExpenses += Sm.GetGrdDec(Grd2, Row, 7);
            }
            TxtTotalExpenses.Text = Convert.ToString(TotalExpenses);
            Sm.FormatNumTxt(TxtTotalExpenses, 0);
            if(BtnSave.Enabled)
                ComputeTotalCost();
            //ComputeAcquisitionPrice();
        }

        internal void ComputeTax()
        {
            decimal Tax = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count -1; Row++)
            {
                if (Sm.GetGrdBool(Grd2, Row, 11))
                    Tax += Sm.GetGrdDec(Grd2, Row, 7);
            }
            TxtTax.Text = Convert.ToString(Tax);
            Sm.FormatNumTxt(TxtTax, 0);
            ComputeTotalCost();
            //ComputeAcquisitionPrice();
        }

        internal void ComputeTotalCost()
        {
            decimal InvestmentCost = 0m,
            TotalExpenses = 0m,
            Tax = 0m;

            if (TxtInvestmentCost.Text != null)
            {
                InvestmentCost = Convert.ToDecimal(TxtInvestmentCost.Text);
            }
            if (TxtTotalExpenses.Text != null)
            {
                TotalExpenses = Convert.ToDecimal(TxtTotalExpenses.Text);
            }
            if (TxtTax.Text != null)
            {
                Tax = Convert.ToDecimal(TxtTax.Text);
            }

            decimal TotalCost = InvestmentCost + TotalExpenses + Tax;

            TxtTotalCost.Text = Convert.ToString(TotalCost);
            Sm.FormatNumTxt(TxtTotalCost, 0);
            Grd1.Cells[0, 11].Value = TotalCost;

            ComputeAcquisitionPrice();
        }

        internal void ComputeAmtDtl2()
        {
            decimal Rate = 0m, Amt = 0m;

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                Rate = Sm.GetGrdDec(Grd2, r, 9);
                Amt = Sm.GetGrdDec(Grd2, r, 10);
                
                if (Amt == 0)
                    Grd2.Cells[r, 7].Value = (Rate / 100) * Sm.GetGrdDec(Grd1, 0, 10);
                else if (Rate == 0)
                    Grd2.Cells[r, 7].Value = Amt;
            }
            ComputeTotalExpenses();
            ComputeTax();
        }

        internal void ComputeAmtDtl2(List<String> ExpensesCode)
        {
            decimal Rate = 0m, Amt = 0m;

            if (ExpensesCode.Count > 0)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    for (int i = 0; i < ExpensesCode.Count; i++)
                    {
                        string test = Sm.GetGrdStr(Grd2, r, 1) + Sm.GetGrdStr(Grd2, r, 12);
                        if (Sm.GetGrdStr(Grd2, r, 1) + Sm.GetGrdStr(Grd2, r, 12) == ExpensesCode[i])
                        {
                            Rate = Sm.GetGrdDec(Grd2, r, 9);
                            Amt = Sm.GetGrdDec(Grd2, r, 10);

                            if (Amt == 0)
                                Grd2.Cells[r, 7].Value = (Rate / 100) * Sm.GetGrdDec(Grd1, 0, 10);
                            else if (Rate == 0)
                                Grd2.Cells[r, 7].Value = Amt;
                        }
                    }
                }
            }
            
            ComputeTotalExpenses();
            ComputeTax();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7, 9, 10, 11, 12 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] {  }, e);

                if (e.ColIndex == 7 || e.ColIndex == 9)
                {
                    ComputeInvestmentCost();
                    //ComputeAmtDtl2();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
       
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFinancialInstitution_EditValueChanged(object sender, EventArgs e)
        {
            string code = Sm.GetLue(LueFinancialInstitution);
            Sm.RefreshLookUpEdit(LueFinancialInstitution, new Sm.RefreshLue2(SetLueFinancialInstitutionCode), string.Empty);
        }

        private void TxtCurCode_EditValueChanged(object sender, EventArgs e)
        {
            var Rate = string.Empty;
            if (TxtCurCode.Text.Length > 0)
                if (TxtCurCode.Text == "IDR")
                    TxtRate.Text = "1";
                else
                {
                    Rate = Sm.GetValue("SELECT Amt " +
                        "FROM tblcurrencyrate WHERE CurCode1 = '" + TxtCurCode.Text + "' AND CurCode2 = 'IDR' " +
                        "AND RateDt = (SELECT MAX(RateDt) FROM tblcurrencyrate) ");
                    TxtRate.Text = Rate;
                }
                Sm.FormatNumTxt(TxtRate, 0);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentType");
        }

        private void LueType_Validated(object sender, EventArgs e)
        {
            LueType.Visible = false;
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (LueType.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueType).Trim();
                    Grd1.Cells[fCell.RowIndex, 4].Value = LueType.GetColumnValue("Col2");
                }
            }
        }

        private void TxtInvestmentCost_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if(Sm.GetDte(DteDocDt2).Length > 0)
                SetSettlementDate(Sm.GetDte(DteDocDt2));
        }

        private void DteExpensesDocDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteExpensesDocDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteExpensesDocDt, ref fCell, ref fAccept);
        }

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 &&
                !Sm.IsGrdValueEmpty(Grd1, 0, 1, false, "You have to choose Investment Item first.") &&
                !Sm.IsGrdValueEmpty(Grd1, 0, 10, true, "You have to fill in the Investment Cost value first."))
            {
                ShowExpensesData();
            }
        }

        #endregion

        #region Grid Event
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (e.ColIndex == 4) Sm.LueRequestEdit(ref Grd1, ref LueType, ref fCell, ref fAccept, e);
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmAcquisitionEquitySecuritiesDlg2(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeTotalExpenses();
                ComputeTax();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 7, 9, 10 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { }, e);
                if (Grd2.Rows.Count > 1)
                {
                    ComputeTotalExpenses();
                    ComputeTax();
                }
                //if (Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length > 0)
                //    ComputeAmtDtl2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 9, 10 });
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd2, DteExpensesDocDt, ref fCell, ref fAccept, e);

                if (Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 8 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    IsExpensesCodeExists();
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsGrdValueEmpty(Grd1, 0, 1, false, "Investment Name"))
            {
                if(e.ColIndex == 0)
                    Sm.FormShowDialog(new FrmAcquisitionEquitySecuritiesDlg3(this, e.RowIndex));
                if(e.ColIndex == 14)
                    Sm.FormShowDialog(new FrmAcquisitionEquitySecuritiesDlg4(this));
            }
        }


        #endregion

        #endregion
    }
}
