﻿#region Update
/*
    05/04/2023 [WED/MNET] new apps, display Voucher Incoming
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectSystemRevenueExpenseDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProjectSystemRevenueExpense mFrmParent;
        private string mVCIncomingDocNo = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectSystemRevenueExpenseDlg2(FrmRptProjectSystemRevenueExpense FrmParent, string VCIncomingDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVCIncomingDocNo = VCIncomingDocNo;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = BtnRefresh.Visible = false;
            SetGrd();
            SetSQL();
            ShowData();
        }

        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-4
                    "Document#",
                    "",
                    "Date",
                    "Amount"
                }, new int[]
                {
                    //0
                    50,

                    //1-4
                    180, 20, 80, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, Amt ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where Find_In_Set(DocNo, @DocNo) ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
        }
        #endregion

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var j = new FrmVoucher(mFrmParent.mMenuCode);
                j.Tag = mFrmParent.mMenuCode;
                j.WindowState = FormWindowState.Normal;
                j.StartPosition = FormStartPosition.CenterScreen;
                j.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                j.ShowDialog();
            }
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, false);

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", mVCIncomingDocNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL,
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-2
                        "DocDt", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }
        #endregion

        #endregion

    }
}
