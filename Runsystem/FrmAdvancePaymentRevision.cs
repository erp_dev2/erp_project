﻿#region Update
/*
    17/01/2021 [TKG/PHT] ubah GenerateDocNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAdvancePaymentRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
        mDocNo = string.Empty;
        iGCell fCell;
        bool fAccept;
        string mItCtRMPActual = string.Empty, mSQLForActualItCode = string.Empty;
        internal FrmAdvancePaymentRevisionFind FrmFind;
        bool Approval;

        #endregion

        #region Constructor

        public FrmAdvancePaymentRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            SetLueMonth(ref LueMonth);
            SetLueMonth(ref LueMth);
            Sl.SetLueYr(LueYr, string.Empty);
            SetGrd();
            LueMth.Visible = false;

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowDataAdvancePayment(mDocNo, 1);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5 ;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        //1-3
                        "Month", "Year", "Amount", "Code"
                    },
                    new int[] 
                    {
                        //0-3
                        0, 
                        150, 60, 100, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, TxtAdvancePaymentDocNo, TxtEmpName, TxtEmpCode, DteDocDt2,
                        TxtPosName, TxtDeptName, TxtSiteName, TxtAmt, TxtTop, LueMonth, LueYr, MeeRemark, MeeRemark2,
                        LueMth, TxtTotal1
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnAdvancePaymentDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark2, LueMth
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnAdvancePaymentDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtAdvancePaymentDocNo, DteDocDt2, TxtStatus, TxtEmpCode, TxtEmpName, TxtPosName, 
                TxtDeptName, TxtSiteName, TxtTop, LueMonth, LueYr, 
                MeeRemark, MeeRemark2, LueMth
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt,  TxtTotal1
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               DteDocDt2, TxtEmpCode, TxtEmpName, TxtPosName, TxtDeptName, TxtSiteName, TxtTop, LueMonth, LueYr, MeeRemark, LueMth
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt,  TxtTotal1
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3 }, e);
            if (e.ColIndex == 3)
            {
                ComputeTotal(Grd1, TxtTotal1);
                ComputeTOP();
                Sm.FocusGrd(Grd1, e.RowIndex + 1, 3);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotal(Grd1, TxtTotal1);
            ComputeTOP();
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueMth, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueMonth(ref LueMth);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmAdvancePaymentRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            TxtStatus.EditValue = "Outstanding";
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAdvancePaymenthdr Where DocNo=@DocNo" };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
      
        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AdvancePaymentRevision", "TblAdvancePaymentRevisionhdr");
            string DocNoAdvancePayment = TxtAdvancePaymentDocNo.Text;
            string Rev = GenerateRevision(TxtAdvancePaymentDocNo.Text);
       
           

            var cml = new List<MySqlCommand>();

            if (IsDocNeedApproval() == true)
            {
                cml.Add(SaveAdvancePaymentRevisionHdr(DocNo, DocNoAdvancePayment, Rev));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                        cml.Add(SaveAdvancePaymentRevisionDtl(DocNo, Row, DocNoAdvancePayment));
            }
            else
            {
                cml.Add(SaveAdvancePaymentRevisionHdr(DocNo, DocNoAdvancePayment, Rev));
                cml.Add(SaveAdvancePaymentHdr(DocNo, DocNoAdvancePayment, Rev));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        cml.Add(SaveAdvancePaymentRevisionDtl(DocNo, Row, DocNoAdvancePayment));
                        cml.Add(SaveAdvancePaymentDtl(DocNo, Row, DocNoAdvancePayment));
                    }
            }

            Sm.ExecCommands(cml);

            ShowDataAdvancePayment(DocNo, 1);
        }

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsTxtEmpty(TxtTop, "Term Of Payment", true) ||
                Sm.IsLueEmpty(LueMonth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsAmountNotBalance() ||
                IsStartMonthNotBalance() ||
                IsStartYearNotBalance() ||
                IsStartYear() ||
                IsTopNotValid() ||
                IsAmountNotValid();
        }

        private bool IsDocNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocType ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='AdvancePaymentRevision' ");
            SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            if (Sm.IsDataExist(cm))
            {
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Month is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Year is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Amount is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsStartMonthNotBalance()
        {
            if ( Sm.GetLue(LueMonth) != Sm.GetGrdStr(Grd1, 0, 0)) 
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Month");
                return true;
            }
            return false;
        }

        private bool IsTopNotValid()
        {
            decimal top = decimal.Parse(TxtTop.Text);
            decimal dtl = (Grd1.Rows.Count -1);
            if (top != dtl)
            {
               Sm.StdMsg(mMsgType.Warning, "Number Of Month is Not Valid .");
               return true;
            }
            return false;
        }

        private bool IsStartYearNotBalance()
        {
            if (Sm.GetLue(LueYr) != Sm.GetGrdStr(Grd1, 0, 2))
            {
                Sm.StdMsg(mMsgType.Warning, "Year is not balance");
                return true;
            }
            return false;
        }

        private bool IsStartYear()
        {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    int aa = int.Parse(Sm.GetLue(LueYr));
                    int bb =(Sm.GetGrdInt(Grd1, Row, 2));

                    if (aa > bb)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid Year Information");
                        return true;
                    }
                }
           
            return false;
        }

        private bool IsAmountNotBalance()
        {
            decimal amt = decimal.Parse(TxtAmt.Text);
            decimal total = decimal.Parse(TxtTotal1.Text);
            if (amt != total )
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount is not balance.");
                return true;
            }
            return false;
        }

        private bool IsAmountNotValid()
        {
            decimal Amount = 0m;
            if (TxtAmt.Text.Length != 0) Amount = decimal.Parse(TxtAmt.Text);
            if (Amount < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Amount, Please check again.");
                return true;
            }
            return false;
        }

        private string GenerateDocNo(string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblAdvancePaymentRevisionHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblAdvancePaymentRevisionhdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("   ), '0001' ");
            SQL.Append("), '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/AP/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveAdvancePaymentRevisionHdr(string DocNo, string DocNoAdvancePayment, string Revision)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAdvancePaymentRevisionHdr(DocNo, DocDt, AdvancePaymentDocNo,AdvancePaymentDocDt, EmpCode, Amt, Top, StartMth, Yr, Revision,  Remark, Remark2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @AdvancePaymentDocNo, @AdvancePaymentDocDt, @EmpCode, @Amt, @Top, @StartMth, @Yr, @Revision,  @Remark, @Remark2, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='AdvancePaymentRevision' ");
                SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode); ");

            SQL.AppendLine("Update TblAdvancePaymentRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='AdvancePaymentRevision' And DocNo=@DocNo ");
            SQL.AppendLine("); ");


           var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo", DocNoAdvancePayment);
                Sm.CmParamDt(ref cm, "@AdvancePaymentDocDt", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text); 
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text)); 
                Sm.CmParam<Decimal>(ref cm, "@Top", Decimal.Parse(TxtTop.Text));
                Sm.CmParam<String>(ref cm, "@StartMth", Sm.GetLue(LueMonth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Revision", Revision);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@Remark2", MeeRemark2.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

             return cm;
        }

        private MySqlCommand SaveAdvancePaymentHdr(string DocNo, string DocNoAdvancePayment, string Revision)
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Update TblAdvancePaymentHdr A ");
                SQL.AppendLine("Inner Join TblAdvancePaymentRevisionHdr B On A.DocNo = B.AdvancePaymentDocNo ");
                SQL.AppendLine("Set A.Top=@Top, A.StartMth=@StartMth, A.Yr=@Yr, A.Remark=@Remark2, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where A.DocNo=@AdvancePaymentDocNo ");
                SQL.AppendLine("And B.DocNo Not in( ");
                SQL.AppendLine("    Select DocNo From TblDocApproval ");
                SQL.AppendLine("    Where DocType='AdvancePaymentRevision' And DocNo=@DocNo ");
                SQL.AppendLine("); ");

                SQL.AppendLine("Delete A  ");
                SQL.AppendLine("FROM tblAdvancePaymentDtl AS A  ");
                SQL.AppendLine("INNER JOIN TblAdvancepaymentRevisionDtl AS B ON A.DocNo = B.AdvancepaymentDocNo  ");
                SQL.AppendLine("WHERE A.Docno = @AdvancePaymentDocNo ");
                SQL.AppendLine("And B.DocNo Not In( ");
                SQL.AppendLine("    Select DocNo From TblDocApproval ");
                SQL.AppendLine("    Where DocType='AdvancePaymentRevision' And DocNo=@DocNo ");
                SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo", DocNoAdvancePayment);
            Sm.CmParamDt(ref cm, "@AdvancePaymentDocDt", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Top", Decimal.Parse(TxtTop.Text));
            Sm.CmParam<String>(ref cm, "@StartMth", Sm.GetLue(LueMonth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Remark2", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentRevisionDtl(string DocNo,int Row, string AdvancePaymentDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAdvancePaymentRevisionDtl(DocNo, DNo, AdvancePaymentDocNo, AdvancePaymentDno, Mth, Yr, Amt, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @AdvancePaymentDocNo, @AdvancePaymentDno, @Mth, @Yr, @Amt, @CreateBy, CurrentDateTime());"                    

            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo", AdvancePaymentDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveAdvancePaymentDtl(string DocNo, int Row, string AdvancePaymentDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr, Amt, CreateBy, CreateDt) " +
                    "Values (@AdvancePaymentDocNo, @DNo, @Mth, @Yr, @Amt, @CreateBy, CurrentDateTime());"

            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo", AdvancePaymentDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #endregion

        #region Show Data

        public void ShowDataAdvancePayment(string AdvancePaymentDocNo, int Type)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (Type == 0)
                {
                    ClearData2();
                }
                else
                {
                    ClearData();
                }
                ShowAdvancePaymentRevisionHdr(AdvancePaymentDocNo, Type);
                ShowAdvancePaymentRevisionDtl(AdvancePaymentDocNo, Type);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Type == 0)
                {
                    SetFormControl(mState.Insert);
                    Cursor.Current = Cursors.Default;
                }
                else
                {
                    SetFormControl(mState.View);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ShowAdvancePaymentRevisionHdr(string AdvancePaymentDocNo, int Type)
        {
            string[] data;

            var cm = new MySqlCommand();
            if (Type == 0)
            {
                Sm.CmParam<String>(ref cm, "@DocNo", AdvancePaymentDocNo);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DocNo", AdvancePaymentDocNo);
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.Empname, C.DeptName, D.PosName, E.SiteName, ");
            SQL.AppendLine("A.Amt, A.Top, A.StartMth, A.Yr, A.Remark ");
            if (Type == 0)
            {
                SQL.AppendLine(", A.DocDt As DocDtAP");
                SQL.AppendLine("From TblAdvancePaymentHdr A ");
            }
            else
            {
                SQL.AppendLine(",A.DocNo, A.DocDt, A.AdvancePaymentDocNo, A.AdvancepaymentDocDt, A.remark2, ");
                SQL.AppendLine("Case When A.Status= 'O' Then 'Outstanding' when A.Status='A' Then 'Approve' when A.Status='C' Then 'Cancelled' End as Stat ");
                SQL.AppendLine("From TblAdvancePaymentRevisionHdr A ");
            }
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C On b.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("left Join TblSite E On B.SiteCode = E.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            
            if (Type == 0)
            {
                data = new string[] 
                    { 
                        "EmpCode", 
                        "EmpName", "DeptName", "PosName", "SiteName", "Amt", 
                        "Top", "StartMth", "Yr", "Remark", "DocDtAP"
                    };
            }
            else
            {
                data = new string[] 
                    { 
                        "EmpCode", 
                        "EmpName", "DeptName", "PosName", "SiteName", "Amt", 
                        "Top", "StartMth", "Yr", "Remark",  "DocDt", 
                        "DocNo", "AdvancePaymentDocNo", "AdvancepaymentDocDt","remark2", "Stat" 
                    };
            }

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(), data,
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Type == 0)
                        {
                            Sm.SetDte(DteDocDt2, Sm.DrStr(dr, c[10]));
                        }
                        else
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[11]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[10]));
                            TxtAdvancePaymentDocNo.EditValue = Sm.DrStr(dr, c[12]);
                            Sm.SetDte(DteDocDt2, Sm.DrStr(dr, c[13]));
                            MeeRemark2.EditValue = Sm.DrStr(dr, c[14]);
                            TxtStatus.EditValue = Sm.DrStr(dr, c[15]);
                        }
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDeptName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPosName.EditValue = Sm.DrStr(dr, c[3]);
                        TxtSiteName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAmt.EditValue = Sm.DrDec(dr, c[5]);
                        TxtTop.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        Sm.SetLue(LueMonth, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[8]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        
                    }, true
                );
        }

        private void ShowAdvancePaymentRevisionDtl(string AdvancePaymentDocNo, int Type)
        {
            var cm = new MySqlCommand();

            string Tbl = "TblAdvancePayment";
            if (Type == 0)
            {
                Sm.CmParam<String>(ref cm, "@DocNo", AdvancePaymentDocNo);
                Tbl = Tbl + "Dtl";
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Tbl = Tbl + "RevisionDtl";
            }

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    " Select A.Mth As No, Case A.Mth When '01' Then 'January' When '02' Then 'February'  When '03' Then 'March' " +
                    " When '04' Then 'April' When '05' Then 'May' When '06' Then 'June' When '07' Then 'July' " +
                    " When '08' Then 'August' When '09' Then 'September' When '10' Then 'October' When '11' Then 'November' " +
                    " When '12' Then 'December' End As Mth, A.Yr, A.Amt, if(length(B.DocNo)>0, 1, 0) As Code " +
                    " From " + Tbl + " A " +
                    " Left Join TblAdvancePaymentProcess B On A.DocNo = B.Docno And A.Mth = B.Mth "+
                    " Where A.DocNo=@DocNo ", 
                    new string[] 
                    { 
                        //0
                        "No",
                        //1-3
                        "Mth", "Yr", "Amt", "Code"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        ComputeTotal(Grd, TxtTotal1);
                        CekData(Row);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
          
        }

        #endregion

        #region Additional Method

        private void CekData(int Row)
        {
            string DateNow = Sm.ServerCurrentDateTime().Substring(0, 6);
           
            Grd1.BeginUpdate();
            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdStr(Grd1, Row, 4)== "1")
            {
                Grd1.Rows[Row].ReadOnly = iGBool.True;
                Grd1.Rows[Row].CellStyle.BackColor = Color.Violet;
            }
            Grd1.EndUpdate();
        }

        static string GenerateRevision(string DocNoAP)
        {
            string Rev = Sm.GetValue("Select (CountRev) As CountRecv From ( " +
                         "Select Count(AdvancepaymentDocNo) As CountRev from TblAdvancePaymentRevisionHdr " +
                         "Where AdvancepaymentDocNo='" + DocNoAP + "' " +
                         ")X ");
            return Rev;
        }


        public void SetLueEmpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue3(
                ref Lue,
                "Select A.EmpCode As Col1, A.EmpName As Col2, B.Deptname As Col3 " +
                "From TblEmployee A " +
                "Inner Join TblDepartment B On A.DeptCode = B.DeptCode "+
                "Where (A.ResignDt Is Not Null And A.ResignDt>='" + Sm.GetDte(DteDocDt) + "' ) " +
                "Or A.ResignDt Is Null " +
                "Order By A.EmpName",
                0, 35, 40, false, true, true, "Code", "Name", "Department", "Col2", "Col1");
        }

        public static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "select '01' As Col1, 'January' As Col2 union All " +
                "select '02','February' Union All " +
                "select '03','March' Union All " +
                "select '04','April' Union All " +
                "select '05','May' Union All " +
                "select '06','June' Union All "+
                "select '07','July' Union All "+
                "select '08','August' Union All "+
                "select '09','September' Union All "+
                "select '10','October' Union All "+
                "select '11','November' Union All "+
                "select '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
     
        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void GrdItemAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e, iGrid Grd)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd, new int[] { 3 }, e);
            if (e.ColIndex == 3)
            {
                ComputeTotal(Grd, TxtTotal1);

               Sm.FocusGrd(Grd, e.RowIndex + 1, 3);
            }
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit Txt)
        {
            decimal Total1 = 0m;
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
              if (Sm.GetGrdStr(Grd, Row, 3).Length != 0) Total1 += Sm.GetGrdDec(Grd, Row, 3);
            Txt.Text = Sm.FormatNum(Total1, 0);
        }

        private void ComputeTOP()
        {
            decimal dtl = (Grd1.Rows.Count - 1);
            TxtTop.EditValue = Sm.FormatNum(dtl, 0);
        }

        #endregion

        #endregion

        #region Event

        private void LueMonth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMonth, new Sm.RefreshLue1(SetLueMonth));
        }

        

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMth, new Sm.RefreshLue1(SetLueMonth));
        }

        private void LueMth_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueMth_Leave(object sender, EventArgs e)
        {
            if (LueMth.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueMth).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueMth);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueMth.GetColumnValue("Col2");
                }
                LueMth.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, (fCell.RowIndex + 1), new int[] { 3 });
            }
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void BtnAdvancePaymentDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAdvancePaymentRevisionDlg(this));
        }

        #endregion
       
    }
}
