﻿#region update
/*
    11/12/2019 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTravelRequest3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTravelRequest3 mFrmParent;
        private iGrid mGrd;
        private string mSQL = string.Empty;
        private string mSiteCode = string.Empty;

        #endregion

        #region Constructor

        public FrmTravelRequest3Dlg(FrmTravelRequest3 FrmParent, iGrid Grd, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mGrd = Grd;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        //1-5
                        "", 
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name", 
                        "Old Code",
                        "Position",
                        //6-10
                        "Department",
                        "Site",
                        "Grade",
                        "Level",
                        "Level Dno",
                        //11-15
                        "Breakfast Amount",
                        "Grade",
                        "Level",
                        "Level Dno",
                        "Lunch Amount",
                        //16-20
                        "Grade",
                        "Level",
                        "Level Dno",
                        "Dinner Amount",
                        "Grade",
                        //21-25
                        "Level",
                        "Level Dno",
                        "Daily Amount",
                        "Grade",
                        "Level",
                        //26-30
                        "Level Dno",
                        "City Transport Amount",
                        "Grade",
                        "Level",
                        "Level Dno",
                        //31-35
                        "Transport Amount",
                        "Grade",
                        "Level",
                        "Level Dno",
                        "Accomodation Amount"
                    },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    20, 100, 200, 100, 180,
                    //6-10
                    180, 180, 80, 80, 80, 
                    //11-15
                    120, 80, 80, 80, 120,
                    //16-20
                    80, 80, 80, 120, 80, 
                    //21-25
                    80, 80, 120, 80, 80, 
                    //26-30
                    80, 120, 80, 80, 80, 
                    //31-35
                    120, 80, 80, 80, 120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35});
            Sm.GrdFormatDec(Grd1, new int[] { 11, 15, 19, 23, 26, 31, 35 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
            23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35}, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.SiteCode, D.SiteName, ");
            SQL.AppendLine("E.GrdLvlCode Gd1, E.levelCode Lv1, E.Dno LvDno1, ifnull(E.Amt, 0) Amt1, ");
            SQL.AppendLine("F.GrdLvlCode Gd2, F.levelCode Lv2, F.Dno LvDno2, ifnull(F.Amt, 0) Amt2, ");
            SQL.AppendLine("G.GrdLvlCode Gd3, G.levelCode Lv3, G.Dno LvDno3, ifnull(G.Amt, 0) Amt3, ");
            SQL.AppendLine("H.GrdLvlCode Gd4, H.levelCode Lv4, H.Dno LvDno4, ifnull(H.Amt, 0) Amt4, ");
            SQL.AppendLine("I.GrdLvlCode Gd5, I.levelCode Lv5, I.Dno LvDno5, ifnull(I.Amt, 0) Amt5, ");
            SQL.AppendLine("J.GrdLvlCode Gd6, J.levelCode Lv6, J.Dno LvDno6, ifnull(J.Amt, 0) Amt6, ");
            SQL.AppendLine("K.GrdLvlCode Gd7, K.levelCode Lv7, K.Dno LvDno7, ifnull(K.Amt, 0) Amt7 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeBreakfast')  ");
            SQL.AppendLine(")E On A.EmpCode = E.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeLunch')  ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeDinner')  ");
            SQL.AppendLine(")G On A.EmpCode = G.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeDailyAllowance')  ");
            SQL.AppendLine(")H On A.EmpCode = H.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeCityTransport') "); 
            SQL.AppendLine(")I On A.EmpCode = I.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
            if (mFrmParent.mDocTitle == "TWC")
            {
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeTransport2')  ");
            }
            else
            {
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeTransport')  ");
            }
            SQL.AppendLine(")J On A.EmpCode = J.EmpCode  ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt ");
	        SQL.AppendLine("    From Tblemployee A ");
	        SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
	        SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
	        SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
	        SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeAccomodation')  ");
            SQL.AppendLine(")K On A.EmpCode = K.EmpCode ");
            SQL.AppendLine("Where A.ResignDt Is Null And A.SiteCode='"+mSiteCode+"' ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();
                var EmpCode = string.Empty;

                if (mGrd.Rows.Count >= 1)
                {
                    for (int r = 0; r < mGrd.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(mGrd, r, 2);
                        if (EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += "(A.EmpCode<>@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                            Sm.CmParam<String>(ref cm, "@SiteCode" + r.ToString(), mSiteCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.EmpName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode",
                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName",
                            //6-10
                            "Gd1", "Lv1", "LvDno1", "Amt1", "Gd2", 
                            //11-15
                            "Lv2", "LvDno2", "Amt2", "Gd3", "Lv3", 
                            //16-20
                            "LvDno3", "Amt3",  "Gd4", "Lv4", "LvDno4", 
                            //21-25
                            "Amt4", "Gd5", "Lv5", "LvDno5", "Amt5",
                            //26-30
                            "Gd6", "Lv6", "LvDno6", "Amt6", "Gd7", 
                            //31-33
                            "Lv7", "LvDno7", "Amt7",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 33);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                int Row1 = 0, Row2 = 0, Row3 = 0;
                int RowA = 0, RowB = 0, RowC = 0;
                int RowD = 0, RowE = 0, RowF = 0;
                bool IsChoose = false;

                if (Grd1.Rows.Count != 0)
                {
                    mFrmParent.Grd1.BeginUpdate();
                    mFrmParent.Grd2.BeginUpdate();
                    mFrmParent.Grd3.BeginUpdate();
                    mFrmParent.Grd4.BeginUpdate();
                    mFrmParent.Grd5.BeginUpdate();
                    mFrmParent.Grd6.BeginUpdate();
                    mFrmParent.Grd7.BeginUpdate();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsEmpCodeAlreadyChosenMain(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            //Row1 = mGrd.Rows.Count - 1;
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            RowA = mFrmParent.Grd2.Rows.Count - 1;
                            RowB = mFrmParent.Grd3.Rows.Count - 1;
                            RowC = mFrmParent.Grd4.Rows.Count - 1;
                            RowD = mFrmParent.Grd5.Rows.Count - 1;
                            RowE = mFrmParent.Grd6.Rows.Count - 1;
                            RowF = mFrmParent.Grd7.Rows.Count - 1;
                            Row2 = Row;

                            //Grd1
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
                            //Grd2
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, RowA, new int[] { 7, 8, 12, 13, 17, 18, 19 });
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 4, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 5, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 6, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 7, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 9, Grd1, Row2, 12);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 10, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 11, Grd1, Row2, 14);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 12, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 14, Grd1, Row2, 16);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 15, Grd1, Row2, 17);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 16, Grd1, Row2, 18);
                            Sm.CopyGrdValue(mFrmParent.Grd2, RowA, 17, Grd1, Row2, 19);
                            mFrmParent.Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 7, 8, 12, 13, 17, 18, 19 });
                            //Grd3 
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, RowB, new int[] { 7, 8, 9 });
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 4, Grd1, Row2, 20);
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 5, Grd1, Row2, 21);
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 6, Grd1, Row2, 22);
                            Sm.CopyGrdValue(mFrmParent.Grd3, RowB, 7, Grd1, Row2, 23);
                            mFrmParent.Grd3.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 7, 8, 9 });
                            //Grd4
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd4, RowC, new int[] { 7, 8, 9 });
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 4, Grd1, Row2, 24);
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 5, Grd1, Row2, 25);
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 6, Grd1, Row2, 26);
                            Sm.CopyGrdValue(mFrmParent.Grd4, RowC, 7, Grd1, Row2, 27);
                            mFrmParent.Grd4.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 7, 8, 9 });
                            //Grd5
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd5, RowD, new int[] { 8, 9, 10 });
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 5, Grd1, Row2, 28);
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 6, Grd1, Row2, 29);
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 7, Grd1, Row2, 30);
                            Sm.CopyGrdValue(mFrmParent.Grd5, RowD, 8, Grd1, Row2, 31);
                            mFrmParent.Grd5.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd5, mFrmParent.Grd5.Rows.Count - 1, new int[] { 8, 9, 10 });
                            //Grd6
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd6, RowE, new int[] { 8, 9, 10 });
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 5, Grd1, Row2, 32);
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 6, Grd1, Row2, 33);
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 7, Grd1, Row2, 34);
                            Sm.CopyGrdValue(mFrmParent.Grd6, RowE, 8, Grd1, Row2, 35);
                            mFrmParent.Grd6.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd6, mFrmParent.Grd6.Rows.Count - 1, new int[] { 8, 9, 10 });
                            //Grd7 
                            Sm.CopyGrdValue(mFrmParent.Grd7, RowF, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd7, RowF, 3, Grd1, Row2, 3);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd7, RowF, new int[] { 9, 10, 11 });
                            mFrmParent.Grd7.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd7, mFrmParent.Grd7.Rows.Count - 1, new int[] { 9, 10, 11 });

                        }
                    }
                    mFrmParent.Grd1.EndUpdate();
                    mFrmParent.Grd2.EndUpdate();
                    mFrmParent.Grd3.EndUpdate();
                    mFrmParent.Grd4.EndUpdate();
                    mFrmParent.Grd5.EndUpdate();
                    mFrmParent.Grd6.EndUpdate();
                    mFrmParent.Grd7.EndUpdate();
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mGrd.Rows.Count - 1; r++)
                if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(mGrd, r, 2))) return true;
            return false;
        }

        private bool IsEmpCodeAlreadyChosenMain(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(mFrmParent.Grd1, r, 2)))
                    return true;
            return false;
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
