﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvRawMaterial2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvRawMaterial2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvRawMaterial2Find(FrmRecvRawMaterial2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.QueueNo, ");
            SQL.AppendLine("Case A.DocType When '1' Then 'Log' When '2' Then 'Balok' End As DocTypeDesc, ");
            SQL.AppendLine("    Case SectionNo ");
            SQL.AppendLine("        When '1' Then A.Shelf1 ");
            SQL.AppendLine("        When '2' Then A.Shelf2 ");
            SQL.AppendLine("        When '3' Then A.Shelf3 ");
            SQL.AppendLine("        When '4' Then A.Shelf4 ");
            SQL.AppendLine("        When '5' Then A.Shelf5 ");
            SQL.AppendLine("        When '6' Then A.Shelf6 ");
            SQL.AppendLine("        When '7' Then A.Shelf7 ");
            SQL.AppendLine("        When '8' Then A.Shelf8 ");
            SQL.AppendLine("        When '9' Then A.Shelf9 ");
            SQL.AppendLine("        When '10' Then A.Shelf10 ");
            SQL.AppendLine("        When '11' Then A.Shelf11 ");
            SQL.AppendLine("        When '12' Then A.Shelf12 ");
            SQL.AppendLine("        When '13' Then A.Shelf13 ");
            SQL.AppendLine("        When '14' Then A.Shelf14 ");
            SQL.AppendLine("        When '15' Then A.Shelf15 ");
            SQL.AppendLine("End As Shelf, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then Concat(C.ItName, ' (P:', Convert(Format(C.Length, 2) Using utf8), ', D:', Convert(Format(C.Diameter, 2) Using utf8), ')') ");
            SQL.AppendLine("    When '2' Then Concat(C.ItName, ' (P:', Convert(Format(C.Length, 2) Using utf8), ', T:', Convert(Format(C.Height, 2) Using utf8), ', L:', Convert(Format(C.Width, 2) Using utf8), ')') ");
            SQL.AppendLine("End As ItName, ");
            SQL.AppendLine("B.ItCode, B.Qty, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, ");
            SQL.AppendLine("IfNull(B.LastUpBy, Case When A.CreateDt=B.CreateDt Then Null Else B.CreateBy End) As LastUpBy, ");
            SQL.AppendLine("IfNull(B.LastUpDt, Case When A.CreateDt=B.CreateDt Then Null Else B.CreateDt End) As LastUpDt ");
            SQL.AppendLine("From TblRecvRawMaterial2Hdr A ");
            SQL.AppendLine("Inner Join TblRecvRawMaterial2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocType=@DocType ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Dokumen#", 
                        "Date",
                        "Cancel",
                        "Type",
                        "Queue#",
                        
                        //6-10
                        "Rak",
                        "Item Code",
                        "Item Name",
                        "Quantity",
                        "Created"+Environment.NewLine+"By",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 100, 150,  
                        
                        //6-10
                        100, 100, 250, 100, 100,
                        
                        //11-15
                        300, 100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocType", mFrmParent.mType.ToString());
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtLoadingQueue.Text, "A.QueueNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "DocTypeDesc", "QueueNo", "Shelf", 
                            
                            //6-10
                            "ItCode", "ItName", "Qty", "CreateBy", "CreateDt", 
                            
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, true, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkLoadingQueue_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Queue#");
        }

        private void TxtLoadingQueue_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
