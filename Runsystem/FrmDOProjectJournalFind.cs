﻿#region Update
/*
    09/02/2021 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOProjectJournalFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOProjectJournal mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOProjectJournalFind(FrmDOProjectJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DO#", 
                        "Journal#",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",

                        //6-8
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        30,
                        //1-5
                        180, 180, 100, 100, 100,
                        //6-10
                        100, 100, 100                    
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 7 });
            Sm.GrdFormatTime(Grd1, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DODocNo, JournalDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblDOProjectJournal ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDODocNo.Text, "DODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtJournalDocNo.Text, "JournalDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By CreateDt Desc; ",

                    new string[]
                    {
                        //0
                        "DODocNo", 
                            
                        //1-5
                        "JournalDOcNo", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 5);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void TxtJournalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkJournalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Journal#");
        }

        #endregion

        #endregion
    }
}
