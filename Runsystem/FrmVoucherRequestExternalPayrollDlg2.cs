﻿#region Update
/*
    06/02/2020 [WED/YK] selama semua Dropping Request sudah fulfilled dan ada remuneration nya, bisa ditarik
    25/01/2023 [TYO/MNET] menambah kolom Dropping Request Type berdasarkan parameter IsDroppingRequestUseType
    25/01/2023 [TYO/MNET] validasi hanya menarik DRQ dengan type payroll berdasarkan parameter IsDroppingRequestUseType
    22/02/2023 [MYA/MNET] Penyesuaian menu Voucher for External Payroll
    07/03/2023 [MYA/MNET] BUG: Bisa save VR external payroll tanpa milih account from
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestExternalPayrollDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestExternalPayroll mFrmParent;
        private string 
            mSQL = string.Empty,
            mBankCode = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestExternalPayrollDlg2(FrmVoucherRequestExternalPayroll FrmParent, string BankCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBankCode = BankCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueVdCode(ref LueVdCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "DNo", 
                    "Item Code", 
                    "Item Name",
                    "Vendor Code",

                    //6-10
                    "Vendor",
                    "Bank Account",
                    "Remuneration Amount",
                    "Dropping#",
                    "PRJI#",

                    //11-14
                    "Project Name",
                    "Customer",
                    "Site",
                    "Dropping Request Type"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 80, 100, 190, 120, 
                    
                    //6-10
                    200, 200, 150, 180, 180,

                    //11-14
                    200, 200, 200, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5 });
            Sm.SetGrdProperty(Grd1, false);
            if(!mFrmParent.mIsDroppingRequestUseType)
                Sm.GrdColInvisible(Grd1, new int[] { 14 });
            Grd1.Cols[9].Move(2);
            Grd1.Cols[14].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT A.DocNo, A.DNo, F.ItCode, F.ItName, A.VdCode, B.VdName, ");
            //SQL.AppendLine("Concat('[', C.BankAcNo, '] ', C.BankAcName, ' ', D.BankName) BankAcNo, A.RemunerationAmt ");
            //SQL.AppendLine("FROM TblDroppingRequestDtl A ");
            //SQL.AppendLine("INNER JOIN TblVendor B ON A.VdCode = B.VdCode ");
            //SQL.AppendLine("INNER JOIN TblVendorBankAccount C ON A.VdCode = C.VdCode AND A.VdBankAcDNo = C.DNo ");
            //SQL.AppendLine("INNER JOIN TblBank D ON C.BankCode = D.BankCode AND D.BankCode = @BankCode ");
            //SQL.AppendLine("INNER JOIN TblProjectImplementationRBPHdr E ON A.PRBPDocNo = E.DocNo ");
            //SQL.AppendLine("INNER JOIN TblItem F ON E.ResourceItCode = F.ItCode ");
            //SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @SelectedDroppingRequest) < 1 ");

            SQL.AppendLine("SELECT T.* FROM ( ");
            SQL.AppendLine("    SELECT A.DocNo, B.DNo, A.DeptCode, Null As DeptName, A.PRJIDocNo, G.ProjectName, H.CtName, I.SiteName, B.RemunerationAmt, ");
            SQL.AppendLine("    Concat('[', K.BankAcNo, '] ', K.BankAcName, ' ', L.BankName) BankAcNo, N.ItCode, N.ItName, N.ItCodeInternal, B.VdCode, J.VdName, ");
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("O.OptDesc As DroppingRequestType ");
            else
                SQL.AppendLine("NULL As DroppingRequestType ");
            SQL.AppendLine("    FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("    INNER JOIN TblDroppingRequestDtl B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("         And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("         AND B.PaidInd = 'F'  ");
            SQL.AppendLine("         AND B.RemunerationAmt != 0 ");
            //SQL.AppendLine("         AND A.DocNo NOT In ");
            //SQL.AppendLine("         ( ");
            //SQL.AppendLine("            SELECT X1.DocNo ");
            //SQL.AppendLine("            FROM TblDroppingRequestDtl X1 ");
            //SQL.AppendLine("            WHERE X1.DirectCostAmt != 0 ");
            //SQL.AppendLine("         ) ");
            SQL.AppendLine("    INNER JOIN TblProjectImplementationHdr C ON A.PRJIDocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr D ON C.SOContractDocNo = D.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSOContractHdr E ON D.SOCDocNo = E.DocNo ");
            SQL.AppendLine("    INNER JOIN TblBOQHdr F ON E.BOQDocNo = F.DocNo ");
            SQL.AppendLine("    INNER JOIN TblLOPHdr G ON F.LOPDocNo = G.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("        And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("            G.SiteCode Is Not Null ");
                SQL.AppendLine("            And Exists( ");
                SQL.AppendLine("                Select 1 From TblGroupSite ");
                SQL.AppendLine("                Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("                And GrpCode In ( ");
                SQL.AppendLine("                    Select GrpCode From TblUser ");
                SQL.AppendLine("                    Where UserCode=@UserCode ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        )) ");
            }
            SQL.AppendLine("    INNER JOIN TblCustomer H ON G.CtCode = H.CtCode ");
            SQL.AppendLine("    INNER JOIN TblSite I ON G.SiteCode = I.SiteCode ");
            
            if (mFrmParent.mIsVoucherRequestExternalPayrollHideVendor)
            {
                SQL.AppendLine("    Left Join TblVendor J On B.VdCode = J.VdCode ");
                SQL.AppendLine("    Left Join TblVendorBankAccount K On B.VdCode = K.VdCode And B.VdBankAcDNo = K.DNo ");
                SQL.AppendLine("    Left Join TblBank L On K.BankCode = L.BankCode ");
            }
            else
            {
                SQL.AppendLine("    Inner Join TblVendor J On B.VdCode = J.VdCode ");
                SQL.AppendLine("    Inner Join TblVendorBankAccount K On B.VdCode = K.VdCode And B.VdBankAcDNo = K.DNo ");
                SQL.AppendLine("    Inner Join TblBank L On K.BankCode = L.BankCode And L.BankCode = @BankCode ");
            }
            SQL.AppendLine("    Inner Join TblProjectImplementationRBPHDr M On B.PRBPDocNo = M.DocNo ");
            SQL.AppendLine("    Inner Join TblItem N On M.ResourceItCode = N.ItCode ");
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("Left Join TblOption O On A.DocType = O.OptCode And O.OptCat ='DroppingRequestDocType' ");
            SQL.AppendLine("    WHERE A.Status = 'A' AND A.CancelInd = 'N' AND A.ProcessInd = 'F' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedDroppingRequest) < 1 ");
            SQL.AppendLine("    AND CONCAT(A.DocNo, B.DNo) NOT IN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT CONCAT(T2.DroppingRequestDocNo, T2.DroppingRequestDNo) ");
            SQL.AppendLine("        FROM TblVoucherRequestExternalPayrollHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblVoucherRequestExternalPayrollDtl T2 ON T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Status IN ('O', 'A') ");
            SQL.AppendLine("    ) ");
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("And A.DocType = '5' ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SelectedDroppingRequest", mFrmParent.GetSelectedDroppingRequest());

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPRJIDocNo.Text, "T.PRJIDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "T.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName", "T.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By T.DocNo, T.DNo;",
                    new string[] 
                    { 
                        "DNo", 
                        "ItCode", "ItName", "VdCode", "VdName", "BankAcNo", 
                        "RemunerationAmt", "DocNo", "PRJIDocNo", "ProjectName", "CtName", 
                        "SiteName", "DroppingRequestType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDroppingRequestAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        mFrmParent.ComputeAmt();

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDroppingRequestAlreadyChosen(int Row)
        {
            string DroppingRequestData = string.Concat(Sm.GetGrdStr(Grd1, Row, 9), Sm.GetGrdStr(Grd1, Row, 2));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), Sm.GetGrdStr(mFrmParent.Grd1, Index, 7)), DroppingRequestData)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPRJIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPRJIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Impl.#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
