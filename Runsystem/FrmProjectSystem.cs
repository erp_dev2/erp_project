﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectSystem : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private bool mIsProjectSystemActive = false;
        internal FrmProjectSystemFind FrmFind;

        #endregion

        #region Constructor

        public FrmProjectSystem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                SetLueProfitCenterCode(ref LueProfitCenterCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Description",

                        //1-5
                        "Account#",
                        "Document",
                        "Allow",
                        "Qty",
                        "Uom",
                        //6-10
                        "Category",
                        "Price",
                        "Amount",
                        "AmtValidasi",
                        "AmtPrev"
                    },
                    new int[] 
                    {
                        300,
                        150, 150, 20, 100, 80,
                        100, 100, 150, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 7, 8, 9, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10});
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 9, 10 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt,LueProfitCenterCode, TxtProjectDocNo,  MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueProfitCenterCode, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueProfitCenterCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 7, 8, 9, 10 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 8)
            {
                decimal Amt = Sm.GetGrdDec(Grd1, e.RowIndex, 9);
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < e.RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd1, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd1, e.RowIndex, 8));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd1, y, 8));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd1.Cells[y, 8].Value = Qty;
                                    Grd1.Cells[y, 9].Value = Qty;
                                    Grd1.Cells[e.RowIndex, 9].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this account");
                }
            }
        }



        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectSystemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectSystem", "TblProjectSystemHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePSHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 8) > 0m
                    )
                    cml.Add(SavePSDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Project / Profit Center")||
                IsGrdEmpty() ||
                IsDocAlreadyCreated()
                ;
        }


        private bool IsDocAlreadyCreated()
        {
            var ProjectDocNo = Sm.GetLue(LueProfitCenterCode);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProjectSystemHdr ");
            SQL.AppendLine("Where ProjectDocNo=@ProjectDocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ProjectDocNo", ProjectDocNo);

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Project system has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePSHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblProjectSystemHdr(DocNo, DocDt, ProjectDocNo, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ProjectDocNo, 'N', @Remark, @CreateBy, @CreateDt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProjectDocNo", TxtProjectDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SavePSDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblProjectSystemDtl(DocNo, ProjectDocNo, Qty, Uom, Category, Price, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("values(@DocNo, @ProjectDocNo, @Qty, @uom, @Category, @Price, @Amt, @CreateBy, @CreateDt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Uom", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Category", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Price", Sm.GetGrdDec(Grd1, Row, 7)); 
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPSHdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 8) != Sm.GetGrdDec(Grd1, Row, 10)
                    )
                    cml.Add(EditPSDtl(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblProjectSystemHdr Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPSHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblProjectSystemHdr Set " +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditPSDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblProjectSystemDtl Set " +
                    "   Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And ProjectDocNo=@ProjectDocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPSHdr(DocNo);
                ShowAccount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPSHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.ProjectDocNo, A.CancelInd, A.Remark " +
                    "From TblProjectSystemHdr A " +
                    "Inner Join TblProject B On A.ProjectDocno = B.DocNo " +
                    "Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode "+
                    "Where A.DocNo=@DocNo; ",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-4
                        "DocDt", "ProjectDocNo", "CancelInd", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtProjectDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsProjectSystemActive = Sm.GetParameter("mIsProjectSystemActive") == "Y";
        }

        private void SetLueProfitCenterCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo As Col1, Concat(AcDesc, ' (', AcCode, ')') As Col2 ");
            SQL.AppendLine("From TblProject ");
            SQL.AppendLine("Where ParentDocNo is null And level=1 And ActInd = 'Y' ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowAccount()
        {
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblProject Where DocNo = '"+TxtProjectDocNo.Text+"'");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select A.DocNo As ProjectDocNo, A.AcCode, concat(A.AcCode, ' ', A.AcDesc) As AcDesc, ");
                        SQL.AppendLine("If(B.DocNo Is Null, 'N', 'Y') As Allow, 0 As Amt ");
                        SQL.AppendLine("From TblProject A ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("    Select DocNo From TblProject ");
                        SQL.AppendLine("    Where DocNo Not In ( ");
                        SQL.AppendLine("        Select ParentDocNo From TblProject ");
                        SQL.AppendLine("        Where ParentDocNo Is Not Null ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine(") B On A.Docno=B.DocNo ");
                        SQL.AppendLine("Where A.ProfitCenterCode=@ProfitCenterCode And A.ActInd = 'Y'  ");
                        SQL.AppendLine("Order by A.AcCode ; ");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
                    }
                    else
                    {
                        SQL.AppendLine("Select A.ProjectDocno, C.AcCode, Concat(C.AcCode, C.AcDesc) As AcDesc, ");
                        SQL.AppendLine("If(B.DocNo Is Null, 'N', 'Y') As Allow, A.Amt ");
                        SQL.AppendLine("From TblProjectSystemDtl A ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("    Select DocNo, AcCode, AcDesc, ParentDocNo From TblProject ");
                        SQL.AppendLine("    Where DocNo Not In ( ");
                        SQL.AppendLine("        Select ParentDocNo From TblProject ");
                        SQL.AppendLine("        Where ParentDocNo Is Not Null ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine(") B On A.ProjectDocno=B.DocNo  ");
                        SQL.AppendLine("Inner Join TblProject C On A.projectDocNo = C.DocNo And ActInd = 'Y' ");
                        SQL.AppendLine("Where A.DocNo=@DocNo Order By C.AcCode, C.level ; ;");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "AcDesc", "AcCode",  "ProjectDocNo", "Allow", "Amt" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2); 
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd1.Cells[Row, 4].Value = 0;
                                Grd1.Cells[Row, 5].Value = string.Empty;
                                Grd1.Cells[Row, 6].Value = string.Empty;
                                Grd1.Cells[Row, 7].Value = 0;
                                Grd1.Cells[Row, 8].Value = 0;
                                Grd1.Cells[Row, 9].Value = 0;
                                Grd1.Cells[Row, 10].Value = 0;
                            }
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 4);
                            if (Sm.GetGrdStr(Grd1, Row, 3) == "N")
                            {
                                Grd1.Rows[Row].ReadOnly = iGBool.True;
                                Grd1.Rows[Row].BackColor = Color.FromArgb(224, 224, 224);
                            }
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevMenuCode = Sm.DrStr(dr, 1);
                            Row++;
                        }
                        //if (Row > 0)
                        //    Grd1.Rows[Row - 1].TreeButton =
                        //        (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                        //            iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Event
        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue1(SetLueProfitCenterCode));
            if (BtnSave.Enabled)
            {
                TxtProjectDocNo.EditValue = Sm.GetLue(LueProfitCenterCode);
            }
            Sm.ClearGrd(Grd1, true);
        }

        private void BtnShowAc_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center")) 
                ShowAccount();

        }
        #endregion
    }
}
