﻿#region Update
/* 
    17/04/2017 (HAR) tambah informasi tebal dari master item dan negara / provoince ambil dari SO
    21/02/2018 (HAR) tambah informasi SO
    01/04/2019 [DITA] Pembenahan source Negara/Tujuan & Negara Provinsi di Reporting Export Performance 
    09/04/2019 [MEY] menambah informasi UOM Item
    20/04/2019 [TKG] Negara/Provinsi diganti menjadi negara saja.
    25/04/2019 [TKG] yg local ngambil dari propinsi berdasarkan city di SO yg menggunakan DR sedangkan export ambil dari SP (placeDelivery).
    11/07/2019 [TKG] propinsi/negara diambil dari DR untuk lokal
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDinasKehutanan3 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDinasKehutanan3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

            #region Standard Methods

            override protected void FrmLoad(object sender, EventArgs e)
            {
                try
                {
                    mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                    Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                    Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                    SetGrd();
                    SetSQL();
                    base.FrmLoad(sender, e);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            override protected void SetSQL()
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From (");
                SQL.AppendLine("SELECT  X.Name, X.LocalDocno, X.DocDt, X.Sortimen, X.ItName, SUM(IFNULL(X.Qty, 0)) AS Qty, ");
                SQL.AppendLine("SUM(IFNULL(X.QtyInventory, 0)) QtyInventory, X.CtName, X.Destination, X.LocalDOcNo2, ");
                SQL.AppendLine("if(X.OSInd = 'Y', 'Eksport', 'Local') As OSInd, X.Height, X.heightUomCode,    ");
                SQL.AppendLine("X.CntName As Location, X.Length, X.LengthUomCode, X.Width, X.WidthUomCode, X.SODocNo,  X.PurchaseUOMCode  ");
                SQL.AppendLine("FROM( ");
                SQL.AppendLine("     SELECT A.DocNo, 'NOTA ANGKUTAN' AS Name, A.LocalDocNo, A.DocDt, A.Sortimen As Sortimen2,  Ifnull(D.ItName, E.ItName) As ItName, ");
                SQL.AppendLine("     IFNULL(D.Qty2, E.Qty2) AS Qty, Ifnull(D.ItGrpName, E.ItGrpName) As Sortimen, ");
                SQL.AppendLine("     IFNULL(D.QtyInventory, E.QtyInventory) AS QtyInventory, ");
                SQL.AppendLine("     C.CtName, B.Destination, A.DkoDocNo, Ifnull(D.LocalDocNo, E.LocalDocNo) As LocalDocNo2,");
                SQL.AppendLine("     Ifnull(D.OSInd, E.OSInd) As OSInd, ifnull(D.height, E.Height) As Height, ifnull(D.heightuomcode, E.Heightuomcode) As Heightuomcode, ");
                SQL.AppendLine("     Ifnull(D.SACityCode, E.SACityCode) As SACityCode, Ifnull(D.PlaceDelivery, E.ProvName) As CntName, ");
                SQL.AppendLine("     ifnull(D.length, E.Length) As Length, ifnull(D.Lengthuomcode, E.Lengthuomcode) As lengthuomcode, ");
                SQL.AppendLine("     ifnull(D.Width, E.Width) As Width, ifnull(D.Widthuomcode, E.Widthuomcode) As Widthuomcode, ifnull(D.SODOcNo, E.SODocNo) As SODocno, IfNull(D.PurchaseUOMCode, E.PurchaseUomCode) PurchaseUomCode ");
                SQL.AppendLine("     FROM TblFako A ");
                SQL.AppendLine("     INNER JOIN TblDko B ON A.DKODocNo = B.Docno AND A.DocDt IN ");
                SQL.AppendLine("            (SELECT DocDt FROM TblFako WHERE DocDt BETWEEN @DocDt1 AND @DocDt2) ");
                SQL.AppendLine("     INNER JOIN TblCustomer C ON B.CtCode = C.CtCode ");
                SQL.AppendLine("     LEFT JOIN ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("          SELECT A.DocNo,  A2.SectionNo, B.ItName, A.LocalDocNo, D.OverSeaInd  As OSInd,");
                SQL.AppendLine("          ROUND(A2.QtyInventory, 4) As QtyInventory,  J.ITGrpName, ");
                SQL.AppendLine("          ROUND(if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2, B.Height, B.heightuomCode, ");
                SQL.AppendLine("          D.SACityCode, ");
                SQL.AppendLine("          Case When L.PlaceDelivery Is Null Then Null ");
                SQL.AppendLine("          Else ");
	            SQL.AppendLine("            Case When Position(',' In L.PlaceDelivery)=0 Then ");
		        SQL.AppendLine("                L.PlaceDelivery ");
	            SQL.AppendLine("            Else ");
		        SQL.AppendLine("                Trim(Substring(L.PlaceDelivery, Position(',' In L.PlaceDelivery)+1, Length(L.PlaceDelivery)-Position(',' In L.PlaceDelivery))) ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("          End As PlaceDelivery, ");
                SQL.AppendLine("          B.Length, B.LengthUomCode, B.Width, B.WidthUomCOde, D.DocNo As SoDocNo, B.PurchaseUOMCode   ");
                SQL.AppendLine("          FROM TblPlhdr A ");
                SQL.AppendLine("          INNER JOIN TblPlDtl A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblItem B On A2.ItCode = B.ItCode ");
                SQL.AppendLine("          INNER JOIN TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                SQL.AppendLine("          INNER JOIN TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                SQL.AppendLine("          INNER JOIN TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                SQL.AppendLine("          INNER JOIN TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("          LEFT JOIN TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode ");
                SQL.AppendLine("          LEFT JOIN TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                SQL.AppendLine("          Inner Join TblSIhdr K On A.SIDocNo = K.DocNo ");
                SQL.AppendLine("          Inner Join TblSP L On K.SPDocNo=L.DocNo ");
                SQL.AppendLine("     )D On B.PlDocno = D.DocNo And B.SectionNo = D.SectionNo ");
                SQL.AppendLine("     LEFT JOIN ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("          SELECT A.DocNo, F.ItName, A.LocalDocNo, J.ItGrpname, ");
                SQL.AppendLine("          ROUND(A2.Qty, 0) As Qty, ROUND(A2.QtyInventory, 4) As QtyInventory, B.OverSeaInd As OSInd, ");
                SQL.AppendLine("          ROUND(if(I.Qty =0, 0, if(G.PriceUomCode = F.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2, F.height, F.heightuomCode, ");
                SQL.AppendLine("          A.SACityCode, L.ProvName, F.Length, F.LengthUomCode, F.Width, F.WidthUomCOde, B.DocNo As SoDocNo, F.PurchaseUoMCode  ");
                SQL.AppendLine("          FROM TblDRhdr A ");
                SQL.AppendLine("          INNER JOIN TblDRDtl A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblSOHdr B On A2.SODocNo=B.DocNo ");
                SQL.AppendLine("          INNER JOIN TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                SQL.AppendLine("          INNER JOIN TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                SQL.AppendLine("          INNER JOIN TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo ");
                SQL.AppendLine("          INNER JOIN TblItem F On E.ItCode=F.ItCode ");
                SQL.AppendLine("          INNER JOIN TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("          LEFT JOIN TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                SQL.AppendLine("          LEFT JOIN TblItemGroup J On F.ItGrpCode = J.ItGrpCode ");
                SQL.AppendLine("          Left Join tblCity K On A.SACityCode = K.CityCode  ");
                SQL.AppendLine("          Left Join TblProvince L On K.ProvCode=L.ProvCode  ");
                SQL.AppendLine("          Where A.CancelInd='N' ");
                SQL.AppendLine("     )E On B.DrDocNo = E.DocNo");
                SQL.AppendLine("     Where A.CancelInd='N' ");
                SQL.AppendLine(")X ");
               
                SQL.AppendLine("Left Join TblCity X2 On X.SACityCode = X2.CityCode ");
                SQL.AppendLine("Left Join TblProvince X3 On X2.ProvCode = X3.ProvCode ");
                SQL.AppendLine("GROUP BY X.Docno, X.Name, X.LocalDocno, X.DocDt, X.Sortimen, X.ItName, X.CtName, X.Destination");
                SQL.AppendLine(") Z ");

                mSQL = SQL.ToString();
            }

            private void SetGrd()
            {
                Grd1.Cols.Count = 21;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                        {
                            //0
                            "No.",

                            //1-5
                            "Dokumen"+Environment.NewLine+"Eksport",
                            "Local Dokumen"+Environment.NewLine+"Eksport", 
                            "Tanggal"+Environment.NewLine+"Stuffing",
                            "Jenis"+Environment.NewLine+"Hasil Hutan",
                            "Nama Item",

                            //6-10
                            "Tebal",
                            "Satuan",
                            "Panjang",
                            "Satuan",
                            "Lebar",

                            //11-15
                            "Satuan",
                            "Jumlah",
                            "Vol./Berat",
                            "UOM", 
                            "Tujuan"+Environment.NewLine+"Pengangkutan",
                           

                            //16-20
                            "Negara Tujuan",
                            "Propinsi/Negara",
                            "Local",
                            "Eksport/Local",
                            "Sales Order"
                        },
                        new int[] 
                        {
                            //0
                            50,

                            //1-5
                            120, 150, 100, 150, 200, 
                            
                            //6-10
                            100, 100, 100, 100, 100, 
                            
                            //11-15
                            100, 100, 100, 100, 280, 

                            //16-19
                            150, 130, 120, 100, 150
                        }
                    );
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 12, 13 }, 0);
                Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 11, 20 });
                Sm.SetGrdProperty(Grd1, false);
            }

            override protected void HideInfoInGrd()
            {
                Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 11, 20 }, !ChkHideInfoInGrd.Checked);
            }

            override protected void ShowData()
            {
                Sm.ClearGrd(Grd1, false);
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                    ) return;

                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = string.Empty;

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "Z.LocalDocNo", false);
                    Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo2.Text, "Z.LocalDocNo2", false);
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ORDER BY Z.DocDt, Z.LocalDocNo;",
                        new string[]
                            { 
                                //0
                                "Name",  

                                //1-5
                                "LocalDocNo", "DocDt", "Sortimen", "ItName", "Height",    

                                //6-10
                                "HeightUomCode", "Length", "LengthUomCOde", "Width", "WidthUomCode", 
                                
                                //11-15
                                "Qty", "QtyInventory", "PurchaseUOMCode","CtName", "Destination", 
                                
                                //16-19
                                 "Location", "LocalDocNo2", "OSInd", "SODocNo"
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);

                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);

                                Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            }, true, false, false, false
                        );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13 });
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }

            }

            #endregion

            #region Grid Method

            override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
            {
                SetGrd();
                Sm.SetGridNo(Grd1, 0, 1, true);
            }

            override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
            {
                Sm.SetGridNo(Grd1, 0, 1, true);
            }

            #endregion

            #region Misc Control Method

            override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
            {
                if (Sm.GetLue(LueFontSize).Length != 0)
                {
                    Grd1.Font = new Font(
                        Grd1.Font.FontFamily.Name.ToString(),
                        int.Parse(Sm.GetLue(LueFontSize))
                        );
                }
            }

            #endregion

        #endregion

        #region Events

            #region Misc Control Events

            private void TxtLocalDocNo_Validated(object sender, EventArgs e)
            {
                Sm.FilterTxtSetCheckEdit(this, sender);
            }
            private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetTextEdit(this, sender, "Export Document#");
            }
            private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
            {
                if (Sm.GetDte(DteDocDt2).Length == 0)
                    DteDocDt2.EditValue = DteDocDt1.EditValue;
            }

            private void TxtLocalDocNo2_Validated(object sender, EventArgs e)
            {
                Sm.FilterTxtSetCheckEdit(this, sender);
            }

            private void ChkLocalDocNo2_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetTextEdit(this, sender, "Local Document#");
            }

            #endregion

        #endregion
    }
}
