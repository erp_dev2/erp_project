﻿#region Update
/*
  23/12/2020 [DITA/PHT] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptQuarterlyCBPBalanceSheet : RunSystem.FrmBase6
    {
        #region Field

        private string
       mMenuCode = string.Empty,
       mAccessInd = string.Empty,
       mSQL = string.Empty;
        private bool
            mIsAccountingRptUseJournalPeriod = false;

        #endregion

        #region Constructor

        public FrmRptQuarterlyCBPBalanceSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueProfitCenterCode(ref LueProfitCenter);
                SetLueCCCode(ref LueCCCode, string.Empty);
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Cols[0].Width = 30;

            Grd1.Header.Cells[0, 1].Value = "Account#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;
            Grd1.Cols[1].Width = 250;

            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;
            Grd1.Cols[2].Width = 250;

            Grd1.Header.Cells[1, 3].Value = "1st Quarter";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 2;
            Grd1.Header.Cells[0, 3].Value = "Total RKAP";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].Value = "Total Journal";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Cols[3].Width = 150;
            Grd1.Cols[4].Width = 150;

            Grd1.Header.Cells[1, 5].Value = "2nd Quarter";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 2;
            Grd1.Header.Cells[0, 5].Value = "Total RKAP";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].Value = "Total Journal";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Cols[5].Width = 150;
            Grd1.Cols[6].Width = 150;

            Grd1.Header.Cells[1, 7].Value = "3rd Quarter";
            Grd1.Header.Cells[1, 7].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 7].SpanCols = 2;
            Grd1.Header.Cells[0, 7].Value = "Total RKAP";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].Value = "Total Journal";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Cols[7].Width = 150;
            Grd1.Cols[8].Width = 150;

            Grd1.Header.Cells[1, 9].Value = "4th Quarter";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 2;
            Grd1.Header.Cells[0, 9].Value = "Total RKAP";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].Value = "Total Journal";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Cols[9].Width = 150;
            Grd1.Cols[10].Width = 150;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10 }, 0);
        }

        override protected void HideInfoInGrd()
        {
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year")
                ) return;
            var Yr = Sm.GetLue(LueYr);
            string SelectedCOA = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<CBP>();
                var l2 = new List<COACBP>();
                var l3 = new List<COAJournal>();
                var l4 = new List<JournalTemp>();

                PrepDataCBP(ref l, Yr);
                if (l.Count > 0)
                {
                    SelectedCOA = GetSelectedCOA(ref l);
                    PrepCOACBP(ref l2, SelectedCOA);

                    //proses total nilai RKAP
                    if (l2.Count > 0)
                    {
                        Process1(ref l, ref l2, Yr);
                    }
                    PrepCOAJournal(ref l3, SelectedCOA, Yr);
                    PrepJournalTemp(ref l4, SelectedCOA, Yr);
                    Process2(ref l3, ref l4);
                    Process3(ref l2, ref l3);
                    Process4(ref l2);
                    Process5(ref l2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10 });

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueCCCode(ref LookUpEdit Lue, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter Where CBPInd = 'Y' And ProfitCenterCode = @ProfitCenterCode Order By CCName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

        }

        private void PrepDataCBP(ref List<CBP> l, string Yr)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("SELECT A.Yr, B.AcNo, SUM(B.Amt01+B.Amt02+B.Amt03)AmtQ1, SUM(B.Amt04+B.Amt05+B.Amt06)AmtQ2,  ");
            SQL.AppendLine("SUM(B.Amt07+B.Amt08+B.Amt09)AmtQ3, SUM(B.Amt10+B.Amt11+B.Amt12)AmtQ4  ");
            SQL.AppendLine("FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo=B.DocNo AND A.CancelInd != 'Y'  ");
            if (Sm.GetLue(LueProfitCenter).Length != 0)
                SQL.AppendLine("INNER JOIN TblCostCenter C On A.CCCode = C.CCCode And C.ProfitCenterCode=@ProfitCenterCode ");
            if (Sm.GetLue(LueCCCode).Length != 0)
                SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("WHERE A.Yr = @Yr ");
            SQL.AppendLine("And A.DocType = '3' ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("; ");


            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenter));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "Yr", 

                    //1-5
                    "AcNo", 
                    "AmtQ1" ,
                    "AmtQ2",
                    "AmtQ3",
                    "AmtQ4",

                   
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CBP()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AmtQ1 = Sm.DrDec(dr, c[2]),
                            AmtQ2 = Sm.DrDec(dr, c[3]),
                            AmtQ3 = Sm.DrDec(dr, c[4]),
                            AmtQ4 = Sm.DrDec(dr, c[5]),


                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOACBP(ref List<COACBP> l2, string SelectedCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcDesc, LEVEL, Parent ");
            SQL.AppendLine("FROM TblCOA ");
            SQL.AppendLine("WHERE Find_In_Set(AcNo, @SelectedCOA); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Level", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COACBP()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrDec(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            AmtCBPQ1 = 0m,
                            AmtJNQ1 = 0m,
                            AmtCBPQ2 = 0m,
                            AmtJNQ2 = 0m,
                            AmtCBPQ3 = 0m,
                            AmtJNQ3 = 0m,
                            AmtCBPQ4 = 0m,
                            AmtJNQ4 = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<CBP> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        //Total RKAP
        private void Process1(ref List<CBP> l, ref List<COACBP> l2, string Yr)
        {
            foreach (var x in l)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtCBPQ1 += x.AmtQ1;
                    y.AmtCBPQ2 += x.AmtQ2;
                    y.AmtCBPQ3 += x.AmtQ3;
                    y.AmtCBPQ4 += x.AmtQ4;
                }
            }
        }

        private void PrepCOAJournal(ref List<COAJournal> l3, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcType ");
            SQL.AppendLine("FROM TblCOA  ");
            SQL.AppendLine("WHERE AcNo IN( ");
            SQL.AppendLine("	SELECT DISTINCT B.AcNo ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else
            {
                SQL.AppendLine("	AND LEFT(A.DocDt, 4) = @Yr ");
            }
            SQL.AppendLine(") ");

            SQL.AppendLine("AND ActInd = 'Y' ");
            SQL.AppendLine("AND Parent IS NOT NULL  ");
            SQL.AppendLine("AND AcNo NOT IN ( ");
            SQL.AppendLine("SELECT Parent FROM TblCOA WHERE Parent IS NOT null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DAmtQ1 = 0m,
                            CAmtQ1 = 0m,
                            DAmtQ2 = 0m,
                            CAmtQ2 = 0m,
                            DAmtQ3 = 0m,
                            CAmtQ3 = 0m,
                            DAmtQ4 = 0m,
                            CAmtQ4 = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepJournalTemp(ref List<JournalTemp> l4, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("SELECT A.AcNo, SUM(A.DAmt) DAmtQ1, SUM(A.CAmt) CAmtQ1, 0.00 As DAmtQ2, 0.00 As CAmtQ2, ");
            SQL.AppendLine("0.00 As DAmtQ3, 0.00 As CAmtQ3, 0.00 As DAmtQ4, 0.00 As CAmtQ4 ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '03') ");
            }
            else
            {
                SQL.AppendLine("AND LEFT(B.DocDt, 6) Between CONCAT(@Yr, '01') AND CONCAT(@Yr, '03') ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("SELECT A.AcNo, 0.00 As DAmtQ1, 0.00 As CAmtQ1, SUM(A.DAmt) As DAmtQ2, SUM(A.CAmt) As CAmtQ2, ");
            SQL.AppendLine("0.00 As DAmtQ3, 0.00 As CAmtQ3, 0.00 As DAmtQ4, 0.00 As CAmtQ4 ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '04') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '06') ");
            }
            else
            {
                SQL.AppendLine("AND LEFT(B.DocDt, 6) Between CONCAT(@Yr, '04') AND CONCAT(@Yr, '06') ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("SELECT A.AcNo, 0.00 As DAmtQ1, 0.00 As CAmtQ1,  0.00 As DAmtQ2, 0.00 As CAmtQ2, ");
            SQL.AppendLine("SUM(A.DAmt) As DAmtQ3, SUM(A.CAmt) As CAmtQ3, 0.00 As DAmtQ4, 0.00 As CAmtQ4 ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '07') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '09') ");
            }
            else
            {
                SQL.AppendLine("AND LEFT(B.DocDt, 6) Between CONCAT(@Yr, '07') AND CONCAT(@Yr, '09') ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("SELECT A.AcNo, 0.00 As DAmtQ1, 0.00 As CAmtQ1,  0.00 As DAmtQ2, 0.00 As CAmtQ2, ");
            SQL.AppendLine("0.00 As DAmtQ3, 0.00 As CAmtQ3, SUM(A.DAmt) As DAmtQ4, SUM(A.CAmt) As CAmtQ4 ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '10') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else
            {
                SQL.AppendLine("AND LEFT(B.DocDt, 6) Between CONCAT(@Yr, '10') AND CONCAT(@Yr, '12') ");
            }

            SQL.AppendLine(")T ");
            SQL.AppendLine("GROUP BY T.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "AcNo", 
                    "DAmtQ1", "CAmtQ1", "DAmtQ2", "CAmtQ2", "DAmtQ3",
                    "CAmtQ3", "DAmtQ4", "CAmtQ4",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new JournalTemp()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmtQ1 = Sm.DrDec(dr, c[1]),
                            CAmtQ1 = Sm.DrDec(dr, c[2]),
                            DAmtQ2 = Sm.DrDec(dr, c[3]),
                            CAmtQ2 = Sm.DrDec(dr, c[4]),
                            DAmtQ3 = Sm.DrDec(dr, c[5]),
                            CAmtQ3 = Sm.DrDec(dr, c[6]),
                            DAmtQ4 = Sm.DrDec(dr, c[7]),
                            CAmtQ4 = Sm.DrDec(dr, c[8]),

                        });
                    }
                }
                dr.Close();
            }
        }

        // ceplokin DAmt & CAmt ke list
        private void Process2(ref List<COAJournal> l3, ref List<JournalTemp> l4)
        {

            foreach (var x in l3)
            {
                foreach (var y in l4.Where(w => w.AcNo == x.AcNo))
                {
                    x.DAmtQ1 = y.DAmtQ1;
                    x.CAmtQ1 = y.CAmtQ1;
                    x.DAmtQ2 = y.DAmtQ2;
                    x.CAmtQ2 = y.CAmtQ2;
                    x.DAmtQ3 = y.DAmtQ3;
                    x.CAmtQ3 = y.CAmtQ3;
                    x.DAmtQ4 = y.DAmtQ4;
                    x.CAmtQ4 = y.CAmtQ4;
                }
            }
        }

        // Total Journal
        private void Process3(ref List<COACBP> l2, ref List<COAJournal> l3)
        {
            foreach (var x in l3)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtJNQ1 += (x.AcType == "C") ? (x.CAmtQ1 - x.DAmtQ1) : (x.DAmtQ1 - x.CAmtQ1);
                    y.AmtJNQ2 += (x.AcType == "C") ? (x.CAmtQ2 - x.DAmtQ2) : (x.DAmtQ2 - x.CAmtQ2);
                    y.AmtJNQ3 += (x.AcType == "C") ? (x.CAmtQ3 - x.DAmtQ3) : (x.DAmtQ3 - x.CAmtQ3);
                    y.AmtJNQ4 += (x.AcType == "C") ? (x.CAmtQ4 - x.DAmtQ4) : (x.DAmtQ4 - x.CAmtQ4);
                }
            }

        }

        // proses masukin amount journal ke parent dari si amount bontotnya
        private void Process4(ref List<COACBP> l2)
        {
            int MaxLvlCOA = Int32.Parse(Sm.GetValue("SELECT MAX(LEVEL) FROM TblCOA"));

            for (int i = MaxLvlCOA; i > 0; i--)
            {
                foreach (var x in l2.Where(w => w.Level == i).OrderBy(o => o.Parent))
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.Parent))
                    {
                        y.AmtJNQ1 += x.AmtJNQ1;
                        y.AmtJNQ2 += x.AmtJNQ2;
                        y.AmtJNQ3 += x.AmtJNQ3;
                        y.AmtJNQ4 += x.AmtJNQ4;
                    }
                }
            }
        }

        // ceplokin data ke grid
        private void Process5(ref List<COACBP> l2)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l2)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.AmtCBPQ1, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.AmtJNQ1, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.AmtCBPQ2, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.AmtJNQ2, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = Sm.FormatNum(x.AmtCBPQ3, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.AmtJNQ3, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.FormatNum(x.AmtCBPQ4, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.FormatNum(x.AmtJNQ4, 0);

                Row += 1;
            }

            Grd1.EndUpdate();
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Event

        private void LueProfitCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenter, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
            SetLueCCCode(ref LueCCCode, Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProfitCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Profit Center");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion

        #region Class

        private class CBP
        {
            public string Yr { get; set; }
            public string AcNo { get; set; }
            public decimal AmtQ1 { get; set; }
            public decimal AmtQ2 { get; set; }
            public decimal AmtQ3 { get; set; }
            public decimal AmtQ4 { get; set; }

        }

        private class COACBP
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public decimal Level { get; set; }
            public decimal AmtCBPQ1 { get; set; }
            public decimal AmtJNQ1 { get; set; }
            public decimal AmtCBPQ2 { get; set; }
            public decimal AmtJNQ2 { get; set; }
            public decimal AmtCBPQ3 { get; set; }
            public decimal AmtJNQ3 { get; set; }
            public decimal AmtCBPQ4 { get; set; }
            public decimal AmtJNQ4 { get; set; }
        }

        private class COAJournal
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmtQ1 { get; set; }
            public decimal CAmtQ1 { get; set; }
            public decimal DAmtQ2 { get; set; }
            public decimal CAmtQ2 { get; set; }
            public decimal DAmtQ3 { get; set; }
            public decimal CAmtQ3 { get; set; }
            public decimal DAmtQ4 { get; set; }
            public decimal CAmtQ4 { get; set; }
        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public decimal DAmtQ1 { get; set; }
            public decimal CAmtQ1 { get; set; }
            public decimal DAmtQ2 { get; set; }
            public decimal CAmtQ2 { get; set; }
            public decimal DAmtQ3 { get; set; }
            public decimal CAmtQ3 { get; set; }
            public decimal DAmtQ4 { get; set; }
            public decimal CAmtQ4 { get; set; }
        }

        #endregion

    }
}
