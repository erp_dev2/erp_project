﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWSDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmWS mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmWSDlg(FrmWS FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sm.SetLue(LueDeptCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Department",
                        "Year",
                        "Month",

                        //6-8
                        "Start Date",
                        "End Date",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 80, 200, 100, 100,
                        
                        //6-8
                        100, 100, 250
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.DeptName, A.Period, A.StartDt, A.EndDt, A.Remark ");
            SQL.AppendLine("From TblATDHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc, A.DocNo Desc;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DocDt", 
                             "DeptName", 
                             "Period",
                             "StartDt",
                             "EndDt",

                             //6
                             "Remark"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Grd.Cells[Row, 4].Value = Sm.Left(dr.GetString(c[3]), 4);
                            Grd.Cells[Row, 5].Value = Sm.Right(dr.GetString(c[3]), 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtATDDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtPeriodYr.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtPeriodMth.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                Sm.SetDte(mFrmParent.DteStartDt, Sm.GetGrdDate(Grd1, Row, 6));
                Sm.SetDte(mFrmParent.DteEndDt, Sm.GetGrdDate(Grd1, Row, 7));
                mFrmParent.MeeRemark.EditValue = Sm.GetGrdStr(Grd1, Row, 8);

                mFrmParent.ClearGrd();
                mFrmParent.ShowEmployeeInfo(Sm.GetGrdStr(Grd1, Row, 1));

                this.Close();
            }
        }

        #endregion

        #region Grid Method


        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
