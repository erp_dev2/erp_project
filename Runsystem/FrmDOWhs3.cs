﻿#region Update
/*
    10/05/2017 [TKG] Cancel data belum merubah indikator di aplikasi transfer request.
    08/09/2018 [TKG] Bug saat do to other warehouse, apabila stok gudang sudah kosong masih bisa di-DO-kan.

*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool mIsShowForeignName = false;
        internal int mNumberOfInventoryUomCode = 1;
        private string mWhsCode = string.Empty, mWhsCode2 = string.Empty; 
        internal FrmDOWhs3Find FrmFind;

        #endregion

        #region Constructor

        public FrmDOWhs3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Other Warehouse (Transfer Request With Batch#)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Requested DNo",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Foreign Name",

                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin",
                        "Stock",
                        "Requested",
                        "DO",
                        "Balance",

                        //16-20
                        "UoM",
                        "Stock",
                        "Requested",
                        "DO",
                        "Balance",

                        //21-25
                        "UoM",
                        "Stock",
                        "Requested",
                        "DO",
                        "Balance",

                        //26-29
                        "UoM",
                        "Requested's Remark",
                        "Remark",
                        "Received#"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        0, 100, 20, 250, 100,

                        //6-10
                        0, 0, 200, 200, 60, 

                        //11-15
                        60, 100, 100, 100, 100, 
                        
                        //16-20
                        60, 100, 100, 100, 100, 
                        
                        //21-25
                        60, 100, 100, 100, 100,

                        //26-29
                        60, 300, 300, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] 
            { 
                0, 1, 6,
                2, 3, 5, 9,
                17, 18, 19, 20, 21, 
                22, 23, 24, 25, 26 
            }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] 
            { 
                0, 
                1, 2, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 15, 16, 17, 18, 20,
                21, 22, 23, 25, 26, 27, 29
            });
            if (mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26  }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeCancelReason, ChkCancelInd, MeeRemark }, true);
                    BtnTransferRequestWhs2DocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 14, 19, 24, 28 });
                    Sm.GrdColInvisible(Grd1, new int[] { 12, 17, 22 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    BtnTransferRequestWhs2DocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 14, 19, 24, 28 });
                    Sm.GrdColInvisible(Grd1, new int[] { 12 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 17 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 17, 22 }, true);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mWhsCode = string.Empty;
            mWhsCode2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, TxtTransferRequestWhs2DocNo, TxtWhsCode, TxtWhsCode2, 
                MeeTransferRequestWhs2Remark, MeeCancelReason, MeeRemark 
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25 });
        }

        private void ClearData2()
        {
            mWhsCode = string.Empty;
            mWhsCode2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtWhsCode, TxtWhsCode2, MeeTransferRequestWhs2Remark });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOWhs3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 28 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 19, 24 }, e);

            if (e.ColIndex == 14)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 14, 19, 24, 16, 21, 26);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 2, 14, 24, 19, 16, 26, 21);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 19, 14, 24, 21, 16, 26);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 2, 19, 24, 14, 21, 26, 16);
            }

            if (e.ColIndex == 24)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 2, 24, 14, 19, 26, 16, 21);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 2, 24, 19, 14, 26, 21, 16);
            }

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 14);

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 26)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 24, Grd1, e.RowIndex, 14);

            if (e.ColIndex == 19 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 21), Sm.GetGrdStr(Grd1, e.RowIndex, 26)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 24, Grd1, e.RowIndex, 19);

            if (e.ColIndex==14)
            {
                ComputeBalanceQty(e.RowIndex, 13, 14, 15);
                ComputeBalanceQty(e.RowIndex, 18, 19, 20);
                ComputeBalanceQty(e.RowIndex, 23, 24, 25);
            }

            if (e.ColIndex == 19)
            {
                ComputeBalanceQty(e.RowIndex, 18, 19, 20);
                ComputeBalanceQty(e.RowIndex, 23, 24, 25);
            }

            if (e.ColIndex==24) ComputeBalanceQty(e.RowIndex, 23, 24, 25);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOWhs3", "TblDOWhsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOWhs3Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveDOWhs3Dtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdValueNotValid() ||
                IsTransferRequestWhsAlreadyCancelled() ||
                IsTransferRequestWhsAlreadyDO();
        }

        private bool IsTransferRequestWhsAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblTransferRequestWhs2Hdr " +
                    "Where CancelInd='Y' And DocNo=@Param;",
                    TxtTransferRequestWhs2DocNo.Text,
                    "This requested document already cancelled."
                );
        }

        private bool IsTransferRequestWhsAlreadyDO()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblTransferRequestWhs2Hdr " +
                    "Where DOWhs3DocNo Is Not Null And DocNo=@Param;",
                    TxtTransferRequestWhs2DocNo.Text,
                    "This requested document already processed to DO."
                );
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;

                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    (mIsShowForeignName ? ("Foreign Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine) : string.Empty) +
                    Environment.NewLine;


                if (Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 13))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than requested quantity.");
                    return true;
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) > Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 19) > Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than requested quantity.");
                        return true;
                    }
                }

                if (Grd1.Cols[24].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 24) > Sm.GetGrdDec(Grd1, Row, 22))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 24) > Sm.GetGrdDec(Grd1, Row, 23))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than requested quantity.");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveDOWhs3Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsHdr(DocNo, DocDt, CancelInd, Status, TransferRequestWhs2DocNo, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'A', @TransferRequestWhs2DocNo, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblTransferRequestWhs2Hdr Set DOWhs3DocNo=@DocNo Where DocNo=@TransferRequestWhs2DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhs2DocNo", TxtTransferRequestWhs2DocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@WhsCode2", mWhsCode2);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOWhs3Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsDtl");
            SQL.AppendLine("(DocNo, DNo, CancelInd, TransferRequestWhs2DNo, ");
            SQL.AppendLine("ItCode, PropCode, BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @TransferRequestWhs2DNo, ");
            SQL.AppendLine("@ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhs2DNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditDOWhs3Hdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyReceived();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblDOWhsHdr " +
                    "Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled."
                );
        }

        private bool IsDataAlreadyReceived()
        {
            return
                Sm.IsDataExist(
                   "Select DocNo From TblRecvWhs4Dtl " +
                   "Where CancelInd='N' And DOWhsDocNo=@Param Limit 1;",
                    TxtDocNo.Text,
                    "This document already processed to DO."
                );
        }

        private MySqlCommand EditDOWhs3Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhsHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblTransferRequestWhs2Hdr Set DOWhs3DocNo=Null Where DocNo=@TransferRequestWhs2DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TransferRequestWhs2DocNo", TxtTransferRequestWhs2DocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowDOWhs3Hdr(DocNo);
                ShowDOWhs3Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOWhs3Hdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TransferRequestWhs2DocNo, ");
            SQL.AppendLine("A.WhsCode, C.WhsName As WhsName, A.WhsCode2, D.WhsName As WhsName2, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, B.Remark As TransferRequestWhs2Remark, A.Remark ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestWhs2Hdr B On A.TransferRequestWhs2DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode2=D.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "TransferRequestWhs2DocNo", "WhsCode", "WhsName", "WhsCode2", 
                        
                        //6-10
                        "WhsName2", "CancelReason", "CancelInd", "TransferRequestWhs2Remark", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtTransferRequestWhs2DocNo.EditValue = Sm.DrStr(dr, c[2]);
                        mWhsCode = Sm.DrStr(dr, c[3]);
                        TxtWhsCode.EditValue = Sm.DrStr(dr, c[4]);
                        mWhsCode2 = Sm.DrStr(dr, c[5]);
                        TxtWhsCode2.EditValue = Sm.DrStr(dr, c[6]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[7]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[8]), "Y");
                        MeeTransferRequestWhs2Remark.EditValue = Sm.DrStr(dr, c[9]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ShowDOWhs3Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.TransferRequestWhsDNo, ");
            SQL.AppendLine("B.ItCode, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("E.Qty As TransferRequestWhsQty, B.Qty, C.InventoryUomCode, ");
            SQL.AppendLine("E.Qty As TransferRequestWhsQty2, B.Qty2, C.InventoryUomCode2, ");
            SQL.AppendLine("E.Qty As TransferRequestWhsQty3, B.Qty3, C.InventoryUomCode3, ");
            SQL.AppendLine("E.Remark As TransferRequestWhsRemark, B.Remark, F.DocNo As RecvWhs4DocNo ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblTransferRequestWhs2Dtl E ");
            SQL.AppendLine("    On A.TransferRequestWhs2DocNo=E.DocNo ");
            SQL.AppendLine("    And B.TransferRequestWhs2DNo=E.DNo ");
            SQL.AppendLine("Left Join TblRecvWhs4Dtl F ");
            SQL.AppendLine("    On B.DocNo=F.DOWhsDocNo ");
            SQL.AppendLine("    And B.DNo=F.DOWhsDNo ");
            SQL.AppendLine("    And F.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",  

                    //1-5
                    "TransferRequestWhsDNo", "ItCode", "ItName", "ForeignName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "TransferRequestWhsQty", "Qty", "InventoryUomCode", "TransferRequestWhsQty2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "TransferRequestWhsQty3", "Qty3", "InventoryUomCode3", "TransferRequestWhsRemark", 
                    
                    //21-22
                    "Remark", "RecvWhs4DocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    ComputeBalanceQty(Row, 13, 14, 15);
                    ComputeBalanceQty(Row, 18, 19, 20);
                    ComputeBalanceQty(Row, 23, 24, 25);
                    Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 12, 17, 22 });
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length > 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        private void ReComputeStock()
        {
            if (Grd1.Rows.Count <= 1) return;

            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

                if (Grd1.Rows.Count != 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 9);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(A.Source=@Source0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ") ";

                SQL.AppendLine("Select A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("IfNull(A.Qty, 0)-IfNull(B.Qty, 0) As Qty, ");
                SQL.AppendLine("IfNull(A.Qty2, 0)-IfNull(B.Qty2, 0) As Qty2, ");
                SQL.AppendLine("IfNull(A.Qty3, 0)-IfNull(B.Qty3, 0) As Qty3 ");
                SQL.AppendLine("From TblStockSummary A ");
                SQL.AppendLine("Left join ( ");
                SQL.AppendLine("    Select T2.Lot, T2.Bin, T2.Source, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2, Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOWhsHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T2.ProcessInd='O' ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine(Filter.Replace("A.", "T2."));
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
                SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
                SQL.AppendLine("    Group By T2.Lot, T2.Bin, T2.Source");
                SQL.AppendLine(") B On A.Lot=B.Lot And A.Bin=B.Bin And A.Source=B.Source ");
                SQL.AppendLine("Where A.WhsCode=@WhsCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 

                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 11), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        public void ShowTransferRequestWhs2Info(string DocNo)
        {
            try
            {
                ClearData2();
                ShowTransferRequestWhs2Hdr(DocNo);
                ShowTransferRequestWhs2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowTransferRequestWhs2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.WhsCode, B.WhsName, A.WhsCode2, C.WhsName As WhsName2, A.Remark ");
            SQL.AppendLine("From TblTransferRequestWhs2Hdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2=C.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
 
                        //1-5
                        "WhsCode", "WhsName", "WhsCode2", "WhsName2", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtTransferRequestWhs2DocNo.EditValue = Sm.DrStr(dr, c[0]);
                        mWhsCode = Sm.DrStr(dr, c[1]);
                        TxtWhsCode.EditValue = Sm.DrStr(dr, c[2]);
                        mWhsCode2 = Sm.DrStr(dr, c[3]);
                        TxtWhsCode2.EditValue = Sm.DrStr(dr, c[4]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowTransferRequestWhs2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ForeignName, ");
            SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("IfNull(D.Qty, 0)-IfNull(E.Qty, 0) As Stock, A.Qty, B.InventoryUomCode, ");
            SQL.AppendLine("IfNull(D.Qty2, 0)-IfNull(E.Qty2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("IfNull(D.Qty3, 0)-IfNull(E.Qty3, 0) As Stock3, A.Qty3, B.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblTransferRequestWhs2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary D On A.Source=D.Source And A.Lot=D.Lot And A.Bin=D.Bin And D.WhsCode=@WhsCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.Lot, T2.Bin, T2.Source, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2, Sum(T2.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblDOWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='O' And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T2.Lot, T2.Bin, T2.Source");
            SQL.AppendLine(") E On A.Lot=E.Lot And A.Bin=E.Bin And A.Source=E.Source");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "ForeignName", "PropCode", "PropName",   
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Bin", "Stock",   
                    
                    //11-15
                    "Qty", "InventoryUomCode", "Stock2", "Qty2", "InventoryUomCode2", 
                    
                    //16-19
                    "Stock3", "Qty3", "InventoryUomCode3", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    ComputeDOQty(Row, 12, 13, 14, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    ComputeDOQty(Row, 17, 18, 19, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                    ComputeDOQty(Row, 22, 23, 24, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 19);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ComputeDOQty(int Row, int ColStock, int ColRequest, int ColDO, int ColBalance)
        {
            decimal
                Stock = Sm.GetGrdDec(Grd1, Row, ColStock),
                Request = Sm.GetGrdDec(Grd1, Row, ColRequest);
            decimal DO = (Stock >= Request)?Request:Stock;
            Grd1.Cells[Row, ColDO].Value = DO;
            Grd1.Cells[Row, ColBalance].Value = Request-DO;
        }

        private void ComputeBalanceQty(int Row, int ColRequest, int ColDO, int ColBalance)
        {
            decimal
                Request = Sm.GetGrdDec(Grd1, Row, ColRequest),
                DO = Sm.GetGrdDec(Grd1, Row, ColDO);
            Grd1.Cells[Row, ColBalance].Value = Request - DO;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnTransferRequestWhs2DocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOWhs3Dlg(this));
        }

        private void BtnTransferRequestWhs2DocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtTransferRequestWhs2DocNo, "Requested#", false))
            {
                var f1 = new FrmTransferRequestWhs2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtTransferRequestWhs2DocNo.Text;
                f1.ShowDialog();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion
    }
}
