﻿#region Update
/*
    06/01/2020 [WED/IMS] ambil dari master item
    16/07/2020 [WED/IMS] item bisa dipilih berkali kali
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementation2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProjectImplementation2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementation2Dlg2(FrmProjectImplementation2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select T1.DocType, T1.BOQDocNo, T1.BOMDocNo, T1.BOMDNo, T1.ItCode, T2.ItName, T2.ItGrpCode, T3.ItGrpName ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select '2' As DocType, A.DocNo As BOQDocNo, A.BOMDocNo, A.BOMDNo, B.ItCode ");
            //SQL.AppendLine("    From TblBOQDtl2 A ");
            //SQL.AppendLine("    Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            //SQL.AppendLine("    Inner Join TblSOContractRevisionHdr D On D.DocNo = @SOCDocNo ");
            //SQL.AppendLine("    Inner Join TblSOContractHdr E On D.SOCDocNo = E.DocNo And A.DocNo = E.BOQDocNo ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("    Select '3' As DocType, A.DocNo As BOQDocNo, A.BOMDocNo, A.BOMDNo, B.ItCode ");
            //SQL.AppendLine("    From TblBOQDtl3 A ");
            //SQL.AppendLine("    Inner Join TblBOMDtl2 B On A.BOMDocNo = B.DocNo And A.BOMDNo = B.DNo ");
            //SQL.AppendLine("    Inner Join TblSOContractRevisionHdr D On D.DocNo = @SOCDocNo ");
            //SQL.AppendLine("    Inner Join TblSOContractHdr E On D.SOCDocNo = E.DocNo And A.DocNo = E.BOQDocNo ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("    Select '1' As DocType, Null As BOQDocNo, A.DocNo BOMDocNo, B.DNo BOMDNo, B.DocCode ItCode ");
            //SQL.AppendLine("    From TblBOMHdr A ");
            //SQL.AppendLine("    Inner Join TblBOMDtl B On A.DocNo = B.DocNo And A.ActiveInd = 'Y' ");
            //SQL.AppendLine(") T1 ");
            //SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            #endregion

            SQL.AppendLine("Select T1.ItCode, T1.ItName, T1.ItGrpCode, T2.ItGrpName ");
            SQL.AppendLine("From TblItem T1 ");
            SQL.AppendLine("Left Join TblItemGroup T2 On T1.ItGrpCode = T2.ItGrpCode ");
            SQL.AppendLine("Where Locate(Concat('##', T1.ItCode, '##'), @SelectedItCode) < 1 ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "DocType",
                    "BOQ#",
                    "BOM#",
                    "BOM Detail#",

                    //6-10
                    "",
                    "Resource Code",
                    "Resource Name",
                    "Group",
                    "ItGrpCode"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 0, 180, 180, 100, 
                    
                    //6-10
                    20, 120, 280, 200, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedItCode", mFrmParent.GetSelectedItCode());
                Sm.FilterStr(ref Filter, ref cm, TxtResourceItCode.Text, new string[] { "T1.ItCode", "T1.ItName", "T1.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T1.ItName; ",
                    new string[]
                    {
                        //0
                        "ItCode", 

                        //1-3
                        "ItName", "ItGrpName", "ItGrpCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 9); 
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 10);
                        //Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 15, Grd1, Row2, 2);
                        //Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 16, Grd1, Row2, 3);
                        //Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 17, Grd1, Row2, 4);
                        //Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 18, Grd1, Row2, 5);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 resource.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 7);
            //for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
            //    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 1), ItCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtResourceItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkResourceItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Resource");
        }

        #endregion

        #endregion

    }
}
