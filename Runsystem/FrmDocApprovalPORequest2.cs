﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalPORequest2 : RunSystem.FrmBase9
    {
        #region Field

        internal string mDocNo = string.Empty, mDNo = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalPORequest2(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                TxtDocNo.EditValue = mDocNo;
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Requested"+Environment.NewLine+"Date",
                        "Requested#", 
                        "Item",
                        "Requested"+Environment.NewLine+"Qty",
                        "PO Request"+Environment.NewLine+"Date",

                        //6-8
                        "PO Request#",
                        "Vendor", 
                        "Price"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 8 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select ");
                SQL.AppendLine("C.DocDt As MaterialRequestDocDt, C.LocalDocNo As MaterialRequestLocalDocNo, G.ItName, D.Qty, ");
                SQL.AppendLine("A.DocDt As PORequestDocDt, A.LocalDocNo As PORequestLocalDocNo, H.VdName, F.UPrice ");
                SQL.AppendLine("From TblPORequestHdr A ");
                SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo=D.DocNo And B.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr E On B.QtDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblQtDtl F On B.QtDocNo=F.DocNo And B.QtDNo=F.DNo ");
                SQL.AppendLine("Inner Join TblItem G On D.ItCode=G.ItCode ");
                SQL.AppendLine("Inner Join TblVendor H On E.VdCode=H.VdCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Order By G.ItName, H.VdName;");

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mDNo);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "MaterialRequestDocDt",
                            
                            //1-5
                            "MaterialRequestLocalDocNo", 
                            "ItName", 
                            "Qty", 
                            "PORequestDocDt", 
                            "PORequestLocalDocNo", 

                            //6-7
                            "VdName", 
                            "UPrice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
    }
}

