﻿#region Update
/*
    27/12/2022 [SET/BBT] Menu baru
    11/01/2023 [SET/BBT] penyesuaian cancel yang berpengaruh ke Property Inventory
    13/01/2023 [MAU/BBT] BUG : update coa ketika transaksi property inventory transfer
    13/01/2023 [IBL/BBT] BUG : Belum ada validasi saat certificate information nya mandatory
    13/01/2023 [IBL/BBT] BUG : Saat save, field certificate type hilang
    15/01/2023 [SET/BBT] BUg : format save belum sesuai dengan format table db
    17/01/2023 [MAU/BBT] BUG : clear Grd2 dan clear data TxtStatus
    02/02/2023 [WED/BBT] rubah label pada tab Certificate Information
    02/02/2023 [WED/BBT] rubah panjang karakter Cert. Expired Date menjadi 1000, dan free text (yang sebelumnya tanggal)
    16/02/2023 [MAU/BBT] Bug: Informasi penyesuaian show data nilai property inventory value 
    12/04/2023 [VIN/BBT] tambah karakter input
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Collections;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryTransfer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmPropertyInventoryTransferFind FrmFind;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsFilterByCC = false;
        internal bool mIsAssetTransferUseLocation = false;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmPropertyInventoryTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                ExecQuery();
                if (this.Text.Length == 0) this.Text = "Property Inventory Transfer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCCCode(ref LueCCCodeFrom, mIsFilterByCC ? "Y" : "N"); 
                Sl.SetLueCCCode(ref LueCCCodeTo);
                Sl.SetLueSiteCode(ref LueSiteCodeFrom);
                Sl.SetLueSiteCode(ref LueSiteCodeTo);
                SetLuePropertyCtCode(ref LuePropertyCtFrom, string.Empty);
                SetLuePropertyCtCode(ref LuePropertyCtTo, string.Empty);
                Tc.SelectedTab = TpCertificationInformation;
                Sl.SetLueOption(ref LueCertificateType, "CertificateType");
                Tc.SelectedTab = TpGeneral;

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Property Code",
                        "Property Name",
                        "",
                        "Inventory" + Environment.NewLine + "Quantity",
                        "UoM",
                        //6-7
                        "Unit Price",
                        "Property Inventory Value",
                        "Date of Registration"
                    },
                     new int[] 
                    {
                        20, 
                        150, 250, 20, 100, 100, 
                        150, 200, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8 });
            //Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 4, 6, 7 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            #endregion

            #region Grd2
            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 100, 80, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LuePropertyCtFrom, LuePropertyCtTo, MeeRemark, LueSiteCodeFrom, LueSiteCodeTo, LueCCCodeFrom, LueCCCodeTo, MeeCancelReason,
                        LueCertificateType, TxtCertificateNo, DteCertIssuedDt, DteCertExpDt, TxtCertIssuedLocation, TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, DteBasisDocDt, TxtSurveyDocNo, DteSurveyDocDt, TxtAreaBased,
                        TxtFile1, TxtFile2, MeeCertExpDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnFile1.Enabled = false;
                    if (TxtFile1.Text.Length > 0)
                        BtnDownload1.Enabled = true;
                    BtnFile2.Enabled = false;
                    if (TxtFile2.Text.Length > 0)
                        BtnDownload2.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark //LuePropertyCtTo, LueCCCodeTo, LueSiteCodeTo 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    BtnFile1.Enabled = true;
                    BtnFile2.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        MeeCancelReason 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LuePropertyCtFrom, LuePropertyCtTo, MeeRemark, LueSiteCodeTo, LueSiteCodeFrom, LueCCCodeFrom, LueCCCodeTo, MeeCancelReason,
                LueCertificateType, TxtCertificateNo, DteCertIssuedDt, DteCertExpDt, TxtCertIssuedLocation, TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, DteBasisDocDt, TxtSurveyDocNo, DteSurveyDocDt, TxtAreaBased,
                TxtFile1, TxtFile2, TxtStatus, MeeCertExpDt
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPropertyInventoryTransferFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
            SetTabCertificate();
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ") && !IsGrdExceedMaxRecords())
                            Sm.FormShowDialog(new FrmPropertyInventoryTransferDlg(this, e.RowIndex));
                    }
                }
              
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "1")
                {
                    var f = new FrmAsset2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "2")
                {
                    var f = new FrmAsset3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete && Sm.GetGrdStr(Grd1, 0, 2).Length == 0)
                {
                    ClearData();
                    Sm.SetDteCurrentDate(DteDocDt);
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) 
            {
                if(!IsGrdExceedMaxRecords())
                    Sm.FormShowDialog(new FrmPropertyInventoryTransferDlg(this, e.RowIndex));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPropertyInventory(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPropertyCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                //if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "2")
                //{
                //    var f = new FrmAsset3(mMenuCode);
                //    f.Tag = mMenuCode;
                //    f.WindowState = FormWindowState.Normal;
                //    f.StartPosition = FormStartPosition.CenterScreen;
                //    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                //    f.ShowDialog();
                //}
            }
        }

       
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PropertyInventoryTransfer", "TblPropertyInventoryTransferHdr");
            var IsNeedApproval = IsDocNeedApproval();

            var cml = new List<MySqlCommand>();

            cml.Add(SavePropertyInventoryTransferHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SavePropertyInventoryTransferDtl(DocNo, Row));
            if(IsNeedApproval)
                cml.Add(SaveDocApproval(DocNo));
            if (!IsNeedApproval)
            {
                cml.Add(SaveJournal(DocNo));
                cml.Add(SavePropertyInventory(DocNo));
            }

            Sm.ExecCommands(cml);

            if (TxtFile1.Text.Length > 0 && TxtFile1.Text != "openFileDialog1")
                UploadFile1(DocNo);
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") || 
                IsRegDtMoreThanDocDt() ||
                Sm.IsLueEmpty(LuePropertyCtTo, "Property Category To") || 
                Sm.IsLueEmpty(LueSiteCodeTo, "Site To") || 
                Sm.IsLueEmpty(LueCCCodeTo, "Cost Center To")||
                IsSamePropertyCtSiteCC() ||
                Sm.IsTxtEmpty(MeeRemark, "Remark", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCertificateDataNotValid();
        }

        private bool IsCertificateDataNotValid()
        {
            string PropertyCtCode = Sm.GetLue(LuePropertyCtTo);
            string MandatoryCertificaeInd = Sm.GetValue("Select MandatoryCertificateInd From Tblpropertyinventorycategory Where PropertyCategoryCode = @Param ", PropertyCtCode);
            if (Sm.GetLue(LuePropertyCtTo) != Sm.GetLue(LueCCCodeFrom) && MandatoryCertificaeInd == "Y")
            {
                return
                    Sm.IsLueEmpty(LueCertificateType, "Certificate Type") ||
                    Sm.IsTxtEmpty(TxtCertificateNo, "Certificate No.", false) ||
                    Sm.IsDteEmpty(DteCertIssuedDt, "Cert. Issued Date") ||
                    Sm.IsTxtEmpty(TxtCertIssuedLocation, "Cert. Issued Loccation", false) ||
                    Sm.IsTxtEmpty(TxtNIB, "Nomor Indeks Bidang (NIB)", false) ||
                    Sm.IsMeeEmpty(MeeRegistrationBasis, "Registration Basis") ||
                    Sm.IsDteEmpty(DteBasisDocDt, "Basis Document Date") ||
                    Sm.IsTxtEmpty(TxtSurveyDocNo, "Survey Document No.", false) ||
                    Sm.IsDteEmpty(DteSurveyDocDt, "Survey Document Date") ||
                    Sm.IsTxtEmpty(TxtAreaBased, "Area Based on Suervey Doc.", true);
            }

            return false;
        }

        private bool IsSamePropertyCtSiteCC()
        {
            if(Sm.GetLue(LuePropertyCtTo) == Sm.GetLue(LuePropertyCtFrom) && 
                Sm.GetLue(LueSiteCodeFrom) == Sm.GetLue(LueSiteCodeTo) && 
                Sm.GetLue(LueCCCodeFrom) == Sm.GetLue(LueCCCodeTo))
            {
                Sm.StdMsg(mMsgType.Warning, "Property Category To can't be the same Property Category From" + Environment.NewLine +
                    "Site To can't be the same Site From" + Environment.NewLine +
                    "Cost Center To can't be the same Cost Center From");
                LuePropertyCtTo.Focus();
                return true;
            }
            else
                return false;
        }
        
        private bool IsRegDtMoreThanDocDt()
        {
            if (Sm.GetDte(DteDocDt).CompareTo(Sm.GetGrdDate(Grd1, 0, 8)) < 1)
            {
                Sm.StdMsg(mMsgType.Warning, "The document date can't be more than the registration date of Property");
                LuePropertyCtTo.Focus();
                return true;
            }
            else
                return false;
        }
        
        private bool IsSameSite()
        {
            if(Sm.GetLue(LueSiteCodeFrom).Length > 0 && Sm.GetLue(LueSiteCodeTo).Length > 0 && Sm.GetLue(LueSiteCodeTo) == Sm.GetLue(LueSiteCodeFrom))
            {
                Sm.StdMsg(mMsgType.Warning, "Site To can't be the same Site From");
                LueSiteCodeTo.Focus();
                return true;
            }
            else
                return false;
        }
        
        private bool IsSameCostCenter()
        {
            if(Sm.GetLue(LueCCCodeFrom).Length > 0 && Sm.GetLue(LueCCCodeTo).Length > 0 && Sm.GetLue(LueCCCodeTo) == Sm.GetLue(LueCCCodeFrom))
            {
                Sm.StdMsg(mMsgType.Warning, "Cost Center To can't be the same Cost Center From");
                LueCCCodeTo.Focus();
                return true;
            }
            else
                return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Property.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 1).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only fill in one property data.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Property is empty.")) return true;

                Msg =
                    "Property Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Property Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;
            }
            return false;
        }

        private MySqlCommand SavePropertyInventoryTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPropertyInventoryTransferHdr(DocNo, Status, DocDt, CancelInd, PropertyCategoryCodeFrom, PropertyCategoryCodeTo, SiteCodeFrom, SiteCodeTo, CCCodeFrom, CCCodeTo, Remark, " +
                    "CertificateType, CertificateNo, CertificateIssuedDt, CertificateExpDt, CertificateIssuedLocation, NIB, RegistrationBasis, BasisDocDt, SurveyDocNo, SurveyDocDt, AreaBased, " +
                    "CreateBy, CreateDt) " +
                    "Values(@DocNo, 'O', @DocDt, 'N', @PropertyCategoryCodeFrom, @PropertyCategoryCodeTo, @SiteCodeFrom, @SiteCodeTo, @CCCodeFrom, @CCCodeTo, @Remark, " +
                    "@CertificateType, @CertificateNo, @CertificateIssuedDt, @CertificateExpDt, @CertificateIssuedLocation, @NIB, @RegistrationBasis, @BasisDocDt, @SurveyDocNo, @SurveyDocDt, @AreaBased, " +
                    "@CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PropertyCategoryCodeFrom", Sm.GetLue(LuePropertyCtFrom));
            Sm.CmParam<String>(ref cm, "@PropertyCategoryCodeTo", Sm.GetLue(LuePropertyCtTo));
            Sm.CmParam<String>(ref cm, "@SiteCodeFrom", Sm.GetLue(LueSiteCodeFrom));
            Sm.CmParam<String>(ref cm, "@SiteCodeTo", Sm.GetLue(LueSiteCodeTo));
            Sm.CmParam<String>(ref cm, "@CCCodeFrom", Sm.GetLue(LueCCCodeFrom));
            Sm.CmParam<String>(ref cm, "@CCCodeTo", Sm.GetLue(LueCCCodeTo));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CertificateType", Sm.GetLue(LueCertificateType));
            Sm.CmParam<String>(ref cm, "@CertificateNo", TxtCertificateNo.Text);
            Sm.CmParamDt(ref cm, "@CertificateIssuedDt", Sm.GetDte(DteCertIssuedDt));
            Sm.CmParam<String>(ref cm, "@CertificateExpDt", MeeCertExpDt.Text);
            Sm.CmParam<String>(ref cm, "@CertificateIssuedLocation", TxtCertIssuedLocation.Text);
            Sm.CmParam<String>(ref cm, "@NIB", TxtNIB.Text);
            Sm.CmParam<String>(ref cm, "@RegistrationBasis", MeeRegistrationBasis.Text);
            Sm.CmParamDt(ref cm, "@BasisDocDt", Sm.GetDte(DteBasisDocDt));
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
            Sm.CmParamDt(ref cm, "@SurveyDocDt", Sm.GetDte(DteSurveyDocDt));
            Sm.CmParam<Decimal>(ref cm, "@AreaBased", Decimal.Parse(TxtAreaBased.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePropertyInventoryTransferDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPropertyInventoryTransferDtl(DocNo, DNo, PropertyCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @PropertyCode, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PropertyCode", Sm.GetGrdStr(Grd1, Row,1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            // string testcAssetCode = Sm.GetGrdStr(Grd1, Row, 2);
            return cm;
        }

        internal MySqlCommand SavePropertyInventory(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Ignore Into TblPropertyInventoryMovement(DocNo, DNo, DocDt, PropertyCode, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, B.DNo, A.DocDt, B.PropertyCode, A.CCCodeTo, A.Remark, A.CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblPropertyInventorySummary(PropertyCode, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.PropertyCode, A.CCCodeTo, A.CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("    On Duplicate Key Update CCCode = A.CCCodeTo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblPropertyInventoryHdr A ");
            SQL.AppendLine("Left Join TblPropertyInventoryTransferDtl B On A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Left Join TblPropertyInventoryTransferHdr C On B.DocNo = C.DocNo ");
            
            SQL.AppendLine("Left Join TblPropertyInventoryCategory D On C.PropertyCategoryCodeTo = D.PropertyCategoryCode ");
            SQL.AppendLine("Left Join TblCoa E On D.AcNo1 = E.AcNo ");
            
            SQL.AppendLine("Set A.CCCode = C.CCCodeTo, A.PropertyCategoryCode=C.PropertycategoryCodeTo, A.SiteCode=C.SiteCodeTo, A.AcNo = E.AcNo ");
            SQL.AppendLine("Where B.DocNo = @DocNo And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            //Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Inner JOIN tblpropertyinventorytransferhdr U ON U.DocNo = @DocNo ");
            SQL.AppendLine("Inner JOIN tblcostcenter V ON U.CCCodeFrom = V.CCCode ");
            SQL.AppendLine("Where T.DocType='PropertyInventoryTransfer' ");
            SQL.AppendLine("And T.DeptCode = V.DeptCode ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update CreateBy=@UserCode, CreateDt=CurrentDateTime() ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType='PropertyInventoryTransfer' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        internal MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @row:= 0; ");
            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    `Status`='A', JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Property Inventory Transfer : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, A.CCCodeFrom, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            //SQL.AppendLine("Inner Join tblpropertyinventoryhdr B on A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From ( ");
            SQL.AppendLine("		Select C.AcNo1 AcNo, D.RemStockValue As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryTransferDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCategory C ON A.PropertyCategoryCodeTo = C.PropertyCategoryCode ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryHdr D ON B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select C.AcNo1 AcNo, 0.00 As DAmt, D.RemStockValue As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryTransferDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCategory C ON A.PropertyCategoryCodeFrom = C.PropertyCategoryCode ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryHdr D ON B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("    )Tbl Where AcNo Is Not Null -- Group By AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.ServerCurrentDate(), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        private void SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);

            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
           Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            var cm = new List<MySqlCommand>();
            var IsNeedApproval = IsDocNeedApproval();
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditPropertyInventoryTransferCancel();
            if (!IsNeedApproval)
                SaveJournal2();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Property Cancelled", false) ||
                //IsDataAlreadyReceived() ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblPropertyInventoryTransferHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyReceived()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblPropertyInventoryTransfer Where DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already Received.");
                return true;
            }
            return false;
        }

        private void EditPropertyInventoryTransferCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblPropertyInventoryTransferHdr Set CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;" +
                    "Update TblPropertyInventoryHdr A " +
                    "Inner Join TblPropertyInventoryTransferDtl B On A.PropertyCode = B.PropertyCode And B.DocNo = @DocNo " +
                    "Inner Join TblPropertyInventoryTransferHdr C On B.DocNo = C.DocNo " +
                    "Set A.PropertyCategoryCode = C.PropertyCategoryCodeFrom, A.SiteCode=C.SiteCodeFrom, A.CCCode=C.CCCodeFrom " +
                    "Where C.DocNo = @DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowPropertyInventoryTransferHdr(DocNo);
                ShowPropertyInventoryTransferDtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPropertyInventoryTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, case when Status = 'O' then 'Outstanding' when Status = 'A' then 'Approved' Else 'Cancel' End As Status, DocDt, CancelInd, CancelReason, PropertyCategoryCodeFrom, PropertyCategoryCodeTo, SiteCodeFrom, SiteCodeTo, CCCodeFrom, CCCodeTo, Remark, FileName1, FileName2, " +
                    "CertificateType, CertificateNo, CertificateIssuedDt, CertificateExpDt, CertificateIssuedLocation, NIB, RegistrationBasis, BasisDocDt, SurveyDocNo, SurveyDocDt, AreaBased " +
                    "From TblPropertyInventoryTransferHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "Status", "DocDt", "CancelInd", "CancelReason", "PropertyCategoryCodeFrom", 
                        //6-10
                        "PropertyCategoryCodeTo", "SiteCodeFrom", "SiteCodeTo", "CCCodeFrom", "CCCodeTo", 
                        //11-15
                        "Remark", "FileName1", "FileName2", "CertificateType", "CertificateNo", 
                        //16-20
                        "CertificateIssuedDt", "CertificateExpDt", "CertificateIssuedLocation", "NIB", "RegistrationBasis", 
                        //21-23
                        "BasisDocDt", "SurveyDocNo", "SurveyDocDt", "AreaBased"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        Sm.SetLue(LuePropertyCtFrom, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LuePropertyCtTo, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueSiteCodeFrom, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueSiteCodeTo, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueCCCodeFrom, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueCCCodeTo, Sm.DrStr(dr, c[10]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        TxtFile1.EditValue = Sm.DrStr(dr, c[12]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetLue(LueCertificateType, Sm.DrStr(dr, c[14]));
                        TxtCertificateNo.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteCertIssuedDt, Sm.DrStr(dr, c[16]));
                        //Sm.SetDte(DteCertExpDt, Sm.DrStr(dr, c[17]));
                        MeeCertExpDt.EditValue = Sm.DrStr(dr, c[17]);
                        TxtCertIssuedLocation.EditValue = Sm.DrStr(dr, c[18]);
                        TxtNIB.EditValue = Sm.DrStr(dr, c[19]);
                        MeeRegistrationBasis.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteBasisDocDt, Sm.DrStr(dr, c[21]));
                        TxtSurveyDocNo.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetDte(DteSurveyDocDt, Sm.DrStr(dr, c[23]));
                        TxtAreaBased.EditValue = Sm.DrDec(dr, c[24]);
                    }, true
                );
            Sm.FormatNumTxt(TxtAreaBased, 0);
        }

        private void ShowPropertyInventoryTransferDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, B.PropertyName, B.InventoryQty, B.UoMCode, B.UPrice, B.PropertyInventoryValue ");
            SQL.AppendLine("From TblPropertyInventoryTransferDtl A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryHdr B On A.PropertyCode=B.PropertyCode ");
            //SQL.AppendLine("Inner Join TblAsset C On B.AssetCode=C.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "PropertyCode", 

                    //1-5
                    "PropertyName", "InventoryQty", "UoMCode", "UPrice", "PropertyInventoryValue" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, IfNull(B.UserName, A.UserCode) As UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='PropertyInventoryTransfer' ");
            SQL.AppendLine("And IfNull(A.Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt)  ");
            //SQL.AppendLine("Values('PropertyInventoryCertificateInformationLabelFormat', 'Format label pada tab Certificate Information master Property Inventory. 1 : Bhs Inggris, 2 : Bhs Indonesia', '1', 'BBT', 'WEDHA', CurrentDateTime()); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '01', 'Jenis Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '02', 'No. Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '03', 'Penerbitan Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '04', 'Tanggal Berakhirnya Hak', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '05', 'Letak Tanah', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '06', 'Nomor Identifikasi Bidang (NIB)', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '07', 'Dasar Pendaftaran', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '08', 'Tanggal Dasar Pendaftaran', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '09', 'No. Surat Ukur', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '10', 'Tanggal Surat Ukur', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '11', 'Luas Berdasarkan Surat Ukur (m2)', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tblpropertyinventorytransferhdr` ");
            SQL.AppendLine("    CHANGE COLUMN `CertificateExpDt` `CertificateExpDt` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'; ");

            SQL.AppendLine("ALTER TABLE `tblpropertyinventorytransferhdr` ");
            SQL.AppendLine("    CHANGE COLUMN `RegistrationBasis` `RegistrationBasis` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            var cml = new List<MySqlCommand>();
            cml.Add(cm);

            Sm.ExecCommands(cml);

            cml.Clear();
        }

        private void SetTabCertificate()
        {
            string PropertyCtCode = Sm.GetLue(LuePropertyCtTo);

            string MandatoryCertificaeInd = Sm.GetValue("Select MandatoryCertificateInd From Tblpropertyinventorycategory Where PropertyCategoryCode = @Param ", PropertyCtCode);

            if(Sm.GetLue(LuePropertyCtTo) != Sm.GetLue(LueCCCodeFrom) && MandatoryCertificaeInd == "Y")
            {
                //Label
                LblCertificateType.ForeColor = Color.Red;
                LblCertNo.ForeColor = Color.Red;
                LblCertIssuedDt.ForeColor = Color.Red;
                LblCertIssuedLoc.ForeColor = Color.Red;
                LblNIB.ForeColor = Color.Red;
                LblRegistrationBasis.ForeColor = Color.Red;
                LblBasisDocDt.ForeColor = Color.Red;
                LblSurveyDocNo.ForeColor = Color.Red;
                LblSurveyDocDt.ForeColor = Color.Red;
                LblAreaBased.ForeColor = Color.Red;
                //field
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueCertificateType, TxtCertificateNo, TxtCertIssuedLocation, DteCertIssuedDt, DteCertExpDt, MeeCertExpDt, TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, TxtSurveyDocNo, DteBasisDocDt, DteSurveyDocDt, TxtAreaBased
                }, false);
            }
            else if(Sm.GetLue(LuePropertyCtTo).Length == 0 || (Sm.GetLue(LuePropertyCtTo) != Sm.GetLue(LueCCCodeFrom) && MandatoryCertificaeInd == "N"))
            {
                //Label
                LblCertificateType.ForeColor = Color.Black;
                LblCertNo.ForeColor = Color.Black;
                LblCertIssuedDt.ForeColor = Color.Black;
                LblCertIssuedLoc.ForeColor = Color.Black;
                LblNIB.ForeColor = Color.Black;
                LblRegistrationBasis.ForeColor = Color.Black;
                LblBasisDocDt.ForeColor = Color.Black;
                LblSurveyDocNo.ForeColor = Color.Black;
                LblSurveyDocDt.ForeColor = Color.Black;
                LblAreaBased.ForeColor = Color.Black;
                //Field
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                {
                    LueCertificateType, TxtCertificateNo, DteCertIssuedDt, DteCertExpDt, MeeCertExpDt, TxtCertIssuedLocation,
                    TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, DteBasisDocDt, TxtSurveyDocNo, DteSurveyDocDt
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAreaBased }, 0);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueCertificateType, TxtCertificateNo, TxtCertIssuedLocation, DteCertIssuedDt, DteCertExpDt, MeeCertExpDt,
                    TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, TxtSurveyDocNo, DteBasisDocDt, DteSurveyDocDt, TxtAreaBased
                }, true);
            }
        }

        private void GetParameter()
        {
            mIsAssetTransferUseLocation = Sm.GetParameter("IsAssetTransferUseLocation") == "Y";
            mIsSystemUseCostCenter = Sm.GetParameter("IsSystemUseCostCenter") == "Y";
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC"); 
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void SetLuePropertyCtCode(ref LookUpEdit Lue, string PropertyCategoryCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PropertyCategoryCode As Col1, A.PropertyCategoryName As Col2 ");
            SQL.AppendLine("From TblPropertyInventoryCategory A  ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if(PropertyCategoryCode.Length > 0)
                SQL.AppendLine("And A.PropertyCategoryCode = @PropertyCategoryCode ");
            SQL.AppendLine("Order By A.PropertyCategoryName ");

            cm.CommandText = SQL.ToString();
            if (PropertyCategoryCode.Length > 0) Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", PropertyCategoryCode);

            Sm.SetLue2(
                ref Lue,
                ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PropertyCategoryCode.Length > 0) Sm.SetLue(Lue, PropertyCategoryCode);
        }

        private void UploadFile1(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile1.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile1.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload1.Invoke(
                    (MethodInvoker)delegate { PbUpload1.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload1.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload1.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryTransferFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdatePropertyInventoryTransferFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    FileName1=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        
        private void UploadFile2(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryTransferFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdatePropertyInventoryTransferFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void DownloadFile1(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload1.Value = 0;
                PbUpload1.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload1.Value = PbUpload1.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload1.Value + bytesRead <= PbUpload1.Maximum)
                        {
                            PbUpload1.Value += bytesRead;

                            PbUpload1.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode + "-Property Inventory Transfer";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }
        
        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode + "-Property Inventory Transfer";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsDocNeedApproval()
        {
            string mDeptCode = Sm.GetValue("Select B.DeptCode From TblPropertyInventoryHdr A " +
                "Inner Join TblCostCenter B On A.CCCode = B.CCCode " +
                "Where A.CCCode = @Param ", Sm.GetLue(LueCCCodeFrom));
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PropertyInventoryTransfer' And DeptCode = @Param Limit 1;", mDeptCode);
        }

        #endregion

        #endregion

        #region Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCodeFrom, new Sm.RefreshLue1(Sl.SetLueCCCode));
        }

        private void LueCCCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCodeTo, new Sm.RefreshLue1(Sl.SetLueCCCode));
            //if (Sm.GetLue(LueCCCode2).Length > 0)
            //{
            //    SetLueAssetCatCode(ref LueAssetCatCode2, Sm.GetLue(LueCCCode2));
            //}
        }

        private void BtnFile1_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile1.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile1.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ChkFile1_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile1.Checked == false)
            {
                TxtFile1.EditValue = string.Empty;
            }
        }

        private void ChkFile2_Click(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void BtnDownload1_Click(object sender, EventArgs e)
        {
            DownloadFile1(mHostAddrForFTPClient, mPortForFTPClient, TxtFile1.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile1.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile1, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }
        
        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCodeFrom, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void LuePropertyCtTo_Validated(object sender, EventArgs e)
        {
            
        }

        private void LueCertificateType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCertificateType, new Sm.RefreshLue2(Sl.SetLueOption), "CertificateType");
        }

        private void LuePropertyCtFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCtFrom, new Sm.RefreshLue2(SetLuePropertyCtCode), string.Empty);
        }

        private void LuePropertyCtTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCtTo, new Sm.RefreshLue2(SetLuePropertyCtCode), string.Empty);
            if (BtnSave.Enabled && Sm.GetLue(LuePropertyCtTo).Length > 0)
                SetTabCertificate();
        }

        private void LueSiteCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCodeTo, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }


        #endregion

    }
}
