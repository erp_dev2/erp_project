﻿#region update

/*
 * 25/04/2022 [SET/PRODUCT] Menu find baru
 * 17/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesEquitySecuritiesFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesEquitySecurities mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesEquitySecuritiesFind(FrmSalesEquitySecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Document Date",
                        "Investmnent Bank Account (RDN)",
                        "Investment Name",
                        "Investment Code",

                        //6-10
                        "Type",
                        "Category",
                        "Quantity",
                        "Moving Average Price",
                        "Sales Amount",

                        //11-15
                        "Realized Gain/Loss",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",

                        //16-17
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 50, 150, 150,  

                        //6-10
                        100, 100, 100, 100, 150,

                        //11-15
                        150, 100, 100, 100, 100,
                        
                        //16-20
                        100, 100
                    }
                );
            //Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, C.BankAcNm, E.PortofolioName InvestmentEquityName, B.InvestmentCode, F.OptDesc InvestmentType, G.InvestmentCtName, ");
            SQL.AppendLine("B.Qty, B.MovingAvgPrice, B.SalesAmt, B.RealizedGainLoss, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblsalesequitysecuritieshdr A ");
            SQL.AppendLine("INNER JOIN tblsalesequitysecuritiesdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN tblbankaccount C ON B.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemequity D ON B.InvestmentEquityCode = D.InvestmentEquityCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio E ON D.PortofolioId = E.PortofolioId ");
            SQL.AppendLine("INNER JOIN tbloption F ON B.InvestmentType = F.OptCode AND F.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory G ON E.InvestmentCtCode = G.InvestmentCtCode   ");

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "BankAcNm", "InvestmentEquityName", "InvestmentCode", "InvestmentType",

                            //6-10
                            "InvestmentCtName", "Qty", "MovingAvgPrice", "SalesAmt", "RealizedGainLoss", 
                            
                            //11-14
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt" 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 14);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }


        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
