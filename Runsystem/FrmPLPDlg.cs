﻿#region Update
/*
    16/09/2018 [WED] ubah source nya ke TblLoanSummary
    25/09/2018 [WED] bisa milih lebih dari satu data Survey Document
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPLPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPLP mFrmParent;
        private string mSQL = string.Empty, mPnCode = string.Empty, mSurveyDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmPLPDlg(FrmPLP FrmParent, string PnCode, string SurveyDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPnCode = PnCode;
            mSurveyDocNo = SurveyDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Partner#",
                    "",
                    "Partner Name",
                    "Survey#",
                    
                    //6-10
                    "Date",
                    "Month",
                    "Year",
                    "Amount",
                    "Interest Amount",

                    //11-13
                    "Total Amount",
                    "Mth",
                    "SurveyDNo",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 20, 180, 160, 
                    
                    //6-10
                    80, 120, 100, 200, 200, 

                    //11-13
                    200, 0, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.PnCode, D.PnName, A.DocNo, A.DocDt, MonthName(Str_To_Date(B.Mth, '%m')) As Mth, (B.AmtMain - B.PaymentAmtMain) AmtMain, (B.AmtRate - B.PaymentAmtRate) AmtRate, ((B.AmtMain - B.PaymentAmtMain) + (B.AmtRate - B.PaymentAmtRate)) Amt, ");
            SQL.AppendLine("B.Mth As MthNo, B.Yr, B.SurveyDNo ");
            SQL.AppendLine("From (Select * From TblSurvey Where Status = 'A' And CancelInd = 'N' And DocNo = @SurveyDocNo) A ");
            SQL.AppendLine("Inner Join TblLoanSummary B On A.DocNo = B.SurveyDocNo And B.PaymentInd In ('O', 'P') ");
            SQL.AppendLine("    And Concat(B.SurveyDocNo, B.SurveyDNo) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(T2.SurveyDocNo, T2.SurveyDNo) ");
            SQL.AppendLine("        From TblPLPHdr T1 ");
            SQL.AppendLine("        Inner Join TblPLPDtl T2 On T1.DocNo = T2.DocNo And T1.Status = 'O' And T1.CancelInd = 'N' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And Not Find_In_Set(Concat(B.SurveyDocNo, B.SurveyDNo), @GetSelectedSurvey) ");
            SQL.AppendLine("Inner Join TblRequestLP C On A.RQLPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPartner D On C.PnCode = D.PnCode And D.PnCode = @PnCode ");
            SQL.AppendLine("Inner Join TblVoucherHdr E On A.VoucherRequestDocNo = E.VoucherRequestDocNo And E.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@GetSelectedSurvey", mFrmParent.GetSelectedSurvey());
                Sm.CmParam<String>(ref cm, "@PnCode", mPnCode);
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", mSurveyDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtPnCode.Text, new string[] { "C.PnCode", "D.PnName", "D.CompanyName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter +
                    " Order By D.PnName, B.SurveyDNo; ",
                    new string[] 
                    {                     
                        //0
                        "PnCode",
                        
                        //1-5
                        "PnName", "DocNo", "DocDt", "Mth", "AmtMain", 
                        
                        //6-10
                        "AmtRate", "Amt", "MthNo", "Yr", "SurveyDNo"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            //if (Sm.IsFindGridValid(Grd1, 1))
            //{
            //    mFrmParent.ClearData2();
            //    int Row1 = Grd1.CurRow.Index, Row2 = 0;
            //    mFrmParent.ShowSurveyHdr(Sm.GetGrdStr(Grd1, Row1, 4));
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 10);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 6);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 11);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 7);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 8);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 9);
            //    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 12);
            //    mFrmParent.ComputeOutstandingAmt();
            //    mFrmParent.Grd1.Rows.Add();
            //    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3, 4, 5 });
            //    this.Close();
            //}

            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsSurveyAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        mFrmParent.Grd1.Cells[Row1, 9].Value = 0m;

                        mFrmParent.ShowSurveyHdr(Sm.GetGrdStr(Grd1, Row2, 5));
                        mFrmParent.ComputeOutstandingAmt(Sm.GetGrdStr(Grd1, Row2, 5));

                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, Row1, new int[] { 9 });

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 7, 8, 9 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 survey.");

        }

        private bool IsSurveyAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 13);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    key, mFrmParent.TxtSurveyDocNo.Text + Sm.GetGrdStr(mFrmParent.Grd1, Index, 6)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                ChooseData();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmPartner(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmPartner");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mPnCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmPartner(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmPartner");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mPnCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtPnCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPnCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }

        #endregion

        #endregion

    }
}
