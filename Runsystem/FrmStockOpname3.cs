﻿#region Update
/*
    16/02/2022 [IBL/PHT] New Apps
    28/03/2022 [IBL/PHT] Feedback: Journal ke 3 tidak dibentuk.
                         Bug: Journal ke 2 nilainya masih sama dengan journal ke 1
    30/03/2022 [IBL/PHT] Bug: Journal ke 2 nilai debit dan credit terbalik.
    25/04/2022 [RDA/PHT] Validasi muncul warning ketika item stock > 0 ada yg belum dipilih di dialog 
    27/04/2022 [RDA/PHT] cancel data + validasi dengan param JournalClosingInventoryPeriodValidation
    17/05/2022 [IBL/PHT] Bug: Tidak bisa save krna warning dari List yg bernilai NULL.
    20/07/2022 [IBL/PHT] Ngga update journal menjadi cancel ketika cancel transaksi.
                         Update JournalDocNo Cancelling ke TblStockOpname2Hdr
    11/04/2022 [RDA/PHT] Bug: tidak bisa save karena muncul warning dari IsNotAllItemDlgSelected
    28/11/2022 [RDA/PHT] validasi closing journal melihat ke monthly closing journal
    28/11/2022 [RDA/PHT] save ketika qty 0
    20/12/2022 [IBL/BBT] Saat cancel dg stock yg mempunyai source2 qty nya dibalikin seperti semula
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    15/03/2023 [VIN/PHT] hide kolom currency
    21/03/2023 [WED/PHT] truncate nilai balance
    27/03/2023 [WED/PHT] truncate balance di akhir kalkulasi
    31/03/2023 [WED/PHT] tambah kondisi untuk validasi IsNotAllItemDlgSelected, yaitu ketika Items juga ada isinya
    01/04/2023 [WED?PHT] tambah method untuk hapus dari stock summary untuk warehouse dan item yang pertama lalu di cancel
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockOpname3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmStockOpname3Find FrmFind;

        internal int mNumberOfInventoryUomCode = 0;
        internal bool
            mIsAutoJournalActived = false,
            mIsShowForeignName = false,
            mIsBOMShowSpecifications = false;
        internal string
            mWhsCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;

        internal string
            mProfitCenterCode = string.Empty,
            mItSrcDlg = string.Empty;

        private List<String> mlAcNoStock = new List<String>();
        private string
            mDocType = "56",
            mEntCode = string.Empty,
            mFormulaForComputeProfitLoss = "0",
            mCurrentEarningFormulaType = "1",
            mAcNoForCurrentEarning = "3.3",
            mAcNoForCurrentEarning2 = "9",
            mAcNoForCurrentEarning3 = string.Empty,
            mAcNoForIncome = "4",
            mAcNoForIncome2 = string.Empty,
            mAcNoForCost = "5",
            mAcNoForActiva = "1",
            mAcNoForPassiva = "2",
            mAcNoForCapital = "3",
            mMaxAccountCategory = "9",
            mJournalClosingInventoryPeriodValidation = "1",
            mJournalDocNoFormat = string.Empty,
            mQueryProfitCenter = " From TblWarehouse A Inner Join TblCostCenter B On A.CCCode = B.CCCode Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode Where A.WhsCode = @Param Limit 1;";

        private bool
            mIsAccountingRptUseJournalPeriod = false,
            mIsReportingFilterByEntity = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptTBCalculateFicoNetIncome = false;

        #endregion

        #region Constructor

        public FrmStockOpname3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region From Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Stock Opname With Value";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Item's Category"+Environment.NewLine+"Code",
                        "Item's Category",

                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Account#",

                        //11-15
                        "Account Description",
                        "Currency",
                        "Item's Price"+Environment.NewLine+"(Beg.)",
                        "Item's Price"+Environment.NewLine+"(End.) (!)",
                        "Item's Price" + Environment.NewLine + "Adjustment",

                        //16-20
                        "Stock Qty"+Environment.NewLine+"(Beg.)",
                        "Stock Qty"+Environment.NewLine+"(End.)",
                        "Adjustment",
                        "UoM",
                        "Stock Qty"+Environment.NewLine+"(Beg.)", 

                        //21-25
                        "Stock Qty"+Environment.NewLine+"(End.)",
                        "Adjustment",
                        "UoM",
                        "Stock Qty"+Environment.NewLine+"(Beg.)",
                        "Stock Qty"+Environment.NewLine+"(End.)",

                        //26-30
                        "Adjustment",
                        "UoM",
                        "Beginning Balance",
                        "Ending Balance",
                        "Adjustment",

                        //31-32
                        "Type",
                        "ExcRate"
                    },
                     new int[]
                    {
                        //0
                        20,
 
                        //1-5
                        80, 20, 300, 100, 300,

                        //6-10
                        200, 170, 60, 50, 150,

                        //11-15
                        300, 80, 120, 120, 120,

                        //16-20
                        80, 80, 80, 100, 80,

                        //21-25
                        80, 80, 100, 80, 80,

                        //26-30
                        80, 100, 120, 120, 120,

                        //31-32
                        0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30, 32 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 12, 20, 21, 22, 23, 24, 25, 26, 27, 31, 32 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueProfitCenterCode, DteClosingDt, ChkCancelInd, MeeCancelReason }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueProfitCenterCode, DteClosingDt }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 14, 17 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    break;
                default:
                    break;
            }
        }
        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, LueProfitCenterCode, DteClosingDt, TxtEndingBalanceAmt,
                DtePriorClosingDt, TxtBeginningBalanceAmt, MeeCancelReason, TxtJournalDocNo, TxtJournalDocNo2, TxtJournalDocNo3, TxtJournalDocNo4
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }
        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 13, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30 });
        }

        #endregion

        #region Button Method
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockOpname3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueProfitCenterCode(ref LueProfitCenterCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmStockOpname3Dlg(this, mWhsCode));
                }

                if (e.ColIndex == 0)
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 13, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30, 32 });
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center"))
                Sm.FormShowDialog(new FrmStockOpname3Dlg(this, mWhsCode));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotal();

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17, 21, 25, 14 }, e);
            if (e.ColIndex == 17)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 1, 17, 21, 25, 19, 23, 27);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 1, 17, 25, 21, 19, 27, 23);
            }
            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 1, 21, 17, 25, 23, 19, 27);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 1, 21, 25, 17, 23, 27, 19);
            }
            if (e.ColIndex == 25)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 1, 25, 17, 21, 27, 19, 23);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 1, 25, 21, 17, 27, 23, 19);
            }

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 27)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 25, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 21 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 23), Sm.GetGrdStr(Grd1, e.RowIndex, 27)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 25, Grd1, e.RowIndex, 21);


            Grd1.Cells[e.RowIndex, 18].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 17) - Sm.GetGrdDec(Grd1, e.RowIndex, 16);
            Grd1.Cells[e.RowIndex, 22].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 21) - Sm.GetGrdDec(Grd1, e.RowIndex, 20);
            Grd1.Cells[e.RowIndex, 26].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 25) - Sm.GetGrdDec(Grd1, e.RowIndex, 24);
            ComputeTotal();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            mEntCode = Sm.GetValue("Select C.EntCode " + mQueryProfitCenter, mWhsCode);

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockOpname2", "TblStockOpname2Hdr");
            var cml = new List<MySqlCommand>();
            var lCOA3 = new List<COA3>();
            string profitCenterCode = Sm.GetValue("Select B.ProfitCenterCode " + mQueryProfitCenter, mWhsCode);
            string code1 = Sm.GetCode1ForJournalDocNo("FrmStockOpname3", string.Empty, string.Empty, mJournalDocNoFormat);

            cml.Add(SaveStockOpname2Hdr(DocNo));
            cml.Add(SaveStockOpname2Dtl(DocNo));

            cml.Add(SaveStock(DocNo));
            if (mIsAutoJournalActived)
            {
                //ProcessTrialBalance(ref lCOA3);
                cml.Add(SaveJournal(DocNo, ref lCOA3, code1, profitCenterCode));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center") ||
                Sm.IsDteEmpty(DteClosingDt, "Closing Date") ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteClosingDt), Sm.GetLue(LueProfitCenterCode)) ||
                IsGrdEmpty() ||
                IsGrdNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsNotAllItemDlgSelected() ||
                JournalClosingPeriodValidation()
                ;
        }

        private bool JournalClosingPeriodValidation()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ProfitCenterCode = Sm.GetLue(LueProfitCenterCode), ClosingDate = Sm.GetDte(DteClosingDt).Substring(0, 8);

            SQL.AppendLine("SELECT 1 FROM tblstockopname2hdr ");
            SQL.AppendLine("WHERE CancelInd='N' AND ProfitCenterCode = @ProfitCenterCode ");
            if (mJournalClosingInventoryPeriodValidation == "1")
                SQL.AppendLine("AND ClosingDt = @ClosingDt ");
            else
                SQL.AppendLine("AND LEFT(ClosingDt,6) = LEFT(@ClosingDt,6)  ");
            SQL.AppendLine("LIMIT 1 ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParamDt(ref cm, "@ClosingDt", ClosingDate);

            return Sm.IsDataExist(cm, "Selected profit center and closing date is already in active document.");
        }

        private bool IsNotAllItemDlgSelected()
        {
            var SQL = new StringBuilder();
            string Items = GetItSrcCode();

            SQL.AppendLine("Select Group_Concat(C.Source) As Source ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblStockSummary C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Where C.WhsCode = @Param  ");
            SQL.AppendLine("And C.Qty <> 0; ");

            mItSrcDlg = Sm.GetValue(SQL.ToString(), mWhsCode);

            List<string> ItemSrcMaster = Items.Split(',').ToList();
            List<string> ItemSrcDlg = mItSrcDlg.Split(',').ToList();

            if (mItSrcDlg.Length > 0 && Items.Length > 0)
            {
                if (!ItemSrcDlg.All(x => ItemSrcMaster.Contains(x)))
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input all item with stock more than zero.");
                    return true;
                }
                else return false;
            }
            else
                return false;
        }

        private bool IsGrdNotValid()
        {
            var SQL = new StringBuilder();
            string mItName = string.Empty;

            SQL.AppendLine("Select C.ItName From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblProfitCenter D On A.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("Where A.PriorClosingDt = @Param1 ");
            SQL.AppendLine("And A.ProfitCenterCode = @Param2 ");
            SQL.AppendLine("And B.ItCode = @Param3 ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                mItName = Sm.GetValue(SQL.ToString(), Sm.GetDte(DteClosingDt).Substring(0, 8), Sm.GetLue(LueProfitCenterCode), Sm.GetGrdStr(Grd1, r, 1));
                if (mItName.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "This item already exists in another document." + Environment.NewLine + Environment.NewLine +
                        "Profit Center : " + LueProfitCenterCode.Text + Environment.NewLine +
                        "Prior Closing Date : " + DteClosingDt.Text + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 3)
                        );
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveStockOpname2Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblStockOpname2Hdr(DocNo, DocDt, CancelInd, ClosingDt, PriorClosingDt, ProfitCenterCode, WhsCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @ClosingDt, @PriorClosingDt, @ProfitCenterCode, @WhsCode, @Remark, @UserCode, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParamDt(ref cm, "@PriorClosingDt", Sm.GetDte(DtePriorClosingDt));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockOpname2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            string mBatchNo = string.Empty, mSource = string.Empty, mSource2 = string.Empty,
                   mCurCode = string.Empty, mLot = string.Empty, mBin = string.Empty;

            SQL.AppendLine("/* Stock Opname With Value (Customize PHT) - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblStockOpname2Dtl(DocNo, DNo, ItCode, BatchNo, Source, Lot, Bin, ");
                        SQL.AppendLine("QtyActual, QtyActual2, QtyActual3, Qty, Qty2, Qty3, ");
                        SQL.AppendLine("CurCode, UPrice, Source2, UPrice2, ItemSourceInd, ");
                        SQL.AppendLine("CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @BatchNo_" + r.ToString() +
                        ", @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @QtyActual_" + r.ToString() +
                        ", @QtyActual2_" + r.ToString() +
                        ", @QtyActual3_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @CurCode_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Source2_" + r.ToString() +
                        ", @UPrice2_" + r.ToString() +
                        ", @ItemSourceInd_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    if (Sm.GetGrdStr(Grd1, r, 31) == "1")
                    {
                        mBatchNo = Sm.Left(Sm.GetDte(DteDocDt), 8);
                        mSource = mDocType + "*" + DocNo + "*" + Sm.Right("00000" + (r + 1).ToString(), 5);
                        mSource2 = string.Empty;
                        mCurCode = Sm.GetParameter("MainCurCode");
                        mLot = "-";
                        mBin = "-";
                    } else
                    {
                        mBatchNo = Sm.GetGrdStr(Grd1, r, 6);
                        mSource = Sm.GetGrdStr(Grd1, r, 7);
                        mSource2 = (Sm.GetGrdDec(Grd1, r, 13) != Sm.GetGrdDec(Grd1, r, 14) ? mDocType + "*" + DocNo + "*" + Sm.Right("00000" + (r + 1).ToString(), 5) : string.Empty);
                        mCurCode = Sm.GetGrdStr(Grd1, r, 12);
                        mLot = Sm.GetGrdStr(Grd1, r, 8);
                        mBin = Sm.GetGrdStr(Grd1, r, 9);
                    }

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), mBatchNo);
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), mSource);
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), mLot);
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), mBin);
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 21));
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 25));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 22));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), mCurCode);
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                    Sm.CmParam<String>(ref cm, "@Source2_" + r.ToString(), mSource2);
                    Sm.CmParam<Decimal>(ref cm, "@UPrice2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
                    Sm.CmParam<String>(ref cm, "@ItemSourceInd_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 31));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            //ItemSourceInd 1: Master Item; 2: Stock Summary

            var SQL = new StringBuilder();
            //Save item stock from master item
            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.ItemSourceInd = '1'; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.ItemSourceInd = '1'; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, B.CurCode, B.UPrice2, 1.00, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.ItemSourceInd = '1'; ");

            //Save item stock from stock summary
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.ItemSourceInd = '2'; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source, ");
            SQL.AppendLine("    QtyActual, QtyActual2, QtyActual3 ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And Source2 Is Null And ItemSourceInd = '2' ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=IfNull(B.QtyActual, 0), ");
            SQL.AppendLine("    A.Qty2=IfNull(B.QtyActual2, 0), ");
            SQL.AppendLine("    A.Qty3=IfNull(B.QtyActual3, 0), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode;");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*(B.QtyActual-B.Qty), -1*(B.QtyActual2-B.Qty2), -1*(B.QtyActual3-B.Qty3), ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null And B.ItemSourceInd = '2' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source2, ");
            SQL.AppendLine("B.QtyActual, B.QtyActual2, B.QtyActual3, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null And B.ItemSourceInd = '2' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And Source2 Is Not Null And ItemSourceInd = '2'");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=0, A.Qty2=0, A.Qty3=0, ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode;");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, Lot, Bin, ItCode, BatchNo, Source2, QtyActual, QtyActual2, QtyActual3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Dtl ");
            SQL.AppendLine("Where DocNo=@DocNo And Source2 Is Not Null And ItemSourceInd = '2'; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.ItCode, A.BatchNo, A.Source2, A.CurCode, A.UPrice2, B.ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Dtl A ");
            SQL.AppendLine("Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Source2 Is Not Null And A.ItemSourceInd = '2';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, ref List<COA3> lCOA3, string code1, string profitCenterCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            //Save Journal 1
            SQL.AppendLine("Set @Row:=0; ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.ClosingDt, ");
            SQL.AppendLine("Concat('Inventory Closing Balance : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    	Select T4.AcNo, ");
            SQL.AppendLine("    	Case ");
            SQL.AppendLine("    		When T1.Source2 Is Null ");
            //SQL.AppendLine("    			Then IfNull(Truncate(T1.QtyActual, 0), 0.00)*IfNull(Truncate(T1.UPrice2, 0), 0.00)*IfNull(Truncate(T5.ExcRate, 0), 0.00) ");
            SQL.AppendLine("    			Then Truncate(IfNull(T1.QtyActual, 0.00)*IfNull(T1.UPrice2, 0.00)*IfNull(T5.ExcRate, 0.00), 0) ");
            SQL.AppendLine("    		When T1.Source2 Is Not Null ");
            //SQL.AppendLine("    			Then IfNull(Truncate(T1.QtyActual, 0), 0.00)*IfNull(Truncate(T1.UPrice2, 0), 0.00)*IfNull(Truncate(T6.ExcRate, 0), 0.00) ");
            SQL.AppendLine("    			Then Truncate(IfNull(T1.QtyActual, 0.00)*IfNull(T1.UPrice2, 0.00)*IfNull(T6.ExcRate, 0.00), 0) ");
            SQL.AppendLine("    	End As DAmt, ");
            SQL.AppendLine("    	0.00 As CAmt ");
            SQL.AppendLine("    	From TblStockOpname2Dtl T1 ");
            SQL.AppendLine("    	Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            SQL.AppendLine("    	Inner Join TblItemCategory T3 On T2.ItCtCode = T3.ItCtCode ");
            SQL.AppendLine("    	Inner Join TblCOA T4 On T3.AcNo = T4.AcNo And T3.AcNo Is Not Null ");
            SQL.AppendLine("    	Left Join TblStockPrice T5 On T1.Source = T5.Source And T1.Source2 Is Null ");
            SQL.AppendLine("    	Left Join TblStockPrice T6 On T1.Source2 = T6.Source And T1.Source2 Is Not Null ");
            SQL.AppendLine("    	Where T1.DocNo = @DocNo ");
            SQL.AppendLine("    	Union All ");
            SQL.AppendLine("    	Select T2.ParValue As AcNo, 0.00 As DAmt, Sum(T1.Amt) As CAmt ");
            SQL.AppendLine("    	From ( ");
            SQL.AppendLine("    		Select Case ");
            SQL.AppendLine("    			When X1.Source2 Is Null ");
            //SQL.AppendLine("    				Then IfNull(Truncate(X1.QtyActual, 0), 0.00)*IfNull(Truncate(X1.UPrice2, 0), 0.00)*IfNull(Truncate(X4.ExcRate, 0), 0.00) ");
            SQL.AppendLine("    				Then Truncate(IfNull(X1.QtyActual, 0.00)*IfNull(X1.UPrice2, 0.00)*IfNull(X4.ExcRate, 0.00), 0) ");
            SQL.AppendLine("    			When X1.Source2 Is Not Null ");
            //SQL.AppendLine("    				Then IfNull(Truncate(X1.QtyActual, 0), 0.00)*IfNull(Truncate(X1.UPrice2, 0), 0.00)*IfNull(Truncate(X5.ExcRate, 0), 0.00) ");
            SQL.AppendLine("    				Then Truncate(IfNull(X1.QtyActual, 0.00)*IfNull(X1.UPrice2, 0.00)*IfNull(X5.ExcRate, 0.00), 0) ");
            SQL.AppendLine("    		End As Amt ");
            SQL.AppendLine("    		From TblStockOpname2Dtl X1 ");
            SQL.AppendLine("    		Inner Join TblItem X2 On X1.ItCode = X2.ItCode ");
            SQL.AppendLine("    		Inner Join TblItemCategory X3 On X2.ItCtCode = X3.ItCtCode ");
            SQL.AppendLine("    		Left Join TblStockPrice X4 On X1.Source = X4.Source And X1.Source2 Is Null ");
            SQL.AppendLine("    		Left Join TblStockPrice X5 On X1.Source2 = X5.Source And X1.Source2 Is Not Null ");
            SQL.AppendLine("    		Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    	) T1 ");
            SQL.AppendLine("    	Inner Join TblParameter T2 On T2.ParCode = 'AcNoForStockOpname' And T2.ParCode Is Not Null ");
            SQL.AppendLine("    )T Where T.AcNo Is Not Null ");
            SQL.AppendLine("    Group By T.AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            //Save Journal 2. 
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo2, A.ClosingDt, ");
            SQL.AppendLine("Concat('Inventory Beginning Balance : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("Select T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select T4.AcNo, 0.00 As DAmt, ");
            SQL.AppendLine("	Case ");
            SQL.AppendLine("		When T1.Source2 Is Null ");
            //SQL.AppendLine("			Then IfNull(Truncate(T1.QtyActual, 0) - Truncate(T1.Qty, 0), 0.00)*IfNull(Truncate(T1.UPrice, 0), 0.00)*IfNull(Truncate(T5.ExcRate, 0), 0.00) ");
            SQL.AppendLine("			Then Truncate((IfNull(T1.QtyActual, 0.00) - IfNull(T1.Qty, 0.00)) * IfNull(T1.UPrice, 0.00) * IfNull(T5.ExcRate, 0.00), 0) ");
            SQL.AppendLine("		When T1.Source2 Is Not Null ");
            //SQL.AppendLine("			Then IfNull(Truncate(T1.QtyActual, 0) - Truncate(T1.Qty, 0), 0.00)*IfNull(Truncate(T1.UPrice, 0), 0.00)*IfNull(Truncate(T6.ExcRate, 0), 0.00) ");
            SQL.AppendLine("			Then Truncate((IfNull(T1.QtyActual, 0.00) - IfNull(T1.Qty, 0.00)) * IfNull(T1.UPrice, 0.00) * IfNull(T6.ExcRate, 0.00), 0) ");
            SQL.AppendLine("	End As CAmt ");
            SQL.AppendLine("	From TblStockOpname2Dtl T1 ");
            SQL.AppendLine("	Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            SQL.AppendLine("	Inner Join TblItemCategory T3 On T2.ItCtCode = T3.ItCtCode ");
            SQL.AppendLine("	Inner Join TblCOA T4 On T3.AcNo = T4.AcNo And T3.AcNo Is Not Null ");
            SQL.AppendLine("	Left Join TblStockPrice T5 On T1.Source = T5.Source And T1.Source2 Is Null ");
            SQL.AppendLine("	Left Join TblStockPrice T6 On T1.Source2 = T6.Source And T1.Source2 Is Not Null ");
            SQL.AppendLine("	Where T1.DocNo = @DocNo ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select T2.ParValue As AcNo, Sum(T1.Amt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("	From ( ");
            SQL.AppendLine("		Select Case ");
            SQL.AppendLine("			When X1.Source2 Is Null ");
            //SQL.AppendLine("				Then IfNull(Truncate(X1.QtyActual, 0) - Truncate(X1.Qty, 0), 0.00)*IfNull(Truncate(X1.UPrice, 0), 0.00)*IfNull(Truncate(X4.ExcRate, 0), 0.00) ");
            SQL.AppendLine("			Then Truncate((IfNull(X1.QtyActual, 0.00) - IfNull(X1.Qty, 0.00)) * IfNull(X1.UPrice, 0.00) * IfNull(X4.ExcRate, 0.00), 0) ");
            SQL.AppendLine("			When X1.Source2 Is Not Null ");
            //SQL.AppendLine("				Then IfNull(Truncate(X1.QtyActual, 0) - Truncate(X1.Qty, 0), 0.00)*IfNull(Truncate(X1.UPrice, 0), 0.00)*IfNull(Truncate(X5.ExcRate, 0), 0.00) ");
            SQL.AppendLine("			Then Truncate((IfNull(X1.QtyActual, 0.00) - IfNull(X1.Qty, 0.00)) * IfNull(X1.UPrice, 0.00) * IfNull(X5.ExcRate, 0.00), 0) ");
            SQL.AppendLine("		End As Amt ");
            SQL.AppendLine("		From TblStockOpname2Dtl X1 ");
            SQL.AppendLine("		Inner Join TblItem X2 On X1.ItCode = X2.ItCode ");
            SQL.AppendLine("		Inner Join TblItemCategory X3 On X2.ItCtCode = X3.ItCtCode ");
            SQL.AppendLine("		Left Join TblStockPrice X4 On X1.Source = X4.Source And X1.Source2 Is Null ");
            SQL.AppendLine("		Left Join TblStockPrice X5 On X1.Source2 = X5.Source And X1.Source2 Is Not Null ");
            SQL.AppendLine("		Where X1.DocNo = @DocNo ");
            SQL.AppendLine("	) T1 ");
            SQL.AppendLine("	Inner Join TblParameter T2 On T2.ParCode = 'AcNoForStockOpname' And T2.ParCode Is Not Null ");
            SQL.AppendLine(")T Where T.AcNo Is Not Null ");
            SQL.AppendLine("Group By T.AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            //Save Journal 3
            #region Comment By IBL
            /*
            if (lCOA3.Count > 0)
            {
                bool IsFirst = true;
                int i = 0;

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Update TblStockOpname2Hdr Set ");
                SQL.AppendLine("    JournalDocNo3=@JournalDocNo3 ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.JournalDocNo3, A.ClosingDt, ");
                SQL.AppendLine("Concat('Offsetting Inventory Beginning Balance : ', A.DocNo) As JnDesc, ");
                SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblStockOpname2Hdr A ");
                SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
                SQL.AppendLine("    From ( ");
                
                foreach (var x in lCOA3)
                {
                    if (!IsFirst)
                        SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select @AcNoBeg_" + i.ToString() + " As AcNo, @DAmt_" + i.ToString() + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select @AcNoStock_" + i.ToString() + " As AcNo, 0.00 As DAmt, @CAmt_" + i.ToString() + " As CAmt ");
                    IsFirst = false;

                    Sm.CmParam<String>(ref cm, "@AcNoStock_" + i.ToString(), x.AcNoStock);
                    Sm.CmParam<String>(ref cm, "@AcNoBeg_" + i.ToString(), x.AcNoBeg);
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + i.ToString(), x.YearToDtBalance);
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + i.ToString(), x.YearToDtBalance);

                    i++;
                }
                SQL.AppendLine("    )T Where T.AcNo Is Not Null ");
                SQL.AppendLine("    Group By T.AcNo ");
                SQL.AppendLine(")B On 1=1 ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo3;");

                Sm.CmParam<String>(ref cm, "@JournalDocNo3", Sm.GenerateDocNoJournal(Sm.GetDte(DteClosingDt), 3));
            }
            */
            #endregion

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            if (mJournalDocNoFormat == "1")
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteClosingDt), 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteClosingDt), 2));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteClosingDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteClosingDt), 2, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mWhsCode.Length == 0)
            {
                mWhsCode = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'WhsCodeForProfitCenter' And OptCode = @Param;", Sm.GetLue(LueProfitCenterCode));
            }

            string profitCenterCode = Sm.GetValue("Select B.ProfitCenterCode " + mQueryProfitCenter, mWhsCode);
            string code1 = Sm.GetCode1ForJournalDocNo("FrmStockOpname3", string.Empty, string.Empty, mJournalDocNoFormat);

            cml.Add(EditStockOpname2Hdr());
            cml.Add(EditStock());
            cml.Add(SaveJournal2(code1, profitCenterCode));

            Sm.ExecCommands(cml);

            //jika item tersebut untuk profit center warehouse tersebut adalah yg "PERTAMA" dibuat untuk stock nya dan di cancel, maka data stocksummary untuk item tersebut untuk profit center warehouse tersebut dihapus (tapi di stock movement tetep ada untuk record history nya) - wed
            //item terhadap profit center warehouse tersebut dihapus, agar ketika insert baru untuk item dan profit center warehouse yg sama itu dianggapnya sebagai item baru.
            var l = new List<StockSummary>();

            GetFirstStockItem(mWhsCode, Sm.GetLue(LueProfitCenterCode), ref l);
            if (l.Count > 0)
            {
                DeleteStockSummary(ref l);
            }

            l.Clear();

            ShowData(TxtDocNo.Text);
        }

        //Validation
        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteClosingDt), Sm.GetLue(LueProfitCenterCode)) ||
                IsDataCancelledAlready() ||
                IsItemAlreadyUseInAnotherDocument()||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue()
                ;
        }


        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblStockOpname2Hdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsItemAlreadyUseInAnotherDocument()
        {
            var SQL = new StringBuilder();
            string DocNo = string.Empty;

            SQL.AppendLine("Select Group_Concat(Distinct(A.DocNo) Separator ', ') From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo <> @Param1 ");
            SQL.AppendLine("And A.ProfitCenterCode = @Param2 ");
            SQL.AppendLine("And B.Source Like Concat('%',@Param1,'%') ");
            SQL.AppendLine("And A.CancelInd ='N' ");
            SQL.AppendLine("And B.ItCode In ( ");
            SQL.AppendLine("	Select Distinct(ItCode) From TblStockOpname2Dtl ");
            SQL.AppendLine("	Where DocNo = @Param1 ");
            SQL.AppendLine(") ");
            if (mJournalClosingInventoryPeriodValidation == "1")
                SQL.AppendLine("And A.ClosingDt > @Param3; ");
            else
                SQL.AppendLine("And Left(A.ClosingDt,6) > Left(@Param3, 6); ");

            DocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text, Sm.GetLue(LueProfitCenterCode), Sm.GetDte(DteClosingDt));

            if (DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "There are already items with the same source and the same profit center in another document with a later date than this document." +
                    Environment.NewLine + "Document#: " + DocNo);
                return true;
            }

            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        //Edit Data
        private MySqlCommand EditStockOpname2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditStock()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, -1*(B.Qty), -1*(B.Qty2), -1*(B.Qty3), ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.Source2 Is Null; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source, Source2, ");
            SQL.AppendLine("    Qty, Qty2, Qty3 ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=IfNull(A.Qty, 0) - ifnull(B.Qty, 0), ");
            SQL.AppendLine("    A.Qty2=IfNull(A.Qty2, 0) - ifnull(B.Qty, 0), ");
            SQL.AppendLine("    A.Qty3=IfNull(A.Qty3, 0) - ifnull(B.Qty, 0), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode And B.Source2 Is Null;");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source2, -1*(B.QtyActual), -1*(B.QtyActual2), -1*(B.QtyActual3), ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.Source2 Is Not Null; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source, Source2, ");
            SQL.AppendLine("    QtyActual, QtyActual2, QtyActual3 ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source2 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=IfNull(A.Qty, 0) - ifnull(B.QtyActual, 0), ");
            SQL.AppendLine("    A.Qty2=IfNull(A.Qty2, 0) - ifnull(B.QtyActual2, 0), ");
            SQL.AppendLine("    A.Qty3=IfNull(A.Qty3, 0) - ifnull(B.QtyActual3, 0), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode And B.Source2 Is Not Null;");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, IfNull(B.QtyActual-B.Qty, 0.00), IfNull(B.QtyActual2-B.Qty2, 0.00), IfNull(B.QtyActual3-B.Qty3, 0.00), ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.Source2 Is Not Null; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source, Source2, ");
            SQL.AppendLine("    QtyActual-Qty As Qty, QtyActual2-Qty2 As Qty2, QtyActual3-Qty3 As Qty3 ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=IfNull(A.Qty, 0) + ifnull(B.Qty, 0), ");
            SQL.AppendLine("    A.Qty2=IfNull(A.Qty2, 0) + ifnull(B.Qty2, 0), ");
            SQL.AppendLine("    A.Qty3=IfNull(A.Qty3, 0) + ifnull(B.Qty3, 0), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode And B.Source2 Is Not Null;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();

            //Cancel Journal 1
            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    JournalDocNo3=@JournalDocNo1 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo1, DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime()  ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In( ");
            SQL.AppendLine("    Select JournalDocNo From TblStockOpname2Hdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo1, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, ");
            SQL.AppendLine("EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In( ");
            SQL.AppendLine("    Select JournalDocNo From TblStockOpname2Hdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    ); ");


            //Cancel Journal 2
            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    JournalDocNo4=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime()  ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In( ");
            SQL.AppendLine("    Select JournalDocNo2 From TblStockOpname2Hdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("    And JournalDocNo2 is Not Null ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, ");
            SQL.AppendLine("EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In( ");
            SQL.AppendLine("    Select JournalDocNo2 From TblStockOpname2Hdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("    And JournalDocNo2 is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mJournalDocNoFormat == "1")
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo1", Sm.GenerateDocNoJournal(Sm.GetDte(DteClosingDt), 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteClosingDt), 2));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo1", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteClosingDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteClosingDt), 2, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowStockOpname2Hdr(DocNo);
                ShowStockOpname2Dtl(DocNo);
                ComputeTotal();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowStockOpname2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ClosingDt, PriorClosingDt, ProfitCenterCode, CancelInd, CancelReason, ");
            SQL.AppendLine("JournalDocNo, JournalDocNo2, JournalDocNo3, JournalDocNo4 ");
            SQL.AppendLine("From TblStockOpname2Hdr Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "ClosingDt", "PriorClosingDt", "ProfitCenterCode", "CancelInd",

                    //6-10
                    "CancelReason", "JournalDocNo", "JournalDocNo2", "JournalDocNo3", "JournalDocNo4"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[2]));
                    Sm.SetDte(DtePriorClosingDt, Sm.DrStr(dr, c[3]));
                    SetLueProfitCenterCode(ref LueProfitCenterCode, Sm.DrStr(dr, c[4]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[8]);
                    TxtJournalDocNo3.EditValue = Sm.DrStr(dr, c[9]);
                    TxtJournalDocNo4.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        public void ShowStockOpname2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, C.ItCtCode, C.ItCtName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("D.AcNo, D.AcDesc, A.CurCode, A.UPrice, A.UPrice2, (A.QtyActual - A.Qty) As StockBeg, A.QtyActual, A.Qty, ");
            SQL.AppendLine("B.InventoryUOMCode, (A.QtyActual2 - A.Qty2) As StockBeg2, A.QtyActual2, A.Qty2, B.InventoryUOMCode2, ");
            SQL.AppendLine("(A.QtyActual3 - A.Qty3) As StockBeg3, A.QtyActual3, A.Qty3, B.InventoryUOMCode3, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When A.Source2 Is Null Then E.ExcRate ");
            SQL.AppendLine("    When A.Source2 Is Not Null Then F.ExcRate ");
            SQL.AppendLine("End As ExcRate ");
            SQL.AppendLine("From TblStockOpname2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode = C.ItCtCode ");
            SQL.AppendLine("Inner Join TblCoa D On C.AcNo = D.AcNo ");
            SQL.AppendLine("Left Join TblStockPrice E On A.Source = E.Source And A.Source2 Is Null ");
            SQL.AppendLine("Left Join TblStockPrice F On A.Source2 = F.Source And A.Source2 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCtCode", "ItCtName", "BatchNo", "Source",   
                    
                    //6-10
                    "Lot", "Bin", "AcNo", "AcDesc", "CurCode", 
                    
                    //11-15
                    "UPrice", "UPrice2", "StockBeg", "QtyActual", "Qty",
                    
                    //16-20
                    "InventoryUOMCode", "StockBeg2", "QtyActual2", "Qty2", "InventoryUomCode2",
                    
                    //21-25
                    "StockBeg3", "QtyActual3", "Qty3", "InventoryUomCode3", "ExcRate"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 25);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 13, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetFirstStockItem(string WhsCode, string ProfitCenterCode, ref List<StockSummary> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ItCode = string.Empty;

            for(int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                string itCode = Sm.GetGrdStr(Grd1, i, 1);
                if (itCode.Length > 0)
                {
                    if (ItCode.Length > 0) ItCode += ",";
                    ItCode += itCode;
                }
            }

            SQL.AppendLine("Select B.ItCode, C.OptDesc As WhsCode ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("And A.ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine("And Find_In_Set(B.ItCode, @ItCode)  ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCat = 'WhsCodeForProfitCenter' And C.OptCode = A.ProfitCenterCode And C.OptDesc = @WhsCode ");
            SQL.AppendLine("Where Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblStockOpname2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblStockOpname2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And T1.ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblOption T3 On T3.OptCat = 'WhsCodeForProfitCenter' And T3.OptCode = T1.ProfitCenterCode And T3.OptDesc = @WhsCode ");
            SQL.AppendLine("    Where T2.ItCode = B.ItCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Group By B.ItCode, C.OptDesc; ");

            Sm.CmParam<string>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParam<string>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParam<string>(ref cm, "@ItCode", ItCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "WhsCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new StockSummary()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            WhsCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void DeleteStockSummary(ref List<StockSummary> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var cml = new List<MySqlCommand>();
            int i = 0;

            foreach (var x in l)
            {
                SQL.AppendLine("Delete From TblStockSummary Where WhsCode = @WhsCode__" + i.ToString() + " And ItCode = @ItCode__" + i.ToString() +"; ");

                Sm.CmParam<String>(ref cm, "@WhsCode__" + i.ToString(), x.WhsCode);
                Sm.CmParam<String>(ref cm, "@ItCode__" + i.ToString(), x.ItCode);

                cm.CommandText = SQL.ToString();

                cml.Add(cm);

                i++;
            }

            Sm.ExecCommands(cml);

            cml.Clear();
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsBOMShowSpecifications', 'IsAutoJournalActived', 'IsShowForeignName', 'NumberOfInventoryUomCode', 'IsAccountingRptUseJournalPeriod', ");
            SQL.AppendLine("'CurrentEarningFormulaType', 'AcNoForCurrentEarning', 'AcNoForCurrentEarning2', 'AcNoForActiva', 'AcNoForPassiva', 'AcNoForCapital', 'AcNoForIncome', 'AcNoForCost', ");
            SQL.AppendLine("'MaxAccountCategory', 'IsReportingFilterByEntity', 'AcNoForCurrentEarning3', 'AcNoForIncome2', 'FormulaForComputeProfitLoss', 'IsRptAccountingShowJournalMemorial', ");
            SQL.AppendLine("'IsRptTBCalculateFicoNetIncome', 'JournalClosingInventoryPeriodValidation', 'JournalDocNoFormat' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsAccountingRptUseJournalPeriod": mIsAccountingRptUseJournalPeriod = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptTBCalculateFicoNetIncome": mIsRptTBCalculateFicoNetIncome = ParValue == "Y"; break;

                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;

                            //String
                            case "CurrentEarningFormulaType": mCurrentEarningFormulaType = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "AcNoForCurrentEarning2": mAcNoForCurrentEarning2 = ParValue; break;
                            case "AcNoForCurrentEarning3": mAcNoForCurrentEarning3 = ParValue; break;
                            case "AcNoForActiva": mAcNoForActiva = ParValue; break;
                            case "AcNoForPassiva": mAcNoForPassiva = ParValue; break;
                            case "AcNoForCapital": mAcNoForCapital = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForIncome2": mAcNoForIncome2 = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "MaxAccountCategory": mMaxAccountCategory = ParValue; break;
                            case "FormulaForComputeProfitLoss": mFormulaForComputeProfitLoss = ParValue; break;
                            case "JournalClosingInventoryPeriodValidation": mJournalClosingInventoryPeriodValidation = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private void SetLueProfitCenterCode(ref DXE.LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ProfitCenterCode As Col1, ProfitCenterName As Col2 ");
            SQL.AppendLine("From TblProfitCenter A ");
            if (Code.Length > 0)
                SQL.AppendLine("Where A.ProfitCenterCode=@Code ");
            else
            {
                SQL.AppendLine("Inner Join TblOption B On A.ProfitCenterCode = B.OptCode And B.OptCat = 'WhsCodeForProfitCenter' ");
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("    Where ProfitCenterCode=A.ProfitCenterCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By A.ProfitCenterName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }
        internal void ComputeTotal()
        {
            decimal mTotalBeginningBalance = 0m, mTotalEndingBalance = 0m;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                Grd1.Cells[r, 15].Value = Sm.GetGrdDec(Grd1, r, 14) - Sm.GetGrdDec(Grd1, r, 13);
                Grd1.Cells[r, 28].Value = Math.Truncate(Sm.GetGrdDec(Grd1, r, 13) * Sm.GetGrdDec(Grd1, r, 16));//Math.Truncate(Sm.GetGrdDec(Grd1, r, 13)) * Math.Truncate(Sm.GetGrdDec(Grd1, r, 16));
                Grd1.Cells[r, 29].Value = Math.Truncate(Sm.GetGrdDec(Grd1, r, 14) * Sm.GetGrdDec(Grd1, r, 17));//Math.Truncate(Sm.GetGrdDec(Grd1, r, 14)) * Math.Truncate(Sm.GetGrdDec(Grd1, r, 17));
                Grd1.Cells[r, 30].Value = Sm.GetGrdDec(Grd1, r, 29) - Sm.GetGrdDec(Grd1, r, 28);
                
                mTotalBeginningBalance += Math.Truncate(Sm.GetGrdDec(Grd1, r, 28) * Sm.GetGrdDec(Grd1, r, 32));
                mTotalEndingBalance += Math.Truncate(Sm.GetGrdDec(Grd1, r, 29) * Sm.GetGrdDec(Grd1, r, 32));
            }

            TxtBeginningBalanceAmt.EditValue = Sm.FormatNum(mTotalBeginningBalance, 0);
            TxtEndingBalanceAmt.EditValue = Sm.FormatNum(mTotalEndingBalance, 0);
        }

        private string GetItCode()
        {
            string ItCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (ItCode.Length > 0) ItCode += ",";
                ItCode += Sm.GetGrdStr(Grd1, i, 1);
            }
            return ItCode;
        }
        internal string GetItCode2()
        {
            string ItCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 31) == "1")
                {
                    if (ItCode.Length > 0) ItCode += ",";
                    ItCode += Sm.GetGrdStr(Grd1, i, 1);
                }
            }
            return ItCode;
        }
        private string GetItSrcCode()
        {
            var SQL = new StringBuilder();
            string ItCodeTmp = string.Empty,
                   ItCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (ItCode.Length > 0) ItCode += ",";
                ItCode += Sm.GetGrdStr(Grd1, i, 7);
            }

            SQL.AppendLine("Select Group_Concat(C.Source) As ItSrcCode ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblStockSummary C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Where C.WhsCode = @Param1  ");
            SQL.AppendLine("And Find_In_Set(C.Source, @Param2)");
            SQL.AppendLine("And C.Qty <> 0; ");

            ItCode = Sm.GetValue(SQL.ToString(), mWhsCode, ItCode, string.Empty);

            return ItCode;
        }


        #region Process Get Data Trial Balance

        private void ProcessTrialBalance(ref List<COA3> lCOA3)
        {
            var lCOA = new List<COA>();
            var lEntityCOA = new List<EntityCOA>();
            var lCOAPLSettingHdr = new List<COAProfitLossSettingHdr>();
            var lCOAPLSettingDtl = new List<COAProfitLossSettingDtl>();
            string AcNoPL = string.Empty;

            var Yr = Sm.Left(Sm.GetDte(DteClosingDt), 4);
            var Mth = Sm.GetDte(DteClosingDt).Substring(4,2);
            var IsFilterByEntity = mIsReportingFilterByEntity && mEntCode.Length > 0;

            Process0(ref lCOA3);
            if (lCOA3.Count > 0)
            {
                Process1(ref lCOA);
                if (mFormulaForComputeProfitLoss == "2")
                {
                    GetCOAProfitLossSettingHdr(ref lCOAPLSettingHdr);
                    GetCOAProfitLossSettingDtl(ref lCOAPLSettingDtl);
                    AcNoPL = GetSelectedCOAPLSetting(ref lCOAPLSettingHdr);
                }
                if (IsFilterByEntity)
                    Process8(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    Process2(ref lCOA, Yr);
                    if (mIsRptAccountingShowJournalMemorial)
                        Process3_JournalMemorial(ref lCOA, Yr, Mth);
                    else
                        Process3(ref lCOA, Yr, Mth);
                    Process4(ref lCOA, Yr, Mth);
                    Process5(ref lCOA);
                    if (mIsRptTBCalculateFicoNetIncome)
                        Process5e(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);
                    if (mFormulaForComputeProfitLoss == "2")
                    {
                        ComputeProfitLoss(ref lCOAPLSettingHdr, ref lCOAPLSettingDtl, ref lCOA);
                        ComputeProfitLoss2(ref lCOAPLSettingHdr, ref lCOA);
                        Process12(ref lCOA, ref lCOAPLSettingHdr, AcNoPL);
                    }
                }
                Process13(ref lCOA, ref lCOA3);
            }
        }

        private void Process0(ref List<COA3> lCOA3)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select C.AcNo As AcNoStock, D.AcNo As AcNoBeg ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Left Join TblCOA C On B.AcNo = C.AcNo And C.AcNo Is Not Null ");
            SQL.AppendLine("Left Join TblCOA D On B.AcNo12 = D.AcNo And D.AcNo Is Not Null ");
            SQL.AppendLine("Where Find_In_Set(A.ItCode, @ItCode) ");
            SQL.AppendLine("Group By C.AcNo, D.AcNo ");
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<string>(ref cm, "@ItCode", GetItCode());
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNoStock", "AcNoBeg" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA3.Add(new COA3()
                        {
                            AcNoStock = Sm.DrStr(dr, c[0]),
                            AcNoBeg = Sm.DrStr(dr, c[1]),
                            YearToDtBalance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.Alias, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Where A.ActInd='Y'  ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Alias", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Alias = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[3]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("And A.ProfitCenterCode = @ProfitCenterCode ");
            if(mIsReportingFilterByEntity && mEntCode.Length > 0)
                SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            SQL.AppendLine("Order By C.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
            }
            SQL.AppendLine("And A.CCCode Is Not Null ");
            SQL.AppendLine("And A.CCCode In ( ");
            SQL.AppendLine("    Select Distinct CCCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where ActInd='Y' ");
            SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("    And ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr,Mth));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("From ( ");

            #region Journal

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
            }
            SQL.AppendLine("And A.CCCode Is Not Null ");
            SQL.AppendLine("And A.CCCode In ( ");
            SQL.AppendLine("    Select Distinct CCCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where ActInd='Y' ");
            SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("    And ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine(") ");

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
            }
            SQL.AppendLine("And A.CCCode Is Not Null ");
            SQL.AppendLine("And A.CCCode In ( ");
            SQL.AppendLine("    Select Distinct CCCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where ActInd='Y' ");
            SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("    And ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine(") ");

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
            }
            if (mIsReportingFilterByEntity && mEntCode.Length > 0) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And A.CCCode Is Not Null ");
            SQL.AppendLine("And A.CCCode In ( ");
            SQL.AppendLine("    Select Distinct CCCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where ActInd='Y' ");
            SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("    And ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Group By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            if (mCurrentEarningFormulaType == "2")
            {
                if (
                    mAcNoForCurrentEarning.Length == 0 ||
                    mAcNoForCurrentEarning2.Length == 0 ||
                    mAcNoForActiva.Length == 0 ||
                    mAcNoForPassiva.Length == 0 ||
                    mAcNoForCapital.Length == 0
                    ) return;

                Process5c(ref lCOA);
            }
            else
            {
                if (mCurrentEarningFormulaType == "3")
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForActiva.Length == 0 ||
                        mAcNoForPassiva.Length == 0 ||
                        mAcNoForCapital.Length == 0
                        ) return;

                    Process5d(ref lCOA);
                }
                else
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForCurrentEarning2.Length == 0 ||
                        mAcNoForIncome.Length == 0 ||
                        mAcNoForCost.Length == 0
                        ) return;

                    Process5a(ref lCOA);
                    Process5b(ref lCOA);
                }
            }
        }

        private void Process5a(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5b(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5c(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                CurrentProfiLossIndex2 = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory > 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory > 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory > 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            var CurrentProfiLossParentIndex2 = -1;
            var CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            var f2 = true;

            //Month To Date
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].MonthToDateDAmt - lCOA[IncomeIndex].MonthToDateCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].MonthToDateCAmt - lCOA[CostIndex1].MonthToDateDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].MonthToDateCAmt - lCOA[CostIndex2].MonthToDateDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].MonthToDateCAmt - lCOA[CostIndex3].MonthToDateDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].MonthToDateCAmt - lCOA[CostIndex4].MonthToDateDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].MonthToDateDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].MonthToDateCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].CurrentMonthDAmt - lCOA[IncomeIndex].CurrentMonthCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].CurrentMonthCAmt - lCOA[CostIndex1].CurrentMonthDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].CurrentMonthCAmt - lCOA[CostIndex2].CurrentMonthDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].CurrentMonthCAmt - lCOA[CostIndex3].CurrentMonthDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].CurrentMonthCAmt - lCOA[CostIndex4].CurrentMonthDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].CurrentMonthDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].CurrentMonthCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5d(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory >= 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }


                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory >= 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory >= 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }


            //Current Month
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5e(ref List<COA> lCOA)
        {
            var lCOATemp2 = new List<COATemp>();
            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();

            //SetProfitCenter();
            string mListAcNo = "4,5,6,7,8,9.04.01.01.01.01.01.01.01.01.03,9.02,9.03";
            string mUsedAcNo = string.Empty;
            decimal mYearToDateBalance = 0m;

            var Yr = Sm.Left(Sm.GetDte(DteClosingDt), 4);
            var Mth = Sm.GetDte(DteClosingDt).Substring(4,2);
            var StartFrom = Yr;

            GetAllCOA(ref lCOATemp, mListAcNo);
            if (lCOATemp.Count > 0)
            {
                foreach (var i in lCOATemp)
                {
                    if (mUsedAcNo.Length > 0) mUsedAcNo += ",";
                    mUsedAcNo += i.AcNo;
                }
            }

            if (StartFrom.Length > 0)
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, StartFrom);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, StartFrom);
            }
            else
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, Yr);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, string.Empty);
            }

            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, mUsedAcNo, Yr, Mth);

            Process5f(ref lCOATemp, ref lCOATemp2);
            Process5g(ref lCOAOpeningBalanceTemp, ref lCOATemp2);
            Process5h(ref lJournalTemp, ref lCOATemp2);
            Process5i(ref lJournalCurrentMonthTemp, ref lCOATemp2);
            Process5j(ref lCOATemp2);
            Process5k(ref lCOATemp2);
            Process5l(ref lCOATemp2, ref mYearToDateBalance);
            Process5m(ref lCOA, ref mYearToDateBalance);
        }

        private void Process5f(ref List<COA2> lCOATemp, ref List<COATemp> lCOATemp2)
        {
            foreach (var y in lCOATemp)
            {
                lCOATemp2.Add(new COATemp()
                {
                    AcNo = y.AcNo,
                    AcDesc = y.AcDesc,
                    AcType = y.AcType,
                    HasChild = y.HasChild,
                    Level = y.Level,
                    Parent = y.Parent,
                    CurrentMonthBalance = y.CurrentMonthBalance,
                    CurrentMonthCAmt = y.CurrentMonthCAmt,
                    CurrentMonthDAmt = y.CurrentMonthDAmt,
                    MonthToDateBalance = y.MonthToDateBalance,
                    MonthToDateCAmt = y.MonthToDateCAmt,
                    MonthToDateDAmt = y.MonthToDateDAmt,
                    OpeningBalanceCAmt = y.OpeningBalanceCAmt,
                    OpeningBalanceDAmt = y.OpeningBalanceDAmt,
                    YearToDateBalance = y.YearToDateBalance,
                    YearToDateCAmt = y.YearToDateCAmt,
                    YearToDateDAmt = y.YearToDateDAmt
                });
            }
        }

        private void Process5g(ref List<COAOpeningBalance2> l, ref List<COATemp> lCOATemp2) //Opening Balance
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5h(ref List<Journal2> l, ref List<COATemp> lCOATemp2) //Journal Month To Date
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5i(ref List<Journal3> l, ref List<COATemp> lCOATemp2) //Journal Current Month
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5j(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                lCOATemp2[i].YearToDateDAmt =
                    lCOATemp2[i].OpeningBalanceDAmt +
                    lCOATemp2[i].MonthToDateDAmt +
                    lCOATemp2[i].CurrentMonthDAmt;

                lCOATemp2[i].YearToDateCAmt =
                    lCOATemp2[i].OpeningBalanceCAmt +
                    lCOATemp2[i].MonthToDateCAmt +
                    lCOATemp2[i].CurrentMonthCAmt;
            }
        }

        private void Process5k(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                if (lCOATemp2[i].AcType == "D")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceDAmt +
                        lCOATemp2[i].MonthToDateDAmt -
                        lCOATemp2[i].MonthToDateCAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthDAmt -
                        lCOATemp2[i].CurrentMonthCAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateDAmt - lCOATemp2[i].YearToDateCAmt;
                }
                if (lCOATemp2[i].AcType == "C")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceCAmt +
                        lCOATemp2[i].MonthToDateCAmt -
                        lCOATemp2[i].MonthToDateDAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthCAmt -
                        lCOATemp2[i].CurrentMonthDAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateCAmt - lCOATemp2[i].YearToDateDAmt;
                }
            }
        }

        private void Process5l(ref List<COATemp> lCOATemp2, ref decimal YearToDateBalance)
        {
            char[] delimiters = { '+', '-', '(', ')' };
            string SQLFormula = "#4#-#5#-#6#-#7#+#8#-#9.04.01.01.01.01.01.01.01.01.03#-#9.02#-#9.03#";
            string[] ArraySQLFormula = SQLFormula.Split(delimiters);
            var lFormula = new List<Formula>();

            ArraySQLFormula = ArraySQLFormula.Where(val => val != "").ToArray();
            for (int i = 0; i < ArraySQLFormula.Count(); i++)
                lFormula.Add(new Formula() { AcNo = ArraySQLFormula[i], Amt = 0m });

            for (int ind = 0; ind < lFormula.Count; ind++)
                for (var h = 0; h < lCOATemp2.Count; h++)
                    if (lFormula[ind].AcNo == string.Concat("#", lCOATemp2[h].AcNo, "#"))
                        lFormula[ind].Amt = lCOATemp2[h].YearToDateBalance;

            for (int j = 0; j < ArraySQLFormula.Count(); j++)
            {
                for (int k = 0; k < lFormula.Count; k++)
                {
                    if (ArraySQLFormula[j].ToString() == lFormula[k].AcNo)
                    {
                        string oldS = ArraySQLFormula[j].ToString();
                        string newS = lFormula[k].Amt.ToString();
                        SQLFormula = SQLFormula.Replace(oldS, newS);
                    }
                }
            }
            decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            YearToDateBalance = baltemp;
        }

        private void Process5m(ref List<COA> lCOA, ref decimal YearToDateBalance)
        {
            if (mAcNoForCurrentEarning3.Length == 0 || mAcNoForIncome2.Length == 0) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning3) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome2) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }
            }
            decimal Amt = 0m;
            Amt = YearToDateBalance;

            //Laba rugi tahun berjalan
            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            if (lCOA[CurrentProfiLossIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        if (lCOA[CurrentProfiLossIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Laba rugi - penghasilan
            var IncomeParentIndex = -1;
            var IncomeParentAcNo = lCOA[IncomeIndex].Parent;
            f = true;

            if (lCOA[IncomeIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, IncomeParentAcNo) == 0)
                    {
                        IncomeParentIndex = i;
                        if (lCOA[IncomeIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        IncomeParentAcNo = lCOA[IncomeParentIndex].Parent;
                        f = IncomeParentAcNo.Length > 0;
                        break;
                    }
                }
            }
        }

        private void GetAllCOA(ref List<COA2> l, string ListAcNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
            string[] AcNo = ListAcNo.Split(',');

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                if (mIsReportingFilterByEntity && mEntCode.Length > 0)
                {
                    SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("And B.EntCode=@EntCode ");

                    if (mIsReportingFilterByEntity)
                    {
                        SQL.AppendLine("And B.EntCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And ( ");
                for (int x = 0; x < AcNo.Count(); ++x)
                {
                    SQL.AppendLine("(A.AcNo Like '" + AcNo[x] + "%') ");
                    if (x != AcNo.Count() - 1)
                        SQL.AppendLine(" Or ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournal(ref List<Journal2> l, string ListAcNo, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }

            SQL.AppendLine("    And A.CCCode Is Not Null ");
            SQL.AppendLine("    And A.CCCode In ( ");
            SQL.AppendLine("        Select Distinct CCCode ");
            SQL.AppendLine("        From TblCostCenter ");
            SQL.AppendLine("        Where ActInd='Y' ");
            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("        And ProfitCenterCode=@ProfitCenterCode) ");
            SQL.AppendLine("    ) ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string ListAcNo, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");

            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
            {
                SQL.AppendLine("    And A.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And A.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("And A.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("And A.ProfitCenterCode = @ProfitCenterCode ");
            
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string ListAcNo, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth ");
            SQL.AppendLine("    And A.CCCode Is Not Null ");
            SQL.AppendLine("    And A.CCCode In ( ");
            SQL.AppendLine("        Select Distinct CCCode ");
            SQL.AppendLine("        From TblCostCenter ");
            SQL.AppendLine("        Where ActInd='Y' ");
            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
            SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                {
                    int MaxAccountCategory = int.Parse(mMaxAccountCategory);

                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (mIsReportingFilterByEntity && mEntCode.Length > 0)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (mIsReportingFilterByEntity && mEntCode.Length > 0) Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt -
                        //tambahan berdasarkan OpeningBalanceCAmt bit.ly/31WjiQS
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt -
                        //tambahan MonthToDateCAmt berdasarkan bit.ly/31WjiQS

                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateCAmt -
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }

                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                {
                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
            }
        }

        private string GetSelectedCOAPLSetting(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr)
        {
            string AcNo = string.Empty;

            foreach (var x in lCOAProfitLossSettingHdr)
            {
                if (AcNo.Length > 0)
                    AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }


        private void GetCOAProfitLossSettingDtl(ref List<COAProfitLossSettingDtl> lCOAProfitLossSettingDtl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    Select Tb1.SettingCode, Tb1.AcNo, Tb1.ProfitLossSetting, tb2.Level, tb2.parent, tb1.ProcessInd From ( ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'plus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND ProfitLossInd = 'Y' AND processind = 'Y'  ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'minus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN tblprofitlosssettingdtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND ProfitLossInd = 'Y' AND processind = 'Y'  ");
            SQL.AppendLine("    )tb1 ");
            SQL.AppendLine("    Inner Join TblCoa tb2 On Tb1.Acno = tb2.acNo ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteClosingDt), 4));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "ProfitLossSetting", "ProcessInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingDtl.Add(new COAProfitLossSettingDtl()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            ProfitLossSetting = Sm.DrStr(dr, c[2]),
                            ProcessInd = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }
        
        private void GetCOAProfitLossSettingHdr(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    SELECT A.SettingCode, A.CurrentEarningAcno As AcNo, B.AcType");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblCoa B On A.CurrentEarningAcno = B.Acno");
            SQL.AppendLine("    WHERE A.Yr = @Yr AND A.ActInd = 'Y'  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteClosingDt), 4));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingHdr.Add(new COAProfitLossSettingHdr()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            ProfitLossSetting = "Y",
                            ProcessInd = "Y",
                            OpeningBalanceCAmt = 0,
                            OpeningBalanceDAmt = 0,
                            MonthToDateDAmt = 0,
                            MonthToDateCAmt = 0,
                            MonthToDateBalance = 0,
                            CurrentMonthDAmt = 0,
                            CurrentMonthCAmt = 0,
                            CurrentMonthBalance = 0,
                            YearToDateDAmt = 0,
                            YearToDateCAmt = 0,
                            YearToDateBalance = 0,
                        });
                    }
                }
                dr.Close();
            }
        }
        
        private void ComputeProfitLoss(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COAProfitLossSettingDtl> lCOAPLSettingDtl, ref List<COA> lCOA)
        {
            decimal penambahMTD = 0;
            decimal pengurangMTD = 0;
            decimal penambahCMD = 0;
            decimal pengurangCMD = 0;
            decimal penambahYTD = 0;
            decimal pengurangYTD = 0;

            foreach (var h in lCOAPLSettingHdr)
            {
                foreach (var d in lCOAPLSettingDtl.OrderBy(d2 => d2.SettingCode).ThenBy(d3 => d3.ProfitLossSetting))
                {
                    if (h.SettingCode == d.SettingCode)
                    {
                        foreach (var c in lCOA)
                        {
                            if (d.AcNo == c.AcNo)
                            {
                                if (d.ProfitLossSetting == "plus")
                                {
                                    penambahMTD += c.MonthToDateBalance;
                                    penambahCMD += c.CurrentMonthBalance;
                                    penambahYTD += c.YearToDateBalance;
                                }
                                else
                                {
                                    pengurangMTD += c.MonthToDateBalance;
                                    pengurangCMD += c.CurrentMonthBalance;
                                    pengurangYTD += c.YearToDateBalance;
                                }
                            }
                        }
                    }
                }
                h.OpeningBalanceCAmt = 0m;
                h.OpeningBalanceDAmt = 0m;
                h.MonthToDateBalance = penambahMTD - pengurangMTD;
                h.CurrentMonthBalance = penambahCMD - pengurangCMD;
                h.YearToDateBalance = penambahYTD - pengurangYTD;
                if (h.AcType == "D")
                {
                    h.CurrentMonthDAmt = h.CurrentMonthBalance;
                    h.CurrentMonthCAmt = 0m;
                    h.MonthToDateDAmt = h.MonthToDateBalance;
                    h.MonthToDateCAmt = 0m;
                    h.YearToDateDAmt = h.YearToDateBalance;
                    h.YearToDateCAmt = 0m;
                }
                else
                {
                    h.CurrentMonthCAmt = h.CurrentMonthBalance;
                    h.CurrentMonthDAmt = 0m;
                    h.MonthToDateCAmt = h.MonthToDateBalance;
                    h.MonthToDateDAmt = 0m;
                    h.YearToDateCAmt = h.YearToDateBalance;
                    h.YearToDateDAmt = 0m;
                }
            }
        }
        
        private void ComputeProfitLoss2(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COA> lCOA)
        {

            foreach (var h in lCOAPLSettingHdr.OrderBy(h2 => h2.SettingCode).ThenBy(h3 => h3.AcNo))
            {
                foreach (var c in lCOA.OrderBy(c2 => c2.AcNo))
                {
                    if (h.AcNo == c.AcNo)
                    {
                        //yg ditarik hanya year to date saja
                        //c.OpeningBalanceCAmt = h.OpeningBalanceCAmt;
                        //c.OpeningBalanceDAmt = h.OpeningBalanceDAmt;
                        //c.MonthToDateDAmt = h.MonthToDateDAmt;
                        //c.MonthToDateCAmt = h.MonthToDateCAmt;
                        //c.MonthToDateBalance = h.MonthToDateBalance;
                        //c.CurrentMonthDAmt = h.CurrentMonthDAmt;
                        //c.CurrentMonthCAmt = h.CurrentMonthCAmt;
                        //c.CurrentMonthBalance = h.CurrentMonthBalance;
                        c.YearToDateDAmt = h.YearToDateDAmt;
                        c.YearToDateCAmt = h.YearToDateCAmt;
                        c.YearToDateBalance = h.YearToDateBalance;
                    }
                }
            }

        }
        
        private void Process11(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr)
        {
            if (lCOAPLSettingHdr.Count > 0)
            {
                lCOAPLSettingHdr.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lCOAPLSettingHdr.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lCOAPLSettingHdr[i].AcNo, lCOA[j].AcNo) ||
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lCOAPLSettingHdr[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            //lCOA[j].OpeningBalanceDAmt += lCOAPLSettingHdr[i].OpeningBalanceDAmt;
                            //lCOA[j].OpeningBalanceCAmt += lCOAPLSettingHdr[i].OpeningBalanceCAmt;


                            //lCOA[j].MonthToDateDAmt += lCOAPLSettingHdr[i].MonthToDateDAmt;
                            //lCOA[j].MonthToDateCAmt += lCOAPLSettingHdr[i].MonthToDateCAmt;
                            //lCOA[j].MonthToDateBalance += lCOAPLSettingHdr[i].MonthToDateBalance;

                            //lCOA[j].CurrentMonthDAmt += lCOAPLSettingHdr[i].CurrentMonthDAmt;
                            //lCOA[j].CurrentMonthCAmt += lCOAPLSettingHdr[i].CurrentMonthCAmt;
                            //lCOA[j].CurrentMonthBalance += lCOAPLSettingHdr[i].CurrentMonthBalance;


                            lCOA[j].YearToDateDAmt += lCOAPLSettingHdr[i].YearToDateDAmt;
                            lCOA[j].YearToDateCAmt += lCOAPLSettingHdr[i].YearToDateCAmt;
                            lCOA[j].YearToDateBalance += lCOAPLSettingHdr[i].YearToDateBalance;

                            if (string.Compare(lCOA[j].AcNo, lCOAPLSettingHdr[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
        }
        
        private void Process12(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, string GetSelectedCOAPLSetting)
        {
            foreach (var x in lCOAPLSettingHdr.OrderBy(w => w.AcNo))
            {
                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                {
                    if (x.AcNo.Count(y => y == '.') != i.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(i.AcNo))
                    {
                        //i.OpeningBalanceDAmt = 0m;
                        //i.OpeningBalanceCAmt = 0m;

                        //i.MonthToDateDAmt = 0m;
                        //i.MonthToDateCAmt =0m;
                        //i.MonthToDateBalance =0m;

                        //i.CurrentMonthDAmt = 0m;
                        //i.CurrentMonthCAmt = 0m;
                        //i.CurrentMonthBalance = 0m;


                        i.YearToDateDAmt = 0m;
                        i.YearToDateCAmt = 0m;
                        i.YearToDateBalance = 0m;


                    }
                }

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && !w.HasChild && x.ProfitLossSetting == "Y"))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                    {
                        if (x.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(j.AcNo))
                        {
                            if (
                                i.AcNo.Count(y => y == '.') == j.AcNo.Count(y => y == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                                i.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                            {
                                //j.OpeningBalanceDAmt += i.OpeningBalanceDAmt; 
                                //j.OpeningBalanceCAmt += i.OpeningBalanceCAmt;

                                //j.MonthToDateDAmt += i.MonthToDateDAmt;
                                //j.MonthToDateCAmt += i.MonthToDateCAmt;
                                //j.MonthToDateBalance += i.MonthToDateBalance;

                                //j.CurrentMonthDAmt += i.CurrentMonthDAmt;
                                //j.CurrentMonthCAmt += i.CurrentMonthCAmt;
                                //j.CurrentMonthBalance += i.CurrentMonthBalance;

                                j.YearToDateDAmt += i.YearToDateDAmt;
                                j.YearToDateCAmt += i.YearToDateCAmt;
                                j.YearToDateBalance += i.YearToDateBalance;
                                if (string.Compare(j.AcNo, i.AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }


            }
        }

        private void Process13(ref List<COA> lCOA, ref List<COA3> lCOA3)
        {
            foreach (var x in lCOA3)
            {
                foreach (var y in lCOA)
                {
                    if (y.AcNo == x.AcNoBeg)
                    {
                        x.YearToDtBalance = y.YearToDateBalance*-1;
                        break;
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Button Click

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal Closing#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal Beginning#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo3_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo3, "Cancel Journal Closing#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo3.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo4_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo4, "Cancel Journal Beginning#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo4.Text;
                f.ShowDialog();
            }
        }

        #endregion


        #region Misc Control Event
        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue2(SetLueProfitCenterCode), string.Empty);
            mWhsCode = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'WhsCodeForProfitCenter' And OptCode = @Param;", Sm.GetLue(LueProfitCenterCode));
            ClearGrd();
            ComputeTotal();
        }

        private void DteClosingDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && DteClosingDt.Text.Length > 0)
            {
                DateTime ClosingDt = DateTime.ParseExact(Sm.GetDte(DteClosingDt).Substring(0, 8), "yyyyMMdd", null).AddMonths(-1);
                string PriorClosingDt = Sm.FormatDate(ClosingDt);
                Sm.SetDte(DtePriorClosingDt, PriorClosingDt);
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Event
        private void Grd1_RequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 14)
                e.Text = "Item's Price (End) can only be editable " + Environment.NewLine +
                    "if the related item is profiled as Non-Purchase Item" + Environment.NewLine +
                    "in Master Item menu.";
        }
        #endregion

        #endregion

        #region Class

        private class StockSummary
        {
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }

            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }

            public string ProcessInd { get; set; }

            public string ProfitLossSetting { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COATemp
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Formula
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class COAProfitLossSettingDtl
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }

        }

        private class COAProfitLossSettingHdr
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }
            public string AcType { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }

        }

        private class COA3
        {
            public string AcNoStock { get; set; }
            public string AcNoBeg { get; set; }
            public decimal YearToDtBalance { get; set; }
        }

        #endregion
    }
}
